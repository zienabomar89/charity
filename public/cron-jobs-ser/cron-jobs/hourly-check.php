<?php

$DB_HOST     = "localhost";
$DB_USER     = "takaful_sys_usr";
$DB_PASSWORD = "At3{*6]PAS5U";
$DB_NAME     = "takaful_db_sys";

// CREATE DB CONNECTION
$connection = mysqli_connect($DB_HOST,$DB_USER,$DB_PASSWORD,$DB_NAME);

mysqli_query($connection,"SET CHARACTER SET 'utf8'");
mysqli_query($connection,"SET SESSION collation_connection ='utf8_unicode_ci'");

ini_set('max_execution_time', 0);
set_time_limit(0);
Ini_set('memory_limit','2048M');


$str_replace = "update char_persons set street_address = Replace(street_address, '-', '_') where street_address like '%-%'";
mysqli_query($connection, $str_replace);


$null = "UPDATE char_persons SET gov_commercial_records_details =null WHERE (active_commercial_records = 0 or has_commercial_records = 0)";
mysqli_query($connection, $null);


$null = "UPDATE char_persons SET spouses =0 WHERE spouses is null";
mysqli_query($connection, $null);


$null = "UPDATE char_persons SET female_live =0 WHERE female_live is null";
mysqli_query($connection, $null);


$null = "UPDATE char_persons SET male_live =0 WHERE male_live is null";
mysqli_query($connection, $null);

$death_spouses = "UPDATE char_persons SET spouses =0 , family_cnt = ( female_live  + male_live + 1 )  ,
                                         family_ ='updated' , updated_at = NOW()
                                         WHERE spouses != 30 and death_date is not null";
mysqli_query($connection, $death_spouses);

// ****************************************************************************************** //
$null = "UPDATE char_persons SET family_cnt = ( female_live  + male_live + 1 )   WHERE family_cnt is null or family_cnt = 0 ";
mysqli_query($connection, $null);

// ****************************************************************************************** //
$single = "UPDATE char_persons SET family_cnt =1 , spouses =0 , female_live = 0 , male_live =0 ,
                                        family_ ='updated' , updated_at = NOW()
                                         WHERE marital_status_id= 10 or marital_status_id is null";
mysqli_query($connection, $single);

// ****************************************************************************************** //
$divorced_spouses = "UPDATE char_persons SET spouses =0 , family_cnt = ( female_live  + male_live + 1 )  ,
                                         family_ ='updated' , updated_at = NOW()
                                         WHERE marital_status_id= 30 ";
mysqli_query($connection, $divorced_spouses);

// ****************************************************************************************** //
$divorced_spouses_kin = "DELETE FROM char_persons_kinship
                             where (l_person_id in (SELECT id from char_persons WHERE marital_status_id = 30) and kinship_id in (21,29)) or 
                                   (r_person_id in (SELECT id from char_persons WHERE marital_status_id = 30) and kinship_id in (21,29)) ";
mysqli_query($connection, $divorced_spouses_kin);

// ****************************************************************************************** //
$death_case_disable = "UPDATE char_cases SET status =1 , reason ='الشخص المسجلة باسمه الحالة متوفى' , updated_at = NOW()
                         WHERE person_id in (select id from char_persons where death_date is not null) ";
mysqli_query($connection, $death_case_disable);

// ****************************************************************************************** //
$case_disable = " INSERT INTO `char_cases_status_log` (`id`,`case_id`,`user_id`,`status`,`date`,`reason`,`created_at`)
                  SELECT null as id ,  id as case_id  , 1 as user_id , DATE_FORMAT(NOW(), '%Y-%m-%d') as date , 1 as status , 'العنوان غير مكتمل ، يجب تصحيح العنوان ضمن نطاق جمعيتك لتتمكن من تفعيله' reason , now() as created_at
                  FROM `char_cases` WHERE status = 0 and category_id in ( select id from char_categories WHERE type = 2 ) and person_id in ( SELECT id from char_persons WHERE death_date is null and 
                 (adsmosques_id is null or adssquare_id is null or adsneighborhood_id is null or adsregion_id is null or adsdistrict_id is null or adscountry_id is null)  )  
                ";
mysqli_query($connection, $case_disable);

// ****************************************************************************************** //

$case_disable = " UPDATE char_cases SET status =1 , reason = 'العنوان غير مكتمل ، يجب تصحيح العنوان ضمن نطاق جمعيتك لتتمكن من تفعيله' , updated_at = NOW()
                  WHERE status = 0 and category_id in ( select id from char_categories WHERE type = 2 ) and person_id in ( SELECT id from char_persons WHERE death_date is null and 
                  (adsmosques_id is null or adssquare_id is null or adsneighborhood_id is null or adsregion_id is null or adsdistrict_id is null or adscountry_id is null)  )
                ";
mysqli_query($connection, $case_disable);


// ****************************************************************************************** //
$death_case_log = "INSERT INTO `char_cases_status_log`
                (`id`,`case_id`,`user_id`,`status`,`date`,`reason`,`created_at`)
                 
                 SELECT  null as id , id as case_id , 1 as user_id , 1 as status , DATE_FORMAT(NOW(), '%Y-%m-%d') as date 
                 , 'الشخص المسجلة باسمه الحالة متوفى' as reason , now() as created_at FROM char_cases 
                 where  reason = 'الشخص المسجلة باسمه الحالة متوفى' 
                 and id not in ( select case_id from char_cases_status_log where char_cases_status_log.reason = 'الشخص المسجلة باسمه الحالة متوفى')
                ;
                ";

mysqli_query($connection, $death_case_log);

// ****************************************************************************************** //
$single_case_disable = "UPDATE char_cases SET status =1 , reason ='رقم الهوية مسجل لشخص أعزب أو لم تحدد حالته الاجتماعية' , updated_at = NOW()
                           WHERE category_id = 5 and person_id in (select id from char_persons where marital_status_id= 10 or marital_status_id is null) ";
mysqli_query($connection, $single_case_disable);

// ****************************************************************************************** //
$single_case_disable_log = "INSERT INTO `char_cases_status_log`
                (`id`,`case_id`,`user_id`,`status`,`date`,`reason`,`created_at`)
                 
                 SELECT  null as id , id as case_id , 1 as user_id , 1 as status , DATE_FORMAT(NOW(), '%Y-%m-%d') as date 
                 , 'رقم الهوية مسجل لشخص أعزب أو لم تحدد حالته الاجتماعية' as reason , now() as created_at FROM char_cases 
                 where  reason = 'رقم الهوية مسجل لشخص أعزب أو لم تحدد حالته الاجتماعية'
                 and id not in ( select case_id from char_cases_status_log where char_cases_status_log.reason like '%رقم الهوية مسجل لشخص أعزب أو لم تحدد حالته الاجتماعية%')
                ;
                ";

mysqli_query($connection, $single_case_disable_log);

mysqli_close($connection);


?>
