/***
GLobal Directives
***/

// Route State Load Spinner(used on page or content load)
CharityApp.directive('ngSpinnerBar', ['$rootScope', '$http',
    function($rootScope, $http) {
        return {
            link: function(scope, element, attrs) {
                // by defult hide the spinner bar
                element.addClass('hide'); // hide spinner bar by default

                // display the spinner bar whenever the route changes(the content part started loading)
                $rootScope.$on('$stateChangeStart', function() {
                    element.removeClass('hide'); // show spinner bar
                });
                
                $http.defaults.transformRequest.push(function (data) {
                    element.removeClass('hide');
                    return data;
                });

                $http.defaults.transformResponse.push(function(data){ 
                    if($http.pendingRequests.length < 1) {
                        element.addClass('hide');
                    }
                    return data;
                });

                // hide the spinner bar on rounte change success(after the content loaded)
                $rootScope.$on('$stateChangeSuccess', function() {
                    element.addClass('hide'); // hide spinner bar
                    $('body').removeClass('page-on-load'); // remove page loading indicator
                    Layout.setSidebarMenuActiveLink('match'); // activate selected link in the sidebar menu
                   
                    // auto scorll to page top
                    setTimeout(function () {
                        App.scrollTop(); // scroll to the top on content load
                    }, $rootScope.settings.layout.pageAutoScrollOnLoad);     
                });

                // handle errors
                $rootScope.$on('$stateNotFound', function() {
                    element.addClass('hide'); // hide spinner bar
                });

                // handle errors
                $rootScope.$on('$stateChangeError', function() {
                    element.addClass('hide'); // hide spinner bar
                });
            }
        };
    }
]);

// Handle global LINK click
CharityApp.directive('a', function() {
    return {
        restrict: 'E',
        link: function(scope, elem, attrs) {
            if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                elem.on('click', function(e) {
                    e.preventDefault(); // prevent link click for above criteria
                });
            }
        }
    };
});

// Handle Dropdown Hover Plugin Integration
CharityApp.directive('dropdownMenuHover', function () {
  return {
    link: function (scope, elem) {
      elem.dropdownHover();
    }
  };  
});

CharityApp.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value);
      });
    }
  };
});

CharityApp.directive('restrictInput', [function(){

            return {
                restrict: 'A',
                link: function (scope, element, attrs) {
                    var ele = element[0];
                    var regex = RegExp(attrs.restrictInput);
                    var value = ele.value;

                    ele.addEventListener('keyup',function(e){
                        if (regex.test(ele.value)){
                            value = ele.value;
                        }else{
                            ele.value = value;
                        }
                    });
                }
            };
        }]);
        
CharityApp.directive('numericOnly', [function(){
    return {
        restrict: 'A',
        link: function (scope, element, attrs, ctrl) {
            element.on('cut copy paste', function (e) {
                return true;
            });
            element.on('keydown', function (event) {
                var ew =event.which;
                if (ew == 64 || ew == 16) {
                    return false;
                }
                if (((event.keyCode == 65 || event.keyCode == 86 || event.keyCode == 67|| event.keyCode == 88) &&
                    (event.ctrlKey === true || event.metaKey === true)) ||
                    (event.keyCode >= 35 && event.keyCode <= 40) ||
                    ([8, 13, 27, 37, 38, 39, 40, 110,9].indexOf(ew) > -1) ||(ew >= 48 && ew <= 57) || (ew >= 96 && ew <= 105) ) {
                    return true;
                } else {
                    event.preventDefault();
                    return false;
                }
            });
        }
    }
}]);

CharityApp.directive('decimalNumbers', [function(){
    return {
        restrict: 'A',
        link: function (scope, element, attrs, ctrl) {
            element.on('cut copy paste', function (e) {
                return true;
            });
            element.on('keypress', function (event) {
                var $input = $(this);
                var value = $input.val();
                value = value.replace(/[^0-9\.]/g, '');
                var findsDot = new RegExp(/\./g);
                var containsDot = value.match(findsDot);
                if (containsDot != null && ([46, 110, 190].indexOf(event.which) > -1)) {
                    event.preventDefault();
                    return false;
                }
                if (event.which == 64 || event.which == 16) {
                    return false;
                }

                if (
                    (event.keyCode == 65 && event.ctrlKey === true) ||
                    (event.keyCode == 88 && event.ctrlKey === true) ||
                    (event.keyCode == 67 && event.ctrlKey === true) ||
                    (event.keyCode == 86 && event.ctrlKey === true) ||
                    (event.keyCode >= 35 && event.keyCode <= 39)    ||
                    ([8,9, 13, 27, 37, 38, 39, 40, 110].indexOf(event.which) > -1) ||
                    ([46, 110, 190].indexOf(event.which) > -1) ||
                    (event.which >= 96 && event.which <= 105)  ||
                    (event.which >= 48 && event.which <= 57)
                ) {
                    return true;
                }else {
                    event.preventDefault();
                    return false;
                }
                $input.val(value);

            });
        }
    }
}]);

CharityApp.directive('onlyAlphabet', [function(){
    return {
        restrict: 'A',
        link: function (scope, element, attrs, ctrl) {
            element.on('cut copy paste', function (e) {
                return true;
            });
            element.on('keypress', function (event) {
                var ew = event.which;
                var shiftKey = event.shiftKey;

                if((ew == 32)){
                    return false;
                }

                if((ew == 95 && shiftKey == true ) || ([8, 13, 27,9, 37, 38, 39, 40, 110,32].indexOf(event.which) > -1) || (65 <= ew && ew <= 90) || (97 <= ew && ew <= 122)){
                    return true;
                }
                return false;
            });
            element.on('keydown', function (event) {
                var ew = event.which;
                var shiftKey = event.shiftKey;

                if((ew == 32)){
                    return false;
                }

                if((ew == 95 && shiftKey == true ) || ([8, 13, 27,9, 37, 38, 39, 40, 110,32].indexOf(event.which) > -1) || (65 <= ew && ew <= 90) || (97 <= ew && ew <= 122)){
                    return true;
                }
                return false;
            });
        }
    }
}]);

CharityApp.directive('englishAlphabet', [function(){
    return {
        restrict: 'A',
        link: function (scope, element, attrs, ctrl) {
            element.on('cut copy paste', function (e) {
                return true;
            });
            element.on('keypress', function (event) {
                var ew = event.which;
                if((ew == 32) || ([8, 13, 27,9, 37, 38, 39, 40, 110,32].indexOf(event.which) > -1) || (65 <= ew && ew <= 90) || (97 <= ew && ew <= 122)){
                    return true;
                }
                return false;
            });
            element.on('keydown', function (event) {
                var ew = event.which;
                if((ew == 32) || ([8, 13, 27,9, 37, 38, 39, 40, 110,32].indexOf(event.which) > -1) || (65 <= ew && ew <= 90) || (97 <= ew && ew <= 122)){
                    return true;
                }
                return false;
            });
        }
    }
}]);

CharityApp.directive('userName', [function(){
    return {
        restrict: 'A',
        link: function (scope, element, attrs, ctrl) {
            element.on('cut copy paste', function (e) {
                return true;
            });


            element.on('keypress', function (event) {
                var ew = event.which;
                if((ew == 32) || ([8, 13, 27,9, 37, 38, 39, 40,32,190,189,110,46,45,95].indexOf(event.which) > -1) || (65 <= ew && ew <= 90) || (97 <= ew && ew <= 122)){
                    return true;
                }
                return false;
            });
            element.on('keydown', function (event) {
                var ew = event.which;
                if((ew == 32) || ([8, 13, 27,9, 37, 38, 39, 40,32,190,189,110,46,45,95].indexOf(event.which) > -1) || (65 <= ew && ew <= 90) || (97 <= ew && ew <= 122)){
                    return true;
                }
                return false;
            });
        }
    }
}]);

CharityApp.directive('arabicAlphabet', [function(){
    return {
        restrict: 'A',
        link: function (scope, element, attrs, ctrl) {
            element.on('cut copy paste', function (e) {
                return true;
            });
            element.on('keypress', function (e) {

                var unicode=e.charCode? e.charCode : e.keyCode;
                if (unicode!=8 && unicode!=32 ){
                    if ((unicode < 0x0600 || unicode > 0x06FF))
                        return false ;
                }
            });
        }
    }
}]);

CharityApp.directive('serverValidate', ['$compile',
    function ($compile) {
        return {
            restrict: 'A',
            require: 'form',
            link: function ($scope, $elem, $attrs, form) {
                var invalidateField = function (field, fieldErrors) {
                    var changeListener = function () {
                        field.$setValidity('server', true);
                        
                        var index = field.$viewChangeListeners.indexOf(changeListener);
                        if (index > -1) {
                            field.$viewChangeListeners.splice(index, 1);
                        }
                    };
                    
                    messages = [];
                    field.$setDirty();
                    field.$setValidity('server', false);
                    angular.forEach(fieldErrors, function (error) {
                        messages.push(error);
                    });
                    field.serverMessages = messages.join('<br>');
                    field.$viewChangeListeners.push(changeListener);
                    
                };
                
                $scope.$watch('serverErrors', function (errors) {
                    if (errors) {
                        angular.forEach(errors, function (fieldErrors, fieldName) {
                            if (fieldName in form) {
                                invalidateField(form[fieldName], fieldErrors);
                            }
                        });
                    }
                });
            }
        };
    }
]);

CharityApp.directive('ngThumb', ['$window', function($window) {
    var helper = {
        support: !!($window.FileReader && $window.CanvasRenderingContext2D),
        isFile: function(item) {
            return angular.isObject(item) && item instanceof $window.File;
        },
        isImage: function(file) {
            var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    };

    return {
        restrict: 'A',
        template: '<canvas/>',
        link: function(scope, element, attributes) {
            if (!helper.support) return;

            var params = scope.$eval(attributes.ngThumb);

            if (!helper.isFile(params.file)) return;
            if (!helper.isImage(params.file)) return;

            var canvas = element.find('canvas');
            var reader = new FileReader();

            reader.onload = onLoadFile;
            reader.readAsDataURL(params.file);

            function onLoadFile(event) {
                var img = new Image();
                img.onload = onLoadImage;
                img.src = event.target.result;
            }

            function onLoadImage() {
                var width = params.width || this.width / this.height * params.height;
                var height = params.height || this.height / this.width * params.width;
                canvas.attr({ width: width, height: height });
                canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
            }
        }
    };
}]);

CharityApp.directive('imgBlob', ['$http',
function ($http) {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs) {
            $http({
                method: 'GET',
                url: attrs.url,
                responseType: 'arraybuffer',
                cache: false
            }).then(function (response) {
                var blob = new Blob([response.data], {type: response.headers('Content-Type')});
                url = (window.URL || window.webkitURL).createObjectURL(blob);
                scope.imgSrc = url;
            }, function (response) {
            });
        }
    };
}]);

CharityApp.directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if(event.which === 13) {
                scope.$apply(function(){
                    scope.$eval(attrs.ngEnter, {'event': event});
                });

                event.preventDefault();
            }
        });
    };
});

CharityApp.directive('docFileDownload', ['$http',
    function ($http) {
        return {
            restrict: 'A',
            link: function (scope, elem, attrs) {
                elem.on('click', function (e) {
                    e.preventDefault();
                    $http({
                        method: 'GET',
                        url: attrs.url,
                        //responseType: 'arraybuffer',
                        cache: false
                    }).then(function (response) {
                        angular.element('.btn').removeClass("disabled");
                        angular.element('.btn').removeAttr('disabled');
                        var msg = response.data.msg;
                        if (msg) {
                            // rootScope.toastrMessages('error',response.msg);
                            toastr.remove();
                            toastr.options = {
                                tapToDismiss: false
                                , timeOut: 0
                                , extendedTimeOut: 0
                                , allowHtml: true
                                , preventDuplicates: false
                                , preventOpenDuplicates: false
                                , newestOnTop: true
                                , closeButton: true
                                , closeHtml: '<button><i class="icon-off pull-right"></i></button>'
                            };
                            toastr.error(msg);

                            return;
                        }

                        var token = response.data.msg;
                        if (token) {
                            window.location = attrs.url + "?token=" + token;
                            return;
                        }
                        /*var a = document.createElement("a");
                         a.style = "display: none";
                         document.body.appendChild(a);

                         var blob = new Blob([response.data], {type: response.headers('Content-Type')});
                         url = (window.URL || window.webkitURL).createObjectURL(blob);
                         a.href = url;
                         a.download = attrs.download;
                         a.click();
                         (window.URL || window.webkitURL).revokeObjectURL(url);
                         document.body.removeChild(a);*/
                    });
                });



            }
        };
    }]);

// File Download
CharityApp.directive('docFileDownload', ['$http',
    function ($http) {
        return {
            restrict: 'A',
            link: function (scope, elem, attrs) {
                elem.on('click', function (e) {
                    angular.element('.btn').addClass("disabled");
                    angular.element('.fa').addClass("disabled");

                    e.preventDefault();
                    $http({
                        method: 'GET',
                        url: attrs.url,
                        responseType: 'arraybuffer',
                        cache: false
                    }).then(function (response) {
                        angular.element('.btn').removeClass("disabled");
                        angular.element('.btn').removeAttr('disabled');
                        angular.element('.fa').removeClass("disabled");
                        var a = document.createElement("a");
                        a.style = "display: none";
                        document.body.appendChild(a);

                        var blob = new Blob([response.data], {type: response.headers('Content-Type')});
                        url = (window.URL || window.webkitURL).createObjectURL(blob);
                        a.href = url;
                        a.download = attrs.download;
                        a.click();
                        (window.URL || window.webkitURL).revokeObjectURL(url);
                        document.body.removeChild(a);

                    });
                });
            }
        };
    }]);

// File Print
CharityApp.directive('docFilePrint', ['$http',
    function ($http) {
        return {
            restrict: 'A',
            link: function (scope, elem, attrs) {
                elem.on('click', function (e) {
                    e.preventDefault();
                    angular.element('.btn').addClass("disabled");
                       angular.element('.fa').addClass("disabled");
                    $http({
                        method: 'GET',
                        url: attrs.url,
                        responseType: 'arraybuffer',
                        cache: false
                    }).then(function (response) {
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                           angular.element('.fa').removeClass("disabled");
                        var a = document.createElement("a");
                        a.style = "display: none";
                        document.body.appendChild(a);
                        var blob = new Blob([response.data], {type: response.headers('Content-Type')});
                        url = (window.URL || window.webkitURL).createObjectURL(blob);
                        var printwWindow = window.open(url);
                        printwWindow.print();
                    });
                });
            }
        };
    }]);

// File View
CharityApp.directive('docFileView', ['$http',
    function ($http) {
        return {
            restrict: 'A',
            link: function (scope, elem, attrs) {
                elem.on('click', function (e) {
                    e.preventDefault();
                    angular.element('.btn').addClass("disabled");
                       angular.element('.fa').addClass("disabled");
                    $http({
                        method: 'GET',
                        url: attrs.url,
                        responseType: 'arraybuffer',
                        cache: false
                    }).then(function (response) {
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        angular.element('.fa').removeClass("disabled");
                        var a = document.createElement("a");
                        a.style = "display: none";
                        document.body.appendChild(a);
                        var blob = new Blob([response.data], {type: response.headers('Content-Type')});
                        url = (window.URL || window.webkitURL).createObjectURL(blob);
                        window.open(url);
                    });
                });
            }
        };
    }]);


CharityApp.directive('citizenImg', ['$http',
    function ($http) {
        return {
            restrict: 'A',
            link: function (scope, elem, attrs) {
                if(attrs.card.length == 9){
                    $http({
                        method: 'GET',
                        url: attrs.url,
                        responseType: 'arraybuffer',
                        cache: false
                    }).then(function (response) {
                        var blob = new Blob([response.data], {type: response.headers('Content-Type')});
                        url = (window.URL || window.webkitURL).createObjectURL(blob);
                        scope.imgSrc = url;
                    }, function (response) {
                    });
                }
            }
        };
    }]);



CharityApp.directive('displayImg', ['$http',
    function ($http) {
        return {
            restrict: 'A',
            link: function (scope, elem, attrs) {
                $http({
                    method: 'GET',
                    url: attrs.url,
                    responseType: 'arraybuffer',
                    cache: false
                }).then(function (response) {
                    var blob = new Blob([response.data], {type: response.headers('Content-Type')});
                    url = (window.URL || window.webkitURL).createObjectURL(blob);
                    scope.imgSrc = url;
                }, function (response) {
                });
            }
        };
    }]);
// var full_name = row.full_name.split(" ");
// var new_name = '';
//
// full_name.forEach(function (element) {
//     if (element.startsWith("آ") || element.startsWith("أ") || element.startsWith("ا") || element.startsWith("إ")) {
//         element = element.substr(1);
//         element = 'ا' + element;
//     } else if (element.endsWith("ه") || element.endsWith("ة")) {
//         element = element.substring(0, element.length - 1);
//         element = element + 'ه';
//     } else {
//         element = element;
//     }
//
//     new_name += element + ' ';
// });

// var search = angular.copy($scope.search);
// if (search.startsWith("آ") || search.startsWith("أ") || search.startsWith("ا") || search.startsWith("إ")) {
//     search = search.substr(1);
//     search = ['آ' + search, 'أ' + search, 'إ' + search, 'ا' + search];
// } else if (search.endsWith("ه") || search.endsWith("ة") || search.endsWith("ء")) {
//     search = search.substring(0, search.length - 1);
//     search = [search + 'ه', search + 'ة', search + 'ء'];
// } else {
//     search = [search];
// }
// if (x.full_name.indexOf(search[0]) > -1
//     || x.full_name.indexOf(search[1]) > -1
//     || x.full_name.indexOf(search[2]) > -1
//     || x.full_name.indexOf(search[3]) > -1
//     || x.idno.indexOf(search) > -1
//     || x.work_no.indexOf(search) > -1
//     || x.work_start_dt.indexOf(search) > -1
//     || x.mobile_no.indexOf(search) > -1
// )
//     return true;



CharityApp.directive('onlyNumbers', function () {
    return  {
        restrict: 'A',
        link: function (scope, elm, attrs, ctrl) {
            elm.on('keydown', function (event) {
                if(event.shiftKey){event.preventDefault(); return false;}
                //console.log(event.which);
                if ([8, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {
                    // backspace, enter, escape, arrows
                    return true;
                } else if (event.which >= 49 && event.which <= 57) {
                    // numbers
                    return true;
                } else if (event.which >= 96 && event.which <= 105) {
                    // numpad number
                    return true;
                }
                    // else if ([110, 190].indexOf(event.which) > -1) {
                    //     // dot and numpad dot
                    //     return true;
                // }
                else {
                    event.preventDefault();
                    return false;
                }
            });
        }
    }
});

CharityApp.directive('noFloat', function () {
    return  {
        restrict: 'A',
        link: function (scope, elm, attrs, ctrl) {
            elm.on('keydown', function (event) {
                if ([110, 190].indexOf(event.which) > -1) {
                    // dot and numpad dot
                    event.preventDefault();
                    return false;
                }
                else{
                    return true;
                }
            });
        }
    }
});

CharityApp.directive("floatingNumberOnly", function() {
    return {
        require: 'ngModel',
        link: function(scope, ele, attr, ctrl) {

            ctrl.$parsers.push(function(inputValue) {
                // console.log(inputValue);
                var pattern = new RegExp("(^[0-9]{1,9})+(\.[0-9]{1,1})?$", "g");
                // var pattern = new RegExp("(^[0-9]{1,9})+([.,][0-9]{1,1})?$","g");

                if (inputValue == '')
                    return '';
                var dotPattern = /^[.]*$/;

                if (dotPattern.test(inputValue)) {
                    // console.log("inside dot Pattern");
                    ctrl.$setViewValue('');
                    ctrl.$render();
                    return '';
                }

                var newInput = inputValue.replace(/[^0-9.]/g, '');
                // newInput=inputValue.replace(/.+/g,'.');

                if (newInput != inputValue) {
                    ctrl.$setViewValue(newInput);
                    ctrl.$render();
                }
                //******************************************
                //***************Note***********************
                /*** If a same function call made twice,****
                 *** erroneous result is to be expected ****/
                    //******************************************
                    //******************************************

                var result;
                var dotCount;
                var newInputLength = newInput.length;
                if (result = (pattern.test(newInput))) {
                    dotCount = newInput.split(".").length - 1; // count of dots present
                    if (dotCount == 0 && newInputLength > 9) { //condition to restrict "integer part" to 9 digit count
                        newInput = newInput.slice(0, newInputLength - 1);
                        ctrl.$setViewValue(newInput);
                        ctrl.$render();
                    }
                } else { //pattern failed
                    // console.log(newInput.length);

                    dotCount = newInput.split(".").length - 1; // count of dots present
                    // console.log("dotCount  :  " + dotCount);
                    if (newInputLength > 0 && dotCount > 1) { //condition to accept min of 1 dot
                        // console.log("length>0");
                        newInput = newInput.slice(0, newInputLength - 1);
                        // console.log("newInput  : " + newInput);

                    }
                    if ((newInput.slice(newInput.indexOf(".") + 1).length) > 1) { //condition to restrict "fraction part" to 1 digit count only.
                        newInput = newInput.slice(0, newInputLength - 1);
                        // console.log("newInput  : " + newInput);

                    }
                    ctrl.$setViewValue(newInput);
                    ctrl.$render();
                }

                return newInput;
            });
        }
    };
});



CharityApp.directive('justAlphabet', [function(){
    return {
        restrict: 'A',
        link: function (scope, element, attrs, ctrl) {
            element.on('cut copy paste', function (e) {
                return true;
            });
            element.on('keypress', function (event) {
                var ew = event.which;
                // (ew == 32) ||
                if(([8, 13, 27,9, 37, 38, 39, 40, 110,32].indexOf(event.which) > -1) || (65 <= ew && ew <= 90) || (97 <= ew && ew <= 122)){
                    return true;
                }
                return false;
            });
            element.on('keydown', function (event) {
                var ew = event.which;
                // (ew == 32) ||
                if(([8, 13, 27,9, 37, 38, 39, 40, 110,32].indexOf(event.which) > -1) || (65 <= ew && ew <= 90) || (97 <= ew && ew <= 122)){
                    return true;
                }
                return false;
            });
        }
    }
}]);