
var AidModule = angular.module("AidModule",[]);
angular.module('AidModule')
    .config(function ($stateProvider) {
    $stateProvider

        /* ********************************* AID CASES  ********************************* */
        .state('aidCases', {
            url: '/aids/:type/cases',
            templateUrl: '/app/modules/aid/views/cases/index.html',
            params:{mode: null,status:null,'type':null },
            data: {pageTitle: ''},
            controller: "aidCasesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        reload: false,
                        files: [

                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/aid/controllers/aidCasesController.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/forms/services/CustomFormsService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/sms/services/SmsService.js',
                             '/app/modules/organization/services/OrgService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/app/modules/aid/services/VouchersCategoriesService.js'

                        ]
                    });
                }]
            }

        })
        .state('caseIndividual', {
            url: '/aids/cases/individual',
            templateUrl: '/app/modules/aid/views/cases/individual.html',
            params:{mode: null,status:null,'type':null },
            data: {pageTitle: ''},
            controller: "caseIndividualController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        reload: false,
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/forms/services/CustomFormsService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/app/modules/aid/services/VouchersCategoriesService.js',
                           '/app/modules/aid/controllers/caseIndividualController.js',

                        ]
                    });
                }]
            }

        })
        .state('organizationCases', {
            url: '/aids/organizations',
            templateUrl: '/app/modules/aid/views/cases/organizations.html',
            params:{},
            data: {pageTitle: ''},
            controller: "organizationCasesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        reload: false,
                        files: [

                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/aid/controllers/organizationsCasesController.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/forms/services/CustomFormsService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/app/modules/aid/services/VouchersCategoriesService.js'

                        ]
                    });
                }]
            }

        })
        /* ********************************* VOUCHERS  ********************************* */
        /* ************************************************************************** */
        .state('allVouchers', {
            url: '/aids/vouchers',
            templateUrl: '/app/modules/aid/views/vouchers/index.html',
            data: {pageTitle: ''},
            controller: "VouchersController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/aid/controllers/vouchers/VouchersController.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/aid/services/VouchersCategoriesService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/document/directives/fileDownload.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/SettingsService.js'


                        ]
                    });
                }]
            }

        })
        .state('trashed-vouchers', {
            url: '/aids/trashed-vouchers',
            templateUrl: '/app/modules/aid/views/vouchers/trashed-vouchers.html',
            data: {pageTitle: ''},
            controller: "trashedVouchersController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/aid/controllers/vouchers/trashedVouchersController.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/aid/services/VouchersCategoriesService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/document/directives/fileDownload.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/SettingsService.js'


                        ]
                    });
                }]
            }

        })
        .state('personsVouchers', {
            url: '/aids/Vouchers/persons',
            templateUrl: '/app/modules/aid/views/vouchers/persons-vouchers-reports.html',
            data: {pageTitle: ''},
            controller: "personsVouchersController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/aid/controllers/vouchers/personsVouchersController.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/aid/services/VouchersCategoriesService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/SettingsService.js'


                        ]
                    });
                }]
            }

        })
        .state('vouchers-beneficiary', {
            url: '/aids/voucher/:id',
            templateUrl: '/app/modules/aid/views/vouchers/persons.html',
            params:{id: null,page:1},
            data: {pageTitle: ''},
            controller: "VoucherPersonsController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/aid/controllers/vouchers/VoucherPersonsController.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/app/modules/sms/services/SmsService.js'

                        ]
                    });
                }]
            }

        })
        .state('vouchers-attachment', {
            url: '/aids/voucher/attachments/:id',
            templateUrl: '/app/modules/aid/views/vouchers/attachments.html',
            params:{id: null},
            data: {pageTitle: ''},
            controller: "VoucherAttachmentsController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/aid/controllers/vouchers/VoucherAttachmentsController.js',
                            '/app/modules/aid/services/VouchersService.js'

                        ]
                    });
                }]
            }

        })
        .state('vouchers-categories', {
            url: '/aids/vouchers/categories',
            templateUrl: '/app/modules/aid/views/vouchers/categories.html',
            params:{type: null},
            data: {pageTitle: ''},
            controller: "VouchersCategoriesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/app/modules/aid/controllers/vouchers/VouchersCategoriesController.js',
                            '/app/modules/aid/services/VouchersCategoriesService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js'



                        ]
                    });
                }]
            }

        })
        .state('vouchers-statistics', {
            url: '/aids/voucher/statistics/:id',
            templateUrl: '/app/modules/aid/views/vouchers/statistics.html',
            params:{id: null},
            data: {pageTitle: ''},
            controller: "VoucherStatisticsController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/aid/controllers/vouchers/VoucherStatisticsController.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/app/modules/sms/services/SmsService.js'

                        ]
                    });
                }]
            }

        })

        /* ************************************************************************** */
        .state('neighborhoods-ratios', {
            url: '/aids/neighborhoods-ratios',
            templateUrl: '/app/modules/aid/views/cases/neighborhoods-ratios.html',
            params:{},
            data: {pageTitle: ''},
            controller: "NeighborhoodsRatiosController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/app/modules/aid/controllers/NeighborhoodsRatiosController.js',
                            '/app/modules/aid/services/NeighborhoodsRatiosService.js'

                        ]
                    });
                }]
            }

        })
        /* ********************************* CASE FORM  ********************************* */
        .state('caseform', {
            url: '/aids/caseform',
            templateUrl: '/app/modules/aid/views/cases/casessform/caseform.html',
            params:{mode:'add',id:null,category_id:null },
            controller: "aidCaseFormController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/app/modules/aid/controllers/caseform/aidCaseFormController.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/organization/services/OrgService.js'

                        ]
                    });
                }]
            }

        })
        .state('caseform.personal-info', {
            url: '/personal-info/:category_id/:mode/:id',
            templateUrl: '/app/modules/aid/views/cases/casessform/personal-info.html',
            params:{mode:'add',id:null,category_id:null },
            data: {pageTitle: ''},
            controller: "PersonalInfoController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/aid/controllers/caseform/PersonalInfoController.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/common/services/PersonsService.js'

                        ]
                    });
                }]
            }

        })
        .state('caseform.residence-data', {
            url: '/residence-data/:category_id/:mode/:id',
            templateUrl: '/app/modules/aid/views/cases/casessform/residence-data.html',
            params:{mode:'add',id:null,category_id:null },
            data: {pageTitle: ''},
            controller: "ResidenceDataController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/aid/controllers/caseform/ResidenceDataController.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/common/services/CaseService.js'
                        ]
                    });
                }]
            }

        })
        .state('caseform.home-indoor-data', {
            url: '/home-indoor-data/:category_id/:mode/:id',
            templateUrl: '/app/modules/aid/views/cases/casessform/home-indoor-data.html',
            params:{mode:'add',id:null,category_id:null },
            data: {pageTitle: ''},
            controller: "HomeIndoorDataController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/app/modules/aid/controllers/caseform/HomeIndoorDataController.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/common/services/CategoriesService.js'
                        ]
                    });
                }]
            }

        })
        .state('caseform.other-data', {
            url: '/other-data/:category_id/:mode/:id',
            templateUrl: '/app/modules/aid/views/cases/casessform/other-data.html',
            params:{mode:'add',id:null,category_id:null },
            data: {pageTitle: ''},
            controller: "OtherdataController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/app/modules/aid/controllers/caseform/OtherdataController.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/CategoriesService.js'

                        ]
                    });
                }]
            }

        })
        .state('caseform.recommendations', {
            url: '/recommendations/:category_id/:mode/:id',
            templateUrl: '/app/modules/aid/views/cases/casessform/recommendations.html',
            params:{mode:'add',id:null,category_id:null },
            data: {pageTitle: ''},
            controller: "RecommendationsController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/app/modules/aid/controllers/caseform/RecommendationsController.js',
                            '/app/modules/common/services/CaseService.js'

                        ]
                    });
                }]
            }

        })
        .state('caseform.custom-form', {
            url: '/custom-form/:category_id/:mode/:id',
            templateUrl: '/app/modules/aid/views/cases/casessform/custom-form.html',
            params:{mode:'add',id:null,category_id:null },
            data: {pageTitle: ''},
            controller: "CustomFormController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/app/modules/aid/controllers/caseform/CustomFormController.js',
                            '/app/modules/forms/services/FormElementsService.js',
                            '/app/modules/forms/services/CustomFormsService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/forms/services/FormCaseDataService.js'


                        ]
                    });
                }]
            }

        })
        .state('caseform.family-structure', {
            url: '/family-structure/:category_id/:mode/:id',
            templateUrl: '/app/modules/aid/views/cases/casessform/family-structure.html',
            params:{mode:'add',id:null,category_id:null },
            data: {pageTitle: ''},
            controller: "FamilyStructureController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                             '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/aid/controllers/caseform/FamilyStructureController.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/common/services/CategoriesService.js'
                        ]
                    });
                }]
            }
        })
        .state('caseform.case-document', {
            url: '/case-document/:category_id/:mode/:id',
            templateUrl: '/app/modules/aid/views/cases/casessform/case-document.html',
            data: {pageTitle: ''},
            controller: "DocumentController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/app/modules/aid/controllers/caseform/DocumentController.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js'

                        ]
                    });
                }]
            }

        })
        .state('caseform.vouchers-data', {
            url: '/vouchers-data/:category_id/:mode/:id',
            templateUrl: '/app/modules/aid/views/cases/casessform/vouchers-data.html',
            params:{mode:'add',id:null,category_id:null },
            data: {pageTitle: ''},
            controller: "PersonVoucherController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/aid/controllers/caseform/PersonVoucherController.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/aid/services/VouchersCategoriesService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/organization/services/OrgService.js'


                        ]
                    });
                }]
            }

        })
        .state('case-vouchers', {
            url: '/aids/case-vouchers/:id',
            templateUrl: '/app/modules/aid/views/cases/casessform/case-vouchers.html',
            params:{id:null},
            controller: "CaseVoucherController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/aid/controllers/caseform/show/CaseVoucherController.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/aid/services/VouchersCategoriesService.js',
                            '/app/modules/organization/services/OrgService.js'

                        ]
                    });
                }]
            }

        })
        .state('categories-caseform', {
            url: '/aids/categories-caseform/:category_id/:mode/:id',
            templateUrl: '/app/modules/aid/views/cases/categories-caseform.html',
            Params: {id:null,mode:null,category_id:null},
            data: {pageTitle: ''},
            controller: "CategoriesCaseFormController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        files: [
                            '/app/modules/aid/controllers/CategoriesCaseFormController.js',
                            '/app/modules/common/services/CategoriesService.js'

                        ]
                    });
                }]
            }

        })

        /* ********************************* Social Search ********************************* */
        .state('social-search', {
            url: '/aids/social-search',
            templateUrl: '/app/modules/aid/views/social-search/index.html',
            params:{mode: null,status:null,'type':null },
            data: {pageTitle: ''},
            controller: "SocialSearchController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        reload: false,
                        files: [

                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/aid/controllers/SocialSearchController.js',
                            '/app/modules/aid/services/SocialSearchService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/EntityService.js'

                        ]
                    });
                }]
            }

        })
        .state('social-search-trashed', {
            url: '/aids/social-search/trashed',
            templateUrl: '/app/modules/aid/views/social-search/trashed.html',
            params:{mode: null,status:null,'type':null },
            data: {pageTitle: ''},
            controller: "TrashedSocialSearchController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        reload: false,
                        files: [

                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/aid/controllers/SocialSearchController.js',
                            '/app/modules/aid/services/SocialSearchService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/EntityService.js'

                        ]
                    });
                }]
            }

        })

        .state('social-search-templates', {
            url: '/aids/social-search/templates',
            templateUrl: '/app/modules/aid/views/social-search/templates.html',
            params:{mode: null,status:null,'type':null },
            data: {pageTitle: ''},
            controller: "SocialSearchTemplatesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        reload: false,
                        files: [

                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/aid/controllers/SocialSearchController.js',
                            '/app/modules/aid/services/SocialSearchService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/EntityService.js'

                        ]
                    });
                }]
            }

        })
        .state('social-search-persons-data', {
            url: '/aids/social-search/data/:id',
            templateUrl: '/app/modules/aid/views/social-search/cases.html',
            params:{id: null},
            data: {pageTitle: ''},
            controller: "SocialSearchCasesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        reload: false,
                        files: [

                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/aid/controllers/SocialSearchController.js',
                            '/app/modules/aid/services/SocialSearchService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/EntityService.js'

                        ]
                    });
                }]
            }

        })
        .state('social-search-person-form', {
            url: '/aids/social-search/form/:id',
            templateUrl: '/app/modules/aid/views/social-search/form.html',
            params:{id: null},
            data: {pageTitle: ''},
            controller: "SocialSearchCaseFormController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AidModule',
                        reload: false,
                        files: [

                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/aid/controllers/SocialSearchController.js',
                            '/app/modules/aid/services/SocialSearchService.js'

                        ]
                    });
                }]
            }

        })


    ;
});
