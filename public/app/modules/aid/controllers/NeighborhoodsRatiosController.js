
angular.module('AidModule').controller('NeighborhoodsRatiosController', function($stateParams,$rootScope, $scope,$uibModal, $http, $timeout,$state,neighborhoods_ratios,$filter) {

    $state.current.data.pageTitle = $filter('translate')('neighborhoods-ratios');
    $rootScope.status ='';
    $rootScope.msg ='';
    $scope.msg1 =[];
    $scope.items=[];
    $rootScope.model_error_status =false;
    $scope.noChange =true;
    $scope.sum=0;
    $scope.total_count=0;


    var LoadRatios =function($param){
        $rootScope.clearToastr();
        neighborhoods_ratios.query($param,function (response) {

                if(response.status =='true' || response.status == true){
                    $scope.sum=response.sum;
                    $scope.total_count=response.count;
                    $scope.cases=response.cases;
                    $scope.items= response.Ratios;
                }else{
                    $scope.items=[];
                    $scope.sum=0;
                    $scope.total_count=0;
                }
            });

    };

    LoadRatios();

    $scope.UpdateTotal = function (value,index) {
        if(angular.isUndefined(value)) {
            value=0;
        }
        if(value == " " || value == "") {
            value=0;
        }
        $scope.msg1=[];
        var total=0;

        angular.forEach($scope.items, function(v, k) {
            if(angular.isUndefined(v.ratio)) {
                v.ratio=0;
            }
            if(v.ratio == " " || v.ratio == "") {
                v.ratio=0;
            }
                total= total + parseInt(v.ratio);
        });

        if(parseInt(total)>100){
            var temp=[];
            angular.forEach($scope.items, function(v, k) {
                if(index == k){
                    temp={location_id: v.location_id,ratio:0,id: v.id,name: v.name};
                }
            });
            $scope.items[index]=temp;
            $scope.msg1[index]=$filter('translate')('please enter another ratio , the summation of ratio');
            $scope.sum= total - parseInt(value);
            $scope.noChange =true;

        }else{
            $scope.sum= total;
            $scope.noChange =false;
        }
    };

    $scope.save=function(){
        $rootScope.clearToastr();
        var target = new neighborhoods_ratios({rows:$scope.items,id:-1});
        target.$update({id:-1})
            .then(function (response) {
                if(response.status=='error' || response.status=='failed') {
                    $rootScope.toastrMessages('error',response.msg);
                }
                else if(response.status=='failed_valid')
                {
                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                    $scope.status1 =response.status;
                    $scope.msg1 =response.msg;
                }
                else{
                    if(response.status=='success')
                    {
                        if( angular.isUndefined($rootScope.CurrentPagee)) {
                            $rootScope.CurrentPage=1;
                        }
                        LoadRatios({page:$rootScope.CurrentPage});
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                }
            }, function(error) {
                $rootScope.toastrMessages('error',error.data.msg);
            });
    };

    $scope.adjust=function(){
        var total=0;
        angular.forEach($scope.items, function(v, k) {
            v.ratio=  Math.round( (v.count/$scope.total_count)*100);
            total+=v.ratio;
        });
        $scope.sum= total;

        $scope.noChange =false;

    }
});
