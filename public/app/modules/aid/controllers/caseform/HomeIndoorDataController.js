
angular.module('AidModule')
    .controller('HomeIndoorDataController',function($stateParams,$state,$rootScope,$scope,$http,$timeout,cases,category,$filter) {

        $state.current.data.pageTitle = $filter('translate')('home-indoor-data');
        $rootScope.mode=$stateParams.mode;
        $scope.mode=$stateParams.mode;
        $rootScope.category_id=$stateParams.category_id;
        $scope.msg1=[];
        $scope.items=$scope.row=[];
        $scope.temp=[];
        $scope.priority=[];

    if(angular.isUndefined($stateParams.mode) ||$stateParams.mode == null){
        $scope.mode=$rootScope.mode=$stateParams.mode="new";
    }

    if((!angular.isUndefined($stateParams.id) || $stateParams.id !== null) && !$stateParams.mode){
        $scope.mode=$rootScope.mode=$stateParams.mode="edit";
    }

    if(!angular.isUndefined($stateParams.category_id) && $stateParams.category_id !== 'null'&& $stateParams.category_id !== null) {

        $rootScope.ResetCaseForm($rootScope.category_id, $rootScope.mode,'homeIndoorData');
        $rootScope.CurrentStep=$rootScope.HomeIndoorStepNumber;

        if($stateParams.id == null || $rootScope.CurrentStep==1 ) {
        }else{
            if($stateParams.mode !=="show") {
                category.getPriority({'category_id':$rootScope.category_id ,'target':'homeIndoorData','type':'aids'},function (response) {
                    $scope.priority=response.priority;
                });
            }
        }

        var params={'case_id':$stateParams.id,
            'target':'info',
            'action':'filters',
            'mode':$stateParams.mode,
            'category_type' : 2,
            'home_indoor' :true
        };
        $rootScope.progressbar_start();
        cases.getCaseReports(params,function(response) {
            $rootScope.progressbar_complete();
            $rootScope.id =response.id;
            var temp =response.home_indoor;
            if($rootScope.mode =="new" || $rootScope.mode =="edit" ) {
                angular.forEach(temp, function(v, k) {
                    if(v.condition != null && v.condition != 'null'){
                        v.condition=parseInt(v.condition) +"";
                    }else{
                        v.condition= "";
                    }
                });
            }
            $scope.items= temp;

        });

    }else{
        if($rootScope.mode !== 'new'){
            $state.go('aidCases',{"type": 'all'});
        }else{
            $state.go('categories-caseform',{"mode": $rootScope.mode,"case_id" :$stateParams.id});
        }
    }

    $scope.save= function(records,step){
        $rootScope.progressbar_start();
        $rootScope.clearToastr();
        cases.saveHomeIndoor({'id':$rootScope.id,'type':'aids','items':records,'priority': $scope.priority},function (response) {
            $rootScope.progressbar_complete();
            if(response.status=='failed_disabled')
            {
                $rootScope.toastrMessages('error',response.msg);
                $timeout(function() {
                    $state.go('aidCases',{"type": 'all',"mode": null,"status" : null});
                },3000);
            }
            else if(response.status==true){
                if(step =='next'){
                    $rootScope.getNext();
                }else{
                    $rootScope.getPrevious();
                }
            }else{
                $rootScope.toastrMessages('error',response.msg);
            }
            }, function(error) {
                $rootScope.toastrMessages('error',error.data.msg);
            });
    };

});
