
angular.module('AidModule').controller('ResidenceDataController', function($stateParams,$state,$rootScope,$scope,$http,$timeout,cases,category,Entity,$filter) {

    $state.current.data.pageTitle = $filter('translate')('residence-data');

    $rootScope.mode=$stateParams.mode;
    $scope.mode=$stateParams.mode;
    $scope.msg1=[];
    $scope.residence={};
    $scope.priority=[];

    if(angular.isUndefined($stateParams.mode) ||$stateParams.mode == null){
        $scope.mode=$rootScope.mode=$stateParams.mode="new";
    }

    if((!angular.isUndefined($stateParams.id) || $stateParams.id !== null) && !$stateParams.mode){
        $scope.mode=$rootScope.mode=$stateParams.mode="edit";
    }

    $rootScope.category_id=$stateParams.category_id;
    $rootScope.ResetCaseForm($rootScope.category_id, $rootScope.mode,'residenceData');
    $rootScope.CurrentStep=$rootScope.ResidenceStepNumber;

    if(angular.isUndefined($stateParams.category_id) ||$stateParams.category_id == null){
        if($rootScope.mode !== 'new'){
            $state.go('aidCases',{"type": 'all'});
        }else{
            $state.go('categories-caseform',{"mode": $rootScope.mode,"case_id" :$stateParams.id});
        }
    }
    else{
        if($stateParams.mode !=="show") {

            Entity.get({entity:'entities',c:'propertyTypes,roofMaterials,buildingStatus,furnitureStatus,houseStatus,habitableStatus'},function (response) {
                $scope.PropertyTypes = response.propertyTypes;
                $scope.RoofMaterials = response.roofMaterials;
                $scope.buildingStatus = response.buildingStatus;
                $scope.furnitureStatus = response.furnitureStatus;
                $scope.houseStatus = response.houseStatus;
                $scope.habitableStatus = response.habitableStatus;
            });

            category.getPriority({'category_id':$rootScope.category_id ,'target':'residenceData','type':'aids'},function (response) {
                $scope.priority=response.priority;
            });

        }

        if(!angular.isUndefined($stateParams.id) || $stateParams.id !== null){
            if($rootScope.mode =="show" || $rootScope.mode =="edit" ) {
                var mode='show';

                if($stateParams.mode == 'new' || $rootScope.mode =="edit" ){
                    mode='edit';
                }

                var params={'case_id':$stateParams.id,
                    'target':'info',
                    'action':'filters',
                    'mode':mode,
                    'category_type' : 2,
                    'residence' :true
                };
                $rootScope.progressbar_start();
                cases.getCaseReports(params,function(response) {
                    $rootScope.progressbar_complete();
                    $scope.residence = response;
                    if($rootScope.mode =="new" || $rootScope.mode =="edit" ) {
                        if($scope.residence.property_type_id == null){
                            $scope.residence.property_type_id="";
                        }else{
                            $scope.residence.property_type_id    = $scope.residence.property_type_id +"";

                            if($scope.residence.property_type_id != 1){
                                $scope.residence.rent_value="";
                            }
                        }
                        if($scope.residence.roof_material_id == null){
                            $scope.residence.roof_material_id="";
                        }else{
                            $scope.residence.roof_material_id    = $scope.residence.roof_material_id +"" ;
                        }
                        if($scope.residence.residence_condition == null){
                            $scope.residence.residence_condition="";
                        }else{
                            $scope.residence.residence_condition = $scope.residence.residence_condition +"" ;
                        }
                        if($scope.residence.indoor_condition == null){
                            $scope.residence.indoor_condition="";
                        }else{
                            $scope.residence.indoor_condition    = $scope.residence.indoor_condition +"";
                        }
                        if($scope.residence.habitable == null){
                            $scope.residence.habitable="";
                        }else{
                            $scope.residence.habitable           = $scope.residence.habitable +"";
                        }
                        if($scope.residence.house_condition == null){
                            $scope.residence.house_condition="";
                        }else{
                            $scope.residence.house_condition     = $scope.residence.house_condition +"";
                        }
                    }


                });

            }
        }


    }

    $scope.save= function(residence,step){
        $rootScope.clearToastr();
        residence.priority=$scope.priority;
         residence.id=$stateParams.id;
         residence.type='aids';
        $rootScope.progressbar_start();
        cases.saveResidence(residence,function (response) {
            $rootScope.progressbar_complete();
            if(response.status=='failed_disabled')
            {
                $rootScope.toastrMessages('error',response.msg);
                $timeout(function() {
                    $state.go('aidCases',{"type": 'all',"mode": null,"status" : null});
                },3000);
            }
            else if(response.status =='failed_valid') {
                  $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                  $scope.status1 =response.status;
                   $scope.msg1 =response.msg;
             }
              else{
                    if(step =='next'){
                        $rootScope.getNext();
                    }else{
                        $rootScope.getPrevious();
                    }
              }}, function(error) {
                    $rootScope.toastrMessages('error',error.data.msg);
              });

    };

    $scope.RestValue = function(value){

       if( !angular.isUndefined($scope.msg1.property_type_id)) {
             $scope.msg1.property_type_id = [];
        }

        if(value != 1){

            if( !angular.isUndefined($scope.msg1.rent_value)) {
                $scope.msg1.rent_value = [];
            }
            $scope.residence.rent_value="";
        }
    };

});
