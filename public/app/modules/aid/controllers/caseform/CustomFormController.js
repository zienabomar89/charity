
angular.module('AidModule')
    .directive("dynamicName",function($compile){
        return {
            restrict:"A",
            terminal:true,
            priority:1000,
            link:function(scope,element,attrs){
                element.attr('name', scope.$eval(attrs.dynamicName));
                element.removeAttr("dynamic-name");
                $compile(element)(scope);
            }
        }
    })
    .controller('CustomFormController',function($filter,$state,$stateParams,$rootScope,$scope,$http,$timeout,forms_case_data) {

        $state.current.data.pageTitle = $filter('translate')('custom-form');
        $rootScope.result_1 = 'true';
        $rootScope.result = 'true';
        $rootScope.mode=$stateParams.mode;
        $scope.mode=$stateParams.mode;
        $scope.temp_mode=null;
        $scope.msg1=[];
        $scope.fields=[];
        if(angular.isUndefined($stateParams.mode) ||$stateParams.mode == null){
            $stateParams.mode="new";
        }

        if((!angular.isUndefined($stateParams.id) || $stateParams.id !== null) && !$stateParams.mode){
            $stateParams.mode="edit";
        }

        $rootScope.category_id=$stateParams.category_id;
        $rootScope.ResetCaseForm($rootScope.category_id, $rootScope.mode,'customFormData');
        $rootScope.CurrentStep=$rootScope.CustomFormStepNumber;


        if($stateParams.category_id == null) {
            if($rootScope.mode =='new'){
                $state.go('categories-caseform',{'mode':$rootScope.mode});
            }else{
                $state.go('aidCases',{"type": 'all'});

            }
        }else{
            if($stateParams.id == null || $rootScope.CurrentStep==1 ) {

            }else{
                $rootScope.progressbar_start();
                forms_case_data.get({id: $stateParams.id,module:'aid',category_id:$stateParams.category_id},function (response) {
                    $rootScope.progressbar_complete();

                        if( !angular.isUndefined(response.status)){
                            if(response.status == false) {
                                $rootScope.result_1 = 'false';
                            }
                        }

                        if( !angular.isUndefined(response.status_2)){
                            if(response.status_2 == false) {
                                $rootScope.result = 'false';
                            }
                        }

                        if(angular.isUndefined(response.status_2) && angular.isUndefined(response.status)){
                            if (response.data.length != 0) {
                                var temp = response.data;

                                angular.forEach(temp, function(v, k) {
                                    v.data=v.value;
                                    if (v.options.length != 0) {
                                        if(v.value != null && v.value != 'null'){
                                            v.data=parseInt(v.value) +"";
                                        }
                                    }
                                });
                                $scope.fields=temp;
                            }
                        }


                    });
              }
        }


        $scope.confirmInsert=function(fields,step){
            $rootScope.clearToastr();
            if($scope.fields.length != 0){
                $rootScope.progressbar_start();
                var target = new forms_case_data({case_id:$stateParams.id,'fieldsData':fields});
                target.$save()
                    .then(function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='failed_valid')
                        {
                            $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                            $scope.status1 =response.status;
                            $scope.msg1 =response.msg;
                        }
                        else {
                            if(step =='next'){
                                $rootScope.getNext();
                            }else{
                                $rootScope.getPrevious();
                            }

                        }
                    },function(error) {
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
            }else{
                if(step =='next'){
                    $rootScope.getNext();
                }else{
                    $rootScope.getPrevious();
                }
            }


        };

});
