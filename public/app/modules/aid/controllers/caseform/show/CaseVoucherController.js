
angular.module('AidModule')
    .controller('CaseVoucherController', function($filter,$state,$stateParams,$rootScope,$scope,Entity,$http,Org,cases,$timeout,cs_persons_vouchers,vouchers_categories) {

        $scope.id=$stateParams.id;
        $scope.case_vouchers = true;
        $scope.success =false;
        $scope.error =false;
        $scope.caseVouchersList=[];


        $scope.close=function(){
            $scope.success =false;
            $scope.failed =false;
            $scope.error =false;
            $scope.model_error_status =false;
            $scope.model_error_status2 =false;
        };

        Entity.get({entity:'entities',c:'currencies'},function (response) {
            $rootScope.currency = response.currencies;
        });
        Org.list({type:'sponsors'},function (response) { $rootScope.Sponsors = response; });

        var resetSearchFilters =function(){
            $scope.voucher_filters={
                                    "organization_id":"",
                                    "category_id":"",
                                    "voucher_type":"",
                                    "title"  :"",
                                    "voucher_date_from":"",
                                    "voucher_date_to":"",
                                    "content":"",
                                    "notes":"",
                                    "voucher_source":"",
                                    "receipt_date_from":"",
                                    "receipt_date_to":"",
                                    "receipt_location":""
                                   };
        };
        var LoadVouchers=function(params){
            params.case_id=$scope.id;
            params.action='filters';
            params.mode='show';
            params.target='case_vouchers';
            $rootScope.progressbar_start();
            cases.getCaseReports(params,function(response) {
                $rootScope.progressbar_complete();
                $scope.CurrentPage = response.data.current_page;
                    $scope.TotalItems = response.data.total;
                    $scope.ItemsPerPage = response.data.per_page;
                    $scope.caseVouchersList= response.data.data;
                      $scope.total= response.total;
            });
        };


        $scope.itemsPerPage_ = function (itemsCount) {
            $rootScope.clearToastr();
            LoadVouchers({itemsCount:itemsCount});
        };


        if($scope.id != null){
            cases.get({'id':$scope.id, 'action':'show', 'category_type' : 2, 'person' :true},function(response) {
                $scope.category_name = response.category_name;
                $scope.name = response.full_name;
            });

            resetSearchFilters();
            LoadVouchers({page:1});

            Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

            cs_persons_vouchers.list({id:-1},function (response) {
                $scope.Choices = response.Vouchers;
            });

            vouchers_categories.List(function (response) {
                $scope.Categories = response;
            });

        }

        $scope.toggleSearch = function() {
            $scope.case_vouchers = $scope.case_vouchers == false ? true: false;
            resetSearchFilters();
        };

        $scope.Search=function(data,action) {
            $rootScope.clearToastr();

        if( !angular.isUndefined(data.receipt_date_to)) {
            data.receipt_date_to=$filter('date')(data.receipt_date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.receipt_date_from)) {
            data.receipt_date_from= $filter('date')(data.receipt_date_from, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(data.voucher_date_to)) {
            data.voucher_date_to=$filter('date')(data.voucher_date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.voucher_date_from)) {
            data.voucher_date_from= $filter('date')(data.voucher_date_from, 'dd-MM-yyyy')
        }

        if(action == 'export'){
            $rootScope.progressbar_start();
            data.case_id=$scope.id;
            data.mode='show';
            data.target='case_vouchers';
            data.action=action;
            $http({
                url: "/api/v1.0/common/cases/getCaseReports",
                method: "POST",
                data: data,
            }).then(function (response) {
                $rootScope.progressbar_complete();
                var file_type='xlsx';
                 window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
            }, function(error) {
                $rootScope.toastrMessages('error',error.data.msg);
            });
        }else{
            LoadVouchers(data);
        }
    };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 0
        };
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {$scope.dt = new Date(); };
        $scope.today();
        $scope.clear = function() { $scope.dt = null; };

        $scope.popup9  = { opened: false };
        $scope.popup8  = { opened: false };
        $scope.popup99 = { opened: false };
        $scope.popup88 = { opened: false};
        $scope.popup999 = { opened: false };
        $scope.popup888 = { opened: false };

        $scope.open9  = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup9.opened = true;  };
        $scope.open8  = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup8.opened = true;  };
        $scope.open99 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup99.opened = true; };
        $scope.open88 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup88.opened = true; };
        $scope.open999 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup999.opened = true; };
        $scope.open888 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup888.opened = true;};

    });
