

angular.module('AidModule').controller('PersonalInfoController',function( $uibModal,$filter,$state,$stateParams,$rootScope,$scope,$http,$timeout,Entity,category,cases,persons) {


    $state.current.data.pageTitle = $filter('translate')('case form') + ' : ' + $filter('translate')('personal-info');

    $rootScope.ShowCitizenImage=false;
    $rootScope.setAuthUser();
    $rootScope.msg1=[];
    $scope.mg=" ";
    $scope.blocked=false;
    $rootScope.success_frm=false;
    $rootScope.success_msg="";
    $scope.exist=false;

    $rootScope.persons={banks:[]};
    $scope.priority=[];

    $scope.msg="";
    $scope.model_error_status=false;
    $scope.has_bank=false;

    $rootScope.category_id=$stateParams.category_id;
    $rootScope.id=$stateParams.id;
    $rootScope.mode=$stateParams.mode;
    $scope.mode=$stateParams.mode;
    $rootScope.has_image = false;

    if(angular.isUndefined($stateParams.mode) ||$stateParams.mode == null){
        $scope.mode=$rootScope.mode=$stateParams.mode="new";
    }
    if((!angular.isUndefined($stateParams.id) || $stateParams.id !== null) && !$stateParams.mode){
        $scope.mode=$rootScope.mode=$stateParams.mode="edit";
    }

    $scope.getLocation = function(entity,parent,value){
        if(!angular.isUndefined(value)) {
            if (value !== null && value !== "" && value !== " ") {
            Entity.listChildren({'entity': entity, 'id': value, 'parent': parent}, function (response) {

                if (entity == 'sdistricts') {
                    if (!angular.isUndefined($rootScope.msg1.adscountry_id)) {
                        $rootScope.msg1.adscountry_id = [];
                    }

                    $scope.governarate = response;
                    $scope.regions = [];
                    $scope.nearlocation = [];
                    $scope.squares = [];
                    $scope.mosques = [];

                    $rootScope.persons.adsdistrict_id = "";
                    $rootScope.persons.adsregion_id = "";
                    $rootScope.persons.adsneighborhood_id = "";
                    $rootScope.persons.adssquare_id = "";
                    $rootScope.persons.adsmosques_id = "";

                }
                else if (entity == 'sregions') {
                    if (!angular.isUndefined($rootScope.msg1.adsdistrict_id)) {
                        $rootScope.msg1.adsdistrict_id = [];
                    }

                    $scope.regions = response;
                    $scope.nearlocation = [];
                    $scope.squares = [];
                    $scope.mosques = [];
                    $rootScope.persons.adsregion_id = "";
                    $rootScope.persons.adsneighborhood_id = "";
                    $rootScope.persons.adssquare_id = "";
                    $rootScope.persons.adsmosques_id = "";
                }
                else if (entity == 'sneighborhoods') {
                    if (!angular.isUndefined($rootScope.msg1.adsregion_id)) {
                        $rootScope.msg1.adsregion_id = [];
                    }
                    $scope.nearlocation = response;
                    $scope.squares = [];
                    $scope.mosques = [];
                    $rootScope.persons.adsneighborhood_id = "";
                    $rootScope.persons.adssquare_id = "";
                    $rootScope.persons.adsmosques_id = "";

                }
                else if (entity == 'ssquares') {
                    if (!angular.isUndefined($rootScope.msg1.adssquare_id)) {
                        $rootScope.msg1.city = [];
                    }
                    $scope.squares = response;
                    $scope.mosques = [];
                    $rootScope.persons.adssquare_id = "";
                    $rootScope.persons.adsmosques_id = "";
                }
                else if (entity == 'smosques') {
                    if (!angular.isUndefined($rootScope.msg1.adssquare_id)) {
                        $rootScope.msg1.adssquare_id = [];
                    }
                    $scope.mosques = response;
                    $rootScope.persons.adsmosques_id = "";
                }
                });
            }
        }
    };
    $scope.getBranches=function(id){
        if( !angular.isUndefined($rootScope.msg1.bank_id)) {
            $rootScope.msg1.bank_id = [];
        }
        Entity.listChildren({entity:'branches',id:id,parent:'banks'},function (response) {
            $scope.Branches = response;
            $rootScope.persons.branch_name = "";
        });

    };
    $scope.RestValue = function(i,value){

        if(i==15){
            if( !angular.isUndefined($rootScope.msg1.is_qualified)) {
                $rootScope.msg1.is_qualified = [];
            }
            if(value != 1){
                if( !angular.isUndefined($rootScope.msg1.qualified_card_number)) {
                    $rootScope.msg1.qualified_card_number = [];
                }
                $rootScope.persons.qualified_card_number="";
            }
        }else if(i==1){
            if( !angular.isUndefined($rootScope.msg1.refugee)) {
                $rootScope.msg1.refugee = [];
            }
            if(value != 2){
                if( !angular.isUndefined($rootScope.msg1.unrwa_card_number)) {
                    $rootScope.msg1.unrwa_card_number = [];
                }
                $rootScope.persons.unrwa_card_number="";
            }
        }
        else if(i==2){
            if( !angular.isUndefined($rootScope.msg1.working)) {
                $rootScope.msg1.working = [];
            }

            if(value == 2){
                if( !angular.isUndefined($rootScope.msg1.work_job_id)) {
                    $rootScope.msg1.work_job_id = [];
                }
                if( !angular.isUndefined($rootScope.msg1.work_status_id)) {
                    $rootScope.msg1.work_status_id = [];
                }
                if( !angular.isUndefined($rootScope.msg1.work_wage_id)) {
                    $rootScope.msg1.work_wage_id = [];
                }

                if( !angular.isUndefined($rootScope.msg1.work_location)) {
                    $rootScope.msg1.work_location = [];
                }

                $rootScope.persons.work_job_id="";
                $rootScope.persons.work_status_id="";
                $rootScope.persons.work_wage_id="";
                $rootScope.persons.work_location="";
            }
        }
        else if(i==3){
            if( !angular.isUndefined($rootScope.msg1.can_work)) {
                $rootScope.msg1.can_work = [];
            }

            if(value == 1){
                if( !angular.isUndefined($rootScope.msg1.work_reason_id)) {
                    $rootScope.msg1.work_reason_id = [];
                }

                $rootScope.persons.work_reason_id="";
            }
        }
        else if(i==4){
            if( !angular.isUndefined($rootScope.msg1.condition)) {
                $rootScope.msg1.condition = [];
            }
            if(value == 1){
                if( !angular.isUndefined($rootScope.msg1.disease_id)) {
                    $rootScope.msg1.disease_id = [];
                }
                if( !angular.isUndefined($rootScope.msg1.details)) {
                    $rootScope.msg1.details = [];
                }

                $rootScope.persons.details="";
                $rootScope.persons.disease_id="";
            }
        }
        else if(i==5){
            if( !angular.isUndefined($rootScope.msg1.receivables)) {
                $rootScope.msg1.receivables = [];
            }
            if(value == 0){
                if( !angular.isUndefined($rootScope.msg1.receivables_sk_amount)) {
                    $rootScope.msg1.receivables_sk_amount = [];
                }
                $rootScope.persons.receivables_sk_amount="";
            }
        }
        else if(i==7){
            if( !angular.isUndefined($rootScope.msg1.has_device)) {
                $rootScope.msg1.has_device = [];
            }
            if(value == 0){
                if( !angular.isUndefined($rootScope.msg1.used_device_name)) {
                    $rootScope.msg1.used_device_name = [];
                }
                $rootScope.persons.used_device_name="";
            }
        }
        else if(i==6){
            if( !angular.isUndefined($rootScope.msg1.has_health_insurance)) {
                $rootScope.msg1.has_health_insurance = [];
            }
            if(value == 0){
                if( !angular.isUndefined($rootScope.msg1.health_insurance_type)) {
                    $rootScope.msg1.health_insurance_type = [];
                }
                if( !angular.isUndefined($rootScope.msg1.health_insurance_number)) {
                    $rootScope.msg1.health_insurance_number = [];
                }

                $rootScope.persons.health_insurance_type="";
                $rootScope.persons.health_insurance_number=0;
            }
        }

    };

    $scope.checkBirthday  = function(birthday){
        if( !angular.isUndefined($rootScope.msg1.birthday)) {
            $rootScope.msg1.birthday = [];
        }

        var curDate = new Date();

        if(new Date(birthday) > curDate){
            $rootScope.msg1.birthday = {0 :$filter('translate')('the enter date must be less than current date')};
            $scope.status1 ='failed_valid';
        }

    };

    $scope.FindPerson = function(id_card_number){

        $rootScope.clearToastr();
        if( $rootScope.model_error_status == true){
            $rootScope.model_error_status =false;
        }

        if( !angular.isUndefined($rootScope.msg1.id_card_number)) {
            $rootScope.msg1.id_card_number = [];
        }

        if(($stateParams.id ==null) || ( id_card_number =="" && $rootScope.mode=="new")){
            $stateParams.mode="new";
        }

        // if($stateParams.mode =="new"){
        if(angular.isUndefined($stateParams.id) || $stateParams.id ==null) {
            if( !angular.isUndefined(id_card_number)) {
                if(id_card_number.length ==9){

                    $rootScope.progressbar_start();
                    if($rootScope.check_id(id_card_number)){
                     var params={ 'mode':'edit', 'category_id' : $rootScope.category_id, 'id_card_number' :id_card_number,
                        'person' :true,
                        'persons_i18n' : true,
                        'contacts' : true,
                        'health' : true,
                        'work' : true,
                        'banks' : true
                    };

                    $rootScope.progressbar_complete();

                        persons.find(params,function(response){
                        $scope.exist=response.status;
                        const get_image = false;
                        if($scope.exist){
                            // $rootScope.mode="edit";
                            $rootScope.toastrMessages('error',response.msg);
                            $rootScope.persons             = response.person;
                            if($rootScope.ShowCitizenImage){
                                $rootScope.has_image = $rootScope.persons.has_image;
                                // $rootScope.loadPhoto($rootScope.persons.id_card_number);
                            }
                            $rootScope.persons.person_id = response.person.id;

                            if($rootScope.persons.card_type ==0 || $rootScope.persons.card_type ==null){
                                $rootScope.persons.card_type ="";
                            }else{
                                $rootScope.persons.card_type = $rootScope.persons.card_type +"";
                            }

                            delete $rootScope.persons.id;

                            if($rootScope.persons.case_id != null) {
                                $rootScope.id=$stateParams.id=$rootScope.persons.case_id;
                            }

                            $rootScope.full_name = $rootScope.persons.first_name +  " " +
                                $rootScope.persons.second_name + " " +
                                $rootScope.persons.third_name + " " +
                                $rootScope.persons.last_name ;

                            if($rootScope.persons.birthday =="0000-00-00"){
                                $rootScope.persons.birthday    = new Date();
                            }else{
                                $rootScope.persons.birthday    = new Date($rootScope.persons.birthday);
                            }


                            if($rootScope.persons.birth_place ==0 || $rootScope.persons.birth_place ==null){
                                $rootScope.persons.birth_place ="";
                            }else{
                                $rootScope.persons.birth_place = $rootScope.persons.birth_place +"";
                            }

                            if($rootScope.persons.nationality ==0 || $rootScope.persons.nationality ==null){
                                $rootScope.persons.nationality ="";
                            }else{
                                $rootScope.persons.nationality = $rootScope.persons.nationality +"";
                            }

                            if($rootScope.persons.gender ==0 || $rootScope.persons.gender ==null){
                                $rootScope.persons.gender ="";
                            }else{
                                $rootScope.persons.gender = $rootScope.persons.gender +"";
                            }

                            if($rootScope.persons.refugee ==0 || $rootScope.persons.refugee ==null){
                                $rootScope.persons.refugee ="";
                            }else{
                                $rootScope.persons.refugee = $rootScope.persons.refugee +"";
                            }

                            if($rootScope.persons.marital_status_id ==0 || $rootScope.persons.marital_status_id ==null){
                                $rootScope.persons.marital_status_id ="";
                            }else{
                                $rootScope.persons.marital_status_id = $rootScope.persons.marital_status_id +"";
                            }

                            if($rootScope.persons.adscountry_id ==null){
                                $rootScope.persons.adscountry_id     = "";
                            }else {
                                $rootScope.persons.country = $rootScope.persons.adscountry_id+ "";
                                Entity.listChildren({
                                    entity: 'sdistricts',
                                    'id': $rootScope.persons.adscountry_id,
                                    'parent': 'scountries'
                                }, function (response) {
                                    $scope.governarate = response;
                                });
                            }
                            if($rootScope.persons.adsdistrict_id ==null){
                                $rootScope.persons.adsdistrict_id = "";
                            }else {
                                $rootScope.persons.adsdistrict_id = $rootScope.persons.adsdistrict_id+ "";
                                Entity.listChildren({
                                    entity: 'sregions',
                                    'id': $rootScope.persons.adsdistrict_id,
                                    'parent': 'sdistricts'
                                }, function (response) {
                                    $scope.regions = response;
                                });

                            }
                            if($rootScope.persons.adsregion_id ==null){
                                $rootScope.persons.adsregion_id = "";
                            }else {
                                $rootScope.persons.adsregion_id = $rootScope.persons.adsregion_id+ "";
                                Entity.listChildren({
                                    entity: 'sneighborhoods',
                                    'id': $rootScope.persons.adsregion_id,
                                    'parent': 'sregions'
                                }, function (response) {
                                    $scope.nearlocation = response;
                                });

                            }
                             if($rootScope.persons.adsneighborhood_id ==null){
                                $rootScope.persons.adsneighborhood_id = "";
                            }else {
                                $rootScope.persons.adsneighborhood_id = $rootScope.persons.adsneighborhood_id+ "";
                                Entity.listChildren({
                                    entity: 'ssquares',
                                    'id': $rootScope.persons.adsneighborhood_id,
                                    'parent': 'sneighborhoods'
                                }, function (response) {
                                    $scope.squares = response;
                                });

                            }

                            if($rootScope.persons.adssquare_id ==null){
                                $rootScope.persons.adssquare_id = "";
                            }else{
                                $rootScope.persons.adssquare_id = $rootScope.persons.adssquare_id +"";
                                 Entity.listChildren({
                                    entity: 'smosques',
                                    'id': $rootScope.persons.adssquare_id,
                                    'parent': 'ssquares'
                                }, function (response) {
                                    $scope.mosques = response;
                                });
                            }
                           
                           if($rootScope.persons.adsmosques_id ==null){
                                $rootScope.persons.adsmosques_id = "";
                            }else{
                                $rootScope.persons.adsmosques_id = $rootScope.persons.adsmosques_id +"";
                            }

                            if($rootScope.persons.working == null) {
                                $rootScope.persons.working = "";
                                $rootScope.persons.work_status_id = "";
                                $rootScope.persons.work_job_id    = "";
                                $rootScope.persons.work_wage_id   = "";
                                $rootScope.persons.work_location  = "";

                            }else{
                                $rootScope.persons.working = $rootScope.persons.working +"";

                                if($rootScope.persons.working == 1) {
                                    $rootScope.persons.work_status_id = $rootScope.persons.work_status_id +"";
                                    $rootScope.persons.work_job_id    = $rootScope.persons.work_job_id +"";
                                    $rootScope.persons.work_wage_id   = $rootScope.persons.work_wage_id +"";
                                }else{
                                    $rootScope.persons.work_status_id = "";
                                    $rootScope.persons.work_job_id    = "";
                                    $rootScope.persons.work_wage_id   = "";
                                    $rootScope.persons.work_location  = "";
                                }

                            }

                            if($rootScope.persons.can_work == null) {
                                $rootScope.persons.can_work = "";
                                $rootScope.persons.work_reason_id = "";

                            }else{
                                $rootScope.persons.can_work = $rootScope.persons.can_work +"";

                                if($rootScope.persons.can_work == 2) {
                                    $rootScope.persons.work_reason_id = $rootScope.persons.work_status_id +"";
                                }else{
                                    $rootScope.persons.work_reason_id = "";
                                }

                            }

                            if($rootScope.persons.receivables ==0 || $rootScope.persons.receivables ==null){
                                $rootScope.persons.receivables_sk_amount ="";
                            }else{
                                $rootScope.persons.receivables = $rootScope.persons.receivables +"";
                            }

                            if($rootScope.persons.has_device ==0 || $rootScope.persons.has_device ==null){
                                $rootScope.persons.used_device_name ="";
                            }else{
                                $rootScope.persons.has_device = $rootScope.persons.has_device +"";
                            }

                            if($rootScope.persons.has_health_insurance ==0 || $rootScope.persons.has_health_insurance ==null){
                                $rootScope.persons.health_insurance_type ="";
                                $rootScope.persons.health_insurance_number ="";
                            }else{
                                $rootScope.persons.has_health_insurance = $rootScope.persons.has_health_insurance +"";
                            }

                        }
                        else{

                            if( !angular.isUndefined(response.blocked)) {
                                $rootScope.clearToastr();
                                $rootScope.toastrMessages('error',response.msg);
                            } else{
                                $rootScope.toastrMessages('error',response.msg);
                                $rootScope.persons = [];
                                if(response.msg){
                                    $rootScope.toastrMessages('error',response.msg);
                                    $rootScope.persons = [];
                                }else{
                                    $rootScope.clearToastr();
                                }

                                if(response.person){
                                    $rootScope.clearToastr();

                                    response.banks=[];
                                    $rootScope.persons    = response.person;
                                    // $rootScope.loadPhoto($rootScope.persons.id_card_number);

                                    if($rootScope.persons.birthday =="0000-00-00"){
                                        $rootScope.persons.birthday    = new Date();
                                    }else{
                                        $rootScope.persons.birthday    = new Date($rootScope.persons.birthday);
                                    }

                                    if($rootScope.persons.gender ==0 || $rootScope.persons.gender ==null){
                                        $rootScope.persons.gender ="";
                                    }else{
                                        $rootScope.persons.gender = $rootScope.persons.gender +"";
                                    }
                                    if($rootScope.persons.receivables ==0 || $rootScope.persons.receivables ==null){
                                        $rootScope.persons.receivables_sk_amount ="";
                                    }else{
                                        $rootScope.persons.receivables = $rootScope.persons.receivables +"";
                                    }

                                    if($rootScope.persons.has_device ==0 || $rootScope.persons.has_device ==null){
                                        $rootScope.persons.used_device_name ="";
                                    }else{
                                        $rootScope.persons.has_device = $rootScope.persons.has_device +"";
                                    }

                                    if($rootScope.persons.has_health_insurance ==0 || $rootScope.persons.has_health_insurance ==null){
                                        $rootScope.persons.health_insurance_type ="";
                                        $rootScope.persons.health_insurance_number ="";
                                    }else{
                                        $rootScope.persons.has_health_insurance = $rootScope.persons.has_health_insurance +"";
                                    }

                                }
                            }
                            $timeout(function () {
                                // document.getElementById('id_card_number').focus();
                            });
                            $rootScope.persons.id=null;
                            $rootScope.persons.person_id=null;
                            $rootScope.persons.case_id=null;

                        }
                    });
                    }else{
                        $rootScope.persons = [];
                        $rootScope.persons.id_card_number = id_card_number;
                        $rootScope.toastrMessages('error',$filter('translate')('invalid card number'));
                    }
                }else{
                    $scope.id_length_error=true;
                }

            }
        }
    };

    $scope.confirmInsert= function(persons){
        $rootScope.clearToastr();
        if( $rootScope.model_error_status == true){
            $rootScope.model_error_status =false;
        }

        if( ($stateParams.id !=null) || $rootScope.mode =="edit"){
            $stateParams.mode="edit";
        }

        if( !angular.isUndefined(persons.birthday)) {
            persons.birthday= $filter('date')(persons.birthday, 'dd-MM-yyyy');
        }
        persons.category_id=$stateParams.category_id;
        persons.category_type=2;
        persons.priority=$scope.priority;
        persons.target='case';

        $rootScope.progressbar_start();
        $http({
            method: 'POST',
            url: '/api/v1.0/common/aids/cases',
            data:persons
        }).then(function successCallback(response) {
            $rootScope.progressbar_complete();
            if(response.data.status=='failed_disabled')
            {
                $rootScope.toastrMessages('error',response.data.msg);
                $timeout(function() {
                    $state.go('aidCases',{"type": 'all',"mode": null,"status" : null});
                },3000);
            }
            else if(response.data.status=='failed_valid') {
                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                $scope.status1 =response.data.status;
                $rootScope.msg1 =response.data.msg;
            }
            else if(response.data.status=='failed')
            {
                $rootScope.toastrMessages('error',response.data.msg);
            }
            else if(response.data.status== 'success'){
               $rootScope.mode=$stateParams.mode="edit";
                $rootScope.id=response.data.case_id ;
                $rootScope.getNext();
            }
        }, function errorCallback(error) {
            $rootScope.toastrMessages('error',error.data.msg);
        });

    };

// Banks Account
    $scope.bankIns=function(size){
        $rootScope.clearToastr();
        $rootScope.success_frm=false;
        $uibModal.open({
            templateUrl: 'addBank.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: size ,
            controller: function ($rootScope,$scope,$stateParams, $modalInstance,Entity) {
                $scope.action='add';
                $scope.error_frm=false;
                $scope.msg3='';
                $scope.msg4={};
                $scope.detail={};
                $scope.getBranches=function(id){
                    if( !angular.isUndefined($scope.msg4.bank_id)) {
                        $scope.msg4.bank_id = [];
                    }

                    Entity.listChildren({parent:'banks',id:id,entity:'branches'},function (response) {
                        $scope.Branches = response;
                    });
                };

                Entity.list({entity:'banks'},function (response) {
                    $scope.Banks = response;
                });


                $scope.save = function (data) {

                    $scope.error_frm=false;
                    $scope.msg3='';
                    data.id=-1;

                    var flag =-1;
                    angular.forEach($rootScope.persons.banks, function(val,key) {
                        if(val.bank_id == data.bank_id){
                            flag = 1;
                            return;
                        }
                    });

                    if(flag == -1){

                        cases.checkAccount({'id':data.bank_id,'account_number':data.account_number},function(response){
                            if(response.status == true){
                                angular.forEach($scope.Banks, function(val,key) {
                                    if(val.id == data.bank_id){
                                        data.bank_name =val.name;
                                        return;
                                    }
                                });
                                angular.forEach($scope.Branches, function(val,key) {
                                    if(val.id == data.branch_name){
                                        data.branch =val.name;
                                        return;
                                    }
                                });

                                if(angular.isUndefined($rootScope.persons.banks)) {
                                    $rootScope.persons.banks=new Array();
                                    data.check=true;
                                }else{
                                    if($rootScope.persons.banks.length ==0 ){
                                        data.check=true;
                                    }else{
                                        data.check=false;
                                    }
                                }

                                $rootScope.persons.banks.push(data);
                                $rootScope.no_banks='true';
                                $rootScope.status1 ='failed_valid_1';
                                // $rootScope.msg1.banks=[];
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',$filter ('translate') ('this account previously in use of other person'));
                            }
                        });

                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('the account has been entered for the person in the same bank. Please delete the account or modify it'));
                    }
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }


        });
    };
    $scope.update=function(size,index,item){
        $rootScope.clearToastr();
        $rootScope.success_frm=false;

        $uibModal.open({
            templateUrl: 'addBank.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: size ,
            controller: function ($rootScope,$scope,$stateParams, $modalInstance,Entity) {

                $scope.error_frm=false;
                $scope.msg3='';
                $scope.msg4={};
                $scope.action='edit';
                $scope.getBranches=function(id){
                    if( !angular.isUndefined($scope.msg4.bank_id)) {
                        $scope.msg4.bank_id = [];
                    }
                    Entity.listChildren({parent:'banks',id:id,entity:'branches'},function (response) {
                        $scope.Branches = response;

                        if($scope.Branches.length ==0){
                            $scope.detail.branch_name = "";
                        }
                    });
                };

                Entity.list({entity:'banks'},function (response) {
                    $scope.Banks = response;
                });

                $scope.detail=item;
                if( $scope.detail.bank_id == null) {
                    $scope.detail.bank_id  = "";
                    $scope.detail.branch_name = "";
                    $scope.detail.account_number = null;
                    $scope.detail.account_owner = null;
                }else{
                    var branch_name =$scope.detail.branch_name;
                    $scope.getBranches($scope.detail.bank_id);
                    $scope.detail.bank_id = $scope.detail.bank_id +"";
                    if( $scope.detail.branch_name == null) {
                        $scope.detail.branch_name = "";
                    }else{
                        $scope.detail.branch_name = branch_name +"";
                    }
                }

                $scope.save = function (data) {
                    $scope.msg3='';
                    data.id=-1;

                    cases.checkAccount({'id':data.bank_id,'account_number':data.account_number},function(response){
                        if(response.status == true){
                            angular.forEach($scope.Banks, function(val,key) {
                                if(val.id == data.bank_id){
                                    data.bank_name =val.name;
                                    return;
                                }
                            });
                            angular.forEach($scope.Branches, function(val,key) {
                                if(val.id == data.branch_name){
                                    data.branch =val.name;
                                    return;
                                }
                            });
                            if(angular.isUndefined($rootScope.persons.banks)) {
                                $rootScope.persons.banks=new Array();
                                data.check=true;
                            }else{
                                if($rootScope.persons.banks.length ==0 ){
                                    data.check=true;
                                }else{
                                    data.check=false;
                                }
                            }

                            $rootScope.persons.banks[index]=data;
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));

                            $rootScope.no_banks='true';
                            $rootScope.status1 ='failed_valid_1';
                            // $rootScope.msg1.banks=[];
                            $modalInstance.close();
                        }else{
                            $rootScope.toastrMessages('error',$filter ('translate') ('this account previously in use of other person'));
                        }
                    });


                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }


        });
    };
    $scope.delete = function (index) {
        $rootScope.clearToastr();

        $rootScope.persons.banks.splice(index,1);
        if($rootScope.persons.banks.length == 0){
            $rootScope.no_banks='false';
        }
        $rootScope.toastrMessages('success',$filter('translate')('The row is deleted from db'));
    };
    $scope.setOrgDefault  = function(status,index,item){
        $rootScope.success_frm=false;
        var l=-1;
        var bk=-1;
        angular.forEach($rootScope.persons.banks, function(val,key) {
            if(val.check){
                l=key;
                bk=val;
            }
        });
        if(bk != -1){
            if( l!=index ){
                $rootScope.persons.banks[l].check=false;
                $rootScope.persons.banks[index].check=true;
                $rootScope.default_bank=item.bank_id;
            }
        }else{
            $rootScope.persons.banks[index].check=true;
            $rootScope.default_bank=item.bank_id;
        }

    };
    $rootScope.savePersonalImage =function (id_card_number) {
        $rootScope.progressbar_start();
        cases.savePhoto({'id_card_number' : id_card_number},function(response) {
            $rootScope.progressbar_complete();

            if(response.status == 'true' || response.status == true){
                $rootScope.toastrMessages('success',$filter('translate')('action success'));
            }else{
                $rootScope.toastrMessages('error',response.msg);

            }
        });
    };

    $rootScope.loadPhoto =function (id_card_number) {

        if(id_card_number.length ==9){
            $rootScope.has_image = false;
            if($rootScope.ShowCitizenImage){
                $http({
                    method: 'GET',
                    url: 'api/v1.0/common/cases/loadPhoto/' + id_card_number,
                    responseType: 'arraybuffer',
                    cache: false
                }).then(function (response_) {
                    $rootScope.has_image = true;
                    var blob = new Blob([response_.data], {type: response_.headers('Content-Type')});
                    url = (window.URL || window.webkitURL).createObjectURL(blob);
                    angular.element('#img').attr('src',url);
                });
            }
        }

    };
// DatePiker
    $scope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup1.opened = true;};
    $scope.popup1 = {opened: false};


    if(angular.isUndefined($stateParams.category_id) || $stateParams.category_id == null) {
        if($rootScope.mode !== 'new'){
            $state.go('aidCases',{"type": 'all'});
        }else{
            $state.go('categories-caseform',{"mode": $rootScope.mode ,"case_id" :$stateParams.id});
        }
    }else{

        $rootScope.ResetCaseForm($rootScope.category_id, $rootScope.mode,'personalInfo');
        $rootScope.CurrentStep=$rootScope.PersonalStepNumber;

        if($stateParams.mode !== "show") {
            category.getPriority({'category_id':$rootScope.category_id ,'target':'personalInfo','type':'aids'},function (response) {
                $scope.priority=response.priority;
            });

            Entity.get({entity:'entities',c:'countries,aidCountries,maritalStatus,workReasons,workStatus,workWages,workJobs,diseases,banks'},function (response) {
                $scope.Banks = response.banks;
                $scope.aidCountries = response.aidCountries;
                $rootScope.Diseases = response.diseases;
                $scope.Country = response.countries;
                $scope.MaritalStatus = response.maritalStatus;
                $scope.WorkReasons = response.workReasons;
                $scope.WorkStatus = response.workStatus;
                $scope.WorkWages = response.workWages;
                $scope.WorkJobs = response.workJobs;

            });
        }
        if($stateParams.mode =="show" || $stateParams.mode =="edit" || $stateParams.id != null) {
            var params={'case_id':$stateParams.id,
                'target':'info',
                'action':'filters',
                'mode':$stateParams.mode,
                'category_type' : 2,
                'person' :true,
                'persons_i18n' : true,
                'health' : true,
                'contacts' : true,
                'work' : true,
                'banks' : true
            };
            $rootScope.progressbar_start();
            cases.getCaseReports(params,function(response) {
                $rootScope.progressbar_complete();
                $rootScope.persons = response;

                if($rootScope.ShowCitizenImage){
                    $rootScope.has_image = $rootScope.persons.has_image;
                }
                $rootScope.persons.case_id = $rootScope.persons.id;
                $rootScope.full_name = $rootScope.persons.full_name;

                if( !angular.isUndefined($rootScope.persons.banks)) {
                    if($rootScope.persons.banks !== null && $rootScope.persons.banks.length !== 0){
                        $scope.has_bank=true;
                    }else{
                        $rootScope.persons.banks=[];
                        $scope.has_bank=false;
                    }
                }else{
                    $rootScope.persons.banks=[];
                    $scope.has_bank=false;
                }


                if($stateParams.mode =="edit"  || $stateParams.id != null) {

                    if($stateParams.mode !="show"){
                        if($rootScope.persons.birthday =="0000-00-00"){
                            $rootScope.persons.birthday    = new Date();
                        }else{
                            $rootScope.persons.birthday    = new Date($rootScope.persons.birthday);
                        }
                    }


                    if($rootScope.persons.condition == null) {
                        $rootScope.persons.disease_id = "";
                        $rootScope.persons.details  = "";

                    }else{
                        $rootScope.persons.condition = $rootScope.persons.condition +"";

                        if(!($rootScope.persons.condition == 2 || $rootScope.persons.condition == 3  ||
                            $rootScope.persons.condition == '2' || $rootScope.persons.condition == '3')) {
                            $rootScope.persons.disease_id = "";
                            $rootScope.persons.details  = "";
                        }
                    }

                    if($rootScope.persons.birth_place ==0 || $rootScope.persons.birth_place ==null){
                        $rootScope.persons.birth_place ="";
                    }else{
                        $rootScope.persons.birth_place = $rootScope.persons.birth_place +"";
                    }

                    if($rootScope.persons.nationality ==0 || $rootScope.persons.nationality ==null){
                        $rootScope.persons.nationality ="";
                    }else{
                        $rootScope.persons.nationality = $rootScope.persons.nationality +"";
                    }

                    if($rootScope.persons.gender ==0 || $rootScope.persons.gender ==null){
                        $rootScope.persons.gender ="";
                    }else{
                        $rootScope.persons.gender = $rootScope.persons.gender +"";
                    }

                    if($rootScope.persons.refugee ==0 || $rootScope.persons.refugee ==null){
                        $rootScope.persons.refugee ="";
                    }else{
                        $rootScope.persons.refugee = $rootScope.persons.refugee +"";
                    }

                    if($rootScope.persons.marital_status_id ==0 || $rootScope.persons.marital_status_id ==null){
                        $rootScope.persons.marital_status_id ="";
                    }else{
                        $rootScope.persons.marital_status_id = $rootScope.persons.marital_status_id +"";
                    }


                    if($rootScope.persons.adscountry_id ==null){
                        $rootScope.persons.adscountry_id     = "";
                    }else {
                        $rootScope.persons.country = $rootScope.persons.adscountry_id+ "";
                        Entity.listChildren({
                            entity: 'sdistricts',
                            'id': $rootScope.persons.adscountry_id,
                            'parent': 'scountries'
                        }, function (response) {
                            $scope.governarate = response;
                        });
                    }
                    if($rootScope.persons.adsdistrict_id ==null){
                        $rootScope.persons.adsdistrict_id = "";
                    }else {
                        $rootScope.persons.adsdistrict_id = $rootScope.persons.adsdistrict_id+ "";
                        Entity.listChildren({
                            entity: 'sregions',
                            'id': $rootScope.persons.adsdistrict_id,
                            'parent': 'sdistricts'
                        }, function (response) {
                            $scope.regions = response;
                        });

                    }
                    if($rootScope.persons.adsregion_id ==null){
                        $rootScope.persons.adsregion_id = "";
                    }else {
                        $rootScope.persons.adsregion_id = $rootScope.persons.adsregion_id+ "";
                        Entity.listChildren({
                            entity: 'sneighborhoods',
                            'id': $rootScope.persons.adsregion_id,
                            'parent': 'sregions'
                        }, function (response) {
                            $scope.nearlocation = response;
                        });

                    }
                    if($rootScope.persons.adsneighborhood_id ==null){
                        $rootScope.persons.adsneighborhood_id = "";
                    }else {
                        $rootScope.persons.adsneighborhood_id = $rootScope.persons.adsneighborhood_id+ "";
                        Entity.listChildren({
                            entity: 'ssquares',
                            'id': $rootScope.persons.adsneighborhood_id,
                            'parent': 'sneighborhoods'
                        }, function (response) {
                            $scope.squares = response;
                        });

                    }

                    if($rootScope.persons.adssquare_id ==null){
                        $rootScope.persons.adssquare_id = "";
                    }else{
                        $rootScope.persons.adssquare_id = $rootScope.persons.adssquare_id +"";
                        Entity.listChildren({
                            entity: 'smosques',
                            'id': $rootScope.persons.adssquare_id,
                            'parent': 'ssquares'
                        }, function (response) {
                            $scope.mosques = response;
                        });
                    }

                    if($rootScope.persons.adsmosques_id ==null){
                        $rootScope.persons.adsmosques_id = "";
                    }else{
                        $rootScope.persons.adsmosques_id = $rootScope.persons.adsmosques_id +"";
                    }


                    if($rootScope.persons.working == null) {
                        $rootScope.persons.working = "";
                        $rootScope.persons.work_status_id = "";
                        $rootScope.persons.work_job_id    = "";
                        $rootScope.persons.work_wage_id   = "";
                        $rootScope.persons.work_location  = "";

                    }else{
                        $rootScope.persons.working = $rootScope.persons.working +"";

                        if($rootScope.persons.working == 1) {
                            $rootScope.persons.work_status_id = $rootScope.persons.work_status_id +"";
                            $rootScope.persons.work_job_id    = $rootScope.persons.work_job_id +"";
                            $rootScope.persons.work_wage_id   = $rootScope.persons.work_wage_id +"";
                        }else{
                            $rootScope.persons.work_status_id = "";
                            $rootScope.persons.work_job_id    = "";
                            $rootScope.persons.work_wage_id   = "";
                            $rootScope.persons.work_location  = "";
                        }

                    }

                    if($rootScope.persons.can_work == null) {
                        $rootScope.persons.can_work = "";
                        $rootScope.persons.work_reason_id = "";

                    }else{
                        $rootScope.persons.can_work = $rootScope.persons.can_work +"";

                        if($rootScope.persons.can_work == 2) {
                            $rootScope.persons.work_reason_id = $rootScope.persons.work_reason_id+"";
                        }else{
                            $rootScope.persons.work_reason_id = "";
                        }
                    }

                    if($rootScope.persons.receivables ==0 || $rootScope.persons.receivables ==null){
                        $rootScope.persons.receivables_sk_amount ="";
                    }else{
                        $rootScope.persons.receivables = $rootScope.persons.receivables +"";
                    }

                    if($rootScope.persons.has_device ==0 || $rootScope.persons.has_device ==null){
                        $rootScope.persons.used_device_name ="";
                    }else{
                        $rootScope.persons.has_device = $rootScope.persons.has_device +"";
                    }

                    if($rootScope.persons.has_health_insurance ==0 || $rootScope.persons.has_health_insurance ==null){
                        $rootScope.persons.health_insurance_type ="";
                        $rootScope.persons.health_insurance_number ="";
                    }else{
                        $rootScope.persons.has_health_insurance = $rootScope.persons.has_health_insurance +"";
                    }



                }

            });
        }
    }
});
