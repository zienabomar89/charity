
angular.module('AidModule')
    .controller('aidCaseFormController',function($filter,$stateParams,$rootScope,$scope,$http,$timeout,$state,cases,category,cs_persons_vouchers,$uibModal,Entity,Org) {

        $rootScope.Step=null;
        $rootScope.mode=$scope.mode=$stateParams.mode;
        $rootScope.id=$stateParams.id ;
        $rootScope.category_id=$state.params.category_id;
        $rootScope.category_id=$stateParams.category_id ;
        $rootScope.category_name=null;
        $rootScope.family_cnt = 0;
        $rootScope.spouses = 0;
        $rootScope.male_live = 0;
        $rootScope.female_live = 0;

//-----------------------------------------------------------------------------------------------------
       $rootScope.ResetCaseForm= function(category_id,mode,step) {
           $rootScope.clearToastr();
           if(angular.isUndefined($rootScope.full_name)) {
               if($stateParams.id != null){
                   cases.getCaseReports({'case_id':$stateParams.id,
                       'target':'info',
                       'action':'filters',
                       'category':true,
                       'mode':'show',
                       'full_name' :true
                   },function(response) {

                       $rootScope.person_id = response.person_id;
                       $rootScope.organization_id = response.organization_id;
                       $rootScope.full_name = response.full_name;
                       $rootScope.category_name = response.category_name;
                       $rootScope.family_cnt = response.family_cnt;
                       $rootScope.spouses = response.spouses;
                       $rootScope.male_live = response.male_live;
                       $rootScope.female_live = response.female_live;

                   });
               }
           }

           if(angular.isUndefined($stateParams.category_id) || $stateParams.category_id == null) {
                 if($rootScope.mode !== 'new'){
                     $state.go('aidCases',{"type": 'all'});
                 }else{
                     $state.go('categories-caseform',{"mode": $rootScope.mode,"case_id" :$stateParams.id});
                 }
             }
            if($stateParams.category_id !== null && !angular.isUndefined($stateParams.category_id)) {
                category.getCategoryName({'id':$stateParams.category_id,'type':'aids'},function (response2){
                    $rootScope.category_name=response2.name;
                });
            }
            if(angular.isUndefined($rootScope.Steps)) {
                 $rootScope.Steps=[];
                 $rootScope.PersonalStep=false;
                 $rootScope.ResidenceStep=false;
                 $rootScope.HomeIndoorStep=false;
                 $rootScope.OtherStep=false;
                 $rootScope.VouchersStep=false;
                 $rootScope.FamilyStructureStep=false;
                 $rootScope.CaseDocumentStep=false;
                 $rootScope.CustomFormStep=false;
                 $rootScope.RecommendationsStep=false;
                 category.getCategoryFormsSections({'id':category_id ,'mode':mode,'type':'aids'},function (response){
                             $rootScope.FormSection=response.FormSection;
                             if($rootScope.FormSection.length !=0){
                                 $rootScope.TotalOfStep=$rootScope.FormSection.length;
                                 angular.forEach($rootScope.FormSection, function(v, k) {
                                     $rootScope.Steps[v.id]=k+1;

                                     if(v.id == 'personalInfo'){
                                         $rootScope.PersonalStep=true;
                                         $rootScope.PersonalStepNumber=k+1;

                                         if(step == 'personalInfo'){
                                             $rootScope.CurrentStep = k+1;
                                         }
                                         // $rootScope.Steps.push({'personalInfo' : k+1});
                                     }else if(v.id == 'residenceData'){
                                         $rootScope.ResidenceStep=true;
                                         $rootScope.ResidenceStepNumber=k+1;

                                         if(step == 'residenceData'){
                                             $rootScope.CurrentStep = k+1;
                                         }
                                         // $rootScope.Steps.push({'residenceData' : k+1});
                                     }else if(v.id == 'homeIndoorData'){
                                         $rootScope.HomeIndoorStep=true;
                                         $rootScope.HomeIndoorStepNumber=k+1;

                                         if(step == 'homeIndoorData'){
                                             $rootScope.CurrentStep = k+1;
                                         }
                                         // $rootScope.Steps.push({'homeIndoorData' : k+1});
                                     }else if(v.id == 'otherData'){
                                         $rootScope.OtherStep=true;
                                         $rootScope.OtherStepNumber=k+1;

                                         if(step == 'otherData'){
                                             $rootScope.CurrentStep = k+1;
                                         }
                                         // $rootScope.Steps.push({'otherData' : k+1});
                                     }else if(v.id == 'vouchersData'){
                                         $rootScope.VouchersStep=true;
                                         $rootScope.VouchersStepNumber=k+1;

                                         if(step == 'vouchersData'){
                                             $rootScope.CurrentStep = k+1;
                                         }
                                         // $rootScope.Steps.push({'vouchersData' : k+1});
                                     }else if(v.id == 'familyStructure'){
                                         $rootScope.FamilyStructureStep=true;
                                         $rootScope.FamilyStructureStepNumber=k+1;

                                         if(step == 'familyStructure'){
                                             $rootScope.CurrentStep = k+1;
                                         }
                                         // $rootScope.Steps.push({'familyStructure' : k+1});
                                     }else if(v.id == 'CaseDocumentData'){
                                         $rootScope.CaseDocumentStep=true;
                                         $rootScope.CaseDocumentStepNumber=k+1;

                                         if(step == 'CaseDocumentData'){
                                             $rootScope.CurrentStep = k+1;
                                         }
                                         // $rootScope.Steps.push({'CaseDocumentData' : k+1});
                                     }else if(v.id == 'customFormData'){
                                         $rootScope.CustomFormStep=true;
                                         $rootScope.CustomFormStepNumber=k+1;

                                         if(step == 'customFormData'){
                                             $rootScope.CurrentStep = k+1;
                                         }
                                         // $rootScope.Steps.push({'customFormData' : k+1});
                                     }else if(v.id == 'recommendations'){
                                         $rootScope.RecommendationsStep=true;
                                         $rootScope.RecommendationsStepNumber=k+1;

                                         if(step == 'recommendations'){
                                             $rootScope.CurrentStep = k+1;
                                         }
                                         // $rootScope.Steps.push({'recommendations' : k+1});
                                     }
                                 });
                             }
                         });
             }else{
                 $rootScope.CurrentStep = $rootScope.Steps[step];
            }
             $rootScope.getTarget(step);

         };
//-----------------------------------------------------------------------------------------------------
        if((angular.isUndefined($stateParams.id)  || $stateParams.id == null) && ( $stateParams.category_id !== null || ($rootScope.CurrentStep === 1  || $rootScope.CurrentStep === '1' ))) {
            $rootScope.mode='new';
        }

        // if($rootScope.mode !='new' ){
        //     if($stateParams.id == null ||angular.isUndefined($stateParams.id)) {
        //         $state.go('categories-caseform',{'mode':$rootScope.mode});
        //     }
        // }
       if($stateParams.category_id == null || angular.isUndefined($stateParams.category_id)) {

           if($rootScope.mode =='new'){
                $state.go('categories-caseform',{'mode':$rootScope.mode});
            }else{
                $state.go('aidCases',{"type": 'all','mode':$rootScope.mode});
            }
       }

       if($rootScope.mode !== 'new'){
            var sec=['#personalInfo','#residenceData','#homeIndoorData','#otherData','#vouchersData','#familyStructure'
                     ,'#customFormData','#CaseDocumentData','#recommendations'];

            angular.forEach(sec, function(val,key)
            {
                angular.element(val).removeClass("disabled");
            });
    }

//-----------------------------------------------------------------------------------------------------
        var TheState = function(name) {
            $state.go('caseform.'+ name,{
                "mode":$rootScope.mode,
                "id" :$rootScope.id,
                "category_id" :$rootScope.category_id
            });
        };

        $rootScope.getTarget= function(target) {
            var target_1='';
                if(target == 'personalInfo'){
                    angular.element('#personalInfo').removeClass("disabled");
                    $rootScope.CurrentStep=$rootScope.PersonalStepNumber;
                    target_1="personal-info";
                }
                else if(target == 'residenceData'){
                    angular.element('#residenceData').removeClass("disabled");
                    $rootScope.CurrentStep=$rootScope.ResidenceStepNumber;
                    target_1="residence-data";
                }
                else if(target == 'homeIndoorData'){
                    angular.element('#homeIndoorData').removeClass("disabled");
                    $rootScope.CurrentStep=$rootScope.HomeIndoorStepNumber;
                    target_1="home-indoor-data";
                }
                else if(target == 'otherData'){
                    angular.element('#otherData').removeClass("disabled");
                    $rootScope.CurrentStep=$rootScope.OtherStepNumber;
                    target_1="other-data";
                }
                else if(target == 'vouchersData'){
                    angular.element('#vouchersData').removeClass("disabled");
                    $rootScope.CurrentStep=$rootScope.VouchersStepNumber;
                    target_1="vouchers-data";
                }
                else if(target == 'familyStructure'){
                    angular.element('#familyStructure').removeClass("disabled");
                    $rootScope.CurrentStep=$rootScope.FamilyStructureStepNumber;
                    target_1="family-structure";
                }
                else if(target == 'customFormData'){
                    angular.element('#customFormData').removeClass("disabled");
                    $rootScope.CurrentStep=$rootScope.CustomFormStepNumber;
                    target_1="custom-form";
                }
                else if(target == 'CaseDocumentData'){
                    angular.element('#CaseDocumentData').removeClass("disabled");
                    $rootScope.CurrentStep=$rootScope.CaseDocumentStepNumber;
                    target_1="case-document";
                }
                else if(target == 'recommendations'){
                    angular.element('#recommendations').removeClass("disabled");
                    $rootScope.CurrentStep=$rootScope.RecommendationsStepNumber;
                    target_1="recommendations";
                }
            TheState(target_1);
        };
        $rootScope.getNext= function() {
            var next=null;
            next=$rootScope.CurrentStep;
            if($rootScope.TotalOfStep != $rootScope.CurrentStep ){
                var target =$rootScope.FormSection[next].id;
                $rootScope.getTarget(target);
            }else{
                $state.go('aidCases',{"type": 'all','status':'success','mode':$rootScope.mode});
            }
        };
        $rootScope.getPrevious= function() {
            var pre=-null;
            pre=$rootScope.CurrentStep-2;
            var t =$rootScope.FormSection[pre].id;
            if($rootScope.mode =='new'){
                $rootScope.mode='edit';
            }
            $rootScope.getTarget(t);
        };
        $rootScope.getTheState=function(name){
            TheState(name);
        };

        $rootScope.download = function (target,id) {

            $rootScope.clearToastr();
            var url ='';
            var file_type ='zip';
            var  params={};
            var method='';

            if(target =='word'){
                method='POST';
                params={'case_id':$rootScope.id,
                    'target':'info',
                    'action':'filters',
                    'mode':'show',
                    'category_type' : 2,
                    'person' :true,
                    'visitor_note' :true,
                    'health' : true,
                    'persons_i18n' : true,
                    'category' : true,
                    'contacts' : true,
                    'work' : true,
                    'banks' : true,
                    'default_bank' : true,
                    'residence' : true,
                    'case_needs' : true,
                    'reconstructions' : true,
                    'home_indoor' : true,
                    'financial_aid_source' : true,
                    'non_financial_aid_source' : true,
                    'persons_properties' : true,
                    'persons_documents' : true,
                    'family_member' : true
                };
                url = "/api/v1.0/common/aids/cases/word";
            }
            else if(target =='pdf'){
                method='POST';
                params={'case_id':$rootScope.id,
                    'target':'info',
                    'action':'filters',
                    'mode':'show',
                    'category_type' : 2,
                    'person' :true,
                    'visitor_note' :true,
                    'persons_i18n' : true,
                    'category' : true,
                    'contacts' : true,
                    'work' : true,
                    'banks' : true,
                    'default_bank' : true,
                    'health' : true,
                    'residence' : true,
                    'case_needs' : true,
                    'reconstructions' : true,
                    'home_indoor' : true,
                    'financial_aid_source' : true,
                    'non_financial_aid_source' : true,
                    'persons_properties' : true,
                    'persons_documents' : true,
                    'family_member' : true
                };
                url = "/api/v1.0/common/aids/cases/pdf";
            }else{
                method='GET';
                url = "/api/v1.0/common/aids/cases/exportCaseDocument/"+$rootScope.id;
            }

            $http({
                url:url,
                method: method,
                data:params
            }).then(function (response) {

                if(target =='archive' && response.data.status == false){
                    $rootScope.toastrMessages('error',$filter('translate')('no attachment to export'));
                }else{
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }
            }, function(error) {
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };
        $rootScope.goTo=function(action,case_id,category_id){
            angular.forEach($rootScope.aidCategories, function(v, k) {
                if(v.id ==category_id){
                    $state.go('caseform.'+v.FirstStep,{"mode":action,"id" :case_id,"category_id" :category_id});
                }
            });

        };
        $scope.urgentVoucher=function(size,person_id,case_id,case_category_id){
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'urgentVoucher.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope, $modalInstance, $log,cs_persons_vouchers,Entity) {
                    $scope.voucher={urgent:1,count:1,persons:[person_id],case_id:case_id,case_category_id:case_category_id};

                    Entity.get({entity:'entities',c:'currencies'},function (response) {
                        $rootScope.currency = response.currencies;
                    });
                    Org.list({type:'sponsors'},function (response) { $rootScope.Sponsor = response; });


                    $scope.confirm = function (voucher) {
                        $rootScope.clearToastr();
                        $rootScope.progressbar_start();
                        if( !angular.isUndefined(voucher.voucher_date)) {
                            voucher.voucher_date=$filter('date')(voucher.voucher_date, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(voucher.receipt_date)) {
                            voucher.receipt_date=$filter('date')(voucher.receipt_date, 'dd-MM-yyyy')
                        }

                        cs_persons_vouchers.save(voucher,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                $modalInstance.close();
                                if(response.status=='success')
                                {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }
                        }, function(error) {
                            $modalInstance.close();
                            $rootScope.toastrMessages('error',error.data.msg);
                        });

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.open = function($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.popup.opened = true;
                    };

                    $scope.popup = {
                        opened: false
                    };
                    $scope.open45 = function($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.popup45.opened = true;
                    };

                    $scope.popup45 = {
                        opened: false
                    };

                }});
        };

        $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $rootScope.format = $scope.formats[0];
        $rootScope.today = function() {$rootScope.dt = new Date();};
        $rootScope.today();
        $rootScope.clear = function() {$rootScope.dt = null;};

    });
