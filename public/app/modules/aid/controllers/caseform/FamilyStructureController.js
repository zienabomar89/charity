
angular.module('AidModule')
    .filter('ageFilter', function($filter) {
        function getAge(birthday) { // birthday is a date

            if(!birthday) // if a is negative,undefined,null,empty value then...
            {
                return  0 + $filter('translate')('year')  ;
            }

            var today = new Date();
            var birthDate = new Date(birthday);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }

            return (age == 0) ? (m > 1) ?  m +  $filter('translate')('month')+"/"+$filter('translate')('months') : m + $filter('translate')('month')+"/"+$filter('translate')('months')  : (age > 1) ? age +$filter('translate')('year')+"/"+$filter('translate')('years') : age +$filter('translate')('year')+"/"+$filter('translate')('years')  ;
        }

        return function(birthdate) {
            return getAge(birthdate);
        };
    })
    .controller('FamilyStructureController',function($filter,$stateParams,$state,$rootScope,$scope,$http,$timeout,$uibModal,category,$ngBootbox, Entity,persons,cases) {

        $state.current.data.pageTitle = $filter('translate')('family-structure');
        $scope.msg1={};

        $scope.members=[];
        $scope.persons=[];
        $rootScope.persons=[];

        $scope.result = 'false';
        $rootScope.disable_=false;
        $rootScope.load_not_inserted= true;
        $rootScope.category_id=$stateParams.category_id;
        $rootScope.mode=$stateParams.mode;
        $scope.mode=$stateParams.mode;

        if(angular.isUndefined($stateParams.mode) ||$stateParams.mode == null){
            $scope.mode=$rootScope.mode=$stateParams.mode="new";
        }
        if((!angular.isUndefined($stateParams.id) || $stateParams.id !== null) && !$stateParams.mode){
            $scope.mode=$rootScope.mode=$stateParams.mode="edit";
        }

        $rootScope.ResetCaseForm($rootScope.category_id, $rootScope.mode,'familyStructure');
        $rootScope.CurrentStep=$rootScope.FamilyStructureStepNumber;

        var LoadFamily=function() {

            var params ={'case_id':$stateParams.id,
                'target':'info',
                'action':'filters',
                'mode':'show',
                'family_member' : true};

            if($rootScope.load_not_inserted == true &&($rootScope.mode =='new' || $rootScope.mode =='edit')){
                params.citizen = true;
                params.citizen_target = 'aids';
            }
            $rootScope.progressbar_start();
            cases.getCaseReports(params,function(response) {
                $rootScope.progressbar_complete();
                if($rootScope.load_not_inserted == true){
                    $rootScope.members_not_inserted = response.family_member_not_inserted;
                }
                $scope.members = response.family_member;
                $scope.male_members = response.male_children;
                $scope.male_members = response.male_children;
                $scope.female_members = response.female_children;
                $scope.l_person_id = response.person_id;
                $rootScope.l_person_id = response.person_id;

                $rootScope.family_cnt = response.family_cnt;
                $rootScope.spouses = response.spouses;
                $rootScope.male_live = response.male_live;
                $rootScope.female_live = response.female_live;
            });
        };

        $scope.delete = function (size,id,l_person_id) {
            $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                .then(function() {
                    $rootScope.progressbar_start();
                    $rootScope.clearToastr();
                    persons.delete({id:id,rel:'delete'},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status == 'success'){
                            $rootScope.load_not_inserted = true ;
                            LoadFamily();
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                });
        };
        $scope.open= function (action,id) {
            $rootScope.operation=action;
            if(action =='edit' || action == 'show'){
                var params={'target':'info','person_id':id,'mode':action,'person' :true,'l_person_id': $rootScope.l_person_id,
                    'health' : true,  'persons_i18n' : true,  'education' : true, 'work' : true};
                $rootScope.progressbar_start();
                persons.getPersonReports(params,function (response) {
                    $rootScope.progressbar_complete();
                    $rootScope.persons=response;

                    if(action =='edit'){
                        if($rootScope.persons.gender == null){
                            $rootScope.persons.gender ="";
                        }else{
                            $rootScope.persons.gender = $rootScope.persons.gender +"";
                        }

                        if($rootScope.persons.marital_status_id == null){
                            $rootScope.persons.marital_status_id ="";
                        }else{
                            $rootScope.persons.marital_status_id = $rootScope.persons.marital_status_id +"";
                        }

                        if($rootScope.persons.birth_place == null){
                            $rootScope.persons.birth_place ="";
                        }else{
                            $rootScope.persons.birth_place = $rootScope.persons.birth_place +"";
                        }

                        if($rootScope.persons.birthday =="0000-00-00"){
                            $rootScope.persons.birthday    = new Date();
                        }else{
                            $rootScope.persons.birthday    = new Date($rootScope.persons.birthday);
                        }

                        if($rootScope.persons.kinship_id != null){
                            $rootScope.persons.kinship_id = $rootScope.persons.kinship_id +"";
                        }else{
                            $rootScope.persons.kinship_id = "";
                        }

                        if($rootScope.persons.working == null){
                            $rootScope.persons.working = "";
                            $rootScope.persons.work_job_id = "";
                            $rootScope.persons.work_location  ="";
                        }else{
                            $rootScope.persons.working = $rootScope.persons.working +"";

                            if($rootScope.persons.work_job_id != null){
                                $rootScope.persons.work_job_id = $rootScope.persons.work_job_id +"";
                            }else{
                                $rootScope.persons.work_job_id="";
                            }

                        }


                        if($rootScope.persons.currently_study == null){
                            $rootScope.persons.currently_study = "";
                        }else{
                            $rootScope.persons.currently_study= $rootScope.persons.currently_study +"";
                        }


                        if($rootScope.persons.study == null){
                            $rootScope.persons.study = "";
                        }else{
                            $rootScope.persons.study= $rootScope.persons.study +"";
                        }

                        if($rootScope.persons.grade == null){
                            $rootScope.persons.grade = "";
                        }else{
                            $rootScope.persons.grade= $rootScope.persons.grade +"";
                        }

                        if($rootScope.persons.stage == null){
                            $rootScope.persons.stage = "";
                        }else{
                            $rootScope.persons.stage= $rootScope.persons.stage +"";
                        }

                        if($rootScope.persons.degree == null){
                            $rootScope.persons.degree = "";
                        }else{
                            $rootScope.persons.degree= $rootScope.persons.degree +"";
                        }

                        if($rootScope.persons.authority == null){
                            $rootScope.persons.authority = "";
                        }else{
                            $rootScope.persons.authority = $rootScope.persons.authority  +"";
                        }

                        if($rootScope.persons.condition == null || $rootScope.persons.condition == ""){
                            $rootScope.persons.disease_id  = "";
                            $rootScope.persons.details  = "";
                        }else{
                            $rootScope.persons.condition  = $rootScope.persons.condition +"";
                            if($rootScope.persons.disease_id == null){
                                $rootScope.persons.disease_id = "";
                            }else{
                                $rootScope.persons.disease_id  = $rootScope.persons.disease_id +"";
                            }
                        }
                    }

                }, function(error) {
                    $rootScope.toastrMessages('error',error.data.msg);
                });

            }
            else if(action =='add'){
                $rootScope.persons={};
            }

            $uibModal.open({
                templateUrl: 'save.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance) {
                    $scope.msg1={};
                    $scope.ResetData=function(target,value){
                        if(target==1){
                            if( !angular.isUndefined($scope.msg1.working)) {
                                $scope.msg1.working = [];
                            }

                            if(value == 2){
                                if( !angular.isUndefined($scope.msg1.work_job_id)) {
                                    $scope.msg1.work_job_id = [];
                                }
                                if( !angular.isUndefined($scope.msg1.work_status_id)) {
                                    $scope.msg1.work_status_id = [];
                                }
                                if( !angular.isUndefined($scope.msg1.work_wage_id)) {
                                    $scope.msg1.work_wage_id = [];
                                }

                                if( !angular.isUndefined($scope.msg1.work_location)) {
                                    $scope.msg1.work_location = [];
                                }

                                $rootScope.persons.work_job_id="";
                                $rootScope.persons.work_status_id="";
                                $rootScope.persons.work_wage_id="";
                                $rootScope.persons.work_location="";

                            }

                        }else if(target==2){
                            if( !angular.isUndefined($scope.msg1.condition)) {
                                $scope.msg1.condition = [];
                            }

                            if(value == 1){
                                if( !angular.isUndefined($scope.msg1.disease_id)) {
                                    $scope.msg1.disease_id = [];
                                }
                                if( !angular.isUndefined($scope.msg1.details)) {
                                    $scope.msg1.details = [];
                                }

                                $rootScope.persons.disease_id="";
                                $rootScope.persons.details="";

                            }
                        }
                    };
                    $scope.Reset=function(value){
                        if( !angular.isUndefined($scope.msg1.study)) {
                            $scope.msg1.study = [];
                        }

                        if(value == 0){
                            if( !angular.isUndefined($scope.msg1.authority)) {
                                $scope.msg1.authority = [];
                            }
                            if( !angular.isUndefined($scope.msg1.school)) {
                                $scope.msg1.school = [];
                            }
                            if( !angular.isUndefined($scope.msg1.stage)) {
                                $scope.msg1.stage = [];
                            }

                            if( !angular.isUndefined($scope.msg1.grade)) {
                                $scope.msg1.grade = [];
                            }
                            if( !angular.isUndefined($scope.msg1.degree)) {
                                $scope.msg1.degree = [];
                            }

                            $rootScope.persons.authority="";
                            $rootScope.persons.grade="";
                            $rootScope.persons.degree="";
                            $rootScope.persons.school="";
                            $rootScope.persons.stage="";

                        }


                    };

                    $scope.confirm = function (target) {
                        $rootScope.clearToastr();
                        $rootScope.progressbar_start();
                        // target.father_id= $rootScope.l_person_id;
                        target.l_person_id= $rootScope.l_person_id;
                        target.l_person_update= true;
                        target.priority= $rootScope.priority;

                        if( !angular.isUndefined(target.birthday)) {
                            target.birthday=$filter('date')(target.birthday, 'dd-MM-yyyy')
                        }

                        target.no_en=true;
                        if(target.id){
                            target.person_id=target.id;
                            delete target.id;
                        }

                        persons.save(target,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid') {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else if(response.status== 'success'){
                                $modalInstance.close();
                                LoadFamily();
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            }
                        }, function(error) {
                            $rootScope.toastrMessages('error',error.data.msg);
                        });

                    };
                    $scope.isPersonExists = function(id_card_number){
                        $rootScope.clearToastr();
                        $scope.status1 ='';
                        $scope.msg2='';
                        $scope.msg1={};
                        if( !angular.isUndefined($scope.msg1.id_card_number)) {
                            $scope.msg1.id_card_number = [];
                        }

                        if($rootScope.operation =='add'){
                            if(!angular.isUndefined(id_card_number)){
                                if(id_card_number.length==9 ) {
                                    if($rootScope.check_id(id_card_number)) {
                                        // cs_persons_family_structure.isPersonExists({'id':id_card_number} ,function (response) {
                                        var params = {
                                            'target': 'info',
                                            'id_card_number': id_card_number,
                                            'mode': 'edit',
                                            'person': true,
                                            'l_person_id': $rootScope.l_person_id,
                                            'persons_i18n': true,
                                            'health': true,
                                            'education': true,
                                            'work': true
                                        };
                                        $rootScope.progressbar_start();
                                        persons.find(params, function (response) {
                                            $rootScope.progressbar_complete();
                                            if (response.status == true) {
                                                $rootScope.toastrMessages('error', response.msg);
                                                $rootScope.persons = response.person;

                                                if ($rootScope.persons.birthday == "0000-00-00") {
                                                    $rootScope.persons.birthday = new Date();
                                                } else {
                                                    $rootScope.persons.birthday = new Date($rootScope.persons.birthday);
                                                }
                                                if ($rootScope.persons.gender == 0 || $rootScope.persons.gender == null) {
                                                    $rootScope.persons.gender = "";
                                                } else {
                                                    $rootScope.persons.gender = $rootScope.persons.gender + "";
                                                }
                                                if ($rootScope.persons.marital_status_id == 0 || $rootScope.persons.marital_status_id == null) {
                                                    $rootScope.persons.marital_status_id = "";
                                                } else {
                                                    $rootScope.persons.marital_status_id = $rootScope.persons.marital_status_id + "";
                                                }
                                                if ($rootScope.persons.birth_place == null) {
                                                    $rootScope.persons.birth_place = "";
                                                } else {
                                                    $rootScope.persons.birth_place = $rootScope.persons.birth_place + "";
                                                }
                                                if ($rootScope.persons.working == null) {
                                                    $rootScope.persons.working = "";
                                                    $rootScope.persons.work_job_id = "";
                                                    $rootScope.persons.work_location = "";
                                                } else {
                                                    $rootScope.persons.working = $rootScope.persons.working + "";
                                                    if ($rootScope.persons.working == 1) {
                                                        $rootScope.persons.work_job_id = $rootScope.persons.work_job_id + "";
                                                    } else {
                                                        $rootScope.persons.work_job_id = "";
                                                        $rootScope.persons.work_location = "";
                                                    }
                                                }

                                                if ($rootScope.persons.kinship_id == null) {
                                                    $rootScope.persons.kinship_id = "";
                                                } else {
                                                    $rootScope.persons.kinship_id = $rootScope.persons.kinship_id + "";
                                                }

                                                if($rootScope.persons.study == null){
                                                    $rootScope.persons.study = "";
                                                }else{
                                                    $rootScope.persons.study= $rootScope.persons.study +"";
                                                }

                                                if ($rootScope.persons.study == null) {
                                                    $rootScope.persons.study = "";
                                                    $rootScope.persons.stage = "";
                                                    $rootScope.persons.grade = "";
                                                    $rootScope.persons.degree = "";
                                                    $rootScope.persons.authority = "";
                                                    $rootScope.persons.school = "";
                                                } else {
                                                    $rootScope.persons.study = $rootScope.persons.study + "";

                                                    if ($rootScope.persons.study == 0) {
                                                        $rootScope.persons.stage = "";
                                                        $rootScope.persons.grade = "";
                                                        $rootScope.persons.degree = "";
                                                        $rootScope.persons.authority = "";
                                                        $rootScope.persons.school = "";
                                                    } else {
                                                        if ($rootScope.persons.grade == null) {
                                                            $rootScope.persons.grade = "";
                                                        } else {
                                                            $rootScope.persons.grade = $rootScope.persons.grade + "";
                                                        }
                                                        if ($rootScope.persons.stage == null) {
                                                            $rootScope.persons.stage = "";
                                                        } else {
                                                            $rootScope.persons.stage = $rootScope.persons.stage + "";
                                                        }
                                                        if ($rootScope.persons.degree == null) {
                                                            $rootScope.persons.degree = "";
                                                        } else {
                                                            $rootScope.persons.degree = $rootScope.persons.degree + "";
                                                        }
                                                        if ($rootScope.persons.authority == null) {
                                                            $rootScope.persons.authority = "";
                                                        } else {
                                                            $rootScope.persons.authority = $rootScope.persons.authority + "";
                                                        }
                                                    }
                                                }


                                                if ($rootScope.persons.condition == null || $rootScope.persons.condition == "") {
                                                    $rootScope.persons.condition = "";
                                                    $rootScope.persons.disease_id = "";
                                                    $rootScope.persons.details = "";
                                                } else {
                                                    $rootScope.persons.condition = $rootScope.persons.condition + "";

                                                    if ($rootScope.persons.disease_id == null) {
                                                        $rootScope.persons.disease_id = "";
                                                    } else {
                                                        $rootScope.persons.disease_id = $rootScope.persons.disease_id + "";
                                                    }
                                                }
                                            }
                                            else {

                                                if (response.person) {
                                                    $rootScope.persons = response.person;

                                                    if ($rootScope.persons.birthday == "0000-00-00") {
                                                        $rootScope.persons.birthday = new Date();
                                                    } else {
                                                        $rootScope.persons.birthday = new Date($rootScope.persons.birthday);
                                                    }

                                                    if ($rootScope.persons.gender == 0 || $rootScope.persons.gender == null) {
                                                        $rootScope.persons.gender = "";
                                                    } else {
                                                        $rootScope.persons.gender = $rootScope.persons.gender + "";
                                                    }
                                                }
                                                if (response.msg) {
                                                    $rootScope.persons = [];
                                                    $rootScope.toastrMessages('error', response.msg);
                                                } else {
                                                    $scope.status1 = '';
                                                    $scope.msg2 = '';
                                                }
                                                $rootScope.persons.id = null;
                                            }

                                        });
                                    }else{
                                        $rootScope.toastrMessages('error',$filter('translate')('invalid card number'));
                                    }
                                }
                            }
                        }
                    };

                    $scope.dateOptions = {
                        formatYear: 'yy',
                        startingDay: 0
                    };

                    $scope.formats = [ 'dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                    $scope.format = $scope.formats[0];


                    $scope.today = function()
                    {
                        $scope.dt = new Date();
                    };

                    $scope.today();

                    $scope.clear = function()
                    {
                        $scope.dt = null;
                    };

                    $scope.open4 = function($event)
                    {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.popup4.opened = true;
                    };

                    $scope.popup4 = {
                        opened: false
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

        $scope.confirmAdd= function (data,index) {

            $rootScope.clearToastr();
            $rootScope.operation = 'add';
            $rootScope.persons = angular.copy(data);
            $rootScope.disable_=true;

            if (data.person_id) {
                var params={'target':'info','person_id':data.person_id,
                    'mode':'edit','person' :true,'l_person_id': $rootScope.l_person_id, 'persons_i18n' : true,'health' : true,  'education' : true, 'work' : true};
                persons.getPersonReports(params,function (response) {
                    $rootScope.persons=response;
                    $rootScope.persons.kinship_id = data.kinship_id +"";
                    if($rootScope.persons.gender == null){
                        $rootScope.persons.gender ="";
                    }else{
                        $rootScope.persons.gender = $rootScope.persons.gender +"";
                    }

                    if($rootScope.persons.marital_status_id == null){
                        $rootScope.persons.marital_status_id ="";
                    }else{
                        $rootScope.persons.marital_status_id = $rootScope.persons.marital_status_id +"";
                    }

                    if($rootScope.persons.birth_place == null){
                        $rootScope.persons.birth_place ="";
                    }else{
                        $rootScope.persons.birth_place = $rootScope.persons.birth_place +"";
                    }

                    if($rootScope.persons.birthday =="0000-00-00"){
                        $rootScope.persons.birthday    = new Date();
                    }else{
                        $rootScope.persons.birthday    = new Date($rootScope.persons.birthday);
                    }



                    if($rootScope.persons.working == null){
                        $rootScope.persons.working = "";
                        $rootScope.persons.work_job_id = "";
                        $rootScope.persons.work_location  ="";
                    }else{
                        $rootScope.persons.working = $rootScope.persons.working +"";

                        if($rootScope.persons.work_job_id != null){
                            $rootScope.persons.work_job_id = $rootScope.persons.work_job_id +"";
                        }else{
                            $rootScope.persons.work_job_id="";
                        }

                    }


                    if($rootScope.persons.study == null){
                        $rootScope.persons.study = "";
                    }else{
                        $rootScope.persons.study= $rootScope.persons.study +"";
                    }

                    if($rootScope.persons.currently_study == null){
                        $rootScope.persons.currently_study = "";
                    }else{
                        $rootScope.persons.currently_study= $rootScope.persons.currently_study +"";
                    }

                    if($rootScope.persons.grade == null){
                        $rootScope.persons.grade = "";
                    }else{
                        $rootScope.persons.grade= $rootScope.persons.grade +"";
                    }

                    if($rootScope.persons.stage == null){
                        $rootScope.persons.stage = "";
                    }else{
                        $rootScope.persons.stage= $rootScope.persons.stage +"";
                    }

                    if($rootScope.persons.degree == null){
                        $rootScope.persons.degree = "";
                    }else{
                        $rootScope.persons.degree= $rootScope.persons.degree +"";
                    }

                    if($rootScope.persons.authority == null){
                        $rootScope.persons.authority = "";
                    }else{
                        $rootScope.persons.authority = $rootScope.persons.authority  +"";
                    }

                    if($rootScope.persons.condition == null || $rootScope.persons.condition == ""){
                        $rootScope.persons.disease_id  = "";
                        $rootScope.persons.details  = "";
                    }else{
                        $rootScope.persons.condition  = $rootScope.persons.condition +"";
                        if($rootScope.persons.disease_id == null){
                            $rootScope.persons.disease_id = "";
                        }else{
                            $rootScope.persons.disease_id  = $rootScope.persons.disease_id +"";
                        }
                    }
                });

            } else {
                if ($rootScope.persons.birthday == "0000-00-00") {
                    $rootScope.persons.birthday = new Date();
                } else {
                    $rootScope.persons.birthday = new Date($rootScope.persons.birthday);
                }
                $rootScope.persons.kinship_id = data.kinship_id +"";
            }

            $uibModal.open({
                templateUrl: 'save.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance) {
                    $scope.msg1={};
                    $scope.ResetData=function(target,value){
                        $rootScope.clearToastr();
                        if(target==1){
                            if( !angular.isUndefined($scope.msg1.working)) {
                                $scope.msg1.working = [];
                            }

                            if(value == 2){
                                if( !angular.isUndefined($scope.msg1.work_job_id)) {
                                    $scope.msg1.work_job_id = [];
                                }
                                if( !angular.isUndefined($scope.msg1.work_status_id)) {
                                    $scope.msg1.work_status_id = [];
                                }
                                if( !angular.isUndefined($scope.msg1.work_wage_id)) {
                                    $scope.msg1.work_wage_id = [];
                                }

                                if( !angular.isUndefined($scope.msg1.work_location)) {
                                    $scope.msg1.work_location = [];
                                }

                                $rootScope.persons.work_job_id="";
                                $rootScope.persons.work_status_id="";
                                $rootScope.persons.work_wage_id="";
                                $rootScope.persons.work_location="";

                            }

                        }else if(target==2){
                            if( !angular.isUndefined($scope.msg1.condition)) {
                                $scope.msg1.condition = [];
                            }

                            if(value == 1){
                                if( !angular.isUndefined($scope.msg1.disease_id)) {
                                    $scope.msg1.disease_id = [];
                                }
                                if( !angular.isUndefined($scope.msg1.details)) {
                                    $scope.msg1.details = [];
                                }

                                $rootScope.persons.disease_id="";
                                $rootScope.persons.details="";

                            }
                        }
                    };
                    $scope.Reset=function(value){
                        if( !angular.isUndefined($scope.msg1.study)) {
                            $scope.msg1.study = [];
                        }

                        if(value == 0){
                            if( !angular.isUndefined($scope.msg1.authority)) {
                                $scope.msg1.authority = [];
                            }
                            if( !angular.isUndefined($scope.msg1.school)) {
                                $scope.msg1.school = [];
                            }
                            if( !angular.isUndefined($scope.msg1.stage)) {
                                $scope.msg1.stage = [];
                            }

                            if( !angular.isUndefined($scope.msg1.grade)) {
                                $scope.msg1.grade = [];
                            }
                            if( !angular.isUndefined($scope.msg1.degree)) {
                                $scope.msg1.degree = [];
                            }

                            $rootScope.persons.authority="";
                            $rootScope.persons.grade="";
                            $rootScope.persons.degree="";
                            $rootScope.persons.school="";
                            $rootScope.persons.stage="";

                        }


                    };
                    $scope.confirm = function (target) {
                        $rootScope.clearToastr();
                        // target.father_id= $rootScope.l_person_id;
                        target.l_person_id= $rootScope.l_person_id;
                        target.l_person_update= true;
                        target.priority= $rootScope.priority;

                        if( !angular.isUndefined(target.birthday)) {
                            target.birthday=$filter('date')(target.birthday, 'dd-MM-yyyy')
                        }

                        // target.no_en=true;
                        if(target.id){
                            target.person_id=target.id;
                            delete target.id;
                        }

                        $rootScope.progressbar_start();
                        persons.save(target,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed') {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid') {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else if(response.status== 'success'){
                                $modalInstance.close();
                                $rootScope.members_not_inserted.splice(index, 1);
                                $rootScope.load_not_inserted= false;
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                LoadFamily();
                            }
                        }, function(error) {
                            $rootScope.toastrMessages('error',error.data.msg);
                        });

                    };

                    $scope.dateOptions = {formatYear: 'yy', startingDay: 0 };
                    $scope.formats = [ 'dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                    $scope.format = $scope.formats[0];

                    $scope.today = function() { $scope.dt = new Date();};
                    $scope.today();
                    $scope.clear = function() {$scope.dt = null;};

                    $scope.open4 = function($event)
                    {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.popup4.opened = true;
                    };

                    $scope.popup4 = {
                        opened: false
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };
        $scope.popup99 = {opened: false};
        $scope.popup88 = {opened: false};

        // cases.getCaseReports({'case_id':$stateParams.id,
        //     'target':'info',
        //     'action':'filters',
        //     'category':true,
        //     'mode':'show',
        //     'full_name' :true
        // },function(response) {
        //     $rootScope.person_id = response.person_id;
        //     $rootScope.organization_id = response.organization_id;
        //     $rootScope.full_name = response.full_name;
        //     $rootScope.category_name = response.category_name;
        //
        //
        // });


        if(angular.isUndefined($stateParams.category_id) ||$stateParams.category_id == null){
            if($rootScope.mode !== 'new'){
                $state.go('aidCases',{"type": 'all'});
            }else{
                $state.go('categories-caseform',{"mode": $rootScope.mode,"case_id" :$stateParams.id});
            }
        }
        else{
            if((!angular.isUndefined($stateParams.id) || $stateParams.id !== null)) {
                LoadFamily();
            }
            if($stateParams.mode !== "show") {
                $rootScope.priority=[];
                category.getPriority({'category_id':$rootScope.category_id ,'target':'familyStructure','type':'aids'},function (response) {
                    $rootScope.priority=response.priority;
                });

                Entity.get({entity:'entities',c:'educationAuthorities,educationDegrees,educationStages,countries,workJobs,diseases,maritalStatus,kinship'},function (response) {
                    $rootScope.EduAuthorities = response.educationAuthorities;
                    $rootScope.EduDegrees = response.educationDegrees;
                    $rootScope.EduStages = response.educationStages;
                    $rootScope.Country = response.countries;
                    $rootScope.WorkJobs = response.workJobs;
                    $rootScope.Diseases = response.diseases;
                    $rootScope.Kinship = response.kinship;

                    $rootScope.MaritalStatus = response.maritalStatus;
                    $rootScope.marital_status_map=[];
                    angular.forEach($rootScope.MaritalStatus, function(v, k) {
                        $scope.marital_status_map[v.id] = v.name;
                    });
                });
            }
        }

    });
