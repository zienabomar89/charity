
angular.module('AidModule').controller('OtherdataController',function($stateParams,$state,$rootScope,$scope,$http,$timeout,category,cases,Entity,$filter) {

    $state.current.data.pageTitle = $filter('translate')('other-data');
    $rootScope.mode=$stateParams.mode;
    $scope.mode=$stateParams.mode;
    $rootScope.category_id=$stateParams.category_id;
    $scope.msg1={'promised':[],'organization_name':[],'properties_condition':[],'non_financial_aid_condition':[],'financial_aid_condition':[]};
    $scope.priority=[];
    $scope.status=' ';
    $scope.status1=' ';
    $scope.status2=' ';
    $scope.status3=' ';
    $scope.status4=' ';
    $scope.otherdata = {financial_aid_source:[],non_financial_aid_source:[],persons_properties:[]};

    if(angular.isUndefined($stateParams.mode) ||$stateParams.mode == null){
        $scope.mode=$rootScope.mode=$stateParams.mode="new";
    }
    if((!angular.isUndefined($stateParams.id) || $stateParams.id !== null) && !$stateParams.mode){
        $scope.mode=$rootScope.mode=$stateParams.mode="edit";
    }
    if(angular.isUndefined($stateParams.category_id) ||$stateParams.category_id == null) {
        if($rootScope.mode !== 'new'){
            $state.go('aidCases',{"type": 'all'});
        }else{
            $state.go('categories-caseform',{"mode": $rootScope.mode,"case_id" :$stateParams.id});
        }
    }else{
        $rootScope.ResetCaseForm($rootScope.category_id, $rootScope.mode,'otherData');
        $rootScope.CurrentStep=$rootScope.OtherStepNumber;

        if($stateParams.mode !== 'show') {
           Entity.list({entity:'currencies'},function (response) {
                $scope.Currencies = response;
            });

            category.getPriority({'category_id':$rootScope.category_id ,'target':'otherData','type':'aids'},function (response) {
                $scope.priority=response.priority;
            });

        }

        var params={'case_id':$stateParams.id,
                    'target':'info',
                    'action':'filters',
                    'mode':$stateParams.mode,
                    'category_type' : 2,
                    'case_needs': true, 'reconstructions': true,
                    'financial_aid_source' : true, 'non_financial_aid_source' : true, 'persons_properties' : true
                };
        $rootScope.progressbar_start();
        cases.getCaseReports(params,function(response) {
            $rootScope.progressbar_complete();
            $rootScope.id =response.id;
            var temp=response;

            if($rootScope.mode =="new" || $rootScope.mode =="edit" ) {
                    angular.forEach(temp.financial_aid_source, function(v, k) {
                        if(v.aid_take != null){
                            v.aid_take=parseInt(v.aid_take) +"";
                            if(v.currency_id != null){
                                v.currency_id=parseInt(v.currency_id) +"";
                            }else{
                                v.currency_id= "";
                            }

                        }else{
                            v.aid_take= 0 +"";
                            v.currency_id= "";
                            v.aid_value= "";
                        }
                    });
                    angular.forEach(temp.non_financial_aid_source, function(v, k) {
                        if(v.aid_take != null){
                            v.aid_take=parseInt(v.aid_take) +"";
                        }else{
                            v.aid_take= 0 +"";
                            v.aid_value= "";
                        }
                    });
                    angular.forEach(temp.persons_properties, function(v, k) {
                        if(v.has_property != null){
                            v.has_property=parseInt(v.has_property) +"";
                        }else{
                            v.has_property=  0 +"";
                            if(v.has_property == 0){
                                v.quantity= "";
                            }
                        }
                    });
                    if(temp.promised !== null){
                        temp.promised=temp.promised +"";

                        if(temp.promised == 1){
                            temp.reconstructions_organization_name="";
                        }
                    }else{
                        temp.promised= "";
                        temp.reconstructions_organization_name="";
                    }
            }
            $scope.otherdata = temp;

        });


    }


    $scope.ChangePromised = function(value){

        if( !angular.isUndefined($scope.msg1.promised)) {
            $scope.msg1.promised = [];
        }

        if(value == 1){

            if( !angular.isUndefined($scope.msg1.organization_name)) {
                $scope.msg1.organization_name = [];
            }
            $scope.otherdata.organization_name="";
        }
    };
    $scope.save= function(otherdata,step){
        $rootScope.clearToastr();
        otherdata.priority=$scope.priority;
        otherdata.id=$stateParams.id;
        otherdata.type='aids';
        otherdata.mode=$rootScope.mode;

        $rootScope.progressbar_start();
        cases.saveOthersData(otherdata,function (response) {
            $rootScope.progressbar_complete();

            if(response.status=='failed_disabled')
            {
                $rootScope.toastrMessages('error',response.msg);
                $timeout(function() {
                    $state.go('aidCases',{"type": 'all',"mode": null,"status" : null});
                },3000);
            }
            else if(response.status =='failed_valid') {
                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                $scope.status1 =response.status;
                $scope.msg1 =response.msg;
            }else{
                $scope.msg1={};
                if(step =='next'){
                    $rootScope.getNext();
                }else{
                    $rootScope.getPrevious();
                }
            }

            }, function(error) {
                 $rootScope.toastrMessages('error',error.data.msg);
            });




    };

});
