

angular.module('AidModule').controller('PersonVoucherController',function($filter,$stateParams,$rootScope,Entity,$scope,$http,$timeout,$state,cases,cs_persons_vouchers,vouchers_categories,Org) {

    $state.current.data.pageTitle = $filter('translate')('vouchers-data');
    $rootScope.mode=$stateParams.mode;
    $scope.mode=$stateParams.mode;
    $rootScope.category_id=$stateParams.category_id;
    $scope.case_vouchers = true;

    $scope.success =false;
    $scope.error =false;
    $scope.caseVouchersList =[];

    $scope.close=function(){
        $scope.success =false;
        $scope.failed =false;
        $scope.error =false;
        $scope.model_error_status =false;
        $scope.model_error_status2 =false;
    };

    if(angular.isUndefined($stateParams.mode) ||$stateParams.mode == null){
        $scope.mode=$rootScope.mode=$stateParams.mode="new";
    }

    if((!angular.isUndefined($stateParams.id) || $stateParams.id !== null) && !$stateParams.mode){
        $scope.mode=$rootScope.mode=$stateParams.mode="edit";
    }

    $rootScope.ResetCaseForm($rootScope.category_id, $rootScope.mode,'vouchersData');
    $rootScope.CurrentStep=$rootScope.VouchersStepNumber;


    Entity.get({entity:'entities',c:'currencies'},function (response) {
        $rootScope.currency = response.currencies;
    });
    Org.list({type:'sponsors'},function (response) { $rootScope.Sponsors = response; });

    var resetSearchFilters =function(){
        $scope.voucher_filters={
            "organization_id":"",
            "category_id":"",
            "voucher_type":"",
            "title"  :"",
            "voucher_date_from":"",
            "voucher_date_to":"",
            "content":"",
            "notes":"",
            "voucher_source":"",
            "receipt_date_from":"",
            "receipt_date_to":"",
            "receipt_location":""
        };
    };
    LoadVouchers=function(params){
        params.case_id=$stateParams.id;
        params.action='filters';
        params.mode='show';
        params.target='case_vouchers';
        $rootScope.progressbar_start();
        cases.getCaseReports(params,function(response) {
            $rootScope.progressbar_complete();
            $scope.CurrentPage = response.data.current_page;
            $scope.TotalItems = response.data.total;
            $scope.ItemsPerPage = response.data.per_page;
            $scope.caseVouchersList= response.data.data;
            $scope.total= response.total;
        });
    };

    if(angular.isUndefined($stateParams.category_id) ||$stateParams.category_id == null) {
        if($rootScope.mode !== 'new'){
            $state.go('aidCases',{"type": 'all'});
        }else{
            $state.go('categories-caseform',{"mode": $rootScope.mode,"case_id" :$stateParams.id});
        }
    }else{
        resetSearchFilters();
        LoadVouchers({});

        Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

        cs_persons_vouchers.list({id:-1},function (response) {
            $scope.Choices = response.Vouchers;
        });

        vouchers_categories.List(function (response) {
            $scope.Categories = response;
        });
    }


    $scope.toggleSearch = function() {
        $scope.case_vouchers = $scope.case_vouchers == false ? true: false;
        resetSearchFilters();
    };

    $scope.Search=function(data,action) {
        if( !angular.isUndefined(data.receipt_date_to)) {
            data.receipt_date_to=$filter('date')(data.receipt_date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.receipt_date_from)) {
            data.receipt_date_from= $filter('date')(data.receipt_date_from, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(data.voucher_date_to)) {
            data.voucher_date_to=$filter('date')(data.voucher_date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.voucher_date_from)) {
            data.voucher_date_from= $filter('date')(data.voucher_date_from, 'dd-MM-yyyy')
        }

        if(action == 'export'){
            data.case_id=$stateParams.id;
            data.mode='show';
            data.target='case_vouchers';
            data.action=action;
            $http({
                url: "/api/v1.0/common/cases/getCaseReports",
                method: "POST",
                data: data,
            }).then(function (response) {
                var file_type='xlsx';
                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
            }, function(error) {
                $rootScope.toastrMessages('error',error.data.msg);
            });
        }else{
            LoadVouchers(data);
        }
    };

   $scope.open9 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup9.opened = true;
    };

    $scope.popup9 = {
        opened: false
    };
    $scope.open8 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup8.opened = true;
    };

    $scope.popup8 = {
        opened: false
    };

    $scope.open99 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup99.opened = true;
    };

    $scope.popup99 = {
        opened: false
    };
    $scope.open88 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup88.opened = true;
    };

    $scope.popup88 = {
        opened: false
    };
    $scope.open999 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup999.opened = true;
    };

    $scope.popup999 = {
        opened: false
    };
    $scope.open888 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup888.opened = true;
    };

    $scope.popup888 = {
        opened: false
    };

});
