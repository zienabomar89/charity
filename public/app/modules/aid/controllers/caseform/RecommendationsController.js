

angular.module('AidModule').controller('RecommendationsController', function($filter,$stateParams,$rootScope,$scope,$http,$timeout,$state,category,cases) {

    $state.current.data.pageTitle = $filter('translate')('neighborhoods-ratios');
    $rootScope.mode=$stateParams.mode;
    $scope.mode=$stateParams.mode;
    $scope.msg1=[];
    $scope.priority=[];

    if(angular.isUndefined($stateParams.mode) ||$stateParams.mode == null){
        $scope.mode=$rootScope.mode=$stateParams.mode="new";
    }

    if((!angular.isUndefined($stateParams.id) || $stateParams.id !== null) && !$stateParams.mode){
        $scope.mode=$rootScope.mode=$stateParams.mode="edit";
    }

    $rootScope.category_id=$stateParams.category_id;
    $rootScope.ResetCaseForm($rootScope.category_id, $rootScope.mode,'recommendations');
    $rootScope.CurrentStep=$rootScope.RecommendationsStepNumber;

    if(angular.isUndefined($stateParams.category_id) ||$stateParams.category_id == null) {
        if($rootScope.mode !== 'new'){
            $state.go('aidCases',{"type": 'all'});
        }else{
            $state.go('categories-caseform',{"mode": $rootScope.mode,"case_id" :$stateParams.id});
        }
    }else{
        if($stateParams.mode !=="show") {
            category.getPriority({'category_id':$rootScope.category_id ,'target':'recommendations','type':'aids'},function (response) {
                $scope.priority=response.priority;
            });
         }

        $rootScope.progressbar_start();
        cases.get({'id':$stateParams.id, 'action':$stateParams.mode},function (response) {
            $rootScope.progressbar_complete();
            $scope.recommendations = response;
            $scope.recommendations.visitor_evaluation = $scope.recommendations.visitor_evaluation + "";
        });
        // $scope.recommendations = cases.get({'id':$stateParams.id, 'action':$stateParams.mode});
    }

    $scope.endShow= function(){
        $state.go('aidCases',{"mode": $rootScope.mode});
    };

    $scope.save= function(recommendations , step){

        $rootScope.clearToastr();
        if( !angular.isUndefined(recommendations.visited_at)) {
            recommendations.visited_at= $filter('date')(recommendations.visited_at, 'dd-MM-yyyy');
        }

        recommendations.priority=$scope.priority;
        recommendations.type='aids';
        recommendations.mode=$rootScope.mode;

        $rootScope.progressbar_start();
        cases.saveRecommendations(recommendations,function (response) {
            $rootScope.progressbar_complete();
            if(response.status=='failed_disabled')
            {
                $rootScope.toastrMessages('error',response.msg);
                $timeout(function() {
                    $state.go('aidCases',{"type": 'all',"mode": null,"status" : null});
                },3000);
            }
            else
                if(response.status=='failed_valid')
            {
                $scope.status1 =response.status;
                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                $scope.msg1 =response.msg;
            }
            else if(response.status== 'success'){
                if(step =='next'){
                    $state.go('aidCases',{"type": 'all',"mode": $rootScope.mode,"status" : response.status});
                }else{
                    $rootScope.getPrevious();
                }

            }}, function(error) {
                $rootScope.toastrMessages('error',error.data.msg);
            });
    };

    $scope.open2 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup2.opened = true;};
    $scope.popup2 = {opened: false};

});
