

angular.module('AidModule')
.controller('DocumentController',function($scope,$rootScope,$state,$stateParams,$uibModal,cases,OAuthToken,FileUploader,$filter) {

    $state.current.data.pageTitle = $filter('translate')('case-document');
        $rootScope.mode=$stateParams.mode;
        $scope.mode=$stateParams.mode;
        $rootScope.category_id=$stateParams.category_id;

        $rootScope.Documents =[];

        if(angular.isUndefined($stateParams.mode) ||$stateParams.mode == null){
            $scope.mode=$rootScope.mode=$stateParams.mode="new";
        }
        if((!angular.isUndefined($stateParams.id) || $stateParams.id !== null) && !$stateParams.mode){
            $scope.mode=$rootScope.mode=$stateParams.mode="edit";
        }
        if(angular.isUndefined($stateParams.category_id) ||$stateParams.category_id == null){
            if($rootScope.mode !== 'new'){
                $state.go('aidCases',{"type": 'all'});
            }else{
                $state.go('categories-caseform',{"mode": $rootScope.mode,"case_id" :$stateParams.id});
            }
        }
        else{
            $rootScope.ResetCaseForm($rootScope.category_id, $rootScope.mode,'CaseDocumentData');
            $rootScope.CurrentStep=$rootScope.CaseDocumentStepNumber;
            LoadCaseDocuments=function(){

                $rootScope.progressbar_start();
                cases.getCaseReports({'action':'filters','target':'info','case_id':$stateParams.id, 'mode':'edit', 'persons_documents' :true,  'category' :true},function(response) {
                    $rootScope.progressbar_complete();
                    $rootScope.Documents =response.persons_documents;
                        if($rootScope.Documents.length != 0){
                            $rootScope.result='true';
                        }else{
                            $rootScope.result='false';
                        }
                });

            };
            LoadCaseDocuments();
        }

        $scope.BackToAllCases=function(){
            $state.go('sponsorshipCases',{"mode": $rootScope.mode,"status" :'success'});

        };

        $scope.showImage=function(file){
            $rootScope.target=file;

            $uibModal.open({
                templateUrl: 'showImage.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance) {

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                },
                resolve: {

                }
            });
        };

        $scope.ChooseAttachment=function(item,action){

            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'myModalContent2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,cases,OAuthToken, FileUploader) {

                    $scope.fileupload = function () {
                        if($scope.uploader.queue.length){
                            var image = $scope.uploader.queue.slice(1);
                            $scope.uploader.queue = image;
                        }
                    };
                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/doc/files',
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });
                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.clearToastr();
                    };
                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);

                        if(response.status=='error')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else{

                            fileItem.id = response.id;
                            cases.setAttachment({id:item.person_id,type:'aids',action:action,document_type_id:item.document_type_id,document_id:fileItem.id},function (response2) {
                                if(response2.status=='error' || response2.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response2.msg);
                                }
                                else if(response2.status== 'success'){

                                    $modalInstance.close();
                                    $rootScope.toastrMessages('success',response2.msg);
                                    LoadCaseDocuments();
                                }

                            }, function(error) {
                                $rootScope.toastrMessages('error',error.data.msg);
                            });
                        }
                    };


                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

    });
