
angular.module('AidModule').controller('CategoriesCaseFormController', function( $state,$stateParams,$rootScope,$scope, $http, $timeout,category,$filter) {

    $state.current.data.pageTitle = $filter('translate')('Organizations');
    $rootScope.Formcat=[];
    $rootScope.mode=$scope.mode=$stateParams.mode;
    $rootScope.id=$stateParams.id ;
    $rootScope.category_id=$state.params.category_id;
    $rootScope.category_id=$stateParams.category_id ;

    if(angular.isUndefined($stateParams.mode)) {
        $rootScope.mode=$scope.mode='new';
    }else{
        $rootScope.mode=$scope.mode=$stateParams.mode;
    }


    var getAidState= function(target) {

        var CurState='';

        if(target == 'personal-info'){
            $rootScope.PersonalStepNumber=$rootScope.CurrentStep;
            CurState='personal-info'
        }else if(target == 'residenceData'){
            $rootScope.ResidenceStepNumber=$rootScope.CurrentStep;
            CurState='residence-data'
        }else if(target == 'homeIndoorData'){
            $rootScope.HomeIndoorStepNumber=$rootScope.CurrentStep;
            CurState='home-indoor-data'
        }else if(target == 'otherData'){
            $rootScope.OtherStepNumber=$rootScope.CurrentStep;
            CurState='other-data'
        }else if(target == 'vouchersData'){
            $rootScope.VouchersStepNumber=$rootScope.CurrentStep;
            CurState='vouchers-data'
        }else if(target == 'familyStructure'){
            $rootScope.FamilyStructureStepNumber=$rootScope.CurrentStep;
            CurState='family-structure'
        }else if(target == 'customFormData'){
            $rootScope.CustomFormStepNumber=$rootScope.CurrentStep;
            CurState='custom-form'
        }else if(target == 'recommendations'){
            $rootScope.RecommendationsStepNumber=$rootScope.CurrentStep;
            CurState='recommendations'
        }
        $state.go('caseform.'+CurState,{"mode":$rootScope.mode,"id" :$rootScope.id,"category_id" :$rootScope.category_id});
    };


    $scope.GotoForm= function(Formcat){
        $rootScope.clearToastr();
        $rootScope.mode='new';

           var FirstStep='';

      if($rootScope.mode == 'new' || $rootScope.mode == 'edit'){
                if( !angular.isUndefined($rootScope.aidCategories)) {
                    angular.forEach($rootScope.aidCategories, function(v, k) {
                        if(v.id == Formcat.category_id){
                            $rootScope.category_id=v.id;
                            $rootScope.CurrentStep=k+1;
                            getAidState(v.FirstStep)
                        }
                    });
                }else{
                    category.getCategoryFormsSections({'id':Formcat.category_id ,'mode':$rootScope.mode,'type':'aids'},function (response){
                                $rootScope.TotalOfStep=response.FormSection.length;
                            angular.forEach(response.FormSection, function(v, k) {
                                if(v.id == Formcat.category_id){
                                    $rootScope.category_id=v.id;
                                    $rootScope.CurrentStep=k+1;
                                    getAidState(v.FirstStep)
                              }
                           });
                        }
                    );
                }
    }else{
          category.getCategoryFormsSections({'id':Formcat.category_id ,'mode':$rootScope.mode,'type':'aids'},function (response){
                  $rootScope.TotalOfStep=response.FormSection.length;
                  angular.forEach(response.FormSection, function(v, k) {
                      if(v.id == Formcat.category_id){
                          $rootScope.CurrentStep=k+1;
                          getAidState(v.FirstStep)
                      }
                  });
              });
      }
    };


});
