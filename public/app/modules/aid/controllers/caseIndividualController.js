
angular.module('AidModule').controller('caseIndividualController', function($filter,$stateParams,$rootScope, $scope,$uibModal, $http, $timeout,$state,setting,
                                                                      cs_persons_vouchers,org_sms_service,Org,Entity,custom_forms,vouchers_categories,cases) {

    var AuthUser = localStorage.getItem("charity_LoggedInUser");
    $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
    $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
    if ($rootScope.charity_LoggedInUser.organization != null) {
        $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
    }
    $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;
    $state.current.data.pageTitle = $filter('translate')('case individual');
    $scope.DataFilter = {page : 1};
    $scope.aidsCasesfilters = {};
    $scope.CurrentPage = 1 ;
    $scope.itemsCount = 50;

    $scope.items = [];
    $scope.toggleStatus = true;
    $scope.indivCollapsed  =
    $scope.infoCollapsed  = $scope.banksCollapsed     =
    $scope.worksCollapsed = $scope.contactsCollapsed  =
    $scope.residenceCollapsed = $scope.homeIndoorCollapsed =
    $scope.othersCollapsed= $scope.aidSourceCollapsed=
    $scope.propertiesCollapsed=$scope.vouchersCollapsed=true;
    $rootScope.aidsCasesfilters ={};
    $scope.PropertiesFilter=[];
    $scope.pro_id=[];
    $scope.AidSourceFilter=[];
    $scope.aid_id=[];


    $rootScope.aidCategories_=[];
    angular.forEach($rootScope.aidCategories, function(v, k) {
        if ($rootScope.lang == 'ar'){
            $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
        }
        else{
            $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
        }
    });
    $scope.action = 'filter';
     var params = {};

    cs_persons_vouchers.list({id:2},function (response) {
         $rootScope.Vouchers = response.Vouchers;
         $rootScope.Owns = response.Owns;
    });
    $scope.get = function(target,value,parant){
        $rootScope.clearToastr();

        if (!angular.isUndefined(value)) {
            if(value != null && value != "" && value != " " ) {
                Entity.listChildren({entity:target,'id':value,'parent':parant},function (response) {

                    if (target == 'sdistricts') {
                        $scope.governarate = response;
                        $scope.regions = [];
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];

                        $scope.DataFilter.adsdistrict_id = "";
                        $scope.DataFilter.adsregion_id = "";
                        $scope.DataFilter.adsneighborhood_id = "";
                        $scope.DataFilter.adssquare_id = "";
                        $scope.DataFilter.adsmosques_id = "";

                    }
                    else if (target == 'sregions') {
                        $scope.regions = response;
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];
                        $scope.DataFilter.adsregion_id = "";
                        $scope.DataFilter.adsneighborhood_id = "";
                        $scope.DataFilter.adssquare_id = "";
                        $scope.DataFilter.adsmosques_id = "";
                    }
                    else if (target == 'sneighborhoods') {
                        $scope.nearlocation = response;
                        $scope.squares = [];
                        $scope.mosques = [];
                        $scope.DataFilter.adsneighborhood_id = "";
                        $scope.DataFilter.adssquare_id = "";
                        $scope.DataFilter.adsmosques_id = "";

                    }
                    else if (target == 'ssquares') {
                        $scope.squares = response;
                        $scope.mosques = [];
                        $scope.DataFilter.adssquare_id = "";
                        $scope.DataFilter.adsmosques_id = "";
                    }
                    else if (target == 'smosques') {
                        $scope.mosques = response;
                        $scope.DataFilter.adsmosques_id = "";
                    }else if(target =='branches'){
                        $scope.Branches = response;
                        $scope.DataFilter.branch_name="";
                    }
                });
            }
        }
    };
    $scope.toggleSearchForm = function() {
            $scope.toggleStatus = $scope.toggleStatus == false ? true: false;
    };
    $scope.setPropertiesFilter=function(id,value){
        var index = id_index = -1;
        if ($scope.pro_id.indexOf(id) == -1) {
            if (value != "") {
                $scope.PropertiesFilter.push({'id':id,'exist':value});
                $scope.pro_id.push(id);
            }
        }else{
            id_index=$scope.pro_id.indexOf(id);
            for (var i = 0, len = $scope.PropertiesFilter.length; i <= len; i++) {
                if ($scope.PropertiesFilter[i].id == id) {
                    index= i;
                    break;
                }
            }
        }
        if(index != -1){
            if (value == "") {
                $scope.PropertiesFilter.splice(index, 1);
                $scope.pro_id.splice(id_index, 1);
            } else {
                $scope.PropertiesFilter[index].exist = value;
            }
        }
    };
    $scope.setAidsourceFilter=function(id,value){
        var aindex = aid_index = -1;
        if ($scope.aid_id.indexOf(id) == -1) {
            if (value != "") {
                $scope.AidSourceFilter.push({'id':id,'aid_take':value});
                $scope.aid_id.push(id);
            }
        }else{
            aid_index=$scope.aid_id.indexOf(id);
            for (var i = 0, len = $scope.AidSourceFilter.length; i <= len; i++) {
                if ($scope.AidSourceFilter[i].id == id) {
                    aindex= i;
                    break;
                }
            }
        }
        if(aindex != -1){
            if (value == "") {
                $scope.AidSourceFilter.splice(aindex, 1);
                $scope.aid_id.splice(aid_index, 1);
            } else {
                $scope.AidSourceFilter[aindex].aid_take = value;
            }
        }
    };
    $scope.setEssentialsFilter=function(z,target,id,value){
        $rootScope.aidsCasesfilters.Essential[z].id=id;
    };
    $scope.allCheckbox=function(value){
        if (value) {
            $scope.selectedAll = true;
        } else {
            $scope.selectedAll = false;
        }

        angular.forEach($scope.items, function(v, k) {
            v.check=$scope.selectedAll;
        });
    };

    $scope.resetFilters =function(){


        $rootScope.aidsCasesfilters={
            "Properties":[],
            "all_organization":false,
            "order_by_rank":"",
            "status":"",
            "date_from":"",
            "date_to":"",
            "first_name":"",
            "second_name":"",
            "third_name":"",
            "last_name":"",
            "en_first_name":"",
            "en_second_name":"",
            "en_third_name":"",
            "en_last_name":"",
            "category_id":"",
            "id_card_number":"",
            "gender":"",
            "marital_status_id":"",
            "birthday_from":"",
            "birthday_to":"",
            "birth_place":"",
            "nationality":"",
            "refugee":"",
            "unrwa_card_number":"",
            "adscountry_id":"",
            "adsdistrict_id":"",
            "adsregion_id":"",
            "adsneighborhood_id":"",
            "adssquare_id":"",
            "adsmosques_id":"",
            "person_country":"",
            "person_governarate":"",
            "person_city":"",
            "location_id":"",
            "mosques_id":"",
            "street_address":"",
            "monthly_income_from":"",
            "monthly_income_to":"",
            "primary_mobile":"",
            "secondery_mobile":"",
            "phone":"",
            "bank_id":"",
            "branch_name":"",
            "account_number":"",
            "account_owner":"",
            "property_type_id":"",
            "roof_material_id":"",
            "residence_condition":"",
            "indoor_condition":"",
            "min_rooms":"",
            "max_rooms":"",
            "min_area":"",
            "max_area":"",
            "habitable":"",
            "house_condition":"",
            "rent_value":"",
            "working":"",
            "can_work":"",
            "work_reason_id":"",
            "work_job_id":"",
            "work_status_id":"",
            "work_wage_id":"",
            "work_location":"",
            "promised":"",
            "visitor":"",
            "visited_at_from":"",
            "visited_at_to":"",
            "min_family_count":"",
            "max_family_count":"",
            "min_total_vouchers":"",
            "max_total_vouchers":"",
            'organization_id':"",
            'got_voucher':"",
            'voucher_organization':"",
            'voucher_sponsors':"",
            'voucher_to':"",
            'voucher_from':"",
            "voucher_ids":"",
            "individual_first_name":"",
            "individual_second_name":"",
            "individual_third_name":"",
            "individual_last_name":"",
            "individual_id_card_number":"",
            "individual_gender":"",
            "individual_marital_status_id":"",
            "individual_birthday_from":"",
            "individual_birthday_to":"",
            "individual_birth_place":"",
            "individual_nationality":""
        };

        $scope.PropertiesFilter=[];
        $scope.pro_id=[];
        $scope.AidSourceFilter=[];
        $scope.aid_id=[];
    };


    $scope.resetFilters();

    Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

    Entity.get({entity:'entities',c:'transferCompany,organizationsCat,propertyTypes,roofMaterials,buildingStatus,furnitureStatus,houseStatus,habitableStatus,currencies,aidCountries,countries,maritalStatus,workReasons,workStatus,workWages,workJobs,banks,aidSources,properties,essentials'},function (response) {
        $rootScope.organizationsCat = response.organizationsCat;
        $rootScope.transferCompany = response.transferCompany;
        $rootScope.currency = response.currencies;
        $scope.PropertyTypes = response.propertyTypes;
        $scope.RoofMaterials = response.roofMaterials;
        $scope.buildingStatus = response.buildingStatus;
        $scope.furnitureStatus = response.furnitureStatus;
        $scope.houseStatus = response.houseStatus;
        $scope.habitableStatus = response.habitableStatus;
        $scope.Country = response.countries;
        $scope.aidCountries = response.aidCountries;
        $scope.MaritalStatus = response.maritalStatus;
        $scope.WorkReasons = response.workReasons;
        $scope.WorkStatus = response.workStatus;
        $scope.WorkWages = response.workWages;
        $scope.WorkJobs = response.workJobs;
        $scope.Banks = response.banks;
        $scope.Properties = response.properties;
        $scope.Aidsource = response.aidSources;
        $scope.Essentials = response.essentials;
        $rootScope.aidsCasesfilters.Essential = response.essentials;
    });

    var LoadIndividual =function(params){

        params.category_type=2;
        params.deleted=$rootScope.deleted;

        if(angular.isUndefined(params.action)){
            params.action ='filter';
        }
        if(params.action && angular.isUndefined(params.page) ){
            params.page =1;
        }
        if( !angular.isUndefined(params.all_organization)) {
            params.organization_category=[];
            if(params.all_organization == true ||params.all_organization == 'true'){
                params.organization_category=[];
                angular.forEach(params.organization_category_id, function(v, k) {
                    params.organization_category.push(v.id);
                });

                var orgs=[];
                angular.forEach(params.organization_id, function(v, k) {
                    orgs.push(v.id);
                });

                params.organization_ids=orgs;

            }else if(params.all_organization == false ||params.all_organization == 'false') {
                params.organization_ids=[];
                $scope.master=false
            }
        }

        if( !angular.isUndefined(params.date_from)) {
            params.date_from=$filter('date')(params.date_from, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.date_to)) {
            params.date_to=$filter('date')(params.date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.individual_birthday_to)) {
            params.birthday_to=$filter('date')(params.individual_birthday_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.individual_birthday_from)) {
            params.individual_birthday_from= $filter('date')(params.individual_birthday_from, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.individual_birthday_to)) {
            params.individual_birthday_to=$filter('date')(params.individual_birthday_to, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(params.birthday_from)) {
            params.birthday_from= $filter('date')(params.birthday_from, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.visited_at_to)) {
            params.visited_at_to=$filter('date')(params.visited_at_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.visited_at_from)) {
            params.visited_at_from= $filter('date')(params.visited_at_from, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.voucher_to)) {
            params.voucher_to=$filter('date')(params.voucher_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.visited_at_from)) {
            params.voucher_from= $filter('date')(params.voucher_from, 'dd-MM-yyyy')
        }

        params.page = $scope.CurrentPage;

        $rootScope.progressbar_start();
        cases.individual(params,function (response) {
            $rootScope.progressbar_complete();
            if(params.action == 'ExportToExcel'){
                if(response.download_token) {
                    window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token;
                }
            }else{
                $scope.items = response.data ;
                $scope.CurrentPage = response.current_page ;
                $scope.DataFilter.page=$scope.CurrentPage;
                $scope.ItemsPerPage = response.per_page ;
                $scope.TotalItems = response.total;
            }
        });

    };

    $scope.Search=function(selected,data,action) {
        $rootScope.clearToastr();
        data.Properties=$scope.PropertiesFilter;
        data.AidSources =$scope.AidSourceFilter;
        data.action=action;
        
         if(selected == true || selected == 'true' ){
           var persons=[];
            angular.forEach($rootScope.items, function(v, k){
                if(v.check){
                    persons.push(v.r_person_id);
                }

            });
            
            if(persons.length==0){
                $rootScope.toastrMessages('error',$filter('translate')('You have not select people to export'));
                return ;
            }
            data.persons=persons;
        }
          
        if(action != 'ExportToExcel'){
            data.page=$scope.CurrentPage;
        }
        LoadIndividual(data);

    };

    $scope.insertNewVouchers=function(type){

        if(type == 'xlx'){
            $uibModal.open({
                templateUrl: 'ImportToVouchers.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;
                    $scope.row={category_id:"",status:""};
                    $scope.person_vouchers_import = {};
                    $scope.person_vouchers_import.withRatio=false;
                    $scope.person_vouchers_import.add_cases=false;

                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };

                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;;
                    };
                    $scope.download=function(){
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-individual-voucher-template&template=true';
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/aids/cs_persons_vouchers/individualVoucher',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    $scope.confirm = function (items) {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                        $scope.spinner=false;
                        $scope.import=false;
                        Uploader.onBeforeUploadItem = function(i) {
                            $rootScope.clearToastr();
                            var ratio = 0;
                            if(items.with_ratio == 'true' || items.with_ratio == 'true' ){
                                ratio = 1;
                            }

                            i.formData = [{voucher_id: items.voucher_id, ratio:ratio,
                                add_cases:items.add_cases,
                                withRatio:items.withRatio,
                                receipt_location:items.receipt_location,
                                receipt_date:$filter('date')(items.receipt_date, 'yyyy/MM/dd')}];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {

                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);

                            if(response.download_token){
                                window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token;
                            }

                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);

                                $scope.upload=false;
                            }
                            else if(response.status=='success')
                            {
                                $scope.spinner=false;
                                $scope.upload=false;
                                $scope.import=false;
                                $rootScope.toastrMessages('success',response.msg);
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $modalInstance.close();
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.dateOptions = {
                        formatYear: 'yy',
                        startingDay: 0
                    };
                    $scope.formats = [ 'dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                    $scope.format = $scope.formats[0];
                    $scope.today = function() {
                        $scope.dt = new Date();
                    };
                    $scope.today();
                    $scope.clear = function() {
                        $scope.dt = null;
                    };
                    $scope.open4 = function($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.popup4.opened = true;
                    };
                    $scope.popup4 = {
                        opened: false
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});
        }else{
            $rootScope.clearToastr();
            var persons=[];
            angular.forEach($scope.items, function(v, k) {
            // && v.organization_id == $rootScope.UserOrgId
                if(v.check && (v.status == 0 || v.status == '0')){
                    persons.push({'id':v.l_person_id,'individual_id':v.r_person_id,'l_full_name':v.full_name,'r_full_name':v.individual_name,
                                  'case_id':v.case_id,'category_id':v.category_id,'id_card_number': v.id_card_number,'individual_id_card_number':v.individual_id_card_number});
                }
            });
            if(persons.length==0){
                $rootScope.toastrMessages('error',$filter('translate')('You have not select any invalid person on your locations or on your organization for an active case to get voucher'));
            }
            else{
                $uibModal.open({
                    templateUrl: 'insertNewVouchers.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'md',
                    controller: function ($rootScope,$scope, $modalInstance, $log,cs_persons_vouchers) {
                        $scope.person_vouchers={};
                        $scope.person_vouchers.withRatio=false;


                        $scope.confirmInsert = function (persons_vouchers) {
                            $rootScope.progressbar_start();
                            $rootScope.clearToastr();

                            var case_category_id = -1;

                            angular.forEach($rootScope.Vouchers, function(v, k) {
                                if(v.id == persons_vouchers.voucher_id){
                                    case_category_id = v.case_category_id
                                }
                            });

                            var persons_vouchers1={};


                            var final_persons =[];
                            if(case_category_id != -1){

                                angular.forEach(persons, function(v, k) {
                                    if(v.category_id == case_category_id){
                                        final_persons.push(v);
                                    }
                                });
                                if(final_persons.length==0){
                                    $rootScope.toastrMessages('error',$filter('translate')('You have not select any invalid case on your locations or on your organization for an active case to get voucher , or case not has the same category of voucher'));
                                    return ;
                                }

                                if(final_persons.length != persons.length){
                                    $rootScope.toastrMessages('warning',$filter('translate')('There are case not has the same category of voucher'));
                                }

                            }else{
                                final_persons =persons;
                            }

                            persons_vouchers1.persons=final_persons;
                            persons_vouchers1.receipt_date=$filter('date')(persons_vouchers.receipt_date, 'yyyy/MM/dd');
                            persons_vouchers1.receipt_location=persons_vouchers.receipt_location;
                            persons_vouchers1.voucher_id=persons_vouchers.voucher_id;
                            persons_vouchers1.source='ids';

                            cs_persons_vouchers.individualVoucher(persons_vouchers1,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.download_token){
                                        var file_type='xlsx';
                                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;

                                    }

                                    if(response.status=='failed')
                                    {
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    else if(response.status=='failed_valid')
                                    {
                                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                        $scope.status1 =response.status;
                                        $scope.msg1 =response.msg;
                                    }
                                    else{
                                        $modalInstance.close();
                                        if(response.status=='success')
                                        {
                                            $rootScope.toastrMessages('success',response.msg);
                                        }else{
                                            $rootScope.toastrMessages('error',response.msg);
                                        }
                                    }
                                }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                });
                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };


                        $scope.dateOptions = {
                            formatYear: 'yy',
                            startingDay: 0
                        };
                        $scope.formats = [ 'dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                        $scope.format = $scope.formats[0];
                        $scope.today = function() {
                            $scope.dt = new Date();
                        };
                        $scope.today();
                        $scope.clear = function() {
                            $scope.dt = null;
                        };
                        $scope.open4 = function($event) {
                            $event.preventDefault();
                            $event.stopPropagation();
                            $scope.popup4.opened = true;
                        };
                        $scope.popup4 = {
                            opened: false
                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    },
                    resolve: {
                    }
                });
            }
        }
    };

    $scope.pageChanged = function (currentPage) {
        $rootScope.clearToastr();
        $rootScope.aidsCasesfilters.page=currentPage;
        LoadIndividual($rootScope.aidsCasesfilters);
    };

    $rootScope.aidsCasesfilters.itemsCount='50';

    $scope.itemsPerPage_ = function (itemsCount) {
        $rootScope.clearToastr();
        $rootScope.aidsCasesfilters.itemsCount=itemsCount;
        $rootScope.aidsCasesfilters.page=1;
        LoadIndividual($rootScope.aidsCasesfilters);
    };

    $scope.disblay_orgs =false;
    $scope.resetOrg=function(all_organization,organization_category){
        $rootScope.clearToastr();
        $scope.disblay_orgs =false;
        let ids = [];
        if( !angular.isUndefined(all_organization)) {
            if(all_organization == true ||all_organization == 'true'){
                angular.forEach(organization_category, function(v, k) {
                    ids.push(v.id);
                });

                Org.ListOfCategory({ids:ids},function (response) {
                    if (response.list.length > 0){
                        $scope.Org_ = response.list;
                        $scope.disblay_orgs =true;
                    }else{
                        $scope.Org_ = [];
                        $scope.disblay_orgs =false;
                        $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');
                    }
                });
            }else{
                $scope.Org_ = [];
                $scope.disblay_orgs =false;
                $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');
            }
        }else{
            $scope.Org_ = [];
            $scope.disblay_orgs =false;
            $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');

        }


    };

    $rootScope.labels = {
        "itemsSelected": $filter('translate')('itemsSelected'),
        "selectAll": $filter('translate')('selectAll'),
        "unselectAll": $filter('translate')('unselectAll'),
        "search": $filter('translate')('search'),
        "select": $filter('translate')('select')
    }
    $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0};
    $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $rootScope.format = $scope.formats[0];
    $rootScope.today = function() {$rootScope.dt = new Date();};
    $rootScope.today();
    $rootScope.clear = function() {$rootScope.dt = null;};

    $scope.open1 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        opened: false
    };
    $scope.open2 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        opened: false
    };
    $scope.open8 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup8.opened = true;
    };
    $scope.popup8 = {
        opened: false
    };
    $scope.open3 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup3.opened = true;
    };
    $scope.popup3 = {
        opened: false
    };
    $scope.open4 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup4.opened = true;
    };
    $scope.popup4 = {
        opened: false
    };
    $scope.open23 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup23.opened = true;
    };
    $scope.popup23 = {
        opened: false
    };
    $scope.open24 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup24.opened = true;
    };
    $scope.popup24 = {
        opened: false
    };
    $scope.open224 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup224.opened = true;
    };
    $scope.popup224 = {
        opened: false
    };
    $scope.open15 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup15.opened = true;
    };
    $scope.popup15 = {
        opened: false
    };
    $scope.open25 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup25.opened = true;
    };
    $scope.popup25 = {
        opened: false
    };

    $scope.open99 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup99.opened = true;
    };
    $scope.popup99 = {
        opened: false
    };
    $scope.open88 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup88.opened = true;
    };
    $scope.popup88 = {
        opened: false
    };

    $scope.open111 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup111.opened = true;
    };
    $scope.popup111 = {
        opened: false
    };

    $scope.open222 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup222.opened = true;
    };
    $scope.popup222 = {
        opened: false
    };

    $scope.open111000 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup111000.opened = true;
    };
    $scope.popup111000 = {
        opened: false
    };

    $scope.open222000 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup222000.opened = true;
    };
    $scope.popup222000 = {
        opened: false
    };

    $scope.open1 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        opened: false
    };
    $scope.open2 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        opened: false
    };
    $scope.open8 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup8.opened = true;
    };
    $scope.popup8 = {
        opened: false
    };
    $scope.open3 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup3.opened = true;
    };
    $scope.popup3 = {
        opened: false
    };
    $scope.open4 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup4.opened = true;
    };
    $scope.popup4 = {
        opened: false
    };
    $scope.open23 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup23.opened = true;
    };
    $scope.popup23 = {
        opened: false
    };
    $scope.open24 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup24.opened = true;
    };
    $scope.popup24 = {
        opened: false
    };
    $scope.open224 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup224.opened = true;
    };
    $scope.popup224 = {
        opened: false
    };
    $scope.open15 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup15.opened = true;
    };
    $scope.popup15 = {
        opened: false
    };
    $scope.open25 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup25.opened = true;
    };
    $scope.popup25 = {
        opened: false
    };

    $scope.open99 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup99.opened = true;
    };
    $scope.popup99 = {
        opened: false
    };
    $scope.open88 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup88.opened = true;
    };
    $scope.popup88 = {
        opened: false
    };



});
