
angular.module('AidModule').controller('aidCasesController', function($filter,$stateParams,$rootScope, $scope,$uibModal, $http, $timeout,$state,setting,
                                                                      cs_persons_vouchers,org_sms_service,Org,Entity,custom_forms,vouchers_categories,cases,category) {

    var AuthUser =localStorage.getItem("charity_LoggedInUser");
    $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
    $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
    if($rootScope.charity_LoggedInUser.organization != null) {
        $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
    }
    $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;

    $rootScope.labels = {
        "itemsSelected": $filter('translate')('itemsSelected'),
        "selectAll": $filter('translate')('selectAll'),
        "unselectAll": $filter('translate')('unselectAll'),
        "search": $filter('translate')('search'),
        "select": $filter('translate')('select')
    }

    $rootScope.type=$scope.type=$stateParams.type == 'deleted'? 'deleted' : 'all';
    $rootScope.settings.layout.pageSidebarClosed = true;

    $state.current.data.pageTitle = $stateParams.type == 'deleted'? $filter('translate')('deleted aid cases') :
        $filter('translate')('all aid cases');

    $scope.infoCollapsed  = $scope.banksCollapsed     =
        $scope.healthCollapsed = $scope.worksCollapsed = $scope.contactsCollapsed  =
            $scope.residenceCollapsed = $scope.homeIndoorCollapsed =
                $scope.othersCollapsed= $scope.aidSourceCollapsed=
                    $scope.propertiesCollapsed=$scope.vouchersCollapsed=true;
    $rootScope.items=[];

    if($rootScope.type == 'deleted'){
        $rootScope.deleted=$scope.deleted=true;
    }else{
        $rootScope.deleted=$scope.deleted=false;
    }

    $scope.master=false;

    $scope.PropertiesFilter=[];
    $scope.pro_id=[];
    $scope.AidSourceFilter=[];
    $scope.aid_id=[];
    $rootScope.person_restrict=false;
    $rootScope.card_restrict=false;
    $rootScope.restricted=[];
    $rootScope.invalid_cards=[];

    $rootScope.status ='';
    $rootScope.msg ='';
    $scope.toggleStatus = true;
    $rootScope.mode=$stateParams.mode;


    var resetFilter =function(){
        if($rootScope.type == 'deleted'){
            $rootScope.aidsCasesfilters={
                "all_organization":false,
                "first_name":"",
                "second_name":"",
                "third_name":"",
                "last_name":"",
                "category_id":"",
                "id_card_number":"",
                "deleted_from":"",
                "deleted_to":"",
                "organization_id":""
            };
        }
        else{
            $rootScope.aidsCasesfilters={
                "Properties":[],
                "all_organization":false,
                "order_by_rank":"",
                "status":"0",
                "date_from":"",
                "date_to":"",
                "first_name":"",
                "second_name":"",
                "third_name":"",
                "last_name":"",
                "en_first_name":"",
                "en_second_name":"",
                "en_third_name":"",
                "en_last_name":"",
                "category_id":"",
                "id_card_number":"",
                "gender":"",
                "marital_status_id":"",
                "birthday_from":"",
                "birthday_to":"",
                "birth_place":"",
                "nationality":"",
                "refugee":"",
                "unrwa_card_number":"",
                "adscountry_id":"",
                "adsdistrict_id":"",
                "adsregion_id":"",
                "adsneighborhood_id":"",
                "adssquare_id":"",
                "adsmosques_id":"",
                "person_country":"",
                "person_governarate":"",
                "person_city":"",
                "location_id":"",
                "mosques_id":"",
                "street_address":"",
                "monthly_income_from":"",
                "monthly_income_to":"",
                "primary_mobile":"",
                "secondery_mobile":"",
                "phone":"",
                "bank_id":"",
                "branch_name":"",
                "account_number":"",
                "account_owner":"",
                "property_type_id":"",
                "roof_material_id":"",
                "residence_condition":"",
                "indoor_condition":"",
                "min_rooms":"",
                "max_rooms":"",
                "min_area":"",
                "max_area":"",
                "habitable":"",
                "house_condition":"",
                "rent_value":"",
                "need_repair":"",
                "repair_notes":"",
                "working":"",
                "can_work":"",
                "work_reason_id":"",
                "work_job_id":"",
                "work_status_id":"",
                "work_wage_id":"",
                "work_location":"",
                "promised":"",
                "visited_at_from":"",
                "visited_at_to":"",
                'notes':"",
                'visitor':"",
                'visited_at':"",
                'visitor_card':"",
                'visitor_opinion':"",
                'visitor_evaluation':"",
                "min_family_count":"",
                "max_family_count":"",
                "min_total_vouchers":"",
                "max_total_vouchers":"",
                'organization_id':"",
                'got_voucher':"",
                'voucher_organization':"",
                'voucher_sponsors':"",
                'voucher_to':"",
                'voucher_from':"",
                "voucher_ids":"",
                'has_health_insurance':"",
                'health_insurance_number':"",
                'health_insurance_type':"",
                'has_device':"",
                'used_device_name':"",
                'gov_health_details':"",
            };

            $scope.PropertiesFilter=[];
            $scope.pro_id=[];
            $scope.AidSourceFilter=[];
            $scope.aid_id=[];

            if($stateParams.id){
                $rootScope.aidsCasesfilters.sponsor_id=$stateParams.id;
                $scope.sponsor=true;
            }else{
                $scope.sponsor=false;
            }
        }


    };

    var LoadCases =function(params){
        // $rootScope.clearToastr();
        params.category_type=2;
        params.deleted=$rootScope.deleted;

        if(angular.isUndefined(params.action)){
            params.action ='filter';
        }
        if(params.action && angular.isUndefined(params.page) ){
            params.page =1;
        }
        if( !angular.isUndefined(params.all_organization)) {
            params.organization_category=[];
            if(params.all_organization == true ||params.all_organization == 'true'){
                params.organization_category=[];
                angular.forEach(params.organization_category_id, function(v, k) {
                    params.organization_category.push(v.id);
                });

                var orgs=[];
                angular.forEach(params.organization_id, function(v, k) {
                    orgs.push(v.id);
                });

                params.organization_ids=orgs;

            }else if(params.all_organization == false ||params.all_organization == 'false') {
                params.organization_ids=[];
                $scope.master=false
            }
        }

        if($rootScope.type != 'deleted') {
            if( !angular.isUndefined(params.date_from)) {
                params.date_from=$filter('date')(params.date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.date_to)) {
                params.date_to=$filter('date')(params.date_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.birthday_to)) {
                params.birthday_to=$filter('date')(params.birthday_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.birthday_from)) {
                params.birthday_from= $filter('date')(params.birthday_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.visited_at_to)) {
                params.visited_at_to=$filter('date')(params.visited_at_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.visited_at_from)) {
                params.visited_at_from= $filter('date')(params.visited_at_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.voucher_to)) {
                params.voucher_to=$filter('date')(params.voucher_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.visited_at_from)) {
                params.voucher_from= $filter('date')(params.voucher_from, 'dd-MM-yyyy')
            }
        }
        $rootScope.progressbar_start();
        $http({
            url: "/api/v1.0/common/aids/cases/filterCases",
            method: "POST",
            data: params
        })
            .then(function (response) {
                $rootScope.progressbar_complete();
                if(params.all_organization == true ||params.all_organization == 'true'){
                    $scope.master=true
                }
                if(params.action != 'filter'){

                    if(response.data.status == 'false'){
                        $rootScope.toastrMessages('error',$filter('translate')('there is no cases to export'));
                    }else if((params.action == 'FamilyMember' || params.action == 'parents' ||params.action == 'childs' || params.action == 'wives' )&& response.data.status == false){
                        $rootScope.toastrMessages('error',$filter('translate')('there is no family member to export'));
                    }else{
                        var file_type='zip';
                        if(params.action == 'ExportToExcel'  || params.action == 'Basic'|| params.action == 'parents'||params.action == 'FamilyMember'||params.action == 'wives'|| params.action == 'childs') {
                            file_type='xlsx';
                        }
                         window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                    }
                }
                else{
                    if(response.data.Cases.total != 0){
                        $rootScope.UserOrgId=response.data.organization_id;
                        $scope.organization_id=response.data.organization_id;

                        var temp= response.data.Cases.data;
                        if($rootScope.type != 'deleted') {
                            angular.forEach(temp, function(v, k) {
                                if($rootScope.type=='all'){
                                    v.rank=parseInt(v.rank);
                                }
                                v.organization_id=parseInt(v.organization_id);
                            });
                        }

                        $rootScope.items= temp;
                        $scope.CurrentPage = response.data.Cases.current_page;
                        $scope.TotalItems = response.data.Cases.total;
                        $scope.ItemsPerPage = response.data.Cases.per_page;
                    }else{
                        $rootScope.items=[];
                    }
                }

            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });

    };

    $rootScope.aidCategories_=[];
    category.getSectionForEachCategory({'type':'aids'},function (response) {
        if(!angular.isUndefined(response)){
            $rootScope.aidCategories_firstStep_map =[];
            $rootScope.aidCategories = response.Categories;
            if($rootScope.aidCategories.length > 0){
                for (var i in $rootScope.aidCategories) {
                    var item = $rootScope.aidCategories[i];
                    $rootScope.aidCategories_firstStep_map[item.id] = item.FirstStep;
                }
                 var aidCategories_=[];
                angular.forEach($rootScope.aidCategories, function(v, k) {
                    if ($rootScope.lang == 'ar'){
                        aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
                    }
                    else{
                        aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
                    }
                });

                $rootScope.aidCategories_ = aidCategories_ ;
            }

        }
    });
    resetFilter();

    if($rootScope.type == 'deleted'){
        LoadCases({page:1,'all_organization':false});
    }

    if($rootScope.type =='all'){

        Entity.get({entity:'entities',c:'diseases,organizationsCat,transferCompany,propertyTypes,roofMaterials,buildingStatus,furnitureStatus,houseStatus,habitableStatus,currencies,aidCountries,countries,maritalStatus,workReasons,workStatus,workWages,workJobs,banks,aidSources,properties,essentials'},function (response) {
            $rootScope.organizationsCat = response.organizationsCat;
            $rootScope.transferCompany = response.transferCompany;
            $rootScope.currency = response.currencies;
            $scope.PropertyTypes = response.propertyTypes;
            $scope.RoofMaterials = response.roofMaterials;
            $scope.buildingStatus = response.buildingStatus;
            $scope.furnitureStatus = response.furnitureStatus;
            $scope.houseStatus = response.houseStatus;
            $scope.habitableStatus = response.habitableStatus;
            $scope.Diseases = response.diseases;
            $scope.Country = response.countries;
            $scope.aidCountries = response.aidCountries;
            $scope.MaritalStatus = response.maritalStatus;
            $scope.WorkReasons = response.workReasons;
            $scope.WorkStatus = response.workStatus;
            $scope.WorkWages = response.workWages;
            $scope.WorkJobs = response.workJobs;
            $scope.Banks = response.banks;
            $scope.Properties = response.properties;
            $scope.Aidsource = response.aidSources;
            $rootScope.aidsCasesfilters.Essential =  $scope.Essentials = response.essentials;
        });

        Org.list({type:'sponsors'},function (response) { $rootScope.Sponsors = response; });

    }

    $scope.sendSms=function(type,receipt_id) {

        if(type == 'manual'){
            var receipt = [];
            var pass = true;

            if(receipt_id ==0){
                angular.forEach($rootScope.items, function (v, k) {
                    if (v.check) {
                        receipt.push(v.person_id);
                    }
                });
                if(receipt.length==0){
                    pass = false;
                    $rootScope.toastrMessages('error',$filter('translate')("you did not select anyone to send them sms message"));
                    return;
                }
            }else{
                receipt.push(receipt_id);
            }

            if(pass){
                org_sms_service.getList(function (response) {
                    $rootScope.Providers = response.Service;
                });
                $uibModal.open({
                    templateUrl: '/app/modules/sms/views/modal/sent_sms_from_person_general.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance) {
                        $scope.sms={source: 'check' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:"",cont_type:""};
                        $scope.confirm = function (data) {
                            $rootScope.progressbar_start();
                            $rootScope.clearToastr();
                            var params = angular.copy(data);
                            params.receipt=receipt;
                            params.target='person';

                            org_sms_service.sendSmsToTarget(params,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed') {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status== 'success'){
                                    if(response.download_token){
                                        var file_type='xlsx';
                                         window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                    }

                                    $modalInstance.close();
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                            });
                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    },
                    resolve: {
                    }
                });

            }
        }
        else{
            org_sms_service.getList(function (response) {
                $rootScope.Providers = response.Service;
            });
            $uibModal.open({
                templateUrl: '/app/modules/sms/views/modal/sent_sms_from_xlx_general.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;
                    $scope.sms={source: 'file' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:"",cont_type:""};

                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };

                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };
                    $scope.download=function(){
                         window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-to-send-sms-template&template=true';
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: 'api/v1.0/sms/excel',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });


                    $scope.confirm = function (item) {
                        $scope.spinner=false;
                        $scope.import=false;
                        $rootScope.progressbar_start();
                        Uploader.onBeforeUploadItem = function(item) {

                            if($scope.sms.same_msg == 0){
                                $scope.sms.sms_messege = '';
                            }
                            item.formData = [$scope.sms];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);

                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                            }
                            else if(response.status=='success')
                            {
                                if(response.download_token){
                                    var file_type='xlsx';
                                     window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                }
                                $scope.spinner=false;
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $scope.import=false;
                                $rootScope.toastrMessages('success',response.msg);
                                $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});

        }
    };

    $scope.changeCategoryToGroup=function(size){
        $rootScope.clearToastr();
        var persons=[];
        angular.forEach($rootScope.items, function(v, k) {
            if(v.check){
                persons.push({'case_id':v.case_id,'category_id':v.category_id});
            }
        });

        if(persons.length==0){
            $rootScope.toastrMessages('error',$filter('translate')('All the cases you selected have the same type of help specified'));
        }else{
            $rootScope.row={category_id:""};
            $uibModal.open({
                templateUrl: 'changeCategory2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope, $modalInstance, $log) {

                    $rootScope.msg='';
                    $scope.confirm = function (params) {

                        $rootScope.clearToastr();
                        var FinalCases=[];
                        angular.forEach(persons, function(v, k) {
                            if(v.category_id != params.category_id){
                                FinalCases.push(v.case_id);
                            }
                        });

                        if(FinalCases.length ==0){
                            $rootScope.toastrMessages('error',$filter('translate')('All the cases you selected have the same type of help specified'));
                        }else {
                            params.cases =FinalCases;
                            params.id = params.category_id;
                            params.restricted = persons.length - FinalCases.length;
                            params.type='aids';
                            $rootScope.progressbar_start();
                            cases.changeCategoryToGroup(params,function (response) {
                                $rootScope.progressbar_complete();
                                $modalInstance.close();
                                if(response.status=='success')
                                {
                                    $rootScope.DataFilter.page=$rootScope.CurrentPage;
                                    getCases($rootScope.DataFilter);
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();
                            });
                        }


                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };


                }});

        }



    };

    $scope.changeCategory=function(size,case_id,category_id){
        $rootScope.clearToastr();

        $rootScope.row={count:1,id:case_id,pre_category_id:category_id,category_id:category_id};
        $uibModal.open({
            templateUrl: 'changeCategory.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: size,
            controller: function ($rootScope,$scope, $modalInstance, $log) {

                $rootScope.msg='';
                $scope.confirm = function (params) {
                    $rootScope.clearToastr();
                    if(params.pre_category_id == params.category_id){
                        $rootScope.toastrMessages('error',$filter('translate')("You do not change the status label"));
                    }else{
                        params.type='aids';
                        $rootScope.progressbar_start();
                        cases.changeCategory(params,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed') {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else{
                                $modalInstance.close();
                                if(response.status=='success')
                                {
                                    $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    LoadCases($rootScope.aidsCasesfilters);
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }
                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                            $modalInstance.close();
                        });
                    }
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };


            }});
    };

    $scope.ImportToUpdate=function(type){

        $rootScope.clearToastr();
        if(type == 'xlx')
            $uibModal.open({
                templateUrl: 'ImportToUpdate.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'md',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;
                    $scope.row={category_id:"",status:""};

                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };


                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };
                    $scope.download=function(){
                         window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-to-update-template&template=true';
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/common/aids/cases/importToUpdate',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    Uploader.onBeforeUploadItem = function(item) {
                        item.formData = [{source: 'file'}];
                    };

                    $scope.confirm = function (item) {
                        // angular.element('.btn').addClass("disabled");
                       $rootScope.clearToastr();

                        $scope.spinner=false;
                        $scope.import=false;

                        $rootScope.progressbar_start();
                        Uploader.onBeforeUploadItem = function(i) {
                            i.formData = [{category_id: item.category_id,status:item.status}];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            if(response.download_token){
                                // $rootScope.card_restrict=true;
                                // $rootScope.invalid_cards=response.invalid_cards;
                                 window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token;
                            }

                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            else if(response.status=='success')
                            {
                                $scope.spinner=false;
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $scope.import=false;
                                $rootScope.toastrMessages('success',response.msg);
                                $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                LoadCases($rootScope.aidsCasesfilters);
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                LoadCases($rootScope.aidsCasesfilters);
                                $modalInstance.close();
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});
        else{

            $uibModal.open({
                templateUrl: 'updateResone.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'md',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken,cases) {
                    $scope.row={category_id:"",status:"",reason:"",cases:[]};
                    $scope.confirm = function (item) {
                        $scope.row.type = 'aids';
                        $rootScope.cases=[];
                        angular.forEach($rootScope.items, function(v, k) {
                            if(v.check &&
                                (v.category_id == item.category_id) &&
                                (v.is_mine == '1' || v.is_mine == 1) && v.status != item.status){
                                if( (v.status == 0 ) || (  v.status == 1 && v.active_case_count == 0 )){
                                    $rootScope.cases.push(v.case_id);
                                }
                            }
                        });
                        if($rootScope.cases.length==0){
                            if($scope.row.status == 0){
                                $rootScope.toastrMessages('error',$filter('translate')('You have not select any case , may be not in your organization or active in other'));

                            }else{
                                $rootScope.toastrMessages('error',$filter('translate')('You have not select any case_'));
                            }
                            $modalInstance.close();
                        }else{
                            $rootScope.progressbar_start();
                            $scope.row.cases = $rootScope.cases;
                            cases.updateStatusAndCategories($scope.row,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='success')
                                {
                                    $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                    LoadCases($rootScope.aidsCasesfilters);
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    $modalInstance.dismiss('cancel');
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                $modalInstance.close();
                            });
                        }

                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                }});

        }
    };

    // ******************************************************************************************
    $scope.transferOne = function(item){

        $rootScope.clearToastr();

        $rootScope.full_name = item.full_name;
        $uibModal.open({
            templateUrl: 'transfer-request-manual.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken,cases) {

                $scope.row={source: 'manual' , category_id:"" , person_id:item.person_id,
                            adscountry_id:"", adsdistrict_id:"", adsneighborhood_id:"",
                            adssquare_id:"", adsmosques_id:""};

                Entity.get({entity:'entities',c:'aidCountries'},function (response) {
                    $scope.countries = response.aidCountries;
                });

                $scope.getLocation = function(entity,parent,value){
                    if(!angular.isUndefined(value)) {
                        if (value !== null && value !== "" && value !== " ") {
                            // , 'tree': true
                            Entity.listChildren({'entity': entity, 'id': value, 'parent': parent}, function (response) {

                                if (entity == 'sdistricts') {
                                    $scope.governarate = response;
                                    $scope.regions = [];
                                    $scope.nearlocation = [];
                                    $scope.squares = [];
                                    $scope.mosques = [];

                                    $scope.row.adsdistrict_id = "";
                                    $scope.row.adsregion_id = "";
                                    $scope.row.adsneighborhood_id = "";
                                    $scope.row.adssquare_id = "";
                                    $scope.row.adsmosques_id = "";

                                }
                                else if (entity == 'sregions') {
                                    $scope.regions = response;
                                    $scope.nearlocation = [];
                                    $scope.squares = [];
                                    $scope.mosques = [];
                                    $scope.row.adsregion_id = "";
                                    $scope.row.adsneighborhood_id = "";
                                    $scope.row.adssquare_id = "";
                                    $scope.row.adsmosques_id = "";
                                }
                                else if (entity == 'sneighborhoods') {
                                    $scope.nearlocation = response;
                                    $scope.squares = [];
                                    $scope.mosques = [];
                                    $scope.row.adsneighborhood_id = "";
                                    $scope.row.adssquare_id = "";
                                    $scope.row.adsmosques_id = "";

                                }
                                else if (entity == 'ssquares') {
                                    $scope.squares = response;
                                    $scope.mosques = [];
                                    $scope.row.adssquare_id = "";
                                    $scope.row.adsmosques_id = "";
                                }
                                else if (entity == 'smosques') {
                                    $scope.mosques = response;
                                    $scope.row.adsmosques_id = "";
                                }
                            });
                        }
                    }
                };

                $scope.confirm = function () {
                    $scope.row.type = 'aids';
                    cases.transferOne($scope.row,function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            $modalInstance.dismiss('cancel');
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        $modalInstance.close();
                    });
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };

            }});


    }
    $scope.transferGroup = function(type,item){

        $rootScope.clearToastr();

        if(type == 'xlx'){
            $uibModal.open({
                templateUrl: 'transfer-request-excel.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;
                    $scope.row={source: 'file' , category_id:"" , persons:[],
                        adscountry_id:"", adsdistrict_id:"", adsneighborhood_id:"",
                        adssquare_id:"", adsmosques_id:""};

                    Entity.get({entity:'entities',c:'aidCountries'},function (response) {
                        $scope.countries = response.aidCountries;
                    });

                    $scope.getLocation = function(entity,parent,value){
                        if(!angular.isUndefined(value)) {
                            if (value !== null && value !== "" && value !== " ") {
                                // , 'tree': true
                                Entity.listChildren({'entity': entity, 'id': value, 'parent': parent}, function (response) {

                                    if (entity == 'sdistricts') {
                                        $scope.governarate = response;
                                        $scope.regions = [];
                                        $scope.nearlocation = [];
                                        $scope.squares = [];
                                        $scope.mosques = [];

                                        $scope.row.adsdistrict_id = "";
                                        $scope.row.adsregion_id = "";
                                        $scope.row.adsneighborhood_id = "";
                                        $scope.row.adssquare_id = "";
                                        $scope.row.adsmosques_id = "";

                                    }
                                    else if (entity == 'sregions') {
                                        $scope.regions = response;
                                        $scope.nearlocation = [];
                                        $scope.squares = [];
                                        $scope.mosques = [];
                                        $scope.row.adsregion_id = "";
                                        $scope.row.adsneighborhood_id = "";
                                        $scope.row.adssquare_id = "";
                                        $scope.row.adsmosques_id = "";
                                    }
                                    else if (entity == 'sneighborhoods') {
                                        $scope.nearlocation = response;
                                        $scope.squares = [];
                                        $scope.mosques = [];
                                        $scope.row.adsneighborhood_id = "";
                                        $scope.row.adssquare_id = "";
                                        $scope.row.adsmosques_id = "";

                                    }
                                    else if (entity == 'ssquares') {
                                        $scope.squares = response;
                                        $scope.mosques = [];
                                        $scope.row.adssquare_id = "";
                                        $scope.row.adsmosques_id = "";
                                    }
                                    else if (entity == 'smosques') {
                                        $scope.mosques = response;
                                        $scope.row.adsmosques_id = "";
                                    }
                                });
                            }
                        }
                    };


                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };

                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };
                    $scope.download=function(){
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token=transfer-template&template=true';
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/common/aids/cases/transferGroup',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    Uploader.onBeforeUploadItem = function(item) {
                        item.formData = [{source: 'file'}];
                    };

                    $scope.confirm = function (item) {
                        // angular.element('.btn').addClass("disabled");
                        $rootScope.clearToastr();

                        $scope.spinner=false;
                        $scope.import=false;

                        $rootScope.progressbar_start();
                        Uploader.onBeforeUploadItem = function(i) {
                            i.formData = [{source: 'file',category_id: item.category_id, adscountry_id: item.adscountry_id,
                                           adsdistrict_id: item.adsdistrict_id, adsneighborhood_id: item.adsneighborhood_id,
                                           adssquare_id: item.adssquare_id, adsmosques_id: item.adsmosques_id}];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            if(response.download_token){
                                window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token;
                            }

                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            else if(response.status=='success')
                            {
                                $scope.spinner=false;
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $scope.import=false;
                                $rootScope.toastrMessages('success',response.msg);
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $modalInstance.close();
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});
        }
        else{

            $rootScope.fullname = '';
            var persons=[];
            angular.forEach($rootScope.items, function(v, k) {
                if(v.check){
                    persons.push(v.person_id);
                }
            });

            if(persons.length==0){
                $rootScope.toastrMessages('error',$filter('translate')('You have not select any case_'));
            }else{

                $uibModal.open({
                    templateUrl: 'transfer-request-manual.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken,cases) {

                        $scope.row={source: 'manual' , category_id:"" , persons:persons,
                                    adscountry_id:"", adsdistrict_id:"", adsneighborhood_id:"",
                                    adssquare_id:"", adsmosques_id:""};

                        Entity.get({entity:'entities',c:'aidCountries'},function (response) {
                            $scope.countries = response.aidCountries;
                        });

                        $scope.getLocation = function(entity,parent,value){
                            if(!angular.isUndefined(value)) {
                                if (value !== null && value !== "" && value !== " ") {
                                    // , 'tree': true
                                    Entity.listChildren({'entity': entity, 'id': value, 'parent': parent}, function (response) {

                                        if (entity == 'sdistricts') {
                                            $scope.governarate = response;
                                            $scope.regions = [];
                                            $scope.nearlocation = [];
                                            $scope.squares = [];
                                            $scope.mosques = [];

                                            $scope.row.adsdistrict_id = "";
                                            $scope.row.adsregion_id = "";
                                            $scope.row.adsneighborhood_id = "";
                                            $scope.row.adssquare_id = "";
                                            $scope.row.adsmosques_id = "";

                                        }
                                        else if (entity == 'sregions') {
                                            $scope.regions = response;
                                            $scope.nearlocation = [];
                                            $scope.squares = [];
                                            $scope.mosques = [];
                                            $scope.row.adsregion_id = "";
                                            $scope.row.adsneighborhood_id = "";
                                            $scope.row.adssquare_id = "";
                                            $scope.row.adsmosques_id = "";
                                        }
                                        else if (entity == 'sneighborhoods') {
                                            $scope.nearlocation = response;
                                            $scope.squares = [];
                                            $scope.mosques = [];
                                            $scope.row.adsneighborhood_id = "";
                                            $scope.row.adssquare_id = "";
                                            $scope.row.adsmosques_id = "";

                                        }
                                        else if (entity == 'ssquares') {
                                            $scope.squares = response;
                                            $scope.mosques = [];
                                            $scope.row.adssquare_id = "";
                                            $scope.row.adsmosques_id = "";
                                        }
                                        else if (entity == 'smosques') {
                                            $scope.mosques = response;
                                            $scope.row.adsmosques_id = "";
                                        }
                                    });
                                }
                            }
                        };

                        $scope.confirm = function () {
                            $scope.row.type = 'aids';
                            cases.transferGroup($scope.row,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='success') {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    $modalInstance.dismiss('cancel');
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                $modalInstance.close();
                            });
                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    }});

            }
        }

    }

    // ******************************************************************************************


    $scope.update=function(type){

        $rootScope.clearToastr();
        if(type == 'xlx')
            $uibModal.open({
                templateUrl: '/app/modules/aid/views/cases/updateData.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'md',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;
                    $scope.row={category_id:"",status:""};

                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };


                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };
                    $scope.download=function(){
                         window.location = '/api/v1.0/common/files/xlsx/export?download_token=card-check-template&template=true';
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/common/aids/cases/goverment_update',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    Uploader.onBeforeUploadItem = function(item) {
                        item.formData = [{source: 'file'}];
                    };

                    $scope.confirm = function (item) {
                        // angular.element('.btn').addClass("disabled");

                        $scope.spinner=false;
                        $scope.import=false;

                        $rootScope.progressbar_start();
                        Uploader.onBeforeUploadItem = function(i) {
                            i.formData = [{source:'file'}];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            if(response.download_token){
                                 window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token;
                            }

                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            else if(response.status=='success')
                            {
                                $scope.spinner=false;
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $scope.import=false;
                                $rootScope.toastrMessages('success',response.msg);
                                $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                LoadCases($rootScope.aidsCasesfilters);
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                LoadCases($rootScope.aidsCasesfilters);
                                $modalInstance.close();
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});
        else{
            $rootScope.cases=[];
            angular.forEach($rootScope.items, function(v, k) {
                if(v.check){
                    $rootScope.cases.push(v.id_card_number);
                }
            });
            if($rootScope.cases.length==0){
                $rootScope.toastrMessages('error',$filter('translate')('You have not select any case'));
            }else{
                $rootScope.progressbar_start();
                cases.goverment_update({'type':'aids' , 'source':'cards' , cases:$rootScope.cases},function (response) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                    if(response.download_token){
                         window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token;
                    }

                    if(response.status=='error' || response.status=='failed')
                    {
                        $rootScope.toastrMessages('error',response.msg);
                    }
                    else if(response.status=='success')
                    {
                        $rootScope.toastrMessages('success',response.msg);
                        $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                        LoadCases($rootScope.aidsCasesfilters);
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                        $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                        LoadCases($rootScope.aidsCasesfilters);
                    }

                });
            }
        }
    };

    Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

    if($rootScope.type != 'deleted'){
        cs_persons_vouchers.list({id:1},function (response) {
              $rootScope.Vouchers = response.Vouchers;
              $rootScope.Owns = response.Owns;
        });

        $rootScope.Choices = vouchers_categories.List();
    }
    $scope.Search=function(selected,params,action) {
        $rootScope.clearToastr();

        var data = angular.copy(params);
            data.persons=[];

        if(selected == true || selected == 'true' ){
           var persons=[];
            angular.forEach($rootScope.items, function(v, k){

                if(v.check){
                    persons.push(v.case_id);
                }

            });

            if(persons.length==0){
                $rootScope.toastrMessages('error',$filter('translate')('You have not select people to export'));
                return ;
            }
            data.persons=persons;
        }

        data.Properties=$scope.PropertiesFilter;
        data.AidSources =$scope.AidSourceFilter;
        data.action=action;
        if(action != 'ExportToExcel' && action != 'Basic'&& action != 'ExportToWord' && action != 'childs' && action != 'wives' && action != 'parents'&& action != 'FamilyMember'){
            data.page=$scope.CurrentPage;
        }
        LoadCases(data);

    };

    $scope.disblay_orgs =false;
    $scope.resetOrg=function(all_organization,organization_category){
        $rootScope.clearToastr();
        $scope.disblay_orgs =false;
        let ids = [];
        if( !angular.isUndefined(all_organization)) {
            if(all_organization == true ||all_organization == 'true'){
                angular.forEach(organization_category, function(v, k) {
                    ids.push(v.id);
                });

                Org.ListOfCategory({ids:ids},function (response) {
                    if (response.list.length > 0){
                        $scope.Org_ = response.list;
                        $scope.disblay_orgs =true;
                    }else{
                        $scope.Org_ = [];
                        $scope.disblay_orgs =false;
                        $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');
                    }
                });
            }else{
                $scope.Org_ = [];
                $scope.disblay_orgs =false;
                $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');
            }
        }else{
            $scope.Org_ = [];
            $scope.disblay_orgs =false;
            $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');

        }


    };

    $scope.sortKeyArr=[];
    $scope.sortKeyArr_rev=[];
    $scope.sortKeyArr_un_rev=[];
    $scope.sort_ = function(keyname){
        $scope.sortKey_ = keyname;
        var reverse_ = false;
        if (
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
        ) {
            reverse_ = true;
        }
        var data = angular.copy($rootScope.aidsCasesfilters);
        data.action ='filter';

        if (reverse_) {
            if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_rev.push(keyname);
            }
        }
        else {
            if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_un_rev.push(keyname);
            }

        }

        data.sortKeyArr_rev = $scope.sortKeyArr_rev;
        data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;
        $scope.Search(data,'filter');
    };


    $scope.get = function(target,value,parant){
        $rootScope.clearToastr();

        if (!angular.isUndefined(value)) {
            if(value != null && value != "" && value != " " ) {
                Entity.listChildren({entity:target,'id':value,'parent':parant},function (response) {

                    if (target == 'sdistricts') {
                        $scope.governarate = response;
                        $scope.regions = [];
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];

                        $rootScope.aidsCasesfilters.adsdistrict_id = "";
                        $rootScope.aidsCasesfilters.adsregion_id = "";
                        $rootScope.aidsCasesfilters.adsneighborhood_id = "";
                        $rootScope.aidsCasesfilters.adssquare_id = "";
                        $rootScope.aidsCasesfilters.adsmosques_id = "";

                    }
                    else if (target == 'sregions') {
                        $scope.regions = response;
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];
                        $rootScope.aidsCasesfilters.adsregion_id = "";
                        $rootScope.aidsCasesfilters.adsneighborhood_id = "";
                        $rootScope.aidsCasesfilters.adssquare_id = "";
                        $rootScope.aidsCasesfilters.adsmosques_id = "";
                    }
                    else if (target == 'sneighborhoods') {
                        $scope.nearlocation = response;
                        $scope.squares = [];
                        $scope.mosques = [];
                        $rootScope.aidsCasesfilters.adsneighborhood_id = "";
                        $rootScope.aidsCasesfilters.adssquare_id = "";
                        $rootScope.aidsCasesfilters.adsmosques_id = "";

                    }
                    else if (target == 'ssquares') {
                        $scope.squares = response;
                        $scope.mosques = [];
                        $rootScope.aidsCasesfilters.adssquare_id = "";
                        $rootScope.aidsCasesfilters.adsmosques_id = "";
                    }
                    else if (target == 'smosques') {
                        $scope.mosques = response;
                        $rootScope.aidsCasesfilters.adsmosques_id = "";
                    }else if(target =='branches'){
                        $scope.Branches = response;
                        $rootScope.aidsCasesfilters.branch_name="";
                    }

                    // if(target =='districts'){
                    //     $scope.governarate = response;
                    //     $scope.city = [];
                    //     $scope.nearlocation =[];
                    //     $scope.mosques = [];
                    //     $rootScope.aidsCasesfilters.person_governarate="";
                    // }else if(target =='cities'){
                    //     $scope.city = response;
                    //     $rootScope.aidsCasesfilters.person_city="";
                    // }else if(target =='neighborhoods'){
                    //     $scope.nearlocation = response;
                    //     $rootScope.aidsCasesfilters.location_id="";
                    // }else if(target =='mosques'){
                    //     $scope.mosques = response;
                    //     $rootScope.aidsCasesfilters.mosques_id="";
                    // }

                });
            }
        }
    };
    $scope.pageChanged = function (currentPage) {
        $rootScope.clearToastr();
        $rootScope.aidsCasesfilters.page=currentPage;
        LoadCases($rootScope.aidsCasesfilters);
    };

    $scope.itemsCount = '50';

    $scope.itemsPerPage_ = function (itemsCount) {
        $rootScope.clearToastr();
        $rootScope.aidsCasesfilters.itemsCount=itemsCount;
        $rootScope.aidsCasesfilters.page=1;
        LoadCases($rootScope.aidsCasesfilters);
    };

    $scope.exportRestricted = function (restricted) {

        $rootScope.clearToastr();
        $rootScope.progressbar_start();
        $http({
            url: "/api/v1.0/common/cases/exportRestricted",
            method: "POST",
            data: {exported:restricted}
        }).then(function (response) {
            $rootScope.progressbar_complete();
             window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token;

        }, function(error) {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('error',error.data.msg);
        });
    };
    $scope.exportRestrictedCard = function (restricted) {
        $rootScope.clearToastr();
        $rootScope.progressbar_start();
        $http({
            url: "/api/v1.0/common/cases/exportRestrictedCard",
            method: "POST",
            data: {exported:restricted}
        }).then(function (response) {
            $rootScope.progressbar_complete();
             window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token;
        }, function(error) {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('error',error.data.msg);
        });
    };
    $scope.download = function (target,id) {

        $rootScope.clearToastr();
        var url ='';
        var file_type ='zip';
        var  params={};
        var method='';

        if(target =='word'){
            method='POST';
            params={'case_id':id,
                'target':'info',
                'action':'filters',
                'mode':'show',
                'category_type' : 2,
                'person' :true,
                'visitor_note' :true,
                'persons_i18n' : true,
                'category' : true,
                'health' : true,
                'contacts' : true,
                'work' : true,
                'default_bank' : true,
                'residence' : true,
                'case_needs' : true,
                'reconstructions' : true,
                'home_indoor' : true,
                'financial_aid_source' : true,
                'non_financial_aid_source' : true,
                'persons_properties' : true,
                'persons_documents' : true,
                'family_member' : true
            };
            url = "/api/v1.0/common/aids/cases/word";
        }
        else if(target =='pdf'){
            method='POST';
            params={'case_id':id,
                'target':'info',
                'action':'filters',
                'mode':'show',
                'category_type' : 2,
                'person' :true,
                'visitor_note' :true,
                'persons_i18n' : true,
                'category' : true,
                'contacts' : true,
                'work' : true,
                'banks' : true,
                'default_bank' : true,
                'residence' : true,
                'case_needs' : true,
                'reconstructions' : true,
                'home_indoor' : true,
                'financial_aid_source' : true,
                'non_financial_aid_source' : true,
                'persons_properties' : true,
                'health' : true,
                'persons_documents' : true,
                'family_member' : true
            };
            url = "/api/v1.0/common/aids/cases/pdf";
        }else{
            method='GET';
            url = "/api/v1.0/common/aids/cases/exportCaseDocument/"+id;
        }

        $rootScope.progressbar_start();
        $http({
            url:url,
            method: method,
            data:params
        }).then(function (response) {
            $rootScope.progressbar_complete();
            if(target =='archive' && response.data.status == false){
                $rootScope.toastrMessages('error',$filter('translate')('no attachment to export'));
            }else{
                 window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
            }
        }, function(error) {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('error',error.data.msg);
        });
    };
    $scope.setPropertiesFilter=function(id,value){
        var index = id_index = -1;
        if ($scope.pro_id.indexOf(id) == -1) {
            if (value != "") {
                $scope.PropertiesFilter.push({'id':id,'exist':value});
                $scope.pro_id.push(id);
            }
        }else{
            id_index=$scope.pro_id.indexOf(id);
            for (var i = 0, len = $scope.PropertiesFilter.length; i <= len; i++) {
                if ($scope.PropertiesFilter[i].id == id) {
                    index= i;
                    break;
                }
            }
        }
        if(index != -1){
            if (value == "") {
                $scope.PropertiesFilter.splice(index, 1);
                $scope.pro_id.splice(id_index, 1);
            } else {
                $scope.PropertiesFilter[index].exist = value;
            }
        }
    };
    $scope.setAidsourceFilter=function(id,value){
        var aindex = aid_index = -1;
        if ($scope.aid_id.indexOf(id) == -1) {
            if (value != "") {
                $scope.AidSourceFilter.push({'id':id,'aid_take':value});
                $scope.aid_id.push(id);
            }
        }else{
            aid_index=$scope.aid_id.indexOf(id);
            for (var i = 0, len = $scope.AidSourceFilter.length; i <= len; i++) {
                if ($scope.AidSourceFilter[i].id == id) {
                    aindex= i;
                    break;
                }
            }
        }
        if(aindex != -1){
            if (value == "") {
                $scope.AidSourceFilter.splice(aindex, 1);
                $scope.aid_id.splice(aid_index, 1);
            } else {
                $scope.AidSourceFilter[aindex].aid_take = value;
            }
        }
    };
    $scope.setEssentialsFilter=function(z,target,id,value){
        $rootScope.aidsCasesfilters.Essential[z].id=id;
    };
    $scope.toggleSearchform = function() {

        $scope.toggleStatus = $scope.toggleStatus == false ? true: false;
        if($scope.toggleStatus == true) {
            $rootScope.aidsCasesfilters.page=1;
            $rootScope.aidsCasesfilters.action='filter';
        }
        resetFilter();

    };
    $scope.insertNewVouchers=function(type){

        if(type == 'xlx'){
            $uibModal.open({
                templateUrl: 'ImportToVouchers.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'md',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;
                    $scope.row={category_id:"",status:""};
                    $scope.person_vouchers_import = {};
                    $scope.person_vouchers_import.withRatio=false;
                    $scope.person_vouchers_import.add_cases=false;

                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };


                    $scope.fileUpload=function(){
                        Uploader.destroy();
                        Uploader.queue=[];
                        $scope.upload=true;
                    };
                    $scope.download=function(){
                         window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-persons-voucher-template&template=true';
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/aids/vouchers/beneficiary/excel/cases',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    Uploader.onBeforeUploadItem = function(item) {
                        item.formData = [{source: 'file'}];
                    };

                    $scope.confirm = function (items) {
                        $rootScope.clearToastr();
                        $scope.spinner=false;
                        $scope.import=false;
                        $rootScope.progressbar_start();
                        Uploader.onBeforeUploadItem = function(i) {
                            var ratio = 0;
                            if(items.with_ratio == 'true' || items.with_ratio == 'true' ){
                                ratio = 1;
                            }
                            i.formData = [{voucher_id: items.voucher_id, ratio:ratio,
                                withRatio:items.withRatio,
                                add_cases:items.add_cases,
                                // receipt_location:items.receipt_location,
                                receipt_date:$filter('date')(items.receipt_date, 'yyyy/MM/dd')
                            }];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);

                            if(response.download_token){
                                 window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token;
                            }

                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            else if(response.status=='success')
                            {
                                $scope.spinner=false;
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $scope.import=false;
                                $rootScope.toastrMessages('success',response.msg);
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $modalInstance.close();
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.dateOptions = {
                        formatYear: 'yy',
                        startingDay: 0
                    };
                    $scope.formats = [ 'dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                    $scope.format = $scope.formats[0];
                    $scope.today = function() {
                        $scope.dt = new Date();
                    };
                    $scope.today();
                    $scope.clear = function() {
                        $scope.dt = null;
                    };
                    $scope.open4 = function($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.popup4.opened = true;
                    };
                    $scope.popup4 = {
                        opened: false
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});
        }else{
            $rootScope.clearToastr();
            var persons=[];
            angular.forEach($rootScope.items, function(v, k) {
                if(v.check && (v.status == 0 || v.status == '0')){
                    persons.push({'id':v.person_id,'case_id':v.case_id,'category_id':v.category_id,'id_card_number': v.id_card_number});
                }
            });
            if(persons.length==0){
                $rootScope.toastrMessages('error',$filter('translate')('You have not select any invalid case on your locations or on your organization for an active case to get voucher'));
            }
            else{
                $uibModal.open({
                    templateUrl: 'insertNewVouchers.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance, $log,cs_persons_vouchers) {
                        $scope.person_vouchers={};
                        $scope.person_vouchers.withRatio=false;
                        $scope.person_vouchers.add_cases=false;

                        if( $rootScope.person_restrict == true){
                            $rootScope.person_restrict =false;
                        }

                        $scope.confirmInsert = function (persons_vouchers) {
                            $rootScope.clearToastr();
                            $rootScope.progressbar_start();
                            var persons_vouchers1= {};

                            var case_category_id = -1;

                            angular.forEach($rootScope.Vouchers, function(v, k) {
                                if(v.id == persons_vouchers.voucher_id){
                                    case_category_id = v.case_category_id
                                }
                            });

                            var final_persons =[];
                            if(case_category_id != -1){

                                angular.forEach(persons, function(v, k) {
                                    if(v.category_id == case_category_id){
                                        final_persons.push(v);
                                    }
                                });
                                if(final_persons.length==0){
                                    $rootScope.toastrMessages('error',$filter('translate')('You have not select any invalid case on your locations or on your organization for an active case to get voucher , or case not has the same category of voucher'));
                                    return ;
                                }

                                if(final_persons.length != persons.length){
                                    $rootScope.toastrMessages('warning',$filter('translate')('There are case not has the same category of voucher'));
                                }

                            }else{
                                final_persons =persons;
                            }

                            persons_vouchers1.persons=final_persons;
                            persons_vouchers1.receipt_date=$filter('date')(persons_vouchers.receipt_date, 'yyyy/MM/dd');
                            persons_vouchers1.receipt_location=persons_vouchers.receipt_location;
                            persons_vouchers1.voucher_id=persons_vouchers.voucher_id;
                            persons_vouchers1.withRatio=persons_vouchers.withRatio;
                            persons_vouchers1.add_cases=persons_vouchers.add_cases;
                            persons_vouchers1.source='ids';
                            cs_persons_vouchers.setBeneficiary(persons_vouchers1,function (response) {
                                $rootScope.progressbar_complete();

                                if(response.download_token){
                                    var file_type='xlsx';
                                     window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;

                                }

                                if(response.status=='failed') {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid') {

                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;

                                } else{
                                    $modalInstance.close();
                                    if(response.status=='success') {
                                        $rootScope.toastrMessages('success',response.msg);
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                }
                            });
                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };


                        $scope.dateOptions = {
                            formatYear: 'yy',
                            startingDay: 0
                        };
                        $scope.formats = [ 'dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                        $scope.format = $scope.formats[0];
                        $scope.today = function() {
                            $scope.dt = new Date();
                        };
                        $scope.today();
                        $scope.clear = function() {
                            $scope.dt = null;
                        };
                        $scope.open4 = function($event) {
                            $event.preventDefault();
                            $event.stopPropagation();
                            $scope.popup4.opened = true;
                        };
                        $scope.popup4 = {
                            opened: false
                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    },
                    resolve: {
                    }
                });
            }
        }
    };

    $scope.allCheckbox=function(value){
        if (value) {
            $scope.selectedAll = true;
        } else {
            $scope.selectedAll = false;
        }

        angular.forEach($rootScope.items, function(v, k) {
            v.check=$scope.selectedAll;
        });
    };
    $scope.selectCustomForm = function (size) {
        $rootScope.clearToastr();

        $rootScope.Forms = custom_forms.getCustomFormsList();

        $rootScope.formdata={};
        setting.getCustomForm({'id':'aid_custom_form'},function (response) {
            $rootScope.has_custom_form=response.status;
            if(response.status){
                $rootScope.formdata.form_id=response.Setting.value +"";
            }
        });


        $uibModal.open({
            templateUrl: 'myModalContent1.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: size,
            controller: function ($rootScope,$scope, $modalInstance, $log,setting) {

                if( $rootScope.person_restrict == true){
                    $rootScope.person_restrict =false;
                }

                $scope.confirm = function (form) {
                    $rootScope.clearToastr();
                    form.setting_id='aid_custom_form';
                    $rootScope.progressbar_start();
                    setting.setCustomForm(form,function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='failed')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else if(response.status=='failed_valid')
                        {
                            $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                            $scope.status1 =response.status;
                            $scope.msg1 =response.msg;
                        }
                        else{
                            $modalInstance.close();
                            if(response.status == 'success')
                            {
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                        }

                    }, function(error) {
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);
                        $modalInstance.close();
                    });
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            },
            resolve: {

            }
        });
    };
    $scope.visitorNote=function(id){
        $rootScope.clearToastr();
        $rootScope.Notes='';
        $rootScope.progressbar_start();
        cases.getCaseReports
        ({'action':'filters','target':'info','case_id':id,'visitor_note':true, 'mode':'show'},function(response) {
            $rootScope.progressbar_complete();
            if(response.visitor_note_status==false)
            {
                $rootScope.toastrMessages('error',$filter('translate')('no visitor note in setting'));
            }else{
                $rootScope.Notes = response.visitor_note;
                $uibModal.open({
                    templateUrl: 'VisitorNote.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance, $log) {
                        $rootScope.model_error_status =false;

                        if( $rootScope.person_restrict == true){
                            $rootScope.person_restrict =false;
                        }

                        $rootScope.msg='';



                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    },
                    resolve: {
                    }
                });
            }
        });

    };
    $scope.statusLog=function(id){
        $rootScope.clearToastr();
        $uibModal.open({
            size: 'lg',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            templateUrl: 'statusLog.html',
            controller: function ($rootScope,$scope,$modalInstance) {
                $scope.logs=[];

                if( $rootScope.person_restrict == true){
                    $rootScope.person_restrict =false;
                }

                if( $rootScope.model_error_status2 == true){
                    $rootScope.model_error_status2 =false;
                }

                var loadStatusLog = function(params) {
                    params.type='aids';
                    $rootScope.progressbar_start();
                    cases.getStatusLogs(params ,function (response) {
                        $rootScope.progressbar_complete();
                        $scope.logs= response.data;
                        $scope.CurrentPage_1 = response.current_page;
                        $scope.TotalItems_1 = response.total;
                        $scope.ItemsPerPage_1 = response.per_page;
                    });
                };

                $scope.statusPageChanged = function (CurrentPage) {
                    loadStatusLog({page:CurrentPage,id:id});
                };

                loadStatusLog({page:1,id:id});


                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }});

    };
    $scope.updateStatus= function($case_id,status,card){

        $rootScope.clearToastr();

        if($rootScope.check_id(card)){

            if( $rootScope.person_restrict == true){
                $rootScope.person_restrict =false;
            }
            $rootScope.msg='';
            $uibModal.open({

                size: 'sm',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                templateUrl: 'detail.html',
                controller: function ($rootScope,$scope,$modalInstance) {


                    $scope.confirm = function (params) {
                        $rootScope.clearToastr();

                        if( !angular.isUndefined(params.date)) {
                            params.date=$filter('date')(params.date, 'dd-MM-yyyy')
                        }

                        params.type='aids';
                        params.id=$case_id;
                        params.status=status;
                        $rootScope.progressbar_start();

                        cases.update(params,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                $modalInstance.close();
                                if(response.status=='success')
                                {
                                    $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                    LoadCases($rootScope.aidsCasesfilters);
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }
                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                        });

                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }});

        }else{
            $rootScope.toastrMessages('error',$filter('translate')('you can not update status , invalid number'));
        }
    };
    $scope.delete = function (size,id) {
        $rootScope.clearToastr();

        $uibModal.open({
            templateUrl: 'myModalContent2.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: size,
            controller: function ($rootScope,$scope, $modalInstance, $log) {
                if( $rootScope.person_restrict == true){
                    $rootScope.person_restrict =false;
                }

                $scope.data={};
                $scope.confirmDelete = function (data) {
                    $rootScope.clearToastr();
                    data.id=id;
                    data.type='aids';
                    $rootScope.progressbar_start();
                    cases.delete(data,function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status== 'success'){
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            $modalInstance.close();
                            $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                            LoadCases($rootScope.aidsCasesfilters);
                        }

                    }, function(error) {
                        $rootScope.progressbar_complete();
                        $modalInstance.close();
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    };
    $scope.restore = function (size,id) {
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: 'restore.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: size,
            controller: function ($rootScope,$scope, $modalInstance, $log) {
                $scope.confirm = function () {
                    $rootScope.clearToastr();
                    $rootScope.progressbar_start();
                    cases.restore({id:id,type:'aids'},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status== 'success'){
                            $modalInstance.close();
                            $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            LoadCases($rootScope.aidsCasesfilters);
                        }
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        $modalInstance.close();
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    };

    $scope.urgentVoucher=function(size,person_id,case_id,case_category_id){
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: 'urgentVoucher.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: size,
            controller: function ($rootScope,$scope, $modalInstance, $log,cs_persons_vouchers) {
                $scope.voucher={urgent:1,count:1,persons:[person_id],case_id:case_id,transfer_company_id:null,case_category_id:case_category_id};
                // receipt_time input

                $scope.mytime = new Date();
                $scope.displayTime = 'n/a';
                $scope.$watch('mytime', function(newValue, oldValue) {
                    var hour = $scope.mytime.getHours()-($scope.mytime.getHours() >= 12 ? 12 : 0),
                        period = $scope.mytime.getHours() >= 12 ? 'PM' : 'AM';
                    $scope.displayTime = hour+':'+$scope.mytime.getMinutes()+' '+period
                })
                $scope.hstep = 1;
                $scope.mstep = 1;
                $scope.options = {
                    hstep: [1, 2, 3],
                    mstep: [1, 2, 3]
                };
                $scope.ismeridian = true;
                $scope.toggleMode = function() {
                    $scope.ismeridian = !$scope.ismeridian;
                };

                $rootScope.msg='';
                $scope.confirm = function (voucher) {
                    voucher.receipt_time = $filter('date')( new Date($scope.mytime), 'HH:MM:ss', 'Asia/Gaza') ;
                    $rootScope.clearToastr();
                    if( !angular.isUndefined(voucher.voucher_date)) {
                        voucher.voucher_date=$filter('date')(voucher.voucher_date, 'dd-MM-yyyy')
                    }
                    if( !angular.isUndefined(voucher.receipt_date)) {
                        voucher.receipt_date=$filter('date')(voucher.receipt_date, 'dd-MM-yyyy')
                    }

                    if( !angular.isUndefined(voucher.transfer)) {
                        if(voucher.transfer == 0 ||voucher.transfer == '0'){
                            voucher.transfer_company_id = null
                        }
                    }

                    voucher.beneficiary =1;
                    $rootScope.progressbar_start();
                    cs_persons_vouchers.save(voucher,function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='error' || response.status=='failed')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else if(response.status=='failed_valid')
                        {
                            $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                            $scope.status1 =response.status;
                            $scope.msg1 =response.msg;
                        }
                        else{
                            $modalInstance.close();
                            if(response.status=='success') {
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                        }
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);
                        $modalInstance.close();
                    });

                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };

                $scope.open = function($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.popup.opened = true;
                };

                $scope.popup = {
                    opened: false
                };
                $scope.open45 = function($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.popup45.opened = true;
                };

                $scope.popup45 = {
                    opened: false
                };

            }});
    };
    $scope.goTo=function(action,person,case_id,category_id){
        $rootScope.clearToastr();
        angular.forEach($rootScope.aidCategories, function(v, k) {
            if(v.id ==category_id){
                $state.go('caseform.'+v.FirstStep,{"mode":action,"id" :case_id,"category_id" :category_id});
            }
        });

    };
    $scope.resetFilters=function(){
        resetFilter();
    }

    $scope.import=function(target){
        $rootScope.clearToastr();
        if(target =='cases'){
            $uibModal.open({
                templateUrl: 'import.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;
                    $scope.row={source: 'file' , category_id:"" , import_type:"",
                                adscountry_id:"", adsdistrict_id:"", adsneighborhood_id:"",
                                adssquare_id:"", adsmosques_id:""};

                    Entity.get({entity:'entities',c:'aidCountriesTree'},function (response) {
                        $scope.countries = response.aidCountriesTree;
                    });

                    $scope.getLocation = function(entity,parent,value){
                        if(!angular.isUndefined(value)) {
                            if (value !== null && value !== "" && value !== " ") {
                                Entity.listChildren({'entity': entity, 'id': value, 'parent': parent, 'tree': true}, function (response) {

                                    if (entity == 'sdistricts') {
                                        $scope.governarate = response;
                                        $scope.regions = [];
                                        $scope.nearlocation = [];
                                        $scope.squares = [];
                                        $scope.mosques = [];

                                        $scope.row.adsdistrict_id = "";
                                        $scope.row.adsregion_id = "";
                                        $scope.row.adsneighborhood_id = "";
                                        $scope.row.adssquare_id = "";
                                        $scope.row.adsmosques_id = "";

                                    }
                                    else if (entity == 'sregions') {
                                        $scope.regions = response;
                                        $scope.nearlocation = [];
                                        $scope.squares = [];
                                        $scope.mosques = [];
                                        $scope.row.adsregion_id = "";
                                        $scope.row.adsneighborhood_id = "";
                                        $scope.row.adssquare_id = "";
                                        $scope.row.adsmosques_id = "";
                                    }
                                    else if (entity == 'sneighborhoods') {
                                        $scope.nearlocation = response;
                                        $scope.squares = [];
                                        $scope.mosques = [];
                                        $scope.row.adsneighborhood_id = "";
                                        $scope.row.adssquare_id = "";
                                        $scope.row.adsmosques_id = "";

                                    }
                                    else if (entity == 'ssquares') {
                                        $scope.squares = response;
                                        $scope.mosques = [];
                                        $scope.row.adssquare_id = "";
                                        $scope.row.adsmosques_id = "";
                                    }
                                    else if (entity == 'smosques') {
                                        $scope.mosques = response;
                                        $scope.row.adsmosques_id = "";
                                    }
                                });
                            }
                        }
                    };

                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };

                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };

                    $scope.uploader = new FileUploader({
                        url: '/api/v1.0/common/aids/cases/import' ,
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    $scope.download=function(import_type){
                        var   file = 'import-cases-template';
                        if(import_type == 2){
                            file = 'import-m-cases-template';
                        }

                        window.location = '/api/v1.0/common/files/xlsx/export?download_token='+file+'&template=true';
                    };

                    $scope.RestType=function(import_type){
                        $scope.upload=false;
                        $scope.row.adsdistrict_id = "";
                        $scope.row.adsregion_id = "";
                        $scope.row.adsneighborhood_id = "";
                        $scope.row.adssquare_id = "";
                        $scope.row.adsmosques_id = "";
                    };

                    $scope.confirm = function (item) {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                        $scope.spinner=false;
                        $scope.import=false;

                        $scope.uploader.onBeforeUploadItem = function(item) {
                            var params = angular.copy($scope.row);
                            item.formData = [params];
                        };

                        $scope.uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);

                            if(response.download_token){
                                var file_type='xlsx';
                                 window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                            }

                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            else if(response.status=='success')
                            {
                                $scope.spinner=false;
                                $scope.import=false;
                                $rootScope.toastrMessages('success',response.msg);
                                $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                LoadCases($rootScope.aidsCasesfilters);
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $modalInstance.close();
                            }



                        };
                        $scope.uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});
        }else{
            $uibModal.open({
                templateUrl: 'import_.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'md',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;
                    $scope.row={};

                    if(target == 'banks'){
                        $scope.model_title = $filter('translate')('import banks');
                    }else if(target == 'essentials'){
                        $scope.model_title = $filter('translate')('import essentials');
                    }else if(target == 'fin-aids'){
                        $scope.model_title = $filter('translate')('import financial data');
                    }else if(target == 'non-fin-aids'){
                        $scope.model_title = $filter('translate')('import nonfinancial data');
                    }else if(target == 'properties'){
                        $scope.model_title = $filter('translate')('import properties');
                    }else if(target == 'family'){
                        $scope.model_title = $filter('translate')('import family member');
                    }else{
                        $scope.model_title = $filter('translate')('import cases');
                    }

                    var url = '/api/v1.0/common/aids/cases/import-' + target;

                    $scope.RestUpload=function(value){
                        $scope.upload=false;
                    };

                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };

                    $scope.download=function(){

                        var template ='';
                        if(target == 'banks'){
                            template = 'import-to-cases-banks-template';
                        }else if(target == 'essentials'){
                            template = 'import-to-cases-essentials-template';
                        }else if(target == 'fin-aids'){
                            template = 'import-to-cases-aids-template';
                        }else if(target == 'non-fin-aids'){
                            template = 'import-to-cases-aids-template';
                        }else if(target == 'properties'){
                            template = 'import-to-cases-properties-template';
                        }else if(target == 'family'){
                            template = 'aid-family-import-template';
                        }

                         window.location = '/api/v1.0/common/files/xlsx/export?download_token='+ template +'&template=true';
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: url,
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    Uploader.onBeforeUploadItem = function(item) {
                        item.formData = [{}];
                    };

                    $scope.confirm = function () {
                        $rootScope.progressbar_start();
                        // angular.element('.btn').addClass("disabled");

                        $scope.spinner=false;
                        $scope.import=false;

                        Uploader.onBeforeUploadItem = function(i) {
                            i.formData = [];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);
                            // angular.element('.btn').removeClass("disabled");
                            //  angular.element('.btn').removeAttr('disabled');

                            if(response.download_token){
                                 window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token;
                            }

                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $modalInstance.close();
                            }
                            else if(response.status=='success')
                            {
                                $scope.spinner=false;
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $scope.import=false;
                                $rootScope.toastrMessages('success',response.msg);
                                if(target =='family'){
                                    $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                    LoadCases($rootScope.aidsCasesfilters);
                                }
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                if(target =='family'){
                                    $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                    LoadCases($rootScope.aidsCasesfilters);
                                }
                                $modalInstance.close();
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});

        }

    };

    $scope.importFamily=function(){
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: 'importFamily.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'md',
            controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                $scope.upload=false;
                $scope.row={};

                $scope.RestUpload=function(value){
                    if(value ==2){
                        $scope.upload=false;
                    }
                };

                $scope.fileUpload=function(){
                    Uploader.destroy();
                    Uploader.queue=[];
                    $scope.upload=true;
                };
                $scope.download=function(){

                    $rootScope.progressbar_start();
                    $http({
                        url:'/api/v1.0/common/aids/cases/import-family-template',
                        method: "GET"
                    }).then(function (response) {
                        $rootScope.progressbar_complete();
                         window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token+'&template=true';
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                };

                var Uploader = $scope.uploader = new FileUploader({
                    url: '/api/v1.0/common/aids/cases/import-family',
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });
                Uploader.onBeforeUploadItem = function(item) {
                    item.formData = [{source: 'file'}];
                };

                $scope.confirm = function (item) {
                    $rootScope.clearToastr();
                    $rootScope.progressbar_start();

                    $scope.spinner=false;
                    $scope.import=false;

                    Uploader.onBeforeUploadItem = function(i) {
                        // i.formData = [{category_id: item.category_id}];
                    };
                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);

                        if(response.download_token){
                            var file_type='xlsx';
                             window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }

                        if(response.status=='error' || response.status=='failed')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                        }
                        else if(response.status=='success')
                        {
                            $scope.spinner=false;
                            $scope.import=false;
                            $rootScope.toastrMessages('success',response.msg);
                            $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                            LoadCases($rootScope.aidsCasesfilters);
                            $modalInstance.close();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);

                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                            $modalInstance.close();
                        }

                    };
                    Uploader.uploadAll();
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.remove=function(){
                    angular.element("input[type='file']").val(null);
                    $scope.uploader.clearQueue();
                    angular.forEach(
                        angular.element("input[type='file']"),
                        function(inputElem) {
                            angular.element(inputElem).val(null);
                        });
                };

            }});

    };

    $scope.import2 = function ($files) {

        $rootScope.clearToastr();
        var formData = new FormData();
        formData.append('file', $files[0]);
        angular.element("input[type='file']").val(null);
        $scope.ngFiles({$files: []})
        $scope.spinner=true;
        $scope.import=true;
        $rootScope.progressbar_start();
        $http({
            method:'POST',
            url: "/api/v1.0/common/aids/cases/import",
            data: formData,
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined,'Process-Data': false}
        }).then(function successCallback(response) {
            $rootScope.progressbar_complete();

            $scope.spinner=false;
            $scope.import=false;

            if(response.data.status=='success') {
                $scope.spinner=false;
                $scope.import=false;
                $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                LoadCases($rootScope.aidsCasesfilters);
                $rootScope.toastrMessages('success',response.data.msg);
            }else{
                $rootScope.toastrMessages('error',response.data.msg);
            }

        }, function errorCallback(error) {
            $rootScope.progressbar_complete();
            $scope.spinner=false;
            $scope.import=false;
            $rootScope.toastrMessages('error',error.data.msg);
        });
        $rootScope.$apply();
    };


    $scope.check = function(type){
        $rootScope.clearToastr();
        $rootScope.checkType=type;

        $uibModal.open({
            templateUrl: '/app/modules/common/views/reports/model/checkPerson.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'md',
            controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {
                $scope.upload=false;

                $scope.fileUpload=function(){
                    Uploader.destroy();
                    Uploader.queue=[];
                    $scope.upload=true;
                };
                $scope.download=function(){
                    // angular.element('.btn').addClass("disabled");
                    $rootScope.progressbar_start();

                    $rootScope.clearToastr();
                    $http({
                        url:'/api/v1.0/common/reports/template',
                        method: "GET"
                    }).then(function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                         window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token+'&template=true';
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                };

                var Uploader = $scope.uploader = new FileUploader({
                    url:'/api/v1.0/common/aids/cases/check',
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });

                $scope.confirm = function () {
                    $rootScope.clearToastr();
                    $scope.spinner=false;
                    $scope.import=false;

                    // angular.element('.btn').addClass("disabled");

                    $rootScope.progressbar_start();
                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.clearToastr();
                        item.formData = [{checkType:$rootScope.checkType}];
                    };

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);

                        if(response.status=='success')
                        {
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $scope.spinner=false;
                            $scope.import=false;
                            $rootScope.toastrMessages('success',response.msg);
                        }else{
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $rootScope.toastrMessages('error',response.msg);

                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                        }
                        if(response.download_token){
                            var file_type='xlsx';
                             window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }
                        $modalInstance.close();

                    };
                    Uploader.uploadAll();
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.remove=function(){
                    angular.element("input[type='file']").val(null);
                    $scope.uploader.clearQueue();
                    angular.forEach(
                        angular.element("input[type='file']"),
                        function(inputElem) {
                            angular.element(inputElem).val(null);
                        });
                };

            }});
    };


    // *************** DATE PIKER ***************
    $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0};
    $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $rootScope.format = $scope.formats[0];
    $rootScope.today = function() {$rootScope.dt = new Date();};
    $rootScope.today();
    $rootScope.clear = function() {$rootScope.dt = null;};

    $scope.open1 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        opened: false
    };
    $scope.open2 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        opened: false
    };
    $scope.open8 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup8.opened = true;
    };
    $scope.popup8 = {
        opened: false
    };
    $scope.open3 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup3.opened = true;
    };
    $scope.popup3 = {
        opened: false
    };
    $scope.open4 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup4.opened = true;
    };
    $scope.popup4 = {
        opened: false
    };
    $scope.open23 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup23.opened = true;
    };
    $scope.popup23 = {
        opened: false
    };
    $scope.open24 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup24.opened = true;
    };
    $scope.popup24 = {
        opened: false
    };
    $scope.open224 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup224.opened = true;
    };
    $scope.popup224 = {
        opened: false
    };
    $scope.open15 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup15.opened = true;
    };
    $scope.popup15 = {
        opened: false
    };
    $scope.open25 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup25.opened = true;
    };
    $scope.popup25 = {
        opened: false
    };

    $scope.open99 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup99.opened = true;
    };
    $scope.popup99 = {
        opened: false
    };
    $scope.open88 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup88.opened = true;
    };
    $scope.popup88 = {
        opened: false
    };


});
