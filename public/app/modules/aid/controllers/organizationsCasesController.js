
angular.module('AidModule').controller('organizationCasesController', function($filter,$stateParams,$rootScope, $scope,$uibModal, $http, $timeout,$state,setting,
                                                                    cs_persons_vouchers,org_sms_service,Org,Entity,custom_forms,vouchers_categories,cases) {


    $rootScope.labels = {
        "itemsSelected": $filter('translate')('itemsSelected'),
        "selectAll": $filter('translate')('selectAll'),
        "unselectAll": $filter('translate')('unselectAll'),
        "search": $filter('translate')('search'),
        "select": $filter('translate')('select')
    }
    var AuthUser =localStorage.getItem("charity_LoggedInUser");
    $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
    $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
    if($rootScope.charity_LoggedInUser.organization != null) {
        $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
    }
    $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;

    $rootScope.type=$scope.type=$stateParams.type = 'all';
    $rootScope.settings.layout.pageSidebarClosed = true;

    $state.current.data.pageTitle = $stateParams.type = $filter('translate')('all aid cases');

    $scope.infoCollapsed  = $scope.banksCollapsed     =
    $scope.worksCollapsed = $scope.contactsCollapsed  =
    $scope.residenceCollapsed = $scope.homeIndoorCollapsed =
    $scope.othersCollapsed= $scope.aidSourceCollapsed=
    $scope.propertiesCollapsed=$scope.vouchersCollapsed=true;
    $scope.items=[];

    $scope.master=false;

    $scope.PropertiesFilter=[];
    $scope.pro_id=[];
    $scope.AidSourceFilter=[];
    $scope.aid_id=[];
    $rootScope.person_restrict=false;
    $rootScope.card_restrict=false;
    $rootScope.restricted=[];
    $rootScope.invalid_cards=[];

    $rootScope.status ='';
    $rootScope.msg ='';
    $scope.toggleStatus = true;
    $rootScope.mode=$stateParams.mode;


    var resetFilter =function(){
       
        $rootScope.aidsCasesfilters={
            "Properties":[],
            "all_organization":false,
            "order_by_rank":"",
            "status":"",
            "date_from":"",
            "date_to":"",
            "first_name":"",
            "second_name":"",
            "third_name":"",
            "last_name":"",
            "en_first_name":"",
            "en_second_name":"",
            "en_third_name":"",
            "en_last_name":"",
            "category_id":"",
            "id_card_number":"",
            "gender":"",
            "marital_status_id":"",
            "birthday_from":"",
            "birthday_to":"",
            "birth_place":"",
            "nationality":"",
            "refugee":"",
            "unrwa_card_number":"",
            "adscountry_id":"",
            "adsdistrict_id":"",
            "adsregion_id":"",
            "adsneighborhood_id":"",
            "adssquare_id":"",
            "adsmosques_id":"",
            "person_country":"",
            "person_governarate":"",
            "person_city":"",
            "location_id":"",
            "mosques_id":"",
            "street_address":"",
            "monthly_income_from":"",
            "monthly_income_to":"",
            "primary_mobile":"",
            "secondery_mobile":"",
            "phone":"",
            "bank_id":"",
            "branch_name":"",
            "account_number":"",
            "account_owner":"",
            "property_type_id":"",
            "roof_material_id":"",
            "residence_condition":"",
            "indoor_condition":"",
            "min_rooms":"",
            "max_rooms":"",
            "min_area":"",
            "max_area":"",
            "habitable":"",
            "house_condition":"",
            "rent_value":"",
            "working":"",
            "can_work":"",
            "work_reason_id":"",
            "work_job_id":"",
            "work_status_id":"",
            "work_wage_id":"",
            "work_location":"",
            "promised":"",
            "visitor":"",
            "visited_at_from":"",
            "visited_at_to":"",
            "min_family_count":"",
            "max_family_count":"",
            "min_total_vouchers":"",
            "max_total_vouchers":"",
            'organization_id':"",
            'got_voucher':"",
            'voucher_organization':"",
            'voucher_sponsors':"",
            'voucher_to':"",
            'voucher_from':"",
            "voucher_ids":""
        };

        $scope.PropertiesFilter=[];
        $scope.pro_id=[];
        $scope.AidSourceFilter=[];
        $scope.aid_id=[];

        if($stateParams.id){
            $rootScope.aidsCasesfilters.sponsor_id=$stateParams.id;
            $scope.sponsor=true;
        }else{
            $scope.sponsor=false;
        }

    };

    var LoadCases =function(params){

        params.category_type=2;
        params.deleted=$rootScope.deleted;

        if(angular.isUndefined(params.action)){
            params.action ='filter';
        }
        if(params.action && angular.isUndefined(params.page) ){
            params.page =1;
        }
        if( !angular.isUndefined(params.all_organization)) {
            if(params.all_organization == true ||params.all_organization == 'true'){
                var orgs=[];
                angular.forEach(params.organization_id, function(v, k) {
                    orgs.push(v.id);
                });

                params.organization_ids=orgs;

            }else if(params.all_organization == false ||params.all_organization == 'false') {
                params.organization_ids=[];
                $scope.master=false
            }
        }

        params.organizationsCases = true;
        $rootScope.progressbar_start();

        $http({
            url: "/api/v1.0/common/aids/cases/filterCases",
            method: "POST",
            data: params
        })
            .then(function (response) {
                $rootScope.progressbar_complete();

                if(params.action != 'filter'){

                    if(response.data.status == 'false'){
                        $rootScope.toastrMessages('error',$filter('translate')('there is no cases to export'));
                    }else if(params.action == 'FamilyMember' && response.data.status == false){
                        $rootScope.toastrMessages('error',$filter('translate')('there is no family member to export'));
                    }else{
                            var file_type='zip';
                            if(params.action == 'ExportToExcel' || params.action == 'FamilyMember') {
                                file_type='xlsx';
                            }
                              window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                    }
                }
                else{
                    if(response.data.Cases.total != 0){
                        $rootScope.UserOrgId=response.data.organization_id;
                        $scope.organization_id=response.data.organization_id;

                        var temp= response.data.Cases.data;
                        

                        $scope.items= temp;
                        $scope.CurrentPage = response.data.Cases.current_page;
                        $scope.TotalItems = response.data.Cases.total;
                        $scope.ItemsPerPage = response.data.Cases.per_page;
                    }else{
                        $scope.items=[];
                    }
                }

        }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });

    };

    $rootScope.aidCategories_=[];
    angular.forEach($rootScope.aidCategories, function(v, k) {
        if ($rootScope.lang == 'ar'){
            $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
        }
        else{
            $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
        }
    });

    resetFilter();

    if($rootScope.type =='all'){

        Entity.get({entity:'entities',c:'transferCompany,propertyTypes,roofMaterials,buildingStatus,furnitureStatus,houseStatus,habitableStatus,currencies,aidCountries,countries,maritalStatus,workReasons,workStatus,workWages,workJobs,banks,aidSources,properties,essentials'},function (response) {
            $rootScope.transferCompany = response.transferCompany;
            $rootScope.currency = response.currencies;
            $scope.PropertyTypes = response.propertyTypes;
            $scope.RoofMaterials = response.roofMaterials;
            $scope.buildingStatus = response.buildingStatus;
            $scope.furnitureStatus = response.furnitureStatus;
            $scope.houseStatus = response.houseStatus;
            $scope.habitableStatus = response.habitableStatus;
            $scope.Country = response.countries;
            $scope.aidCountries = response.aidCountries;
            $scope.MaritalStatus = response.maritalStatus;
            $scope.WorkReasons = response.workReasons;
            $scope.WorkStatus = response.workStatus;
            $scope.WorkWages = response.workWages;
            $scope.WorkJobs = response.workJobs;
            $scope.Banks = response.banks;
            $scope.Properties = response.properties;
            $scope.Aidsource = response.aidSources;
            $rootScope.aidsCasesfilters.Essential =  $scope.Essentials = response.essentials;
        });

        Org.list({type:'sponsors'},function (response) { $rootScope.Sponsor = response; });

    }

    $scope.sendSms=function(type,receipt_id) {

        if(type == 'manual'){
            var receipt = [];
            var pass = true;

            if(receipt_id ==0){
                angular.forEach($scope.items, function (v, k) {
                    if (v.check) {
                        receipt.push(v.person_id);
                    }
                });
                if(receipt.length==0){
                    pass = false;
                    $rootScope.toastrMessages('error',$filter('translate')("you did not select anyone to send them sms message"));
                    return;
                }
            }else{
                receipt.push(receipt_id);
            }

            if(pass){
                org_sms_service.getList(function (response) {
                    $rootScope.Providers = response.Service;
                });
                $uibModal.open({
                    templateUrl: '/app/modules/sms/views/modal/sent_sms_from_person_general.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance) {
                        $scope.sms={source: 'check' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:"",cont_type:""};
                        $scope.confirm = function (data) {
                            $rootScope.clearToastr();
                            var params = angular.copy(data);
                            params.receipt=receipt;
                            params.target='person';
                            $rootScope.progressbar_start();

                            org_sms_service.sendSmsToTarget(params,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed') {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status== 'success'){
                                    if(response.download_token){
                                        var file_type='xlsx';
                                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                    }

                                     $modalInstance.close();
                                    $rootScope.toastrMessages('success',response.msg);
                                }
                            }, function(error) {
                                $rootScope.toastrMessages('error',error.data.msg);
                            });
                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    },
                    resolve: {
                    }
                });

            }
        }
        else{
            org_sms_service.getList(function (response) {
                $rootScope.Providers = response.Service;
            });
            $uibModal.open({
                    templateUrl: '/app/modules/sms/views/modal/sent_sms_from_xlx_general.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                        $scope.upload=false;
                        $scope.sms={source: 'file' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:"",cont_type:""};

                        $scope.RestUpload=function(value){
                            if(value ==2){
                                $scope.upload=false;
                            }
                        };

                        $scope.fileUpload=function(){
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.upload=true;
                        };
                        $scope.download=function(){
                            window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-to-send-sms-template&template=true';
                        };

                        var Uploader = $scope.uploader = new FileUploader({
                            url: 'api/v1.0/sms/excel',
                            removeAfterUpload: false,
                            queueLimit: 1,
                            headers: {
                                Authorization: OAuthToken.getAuthorizationHeader()
                            }
                        });


                        $scope.confirm = function (item) {
                            $scope.spinner=false;
                            $scope.import=false;
                            $rootScope.progressbar_start();
                            Uploader.onBeforeUploadItem = function(item) {

                                if($scope.sms.same_msg == 0){
                                    $scope.sms.sms_messege = '';
                                }
                                item.formData = [$scope.sms];
                            };
                            Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                                $rootScope.progressbar_complete();
                                $scope.uploader.destroy();
                                $scope.uploader.queue=[];
                                $scope.uploader.clearQueue();
                                angular.element("input[type='file']").val(null);

                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                    $scope.upload=false;
                                    angular.element("input[type='file']").val(null);
                                }
                                else if(response.status=='success')
                                {
                                    if(response.download_token){
                                        var file_type='xlsx';
                                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                    } 
                                    $scope.spinner=false;
                                    $scope.upload=false;
                                    angular.element("input[type='file']").val(null);
                                    $scope.import=false;
                                    $rootScope.toastrMessages('success',response.msg);
                                    $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                     $modalInstance.close();
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                    $scope.upload=false;
                                    angular.element("input[type='file']").val(null);
                                    $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                }

                            };
                            Uploader.uploadAll();
                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                        $scope.remove=function(){
                            angular.element("input[type='file']").val(null);
                            $scope.uploader.clearQueue();
                            angular.forEach(
                                angular.element("input[type='file']"),
                                function(inputElem) {
                                    angular.element(inputElem).val(null);
                                });
                        };

                    }});

            }
    };

    Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

    $scope.Search=function(selected,data,action) {
        $rootScope.clearToastr();
        data.Properties=$scope.PropertiesFilter;
        data.AidSources =$scope.AidSourceFilter;
        data.action=action;
        data.persons=[];

        if(action != 'ExportToExcel' && action != 'ExportToWord' && action != 'FamilyMember'&& action != 'FamilyMember'){
            data.page=$scope.CurrentPage;
        }
        
        if(selected == true || selected == 'true' ){
           var persons=[];
            angular.forEach($scope.items, function(v, k){                
                if(v.check){
                    persons.push(v.case_id);
                }
            });
            
            if(persons.length==0){
                $rootScope.toastrMessages('error',$filter('translate')('You have not select people to export'));
                return ;
            }
            data.persons=persons;
        }
        
        LoadCases(data);

    };


    $scope.sortKeyArr=[];
    $scope.sortKeyArr_rev=[];
    $scope.sortKeyArr_un_rev=[];
    $scope.sort_ = function(keyname){
        $scope.sortKey_ = keyname;
        var reverse_ = false;
        if (
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
        ) {
            reverse_ = true;
        }
        var data = angular.copy($rootScope.aidsCasesfilters);
        data.action ='filter';

        if (reverse_) {
            if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_rev.push(keyname);
            }
        }
        else {
            if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_un_rev.push(keyname);
            }

        }

        data.sortKeyArr_rev = $scope.sortKeyArr_rev;
        data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;
        $scope.Search(data,'filter');
    };

    $scope.get = function(target,value,parant){
        $rootScope.clearToastr();

        if (!angular.isUndefined(value)) {
            if(value != null && value != "" && value != " " ) {
                Entity.listChildren({entity:target,'id':value,'parent':parant},function (response) {

                    if (target == 'sdistricts') {
                        $scope.governarate = response;
                        $scope.regions = [];
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];

                        $rootScope.aidsCasesfilters.adsdistrict_id = "";
                        $rootScope.aidsCasesfilters.adsregion_id = "";
                        $rootScope.aidsCasesfilters.adsneighborhood_id = "";
                        $rootScope.aidsCasesfilters.adssquare_id = "";
                        $rootScope.aidsCasesfilters.adsmosques_id = "";

                    }
                    else if (target == 'sregions') {
                        $scope.regions = response;
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];
                        $rootScope.aidsCasesfilters.adsregion_id = "";
                        $rootScope.aidsCasesfilters.adsneighborhood_id = "";
                        $rootScope.aidsCasesfilters.adssquare_id = "";
                        $rootScope.aidsCasesfilters.adsmosques_id = "";
                    }
                    else if (target == 'sneighborhoods') {
                        $scope.nearlocation = response;
                        $scope.squares = [];
                        $scope.mosques = [];
                        $rootScope.aidsCasesfilters.adsneighborhood_id = "";
                        $rootScope.aidsCasesfilters.adssquare_id = "";
                        $rootScope.aidsCasesfilters.adsmosques_id = "";

                    }
                    else if (target == 'ssquares') {
                        $scope.squares = response;
                        $scope.mosques = [];
                        $rootScope.aidsCasesfilters.adssquare_id = "";
                        $rootScope.aidsCasesfilters.adsmosques_id = "";
                    }
                    else if (target == 'smosques') {
                        $scope.mosques = response;
                        $rootScope.aidsCasesfilters.adsmosques_id = "";
                    }else if(target =='branches'){
                        $scope.Branches = response;
                        $rootScope.aidsCasesfilters.branch_name="";
                    }

                });
            }
        }
    };

    $scope.pageChanged = function (currentPage) {
        $rootScope.clearToastr();
        $rootScope.aidsCasesfilters.page=currentPage;
        LoadCases($rootScope.aidsCasesfilters);
    };

    $scope.itemsPerPage_ = function (itemsCount) {
        $rootScope.clearToastr();
        $rootScope.aidsCasesfilters.itemsCount=itemsCount;
        $rootScope.aidsCasesfilters.page=1;
        LoadCases($rootScope.aidsCasesfilters);
    };

    $scope.download = function (target,id) {

        $rootScope.clearToastr();
        $rootScope.clearToastr();
        var url ='';
        var file_type ='zip';
        var  params={};
        var method='';

        if(target =='word'){
            method='POST';
            params={'case_id':id,
                'target':'info',
                'action':'filters',
                'mode':'show',
                'category_type' : 2,
                'person' :true,
                'visitor_note' :true,
                'persons_i18n' : true,
                'category' : true,
                'contacts' : true,
                'work' : true,
                'banks' : true,
                'health' : true,
                'default_bank' : true,
                'residence' : true,
                'case_needs' : true,
                'reconstructions' : true,
                'home_indoor' : true,
                'financial_aid_source' : true,
                'non_financial_aid_source' : true,
                'persons_properties' : true,
                'persons_documents' : true,
                'family_member' : true
            };
            url = "/api/v1.0/common/aids/cases/word";
        }
        else if(target =='pdf'){
            method='POST';
            params={'case_id':id,
                'target':'info',
                'action':'filters',
                'mode':'show',
                'category_type' : 2,
                'person' :true,
                'visitor_note' :true,
                'persons_i18n' : true,
                'category' : true,
                'contacts' : true,
                'work' : true,
                'banks' : true,
                'default_bank' : true,
                'residence' : true,
                'case_needs' : true,
                'reconstructions' : true,
                'home_indoor' : true,
                'financial_aid_source' : true,
                'non_financial_aid_source' : true,
                'persons_properties' : true,
                'health' : true,
                'persons_documents' : true,
                'family_member' : true
            };
            url = "/api/v1.0/common/aids/cases/pdf";
        }else{
            method='GET';
            url = "/api/v1.0/common/aids/cases/exportCaseDocument/"+id;
        }

        $rootScope.progressbar_start();
        $http({
            url:url,
            method: method,
            data:params
        }).then(function (response) {
            $rootScope.progressbar_complete();
            if(target =='archive' && response.data.status == false){
                $rootScope.toastrMessages('error',$filter('translate')('no attachment to export'));
            }else{
                  window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
            }
        }, function(error) {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('error',error.data.msg);
        });
    };

    $scope.toggleSearchform = function() {

        $scope.toggleStatus = $scope.toggleStatus == false ? true: false;
        if($scope.toggleStatus == true) {
            $rootScope.aidsCasesfilters.page=1;
            $rootScope.aidsCasesfilters.action='filter';
        }
        resetFilter();

    };

    $scope.allCheckbox=function(value){
        if (value) {
            $scope.selectedAll = true;
        } else {
            $scope.selectedAll = false;
        }

        angular.forEach($scope.items, function(v, k) {
            v.check=$scope.selectedAll;
        });
    };

    $scope.selectCustomForm = function (size) {
        $rootScope.clearToastr();

        custom_forms.getCustomFormsList(function (response) {
            $rootScope.Forms = response;
        });

        $rootScope.formdata={};
        setting.getCustomForm({'id':'aid_custom_form'},function (response) {
            $rootScope.has_custom_form=response.status;
            if(response.status){
                $rootScope.formdata.form_id=response.Setting.value +"";
            }
        });


        $uibModal.open({
            templateUrl: 'myModalContent1.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: size,
            controller: function ($rootScope,$scope, $modalInstance, $log,setting) {

                if( $rootScope.person_restrict == true){
                    $rootScope.person_restrict =false;
                }

                $scope.confirm = function (form) {
                    $rootScope.clearToastr();
                    $rootScope.progressbar_start();
                    form.setting_id='aid_custom_form';
                    setting.setCustomForm(form,function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                 $modalInstance.close();
                                if(response.status == 'success')
                                {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }

                        }, function(error) {
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);
                         $modalInstance.close();
                        });
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            },
            resolve: {

            }
        });
    };

    $scope.delete = function (size,id) {
        $rootScope.clearToastr();

        $uibModal.open({
            templateUrl: 'myModalContent2.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: size,
            controller: function ($rootScope,$scope, $modalInstance, $log) {
                if( $rootScope.person_restrict == true){
                    $rootScope.person_restrict =false;
                }

                $scope.data={};
                $scope.confirmDelete = function (data) {
                    $rootScope.clearToastr();
                    data.id=id;
                    data.type='aids';
                    $rootScope.progressbar_start();
                    cases.delete(data,function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status== 'success'){
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                             $modalInstance.close();
                            $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                            LoadCases($rootScope.aidsCasesfilters);
                        }

                    }, function(error) {
                        $rootScope.progressbar_complete();
                        $modalInstance.close();
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    };

    $scope.goTo=function(action,person,case_id,category_id){
        $rootScope.clearToastr();
        angular.forEach($rootScope.aidCategories, function(v, k) {
            if(v.id ==category_id){
                $state.go('caseform.'+v.FirstStep,{"mode":action,"id" :case_id,"category_id" :category_id});
            }
        });

    };
    $scope.resetFilters=function(){
        resetFilter();
    }

    // *************** DATE PIKER ***************
    $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0};
    $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $rootScope.format = $scope.formats[0];
    $rootScope.today = function() {$rootScope.dt = new Date();};
    $rootScope.today();
    $rootScope.clear = function() {$rootScope.dt = null;};

    $scope.open1 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup1.opened = true;
    };
    $scope.popup1 = {
        opened: false
    };
    $scope.open2 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup2.opened = true;
    };
    $scope.popup2 = {
        opened: false
    };
    $scope.open8 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup8.opened = true;
    };
    $scope.popup8 = {
        opened: false
    };
    $scope.open3 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup3.opened = true;
    };
    $scope.popup3 = {
        opened: false
    };
    $scope.open4 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup4.opened = true;
    };
    $scope.popup4 = {
        opened: false
    };
    $scope.open23 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup23.opened = true;
    };
    $scope.popup23 = {
        opened: false
    };
    $scope.open24 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup24.opened = true;
    };
    $scope.popup24 = {
        opened: false
    };
    $scope.open224 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup224.opened = true;
    };
    $scope.popup224 = {
        opened: false
    };
    $scope.open15 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup15.opened = true;
    };
    $scope.popup15 = {
        opened: false
    };
    $scope.open25 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup25.opened = true;
    };
    $scope.popup25 = {
        opened: false
    };

    $scope.open99 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup99.opened = true;
    };
    $scope.popup99 = {
        opened: false
    };
    $scope.open88 = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.popup88.opened = true;
    };
    $scope.popup88 = {
        opened: false
    };

});
