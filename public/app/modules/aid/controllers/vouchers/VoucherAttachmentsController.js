
angular.module('AidModule')
    .controller('VoucherAttachmentsController', function( $filter,$stateParams,$rootScope, $scope,$ngBootbox,$uibModal, $http, FileUploader, OAuthToken,$timeout,$state,cs_persons_vouchers) {

        $state.current.data.pageTitle = $filter('translate')('voucher attachments');

        if($stateParams.id==null){
            $rootScope.toastrMessages('error',$filter('translate')('Your are request invalid link , you will be redirected to the vouchers page to search again and request the valid link'));
            $timeout(function() {
                $state.go('allVouchers');
            }, 2000);

        }else{


            $rootScope.failed =false;
            ;
            $rootScope.model_error_status =false;
            $rootScope.status ='';
            $rootScope.msg ='';
            $rootScope.row={attachments:[]};
            $rootScope.voucher_id= $stateParams.id;

            var LoadVoucherAttachments =function(){
                $rootScope.progressbar_start();
                cs_persons_vouchers.attachments({id:$stateParams.id},function (response) {
                    $rootScope.progressbar_complete();
                    $rootScope.row= response.row;
                });
            };

            $scope.close=function(){
                $scope.success =false;
                $scope.failed =false;
                $scope.error =false;
                $scope.model_error_status =false;
            };
            $scope.download=function(){
                $rootScope.progressbar_start();
                $http({
                    url: '/api/v1.0/aids/cs_persons_vouchers/export/'+$stateParams.id,
                    method: "GET"
                }).then(function (response) {
                    $rootScope.progressbar_complete();
                    window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token;
                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });
            };
            $scope.select=function () {

                $uibModal.open({
                    templateUrl: 'myModalContent2.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                        var Uploader = $scope.uploader = new FileUploader({
                            url: '/api/v1.0/doc/files',
                            headers: {
                                Authorization: OAuthToken.getAuthorizationHeader()
                            }
                        });
                        Uploader.onBeforeUploadItem = function(item) {
                            $rootScope.clearToastr();
                            item.formData = [{action:"not_check"}];
                            $rootScope.progressbar_start();
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);
                            fileItem.id = response.id;
                            var count=0;
                            cs_persons_vouchers.upload({voucher_id:$stateParams.id, document_id:fileItem.id},function (response) {
                                    if(response.status== 'success'){
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        LoadVoucherAttachments();
                                        $modalInstance.close();
                                    }
                                }, function(error) {
                                $rootScope.toastrMessages('error',error.data.msg);
                            });

                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                        $scope.closeuploader = function () {
                            $modalInstance.dismiss('cancel');
                            LoadDocuments();

                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    }
                });

            };
            $scope.delete=function (id,index) {
                $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                    .then(function() {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                        cs_persons_vouchers.delete({id:id,parant:$stateParams.id},function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status== 'success'){
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            LoadVoucherAttachments();

                        });
                    });



                };

            LoadVoucherAttachments();
        }

    });
