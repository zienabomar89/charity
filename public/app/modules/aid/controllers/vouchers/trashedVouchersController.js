

angular.module('AidModule').controller('trashedVouchersController', function($location,$filter,$stateParams,$rootScope, $scope,$uibModal, $http, $timeout,$state,Entity,setting,Org,FileUploader,$ngBootbox, OAuthToken,vouchers_categories,cs_persons_vouchers,cases,org_sms_service,category) {

    $rootScope.clearToastr();

    $state.current.data.pageTitle = $filter('translate')('vouchers');
    $rootScope.settings.layout.pageSidebarClosed = true;

    var AuthUser = localStorage.getItem("charity_LoggedInUser");
    $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
    $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
    $rootScope.UserType = $rootScope.charity_LoggedInUser.type
    if ($rootScope.charity_LoggedInUser.organization != null) {
        $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
    }
    $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;

    $scope.master=false;
    $rootScope.status ='';
    $scope.CurrentPage = 1;
    $rootScope.msg ='';
    $rootScope.default_template=null;
    $rootScope.has_default_template=false;
    $scope.sortKeyArr=[];
    $scope.sortKeyArr_rev=[];
    $scope.sortKeyArr_un_rev=[];

    $rootScope.filter={};
    $scope.items=[];
    $scope.toggleStatus = true;

    Org.list({type:'sponsors'},function (response) { $rootScope.Sponsors = response; });

    Entity.get({entity:'entities',c:'currencies,transferCompany,organizationsCat'},function (response) {
        $rootScope.currency = response.currencies;
        $rootScope.transferCompany = response.transferCompany;
        $rootScope.organizationsCat = response.organizationsCat;
    });

    $rootScope.aidCategories_=[];
    category.getSectionForEachCategory({'type':'aids'},function (response) {
        if(!angular.isUndefined(response)){
            $rootScope.aidCategories_firstStep_map =[];
            $rootScope.aidCategories = response.Categories;
            if($rootScope.aidCategories.length > 0){
                for (var i in $rootScope.aidCategories) {
                    var item = $rootScope.aidCategories[i];
                    $rootScope.aidCategories_firstStep_map[item.id] = item.FirstStep;
                }
                angular.forEach($rootScope.aidCategories, function(v, k) {
                    if ($rootScope.lang == 'ar'){
                        $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
                    }
                    else{
                        $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
                    }
                });
            }
        }
    });
    var resetVouchersFilter =function(){
        $rootScope.filter={
            "page":1,
            "all_organization":false,
            "organization_category":[],
            "organization_project":"",
            "serial":"",
            "type":"",
            "voucher_source":"",
            "category_id":"",
            "case_category_id":"",
            "sponsor_id":"",
            "title":"",
            "content":"",
            "notes":"",
            "min_value":"",
            "max_value":"",
            "min_count":"",
            "transfer":"",
            "allow_day":"",
            "transfer_company_id":null,
            "max_count":"",
            "date_to":"",
            "date_from":"",
            "voucher_date_to":"",
            "voucher_date_from":""
        };

        if($stateParams.id){
            $rootScope.filter.sponsor_id=$stateParams.id;
            $scope.sponsor=true;
        }else{
            $scope.sponsor=false;
        }

    };

    var LoadVouchersList =function($param){
        // $rootScope.clearToastr();
        $param.page =  $scope.CurrentPage ;

        if( !angular.isUndefined($param.transfer)) {
            if($param.transfer == 0 ||$param.transfer == '0'){
                $param.transfer_company_id = null
            }
        }
        $rootScope.progressbar_start();
        // // angular.element('.btn').addClass("disabled");
        $rootScope.progressbar_start();
        cs_persons_vouchers.trashed($param,function (response) {
            $rootScope.progressbar_complete();
            // angular.element('.btn').removeClass("disabled");
            if($param.action == 'ExportToExcel' || $param.action == 'ExportToWord' || $param.action == 'ExportToPDF'){

                if(response.status == 'false' || response.status == false){
                    $rootScope.toastrMessages('error',$filter('translate')('no beneficiaries to export coupons'));
                }else{
                    var file_type='xlsx';
                    if($param.action == 'ExportToWord' || $param.action == 'ExportToPDF') {
                        file_type='zip';
                    }
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }

            }else{
                if(response.Vouchers.data.total != 0){

                    if( !angular.isUndefined($param.all_organization)) {
                        if($param.all_organization == true ||$param.all_organization == 'true'){
                            $scope.master=true
                        }else if($param.all_organization == false ||$param.all_organization == 'false') {
                            $scope.master=false
                        }
                    }

                    // $scope.master=response.master;
                    var temp =response.Vouchers.data;

                    angular.forEach(temp, function (v, k) {
                        if (v.Beneficiary == 0) {
                            v.linkEnabled=false;
                        }else{
                            v.linkEnabled=true;
                        }
                    });
                    $scope.items =temp;
                    $scope.CurrentPage = response.Vouchers.current_page;
                    if($param.page == 1){
                        $rootScope.total_value = response.total;
                    }
                    $rootScope.TotalItems = response.Vouchers.total;
                    $rootScope.ItemsPerPage = response.Vouchers.per_page;
                }else{
                    $scope.items= [];
                    $rootScope.total_value= 0;
                }
            }
        });

    };
    $rootScope.labels = {
        "itemsSelected": $filter('translate')('itemsSelected'),
        "selectAll": $filter('translate')('selectAll'), "unselectAll": $filter('translate')('unselectAll'),
        "search": $filter('translate')('search'), "select": $filter('translate')('select')
    }
    $scope.selectedAll = false;
    $scope.selectAll=function(value){
        if (value) {
            $scope.selectedAll = true;
        } else {
            $scope.selectedAll = false;
        }

        angular.forEach($scope.items, function(v, k) {
            v.check=$scope.selectedAll;
        });
    };
    var RestTemplate= function () {
        $rootScope.clearToastr();
        setting.getSettingById({'id':'default-voucher-template'},function (response) {
            $rootScope.has_default_template=response.status;
            if(response.status){
                $rootScope.default_template=response.Setting.value +"";
            }
        });
    };

    var resetTable = function () {

        var data = angular.copy($rootScope.filter);
        data.action ='filter';
        data.page =$scope.CurrentPage;
        data.itemsCount =$scope.itemsCount;
        data.organization_ids=[];
        data.organization_category=[];

        if( !angular.isUndefined(data.all_organization)) {
            if(data.all_organization == true ||data.all_organization == 'true'){

                angular.forEach(data.organization_category_id, function(v, k) {
                    data.organization_category.push(v.id);
                });

                angular.forEach(data.organization_id, function(v, k) {
                    data.organization_ids.push(v.id);
                });
                delete data.organization_id;
                $scope.master=true
            }else if(data.all_organization == false ||data.all_organization == 'false') {
                data.organization_ids=[];
                $scope.master=false
            }

        }
        LoadVouchersList(data);
    };

    resetVouchersFilter();
    RestTemplate();
    // resetTable();

    $scope.disblay_orgs =false;
    $scope.resetOrg=function(all_organization,organization_category){
        $rootScope.clearToastr();
        $scope.disblay_orgs =false;
        let ids = [];
        if( !angular.isUndefined(all_organization)) {
            if(all_organization == true ||all_organization == 'true'){
                angular.forEach(organization_category, function(v, k) {
                    ids.push(v.id);
                });

                Org.ListOfCategory({ids:ids},function (response) {
                    if (response.list.length > 0){
                        $scope.Org = response.list;
                        $scope.disblay_orgs =true;
                    }else{
                        $scope.Org = [];
                        $scope.disblay_orgs =false;
                        $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');
                    }
                });
            }else{
                $scope.Org = [];
                $scope.disblay_orgs =false;
                $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');
            }
        }else{
            $scope.Org = [];
            $scope.disblay_orgs =false;
            $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');

        }


    };

    $rootScope.Choices = vouchers_categories.List();

    $scope.resetVouchersFilter_=function(){
        resetVouchersFilter();
    }
    $scope.export=function(action,id,target){
        $rootScope.clearToastr();
        var url='';
        var type='';

        if(action =='excel'){
            url='/api/v1.0/aids/cs_persons_vouchers/export/'+id+'?target='+target;
            type='xlsx';
        }else if (action == 'word'){
            url= "/api/v1.0/aids/cs_persons_vouchers/exportToWord/-1/"+id;
            type='docx';
        } else if (action =='pdf'){
            url= "/api/v1.0/aids/cs_persons_vouchers/exportToPDF/-1/"+id;
            type='pdf';

        }

        $rootScope.progressbar_start();
        $http({
            url: url,
            method: "GET"
        }).then(function (response) {
            $rootScope.progressbar_complete();
            window.location = '/api/v1.0/common/files/'+type+'/export?download_token='+response.data.download_token;
        }, function(error) {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('error',error.data.msg);
        });

    };
    $rootScope.download=function(action,id){

        $rootScope.clearToastr();
        var url='';
        var type='docx';

        if(action =='voucher_template'){
            url='/api/v1.0/aids/cs_persons_vouchers/template/'+id;
        }else if(action =='default_template'){
            url='/api/v1.0/aids/cs_persons_vouchers/default-template/-1';
        }else if(action =='import_vouchers'){
            url='/api/v1.0/aids/cs_persons_vouchers/default-import-template';
            type='xlsx';
        }else if(action =='template_instruction'){
            url='/api/v1.0/aids/cs_persons_vouchers/templateInstruction';
            type='pdf';
        }else if(action =='system_template'){
            url='/api/v1.0/aids/cs_persons_vouchers/sys-default-template';
        }

        $rootScope.progressbar_start();
        $http({
            url:url,
            method: "GET"
        }).then(function (response) {
            $rootScope.progressbar_complete();
            window.location = '/api/v1.0/common/files/'+type+'/export?download_token='+response.data.download_token+'&template=true';
        }, function(error) {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('error',error.data.msg);
        });
    };
    $rootScope.pageChanged = function (CurrentPage) {
        $scope.CurrentPage = CurrentPage;
        resetTable();
    };

    $rootScope.itemsPerPage_ = function (itemsCount) {
        $scope.itemsCount = itemsCount;
        $scope.CurrentPage = 1;
        resetTable();
    };

    $scope.Search=function(selected , params,action) {

        $rootScope.clearToastr();
        var data = angular.copy(params);
        data.action = action;
        data.items=[];

        if(selected == true || selected == 'true' ){

            var items =[];
            angular.forEach($scope.items, function(v, k){

                if(action == 'ExportToWord' || action == 'ExportToPDF'){
                    if(v.check && v.beneficiary > 0){
                        items.push(v.id);
                    }
                }else{
                    if(v.check){
                        items.push(v.id);
                    }
                }


            });

            if(items.length==0){
                $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                return ;
            }
            data.items=items;
        }

        data.organization_ids = [];
        data.organization_category = [];

        if( !angular.isUndefined(data.all_organization)) {
            if(data.all_organization == true ||data.all_organization == 'true'){

                angular.forEach(data.organization_category_id, function(v, k) {
                    data.organization_category.push(v.id);
                });
                angular.forEach(data.organization_id, function(v, k) {
                    data.organization_ids.push(v.id);
                });
                delete data.organization_id;
                $scope.master=true
            }else if(data.all_organization == false ||data.all_organization == 'false') {
                data.organization_ids=[];
                data.organization_category=[];
                $scope.master=false
            }

        }

        if( !angular.isUndefined(data.date_to)) {
            data.date_to=$filter('date')(data.date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.date_from)) {
            data.date_from= $filter('date')(data.date_from, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(data.voucher_date_to)) {
            data.voucher_date_to=$filter('date')(data.voucher_date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.voucher_date_from)) {
            data.voucher_date_from= $filter('date')(data.voucher_date_from, 'dd-MM-yyyy')
        }
        data.action=action;

        if( !angular.isUndefined(data.transfer)) {
            if(data.transfer == 0 ||data.transfer == '0'){
                data.transfer_company_id = null
            }
        }

        data.page=1;
        LoadVouchersList(data);

    };

    $scope.sort_ = function(keyname){
        $scope.sortKey_ = keyname;
        var reverse_ = false;
        if (
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
        ) {
            reverse_ = true;
        }
        var data = angular.copy($rootScope.filter);
        data.action ='filter';

        if (reverse_) {
            if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_rev.push(keyname);
            }
        }
        else {
            if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_un_rev.push(keyname);
            }

        }

        data.sortKeyArr_rev = $scope.sortKeyArr_rev;
        data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;
        $scope.Search(false,data,'filter');
    };

    $scope.toggle = function() {
        $rootScope.clearToastr();
        $scope.toggleStatus = $scope.toggleStatus == false ? true: false;
        resetVouchersFilter();
        if($scope.toggleStatus == true) {
            $rootScope.filter.page =1;
            // resetTable();
        }
    };

    $scope.update_un_serial=function(){
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: 'update_un_serial.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance, $log) {

                $scope.row={old: '' ,new_un:"",voucher_ids:[],vouchers:[]};
                $scope.display_vouchers = false;
                $scope.vouchers_list = [];

                $scope.resetList=function(){
                    $rootScope.clearToastr();
                    $scope.display_vouchers =false;
                    if( !angular.isUndefined($scope.row.old) ) {
                        if( !( $scope.row.old == '' || $scope.row.old == ' ') ) {
                            cs_persons_vouchers.getVouchers({id:$scope.row.old},function (response) {
                                if (response.list.length > 0){
                                    $scope.vouchers_list = response.list;
                                    $scope.display_vouchers =true;
                                }else{
                                    $scope.display_vouchers = false;
                                    $scope.vouchers_list = [];
                                    $rootScope.toastrMessages('error','لا يوجد قسائم تمتلك هذا الكود الموحد و تابعة لجمعيتك');
                                }
                            });

                        }
                    }
                    else{
                        $scope.display_vouchers = false;
                        $scope.vouchers_list = [];
                        $rootScope.toastrMessages('error','لا يوجد قسائم تمتلك هذا الكود الموحد و تابعة لجمعيتك');
                    }

                };

                $scope.confirm = function (data) {
                    $rootScope.clearToastr();
                    var params = angular.copy(data);

                    let vouchers=[];

                    angular.forEach(params.voucher_ids, function(v, k) {
                        vouchers.push(v.id);
                    });

                    $rootScope.progressbar_start();
                    cs_persons_vouchers.updateSerial({old: params.old ,new_un: params.new_un , vouchers:vouchers},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='error' || response.status=='failed') {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else if(response.status== 'success'){
                            resetTable();
                            $modalInstance.close();
                            $rootScope.toastrMessages('success',response.msg);
                        }
                    }, function(error) {
                        $rootScope.toastrMessages('error',error.data.msg);
                    });

                };


                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };

            },
            resolve: {
            }
        });
    };

    $scope.delete_un_serial=function(){
        $rootScope.clearToastr();
        $rootScope.codes_list = [];
        $rootScope.codes =[];
        cs_persons_vouchers.codesList(function (response) {
            if (response.list.length > 0){
                $rootScope.codes_list = response.list;
                $uibModal.open({
                    templateUrl: 'delete_un_serial.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'md',
                    controller: function ($rootScope,$scope, $modalInstance, $log) {

                        $rootScope.codes =[];
                        $scope.confirm = function (list) {
                            $rootScope.clearToastr();

                            $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                                .then(function() {
                                    let codes=[];
                                    angular.forEach(list, function(v, k) {
                                        codes.push(v.id);
                                    });

                                    $rootScope.progressbar_start();
                                    cs_persons_vouchers.deleteSerial({codes: codes},function (response) {
                                        $rootScope.progressbar_complete();
                                        if(response.status=='error' || response.status=='failed') {
                                            $rootScope.toastrMessages('error',response.msg);
                                        }
                                        else if(response.status== 'success'){
                                            resetTable();
                                            $modalInstance.close();
                                            $rootScope.toastrMessages('success',response.msg);
                                        }
                                    }, function(error) {
                                        $rootScope.toastrMessages('error',error.data.msg);
                                    });
                                });


                            $modalInstance.close();

                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    },
                    resolve: {
                    }
                })
            }else{
                $rootScope.toastrMessages('error','لا يوجد قسائم تمتلك كود موحد غير محذوفة');
            }
        });


    };

    $scope.sentSms=function(voucher_id ,size){
        $rootScope.clearToastr();
        cs_persons_vouchers.getPersonsList({id:voucher_id},function (response) {
            if(response.status== true){
                var receipt=response.persons;
                org_sms_service.getList(function (response) {
                    $rootScope.Providers = response.Service;
                });

                $uibModal.open({
                    templateUrl: '/app/modules/sms/views/modal/sent_sms_from_person_general.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance, $log) {

                        $scope.has_voucher =true;
                        $scope.sms={source: 'check' ,sms_provider:"",is_code:"1",
                            sms_messege:'{name} - {card}\n' +
                                'توجه لاستلام {title} بقيمة {sh_value}' +
                                'المكان {location}',same_msg:"",cont_type:""};

                        $scope.RestMsg=function(){
                            if($scope.sms.is_code != "1"){
                                $scope.sms.sms_messege = '';
                            }
                        };

                        $scope.confirm = function (data) {
                            $rootScope.clearToastr();
                            var params = angular.copy(data);
                            params.receipt=receipt;
                            params.target='person';

                            $rootScope.progressbar_start();
                            org_sms_service.sendSmsToTarget(params,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed') {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status== 'success'){
                                    if(response.download_token){
                                        var file_type='xlsx';
                                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                    }

                                    $modalInstance.close();
                                    $rootScope.toastrMessages('success',response.msg);
                                }
                            }, function(error) {
                                $rootScope.toastrMessages('error',error.data.msg);
                            });

                        };


                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    },
                    resolve: {
                    }
                });
            }
        });

    };

    $scope.checkToken=function(voucher_id){
        $rootScope.clearToastr();
        $scope.token_exist = false;

        $uibModal.open({
            templateUrl: 'check_link_screen.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: '',
            controller: function ($rootScope,$scope, $modalInstance, $log,cs_persons_vouchers) {

                cs_persons_vouchers.get({id:voucher_id},function (response) {
                    if(response.token){
                        $scope.token_exist = true;
                        // $scope.token = response.token;
                        $scope.token = $location.host() + '/citizens/#/check/' + response.token;
                        // $rootScope.tokenHost = $location.host() + '/#/check/' + response.token
                    }else{
                        $scope.token_exist = false;
                    }
                    $scope.valid_token = response.valid_token;
                });

                $scope.confirm = function (type) {
                    $rootScope.clearToastr();
                    $rootScope.progressbar_start();
                    cs_persons_vouchers.createToken({voucher_id:voucher_id,create_token:type},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='error' || response.status=='failed') {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else if(response.status== 'success'){
                            $scope.token_exist = true;
                            $scope.token = $location.host() + '/citizens/#/check/' + response.token_value;
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        }
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);
                    });

                };

                $scope.activate_deactivate = function (type) {
                    var activate = 0;
                    if(type == 'activate')
                        activate = 1;

                    $rootScope.progressbar_start();
                    cs_persons_vouchers.activateDeactivate({voucher_id:voucher_id,activate:activate},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='error' || response.status=='failed') {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else if(response.status== 'success'){
                            $scope.token_exist = true;
                            $scope.valid_token = response.valid_token;
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        }
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                }

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };

            },
            resolve: {
            }
        });


    };

    $scope.new=function(action){
        $rootScope.clearToastr();
        $rootScope.reanOnlyInput=false;

        if(action == 0){
            $rootScope.voucher={center:'0' , un_serial :''};
            $uibModal.open({
                templateUrl: 'myModalContent.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,cs_persons_vouchers) {

                    $scope.mode='add';

                    $scope.confirm = function (params) {
                        $rootScope.clearToastr();
                        if( !angular.isUndefined(params.voucher_date)) {
                            params.voucher_date=$filter('date')(params.voucher_date, 'dd-MM-yyyy')
                        }
                        $rootScope.progressbar_start();
                        cs_persons_vouchers.save(params,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                $modalInstance.close();
                                if(response.status=='success')
                                {
                                    resetTable();
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }
                        });

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.open = function($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.popup.opened = true;
                    };

                    $scope.popup = {
                        opened: false
                    };

                }});

        }
        else{
            $rootScope.voucher={add_cases:false,center:'0',collected:'0',un_serial:null};
            $uibModal.open({
                templateUrl: 'importFromExcel.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,cs_persons_vouchers,OAuthToken, FileUploader) {
                    $scope.remove=function(){
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };


                    $scope.upload=false;
                    $scope.mode='add';

                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };
                    $scope.setAdd=function(){
                        if($rootScope.voucher.center == 1 || $rootScope.voucher.center == '1' ){
                            $rootScope.voucher.add_cases=true;
                        }else{
                            $rootScope.voucher.add_cases=false;
                        }
                    };

                    $scope.fileupload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/aids/cs_persons_vouchers/storePersonsVoucher',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    Uploader.onBeforeUploadItem = function(item) {
                        item.formData = [{source: 'file'}];
                    };

                    $scope.confirm = function (item) {
                        $rootScope.progressbar_start();
                        Uploader.onBeforeUploadItem = function(i) {
                            let center = 0;

                            if($rootScope.can('aid.voucher.center')){
                                center = 1;
                            }

                            i.formData = [{title: item.title,
                                center: center,
                                beneficiary: 1,
                                notes: item.notes,
                                currency_id: item.currency_id,
                                exchange_rate: item.exchange_rate,
                                transfer: item.transfer,
                                add_cases: item.add_cases,
                                un_serial: item.un_serial,
                                collected: item.collected,
                                allow_day: item.allow_day,
                                transfer_company_id: item.transfer_company_id,
                                category_id: item.category_id,
                                case_category_id: item.case_category_id,
                                voucher_date: $filter('date')(item.voucher_date, 'dd-MM-yyyy'),
                                source:'file',
                                type: item.type,
                                sponsor_id: item.sponsor_id}];
                        };


                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);

                            if(response.download_token){
                                var file_type='xlsx';
                                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;

                            }
                            $modalInstance.close();
                            if(response.status=='true' || response.status==true)
                            {
                                resetTable();
                                $rootScope.toastrMessages('success',response.msg);
                            }else if(response.status=='inserted_error')
                            {
                                resetTable();
                                $rootScope.toastrMessages('error',response.msg);
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                        };
                        Uploader.uploadAll();
                    };
                    $scope.confirm2 = function (params) {
                        $rootScope.clearToastr();
                        $rootScope.progressbar_start();
                        if( !angular.isUndefined(params.voucher_date)) {
                            params.voucher_date=$filter('date')(vouchers.voucher_date, 'dd-MM-yyyy')
                        }
                        var target = new cs_persons_vouchers(params);
                        target.$save()
                            .then(function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    $modalInstance.close();
                                    if(response.status=='success')
                                    {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        resetTable();
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();
                            });

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.open = function($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.popup.opened = true;
                    };

                    $scope.popup = {
                        opened: false
                    };

                }});


        }
    };

    $scope.update=function(size,voucher_id){
        $rootScope.clearToastr();
        $rootScope.reanOnlyInput=false;
        $rootScope.voucher_={};
        cs_persons_vouchers.get({id:voucher_id},function (response) {
            $rootScope.voucher=response;
            $rootScope.voucher.voucher_date=new Date($rootScope.voucher.voucher_date);
            $rootScope.voucher.category_id=$rootScope.voucher.category_id +"";
            $rootScope.voucher.type=$rootScope.voucher.type +"";
            $rootScope.voucher.allow_day = parseInt(response.allow_day);

            if($rootScope.voucher.status == 1){
                $rootScope.reanOnlyInput=true;
            }
            if($rootScope.voucher.project_id != null){
                $rootScope.reanOnlyInput=true;
            }

            $uibModal.open({
                templateUrl: 'myModalContent.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope, $modalInstance, $log,cs_persons_vouchers) {
                    $scope.mode='edit';
                    $scope.confirm = function (params) {
                        $rootScope.clearToastr();
                        if( !angular.isUndefined(params.voucher_date)) {
                            params.voucher_date=$filter('date')(params.voucher_date, 'dd-MM-yyyy')
                        }

                        $rootScope.progressbar_start();
                        cs_persons_vouchers.update(params,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid')
                            {
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                $modalInstance.close();
                                if(response.status=='success')
                                {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    resetTable();

                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }


                            }
                        });
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.open = function($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.popup.opened = true;
                    };

                    $scope.popup = {
                        opened: false
                    };

                } });

        });
    };

    $scope.show=function(size,voucher_id){
        $rootScope.clearToastr();
        $rootScope.reanOnlyInput=false;
        cs_persons_vouchers.get({id:voucher_id},function (response) {
            $rootScope.voucher=response;
            $rootScope.voucher.voucher_date=new Date($rootScope.voucher.voucher_date);
            $rootScope.voucher.category_id=$rootScope.voucher.category_id +"";
            $rootScope.voucher.type=$rootScope.voucher.type +"";
            // $rootScope.voucher.sponsor_id=$rootScope.voucher.sponsor_id +"";
            if($rootScope.voucher.status == 1){
                $rootScope.reanOnlyInput=true;
            }
            if($rootScope.voucher.project_id != null){
                $rootScope.reanOnlyInput=true;
            }

            $uibModal.open({
                templateUrl: 'showVoucher.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope, $modalInstance, $log,cs_persons_vouchers) {
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.open = function($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.popup.opened = true;
                    };

                    $scope.popup = {
                        opened: false
                    };

                } });

        });
    };

    $scope.duplicate=function(voucher_id){
        $rootScope.clearToastr();
        $rootScope.reanOnlyInput=false;
        cs_persons_vouchers.get({id:voucher_id},function (response) {
            $rootScope.voucher=response;
            delete $rootScope.voucher.id;
            delete $rootScope.voucher.status;
            delete $rootScope.voucher.status;
            $rootScope.voucher.voucher_date=new Date($rootScope.voucher.voucher_date);
            $rootScope.voucher.category_id=$rootScope.voucher.category_id +"";
            $rootScope.voucher.type=$rootScope.voucher.type +"";
            $uibModal.open({
                templateUrl: 'myModalContent.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,cs_persons_vouchers) {
                    $scope.mode='add';

                    $rootScope.model_error_status =false;

                    $rootScope.failed =false;
                    ;

                    $scope.confirm = function (params) {
                        $rootScope.clearToastr();
                        if( !angular.isUndefined(params.voucher_date)) {
                            params.voucher_date=$filter('date')(params.voucher_date, 'dd-MM-yyyy')
                        }
                        $rootScope.progressbar_start();

                        cs_persons_vouchers.save(params,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid')
                            {
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                $modalInstance.close();
                                if(response.status=='success')
                                {
                                    resetTable();
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }
                        });

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.open = function($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.popup.opened = true;
                    };

                    $scope.popup = {
                        opened: false
                    };

                } });

        });
    };

    $scope.delete = function (all,id) {
        $rootScope.msg='delete';
        $rootScope.clearToastr();
        let msg = $filter('translate')('are you want to confirm delete');
        if (all)
            msg ='هل تريد حذف القسيمة وتفريغها من المستفيدين و المرفقات في آن واحد ؟ '

        $ngBootbox.confirm(msg)
            .then(function() {
                $rootScope.clearToastr();
                $rootScope.progressbar_start();
                cs_persons_vouchers.delete({id:id , empty : all},function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status=='success') {
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        resetTable();
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                });
            });


    };

    $scope.reset=function(id){
        $rootScope.clearToastr();
        $rootScope.msg ='';
        $rootScope.progressbar_start();
        cs_persons_vouchers.reset({id:id},function (response) {
            $rootScope.progressbar_complete();
            if(response.status=='success') {
                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                resetTable();
            }else{
                $rootScope.toastrMessages('error',response.msg);

            }
        });
    };

    $scope.updateStatus=function(id,status,index){
        $rootScope.clearToastr();
        $rootScope.progressbar_start();
        cs_persons_vouchers.setStatus({id:id,'status':status},function (response) {
            $rootScope.progressbar_complete();
            if(response.status=='success') {
                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                resetTable();
            }else{
                $rootScope.toastrMessages('error',response.msg);

            }
        });
    };

    $scope.import_beneficiary = function (id){
        $uibModal.open({
            templateUrl: 'import_beneficiary.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'md',
            controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                $scope.download=function(){
                    window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-persons-voucher-template&template=true';
                };
                var Uploader = $scope.uploader = new FileUploader({
                    url: '/api/v1.0/aids/cs_persons_vouchers/importBeneficiary',
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });

                $scope.fileupload=function(){
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.upload=true;
                };
                $scope.confirm = function () {
                    // // angular.element('.btn').addClass("disabled");
                    $rootScope.progressbar_start();
                    $rootScope.clearToastr();
                    $scope.spinner=false;
                    $scope.import=false;
                    $rootScope.progressbar_start();
                    Uploader.onBeforeUploadItem = function(item) {
                        item.formData = [{voucher_id: id}];
                    };

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        // angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        $rootScope.progressbar_complete();

                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);

                        if(response.download_token){
                            window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token;
                        }

                        if(response.status=='error' || response.status=='failed')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                        }
                        else if(response.status=='success')
                        {
                            $scope.spinner=false;
                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                            $scope.import=false;
                            $rootScope.toastrMessages('success',response.msg);
                            resetTable();
                            $modalInstance.close();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                            resetTable();
                            $modalInstance.close();
                        }


                    };

                    Uploader.uploadAll();
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    }
    $scope.ChooseTemplate=function(id,action){
        $rootScope.clearToastr();

        if(id != -1){
            $rootScope.Voucher_id=id;
            $rootScope.action=action;
            $uibModal.open({
                templateUrl: 'myModalContent2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/aids/cs_persons_vouchers/template-upload',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.progressbar_start();
                        item.formData = [{Voucher_id: $rootScope.Voucher_id}];
                    };

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(!response){
                            $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                        }else{
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            resetTable();
                        }

                        $modalInstance.close();

                    };


                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        }else{
            $uibModal.open({
                templateUrl: 'myModalContent2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/aids/cs_persons_vouchers/default-template-upload',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.progressbar_start();
                    };

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(!response){
                            $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                        }else{
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            RestTemplate();
                        }

                        $modalInstance.close();

                    };


                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        }


    };

    $scope.Export=function(voucher_id){
        $rootScope.progressbar_start();
        cs_persons_vouchers.persons({target : 1 ,action:'xls' , voucher_id:voucher_id},function (response) {
            $rootScope.progressbar_complete();
            if(response.download_token){
                window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token;
            }
        });

    };

    $scope.voucherFiles = function (id) {

        $rootScope.clearToastr();
        $rootScope.progressbar_start();

        cs_persons_vouchers.getAttachments({id:id},function (response) {
            $rootScope.progressbar_complete();
            $rootScope.voucherStatus= response.status;
            $rootScope.progressbar_complete();
            if(response.status == false){
                $rootScope.toastrMessages('error',$filter('translate')('no attachment to export'));
            }else{
                window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token;
            }
        });
    };
    $scope.beneficiary=function(voucher_id){

        $uibModal.open({
            templateUrl: 'ImportToVouchers.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'md',
            controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                $scope.upload=false;
                $scope.row={category_id:"",status:""};
                $scope.person_vouchers_import = {};
                $scope.person_vouchers_import.withRatio=false;
                $scope.person_vouchers_import.add_cases=false;

                $scope.RestUpload=function(value){
                    if(value ==2){
                        $scope.upload=false;
                    }
                };


                $scope.fileUpload=function(){
                    Uploader.destroy();
                    Uploader.queue=[];
                    $scope.upload=true;
                };
                $scope.download=function(){
                    window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-persons-voucher-template&template=true';
                };

                var Uploader = $scope.uploader = new FileUploader({
                    url: '/api/v1.0/aids/vouchers/beneficiary/excel/cases',
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });

                Uploader.onBeforeUploadItem = function(item) {
                    item.formData = [{source: 'file'}];
                };

                $scope.confirm = function (items) {
                    $rootScope.progressbar_start();
                    $rootScope.clearToastr();
                    $scope.spinner=false;
                    $scope.import=false;
                    Uploader.onBeforeUploadItem = function(i) {
                        $rootScope.clearToastr();
                        var ratio = 0;
                        if(items.with_ratio == 'true' || items.with_ratio == 'true' ){
                            ratio = 1;
                        }
                        i.formData = [{voucher_id: voucher_id, ratio:ratio,
                            withRatio:items.withRatio,
                            add_cases:items.add_cases,
                            // receipt_location:items.receipt_location,
                            receipt_date:$filter('date')(items.receipt_date, 'yyyy/MM/dd')
                        }];
                    };
                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);

                        if(response.download_token){
                            window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token;
                        }

                        if(response.status=='error' || response.status=='failed')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                        }
                        else if(response.status=='success')
                        {
                            $scope.spinner=false;
                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                            $scope.import=false;
                            $rootScope.toastrMessages('success',response.msg);
                            $modalInstance.close();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                            $modalInstance.close();
                        }

                    };
                    Uploader.uploadAll();
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 0
                };
                $scope.formats = [ 'dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                $scope.format = $scope.formats[0];
                $scope.today = function() {
                    $scope.dt = new Date();
                };
                $scope.today();
                $scope.clear = function() {
                    $scope.dt = null;
                };
                $scope.open4 = function($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.popup4.opened = true;
                };
                $scope.popup4 = {
                    opened: false
                };
                $scope.remove=function(){
                    angular.element("input[type='file']").val(null);
                    $scope.uploader.clearQueue();
                    angular.forEach(
                        angular.element("input[type='file']"),
                        function(inputElem) {
                            angular.element(inputElem).val(null);
                        });
                };

            }});

    };

    $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0};
    $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $rootScope.format = $scope.formats[0];
    $rootScope.today = function() {$rootScope.dt = new Date();};
    $rootScope.today();
    $rootScope.clear = function() {$rootScope.dt = null;};
    $scope.popup99 = {opened: false};
    $scope.popup88 = {opened: false};
    $scope.popup999 = {opened: false};
    $scope.popup888 = {opened: false};
    $scope.open88 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup88.opened = true;};
    $scope.open999 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup999.opened = true;};
    $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
    $scope.open888 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup888.opened = true;};

    // restore social search by id
    $scope.restore = function (id) {
        $rootScope.clearToastr();
        $ngBootbox.confirm($filter('translate')('are you want to confirm restore') )
            .then(function() {
                $rootScope.progressbar_start();
                cs_persons_vouchers.restore({id: id}, function (response) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                    resetTable();
                });
            });
    };
});
