

angular.module('AidModule')
        .controller('personsVouchersController', function( $filter,$stateParams,$rootScope, $scope,$uibModal, $http, $timeout,$state,Entity,setting,Org,FileUploader, OAuthToken,vouchers_categories,cs_persons_vouchers,cases,category) {

    $rootScope.clearToastr();

    $state.current.data.pageTitle = $filter('translate')('personsVouchers');
    $rootScope.settings.layout.pageSidebarClosed = true;

    $scope.master=false;
    $rootScope.status ='';
    $scope.CurrentPage = 1;
    $scope.itemsCount = 50;
    $rootScope.msg ='';
    $rootScope.default_template=null;
    $rootScope.has_default_template=false;
    $rootScope.filter={};
    $scope.items=[];
    $scope.toggleStatus = true;
    $rootScope.labels = {
        "itemsSelected": $filter('translate')('itemsSelected'),
        "selectAll": $filter('translate')('selectAll'), "unselectAll": $filter('translate')('unselectAll'),
        "search": $filter('translate')('search'), "select": $filter('translate')('select')
    }
    // Org.list({type:'organizations'},function (response) {  $scope.Org = response; });
    Org.list({type:'sponsors'},function (response) { $rootScope.Sponsors = response; });
    Entity.get({entity:'entities',c:'currencies,transferCompany,organizationsCat'},function (response) {
               $rootScope.currency = response.currencies;
               $rootScope.transferCompany = response.transferCompany;
               $rootScope.organizationsCat = response.organizationsCat;
    });

    $rootScope.aidCategories_=[];
    category.getSectionForEachCategory({'type':'aids'},function (response) {
        if(!angular.isUndefined(response)){
            $rootScope.aidCategories_firstStep_map =[];
            $rootScope.aidCategories = response.Categories;
            if($rootScope.aidCategories.length > 0){
                for (var i in $rootScope.aidCategories) {
                    var item = $rootScope.aidCategories[i];
                    $rootScope.aidCategories_firstStep_map[item.id] = item.FirstStep;
                }
                angular.forEach($rootScope.aidCategories, function(v, k) {
                    if ($rootScope.lang == 'ar'){
                        $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
                    }
                    else{
                        $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
                    }
                });
            }
        }
    });

    var resetVouchersFilter =function(){
        $rootScope.filter={
            "page":1,
            "all_organization":false,
            "type":"",
            "serial":"",
            "voucher_source":"",
            "category_id":"",
            "sponsor_id":"",
            "title":"",
            "content":"",
            "notes":"",
            "min_value":"",
            "max_value":"",
            "min_count":"",
            "transfer":"",
            "transfer_company_id":null,
            "case_category_id":"",
            "max_count":"",
            "date_to":"",
            "date_from":"",
            "voucher_date_to":"",
            "voucher_date_from":""
        };

        if($stateParams.id){
            $rootScope.filter.sponsor_id=$stateParams.id;
            $scope.sponsor=true;
        }else{
            $scope.sponsor=false;
        }

    };
    var LoadPersonsVouchersList =function($param){
        // $rootScope.clearToastr();
        $param.page =  $scope.CurrentPage ;
        $param.itemsCount =  $scope.itemsCount ;

        if( !angular.isUndefined($param.transfer)) {
            if($param.transfer == 0 ||$param.transfer == '0'){
                $param.transfer_company_id = null
            }
        }
        $rootScope.progressbar_start();
        cs_persons_vouchers.cases($param,function (response) {
            $rootScope.progressbar_complete();
            if($param.action == 'ExportToExcel' || $param.action == 'ExportToWord' || $param.action == 'ExportToPDF'){
                
                if(response.status == 'false' || response.status == false){
                    $rootScope.toastrMessages('error',$filter('translate')('no beneficiaries to export coupons'));
                }else{
                    var file_type='xlsx';
                    if($param.action == 'ExportToWord' || $param.action == 'ExportToPDF') {
                        file_type='zip';
                    }
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }
                
            }else{
               
                if(response.data.data.total != 0){
                    if( !angular.isUndefined($param.all_organization)) {
                        if($param.all_organization == true ||$param.all_organization == 'true'){
                            $scope.master=true
                        }else if($param.all_organization == false ||$param.all_organization == 'false') {
                            $scope.master=false
                        }
                    }
                    // $scope.master=response.master;
                    $scope.items = response.data.data;
                    $scope.CurrentPage = response.data.current_page;
                    if($param.page == 1){
                        $rootScope.total_value = response.total;
                    }
                    $rootScope.TotalItems = response.data.total;
                    $rootScope.ItemsPerPage = response.data.per_page;
                }else{
                    $scope.items= [];
                    $rootScope.total_value= 0;
                }

            }
        });

    };

    var resetTable = function () {

        var data = angular.copy($rootScope.filter);
        data.action ='filter';
        data.page =$scope.CurrentPage;
        data.organization_ids=[];
        data.organization_category=[];

        if( !angular.isUndefined(data.all_organization)) {
            if(data.all_organization == true ||data.all_organization == 'true'){

                angular.forEach(data.organization_category_id, function(v, k) {
                    data.organization_category.push(v.id);
                });

                angular.forEach(data.organization_id, function(v, k) {
                    data.organization_ids.push(v.id);
                });
                delete data.organization_id;
                $scope.master=true
            }else if(data.all_organization == false ||data.all_organization == 'false') {
                data.organization_category=[];
                data.organization_ids=[];
                $scope.master=false
            }

        }
        LoadPersonsVouchersList(data);
    };
    resetVouchersFilter();
    // resetTable();

    vouchers_categories.List(function (response) {
        $rootScope.Choices = response;
    });

    $scope.resetVouchersFilter_=function(){
        resetVouchersFilter();
    }

    $scope.disblay_orgs =false;
    $scope.resetOrg=function(all_organization,organization_category){

        $rootScope.clearToastr();
        $scope.disblay_orgs =false;
        let ids = [];
        if( !angular.isUndefined(all_organization)) {
                    if(all_organization == true ||all_organization == 'true'){
                        angular.forEach(organization_category, function(v, k) {
                            ids.push(v.id);
                        });

                        Org.ListOfCategory({ids:ids},function (response) {
                            if (response.list.length > 0){
                                $scope.Org = response.list;
                                $scope.disblay_orgs =true;
                            }else{
                                $scope.Org = [];
                                $scope.disblay_orgs =false;
                                $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');
                            }
                        });
                    }else{
                        $scope.Org = [];
                        $scope.disblay_orgs =false;
                        $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');
                    }
        }else{
                    $scope.Org = [];
                    $scope.disblay_orgs =false;
                    $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');

        }

    };


    $rootScope.pageChanged = function (CurrentPage) {
        $scope.CurrentPage = CurrentPage;
        resetTable();
    };

    $rootScope.itemsPerPage_ = function (itemsCount) {
        $scope.CurrentPage = 1;
        $scope.itemsCount = itemsCount;
        resetTable();
    };

    $scope.selectedAll = false;
    $scope.selectAll=function(value){
            if (value) {
                $scope.selectedAll = true;
            } else {
                $scope.selectedAll = false;
            }

            angular.forEach($scope.items, function(v, k) {
                v.check=$scope.selectedAll;
            });
    };
    $scope.Search=function(selected,params,action) {

        $rootScope.clearToastr();
        var data = angular.copy(params);
        data.action = action;
        data.organization_ids = [];
        data.items=[];

       if(selected == true || selected == 'true' ){
            
           var items =[];
            angular.forEach($scope.items, function(v, k){
                
                 if(action == 'ExportToWord' || action == 'ExportToPDF'){
                    if(v.check && v.beneficiary > 0){
                        items.push(v.id);
                    } 
                 }else{
                     if(v.check){
                        items.push(v.id);
                    }
                 }
                

            });
            
            if(items.length==0){
                $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                return ;
            }
            data.items=items;
        }
        
        if( !angular.isUndefined(data.all_organization)) {
            if(data.all_organization == true ||data.all_organization == 'true'){
                angular.forEach(data.organization_id, function(v, k) {
                    data.organization_ids.push(v.id);
                });
                delete data.organization_id;
                $scope.master=true
            }else if(data.all_organization == false ||data.all_organization == 'false') {
                data.organization_ids=[];
                $scope.master=false
            }

        }

        if( !angular.isUndefined(data.date_to)) {
            data.date_to=$filter('date')(data.date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.date_from)) {
            data.date_from= $filter('date')(data.date_from, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(data.voucher_date_to)) {
            data.voucher_date_to=$filter('date')(data.voucher_date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.voucher_date_from)) {
            data.voucher_date_from= $filter('date')(data.voucher_date_from, 'dd-MM-yyyy')
        }
        data.action=action;

        if( !angular.isUndefined(angular.transfer)) {
            if(angular.transfer == 0 ||angular.transfer == '0'){
                angular.transfer_company_id = null
            }
        }
        
         data.page=1;
         LoadPersonsVouchersList(data);
    };

    $scope.sortKeyArr=[];
    $scope.sortKeyArr_rev=[];
    $scope.sortKeyArr_un_rev=[];
    $scope.sort_ = function(keyname){
        $scope.sortKey_ = keyname;
        var reverse_ = false;
        if (
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
        ) {
            reverse_ = true;
        }
        var data = angular.copy($rootScope.filter);
        data.action ='filter';

        if (reverse_) {
            if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_rev.push(keyname);
            }
        }
        else {
            if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_un_rev.push(keyname);
            }

        }

        data.sortKeyArr_rev = $scope.sortKeyArr_rev;
        data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;
        $scope.Search(data,'filter');
    };

    $scope.toggle = function() {
        $rootScope.clearToastr();
        $scope.toggleStatus = $scope.toggleStatus == false ? true: false;
        resetVouchersFilter();
        if($scope.toggleStatus == true) {
            $rootScope.filter.page =1;
            resetTable();
        }
    };

    $scope.check = function(){


        $rootScope.clearToastr();
        $rootScope.vouchers_code_list = [];
        $rootScope.codes_list = [];
        $rootScope.titles_list = [];
        // $rootScope.vouchers_code =[];
        // $rootScope.codes =[];

        let  getList = 0;
        cs_persons_vouchers.codesList(function (response) {
            $rootScope.codes_list = response.list;
            getList++;
        });

        cs_persons_vouchers.vouchersCodeList(function (response) {
            $rootScope.vouchers_code_list = response.list;
            getList++;
        });

        cs_persons_vouchers.vouchersTitleList(function (response) {
            $rootScope.titles_list = response.list;
            getList++;
        });


        $timeout(function() {
            $uibModal.open({
                templateUrl: 'checkPerson.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope._vouchers_code =[];
                    $scope._codes =[];
                    $scope._titles =[];

                    $scope.upload=false;
                    $scope.filter_={
                        "vouchers_code":[],
                        "codes":[],
                        "titles":[],
                        "un_serial":"",
                        "receipt_date_from":"",
                        "receipt_date_to":"",
                        "voucher_source":"",
                        "category_id":"",
                        "case_category_id":"",
                        "sponsor_id":"",
                        "title":"",
                        "content":"",
                        "notes":"",
                        "min_value":"",
                        "max_value":"",
                        "min_count":"",
                        "transfer":"",
                        "allow_day":"",
                        "transfer_company_id":null,
                        "max_count":"",
                        "date_to":"",
                        "date_from":"",
                        "voucher_date_to":"",
                        "voucher_date_from":"",
                        "to_all" : false,
                        "for_person" :true,
                        "for_parent" : false,
                        "for_wife" : false,
                        "for_children" : false
                    };

                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };

                    $scope.download=function(){
                        angular.element('.btn').addClass("disabled");

                        $rootScope.clearToastr();
                        $rootScope.progressbar_start();
                        $http({
                            url:'/api/v1.0/common/reports/template',
                            method: "GET"
                        }).then(function (response) {
                            $rootScope.progressbar_complete();
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token+'&template=true';
                        }, function(error) {
                            $rootScope.progressbar_complete();
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $rootScope.toastrMessages('error',error.data.msg);
                        });
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url:'/api/v1.0/common/reports/voucherSearch/check',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.clearToastr();
                        //   ,type:type
                        var params = {source: 'file',target: 'in'} ;

                        angular.forEach($scope.filter_, function(v, k) {

                            if(!angular.isUndefined(v)) {
                                if( !( v == null || v == 'null' || v == '' || v == ' ') ) {
                                    if (k == 'receipt_date_from' || k == 'receipt_date_to' ||
                                        k == 'date_to' || k == 'date_from' ||
                                        k == 'voucher_date_to' ||k == 'voucher_date_from')
                                    {
                                        params[k] = $filter('date')(v, 'dd-MM-yyyy')
                                    }
                                    else if (k == 'codes' || k == 'vouchers_code' || k == 'titles'){

                                        let codes=[];
                                        angular.forEach(v, function(v2, k2) {
                                            codes.push(v2.id);
                                        });

                                        params[k]=codes;
                                    }
                                    else{
                                        params[k]=v;
                                    }
                                }
                            }

                        });

                        item.formData = [params];
                    };

                    $scope.confirm = function () {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                        $scope.spinner=false;
                        $scope.import=false;
                        angular.element('.btn').addClass("disabled");

                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);
                            if(response.status=='success')
                            {
                                $scope.tabs = true;
                                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                                $scope.spinner=false;
                                $scope.import=false;
                                $rootScope.toastrMessages('success',response.msg);
                            }else{
                                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            if(response.download_token){
                                var file_type='xlsx';
                                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                            }
                            $modalInstance.close();

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                    $scope.dateOptions = { formatYear: 'yy',  startingDay: 0 };
                    $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                    $scope.today = function() { $scope.dt = new Date(); };
                    $scope.clear = function() { $scope.dt = null; };

                    $scope.format = $scope.formats[0];
                    $scope.today();

                    $scope.popup99 = {opened: false};
                    $scope.popup88 = {opened: false};
                    $scope.popup999 = {opened: false};
                    $scope.popup888 = {opened: false};

                    $scope.popup880  = {opened: false};
                    $scope.popup90  = {opened: false};
                    $scope.popup880  = {opened: false};
                    $scope.popup900  = {opened: false};

                    $scope.open880 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup880.opened = true;};
                    $scope.open999 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup999.opened = true;};
                    $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
                    $scope.open888 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup888.opened = true;};

                    $scope.open88 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup88.opened = true; };
                    $scope.open90 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup90.opened = true;};
                    $scope.open880 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup880.opened = true; };
                    $scope.open900 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup900.opened = true;};


                }});
        },500);

    };


    $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0};
    $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $rootScope.format = $scope.formats[0];
    $rootScope.today = function() {$rootScope.dt = new Date();};
    $rootScope.today();
    $rootScope.clear = function() {$rootScope.dt = null;};
    $scope.popup99 = {opened: false};
    $scope.popup88 = {opened: false};
    $scope.popup999 = {opened: false};
    $scope.popup888 = {opened: false};
    $scope.open88 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup88.opened = true;};
    $scope.open999 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup999.opened = true;};
    $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
    $scope.open888 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup888.opened = true;};

});
