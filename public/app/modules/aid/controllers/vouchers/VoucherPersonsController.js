angular.module('AidModule')
    .controller('VoucherPersonsController', function( $filter,$stateParams,$rootScope, $scope,$uibModal, $http, $timeout,$state,cases,$ngBootbox,cs_persons_vouchers,org_sms_service) {

        $state.current.data.pageTitle = $filter('translate')('voucher_beneficiary');
        $rootScope.labels = {
            "itemsSelected": $filter('translate')('itemsSelected'),
            "selectAll": $filter('translate')('selectAll'), "unselectAll": $filter('translate')('unselectAll'),
            "search": $filter('translate')('search'), "select": $filter('translate')('select')
        }
       $rootScope.no_vouchers =false;
        $rootScope.is_mine =false;
        $rootScope.status ='';
        $rootScope.msg ='';
        $rootScope.items=[];
        $rootScope.voucher= '';
        $rootScope.voucher_id= $stateParams.id;
        $rootScope.voucherStatus=1;
        $rootScope.CurrentPage = 1;
        $scope.itemsCount='50';
        if($stateParams.id==null){
            $rootScope.toastrMessages('error',$filter('translate')('Your are request invalid link , you will be redirected to the vouchers page to search again and request the valid link'));
            $timeout(function() {
                $state.go('allVouchers');
            }, 3000);
        }else{

            $rootScope.clearToastr();

            var LoadBeneficiary=function(params){
                params.target =0;
                params.page = $rootScope.CurrentPage;
                params.voucher_id = $stateParams.id;
                $rootScope.progressbar_start();
                params.itemsCount =$scope.itemsCount;
                cs_persons_vouchers.persons(params,function (response) {
                    $rootScope.progressbar_complete();
                    $rootScope.voucherStatus= response.status;
                    $rootScope.beneficiary= response.beneficiary;
                    $rootScope.is_mine= response.is_mine;
                    $rootScope.items= response.Beneficiary.data;
                    $rootScope.CurrentPage = response.Beneficiary.current_page;
                    $rootScope.TotalItems = response.Beneficiary.total;
                    $rootScope.ItemsPerPage = response.Beneficiary.per_page;
                    $rootScope.voucher = response.voucher;
                    $rootScope.voucher_id = response.voucher_id;
                });
            };

            $scope.close=function(){
                $scope.success =false;
                $scope.failed =false;
                $scope.error =false;
                $scope.model_error_status =false;
            };
            $scope.Export=function(target){
                $rootScope.progressbar_start();
                cs_persons_vouchers.persons({target : 1 ,action:target , voucher_id:$stateParams.id},function (response) {
                    $rootScope.progressbar_complete();
                    if(response.download_token){

                        var type = 'xlsx';
                        if(target == 'pdf'){
                            type = 'pdf';
                        }
                        window.location = '/api/v1.0/common/files/'+type+'/export?download_token='+response.download_token;
                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                    }
                });

            };
            $scope.ExportToWord=function(id,voucher_id){
                $rootScope.progressbar_start();
                $http({
                    url: "/api/v1.0/aids/cs_persons_vouchers/exportToWord/"+id+'/'+voucher_id,
                    method: "GET"
                }).then(function (response) {
                    $rootScope.progressbar_complete();
                    window.location = '/api/v1.0/common/files/docx/export?download_token='+response.data.download_token;
                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });
            };
            $rootScope.pageChanged = function (CurrentPage) {
                $rootScope.CurrentPage = CurrentPage;
                LoadBeneficiary({});
            };
            $rootScope.itemsPerPage_ = function (itemsCount) {
                $scope.itemsCount = itemsCount;
                $scope.CurrentPage = 1;
                LoadBeneficiary({});
            };
            $scope.allCheckbox=function(value){
                if (value) {
                    $rootScope.selectedAll = true;
                } else {
                    $rootScope.selectedAll = false;
                }

                angular.forEach($scope.items, function(v, k) {
                    v.check=$rootScope.selectedAll;
                });


            };
            $scope.deletePerson = function(size,id,voucher_id){
                $rootScope.clearToastr();
                $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                    .then(function() {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                        cs_persons_vouchers.deletePersonsVoucher({'id':id,'voucher_id':voucher_id}, function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='success') {
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                LoadBeneficiary({});
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                        });
                    });



            };
            $scope.sendSms=function(type,receipt_id) {
                $rootScope.clearToastr();
                if(type == 'manual'){
                    var receipt = [];
                    var pass = true;

                    if(receipt_id ==0){
                        angular.forEach($scope.items, function (v, k) {
                            if (v.check) {
                                receipt.push(v.person_id);
                            }
                        });
                        if(receipt.length==0){
                            pass = false;
                            $rootScope.toastrMessages('error',$filter('translate')("you did not select anyone to send them sms message"));
                            return;
                        }
                    }else{
                        receipt.push(receipt_id);
                    }

                    if(pass){
                        org_sms_service.getList(function (response) {
                            $rootScope.Providers = response.Service;
                        });
                        $uibModal.open({
                            templateUrl: '/app/modules/sms/views/modal/sent_sms_from_person_general.html',
                            backdrop  : 'static',
                            keyboard  : false,
                            windowClass: 'modal',
                            size: 'lg',
                            controller: function ($rootScope,$scope, $modalInstance) {
                                $scope.has_voucher =true;
                                $scope.sms={source: 'check' ,sms_provider:"",is_code:"1",voucher_id :$stateParams.id,
                                    sms_messege:'{name} - {card}\n' +
                                        'توجه لاستلام {title} بقيمة {sh_value}' +
                                        'المكان {location}',same_msg:"",cont_type:""};

                                $scope.RestMsg=function(){
                                    if($scope.sms.is_code != "1"){
                                        $scope.sms.sms_messege = '';
                                    }
                                };

                                $scope.confirm = function (data) {
                                    $rootScope.progressbar_start();
                                    $rootScope.clearToastr();
                                    var params = angular.copy(data);
                                    params.receipt=receipt;
                                    params.target='person';
                                    org_sms_service.sendSmsToTarget(params,function (response) {
                                        $rootScope.progressbar_complete();
                                        if(response.status=='error' || response.status=='failed') {
                                            $rootScope.toastrMessages('error',response.msg);
                                        }
                                        else if(response.status== 'success'){
                                            if(response.download_token){
                                                var file_type='xlsx';
                                                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                            }

                                            $modalInstance.close();
                                            $rootScope.toastrMessages('success',response.msg);
                                        }
                                    }, function(error) {
                                        $rootScope.progressbar_complete();
                                        $rootScope.toastrMessages('error',error.data.msg);
                                    });

                                };

                                $scope.cancel = function () {
                                    $modalInstance.dismiss('cancel');
                                };

                            },
                            resolve: {
                            }
                        });

                    }
                }else{
                    org_sms_service.getList(function (response) {
                        $rootScope.Providers = response.Service;
                    });
                    $uibModal.open({
                        templateUrl: '/app/modules/sms/views/modal/sent_sms_from_xlx_general.html',
                        backdrop  : 'static',
                        keyboard  : false,
                        windowClass: 'modal',
                        size: 'lg',
                        controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                            $scope.has_voucher =true;
                            $scope.upload=false;
                            $scope.sms={source: 'file' ,sms_provider:"",voucher_id :$stateParams.id, is_code:"1",
                                sms_messege:'{name} - {card}\n' +
                                    'توجه لاستلام {title} بقيمة {sh_value}' +
                                    'المكان {location}',same_msg:"",cont_type:""};

                            $scope.RestMsg=function(){
                                if($scope.sms.is_code != "1"){
                                    $scope.sms.sms_messege = '';
                                }
                            };

                            $scope.RestMsg=function(){
                                if($scope.sms.is_code != "1"){
                                    $scope.sms.sms_messege = '';
                                }
                            };
                            $scope.RestUpload=function(value){
                                if(value ==2){
                                    $scope.upload=false;
                                }
                            };

                            $scope.fileUpload=function(){
                                $scope.uploader.destroy();
                                $scope.uploader.queue=[];
                                $scope.upload=true;
                            };
                            $scope.download=function(){
                                window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-to-send-sms-template&template=true';
                            };

                            var Uploader = $scope.uploader = new FileUploader({
                                url: 'api/v1.0/sms/excel',
                                removeAfterUpload: false,
                                queueLimit: 1,
                                headers: {
                                    Authorization: OAuthToken.getAuthorizationHeader()
                                }
                            });


                            $scope.confirm = function (item) {
                                $scope.spinner=false;
                                $scope.import=false;
                                $rootScope.progressbar_start();
                                Uploader.onBeforeUploadItem = function(item) {

                                    if($scope.sms.same_msg == 0){
                                        $scope.sms.sms_messege = '';
                                    }
                                    item.formData = [$scope.sms];
                                };
                                Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                                    $rootScope.progressbar_complete();
                                    $scope.uploader.destroy();
                                    $scope.uploader.queue=[];
                                    $scope.uploader.clearQueue();
                                    angular.element("input[type='file']").val(null);

                                    if(response.status=='error' || response.status=='failed')
                                    {
                                        $rootScope.toastrMessages('error',response.msg);
                                        $scope.upload=false;
                                    }
                                    else if(response.status=='success')
                                    {
                                        if(response.download_token){
                                            var file_type='xlsx';
                                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                        }
                                        $scope.spinner=false;
                                        $scope.upload=false;
                                        angular.element("input[type='file']").val(null);
                                        $scope.import=false;
                                        $rootScope.toastrMessages('success',response.msg);
                                        $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                        $modalInstance.close();
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                        $scope.upload=false;
                                        angular.element("input[type='file']").val(null);
                                        $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                    }

                                };
                                Uploader.uploadAll();
                            };
                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };
                            $scope.remove=function(){
                                angular.element("input[type='file']").val(null);
                                $scope.uploader.clearQueue();
                                angular.forEach(
                                    angular.element("input[type='file']"),
                                    function(inputElem) {
                                        angular.element(inputElem).val(null);
                                    });
                            };

                        }});

                }
            };

            // *******************************************************************************************************************//

            $scope.update=function(size,person_id,voucher_id,receipt_date,receipt_time,receipt_location,status){
                $rootScope.clearToastr();
                var person_vouchers_data ={};
                if(person_id == 0 && voucher_id == 0 && receipt_date == 0 && receipt_location == 0 && receipt_time ==0){
                    var persons = [];
                    angular.forEach($scope.items, function (v, k) {
                        if (v.check) {
                            persons.push(v);
                        }
                    });

                    if(persons.length==0){
                        $rootScope.toastrMessages('error',$filter('translate')('no person select to update'));
                    }else{
                        person_vouchers_data={
                            id:0,
                            voucher_id:0,
                            receipt_date:new Date(),
                            receipt_time:'',
                            status:'',
                            receipt_location:'',
                            persons:persons

                        };

                    }
                }
                else{
                    person_vouchers_data={
                        id:person_id,
                        voucher_id:voucher_id,
                        status:status,
                        receipt_date:new Date(receipt_date),
                        receipt_time: $filter('date')( new Date(receipt_time), 'HH:MM:ss', 'Asia/Gaza')
                        ,
                        receipt_location:receipt_location
                    };
                }

                if(person_id !=0 || persons.length !=0){

                    $rootScope.person_vouchers=person_vouchers_data;
                    $uibModal.open({
                        templateUrl: 'myModalContent.html',
                        backdrop  : 'static',
                        keyboard  : false,
                        windowClass: 'modal',
                        size: 'md',
                        controller: function ($rootScope,$scope, $modalInstance, $log,cs_persons_vouchers) {
                            $rootScope.model_error_status =false;

                            $scope.confirmUpdate = function (persons_vouchers) {
                                $rootScope.progressbar_start();
                                persons_vouchers.receipt_time = $filter('date')( new Date($scope.mytime), 'yyyy/MM/dd HH:MM:ss', 'Asia/Gaza') ;
                                $rootScope.clearToastr();
                                var target = new cs_persons_vouchers(persons_vouchers);
                                target.$updatePersonsVoucherDetails()
                                    .then(function (response) {
                                        $rootScope.progressbar_complete();
                                        if(response.status=='error' || response.status=='failed')
                                        {
                                            $rootScope.toastrMessages('error',response.msg);
                                        }
                                        else if(response.status=='failed_valid')
                                        {
                                            $scope.status1 =response.status;
                                            $scope.msg1 =response.msg;
                                        }
                                        else{
                                            $modalInstance.close();
                                            if(response.status=='success')
                                            {
                                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                                LoadBeneficiary({});
                                            }else{
                                                $rootScope.toastrMessages('error',response.msg);
                                            }


                                        }
                                    }, function(error) {
                                        $rootScope.progressbar_complete();
                                        $rootScope.toastrMessages('error',error.data.msg);
                                        $modalInstance.close();
                                    });
                            };

                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };


                            $scope.dateOptions = {
                                formatYear: 'yy',
                                startingDay: 0
                            };
                            $scope.formats = [ 'dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                            $scope.format = $scope.formats[0];
                            $scope.today = function() {
                                $scope.dt = new Date();
                            };
                            $scope.today();
                            $scope.clear = function() {
                                $scope.dt = null;
                            };
                            $scope.open4 = function($event) {
                                $event.preventDefault();
                                $event.stopPropagation();
                                $scope.popup4.opened = true;
                            };
                            $scope.popup4 = {
                                opened: false
                            };
                            if(receipt_time)
                                $scope.mytime = new Date('1970-01-01T' + receipt_time);
                            else
                                $scope.mytime = new Date();

                            $scope.displayTime = 'n/a';
                            $scope.$watch('mytime', function(newValue, oldValue) {
                                var hour = $scope.mytime.getHours()-($scope.mytime.getHours() >= 12 ? 12 : 0),
                                    period = $scope.mytime.getHours() >= 12 ? 'PM' : 'AM';
                                $scope.displayTime = hour+':'+$scope.mytime.getMinutes()+' '+period
                            })
                            $scope.hstep = 1;
                            $scope.mstep = 1;
                            $scope.options = {
                                hstep: [1, 2, 3],
                                mstep: [1, 2, 3]
                            };
                            $scope.ismeridian = true;
                            $scope.toggleMode = function() {
                                $scope.ismeridian = ! $scope.ismeridian;
                            };

                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };

                        },
                        resolve: {
                        }
                    });
                }

            };

            // *******************************************************************************************************************//

            $scope.updateData = function (target,id,value,item){

                if($rootScope.voucherStatus == 0) {
                    if(target =='status'){
                        $rootScope.clearToastr();
                        var persons = [];
                        var pass = true;

                        if(id ==0){
                            angular.forEach($scope.items, function (v, k) {
                               if (v.check) {
                                   if (!(parseInt(v.receipt_status) == parseInt(value))) {
                                        persons.push(v.id);
                                    }
                                }
                            });
                            if(persons.length==0){
                                pass = false;
                                $rootScope.toastrMessages('error',$filter('translate')('no person select to update'));
                                return;
                            }
                        }else{
                            persons.push(id);
                        }

                        if(pass) {
                            $rootScope.progressbar_start();
                            cs_persons_vouchers.setDetails({'voucher_id':$stateParams.id ,status:value, target:persons,'source':'id'},function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    $rootScope.All = false;
                                    $rootScope.selectedAll = false;
                                    if(response.status=='success')
                                    {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        LoadBeneficiary({});
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }


                                }
                            });
                        }
                    }
                    else if(target =='all'){
                        $rootScope.clearToastr();
                        cs_persons_vouchers.setDetails({'voucher_id':$stateParams.id,'status':value,'target':persons,'source':'voucher'},
                                            function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid')
                            {
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                $rootScope.All = false;
                                $rootScope.selectedAll = false;
                                if(response.status=='success')
                                {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    LoadBeneficiary({});
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }
                        });

                    }
                    else{
                        if(value == 'manually'){
                            $rootScope.clearToastr();
                            var persons = [];
                            var pass = true;

                            if(id ==0){
                                angular.forEach($scope.items, function (v, k) {
                                    if (v.check) {
                                        persons.push(v.id);
                                    }
                                });
                                if(persons.length==0){
                                    pass = false;
                                    $rootScope.toastrMessages('error',$filter('translate')('no person select to update'));
                                    return;
                                }
                            }else{
                                persons.push(id);
                            }

                            if(pass) {
                                $uibModal.open({
                                    templateUrl: 'myModalContent.html',
                                    backdrop  : 'static',
                                    keyboard  : false,
                                    windowClass: 'modal',
                                    size: 'md',
                                    controller: function ($rootScope,$scope, $modalInstance, $log,cs_persons_vouchers) {
                                        $rootScope.model_error_status =false;
                                        $rootScope.has_update =false;
                                        $rootScope.person_vouchers={'voucher_id':$stateParams.id, target:persons,'source':'id'};

                                        $scope.confirmUpdate = function (params) {

                                            var row = angular.copy(params);
                                            if(angular.isUndefined(row.receipt_time) && angular.isUndefined(row.receipt_date) &&
                                                angular.isUndefined(row.receipt_location)) {
                                                $rootScope.toastrMessages('error',$filter('translate')('no data to update'));
                                                return;
                                            }

                                            if(angular.isUndefined(row.receipt_time)) {
                                                row.receipt_time = $filter('date')( new Date($scope.mytime), 'yyyy/MM/dd HH:MM:ss', 'Asia/Gaza') ;
                                            }

                                            if( !angular.isUndefined(row.receipt_date)) {
                                                row.receipt_date=$filter('date')(row.receipt_date, 'dd-MM-yyyy')
                                            }

                                            $rootScope.progressbar_start();
                                            cs_persons_vouchers.setDetails(row,function (response) {
                                                $rootScope.progressbar_complete();
                                                if(response.status=='error' || response.status=='failed')
                                                {
                                                    $rootScope.toastrMessages('error',response.msg);
                                                }
                                                else if(response.status=='failed_valid')
                                                {
                                                    $scope.status1 =response.status;
                                                    $scope.msg1 =response.msg;
                                                }
                                                else{
                                                    $rootScope.All = false;
                                                    $rootScope.selectedAll = false;

                                                    $modalInstance.close();
                                                    if(response.status=='success')
                                                    {
                                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                                        LoadBeneficiary({});
                                                    }else{
                                                        $rootScope.toastrMessages('error',response.msg);
                                                    }
                                                }
                                            });
                                        };

                                        $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
                                        $scope.formats = [ 'dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                                        $scope.format = $scope.formats[0];
                                        $scope.today = function() {$scope.dt = new Date();};
                                        $scope.today();
                                        $scope.clear = function() {$scope.dt = null;};
                                        $scope.open4 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup4.opened = true;};
                                        $scope.popup4 = {opened: false};
                                        // $scope.mytime = new Date();
                                        $scope.displayTime = 'n/a';
                                        $scope.$watch('mytime', function(newValue, oldValue) {
                                            if(!angular.isUndefined($scope.mytime)) {
                                                var hour = $scope.mytime.getHours()-($scope.mytime.getHours() >= 12 ? 12 : 0),
                                                    period = $scope.mytime.getHours() >= 12 ? 'PM' : 'AM';
                                                $scope.displayTime = hour+':'+$scope.mytime.getMinutes()+' '+period
                                            }
                                        });

                                        $scope.hstep = 1;
                                        $scope.mstep = 1;
                                        $scope.options = {hstep: [1, 2, 3], mstep: [1, 2, 3]};
                                        $scope.ismeridian = true;
                                        $scope.toggleMode = function() {$scope.ismeridian = ! $scope.ismeridian;};

                                        $scope.cancel = function () {
                                            $modalInstance.dismiss('cancel');
                                        };

                                    },
                                    resolve: {
                                    }
                                });

                            }


                        }
                        else{
                            $uibModal.open({
                                templateUrl: 'update_beneficiary.html',
                                backdrop  : 'static',
                                keyboard  : false,
                                windowClass: 'modal',
                                size: 'md',
                                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                                    $scope.download=function(){
                                        if($rootScope.beneficiary == 1){
                                            window.location = '/api/v1.0/common/files/xlsx/export?download_token=update-persons-receipt-data-template&template=true';
                                        }else{
                                            window.location = '/api/v1.0/common/files/xlsx/export?download_token=update-individual-receipt-data-template&template=true';
                                        }
                                    };
                                    var Uploader = $scope.uploader = new FileUploader({
                                        url: '/api/v1.0/aids/cs_persons_vouchers/setDetails',
                                        removeAfterUpload: false,
                                        queueLimit: 1,
                                        headers: {
                                            Authorization: OAuthToken.getAuthorizationHeader()
                                        }
                                    });

                                    $scope.fileupload=function(){
                                        $scope.uploader.destroy();
                                        $scope.uploader.queue=[];
                                        $scope.upload=true;
                                    };
                                    $scope.confirm = function () {
                                        // // angular.element('.btn').addClass("disabled");
                                        $rootScope.progressbar_start();
                                        $rootScope.clearToastr();
                                        $scope.spinner=false;
                                        $scope.import=false;
                                        $rootScope.progressbar_start();
                                        Uploader.onBeforeUploadItem = function(item) {
                                            item.formData = [{target:[],'source':'xlx','voucher_id':$stateParams.id}];
                                        };

                                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                                            $rootScope.progressbar_complete();
                                            // angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                                            $rootScope.progressbar_complete();

                                            $scope.uploader.destroy();
                                            $scope.uploader.queue=[];
                                            $scope.uploader.clearQueue();
                                            angular.element("input[type='file']").val(null);

                                            if(response.download_token){
                                                window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token;
                                            }
                                            if(response.status=='success')
                                            {
                                                $scope.spinner=false;
                                                $scope.upload=false;
                                                angular.element("input[type='file']").val(null);
                                                $scope.import=false;
                                                $rootScope.toastrMessages('success',response.msg);
                                                LoadBeneficiary({});
                                                $modalInstance.close();
                                            }else{
                                                $rootScope.toastrMessages('error',response.msg);
                                                $scope.upload=false;
                                                angular.element("input[type='file']").val(null);
                                                LoadBeneficiary({});
                                                $modalInstance.close();
                                            }

                                        };

                                        Uploader.uploadAll();
                                    };

                                    $scope.cancel = function () {
                                        $modalInstance.dismiss('cancel');
                                    };
                                }
                            });
                        }

                    }
                }

            }

            // *******************************************************************************************************************//
            LoadBeneficiary({});

            $scope.ChooseAttachment=function(item,action){

                $rootScope.clearToastr();
                $uibModal.open({
                    templateUrl: 'file_upload.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'md',
                    controller: function ($rootScope,$scope, $modalInstance, $log,cases,OAuthToken, FileUploader) {

                        $scope.fileupload = function () {
                            if($scope.uploader.queue.length){
                                var image = $scope.uploader.queue.slice(1);
                                $scope.uploader.queue = image;
                            }
                        };
                        var Uploader = $scope.uploader = new FileUploader({
                            url: '/api/v1.0/doc/files',
                            headers: {
                                Authorization: OAuthToken.getAuthorizationHeader()
                            }
                        });
                        Uploader.onBeforeUploadItem = function(item) {
                            $rootScope.clearToastr();
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);

                            if(response.status=='error')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else{

                                fileItem.id = response.id;
                                cs_persons_vouchers.setAttachment({cur_id:item.id,action:action,document_id:fileItem.id},function (response2) {
                                    if(response2.status=='error' || response2.status=='failed')
                                    {
                                        $rootScope.toastrMessages('error',response2.msg);
                                    }
                                    else if(response2.status== 'success'){

                                        $modalInstance.close();
                                        $rootScope.toastrMessages('success',response2.msg);
                                        LoadBeneficiary({});
                                    }

                                }, function(error) {
                                    $rootScope.toastrMessages('error',error.data.msg);
                                });
                            }
                        };


                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    }
                });
            };

            $scope.voucherFiles = function () {

                $rootScope.clearToastr();
                $rootScope.progressbar_start();

                cs_persons_vouchers.getBeneficiaryAttachments({id:$stateParams.id},function (response) {
                    $rootScope.progressbar_complete();
                    $rootScope.voucherStatus= response.status;
                    $rootScope.progressbar_complete();
                    if(response.status == false){
                        $rootScope.toastrMessages('error',$filter('translate')('no attachment to export'));
                    }else{
                        window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token;
                    }
                });

            };

        }

    });
