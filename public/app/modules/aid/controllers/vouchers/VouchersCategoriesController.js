
angular.module('AidModule')
    .controller('VouchersCategoriesController' ,function($stateParams,$rootScope, $scope, $http ,$timeout, $uibModal,$ngBootbox, $log,FileUploader, OAuthToken,vouchers_categories,$state,$filter) {

        $state.current.data.pageTitle = $filter('translate')('vouchers-categories');

        $rootScope.failed =false;
        $scope.itemsCount = 50;
        $scope.items= [];
        $rootScope.status = $stateParams.status;
        $rootScope.mode = $stateParams.mode;
        $rootScope.msg ='';

        VouchersCategories(1);

        $rootScope.pageChanged = function (CurrentPage) {
            VouchersCategories(CurrentPage);
        };

        $rootScope.itemsPerPage_ = function (itemsCount) {

            VouchersCategories(1,itemsCount);
        };

        $scope.new = function (size) {
            $rootScope.action='out';
            $rootScope.category = {name: ''};
            $uibModal.open({
                templateUrl: 'Content.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope, $modalInstance, $log,vouchers_categories) {
                    $scope.msg1={};

                    $scope.setParent = function (value) {
                        if( !angular.isUndefined($scope.msg1.type)) {
                            $scope.msg1.type = [];
                        }
                        if( !angular.isUndefined($scope.msg1.parent)) {
                            $scope.msg1.parent = [];
                        }

                        if(value ==1 ){
                            vouchers_categories.getParentsCategory(function (response) {
                                $scope.Parent = response;
                            });
                        }
                    };

                    $scope.confirm = function (item) {
                        item.page=$scope.CurrentPage;
                        $rootScope.progressbar_start();
                        var cat = new vouchers_categories(item);
                        cat.$save()
                            .then(function (response) {
                                $rootScope.progressbar_complete();

                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    $modalInstance.close();
                                    if(response.status=='success')
                                    {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        VouchersCategories($scope.CurrentPage);
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }


                                }

                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();

                            });

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

        $scope.delete = function (size,id) {
            $rootScope.action='delete';
            $rootScope.category=id;

            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                .then(function() {
                    $rootScope.clearToastr();
                    $rootScope.progressbar_start();
                    vouchers_categories.delete({id:id},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success')
                            {
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                VouchersCategories($scope.CurrentPage);
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }

                        });
                });


        };

        $scope.edit= function (size,id) {
            $rootScope.clearToastr();
            $rootScope.action='edit';
            var target= new vouchers_categories(id);
            target.$get({id:id})
                .then(function (response) {
                    $rootScope.category = response;

                    $uibModal.open({
                        templateUrl: 'myModalContent.html',
                        backdrop  : 'static',
                        keyboard  : false,
                        windowClass: 'modal',
                        size: size,
                        controller: function ($rootScope,$scope, $modalInstance, $log,vouchers_categories) {


                            $scope.confirm = function (item) {
                                item.page=$scope.CurrentPage;
                                $rootScope.progressbar_start();
                                var edit = new vouchers_categories(item);
                                edit.$update()
                                    .then(function (response) {
                                        $rootScope.progressbar_complete();
                                        if(response.status=='error' || response.status=='failed')
                                        {
                                            $rootScope.toastrMessages('error',response.msg);
                                        }
                                        else if(response.status=='failed_valid')
                                        {
                                            $scope.status1 =response.status;
                                            $scope.msg1 =response.msg;
                                        }
                                        else{
                                            $modalInstance.close();
                                            if(response.status=='success')
                                            {
                                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                                VouchersCategories($scope.CurrentPage);
                                            }else{
                                                $rootScope.toastrMessages('error',response.msg);
                                            }


                                        }
                                    }, function(error) {
                                        $rootScope.progressbar_complete();
                                        $modalInstance.close();
                                        $rootScope.toastrMessages('success',error.data.msg);
                                    });
                            };
                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };
                        }
                    });
                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                    $modalInstance.close();

                });

        };

        $scope.getSub = function (size,id) {
            $rootScope.clearToastr();
            $rootScope.ValidForm=false;
            $rootScope.Parent=id;
            $rootScope.hasSub=true;

            var target = new vouchers_categories(id);
            target.$getSiblingsCategory({'id':id})
                .then(function (response) {
                    $rootScope.Subs =response.data;
                    if($rootScope.Subs.length == 0){
                        $rootScope.hasSub=false;
                    }

                    $uibModal.open({
                        templateUrl: 'SubCat.html',
                        backdrop  : 'static',
                        keyboard  : false,
                        windowClass: 'modal',
                        size: size,
                        controller: function ($rootScope,$scope, $modalInstance, $log) {
                            $scope.CheckChange=function(name){
                                if(name =="" || name==null){
                                    $rootScope.ValidForm=false;
                                }else{
                                    $rootScope.ValidForm=true;
                                }
                            };


                            $scope.confirm = function (data) {
                                $rootScope.clearToastr();

                                $rootScope.progressbar_start();
                                vouchers_categories.updateSub({'id':$rootScope.Parent,'Sub':data},function (response) {
                                    $rootScope.progressbar_complete();
                                    if(response.status=='error' || response.status=='failed')
                                        {
                                            $rootScope.toastrMessages('error',response.msg);
                                        }
                                        else{
                                            $modalInstance.close();
                                            if(response.status=='success')
                                            {
                                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                            }else{
                                                $rootScope.toastrMessages('error',response.msg);
                                            }

                                        }
                                    }, function(error) {
                                    $rootScope.progressbar_complete();
                                    $rootScope.toastrMessages('error',error.data.msg);
                                        $modalInstance.close();

                                    });
                            };

                            $scope.delete = function (size,id) {
                                $rootScope.clearToastr();
                                $rootScope.action='delete';
                                $rootScope.category=id;

                                $rootScope.clearToastr();
                                $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                                    .then(function() {
                                        $rootScope.progressbar_start();
                                        $rootScope.clearToastr();
                                        vouchers_categories.delete({id:id,page:$scope.CurrentPage},function (response) {
                                            $rootScope.progressbar_complete();
                                            if(response.status=='success')
                                                {
                                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                                    vouchers_categories.getSiblingsCategory({'id':$rootScope.Parent},function (response) {
                                                            $rootScope.Subs =response.data;
                                                            if($rootScope.Subs.length == 0){
                                                                $rootScope.hasSub=false;
                                                            }
                                                        });
                                                }else{
                                                    $rootScope.toastrMessages('error',response.msg);
                                                }
                                            });
                                    });

                            };
                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };
                        }
                    });

                }, function(error) {
                    $rootScope.toastrMessages('error',error.data.msg);
                });

        };

        $scope.addSub = function (size,id) {
            $rootScope.clearToastr();
            $rootScope.action='sub';
            $rootScope.category = {'parent':id,type:1,name: ''};
            $uibModal.open({
                templateUrl: 'Content.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope, $modalInstance, $log,vouchers_categories) {
                    $scope.msg1={};

                    $scope.setParent = function (value) {
                        if( !angular.isUndefined($scope.msg1.type)) {
                            $scope.msg1.type = [];
                        }
                        if( !angular.isUndefined($scope.msg1.parent)) {
                            $scope.msg1.parent = [];
                        }

                        if(value ==1 ){
                            vouchers_categories.getParentsCategory(function (response) {
                                $scope.Parent = response;
                            });
                        }
                    };

                    $scope.confirm = function (item) {
                        $rootScope.clearToastr();
                        item.page=$scope.CurrentPage;
                        $rootScope.progressbar_start();
                        var cat = new vouchers_categories(item);
                        cat.$save()
                            .then(function (response) {

                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    $modalInstance.close();
                                    if(response.status=='success')
                                    {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                               }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();
                            });

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

        $scope.ChooseTemplate=function(id,action){
            $rootScope.category=id;
            $rootScope.action=action;

            $scope.f=false;
            $uibModal.open({
                templateUrl: 'myModalContent2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,vouchers_categories,OAuthToken, FileUploader) {

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/aids/vouchers_categories/template-upload',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    Uploader.onBeforeUploadItem = function(item) {
                        item.formData = [{category_id: $rootScope.category}];
                        $rootScope.progressbar_start();
                    };

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        VouchersCategories($scope.CurrentPage);
                        $modalInstance.close();

                    };


                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                },
                resolve: {

                }
            });


        };

        function VouchersCategories(CurrentPage,itemsCount){
            var itemsCount = (itemsCount!=undefined)?itemsCount:$scope.itemsCount;
            $rootScope.progressbar_start();
            vouchers_categories.query({page:CurrentPage,itemsCount:itemsCount} ,function (response) {
                $rootScope.progressbar_complete();
                $scope.items= response.data;
                    $scope.CurrentPage = response.current_page;
                    $rootScope.TotalItems = response.total;
                    $rootScope.ItemsPerPage = response.per_page;
            });
        }

    });

