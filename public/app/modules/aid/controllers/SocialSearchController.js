
angular.module('AidModule')
    .controller('SocialSearchController' ,function($state,$rootScope,category, $ngBootbox,$scope, $http, $timeout, $uibModal, $log,social_search,$filter,FileUploader, OAuthToken,Org) {


        $rootScope.labels = {
            "itemsSelected": $filter('translate')('itemsSelected'),
            "selectAll": $filter('translate')('selectAll'),
            "unselectAll": $filter('translate')('unselectAll'),
            "search": $filter('translate')('search'),
            "select": $filter('translate')('select')
        }

        $rootScope.clearToastr();
        $state.current.data.pageTitle = $filter('translate')('manage') + ' '+ $filter('translate')('Social Search');

        $scope.master=false;
        $rootScope.items=[];
        $scope.items=[];
        $scope.CurrentPage=1;
        $scope.itemsCount = 50;
        $scope.filters={"all_organization":false,
            'page':1,'action':"paginate",'title':"", 'notes':"", 'created_at_to':"", 'created_at_from':"",
            'begin_date_from':"", 'end_date_from':"", 'begin_date_to':"", 'end_date_to':""};

        $rootScope.aidCategories_=[];
        category.getSectionForEachCategory({'type':'aids'},function (response) {
            if(!angular.isUndefined(response)){
                $rootScope.aidCategories_firstStep_map =[];
                $rootScope.aidCategories = response.Categories;
                if($rootScope.aidCategories.length > 0){
                    for (var i in $rootScope.aidCategories) {
                        var item = $rootScope.aidCategories[i];
                        $rootScope.aidCategories_firstStep_map[item.id] = item.FirstStep;
                    }
                    angular.forEach($rootScope.aidCategories, function(v, k) {
                        if ($rootScope.lang == 'ar'){
                            $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
                        }
                        else{
                            $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
                        }
                    });
                }
            }
        });

        var LoadSocialSearch=function(params){
            // $rootScope.clearToastr();
            if( !angular.isUndefined(params.begin_date_from)) {
                params.begin_date_from=$filter('date')(params.begin_date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.end_date_from)) {
                params.end_date_from= $filter('date')(params.end_date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.begin_date_to)) {
                params.begin_date_to=$filter('date')(params.begin_date_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.end_date_to)) {
                params.end_date_to= $filter('date')(params.end_date_to, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(params.created_at_to)) {
                params.created_at_to=$filter('date')(params.created_at_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.created_at_from)) {
                params.created_at_from= $filter('date')(params.created_at_from, 'dd-MM-yyyy')
            }
            // angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();
            params.trashed = false;
            social_search.filter(params,function (response) {
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');

                $rootScope.progressbar_complete();
                if(params.action == 'paginate'){
                    $scope.items= response.items.data;
                    $scope.CurrentPage = response.items.current_page;
                    $rootScope.TotalItems = response.items.total;
                    $rootScope.ItemsPerPage = response.items.per_page;
                }else{
                    if(!angular.isUndefined(response.download_token)) {
                        $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                        var file_type='xlsx';
                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                    }
                }
            });

        };

        var reLoadSocialSearch = function () {
            var params = angular.copy($scope.filters);
            params.page=$scope.CurrentPage;
            params.action='paginate';
            params.itemsCount= $scope.itemsCount ;
            LoadSocialSearch(params);
        };

        Org.list({type:'sponsors'},function (response) { $rootScope.Sponsor = response; });
        Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

        $scope.resetSearchFilter =function(){
            $scope.filters={"all_organization":false,
                'page':1,'action':"paginate",'title':"", 'notes':"", 'created_at_to':"", 'created_at_from':"",
                'begin_date_from':"", 'end_date_from':"", 'begin_date_to':"", 'end_date_to':""};
        };

        $scope.pageChanged = function (currentPage) {
            $scope.CurrentPage=currentPage;
            reLoadSocialSearch();
        };

        $scope.itemsCount = '50';
        $scope.itemsPerPage_ = function (itemsCount) {
            $scope.CurrentPage=1;
            $scope.itemsCount = itemsCount ;
            reLoadSocialSearch();
        };
        
        $scope.selectedAll = false;
        $scope.selectAll=function(value){
                if (value) {
                    $scope.selectedAll = true;
                } else {
                    $scope.selectedAll = false;
                }

                angular.forEach($scope.items, function(v, k) {
                    v.check=$scope.selectedAll;
                });
        };

        $scope.searchReports = function (selected,filter,action) {
            $scope.CurrentPage=1;
            filter.action = action;
            filter.items=[];

            if(selected == true || selected == 'true' ){
            
                var items =[];
                 angular.forEach($scope.items, function(v, k){

                      if(action == 'ExportToWord' || action == 'ExportToPDF'){
                         if(v.check && v.beneficiary > 0){
                             items.push(v.id);
                         } 
                      }else{
                          if(v.check){
                             items.push(v.id);
                         }
                      }


                 });

                 if(items.length==0){
                     $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                     return ;
                 }
                 filter.items=items;
            }
            filter.page = 1;
            LoadSocialSearch(filter);
        };

        $scope.delete = function (id) {
            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                .then(function() {
                    $rootScope.progressbar_start();
                    social_search.delete({id:id},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            reLoadSocialSearch();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                });

        };

        $scope.refresh = function (id) {
            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to update cases data according this social search') )
                .then(function() {
                    $rootScope.progressbar_start();
                    social_search.refresh({id:id},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            reLoadSocialSearch();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                });

        };

        $scope.status=function(id,status,index) {
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            social_search.status({'action':'single',target:id,status:status },function (response) {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                if(response.status == 'success') {
                    $scope.items[index].status = status;
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }

            });
        };

        $scope.createOrUpdate = function (item) {
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: '/app/modules/aid/views/social-search/model/form.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,social_search) {

                    Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

                    $scope.row={};

                    if(item == 'null' || item != null){
                        $scope.row = angular.copy(item);
                        $scope.row.status = $scope.row.status +"";
                        $scope.row.close = $scope.row.close +"";
                    }

                    $scope.confirm = function (item_) {
                        $rootScope.clearToastr();

                        var row = angular.copy(item_);

                        if( !angular.isUndefined(row.date_from)) {
                            row.date_from=$filter('date')(row.date_from, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(row.date_to)) {
                            row.date_to= $filter('date')(row.date_to, 'dd-MM-yyyy')
                        }

                        $rootScope.progressbar_start();
                        if(row.id){
                            social_search.update(row,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    $modalInstance.close();
                                    if(response.status=='success') {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        reLoadSocialSearch();
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }

                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();

                            });
                        }else {
                            social_search.save(row,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    $modalInstance.close();
                                    if(response.status=='success') {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        reLoadSocialSearch();
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }

                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();

                            });
                        }

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    var today = new Date();
                    $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
                    $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};
                    $scope.popup={0:{},1:{opened:false},2:{opened:false}};
                    $scope.open=function (target,$event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        if(target == 1){ $scope.popup[1].opened = true;  }
                        else if(target == 2) { $scope.popup[2].opened = true; }
                    };
                }
            });
        };

        $scope.duplicate = function (item) {
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: '/app/modules/aid/views/social-search/model/duplicate.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'md',
                controller: function ($rootScope,$scope, $modalInstance, $log,social_search) {

                    $scope.row = angular.copy(item);

                    $scope.confirm = function (item_) {

                        var row = angular.copy(item_);
                        $rootScope.clearToastr();
                        if(row.id){
                            row.duplicate_id=row.id;
                            delete row.id;
                        }

                        if( !angular.isUndefined(row.date_from)) {
                            row.date_from=$filter('date')(row.date_from, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(row.date_to)) {
                            row.date_to= $filter('date')(row.date_to, 'dd-MM-yyyy')
                        }

                        $rootScope.progressbar_start();
                        social_search.save(row,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                $modalInstance.close();
                                if(response.status=='success') {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    reLoadSocialSearch();
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }

                            }
                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                            $modalInstance.close();

                        });
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    var today = new Date();
                    $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
                    $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};
                    $scope.popup={0:{},1:{opened:false},2:{opened:false}};
                    $scope.open=function (target,$event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        if(target == 1){ $scope.popup[1].opened = true;  }
                        else if(target == 2) { $scope.popup[2].opened = true; }
                    };
                }
            });
        };

        $scope.import=function(search_id,target){
            var page = $scope.CurrentPage;
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'import.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'md',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;
                    $scope.row={search_id:search_id};

                    if(target == 'banks'){
                        $scope.model_title = $filter('translate')('import banks');
                    }else if(target == 'essentials'){
                        $scope.model_title = $filter('translate')('import essentials');
                    }else if(target == 'fin-aids'){
                        $scope.model_title = $filter('translate')('import financial data');
                    }else if(target == 'non-fin-aids'){
                        $scope.model_title = $filter('translate')('import nonfinancial data');
                    }else if(target == 'properties'){
                        $scope.model_title = $filter('translate')('import properties');
                    }else if(target == 'family'){
                        $scope.model_title = $filter('translate')('import family member');
                    }else{
                        $scope.model_title = $filter('translate')('import cases');
                    }

                    var url = '/api/v1.0/aids/social_search/import-' + target;

                    $scope.RestUpload=function(value){
                        $scope.upload=false;
                    };

                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };

                    $scope.download=function(){

                        var template ='';
                        if(target == 'banks'){
                            template = 'import-to-social-search-cases-banks-template';
                        }else if(target == 'essentials'){
                            template = 'import-to-social-search-cases-essentials-template';
                        }
                        else if(target == 'fin-aids'){
                            template = 'import-to-social-search-cases-aids-template';
                        }else if(target == 'non-fin-aids'){
                            template = 'import-to-social-search-cases-aids-template';
                        }else if(target == 'properties'){
                            template = 'import-to-social-search-cases-properties-template';
                        }else if(target == 'family'){
                            template = 'import-family-to-social-search-template';
                        }else{
                            template = 'import-to-social-search-template';
                        }

                        window.location = '/api/v1.0/common/files/xlsx/export?download_token='+ template +'&template=true';
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: url,
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    Uploader.onBeforeUploadItem = function(item) {
                        item.formData = [{}];
                    };

                    $scope.confirm = function () {
                        $rootScope.progressbar_start();
                        // angular.element('.btn').addClass("disabled");

                        $scope.spinner=false;
                        $scope.import=false;

                        Uploader.onBeforeUploadItem = function(i) {
                            i.formData = [{search_id: search_id}];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);
                           // angular.element('.btn').removeClass("disabled");
                          //  angular.element('.btn').removeAttr('disabled');

                            if(response.download_token){
                                window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token;
                            }

                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            else if(response.status=='success')
                            {
                                $scope.spinner=false;
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $scope.import=false;
                                $rootScope.toastrMessages('success',response.msg);
                                reLoadSocialSearch();
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                reLoadSocialSearch();
                                $modalInstance.close();
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});

        };
        //-----------------------------------------------------------------------------------------------------------
        $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {$scope.dt = new Date();};
        $scope.today();
        $scope.clear = function() {$scope.dt = null;};
        $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
        $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};

        $scope.popup={0:{},1:{opened:false},2:{opened:false},3:{opened:false},4:{opened:false},5:{opened:false},6:{opened:false}};
        $scope.open=function (target,$event) {
            $event.preventDefault();
            $event.stopPropagation();
            if(target == 1){ $scope.popup[1].opened = true;  }
            else if(target == 2) { $scope.popup[2].opened = true; }
            else if(target == 3) { $scope.popup[3].opened = true; }
            else if(target == 4) { $scope.popup[4].opened = true; }
            else if(target == 5) { $scope.popup[5].opened = true; }
            else if(target == 6) { $scope.popup[6].opened = true; }
        };

        reLoadSocialSearch();

    });

angular.module('AidModule')
    .controller('TrashedSocialSearchController' ,function($state,$rootScope, $ngBootbox,$scope, $http, $timeout, $uibModal, $log,social_search,$filter,FileUploader, OAuthToken,Org) {

        $rootScope.clearToastr();
        $state.current.data.pageTitle = $filter('translate')('Trashed Social Search');

        $scope.master=false;
        $rootScope.items=[];
        $scope.items=[];
        $scope.CurrentPage=1;
        $scope.itemsCount = 50;
        $scope.filters={"all_organization":false,
            'page':1,'action':"paginate",'title':"", 'notes':"", 'created_at_to':"", 'created_at_from':"",
            'begin_date_from':"", 'end_date_from':"", 'begin_date_to':"", 'end_date_to':""};

        $rootScope.aidCategories_=[];

        angular.forEach($rootScope.aidCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        Org.list({type:'sponsors'},function (response) { $rootScope.Sponsor = response; });
        Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

        var LoadSocialSearch=function(params){
            // $rootScope.clearToastr();
            if( !angular.isUndefined(params.begin_date_from)) {
                params.begin_date_from=$filter('date')(params.begin_date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.end_date_from)) {
                params.end_date_from= $filter('date')(params.end_date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.begin_date_to)) {
                params.begin_date_to=$filter('date')(params.begin_date_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.end_date_to)) {
                params.end_date_to= $filter('date')(params.end_date_to, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(params.created_at_to)) {
                params.created_at_to=$filter('date')(params.created_at_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.created_at_from)) {
                params.created_at_from= $filter('date')(params.created_at_from, 'dd-MM-yyyy')
            }
            // // angular.element('.btn').addClass("disabled");

            params.organization_ids=[];
            if( !angular.isUndefined(params.all_organization)) {
                if(params.all_organization == true ||params.all_organization == 'true'){
                    angular.forEach(params.organization_id, function(v, k) {
                        params.organization_ids.push(v.id);
                    });
                }
            }

            $rootScope.progressbar_start();
            params.trashed = true;
            social_search.filter(params,function (response) {
                // angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');

                $rootScope.progressbar_complete();
                if(params.action == 'paginate'){
                    $scope.items= response.items.data;
                    $scope.CurrentPage = response.items.current_page;
                    $rootScope.TotalItems = response.items.total;
                    $rootScope.ItemsPerPage = response.items.per_page;
                }else{
                    if(!angular.isUndefined(response.download_token)) {
                        $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                        var file_type='xlsx';
                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                    }
                }
            });

        };

        var reLoadSocialSearch = function () {
            var params = angular.copy($scope.filters);
            params.page=$scope.CurrentPage;
            params.action='paginate';
            params.itemsCount= $scope.itemsCount ;
            LoadSocialSearch(params);
        };

        $scope.resetSearchFilter =function(){
            $scope.filters={"all_organization":false,
                'page':1,'action':"paginate",'title':"", 'notes':"", 'created_at_to':"", 'created_at_from':"",
                'begin_date_from':"", 'end_date_from':"", 'begin_date_to':"", 'end_date_to':""};
        };

        $scope.pageChanged = function (currentPage) {
            $scope.CurrentPage=currentPage;
            reLoadSocialSearch();
        };

        $scope.itemsCount = '50';
        $scope.itemsPerPage_ = function (itemsCount) {
            $scope.CurrentPage=1;
            $scope.itemsCount = itemsCount ;
            reLoadSocialSearch();
        };

        $scope.selectedAll = false;
        $scope.selectAll=function(value){
                if (value) {
                    $scope.selectedAll = true;
                } else {
                    $scope.selectedAll = false;
                }

                angular.forEach($scope.items, function(v, k) {
                    v.check=$scope.selectedAll;
                });
        };
        $scope.searchReports = function (selected,filter,action) {
            $scope.CurrentPage=1;
            filter.action = action;
            filter.items=[];

            if(selected == true || selected == 'true' ){
            
                var items =[];
                 angular.forEach($scope.items, function(v, k){

                      if(action == 'ExportToWord' || action == 'ExportToPDF'){
                         if(v.check && v.beneficiary > 0){
                             items.push(v.id);
                         } 
                      }else{
                          if(v.check){
                             items.push(v.id);
                         }
                      }


                 });

                 if(items.length==0){
                     $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                     return ;
                 }
                 filter.items=items;
             }
             filter.page = 1;

             LoadSocialSearch(filter);
    };

        // delete social search by id
        $scope.delete = function (id) {
            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                .then(function() {
                    $rootScope.progressbar_start();
                    social_search.delete({id:id},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            reLoadSocialSearch();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                });

        };

        // restore social search by id
        $scope.restore = function (id) {
            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to confirm restore') )
                .then(function() {
                    $rootScope.progressbar_start();
                    social_search.restore({id: id}, function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            reLoadSocialSearch();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                });
        };

        //-----------------------------------------------------------------------------------------------------------
        $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {$scope.dt = new Date();};
        $scope.today();
        $scope.clear = function() {$scope.dt = null;};
        $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
        $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};

        $scope.popup={0:{},1:{opened:false},2:{opened:false},3:{opened:false},4:{opened:false},5:{opened:false},6:{opened:false}};
        $scope.open=function (target,$event) {
            $event.preventDefault();
            $event.stopPropagation();
            if(target == 1){ $scope.popup[1].opened = true;  }
            else if(target == 2) { $scope.popup[2].opened = true; }
            else if(target == 3) { $scope.popup[3].opened = true; }
            else if(target == 4) { $scope.popup[4].opened = true; }
            else if(target == 5) { $scope.popup[5].opened = true; }
            else if(target == 6) { $scope.popup[6].opened = true; }
        };

        reLoadSocialSearch();

    });

angular.module('AidModule').controller('SocialSearchCasesController',
                   function($rootScope,$scope,$filter,$stateParams,$uibModal,$http,$timeout,$state,
                                             Org,Entity,social_search) {

    $rootScope.clearToastr();
    var AuthUser =localStorage.getItem("charity_LoggedInUser");
    $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
    $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
    if($rootScope.charity_LoggedInUser.organization != null) {
        $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
    }
    $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;

    $rootScope.settings.layout.pageSidebarClosed = true;

    $state.current.data.pageTitle = $filter('translate')('Social Search Report');

    $scope.infoCollapsed  = $scope.banksCollapsed     =
    $scope.healthCollapsed = $scope.worksCollapsed = $scope.contactsCollapsed  =
    $scope.residenceCollapsed = $scope.homeIndoorCollapsed =
    $scope.othersCollapsed= $scope.aidSourceCollapsed=
    $scope.propertiesCollapsed=$scope.socialSearchCollapsed=true;
    $scope.items=[];

    $scope.master=false;
    $scope.PropertiesFilter=[];
    $scope.pro_id=[];
    $scope.AidSourceFilter=[];
    $scope.aid_id=[];
    $rootScope.person_restrict=false;
    $rootScope.card_restrict=false;
    $rootScope.restricted=[];
    $rootScope.invalid_cards=[];

    $rootScope.status ='';
    $rootScope.msg ='';
    $scope.toggleStatus = true;
    $scope.search_title = null;
    $rootScope.mode=$stateParams.mode;


    var resetFilter =function(){
        $rootScope.filters={
            'action':"filter",
            "Properties":[],
            "all_organization":false,
            "order_by_rank":"",
            "status":"",
            "date_from":"",
            "date_to":"",
            "first_name":"",
            "second_name":"",
            "third_name":"",
            "last_name":"",
            "en_first_name":"",
            "en_second_name":"",
            "en_third_name":"",
            "en_last_name":"",
            "category_id":"",
            "id_card_number":"",
            "gender":"",
            "marital_status_id":"",
            "birthday_from":"",
            "birthday_to":"",
            "birth_place":"",
            "nationality":"",
            "refugee":"",
            "unrwa_card_number":"",
            "adscountry_id":"",
            "adsdistrict_id":"",
            "adsregion_id":"",
            "adsneighborhood_id":"",
            "adssquare_id":"",
            "adsmosque_id":"",
            "person_country":"",
            "person_governarate":"",
            "person_city":"",
            "location_id":"",
            "mosque_id":"",
            "street_address":"",
            "monthly_income_from":"",
            "monthly_income_to":"",
            "primary_mobile":"",
            "secondery_mobile":"",
            "phone":"",
            "bank_id":"",
            "branch_name":"",
            "account_number":"",
            "account_owner":"",
            "property_type_id":"",
            "roof_material_id":"",
            "residence_condition":"",
            "indoor_condition":"",
            "min_rooms":"",
            "max_rooms":"",
            "min_area":"",
            "max_area":"",
            "habitable":"",
            "house_condition":"",
            "rent_value":"",
            "need_repair":"",
            "repair_notes":"",
            "working":"",
            "can_work":"",
            "work_reason_id":"",
            "work_job_id":"",
            "work_status_id":"",
            "work_wage_id":"",
            "work_location":"",
            "promised":"",
            "visited_at_from":"",
            "visited_at_to":"",
            'notes':"",
            'visitor':"",
            'visited_at':"",
            'visitor_card':"",
            'visitor_opinion':"",
            'visitor_evaluation':"",
            "min_family_count":"",
            "max_family_count":"",
            "min_total_vouchers":"",
            "max_total_vouchers":"",
            'organization_id':"",
            'got_voucher':"",
            'voucher_organization':"",
            'voucher_sponsors':"",
            'voucher_to':"",
            'voucher_from':"",
            "voucher_ids":"",
            'has_health_insurance':"",
            'health_insurance_number':"",
            'health_insurance_type':"",
            'has_device':"",
            'used_device_name':"",
            'gov_health_details':"",
        };

        $scope.PropertiesFilter=[];
        $scope.pro_id=[];
        $scope.AidSourceFilter=[];
        $scope.aid_id=[];

        $scope.infoCollapsed  = $scope.banksCollapsed     =
        $scope.healthCollapsed = $scope.worksCollapsed = $scope.contactsCollapsed  =
        $scope.residenceCollapsed = $scope.homeIndoorCollapsed =
        $scope.othersCollapsed= $scope.aidSourceCollapsed=
        $scope.propertiesCollapsed=$scope.socialSearchCollapsed=true;

        if($stateParams.id){
            $rootScope.filters.search_id=$stateParams.id;
            $scope.search_id=true;
        }else{
            $scope.search_id=false;
        }

    };

    var LoadCasesSearch =function(params){
        // $rootScope.clearToastr();
        params.deleted=$rootScope.deleted;

        if(angular.isUndefined(params.action)){
            params.action ='filter';
        }
        if(params.action && angular.isUndefined(params.page) ){
            params.page =1;
        }
        if( !angular.isUndefined(params.all_organization)) {
            if(params.all_organization == true ||params.all_organization == 'true'){
                var orgs=[];
                angular.forEach(params.organization_id, function(v, k) {
                    orgs.push(v.id);
                });

                params.organization_ids=orgs;

            }else if(params.all_organization == false ||params.all_organization == 'false') {
                params.organization_ids=[];
                $scope.master=false
            }
        }

        if( !angular.isUndefined(params.date_from)) {
            params.date_from=$filter('date')(params.date_from, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.date_to)) {
            params.date_to=$filter('date')(params.date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.birthday_to)) {
            params.birthday_to=$filter('date')(params.birthday_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.birthday_from)) {
            params.birthday_from= $filter('date')(params.birthday_from, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.visited_at_to)) {
            params.visited_at_to=$filter('date')(params.visited_at_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.visited_at_from)) {
            params.visited_at_from= $filter('date')(params.visited_at_from, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.voucher_to)) {
            params.voucher_to=$filter('date')(params.voucher_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.visited_at_from)) {
            params.voucher_from= $filter('date')(params.voucher_from, 'dd-MM-yyyy')
        }

        // // angular.element('.btn').addClass("disabled");
        $rootScope.progressbar_start();
        social_search.cases(params,function (response) {
            // angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            $rootScope.progressbar_complete();
            $scope.search_title = response.search_title

            if(params.all_organization == true ||params.all_organization == 'true'){
                $scope.master=true
            }
            if(params.action != 'filter'){

                if(response.status == 'false'){
                    $rootScope.toastrMessages('error',$filter('translate')('there is no cases to export'));
                }else if(params.action == 'FamilyMember' && response.status == false){
                    $rootScope.toastrMessages('error',$filter('translate')('there is no family member to export'));
                }else{
                    var file_type='zip';
                    if(params.action == 'ExportToExcel' || params.action == 'FamilyMember') {
                        file_type='xlsx';
                    }
                    if(!angular.isUndefined(response.download_token)) {
                        $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                    }
                }
            }
            else{
                $rootScope.UserOrgId=response.organization_id;
                $scope.organization_id=response.organization_id;

                var temp= response.items.data;
                    angular.forEach(temp, function(v, k) {
                        v.organization_id=parseInt(v.organization_id);
                        v.check= false;
                    });

                $scope.items= temp;
                $scope.CurrentPage = response.items.current_page;
                $scope.TotalItems = response.items.total;
                $scope.ItemsPerPage = response.items.per_page;
            }
        });
    };

    $rootScope.aidCategories_=[];
    angular.forEach($rootScope.aidCategories, function(v, k) {
        if ($rootScope.lang == 'ar'){
            $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
        }
        else{
            $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
        }
    });
    resetFilter();


    Entity.get({entity:'entities',c:'diseases,propertyTypes,roofMaterials,buildingStatus,furnitureStatus,houseStatus,habitableStatus,aidCountries,countries,maritalStatus,workReasons,workStatus,workWages,workJobs,banks,aidSources,properties,essentials'},function (response) {
        $scope.PropertyTypes = response.propertyTypes;
        $scope.RoofMaterials = response.roofMaterials;
        $scope.buildingStatus = response.buildingStatus;
        $scope.furnitureStatus = response.furnitureStatus;
        $scope.houseStatus = response.houseStatus;
        $scope.habitableStatus = response.habitableStatus;
        $scope.Diseases = response.diseases;
        $scope.Country = response.countries;
        $scope.aidCountries = response.aidCountries;
        $scope.MaritalStatus = response.maritalStatus;
        $scope.WorkReasons = response.workReasons;
        $scope.WorkStatus = response.workStatus;
        $scope.WorkWages = response.workWages;
        $scope.WorkJobs = response.workJobs;
        $scope.Banks = response.banks;
        $scope.Properties = response.properties;
        $scope.Aidsource = response.aidSources;
        $rootScope.filters.Essential =  $scope.Essentials = response.essentials;
    });
    Org.list({type:'sponsors'},function (response) { $rootScope.Sponsor = response; });
    Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

    $scope.selectedAll = false;
    $scope.selectAll=function(value){
            if (value) {
                $scope.selectedAll = true;
            } else {
                $scope.selectedAll = false;
            }

            angular.forEach($scope.items, function(v, k) {
                v.check=$scope.selectedAll;
            });
    };
    $scope.Search=function(data,action) {
        $rootScope.clearToastr();
        data.Properties=$scope.PropertiesFilter;
        data.AidSources =$scope.AidSourceFilter;
        data.action=action;
        if(action == 'filter'){
            data.page=$scope.CurrentPage;
        }

        LoadCasesSearch(data);

    };

    // ******************************************************************************
    $scope.sortKeyArr=[];
    $scope.sortKeyArr_rev=[];
    $scope.sortKeyArr_un_rev=[];
    $scope.sort_ = function(keyname){
        $scope.sortKey_ = keyname;
        var reverse_ = false;
        if (
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
        ) {
            reverse_ = true;
        }
        var data = angular.copy($rootScope.filters);
        data.action ='filter';

        if (reverse_) {
            if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_rev.push(keyname);
            }
        }
        else {
            if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_un_rev.push(keyname);
            }

        }

        data.sortKeyArr_rev = $scope.sortKeyArr_rev;
        data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;
        $scope.Search(data,'filter');
    };
    $scope.pageChanged = function (currentPage) {
        $rootScope.clearToastr();
        $rootScope.filters.page=currentPage;
        LoadCasesSearch($rootScope.filters);
    };

   $scope.itemsCount = '50';

    $scope.itemsPerPage_ = function (itemsCount) {
        $rootScope.clearToastr();
        $rootScope.filters.itemsCount=itemsCount;
        $rootScope.filters.page=1;
        LoadCasesSearch($rootScope.filters);
    };

    // ******************************************************************************
    $scope.word = function (id) {

        $rootScope.clearToastr();
        $rootScope.progressbar_start();
        social_search.word({'id':id },function (response) {
            $rootScope.progressbar_complete();
            // angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if(!angular.isUndefined(response.download_token)) {
                $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token;
            }else{
                $rootScope.toastrMessages('error',$filter('translate')('action failed'));
            }

        });

    };

    $scope.pdf = function (id) {

        $rootScope.clearToastr();
        $rootScope.progressbar_start();
        social_search.pdf({'id':id },function (response) {
            $rootScope.progressbar_complete();
            // angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if(!angular.isUndefined(response.download_token)) {
                $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token;
            }else{
                $rootScope.toastrMessages('error',$filter('translate')('action failed'));
            }

              });

    };

    // ******************************************************************************

    $scope.get = function(target,value,parant){
        $rootScope.clearToastr();

        if (!angular.isUndefined(value)) {
            if(value != null && value != "" && value != " " ) {
                Entity.listChildren({entity:target,'id':value,'parent':parant},function (response) {

                    if (target == 'sdistricts') {
                        $scope.governarate = response;
                        $scope.regions = [];
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];

                        $rootScope.filters.district_id = "";
                        $rootScope.filters.region_id = "";
                        $rootScope.filters.neighborhood_id = "";
                        $rootScope.filters.square_id = "";
                        $rootScope.filters.mosque_id = "";

                    }
                    else if (target == 'sregions') {
                        $scope.regions = response;
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];
                        $rootScope.filters.region_id = "";
                        $rootScope.filters.neighborhood_id = "";
                        $rootScope.filters.square_id = "";
                        $rootScope.filters.mosque_id = "";
                    }
                    else if (target == 'sneighborhoods') {
                        $scope.nearlocation = response;
                        $scope.squares = [];
                        $scope.mosques = [];
                        $rootScope.filters.neighborhood_id = "";
                        $rootScope.filters.square_id = "";
                        $rootScope.filters.mosque_id = "";

                    }
                    else if (target == 'ssquares') {
                        $scope.squares = response;
                        $scope.mosques = [];
                        $rootScope.filters.square_id = "";
                        $rootScope.filters.mosque_id = "";
                    }
                    else if (target == 'smosques') {
                        $scope.mosques = response;
                        $rootScope.filters.mosque_id = "";
                    }else if(target =='branches'){
                        $scope.Branches = response;
                        $rootScope.filters.branch_name="";
                    }
                });
            }
        }
    };

    // ******************************************************************************

    $scope.toggleSearchForm = function() {

        $scope.toggleStatus = $scope.toggleStatus == false ? true: false;
        if($scope.toggleStatus == true) {
            $rootScope.filters.page=1;
            $rootScope.filters.action='filter';
        }
        resetFilter();

    };
    $scope.setPropertiesFilter=function(id,value){
        var index = id_index = -1;
        if ($scope.pro_id.indexOf(id) == -1) {
            if (value != "") {
                $scope.PropertiesFilter.push({'id':id,'exist':value});
                $scope.pro_id.push(id);
            }
        }else{
            id_index=$scope.pro_id.indexOf(id);
            for (var i = 0, len = $scope.PropertiesFilter.length; i <= len; i++) {
                if ($scope.PropertiesFilter[i].id == id) {
                    index= i;
                    break;
                }
            }
        }
        if(index != -1){
            if (value == "") {
                $scope.PropertiesFilter.splice(index, 1);
                $scope.pro_id.splice(id_index, 1);
            } else {
                $scope.PropertiesFilter[index].exist = value;
            }
        }
    };
    $scope.setAidSourceFilter=function(id,value){
        var aindex = aid_index = -1;
        if ($scope.aid_id.indexOf(id) == -1) {
            if (value != "") {
                $scope.AidSourceFilter.push({'id':id,'aid_take':value});
                $scope.aid_id.push(id);
            }
        }else{
            aid_index=$scope.aid_id.indexOf(id);
            for (var i = 0, len = $scope.AidSourceFilter.length; i <= len; i++) {
                if ($scope.AidSourceFilter[i].id == id) {
                    aindex= i;
                    break;
                }
            }
        }
        if(aindex != -1){
            if (value == "") {
                $scope.AidSourceFilter.splice(aindex, 1);
                $scope.aid_id.splice(aid_index, 1);
            } else {
                $scope.AidSourceFilter[aindex].aid_take = value;
            }
        }
    };
    $scope.setEssentialsFilter=function(z,target,id,value){
        $rootScope.filters.Essential[z].id=id;
    };

    $scope.resetFilters=function(){
        resetFilter();
    }

    $scope.selectedAll = false;
    $scope.allCheckbox=function(value){
        if (value) {
            $scope.selectedAll = true;
        } else {
            $scope.selectedAll = false;
        }

        angular.forEach($scope.items, function(v, k) {
            v.check=$scope.selectedAll;
        });
    };
    $scope.exportGroup=function(action){
        $rootScope.clearToastr();
        var reports=[];
        angular.forEach($scope.items, function(v, k) {
            if(v.check){
                reports.push(v.id);
            }
        });

        if(reports.length==0){
            $rootScope.toastrMessages('error',$filter('translate')('You have not select any case'));
        }else{
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            social_search.exportGroup({"action":action,'reports':reports},function (response) {
                $rootScope.progressbar_complete();
                if(response.status == 'false'){
                    $rootScope.toastrMessages('error',$filter('translate')('there is no cases to export'));
                }else if(action == 'FamilyMember' && response.status == false){
                    $rootScope.toastrMessages('error',$filter('translate')('there is no family member to export'));
                }else{
                    var file_type='zip';
                    if(action == 'ExportToExcel' || action == 'FamilyMember') {
                        file_type='xlsx';
                    }
                    if(!angular.isUndefined(response.download_token)) {
                        $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                    }
                }
            });
        }
    };



   // ******************************************************************************
    $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0};
    $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $rootScope.format = $scope.formats[0];
    $rootScope.today = function() {$rootScope.dt = new Date();};
    $rootScope.today();
    $rootScope.clear = function() {$rootScope.dt = null;};

    $scope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup1.opened = true;};
    $scope.open2 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup2.opened = true;};
    $scope.open3 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup3.opened = true;};
    $scope.open4 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup4.opened = true;};
    $scope.open8 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup8.opened = true;};
    $scope.open15 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup15.opened = true;};
    $scope.open23 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup23.opened = true;};
    $scope.open24 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup24.opened = true;};
    $scope.open25 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup25.opened = true;};
    $scope.open224 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup224.opened = true;};

    $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
    $scope.open88 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup88.opened = true;};

    $scope.popup1 = {opened: false};
    $scope.popup2 = {opened: false};
    $scope.popup8 = {opened: false};
    $scope.popup3 = {opened: false};
    $scope.popup4 = {opened: false};
    $scope.popup23 = {opened: false};
    $scope.popup24 = {opened: false};
    $scope.popup224 = {opened: false};
    $scope.popup15 = {opened: false};
    $scope.popup25 = {opened: false};
    $scope.popup99 = {opened: false};
    $scope.popup88 = {opened: false};

    LoadCasesSearch($rootScope.filters);


});

angular.module('AidModule')
    .filter('ageFilter', function($filter) {
        function getAge(birthday) { // birthday is a date

            if(!birthday) // if a is negative,undefined,null,empty value then...
            {
                return  0 + $filter('translate')('year')  ;
            }

            var today = new Date();
            var birthDate = new Date(birthday);
            var age = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }

            return (age == 0) ? (m > 1) ?  m +  $filter('translate')('month')+"/"+$filter('translate')('months') : m + $filter('translate')('month')+"/"+$filter('translate')('months')  : (age > 1) ? age +$filter('translate')('year')+"/"+$filter('translate')('years') : age +$filter('translate')('year')+"/"+$filter('translate')('years')  ;
        }

        return function(birthdate) {
            return getAge(birthdate);
        };
    })
    .controller('SocialSearchCaseFormController',
    function($rootScope,$scope,$filter,$stateParams,$uibModal,$http,$timeout,$state,social_search) {

        $rootScope.clearToastr();
        $state.current.data.pageTitle = $filter('translate')('Social Search');

        angular.element('.data-block').addClass("hide");
        $scope.curStep = 1;
        $rootScope.row = {};

        $rootScope.progressbar_start();
        social_search.form({id:$stateParams.id},function (response) {
            $rootScope.progressbar_complete();
            // angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            $rootScope.row = response;
            $scope.getTheState(1);
        });


        $scope.getTheState = function (number){
            $scope.curStep = number;
            angular.element('.step').removeClass("active");
            var elem = '#step-' + number ;
            angular.element(elem).addClass("active");

            var elem = '#step-' + number + '-block';
            angular.element('.data-block').removeClass("show");
            angular.element('.data-block').addClass("hide");
            angular.element(elem).addClass("show");

        }

        $scope.getNext = function (){
            var step = $scope.curStep + 1;
            $scope.getTheState(step)
        }

        $scope.getPrevious = function (){
            var step = $scope.curStep - 1;
            $scope.getTheState(step)
        }

        $scope.word = function () {

            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            social_search.word({'id':$stateParams.id },function (response) {
                $rootScope.progressbar_complete();
                // angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                if(!angular.isUndefined(response.download_token)) {
                    $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                    window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }

            });

        };

        $scope.pdf = function () {

            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            social_search.pdf({'id':$stateParams.id },function (response) {
                $rootScope.progressbar_complete();
                // angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                if(!angular.isUndefined(response.download_token)) {
                    $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                    window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }

            });

        };
    });


angular.module('AidModule')
    .controller('SocialSearchTemplatesController' ,function($stateParams,$rootScope, $scope, $http ,$timeout, $uibModal, $log,social_search,FileUploader, OAuthToken,$filter,$state) {

        $rootScope.clearToastr();
        $rootScope.items=[];
        $rootScope.has_default_template=false;
        $rootScope.default_template=null;
        $rootScope.close=function(){

            $rootScope.failed =false;
            ;
            $rootScope.model_error_status =false;
        };

        $state.current.data.pageTitle = $filter('translate')('categories-template');

        function LoadTemplates(){
            $rootScope.progressbar_start();
            social_search.templates(function (response) {
                $rootScope.progressbar_complete();
                $rootScope.has_default_template = response.has_default_template;
                $rootScope.default_template = response.default_template;
                $rootScope.items = response.templates;
            });
        }

        // ------------------------------------------------------------------------
        $scope.download = function (target,id){

            if(target == 'template'){
                $rootScope.progressbar_start();
                social_search.template({id:id},function (response) {
                    $rootScope.progressbar_complete();
                    if(!angular.isUndefined(response.download_token)) {
                        $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                        window.location = '/api/v1.0/common/files/docx/export?download_token='+response.download_token+'&template=true';
                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                    }
                });
            }else {
                var template = '';
                var ext = '';
                if(target =='default'){
                    template = 'default-search-form-template'
                    ext ='docx'
                }else {
                    template = 'search-form-instruction'
                    ext ='pdf'
                }

                window.location = '/api/v1.0/common/files/'+ext+'/export?download_token='+template+'&template=true';
            }

        }
        // ------------------------------------------------------------------------
        $scope.ChooseTemplate=function(id,action,target){
            $rootScope.id=id;
            $uibModal.open({
                templateUrl: 'myModalContent2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/aids/social_search/template-upload',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });
                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.progressbar_start();
                        item.formData = [{category_id: $rootScope.id}];
                    };
                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        $rootScope.toastrMessages('success',$filter('translate')('The template is successfully'));
                        LoadTemplates();
                        $modalInstance.close();

                    };


                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });


        };
        // ------------------------------------------------------------------------

        LoadTemplates();
    });

