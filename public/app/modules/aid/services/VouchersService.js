
angular.module('AidModule')
    .factory('cs_persons_vouchers', function ($resource) {
        return $resource('/api/v1.0/aids/cs_persons_vouchers/:operation/:id', {page: '@page',id: '@id'}, {
            sponsor_vouchers_persons: { method:'POST' ,params:{operation:'sponsor_vouchers_persons'}, isArray:false },
            sponsor_vouchers: { method:'POST' ,params:{operation:'sponsor_vouchers'}, isArray:false },
            statistics      : { method:'POST' ,params:{operation:'statistics'}, isArray:false },
            persons      : { method:'POST' ,params:{operation:'persons'}, isArray:false },
            get: { method:'GET', isArray:false },
            reset:{method: 'PUT'  , params: {operation:'reset'}, isArray: false},
            setStatus:{method: 'PUT'  , params: {operation:'setStatus'}, isArray: false},
            update:{method:'PUT'  , params: {}, isArray: false},
            attachments      : { method:'GET' ,params:{operation:'attachments'}, isArray:false },
            upload      : { method:'POST' ,params:{operation:'upload'}, isArray:false },
            all   :{method:'POST' ,params:{operation:'all'}, isArray:false },
            cases   :{method:'POST' ,params:{operation:'cases'}, isArray:false },
            list  :{method:'GET',params:{operation:'list'}, isArray:false },
            getPersonsList: { method:'GET' ,params:{operation:'getPersonsList'}, isArray:false },
            updateStatus:{method: 'PUT'  , params: {operation:'updateStatus'}, isArray: false},
            updateSerial:{method: 'POST'  , params: {operation:'updateSerial'}, isArray: false},
            getVouchers:{method: 'get'  , params: {operation:'getVouchers'}, isArray: false},
            individualVoucher:   {method: 'POST' , params: {operation:'individualVoucher'}, isArray: false},
            setBeneficiary:   {method: 'POST' , params: {operation:'setBeneficiary'}, isArray: false},
            deletePersonsVoucher   : { method:'DELETE', params: {operation:'deletePersonsVoucher'}, isArray: false},
            updateVoucher:{method: 'PUT'  , params: {operation:'updateVoucher'}, isArray: false},
            updatePersonsVoucherDetails:{method: 'PUT'  , params: {operation:'updatePersonsVoucherDetails'}, isArray: false},
            createToken:{method:'POST' ,params:{operation:'createToken'}, isArray:false },
            activateDeactivate:{method:'POST' ,params:{operation:'activateDeactivate'}, isArray:false },
            setDetails:{method: 'POST'  , params: {operation:'setDetails'}, isArray: false},
            setAttachment  : { method:'POST' ,params:{operation:'setAttachment',page: '@page'}, isArray:false },
            getBeneficiaryAttachments  : { method:'GET' ,params:{operation:'getBeneficiaryAttachments'}, isArray:false },
            getAttachments  : { method:'GET' ,params:{operation:'getAttachments'}, isArray:false },

            codesList:{method: 'get'  , params: {operation:'codesList'}, isArray: false},
            vouchersCodeList:{method: 'get'  , params: {operation:'vouchersCodeList'}, isArray: false},
            vouchersTitleList:{method: 'get'  , params: {operation:'vouchersTitleList'}, isArray: false},
            deleteSerial:{method: 'POST'  , params: {operation:'deleteSerial'}, isArray: false},

            trashed   :{method:'POST' ,params:{operation:'trashed'}, isArray:false },
            restore:{method: 'PUT'  , params: {operation:'restore'}, isArray: false},

        });
    });

