
angular.module('AidModule')
    .factory('neighborhoods_ratios', function ($resource) {
        return $resource('/api/v1.0/aids/neighborhoods_ratios/:operation/:id', {page: '@page',id: '@id'}, {
            update:{method:'PUT',isArray: false},
            query :{method:'GET',isArray:false }
        });
    });

