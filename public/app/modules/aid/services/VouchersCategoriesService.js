
angular.module('AidModule')
    .factory('vouchers_categories', function ($resource) {
        return $resource('/api/v1.0/aids/vouchers_categories/:operation/:id', {page: '@page',id: '@id'}, {
            query  : { method:'GET', isArray:false },
            update : { method:'PUT', isArray: false},
            updateSub:{method:'PUT', params: {operation:'updateSub'}, isArray: false},
            List   : { method:'GET', params:{operation:'list'}, isArray:true },
            getParentsCategory:{method: 'GET'  , params: {operation:'getParentsCategory'}, isArray: true},
            getSiblingsCategory:{method: 'GET' , params: {operation:'getSiblingsCategory'},isArray: false}

        });
    });

