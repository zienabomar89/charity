angular.module('AidModule')
    .factory('social_search', function ($resource) {
        return $resource('/api/v1.0/aids/social_search/:operation/:id', {page: '@page',id: '@id'}, {
            filter    :{method:'POST' ,params:{operation:'filter'}, isArray:false },
            get       :{method:'GET', isArray:false },
            update    :{method:'PUT'  , params: {}, isArray: false},

            list      :{method:'GET' ,params:{operation:'list'}, isArray:false },
            cases     :{method:'POST',params:{operation:'cases'}, isArray:false },
            restore   :{method:'PUT' ,params:{operation:'restore'}, isArray: false},
            refresh   :{method:'PUT' ,params:{operation:'refresh'}, isArray:false },
            status    :{method:'POST' ,params:{operation:'status'}, isArray:false },
            exportGroup    :{method:'POST' ,params:{operation:'export-group'}, isArray:false },

            form      :{method:'GET' ,params:{operation:'form'}, isArray:false },
            pdf       :{method:'GET' ,params:{operation:'pdf'}, isArray:false },
            word      :{method:'GET' ,params:{operation:'word'}, isArray:false },
            templates :{method:'GET' ,params:{operation:'templates'}, isArray:false },
            template  :{method:'GET' ,params:{operation:'template'}, isArray:false },

        });
    });

