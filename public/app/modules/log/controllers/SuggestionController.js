LogModule = angular.module('LogModule');


LogModule.controller('MySuggestionController', function ($rootScope,$scope,Suggestion,$ngBootbox,$filter,$state,$http,$resource) {

    $rootScope.CurrentPage=1;
    $scope.suggestion = {};
    $scope.filters = {page : $rootScope.CurrentPage,is_mine:1 ,action  :'paginate'};

    var getSuggestions = function(params){
        params.is_mine =1;
        $rootScope.progressbar_start();
        Suggestion.filter(params,function(response) {
            $rootScope.progressbar_complete();
            $rootScope.allSuggestion=response.data;
            $rootScope.CurrentPage = response.current_page;
            $rootScope.TotalItems = response.total;
            $rootScope.ItemsPerPage = response.per_page;
        });
        $scope.title = $filter('translate')('suggestion');
    };

    $scope.resetFilter =function(){
        $scope.filters = {page : $rootScope.CurrentPage,is_mine:1 ,action  :'paginate'};
    };

    $scope.filterSuggestion = function(inputs) {
        var params = angular.copy(inputs);
        if( !angular.isUndefined(params.from)) {
            params.from=$filter('date')(params.from, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.to)) {
            params.to=$filter('date')(params.to, 'dd-MM-yyyy')
        }

        getSuggestions(params);
    };
    $scope.GoTo = function(id){
        $state.go('show-suggest',{"id":id});
    };


    $scope.selectedAll = function(value){
        if(value){
            $scope.selectAll = true;
        }
        else{$scope.selectAll =false;

        }
        angular.forEach($scope.allSuggestion, function(val,key) {
            val.check=$scope.selectAll;
        });
    };

    $scope.pageChanged = function (CurrentPage) {
        $rootScope.CurrentPage = CurrentPage;
        $scope.filters.page = $rootScope.CurrentPage ;
        getSuggestions($scope.filters);
    };

    $scope.deleteSuggestion = function(){
        $rootScope.clearToastr();

        var checked=[];
        angular.forEach($scope.allSuggestion, function(value, key) {
            if (value.check) {
                checked.push(value.id);
            }
        });
        if (checked.length>0){
            var suggestion_ids = checked.toString();
            $scope.delete(suggestion_ids);

        }else {
            $rootScope.toastrMessages('error',$filter('translate')('There is no selected row to delete'));
            return;
        }
    };
    $scope.delete=function (suggestion_ids) {
        $ngBootbox.confirm($filter('translate')('are you want to confirm delete'))
            .then(function() {
                $rootScope.progressbar_start();
                Suggestion.destroy({'suggestion_ids':suggestion_ids}
                    ,function(response) {
                        $rootScope.progressbar_complete();
                        if (response.status == "success"){
                            $rootScope.toastrMessages('success',$filter('translate')('The row is deleted from db'));
                            getSuggestions($scope.filters);
                        }else {
                            $rootScope.toastrMessages('error',$filter('translate')('The row is not deleted from db'));
                        }
                    });
            });
    };

    getSuggestions($scope.filters);

    $scope.dateOptions = {formatYear: 'yy', startingDay: 0 };
    $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.today = function() {$scope.dt = new Date(); };
    $scope.today();
    $scope.clear = function() { $scope.dt = null;};
    $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
    $scope.open88 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup88.opened = true;};

    $scope.popup99 = {opened: false};
    $scope.popup88 = {opened: false};

});
LogModule.controller('manageSuggestionController', function ($rootScope,$scope,Suggestion, $filter,$state,Log) {

    $rootScope.clearToastr();
    $rootScope.CurrentPage=1;
    $rootScope.allSuggestion =[];
    $scope.filters = {page : $rootScope.CurrentPage,is_mine:0 ,action  :'paginate'};
    $scope.title = $filter('translate')('suggestion');
    $scope.options=Log.getOptions();

    var getSuggestions = function(params){
        $rootScope.progressbar_start();
        params.is_mine =0;
        Suggestion.filter(params,function(response) {
            $rootScope.progressbar_complete();
            $rootScope.allSuggestion=response.data;
            $rootScope.CurrentPage = response.current_page;
            $rootScope.TotalItems = response.total;
            $rootScope.ItemsPerPage = response.per_page;
        });
    };
    $scope.pageChanged = function (CurrentPage) {
        $rootScope.CurrentPage = CurrentPage;
        $scope.filters.page = $rootScope.CurrentPage ;
        getSuggestions($scope.filters);
    };

    $scope.resetFilter =function(){
        $scope.filters = {page : $rootScope.CurrentPage,is_mine:1 ,action  :'paginate'};
    };

    $scope.filterSuggestion = function(inputs) {
        var params = angular.copy(inputs);
        if( !angular.isUndefined(params.from)) {
            params.from=$filter('date')(params.from, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.to)) {
            params.to=$filter('date')(params.to, 'dd-MM-yyyy')
        }

        if(!angular.isUndefined(params.user_id)) {
            var users=[];
            angular.forEach(params.user_id, function(v, k) {
                users.push(v.id);
            });
            params.user_id=users;

        }else{
            params.user_id=[];
        }

        getSuggestions(params);
    };

    $scope.GoTo = function(id){
        $state.go('manage-suggest',{"id":id});
    };

    getSuggestions($scope.filters);

    $scope.dateOptions = {formatYear: 'yy', startingDay: 0 };
    $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.today = function() {$scope.dt = new Date(); };
    $scope.today();
    $scope.clear = function() { $scope.dt = null;};
    $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
    $scope.open88 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup88.opened = true;};

    $scope.popup99 = {opened: false};
    $scope.popup88 = {opened: false};
});

LogModule.controller('addSuggestController', function ($rootScope,$scope,Suggestion,$timeout,$ngBootbox ,$filter,$state,$http,FileUploader,OAuthToken) {

    $rootScope.clearToastr();
    $scope.row = {};
    $scope.attach_files = [];

    $scope.compose = function(row){
        row.attach_files =  $scope.attach_files;
        $rootScope.progressbar_start();
        Suggestion.store(row,function(response) {
            $rootScope.progressbar_complete();
            if (response.status){
                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                $state.go('my-suggestion');

            }else {
                $rootScope.toastrMessages('error',response.msg);
            }
        });
        $scope.row ={};
    };
    $scope.fileUpload=function(item){
        $rootScope.progressbar_start();
        $timeout(function() {
            Uploader.onBeforeUploadItem = function(item) {
                $rootScope.clearToastr();
            };
            Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                $rootScope.progressbar_complete();
                $scope.uploader.destroy();
                $scope.uploader.queue=[];
                $scope.uploader.clearQueue();
                angular.element("input[type='file']").val(null);
                $scope.attach_files.push(response.id);
            };
            Uploader.uploadAll();
        }, 5);
    };
    $scope.removeFile=function(index){
        $ngBootbox.confirm($filter('translate')('are you want to confirm delete'))
            .then(function() {
                var file_id = $scope.attach_files[index] ;
                var msg_type = $rootScope.type;
                $rootScope.progressbar_start();
                Suggestion.destroyFile({id:file_id}, function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status){
                        Uploader.queue.splice(index, 1);
                        $scope.attach_files.splice(index,1);
                    }
                });
            });

    };
    var Uploader = $scope.uploader = new FileUploader({
        url: '/api/v1.0/doc/files',
        headers: {
            Authorization: OAuthToken.getAuthorizationHeader()
        }
    });
    Uploader.onBeforeUploadItem = function(item) {
        $rootScope.progressbar_start();
        $rootScope.clearToastr();
        item.formData = [{action:"not_check"}];
    };
    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
        $rootScope.progressbar_complete();
        $scope.uploader.destroy();
        $scope.uploader.queue=[];
        $scope.uploader.clearQueue();
        angular.element("input[type='file']").val(null);
        $scope.attach_files.push(response.id);
    };

});
LogModule.controller('showSuggestController', function ($rootScope,$scope,Suggestion, $filter,$state,$http, $stateParams,$uibModal) {

    $rootScope.clearToastr();
    var showSuggest = function(){
        $rootScope.progressbar_start();
        Suggestion.show({'id':$stateParams.id},function(response) {
            $rootScope.progressbar_complete();
            $scope.suggestion = response ;
        });
    };

    showSuggest();

    $scope.show = function (x) {
        $uibModal.open(
            {
                templateUrl: '/app/modules/log/views/Suggestion/show_details.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope, $scope, $modalInstance, $log) {
                    $scope.suggestion=x;
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
    }


});
LogModule.controller('manageSuggestController', function ($rootScope,$scope,Suggestion, $filter,$state,$http,popupService, $stateParams , $uibModal) {

    $rootScope.clearToastr();
    $scope.row ={};
    $scope.add_commit_status = false ;
    var showSuggest = function(){
        $rootScope.progressbar_start();
        Suggestion.show({'id':$stateParams.id},function(response) {
            $rootScope.progressbar_complete();
            $scope.suggestion = response ;
        });
    };

    $scope.add_commit = function(){
        $scope.row ={};
        $scope.add_commit_status = true ;
    };

    $scope.close_comment = function(){
        $scope.row ={};
        $scope.add_commit_status = false ;
    };

    $scope.compose = function(){
        $scope.row.suggestion_id = $scope.suggestion.id;
        $rootScope.progressbar_start();
        Suggestion.reply($scope.row,function(response) {
            $rootScope.progressbar_complete();
            if (response.status == "success"){
                $scope.close_comment();
                showSuggest();
                $rootScope.toastrMessages('success',$filter('translate')('action success'));
            }else {
                $rootScope.toastrMessages('error',response.msg);
            }
        });
    };

    showSuggest();


    $scope.show = function (x) {
        $uibModal.open(
            {
                templateUrl: '/app/modules/log/views/Suggestion/show_details.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope, $scope, $modalInstance, $log) {
                    $scope.suggestion=x;
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
    }

});