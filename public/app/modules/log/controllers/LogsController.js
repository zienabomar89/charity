LogModule = angular.module('LogModule');

LogModule.controller('LogsController', function ($rootScope,$scope, Log,$filter,$state,$http) {

    $state.current.data.pageTitle = $filter('translate')('Logs');
    $scope.currentPage=1;
    $scope.itemsCount='50';
    $scope.options=Log.getOptions();


    let actions = [
        'VOUCHER_CREATED',
    'VOUCHER_UPDATED' ,
        'VOUCHER_CLOSED' ,
        'VOUCHER_DELETED' ,
        'VOUCHER_RESTORE' ,
        'VOUCHER_DOCUMENT_DELETE' ,
        'PERSON_VOUCHER_UPDATED',
        'PERSON_VOUCHER_CREATED' ,
        'PERSONS_VOUCHER_EXPORT' ,
        'VOUCHER_CATEGORIES_CREATED' ,
        'VOUCHER_CATEGORIES_UPDATED' ,
        'VOUCHER_CATEGORIES_DELETED' ,
        'CUSTOM_FORM_CREATED' ,
        'CUSTOM_FORM_UPDATED' ,
        'CUSTOM_FORM_DELETED',
        'CUSTOM_FORM_RESTORED',
        'SOCIAL_SEARCH_CREATED' ,
        'SOCIAL_SEARCH_UPDATED' ,
        'SOCIAL_SEARCH_DELETED' ,
        'SOCIAL_SEARCH_RESTORE' ,
        'SOCIAL_SEARCH_CASE_EXPORTED' ,
        'CATEGORIES_TEMPLATE_UPLOADED' ,
        'CITIZEN_REQUEST_STATUS_UPDATE' ,
        'NEIGHBORHOODS_RTIOS_UPDATED' ,
        'CASE_CREATED' ,
        'CASE_UPDATED' ,
        'USER_LOG_IN' ,
        'CARD_CKECK' ,
        'BACKUP_VERSION_DELETED' ,
        'PAYMENTS_CASES_UPDATED',
        'PAYMENTS_RECIPIENT_UPDATED',
        'ORGANIZATION_UPLOAD',
        'ORGANIZATION_DELETED',
        'ORGANIZATION_CREATED',
        'ORGANIZATION_UPDATED',
        'SPONSOR_UPLOAD',
        'SPONSOR_DELETED',
        'SPONSOR_CREATED',
        'SPONSOR_UPDATED',
        'PAYMENTS_EXPORTED',
        'FORM_CASE_DATA_UPDATED',
        'FORM_ELEMENTS_CREATED',
        'FORM_ELEMENTS_UPDATED',
        'FORM_ELEMENTS_DELETED',
        'ELEMENT_OPTION_DELETED',
        'ORGANIZATION_PROJECT_CREATED' ,
        'ORGANIZATION_PROJECT_UPDATED' ,
        'ORGANIZATION_PROJECT_DELETED' ,
        'ORGANIZATION_PROJECT_RESTORE' ,
        'ORGANIZATION_PROJECT_ATTAHMENT_UPLOAD' ,
        'ORGANIZATION_PROJECT_ATTAHMENT_DELETE' ,
        'ADMINSTRATIVE_TAB_OPTION_DELETED' ,
        'ADMINSTRATIVE_TAB_OPTION_UPDATED' ,
        'ADMINSTRATIVE_TAB_OPTION_CREATED' ,
        'ADMINSTRATIVE_REPORT_ORGANIZATION_DATA_STATUS_UPDATED' ,
        'ADMINSTRATIVE_REPORT_TAB_CREATED'  ,
        'ADMINSTRATIVE_REPORT_TAB_UPDATED' ,
        'ADMINSTRATIVE_REPORT_TAB_DELETED' ,
        'ADMINSTRATIVE_TAB_COLUMNS_CREATED' ,
        'ADMINSTRATIVE_TAB_COLUMNS_UPDATED' ,
        'ADMINSTRATIVE_TAB_COLUMNS_DELETED' ,
        'ADMINSTRATIVE_REPORT_CREATED' ,
        'ADMINSTRATIVE_REPORT_UPDATED' ,
        'ADMINSTRATIVE_REPORT_DELETED' ,
        'REPORT_UPDATED' ,
        'ADMINSTRATIVE_REPORT_DATA_UPDATED' ,
        'SENT_SMS'  ,
        'SMS_PROVIDERS_CREATED' ,
        'SMS_PROVIDERS_UPDATED' ,
        'SMS_PROVIDERS_DELETED' ,
        'ORGANIZATIONS_SMS_PROVIDERS_CREATED' ,
        'ORGANIZATIONS_SMS_PROVIDERS_UPDATED' ,
        'ORGANIZATIONS_SMS_PROVIDERS_DELETED' ,
        'PROJECT_CREATED',
        'PROJECT_UPDATED',
        'PROJECT_DELETED',
        'PROJECT_CANDIDATE_UPDATED',
        'PROJECT_CANDIDATE_DELETED',
        'PROJECT_CANDIDATE_CREATED',
        'PROJECT_ORGANIZATION_UPDATED',
        'REPOSITORY_CREATED',
        'REPOSITORY_UPDATED',
        'REPOSITORY_DELETED',
        'REPOSITORY_RESTORE',
        'PROJECT_RULE_CREATED',
        'BLOCK_CATEGORIES_CREATED',
        'BLOCK_CATEGORIES_DELETED',
        'CASE_DELETED',
        'CASE_EXPORTED',
        'CASES_UPDATED',
        'LOCATION_CREATED',
        'PERSON_RESIDENCE_SAVED',
        'RELAY_CREATED',
        'TRANSFERS_CREATED',
        'TRANSFER_CREATED',
        'TRANSFER_REJECT',
        'USER_ACTIVATED',
        'USER_RESTORED',
        'USER_UPDATED',
        'USER_CREATED',
        'USER_DELETED',
        'USER_DEACTIVATED',
        'USER_PASSWORD_CHANGED',
    'USER_PASSWORD_RESET',
    'VISITOR_NOTES_CREATED',
    'VISITOR_NOTES_DELETED',
    'CATEGORY_POLICY_CREATED',
    'CATEGORY_POLICY_UPDATED',
    'CATEGORY_POLICY_DELETED',
    'CATEGORIES_CREATED',
    'CATEGORIES_UPDATED',
    'CATEGORIES_FORM_ELEMENTS_UPDATED',
    'CATEGORY_DELETED',
    'PERSON_UPDATED',
    'PERSON_CREATED',
    'PERSON_DELETED',
    'SPONSORSHIPS_CASES_UPDATED',
    'SPONSORSHIPS_PERSON_EXPORTED',
    'SPONSORSHIPS_PERSONS_UPDATED',
    'SPONSORSHIPS_PERSON_DELETED',
    'UPDATED_DATA_SETTING_DELETED',
    'UPDATED_DATA_SETTING_CREATED',
    'UPDATED_DATA_SETTING_UPDATED',
    'SPONSORSHIP_CASE_CREATED',
    'REPORT_EXPORTED',
    'PAYMENTS_CASES_CREATED',
    'PAYMENTS_CREATED',
    'PAYMENTS_UPDATED',
    'PAYMENTS_DELETED',
    'PAYMENTS_DOCUMENTS_CREATED',
    'PAYMENTS_DOCUMENTS_DELETED',
    'AUTOMATIC_LOGOUT',
    'SPONSORSHIP_UPDATED'
    ];

    $rootScope.logActions = [];
    $rootScope.translateActions = [];
    for (var i = 0; i < actions.length; i++) {
        let key = actions[i];
        let keyTranslate = $filter('translate')(key);

        $rootScope.translateActions[key] = keyTranslate;

        $rootScope.logActions.push({'id' : key , 'name' : keyTranslate});

    }

    $scope.srch = {};
    $scope.logs =[];

    var fetchLogs = function (params) {
        $rootScope.progressbar_start();


        var data = angular.copy(params);
        data.filter_status = 'paginate';

        if( !angular.isUndefined(data.from)) {
            data.from=$filter('date')(data.from, 'dd-MM-yyyy');
        }
        if( !angular.isUndefined(data.to)) {
            data.to=$filter('date')(data.to, 'dd-MM-yyyy');
        }

        let actions=[];
        angular.forEach(data.actions, function(v, k){
            actions.push(v.id);
        });


        let users=[];
        angular.forEach(data.user_id, function(v, k){
            users.push(v.id);
        });

        data.actions = actions;
        data.users = users;

        delete data.user_id;
        Log.filter(data, function (response) {
            $rootScope.progressbar_complete();
            $scope.logs = response.data;
            $scope.totalItems = response.total;
            $scope.currentPage = response.current_page;
            $scope.itemsPerPage = response.per_page;
        });
    };
    
    $scope.pageChanged = function() {
        var params = $scope.srch;
        params.page = $scope.currentPage;
        fetchLogs(params);
    };

    $rootScope.itemsPerPage_ = function (itemsCount) {
        var params = $scope.srch;
        params.page = 1;
        params.itemsCount = itemsCount;
        fetchLogs(params);
    };

    $scope.search = function() {
        var params = $scope.srch;
        fetchLogs(params);
    };

    $scope.actionList = function() {
        window.location = '/api/v1.0/common/files/xlsx/export?download_token=actions-list&template=true';
    };

    $scope.export = function() {
        var data = $scope.srch;
        data.filter_status='xlsx';

        if( !angular.isUndefined(data.from)) {
            data.from=$filter('date')(data.from, 'dd-MM-yyyy');
        }
        if( !angular.isUndefined(data.to)) {
            data.to=$filter('date')(data.to, 'dd-MM-yyyy');
        }

        let actions=[];
        angular.forEach(data.actions, function(v, k){
            actions.push(v.id);
        });


        let users=[];
        angular.forEach(data.user_id, function(v, k){
            users.push(v.id);
        });

        data.actions = actions;
        data.users = users;
        $rootScope.progressbar_start();
        Log.filter(data, function (response) {
            $rootScope.progressbar_complete();
            if(response.status == 'false' || response.status == false){
                $rootScope.toastrMessages('error',$filter('translate')('There is no data to export'));
            }else{
                $rootScope.toastrMessages('success',response.msg);
                var file_type='xlsx';
                // window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
            }
        });

    };

    // fetchLogs({page: 1});
});