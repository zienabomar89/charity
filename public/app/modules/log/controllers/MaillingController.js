LogModule = angular.module('LogModule');

LogModule.controller('MaillingController', function ($resource,$stateParams,$rootScope,$scope,Log, Mailling,$ngBootbox,$timeout,$filter,$state,$http,popupService,FileUploader,OAuthToken) {

    $state.current.data.pageTitle = $filter('translate')('Mailling');

    $rootScope.currentPage=1;
    $rootScope.type = 'sent'; 
    $rootScope.messages = [];
    $scope.Attach_files = [];
    $scope.row = {};
    $rootScope.page={inbox_inbox:true,inbox_compose:false,inbox_view:false};
    $rootScope.reply = false;
    $scope.tags =[];

    $scope.toggleStatus = false;
    $rootScope.filters={};
    $rootScope.filters.page =1;
    $rootScope.usersList = []; // all of the users who them username like what in the receiver input
    $rootScope.receivers = []; // list of receivers
    
    $scope.reset = function(){ 
        $rootScope.reply = false;
        $scope.tags =[];
    }

    $scope.getMessages = function(inboxType){ 
       // if(inboxType != $scope.type ){
            if(inboxType == '' || inboxType == undefined || inboxType == null) 
                inboxType = 'inbox';
            
            $scope.reset();
            $rootScope.type = inboxType;
            $scope.selectAll = false;
            $scope.selectAll1 = false;
            $rootScope.messages =[];
            var page
            $rootScope.page={inbox_inbox:true,inbox_compose:false,inbox_view:false};

            $rootScope.clearToastr();
            var filters = angular.copy($rootScope.filters);
             filters.page = $rootScope.currentPage;

            if(inboxType == 'sent'){
                filters.msg_type = 1;
            }
            else if(inboxType == 'inbox'){
                filters.msg_type = 2;
            }

            if( !angular.isUndefined(filters.from)) {
                filters.from=$filter('date')(filters.from, 'dd-MM-yyyy');
            }
            if( !angular.isUndefined(filters.to)) {
                filters.to=$filter('date')(filters.to, 'dd-MM-yyyy');
            }

            filters.users = [];

            if( !angular.isUndefined(filters.users_)) {
                angular.forEach(filters.users_, function(v, k) {
                    filters.users.push(v.id);
                });
                delete filters.users_;
            }
            $rootScope.progressbar_start();
            Mailling.filter(filters,function(response) {
                $rootScope.progressbar_complete();
                $rootScope.messages = response.data;
                $scope.totalItems = response.total;
                $scope.currentPage = response.current_page;
                $scope.itemsPerPage = response.per_page;
            });
    };
    $scope.composeMsg = function(){
        $rootScope.page={inbox_inbox:false,inbox_compose:true,inbox_view:false};
        $rootScope.reply = false;
        $scope.composeData={};
        // $rootScope.type='';
        // $scope.userTags =[];
        $scope.tags = [];
        $scope.message={};
        // $scope.options=Mailling.getUsersList();
        // Mailling.getUsersList(function(response) {
        //     $scope.userTags =  response.users;
        // });
    };

    if($stateParams.id){
        if($stateParams.id == 'compose'){
            $scope.action =$stateParams.id;
            $scope.composeMsg();
        }else if($stateParams.id == 'inbox' || $stateParams.id == 'sent'  ){
            $scope.action =$stateParams.id;
            $scope.getMessages($stateParams.id);
        }else{
            $rootScope.progressbar_start();
            Mailling.get({id:$stateParams.id},function(response) {
                $rootScope.progressbar_complete();
                $scope.message =  response[0];
                $scope.message.users_to =  response[0].receipts;
                // $scope.tags = response[0].receipts;
                $scope.showMessage($scope.message , 'sent');
            });
        }
    }else{
        $scope.getMessages('inbox');
    }

    $rootScope.pageChanged = function (CurrentPage) {
        $rootScope.currentPage = CurrentPage;
        $scope.getMessages($rootScope.type);
    };

    $scope.deleteTag = function($index,target){
        $scope.row.users_to.splice($index,1);

    };

    $scope.replay = function(){
        $rootScope.user_range=$scope.message.receipts;

        var receipts_=[];
        angular.forEach($scope.message.receipts, function(v, k) {
            v.id = v.id + "";
            receipts_.push({id:v.id , name:v.name});
        });

        $scope.row = {'receipts' : receipts_ , 'users_to' : receipts_};
        $rootScope.reply = true;

    };

    $scope.showMessage = function(message , type){

        if(type == '' || type == undefined || type == null)
            type = 'inbox';

        $rootScope.page={inbox_inbox:false,inbox_compose:false,inbox_view:true};
        $rootScope.reply = false;
        $scope.composeData={};
        $rootScope.type='';
        // $scope.tags = [];
        // $scope.userTags =[];
        var msg_id = 0;

        if(type == 'sent'){
            if(message.msg_replay_id != null){
                msg_id = message.msg_replay_id;
            }else{
                msg_id = message.id;
            }
        }else{
            if(message.msg && message.msg.msg_replay_id != undefined)
                msg_id = message.msg.msg_replay_id ;
            else
                msg_id = message.msg_id;
        }
        $rootScope.progressbar_start();
        Mailling.get({id:msg_id},function(response) {
            $rootScope.progressbar_complete();
            $scope.message =  response[0];
            // $scope.message.users_to =  response[0].receipts;
            // $scope.tags = response[0].receipts;
            $scope.row = {users_to:response[0].receipts};
            // $scope.row.users_to =  response[0].receipts;
        });
            
    };

    $scope.compose = function(row,message){
        var fd = new FormData();
        row.msg_replay_id = 0;
        if(message){
            row.msg_replay_id = message.id;
            row.subject =  $filter('translate')('Reply to a message titled') + message.subject;
        }

        row.attach_files = $scope.attach_files;
        if(row.users_to != undefined && row.details != undefined && row.subject != undefined){
            $rootScope.progressbar_start();
            Mailling.store(row,function(response) {
                $rootScope.progressbar_complete();
                if(response.success){
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                    // $scope.row={};
                    $scope.tags=[];
                    $scope.attach_files=[];
                    angular.element("input[type='file']").val(null);
                    // $scope.getMessages($rootScope.type);
                    window.location = '#/maillingSystem/sent';

                }else{
                    $rootScope.toastrMessages('error',response.msg);
                }
            });
        }else{
            $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
        }
    };

    $scope.compose_ = function(row,message){
        var fd = new FormData();
        row.msg_replay_id = 0;
        if(message){
            row.msg_replay_id = message.id;
            row.subject =  $filter('translate')('Reply to a message titled') + message.subject;
        }

        row.attach_files = $scope.attach_files;
        if(row.users_to != undefined && row.details != undefined && row.subject != undefined){
            $rootScope.progressbar_start();
            Mailling.store(row,function(response) {
                $rootScope.progressbar_complete();
                if(response.success){
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                    // $scope.row={};
                    $scope.tags=[];
                    $scope.attach_files=[];
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.uploader.clearQueue();
                    angular.element("input[type='file']").val(null);
                    $scope.getMessages($rootScope.type);

                }else{
                    $rootScope.toastrMessages('error',response.msg);
                }
            });
        }else{
            $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
        }
    };

    $scope.loadTags = function(query) {
        // return  $scope.userTags;
    };

    $scope.onTagAdded = function($tag) {
        if($tag.id == undefined)
            $scope.tags.splice(($scope.tags.length-1),1);
    }


    $scope.attach_files = [];

    $scope.removeFile=function(index){
        var file_id = $scope.attach_files[index] ;

        $ngBootbox.confirm($filter('translate')('are you want to confirm delete'))
            .then(function() {
                var msg_type = $rootScope.type;
                $rootScope.progressbar_start();
                Mailling.destroyFile({id:file_id}, function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status){
                        Uploader.queue.splice(index, 1);
                        $scope.attach_files.splice(index,1);
                    }
                });
            });

    };
    $scope.fileUpload=function(item){
        $rootScope.progressbar_start();
        $timeout(function() {
            Uploader.onBeforeUploadItem = function(item) {
                $rootScope.clearToastr();
                item.formData = [{action:"not_check"}];
            };
            Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                $rootScope.progressbar_complete();
                $scope.uploader.destroy();
                // $scope.uploader.queue=[];
                // $scope.uploader.clearQueue();
                angular.element("input[type='file']").val(null);
                $scope.attach_files.push(response.id);
            };
            Uploader.uploadAll();
        }, 5);
    };
    var Uploader = $scope.uploader = new FileUploader({
        url: '/api/v1.0/doc/files',
        headers: {
            Authorization: OAuthToken.getAuthorizationHeader()
        }
    });

    $scope.selectedAll = function(value){
        $scope.selectAll = value;
        angular.forEach($rootScope.messages, function(val,key)
        {
            val.caseCheck=$scope.selectAll;
        });
    };
    $scope.selectedAll1 = function(value){
        $scope.selectAll1 = value;
        angular.forEach($rootScope.messages, function(val,key)
        {
            val.caseCheck1=$scope.selectAll1;
        });
    };
    $scope.cancel = function(){
        $ngBootbox.confirm($filter('translate')('are you want to cancel'))
            .then(function() {
                $scope.getMessages($rootScope.type);
            });
    };
    $scope.deleteMessages = function(){
        msgs = [];
        angular.forEach($rootScope.messages, function (v, k) {
            if (v.caseCheck) {
                if($rootScope.type == 'sent')
                    msgs.push(v.id);
                else
                    msgs.push(v.msg_id);
            }
        });
        if(msgs.length==0){
            $rootScope.toastrMessages('error',$filter('translate')('There is no selected row to delete'));
        }else{
            $ngBootbox.confirm($filter('translate')('are you want to confirm delete'))
                .then(function() {
                    var msg_type = $rootScope.type;
                    $rootScope.progressbar_start();
                    Mailling.destroyMesgs({ids:msgs,msg_type:msg_type}, function (response) {
                        $rootScope.progressbar_complete();
                        if(response.success){
                            $scope.getMessages($rootScope.type);
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                });

        }
      
    };
    $scope.remove_item = function($index){
        $scope.Attach_files.splice($index,1);
    }

    $scope.toggle = function() {
        $rootScope.clearToastr();
        $scope.toggleStatus = $scope.toggleStatus == false ? true: false;
        $rootScope.filters={};
        $rootScope.filters.page =1;
    };

    $scope.resetView = function(type) {
        $state.go('mailing-system',{"id":type});
    };

    $scope.options=Mailling.getUsersList();
    $rootScope.Org=Mailling.getUsersList();

});