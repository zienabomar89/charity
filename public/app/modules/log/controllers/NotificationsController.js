LogModule = angular.module('LogModule');

LogModule.controller('NotificationsController', function ($rootScope,$scope, Notification,$filter,$state) {

    $state.current.data.pageTitle = $filter('translate')('Notifications');
    $scope.currentPage=1;

    $scope.items =[];

    var fetchNotifications = function (params) {
        $rootScope.progressbar_start();
        params.filter_status='paginate';
        Notification.filter(params, function (response) {
            $rootScope.progressbar_complete();
            $rootScope.unread_notifications_count = response.unread_notifications_count;
            $scope.items = response.notifications.data;
            $scope.totalItems = response.notifications.total;
            $scope.currentPage = response.notifications.current_page;
            $scope.itemsPerPage = response.notifications.per_page;
        });
    };

    $scope.pageChanged = function() {
        var params = $scope.srch;
        params.page = $scope.currentPage;
        fetchNotifications(params);
    };

    $scope.read=function (row,$index) {
        if(row.status == 1 || row.status == '1'){
            window.location = row.url;
        }else{
            Notification.read({id:row.id}, function (response) {
                var data = response;
                if(data.status == 'true' || data.status == true){
                    // -- notice cnt + remove class from element
                    $rootScope.unread_notifications_count --;
                    row.status = 1;
                    window.location = row.url;
                }
            });
        }
    }

    fetchNotifications({page: 1});
});