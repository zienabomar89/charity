angular.module('LogModule', [
    "ui.router",
    "ngResource",
    "oc.lazyLoad",
    "pascalprecht.translate"
]);

LogModule = angular.module('LogModule');

LogModule.config(function ($stateProvider) {
    $stateProvider
        .state('log-logs-index', {
            url: '/log/logs',
            templateUrl: '/app/modules/log/views/logs/index.html',
            data: {pageTitle: 'Logs'},
            controller: 'LogsController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'LogsController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/app/modules/log/controllers/LogsController.js',
                                '/app/modules/log/services/LogService.js'
                            ]
                        });
                    }]
            }
        })
        .state('notifications', {
            url: '/notifications',
            templateUrl: '/app/modules/log/views/notifications/index.html',
            data: {pageTitle: 'Logs'},
            controller: 'NotificationsController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'notificationsController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/app/modules/log/controllers/NotificationsController.js',
                                '/app/modules/log/services/NotificationsService.js'
                            ]
                        });
                    }]
            }
        })
        .state('mailing-system', {
            url: '/maillingSystem/:id',
            templateUrl: '/app/modules/log/views/maillingSystem/index.html',
            data: {pageTitle: 'Mailling System'},
            params:{id:null},
            controller: 'MaillingController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'notificationsController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/app/modules/log/controllers/MaillingController.js',
                                '/app/modules/log/services/LogService.js',
                                '/app/modules/log/services/MaillingService.js',
                                '/themes/metronic/global/plugins/inbox/inbox.js',
                                "/themes/metronic/apps/css/inbox-rtl.min.css",
                                '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js'

                            ]
                        });
                    }]
            }
        })
        .state('my-suggestion', {
            url: '/suggestions',
            templateUrl: '/app/modules/log/views/Suggestion/my-suggestion/index.html',
            data: {pageTitle: 'Suggestion System'},
            controller: 'MySuggestionController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'suggestionController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/app/modules/log/controllers/SuggestionController.js',
                                '/app/modules/log/services/SuggestionService.js',
                                '/themes/metronic/global/plugins/inbox/inbox.js',
                                '/themes/metronic/apps/css/inbox-rtl.min.css',
                                '/app/modules/document/directives/thumb.js',
                                '/app/modules/log/services/LogService.js',
                                '/app/modules/document/directives/fileDownload.js',

                            ]
                        });
                    }]
            }
        })
        .state('add-suggest', {
            url: '/suggestions/add',
            templateUrl: '/app/modules/log/views/Suggestion/my-suggestion/add.html',
            data: {pageTitle: 'Suggestion System'},
            params: {id:null},
            controller: 'addSuggestController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'addSuggestController',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/log/controllers/SuggestionController.js',
                            '/app/modules/log/services/SuggestionService.js',
                            '/themes/metronic/global/plugins/inbox/inbox.js',
                            '/themes/metronic/apps/css/inbox-rtl.min.css',
                            '/app/modules/document/directives/thumb.js',
                            '/app/modules/document/directives/fileDownload.js',

                        ]
                    });
                }]
            }
        })
        .state('show-suggest', {
            url: '/suggestions/show/{id}',
            templateUrl: '/app/modules/log/views/Suggestion/my-suggestion/show.html',
                data: {pageTitle: 'Suggestion System'},
                params: {id:null},
                controller: 'showSuggestController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'showSuggestController',
                            files: [
                                '/app/modules/log/controllers/SuggestionController.js',
                                '/app/modules/log/services/SuggestionService.js',
                                '/themes/metronic/global/plugins/inbox/inbox.js',
                                '/app/modules/document/directives/fileDownload.js',

                            ]
                        });
                    }]
                }
            })
        .state('manage-suggestion', {
            url: '/suggestions/manage',
            templateUrl: '/app/modules/log/views/Suggestion/manage-suggestion/index.html',
            data: {pageTitle: 'Suggestion System'},
            controller: 'manageSuggestionController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'manageSuggestionController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/app/modules/log/controllers/SuggestionController.js',
                                '/app/modules/log/services/LogService.js',
                                '/app/modules/log/services/SuggestionService.js',
                                '/themes/metronic/global/plugins/inbox/inbox.js',
                                '/themes/metronic/apps/css/inbox-rtl.min.css',
                                '/app/modules/document/directives/thumb.js',
                                '/app/modules/document/directives/fileDownload.js'

                            ]
                        });
                    }]
            }
        })
        .state('manage-suggest', {
            url: '/suggestions/manage/{id}',
            templateUrl: '/app/modules/log/views/Suggestion/manage-suggestion/manage-suggest.html',
            data: {pageTitle: 'Suggestion System'},
            params: {id:null},
            controller: 'manageSuggestController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'manageSuggestController',
                        files: [
                            '/app/modules/log/controllers/SuggestionController.js',
                            '/app/modules/log/services/SuggestionService.js',
                            '/themes/metronic/global/plugins/inbox/inbox.js',
                            '/app/modules/document/directives/fileDownload.js'

                        ]
                    });
                }]
            }
        });
});