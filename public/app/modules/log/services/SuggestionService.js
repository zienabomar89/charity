LogModule = angular.module('LogModule');
LogModule.factory('Suggestion', function ($resource) {
    return $resource('/api/v1.0/Suggestion/:operation/:id', {id: '@id'}, {
        query: {method: 'GET', isArray: true},
        filter:{method:'POST' ,params:{operation:'filter'}, isArray:false },
        store:{method:'POST' ,params:{operation:'store'}, isArray:false },
        show: { method:'GET', isArray: false},
        reply:{method:'POST' ,params:{operation:'reply'}, isArray:false },
        destroy: { method:'DELETE', params: {operation:'destroy'}, isArray: false},
        destroyFile:{method:'DELETE' ,params:{operation:'destroyFile'}, isArray:false },
      });
});
