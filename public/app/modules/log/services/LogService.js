LogModule = angular.module('LogModule');
LogModule.factory('Log', function ($resource) {
    return $resource('/api/v1.0/logs/:operation/:id', {id: '@id'}, {
        getOptions:{method:'GET' ,params:{operation:'getOptions'}, isArray:false },
        filter:{method:'POST' ,params:{operation:'filter'}, isArray:false },
        query: {method: 'GET', isArray: false},
        getCardsByUser:{method:'GET' ,params:{operation:'getCardsByUser'}, isArray:true }
    });
});
