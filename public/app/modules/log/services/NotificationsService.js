LogModule = angular.module('LogModule');
LogModule.factory('Notification', function ($resource) {
    return $resource('/api/v1.0/notifications/:operation/:id', {page: '@page',id: '@id'}, {
        revokeUser:{method:'GET' ,params:{operation:'revokeUser'}, isArray:false },
        unreadMessages:{method:'GET' ,params:{operation:'unreadMessages'}, isArray:false },
        getOptions:{method:'GET' ,params:{operation:'getOptions'}, isArray:false },
        filter:{method:'POST' ,params:{operation:'filter'}, isArray:false },
        header:{method:'POST' ,params:{operation:'header'}, isArray:false },
        read:{method:'PUT' ,params:{operation:'read'}, isArray:false },
        logout:{method:'PUT' ,params:{operation:'logout'}, isArray:false },
        query: {method: 'GET', isArray: false},
        checkCards:{method:'POST' ,params:{operation:'checkCards'}, isArray:false },

    });
});

LogModule.factory('Language', function ($resource) {
    return $resource('/api/v1.0/Language/:operation/:id', {page: '@page',id: '@id'}, {
        query: {method: 'GET', isArray: false},
        getLocale:{method:'GET' ,params:{operation:'getLocale'}, isArray:false },
        setLocale:{method:'PUT' ,params:{operation:'setLocale'}, isArray:false }
    });
});
