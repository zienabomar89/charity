LogModule = angular.module('LogModule');
LogModule.factory('Mailling', function ($resource) {
    return $resource('/api/v1.0/Mailling/:operation/:id', {id: '@id'}, {
        query: {method: 'GET', isArray: true},
        store: {method: 'POST', isArray: false},
        get: {method: 'GET', isArray: true},
        destroyFile:{method:'DELETE' ,params:{operation:'destroyFile'}, isArray:false },
        destroyMesgs:{method:'POST' ,params:{operation:'destroyMesgs'}, isArray:false },
        filter:{method:'POST' ,params:{operation:'filter'}, isArray:false },
        getUsersList:{method:'POST' ,params:{operation:'getUsersList'}, isArray:false },
    });
});
