
var CommonModule = angular.module("CommonModule",[]);
angular.module('CommonModule').config(function ($stateProvider) {
    $stateProvider

        .state('CitizenRequest', {
            url: '/citizens/request',
            templateUrl: '/app/modules/common/views/CitizenRequest/index.html',
            params:{type:null},
            controller: "CitizenRequestController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/common/controllers/CitizenRequestController.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/RelayService.js'

                        ]
                    });
                }]
            }
        })
        .state('relays', {
            url: '/relays/:type',
            templateUrl: '/app/modules/common/views/relays/index.html',
            params:{type:null},
            controller: "relaysController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/common/controllers/reports/relaysController.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/RelayService.js'

                        ]
                    });
                }]
            }
        })
        .state('confirm-relays', {
            url: '/relay/confirm/:type',
            templateUrl: '/app/modules/common/views/relays/confirm.html',
            params:{type:null},
            controller: "confirmRelayController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/app/modules/setting/services/EntityService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/common/controllers/reports/relaysController.js',
                            '/app/modules/common/services/RelayService.js'

                        ]
                    });
                }]
            }
        })

        .state('attachments', {
            url: '/:type/cases/attachments',
            templateUrl: '/app/modules/common/views/cases/attachments.html',
            params:{},
            controller: "AttachmentsController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/common/controllers/AttachmentsController.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js'

                        ]
                    });
                }]
            }

        })
        .state('statistic', {
            url: '/statistic/:type',
            templateUrl: '/app/modules/common/views/statistic/index.html',
            params: {type:null},
            data: {pageTitle: ''},
            controller: "statisticController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/common/controllers/statisticController.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/common/services/PersonsService.js'

                        ]
                    });
                }]
            }

        })
        .state('incomplete-data', {
            url: '/:type/cases/incomplete-data',
            templateUrl: '/app/modules/common/views/cases/incomplete-data.html',
            params:{},
            data: {pageTitle: ''},
            controller: "IncompleteController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/common/controllers/IncompleteController.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/organization/services/OrgService.js'


                        ]
                    });
                }]
            }

        })
        .state('general-setting', {
            url: '/:type/general-setting',
            templateUrl: '/app/modules/common/views/general-setting.html',
            params:{},
            controller: "GeneralSettingController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/common/controllers/GeneralSettingController.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/setting/services/EntityService.js'

                        ]
                    });
                }]
            }
        })
        .state('backup', {
            url: '/backup',
            templateUrl: '/app/modules/common/views/backup.html',
            params:{},
            controller: "BackupController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/app/modules/common/controllers/BackupController.js',
                            '/app/modules/common/services/BackupService.js'

                        ]
                    });
                }]
            }
        })

        .state('categories', {
            url: '/:type/categories',
            templateUrl: '/app/modules/common/views/categories/index.html',
            data: {pageTitle: ''},
            params:{status:null,mode:null},
            controller: "CategoriesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/app/modules/common/controllers/categories/CategoriesController.js',
                            '/app/modules/common/services/CategoriesService.js'
                        ]
                    });
                }]
            }
        })
        .state('categories-edit', {
            url: '/categories/:type/:action/:id',
            templateUrl: '/app/modules/common/views/categories/form.html',
            params:{id:null , action:null,type:null },
            data: {pageTitle: ''},
            controller: "CategoriesEditController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CategoriesEditController',
                        files: [
                            '/app/modules/common/controllers/categories/CategoriesEditController.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/setting/services/EntityService.js'
                        ]
                    });
                }]
            }
        })
        .state('form-setting', {
            url: '/category/:type/form-setting/:id',
            templateUrl: '/app/modules/common/views/categories/form-setting.html',
            params:{id:null , action:null,type:null },
            controller: "CategoriesCaseFormController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/app/modules/common/controllers/categories/CategoriesCaseFormController.js',
                            '/app/modules/common/services/CategoriesService.js'
                        ]
                    });
                }]
            }
        })
        .state('categories-template', {
            url: '/:type/categories/templates',
            templateUrl: '/app/modules/common/views/categories/templates.html',
            params:{type: null},
            controller: "CategoriesTemplateController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/app/modules/common/controllers/categories/CategoriesTemplateController.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js',
                            '/app/modules/setting/services/SettingsService.js'
                        ]
                    });
                }]
            }

        })

        .state('category-custom-forms', {
            url: '/:type/custom-forms',
            templateUrl: '/app/modules/common/views/categories/custom-forms.html',
            params:{type: null},
            data: {pageTitle: ''},
            controller: "CustomFormsController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/common/controllers/categories/CustomFormsController.js',
                            '/app/modules/forms/services/CustomFormsService.js',
                            '/app/modules/common/services/CategoriesService.js'
                        ]
                    });
                }]
            }

        })
        .state('visitor-notes', {
            url: '/:type/visitor-notes',
            templateUrl: '/app/modules/common/views/categories/visitor-notes.html',
            params:{type: null},
            data: {pageTitle: ''},
            controller: "VisitorNotesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/common/controllers/VisitorNotesController.js',
                            '/app/modules/common/services/VisitorNotesService.js'
                        ]
                    });
                }]
            }

        })
        .state('policy', {
            url: '/:type/policy',
            templateUrl: '/app/modules/common/views/categories/policy.html',
            data: { pageParantTitle:'المساعدات',pageTitle: 'القيود على انواع الاستمارات'},
            params:{type: null},
            controller: "PolicyController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/common/controllers/PolicyController.js',
                            '/app/modules/common/services/PolicyService.js'
                        ]
                    });
                }]
            }

        })
        .state('updating-data-setting', {
            url: '/updating-data-setting',
            templateUrl: '/app/modules/common/views/updating-data-setting.html',
            params:{},
            controller: "UpdateDateSettingController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/common/controllers/UpdateDateSettingController.js',
                            '/app/modules/common/services/updateSettingService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/locationsService.js'


                        ]
                    });
                }]
            }

        })

        .state('blocked', {
            url: '/:type/blocked',
            templateUrl: '/app/modules/common/views/blocked/cases.html',
            params:{'type':null },
            data: {pageTitle: ''},
            controller: "BlockedController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/common/controllers/BlockedController.js',
                            '/app/modules/common/services/BlockService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js'
                        ]
                    });
                }]
            }

        })
        .state('blocked-sponsor', {
            url: '/blocked/sponsor',
            templateUrl: '/app/modules/common/views/blocked/sponsor.html',
            params:{'type':null },
            data: {pageTitle: ''},
            controller: "BlockedSponsorController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/common/controllers/BlockedController.js',
                            '/app/modules/common/services/BlockService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js'
                        ]
                    });
                }]
            }

        })
        .state('social-affairs-receipt', {
            url: '/reports/socialAffairsReceipt',
            templateUrl: '/app/modules/common/views/reports/socialAffairsReceipt/index.html',
            params:{},
            controller: "socialAffairsReceiptController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/app/modules/setting/services/EntityService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/common/controllers/reports/socialAffairsReceiptController.js',
                            '/app/modules/common/services/reportsService.js',
                            '/app/modules/common/services/RelayService.js',
                            '/app/modules/log/services/LogService.js'
                        ]
                    });
                }]
            }
        })
        .state('social-affairs-receipt-details', {
            url: '/reports/socialAffairsReceipt/:id',
            templateUrl: '/app/modules/common/views/reports/socialAffairsReceipt/detail.html',
            params:{id:null},
            controller: "socialAffairsReceiptDetailsController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/app/modules/setting/services/EntityService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/common/controllers/reports/socialAffairsReceiptController.js',
                            '/app/modules/common/services/RelayService.js',
                            '/app/modules/common/services/reportsService.js',
                            '/app/modules/log/services/LogService.js'
                        ]
                    });
                }]
            }
        })

        .state('citizen-repository', {
            url: '/reports/citizenRepository',
            templateUrl: '/app/modules/common/views/reports/citizenRepository/index.html',
            params:{},
            controller: "citizenRepositoryController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/common/services/RelayService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/reportsService.js',
                            '/app/modules/common/controllers/reports/citizenRepositoryController.js',
                            '/app/modules/log/services/LogService.js'

                        ]
                    });
                }]
            }
        })
        .state('citizen-repository-details', {
            url: '/reports/citizenRepository/:id',
            templateUrl: '/app/modules/common/views/reports/citizenRepository/detail.html',
            params:{id:null},
            controller: "citizenDetailsController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/common/services/RelayService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/reportsService.js',
                            '/app/modules/common/controllers/reports/citizenRepositoryController.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/themes/metronic/pages/css/profile-rtl.css',
                            '/app/modules/log/services/LogService.js'

                        ]
                    });
                }]
            }
        })

        .state('social-affairs', {
            url: '/reports/socialAffairs',
            templateUrl: '/app/modules/common/views/reports/socialAffairs/index.html',
            params:{},
            controller: "socialAffairsController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/app/modules/setting/services/EntityService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/common/controllers/reports/socialAffairsController.js',
                            '/app/modules/common/services/reportsService.js',
                            '/app/modules/common/services/RelayService.js',
                            '/app/modules/log/services/LogService.js'
                        ]
                    });
                }]
            }
        })
        .state('social-affairs-details', {
            url: '/reports/socialAffairs/:id',
            templateUrl: '/app/modules/common/views/reports/socialAffairs/detail.html',
            params:{id:null},
            controller: "affairsDetailsController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/app/modules/setting/services/EntityService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/common/controllers/reports/socialAffairsController.js',
                            '/app/modules/common/services/RelayService.js',
                            '/app/modules/common/services/reportsService.js',
                            '/app/modules/log/services/LogService.js'
                        ]
                    });
                }]
            }
        })

        .state('aid-committee', {
            url: '/reports/aidsCommittee',
            templateUrl: '/app/modules/common/views/reports/aidsCommittee/index.html',
            params:{},
            controller: "aidsCommitteeController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/app/modules/setting/services/EntityService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/common/controllers/reports/aidsCommitteeController.js',
                            '/app/modules/common/services/RelayService.js',
                            '/app/modules/common/services/reportsService.js',
                            '/app/modules/log/services/LogService.js'
                        ]
                    });
                }]
            }
        })
        .state('aid-committee-details', {
            url: '/reports/aidsCommittee/:id',
            templateUrl: '/app/modules/common/views/reports/aidsCommittee/detail.html',
            params:{id:null},
            controller: "aidsCommitteeDetailsController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/app/modules/setting/services/EntityService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/common/controllers/reports/aidsCommitteeController.js',
                            '/app/modules/common/services/RelayService.js',
                            '/app/modules/common/services/reportsService.js',
                            '/app/modules/log/services/LogService.js'
                        ]
                    });
                }]
            }
        })
        .state('search_all', {
            url: '/reports/searchAll',
            templateUrl: '/app/modules/common/views/reports/searchAll/index.html',
            params:{},
            controller: "searchAllController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/common/services/RelayService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/reportsService.js',
                            '/app/modules/common/services/RelayService.js',
                            '/app/modules/common/controllers/reports/searchAllController.js',
                            '/app/modules/log/services/LogService.js'

                        ]
                    });
                }]
            }
        })
        .state('search_all_details', {
            url: '/reports/searchAll/:id',
            templateUrl: '/app/modules/common/views/reports/searchAll/detail.html',
            params:{id:null},
            controller: "searchAllDetailsController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/common/services/RelayService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/RelayService.js',
                            '/app/modules/common/services/reportsService.js',
                            '/app/modules/common/controllers/reports/searchAllController.js',
                            '/themes/metronic/pages/css/profile-rtl.css',
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/app/modules/log/services/LogService.js'

                        ]
                    });
                }]
            }
        })

        .state('search', {
            url: '/search/:type',
            templateUrl: '/app/modules/common/views/search/index.html',
            params:{'type':null },
            data: {pageTitle: ''},
            controller: "SearchController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        reload: false,
                        files: [
                            '/app/modules/common/controllers/search/SearchController.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/log/services/LogService.js'
                        ]
                    });
                }]
            }
        })
        .state('detail', {
            url: '/search/:type/detail/:id',
            templateUrl: '/app/modules/common/views/search/detail.html',
            controller: "SearchDetailController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/common/controllers/search/SearchDetailController.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/app/modules/aid/services/VouchersCategoriesService.js',
                            '/app/modules/organization/services/OrgService.js'
                        ]
                    });
                }]
            }
        })
        .state('vouchers-search', {
            url: '/vouchers-search',
            templateUrl: '/app/modules/common/views/search/vouchers-search.html',
            params:{'type':null },
            data: {pageTitle: ''},
            controller: "vouchersSearchController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        reload: false,
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/common/controllers/search/vouchersSearchController.js',
                            '/app/modules/aid/services/VouchersCategoriesService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                        ]
                    });
                }]
            }
        })
        .state('sponsorship-search', {
            url: '/sponsorship-search',
            templateUrl: '/app/modules/common/views/search/sponsorship-search.html',
            data: {pageTitle: ''},
            params: {mode:null,status:null},
            controller: "sponsorshipSearchController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/common/controllers/search/sponsorshipSearchController.js',

                        ]
                    });
                }]
            }

        })


        .state('transfers', {
            url: '/transfers',
            templateUrl: '/app/modules/common/views/transfers/index.html',
            params:{type:null},
            controller: "transfersController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/common/controllers/reports/transfersController.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/RelayService.js'

                        ]
                    });
                }]
            }
        })
        .state('accepted-transfers', {
            url: '/accepted-transfers',
            templateUrl: '/app/modules/common/views/transfers/accepted.html',
            params:{type:null},
            controller: "acceptedTransfersController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CommonModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/common/controllers/reports/transfersController.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/RelayService.js'

                        ]
                    });
                }]
            }
        })


    ;

});
