
angular.module('CommonModule').controller('CategoriesCaseFormController', function($filter,$stateParams,$rootScope, $scope,$uibModal, $http, $timeout,$state,category) {

    $rootScope.category_type=$scope.category_type=$stateParams.type == 'aids'? 'aids' : 'sponsorships';

    $scope.success =false;
    $scope.failed =false;
    $scope.error =false;
    $scope.model_error_status =false;

    $scope.close=function(){
        $scope.success =false;
        $scope.failed =false;
        $scope.error =false;
        $scope.model_error_status =false;
    };

    $scope.status ='';
    $scope.msg ='';
    $scope.msg2 ='';
    $scope.msg1=[];

    category.getCategoryName({'id':$stateParams.id,'type':$stateParams.type},function (response){
                $rootScope.category_name=response.name;
    });
    category.getElements({'id':$stateParams.id,'type':$stateParams.type},function (response) {
        var temp=response;
        angular.forEach(temp, function(v, k) {
            v.priority=v.priority +"";
        });

        $scope.items= temp;

    });
    $scope.update=function(element,index){
        element.type=$stateParams.type;
        var target = new category(element);
        $rootScope.progressbar_start();
        category.updateElement(element ,function (response) {
            $rootScope.progressbar_complete();
            if(response.status=='error' || response.status=='failed') {
                    $rootScope.toastrMessages('error',response.msg);
                }
                else if(response.status=='failed_valid')
                {
                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                    $scope.model_error_status =true;
                    $scope.status1=response.status;
                    $scope.msg1[index] =response.msg;
                }
                else{
                    if(response.status=='success') {
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                }
        });
    };


});
