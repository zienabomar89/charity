
angular.module('CommonModule')
    .controller('CategoriesEditController' ,function($filter,$state,$stateParams,$rootScope, $scope, $http ,$timeout,$log,category,Entity) {

        
        $rootScope.failed =false;
        $rootScope.model_error_status =false;

       $rootScope.category_type=$scope.category_type=$stateParams.type == 'aids'? 'aids' : 'sponsorships';

        $rootScope.action=$scope.action=$stateParams.action;

        $rootScope.close=function(){
            $rootScope.failed =false;
            $rootScope.model_error_status =false;
        };

        $rootScope.mode ='';
        $rootScope.status ='';
        $rootScope.msg ='';
        $scope.item={};


        if($stateParams.action=='edit' && $stateParams.id == null){
            $state.go('categories',{type:$stateParams.type});
        }
        else if($stateParams.action=='edit' && $stateParams.id != null){

            $rootScope.progressbar_start();
            category.get({'id':$stateParams.id,'type':$stateParams.type},function (response) {
                $rootScope.progressbar_complete();
                var temp=response;
                if($stateParams.type == 'sponsorships'){
                    temp.has_Wives = temp.has_Wives +"";
                    temp.case_is = temp.case_is +"";
                    temp.family_structure = temp.family_structure +"";
                    temp.has_mother = temp.has_mother +"";
                    temp.father_dead = temp.father_dead +"";
                }
                angular.forEach(temp.documents, function(v, k) {
                    if(v.checked== 'true'){
                        v.checked=true;
                    }else{
                        v.checked=false;
                    }

                    if(v.owner == "-"){
                        v.owner="";
                    }else{
                        v.owner=v.owner +"";
                    }
                });

                $scope.item = response;

            });

        }else if($stateParams.action=='new'){

            Entity.list({entity:'document-types'},function (response) {
                var temp=response;
                angular.forEach(temp, function(v, k) {
                    v.checked=false;
                });
                $scope.item.documents=temp;

            });
        }

        $scope.Confirm = function (choice,item){

           if($rootScope.action =='new'){
               $rootScope.progressbar_start();
               item.type=$rootScope.category_type;
                category.save(item,function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status=='error' || response.status=='failed') {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else if(response.status=='failed_valid') {
                            $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                            $scope.status1 =response.status;
                            $scope.msg1 =response.msg;
                        }
                        else{
                            if(response.status=='success') {
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                $timeout(function() {
                                    if(choice =='continue'){
                                        $state.go('form-setting',{id:response.category_id,'type':$stateParams.type});
                                    }else{
                                        $state.go('categories',{mode:$rootScope.action ,status:'success',type:$stateParams.type});
                                    }
                                }, 3000);

                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $timeout(function() {
                                    $state.go('categories',{mode:$rootScope.action ,status:'error',type:$stateParams.type});
                                }, 3000);

                            }

                        }


                    });

            }else{

                item.category_type=item.type;
                item.type=$rootScope.category_type;

               $rootScope.progressbar_start();
               var editCategory = new category(item);
                editCategory.$update()  .then(function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status=='error' || response.status=='failed')
                    {
                        $rootScope.toastrMessages('error',response.msg);
                    }
                    else if(response.status=='failed_valid')
                    {
                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                        $scope.status1 =response.status;
                        $scope.msg1 =response.msg;
                    }
                    else{

                        if(response.status=='success')
                        {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            $timeout(function() {
                                if(choice =='continue'){
                                    $state.go('form-setting',{id:response.category_id,'type':$stateParams.type});
                                }else{
                                    $state.go('categories',{mode:$rootScope.action ,status:'success',type:$stateParams.type});
                                }
                            }, 3000);
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                            $timeout(function() {
                                $state.go('categories',{mode:$rootScope.action ,status:'error',type:$stateParams.type});
                            }, 3000);

                        }

                    }
                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });

            }
        };


    });

