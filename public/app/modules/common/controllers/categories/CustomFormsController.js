
angular.module('CommonModule')
    .controller('CustomFormsController', function( $filter,$stateParams,$rootScope, $scope,$uibModal, $http, $ngBootbox,$timeout,$state,category,custom_forms) {

        $rootScope.type =$scope.type = $stateParams.type == 'aids'? 'aids' : 'sponsorships';
        $state.current.data.pageTitle = $filter('translate')('custom-forms');
        $scope.items=[];
        $rootScope.deafult_form=null;

        $rootScope.Forms = custom_forms.getCustomFormsList();

        var Forms= function(){
            $rootScope.progressbar_start();
            category.forms({type:$rootScope.type},function (response) {
                $rootScope.progressbar_complete();
                $scope.items= response.data;
                $rootScope.deafult_form= response.deafult_form;
            });

        };

        $scope.setForm=function(category_id,row){

            $uibModal.open({
                templateUrl: 'myModalContent1.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'sm',
                controller: function ($rootScope,$scope, $modalInstance, $log,category) {

                    $scope.row= {};
                    $scope.msg1={};

                    if(category_id == -1){
                        if($rootScope.deafult_form != null ){
                            $scope.row.form_id = $rootScope.deafult_form;
                        }
                        $scope.row.category_id = category_id;
                        $scope.row.form_id = $rootScope.deafult_form;
                    }else{
                        $scope.row= row;
                    }

                    $scope.confirm = function (data) {
                        $rootScope.progressbar_start();
                        category.setForm({'id':data.category_id,'type':$rootScope.type,'form_id':data.form_id},function (response) {
                            $rootScope.progressbar_complete();
                            $modalInstance.close();
                            if(response.status=='success')
                            {
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                Forms({'type':$rootScope.type,'page':$scope.CurrentPage});
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                            $modalInstance.close();
                        });

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                },
                resolve: {}
            });
        };
        
        
        $scope.setFormStatus=function(id,status){
        
         category.setFormStatus({'type':$rootScope.type,'id':id,'status':status},function (response) {
               $rootScope.progressbar_complete();
                      if(response.status=='success'){
                          $rootScope.toastrMessages('success',$filter('translate')('action success'));
                          Forms();
                      }else{
                        $rootScope.toastrMessages('error',response.msg);
                      }
                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });

        };
        
        
       Forms();

    });
