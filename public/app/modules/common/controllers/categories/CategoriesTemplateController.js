

angular.module('CommonModule')
    .controller('CategoriesTemplateController' ,function($stateParams,$rootScope, $scope, $http ,$timeout, $uibModal, $log,category,FileUploader,setting, OAuthToken,$filter,$state) {


        $rootScope.model_error_status =false;
        $scope.items=[];
        $rootScope.close=function(){
            $rootScope.failed =false;
            $rootScope.model_error_status =false;
        };

        $rootScope.msg ='';

        $rootScope.type =$scope.type = $stateParams.type == 'aids'? 'aids' : 'sponsorships';
        $state.current.data.pageTitle = $stateParams.type == 'aids'? $filter('translate')('categories-aid-template') :
                                                                     $filter('translate')('categories-sponsorships-template');

        var RestTemplate= function () {
            var param={};

            if($rootScope.type=='aids'){
                param={id:'aid-default-template'}
            }else{
                param={id:'Sponsorship-default-template'}
            }
            setting.getSettingById(param,function (response) {
                $rootScope.has_default_template=response.status;
                if(response.status){
                    $rootScope.default_template=response.Setting.value +"";
                }
            });
        };

        RestTemplate();
        CategoriesTemplate();

        $scope.ChooseTemplate=function(id,action,target){
            $rootScope.id=id;
            $rootScope.action=action;

            $scope.f=false;
            $uibModal.open({
                templateUrl: 'myModalContent2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/common/'+$stateParams.type+'/category/template-upload',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });
                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.progressbar_start();
                        item.formData = [{category_id: $rootScope.id}];
                    };
                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        $rootScope.toastrMessages('success',$filter('translate')('The template is successfully'));
                        if(target == 'default'){
                            RestTemplate();
                        }else{
                            CategoriesTemplate();
                        }
                        $modalInstance.close();

                    };


                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });


        };

        function CategoriesTemplate(){
            $rootScope.progressbar_start();
            category.getCategoriesTemplate({'type':$stateParams.type},function (response) {
                $rootScope.progressbar_complete();
                $scope.items = response;
            });
        }

        $rootScope.downloadFile=function(target,id){
            var url='';

           if(target =='template-instructions'){

               url='/api/v1.0/common/'+$stateParams.type+'/category/templateInstruction';
               file_type='pdf';
           }else{
               url='/api/v1.0/common/'+$stateParams.type+'/category/template/'+id;
               file_type='docx';
           }


            $rootScope.progressbar_start();
            $http({
                url:url,
                method: "GET"
            }).then(function (response) {
                $rootScope.progressbar_complete();
                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token+'&template=true';
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };

        $rootScope.defaultTemplate=function(){
            $rootScope.progressbar_start();
            $http({
                url:'/api/v1.0/common/'+$stateParams.type+'/category/defaultTemplate',
                method: "GET"
            }).then(function (response) {
                $rootScope.progressbar_complete();
                window.location = '/api/v1.0/common/files/docx/export?download_token='+response.data.download_token+'&template=true';
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };

    });

