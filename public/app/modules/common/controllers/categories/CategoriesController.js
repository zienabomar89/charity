
angular.module('CommonModule')
    .controller('CategoriesController' ,function($state,$stateParams,$rootScope, $scope, $http ,$timeout, $uibModal,category,$filter,$ngBootbox) {

        $rootScope.settings.layout.pageSidebarClosed = true;
        $rootScope.category_type=$scope.category_type=$stateParams.type == 'aids'? 'aids' : 'sponsorships';

        $state.current.data.pageTitle = $stateParams.type == 'aids'? $filter('translate')('categories-aid') :
                                                                     $filter('translate')('categories-sponsorships');
        $scope.items=[];
        if($scope.category_type =='aids'){
            $scope.type=2;
        }else{
            $scope.type=1;
        }

        $scope.itemsCount='50';

        $rootScope.status ='';
        $rootScope.msg ='';

        $rootScope.basic =$scope.mode = $stateParams.mode;

        var loadCategories=function($param){
            $rootScope.progressbar_start();
            $param.type = $scope.category_type;
            $param.itemsCount= $scope.itemsCount ;

            category.query($param,function (response) {
                $rootScope.progressbar_complete();
                $scope.items= response.data;
                $scope.CurrentPage = response.current_page;
                $rootScope.TotalItems = response.total;
                $rootScope.ItemsPerPage = response.per_page;
            });

        };

        loadCategories({'page':1,'type':2 ,'itemsCount' :50});

        $rootScope.pageChanged = function (CurrentPage) {
            loadCategories({'page':CurrentPage,'type':2 , 'itemsCount' :$scope.itemsCount});
        };

        $rootScope.itemsPerPage_ = function (itemsCount) {
            $scope.itemsCount = itemsCount ;
            loadCategories({'page':1,'itemsCount':itemsCount,'type':2});
        };

        $scope.delete = function(size,id) {
            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                .then(function() {
                    $rootScope.progressbar_start();
                    category.delete({'id': id,'type':$rootScope.category_type},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success')
                        {
                            $rootScope.toastrMessages('success',$filter('translate')('The row is deleted from db'));
                            loadCategories({'page':$scope.CurrentPage});
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                });
        };
    });

