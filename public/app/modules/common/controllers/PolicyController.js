
angular.module('AidModule')
    .controller('PolicyController', function( $filter,$stateParams,$rootScope, $scope,$uibModal, $http, $timeout,$state,categories_policy,$ngBootbox) {

        $rootScope.type =$scope.type = $stateParams.type == 'aids'? 'aids' : 'sponsorships';
        $state.current.data.pageTitle = $filter('translate')('Policy');


        $rootScope.failed = false;
        $rootScope.error = false;
        $rootScope.model_error_status = false;
        $scope.items=[];
        $scope.itemsCount='50';
        $rootScope.close = function () {

            $rootScope.failed = false;
            $rootScope.error = false;
            $rootScope.model_error_status = false;
        };
        $scope.sort = function (keyname) {
            $scope.sortKey = keyname;
            $scope.reverse = !$scope.reverse;
        };
        $scope.filter={};
        var resetPolicyFilter =function(){
            $scope.filter={
                "max_amount":"",
                "min_amount":"",
                "level":"",
                "category_id":"",
                "started_at_from":"",
                "started_at_to":"",
                "ends_at_from":"",
                "ends_at_to":"",
                "created_at_from":"",
                "created_at_to":""
            };

            $scope.filter.type=$rootScope.type;
        };

        var LoadPolicy= function($param){
            $rootScope.progressbar_start();
            categories_policy.getPolicy($param,function (response) {
                $rootScope.progressbar_complete();
                $scope.items= response.data;
                    $scope.CurrentPage = response.current_page;
                    $rootScope.TotalItems = response.total;
                    $rootScope.ItemsPerPage = response.per_page;
                });
         };

        LoadPolicy({'type':$rootScope.type,'page':1,'itemsCount' : 50});

        $rootScope.pageChanged = function (CurrentPage) {
            LoadPolicy({'type':$rootScope.type,'page':CurrentPage , 'itemsCount' :$scope.itemsCount});
          };

        $rootScope.itemsPerPage_ = function (itemsCount) {
            $scope.itemsCount = itemsCount ;
            LoadPolicy({'type':$rootScope.type,'page':1,'itemsCount' : $scope.itemsCount});
          };


        if($rootScope.type =='sponsorships'){
            $scope.choices=[];
            angular.forEach($rootScope.sponsorCategories, function(v, k) {
                if ($rootScope.lang == 'ar'){
                    $scope.choices.push({'id':v.id,'name':v.name});
                }
                else{
                    $scope.choices.push({'id':v.id,'name':v.en_name});
                }
            });

        } else{
            $scope.choices=[];
            angular.forEach($rootScope.aidCategories, function(v, k) {
                if ($rootScope.lang == 'ar'){
                    $scope.choices.push({'id':v.id,'name':v.name});
                }
                else{
                    $scope.choices.push({'id':v.id,'name':v.en_name});
                }
            });
        }

        $scope.toggleStatus = true;
        $scope.toggleSearchform = function() {
            $scope.toggleStatus = $scope.toggleStatus == false ? true: false;
                resetPolicyFilter();
        };

        $scope.Search=function(data) {

            if( !angular.isUndefined(data.started_at_from)) {
                data.started_at_from=$filter('date')(data.started_at_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(data.started_at_to)) {
                data.started_at_to= $filter('date')(data.started_at_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(data.ends_at_from)) {
                data.ends_at_from=$filter('date')(data.ends_at_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(data.ends_at_to)) {
                data.ends_at_to= $filter('date')(data.ends_at_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(data.created_at_from)) {
                data.created_at_from=$filter('date')(data.created_at_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(data.ends_at_to)) {
                data.created_at_to= $filter('date')(data.created_at_to, 'dd-MM-yyyy')
            }

            data.type=$rootScope.type;
            data.page=1;
            LoadPolicy(data);

        };

        $scope.new=function(action,size){
            $rootScope.action=action;

            $uibModal.open({
                templateUrl: 'myModalContent.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope, $modalInstance, $log,categories_policy) {
                    $scope.msg1={};
                    if($rootScope.type =='sponsorships'){
                        $scope.choices=[];
                        angular.forEach($rootScope.sponsorCategories, function(v, k) {
                            if ($rootScope.lang == 'ar'){
                                $scope.choices.push({'id':v.id,'name':v.name});
                            }
                            else{
                                $scope.choices.push({'id':v.id,'name':v.en_name});
                            }
                        });

                    } else{
                        $scope.choices=[];
                        angular.forEach($rootScope.aidCategories, function(v, k) {
                            if ($rootScope.lang == 'ar'){
                                $scope.choices.push({'id':v.id,'name':v.name});
                            }
                            else{
                                $scope.choices.push({'id':v.id,'name':v.en_name});
                            }
                        });
                    }

                    $scope.policy={checkedAll:true};

                    $scope.reset = function(value){

                            if (!angular.isUndefined($scope.msg1.started_at)) {
                                $scope.msg1.started_at = [];
                            }
                             if (!angular.isUndefined($scope.msg1.ends_at)) {
                                $scope.msg1.ends_at = [];
                            }
                            $scope.policy.started_at = "";
                            $scope.policy.ends_at = "";


                    };

                    $scope.confirm = function (row) {

                        var policy = angular.copy(row);
                        policy.type=$rootScope.type;

                        if(policy.category_id){
                            var cat = [];
                            angular.forEach(policy.category_id, function(v, k) {
                                cat.push(v.id);
                            });
                            policy.category_id =  cat
                        }

                        $rootScope.progressbar_complete();
                        var target = new categories_policy(policy);
                        target.$save()
                            .then(function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    $modalInstance.close();
                                    if(response.status=='success')
                                    {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        LoadPolicy({'type':$rootScope.type,'page':$scope.CurrentPage});
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }

                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();

                            });
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.dateOptions = {
                        formatYear: 'yy',
                        startingDay: 0
                    };
                    $scope.formats = [ 'dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                    $scope.format = $scope.formats[0];
                    $scope.today = function() {
                        $scope.dt = new Date();
                    };
                    $scope.today();
                    $scope.clear = function() {
                        $scope.dt = null;
                    };
                    $scope.open4 = function($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.popup4.opened = true;
                    };
                    $scope.popup4 = {
                        opened: false
                    };
                    $scope.open3 = function($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.popup3.opened = true;
                    };
                    $scope.popup3 = {
                        opened: false
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                },
                resolve: {
                }
            });
        };
        $scope.edit=function(action,size,id){
            $rootScope.action=action;
            $rootScope.edit_id=id;

            $uibModal.open({
                templateUrl: 'myModalContent.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope, $modalInstance, $log,categories_policy) {
                    $scope.msg1={};
                    if($rootScope.type =='sponsorships'){
                        $scope.choices=[];
                        angular.forEach($rootScope.sponsorCategories, function(v, k) {
                            if ($rootScope.lang == 'ar'){
                                $scope.choices.push({'id':v.id,'name':v.name});
                            }
                            else{
                                $scope.choices.push({'id':v.id,'name':v.en_name});
                            }
                        });

                    } else{
                        $scope.choices=[];
                        angular.forEach($rootScope.aidCategories, function(v, k) {
                            if ($rootScope.lang == 'ar'){
                                $scope.choices.push({'id':v.id,'name':v.name});
                            }
                            else{
                                $scope.choices.push({'id':v.id,'name':v.en_name});
                            }
                        });
                    }

                    $rootScope.progressbar_start();

                    var target= new categories_policy($rootScope.edit_id);
                    target.$get({id:$rootScope.edit_id,type:$rootScope.type})
                        .then(function (response) {
                            $rootScope.progressbar_complete();
                            $scope.policy=response;
                            $scope.policy.checkedAll=false;
                            $scope.policy.category_id=$scope.policy.category_id +"";
                            $scope.policy.level=$scope.policy.level +"";
                            $scope.policy.started_at    = new Date($scope.policy.started_at);
                            $scope.policy.ends_at    = new Date($scope.policy.ends_at);


                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                            $modalInstance.close();

                        });
                    $scope.reset = function(value){

                            if (!angular.isUndefined($scope.msg1.started_at)) {
                                $scope.msg1.started_at = [];
                            }
                             if (!angular.isUndefined($scope.msg1.ends_at)) {
                                $scope.msg1.ends_at = [];
                            }
                            $scope.policy.started_at = "";
                            $scope.policy.ends_at = "";


                    };
                    $scope.confirm = function (policy) {
                        $rootScope.progressbar_start();
                        policy.type=$rootScope.type;
                        var target = new categories_policy(policy);
                        target.$update()
                            .then(function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    $modalInstance.close();
                                    if(response.status=='success')
                                    {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        LoadPolicy({'type':$rootScope.type,'page':$scope.CurrentPage});
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }

                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();

                            });
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };


                    $scope.dateOptions = {
                        formatYear: 'yy',
                        startingDay: 0
                    };
                    $scope.formats = [ 'dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                    $scope.format = $scope.formats[0];
                    $scope.today = function() {
                        $scope.dt = new Date();
                    };
                    $scope.today();
                    $scope.clear = function() {
                        $scope.dt = null;
                    };
                    $scope.open4 = function($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.popup4.opened = true;
                    };
                    $scope.popup4 = {
                        opened: false
                    };
                    $scope.open3 = function($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.popup3.opened = true;
                    };
                    $scope.popup3 = {
                        opened: false
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                },
                resolve: {
                }
            });
        };
        $scope.delete=function(id){
            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                .then(function() {
                    $rootScope.progressbar_start();
                    categories_policy.delete({id:id,'type':$rootScope.type},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='failed') {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else{
                            if(response.status=='success') {
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                LoadPolicy({'type':$rootScope.type,'page':$scope.CurrentPage});
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                        }
                    });
                });
        };


        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 0
        };
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();
        $scope.clear = function() {
            $scope.dt = null;
        };


        $scope.open99 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup99.opened = true;
        };

        $scope.popup99 = {
            opened: false
        };
        $scope.open88 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup88.opened = true;
        };

        $scope.popup88 = {
            opened: false
        };

        $scope.open999 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup999.opened = true;};
        $scope.popup999 = {opened: false};
        $scope.open888 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup888.opened = true;};
        $scope.popup888 = {opened: false};
        $scope.open9999 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup9999.opened = true;};
        $scope.popup9999 = {opened: false};
        $scope.open8888 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup8888.opened = true;};
        $scope.popup8888 = {opened: false};

    });
