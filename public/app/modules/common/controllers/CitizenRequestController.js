
angular.module('CommonModule').
controller('CitizenRequestController', function (Relays, $rootScope, $http,$filter,org_sms_service,$scope,$state,$uibModal,Entity,$stateParams,CitizenRequests) {

    $state.current.data.pageTitle = $filter('translate')('CitizenRequest');
    $rootScope.clearToastr();
    $scope.CurrentPage=1;
    $rootScope.itemsCount=50;
    $rootScope.items=[];
    $rootScope.datafilter ={page:1,action:'paginate'};
    $scope.sortKeyArr=[];
    $scope.sortKeyArr_rev=[];
    $scope.sortKeyArr_un_rev=[];

    Entity.get({entity:'entities',c:'aidCountries'},function (response) {
        $scope.aidCountries = response.aidCountries;
    });
    
    var LoadCitizenRequest = function ($params) {
        if($params.action != 'xlsx') {
            $rootScope.settings.layout.pageSidebarClosed = true;
            $rootScope.items=[];
            $params.page=$scope.CurrentPage;
            $params.itemsCount=$rootScope.itemsCount;
        }

        $params.sortKeyArr_rev = $scope.sortKeyArr_rev;
        $params.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;
        $rootScope.progressbar_start();
        CitizenRequests.filter($params,function (response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if($params.action == 'xlsx'){
                if(response.download_token){
                    $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }

            }else{
                $rootScope.items = response.data;
                $scope.CurrentPage = response.current_page;
                $rootScope.TotalItems = response.total;
                $rootScope.ItemsPerPage = response.per_page;
            }

        });

        $scope.All =false;
        $scope.selectAll =false;
        angular.forEach($rootScope.items, function(val,key) {
            val.checked=$scope.selectAll;
        });

    };

    $scope.sort_ = function(keyname){
        $scope.sortKey_ = keyname;
        var reverse_ = false;

        if (
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
        ) {
            reverse_ = true;
        }

        var data = angular.copy($rootScope.datafilter);
        data.action ='paginate';

        if (reverse_) {
            if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_rev.push(keyname);
            }

            // angular.forEach($scope.sortKeyArr_un_rev, function(val,key) {
            //     if(val == keyname){
            //         $scope.sortKeyArr_un_rev.splice(key,1);
            //         return ;
            //     }
            // });

        }
        else {
            if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_un_rev.push(keyname);
            }
            // angular.forEach($scope.sortKeyArr_rev, function(val,key) {
            //     if(val == keyname){
            //         $scope.sortKeyArr_rev.splice(key,1);
            //         return ;
            //     }
            // });
        }

        data.sortKeyArr_rev = $scope.sortKeyArr_rev;
        data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;

        LoadCitizenRequest(data);

    };
    $scope.toggleSearchForm = function() {
        $scope.custom = $scope.custom == false ? true: false;
        $rootScope.datafilter ={page:1,category_type:$rootScope.categoryType};
    };
    $scope.pageChanged = function (CurrentPage) {
        $scope.CurrentPage = CurrentPage;
        $rootScope.datafilter.page = CurrentPage;
        $rootScope.datafilter.action = 'paginate';
        LoadCitizenRequest($rootScope.datafilter);
    };

    $scope.CitizenRequestSearch = function (selected , params,action) {
        $rootScope.clearToastr();
        var data = angular.copy(params);

        if( !angular.isUndefined(data.created_at_to)) {
            data.created_at_to=$filter('date')(data.created_at_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.created_at_from)) {
            data.created_at_from= $filter('date')(data.created_at_from, 'dd-MM-yyyy')
        }
        data.action=action;
        data.items=[];
        
       if(selected == true || selected == 'true' ){
            
            var items =[];
            angular.forEach($rootScope.items, function(v, k){
                 if(v.checked){
                      items.push(v.id);
                    }
            });
            
            if(items.length==0){
               $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
               return ;
             
            }
            data.items=items;
        }
            
        LoadCitizenRequest(data);

    };
    $scope.getLocation = function(entity,parent,value){
        if(!angular.isUndefined(value)) {
            if (value !== null && value !== "" && value !== " ") {
                Entity.listChildren({'entity': entity, 'id': value, 'parent': parent}, function (response) {

                    if (entity == 'sdistricts') {

                        $scope.governarate = response;
                        $scope.regions = [];
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];

                        $scope.datafilter.adsdistrict_id = "";
                        $scope.datafilter.adsregion_id = "";
                        $scope.datafilter.adsneighborhood_id = "";
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";

                    }
                    else if (entity == 'sregions') {

                        $scope.regions = response;
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];
                        $scope.datafilter.adsregion_id = "";
                        $scope.datafilter.adsneighborhood_id = "";
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";
                    }
                    else if (entity == 'sneighborhoods') {
                        $scope.nearlocation = response;
                        $scope.squares = [];
                        $scope.mosques = [];
                        $scope.datafilter.adsneighborhood_id = "";
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";

                    }
                    else if (entity == 'ssquares') {
                        $scope.squares = response;
                        $scope.mosques = [];
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";
                    }
                    else if (entity == 'smosques') {
                        $scope.mosques = response;
                        $scope.datafilter.adsmosques_id = "";
                    }
                    else if (entity == 'districts') {

                        $scope.governarate = response;
                        $scope.city = [];
                        $scope.nearlocation = [];
                        $scope.mosques = [];
                        $scope.datafilter.governarate = "";
                        $scope.datafilter.city = "";
                        $scope.datafilter.location_id = "";

                    }
                    else if (entity == 'cities') {

                        $scope.city = response;
                        $scope.nearlocation = [];
                        $scope.mosques = [];
                        $scope.datafilter.city = "";
                        $scope.datafilter.location_id = "";

                    }
                    else if (entity == 'neighborhoods') {
                        $scope.nearlocation = response;
                        $scope.mosques = [];
                        $scope.datafilter.location_id = "";
                        $scope.datafilter.mosques_id = "";
                    }
                    else if (entity == 'mosques') {

                        $scope.mosques = response;
                        $scope.datafilter.mosques_id = "";
                    }

                });
            }
        }
    };

    $scope.selectAll =false;
    $scope.selectedAll = function(value){
        if(value){
            $scope.selectAll = true;
        }
        else{
            $scope.selectAll =false;

        }
        angular.forEach($rootScope.items, function(val,key)
        {
            val.checked=$scope.selectAll;
        });
    };

    $scope.sendSms=function(type,citizen_id) {
        $rootScope.clearToastr();
        if(type == 'manual'){
            var receipt = [];
            var pass = true;

            if(citizen_id ==0){
                angular.forEach($rootScope.items, function (v, k) {
                    if (v.checked) {
                        receipt.push(v.citizen_id);
                    }
                });
                if(receipt.length==0){
                    pass = false;
                    $rootScope.toastrMessages('error',$filter('translate')("you did not select anyone to send them sms message"));
                    return;
                }
            }else{
                receipt.push(citizen_id);
            }
            if(pass){
                org_sms_service.getList(function (response) {
                    $rootScope.Providers = response.Service;
                });
                $uibModal.open({
                    templateUrl: '/app/modules/sms/views/modal/send_sms.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance) {

                        $scope.confirm = function (data) {
                            $rootScope.clearToastr();
                            var params = angular.copy(data);
                            params.target='citizens';
                            params.receipt=receipt;

                            $rootScope.progressbar_start();
                            org_sms_service.sendSmsToTarget(params,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed') {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status== 'success'){
                                    if(response.download_token){
                                        var file_type='xlsx';
                                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                    }

                                    $modalInstance.close();
                                    $rootScope.toastrMessages('success',response.msg);
                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                            });
                        };


                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    }
                });

            }
        }
        else{
            org_sms_service.getList(function (response) {
                $rootScope.Providers = response.Service;
            });
            $uibModal.open({
                templateUrl: '/app/modules/sms/views/modal/sent_sms_from_xlx.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;
                    $scope.sms={source: 'file' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:""};

                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };

                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };
                    $scope.download=function(){
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-to-send-sms-template&template=true';
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: 'api/v1.0/sms/sent_sms/excelWithMobiles',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });


                    $scope.confirm = function (item) {
                        $scope.spinner=false;
                        $scope.import=false;
                        $rootScope.progressbar_start();
                        Uploader.onBeforeUploadItem = function(item) {

                            if($scope.sms.same_msg == 0){
                                $scope.sms.sms_messege = '';
                            }
                            item.formData = [$scope.sms];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();

                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            else if(response.status=='success')
                            {
                                if(response.download_token){
                                    var file_type='xlsx';
                                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                }
                                $scope.spinner=false;
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $scope.import=false;
                                $rootScope.toastrMessages('success',response.msg);
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                //$modalInstance.close();
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});

        }
    };

    $scope.notes=function(row) {
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: '/app/modules/common/views/CitizenRequest/notes.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance) {

                $scope.request=row;
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };


            }});
    };

    $scope.cases=function(card) {
        $rootScope.clearToastr();
        $rootScope.cases_list=[];

        $rootScope.progressbar_start();
        CitizenRequests.cases({card:card,action:'list'},function (response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if(!angular.isUndefined(response.cases)) {
                $rootScope.cases_list=response.cases;
            }else{
                $rootScope.toastrMessages('error',$filter('translate')('The citizen is not registered as a case at any organization'));
            }

        });

        $uibModal.open({
            templateUrl: '/app/modules/common/views/CitizenRequest/cases.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance) {

              $scope.export=function(card) {
                  $rootScope.progressbar_start();
                  $rootScope.clearToastr();
                    CitizenRequests.cases({card:card,action:'xlsx'},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.download_token){
                            $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                            var file_type='xlsx';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }else{
                            $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                        }
                    });
              };

              $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };


            }});
    };

    $scope.download=function(){
        window.location = '/api/v1.0/common/files/xlsx/export?download_token=request-update-template&template=true';
    };

    $scope.status=function(id,status,index) {
        $rootScope.clearToastr();
        $rootScope.progressbar_start();
        CitizenRequests.status({'action':'single',target:id,status:status },function (response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if(response.status == 'success') {
                $rootScope.items[index].status = status;
                $rootScope.toastrMessages('success',$filter('translate')('action success'));
            }else{
                $rootScope.toastrMessages('error',$filter('translate')('action failed'));
            }

        });
    };

    $scope.updateStatusToAll=function(status){

        $rootScope.clearToastr();
        if(status == -1){
            $uibModal.open({
                templateUrl: 'myModalContent2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'md',
                controller: function ($rootScope,$scope, $modalInstance, $log, FileUploader, OAuthToken) {

                    $scope.upload=false;
                    $scope.FormData={};

                    $scope.download=function(){
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token=request-update-template&template=true';
                    };
                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };

                    $scope.fileUpload=function(){
                        if($scope.FormData.status != ""){
                            $scope.upload=true;
                        }
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/citizens/request/UpdateFromFile',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    $scope.confirm = function (item) {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();

                        Uploader.onBeforeUploadItem = function(i) {
                            $rootScope.clearToastr();
                            i.formData = [{status: item.status}];
                        };

                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);

                            if(response.download_token){
                                var file_type='xlsx';
                                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                            }

                            if(response.status=='failed_valid') {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $rootScope.status1 =response.status;
                                $rootScope.msg1 =response.msg;
                            }
                            else{
                                if(response.status=='success') {
                                    $rootScope.toastrMessages('success',response.msg);
                                    LoadCitizenRequest($rootScope.datafilter);
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                $modalInstance.close();
                            }
                        };
                        Uploader.uploadAll();
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                }
            });
        }
        else{
            var requests = [];

            angular.forEach($scope.items, function(v,key){
                if (v.checked && (v.status != status)) {
                    requests.push( v.id)
                }
            });

            if(requests.length==0){
                $rootScope.toastrMessages('error',$filter('translate')('no requests selected to update status'));
            }else {

                $rootScope.clearToastr();
                $rootScope.progressbar_start();
                CitizenRequests.status({'action':'group',requests:requests,status:status },function (response) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                    if(response.status == 'success') {
                        LoadCitizenRequest($rootScope.datafilter);
                        $rootScope.toastrMessages('success',$filter('translate')('action success') + $filter('translate')('updated') +' ( '+ requests.length  +'  )');
                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                    }

                });
            }

        }

    };

    $scope.itemsPerPage__ = function (itemsCount) {
        $scope.CurrentPage = 1;
        $rootScope.datafilter.page = 1;
        $rootScope.itemsCount = itemsCount;
        $rootScope.datafilter.itemsCount = itemsCount;
        LoadCitizenRequest($rootScope.datafilter);
    };

    // LoadCitizenRequest($rootScope.datafilter);

    $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
    $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.today = function() { $scope.dt = new Date();};
    $scope.today();
    $scope.clear = function() {$scope.dt = null;};
    $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
    $scope.popup99 = {opened: false};
    $scope.open88 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup88.opened = true;};
    $scope.popup88 = {opened: false};
});
