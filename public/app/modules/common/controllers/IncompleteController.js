
angular.module('CommonModule')
    .controller('IncompleteController', function(cases,$filter,$stateParams,$rootScope, $scope,$uibModal,category,$http, $timeout,$state,Org) {

    $rootScope.settings.layout.pageSidebarClosed = true;
    $rootScope.type =$scope.type = $stateParams.type == 'aids'? 'aids' : 'sponsorships';
    $rootScope.category_type=$scope.category_type=$stateParams.type == 'aids'? 2 : 1;
    $state.current.data.pageTitle = $filter('translate')('case-incomplete-data');
    $scope.itemsCount='50';


    $rootScope.Categories=[];
    category.getSectionForEachCategory({'type':$rootScope.type},function (response) {
        if(!angular.isUndefined(response)){
            $rootScope.Categories = response.Categories;
            if(response.Categories.length > 0){
                var Categories=[];
                angular.forEach(response.Categories, function(v, k) {
                    if ($rootScope.lang == 'ar'){
                        Categories.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
                    }
                    else{
                        Categories.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
                    }
                });

                $rootScope.Categories = Categories ;
            }

        }
    });
    

    $rootScope.failed =false;
    $rootScope.model_error_status =false;
    $scope.master=false;
    $rootScope.status ='';
    $rootScope.msg ='';
    $scope.items=[];
    $scope.filter={};
    $scope.CurrentPage = 1 ;

   Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

    var  resetFilter =function(){
            $scope.filter={
                "all_organization":false,
                "first_name":"",
                "second_name":"",
                "third_name":"",
                "last_name":"",
                "id_card_number":"",
                "category_id":"",
                "organization_id":""
            };

        };

    $scope.close=function(){
        $scope.success =false;
        $scope.failed =false;
        $scope.error =false;
        $scope.model_error_status =false;
    };

    var LoadInCompleteCases =function(params){
        params.type=$scope.type;
        params.itemsCount=$scope.itemsCount;
       params.category_type=$scope.category_type;

        
        $rootScope.progressbar_start();
        cases.getInCompleteCases(params,function (response) {
            $rootScope.progressbar_complete();
            if(response.Cases.data.length != 0){
                $scope.organization_id=response.organization_id;
                //$scope.master=response.master;
                $scope.items= response.Cases.data;
                $scope.CurrentPage = response.Cases.current_page;
                $scope.TotalItems = response.Cases.total;
                $scope.ItemsPerPage = response.Cases.per_page;
            }else{
                $scope.items=[];
            }
        });
    };
    // LoadInCompleteCases({status:'filter',page:1,'all_organization':false});

    $scope.pageChanged = function (currentPage) {
        $scope.filter.page=currentPage;
        $scope.filter.status='filter';
        LoadInCompleteCases($scope.filter);
    };

    $scope.itemsPerPage_ = function (itemsCount) {
        $scope.filter.itemsCount=itemsCount;
        $scope.itemsCount = itemsCount;
        $scope.filter.page=1;
        $scope.filter.status='filter';
        LoadInCompleteCases($scope.filter);
    };

    $scope.goTo=function(action,person,case_id,category_id){
            angular.forEach($rootScope.aidCategories, function(v, k) {
                if(v.id == category_id){
                    $state.go('caseform.'+v.FirstStep,{"mode":action,"persons_id":person ,"case_id" :case_id,"category_id" :category_id});
                }
            });
    };

    $scope.Search=function(data,action) {

        if( !angular.isUndefined(data.all_organization)) {
            if(data.all_organization == true ||data.all_organization == 'true'){
                var orgs=[];
                angular.forEach(data.organization_id, function(v, k) {
                    orgs.push(v.id);
                });

                data.organization_ids=orgs;

            }else if(data.all_organization == false ||data.all_organization == 'false') {
                data.organization_ids=[];
                $scope.master=false
            }
        }
           data.status=action;

        if(action == 'ExportToExcel'){
        }else{
            data.page=$scope.CurrentPage;
            LoadInCompleteCases(data);
        }
    };

});
