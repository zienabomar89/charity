
angular.module('CommonModule')
    .controller('AttachmentsController', function (category,$scope,$rootScope,$uibModal,Org,$http,$state,$stateParams,Entity, FileUploader, OAuthToken,cases,$filter) {

        $rootScope.labels = {
            "itemsSelected": $filter('translate')('itemsSelected'),
            "selectAll": $filter('translate')('selectAll'), "unselectAll": $filter('translate')('unselectAll'),
            "search": $filter('translate')('search'), "select": $filter('translate')('select')
        }

        $rootScope.settings.layout.pageSidebarClosed = true;
        $rootScope.type=$scope.type=$stateParams.type == 'aids'? 'aids' : 'sponsorships';
        $rootScope.category_type=$scope.category_type=$stateParams.type == 'aids'? 2 : 1;
        $state.current.data.pageTitle = $filter('translate')('cases attachments');
        $scope.master =false;
        
        $rootScope.failed =false;
        $rootScope.model_error_status =false;
        $rootScope.model_error_status2 =false;
        $rootScope.status ='';
        $rootScope.msg ='';
        $rootScope.status ='';
        $rootScope.msg ='';
        $scope.custom = true;
        $scope.items=[];
        $scope.map=[];
        $scope.itemsCount = 50;
        $scope.CurrentPage = 1 ;


        $http({
            url: "api/v1.0/common/category/map",
            method: "POST",
            data: {}
        }).then(function (response) {
            $scope.map = response.data.map;
        }, function(error) {
            $rootScope.toastrMessages('error',error.data.msg);
        });
        
        $rootScope.Categories=[];
        category.getSectionForEachCategory({'type':$stateParams.type},function (response) {
            if(!angular.isUndefined(response)){
                $rootScope.Categories = response.Categories;
                if(response.Categories.length > 0){
                    var Categories=[];
                    angular.forEach(response.Categories, function(v, k) {
                        if ($rootScope.lang == 'ar'){
                            Categories.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
                        }
                        else{
                            Categories.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
                        }
                    });

                    $rootScope.Categories = Categories ;
                }

            }
        });
        var resetFilter =function(){
            $rootScope.datafilter={
                "all_organization":false,
                "document":true,
                "date_from":"",
                "date_to":"",
                "first_name":"",
                "second_name":"",
                "third_name":"",
                "last_name":"",
                "category_id":"",
                "id_card_number":"",
                'sponsor':"",
                'guaranteed_status':"",
                'organization_id':"",
                'document_id':""
            };
        };

        Entity.get({entity:'entities',c:'documentTypes,organizationsCat'},function (response) {
            $rootScope.organizationsCat = response.organizationsCat;
            $scope.document = response.documentTypes;
            $rootScope.datafilter.document_id = $scope.document ;
        });

        var  LoadAttachment=function(params){
            $rootScope.clearToastr();
            params.itemsCount = $scope.itemsCount;
            params.type = $scope.type;
            params.category_type = $scope.category_type;

            $rootScope.progressbar_start();
            cases.getCaseAttachments(params,function (response) {
                $rootScope.progressbar_complete();
                
                if(params.status == 'exportToExcel' || params.status == 'ExportToWord' ||  params.status == 'export') {
                   if(response.status == false){
                        $rootScope.toastrMessages('error',$filter('translate')('no attachment to export'));
                    }else{
                        var file_type='xlsx';
                        if(params.status == 'ExportToWord' || params.status == 'export') {
                            file_type='zip';
                        }
                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                    }
                }
                else
                {
                    var temp = response.data;
                    angular.forEach(temp, function(val,key)
                    {
                        val.guardian_dcoument_arr = [];
                        val.mother_dcoument_arr = [];
                        val.person_dcoument_arr = [];
                        val.father_dcoument_arr = [];

                        var docs=[];
                        if($scope.category_type == 2){
                            val.father_dcoument=null;
                            val.guardian_dcoument=null;
                            val.mother_dcoument=null;
                        }

                        if(val.person_dcoument == null && val.father_dcoument == null && val.mother_dcoument == null&& val.guardian_dcoument == null){
                            temp[key].doc=null
                        }
                        else{

                            if(val.person_dcoument !== null){
                                str=val.person_dcoument;
                                splitted = str.split(",");
                                val.person_dcoument_arr = splitted;
                                for(i = 0; i < splitted.length; i++){
                                    if(docs.indexOf(splitted[i]) == -1){
                                        docs.push(parseInt(splitted[i]));
                                    }
                                }
                            }
                            if(val.father_dcoument !== null){
                                str=val.father_dcoument;
                                splitted = str.split(",");
                                val.father_dcoument_arr = splitted;
                                for(i = 0; i < splitted.length; i++){
                                    if(docs.indexOf(splitted[i]) == -1){
                                        docs.push(parseInt(splitted[i]));
                                    }
                                }
                            }
                            if(val.mother_dcoument !== null){
                                str=val.mother_dcoument;
                                splitted = str.split(",");
                                val.mother_dcoument_arr = splitted;
                                for(i = 0; i < splitted.length; i++){
                                    if(docs.indexOf(splitted[i]) == -1){
                                        docs.push(parseInt(splitted[i]));
                                    }
                                }
                            }
                            if(val.guardian_dcoument !== null){
                                str=val.guardian_dcoument;
                                splitted = str.split(",");
                                val.guardian_dcoument_arr = splitted;
                                for(i = 0; i < splitted.length; i++){
                                    if(docs.indexOf(splitted[i]) == -1){
                                        docs.push(parseInt(splitted[i]));
                                    }
                                }
                            }
                            temp[key].doc=docs;
                        }

                    });

                    $scope.items=temp;
                    $scope.CurrentPage = response.current_page;
                    $rootScope.TotalItems = response.total;
                    $rootScope.ItemsPerPage = response.per_page;                            
                }

            });
        };

       // LoadAttachment({'page':1,'status':'filter','all_organization':false ,'document':true ,'document_id' :$scope.document});
        resetFilter();

        Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

        if($scope.type =='sponsorships'){
            Org.list({type:'sponsors'},function (response) { $scope.Sponsors = response; });
        }

        $scope.close=function(){
            
            $rootScope.failed =false;
            $rootScope.model_error_status =false;
            $rootScope.model_error_status2 =false;
        };
        $scope.download = function (id) {

            $rootScope.progressbar_start();
            $http({
                url: "/api/v1.0/common/"+$scope.type+"/cases/exportCaseDocument/"+id,
                method: "GET"
            }).then(function (response) {
                $rootScope.progressbar_complete();
                window.location = '/api/v1.0/common/files/zip/export?download_token='+response.data.download_token;
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };
        $scope.pageChanged = function (CurrentPage) {
            $rootScope.datafilter.page=CurrentPage;
            $rootScope.datafilter.status='filter';
            LoadAttachment($rootScope.datafilter);
        };

        $rootScope.itemsPerPage_ = function (itemsCount) {
            $rootScope.datafilter.status='filter';
            $rootScope.datafilter.page=1;
            $rootScope.datafilter.itemsCount = itemsCount ;
            LoadAttachment($rootScope.datafilter);
        };

       //-----------------------------------------------------------------------------------------------------------
        $scope.disblay_orgs =false;
        $scope.resetOrg=function(all_organization,organization_category){
            $rootScope.clearToastr();
            $scope.disblay_orgs =false;
            let ids = [];
            if( !angular.isUndefined(all_organization)) {
                if(all_organization == true ||all_organization == 'true'){
                    angular.forEach(organization_category, function(v, k) {
                        ids.push(v.id);
                    });

                    Org.ListOfCategory({ids:ids},function (response) {
                        if (response.list.length > 0){
                            $scope.Org_ = response.list;
                            $scope.disblay_orgs =true;
                        }else{
                            $scope.Org_ = [];
                            $scope.disblay_orgs =false;
                            $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');
                        }
                    });
                }else{
                    $scope.Org_ = [];
                    $scope.disblay_orgs =false;
                    $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');
                }
            }else{
                $scope.Org_ = [];
                $scope.disblay_orgs =false;
                $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');

            }


        };
        //-----------------------------------------------------------------------------------------------------------
        $scope.ChooseAttachment=function(item,doc_type,action){

            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'myModalContent2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,cases,OAuthToken, FileUploader) {

                    $scope.fileupload = function () {
                        if($scope.ImageUploader.queue.length){
                            var image = $scope.ImageUploader.queue.slice(1);
                            $scope.ImageUploader.queue = image;
                        }
                    };
                    var ImageUploader = $scope.ImageUploader = new FileUploader({
                        url: '/api/v1.0/doc/files',
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });
                    ImageUploader.onBeforeUploadItem = function(item) {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                        item.formData = [{action:"check"}];
                    };
                    ImageUploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();

                        $scope.ImageUploader.destroy();
                        $scope.ImageUploader.queue=[];
                        $scope.ImageUploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(response.status=='error')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else{

                            fileItem.id = response.id;

                            if($rootScope.type == 'aids'){

                                cases.setAttachment({id:item.person_id,type:$rootScope.type,action:action,document_type_id:doc_type,document_id:fileItem.id},function (response2) {
                                    if(response2.status=='error' || response2.status=='failed')
                                    {
                                        $rootScope.toastrMessages('error',response2.msg);
                                    }
                                    else if(response2.status== 'success'){

                                        $modalInstance.close();
                                        $rootScope.toastrMessages('success',response2.msg);
                                        $rootScope.searchCases($rootScope.datafilter,'filter');
                                    }

                                }, function(error) {
                                    $rootScope.toastrMessages('error',error.data.msg);
                                });
                            }else{

                                cases.setCaseAttachment({id:item.person_id,father_id:item.father_id,mother_id:item.mother_id,guardian_id:item.guardian_id,category_id:item.category_id,type:$rootScope.type,action:action,document_type_id:doc_type,document_id:fileItem.id},function (response2) {
                                    if(response2.status=='error' || response2.status=='failed')
                                    {
                                        $rootScope.toastrMessages('error',response2.msg);
                                    }
                                    else if(response2.status== 'success'){
                                        $rootScope.searchCases($rootScope.datafilter,'filter');
                                        $modalInstance.close();
                                        $rootScope.toastrMessages('success',response2.msg);
                                    }

                                }, function(error) {
                                    $rootScope.toastrMessages('error',error.data.msg);
                                });
                            }
                        }

                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

        $scope.toggleCustom = function() {
            $scope.custom = $scope.custom == false ? true: false;
            resetFilter();
        };

        $scope.selectedAll = false;
        $scope.selectAll=function(value){
                if (value) {
                    $scope.selectedAll = true;
                } else {
                    $scope.selectedAll = false;
                }

                angular.forEach($scope.items, function(v, k) {
                    v.check=$scope.selectedAll;
                });
        };
        $rootScope.searchCases = function (selected , params,action) {

            var filterdata = angular.copy(params);
            filterdata.status=action;
            filterdata.items=[];

            if(selected == true || selected == 'true' ){
            
                var items =[];
                 angular.forEach($scope.items, function(v, k){

                       if(v.check){
                             items.push(v.id);
                         }
                 });

                 if(items.length==0){
                     $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                     return ;
                 }
                 filterdata.items=items;
            }
            
            if( !angular.isUndefined(filterdata.date_from)) {
                filterdata.date_from=$filter('date')(filterdata.date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(filterdata.date_to)) {
                filterdata.date_to=$filter('date')(filterdata.date_to, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(filterdata.all_organization)) {
                if(filterdata.all_organization == true ||filterdata.all_organization == 'true'){

                    filterdata.organization_category=[];
                    angular.forEach(filterdata.organization_category_id, function(v, k) {
                        filterdata.organization_category.push(v.id);
                    });

                    var orgs=[];
                    angular.forEach(filterdata.organization_id, function(v, k) {
                        orgs.push(v.id);
                    });

                    filterdata.organization_ids=orgs;

                }else if(filterdata.all_organization == false ||filterdata.all_organization == 'false') {
                    filterdata.organization_ids=[];
                    $scope.master=false
                }
            }

            if( !angular.isUndefined(filterdata.document)) {
                if(filterdata.document == true ||filterdata.document == 'true'){
                    var documents=[];
                    angular.forEach(filterdata.document_id, function(v, k) {
                        documents.push(v.id);
                    });
                    

                    filterdata.document_ids=documents;
                    $scope.document_ids = documents ;

                }else if(filterdata.document == false ||filterdata.document == 'false') {
                    filterdata.document_ids=[];
                    $scope.master=false
                }
            }

            filterdata.status=action;
            filterdata.type=$scope.type;
            filterdata.category_type=$scope.category_type;
            
            filterdata.page=$scope.CurrentPage;
            LoadAttachment(filterdata);
        };
        // Bulk Export and Import
        $scope.import = {};

        var Uploader = $scope.uploader = new FileUploader({
            url: '/api/v1.0/common/cases-documents/import',
            removeAfterUpload: true,
            queueLimit: 1,
            formData: [
                {category_type: $scope.category_type},
                {organization_id: $rootScope.datafilter.organization_id},
                {category_id: $rootScope.datafilter.category_id},
                {first_name: $rootScope.datafilter.first_name},
                {second_name: $rootScope.datafilter.second_name},
                {third_name: $rootScope.datafilter.third_name},
                {last_name: $rootScope.datafilter.last_name},
                {id_card_number: $rootScope.datafilter.id_card_number},
                {all_organization: $rootScope.datafilter.all_organization},
                {document: $rootScope.datafilter.document},
                {document_id: $rootScope.datafilter.document_id},
            ],
            headers: {
                Authorization: OAuthToken.getAuthorizationHeader()
            }
        });

        Uploader.filters.push({
            name: 'zipFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|zip|x-zip-compressed|'.indexOf(type) !== -1;
            }
        });

        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
            $rootScope.progressbar_complete();
            $scope.uploader.destroy();
            $scope.uploader.queue=[];
            $scope.uploader.clearQueue();
            angular.element("input[type='file']").val(null);
            $rootScope.datafilter.page=$scope.CurrentPage;
            $rootScope.datafilter.status='filter';
            LoadAttachment($rootScope.datafilter);
            $rootScope.toastrMessages('success',$filter('translate')('Imported')+response.total+$filter('translate')('file')+response.insert+' ملف جديد، '+response.update+' ملف تم تحديثه.');
        };
        
        $rootScope.prepare = function (selected , params) {

            var dt = angular.copy(params);
                dt.items=[];

            if(selected == true || selected == 'true' ){
            
                var items =[];
                 angular.forEach($scope.items, function(v, k){

                       if(v.check){
                             items.push(v.id);
                         }
                 });

                 if(items.length==0){
                     $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                     return ;
                 }
                 dt.items=items;
            }
            
            if( !angular.isUndefined(dt.date_from)) {
                dt.date_from=$filter('date')(dt.date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(dt.date_to)) {
                dt.date_to=$filter('date')(dt.date_to, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(dt.all_organization)) {
                if(dt.all_organization == true ||dt.all_organization == 'true'){

                    dt.organization_category=[];
                    angular.forEach(dt.organization_category_id, function(v, k) {
                        dt.organization_category.push(v.id);
                    });

                    var orgs=[];
                    angular.forEach(dt.organization_id, function(v, k) {
                        orgs.push(v.id);
                    });

                    dt.organization_ids=orgs;

                }else if(dt.all_organization == false ||dt.all_organization == 'false') {
                    dt.organization_ids=[];
                }
            }
            dt.category_type=$scope.category_type;
            
            $rootScope.progressbar_start();
            $http({
                url: "/api/v1.0/common/cases-documents/prepare",
                method: "POST",
                data: dt
            }).then(function (response) {
                $rootScope.progressbar_complete();
                if(response.data.status == false){
                    $rootScope.toastrMessages('error',$filter('translate')('There are no cases to export file'));
                }else{
                    window.location = '/api/v1.0/common/cases-documents/export?download_token='+response.data.download_token;
                }
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };

        $scope.attachmentInstruction=function(){
            var url='/api/v1.0/common/attachmentInstruction';
            var type='pdf';

            $rootScope.progressbar_start();
            $http({
                url:url,
                method: "GET"
            }).then(function (response) {
                $rootScope.progressbar_complete();
                window.location = '/api/v1.0/common/files/'+type+'/export?download_token='+response.data.download_token+'&template=true'+window.location.title;
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });

        };

        $scope.open99 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup99.opened = true;
        };
        $scope.popup99 = {
            opened: false
        };
        $scope.open88 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup88.opened = true;
        };
        $scope.popup88 = {
            opened: false
        };

        $scope.open_image=function (type,personal,item) {

            $rootScope.doc_type = type ;
            if($scope.type =='aids'){
                $rootScope.target = 'case' ;
                $rootScope.person_id = personal ;
            }else{
                if(item.guardian_dcoument_arr.indexOf(type) != -1){
                    $rootScope.person_id = item.guardian_id ;
                    $rootScope.target = 'guardian' ;
                }else if(item.mother_dcoument_arr.indexOf(type) != -1){
                    $rootScope.person_id = item.mother_id ;
                    $rootScope.target = 'mother' ;
                }else if(item.father_dcoument_arr.indexOf(type) != -1){
                    $rootScope.target = 'father' ;
                    $rootScope.person_id = item.father_id ;
                }else{
                    $rootScope.target = 'case' ;
                    $rootScope.person_id = personal ;

                }
            }

            $rootScope.clearToastr();
            $uibModal.open({

                templateUrl: 'app/modules/common/views/open_image.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                animation: true,
                controller: function ($rootScope,$scope, $modalInstance, $log) {
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                }});
        };
    });
