
angular.module('CommonModule').controller('SearchController', function($stateParams,$rootScope,$scope,$timeout,$state,persons,$filter,Log) {

    angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
    $rootScope.type=$scope.type=$stateParams.type == 'guardians'? 'guardians' : 'persons';
    $state.current.data.pageTitle = $stateParams.type == 'guardians'? $filter('translate')('Search Of Guardians') : $filter('translate')('General Search');

    $scope.result =false;
    $scope.filterd=false;
    $scope.msg ='';
    $scope.CurrentPage=1;
    $scope.itemsCount='50';
    $rootScope.items=[];
    $scope.checkCards = false ;

    $scope.filter ={id_card_number:"",first_name:"",second_name:"",third_name:"",last_name:"",prev_family_name:""};

    $scope.getCard = function(){
        $scope.checkCards = !$scope.checkCards ;
        Log.getCardsByUser({},function (response) {
            $scope.cards= response;
        });
    };

    $scope.selected = function(id_card_number){
        $scope.checkCards = !$scope.checkCards ;
        $scope.filter.id_card_number = id_card_number ;
        
    };

    var Filters= function () {
        $rootScope.clearToastr();
        $scope.msg=" ";
        $scope.filter.source=$scope.type;
        $scope.filter.action='filter';
        if(angular.isUndefined($scope.CurrentPage)) {
            $scope.CurrentPage=1;
        }

        $scope.filter.page=$scope.CurrentPage;
        $scope.filter.itemsCount=$scope.itemsCount;
        angular.element('.btn').addClass("disabled");

        $rootScope.progressbar_start();
        persons.getPersons($scope.filter,function (response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            $rootScope.items= response.data;
            $rootScope.filterd=true;
            $rootScope.CurrentPage = response.current_page;
            $rootScope.TotalItems = response.total;
            $rootScope.ItemsPerPage = response.per_page;

            if($rootScope.items.length == 0){
                $rootScope.toastrMessages('error','لايوجد نتائج للبحث');
           }
      });

    };

    $scope.Search = function(){
        Filters();
    };
    $scope.pageChanged = function (currentPage) {
        $scope.CurrentPage=currentPage;
        Filters();
    };
    
    $scope.itemsPerPage_ = function (itemsCount) {
        $scope.itemsCount=itemsCount;
        $scope.CurrentPage=1;
        Filters();
    };
});
