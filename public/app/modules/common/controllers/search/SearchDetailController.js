
angular.module('CommonModule')
    .controller('SearchDetailController', function( $filter,$stateParams,$rootScope, $scope,$uibModal, $http, $timeout,$state,persons,Org,cs_persons_vouchers,vouchers_categories,Entity) {

        $rootScope.id=$scope.id=$stateParams.id;
        $rootScope.type=$scope.type=$stateParams.type == 'guardians'? 'guardians' : 'persons';

        $scope.case_payments_filters = $scope.sponsorships_filters  = $scope.guardians_filters=$scope.individuals_filters=$scope.voucher_filters={};
        $scope.cases_frm = $scope.case_payments = $scope.case_sponsorships= $scope.case_guardians= $scope.case_vouchers= $scope.case_individuals= true;

        $scope.casePaymentsList= [];
        $scope.CasesList= [];
        $scope.caseVouchersList= [];
        $scope.sponsorshipList= [];
        $scope.guardiansList= [];
        $scope.individualsList= [];

        $scope.close=function(){
            $scope.success =false;
            $scope.failed =false;
            $scope.error =false;
            $scope.model_error_status =false;
            $scope.model_error_status2 =false;
        };


        Entity.get({entity:'entities',c:'currencies,transferCompany'},function (response) {
            $rootScope.currency = response.currencies;
            $rootScope.transferCompany = response.transferCompany;
        });


        $rootScope.aidCategories_=[];
        angular.forEach($rootScope.aidCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });


        if($stateParams.id == null || $stateParams.id =="" ){
            $rootScope.toastrMessages('error',$filter('translate')('Your request for the link has been error'));
            $timeout(function() {
                $state.go('search');
            }, 3000);
        }
        else{

            Org.list({type:'sponsors'},function (response) { $scope.Sponsor = response; });
            Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

            cs_persons_vouchers.list({id:-1},function (response) {
                $scope.Choices = response.Vouchers;
            });

            vouchers_categories.List(function (response) {
                $scope.Categories = response;
            });

            $scope.toggleSearch = function(target) {

                if(target =='payments') {
                    $scope.case_payments = $scope.case_payments === false ? true : false;
                }else if(target =='sponsorships'){
                    $scope.case_sponsorships = $scope.case_sponsorships === false ? true: false;
                }else if(target =='guardians'){
                    $scope.case_guardians = $scope.case_guardians === false ? true: false;
                }else if(target =='case_individuals'){
                    $scope.case_individuals = $scope.case_individuals === false ? true: false;
                }else if(target =='vouchers'){
                    $scope.case_vouchers = $scope.case_vouchers === false ? true: false;
                }else if(target =='cases'){
                    $scope.cases_frm = $scope.cases_frm === false ? true: false;
                }
                resetSearchFilters(target);
            };

            var resetSearchFilters =function(target){

                if(target  =='payments'){
                    $scope.case_payments_filters={
                        'id_card_number':"",
                        'first_name':"",
                        'second_name':"",
                        'third_name':"",
                        'last_name':"",
                        'max_amount':"",
                        'min_amount':"",
                        'begin_date_from':"",
                        'end_date_from':"",
                        'begin_date_to':"",
                        'end_date_to':"",
                        'start_payment_date':"",
                        'end_payment_date':""
                    };

                }
                else if(target =='sponsorships'){

                    $scope.sponsorships_filters={
                        'sponsor_id':"",
                        'category_id':"",
                        'to_guaranteed_date':"",
                        'from_guaranteed_date':""
                    };
                }
                else if(target =='guardians'){

                    $scope.guardians_filters={
                        'first_name':"",
                        'second_name':"",
                        'third_name':"",
                        'last_name':"",
                        'id_card_number':"",
                        'to_birthday':"",
                        'from_birthday':"" ,
                        'to_guardian_date':"",
                        'from_guardian_date':""
                    };
                }
                else if(target =='vouchers'){
                    $scope.voucher_filters={
                        "organization_id":"",
                        "category_id":"",
                        "voucher_type":"",
                        "title"  :"",
                        "voucher_date_from":"",
                        "voucher_date_to":"",
                        "content":"",
                        "notes":"",
                        "voucher_source":"",
                        "receipt_date_from":"",
                        "receipt_date_to":"",
                        "receipt_location":""
                    };
                }
                else if(target =='cases'){
                    $scope.cases_filters={
                        "organization_id":"",
                        "category_id":"",
                        "visited_at_to":"",
                        "visited_at_from":""
                    };
                }

            };
            var Content =function(params){

                if(params.target =='guardians'){
                    params.individual_id=$scope.id;
                }
                else if(params.target =='individuals'){
                    params.guardian_id=$scope.id;
                }
                else if(params.target=='payments'){

                    params.sub_target=$scope.type;

                    if($scope.type =='guardians'){
                        params.guardian_id=$scope.id;
                    }else{
                        params.person_id=$scope.id;
                    }
                }
                 else{
                    params.person_id=$scope.id;
                }

                // params.person_id=$stateParams.id;
                params.action='filters';
                params.mode='show';

                if( $scope.case_payments === true  || $scope.case_sponsorships === true  ||$scope.case_guardians === true||
                    $scope.case_individuals === true || $scope.cases_frm === true || $scope.case_vouchers === true ){
                    if(params.target =='payments'){
                        if( !angular.isUndefined(params.begin_date_from)) {
                            params.begin_date_from=$filter('date')(params.begin_date_from, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(params.end_date_from)) {
                            params.end_date_from=$filter('date')(params.end_date_from, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.begin_date_to)) {
                            params.begin_date_to=$filter('date')(params.begin_date_to, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(params.end_date_to)) {
                            params.end_date_to=$filter('date')(params.end_date_to, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.start_payment_date)) {
                            params.start_payment_date=$filter('date')(params.start_payment_date, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(params.end_payment_date)) {
                            params.end_payment_date= $filter('date')(params.end_payment_date, 'dd-MM-yyyy')
                        }
                    }
                    else if(params.target =='sponsorships'){
                        if( !angular.isUndefined(params.to_guaranteed_date)) {
                            params.to_guaranteed_date=$filter('date')(params.to_guaranteed_date, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(params.from_guaranteed_date)) {
                            params.from_guaranteed_date=$filter('date')(params.from_guaranteed_date, 'dd-MM-yyyy')
                        }

                    }
                    else if(params.target =='guardians' || params.target =='individuals'){

                        if( !angular.isUndefined(params.to_birthday)) {
                            params.from_birthday=$filter('date')(params.to_birthday, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.from_birthday)) {
                            params.from_birthday=$filter('date')(params.from_birthday, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.to_guardian_date)) {
                            params.to_guardian_date=$filter('date')(params.to_guardian_date, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.from_guardian_date)) {
                            params.from_guardian_date=$filter('date')(params.from_guardian_date, 'dd-MM-yyyy')
                        }

                    }
                    else if(params.target =='cases'){
                        if( !angular.isUndefined(params.visited_at_to)) {
                            params.visited_at_to=$filter('date')(params.visited_at_to, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.visited_at_from)) {
                            params.visited_at_from=$filter('date')(params.visited_at_from, 'dd-MM-yyyy')
                        }

                    }
                    else if(params.target =='vouchers'){

                        if( !angular.isUndefined(params.voucher_date_from)) {
                            params.voucher_date_from=$filter('date')(params.voucher_date_from, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.voucher_date_to)) {
                            params.voucher_date_to=$filter('date')(params.voucher_date_to, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.receipt_date_from)) {
                            params.receipt_date_from=$filter('date')(params.receipt_date_from, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.receipt_date_to)) {
                            params.receipt_date_to=$filter('date')(params.receipt_date_to, 'dd-MM-yyyy')
                        }

                    }

                }

                $rootScope.progressbar_start();
                persons.getPersonReports(params,function(response) {
                    $rootScope.progressbar_complete();
                    if(params.target == 'info') {
                         $scope.person=response;
                         // $rootScope.person_id=response.person_id;
                         // $rootScope.person_id=response.person_id;
                    }
                    else if(params.target =='payments'){
                        $scope.payment_total= response.total;
                        $scope.CurrentPage = response.payment.current_page;
                        $scope.TotalItems = response.payment.total;
                        $scope.ItemsPerPage = response.payment.per_page;
                        $scope.casePaymentsList= response.payment.data;
                    }
                    else if(params.target =='cases'){
                        $scope.CurrentPage10 = response.current_page;
                        $scope.TotalItems10 = response.total;
                        $scope.ItemsPerPage10 = response.per_page;
                        $scope.CasesList= response.data;
                    }
                    else if(params.target =='vouchers'){
                        $scope.CurrentPage11 = response.data.current_page;
                        $scope.TotalItems11 = response.data.total;
                        $scope.ItemsPerPage11 = response.data.per_page;
                        $scope.caseVouchersList= response.data.data;
                        $scope.total= response.total;

                    }
                    else if(params.target =='sponsorships') {
                        $scope.CurrentPage0 = response.current_page;
                        $scope.TotalItems0 = response.total;
                        $scope.ItemsPerPage0 = response.per_page;
                        $scope.sponsorshipList= response.data;


                    }
                    else if(params.target =='guardians'){
                        $scope.CurrentPage5 = response.current_page;
                        $scope.TotalItems5 = response.total;
                        $scope.ItemsPerPage5 = response.per_page;
                        $scope.guardiansList= response.data;
                    }
                    else if(params.target =='individuals'){
                        $scope.CurrentPage50 = response.current_page;
                        $scope.TotalItems50 = response.total;
                        $scope.ItemsPerPage50 = response.per_page;
                        $scope.individualsList= response.data;
                    }
                });
            };

            $scope.fetch=function(target){
                var params={'target':target};
                if(target == 'info'){
                     params.full_name =true;
                     params.person =true;
                     params.persons_i18n =true;
                     params.residence =true;
                     params.education =true;
                     params.health =true;
                     params.contacts =true;
                     params.islamic =true;
                     params.work =true;
                     params.banks =true;
                }
                else if(target == 'cases'){
                    $scope.cases_frm =true;
                    if(angular.isUndefined($rootScope.CurrentPage10)) {
                        params.page=1;
                    }else{
                        params.page=$rootScope.CurrentPage10;
                    }
                }
                else if(target =='payments'){
                    $scope.case_payments =true;
                    if(angular.isUndefined($rootScope.CurrentPage)) {
                        params.page=1;
                    }else{
                        params.page=$rootScope.CurrentPage;
                    }
                }
                else if(target == 'vouchers'){
                    $scope.case_vouchers =true;
                    if(angular.isUndefined($rootScope.CurrentPage11)) {
                        params.page=1;
                    }else{
                        params.page=$rootScope.CurrentPage11;
                    }
                }
                else if(target == 'individuals'){
                    $scope.case_individuals =true;
                    if(angular.isUndefined($rootScope.CurrentPage10)) {
                        params.page=1;
                    }else{
                        params.page=$rootScope.CurrentPage10;
                    }
                }
                else if(target == 'guardians'){
                    if(angular.isUndefined($rootScope.CurrentPage10)) {
                        params.page=1;
                    }else{
                        params.page=$rootScope.CurrentPage10;
                    }
                }
                else if(target == 'sponsorships'){
                    if(angular.isUndefined($rootScope.CurrentPage0)) {
                        params.page=1;
                    }else{
                        params.page=$rootScope.CurrentPage0;
                    }
                }

                resetSearchFilters(target);

                Content(params);
            };
            $scope.pageChanged = function (target,CurrentPage) {
                var param={};

                if(target  =='payments'){ param =$scope.case_payments_filters; }
                else if(target =='sponsorships'){ param =$scope.sponsorships_filters; }
                else if(target =='guardians'){ param = $scope.guardians_filters; }
                else if(target =='vouchers'){  param = $scope.voucher_filters;  }
                else if(target =='cases'){  param = $scope.cases_filters; }

                if(angular.isUndefined(CurrentPage)) {
                    CurrentPage=1;
                }

                param.target=target;
                param.page=CurrentPage;
                Content(param);
            };

            $scope.Search=function(target,data,action) {
                data.action=action;
                data.target=target;

                if(action == 'export'){

                    if(target =='payments'){
                        if( !angular.isUndefined(data.begin_date_from)) {
                            data.begin_date_from=$filter('date')(data.begin_date_from, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(data.end_date_from)) {
                            data.end_date_from=$filter('date')(data.end_date_from, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(data.begin_date_to)) {
                            data.begin_date_to=$filter('date')(data.begin_date_to, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(data.end_date_to)) {
                            data.end_date_to=$filter('date')(data.end_date_to, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(data.start_payment_date)) {
                            data.start_payment_date=$filter('date')(data.start_payment_date, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(data.end_payment_date)) {
                            data.end_payment_date= $filter('date')(data.end_payment_date, 'dd-MM-yyyy')
                        }
                    }
                    else if(target =='sponsorships'){
                        if( !angular.isUndefined(data.to_guaranteed_date)) {
                            data.to_guaranteed_date=$filter('date')(data.to_guaranteed_date, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(data.from_guaranteed_date)) {
                            data.from_guaranteed_date=$filter('date')(data.from_guaranteed_date, 'dd-MM-yyyy')
                        }

                    }
                    else if(target =='guardians' || target =='individuals'){

                        if( !angular.isUndefined(data.to_birthday)) {
                            data.to_birthday=$filter('date')(data.to_birthday, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(data.from_birthday)) {
                            data.from_birthday=$filter('date')(data.from_birthday, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(data.to_guardian_date)) {
                            data.to_guardian_date=$filter('date')(data.to_guardian_date, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(data.from_guardian_date)) {
                            data.from_guardian_date=$filter('date')(data.from_guardian_date, 'dd-MM-yyyy')
                        }

                    }
                    else if(target =='cases'){
                        if( !angular.isUndefined(data.visited_at_to)) {
                            data.visited_at_to=$filter('date')(data.visited_at_to, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(data.visited_at_from)) {
                            data.visited_at_from=$filter('date')(data.visited_at_from, 'dd-MM-yyyy')
                        }

                    }
                    else if(target =='vouchers'){

                        if( !angular.isUndefined(data.voucher_date_from)) {
                            data.voucher_date_from=$filter('date')(data.voucher_date_from, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(data.voucher_date_to)) {
                            data.voucher_date_to=$filter('date')(data.voucher_date_to, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(data.receipt_date_from)) {
                            data.receipt_date_from=$filter('date')(data.receipt_date_from, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(data.receipt_date_to)) {
                            data.receipt_date_to=$filter('date')(data.receipt_date_to, 'dd-MM-yyyy')
                        }

                    }
                    if(target =='guardians'){
                        data.individual_id=$scope.id;
                    }
                    else if(target =='individuals'){
                        data.guardian_id=$scope.id;
                    }
                    else{
                        data.person_id=$scope.id;
                    }

                    data.sub_target=$scope.type;

                    if($scope.type =='guardians'){
                        data.guardian_id=$scope.id;
                        delete  data.person_id;
                    }else{
                        delete  data.guardian_id;
                        data.person_id=$scope.id;
                    }

                    data.mode='show';

                    $rootScope.progressbar_start();
                    $http({
                        url: "/api/v1.0/common/persons/getPersonReports",
                        method: "POST",
                        data: data
                    }).then(function (response) {
                        $rootScope.progressbar_complete();
                        if( !angular.isUndefined(response.data.status)) {
                            $rootScope.toastrMessages('error',$filter('translate')('There are no records to export to Excel'));
                        }else{
                            var file_type='xlsx';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                            $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                        }
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);

                    });


                }else{
                    data.page=1;
                    Content(data);
                }

            };

        }

        $scope.dateOptions = { formatYear: 'yy',  startingDay: 0 };
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.today = function() { $scope.dt = new Date(); };
        $scope.clear = function() { $scope.dt = null; };

        $scope.format = $scope.formats[0];
        $scope.today();

        $scope.open1 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup1.opened = true; };
        $scope.open2 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup2.opened = true; };
        $scope.open3 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup3.opened = true; };
        $scope.open4 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup4.opened = true; };
        $scope.open5 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup5.opened = true; };
        $scope.open6 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup6.opened = true; };
        $scope.open7 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup7.opened = true; };
        $scope.open8 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup8.opened = true; };
        $scope.open9 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup9.opened = true;};
        $scope.open10 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup10.opened = true; };
        $scope.open11 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup11.opened = true; };
        $scope.open12 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup12.opened = true; };
        $scope.open13 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup13.opened = true; };
        $scope.open14 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup14.opened = true; };
        $scope.open88 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup88.opened = true; };
        $scope.open888 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup888.opened = true; };
        $scope.open90 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup90.opened = true;};
        $scope.open999 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup999.opened = true;};

        $scope.popup1  = {opened: false};
        $scope.popup2  = {opened: false};
        $scope.popup3  = {opened: false};
        $scope.popup4  = {opened: false};
        $scope.popup5  = {opened: false};
        $scope.popup6  = {opened: false};
        $scope.popup7  = {opened: false};
        $scope.popup8  = {opened: false};
        $scope.popup9  = {opened: false};
        $scope.popup10 = {opened: false};
        $scope.popup11 = {opened: false};
        $scope.popup12 = {opened: false};
        $scope.popup13 = {opened: false};
        $scope.popup14 = {opened: false};
        $scope.popup88  = {opened: false};
        $scope.popup888  = {opened: false};
        $scope.popup90  = {opened: false};
        $scope.popup999  = {opened: false};

    });
