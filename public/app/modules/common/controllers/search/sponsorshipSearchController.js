
angular.module('CommonModule')
    .controller('sponsorshipSearchController', function($stateParams,$rootScope,$scope,$timeout,$state,$http,casesInfo,$filter) {

        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
        $rootScope.type=$scope.type=$stateParams.type == 'guardians'? 'guardians' : 'persons';
        $state.current.data.pageTitle = $filter('translate')('Search Of Sponsorships');

        $scope.result =false;
        $scope.filterd=false;
        $scope.master=false;
        $scope.msg ='';
        $scope.itemsCount='50';
        $scope.filter ={id_card_number:"",first_name:"",second_name:"",third_name:"",last_name:""};


        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });


        var Filters= function () {
            $rootScope.clearToastr();
            $scope.msg=" ";
            $scope.filter.source=$scope.type;
            $scope.filter.action='filter';
            if(angular.isUndefined($scope.CurrentPage)) {
                $scope.CurrentPage=1;
            }

            $scope.filter.page=$scope.CurrentPage;
            $scope.filter.itemsCount=$scope.itemsCount;
            angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();
            casesInfo.sponsorshipCasesFilter($scope.filter,function (response) {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                $scope.master= response.master;
                $scope.items= response.data.data;
                $scope.organization_id= response.organization_id;
                $scope.filterd=true;
                $scope.CurrentPage = response.data.current_page;
                $scope.TotalItems = response.data.total;
                $scope.ItemsPerPage = response.data.per_page;

                if($scope.items.length != 0){
                    $scope.result=true;
                }else{
                    $scope.result=false;
                }


            }, function(error) {
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                $rootScope.toastrMessages('error',error.data.msg);
            });

        };

        $scope.Search = function(){
            Filters({action:'paginate'});
        };
        $scope.pageChanged = function (currentPage) {
            $scope.CurrentPage=currentPage;
            Filters({action:'paginate'});
        };

        $scope.itemsPerPage_ = function (itemsCount) {
            $scope.itemsCount=itemsCount;
            $scope.CurrentPage=1;
            Filters({action:'paginate' , 'itemsCount' :itemsCount});
        };

        $scope.export = function(){
            Filters({action:'export'});
        };

        $scope.download = function (target,id) {

            $rootScope.clearToastr();
            var url ='';
            var file_type ='zip';
            var  params={};
            var method='';

            if(target =='word'){
                method='POST';
                params={'case_id':id,
                    'target':'info',
                    'action':'filters',
                    'mode':'show',
                    'category_type' : 1,
                    'person' :true,
                    'persons_i18n' : true,
                    'category' : true,
                    'work' : true,
                    'contacts' : true,
                    'health' : true,
                    'default_bank' : true,
                    'education' : true,
                    'islamic' : true,
                    'banks' : true,
                    'residence' : true,
                    'persons_documents' : true,
                    'family_member' : true,
                    'father_id' : true,
                    'mother_id' : true,
                    'guardian_id' : true,
                    'guardian_kinship' : true,
                    'parent_detail':true,
                    'guardian_detail':true,
                    'visitor_note':true
                };
                url = "/api/v1.0/common/sponsorships/cases/word";
            }
            else if(target =='pdf'){
                method='POST';
                params={'case_id':id,
                    'target':'info',
                    'action':'filters',
                    'mode':'show',
                    'category_type' : 1,
                    'person' :true,
                    'persons_i18n' : true,
                    'category' : true,
                    'work' : true,
                    'contacts' : true,
                    'health' : true,
                    'default_bank' : true,
                    'education' : true,
                    'islamic' : true,
                    'banks' : true,
                    'residence' : true,
                    'persons_documents' : true,
                    'family_member' : true,
                    'father_id' : true,
                    'mother_id' : true,
                    'guardian_id' : true,
                    'guardian_kinship' : true,
                    'parent_detail':true,
                    'guardian_detail':true,
                    'visitor_note':true
                };
                url = "/api/v1.0/common/sponsorships/cases/pdf";
            }
            else{
                method='GET';
                url = "/api/v1.0/common/sponsorships/cases/exportCaseDocument/"+id;
            }

            $rootScope.progressbar_start();
            $http({
                url:url,
                method: method,
                data:params
            }).then(function (response) {
                $rootScope.progressbar_complete();

                if(target !='word' && response.data.status == false){
                    $rootScope.toastrMessages('error',$filter('translate')('no attachment to export'));
                }else{
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }

            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };
    });
