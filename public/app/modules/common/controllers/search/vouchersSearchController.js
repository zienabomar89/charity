angular.module('CommonModule').controller('vouchersSearchController', function( $uibModal,$http,$stateParams,$rootScope,$scope,$timeout,$state,category,Entity,persons,vouchers_categories,$filter,Org,cs_persons_vouchers) {

    $rootScope.labels = {
        "itemsSelected": $filter('translate')('itemsSelected'),
        "selectAll": $filter('translate')('selectAll'), "unselectAll": $filter('translate')('unselectAll'),
        "search": $filter('translate')('search'), "select": $filter('translate')('select')
    }

    $scope.selected =false;
 	$scope.tabs = false;
    $scope.filter={
        "vouchers_code":[],
        "codes":[],
        "using":1,
        "id_card_number":"",
        "receipt_date_from":"",
        "receipt_date_to":"",
        "action":'paginate',
        "page":1,
        "all_organization":true,
        "type":"",
        "voucher_source":"",
        "category_id":"",
        "case_category_id":"",
        "sponsor_id":"",
        "title":"",
        "content":"",
        "notes":"",
        "min_value":"",
        "max_value":"",
        "min_count":"",
        "transfer":"",
        "allow_day":"",
        "transfer_company_id":null,
        "max_count":"",
        "date_to":"",
        "date_from":"",
        "voucher_date_to":"",
        "voucher_date_from":""
    };

    $scope.total = 0;
    $scope.toggleStatus = false;
    $scope.full_name ='';
    $scope.id_card_number ='';

    Org.list({type:'organizations'},function (response) {  $scope.Org = response; });
    Org.list({type:'sponsors'},function (response) { $rootScope.Sponsors = response; });
    $rootScope.Choices = vouchers_categories.List();

    Entity.get({entity:'entities',c:'currencies,transferCompany,organizationsCat'},function (response) {
        $rootScope.currency = response.currencies;
        $rootScope.transferCompany = response.transferCompany;
        $rootScope.organizationsCat = response.organizationsCat;
    });

    cs_persons_vouchers.codesList(function (response) {
        $rootScope.codes_list = response.list;
    });

    cs_persons_vouchers.vouchersCodeList(function (response) {
        $rootScope.vouchers_code_list = response.list;
    });

    cs_persons_vouchers.vouchersTitleList(function (response) {
        $rootScope.titles_list = response.list;
    });

    $rootScope.aidCategories_=[];
    category.getSectionForEachCategory({'type':'aids'},function (response) {
        if(!angular.isUndefined(response)){
            $rootScope.aidCategories_firstStep_map =[];
            $rootScope.aidCategories = response.Categories;
            if(response.Categories.length > 0){
                for (var i in $rootScope.aidCategories) {
                    var item = $rootScope.aidCategories[i];
                    $rootScope.aidCategories_firstStep_map[item.id] = item.FirstStep;
                }
                angular.forEach($rootScope.aidCategories, function(v, k) {
                    if ($rootScope.lang == 'ar'){
                        $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
                    }
                    else{
                        $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
                    }
                });
            }
        }
    });

    var resetVouchersFilter =function(){
        $scope.filter={
            "vouchers_code":[],
            "codes":[],
            "using":1,
            "id_card_number":"",
            "receipt_date_from":"",
            "receipt_date_to":"",
            "action":'paginate',
            "page":1,
            "all_organization":true,
            "type":"",
            "voucher_source":"",
            "category_id":"",
            "case_category_id":"",
            "sponsor_id":"",
            "title":"",
            "content":"",
            "notes":"",
            "min_value":"",
            "max_value":"",
            "min_count":"",
            "transfer":"",
            "allow_day":"",
            "transfer_company_id":null,
            "max_count":"",
            "date_to":"",
            "date_from":"",
            "voucher_date_to":"",
            "voucher_date_from":"",
            "organization_category":[],
            "organization_project":"",
            "serial":""
        };
    };

    $scope.toggle = function() {
        $rootScope.clearToastr();
        $scope.toggleStatus = $scope.toggleStatus == false ? true: false;
        $scope.row= {};
        $scope.total = 0;
        $scope.vouchers = [];
        $scope.success = false;
        $scope.full_name ='';
        $scope.id_card_number ='';
        resetVouchersFilter();
    };

    var FiltersVouchersSearch= function (params) {
    	$rootScope.clearToastr();
        params.using=1;
        params.action='paginate';
        $scope.row= {};
        $scope.error=false;
        $scope.full_name ='';
        $scope.id_card_number ='';
        $scope.msg ='';
        $scope.msg ='';

        // if(!angular.isUndefined(params.id_card_number)) {
        //     if(params.id_card_number.length > 0 && params.id_card_number.length < 9) {
        //         $scope.success = false;
        //         $rootScope.toastrMessages('error',$filter('translate')('card number length must not exceed 9'));
        //     }else {
        //         if(params.id_card_number.length ==9) {
                    angular.element('.btn').addClass("disabled");
                    $rootScope.progressbar_start();
                    persons.voucherFilter(params,function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        if(response.success){
                            $scope.full_name = response.full_name;
                            $scope.id_card_number = response.id_card_number;
                            $scope.total = response.total;
                            $scope.vouchers = response.data;
                            $scope.success = true;
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
        //         }
        //     }
        // }
    }

    $scope.resetOrg=function(all_organization,organization_category){
        $rootScope.clearToastr();
        $scope.disblay_orgs =false;
        let ids = [];
        if( !angular.isUndefined(all_organization)) {
            if(all_organization == true ||all_organization == 'true'){
                angular.forEach(organization_category, function(v, k) {
                    ids.push(v.id);
                });

                Org.ListOfCategory({ids:ids},function (response) {
                    if (response.list.length > 0){
                        $scope.Org = response.list;
                        $scope.disblay_orgs =true;
                    }else{
                        $scope.Org = [];
                        $scope.disblay_orgs =false;
                        $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');
                    }
                });
            }else{
                $scope.Org = [];
                $scope.disblay_orgs =false;
                $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');
            }
        }else{
            $scope.Org = [];
            $scope.disblay_orgs =false;
            $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');

        }


    };

    $scope.load = function(){
        FiltersVouchersSearch();
    };

    $scope.export = function(params){
        angular.element('.btn').addClass("disabled");
        $rootScope.clearToastr();

        $scope.filter.action='xlsx';

        var data = angular.copy(params);
        data.action = action;
        data.items=[];

        if(!angular.isUndefined(params.receipt_date_from)) {
            data.receipt_date_from=$filter('date')(data.receipt_date_from, 'dd-MM-yyyy')
        }

        if(!angular.isUndefined(params.receipt_date_to)) {
            data.receipt_date_to=$filter('date')(data.receipt_date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.date_to)) {
            data.date_to=$filter('date')(data.date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.date_from)) {
            data.date_from= $filter('date')(data.date_from, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(data.voucher_date_to)) {
            data.voucher_date_to=$filter('date')(data.voucher_date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.voucher_date_from)) {
            data.voucher_date_from= $filter('date')(data.voucher_date_from, 'dd-MM-yyyy')
        }
        data.action=action;

        if( !angular.isUndefined(data.transfer)) {
            if(data.transfer == 0 ||data.transfer == '0'){
                data.transfer_company_id = null
            }
        }

        let codes=[];
        angular.forEach(data.codes, function(v2, k2) {
            codes.push(v2.id);
        });
        data.codes=codes;

        let vouchers_code=[];
        angular.forEach(data.vouchers_code, function(v2, k2) {
            vouchers_code.push(v2.id);
        });
        data.vouchers_code=vouchers_code;

        $rootScope.progressbar_start();

        $http({
            url: "/api/v1.0/common/reports/voucherSearch/filter",
            method: "POST",
            data: data
        }).then(function (response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                if(response.data.download_token){
                    $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                    window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }
            }, function(error) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                $rootScope.toastrMessages('error',error.data.msg);
            });
    };

    $scope.check = function(){
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: 'checkPerson.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {
                $scope.upload=false;
                $scope.filter_={
                    "vouchers_code":[],
                    "codes":[],
                    "titles":[],
                    "un_serial":"",
                    "receipt_date_from":"",
                    "receipt_date_to":"",
                    "voucher_source":"",
                    "category_id":"",
                    "case_category_id":"",
                    "sponsor_id":"",
                    "title":"",
                    "content":"",
                    "notes":"",
                    "min_value":"",
                    "max_value":"",
                    "min_count":"",
                    "transfer":"",
                    "allow_day":"",
                    "transfer_company_id":null,
                    "max_count":"",
                    "date_to":"",
                    "date_from":"",
                    "voucher_date_to":"",
                    "voucher_date_from":"",
                    "to_all" : false,
                    "for_person" :true,
                    "for_parent" : false,
                    "for_wife" : false,
                    "for_children" : false
                };

                $scope.fileUpload=function(){
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.upload=true;
                };

                $scope.download=function(){
                    angular.element('.btn').addClass("disabled");

                    $rootScope.clearToastr();
                    $rootScope.progressbar_start();
                    $http({
                        url:'/api/v1.0/common/reports/template',
                        method: "GET"
                    }).then(function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token+'&template=true';
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                };

                var Uploader = $scope.uploader = new FileUploader({
                    url:'/api/v1.0/common/reports/voucherSearch/check',
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });

                Uploader.onBeforeUploadItem = function(item) {
                    $rootScope.clearToastr();
             //   ,type:type
                    var params = {source: 'file'} ;

                    angular.forEach($scope.filter_, function(v, k) {

                        if(!angular.isUndefined(v)) {
                            if( !( v == null || v == 'null' || v == '' || v == ' ') ) {
                                if (k == 'receipt_date_from' || k == 'receipt_date_to' ||
                                    k == 'date_to' || k == 'date_from' ||
                                    k == 'voucher_date_to' ||k == 'voucher_date_from')
                                {
                                    params[k] = $filter('date')(v, 'dd-MM-yyyy')
                                }
                                else if (k == 'codes' || k == 'vouchers_code' || k == 'titles'){

                                    let codes=[];
                                    angular.forEach(v, function(v2, k2) {
                                        codes.push(v2.id);
                                    });

                                    params[k]=codes;
                                }else{
                                    params[k]=v;
                                }
                            }
                        }


                    });
                    item.formData = [params];
                };

                $scope.confirm = function () {
                    $rootScope.progressbar_start();
                    $rootScope.clearToastr();
                    $scope.spinner=false;
                    $scope.import=false;
                    angular.element('.btn').addClass("disabled");

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(response.status=='success')
                        {
                        	$scope.tabs = true;
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $scope.spinner=false;
                            $scope.import=false;
                            $rootScope.toastrMessages('success',response.msg);
                        }else{
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $rootScope.toastrMessages('error',response.msg);
                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                        }
                        if(response.download_token){
                            var file_type='xlsx';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }
                         $modalInstance.close();

                    };
                    Uploader.uploadAll();
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.remove=function(){
                    angular.element("input[type='file']").val(null);
                    $scope.uploader.clearQueue();
                    angular.forEach(
                        angular.element("input[type='file']"),
                        function(inputElem) {
                            angular.element(inputElem).val(null);
                        });
                };

                $scope.dateOptions = { formatYear: 'yy',  startingDay: 0 };
                $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                $scope.today = function() { $scope.dt = new Date(); };
                $scope.clear = function() { $scope.dt = null; };

                $scope.format = $scope.formats[0];
                $scope.today();

                $scope.popup99 = {opened: false};
                $scope.popup88 = {opened: false};
                $scope.popup999 = {opened: false};
                $scope.popup888 = {opened: false};

                $scope.popup880  = {opened: false};
                $scope.popup90  = {opened: false};
                $scope.popup880  = {opened: false};
                $scope.popup900  = {opened: false};

                $scope.open880 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup880.opened = true;};
                $scope.open999 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup999.opened = true;};
                $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
                $scope.open888 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup888.opened = true;};

                $scope.open88 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup88.opened = true; };
                $scope.open90 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup90.opened = true;};
                $scope.open880 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup880.opened = true; };
                $scope.open900 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup900.opened = true;};


            }});
    };

    $scope.resetVouchersFilter_=function(){
        resetVouchersFilter();
    }

    $scope.Search=function(params,action) {

        $rootScope.clearToastr();
        var data = angular.copy(params);
        data.action = action;
        data.items=[];

        if(!angular.isUndefined(params.receipt_date_from)) {
            data.receipt_date_from=$filter('date')(data.receipt_date_from, 'dd-MM-yyyy')
        }

        if(!angular.isUndefined(params.receipt_date_to)) {
            data.receipt_date_to=$filter('date')(data.receipt_date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.date_to)) {
            data.date_to=$filter('date')(data.date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.date_from)) {
            data.date_from= $filter('date')(data.date_from, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(data.voucher_date_to)) {
            data.voucher_date_to=$filter('date')(data.voucher_date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.voucher_date_from)) {
            data.voucher_date_from= $filter('date')(data.voucher_date_from, 'dd-MM-yyyy')
        }
        data.action=action;

        if( !angular.isUndefined(data.transfer)) {
            if(data.transfer == 0 ||data.transfer == '0'){
                data.transfer_company_id = null
            }
        }

        let codes=[];
        angular.forEach(data.codes, function(v2, k2) {
            codes.push(v2.id);
        });
        data.codes=codes;

        let vouchers_code=[];
        angular.forEach(data.vouchers_code, function(v2, k2) {
            vouchers_code.push(v2.id);
        });
        data.vouchers_code=vouchers_code;

        data.page=1;
        FiltersVouchersSearch(data);

    };

    $scope.dateOptions = { formatYear: 'yy',  startingDay: 0 };
    $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $scope.today = function() { $scope.dt = new Date(); };
    $scope.clear = function() { $scope.dt = null; };
    $scope.format = $scope.formats[0];
    $scope.today();
    $scope.popup90 = {opened: false};
    $scope.popup88 = {opened: false};
    $scope.popup900 = {opened: false};
    $scope.popup880 = {opened: false};
    $scope.popup999 = {opened: false};
    $scope.popup888 = {opened: false};
    $scope.popup99 = {opened: false};
    $scope.open90 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup90.opened = true;};
    $scope.open88 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup88.opened = true; };
    $scope.open880 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup880.opened = true; };
    $scope.open900 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup900.opened = true;};
    $scope.open999 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup999.opened = true;};
    $scope.open880 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup880.opened = true;};
    $scope.open90 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
    $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
    $scope.open888 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup888.opened = true;};

});
