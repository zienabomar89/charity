
angular.module('CommonModule').
controller('transfersController', function (Transfers,Org, $rootScope, $http,$filter, $scope,$state,$uibModal,Entity,persons,FileUploader, OAuthToken,$stateParams,$ngBootbox) {

    $rootScope.type=$scope.type= 'aids';
    $rootScope.categoryType= 2;
    $state.current.data.pageTitle = $filter('translate')('transfer request');
    $rootScope.clearToastr();
    $scope.CurrentPage=1;
    $scope.CurrentPage_=1;
    $rootScope.itemsCount=50;
    $rootScope.aidsItems=[];
    $rootScope.sponsorshipsItems=[];
    $rootScope.datafilter ={all_organization:false,page:1,category_type:$rootScope.categoryType};
    $scope.custom =false;
    $scope.inProgress = false;
    $rootScope.updateIndex = null;
    $scope.sortKeyArr=[];
    $scope.sortKeyArr_rev=[];
    $scope.sortKeyArr_un_rev=[];

    Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

    var LoadTransfers = function ($params) {
        $rootScope.settings.layout.pageSidebarClosed = true;
        $rootScope.aidsItems=[];
        $rootScope.sponsorshipsItems=[];
        $params.sortKeyArr_rev = $scope.sortKeyArr_rev;
        $params.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;
        $params.category_type = $rootScope.categoryType;
        $params.action='paginate';
        $params.page=$scope.CurrentPage;
        $params.itemsCount=$rootScope.itemsCount;
        $rootScope.progressbar_start();
        Transfers.filter($params,function (response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if( $rootScope.categoryType == 1){
                $rootScope.sponsorshipsItems = response.data;
                $scope.CurrentPage_ = response.current_page;
                $rootScope.TotalItems_ = response.total;
                $rootScope.ItemsPerPage_ = response.per_page;
            }else{
                $rootScope.aidsItems = response.data;
                $scope.CurrentPage = response.current_page;
                $rootScope.TotalItems = response.total;
                $rootScope.ItemsPerPage = response.per_page;
            }
        });
    };

    $rootScope.aidCategories_=[];
    angular.forEach($rootScope.aidCategories, function(v, k) {
        if ($rootScope.lang == 'ar'){
            $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
        }
        else{
            $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
        }
    });

    $rootScope.sponsorCategories_=[];
    angular.forEach($rootScope.sponsorCategories, function(v, k) {
        if ($rootScope.lang == 'ar'){
            $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
        }
        else{
            $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
        }
    });

    $scope.sortKeyArr=[];
    $scope.sortKeyArr_rev=[];
    $scope.sortKeyArr_un_rev=[];
    $scope.sort_ = function(keyname){
        $scope.sortKey_ = keyname;
        var reverse_ = false;

        if (
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
        ) {
            reverse_ = true;
        }

        var data = angular.copy($rootScope.datafilter);
        data.action ='filter';

        if (reverse_) {
            if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_rev.push(keyname);
            }
        }
        else {
            if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_un_rev.push(keyname);
            }
        }

        $rootScope.TransferSearch($rootScope.datafilter,'paginate');

    };
    $scope.toggleSearchForm = function() {
        $scope.custom = $scope.custom == false ? true: false;
        $rootScope.datafilter ={all_organization:false,page:1,category_type:$rootScope.categoryType};
    };
    $scope.pageChanged = function (CurrentPage) {
        $scope.CurrentPage = CurrentPage;
        $rootScope.datafilter.page = CurrentPage;
        $rootScope.TransferSearch($rootScope.datafilter,'paginate');
    };
    $scope.itemsPerPage__ = function (itemsCount) {
        $scope.CurrentPage = 1
        $rootScope.datafilter.page = 1;
        $rootScope.itemsCount = itemsCount;
        $rootScope.datafilter.itemsCount = itemsCount;
        $rootScope.TransferSearch($rootScope.datafilter,'paginate');
    };
    $scope.pageChanged_ = function (CurrentPage) {
        $scope.CurrentPage = CurrentPage;
        $rootScope.datafilter.page = CurrentPage;
        $rootScope.TransferSearch($rootScope.datafilter,'paginate');
    };

    $rootScope.load = function (category_type) {
        $scope.custom =false;
        $rootScope.categoryType = category_type;
        $rootScope.datafilter.category_type = category_type;
        $rootScope.datafilter.page = 1;
        if(category_type == 1){
            Entity.get({entity:'entities',c:'countries'},function (response) {
                $scope.Country = response.countries;
            });
        } else{
            Entity.get({entity:'entities',c:'aidCountries'},function (response) {
                $scope.aidCountries = response.aidCountries;
            });
        }
        LoadTransfers($rootScope.datafilter);
    };

    $rootScope.TransferSearch = function (params,action) {
        $rootScope.clearToastr();
        var data = angular.copy(params);

        if( !angular.isUndefined(data.created_at_to)) {
            data.created_at_to=$filter('date')(data.created_at_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.created_at_from)) {
            data.created_at_from= $filter('date')(data.created_at_from, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(params.confirm_date_to)) {
            params.confirm_date_to=$filter('date')(params.confirm_date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.confirm_date_from)) {
            params.confirm_date_from= $filter('date')(params.confirm_date_from, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.accept_date_to)) {
            params.accept_date_to=$filter('date')(params.accept_date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.accept_date_from)) {
            params.accept_date_from= $filter('date')(params.accept_date_from, 'dd-MM-yyyy')
        }

        data.action=action;

        data.organization_ids=[];

        if( !angular.isUndefined(data.all_organization)) {
            if(data.all_organization == true ||data.all_organization == 'true'){
                angular.forEach(data.organization_id, function(v, k) {
                    data.organization_ids.push(v.id);
                });
                delete data.organization_id;
            }else if(data.all_organization == false ||data.all_organization == 'false') {
                data.organization_ids=[];
            }

        }

        if(action == 'xlsx'){
            data.category_type = $rootScope.categoryType;
            $rootScope.progressbar_start();
            $http({
                url: "/api/v1.0/common/Transfers/filter",
                method: "POST",
                data: data
            }).then(function (response) {
                $rootScope.progressbar_complete();
                if(response.data.download_token){
                    $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });

        }else{

            data.page=1;
            LoadTransfers(data);
        }
    };
    $scope.getLocation = function(entity,parent,value){
        if(!angular.isUndefined(value)) {
            if (value !== null && value !== "" && value !== " ") {
                Entity.listChildren({'entity': entity, 'id': value, 'parent': parent}, function (response) {

                    if (entity == 'sdistricts') {

                        $scope.governarate = response;
                        $scope.regions = [];
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];

                        $scope.datafilter.adsdistrict_id = "";
                        $scope.datafilter.adsregion_id = "";
                        $scope.datafilter.adsneighborhood_id = "";
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";

                    }
                    else if (entity == 'sregions') {

                        $scope.regions = response;
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];
                        $scope.datafilter.adsregion_id = "";
                        $scope.datafilter.adsneighborhood_id = "";
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";
                    }
                    else if (entity == 'sneighborhoods') {
                        $scope.nearlocation = response;
                        $scope.squares = [];
                        $scope.mosques = [];
                        $scope.datafilter.adsneighborhood_id = "";
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";

                    }
                    else if (entity == 'ssquares') {
                        $scope.squares = response;
                        $scope.mosques = [];
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";
                    }
                    else if (entity == 'smosques') {
                        $scope.mosques = response;
                        $scope.datafilter.adsmosques_id = "";
                    }
                    else if (entity == 'districts') {

                        $scope.governarate = response;
                        $scope.city = [];
                        $scope.nearlocation = [];
                        $scope.mosques = [];
                        $scope.datafilter.governarate = "";
                        $scope.datafilter.city = "";
                        $scope.datafilter.location_id = "";

                    }
                    else if (entity == 'cities') {

                        $scope.city = response;
                        $scope.nearlocation = [];
                        $scope.mosques = [];
                        $scope.datafilter.city = "";
                        $scope.datafilter.location_id = "";

                    }
                    else if (entity == 'neighborhoods') {
                        $scope.nearlocation = response;
                        $scope.mosques = [];
                        $scope.datafilter.location_id = "";
                        $scope.datafilter.mosques_id = "";
                    }
                    else if (entity == 'mosques') {

                        $scope.mosques = response;
                        $scope.datafilter.mosques_id = "";
                    }

                });
            }
        }
    };

    $scope.confirm= function(action,id){

        if(action == 'reject'){
            $rootScope.clearToastr();
            $uibModal.open({
                size: 'md',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                templateUrl: 'detail.html',
                controller: function ($rootScope,$scope,$modalInstance) {

                    $scope.reason = '';
                    $scope.confirm = function (reason) {
                        $rootScope.clearToastr();
                        $rootScope.progressbar_start();
                        Transfers.accept({id:id,status:2,reason:reason },function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }else{
                                $modalInstance.close();
                                if(response.status=='success') {
                                    $rootScope.load( $rootScope.categoryType);
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }

                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                        });

                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }});

        }
        else{

            $ngBootbox.confirm($filter('translate')('are you want to confirm transfer') )
                .then(function() {

                    $rootScope.clearToastr();
                    $rootScope.progressbar_start();
                    Transfers.accept({id:id,status:1},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='error' || response.status=='failed')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                        }else{
                            if(response.status=='success') {
                                $rootScope.load( $rootScope.categoryType);
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                        }

                    }, function(error) {
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                });

        }
    };

    $scope.selectedAll = function(value){
        if(value){
            $scope.selectAll = true;
        }
        else{
            $scope.selectAll =false;
        }
        angular.forEach($rootScope.aidsItems, function(val,key) {

            if(!(v.has_case == 1 || v.has_case == '1')  && ( v.status_id == 0 || v.status_id == '0' )){
                val.check=$scope.selectAll;
            }
        });

    };

    $scope.acceptGroup=function(selected){
        $rootScope.clearToastr();

        var pass= true;
        var persons=[];
        if(selected){
            angular.forEach($rootScope.aidsItems, function(v, k) {
                if(!(v.has_case == 1 || v.has_case == '1')  && ( v.status_id == 0 || v.status_id == '0' )){
                    if(v.check){
                        persons.push(v.id);
                    }
                }

            });

        }
        else {
            angular.forEach($rootScope.aidsItems, function(v, k) {
                if(!(v.has_case == 1 || v.has_case == '1')  && ( v.status_id == 0 || v.status_id == '0' )){
                    persons.push(v.id);
                }
            });
        }


        if(persons.length==0){
            pass= false;
            $rootScope.toastrMessages('error',$filter('translate')('no selected cases'));
        }

        if (pass) {
            $ngBootbox.confirm($filter('translate')('are you want to confirm transfer') )
                .then(function() {

                    $rootScope.clearToastr();

                    $rootScope.progressbar_start();
                    Transfers.acceptGroup({'persons':persons,selected:selected},function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        if(response.status == 'success'){
                            $rootScope.toastrMessages('success',response.msg);
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }

                        if(response.download_token){
                            var file_type='xlsx';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }
                        $rootScope.TransferSearch($rootScope.datafilter,'paginate');

                    });

                });

        }

    }

    $rootScope.load($rootScope.categoryType);

    $scope.goTo=function(action,person,case_id,category_id){
        $rootScope.clearToastr();
        angular.forEach($rootScope.aidCategories, function(v, k) {
            if(v.id ==category_id){
                $state.go('caseform.'+v.FirstStep,{"mode":action,"id" :case_id,"category_id" :category_id});
            }
        });

    };

    $scope.sortKeyArr=[];
    $scope.sortKeyArr_rev=[];
    $scope.sortKeyArr_un_rev=[];
    $scope.sort_ = function(keyname){
        $scope.sortKey_ = keyname;
        var reverse_ = false;

        if (
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
        ) {
            reverse_ = true;
        }

        var data = angular.copy($rootScope.datafilter);
        data.action ='filter';

        if (reverse_) {
            if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_rev.push(keyname);
            }

            // angular.forEach($scope.sortKeyArr_un_rev, function(val,key) {
            //     if(val == keyname){
            //         $scope.sortKeyArr_un_rev.splice(key,1);
            //         return ;
            //     }
            // });

        }
        else {
            if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_un_rev.push(keyname);
            }
            // angular.forEach($scope.sortKeyArr_rev, function(val,key) {
            //     if(val == keyname){
            //         $scope.sortKeyArr_rev.splice(key,1);
            //         return ;
            //     }
            // });
        }

        data.sortKeyArr_rev = $scope.sortKeyArr_rev;
        data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;

        $rootScope.TransferSearch($rootScope.datafilter,'paginate');

    };

    $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
    $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.today = function() { $scope.dt = new Date();};
    $scope.today();
    $scope.clear = function() {$scope.dt = null;};
    $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
    $scope.popup99 = {opened: false};
    $scope.open88 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup88.opened = true;};
    $scope.popup88 = {opened: false};

    $scope.open991 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup991.opened = true;};
    $scope.popup991 = {opened: false};
    $scope.open881 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup881.opened = true;};
    $scope.popup881 = {opened: false};

    $scope.open990 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup990.opened = true;};
    $scope.popup990 = {opened: false};
    $scope.open880 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup880.opened = true;};
    $scope.popup880 = {opened: false};
});

angular.module('CommonModule').
controller('acceptedTransfersController', function (Transfers, $rootScope, $http,$filter, $scope,$state,$uibModal,Entity,persons,FileUploader, OAuthToken,$stateParams,$ngBootbox) {

    $rootScope.type=$scope.type= 'aids';
    $rootScope.categoryType= 2;
    $state.current.data.pageTitle = $filter('translate')('accepted transfers');
    $rootScope.clearToastr();
    $scope.CurrentPage=1;
    $scope.CurrentPage_=1;
    $rootScope.itemsCount=50;
    $rootScope.aidsItems=[];
    $rootScope.sponsorshipsItems=[];
    $rootScope.datafilter ={all_organization:false,page:1,category_type:$rootScope.categoryType};
    $scope.custom =false;
    $scope.inProgress = false;
    $rootScope.updateIndex = null;
    $scope.sortKeyArr=[];
    $scope.sortKeyArr_rev=[];
    $scope.sortKeyArr_un_rev=[];

    var acceptedTransfers  = function ($params) {
        $rootScope.settings.layout.pageSidebarClosed = true;
        $rootScope.aidsItems=[];
        $rootScope.sponsorshipsItems=[];
        $params.sortKeyArr_rev = $scope.sortKeyArr_rev;
        $params.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;
        $params.category_type = $rootScope.categoryType;
        $params.action='paginate';
        $params.page=$scope.CurrentPage;
        $params.itemsCount=$rootScope.itemsCount;
        $rootScope.progressbar_start();
        Transfers.acceptedFilter($params,function (response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if( $rootScope.categoryType == 1){
                $rootScope.sponsorshipsItems = response.data;
                $scope.CurrentPage_ = response.current_page;
                $rootScope.TotalItems_ = response.total;
                $rootScope.ItemsPerPage_ = response.per_page;
            }else{
                $rootScope.aidsItems = response.data;
                $scope.CurrentPage = response.current_page;
                $rootScope.TotalItems = response.total;
                $rootScope.ItemsPerPage = response.per_page;
            }
        });
    };

    $rootScope.aidCategories_=[];
    angular.forEach($rootScope.aidCategories, function(v, k) {
        if ($rootScope.lang == 'ar'){
            $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
        }
        else{
            $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
        }
    });

    $rootScope.sponsorCategories_=[];
    angular.forEach($rootScope.sponsorCategories, function(v, k) {
        if ($rootScope.lang == 'ar'){
            $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
        }
        else{
            $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
        }
    });

    $scope.sortKeyArr=[];
    $scope.sortKeyArr_rev=[];
    $scope.sortKeyArr_un_rev=[];
    $scope.sort_ = function(keyname){
        $scope.sortKey_ = keyname;
        var reverse_ = false;

        if (
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
        ) {
            reverse_ = true;
        }

        var data = angular.copy($rootScope.datafilter);
        data.action ='filter';

        if (reverse_) {
            if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_rev.push(keyname);
            }
        }
        else {
            if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_un_rev.push(keyname);
            }
        }

        $rootScope._TransferSearch($rootScope.datafilter,'paginate');

    };
    $scope.toggleSearchForm = function() {
        $scope.custom = $scope.custom == false ? true: false;
        $rootScope.datafilter ={all_organization:false,page:1,category_type:$rootScope.categoryType};
    };
    $scope.pageChanged = function (CurrentPage) {
        $scope.CurrentPage = CurrentPage;
        $rootScope.datafilter.page = CurrentPage;
        $rootScope._TransferSearch($rootScope.datafilter,'paginate');
    };
    $scope.itemsPerPage__ = function (itemsCount) {
        $scope.CurrentPage = 1;
        $rootScope.datafilter.page = 1;
        $rootScope.itemsCount = itemsCount;
        $rootScope.datafilter.itemsCount = itemsCount;
        $rootScope._TransferSearch($rootScope.datafilter,'paginate');
    };
    $scope.pageChanged_ = function (CurrentPage) {
        $scope.CurrentPage = CurrentPage;
        $rootScope.datafilter.page = CurrentPage;
        $rootScope._TransferSearch($rootScope.datafilter,'paginate');
    };

    $rootScope.load = function (category_type) {
        $scope.custom =false;
        $rootScope.categoryType = category_type;
        $rootScope.datafilter.category_type = category_type;
        $rootScope.datafilter.page = 1;
        if(category_type == 1){
            Entity.get({entity:'entities',c:'countries'},function (response) {
                $scope.Country = response.countries;
            });
        } else{
            Entity.get({entity:'entities',c:'aidCountries'},function (response) {
                $scope.aidCountries = response.aidCountries;
            });
        }
        acceptedTransfers($rootScope.datafilter);
    };

    $rootScope._TransferSearch = function (params,action) {
        $rootScope.clearToastr();
        var data = angular.copy(params);

        if( !angular.isUndefined(data.created_at_to)) {
            data.created_at_to=$filter('date')(data.created_at_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.created_at_from)) {
            data.created_at_from= $filter('date')(data.created_at_from, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(params.confirm_date_to)) {
            params.confirm_date_to=$filter('date')(params.confirm_date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.confirm_date_from)) {
            params.confirm_date_from= $filter('date')(params.confirm_date_from, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.accept_date_to)) {
            params.accept_date_to=$filter('date')(params.accept_date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(params.accept_date_from)) {
            params.accept_date_from= $filter('date')(params.accept_date_from, 'dd-MM-yyyy')
        }

        data.action=action;

        if(action == 'xlsx'){
            data.category_type = $rootScope.categoryType;
            $rootScope.progressbar_start();
            $http({
                url: "/api/v1.0/common/Transfers/acceptedFilter",
                method: "POST",
                data: data
            }).then(function (response) {
                $rootScope.progressbar_complete();
                if(response.data.download_token){
                    $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });

        }else{

            data.page=1;
            acceptedTransfers(data);
        }
    };
    $scope.getLocation = function(entity,parent,value){
        if(!angular.isUndefined(value)) {
            if (value !== null && value !== "" && value !== " ") {
                Entity.listChildren({'entity': entity, 'id': value, 'parent': parent}, function (response) {

                    if (entity == 'sdistricts') {

                        $scope.governarate = response;
                        $scope.regions = [];
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];

                        $scope.datafilter.adsdistrict_id = "";
                        $scope.datafilter.adsregion_id = "";
                        $scope.datafilter.adsneighborhood_id = "";
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";

                    }
                    else if (entity == 'sregions') {

                        $scope.regions = response;
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];
                        $scope.datafilter.adsregion_id = "";
                        $scope.datafilter.adsneighborhood_id = "";
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";
                    }
                    else if (entity == 'sneighborhoods') {
                        $scope.nearlocation = response;
                        $scope.squares = [];
                        $scope.mosques = [];
                        $scope.datafilter.adsneighborhood_id = "";
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";

                    }
                    else if (entity == 'ssquares') {
                        $scope.squares = response;
                        $scope.mosques = [];
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";
                    }
                    else if (entity == 'smosques') {
                        $scope.mosques = response;
                        $scope.datafilter.adsmosques_id = "";
                    }
                    else if (entity == 'districts') {

                        $scope.governarate = response;
                        $scope.city = [];
                        $scope.nearlocation = [];
                        $scope.mosques = [];
                        $scope.datafilter.governarate = "";
                        $scope.datafilter.city = "";
                        $scope.datafilter.location_id = "";

                    }
                    else if (entity == 'cities') {

                        $scope.city = response;
                        $scope.nearlocation = [];
                        $scope.mosques = [];
                        $scope.datafilter.city = "";
                        $scope.datafilter.location_id = "";

                    }
                    else if (entity == 'neighborhoods') {
                        $scope.nearlocation = response;
                        $scope.mosques = [];
                        $scope.datafilter.location_id = "";
                        $scope.datafilter.mosques_id = "";
                    }
                    else if (entity == 'mosques') {

                        $scope.mosques = response;
                        $scope.datafilter.mosques_id = "";
                    }

                });
            }
        }
    };

    $scope.confirm= function(action,id){

        if(action == 'reject'){
            $rootScope.clearToastr();
            $uibModal.open({
                size: 'md',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                templateUrl: 'detail.html',
                controller: function ($rootScope,$scope,$modalInstance) {

                    $scope.reason = '';
                    $scope.confirm = function (reason) {
                        $rootScope.clearToastr();
                        $rootScope.progressbar_start();
                        Transfers.confirm({id:id,status:2,reason:reason },function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }else{
                                $modalInstance.close();
                                if(response.status=='success') {
                                    $rootScope.load( $rootScope.categoryType);
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }

                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                        });

                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }});

        }else{

            $ngBootbox.confirm($filter('translate')('are you want to confirm transfer') )
                .then(function() {

                    $rootScope.clearToastr();
                    $rootScope.progressbar_start();
                    Transfers.confirm({id:id,status:3,save:true },function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='error' || response.status=='failed')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                        }else{
                            if(response.status=='success') {
                                $rootScope.load( $rootScope.categoryType);
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                        }

                    }, function(error) {
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                });

        }
    };

    $scope.selectedAll = function(value){
        if(value){
            $scope.selectAll = true;
        }
        else{
            $scope.selectAll =false;
        }
        angular.forEach($rootScope.aidsItems, function(val,key) {
            val.check=$scope.selectAll;
        });

    };

    $scope.acceptGroup=function(selected){
        $rootScope.clearToastr();

        var pass= true;
        var persons=[];
        if(selected){

            angular.forEach($rootScope.aidsItems, function(v, k) {
                if(v.check){
                    persons.push(v.transfer_log_id);
                }
            });
        }
        else {
            angular.forEach($rootScope.aidsItems, function(v, k) {
                persons.push(v.transfer_log_id);
            });

        }

        if(persons.length==0){
            pass= false;
            $rootScope.toastrMessages('error',$filter('translate')('no selected cases'));
        }

        if (pass) {
            $ngBootbox.confirm($filter('translate')('are you want to confirm transfer') )
                .then(function() {

                    $rootScope.clearToastr();

                    $rootScope.progressbar_start();
                    Transfers.confirmGroup({'persons':persons,selected:selected},function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        if(response.status == 'success'){
                            $rootScope.toastrMessages('success',response.msg);
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }

                        if(response.download_token){
                            var file_type='xlsx';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }
                        $rootScope._TransferSearch($rootScope.datafilter,'paginate');
                    });

                });

        }

    }

    $rootScope.load($rootScope.categoryType);

    $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
    $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.today = function() { $scope.dt = new Date();};
    $scope.today();
    $scope.clear = function() {$scope.dt = null;};
    $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
    $scope.popup99 = {opened: false};
    $scope.open88 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup88.opened = true;};
    $scope.popup88 = {opened: false};
});
