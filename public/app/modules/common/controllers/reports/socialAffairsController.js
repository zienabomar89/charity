
angular.module('CommonModule').controller('socialAffairsController', function($stateParams,$rootScope,$scope,$timeout,$uibModal,Entity,$state,reportsService,$filter,$http,FileUploader,Relays, OAuthToken,Log) {

    $rootScope.clearToastr();
    $rootScope.setAuthUser();
    $scope.selected =false;
    $scope.checkCards = false ;
    $scope.items = [] ;

    $scope.filter={type:'socialAffairs',using:"1",first_name:"",second_name:"",third_name:"",last_name:"",id_card_number:"",action:'paginate'};
    $state.current.data.pageTitle = $filter('translate')('Search ON social affairs');

    $scope.searchFilter = function (params,action) {

        if(params.using != 1){
            params.id_card_number = "";
        }else{
            params.first_name = "";
            params.second_name = "";
            params.third_name = "";
            params.last_name = "";
        }

        var pass = 1 ;


        if(params.using == 1 && !params.id_card_number){
            $rootScope.toastrMessages('error',$filter('translate')('You should enter card number to search'));
            return ;
        }

        if( params.using != 1 &&(!params.first_name || !params.last_name) ) {
            $rootScope.toastrMessages('error',$filter('translate')('You should enter first and last name to search'));
            return ;
        }

        if(params.using == 1 && !angular.isUndefined(params.id_card_number)) {
            if(params.id_card_number.length != 9) {
                $rootScope.toastrMessages('error',$filter('translate')('card number length must not exceed 9'));
                return ;
            }
        }

        $rootScope.clearToastr();
        if(pass){
            $scope.items = [];
            params.action = action;
            angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();
            reportsService.searchFilter(params,function (response) {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                if(response.status == 'true' || response.status == true){
                    $scope.items = response.items;
                }else{
                    $rootScope.toastrMessages('error',$filter ('translate') ('there is no data to show'));
                }
            });
        }
    };

    var resetScopeCards = function () {
        $scope.cards=  Log.getCardsByUser();
    };

    $scope.resetScopeOnChange = function(){

        $scope.items = [];
        if($scope.filter.using != 1){
            $scope.filter.id_card_number = "";
        }else{
            $scope.filter.first_name = "";
            $scope.filter.second_name = "";
            $scope.filter.third_name = "";
            $scope.filter.last_name = "";
            resetScopeCards();
        }
    };

    $scope.getCard = function(){
        $scope.checkCards = !$scope.checkCards ;
        resetScopeCards();
    };

    $scope.selected = function(id_card_number){
        $scope.checkCards = !$scope.checkCards ;
        $scope.filter.id_card_number = id_card_number ;

    };

    $scope.check = function(type){
        $rootScope.clearToastr();
        var templateUrl ='checkPerson.html';

        if(type == 0){
            templateUrl='updateData.html';
        }

        $uibModal.open({
            templateUrl: '/app/modules/common/views/reports/socialAffairs/model/'+templateUrl,
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'md',
            controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {
                $scope.upload=false;

                $scope.url='/api/v1.0/common/reports/socialAffairs/check';

                if(type == 0){
                    $scope.url='/api/v1.0/common/reports/socialAffairs/update';
                }

                $scope.fileUpload=function(){
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.upload=true; $scope.upload=true;
                };

                $scope.download=function(){
                    angular.element('.btn').addClass("disabled");

                    $rootScope.progressbar_start();
                    $rootScope.clearToastr();
                    $http({
                        url:'/api/v1.0/common/reports/template',
                        method: "GET"
                    }).then(function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token+'&template=true';
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                };

                var Uploader = $scope.uploader = new FileUploader({
                    url:$scope.url,
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });

                $scope.confirm = function () {
                    $rootScope.clearToastr();
                    $scope.spinner=false;
                    $scope.import=false;
                    angular.element('.btn').addClass("disabled");
                    $rootScope.progressbar_start();

                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.clearToastr();
                        item.formData = [{source: 'file',type:type}];
                    };

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(response.status=='success')
                        {
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $scope.spinner=false;
                            $scope.import=false;
                            $rootScope.toastrMessages('success',response.msg);
                        }else{
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $rootScope.toastrMessages('error',response.msg);
                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                        }
                        if(response.download_token){
                            var file_type='xlsx';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }
                        $modalInstance.close();

                    };
                    Uploader.uploadAll();
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.remove=function(){
                    angular.element("input[type='file']").val(null);
                    $scope.uploader.clearQueue();
                    angular.forEach(
                        angular.element("input[type='file']"),
                        function(inputElem) {
                            angular.element(inputElem).val(null);
                        });
                };

            }});
    };
    $scope.export = function(action){
        angular.element('.btn').addClass("disabled");
        $rootScope.clearToastr();

        $scope.filter.action=action;
        var  file_type = 'zip';
        if(action =='xlsx'){
            file_type = 'xlsx';
        }
        $scope.filter.type='socialAffairs';
        $rootScope.progressbar_start();
        reportsService.filter($scope.filter,function (response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if(response.download_token){
                $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
            }else{
                $rootScope.toastrMessages('error',$filter('translate')('action failed'));

            }
        });
    };

});

angular.module('CommonModule').controller('affairsDetailsController', function($stateParams,$rootScope,$scope,$timeout,$uibModal,Entity,$state,reportsService,$filter,$http,FileUploader,Relays, OAuthToken,Log) {

    $rootScope.clearToastr();
    $rootScope.setAuthUser();
    $scope.selected =false;
    $scope.filter={photo_url:null,type:'socialAffairs',using:1,id_card_number:$stateParams.id,action:'paginate'};
    $scope.name = null;
    $state.current.data.pageTitle = $filter('translate')('Search ON social affairs');
    $rootScope.row= {photo_url:null,parent:[],wives:[],childrens:[]};

    if(!$stateParams.id){
        $rootScope.toastrMessages('error',$filter('translate')('no card number to display data'));
        return;
    }
    else{
        var FiltersSocialAffairs= function (target) {
            $rootScope.clearToastr();
            $rootScope.row= {photo_url:null,parent:[],wives:[],childrens:[]};
           $scope.filter.target=target;
            $scope.filter.using=1;
            $scope.filter.action='paginate';
            $scope.filter.type='socialAffairs';
            $scope.error=false;
            $scope.msg ='';

            if( !angular.isUndefined($scope.filter.id_card_number)) {
                if($scope.filter.id_card_number.length > 0 && $scope.filter.id_card_number.length < 9) {
                    $scope.success = false;
                    $rootScope.toastrMessages('error',$filter('translate')('card number length must not exceed 9'));
                }else {
                    if($scope.filter.id_card_number.length ==9) {
                        angular.element('.btn').addClass("disabled");
                        $rootScope.progressbar_start();
                        reportsService.filter($scope.filter,function (response) {
                            $rootScope.progressbar_complete();
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            if(response.status){
                                $scope.success = true;
                                response.data.CI_ADDRESS = response.data.CI_REGION + ' - ' + response.data.CI_CITY  + ' - ' + response.data.STREET_ARB;
                                $rootScope.row= response.data;
                                $scope.name = response.data.FNAME_ARB + " " +  response.data.SNAME_ARB + " " + response.data.TNAME_ARB + ' ' +  response.data.LNAME_ARB;

                                if (!($rootScope.row.photo_url == null || $rootScope.row.photo_url == 'null')){
                                    $rootScope.ShowCitizenImage=true;
                                }else{
                                    $rootScope.ShowCitizenImage=false;
                                }

                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                        });
                    }
                }
            }
        };

        var resetScope = function () {
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            $scope.success=false;
            $scope.error=false;
            $scope.msg ='';
            $rootScope.row= {photo_url:null,parent:[],wives:[],childrens:[]};
        };


        $scope.load = function(target){
            if(target == 'basic'){
                $scope.success = false;
            }

            FiltersSocialAffairs(target);
        };
        $scope.export = function(action){
            angular.element('.btn').addClass("disabled");
            $rootScope.clearToastr();

            $scope.filter.action=action;
            var  file_type = 'zip';
            if(action =='xlsx'){
                file_type = 'xlsx';
            }
            $scope.filter.type='socialAffairs';
            $rootScope.progressbar_start();
            reportsService.filter($scope.filter,function (response) {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                if(response.download_token){
                    $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));

                }
            });
        };
        $scope.relay_ = function(type){
            // type 1 sponsorship
            // type 2 aid

            $uibModal.open({
                templateUrl: '/app/modules/common/views/reports/citizenRepository/model/relyOne.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {
                    $scope.category_id=[];
                    $scope.persons={};
                    $scope.msg1=[];
                    $scope.type=type;

                    if(type ==1){
                        $scope.choices=$rootScope.sponsorCategories;
                        Entity.get({entity:'entities',c:'countries'},function (response) {
                            $scope.Country = response.countries;
                        });
                    } else{
                        $scope.choices=$rootScope.aidCategories;
                        Entity.get({entity:'entities',c:'aidCountries'},function (response) {
                            $scope.aidCountries = response.aidCountries;
                        });
                    }
                    $scope.confirm = function (item) {

                        angular.element('.btn').addClass("disabled");
                        var inputs = angular.copy(item);
                        var params={type:type,reMap:$rootScope.row,inputs:inputs};
                        params.categories = [];
                        angular.forEach(inputs.category_id, function(v, k) {
                            params.categories.push(v.id);
                        });

                        delete inputs.category_id;
                        $rootScope.progressbar_start();
                        Relays.relayOne(params,function (response) {
                            $rootScope.progressbar_complete();
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            if(response.status=='error' || response.status=='failed') {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid') {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                if(response.status=='success')
                                {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                $modalInstance.close();
                            }
                        }, function(error) {
                            $rootScope.progressbar_complete();
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $rootScope.toastrMessages('error',error.data.msg);
                            $modalInstance.close();
                        });
                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.getLocation = function(entity,parent,value){
                        if(!angular.isUndefined(value)) {
                            if (value !== null && value !== "" && value !== " ") {
                                Entity.listChildren({'entity': entity, 'id': value, 'parent': parent}, function (response) {

                                    if (entity == 'sdistricts') {
                                        if (!angular.isUndefined($scope.msg1.adscountry_id)) {
                                            $scope.msg1.adscountry_id = [];
                                        }

                                        $scope.governarate = response;
                                        $scope.regions = [];
                                        $scope.nearlocation = [];
                                        $scope.squares = [];
                                        $scope.mosques = [];

                                        $scope.persons.adsdistrict_id = "";
                                        $scope.persons.adsregion_id = "";
                                        $scope.persons.adsneighborhood_id = "";
                                        $scope.persons.adssquare_id = "";
                                        $scope.persons.adsmosques_id = "";

                                    }
                                    else if (entity == 'sregions') {
                                        if (!angular.isUndefined($scope.msg1.adsdistrict_id)) {
                                            $scope.msg1.adsdistrict_id = [];
                                        }

                                        $scope.regions = response;
                                        $scope.nearlocation = [];
                                        $scope.squares = [];
                                        $scope.mosques = [];
                                        $scope.persons.adsregion_id = "";
                                        $scope.persons.adsneighborhood_id = "";
                                        $scope.persons.adssquare_id = "";
                                        $scope.persons.adsmosques_id = "";
                                    }
                                    else if (entity == 'sneighborhoods') {
                                        if (!angular.isUndefined($scope.msg1.adsregion_id)) {
                                            $scope.msg1.adsregion_id = [];
                                        }
                                        $scope.nearlocation = response;
                                        $scope.squares = [];
                                        $scope.mosques = [];
                                        $scope.persons.adsneighborhood_id = "";
                                        $scope.persons.adssquare_id = "";
                                        $scope.persons.adsmosques_id = "";

                                    }
                                    else if (entity == 'ssquares') {
                                        if (!angular.isUndefined($scope.msg1.adssquare_id)) {
                                            $scope.msg1.city = [];
                                        }
                                        $scope.squares = response;
                                        $scope.mosques = [];
                                        $scope.persons.adssquare_id = "";
                                        $scope.persons.adsmosques_id = "";
                                    }
                                    else if (entity == 'smosques') {
                                        if (!angular.isUndefined($scope.msg1.adssquare_id)) {
                                            $scope.msg1.adssquare_id = [];
                                        }
                                        $scope.mosques = response;
                                        $scope.persons.adsmosques_id = "";
                                    }
                                    else if (entity == 'districts') {
                                        if (!angular.isUndefined($scope.msg1.country)) {
                                            $scope.msg1.country = [];
                                        }

                                        $scope.governarate = response;
                                        $scope.city = [];
                                        $scope.nearlocation = [];
                                        $scope.mosques = [];
                                        $scope.persons.governarate = "";
                                        $scope.persons.city = "";
                                        $scope.persons.location_id = "";

                                    }
                                    else if (entity == 'cities') {
                                        if (!angular.isUndefined($scope.msg1.governarate)) {
                                            $scope.msg1.governarate = [];
                                        }

                                        $scope.city = response;
                                        $scope.nearlocation = [];
                                        $scope.mosques = [];
                                        $scope.persons.city = "";
                                        $scope.persons.location_id = "";

                                    }
                                    else if (entity == 'neighborhoods') {
                                        if (!angular.isUndefined($scope.msg1.city)) {
                                            $scope.msg1.city = [];
                                        }
                                        $scope.nearlocation = response;
                                        $scope.mosques = [];
                                        $scope.persons.location_id = "";
                                        $scope.persons.mosques_id = "";
                                    }
                                    else if (entity == 'mosques') {
                                        if (!angular.isUndefined($scope.msg1.location_id)) {
                                            $scope.msg1.location_id = [];
                                        }
                                        $scope.mosques = response;
                                        $scope.persons.mosques_id = "";
                                    }

                                });
                            }
                        }
                    };


                }});

        };

        $scope.load('basic');
    }


});

