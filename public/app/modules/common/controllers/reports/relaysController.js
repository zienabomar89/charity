
angular.module('CommonModule').
controller('relaysController', function (Relays, $rootScope, $http,$filter, $scope,$state,$uibModal,Entity,persons,FileUploader, OAuthToken,$stateParams) {

    $rootScope.type=$scope.type=$stateParams.type == 'aids'? 'aids' : 'sponsorships';
    $rootScope.categoryType=$scope.categoryType=$stateParams.type == 'aids'? 2 : 1;
    $state.current.data.pageTitle = $filter('translate')('Relays');
    $rootScope.clearToastr();
    $scope.CurrentPage=1;
    $scope.CurrentPage_=1;
    $rootScope.itemsCount=50;
    $rootScope.aidsItems=[];
    $rootScope.sponsorshipsItems=[];
    $rootScope.datafilter ={page:1,category_type:$rootScope.categoryType};
    $scope.custom =false;
    $scope.inProgress = false;
    $rootScope.updateIndex = null;
    $scope.sortKeyArr=[];
    $scope.sortKeyArr_rev=[];
    $scope.sortKeyArr_un_rev=[];

    var LoadRelays = function ($params) {
        $rootScope.settings.layout.pageSidebarClosed = true;
        $rootScope.aidsItems=[];
        $rootScope.sponsorshipsItems=[];
        $params.sortKeyArr_rev = $scope.sortKeyArr_rev;
        $params.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;
        $params.category_type = $rootScope.categoryType;
        $params.action='paginate';
        $params.page=$scope.CurrentPage;
        $params.itemsCount=$rootScope.itemsCount;
        $rootScope.progressbar_start();
        Relays.filter($params,function (response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if( $rootScope.categoryType == 1){
                $rootScope.sponsorshipsItems = response.data;
                $scope.CurrentPage_ = response.current_page;
                $rootScope.TotalItems_ = response.total;
                $rootScope.ItemsPerPage_ = response.per_page;
            }else{
                $rootScope.aidsItems = response.data;
                $scope.CurrentPage = response.current_page;
                $rootScope.TotalItems = response.total;
                $rootScope.ItemsPerPage = response.per_page;
            }
        });
    };

    $rootScope.aidCategories_=[];
    angular.forEach($rootScope.aidCategories, function(v, k) {
        if ($rootScope.lang == 'ar'){
            $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
        }
        else{
            $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
        }
    });

    $rootScope.sponsorCategories_=[];
    angular.forEach($rootScope.sponsorCategories, function(v, k) {
        if ($rootScope.lang == 'ar'){
            $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
        }
        else{
            $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
        }
    });

    $scope.sortKeyArr=[];
    $scope.sortKeyArr_rev=[];
    $scope.sortKeyArr_un_rev=[];
    $scope.sort_ = function(keyname){
        $scope.sortKey_ = keyname;
        var reverse_ = false;

        if (
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
        ) {
            reverse_ = true;
        }

        var data = angular.copy($rootScope.datafilter);
        data.action ='filter';

        if (reverse_) {
            if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_rev.push(keyname);
            }

            // angular.forEach($scope.sortKeyArr_un_rev, function(val,key) {
            //     if(val == keyname){
            //         $scope.sortKeyArr_un_rev.splice(key,1);
            //         return ;
            //     }
            // });

        }
        else {
            if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_un_rev.push(keyname);
            }
            // angular.forEach($scope.sortKeyArr_rev, function(val,key) {
            //     if(val == keyname){
            //         $scope.sortKeyArr_rev.splice(key,1);
            //         return ;
            //     }
            // });
        }

        data.sortKeyArr_rev = $scope.sortKeyArr_rev;
        data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;

        LoadRelays(data);

    };


    $scope.toggleSearchForm = function() {
        $scope.custom = $scope.custom == false ? true: false;
        $rootScope.datafilter ={page:1,category_type:$rootScope.categoryType};
    };
    $scope.pageChanged = function (CurrentPage) {
        $scope.CurrentPage = CurrentPage;
        $rootScope.datafilter.page = CurrentPage;
        LoadRelays($rootScope.datafilter);
    };
    $scope.itemsPerPage__ = function (itemsCount) {
        $scope.CurrentPage = 1
        $rootScope.datafilter.page = 1;
        $rootScope.itemsCount = itemsCount;
        $rootScope.datafilter.itemsCount = itemsCount;
        LoadRelays($rootScope.datafilter);
    };
    $scope.pageChanged_ = function (CurrentPage) {
        $scope.CurrentPage = CurrentPage;
        $rootScope.datafilter.page = CurrentPage;
        LoadRelays($rootScope.datafilter);
    };
    $scope.load = function (category_type) {
        $scope.custom =false;
        $rootScope.categoryType = category_type;
        $rootScope.datafilter.category_type = category_type;
        $rootScope.datafilter.page = 1;
        if(category_type == 1){
            Entity.get({entity:'entities',c:'countries'},function (response) {
                $scope.Country = response.countries;
            });
        } else{
            Entity.get({entity:'entities',c:'aidCountries'},function (response) {
                $scope.aidCountries = response.aidCountries;
            });
        }
        LoadRelays($rootScope.datafilter);
    };
    $scope.RelaySearch = function (params,action) {
        $rootScope.clearToastr();
        var data = angular.copy(params);

        if( !angular.isUndefined(data.created_at_to)) {
            data.created_at_to=$filter('date')(data.created_at_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.created_at_from)) {
            data.created_at_from= $filter('date')(data.created_at_from, 'dd-MM-yyyy')
        }
        data.action=action;

        if(action == 'xlsx'){
            data.category_type = $rootScope.categoryType;
            $rootScope.progressbar_start();
            $http({
                url: "/api/v1.0/common/Relays/filter",
                method: "POST",
                data: data
            }).then(function (response) {
                $rootScope.progressbar_complete();
                if(response.data.download_token){
                    $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });

        }else{

            data.page=1;
            LoadRelays(data);
        }
    };
    $scope.getLocation = function(entity,parent,value){
        if(!angular.isUndefined(value)) {
            if (value !== null && value !== "" && value !== " ") {
                Entity.listChildren({'entity': entity, 'id': value, 'parent': parent}, function (response) {

                    if (entity == 'sdistricts') {

                        $scope.governarate = response;
                        $scope.regions = [];
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];

                        $scope.datafilter.adsdistrict_id = "";
                        $scope.datafilter.adsregion_id = "";
                        $scope.datafilter.adsneighborhood_id = "";
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";

                    }
                    else if (entity == 'sregions') {

                        $scope.regions = response;
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];
                        $scope.datafilter.adsregion_id = "";
                        $scope.datafilter.adsneighborhood_id = "";
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";
                    }
                    else if (entity == 'sneighborhoods') {
                      $scope.nearlocation = response;
                        $scope.squares = [];
                        $scope.mosques = [];
                        $scope.datafilter.adsneighborhood_id = "";
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";

                    }
                    else if (entity == 'ssquares') {
                       $scope.squares = response;
                        $scope.mosques = [];
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";
                    }
                    else if (entity == 'smosques') {
                       $scope.mosques = response;
                        $scope.datafilter.adsmosques_id = "";
                    }
                    else if (entity == 'districts') {

                        $scope.governarate = response;
                        $scope.city = [];
                        $scope.nearlocation = [];
                        $scope.mosques = [];
                        $scope.datafilter.governarate = "";
                        $scope.datafilter.city = "";
                        $scope.datafilter.location_id = "";

                    }
                    else if (entity == 'cities') {

                        $scope.city = response;
                        $scope.nearlocation = [];
                        $scope.mosques = [];
                        $scope.datafilter.city = "";
                        $scope.datafilter.location_id = "";

                    }
                    else if (entity == 'neighborhoods') {
                        $scope.nearlocation = response;
                        $scope.mosques = [];
                        $scope.datafilter.location_id = "";
                        $scope.datafilter.mosques_id = "";
                    }
                    else if (entity == 'mosques') {

                        $scope.mosques = response;
                        $scope.datafilter.mosques_id = "";
                    }

                });
            }
        }
    };
    $scope._Relays = function (params) {

        $rootScope.clearToastr();
        if( $rootScope.categoryType == 2) {
            if (confirm($filter('translate')("You must confirm the person's address and mobile number before completing the deportation. Do you want to complete the process?"))) {

                $uibModal.open({
                    templateUrl: 'personInfo.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance, $log) {
                        $scope.msg1 = {};
                        $scope.status1='';
                        $rootScope.person_= angular.copy(params);
                        if($rootScope.person_.adscountry_id ==null){
                            $rootScope.person_.adscountry_id     = "";
                        }else {
                            $rootScope.person_.country = $rootScope.person_.adscountry_id+ "";
                            $scope.governarate =  Entity.listChildren({entity: 'sdistricts', 'id': $rootScope.person_.adscountry_id, 'parent': 'scountries'});
                        }
                        if($rootScope.person_.adsdistrict_id ==null){
                            $rootScope.person_.adsdistrict_id = "";
                        }else {
                            $rootScope.person_.adsdistrict_id = $rootScope.person_.adsdistrict_id+ "";
                            $scope.regions = Entity.listChildren({entity: 'sregions', 'id': $rootScope.person_.adsdistrict_id, 'parent': 'sdistricts'});

                        }
                        if($rootScope.person_.adsregion_id ==null || $rootScope.person_.adsregion_id ==" "){
                            $rootScope.person_.adsregion_id = "";
                        }else {
                            $rootScope.person_.adsregion_id = $rootScope.person_.adsregion_id+ "";
                            $scope.nearlocation =  Entity.listChildren({entity: 'sneighborhoods', 'id': $rootScope.person_.adsregion_id, 'parent': 'sregions'});
                        }

                        if($rootScope.person_.adsneighborhood_id ==null || $rootScope.person_.adsneighborhood_id ==" "){
                            $rootScope.person_.adsneighborhood_id = "";
                        }else {
                            $rootScope.person_.adsneighborhood_id = $rootScope.person_.adsneighborhood_id+ "";
                            $scope.squares = Entity.listChildren({entity: 'ssquares', 'id': $rootScope.person_.adsneighborhood_id, 'parent': 'sneighborhoods'});
                        }

                        if($rootScope.person_.adssquare_id ==null || $rootScope.person_.adssquare_id ==" "){
                            $rootScope.person_.adssquare_id = "";
                        }else{
                            $rootScope.person_.adssquare_id = $rootScope.person_.adssquare_id +"";
                            $scope.mosques =  Entity.listChildren({entity: 'smosques', 'id': $rootScope.person_.adssquare_id, 'parent': 'ssquares'});
                        }

                        if($rootScope.person_.adsmosques_id ==null || $rootScope.person_.adsmosques_id ==" "){
                            $rootScope.person_.adsmosques_id = "";
                        }else{
                            $rootScope.person_.adsmosques_id = $rootScope.person_.adsmosques_id +"";
                        }


                        Entity.get({entity:'entities',c:'aidCountries'},function (response) {
                            $scope.aidCountries = response.aidCountries;
                        });

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                        $scope.getLocation = function(entity,parent,value){
                            if(!angular.isUndefined(value)) {
                                if (value !== null && value !== "" && value !== " ") {
                                    Entity.listChildren({'entity': entity, 'id': value, 'parent': parent}, function (response) {
                                        if (entity == 'sdistricts') {
                                            if (!angular.isUndefined($scope.msg1.adscountry_id)) {
                                                $scope.msg1.adscountry_id = [];
                                            }

                                            $scope.governarate = response;
                                            $scope.regions = [];
                                            $scope.nearlocation = [];
                                            $scope.squares = [];
                                            $scope.mosques = [];

                                            $rootScope.person_.adsdistrict_id = "";
                                            $rootScope.person_.adsregion_id = "";
                                            $rootScope.person_.adsneighborhood_id = "";
                                            $rootScope.person_.adssquare_id = "";
                                            $rootScope.person_.adsmosques_id = "";

                                        }
                                        else if (entity == 'sregions') {
                                            if (!angular.isUndefined($scope.msg1.adsdistrict_id)) {
                                                $scope.msg1.adsdistrict_id = [];
                                            }

                                            $scope.regions = response;
                                            $scope.nearlocation = [];
                                            $scope.squares = [];
                                            $scope.mosques = [];
                                            $rootScope.person_.adsregion_id = "";
                                            $rootScope.person_.adsneighborhood_id = "";
                                            $rootScope.person_.adssquare_id = "";
                                            $rootScope.person_.adsmosques_id = "";
                                        }
                                        else if (entity == 'sneighborhoods') {
                                            if (!angular.isUndefined($scope.msg1.adsregion_id)) {
                                                $scope.msg1.adsregion_id = [];
                                            }
                                            $scope.nearlocation = response;
                                            $scope.squares = [];
                                            $scope.mosques = [];
                                            $rootScope.person_.adsneighborhood_id = "";
                                            $rootScope.person_.adssquare_id = "";
                                            $rootScope.person_.adsmosques_id = "";

                                        }
                                        else if (entity == 'ssquares') {
                                            if (!angular.isUndefined($scope.msg1.adssquare_id)) {
                                                $scope.msg1.city = [];
                                            }
                                            $scope.squares = response;
                                            $scope.mosques = [];
                                            $rootScope.person_.adssquare_id = "";
                                            $rootScope.person_.adsmosques_id = "";
                                        }
                                        else if (entity == 'smosques') {
                                            if (!angular.isUndefined($scope.msg1.adssquare_id)) {
                                                $scope.msg1.adssquare_id = [];
                                            }
                                            $scope.mosques = response;
                                            $rootScope.person_.adsmosques_id = "";
                                        }
                                    });
                                }
                            }
                        };
                        $scope.confirm = function (item) {
                            var target = angular.copy(item);
                            $rootScope.clearToastr();
                            target.no_en=true;
                            target.relay=true;
                            target.priority= {location:4};
                            if(target.id){
                                delete target.id;
                            }
                            $rootScope.progressbar_start();

                            persons.save(target,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid') {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else if(response.status== 'success'){
                                    Relays.confirm({'relay_id':params.id,categoryType:$rootScope.categoryType},function (response) {
                                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                                        if(response.status == 'success'){
                                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                            LoadRelays($rootScope.datafilter);
                                            if (confirm($filter('translate')('Status successfully approved, would you like to be redirected to Modify Status Data'))) {
                                                angular.forEach($rootScope.aidCategories, function(v, k) {
                                                    if(v.id ==response.case.category_id){
                                                        $state.go('caseform.'+v.FirstStep,{"mode":'edit',"id" : response.case.id,"category_id" :response.case.category_id});
                                                    }
                                                });
                                            }
                                        }else{
                                            $rootScope.toastrMessages('error',response.msg);
                                        }
                                        $modalInstance.close();
                                    });
                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                            });

                        };

                    }});

            }
        }
        else{
            if (confirm($filter('translate')( "You must enter the guardian data before the person is approved as a sponsorship case. Do you want to complete the process?"))) {

                $uibModal.open({
                    templateUrl: 'guardianInfo.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance, $log) {
                        $rootScope.msg1=[];
                        $scope.msg1 = {};
                        $scope.status1='';
                        $rootScope.person_={};

                        Entity.get({'entity':'entities' , 'c':'kinship,maritalStatus,countries'},function (response){
                            $scope.MaritalStatus = response.maritalStatus;
                            $scope.Country = response.countries;
                            $scope.Kinship = response.kinship;
                        });

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                        $scope.getLocation = function(entity,parent,value){
                            if(!angular.isUndefined(value)) {
                                if (value !== null && value !== "" && value !== " ") {
                                    Entity.listChildren({'entity': entity, 'id': value, 'parent': parent}, function (response) {

                                        if (entity == 'districts') {
                                            if (!angular.isUndefined($rootScope.msg1.country)) {
                                                $rootScope.msg1.country = [];
                                            }

                                            $scope.governarate = response;
                                            $scope.city = [];
                                            $scope.nearlocation =[];
                                            $scope.mosques = [];
                                            $rootScope.person_.governarate = "";
                                            $rootScope.person_.city = "";
                                            $rootScope.person_.location_id = "";

                                        }
                                        else if (entity == 'cities') {
                                            if (!angular.isUndefined($rootScope.msg1.governarate)) {
                                                $rootScope.msg1.governarate = [];
                                            }

                                            $scope.city = response;
                                            $scope.nearlocation = [];
                                            $scope.mosques = [];
                                            $rootScope.person_.city = "";
                                            $rootScope.person_.location_id = "";

                                        }
                                        else if (entity == 'neighborhoods') {
                                            if (!angular.isUndefined($rootScope.msg1.city)) {
                                                $rootScope.msg1.city = [];
                                            }
                                            $scope.nearlocation = response;
                                            $scope.mosques = [];
                                            $rootScope.person_.location_id = "";
                                            $rootScope.person_.mosques_id = "";

                                        } else if(entity =='mosques'){
                                            if( !angular.isUndefined($rootScope.msg1.location_id)) {
                                                $rootScope.msg1.location_id = [];
                                            }

                                            $scope.mosques = response;
                                            $rootScope.person_.mosques_id="";
                                        }


                                    });
                                }
                            }
                        };
                        $scope.isExists = function(id_card_number){
                            $rootScope.clearToastr();
                            $rootScope.has_error=false;
                            if( !angular.isUndefined($scope.msg1.id_card_number)) {
                                $scope.msg1.id_card_number = [];
                            }

                            if(!angular.isUndefined(id_card_number)) {
                                if(id_card_number.length==9) {
                                    if($rootScope.check_id(id_card_number)){
                                        var parameters={'id_card_number':id_card_number, 'mode':'edit','person' :true, 'contacts' : true};
                                    parameters.outer=true;
                                    parameters.category_id=$rootScope.category_id;
                                    parameters.individual_id    = params.person_id ;
                                    $rootScope.progressbar_complete();

                                    persons.find(parameters,function(response){

                                        $rootScope.progressbar_complete();
                                        if(response.status == true) {
                                            $rootScope.toastrMessages('error',response.msg);

                                            $rootScope.person_=response.person;
                                            if($rootScope.person_.country !=null){
                                                $rootScope.person_.country = $rootScope.person_.country+"";
                                                $scope.governarate = Entity.listChildren({entity:'districts','id':$rootScope.person_.country,'parent':'countries'});
                                            }else{
                                                $rootScope.person_.country ="";
                                            }
                                            if($rootScope.person_.governarate !=null){
                                                $rootScope.person_.governarate = $rootScope.person_.governarate+"";
                                                $scope.city = Entity.listChildren({parent:'districts','id':$rootScope.person_.governarate,entity:'cities'});
                                            }else{
                                                $rootScope.person_.governarate ="";
                                            }
                                            if($rootScope.person_.city !=null){
                                                $rootScope.person_.city = $rootScope.person_.city+"";
                                                $scope.nearlocation = Entity.listChildren({entity:'neighborhoods','id':$rootScope.person_.city,'parent':'cities'});
                                            }else{
                                                $rootScope.person_.city ="";
                                            }
                                            if($rootScope.person_.location_id !=null){
                                                $rootScope.person_.location_id = $rootScope.person_.location_id+"";
                                                $scope.mosques = Entity.listChildren({entity:'mosques','id':$rootScope.person_.location_id,'parent':'neighborhoods'});
                                            }else{
                                                $rootScope.person_.location_id ="";
                                            }
                                            if($rootScope.person_.mosques_id !=null){
                                                $rootScope.person_.mosques_id = $rootScope.person_.mosques_id+"";
                                            }else{
                                                $rootScope.person_.mosques_id ="";
                                            }

                                        }
                                        else{
                                            if(response.msg){
                                                $rootScope.toastrMessages('error',response.msg);
                                                $rootScope.person_s = [];
                                            }else{
                                                $rootScope.clearToastr();
                                            }

                                            if(response.person){
                                                $rootScope.person_    = response.person;
                                                if($rootScope.person_.birthday =="0000-00-00" || $rootScope.person_.birthday == null){
                                                    $rootScope.person_.birthday    = new Date();
                                                }else{
                                                    $rootScope.person_.birthday    = new Date($rootScope.person_.birthday);
                                                }
                                            }

                                            $rootScope.person_.id=null;
                                            $rootScope.person_.person_id=null;
                                        }
                                    });
                                    }else{
                                        $rootScope.toastrMessages('error',$filter('translate')('invalid card number'));
                                    }
                                }
                            }
                        };
                        $scope.confirm = function (item) {
                            var target = angular.copy(item);
                            $rootScope.clearToastr();
                            item.individual_id    = params.person_id ;
                            item.priority= {id_card_number:4,kinship_id:4};
                            item.individual_update= true;
                            item.outerEdit = true;
                            item.target    = 'guardian' ;

                            if( !angular.isUndefined(target.birthday)) {
                                target.birthday=$filter('date')(target.birthday, 'dd-MM-yyyy')
                            }

                            target.no_en=true;
                            if(target.id){
                                target.person_id=target.id;
                                delete target.id;
                            }

                            $rootScope.progressbar_start();
                            persons.save(target,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid') {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else if(response.status== 'success'){
                                    Relays.confirm({'relay_id':params.id,categoryType:$rootScope.categoryType},function (response) {
                                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                                        if(response.status == 'success'){
                                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                            LoadRelays($rootScope.datafilter);
                                            if (confirm($filter('translate')('Status migration approved and the provider saved successfully') )) {
                                                $state.go('edit-case',{"id" : response.case.id});
                                            }
                                        }else{
                                            $rootScope.toastrMessages('error',response.msg);
                                        }
                                        $modalInstance.close();
                                    }, function(error) {
                                        $rootScope.toastrMessages('error',error.data.msg);
                                    });
                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                            });

                        };

                    }});

            }
        }

    };
    $scope.selectedAll = function(value){
        if(value){
            $scope.selectAll = true;
        }
        else{
            $scope.selectAll =false;

        }
        if( $rootScope.categoryType == 1){
            angular.forEach($rootScope.sponsorshipsItems, function(val,key) {
                val.check=$scope.selectAll;
            });
        }else{
            angular.forEach($rootScope.aidsItems, function(val,key) {

                if(angular.isUndefined(val.update)) {
                    val.update=false;
                    val.check=false;
                }else {
                    if(val.update == true){
                        val.check=$scope.selectAll;
                    }else{
                        val.check=false;
                    }
                }


            });
        }
    };
    $scope.edit = function (params,$index) {
        $rootScope.updateIndex = $index;
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: 'personInfo.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance, $log) {
                $scope.msg1 = {};
                $scope.status1='';
                $rootScope.person_= angular.copy(params);

                if($rootScope.person_.adscountry_id ==null){
                    $rootScope.person_.adscountry_id     = "";
                }else {
                    $rootScope.person_.country = $rootScope.person_.adscountry_id+ "";
                    $scope.governarate = Entity.listChildren({entity: 'sdistricts', 'id': $rootScope.person_.adscountry_id, 'parent': 'scountries'});
                }
                if($rootScope.person_.adsdistrict_id ==null){
                    $rootScope.person_.adsdistrict_id = "";
                }else {
                    $rootScope.person_.adsdistrict_id = $rootScope.person_.adsdistrict_id+ "";
                    $scope.regions = Entity.listChildren({entity: 'sregions', 'id': $rootScope.person_.adsdistrict_id, 'parent': 'sdistricts'});
                }

                if($rootScope.person_.adsregion_id ==null){
                    $rootScope.person_.adsregion_id = "";
                }else {
                    $rootScope.person_.adsregion_id = $rootScope.person_.adsregion_id+ "";
                    $scope.nearlocation = Entity.listChildren({entity: 'sneighborhoods', 'id': $rootScope.person_.adsregion_id, 'parent': 'sregions'});
                }
                if($rootScope.person_.adsneighborhood_id ==null){
                    $rootScope.person_.adsneighborhood_id = "";
                }else {
                    $rootScope.person_.adsneighborhood_id = $rootScope.person_.adsneighborhood_id+ "";
                    $scope.squares = Entity.listChildren({entity: 'ssquares', 'id': $rootScope.person_.adsneighborhood_id, 'parent': 'sneighborhoods'});

                }

                if($rootScope.person_.adssquare_id ==null){
                    $rootScope.person_.adssquare_id = "";
                }else{
                    $rootScope.person_.adssquare_id = $rootScope.person_.adssquare_id +"";
                    $scope.mosques = Entity.listChildren({entity: 'smosques', 'id': $rootScope.person_.adssquare_id, 'parent': 'ssquares'});
                }

                if($rootScope.person_.adsmosques_id ==null){
                    $rootScope.person_.adsmosques_id = "";
                }else{
                    $rootScope.person_.adsmosques_id = $rootScope.person_.adsmosques_id +"";
                }



                Entity.get({entity:'entities',c:'aidCountries'},function (response) {
                    $scope.aidCountries = response.aidCountries;
                });

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.getLocation = function(entity,parent,value){
                    if(!angular.isUndefined(value)) {
                        if (value !== null && value !== "" && value !== " ") {
                            Entity.listChildren({'entity': entity, 'id': value, 'parent': parent}, function (response) {
                                if (entity == 'sdistricts') {
                                    if (!angular.isUndefined($scope.msg1.adscountry_id)) {
                                        $scope.msg1.adscountry_id = [];
                                    }

                                    $scope.governarate = response;
                                    $scope.regions = [];
                                    $scope.nearlocation = [];
                                    $scope.squares = [];
                                    $scope.mosques = [];

                                    $rootScope.person_.adsdistrict_id = "";
                                    $rootScope.person_.adsregion_id = "";
                                    $rootScope.person_.adsneighborhood_id = "";
                                    $rootScope.person_.adssquare_id = "";
                                    $rootScope.person_.adsmosques_id = "";

                                }
                                else if (entity == 'sregions') {
                                    if (!angular.isUndefined($scope.msg1.adsdistrict_id)) {
                                        $scope.msg1.adsdistrict_id = [];
                                    }

                                    $scope.regions = response;
                                    $scope.nearlocation = [];
                                    $scope.squares = [];
                                    $scope.mosques = [];
                                    $rootScope.person_.adsregion_id = "";
                                    $rootScope.person_.adsneighborhood_id = "";
                                    $rootScope.person_.adssquare_id = "";
                                    $rootScope.person_.adsmosques_id = "";
                                }
                                else if (entity == 'sneighborhoods') {
                                    if (!angular.isUndefined($scope.msg1.adsregion_id)) {
                                        $scope.msg1.adsregion_id = [];
                                    }
                                    $scope.nearlocation = response;
                                    $scope.squares = [];
                                    $scope.mosques = [];
                                    $rootScope.person_.adsneighborhood_id = "";
                                    $rootScope.person_.adssquare_id = "";
                                    $rootScope.person_.adsmosques_id = "";

                                }
                                else if (entity == 'ssquares') {
                                    if (!angular.isUndefined($scope.msg1.adssquare_id)) {
                                        $scope.msg1.city = [];
                                    }
                                    $scope.squares = response;
                                    $scope.mosques = [];
                                    $rootScope.person_.adssquare_id = "";
                                    $rootScope.person_.adsmosques_id = "";
                                }
                                else if (entity == 'smosques') {
                                    if (!angular.isUndefined($scope.msg1.adssquare_id)) {
                                        $scope.msg1.adssquare_id = [];
                                    }
                                    $scope.mosques = response;
                                    $rootScope.person_.adsmosques_id = "";
                                }
                            });
                        }
                    }
                };
                $scope.confirm = function (item) {
                    var target = angular.copy(item);
                    $rootScope.clearToastr();
                    target.no_en=true;
                    target.relay=true;
                    target.priority= {location:4};
                    if(target.id){
                        delete target.id;
                    }
                    $rootScope.progressbar_start();
                    persons.save(target,function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='error' || response.status=='failed')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else if(response.status=='failed_valid') {
                            $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                            $scope.status1 =response.status;
                            $scope.msg1 =response.msg;
                        }
                        else if(response.status== 'success'){
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            $rootScope.aidsItems[$rootScope.updateIndex].update = true;


                            if(!angular.isUndefined($rootScope.person_.adscountry_id)) {
                                $rootScope.aidsItems[$rootScope.updateIndex].adscountry_id = $rootScope.person_.adscountry_id;
                            }

                            if(!angular.isUndefined($rootScope.person_.adsdistrict_id)) {
                                $rootScope.aidsItems[$rootScope.updateIndex].adsdistrict_id = $rootScope.person_.adsdistrict_id;
                            }

                            if(!angular.isUndefined($rootScope.person_.adsregion_id)) {
                                $rootScope.aidsItems[$rootScope.updateIndex].adsregion_id = $rootScope.person_.adsregion_id;
                            }

                            if(!angular.isUndefined($rootScope.person_.adsneighborhood_id)) {
                                $rootScope.aidsItems[$rootScope.updateIndex].adsneighborhood_id = $rootScope.person_.adsneighborhood_id;
                            }

                            if(!angular.isUndefined($rootScope.person_.adssquare_id)) {
                                $rootScope.aidsItems[$rootScope.updateIndex].adssquare_id = $rootScope.person_.adssquare_id;
                            }

                            if(!angular.isUndefined($rootScope.person_.adsmosques_id)) {
                                $rootScope.aidsItems[$rootScope.updateIndex].adsmosques_id = $rootScope.person_.adsmosques_id;
                            }

                            if(!angular.isUndefined($rootScope.person_.street_address)) {
                                $rootScope.aidsItems[$rootScope.updateIndex].street_address = $rootScope.person_.street_address;
                            }

                            if(!angular.isUndefined($rootScope.person_.primary_mobile )) {
                                $rootScope.aidsItems[$rootScope.updateIndex].primary_mobile  = $rootScope.person_.primary_mobile ;
                            }

                            if(!angular.isUndefined($rootScope.person_.secondary_mobile)) {
                                $rootScope.aidsItems[$rootScope.updateIndex].secondary_mobile = $rootScope.person_.secondary_mobile;
                            }

                            if(!angular.isUndefined($rootScope.person_.phone)) {
                                $rootScope.aidsItems[$rootScope.updateIndex].phone = $rootScope.person_.phone;
                            }

                            // LoadRelays($rootScope.datafilter);
                            $modalInstance.close();
                        }
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);
                    });

                };

            }});
    };
    $scope.relayGroup=function(){
        $rootScope.clearToastr();
        var persons=[];

        if( $rootScope.categoryType == 1){
            angular.forEach($rootScope.sponsorshipsItems, function(v, k) {
                if(v.check){
                    persons.push(v.id);
                }
            });
        }else{
            angular.forEach($rootScope.aidsItems, function(v, k) {
                if(v.update && v.check){
                    persons.push(v.id);
                }
            });
        }
        if(persons.length==0){
            $rootScope.toastrMessages('error',$filter('translate')('You have not specified any migration status'));
        }else{
            $rootScope.progressbar_start();
            Relays.confirmGroup({'persons':persons,categoryType:$rootScope.categoryType},function (response) {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                if(response.status == 'success'){
                    $rootScope.toastrMessages('success',response.msg);
                }else{
                    $rootScope.toastrMessages('error',response.msg);
                }

                if(response.download_token){
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }
                LoadRelays($rootScope.datafilter);

            });
        }


    }

    $scope.relayUsingExcel = function(type){
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: 'relyPerson_.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'md',
            controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                $scope.row={category_id :""};
                $scope.upload=false;
                $scope.typeName='aids';

                if(type ==1){
                    $scope.typeName='sponsorships';
                    $scope.choices=$rootScope.sponsorCategories;
                } else{
                    $scope.choices=$rootScope.aidCategories;
                }

                $scope.fileUpload=function(){
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.upload=true;
                };

                $scope.download=function(){
                    angular.element('.btn').addClass("disabled");

                    $rootScope.progressbar_start();
                    $rootScope.clearToastr();
                    $http({
                        url:'/api/v1.0/common/Relays/Confirm/'+$scope.typeName+'/template',
                        // url:'/api/v1.0/common/Relays/'+$scope.typeName+'/template',
                        method: "GET"
                    }).then(function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token+'&template=true';
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                };

                var Uploader = $scope.uploader = new FileUploader({
                    url:'/api/v1.0/common/Relays/relayFromExcel',
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });



                $scope.confirm = function (row_) {
                    $rootScope.clearToastr();
                    $scope.spinner=false;
                    $scope.import=false;
                    angular.element('.btn').addClass("disabled");
                    $rootScope.progressbar_start();

                    Uploader.onBeforeUploadItem = function(item) {
                        angular.element('.btn').addClass("disabled");
                        $rootScope.clearToastr();
                        item.formData = [{source: 'file',category_id:row_.category_id,category_type:type,save:'save'}];
                    };

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(response.status=='success')
                        {
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $scope.spinner=false;
                            $scope.import=false;
                            $rootScope.toastrMessages('success',response.msg);
                        }else{
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $scope.remove();
                            $rootScope.toastrMessages('error',response.msg);
                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                        }
                        if(response.download_token){
                            var file_type='xlsx';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }
                        $modalInstance.close();
                    };
                    Uploader.uploadAll();
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.remove=function(){
                    angular.element("input[type='file']").val(null);
                    $scope.uploader.clearQueue();
                    angular.forEach(
                        angular.element("input[type='file']"),
                        function(inputElem) {
                            angular.element(inputElem).val(null);
                        });

                };

            }});
    };

    $scope.cancel = function(entry) {
        entry.name = $scope.oldEntry.name;
        entry.weight = $scope.oldEntry.weight;
        if ($scope.entitySettings.hasRatio) {
            entry.ratio =$scope.oldEntry.ratio;
        }
        entry.edit = false;
        $scope.inProgress = false;
    };
    $scope.save = function(entry) {
        $rootScope.clearToastr();
        entry.entity = $stateParams.model;
        if ($id && $action) {
            entry.parent_id = $id;
            entry.action = $action;
        }

        if (!$scope.entitySettings.hasRatio) {
            delete  entry.ratio;
        }

        $rootScope.progressbar_start();
        Entity.update(entry, function () {
            $rootScope.progressbar_complete();
            entry.edit = false;
            $rootScope.toastrMessages('success',$filter('translate')('report saved')+'"'+entry.name+'"');
            $scope.inProgress = false;
        }, function (response) {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('error',angular.isArray(response.data)? response.data.join('<br>') : JSON.stringify(response.data));
        });
    };

    // $scope.load($rootScope.categoryType);

    $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
    $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.today = function() { $scope.dt = new Date();};
    $scope.today();
    $scope.clear = function() {$scope.dt = null;};
    $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
    $scope.popup99 = {opened: false};
    $scope.open88 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup88.opened = true;};
    $scope.popup88 = {opened: false};
});



angular.module('CommonModule').
controller('confirmRelayController', function (Relays, $stateParams,$rootScope, $http,$filter, $scope,$state,$uibModal,Entity) {

    $rootScope.type=$scope.type=$stateParams.type == 'aids'? 'aids' : 'sponsorships';
    $rootScope.categoryType=$scope.categoryType=$stateParams.type == 'aids'? 2 : 1;
    $state.current.data.pageTitle = $filter('translate')('Confirmed Relays');
    $rootScope.clearToastr();
    $scope.CurrentPage=1;
    $scope.CurrentPage_=1;
    $rootScope.aidsItems=[];
    $rootScope.sponsorshipsItems=[];
    $rootScope.datafilter ={page:1,category_type:$rootScope.categoryType};
    $scope.custom =false;
    $scope.sortKeyArr=[];
    $scope.sortKeyArr_rev=[];
    $scope.sortKeyArr_un_rev=[];

    var LoadConfirmRelays = function ($params) {
        $rootScope.aidsItems=[];
        $rootScope.sponsorshipsItems=[];
        $params.category_type = $rootScope.categoryType;
        $params.action='paginate';
        $params.page=$scope.CurrentPage;
        $rootScope.progressbar_start();
        Relays.confirmFilter($params,function (response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');

            if( $rootScope.categoryType == 1){
                $rootScope.sponsorshipsItems = response.data;
                $scope.CurrentPage_ = response.current_page;
                $rootScope.TotalItems_ = response.total;
                $rootScope.ItemsPerPage_ = response.per_page;
            }else{
                $rootScope.aidsItems = response.data;
                $scope.CurrentPage = response.current_page;
                $rootScope.TotalItems = response.total;
                $rootScope.ItemsPerPage = response.per_page;
            }
        });
    };

    $rootScope.aidCategories_=[];
    angular.forEach($rootScope.aidCategories, function(v, k) {
        if ($rootScope.lang == 'ar'){
            $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
        }
        else{
            $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
        }
    });

    $rootScope.sponsorCategories_=[];
    angular.forEach($rootScope.sponsorCategories, function(v, k) {
        if ($rootScope.lang == 'ar'){
            $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
        }
        else{
            $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
        }
    });

    $scope.goTo=function(action,person,case_id,category_id){
        $rootScope.clearToastr();
        angular.forEach($rootScope.aidCategories, function(v, k) {
            if(v.id ==category_id){
                $state.go('caseform.'+v.FirstStep,{"mode":action,"id" :case_id,"category_id" :category_id});
            }
        });

    };
    $scope.toggleSearchForm = function() {
        $scope.custom = $scope.custom == false ? true: false;
        $rootScope.datafilter ={page:1,category_type:$rootScope.categoryType};
    };
    $scope.pageChanged = function (CurrentPage) {
        $scope.CurrentPage = CurrentPage;
        $rootScope.datafilter.page = CurrentPage;
        LoadConfirmRelays($rootScope.datafilter);
    };
    $scope.pageChanged_ = function (CurrentPage) {
        $scope.CurrentPage = CurrentPage;
        $rootScope.datafilter.page = CurrentPage;
        LoadConfirmRelays($rootScope.datafilter);
    };

    $scope.itemsPerPage__ = function (itemsCount) {
        $scope.CurrentPage = 1
        $rootScope.datafilter.page = 1;
        $rootScope.itemsCount = itemsCount;
        $rootScope.datafilter.itemsCount = itemsCount;
        LoadConfirmRelays($rootScope.datafilter);
    };

    $scope.load = function (category_type) {
        $scope.custom =false;
        $rootScope.categoryType = category_type;
        $rootScope.datafilter.category_type = category_type;
        $rootScope.datafilter.page = 1;
        if(category_type == 1){
            Entity.get({entity:'entities',c:'countries'},function (response) {
                $scope.Country = response.countries;
            });
        } else{
            Entity.get({entity:'entities',c:'aidCountries'},function (response) {
                $scope.aidCountries = response.aidCountries;
            });
        }
        LoadConfirmRelays($rootScope.datafilter);
    };
    $scope.RelaySearch = function (params,action) {
        $rootScope.clearToastr();

        var data = angular.copy(params);

        if( !angular.isUndefined(data.created_at_to)) {
            data.created_at_to=$filter('date')(data.created_at_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.created_at_from)) {
            data.created_at_from= $filter('date')(data.created_at_from, 'dd-MM-yyyy')
        }
        data.action=action;
        if(action == 'xlsx'){
            data.category_type = $rootScope.categoryType;
            $rootScope.progressbar_start();
            $http({
                url: "/api/v1.0/common/Relays/confirmFilter",
                method: "POST",
                data: data
            }).then(function (response) {
                $rootScope.progressbar_complete();
                if(response.data.download_token){
                    $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });
        }else{

            data.page=1;
            LoadConfirmRelays(data);
        }
    };
    $scope.getLocation = function(entity,parent,value){
        if(!angular.isUndefined(value)) {
            if (value !== null && value !== "" && value !== " ") {
                Entity.listChildren({'entity': entity, 'id': value, 'parent': parent}, function (response) {

                    if (entity == 'sdistricts') {

                        $scope.governarate = response;
                        $scope.regions = [];
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];

                        $scope.datafilter.adsdistrict_id = "";
                        $scope.datafilter.adsregion_id = "";
                        $scope.datafilter.adsneighborhood_id = "";
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";

                    }
                    else if (entity == 'sregions') {

                        $scope.regions = response;
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];
                        $scope.datafilter.adsregion_id = "";
                        $scope.datafilter.adsneighborhood_id = "";
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";
                    }
                    else if (entity == 'sneighborhoods') {
                        $scope.nearlocation = response;
                        $scope.squares = [];
                        $scope.mosques = [];
                        $scope.datafilter.adsneighborhood_id = "";
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";

                    }
                    else if (entity == 'ssquares') {
                        $scope.squares = response;
                        $scope.mosques = [];
                        $scope.datafilter.adssquare_id = "";
                        $scope.datafilter.adsmosques_id = "";
                    }
                    else if (entity == 'smosques') {
                        $scope.mosques = response;
                        $scope.datafilter.adsmosques_id = "";
                    }
                    else if (entity == 'districts') {

                        $scope.governarate = response;
                        $scope.city = [];
                        $scope.nearlocation = [];
                        $scope.mosques = [];
                        $scope.datafilter.governarate = "";
                        $scope.datafilter.city = "";
                        $scope.datafilter.location_id = "";

                    }
                    else if (entity == 'cities') {

                        $scope.city = response;
                        $scope.nearlocation = [];
                        $scope.mosques = [];
                        $scope.datafilter.city = "";
                        $scope.datafilter.location_id = "";

                    }
                    else if (entity == 'neighborhoods') {
                        $scope.nearlocation = response;
                        $scope.mosques = [];
                        $scope.datafilter.location_id = "";
                        $scope.datafilter.mosques_id = "";
                    }
                    else if (entity == 'mosques') {

                        $scope.mosques = response;
                        $scope.datafilter.mosques_id = "";
                    }

                });
            }
        }
    };
    // LoadConfirmRelays($rootScope.datafilter);
    // $scope.load( $rootScope.categoryType);

    $scope.sortKeyArr=[];
    $scope.sortKeyArr_rev=[];
    $scope.sortKeyArr_un_rev=[];
    $scope.sort_ = function(keyname){
        $scope.sortKey_ = keyname;
        var reverse_ = false;

        if (
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
        ) {
            reverse_ = true;
        }

        var data = angular.copy($rootScope.datafilter);
        data.action ='filter';

        if (reverse_) {
            if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_rev.push(keyname);
            }

            // angular.forEach($scope.sortKeyArr_un_rev, function(val,key) {
            //     if(val == keyname){
            //         $scope.sortKeyArr_un_rev.splice(key,1);
            //         return ;
            //     }
            // });

        }
        else {
            if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_un_rev.push(keyname);
            }
            // angular.forEach($scope.sortKeyArr_rev, function(val,key) {
            //     if(val == keyname){
            //         $scope.sortKeyArr_rev.splice(key,1);
            //         return ;
            //     }
            // });
        }

        data.sortKeyArr_rev = $scope.sortKeyArr_rev;
        data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;

        LoadConfirmRelays(data);

    };

    $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
    $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.today = function() { $scope.dt = new Date();};
    $scope.today();
    $scope.clear = function() {$scope.dt = null;};
    $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
    $scope.popup99 = {opened: false};
    $scope.open88 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup88.opened = true;};
    $scope.popup88 = {opened: false};

});