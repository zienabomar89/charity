
angular.module('CommonModule').controller('searchAllController', function($stateParams,$rootScope,$scope,$timeout,$uibModal,Entity,$state,reportsService,$filter,$http,FileUploader, OAuthToken,Relays,Log) {

    $rootScope.clearToastr();
    $rootScope.ShowCitizenImage=false;
    $rootScope.setAuthUser();
    $scope.selected =false;
    $scope.checkCards = false ;
    $scope.items = [] ;

    $scope.filter={type:'searchAll',using:"1",first_name:"",second_name:"",third_name:"",last_name:"",id_card_number:"", action:'paginate',
                   citizen :true ,social:false, government:false,commercial:false, health:false, aids_committee:false};

    $scope.display={social:false, government:false,commercial:false, health:false, aids_committee:false};
    $state.current.data.pageTitle = $filter('translate')('Search ON All');

    $scope.searchFilter = function (params,action) {

        if($scope.filter.citizen == false && $scope.filter.social == false){
            $rootScope.toastrMessages('error',  $filter('translate')('You must select at least one option'));
            return ;
        }

        if(params.using != 1){
            params.id_card_number = "";
        }else{
            params.first_name = "";
            params.second_name = "";
            params.third_name = "";
            params.last_name = "";
        }

        var pass = 1 ;

        if(params.using == 1 && !params.id_card_number){
            $rootScope.toastrMessages('error',$filter('translate')('You should enter card number to search'));
            return ;
        }

        if( params.using != 1 &&(!params.first_name || !params.last_name) ) {
            $rootScope.toastrMessages('error',$filter('translate')('You should enter first and last name to search'));
            return ;
        }

        if(params.using == 1 && !angular.isUndefined(params.id_card_number)) {
            if(params.id_card_number.length != 9) {
                $rootScope.toastrMessages('error',$filter('translate')('card number length must not exceed 9'));
                return ;
            }
        }

        $rootScope.clearToastr();
        if(pass){
            $scope.items = [];
            params.action = action;
            angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();
            $scope.display={social:false, government:false,commercial:false, health:false, aids_committee:false};
            reportsService.searchFilter(params,function (response) {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                if(response.status == 'true' || response.status == true){
                    $scope.items = response.items;
                    $scope.display = response.display;
                }else{
                    $rootScope.toastrMessages('error',response.msg);
                }
            });
        }
    };

    var resetScopeCards = function () {
        $scope.cards=  Log.getCardsByUser();
    };

    $scope.resetScopeOnChange = function(){

        $scope.items = [];
        if($scope.filter.using != 1){
            $scope.filter.id_card_number = "";
        }else{
            $scope.filter.first_name = "";
            $scope.filter.second_name = "";
            $scope.filter.third_name = "";
            $scope.filter.last_name = "";
            resetScopeCards();
        }
    };

    $scope.getCard = function(){
        $scope.checkCards = !$scope.checkCards ;
        resetScopeCards();
    };

    $scope.selected = function(id_card_number){
        $scope.checkCards = !$scope.checkCards ;
        $scope.filter.id_card_number = id_card_number ;

    };

    $scope.check = function(type){
        $rootScope.clearToastr();

        var templateUrl ='checkPerson.html';

        if(type == 0){
            templateUrl='updateData.html'
        }

        $uibModal.open({
            templateUrl: '/app/modules/common/views/reports/searchAll/model/'+templateUrl,
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'md',
            controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {
                $scope.upload=false;

                $scope.fileUpload=function(){
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.upload=true;
                };

                $scope.download=function(){
                    angular.element('.btn').addClass("disabled");

                    $rootScope.clearToastr();
                    $rootScope.progressbar_start();
                    $http({
                        url:'/api/v1.0/common/reports/template',
                        method: "GET"
                    }).then(function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token+'&template=true';
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                };

                var Uploader = $scope.uploader = new FileUploader({
                    url:'/api/v1.0/common/reports/searchAll/check',
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });

                Uploader.onBeforeUploadItem = function(item) {
                    $rootScope.clearToastr();
                    item.formData = [{source: 'file',type:type}];
                };

                $scope.confirm = function () {
                    $rootScope.progressbar_start();
                    $rootScope.clearToastr();
                    $scope.spinner=false;
                    $scope.import=false;
                    angular.element('.btn').addClass("disabled");

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(response.status=='success')
                        {
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $scope.spinner=false;
                            $scope.import=false;
                            $rootScope.toastrMessages('success',response.msg);
                        }else{
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $rootScope.toastrMessages('error',response.msg);
                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                        }
                        if(response.download_token){
                            var file_type='xlsx';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }
                        $modalInstance.close();

                    };
                    Uploader.uploadAll();
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.remove=function(){
                    angular.element("input[type='file']").val(null);
                    $scope.uploader.clearQueue();
                    angular.forEach(
                        angular.element("input[type='file']"),
                        function(inputElem) {
                            angular.element(inputElem).val(null);
                        });
                };

            }});
    };

    $scope.relay = function(type){
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: '/app/modules/common/views/reports/model/relyPerson.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'md',
            controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                $scope.row={category_id :""};
                $scope.upload=false;
                $scope.typeName='aids';

                if(type ==1){
                    $scope.typeName='sponsorships';
                    $scope.choices=$rootScope.sponsorCategories;
                } else{
                    $scope.choices=$rootScope.aidCategories;
                }

                $scope.fileUpload=function(){
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.upload=true;
                };

                $scope.download=function(){
                    angular.element('.btn').addClass("disabled");

                    $rootScope.progressbar_start();
                    $rootScope.clearToastr();
                    $http({
                        url:'/api/v1.0/common/Relays/'+$scope.typeName+'/template',
                        method: "GET"
                    }).then(function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token+'&template=true';
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                };

                var Uploader = $scope.uploader = new FileUploader({
                    url:'/api/v1.0/common/Relays/relayFromExcel',
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });



                $scope.confirm = function (row_) {
                    $rootScope.clearToastr();
                    $rootScope.progressbar_start();
                    $scope.spinner=false;
                    $scope.import=false;
                    angular.element('.btn').addClass("disabled");

                    Uploader.onBeforeUploadItem = function(item) {
                        angular.element('.btn').addClass("disabled");
                        $rootScope.clearToastr();
                        item.formData = [{source: 'file',category_id:row_.category_id,category_type:type,save:"not_save"}];
                    };

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(response.status=='success')
                        {
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $scope.spinner=false;
                            $scope.import=false;
                            $rootScope.toastrMessages('success',response.msg);
                        }else{
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $scope.remove();
                            $rootScope.toastrMessages('error',response.msg);
                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                        }
                        if(response.download_token){
                            var file_type='xlsx';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }
                        $modalInstance.close();
                    };
                    Uploader.uploadAll();
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.remove=function(){
                    angular.element("input[type='file']").val(null);
                    $scope.uploader.clearQueue();
                    angular.forEach(
                        angular.element("input[type='file']"),
                        function(inputElem) {
                            angular.element(inputElem).val(null);
                        });

                };

            }});
    };

    $scope.export = function(action,id_card_number){

        $rootScope.clearToastr();

        angular.element('.btn').addClass("disabled");
        var  file_type = 'zip';
        if(action =='xlsx'){
            file_type = 'xlsx';
        }

        $rootScope.progressbar_start();
        reportsService.filter({using:1,id_card_number:id_card_number,action:action,type:"searchAll"},function (response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if(response.download_token){
                $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
            }else{
                $rootScope.toastrMessages('error',$filter('translate')('action failed'));
            }
        });

    };

});

angular.module('CommonModule')
    .controller('searchAllDetailsController', function($stateParams,$rootScope,$scope,$timeout,$uibModal,Entity,$state,reportsService,$filter,$http,FileUploader, OAuthToken,Relays,Log,persons) {

        $rootScope.clearToastr();
        $rootScope.ShowCitizenImage=false;
        $rootScope.setAuthUser();
        $scope.selected =false;
        $scope.filter={using:1,id_card_number:$stateParams.id,action:'paginate'};
        $scope.name = null;
        $state.current.data.pageTitle = $filter('translate')('Search ON All');


        $rootScope.tabSortData = { childrens : {orderBy : 'AGE' , reverseSort :true} ,
            parent : {orderBy : 'AGE' , reverseSort :true} ,
            wives : {orderBy : 'AGE' , reverseSort :true} ,
            travel : {orderBy : 'CTZN_TRANS_DT' , reverseSort :true} ,
            marriage : {orderBy : 'CONTRACT_DT' , reverseSort :true} ,
            divorce : {orderBy : 'CONTRACT_DT' , reverseSort :true} ,
            health :{orderBy : 'MR_CREATED_ON' , reverseSort :true} ,
            vehicles :{orderBy : 'MODEL_YEAR' , reverseSort :true} ,
            employment :{orderBy : 'JOB_START_DT' , reverseSort :true} ,
            commercial :{orderBy : 'START_DATE' , reverseSort :true} ,
            properties :{orderBy : 'DOC_NO' , reverseSort :true} ,
            reg48_license : {orderBy : 'DATE_FROM' , reverseSort :true} ,
            social_affairs_receipt :{orderBy : 'RECP_DELV_DT' , reverseSort :true} ,

        };

        if(!$stateParams.id){
            $rootScope.toastrMessages('error',$filter('translate')('no card number to display data'));
            return;
        }
        else{
            $rootScope.loadingCard = false;

            var Filters= function (target) {
                $rootScope.loadingCard = false;
                $rootScope.clearToastr();
                $rootScope.row= {IS_DEAD_CD:null,CTZN_STATUS:null,photo_url:null,parent:[],wives:[],childrens:[]};
                $scope.filter.target=target;
                $scope.filter.using=1;
                $scope.filter.action='paginate';
                $scope.filter.type='searchAll';
                $scope.error=false;
                $scope.noData=true;
                $scope.msg ='';

                if( !angular.isUndefined($scope.filter.id_card_number)) {
                    if($scope.filter.id_card_number.length > 0 && $scope.filter.id_card_number.length < 9) {
                        $scope.success = false;
                        $rootScope.toastrMessages('error',$filter('translate')('card number length must not exceed 9'));
                    }else {
                        if($scope.filter.id_card_number.length ==9) {
                            angular.element('.btn').addClass("disabled");
                            $rootScope.progressbar_start();
                            reportsService.filter($scope.filter,function (response) {
                                $rootScope.progressbar_complete();
                                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                                if(response.status){
                                    $scope.noData=false;
                                    $rootScope.loadingCard = true;
                                    $scope.success = true;
                                    response.data.CI_ADDRESS = response.data.CI_REGION + ' - ' + response.data.CI_CITY  + ' - ' + response.data.STREET_ARB;
                                    $rootScope.row= response.data;
                                    $scope.name = response.data.FNAME_ARB + " " +  response.data.SNAME_ARB + " " + response.data.TNAME_ARB + ' ' +  response.data.LNAME_ARB;

                                    if (!($rootScope.row.photo_url == null || $rootScope.row.photo_url == 'null')){
                                        $rootScope.ShowCitizenImage=true;
                                    }else{
                                        $rootScope.ShowCitizenImage=true;
                                        $rootScope.row.photo_url = $rootScope.settings.assetsPath+'/empty-user.png';
                                        // $rootScope.ShowCitizenImage=false;
                                    }

                                }else{
                                    $scope.noData=true;
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            });
                        }
                    }
                }
            };

            var resetScope = function () {
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                $scope.success=false;
                $scope.error=false;
                $scope.msg ='';
                $scope.filter.type='searchAll';
                $rootScope.row= {photo_url:null,parent:[],wives:[],childrens:[]};
            };

            $scope.load = function(target){
                if(target == 'basic'){
                    $scope.success = false;
                }

                Filters(target);
            };

            $rootScope.tabData = { Justice : false , Health : false ,
                Transportation : false , Communications : false ,
                Finance : false , Economy : false ,
                Land : false , Labor : false , Social : false };

            $rootScope.resetTabData = function(target){
                if (target  == 'Justice') {
                    $rootScope.row.marriage = [];
                    $rootScope.row.divorce = [];
                }
                else if(target == 'Health')  {
                    $rootScope.row.health_insurance_data = null;
                    $rootScope.row.health = [];
                }
                else if(target == 'Transportation') {
                    $rootScope.row.vehicles = [];
                }
                else if(target == 'Communications') {
                    $rootScope.row.MOBILE=null;
                    $rootScope.row.TEL=null;
                    $rootScope.row.GOV_NAME=null;
                    $rootScope.row.CITY_NAME=null;
                    $rootScope.row.PART_NAME=null;
                    $rootScope.row.ADDRESS_DET=null;
                }
                else if(target == 'Finance') {
                    $rootScope.row.employment = [];
                }
                else if(target == 'Economy') {
                    $rootScope.row.commercial_data = [];
                }
                else if(target == 'Land') {
                    $rootScope.row.properties = [];
                }
                else if(target == 'Labor') {
                    $rootScope.row.reg48_license = [];
                }
                else if(target == 'Social') {
                    $rootScope.row.social_affairs_status = $filter('translate')('un_beneficiary');
                    $rootScope.row.social_affairs = null;
                    $rootScope.row.social_affairs_receipt = [];
                }
            }
            $rootScope.setTabData = function(data,target){


                if (target  == 'Justice') {
                    $rootScope.row.marriage = data.marriage;
                    $rootScope.row.divorce = data.divorce;
                }
                else if(target == 'Health')  {
                    $rootScope.row.health_insurance_data = data.health_insurance_data;
                    $rootScope.row.health = data.health;
                }
                else if(target == 'Transportation') {
                    $rootScope.row.vehicles = data.vehicles;
                }
                else if(target == 'Communications') {
                    $rootScope.row.MOBILE=data.MOBILE;
                    $rootScope.row.TEL=data.TEL;
                    $rootScope.row.GOV_NAME=data.GOV_NAME;
                    $rootScope.row.CITY_NAME=data.CITY_NAME;
                    $rootScope.row.PART_NAME=data.PART_NAME;
                    $rootScope.row.ADDRESS_DET=data.ADDRESS_DET;
                }
                else if(target == 'Finance') {
                    $rootScope.row.employment = data.employment;
                }
                else if(target == 'Economy') {
                    $rootScope.row.commercial_data = data.commercial_data;
                }
                else if(target == 'Land') {
                    $rootScope.row.properties = data.properties;
                }
                else if(target == 'Labor') {
                    $rootScope.row.reg48_license = data.reg48_license;
                }
                else if(target == 'Social') {
                    $rootScope.row.social_affairs = data.social_affairs;
                    $rootScope.row.social_affairs_status = data.social_affairs_status;
                    $rootScope.row.social_affairs_receipt = data.social_affairs_receipt;
                }
            }

            $rootScope.reloadContent = false;
            $scope.getTabContent = function(target,forceUpdate){
                if (target != 'basic' && $rootScope.tabData[target]  == false && forceUpdate  == false){
                    $rootScope.resetTabData(target);
                    $rootScope.progressbar_start();
                    $rootScope.reloadContent = true;
                    reportsService.tabContent({id_card_number:$stateParams.id,action:target},function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled");
                        angular.element('.btn').removeAttr('disabled');
                        $rootScope.reloadContent = false;
                        if(response.status){
                            $scope.success = true;
                            $rootScope.tabData[target] = true ;
                            $rootScope.setTabData(response.data,target);
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                }
                else{
                    $rootScope.reloadContent = false;
                }
            };

            $scope.export = function(action,card){
                angular.element('.btn').addClass("disabled");
                $rootScope.clearToastr();

                let filter= {type:'searchAll' ,using:1,id_card_number:card,action:action};
                var  file_type = 'zip';
                if(action =='xlsx'){
                    file_type = 'xlsx';
                }
                $rootScope.progressbar_start();
                reportsService.filter(filter,function (response) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                    if(response.download_token){
                        $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('action failed'));

                    }
                });
            };
            $scope.relay_ = function(type){
                // type 1 sponsorship
                // type 2 aid

                $uibModal.open({
                    templateUrl: '/app/modules/common/views/reports/searchAll/model/relyOne.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {
                        $scope.category_id=[];
                        $scope.persons={};
                        $scope.msg1=[];
                        $scope.type=type;

                        if(type ==1){
                            $scope.choices=$rootScope.sponsorCategories;
                            Entity.get({entity:'entities',c:'countries'},function (response) {
                                $scope.Country = response.countries;
                            });
                        } else{
                            $scope.choices=$rootScope.aidCategories;
                            Entity.get({entity:'entities',c:'aidCountries'},function (response) {
                                $scope.aidCountries = response.aidCountries;
                            });
                        }
                        $scope.confirm = function (item) {

                            angular.element('.btn').addClass("disabled");
                            var inputs = angular.copy(item);
                            var params={type:type,reMap:$rootScope.row,inputs:inputs};
                            params.categories = [];
                            angular.forEach(inputs.category_id, function(v, k) {
                                params.categories.push(v.id);
                            });

                            delete inputs.category_id;
                            $rootScope.progressbar_start();
                            Relays.relayOne(params,function (response) {
                                $rootScope.progressbar_complete();
                                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                                if(response.status=='error' || response.status=='failed') {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid') {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    if(response.status=='success')
                                    {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    $modalInstance.close();
                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();
                            });
                        }
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                        $scope.getLocation = function(entity,parent,value){
                            if(!angular.isUndefined(value)) {
                                if (value !== null && value !== "" && value !== " ") {
                                    Entity.listChildren({'entity': entity, 'id': value, 'parent': parent}, function (response) {

                                        if (entity == 'sdistricts') {
                                            if (!angular.isUndefined($scope.msg1.adscountry_id)) {
                                                $scope.msg1.adscountry_id = [];
                                            }

                                            $scope.governarate = response;
                                            $scope.regions = [];
                                            $scope.nearlocation = [];
                                            $scope.squares = [];
                                            $scope.mosques = [];

                                            $scope.persons.adsdistrict_id = "";
                                            $scope.persons.adsregion_id = "";
                                            $scope.persons.adsneighborhood_id = "";
                                            $scope.persons.adssquare_id = "";
                                            $scope.persons.adsmosques_id = "";

                                        }
                                        else if (entity == 'sregions') {
                                            if (!angular.isUndefined($scope.msg1.adsdistrict_id)) {
                                                $scope.msg1.adsdistrict_id = [];
                                            }

                                            $scope.regions = response;
                                            $scope.nearlocation = [];
                                            $scope.squares = [];
                                            $scope.mosques = [];
                                            $scope.persons.adsregion_id = "";
                                            $scope.persons.adsneighborhood_id = "";
                                            $scope.persons.adssquare_id = "";
                                            $scope.persons.adsmosques_id = "";
                                        }
                                        else if (entity == 'sneighborhoods') {
                                            if (!angular.isUndefined($scope.msg1.adsregion_id)) {
                                                $scope.msg1.adsregion_id = [];
                                            }
                                            $scope.nearlocation = response;
                                            $scope.squares = [];
                                            $scope.mosques = [];
                                            $scope.persons.adsneighborhood_id = "";
                                            $scope.persons.adssquare_id = "";
                                            $scope.persons.adsmosques_id = "";

                                        }
                                        else if (entity == 'ssquares') {
                                            if (!angular.isUndefined($scope.msg1.adssquare_id)) {
                                                $scope.msg1.city = [];
                                            }
                                            $scope.squares = response;
                                            $scope.mosques = [];
                                            $scope.persons.adssquare_id = "";
                                            $scope.persons.adsmosques_id = "";
                                        }
                                        else if (entity == 'smosques') {
                                            if (!angular.isUndefined($scope.msg1.adssquare_id)) {
                                                $scope.msg1.adssquare_id = [];
                                            }
                                            $scope.mosques = response;
                                            $scope.persons.adsmosques_id = "";
                                        }
                                        else if (entity == 'districts') {
                                            if (!angular.isUndefined($scope.msg1.country)) {
                                                $scope.msg1.country = [];
                                            }

                                            $scope.governarate = response;
                                            $scope.city = [];
                                            $scope.nearlocation = [];
                                            $scope.mosques = [];
                                            $scope.persons.governarate = "";
                                            $scope.persons.city = "";
                                            $scope.persons.location_id = "";

                                        }
                                        else if (entity == 'cities') {
                                            if (!angular.isUndefined($scope.msg1.governarate)) {
                                                $scope.msg1.governarate = [];
                                            }

                                            $scope.city = response;
                                            $scope.nearlocation = [];
                                            $scope.mosques = [];
                                            $scope.persons.city = "";
                                            $scope.persons.location_id = "";

                                        }
                                        else if (entity == 'neighborhoods') {
                                            if (!angular.isUndefined($scope.msg1.city)) {
                                                $scope.msg1.city = [];
                                            }
                                            $scope.nearlocation = response;
                                            $scope.mosques = [];
                                            $scope.persons.location_id = "";
                                            $scope.persons.mosques_id = "";
                                        }
                                        else if (entity == 'mosques') {
                                            if (!angular.isUndefined($scope.msg1.location_id)) {
                                                $scope.msg1.location_id = [];
                                            }
                                            $scope.mosques = response;
                                            $scope.persons.mosques_id = "";
                                        }

                                    });
                                }
                            }
                        };


                    }});

            };
            $scope.health_= function (x) {
                $uibModal.open({
                    templateUrl: '/app/modules/common/views/reports/model/health.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance) {
                        $scope.row_=x;
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    }});

            }

            $scope.cases= function (x) {
                $uibModal.open({
                    templateUrl: '/app/modules/common/views/reports/model/cases.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance) {
                        $scope.cases=x;
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    }});

            }

            $scope.vFilter={
                "vouchers_code":[],
                "codes":[],
                "using":1,
                "id_card_number":"",
                "receipt_date_from":"",
                "receipt_date_to":"",
                "action":'paginate',
                "page":1,
                "all_organization":true,
                "type":"",
                "voucher_source":"",
                "category_id":"",
                "case_category_id":"",
                "sponsor_id":"",
                "title":"",
                "content":"",
                "notes":"",
                "min_value":"",
                "max_value":"",
                "min_count":"",
                "transfer":"",
                "allow_day":"",
                "transfer_company_id":null,
                "max_count":"",
                "date_to":"",
                "date_from":"",
                "voucher_date_to":"",
                "voucher_date_from":""
            };
            $scope.total = 0;
            $scope.vouchers = [];
            $scope.getVouchers= function (card) {
                $scope.vFilter.id_card_number = card;

                $scope.vFilter.using=1;
                $scope.vFilter.action='paginate';
                $scope.error=false;
                $scope.msg ='';
                angular.element('.btn').addClass("disabled");
                $rootScope.progressbar_start();
                persons.voucherFilter($scope.vFilter,function (response) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                    if(response.success){
                        $scope.total = response.total;
                        $scope.vouchers = response.data;
                        $scope.success = true;
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                });
                //         }
                //     }
                // }


            }
            $scope.exportVouchers = function(params){
                angular.element('.btn').addClass("disabled");
                $rootScope.clearToastr();

                $scope.vFilter.action='xlsx';
                $scope.vFilter.id_card_number = $stateParams.id;

                var data = angular.copy(params);
                data.action = action;
                data.items=[];

                if(!angular.isUndefined(params.receipt_date_from)) {
                    data.receipt_date_from=$filter('date')(data.receipt_date_from, 'dd-MM-yyyy')
                }

                if(!angular.isUndefined(params.receipt_date_to)) {
                    data.receipt_date_to=$filter('date')(data.receipt_date_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(data.date_to)) {
                    data.date_to=$filter('date')(data.date_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(data.date_from)) {
                    data.date_from= $filter('date')(data.date_from, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(data.voucher_date_to)) {
                    data.voucher_date_to=$filter('date')(data.voucher_date_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(data.voucher_date_from)) {
                    data.voucher_date_from= $filter('date')(data.voucher_date_from, 'dd-MM-yyyy')
                }
                data.action=action;

                if( !angular.isUndefined(data.transfer)) {
                    if(data.transfer == 0 ||data.transfer == '0'){
                        data.transfer_company_id = null
                    }
                }

                let codes=[];
                angular.forEach(data.codes, function(v2, k2) {
                    codes.push(v2.id);
                });
                data.codes=codes;

                let vouchers_code=[];
                angular.forEach(data.vouchers_code, function(v2, k2) {
                    vouchers_code.push(v2.id);
                });
                data.vouchers_code=vouchers_code;

                $rootScope.progressbar_start();

                $http({
                    url: "/api/v1.0/common/reports/voucherSearch/filter",
                    method: "POST",
                    data: data
                }).then(function (response) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                    if(response.data.download_token){
                        $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token;
                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                    }
                }, function(error) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                    $rootScope.toastrMessages('error',error.data.msg);
                });
            };

            $scope.load('basic')
        }

    });
