
angular.module('CommonModule')
    .controller('VisitorNotesController', function( $filter,$stateParams,$rootScope, $scope,$uibModal, $http, $ngBootbox,$timeout,$state,visitor_notes) {

        $rootScope.type =$scope.type = $stateParams.type == 'aids'? 'aids' : 'sponsorships';
        $state.current.data.pageTitle = $filter('translate')('visitor-notes');
        $scope.items=[];
        $scope.itemsCount='50';

        var Notes= function($param){
            $rootScope.progressbar_start();
            visitor_notes.query($param,function (response) {
                $rootScope.progressbar_complete();
                $scope.items= response.data;
                $scope.CurrentPage = response.current_page;
                $rootScope.TotalItems = response.total;
                $rootScope.ItemsPerPage = response.per_page;
            });
            
        };
        $rootScope.pageChanged = function (CurrentPage) {
            Notes({'type':$rootScope.type,'page':CurrentPage,'itemsCount' :$scope.itemsCount});
        };
        $rootScope.itemsPerPage_ = function (itemsCount) {
            $scope.itemsCount = itemsCount ;
            Notes({'type':$rootScope.type,'page':1 ,'itemsCount' :itemsCount});
        };
        $rootScope.templateInstruction=function(){

            $rootScope.progressbar_start();
            $http({
                url:'/api/v1.0/common/'+ $rootScope.type +'/visitor_notes/templateInstruction',
                method: "GET"
            }).then(function (response) {
                $rootScope.progressbar_complete();
                window.location = '/api/v1.0/common/files/pdf/export?download_token='+response.data.download_token+'&template=true';
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };
        $scope.actions=function(action,size ,id){
            $rootScope.action=action;
            if(action =='add'){
                $rootScope.formData={};
            }else{
                var target= new visitor_notes(id);
                target.$get({id:id,type:$rootScope.type})
                    .then(function (response) {
                        $rootScope.formData=response;
                    });
            }
            $uibModal.open({
                templateUrl: 'myModalContent.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope, $modalInstance, $log,visitor_notes) {
                    $scope.msg1={};
                    if($rootScope.type =='sponsorships'){
                        $scope.choices=[];
                        angular.forEach($rootScope.sponsorCategories, function(v, k) {
                            if ($rootScope.lang == 'ar'){
                                $scope.choices.push({'id':v.id,'name':v.name});
                            }
                            else{
                                $scope.choices.push({'id':v.id,'name':v.en_name});
                            }
                        });

                    } else{
                        $scope.choices=[];
                        angular.forEach($rootScope.aidCategories, function(v, k) {
                            if ($rootScope.lang == 'ar'){
                                $scope.choices.push({'id':v.id,'name':v.name});
                            }
                            else{
                                $scope.choices.push({'id':v.id,'name':v.en_name});
                            }
                        });
                    }

                    $scope.confirm = function (data) {
                        data.type=$rootScope.type;

                        $rootScope.progressbar_start();
                        var target = new visitor_notes(data);
                        if(action =='add') {
                            $rootScope.progressbar_complete();
                            visitor_notes.save(data,function (response) {
                                    if(response.status=='failed') {
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    else if(response.status=='failed_valid') {
                                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                        $scope.status1 =response.status;
                                        $scope.msg1 =response.msg;
                                    }
                                    else{
                                        $modalInstance.close();
                                        if(response.status=='success')
                                        {
                                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                            Notes({'type':$rootScope.type,'page':$scope.CurrentPage});
                                        }else{
                                            $rootScope.toastrMessages('error',response.msg);
                                        }

                                    }
                            }, function(error) {
                                    $rootScope.toastrMessages('error',error.data.msg);
                                    $modalInstance.close();

                            });
                        }
                        else{
                            $rootScope.progressbar_complete();
                            visitor_notes.update(data,function (response) {
                                    if(response.status=='failed') {
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    else if(response.status=='failed_valid') {
                                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                        $scope.status1 =response.status;
                                        $scope.msg1 =response.msg;
                                    }
                                    else{
                                        $modalInstance.close();
                                        if(response.status=='success')
                                        {
                                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                            Notes({'type':$rootScope.type,'page':$scope.CurrentPage});
                                        }else{
                                            $rootScope.toastrMessages('error',response.msg);
                                        }

                                    }
                                }, function(error) {
                                    $rootScope.toastrMessages('error',error.data.msg);
                                    $modalInstance.close();

                                });
                        }

                    };



                    $scope.dateOptions = {
                        formatYear: 'yy',
                        startingDay: 0
                    };
                    $scope.formats = [ 'dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                    $scope.format = $scope.formats[0];
                    $scope.today = function() {
                        $scope.dt = new Date();
                    };
                    $scope.today();
                    $scope.clear = function() {
                        $scope.dt = null;
                    };
                    $scope.open4 = function($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.popup4.opened = true;
                    };
                    $scope.popup4 = {
                        opened: false
                    };
                    $scope.open3 = function($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.popup3.opened = true;
                    };
                    $scope.popup3 = {
                        opened: false
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                },
                resolve: {
                }
            });
        };
        $scope.delete=function(id){
            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                .then(function() {
                    $rootScope.progressbar_start();
                    visitor_notes.delete({id:id,'type':$rootScope.type},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            Notes({'type':$rootScope.type,'page':$scope.CurrentPage});
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                });
        };

        Notes({'type':$rootScope.type,'page':1,'itemsCount' :50});

    });
