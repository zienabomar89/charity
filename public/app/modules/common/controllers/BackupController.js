
angular.module('CommonModule').
controller('BackupController', function (Backup, $rootScope, $http,$filter, $scope,$state,$uibModal,$ngBootbox) {

    $state.current.data.pageTitle = $filter('translate')('Backup');
    $rootScope.clearToastr();

    $rootScope.error = false;
    $rootScope.msg = '';
    $rootScope.msg2 = '';
    $rootScope.CurrentPage=1;
    $rootScope.itemsCount = 50;
    $rootScope.items=[];

    $scope.close=function(){

        $rootScope.error = false;
        $rootScope.msg = '';
        $rootScope.msg2 = '';
    };

    var LoadBackupVersions = function ($params) {
       // $params.page=$rootScope.CurrentPage;
        $rootScope.progressbar_start();
        $params.itemsCount = $rootScope.itemsCount;
        Backup.filter($params,function (response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            $rootScope.items = response.data;
            $rootScope.CurrentPage = response.current_page;
            $rootScope.TotalItems = response.total;
            $rootScope.ItemsPerPage = response.per_page;

        });
    };

    LoadBackupVersions({page:1});

    $scope.pageChanged = function (CurrentPage) {
        LoadBackupVersions({page:CurrentPage});
    };

    $scope.itemsPerPage_ = function (itemsCount) {
        $rootScope.itemsCount = itemsCount;
        LoadBackupVersions({page:1});
    };

    $scope._backup = function () {
        $rootScope.clearToastr();
        $scope.close();
        angular.element('.btn').addClass("disabled");
        $rootScope.progressbar_start();
        Backup.save(function (data) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if(data.status){
                $rootScope.toastrMessages('success',$filter('translate')('backup successfully done'));
                LoadBackupVersions({page:$rootScope.CurrentPage});
            }else{
                $rootScope.toastrMessages('error',$filter('translate')('something error occurred'));
            }
        });
    };


    $scope.download = function (id) {
        $rootScope.clearToastr();

        $rootScope.progressbar_start();
        $http({
            url:'/api/v1.0/common/Backup/download/'+id,
            method: 'GET'
        }).then(function (response) {
            $rootScope.progressbar_complete();
            if(response.data.status == false){
                $rootScope.toastrMessages('error',$filter('translate')('The backup file does not exist'));
            }else{
                window.location = '/api/v1.0/common/files/sql/export?download_token='+response.data.download_token+'&backup=true';
            }

        }, function(error) {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('error',error.data.msg);
        });
    };

    $rootScope.delete = function (size, id) {
        $rootScope.clearToastr();
        $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
            .then(function() {
                $rootScope.clearToastr();
                $rootScope.progressbar_start();
                Backup.delete({id:id},function (response) {
                    $rootScope.progressbar_complete();
                    if (response.status == 'success')
                    {
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        LoadBackupVersions({page: $rootScope.CurrentPage});
                    } else {
                        $rootScope.toastrMessages('error',response.msg);
                    }
                });
            });
    };


});