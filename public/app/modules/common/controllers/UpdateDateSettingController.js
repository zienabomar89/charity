


angular.module('CommonModule')
    .controller('UpdateDateSettingController', function ($scope,$rootScope,$uibModal,$http,$state,$stateParams,$ngBootbox,updateSetting,locations,Entity,Org,$filter) {

        $rootScope.settings.layout.pageSidebarClosed = true;
        $state.current.data.pageTitle = $filter('translate')('update-data-setting');


        $rootScope.failed =false;
        ;
        $rootScope.model_error_status =false;
        $rootScope.status1 ='';
        $rootScope.status ='';
        $rootScope.msg ='';
        $rootScope.msg1=[];
        $rootScope.actionOn='';
        $rootScope.CurrentPage = 1;


        $rootScope.Addressitems= [];
        $rootScope.categoriesitems=[];
        $rootScope.Sponsoritems=[];

        $scope.itemsCount  = 50 ;
        $scope.itemsCount1 = 50 ;
        $scope.itemsCount2 = 50 ;

        $scope.filters={};
        $rootScope.close=function(){

            $rootScope.failed =false;
            ;
            $rootScope.model_error_status =false;
        };
        $scope.custom = true;

        $scope.toggleCustom = function() {
            $scope.custom = $scope.custom === false ? true: false;

            if($scope.custom === true) {
                $scope.filters={};
            }
        };

        $rootScope.op='';
        $rootScope.formData={};
        var LoadUpdateSetting =function(params){
            $rootScope.progressbar_start();
            updateSetting.query(params,function (response) {
                $rootScope.progressbar_complete();
                if($rootScope.index == 0){
                    $rootScope.Sponsoritems= response.data;
                    $rootScope.CurrentPage = response.current_page;
                    $rootScope.TotalItems = response.total;
                    $rootScope.ItemsPerPage = response.per_page;
                }
                else if($rootScope.index == 1){
                    $rootScope.Addressitems= response.data;
                    $rootScope.CurrentPage1 = response.current_page;
                    $rootScope.TotalItems1 = response.total;
                    $rootScope.ItemsPerPage1 = response.per_page;
                }
                else{
                    $rootScope.categoriesitems= response.data;
                    $rootScope.CurrentPage2 = response.current_page;
                    $rootScope.TotalItems2 = response.total;
                    $rootScope.ItemsPerPage2 = response.per_page;
                }

            });
        };


        Org.list({type:'sponsors'},function (response) { $rootScope.Sponsor = response; });

        locations.get_type_location( {id:3}, function(response) {
            $rootScope.location=  response;
        });

        $scope.fetch=function(target){
            $rootScope.index=target;
            if($rootScope.index == 0){
                LoadUpdateSetting({page:$rootScope.CurrentPage,'type':target,'itemsCount' : $scope.itemsCount});
            }else if($rootScope.index == 1){
                LoadUpdateSetting({page:$rootScope.CurrentPage1,'type':target,'itemsCount' : $scope.itemsCount1});
            }else{
                LoadUpdateSetting({page:$rootScope.CurrentPage2,'type':target,'itemsCount' : $scope.itemsCount2});
            }
        };

        $scope.itemsPerPage_ = function (target,itemsCount) {
             $rootScope.index=target;
            if($rootScope.index == 0){
                $scope.itemsCount = itemsCount ;
            }else if($rootScope.index == 1){
                $scope.itemsCount1 = itemsCount ;
            }else{
                $scope.itemsCount2 = itemsCount ;
            }
            LoadUpdateSetting({'type':target,'page':1,'itemsCount' : itemsCount});
          };

        $scope.pageChanged = function (CurrentPage) {
            if ($rootScope.index == 1){
                $scope.CurrentPage1 = CurrentPage ;
                LoadUpdateSetting({'type':$rootScope.index,'page':CurrentPage,'itemsCount' : $scope.itemsCount1});
            }else if ($rootScope.index == 2){
                $scope.CurrentPage2 = CurrentPage ;
                LoadUpdateSetting({'type':$rootScope.index,'page':CurrentPage,'itemsCount' : $scope.itemsCount2});
            }else{
                $scope.CurrentPage3 = CurrentPage ;
                LoadUpdateSetting({'type':$rootScope.index,'page':CurrentPage,'itemsCount' : $scope.itemsCount3});
            } 
          };

        $scope.action=function(op,size,id){
            $rootScope.op=op;
            if(op =='new'){
                $rootScope.formData={};
            }else{
                $rootScope.progressbar_start();
                var target= new updateSetting(id);
                target.$get({id:id})
                    .then(function (response) {
                        $rootScope.progressbar_complete();
                        if(response.type == 0){
                            $rootScope.List=$rootScope.Sponsor;
                        }else  if(response.type == 1){
                            $rootScope.List=$rootScope.location;
                        }else{
                            $rootScope.List=$rootScope.sponsorCategories.concat($rootScope.aidCategories);
                        }
                        $rootScope.formData=response;
                        $rootScope.index=$rootScope.formData.type;
                        $rootScope.formData.type=$rootScope.formData.type  +"";
                        $rootScope.formData.target_id=$rootScope.formData.target_id  +"";


                    });
            }
            $uibModal.open({
                templateUrl: 'myModalContent.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope, $modalInstance, $log) {

                    $scope.model_error =false;


                    $scope.getTarget=function(type){
                        if(type == 0){
                            $rootScope.List=$rootScope.Sponsor;
                        }else  if(type == 1){
                            $rootScope.List=$rootScope.location;
                        }else{
                            $rootScope.List=$rootScope.sponsorCategories.concat($rootScope.aidCategories);
                        }
                    }

                    $scope.confirm = function (date) {
                        $rootScope.actionOn=date.type;

                        $rootScope.progressbar_start();
                        var item = new updateSetting(date);
                        if($rootScope.op=='new'){
                            item.$save()
                                .then(function (response) {
                                    $rootScope.progressbar_complete();

                                    if(response.status=='error' || response.status=='failed')
                                    {
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    else if(response.status=='failed_valid')
                                    {
                                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                        $scope.status1 =response.status;
                                        $scope.msg1 =response.msg;
                                    }
                                    else{
                                        $modalInstance.close();
                                        if(response.status=='success')
                                        {
                                            if($rootScope.actionOn== $rootScope.index){
                                                LoadUpdateSetting({page:$rootScope.CurrentPage,'type':$rootScope.index});
                                            }
                                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        }else{
                                            $rootScope.toastrMessages('error',response.msg);
                                        }


                                    }

                                }, function(error) {
                                    $rootScope.progressbar_complete();
                                    $rootScope.toastrMessages('error',error.data.msg);
                                });
                        }else{
                            item.$update()
                                .then(function (response) {

                                    if(response.status=='error' || response.status=='failed')
                                    {
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    else if(response.status=='failed_valid')
                                    {
                                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                        $scope.status1 =response.status;
                                        $scope.msg1 =response.msg;
                                    }
                                    else{
                                        $modalInstance.close();
                                        if(response.status=='success')
                                        {
                                            if($rootScope.actionOn == 0){
                                                LoadUpdateSetting({page:$rootScope.CurrentPage,'type':0});
                                            }else if($rootScope.actionOn == 1){
                                                LoadUpdateSetting({page:$rootScope.CurrentPage1,'type':1});
                                            }else{
                                                LoadUpdateSetting({page:$rootScope.CurrentPage2,'type':2});
                                            }
                                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        }else{
                                            $rootScope.toastrMessages('error',response.msg);
                                        }


                                    }

                                }, function(error) {
                                    $rootScope.toastrMessages('error',error.data.msg);
                                });
                        }
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                },
                resolve: {

                }
            });

        }
        $scope.delete=function(size,id,type){
            $rootScope.actionOn=type;
            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                .then(function() {
                    $rootScope.progressbar_start();
                    updateSetting.delete({id:id},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='error' || response.status=='failed') {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else if(response.status=='failed_valid')
                        {
                            $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                            $scope.status1 =response.status;
                            $scope.msg1 =response.msg;
                        }
                        else{
                            if(response.status=='success')
                            {
                                if($rootScope.actionOn == 0){
                                    LoadUpdateSetting({page:$rootScope.CurrentPage,'type':0});
                                }else if($rootScope.actionOn == 1){
                                    LoadUpdateSetting({page:$rootScope.CurrentPage1,'type':1});
                                }else{
                                    LoadUpdateSetting({page:$rootScope.CurrentPage2,'type':2});
                                }
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                        }
                    });
                });
        };





    });
