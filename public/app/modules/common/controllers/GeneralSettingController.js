
angular.module('CommonModule')
    .controller('GeneralSettingController', function($state,$filter,$stateParams,$rootScope, $scope ,$http, $timeout,setting,Entity) {

        $rootScope.type=$scope.type=$stateParams.type == 'aids'? 'aids' : 'sponsorships';
        $state.current.data.pageTitle = $filter('translate')('General Settings');

        $scope.success =false;
        $scope.failed =false;
        $scope.error =false;
        $scope.model_error_status =false;

        $scope.close=function(){
            $scope.success =false;
            $scope.failed =false;
            $scope.error =false;
            $scope.model_error_status =false;
        };

        $scope.status ='';
        $scope.msg ='';
        $scope.msg2 ='';

        if($scope.type =='aids'){
            $scope.entities='max_total_amount_of_voucher,aid_image';
            $scope.c='documentTypes';

        }else{
            $scope.entities='max_organization_share,max_total_amount_of_payments,father_kinship,mother_kinship,son_kinship,daughter_kinship,wife_kinship,husband_kinship,image';
            $scope.c='kinship,documentTypes';

        }
//-----------------------------------------------------------------------------------------------------
        Entity.get({'entity':'entities' , 'c':$scope.c},function (response){
            $scope.kinship = response.kinship;
            $scope.documents = response.documentTypes;
        });

        $scope.setting=[];

        var values=function () {
            $rootScope.progressbar_start();
            setting.getSysSetting({'entities':$scope.entities},function (response) {
                $rootScope.progressbar_complete();
                $scope.setting=response;

                if($scope.type !='aids'){
                    $scope.entities='max_organization_share,max_total_amount_of_payments,father_kinship,mother_kinship,son_kinship,daughter_kinship,wife_kinship,husband_kinship';
                    if($scope.setting.father_kinship == null){
                        $scope.setting.father_kinship="";
                    }else{
                        $scope.setting.father_kinship    = $scope.setting.father_kinship +"" ;
                    }
                    if($scope.setting.mother_kinship == null){
                        $scope.setting.mother_kinship="";
                    }else{
                        $scope.setting.mother_kinship    = $scope.setting.mother_kinship +"" ;
                    }
                    if($scope.setting.son_kinship == null){
                        $scope.setting.son_kinship="";
                    }else{
                        $scope.setting.son_kinship    = $scope.setting.son_kinship +"" ;
                    }
                    if($scope.setting.daughter_kinship == null){
                        $scope.setting.daughter_kinship="";
                    }else{
                        $scope.setting.daughter_kinship    = $scope.setting.daughter_kinship +"" ;
                    }
                    if($scope.setting.wife_kinship == null){
                        $scope.setting.wife_kinship="";
                    }else{
                        $scope.setting.wife_kinship    = $scope.setting.wife_kinship +"" ;
                    }
                    if($scope.setting.husband_kinship == null){
                        $scope.setting.husband_kinship="";
                    }else{
                        $scope.setting.husband_kinship    = $scope.setting.husband_kinship +"" ;
                    }
                }

            });
        };


        values();
//-----------------------------------------------------------------------------------------------------

        $scope.save=function(params){

            $rootScope.progressbar_start();
            var target = new setting(params);
            target.$update()
                .then(function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status=='failed') {
                        $rootScope.toastrMessages('error',response.msg);
                    }
                    else if(response.status=='failed_valid')
                    {
                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                        $scope.status1 =response.status;
                        $scope.msg1 =response.msg;
                    }
                    else{
                        if(response.status=='success') {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    }
                });

        };

    });