angular.module('CommonModule')
    .controller('statisticController', function ($filter,$scope,$rootScope,$uibModal,$http,$state,$stateParams,persons,Org,Entity) {

        $rootScope.settings.layout.pageSidebarClosed = true;
        $rootScope.type=$scope.type=$stateParams.type ;
        $scope.filter={selected:[],selectedSponsor:[]};
        $scope.items=[];
        $scope.descendants=[];
        $scope.Orgs=[];
        $scope.sponsorship_items=[];
        $scope.choices = [];
        if($stateParams.type== null || $stateParams.type== 'null'){
            $rootScope.type=$scope.type=$stateParams.type='charity-act';
        }

        Org.list({type:'organizations'},function (response) {  $scope.organizations_ = response; });

        if($scope.type =='sponsorships-sponsor' || $scope.type == 'vouchers-sponsor'|| $scope.type == 'payments'){
            Org.list({type:'sponsors'},function (response) { $scope.sponsors_ = response; });
        }

        if($scope.type == 'payments'){
            Entity.get({entity:'entities',c:'paymentCategory'},function (response) {
                $scope.paymentCategory = response.paymentCategory;
            });
        }

        if($scope.type =='sponsorships-sponsor'){
            $state.current.data.pageTitle = $filter('translate')("sponsorship-statistic");
        }else if($scope.type == 'vouchers-sponsor'){
            $state.current.data.pageTitle = $filter('translate')("vouchers-sponsor-statistic");
        }else if($scope.type == 'payments' ){
            $state.current.data.pageTitle = $filter('translate')("payments-statistic");
        }else if($scope.type == 'charity-act' ){
            $state.current.data.pageTitle = $filter('translate')("charity-act-statistic");
        }else{
            $state.current.data.pageTitle = $filter('translate')("islamic-commitment-statistic");
        }

        if($scope.type =='sponsorships-sponsor' || $scope.type == 'vouchers-sponsor' || $scope.type == 'charity-act' || $scope.type == 'payments'){
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), 0, 1);
            var lastDay = new Date(date.getFullYear(), 12, 0);
            var filter_first=$filter('date')(firstDay, 'dd-MM-yyyy');
            var filter_last=$filter('date')(lastDay, 'dd-MM-yyyy');
            $scope.firstDay = filter_first;
            $scope.lastDay = filter_last;
            $scope.filter.first = firstDay;
            $scope.filter.last = lastDay;
        }

        var getStatistic =function(params){
            params.type=$scope.type;
            params.action='show';

            if($scope.type =='sponsorships-sponsor' || $scope.type == 'vouchers-sponsor' || $scope.type == 'charity-act' || $scope.type == 'payments') {

                if( !angular.isUndefined(params.first)) {
                    params.first=$filter('date')(params.first, 'dd-MM-yyyy');
                }else{
                    params.first=$filter('date')(firstDay, 'dd-MM-yyyy');
                }
                if( !angular.isUndefined(params.last)) {
                    params.last=$filter('date')(params.last, 'dd-MM-yyyy');
                }else{
                    params.last=$filter('date')(lastDay, 'dd-MM-yyyy');
                }

                $scope.firstDay = params.first;
                $scope.lastDay = params.last;
            }

            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            persons.getStatistic(params,function (response) {
                $rootScope.progressbar_complete();

                if(response.status == true){
                    $scope.organizations=response.descendants;

                    $scope.items =response.result;

                    if($scope.type =='vouchers-sponsor' || $scope.type =='sponsorships-sponsor' || $scope.type =='charity-act' || $scope.type == 'payments'){
                        $scope.sponsors=response.sponsors;
                        $scope.total_amount=response.amount;
                        $scope.total_count=response.count;
                    }

                    if($scope.type =='vouchers-sponsor' || $scope.type =='sponsorships-sponsor' || $scope.type == 'payments'){
                        $scope.sponsors=response.sponsors;
                    }

                    if($scope.type =='charity-act'){
                        $scope.sponsorship_items =response.sponsorship_result;
                    }
                }else{

                    $scope.organizations =[];
                    $scope.items =[];
                    $scope.sponsors =[];
                    $scope.total_amount =[];
                    $scope.total_count =[];
                    $scope.sponsors =[];
                    $scope.sponsorship_items =[];

                    if($scope.type =='vouchers-sponsor' || $scope.type =='sponsorships-sponsor' || $scope.type == 'payments'){
                        $rootScope.toastrMessages('error',$filter('translate')("Associations you have identified are not tied to the sponsors specified") )
                    }else {
                        $rootScope.toastrMessages('error',$filter('translate')("A bug occurred during statistic creation"))

                    }

                }

           });
        };

        // getStatistic($scope.filter);

        $scope.Search=function(data,action) {

            var params = angular.copy(data);
            params.action = action;
            params.selected = [];
            params.selectedSponsor = [];
            params.selectedCategory = [];

            if( !angular.isUndefined(params.organization_id)) {
                angular.forEach(params.organization_id, function(v, k) {
                    params.selected.push(v.id);
                });
                delete params.organization_id;
            }

            if( !angular.isUndefined(params.sponsor_id)) {
                angular.forEach(params.sponsor_id, function(v, k) {
                    params.selectedSponsor.push(v.id);
                });
                delete params.sponsor_id;
            }

            if( !angular.isUndefined(params.category_id)) {
                angular.forEach(params.category_id, function(v, k) {
                    params.selectedCategory.push(v.id);
                });
                delete params.category_id;
            }

            if(action == 'export'){

                if($scope.type =='sponsorships-sponsor' || $scope.type == 'vouchers-sponsor' || $scope.type == 'charity-act' || $scope.type == 'payments') {

                    if( !angular.isUndefined(params.first)) {
                        params.first=$filter('date')(params.first, 'dd-MM-yyyy');
                    }else{
                        params.first=$filter('date')(firstDay, 'dd-MM-yyyy');
                    }
                    if( !angular.isUndefined(params.last)) {
                        params.last=$filter('date')(params.last, 'dd-MM-yyyy');
                    }else{
                        params.last=$filter('date')(lastDay, 'dd-MM-yyyy');
                    }

                    $scope.firstDay = params.first;
                    $scope.lastDay = params.last;
                }

                params.type=$scope.type;
                $rootScope.progressbar_start();
                $http({
                    url: "/api/v1.0/common/persons/getStatistic",
                    method: "POST",
                    data: params
                }).then(function (response) {
                    $rootScope.progressbar_complete();
                    window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token;

                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });

            }else{
                getStatistic(params);
            }
        };

        $scope.dateOptions = {formatYear: 'yy', startingDay: 0 };
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {$scope.dt = new Date(); };
        $scope.today();
        $scope.clear = function() { $scope.dt = null;};
        $scope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup1.opened = true;};
        $scope.popup1 = {opened: false};
        $scope.open2 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup2.opened = true;};
        $scope.popup2 = {opened: false};

    });
