
angular.module('CommonModule')
    .controller('BlockedController', function($filter,$stateParams,$rootScope, $scope,$uibModal, $http, $timeout,$state,block, FileUploader, OAuthToken,Org) {

    $rootScope.type=$scope.type=$stateParams.type == 'aids'? 'aids' : 'sponsorships';
    $rootScope.category_type=$scope.category_type=$stateParams.type == 'aids'? 2 : 1;
    $state.current.data.pageTitle = $filter('translate')('blocked id cards number');


    $rootScope.failed =false;
    $rootScope.model_error_status =false;
    $scope.export='false';
    $rootScope.target=1;
    $rootScope.items=[];
    $rootScope.additems=[];
    $rootScope.nominationitem=[];
        $scope.itemsCount = '50' ;


        $rootScope.aidCategories_=[];
        angular.forEach($rootScope.aidCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });


        var resetBlockFilter =function() {
            $scope.filter = {
                "category_type":$rootScope.category_type,
                "blocked_from":"",
                "blocked_to":"",
                "category_id":"",
                "id_card_number":""
            };
        };
    $scope.toggleStatus = true;
    $scope.toggleSearchForm = function() {
            $scope.toggleStatus = $scope.toggleStatus == false ? true: false;
            if($scope.toggleStatus == true) {
                resetBlockFilter();
                LoadBlocked({'type':$rootScope.target,'page':1});
            }else{

                $scope.export='false';
                resetBlockFilter();
            }
        };

    var LoadBlocked=function($param){
        $param.category_type=$rootScope.category_type;
        $param.status="filters";
            $rootScope.target=$param.type;
        $rootScope.progressbar_start();
        var target = new block($param);
            target.$filter()
                .then(function (response) {
                    $rootScope.progressbar_complete();
                    if(response.data.length != 0) {
                        $scope.export = 'true';
                        if ($param.type == 1) {
                            $rootScope.result = 'true';
                            $rootScope.additems = response.data;
                            $scope.CurrentPage = response.current_page;
                            $rootScope.TotalItems = response.total;
                            $rootScope.ItemsPerPage = response.per_page;
                        } else{
                            $rootScope.result_1 = 'true';
                            $rootScope.nominationitem = response.data;
                            $scope.CurrentPage1 = response.current_page;
                            $rootScope.TotalItems1 = response.total;
                            $rootScope.ItemsPerPage1 = response.per_page;
                        }
                    }else{
                        $scope.export='false';
                        if($param.type==1) {
                            $rootScope.result='false';
                            $rootScope.additems=[];
                        }else if($param.type==2){
                            $rootScope.nominationitem=[];
                            $rootScope.result_1='false';
                        }
                    }

                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });
        };

    $rootScope.close=function(){

            $rootScope.failed =false;
            ;
            $rootScope.model_error_status =false;
        };

    if($rootScope.type == 'sponsor') {
        Org.list({type:'sponsors'},function (response) { $scope.Sponsor = response; });
        $rootScope.target=3;
        LoadBlocked({status:'filters','type':3,'page':1});
    }else{
        $rootScope.target=1;
        LoadBlocked({status:'filters','type':1,'page':1});
    }

    $rootScope.pageChanged = function (type,CurrentPage) {
        LoadBlocked({status:'filters','type':type,'page':CurrentPage});
    };

    $rootScope.itemsPerPage_ = function (type,itemsCount) {
        LoadBlocked({status:'filters','type':type,'page':1,itemsCount:itemsCount});
    };

    $scope.fetch=function(target){
        if($rootScope.type != 'sponsor'){
            $rootScope.target=target;
            if(target==1){
                LoadBlocked({status:'filters','type':target,'page':$scope.CurrentPage1});
            } else{
                LoadBlocked({status:'filters','type':target,'page':$scope.CurrentPage});
            }
        }
    };

    $scope.ExportToExcel=function(target){
        $rootScope.progressbar_start();
        $http({
            url: '/api/v1.0/common/block/export/'+target,
            method: "GET"
        }).then(function (response) {
            $rootScope.progressbar_complete();
            window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token;
        }, function(error) {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('error',error.data.msg);
        });
    };

   $scope.Search=function(data,action) {

            if( !angular.isUndefined(data.blocked_from)) {
                    data.blocked_from=$filter('date')(data.blocked_from, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(data.blocked_to)) {
                data.blocked_to= $filter('date')(data.blocked_to, 'dd-MM-yyyy')
            }

            data.type=$rootScope.target;
            data.status=action;

           if(action =='export'){
               $rootScope.progressbar_start();
               $http({
                   url: '/api/v1.0/common/block/filter',
                   method: "GET",
                   data: data
               }).then(function (response) {
                   $rootScope.progressbar_complete();
                   window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token;
               }, function(error) {
                   $rootScope.progressbar_complete();
                   $rootScope.toastrMessages('error',error.data.msg);
               });
           }else{
               data.page=1;
               LoadBlocked(data);
           }

        };

   $scope.new=function(size){

       if($rootScope.type == 'sponsor') {
           size='sm';
       }else{
           size='md';
       }

        $uibModal.open({
            templateUrl: 'new.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: size,
            controller: function ($rootScope,$scope, $modalInstance, $log,block, FileUploader, OAuthToken) {
                $scope.upload=true;

                $scope.block={checkbox:1};

                $scope.RestUpload=function(value){
                    if(value ==2){
                        $scope.upload=false;
                    }
                };

                $scope.fileupload=function(){
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.upload=true;
                    $scope.upload=true;
                };

                var Uploader = $scope.uploader = new FileUploader({
                    url: '/api/v1.0/common/block',
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });

                var check=false;

                if($rootScope.target == 3 || $rootScope.target == '3'){
                    check=true;
                }



                $scope.confirm = function (params) {
                    var item = angular.copy(params);

                    $rootScope.progressbar_start();
                    if($rootScope.type == 'sponsor') {
                        if(item.category_id){
                            var categories = [];
                            angular.forEach(item.category_id, function(v, k) {
                                categories.push(v.id);
                            });
                            item.category_id =  categories
                        }
                    }


                    if(item.checkbox ==2){
                        Uploader.onBeforeUploadItem = function(i) {
                            i.formData = [{type: item.type,checkbox: item.checkbox,category_id: item.category_id}];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);
                            if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                if(response.status=='success')
                                {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    var page=1;
                                    if($rootScope.target==1){
                                        page =$scope.CurrentPage;
                                    } else if($rootScope.target==1){
                                        page =$scope.CurrentPage1;
                                    }else{
                                        page =$scope.CurrentPage2;
                                    }

                                    LoadBlocked({ status:'filters','type':$rootScope.target,'page':page});

                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                $modalInstance.close();
                            }
                        };
                        Uploader.uploadAll();

                    }


                    if(!check || item.checkbox ==1){
                        if(!check){
                            if(item.id_card_number.length != 9) {
                                $rootScope.toastrMessages('error',$filter('translate')('ID number must be 9 digit'));
                            }else{
                                check=true;
                            }
                        }
                    }
                    if(check){
                        if($rootScope.type == 'sponsor') {
                            item.type=3;
                            $rootScope.target=3;
                        }else{
                            $rootScope.target = item.type;
                        }
                        $rootScope.progressbar_start();
                        var target = new block(item);
                        target.$save()
                            .then(function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='failed') {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid') {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                } else{
                                    if(response.status=='success') {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                         var page=1;
                                         if($rootScope.target==1){
                                               page =$scope.CurrentPage;
                                         } else if($rootScope.target==1){page =$scope.CurrentPage1;}else{
                                              page =$scope.CurrentPage2;
                                         }
                                         LoadBlocked({ status:'filters','type':$rootScope.target,'page':page});
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    $modalInstance.close();
                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();

                            });
                    }
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };

            }
        });

    };
   $scope.UnBlock=function(id,category_id){
       $rootScope.targetCard={id:id,category_id:category_id,'type':$rootScope.target};
       $uibModal.open({
            templateUrl: 'myModalContent1.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'sm',
            controller: function ($rootScope,$scope, $modalInstance, $log,block) {


                $scope.confirm = function () {
                    $rootScope.progressbar_start();
                    var target = new block($rootScope.targetCard);
                    target.$deleteBlockCard()
                        .then(function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                if(response.status=='success')
                                {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    var page=1;
                                    if($rootScope.target==1){
                                        page =$scope.CurrentPage;
                                    } else if($rootScope.target==1){page =$scope.CurrentPage1;}else{
                                        page =$scope.CurrentPage2;
                                    }
                                    LoadBlocked({ status:'filters','type':$rootScope.target,'page':page});
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }

                                $modalInstance.close();
                            }
                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                            $modalInstance.close();

                        });
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };

            },
            resolve: {
            }
        });

    };


        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 0
        };
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();
        $scope.clear = function() {
            $scope.dt = null;
        };
        $scope.open1 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup1.opened = true;
        };
        $scope.popup1 = {
            opened: false
        };
        $scope.open2 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup2.opened = true;
        };
        $scope.popup2 = {
            opened: false
        };


    });

angular.module('CommonModule')
    .controller('BlockedSponsorController', function($filter,$stateParams,$rootScope, $scope,$uibModal, $http, $timeout,$state,block, FileUploader, OAuthToken,Org,$ngBootbox) {

    $rootScope.type=$scope.type= 'sponsor';
    $state.current.data.pageTitle = $filter('translate')('blocked sponsors');


    $rootScope.failed =false;
    ;
    $rootScope.model_error_status =false;
    $rootScope.result='true';
    $rootScope.result_1='true';
    $scope.export='false';
    $rootScope.target=1;
    $rootScope.items=[];
    $rootScope.additems=[];
    $rootScope.nominationitem=[];

        $rootScope.aidCategories_=[];
        angular.forEach($rootScope.aidCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

    var resetBlockFilter =function() {
            $scope.filter = {
                "blocked_from":"",
                "blocked_to":"",
                "category_id":"",
                "id_card_number":""
            };
        };
    $scope.toggleStatus = true;
    $scope.toggleSearchForm = function() {
            $scope.toggleStatus = $scope.toggleStatus == false ? true: false;
            if($scope.toggleStatus == true) {
                resetBlockFilter();

                if($rootScope.target==1){
                    LoadBlocked({'type':$rootScope.target,'page':1});
                } else{
                    LoadBlocked({'type':$rootScope.target,'page':1});
                }

            }else{

                $scope.export='false';
                resetBlockFilter();
            }
        };

    var LoadBlocked=function($param){
        $param.status="filters";
        $rootScope.progressbar_start();
        $rootScope.target=$param.type;
            var target = new block($param);
            target.$filter()
                .then(function (response) {
                    $rootScope.progressbar_complete();
                    if(response.data.length != 0) {
                        $scope.export = 'true';
                        if ($param.type == 1) {
                            $rootScope.result = 'true';
                            $rootScope.additems = response.data;
                            $scope.CurrentPage = response.current_page;
                            $rootScope.TotalItems = response.total;
                            $rootScope.ItemsPerPage = response.per_page;
                        } else if ($param.type == 2) {
                            $rootScope.result_1 = 'true';
                            $rootScope.nominationitem = response.data;
                            $scope.CurrentPage1 = response.current_page;
                            $rootScope.TotalItems1 = response.total;
                            $rootScope.ItemsPerPage1 = response.per_page;
                        } else {
                            $scope.export = 'true';
                            $rootScope.result = 'true';
                            $rootScope.items = response.data;
                            $scope.CurrentPage2 = response.current_page;
                            $rootScope.TotalItems2 = response.total;
                            $rootScope.ItemsPerPage2 = response.per_page;
                        }
                    }else{
                        $scope.export='false';
                        if($param.type==1) {
                            $rootScope.result='false';
                            $rootScope.additems=[];
                        }else if($param.type==2){
                            $rootScope.nominationitem=[];
                            $rootScope.result_1='false';
                        }else{
                            $rootScope.items=[];
                            $rootScope.result='false';
                        }
                    }

                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });
        };

    $rootScope.close=function(){

            $rootScope.failed =false;
            ;
            $rootScope.model_error_status =false;
        };

    if($rootScope.type == 'sponsor') {
        Org.list({type:'sponsors'},function (response) { $rootScope.Sponsor = response; });
        $rootScope.target=3;
        LoadBlocked({status:'filters','type':3,'page':1});
    }else{
        $rootScope.target=1;
        LoadBlocked({status:'filters','type':1,'page':1});
    }

    $rootScope.pageChanged = function (type,CurrentPage) {
        LoadBlocked({status:'filters','type':type,'page':CurrentPage});
    };

    $rootScope.itemsPerPage_ = function (type,itemsCount) {
        LoadBlocked({status:'filters','type':type,'page':1,itemsCount:itemsCount});
    };

    $scope.fetch=function(target){
        if($rootScope.type != 'sponsor'){
            $rootScope.target=target;
            if(target==1){
                LoadBlocked({status:'filters','type':target,'page':$scope.CurrentPage1});
            } else{
                LoadBlocked({status:'filters','type':target,'page':$scope.CurrentPage});
            }
        }
    };

    $scope.ExportToExcel=function(target){
        $rootScope.progressbar_start();
        $http({
            url: '/api/v1.0/common/block/export/'+target,
            method: "GET"
        }).then(function (response) {
            $rootScope.progressbar_complete();
            window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token;
        }, function(error) {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('error',error.data.msg);
        });
    };

   $scope.Search=function(data,action) {

            if( !angular.isUndefined(data.blocked_from)) {
                    data.blocked_from=$filter('date')(data.blocked_from, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(data.blocked_to)) {
                data.blocked_to= $filter('date')(data.blocked_to, 'dd-MM-yyyy')
            }

            data.type=$rootScope.target;
            data.status=action;

           if(action =='export'){
               $rootScope.progressbar_start();
               $http({
                   url: '/api/v1.0/common/block/filter',
                   method: "GET",
                   data: data
               }).then(function (response) {
                   $rootScope.progressbar_complete();
                   window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token;
               }, function(error) {
                   $rootScope.progressbar_complete();
                   $rootScope.toastrMessages('error',error.data.msg);
               });
           }else{
               data.page=1;
               LoadBlocked(data);
           }

        };

   $scope.new=function(size){

       if($rootScope.type == 'sponsor') {
           size='sm';
       }else{
           size='md';
       }

        $uibModal.open({
            templateUrl: 'new.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: size,
            controller: function ($rootScope,$scope, $modalInstance, $log,block, FileUploader, OAuthToken) {
                $scope.upload=true;

                $scope.block={checkbox:1};

                $scope.RestUpload=function(value){
                    if(value ==2){
                        $scope.upload=false;
                    }
                };

                $scope.fileupload=function(){
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.upload=true;
                    $scope.upload=true;
                };

                var Uploader = $scope.uploader = new FileUploader({
                    url: '/api/v1.0/common/block',
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });

                var check=false;

                if($rootScope.target == 3 || $rootScope.target == '3'){
                    check=true;
                }



                $scope.confirm = function (params) {
                    var item = angular.copy(params);
                    $rootScope.progressbar_start();

                    if($rootScope.type == 'sponsor') {
                        if(item.category_id){
                            var categories = [];
                            angular.forEach(item.category_id, function(v, k) {
                                categories.push(v.id);
                            });
                            item.category_id =  categories
                        }
                    }


                    if(item.checkbox ==2){
                        Uploader.onBeforeUploadItem = function(i) {
                            i.formData = [{type: item.type,checkbox: item.checkbox,category_id: item.category_id}];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);
                            if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                if(response.status=='success')
                                {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    var page=1;
                                    if($rootScope.target==1){
                                        page =$scope.CurrentPage;
                                    } else if($rootScope.target==1){
                                        page =$scope.CurrentPage1;
                                    }else{
                                        page =$scope.CurrentPage2;
                                    }

                                    LoadBlocked({ status:'filters','type':$rootScope.target,'page':page});

                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                $modalInstance.close();
                            }
                        };
                        Uploader.uploadAll();

                    }


                    if(!check || item.checkbox ==1){
                        if(!check){
                            if(item.id_card_number.length != 9) {
                                $rootScope.toastrMessages('error',$filter('translate')('ID number must be 9 digit'));
                            }else{
                                check=true;
                            }
                        }
                    }
                    if(check){
                        if($rootScope.type == 'sponsor') {
                            item.type=3;
                            $rootScope.target=3;
                        }else{
                            $rootScope.target = item.type;
                        }
                        $rootScope.progressbar_start();
                        var target = new block(item);
                        target.$save()
                            .then(function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='failed') {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid') {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                } else{
                                    if(response.status=='success') {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                         var page=1;
                                         if($rootScope.target==1){
                                               page =$scope.CurrentPage;
                                         } else if($rootScope.target==1){page =$scope.CurrentPage1;}else{
                                              page =$scope.CurrentPage2;
                                         }
                                         LoadBlocked({ status:'filters','type':$rootScope.target,'page':page});
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    $modalInstance.close();
                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();

                            });
                    }
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };

            }
        });

    };
   $scope.UnBlock=function(id,category_id){
       $rootScope.targetCard={id:id,category_id:category_id,'type':$rootScope.target};
       
       var title = $filter('translate')('are you want to unblock card');
       if($rootScope.target == ''){
           title = $filter('translate')('are you want to unblock sponsor');
       }
       
       $ngBootbox.confirm(title)
                .then(function() {
                    $rootScope.progressbar_start();
                    block.delete({id:id,category_id:category_id,'type':$rootScope.target},function (response) {
                        $rootScope.progressbar_complete();
                            if(response.status=='failed'){
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid') {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                if(response.status=='success')
                                {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    var page=1;
                                    if($rootScope.target==1){
                                        page =$scope.CurrentPage;
                                    } else if($rootScope.target==1){page =$scope.CurrentPage1;}else{
                                        page =$scope.CurrentPage2;
                                    }
                                    LoadBlocked({ status:'filters','type':$rootScope.target,'page':page});
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }
                    });
                });       

    };


        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 0
        };
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();
        $scope.clear = function() {
            $scope.dt = null;
        };
        $scope.open1 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup1.opened = true;
        };
        $scope.popup1 = {
            opened: false
        };
        $scope.open2 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup2.opened = true;
        };
        $scope.popup2 = {
            opened: false
        };


    });
