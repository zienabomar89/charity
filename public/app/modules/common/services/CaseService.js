
angular.module('CommonModule')
    .factory('cases', function ($resource) {
        return $resource('/api/v1.0/common/:type/cases/:operation/:id', {id: '@id',type: '@type',page: '@page'}, {
            update : { method:'PUT', isArray: false},
            restore   : { method:'PUT', params: {operation:'restore'}, isArray: false},
            getInCompleteCases  : { method:'POST' ,params:{operation:'getInCompleteCases'}, isArray:false },
            getCaseAttachments: { method:'POST' ,params:{operation:'getCaseAttachments'}, isArray:false },
            getCaseReports: { method:'POST' ,params:{operation:'getCaseReports'}, isArray:false },
            getSponsorships: { method:'POST' ,params:{operation:'getSponsorships'}, isArray:false },
            getVisitorNote:{method:'GET',params:{operation:'getVisitorNote'}, isArray:false},
            getStatusLogs:{method:'GET',params:{operation:'getStatusLogs'}, isArray:false},
            savePhoto: { method:'POST' ,params:{operation:'savePhoto'}, isArray:false },
            checkAccount: { method:'GET' ,params:{operation:'checkAccount'}, isArray:false },
            getSponsorList:    { method:'POST',params:{operation:'getSponsorList'}, isArray: false},
            setCaseAttachment   :{ method:'PUT',params:{operation:'setCaseAttachment'}  ,isArray:false},
            setAttachment   :{ method:'PUT',params:{operation:'setAttachment'}  ,isArray:false},
            updateStatusAndCategories:{method: 'POST' , params: {operation:'updateStatusAndCategories'}, isArray: false},
            transferGroup:{method: 'POST' , params: {operation:'transferGroup'}, isArray: false},
            transferOne:{method: 'POST' , params: {operation:'transferOne'}, isArray: false},
            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
            saveResidence  :   {method: 'PUT' , params: {operation:'saveResidence'}, isArray: false},
            saveHomeIndoor :   {method: 'PUT' , params: {operation:'saveHomeIndoor'}, isArray: false},
            saveOthersData :   {method: 'PUT' , params: {operation:'saveOthersData'}, isArray: false},
            saveRecommendations :   {method: 'PUT' , params: {operation:'saveRecommendations'}, isArray: false},
            changeCategory :   {method: 'PUT' , params: {operation:'changeCategory'}, isArray: false},
            changeCategoryToGroup :   {method: 'PUT' , params: {operation:'changeCategoryToGroup'}, isArray: false},

            /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
            goverment_update: { method:'POST' ,params:{operation:'goverment_update'}, isArray:false },
            filterCases:    { method:'POST',params:{operation:'filterCases'}, isArray: false},
            individual:    { method:'POST',params:{operation:'individual'}, isArray: false},
        });
    });


