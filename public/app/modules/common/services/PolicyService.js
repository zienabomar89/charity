
angular.module('CommonModule')
    .factory('categories_policy', function ($resource) {
        return $resource('/api/v1.0/common/:type/categories_policy/:operation/:id', {type: '@type',page: '@page',id: '@id'}, {
            query  : { method:'GET', isArray:false },
            update : { method: 'PUT' , isArray: false},
            getPolicy  : { method:'POST' ,params:{operation:'getPolicy'}, isArray:false },
        });
    });


