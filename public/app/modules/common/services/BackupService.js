
angular.module('CommonModule')
    .factory('Backup', function ($resource) {
        return $resource('/api/v1.0/common/Backup/:operation/:id', {id: '@id',page: '@page'}, {
            filter:{method:'POST' ,params:{operation:'filter'}, isArray:false },
            query  : { method:'GET', isArray:false },
            update : { method: 'PUT' , isArray: false}
        });
    });

