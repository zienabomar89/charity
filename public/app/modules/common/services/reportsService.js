
angular.module('CommonModule')
    .factory('reportsService', function ($resource) {
        return $resource('/api/v1.0/common/reports/:type/:operation/:id', {type: '@type',page: '@page',id: '@id'}, {
            searchFilter  : { method:'POST' ,params:{operation:'searchFilter'}, isArray:false },
            filter  : { method:'POST' ,params:{operation:'filter'}, isArray:false },
            tabContent  : { method:'POST' ,params:{operation:'tabContent'}, isArray:false }
        });
    });


