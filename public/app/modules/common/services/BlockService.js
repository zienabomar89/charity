
angular.module('CommonModule')
    .factory('block', function ($resource) {
        return $resource('/api/v1.0/common/block/:operation/:id', {id: '@id',page: '@page'}, {
            filter      : { method:'Post' ,params:{operation:'filter'}, isArray:false },
            storeBlockCard      : { method:'Post' ,params:{operation:'storeBlockCard'}, isArray:false },
            deleteBlockCard   : { method:'DELETE', params: {operation:'deleteBlockCard',category_id: '@category_id',type: '@type'}, isArray: false}
        });
    });

