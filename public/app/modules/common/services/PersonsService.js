


angular.module('CommonModule')
        .factory('persons', function ($resource) {
            return $resource('/api/v1.0/common/persons/:operation/:id/', {id: '@id',page: '@page'}, {
                getPersons: { method:'Post' ,params:{operation:'getPersons'}, isArray:false },
                checkAccount: { method:'GET' ,params:{operation:'checkAccount'}, isArray:false },
                getPersonReports: { method:'POST' ,params:{operation:'getPersonReports'}, isArray:false },
                voucherFilter: { method:'POST' ,params:{operation:'voucherFilter'}, isArray:false },
                getStatistic: { method:'POST' ,params:{operation:'getStatistic'}, isArray:false },
                find: { method:'POST' ,params:{operation:'find'}, isArray:false },
                update : { method: 'PUT' , isArray: false}

            });
        });
