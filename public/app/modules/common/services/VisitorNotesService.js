
angular.module('CommonModule')
            .factory('visitor_notes', function ($resource) {
                return $resource('/api/v1.0/common/:type/visitor_notes/:operation/:id', {type: '@type',page: '@page',id: '@id'}, {
                    query  : { method:'GET' ,params:{type: '@type',page: '@page'}, isArray:false },
                    update:{method: 'PUT'  , params: {id: '@id'}, isArray: false}
                });
            });

