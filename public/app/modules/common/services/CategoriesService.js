
angular.module('CommonModule')
    .factory('category', function ($resource) {
        return $resource('/api/v1.0/common/:type/category/:operation/:id', {type: '@type',page: '@page',id: '@id','mode' :'@mode'}, {
            query  : {method:'GET',isArray:false},
            update : {method:'PUT',isArray:false},
            getCategory:{ method:'GET',params:{operation:'getCategory'}, isArray:true },
            getCategoryName:{ method:'GET',params:{operation:'getCategoryName'}, isArray:false },
            getCategoryDocument  :{ method:'GET',params:{operation:'getCategoryDocument'}, isArray:false },
            getCategoryDocuments :{ method:'POST',params:{operation:'getCategoryDocuments'}, isArray:false },
            getCategoriesTemplate:{ method:'GET',params:{operation:'getCategoriesTemplate'}, isArray:true },
            getSectionForEachCategory:{ method:'GET',params:{operation:'getSectionForEachCategory'}, isArray:false },
            getCategoryFormsSections :{ method:'POST',params:{operation:'getCategoryFormsSections'}, isArray:false },
            getSponsorTemplates:{ method:'GET',params:{operation:'getSponsorTemplates'}, isArray:true },
            getSponsorReports:{ method:'GET',params:{operation:'getSponsorReports'}, isArray:true },
            getElements                  : { method:'GET' ,params:{operation:'getElements'}, isArray:true },
            updateElement                : { method:'PUT' ,params:{operation:'updateElement'}, isArray: false},
            getPriority                  : { method:'GET' ,params:{operation:'getPriority'}, isArray:false },
            forms :{ method:'GET',params:{operation:'forms'}, isArray:false },
            setForm :{ method:'PUT',params:{operation:'setForm'}, isArray:false },
            setFormStatus :{ method:'PUT',params:{operation:'setFormStatus'}, isArray:false },

            

        });
    });
