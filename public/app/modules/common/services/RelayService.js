
angular.module('CommonModule')
    .factory('Relays', function ($resource) {
        return $resource('/api/v1.0/common/Relays/:operation/:id', {id: '@id',page: '@page'}, {
            filter      : { method:'POST' ,params:{operation:'filter'}, isArray:false },
            confirmFilter      : { method:'POST' ,params:{operation:'confirmFilter'}, isArray:false },
            relayOne      : { method:'POST' ,params:{operation:'relayOne'}, isArray:false },
            confirmGroup      : { method:'POST' ,params:{operation:'confirmGroup'}, isArray:false },
            confirm      : { method:'POST' ,params:{operation:'confirm'}, isArray:false }
        });
    });

angular.module('CommonModule')
    .factory('Transfers', function ($resource) {
        return $resource('/api/v1.0/common/Transfers/:operation/:id', {id: '@id',page: '@page'}, {
            filter      : { method:'POST' ,params:{operation:'filter'}, isArray:false },
            confirmFilter      : { method:'POST' ,params:{operation:'confirmFilter'}, isArray:false },
            acceptedFilter       : { method:'POST' ,params:{operation:'acceptedFilter'}, isArray:false },

            acceptGroup      : { method:'POST' ,params:{operation:'acceptGroup'}, isArray:false },
            accept      : { method:'PUT' ,params:{operation:'accept'}, isArray:false },

            confirmGroup      : { method:'POST' ,params:{operation:'confirmGroup'}, isArray:false },
            confirm      : { method:'PUT' ,params:{operation:'confirm'}, isArray:false }
        });
    });

angular.module('CommonModule')
    .factory('CitizenRequests', function ($resource) {
        return $resource('/api/v1.0/citizens/request/:operation/:id', {id: '@id',page: '@page'}, {
            cases      : { method:'POST' ,params:{operation:'cases'}, isArray:false },
            status      : { method:'POST' ,params:{operation:'status'}, isArray:false },
            filter      : { method:'POST' ,params:{operation:'filter'}, isArray:false },
        });
    });

