angular.module('CommonModule')
    .factory('updateSetting', function ($resource) {
        return $resource('/api/v1.0/common/updateSetting/:operation/:id/', {id: '@id'}, {
            query  : { method:'GET' ,params:{page: '@page'}, isArray:false },
            update:{method: 'PUT'  , params: {id: '@id'}, isArray: false}

        });
    });