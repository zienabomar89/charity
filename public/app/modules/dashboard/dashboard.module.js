var DashboardModule = angular.module("DashboardModule", []);

DashboardModule.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    // Redirect any unmatched url
    $urlRouterProvider.otherwise("/dashboard");  
    
    $stateProvider
        // Dashboard
        .state('dashboard', {
            url: "/dashboard",
            templateUrl: '/app/modules/dashboard/views/index.html',
            data: {pageTitle: 'Dashboard'},
            controller: "DashboardController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'DashboardModule',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '/themes/metronic/global/plugins/morris/morris.css',                            
                            '/themes/metronic/global/plugins/morris/morris.min.js',
                            '/themes/metronic/global/plugins/morris/raphael-min.js',                            
                            '/themes/metronic/global/plugins/jquery.sparkline.min.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/themes/metronic/pages/scripts/dashboard.min.js',
                            '/app/modules/dashboard/controllers/DashboardController.js'
                        ] 
                    });
                }]
            }
        })
}]);