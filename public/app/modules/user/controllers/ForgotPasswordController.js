UserModule = angular.module('UserModule');

UserModule.controller('ForgotPasswordController', function ($scope, $http,$filter,$state) {

    $scope.sendEmail = function () {
        $http({
            method: 'POST',
            url: '/api/v1.0/password/email',
            data: {
                email: $scope.email
            }
        }).then(function (response) {
            $scope.success = true;
            $scope.failed = false;
            $scope.message = response.data.status;
        }, function (response) {
            $scope.success = false;
            $scope.failed = true;
            if (response.data.hasOwnProperty('errors')) {
                $scope.message = response.data.errors.email;
            } else {
                $scope.message = response.data.email;
            }
        });
    };
});