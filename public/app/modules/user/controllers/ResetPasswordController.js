UserModule = angular.module('UserModule');

UserModule.controller('ResetPasswordController', function ($scope, $http,$filter,$state) {
    $scope.success = false;
    $scope.failed = false;

    $state.current.data.pageTitle = $filter('translate')('reset_password');

    $scope.resetPassword = function (token) {
        $http({
            method: 'POST',
            url: '/api/v1.0/password/reset',
            data: {
                token: token,
                email: $scope.resetData.email,
                password: $scope.resetData.password,
                password_confirmation: $scope.resetData.password_confirmation
            }
        }).then(function (response) {
            window.location = '/account/login';
        }, function (response) {
            $scope.success = false;
            $scope.failed = true;
            if (response.data.hasOwnProperty('errors')) {
                 errors = response.data.errors;
            } else {
                errors = response.data;
            }
            $scope.message = "";
            angular.forEach(errors, function(v, k) {
                $scope.message += v + "\n";
            });
        });
    };
});