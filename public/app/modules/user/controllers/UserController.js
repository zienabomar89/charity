UserModule = angular.module('UserModule');

UserModule.controller('UserListController', function ($filter,$state, $rootScope, $scope, $http, User, Org,org_sms_service,$uibModal,$ngBootbox) {

    $rootScope.clearToastr();
    $rootScope.labels = {
        "itemsSelected": $filter('translate')('itemsSelected'),
        "selectAll": $filter('translate')('selectAll'),
        "unselectAll": $filter('translate')('unselectAll'),
        "search": $filter('translate')('search'),
        "select": $filter('translate')('select')
    }

    var AuthUser =localStorage.getItem("charity_LoggedInUser");
    $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
    $rootScope.UserType = $rootScope.charity_LoggedInUser.type;
    $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
    if($rootScope.charity_LoggedInUser.organization != null) {
        $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
    }
    $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;


    $rootScope.error = false;

    $trashed = false;
    $scope.openSearch = true;
    $scope.srch={};

    if ($state.current.data.mode && $state.current.data.mode == 'trashed') {
        $trashed = true;
        $state.current.data.pageTitle = $filter('translate')('Trashed_Users');
    }else{
        $state.current.data.pageTitle = $filter('translate')('users');
    }


    Org.all({type:'organizations'},function (response) {  $scope.organizations = response; });
    Org.descendants({type:'organizations'},function (response) {  $scope.Org = response; });


    var loadUsers = function (params) {

        var filters_ = angular.copy(params);

        $scope.users=[];
        if ($trashed) {
            filters_.id = 'trashed';
        }



        if( !angular.isUndefined(filters_.user_range)) {
            var userOrgRange=[];
            angular.forEach(filters_.user_range, function(v, k) {
                userOrgRange.push(v.id);
            });

            filters_.user_range=userOrgRange;
        }
        $rootScope.progressbar_start();
        User.filter(filters_, function (response) {
            $rootScope.progressbar_complete();
            $scope.users = response.data;

            $scope.totalItems = response.total;
            $scope.currentPage = response.current_page;
            $scope.itemsPerPage = response.per_page;
        });
    };

    $scope.pageChanged = function () {
        var params = $scope.srch || {};
        params.page = $scope.currentPage;
        loadUsers(params);
    };

    $scope.itemsPerPage_ = function () {
        var params = $scope.srch || {};
        params.itemsCount = $scope.itemsCount;
        loadUsers(params);
    };

    $scope.toggleSearch = function() {
        $scope.openSearch = $scope.openSearch == false ? true: false;
        if($scope.openSearch == true) {
            $scope.srch={};
            $scope.srch.page=1;
        }

    };
    $scope.search = function() {
        loadUsers($scope.srch);
    };

    $scope.deleteUser = function (user, idx) {
        $rootScope.clearToastr();
        $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
            .then(function() {
                $rootScope.progressbar_start();
                User.delete({id: user.id}, function () {
                    $rootScope.progressbar_complete();
                    $scope.users.splice(idx, 1);
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                });
            });

    };

    $scope.restoreUser = function (user, idx) {
        $rootScope.clearToastr();
        $ngBootbox.confirm($filter('translate')('are you want to confirm restore') )
            .then(function() {
                $rootScope.progressbar_start();
                User.restore({id: user.id}, function () {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                    // $state.go('auth-users-index');
                    loadUsers($scope.srch);
                });
            });
    };

    $scope.changeStatus = function (user) {
        if (user.status == 1) {
            action = 'deactivate';
        } else if (user.status == 2) {

            action = 'activate';
        }
        $rootScope.progressbar_start();
        User.updateStatus({id: user.id, action: action}, function (response) {
            $rootScope.progressbar_complete();
            user.status = response.status;
            user.status_name = response.status_name;
        });
    };


    $scope.selectedAll = function(value){
        if(value){
            $scope.selectAll = true;
        }
        else{
            $scope.selectAll =false;

        }
        angular.forEach($scope.users, function(val,key)
        {
            val.check=$scope.selectAll;
        });
    };

    $scope.sendSms=function(type,user_id) {
        $rootScope.clearToastr();
        if(type == 'manual'){
            var receipt = [];
            var pass = true;

            if(user_id ==0){
                angular.forEach($scope.users, function (v, k) {
                    if (v.check) {
                        receipt.push(v.id);
                    }
                });
                if(receipt.length==0){
                    pass = false;
                    $rootScope.toastrMessages('error',$filter('translate')("you did not select anyone to send them sms message"));
                    return;
                }
            }else{
                receipt.push(user_id);
            }
            if(pass){
                org_sms_service.getList(function (response) {
                    $rootScope.Providers = response.Service;
                });
                $uibModal.open({
                    templateUrl: '/app/modules/sms/views/modal/send_sms.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance) {

                        $scope.confirm = function (data) {
                            $rootScope.clearToastr();
                            var params = angular.copy(data);
                            params.target='users';
                            params.receipt=receipt;

                            $rootScope.progressbar_start();
                            org_sms_service.sendSmsToTarget(params,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed') {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status== 'success'){
                                    if(response.download_token){
                                        var file_type='xlsx';
                                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                    }

                                    $modalInstance.close();
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                            });
                        };


                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    }
                });

            }
        }
        else{
            org_sms_service.getList(function (response) {
                $rootScope.Providers = response.Service;
            });
            $uibModal.open({
                templateUrl: '/app/modules/sms/views/modal/sent_sms_from_xlx.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;
                    $scope.sms={source: 'file' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:""};

                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };

                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };
                    $scope.download=function(){
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-to-send-sms-template&template=true';
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: 'api/v1.0/sms/sent_sms/excelWithMobiles',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });


                    $scope.confirm = function (item) {
                        $scope.spinner=false;
                        $scope.import=false;
                        $rootScope.progressbar_start();
                        Uploader.onBeforeUploadItem = function(item) {

                            if($scope.sms.same_msg == 0){
                                $scope.sms.sms_messege = '';
                            }
                            item.formData = [$scope.sms];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);

                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);

                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            else if(response.status=='success')
                            {
                                if(response.download_token){
                                    var file_type='xlsx';
                                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                } 
                                $scope.spinner=false;
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $scope.import=false;
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                //$modalInstance.close();
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});

        }
    };

    loadUsers({page: 1});

});

UserModule.controller('OnlineUsersController', function ($filter,$state, $rootScope, $scope, $http, User,Entity, Org,org_sms_service,$uibModal,$ngBootbox,Mailling) {

    $rootScope.clearToastr();
    $rootScope.labels = {
        "itemsSelected": $filter('translate')('itemsSelected'),
        "selectAll": $filter('translate')('selectAll'),
        "unselectAll": $filter('translate')('unselectAll'),
        "search": $filter('translate')('search'),
        "select": $filter('translate')('select')
    }

    var AuthUser =localStorage.getItem("charity_LoggedInUser");
    $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
    $rootScope.UserType = $rootScope.charity_LoggedInUser.type;
    $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
    if($rootScope.charity_LoggedInUser.organization != null) {
        $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
    }
    $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;

    $rootScope.error = false;

    $trashed = false;
    $scope.openSearch = true;
    $scope.srch={};
    $scope.totalItems = 0;
    $scope.disblay_orgs =false;
    $scope.Org_ =[];

    if ($state.current.data.mode && $state.current.data.mode == 'trashed') {
        $trashed = true;
        $state.current.data.pageTitle = $filter('translate')('Trashed_Users');
    }else{
        $state.current.data.pageTitle = $filter('translate')('users');
    }


    Org.all({type:'organizations'},function (response) {  $scope.organizations = response; });
    Org.descendants({type:'organizations'},function (response) {  $scope.Org = response; });
    Entity.get({entity:'entities',c:'organizationsCat'},function (response) {
        $rootScope.organizationsCat = response.organizationsCat;
    });


    $scope.resetFilters = function () {
        $scope.srch={};
        $scope.disblay_orgs =false;
        $scope.Org_ =[];
    };

    $scope.resetOrg=function(organization_category){
        $rootScope.clearToastr();
        $scope.disblay_orgs =false;
        $scope.Org_ =[];
        $scope.disblay_orgs =false;
        let ids = [];
        angular.forEach(organization_category, function(v, k) {
            ids.push(v.id);
        });

        Org.ListOfCategory({ids:ids},function (response) {
            if (response.list.length > 0){
                $scope.Org_ = response.list;
                $scope.disblay_orgs =true;
            }else{
                $scope.Org_ = [];
                $scope.disblay_orgs =false;
                $rootScope.toastrMessages('error','لا يوجد جمعيات تابعة للتصنيفات المحددة');
            }
        });

    };

    var loadUsers = function (params) {

        var filters_ = angular.copy(params);

        $scope.users=[];
        if ($trashed) {
            filters_.id = 'trashed';
        }

        if( !angular.isUndefined(filters_.organization_category_id)) {
            var organization_category_id = [];
            angular.forEach(filters_.organization_category_id, function (v, k) {
                organization_category_id.push(v.id);
            });
            filters_.organization_category_id = organization_category_id;
        }


        if( !angular.isUndefined(filters_.organization_id)) {
            var organizations = [];
            angular.forEach(filters_.organization_id, function (v, k) {
                organizations.push(v.id);
            });
            filters_.organization_id = organizations;
        }

        if( !angular.isUndefined(filters_.user_range)) {
            var userOrgRange=[];
            angular.forEach(filters_.user_range, function(v, k) {
                userOrgRange.push(v.id);
            });

            filters_.user_range=userOrgRange;
        }
        $rootScope.progressbar_start();
        filters_.online='true';
        User.online(filters_, function (response) {
            $rootScope.progressbar_complete();
            $scope.users = response.data;

            $scope.totalItems = response.total;
            $scope.currentPage = response.current_page;
            $scope.itemsPerPage = response.per_page;
        });
    };

    $scope.pageChanged = function () {
        var params = $scope.srch || {};
        params.page = $scope.currentPage;
        loadUsers(params);
    };

    $scope.itemsPerPage_ = function () {
        var params = $scope.srch || {};
        params.itemsCount = $scope.itemsCount;
        loadUsers(params);
    };

    $scope.toggleSearch = function() {
        $scope.openSearch = $scope.openSearch == false ? true: false;
        if($scope.openSearch == true) {
            $scope.srch={};
            $scope.srch.page=1;
        }

    };

    $scope.search = function() {
        loadUsers($scope.srch);
    };

    $scope.selectedAll = function(value){
        if(value){
            $scope.selectAll = true;
        }
        else{
            $scope.selectAll =false;

        }
        angular.forEach($scope.users, function(val,key)
        {
            val.check=$scope.selectAll;
        });
    };

    $scope.sendSms=function(type,user_id) {
        $rootScope.clearToastr();
        if(type == 'manual'){
            var receipt = [];
            var pass = true;

            if(user_id ==0){
                angular.forEach($scope.users, function (v, k) {
                    if (v.check) {
                        receipt.push(v.user_id);
                    }
                });
                if(receipt.length==0){
                    pass = false;
                    $rootScope.toastrMessages('error',$filter('translate')("you did not select anyone to send them sms message"));
                    return;
                }
            }else{
                receipt.push(user_id);
            }
            if(pass){
                org_sms_service.getList(function (response) {
                    $rootScope.Providers = response.Service;
                });
                $uibModal.open({
                    templateUrl: '/app/modules/sms/views/modal/send_sms.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance) {

                        $scope.confirm = function (data) {
                            $rootScope.clearToastr();
                            var params = angular.copy(data);
                            params.target='users';
                            params.receipt=receipt;

                            $rootScope.progressbar_start();
                            org_sms_service.sendSmsToTarget(params,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed') {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status== 'success'){
                                    if(response.download_token){
                                        var file_type='xlsx';
                                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                    }

                                    $modalInstance.close();
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                            });
                        };


                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    }
                });

            }
        }
        else{
            org_sms_service.getList(function (response) {
                $rootScope.Providers = response.Service;
            });
            $uibModal.open({
                templateUrl: '/app/modules/sms/views/modal/sent_sms_from_xlx.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;
                    $scope.sms={source: 'file' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:""};

                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };

                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };
                    $scope.download=function(){
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-to-send-sms-template&template=true';
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: 'api/v1.0/sms/sent_sms/excelWithMobiles',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });


                    $scope.confirm = function (item) {
                        $scope.spinner=false;
                        $scope.import=false;
                        $rootScope.progressbar_start();
                        Uploader.onBeforeUploadItem = function(item) {

                            if($scope.sms.same_msg == 0){
                                $scope.sms.sms_messege = '';
                            }
                            item.formData = [$scope.sms];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);

                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);

                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            else if(response.status=='success')
                            {
                                if(response.download_token){
                                    var file_type='xlsx';
                                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                }
                                $scope.spinner=false;
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $scope.import=false;
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                //$modalInstance.close();
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});

        }
    };

    $scope.sendInternal=function(selected) {
        $rootScope.clearToastr();

        var pass = true;
        var users=[];
        if(selected == true || selected == 'true' ){
            angular.forEach($scope.users, function(v, k){
                if(v.check){
                    users.push(v.user_id);
                }

            });

            if(users.length==0){
                pass = false ;
                $rootScope.toastrMessages('error',$filter('translate')('You have not select users to send internal message'));
                return ;
            }
        }

        if (pass){
            $uibModal.open({
                templateUrl: '/app/modules/user/views/user/modal/send_internal.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, Mailling) {

                    $scope.row = {};


                    $scope.confirm = function (row) {

                        row.online = true;
                        row.selected = selected;
                        row.users_to = users;
                        if(row.details != undefined && row.subject != undefined){
                            $rootScope.progressbar_start();
                            Mailling.store(row,function(response) {
                                $rootScope.progressbar_complete();
                                if(response.success){
                                    $modalInstance.close();
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $modalInstance.close();
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            });
                        }else{
                            $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                        }

                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };


                }});
        }
    };

    $scope.sendInternalSingle=function(id) {
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: '/app/modules/user/views/user/modal/send_internal.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, Mailling) {

                $scope.row = {};

                $scope.confirm = function (row) {
                    row.online = true;
                    row.selected = true;
                    row.users_to = [id];
                    if(row.details != undefined && row.subject != undefined){
                        $rootScope.progressbar_start();
                        Mailling.store(row,function(response) {
                            $rootScope.progressbar_complete();
                            if(response.success){
                                $modalInstance.close();
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            }else{
                                $modalInstance.close();
                                $rootScope.toastrMessages('error',response.msg);
                            }
                        });
                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                    }

                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };


            }});

    };

    $scope.logOutGroup=function(selected) {
        $rootScope.clearToastr();

        var pass = true;
        var users=[];
        if(selected == true || selected == 'true' ){
            angular.forEach($scope.users, function(v, k){
                if(v.check){
                    users.push(v.user_id);
                }
            });

            if(users.length==0){
                pass = false ;
                $rootScope.toastrMessages('error',$filter('translate')('You have not select users to logout'));
                return ;
            }
        }

        if (pass){
            $ngBootbox.confirm($filter('translate')('are you want to confirm Logout users') )
                .then(function() {
                    $rootScope.progressbar_start();
                    User.logOutGroup({selected: selected , users: users}, function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            loadUsers($scope.srch);
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
            });
        }
    };

    $scope.LogoutUser = function (user, idx) {
        $rootScope.clearToastr();
        $ngBootbox.confirm($filter('translate')('are you want to Logout selected user') )
            .then(function() {
                $rootScope.progressbar_start();
                User.logoutUser({id: user.user_id}, function () {
                    $rootScope.progressbar_complete();
                    $scope.users.splice(idx, 1);
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                });
            });

    };

    loadUsers({page: 1});

});

UserModule.controller('UserViewController', function ($scope, $stateParams, User) {
        $scope.user = User.get({id: $stateParams.id});
    });

UserModule.controller('UserCreateController', function ($rootScope, $scope, $state, $stateParams, User, Org, Role,$filter) {
    $state.current.data.pageTitle = $filter('translate')('new user');

    var AuthUser =localStorage.getItem("charity_LoggedInUser");
    $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
    $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
    if($rootScope.charity_LoggedInUser.organization != null) {
        $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
    }
    $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;
    $rootScope.connectWithSponsor = $rootScope.charity_LoggedInUser.connect_with_sponsor;

    $rootScope.error = false;

    $rootScope.progressbar_start();
    Org.all(function (response) {
        $rootScope.progressbar_complete();
        $scope.organizations = response;
        $scope.organizations_level_map=[];
        $scope.organizations_type_map=[];
        angular.forEach($scope.organizations, function(v, k) {
            $scope.organizations_level_map[v.id] = v.level;
            $scope.organizations_type_map[v.id] = v.type;
        });
    });

    Org.descendants({type:'organizations'},function (response) {  $scope.Org = response; });


    $scope.roles = Role.query();
    $scope.user = new User();

    $scope.submit = function () {
        $rootScope.clearToastr();
        var user = angular.copy($scope.user);

        if(user.type == 2){
            if( !angular.isUndefined(user.user_range)) {
                var orgs=[];
                angular.forEach(user.user_range, function(v, k) {
                    orgs.push(v.id);
                });

                user.user_range=orgs;
            }
        }

        $rootScope.progressbar_start();
        User.save(user, function (response) {
            $rootScope.progressbar_complete();
            if(response.status=='failed_valid')
            {
                var msg = '';
                angular.forEach(response.msg, function(v, k) {
                    msg += $filter('translate')(k) + "  : " +v +'<br>'
                });
               $rootScope.toastrMessages('error',msg);
            }else{
                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                $state.go('auth-users-index');
            }
        });
    };

    $scope.resetSelected = function () {
        $scope.user.type = "";
    }
});

UserModule.controller('UserEditController', function ($rootScope, $scope, $state, $stateParams, User, Role,$filter,Org) {

    
    $rootScope.error = false;
    $state.current.data.pageTitle = $filter('translate')('edit user');
    var AuthUser =localStorage.getItem("charity_LoggedInUser");
    $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
    $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
    if($rootScope.charity_LoggedInUser.organization != null) {
        $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
    }
    $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;
    $rootScope.connectWithSponsor = $rootScope.charity_LoggedInUser.connect_with_sponsor;

    $rootScope.progressbar_start();
    Org.all(function (response) {
        $rootScope.progressbar_complete();
        $scope.organizations = response;
        $scope.organizations_level_map=[];
        $scope.organizations_type_map=[];
        angular.forEach($scope.organizations, function(v, k) {
            $scope.organizations_level_map[v.id] = v.level;
            $scope.organizations_type_map[v.id] = v.type;
        });
    });

    Org.descendants({type:'organizations'},function (response) {  $scope.Org = response; });


    $scope.submit = function () {
        $rootScope.clearToastr();
        var user = angular.copy($scope.user);
        if(user.type == 2){
            if(!angular.isUndefined(user.user_range)) {
                var orgs=[];
                angular.forEach(user.user_range, function(v, k) {
                    orgs.push(v.id);
                });

                user.user_range=orgs;
            }
        }

        $rootScope.progressbar_start();
        User.update(user, function (response) {
            $rootScope.progressbar_complete();

            if(response.status=='failed_valid')
            {
                var msg = '';
                angular.forEach(response.msg, function(v, k) {
                    msg += $filter('translate')(k) + "  : " +v +'<br>'
                });
                $rootScope.toastrMessages('error',msg);


            }else{
                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                $state.go('auth-users-index');
            }
        });
    };

    $scope.loadUser = function () {
        $scope.roles = Role.query();
        $scope.user = User.get({id: $stateParams.id});
    };

    $scope.loadUser();
    $scope.resetSelected = function () {
        $scope.user.type = "";
    }
});

UserModule.controller('ResetPasswordController', function ($rootScope, $scope, $state, $stateParams, User, Password,$filter) {
    $state.current.data.pageTitle = $filter('translate')('reset_password');

    
    $rootScope.error = false;

    $scope.errorClass = function (name) {
        var s = $scope.form[name];
        if (s === undefined) {
            return;
        }
        return s.$invalid && s.$dirty ? "has-error" : "";
    };

    $scope.errorMessage = function (name) {
        result = [];
        s = $scope.form[name];
        if (s === undefined) {
            return;
        }
        angular.forEach(s.$error, function (key, value) {
            result.push(value);
        });
        return result.join(", ");
    };
    $scope.submit = function () {
        $rootScope.clearToastr();
        if ($scope.password.password != $scope.password.password_confirmation) {
            $scope.form['password_confirmation'].$dirty = true;
            $scope.form['password_confirmation'].$setValidity('no match', false);
            return;
        }
        $rootScope.progressbar_start();
        Password.update({
            id: $scope.user.id,
            password: $scope.password.password,
            password_confirmation: $scope.password.password_confirmation
        }, function (response) {
            $rootScope.progressbar_complete();
            if(response.status=='error' || response.status=='failed')
            {
                $rootScope.toastrMessages('error',response.msg);
            }
            else if(response.status=='failed_valid')
            {
                $scope.status1 =response.status;
                $scope.msg1 =response.msg;
            }
            else{
                if(response.status=='success')
                {
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                }else{
                    $rootScope.toastrMessages('error',response.msg);
                }
            }
     //       $rootScope.progressbar_complete();
       //     $rootScope.toastrMessages('success',$filter('translate')('action success'));
        }, function (error) {
            $rootScope.progressbar_complete();
            angular.forEach(error.data, function (errors, key) {
                angular.forEach(errors, function (e) {
                    $scope.form[key].$dirty = true;
                    $scope.form[key].$setValidity(e, false);
                });
            });
        });
    };

    $scope.loadUser = function () {
        $scope.user = User.get({id: $stateParams.id});
    };

    $scope.loadUser();
});

UserModule.controller('ChangePasswordController', function ($rootScope, $scope, Password,$filter,$state) {
    $state.current.data.pageTitle = $filter('translate')('change_password');
    $scope.errorClass = function (name) {
        var s = $scope.form[name];
        if (s === undefined) {
            return;
        }
        return s.$invalid && s.$dirty ? "has-error" : "";
    };

    $scope.errorMessage = function (name) {
        result = [];
        s = $scope.form[name];
        if (s === undefined) {
            return;
        }
        angular.forEach(s.$error, function (key, value) {
            result.push(value);
        });
        return result.join(", ");
    };
    $scope.submit = function () {
        $rootScope.clearToastr();
        if ($scope.password.password != $scope.password.password_confirmation) {
            $scope.form['password_confirmation'].$dirty = true;
            $scope.form['password_confirmation'].$setValidity('no match', false);
            return;
        }
        $rootScope.progressbar_start();
        Password.update($scope.password, function (response) {

            $rootScope.progressbar_complete();
            if(response.status=='error' || response.status=='failed')
            {
                $rootScope.toastrMessages('error',response.msg);
            }
            else if(response.status=='failed_valid')
            {
                $scope.status1 =response.status;
                $scope.msg1 =response.msg;
            }
            else{
                if(response.status=='success')
                {
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                }else{
                    $rootScope.toastrMessages('error',response.msg);
                }
            }

            // $rootScope.progressbar_complete();
            // $rootScope.toastrMessages('success',$filter('translate')('action success'));
        }, function (error) {
            angular.forEach(error.data, function (errors, key) {
                $rootScope.progressbar_complete();
                angular.forEach(errors, function (e) {
                    $scope.form[key].$dirty = true;
                    $scope.form[key].$setValidity(e, false);
                });
            });
        });
    };

    $scope.loadUser = function () {
        $scope.user = $rootScope.charity_LoggedInUser;
    };

    $scope.loadUser();
});

UserModule.controller('AccountController', function ($rootScope, $scope, $stateParams, User,$filter,$state,$uibModal,OAuthToken,FileUploader,$http) {
    $state.current.data.pageTitle = $filter('translate')('account_information');

    $scope.submit = function () {
        $scope.status1={};
        $scope.msg1={};
        $rootScope.clearToastr();
        $rootScope.progressbar_start();
        User.updateProfile($scope.user, function (response) {
            $rootScope.progressbar_complete();
            if(response.status=='error' || response.status=='failed')
            {
                $rootScope.toastrMessages('error',response.msg);
            }
            else if(response.status=='failed_valid')
            {
                window.scrollTo(30,30);
                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                $scope.status1 =response.status;
                $scope.msg1 =response.msg;
            }
            else{
                $rootScope.toastrMessages('success',$filter('translate')('action success'));
            }

        }, function (error) {
            $rootScope.progressbar_complete();
            angular.forEach(error.data, function (errors, key) {
                angular.forEach(errors, function (e) {
                    $scope.form[key].$dirty = true;
                    $scope.form[key].$setValidity(e, false);
                });
            });
        });
    };

    $rootScope.loadUser = function () {

        if($rootScope.charity_LoggedInUser == undefined){
            var AuthUser =localStorage.getItem("charity_LoggedInUser");
            $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
            $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
            if($rootScope.charity_LoggedInUser.organization != null) {
                $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
            }
            $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;
        }
        $rootScope.user = User.get({id: $rootScope.charity_LoggedInUser.id});
    };


$rootScope.loadUser();

  $scope.ChooseAttachment=function(item,action){

            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'myModalContent2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $rootScope.ddd = false;
                    $scope.fileupload = function () {
                        if($scope.uploader.queue.length){
                            var image = $scope.uploader.queue.slice(1);
                            $scope.uploader.queue = image;
                        }
                    };
                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/doc/files',
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });
                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.progressbar_start();
                        item.formData = [{action:"check"}];
                        $rootScope.clearToastr();
                    };
                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        $rootScope.user.document_id = response.id;
                         if(response.status=='error')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else{
                        if(response.id){
                            User.setImage({id:response.id},function (response2) {
                                if(response2.status=='error' || response2.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response2.msg);
                                }
                                else if(response2.status== 'success'){


                                    $http({
                                        method: 'GET',
                                        url: 'doc/files/userImage',
                                        responseType: 'arraybuffer',
                                        cache: false
                                    }).then(function (response) {
                                        // $rootScope.UserImage =url;
                                        // // angular.element('#headerImg').attr('src',url);
                                        //
                                        var blob = new Blob([response.data], {type: response.headers('Content-Type')});
                                        url = (window.URL || window.webkitURL).createObjectURL(blob);
                                        angular.element('#img').attr('src',url);
                                        $rootScope.imgSrc_ =url;
                                        $rootScope.imgSrc =url;
                                        $rootScope.UserImage =url;
                                    }, function (response) {
                                    });

                                    $modalInstance.close();
                                    $rootScope.toastrMessages('success',response2.msg);
                                    // window.location = '/account';
                                    // $rootScope.loadUser();
                                }

                            }, function(error) {
                                $rootScope.toastrMessages('error',error.data.msg);
                            });
                        }else{
                            $modalInstance.close();
                            $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                        }
                    }};
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

});

UserModule.controller('UserEditPermissionsController', function ($rootScope, $scope, $state, $stateParams, User, Permission,$filter) {

    $state.current.data.pageTitle = $filter('translate')('edit permissions');
    $scope.submit = function () {
        $rootScope.clearToastr();
        allow=[];deny=[];
        angular.forEach($scope.modules, function(v, k) {
            angular.forEach(v.permissions, function(v2, k2) {
                a=parseInt(v2.allow);
                if(a === 1) {
                    allow.push(v2.id);
                } else if(a === 0) {
                    deny.push(v2.id);
                }
            });
        });

        $rootScope.progressbar_start();
        User.updatePermissions({id: $stateParams.id, allow: allow, deny: deny}, function () {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('success',$filter('translate')('action success'));
            $state.go('auth-users-index');
        }, function (response) {
            $rootScope.progressbar_complete();
            angular.forEach(response.data, function (errors, key) {
                angular.forEach(errors, function (e) {
                    $scope.form[key].$dirty = true;
                    $scope.form[key].$setValidity(e, false);
                });
            });
        });
    };

    $scope.loadUserPermissions = function () {
        $scope.modules = Permission.query({user_id: $stateParams.id});
    };

    $scope.checkAll = function(v, i) {
        angular.forEach(i.permissions, function (item) {
            item.allow = v;
        });
    };

    $scope.loadUserPermissions();
});

UserModule.controller('UserPermissionsController', function ($scope,$rootScope, $stateParams, User,$filter,$state) {
    $state.current.data.pageTitle = $filter('translate')('permissions');
    $scope.permissions=[];
    $scope.user = User.get({id: $stateParams.id});
    $rootScope.progressbar_start();
    User.permissions({id: $stateParams.id},function (response) {
        $rootScope.progressbar_complete();
        if(response.length != 0){
            $scope.permissions = response;
        }

    });

});