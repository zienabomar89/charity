UserModule = angular.module('UserModule');

UserModule.controller('RolesController', function ($rootScope, $scope, popupService, Role, Org,$filter,$state,$ngBootbox,$http,$timeout,$uibModal,$log) {


    $scope.roles=[];

    $rootScope.progressbar_start();
    Role.query({type: 'organizations'}, function(response) {
        $rootScope.progressbar_complete();
        $scope.roles = response;
    });
    $scope.organizations = [];
    $state.current.data.pageTitle = $filter('translate')('roles');
    
    $scope.deleteRole = function (role, idx) {
        $rootScope.clearToastr();
        $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
            .then(function() {
                $rootScope.progressbar_start();
                role.$delete(function () {
                    $rootScope.progressbar_complete();
                    $scope.roles.splice(idx, 1);
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                });
            });
    };
    
    $rootScope.cOrganizations = Org.list({type: 'organizations'});
    var gOrganizations = [];
    $scope.getOrganizations = function (open, role) {

        if (!open) {
            angular.forEach(role.organizations, function (item) {
                item.checked = false;
            });
            return;
        }
        if (gOrganizations.length == 0) {
            $rootScope.progressbar_start();
            Org.list({type: 'organizations'}, function(response) {
                $rootScope.progressbar_complete();
                role.organizations = gOrganizations = response;
            });
        }
        
        role.organizations = gOrganizations;
    };
    
    $scope.copyRole = function(role) {
        $rootScope.copy_role= role ;
        $uibModal.open({
                    templateUrl: '/app/modules/user/views/roles/copy.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'md',
                    controller: function ($rootScope,$scope, $modalInstance, $log) {
                        
                        $scope.row = {};
                    
                        $scope.copy = function(row) {
                            $rootScope.clearToastr();
                            var organizations = [];
                            angular.forEach(row.organizations, function (item) {
                                organizations.push(item.id);
                            });
                            $rootScope.progressbar_start();
                            Role.copy({id: role.id, organization_id: organizations}, function () {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            });
                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    }
                });

    
    };
    
    $scope.copy = function(role) {
        $rootScope.clearToastr();
        var organizations = [];
        angular.forEach(role.organizations, function (item) {
            if(item.checked) {
                organizations.push(item.id);
            }
        });
        $rootScope.progressbar_start();
        Role.copy({id: role.id, organization_id: organizations}, function () {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('success',$filter('translate')('action success'));
        });
    };
});
        
UserModule.controller('RoleViewController', function ($scope, $stateParams, Role) {
    $scope.role = Role.get({id: $stateParams.id});
});

UserModule.controller('RoleUsersController', function ($scope,$rootScope, $stateParams, Role,$filter,$state) {
    $state.current.data.pageTitle = $filter('translate')('roles') + " / "+ $filter('translate')('users');
    $rootScope.progressbar_start();
    $scope.role = Role.get({id: $stateParams.id});
    $scope.users = Role.users({id: $stateParams.id});
    $rootScope.progressbar_complete();

});
        
UserModule.controller('RoleCreateController', function ($rootScope, $scope, $state, $stateParams, Role, Permission, AppModule,$filter) {
    $state.current.data.pageTitle = $filter('translate')('new role');
    $scope.modules=[];
    /*$scope.errorClass = function(name) {
            var s = $scope.form[name];
            if (s === undefined) {
                return;
            }
            return s.$invalid && s.$dirty ? "has-error" : "";
        };

        $scope.errorMessage = function(name) {
            result = [];
            s = $scope.form[name];
            if (s === undefined) {
                return;
            }
            angular.forEach(s.$error, function (key, value) {
                result.push(value);
            });
            return result.join(", ");
        };*/
    
    //$scope.permissions = Permission.query();
    $scope.permissions = new Array();
    /*$scope.modules = AppModule.query();
    $scope.getModulePermissions = function(model) {
        if (!$scope.permissions[model.id]) {
            $scope.permissions[model.id] = Permission.query({
                module_id: model.id
            });
        }
    }*/
    $scope.modules = Permission.query();
    
    $scope.checkAll = function(v, i) {
        angular.forEach(i.permissions, function (item) {
            item.allow = v;
        });
    };
    $scope.role = new Role();
    $scope.submit = function () {
        $rootScope.clearToastr();
        var allow=[];
        var deny=[];
        angular.forEach($scope.modules, function(v, k) {
            angular.forEach(v.permissions, function(v2, k2) {
                a=parseInt(v2.allow);
                if(a === 1) {
                    allow.push(v2.id);
                } else if(a === 0) {
                    deny.push(v2.id);
                }
            });
        });

        $scope.role.allow = allow;
        $scope.role.deny = deny;
        $rootScope.progressbar_start();
        $scope.role.$save($scope.role, function () {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('success',$filter('translate')('action success'));
            $state.go('auth-roles-index');
        }, function (response) {
            $rootScope.progressbar_complete();
            angular.forEach(response.data, function (errors, key) {
                angular.forEach(errors, function (e) {
                    $scope.form[key].$dirty = true;
                    $scope.form[key].$setValidity(e, false);
                });
            });
        });
    };
});
        
UserModule.controller('RoleEditController', function ($rootScope, $scope, $state, $stateParams, Role, Permission, AppModule,$filter) {
    $state.current.data.pageTitle = $filter('translate')('edit role');
    $scope.errorClass = function(name) {
        return;
        var s = $rootScope.form[name];
        if (s === undefined) {
            return;
        }
        return s.$invalid && s.$dirty ? "has-error" : "";
    };

    $scope.errorMessage = function(name) {
        result = [];
        return;
        s = $rootScope.form[name];
        if (s === undefined) {
            return;
        }
        angular.forEach(s.$error, function (key, value) {
            result.push(value);
        });
        return result.join(", ");
    };
    
    $scope.submit = function () {
        $rootScope.clearToastr();
        allow=[];deny=[];
        angular.forEach($scope.modules, function(v, k) {
            angular.forEach(v.permissions, function(v2, k2) {
                a=parseInt(v2.allow);
                if(a === 1) {
                    allow.push(v2.id);
                } else if(a === 0) {
                    deny.push(v2.id);
                }
            });
        });
        
        $scope.role.allow = allow;
        $scope.role.deny = deny;
        $rootScope.progressbar_start();
        $scope.role.$update($scope.role, function () {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('success',$filter('translate')('action success'));
            $state.go('auth-roles-index');
        }, function (response) {
            $rootScope.progressbar_complete();
            angular.forEach(response.data, function (errors, key) {
                angular.forEach(errors, function (e) {
                    $scope.form[key].$dirty = true;
                    $scope.form[key].$setValidity(e, false);
                });
            });
        });
    };

    $scope.loadRole = function () {
        $scope.role = Role.get({id: $stateParams.id});
        $scope.permissions = new Array();
        $scope.modules = Permission.query({role_id: $stateParams.id});
        //$scope.permissions = Permission.query({role_id: $stateParams.id});
        //$scope.modules = AppModule.query();
        $scope.role.permissions = [];
    };
    
    $scope.getModulePermissions = function(model) {
        if (!$scope.permissions[model.id]) {
            $scope.permissions[model.id] = Permission.query({
                module_id: model.id,
                role_id: $stateParams.id
            });
        }
    };
    
    $scope.checkAll = function(v, i) {
        angular.forEach(i.permissions, function (item) {
            item.allow = v;
        });
    };

    $scope.loadRole();
});