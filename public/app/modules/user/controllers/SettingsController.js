UserModule = angular.module('UserModule');

UserModule.controller('AuthSettingsController', function ($scope, $http,$filter,$state,$rootScope,User,$ngBootbox) {


    $scope.success = false;
    $scope.error = false;
    $state.current.data.pageTitle = $filter('translate')('Settings');

    $scope.submit = function () {
        $rootScope.clearToastr();
        $rootScope.progressbar_start();
        $http({
            method: 'POST',
            url: '/api/v1.0/auth/settings',
            data: $scope.settings
        }).then(function (response) {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('success',$filter('translate')('action success'));
        }, function (error) {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('error',error.data.msg);
        });
    };


    $scope.resetApiToken = function () {
        $rootScope.clearToastr();
        $ngBootbox.confirm($filter('translate')('are you want to confirm reset api token') )
            .then(function() {
                $rootScope.progressbar_start();
                User.resetApiToken(function (response) {
                    $rootScope.progressbar_complete();

                    if(response.status=='failed') {
                        $rootScope.toastrMessages('error',response.msg);
                    }
                    else if(response.status== 'success'){
                        $scope.settings.gov_service_client_authorization = response.token;
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                    }

                });
            });

    };

    $rootScope.progressbar_start();
    $http({
        method: 'GET',
        url: '/api/v1.0/auth/settings',
        data: $scope.settings
    }).then(function (response) {
        $rootScope.progressbar_complete();
        $scope.settings = {};
        angular.forEach(response.data, function(item) {
            if (item.id =='auth_throttlesLogins_maxAttempts' || item.id == 'auth_throttlesLogins_decayMinutes') {
                $scope.settings[item.id] = parseInt(item.value);
            }else if (item.id =='gov_service_expires_in') {

                console.log(item.value);
                var date = new Date(item.value);
                console.log(date);
                $scope.settings[item.id] = date;

                // // Split the string into date and time parts
                // var parts = dateTimeString.split(' ');
                // var datePart = parts[0];
                // var timePart = parts[1];
                //
                // // Split the date and time parts into their components
                // var dateComponents = datePart.split('-');
                // var timeComponents = timePart.split(':');
                //
                // // Create a JavaScript Date object using the components
                // var year = parseInt(dateComponents[0]);
                // var month = parseInt(dateComponents[1]) - 1; // Months are 0-based in JavaScript
                // var day = parseInt(dateComponents[2]);
                // var hour = parseInt(timeComponents[0]);
                // var minute = parseInt(timeComponents[1]);
                // var second = parseInt(timeComponents[2]);
                //
                // var parsedDate = new Date(year, month, day, hour, minute, second);

                console.log($scope.settings[item.id]);
            }else{
                $scope.settings[item.id] = item.value +"";
            }
        });

    });


    $scope.resetToken = function () {
        $rootScope.clearToastr();
        $rootScope.progressbar_start();
        $http({
            method: 'POST',
            url: '/api/v1.0/auth/settings',
            data: $scope.settings
        }).then(function (response) {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('success',$filter('translate')('action success'));
        }, function (error) {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('error',error.data.msg);
        });
    };

});