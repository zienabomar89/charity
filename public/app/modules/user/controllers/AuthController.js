UserModule = angular.module('UserModule');
UserModule.directive('ngEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if(event.which === 13) {
                scope.$apply(function(){
                    scope.$eval(attrs.ngEnter, {'event': event});
                });

                event.preventDefault();
            }
        });
    };
});

UserModule.controller('UserLoginController', function ($scope, OAuth,$filter,$state,$rootScope) {

    $scope.try=false;
    var d = new Date();
    $rootScope.Curyear = d.getFullYear();

    $scope.login_ = function () {
        $scope.failed = false;

        $scope.login_form.$invalid=true;
        $scope.try=false;
        $scope.login_form.$valid=false;

        OAuth.getAccessToken({username: $scope.username, password: $scope.password}).then(function (response) {
            window.location = '/';
        }).catch(function (response) {

            $scope.login_form.$invalid=false;
            $scope.try=false;
            $scope.login_form.$valid=true;


            // $scope.failed = true;
        });
    };


    $scope.login = function () {
        $scope.failed = false;

        $scope.login_form.$invalid=true;
        $scope.try=false;
        $scope.login_form.$valid=false;
        var row = {};

        row.username = angular.copy($scope.username);
        row.password = angular.copy($scope.password);
        var pass = true ;

        if(angular.isUndefined(row.username)|| angular.isUndefined(row.password)){
            pass = false ;
        }else{
            if(row.username == '' || row.username == ' ' ||
                row.username == 'null' || row.username == null ){
                pass = false ;
            }

            if(row.password == '' || row.password == ' ' ||
                row.password == 'null' || row.password == null ){
                pass = false ;
            }
        }

        if(pass){
            OAuth.getAccessToken(row).then(function (response) {
                window.location = '/';
            }).catch(function (response) {

                $scope.login_form.$invalid=false;
                $scope.try=false;
                $scope.login_form.$valid=true;

                if (response.data.error === 'invalid_credentials') {
                    $rootScope.toastrMessages('error',$filter('translate')('name and password not the same'));
                } else {
                    $rootScope.toastrMessages('error',response.data.message);
                    // $scope.message = response.data.message;
                }
                // $scope.failed = true;
            });
        }else{
            $scope.toastrMessages('error','كلمة المرور أو اسم المستخدم غير مدخل ');
        }

    };
    $scope.forget=function () {
        jQuery('.login-form').hide();
        jQuery('.forget-form').show();
        $scope.email=null;
        $scope.message={};
        $scope.failed = false;

    }
    $rootScope.clearToastr =function () {
        angular.element('.btn').removeClass("disabled");
        angular.element('.btn').removeAttr('disabled');
        toastr.remove();
        toastr.clear();
    };

    $rootScope.toastrMessages =function (type,message) {
        angular.element('.btn').removeClass("disabled");
        angular.element('.btn').removeAttr('disabled');
        toastr.remove();
        toastr.options = {
            tapToDismiss: false
            , timeOut: 0
            , extendedTimeOut: 0
            , allowHtml: true
            , preventDuplicates: false
            , preventOpenDuplicates: false
            , newestOnTop: true
            , closeButton: true
            , closeHtml: '<button><i class="icon-off pull-right"></i></button>'
        };
        switch(type) {
            case 'error':
                toastr.error(message);
                break;
            case 'warning':
                toastr.warning(message);
                break;
            default:
                toastr.success(message);
        }
        // toastr.clear();
    };

});