angular.module('UserModule', [
    "ngCookies",
    "ui.router",
    "ngResource",
    "angular-oauth2",
    "oc.lazyLoad",
    "pascalprecht.translate"
]);

UserModule = angular.module('UserModule');

UserModule.config(function ($translateProvider) {
    if($translateProvider.preferredLanguage() === 'ar') {
        return;
    }
    
    $translateProvider.useSanitizeValueStrategy(null);
    $translateProvider.preferredLanguage('ar');
    
    $translateProvider.useStaticFilesLoader({
        files: [{
            prefix: '/app/modules/user/i18n/',
            suffix: '.json'
        }]
    });
});

UserModule.config(['OAuthProvider', 'OAuthTokenProvider', '$cookiesProvider', function (OAuthProvider, OAuthTokenProvider, $cookiesProvider) {
        $cookiesProvider.defaults.path = '/';

        OAuthTokenProvider.configure({
            name: 'access_token',
            options: {
                secure: false
            }
        });
        
        OAuthProvider.configure({
            baseUrl: window.location.protocol+"//"+window.location.hostname,
            clientId: '2',
            clientSecret: 'XEg14aHqrdJmU42LFk1w0MVaUGbYDF91MhOahuLa',
            grantPath: '/oauth/token',
            revokePath: '/oauth/revoke'
        });
    }]);

UserModule.config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push(function ($q, $rootScope) {
            return {
                responseError: function (rejection) {
                    $rootScope.clearToastr();
                    if (403 === rejection.status) {
                        $rootScope.toastrMessages('error','لا تملك الصلاحيات المناسبة لتنفيذ هذا الإجراء');
                   }

                    return $q.reject(rejection);
                }
            };
        });
    }]);

UserModule.config(function ($stateProvider) {
    $stateProvider
        .state('auth-logout', {
        url: '/account/logout',
        data: {pageTitle: 'Logout'},
        controller: function(OAuthToken, $http, $window) {
            $http({
                method: 'GET',
                url: '/api/v1.0/account/logout'
            }).then(function(response) {
                OAuthToken.removeToken();
                $window.location.href = '/account/login';
            }, function(response) {
                
            });
        }
    })
        .state('auth-users-account', {
        url: '/account',
        templateUrl: '/app/modules/user/views/account/index.html',
        data: {pageTitle: 'My Account'},
        controller: 'AccountController',
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AccountController',
                        files: [
                            '/app/modules/user/controllers/UserController.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/themes/metronic/pages/css/profile-rtl.css',
                            '/app/modules/document/directives/fileDownload.js',
                            '/app/modules/user/services/UserService.js'
                        ]
                    });
                }]
        }
    })
        .state('auth-users-change-password', {
        url: '/account/change-password',
        templateUrl: '/app/modules/user/views/account/change-password.html',
        data: {pageTitle: 'Change Password'},
        controller: 'ChangePasswordController',
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'ChangePasswordController',
                        files: [
                            '/app/modules/user/controllers/UserController.js',
                            '/app/modules/user/services/UserService.js',
                            '/app/modules/user/services/PasswordService.js'
                        ]
                    });
                }]
        }
    })
        .state('auth-users-index', {
        url: '/auth/users',
        templateUrl: '/app/modules/user/views/user/index.html',
        data: {pageTitle: 'Users'},
        controller: 'UserListController',
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'UserListController',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/user/controllers/UserController.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/user/services/UserService.js',
                            '/app/modules/organization/services/OrgService.js'
                        ]
                    });
                }]
        }
    })
        .state('auth-users-trashed', {
        url: '/auth/users/trashed',
        templateUrl: '/app/modules/user/views/user/trashed.html',
        data: {pageTitle: 'Trashed Users', mode: 'trashed'},
        controller: 'UserListController',
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'UserListController',
                        files: [
                            '/app/modules/user/controllers/UserController.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/user/services/UserService.js',
                            '/app/modules/organization/services/OrgService.js'
                        ]
                    });
                }]
        }
    })
        .state('auth-users-view', {
        url: '/auth/users/view/:id',
        templateUrl: '//app/modules/user/views/user/view.html',
        data: {pageTitle: 'View User'},
        controller: 'UserViewController',
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'UserViewController',
                        files: [
                            '/app/modules/user/services/UserService.js',
                            '/app/modules/user/controllers/UserController.js'
                        ]
                    });
                }]
        }
    })
        .state('auth-users-new', {
        url: '/auth/users/new',
        templateUrl: '/app/modules/user/views/user/new.html',
        data: {pageTitle: 'New User'},
        controller: 'UserCreateController',
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'UserCreateController',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/user/controllers/UserController.js',
                            '/app/modules/user/services/UserService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/user/services/RoleService.js'
                        ]
                    });
                }]
        }
    })
        .state('auth-users-edit', {
        url: '/auth/users/edit/:id',
        templateUrl: '/app/modules/user/views/user/edit.html',
        data: {pageTitle: 'Edit User'},
        controller: 'UserEditController',
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'UserEditController',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/user/controllers/UserController.js',
                            '/app/modules/user/services/UserService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/user/services/RoleService.js'
                        ]
                    });
                }]
        }
    })
        .state('auth-users-reset-password', {
        url: '/auth/users/reset-password/:id',
        templateUrl: '/app/modules/user/views/user/reset-password.html',
        data: {pageTitle: 'Reset Password'},
        controller: 'ResetPasswordController',
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'UserEditController',
                        files: [
                            '/app/modules/user/controllers/UserController.js',
                            '/app/modules/user/services/UserService.js',
                            '/app/modules/user/services/PasswordService.js'
                        ]
                    });
                }]
        }
    })
        .state('auth-roles-index', {
        url: '/auth/roles',
        templateUrl: '/app/modules/user/views/roles/index.html',
        data: {pageTitle: 'Roles'},
        controller: 'RolesController',
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RolesController',
                        files: [
                            '/app/modules/user/controllers/RolesController.js',
                            '/app/modules/user/services/RoleService.js',
                            '/app/modules/user/services/PermissionService.js',
                            '/app/modules/organization/services/OrgService.js'
                        ]
                    });
                }]
        }
    })
        .state('auth-roles-new', {
        url: '/auth/roles/new',
        templateUrl: '/app/modules/user/views/roles/new.html',
        data: {pageTitle: 'New Role'},
        controller: 'RoleCreateController',
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RolesController',
                        files: [
                            '/app/modules/user/controllers/RolesController.js',
                            '/app/modules/user/services/RoleService.js',
                            '/app/modules/user/services/PermissionService.js'
                        ]
                    });
                }]
        }
    })
        .state('auth-roles-edit', {
            url: '/auth/roles/edit/:id',
            templateUrl: '/app/modules/user/views/roles/edit.html',
            data: {pageTitle: 'Edit Role'},
            controller: 'RoleEditController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'RolesController',
                            files: [
                                '/app/modules/user/controllers/RolesController.js',
                                '/app/modules/user/services/RoleService.js',
                                '/app/modules/user/services/PermissionService.js'
                            ]
                        });
                    }]
            }
        })
        .state('auth-roles-users', {
            url: '/auth/roles/users/:id',
            templateUrl: '/app/modules/user/views/roles/users.html',
            data: {pageTitle: 'Role Users'},
            controller: 'RoleUsersController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'RoleUsersController',
                            files: [
                                '/app/modules/user/controllers/RolesController.js',
                                '/app/modules/user/services/RoleService.js'
                            ]
                        });
                    }]
            }
        })
        .state('auth-user-edit-permissions', {
            url: '/auth/users/edit/permissions/:id',
            templateUrl: '/app/modules/user/views/user/edit-permissions.html',
            data: {pageTitle: 'User Edit Permissions'},
            controller: 'UserEditPermissionsController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'UserEditPermissionsController',
                            files: [
                                '/app/modules/user/controllers/UserController.js',
                                '/app/modules/user/services/UserService.js',
                                '/app/modules/user/services/PermissionService.js'
                            ]
                        });
                    }]
            }
        })
        .state('auth-user-permissions', {
            url: '/auth/users/permissions/:id',
            templateUrl: '/app/modules/user/views/user/permissions.html',
            data: {pageTitle: 'User Permissions'},
            controller: 'UserPermissionsController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'UserPermissionsController',
                            files: [
                                '/app/modules/user/controllers/UserController.js',
                                '/app/modules/user/services/UserService.js',
                            ]
                        });
                    }]
            }
        })
        .state('auth-settings', {
        url: '/auth/settings',
        templateUrl: '/app/modules/user/views/account/settings.html',
        data: {pageTitle: 'Settings'},
        controller: 'AuthSettingsController',
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'AuthSettingsController',
                        files: [
                            '/app/modules/user/controllers/SettingsController.js',
                            '/app/modules/user/services/UserService.js'
                        ]
                    });
                }]
        }
    })
        .state('auth-users-online', {
            url: '/auth/online-users',
            templateUrl: '/app/modules/user/views/user/online.html',
            data: {pageTitle: 'Users'},
            controller: 'OnlineUsersController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'OnlineUsersController',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/log/services/MaillingService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/user/controllers/UserController.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/user/services/UserService.js',
                            '/app/modules/organization/services/OrgService.js'
                        ]
                    });
                }]
            }
        })
    ;
});

UserModule.run(['$rootScope', '$window', 'OAuth', '$http', function ($rootScope, $window, OAuth, $http, authSettings) {
    if (typeof gInLoginPage !== 'undefined' && gInLoginPage) {
        return;
    }
    $rootScope.$on('oauth:error', function (event, rejection) {
        // Ignore `invalid_grant` error - should be catched on `LoginController`.
        if ('invalid_grant' === rejection.data.error || 'invalid_credentials' === rejection.data.error) {
            return;
        }

        // Refresh token when a `invalid_token` error occurs.
        if ('invalid_token' === rejection.data.error) {
            return OAuth.getRefreshToken();
        }


        // Redirect to `/login` with the `error_reason`.
        return $window.location.href = '/account/login';
        // return $window.location.href = '/account/login?error_reason=' + rejection.data.error;
    });
    
    $http({
        method: 'GET',
        url: '/api/v1.0/auth/account/user'
    }).then(function(response) {
        response.data.initails = (response.data.username.substr(0, 1)).toUpperCase();
        $rootScope.charity_LoggedInUser = response.data;
        localStorage.setItem("charity_LoggedInUser", JSON.stringify(response.data));

    });

    $rootScope.can = function (action) {
        if (parseInt($rootScope.charity_LoggedInUser.super_admin) === 1) {
            return true;
        }

        var $can = false;
        angular.forEach($rootScope.charity_LoggedInUser.permissions, function(item) {
            if (action === item) {
                $can = true;
                return;
            }
        });

        return $can;
    };

    $http({
        method: 'GET',
        url: '/api/v1.0/auth/account/user/notifications'
    }).then(function(response) {
        $rootScope.notifications = response.data;
    });
}]);