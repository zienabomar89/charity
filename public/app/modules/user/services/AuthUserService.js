UserModule = angular.module('UserModule');

UserModule.factory('AuthUser', ['$http', function ($http) {
    var user = {};
    function getAuthUser() {
        if (user.hasOwnProperty('id')) {
            return user;
        }
        $http({
            method: 'GET',
            url: '/api/v1.0/auth/account/user'
        }).then(function (response) {
            user = response.data;
            user.initails = (user.firstname.substr(0, 1)).toUpperCase();
        });
        return user;
    }
    return {
        user: function() {
            return getAuthUser();
        },
        can :function (p) {
            u = this.user();
            return (u.permissions.indexOf(p) !== -1);
        }
    }
}]);