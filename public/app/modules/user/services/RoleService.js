angular.module('RoleService', []);

angular.module('RoleService').factory('Role', function ($resource) {
    return $resource('/api/v1.0/roles/:id', {id: '@id'}, {
        update: {
            method: 'PUT'
        },
        copy: {
            url: "/api/v1.0/roles/copy/:id",
            method: "POST",
            params: {id: '@id'}
        },
        users: {
            url: "/api/v1.0/roles/users/:id",
            method: "GET",
            params: {id: '@id'},
            isArray: true
        }
    });
});