angular.module('PermissionService', []);

angular.module('PermissionService').factory('Permission', function ($resource) {
    return $resource('/api/v1.0/permissions');
});