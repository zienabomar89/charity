

var SettingsModule = angular.module("SettingsModule",[]);

SettingsModule.factory('commonEntities', ['$rootScope', function ($rootScope) {
    // supported languages
    var settings = {
        organizationsCategory: {
            title: 'organizations_category',
            hasRatio: false,
            hasWeight: false,
            i18n: true,
        },
       projectCategory: {
            title: 'project-category',
            hasRatio: false,
            hasWeight: false,
            i18n: true,
            child: {
                entity: 'subProjectCategory',
                title: 'sub_project_category',
                route: 'project-category/:id/sub'
            }
        },
        subProjectCategory: {
            title: 'sub_project_category',
            hasRatio: false,
            hasWeight: false,
            i18n: true,
            parentRoute: 'project-category'
        },
        projectBeneficiaryCategory: {
            title: 'project-beneficiary-category',
            hasRatio: false,
            hasWeight: false,
            i18n: true
        }
        ,projectType: {
            title: 'project-type',
            hasRatio: false,
            hasWeight: false,
            i18n: true
        },
        projectRegion: {
            title: 'project-region',
            hasRatio: false,
            hasWeight: false,
            i18n: true
        },
        projectActivities: {
            title: 'project-activities',
            hasRatio: false,
            hasWeight: false,
            i18n: true
        },
        houseStatus: {
            title: 'House_Status',
            hasRatio: false,
            hasWeight: true,
            i18n: true
        },

        buildingStatus: {
            title: 'Building_Status',
            hasRatio: false,
            hasWeight: true,
            i18n: true
        },
        furnitureStatus: {
            title: 'Furniture_Status',
            hasRatio: false,
            hasWeight: true,
            i18n: true
        },
        habitableStatus: {
            title: 'Habitable_Status',
            hasRatio: false,
            hasWeight: true,
            i18n: true
        }, maritalStatus: {
            title: 'Marital_Status',
            hasRatio: false,
            hasWeight: true,
            i18n: true
        },
        deathCauses: {
            title: 'Death_Causes',
            hasRatio: false,
            hasWeight: false,
            i18n: false
        },
        paymentCategory: {
            title: 'Payment_Category',
            hasRatio: false,
            hasWeight: false,
            i18n: true
        },
        aidSources: {
            title: 'Aid_Sources',
            hasRatio: false,
            hasWeight: false,
            i18n: false,
            hasWeightOptions: true,
            weightOptionsMode: 'custom',
            weightOptions: [{name: 'value_min', label: 'Min_Value'}, {name: 'value_max', label: 'Max_Value'}],
            weightOptionsUri: 'api/v1.0/common/aid-sources/:id/weights'
        },
        transferCompany: {
            title: 'Transfer_Company',
            hasRatio: false,
            hasWeight: false,
            i18n: false,
            hasWeightOptions: false
        },
        banks: {
            title: 'Banks',
            hasRatio: false,
            hasWeight: false,
            i18n: false,
            child: {
                entity: 'branches',
                title: 'Branches',
                route: 'banks/:id/branches'
            }
        },
        branches: {
            title: 'Bank_Branches',
            hasRatio: false,
            hasWeight: false,
            i18n: false
        },
        currencies: {
            title: 'Currencies',
            hasRatio: false,
            hasWeight: false,
            i18n: false,
            inputs: ['code', 'symbol','en_name']
        },
        diseases: {
            title: 'Diseases',
            hasRatio: false,
            hasWeight: true,
            i18n: true
        },
        documentTypes: {
            title: 'Document_Types',
            hasRatio: false,
            hasWeight: false,
            i18n: true,
            inputs: ['expiry']
        },
        educationAuthorities: {
            title: 'Education_Authorities',
            hasRatio: false,
            hasWeight: true,
            i18n: true
        },
        educationDegrees: {
            title: 'Education_Degrees',
            hasRatio: false,
            hasWeight: true,
            i18n: true
        },
        educationStages: {
            title: 'Education_Stages',
            hasRatio: false,
            hasWeight: true,
            i18n: true
        },
        essentials: {
            title: 'Essentials',
            hasRatio: false,
            hasWeight: false,
            i18n: false,
            hasWeightOptions: true,
            weightOptionsMode: 'fixed',
            weightOptions: [
                {id: 0, label: 'Not_Exists'},
                {id: 1, label: 'Very_Bad'},
                {id: 2, label: 'Bad'},
                {id: 3, label: 'Good'},
                {id: 4, label: 'Very_Good'},
                {id: 5, label: 'Excellent'}
            ],
            weightOptionsUri: 'api/v1.0/common/essentials/:id/weights'
        },
        kinship: {
            title: 'Kinship',
            hasRatio: false,
            hasWeight: false,
            i18n: true
        },
        languages: {
            title: 'Languages',
            hasRatio: false,
            hasWeight: false,
            i18n: false,
            inputs: ['native_name', 'code']
        },
        countries: {
            title: 'Countries',
            hasRatio: false,
            hasWeight: false,
            i18n: true,
            child: {
                entity: 'districts',
                title: 'Districts',
                route: 'countries/:id/districts'
            }
        },
        districts: {
            title: 'Districts',
            hasRatio: false,
            hasWeight: false,
            i18n: true,
            parentRoute: 'countries',
            child: {
                entity: 'cities',
                title: 'Cities',
                route: 'districts/:id/cities'
            }
        },
        cities: {
            title: 'Cities',
            hasRatio: false,
            hasWeight: false,
            i18n: true,
            parentRoute: 'countries/:parent_id/districts',
            child: {
                entity: 'neighborhoods',
                title: 'Neighborhoods',
                route: 'cities/:id/neighborhoods'
            }
        },
        neighborhoods: {
            title: 'Neighborhoods',
            hasRatio: false,
            hasWeight: false,
            i18n: true,
            parentRoute: 'districts/:parent_id/cities',
            child: {
                entity: 'mosques',
                title: 'Mosques',
                route: 'neighborhoods/:id/mosques'
            }
    
        },
        mosques: {
            title: 'Mosques',
            hasRatio: false, hasWeight: false,
            i18n: true,
            parentRoute: 'cities/:parent_id/neighborhoods'
        },
        properties: {
            title: 'Properties',
            hasRatio: false,
            hasWeight: false,
            i18n: true,

            hasWeightOptions: true,
            weightOptionsMode: 'custom',
            weightOptions: [{name: 'value_min', label: 'Min_Value'}, {name: 'value_max', label: 'Max_Value'}],
            weightOptionsUri: 'api/v1.0/common/properties/:id/weights'
        },
        propertyTypes: {
            title: 'Property_Types',
            hasRatio: false, hasWeight: true,
            i18n: true
        },
        roofMaterials: {
            title: 'Roof_Materials',
            hasRatio: false, hasWeight: true,
            i18n: true
        },
        workJobs: {
            title: 'Work_Jobs',
            hasRatio: false, hasWeight: true,
            i18n: true
        },
        workReasons: {
            title: 'Work_Reasons',
            hasRatio: false, hasWeight: true,
            i18n: true
        },
        workStatus: {
            title: 'Work_Status',
            hasRatio: false, hasWeight: true,
            i18n: true
        },
        workWages: {
            title: 'Work_Wages',
            hasRatio: true, hasWeight: true,
            i18n: false
        },
        scountries: {
            title: 'Countries',
            hasRatio: true,
            hasWeight: false,
            level: 0,
            hasTransfer: false,
            i18n: true,
            child: {
                entity: 'sdistricts',
                title: 'Districts',
                route: 'scountries/:id/sdistricts'
            }
        },
        sdistricts: {
            title: 'Districts',
            hasRatio: true,
            hasWeight: false,
            i18n: true,
            level: 1,
            hasTransfer: false,
            parentRoute: 'scountries',
            child: {
                entity: 'sregions',
                title: 'Regions',
                route: 'sdistricts/:id/sregions'
            }
        },
        sregions: {
            title: 'Cities',
            hasRatio: true,
            hasWeight: false,
            i18n: true,
            hasTransfer: true,
            level: 2,
            parentRoute: 'scountries/:parent_id/sdistricts',
            child: {
                entity: 'sneighborhoods',
                title: 'Neighborhoods',
                route: 'sregions/:id/sneighborhoods'
            }
        },
        sneighborhoods: {
            title: 'Neighborhoods',
            hasRatio: true,
            hasWeight: false,
            level: 3,
            i18n: true,
            hasTransfer: true,
            parentRoute: 'sdistricts/:parent_id/sregions',
            child: {
                entity: 'ssquares',
                title: 'Squares',
                route: 'sneighborhoods/:id/ssquares'
            }

        },
        ssquares: {
            title: 'Squares',
            hasRatio: true,
            level: 4,
            hasWeight: false,
            i18n: true,
            parentRoute: 'sregions/:parent_id/sneighborhoods',
            hasTransfer: true,
            child: {
                entity: 'smosques',
                title: 'Mosques',
                route: 'ssquares/:id/smosques'
            }

        },
        smosques: {
            title: 'Mosques',
            hasRatio: true, hasWeight: false,
            level: 5,
            i18n: true,
            hasTransfer: true,
            parentRoute: 'sneighborhoods/:parent_id/ssquares'
        }

    };

    $rootScope.commonEntities = settings;

    return settings;
}]);

angular.module('SettingsModule').config(function ($stateProvider) {
    $stateProvider
        .state('common-cases-documents', {
            url: '/common/cases-documents',
            templateUrl: '/app/modules/setting/views/documents.html',
            data: {
                pageParantTitle: 'الثوابت والاعدادات',
                pageTitle: ''
            },
            controller: "CasesDocumentsController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CasesDocumentsController',
                        files: [
                            '/app/modules/setting/controllers/CasesDocumentsController.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js'
                        ]
                    });
                }]
            }
        })
        .state('common-entities', {
            url: '/common/entities/:model',
            templateUrl: '/app/modules/setting/views/index.html',
            params:{model: null,id:null,'action':null },
            data: {
                pageParantTitle: 'الثوابت والاعدادات',
                pageTitle: ''
            },
            controller: "CommonEntitiesController",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'CommonEntitiesController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/app/modules/setting/controllers/CommonEntitiesController.js',
                                '/app/modules/setting/services/EntityService.js'
                            ]
                        });
                    }]
                }
        })
        .state('common-entities-action', {
            url: '/common/entities/:model/:id/:action',
            templateUrl: '/app/modules/setting/views/index.html',
            data: {
                pageParantTitle: 'الثوابت والاعدادات',
                pageTitle: ''
            },
            params:{model: null,id:null,'action':null },
            controller: "CommonEntitiesController",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'CommonEntitiesController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/app/modules/setting/controllers/CommonEntitiesController.js',
                                '/app/modules/setting/services/EntityService.js'
                            ]
                        });
                    }]
                }
        })

        // .state('cheque-template', {
        //     url: '/common/banks/cheque-template',
        //     templateUrl: '/app/modules/setting/views/cheque-banks-template.html',
        //     controller: "ChequeBanksTemplateController",
        //     resolve: {
        //         deps: ['$ocLazyLoad', function($ocLazyLoad) {
        //             return $ocLazyLoad.load({
        //                 name: 'SettingsModule',
        //                 files: [
        //                     '/app/modules/setting/controllers/ChequeBanksTemplateController.js',
        //                     '/app/modules/setting/services/EntityService.js',
        //                     '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
        //                     '/app/modules/document/directives/fileDownload.js'
        //
        //                 ]
        //             });
        //         }]
        //     }
        //
        // })

    ;
});
