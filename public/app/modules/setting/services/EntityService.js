var SettingsModule = angular.module('SettingsModule');

SettingsModule.factory('Entity', function ($resource) {
    return $resource('/api/v1.0/common/:entity/:parent_id/:action/:id', {entity: '@entity', parent_id: '@parent_id', id: '@id', action: '@action'}, {
        query: {
            method: 'GET',
            isArray: false
        },
        update: {
            method: 'PUT'
        },
        translations: {
            method: 'GET',
            isArray: true,
            url: "/api/v1.0/common/:entity/:id/translations"
        },
        saveTranslations: {
            method: 'POST',
            isArray: true,
            url: "/api/v1.0/common/:entity/:id/translations"
        },
        list: {
            method: 'GET',
            isArray: true,
            url: "/api/v1.0/common/:entity/list"
        },
        listChildren: {
            method: 'GET',
            isArray: true,
            // :parent The parent entity, :id The parent ID, :entity The child entity
            url: "/api/v1.0/common/:parent/:id/:entity/list"
        },
        tree: {
            method: 'GET',
            isArray: false,
            url: "/api/v1.0/common/:entity/tree/:id"
        },
        transfer: {
            method: 'PUT',
            isArray: false,
            url: "/api/v1.0/common/:entity/transfer/:id"
        },

    });
});

SettingsModule.factory('BankChequeTemplate', function ($resource) {
    return $resource('/api/v1.0/common/BankChequeTemplate/:operation/:id', {id: '@id',page: '@page'}, {
        query  : { method:'GET' ,params:{page: '@page'}, isArray:true },
        update:{method: 'PUT', params: {id: '@id'}, isArray: false}
    });
});

