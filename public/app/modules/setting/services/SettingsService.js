
angular.module('SettingsModule')
    .factory('setting', function ($resource) {
        return $resource('/api/v1.0/common/setting/:operation/:id', {id: '@id',page: '@page'}, {
            update:{method: 'PUT'  , params: {operation:'update',id: '@id'}, isArray: false},
            getAncestorSetting     : { method:'GET' ,params:{operation:'getAncestorSetting',id: '@id'}, isArray:false },
            getSettingById     : { method:'GET' ,params:{operation:'getSettingById',id: '@id'}, isArray:false },
            getCustomForm     : { method:'GET' ,params:{operation:'getCustomForm',id: '@id'}, isArray:false },
            setCustomForm:   {method: 'POST' , params: {operation:'setCustomForm'}, isArray: false},
            getSysSetting:{method: 'POST'  , params: {operation:'getSysSetting'}, isArray: false}

        });
    });

