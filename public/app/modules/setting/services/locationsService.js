

angular.module('SettingsModule')
    .factory('locations', function ($resource) {
        return $resource('/api/v1.0/common/locations/:operation/:id/', {page: '@page',id: '@id' ,type:'@type'}, {
            get_type_location:{ method:'GET',params:{operation:'get_type_location'}, isArray:true }
        });
    });