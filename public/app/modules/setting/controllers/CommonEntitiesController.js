var SettingsModule = angular.module('SettingsModule');

SettingsModule.controller('CommonEntitiesController', function(commonEntities, $scope,$rootScope ,$uibModal,$state,$ngBootbox, $stateParams, Entity, popupService, $filter, $http,Language) {

    function camelCase(input) {

        // return input.replace(/-(.)/g, function(match, group1) {
        //     return group1.toUpperCase();
        // });

        return input.toLowerCase().replace(/-(.)/g, function(match, group1) {
            return group1.toUpperCase();
        });
    }

    $entity = $stateParams.model;
    $id = $stateParams.id;
    $action = $stateParams.action;
    $params = {entity: $entity};
    $scope.entitySettings = commonEntities[camelCase($entity)];

    $scope.hasTransfer = false;
    $scope.tree = [];

    if (!$scope.entitySettings.hasOwnProperty('inputs')) {
        $scope.entitySettings.inputs = [];
    }

    if ($action) {
        var hasTransferIndex =  ['sdistricts','sregions','sneighborhoods','ssquares','smosques'];
        if (hasTransferIndex.indexOf($action) != -1) {
            var actionRow = commonEntities[camelCase($action)];
            $scope.hasTransfer = actionRow.hasTransfer;
        }

    }

    if ($id && $action) {
        $params.parent_id = $id;
        $params.action = $action;
        if (commonEntities.hasOwnProperty($action) && commonEntities[$action].hasOwnProperty('child')) {
            $scope.hasChild = true;
            $scope.childEntity = commonEntities[$action].child;

            if ($scope.childEntity.hasOwnProperty('hasTransfer')) {
                $scope.hasTransfer = true;
            }

        }

        $state.current.data.pageTitle = $scope.pageTitle = $filter('translate')($scope.entitySettings.child.title);

        var record={id: $id, entity: $entity};
        if ($id && $action) {
            record.parent_id = $id;
            record.action = $action;
        }

        // $rootScope.progressbar_start();
        Entity.get(record, function(response) {
            // $rootScope.progressbar_complete();
            $scope.parentEntity = response;
            var child = commonEntities[$scope.entitySettings.child.entity];
            var parent_url = '#/common/entities/' + child.parentRoute;
            $scope.parentUrl = parent_url;
            $scope.parentUrl = $scope.parentUrl.replace(":parent_id",$scope.parentEntity.parent_id);
            $scope.parentUrl = $scope.parentUrl.replace(":id",null);

            if(!angular.isUndefined(response.tree)) {
                $scope.tree = response.tree ;

            }
        });

    }
    else {
        if ($scope.entitySettings.hasOwnProperty('child')) {
            $scope.hasChild = true;
            $scope.childEntity = $scope.entitySettings.child;

            if ($scope.childEntity.hasOwnProperty('hasTransfer')) {
                $scope.hasTransfer = true;
            }
        }
        $state.current.data.pageTitle = $scope.pageTitle = $filter('translate')($scope.entitySettings.title);

    }

    $scope.baseUri = $entity;

    if ($scope.entitySettings.hasOwnProperty('child')) {
        var url = '#/common/entities/' + $scope.entitySettings.child.route;
        var params = $stateParams;
        $scope.CurUrl = url;
        $scope.CurUrl = $scope.CurUrl.replace(":model",params.model);
        $scope.CurUrl = $scope.CurUrl.replace(":action",params.action);
        $scope.CurUrl = $scope.CurUrl.replace(":id",params.id);
    }else{
        var url = $state.current.url;
        var params = $stateParams;
        $scope.CurUrl = url;
        $scope.CurUrl = $scope.CurUrl.replace(":model",params.model);
        $scope.CurUrl = $scope.CurUrl.replace(":action",params.action);
        $scope.CurUrl = $scope.CurUrl.replace(":id",params.id);
    }

    $scope.messages = {
        success: '',
        error: ''
    };
    $scope.entries=[];
    $scope.searchEntity={name:""};
    $scope.inProgress = false;
    function list(params) {
        $rootScope.progressbar_start();
        Entity.query(params, function (response) {
            $rootScope.progressbar_complete();
            $scope.entries = response.data;
            $scope.totalItems = response.total;
            $scope.currentPage = response.current_page;
            $scope.itemsPerPage = response.per_page;
        });
    }

    $scope.pageChanged = function(currentPage) {
        $params.page = currentPage;
        list($params);
    };
    $scope.search = function(model) {
        $params.name = model.name;
        list($params);
    };

    list($params);

    $rootScope.reload = function(model) {
        list($params);
    };

    $scope.edit = function(entry) {
        if (!$scope.entitySettings.hasRatio) {
            delete  entry.ratio;
        }
        $rootScope.clearToastr();
        if(  $scope.inProgress == false){
            entry.edit = true;
            $scope.oldEntry = {
                name: entry.name,
                weight: entry.weight
            };
            if ($scope.entitySettings.hasRatio) {
                $scope.oldEntry.ratio =entry.ratio;
            }
            $scope.inProgress = true;
        }else{
            $rootScope.toastrMessages('error',$filter('translate')('edit the data of selected record or press the cancel button'));
        }
    };
    $scope.cancel = function(entry) {
        entry.name = $scope.oldEntry.name;
        entry.weight = $scope.oldEntry.weight;
        if ($scope.entitySettings.hasRatio) {
            entry.ratio =$scope.oldEntry.ratio;
        }
        entry.edit = false;
        $scope.inProgress = false;
    };
    $scope.save = function(entry) {
        $rootScope.clearToastr();
        entry.entity = $stateParams.model;
        if ($id && $action) {
            entry.parent_id = $id;
            entry.action = $action;
        }

        if (!$scope.entitySettings.hasRatio) {
            delete  entry.ratio;
        }

        $rootScope.progressbar_start();
        Entity.update(entry, function () {
            $rootScope.progressbar_complete();
            entry.edit = false;
            $rootScope.toastrMessages('success',$filter('translate')('action success'));
            $scope.inProgress = false;
        }, function (response) {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('error',angular.isArray(response.data)? response.data.join('<br>') : JSON.stringify(response.data));
        });
    };
    $scope._save = function(entry, event) {
        if(event.keyCode===13)
            return $scope.save(entry);
        if(event.keyCode===27)
            return $scope.cancel(entry);
    };
    $scope.create = function(model) {
        $rootScope.clearToastr();
        model.entity = $stateParams.model;
        if ($id && $action) {
            model.parent_id = $id;
            model.action = $action;
        }
        $rootScope.progressbar_start();
        Entity.save(model, function (response) {
            $rootScope.progressbar_complete();
            $scope.entries.push(response);
            $scope.createForm = false;
            $rootScope.toastrMessages('success',$filter('translate')('action success'));
            // Reset Form
            $scope.newEntity.name = '';
            if ($scope.entitySettings.hasWeight) {
                $scope.newEntity.weight = '';
            }
            if ($scope.entitySettings.hasRatio) {
                $scope.newEntity.ratio = '';
            }
            angular.forEach($scope.entitySettings.inputs, function (o, i) {
                $scope.entitySettings.inputs[i] = '';
            });

        }, function (response) {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('error',angular.isArray(response.data)? response.data.join('<br>') : JSON.stringify(response.data));
        });
    };
    $scope.delete = function (entry, idx) {

        $rootScope.clearToastr();
        $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
            .then(function() {
                $rootScope.progressbar_start();
                entry.entity = $stateParams.model;
                if ($id && $action) {
                    entry.parent_id = $id;
                    entry.action = $action;
                }
                Entity.delete(entry, function (response) {
                    $rootScope.progressbar_complete();
                    if(response.deleted){
                        $scope.entries.splice(idx, 1);
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                    }else{
                        if(!angular.isUndefined(response.msg)) {

                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else{
                            $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                        }

                    }
                }, function (error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',angular.isArray(error.data)? error.data.join('<br>') : JSON.stringify(error.data));
                });
            });
    };
    $scope.translations = function(entry) {
        Entity.translations({id: entry.id, entity: $action? $action : $stateParams.model}, function(response) {
            $scope.i18nEntries = response;
            $scope.i18nEntityId = entry.id;
            $scope.i18n = true;
            $scope.createForm = false;
        });
    };
    $scope.saveTranslations = function() {
        $rootScope.clearToastr();
        $rootScope.progressbar_start();
        Entity.saveTranslations({id: $scope.i18nEntityId, entity: $action? $action : $stateParams.model, translation: $scope.i18nEntries}, function(response) {
            $rootScope.progressbar_complete();
            $scope.i18nEntries = response;
            $scope.i18n = false;
            $rootScope.toastrMessages('success',$filter('translate')('action success'));
        });
    };
    $scope.weights = function(entity) {
        $scope.weightsEntityId = entity.id;
        $scope.weightOptions = true;
        if ($scope.entitySettings.weightOptionsMode == 'fixed') {
            $http.get($scope.entitySettings.weightOptionsUri.replace(':id', entity.id)).then(function(response) {
                angular.forEach($scope.entitySettings.weightOptions, function(o, e) {
                    o.weight = '';
                    angular.forEach(response.data, function(o2, e2) {
                        if (o.id == o2.value) {
                            o.weight = o2.weight;
                        }
                    });
                });
            });
            return;
        }

        $scope.newWeight = {};
        $scope.entityWeights = [];
        $http.get($scope.entitySettings.weightOptionsUri.replace(':id', entity.id)).then(function(response) {
            $scope.entityWeights = response.data;
        });
    };
    $scope.editWeight = function(entity) {
        entity.edit = true;
    };
    $scope.saveWeight = function(entity) {
        $rootScope.progressbar_start();
        $http.put($scope.entitySettings.weightOptionsUri.replace(':id', $scope.weightsEntityId)+'/'+entity.id, entity).
        then(function(response) {
            $rootScope.progressbar_complete();
            entity.edit = false;
        });
    };
    $scope.saveWeights = function() {
        $rootScope.clearToastr();
        var weights = [];
        angular.forEach($scope.entitySettings.weightOptions, function(o, e) {
            if (o.weight) {
                weights.push(o);
            }
        });
        if (weights.length > 0) {
            $rootScope.progressbar_start();
            $http.post($scope.entitySettings.weightOptionsUri.replace(':id', $scope.weightsEntityId), {weights: weights})
                .then(function(response) {
                    // entity.edit = false;
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                });
        }
    };
    $scope.addWeight = function(entity) {
        $rootScope.progressbar_start();
        $http.post($scope.entitySettings.weightOptionsUri.replace(':id', $scope.weightsEntityId), entity).then(function(response) {
            $scope.entityWeights.push(response.data);
            $rootScope.progressbar_complete();
            $scope.newWeight = {};
        });
    };
    $scope.deleteWeight = function(entity) {
        $rootScope.progressbar_start();
        $http.delete($scope.entitySettings.weightOptionsUri.replace(':id', $scope.weightsEntityId)+'/'+entity.id).then(function(response) {
            $rootScope.progressbar_complete();
            $scope.entityWeights.splice($scope.entityWeights.indexOf(entity), 1);
        });
    };

    $scope.transfer=function(row){

        $rootScope.location_type = row.location_type;
        $rootScope.type_name = row.type_name;
        $rootScope.name = row.name;

        Entity.tree({ 'entity' : 'aidsLocations' , id  :row.id }, function (response) {
            $rootScope.countries = response.countries;
            $rootScope.districts = response.districts;
            $rootScope.regions = response.regions;
            $rootScope.neighborhoods = response.neighborhoods;
            $rootScope.squares = response.squares;
            $rootScope.row={id: row.id ,
                country_id: response.country_id, district_id: response.district_id,
                region_id: response.region_id, neighborhood_id: response.neighborhood_id,
                square_id: response.square_id,
            };
            $rootScope.old_tree={country_id: response.country_id, district_id: response.district_id,
                region_id: response.region_id, neighborhood_id: response.neighborhood_id,
                square_id: response.square_id};

            $rootScope.required={country_id: response.re_country_id,
                district_id: response.re_district_id,
                region_id: response.re_region_id,
                neighborhood_id: response.re_neighborhood_id,
                square_id: response.re_square_id};
        });

        $uibModal.open({
            templateUrl: '/app/modules/setting/views/transfer.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance, $log) {

                $scope.getLocation = function(entity,parent,value){
                    if(!angular.isUndefined(value)) {
                        if (value !== null && value !== "" && value !== " ") {
                            Entity.listChildren({'entity': entity, 'id': value, 'parent': parent}, function (response) {

                                if (entity == 'sdistricts') {
                                    $rootScope.districts = response;
                                    $rootScope.regions = [];
                                    $rootScope.neighborhoods = [];
                                    $rootScope.squares = [];

                                    $rootScope.row.district_id = null;
                                    $rootScope.row.region_id = null;
                                    $rootScope.row.neighborhood_id = null;
                                    $rootScope.row.square_id = null;
                                }
                                else if (entity == 'sregions') {
                                    $scope.regions = response;

                                    $rootScope.regions = response;
                                    $rootScope.neighborhoods = [];
                                    $rootScope.squares = [];

                                    $rootScope.row.region_id = null;
                                    $rootScope.row.neighborhood_id = null;
                                    $rootScope.row.square_id = null;

                                }
                                else if (entity == 'sneighborhoods') {
                                    $rootScope.neighborhoods = response;
                                    $rootScope.squares = [];

                                    $rootScope.row.neighborhood_id = null;
                                    $rootScope.row.square_id = null;
                                }
                                else if (entity == 'ssquares') {
                                    $rootScope.squares = response;
                                    $rootScope.row.square_id = null;
                                }
                            });
                        }
                    }
                };


                $scope.confirm = function () {
                    $ngBootbox.confirm($filter('translate')('are you want to confirm transfer') )
                        .then(function() {
                            $rootScope.progressbar_start();
                            var params = angular.copy($rootScope.row);
                            params.entity = 'aidsLocations';
                            params.pre = $rootScope.old_tree;
                            Entity.transfer(params, function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='failed') {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status== 'success'){
                                    $modalInstance.close();
                                    $rootScope.reload();
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }
                            }, function (error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',angular.isArray(error.data)? error.data.join('<br>') : JSON.stringify(error.data));
                            });
                        });
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });

    };

});