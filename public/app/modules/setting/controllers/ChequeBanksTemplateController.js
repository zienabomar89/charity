
angular.module('SponsorshipModule')
    .controller('ChequeBanksTemplateController', function ($http,$filter,$scope,$rootScope,$state,$uibModal,$stateParams,OAuthToken, FileUploader,Entity) {

        $rootScope.settings.layout.pageSidebarClosed = true;
        $scope.success =false;
        $scope.failed =false;
        $scope.error =false;
        $scope.hasRatio =false;
        $scope.custom = true;
        $scope.status ='';
        $scope.msg ='';

        $scope.close=function(){
            $scope.success =false;
            $scope.failed =false;
            $scope.error =false;
        };

        function list(params) {
            params.entity='banks';
            $scope.entries = Entity.list(params);
        }

        $scope.pageChanged = function(CurrentPage) {
            list({page:CurrentPage});
        };

        list({page:1});

        $scope.download=function(action,id){

            $rootScope.clearToastr();
            var url=' ';
            var type='docx';

            if(action ==='template'){
                url='/api/v1.0/common/banks/template/'+id;
            }else if(action ==='template_instruction'){
                url='/api/v1.0/common/banks/templateInstruction';
                type='pdf';
            }

            $http({
                url:url,
                method: "GET"
            }).then(function (response) {
                window.location = '/api/v1.0/common/files/'+type+'/export?download_token='+response.data.download_token+'&template=true';
            }, function(error) {
                $rootScope.toastrMessages('error',error.data.msg);
            });

        };
        $scope.ChooseTemplate=function(id,action){
            $rootScope.clearToastr();

            $uibModal.open({
                templateUrl: 'myModalContent2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/common/banks/template-upload/'+id,
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });
                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                    };
                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete()
                        if(!response){
                            $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                        }else{
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            list({page:$rootScope.CurrentPage});
                        }

                        $modalInstance.close();

                    };


                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });


        };

    });



