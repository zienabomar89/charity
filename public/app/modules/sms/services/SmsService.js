
angular.module('SmsModule')
    .factory('org_sms_service', function ($resource) {
        return $resource('/api/v1.0/sms/settings/:operation/:id', {id: '@id',page: '@page'}, {
            query  : { method:'GET' ,params:{page: '@page'}, isArray:false },
            getList:{method: 'GET', params: {operation:'getList'}, isArray: false},
            sendSmsToTarget:    { method:'POST',params:{operation:'sendSmsToTarget'}, isArray: false},
            updateDefault :{method: 'PUT', params: {operation:'updateDefault',id: '@id'}, isArray: false},
            updateActive  :{method: 'PUT', params: {operation:'updateActive',id: '@id'}, isArray: false},
            update:{method: 'PUT', params: {id: '@id'}, isArray: false}
        });
    });

angular.module('SmsModule')
    .factory('sent_sms', function ($resource) {
        return $resource('/api/v1.0/sms/sent_sms/:operation/:id', {id: '@id'}, {
            query: {method: 'GET', params: {id:''}, isArray: false},
            sendSms:    { method:'POST',params:{operation:'sendSms'}, isArray: false}
        });
    });


angular.module('SmsModule')
    .factory('sms_service_provider', function ($resource) {
        return $resource('/api/v1.0/sms/providers/:operation/:id', {id: '@id',page: '@page'}, {
            query  : { method:'GET' ,params:{page: '@page'}, isArray:false },
            getActiveProvidersList:{method: 'GET', params: {operation:'getActiveProvidersList'}, isArray: false},
            update:{method: 'PUT', params: {id: '@id'}, isArray: false},
            updateStatus  :{method: 'PUT', params: {operation:'updateStatus',id: '@id'}, isArray: false}
        });
    });

