
angular.module('SmsModule')
    .controller('SentSmsController' ,function($rootScope,$state,$scope,$http,$timeout,$uibModal,$log,sent_sms,org_sms_service,$filter) {

        $state.current.data.pageTitle = $filter('translate')('sent-sms');

        $scope.GoToHome=function(){
            $state.go('dashboard');
        };

        $rootScope.status ='';
        $rootScope.msg ='';

        $scope.MobileNumber=null;
        $scope.mode=false;
        $scope.enabled=false;

        $scope.count= 0;
        $scope.sms= {};
        $scope.sms.sms_messege = '';

        var counter= 0;
        $scope.Mobiles = [];
        org_sms_service.getList(function (response) {
            $scope.Providers = response.Service;
        });

        $scope.AddNewMobile = function () {
            $rootScope.clearToastr();
            var flag = true;
            angular.forEach($scope.Mobiles,function(val,key)
            {
                if( val.mobile ==  $scope.MobileNumber){
                  flag = false;
                  return;
                }
            });


            if(flag){
                $scope.Mobiles.push({
                    mobile: $scope.MobileNumber
                });
                $scope.MobileNumber= null;
                $scope.enabled=false;
                $scope.count= $scope.count+1;
            }else{
                $rootScope.toastrMessages('error',$filter('translate')('sent-sms'));
            }
        };

        $scope.removeMobile = function(index){
           $scope.Mobiles.splice(index,1);
           $scope.count= $scope.count-1;

        };
        $scope.editMobile=function(index){
                $scope.MobileNumber=$scope.Mobiles[index].mobile;
                $scope.temp_index=index;
                $scope.mode=true;
           };
        $scope.enabledadd=function(){
            if($scope.MobileNumber==""){
                $scope.enabled=false;
                if($scope.mode==true){
                    $scope.mode=false;
                }
            }else{
                $scope.enabled=true;
            }
        };

        $scope.ConfirmEditMobile=function(){
            $rootScope.clearToastr();

            var flag = true;
            angular.forEach($scope.Mobiles,function(val,key)
            {
                if( val.mobile ==  $scope.MobileNumber && $scope.temp_index != key){
                    flag = false;
                    return;
                }
            });

            if(flag){
                $scope.Mobiles[$scope.temp_index].mobile=$scope.MobileNumber;
                $scope.mode=false;
                $scope.MobileNumber= "";
            }else{
                $rootScope.toastrMessages('error',$filter('translate')('The number you are trying to edit is already listed. Please delete the number from the list or edit another number as necessary'));
            }


        };

        $scope.sendSmsByExcel=function(){
            org_sms_service.getList(function (response) {
                $rootScope.Providers = response.Service;
            });
            $uibModal.open({
                    templateUrl: '/app/modules/sms/views/modal/sent_sms_from_xlx_general.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                        $scope.upload=false;
                        $scope.sms={source: 'file' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:"",cont_type:""};

                        $scope.RestUpload=function(value){
                            if(value ==2){
                                $scope.upload=false;
                            }
                        };

                        $scope.fileUpload=function(){
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.upload=true;

                        };
                        $scope.download=function(){
                            window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-to-send-sms-template&template=true';
                        };

                        var Uploader = $scope.uploader = new FileUploader({
                            url: 'api/v1.0/sms/excel',
                            removeAfterUpload: false,
                            queueLimit: 1,
                            headers: {
                                Authorization: OAuthToken.getAuthorizationHeader()
                            }
                        });


                        $scope.confirm = function (item) {
                            $scope.spinner=false;
                            $scope.import=false;
                            $rootScope.progressbar_start();
                            Uploader.onBeforeUploadItem = function(item) {

                                if($scope.sms.same_msg == 0){
                                    $scope.sms.sms_messege = '';
                                }
                                item.formData = [$scope.sms];
                            };
                            Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                                $rootScope.progressbar_complete();
                                $scope.uploader.destroy();
                                $scope.uploader.queue=[];
                                $scope.uploader.clearQueue();
                                angular.element("input[type='file']").val(null);

                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                    $scope.upload=false;
                                    angular.element("input[type='file']").val(null);
                                }
                                else if(response.status=='success')
                                {
                                    if(response.download_token){
                                        var file_type='xlsx';
                                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                    } 
                                    $scope.spinner=false;
                                    $scope.upload=false;
                                    angular.element("input[type='file']").val(null);
                                    $scope.import=false;
                                    $rootScope.toastrMessages('success',response.msg);
                                    $modalInstance.close();
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                    $scope.upload=false;
                                    angular.element("input[type='file']").val(null);
                                }

                            };
                            Uploader.uploadAll();
                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                        $scope.remove=function(){
                            angular.element("input[type='file']").val(null);
                            $scope.uploader.clearQueue();
                            angular.forEach(
                                angular.element("input[type='file']"),
                                function(inputElem) {
                                    angular.element(inputElem).val(null);
                                });
                        };

                    }
                });

            
        };


        $scope.SentMessege=function(data){
            $rootScope.clearToastr();

            mobile_number=[];
            angular.forEach($scope.Mobiles,function(v,k) {
               mobile_number.push(v.mobile);
            });

            var params = angular.copy(data);
            params.person = null;
            params.mobile_number = mobile_number;
            $rootScope.progressbar_start();

            sent_sms.sendSms(params,function (response) {
                $rootScope.progressbar_complete();
                if(response.status=='failed_valid')
                {
                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                    $scope.status1 =response.status;
                    $rootScope.msg1 =response.msg;
                } else if(response.status== 'success'){
                    $rootScope.toastrMessages('success',response.msg);
                }else {
                    $rootScope.toastrMessages('error',response.msg);
                }
            });

        }

    });



