
angular.module('SmsModule')
    .controller('SmsServiceProviderController' ,function($state,$ngBootbox,$rootScope,$scope,$http,$timeout,$uibModal,$log,sms_service_provider,$filter) {

        $state.current.data.pageTitle = $filter('translate')('sms service provider');
        $rootScope.items=[];
        $scope.CurrentPage = 1;


        var LoadSmsProvider=function($params){
            $rootScope.progressbar_start();
            sms_service_provider.query($params,function (response) {
                $rootScope.progressbar_complete();
                $rootScope.items= response.data;
                $scope.CurrentPage = response.current_page;
                $rootScope.TotalItems = response.total;
                $rootScope.ItemsPerPage = response.per_page;
            });
        };

        $rootScope.pageChanged = function (CurrentPage) {
            LoadSmsProvider({page:CurrentPage});
        };
        $scope.itemsPerPage_ = function (itemsCount) {
            LoadSmsProvider({page:1,itemsCount:itemsCount});
        };

        $rootScope.resetTb = function () {
            LoadSmsProvider({page:$scope.CurrentPage});
        };


        $scope.createOrUpdate=function(item){
            $rootScope.clearToastr();

            $uibModal.open({
                templateUrl: '/app/modules/sms/views/modal/sms_provider_form.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope,$modalInstance,$log,sms_service_provider) {

                    if(item == 'null' || item == null){
                        $scope.row = {};
                    }else{
                        $scope.row = angular.copy(item);
                    }

                    $scope.confirm = function (params) {
                        $rootScope.clearToastr();
                        if(params.id){
                            $rootScope.progressbar_start();
                            sms_service_provider.update( params ,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                    {
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    else if(response.status=='failed_valid')
                                    {
                                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                        $scope.status1 =response.status;
                                        $scope.msg1 =response.msg;
                                    }
                                    else{
                                        $modalInstance.close();
                                        if(response.status=='success')
                                        {
                                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                            $rootScope.resetTb();
                                        }else{
                                            $rootScope.toastrMessages('error',response.msg);
                                        }


                                    }

                                },function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                    $modalInstance.close();
                                });
                        }else{
                            sms_service_provider.save( params , function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                    {
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    else if(response.status=='failed_valid')
                                    {
                                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                        $scope.status1 =response.status;
                                        $scope.msg1 =response.msg;
                                    }
                                    else{
                                        $modalInstance.close();
                                        if(response.status=='success')
                                        {
                                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                            $rootScope.resetTb();
                                        }else{
                                            $rootScope.toastrMessages('error',response.msg);
                                        }
                                    }
                                },function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                    $modalInstance.close();

                                });
                        }

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                },
                resolve: {
                }
            });

        };
        $scope.delete=function(provider_id){
            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                .then(function() {
                    $rootScope.progressbar_start();
                    sms_service_provider.delete({id:provider_id},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success')
                            {
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                LoadSmsProvider({page:$scope.CurrentPage});
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                    });
                });

        };
        $scope.updateStatus=function(id,status){
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            sms_service_provider.updateStatus({id:id,status:status},function (response) {
                $rootScope.progressbar_complete();
                if(response.status=='success')
                {
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                    LoadSmsProvider({page:$scope.CurrentPage});
                }else{
                    $rootScope.toastrMessages('error',response.msg);
                }

            });
        };

        LoadSmsProvider({page:1});

    });

