
angular.module('SmsModule')
    .controller('SmsProviderSettingsController' ,function($state,$ngBootbox,$rootScope,$scope,$http,$timeout,$uibModal,$log,org_sms_service,sms_service_provider,$filter) {

        $state.current.data.pageTitle = $filter('translate')('sms service settings');

        $rootScope.items=[];
        sms_service_provider.getActiveProvidersList(function (response) {
            $rootScope.Providers = response.Providers;
        });

        var LoadSmsService = function(params){
            $rootScope.progressbar_start();
            org_sms_service.query(params,function (response) {
                $rootScope.progressbar_complete();
                $rootScope.items= response.data;
                $scope.CurrentPage = response.current_page;
                $rootScope.TotalItems = response.total;
                $rootScope.ItemsPerPage = response.per_page;
            });
        };

        $rootScope.pageChanged = function (CurrentPage) {
            LoadSmsService({page:CurrentPage});
        };
        $scope.itemsPerPage_ = function (itemsCount) {
            LoadSmsService({itemsCount:itemsCount,page:1});
        };

        $rootScope.resetTb = function () {
            LoadSmsService({page:$scope.CurrentPage});
        };

        $scope.createOrUpdate=function(item){
            $rootScope.clearToastr();

            $uibModal.open({
                templateUrl: '/app/modules/sms/views/modal/sms_setting_form.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'sm',
                controller: function ($rootScope,$scope,$modalInstance,$log) {

                    if(item == 'null' || item == null){
                        $scope.action =0;
                        $scope.row={sms_provider:"",api_username:"",api_password:""};
                    }else{
                        $scope.action =1;
                        $scope.row = angular.copy(item);
                    }

                    $scope.confirm = function (params) {
                        $rootScope.clearToastr();
                        $rootScope.progressbar_start();
                        if($scope.action == 1){
                            params.id =params.provider_id;
                            org_sms_service.update( params ,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    $modalInstance.close();
                                    if(response.status=='success')
                                    {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        $rootScope.resetTb();
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }


                                }

                            },function(error) {
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();
                            });
                        }else{
                            org_sms_service.save( params , function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    $modalInstance.close();
                                    if(response.status=='success')
                                    {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        $rootScope.resetTb();
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                }
                            },function(error) {
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();

                            });
                        }

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                },
                resolve: {
                }
            });

        }

        $scope.updateDefault= function (id,status) {
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            org_sms_service.updateDefault({ id: id,status:status},function (response) {
                $rootScope.progressbar_complete();
                if(response.status=='error' || response.status=='failed' || response.status=='alert')
                    {
                        $rootScope.toastrMessages('error',$filter('translate')(' have default providers'));
                    }
                    else if(response.status=='failed_valid')
                    {
                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                        $scope.status1 =response.status;
                        $scope.msg1 =response.msg;
                    }
                    else{
                        if(response.status=='success')
                        {
                            LoadSmsService({page:$scope.CurrentPage});
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }


                    }
            });
        };
        $scope.updateActive= function (provider,status) {
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            org_sms_service.updateActive({ id: provider.provider_id,api_username:provider.api_username,status:status},function (response) {
                $rootScope.progressbar_complete();
                if(response.status=='error' || response.status=='failed' || response.status=='alert') {
                        $rootScope.toastrMessages('error',response.msg);
                    }
                    else if(response.status=='failed_valid')
                    {
                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                        $scope.status1 =response.status;
                        $scope.msg1 =response.msg;
                    }
                    else{
                        if(response.status=='success')
                        {
                            LoadSmsService({page:$scope.CurrentPage});
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }


                    }
                });
        };
        $scope.delete = function (service){
            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                .then(function() {
                    $rootScope.progressbar_start();
                    org_sms_service.delete({id:service.provider_id,'api_username':service.api_username},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                LoadSmsService({page:$scope.CurrentPage});
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                  });
                });

        };

        LoadSmsService({page:1});

    });

