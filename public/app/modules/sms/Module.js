

var SmsModule = angular.module("SmsModule",[]);
angular.module('SmsModule').config(function ($stateProvider) {
    $stateProvider
        .state('sms-providers', {
            url: '/sms/providers',
            templateUrl: '/app/modules/sms/views/providers.html',
            controller: "SmsServiceProviderController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SmsModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sms/controllers/SmsServiceProviderController.js',
                            '/app/modules/sms/services/SmsService.js'
                        ]
                    });
                }]
            }
        })
        .state('sms-settings', {
            url: '/sms/settings',
            templateUrl: '/app/modules/sms/views/settings.html',
            controller: "SmsProviderSettingsController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SmsModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sms/controllers/SmsProviderSettingsController.js',
                            '/app/modules/sms/services/SmsService.js'
                        ]
                    });
                }]
            }
        })
        .state('sent-sms', {
             url: '/sms/sent-sms',
            templateUrl: '/app/modules/sms/views/sent-sms.html',
            data: {pageTitle: ''},
            controller: "SentSmsController",
                 resolve: {
                     deps: ['$ocLazyLoad', function($ocLazyLoad) {
                         return $ocLazyLoad.load({
                             name: 'SmsModule',
                             files: [
                                 '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                 '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                 '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                 '/app/modules/sms/controllers/SentSmsController.js',
                                 '/app/modules/sms/services/SmsService.js'

                             ]
                         });
                     }]
                 }
        });
});
