// angular.module('SponsorshipModule', ['SponsorshipController', 'SponsorshipService']);
var SponsorshipModule = angular.module("SponsorshipModule",['SponsorshipService']);

angular.module('SponsorshipModule').config(function ($stateProvider) {
    $stateProvider
        .state('show-case', {
            url: '/sponsorship/cases/show/:id/:target',
            templateUrl: '/app/modules/sponsorship/views/caseForm/show/main.html',
            params: {id:null},
            data: {pageTitle: ''},
            controller: "showCaseController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/case-form/show/showCaseController.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/forms/services/FormCaseDataService.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/common/services/CategoriesService.js'
                        ]
                    });
                }]
            }

        })
        .state('edit-case', {
            url: '/sponsorship/cases/edit/:id/:target',
            templateUrl: '/app/modules/sponsorship/views/caseForm/edit/main.html',
            params: {id:null,target:null},
            controller: "editCaseController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/case-form/edit/editCaseController.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/forms/services/FormCaseDataService.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js'
                        ]
                    });
                }]
            }

        })
        .state('case', {
            url: '/sponsorship/case/:action/:target/:id',
            templateUrl: '/app/modules/sponsorship/views/caseForm/sub-edit.html',
            params:{action: null,target:null,'id':null },
            controller: "subEditCaseController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        reload: false,
                        files: [

                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/case-form/edit/subEditCaseController.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/common/services/CaseService.js'
                        ]
                    });
                }]
            }

        })
        .state('edit-parent', {
            url: '/sponsorship/edit-parent/:id',
            templateUrl: '/app/modules/sponsorship/views/caseForm/edit-parent.html',
            params:{id:null},
            data: {pageTitle: ''},
            controller: "EditParentInfoController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/case-form/edit/EditParentInfoController.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/common/services/CategoriesService.js'
                        ]
                    });
                }]
            }

        })
        /* ********************************* STATISTIC SPONSORSHIPS CASES ********************************* */
        .state('sponsorshipCases', {
            url: '/sponsorship/:type/cases',
            templateUrl: '/app/modules/sponsorship/views/cases/index.html',
            params:{mode: null,status:null,'type':null },
            data: {pageTitle: ''},
            controller: "sponsorshipCasesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/CasesController.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/forms/services/CustomFormsService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',


                        ]
                    });
                }]
            }

        })
        /* ********************************* FAMILIES CASES ********************************* */
        .state('families', {
            url: "/sponsorship/families",
            templateUrl: '/app/modules/sponsorship/views/cases/families.html',
            data: {pageTitle: ''},
            params: {mode:null,status:null},
            controller: "familiesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/sponsorship/controllers/CasesController.js',

                        ]
                    });
                }]
            }

        })
        /* ********************************* ORGANIZATIONS CASES ********************************* */
        .state('organizationsCases', {
            url: '/sponsorship/organizations',
            templateUrl: '/app/modules/sponsorship/views/cases/organizations.html',
            params:{},
            data: {pageTitle: ''},
            controller: "organizationsCasesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/CasesController.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/forms/services/CustomFormsService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',


                        ]
                    });
                }]
            }

        })

        /* ********************************* SPONSORSHIPS CASES ********************************* */
       .state('sponsorship-cases-statistics', {
            url: '/sponsorship/cases/statistics/:id',
           templateUrl: '/app/modules/sponsorship/views/cases/statistics.html',
           data: {pageTitle: ''},
            params:{id:null},
            controller: "sponsorshipCasesStatisticsController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                        '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/app/modules/sponsorship/controllers/sponsorshipCasesStatisticsController.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js',
                            '/app/modules/setting/services/locationsService.js'

                        ]
                    });
                }]
            }

        })
       /* ********************************* SPONSORSHIPS CASES ********************************* */
       .state('reports-index', {
            url: '/sponsorship/reports/{id}',
           templateUrl: '/app/modules/sponsorship/views/cases/reports.html',
            data: {pageTitle: ''},
            params: {mode:null,id:null},
            controller: "ReportsController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/ReportsController.js',
                            '/app/modules/organization/services/OrgService.js'

                        ]
                    });
                }]
            }

        })
       .state('reports-attachments', {
            url: '/sponsorship/report/attachments',
           templateUrl: '/app/modules/sponsorship/views/cases/reports-attachments.html',
            data: {pageTitle: ''},
            params: {mode:null,id:null},
            controller: "ReportsAttachmentsController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/ReportsAttachmentsController.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js'
                        ]
                    });
                }]
            }

        })
       .state('case-reports-attachments', {
            url: '/sponsorship/cases/:id/reports/attachments',
           templateUrl: '/app/modules/sponsorship/views/cases/case-reports-attachments.html',
            data: {pageTitle: ''},
            params: {id:null},
           controller: "CaseReportsAttachmentsController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/CaseReportsAttachmentsController.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js'
                        ]
                    });
                }]
            }

        })
        /* ************************************************************************************************ */
        .state('categories-sponsorships-form', {
            url: '/sponsorship/categories-sponsorships-form/:child1_id?mode="add"&person_id&case_id&category_id&mother_id&father_id&guardian_id&brother_id',
            templateUrl: '/app/modules/sponsorship/views/categories-sponsorships-form.html',
            data: {pageTitle: ''},
            controller: "CategoriesSponsorshipFormController",
            params: {mode:null,child_id: null,child1_id:null,guardian_id:null,mother_id:null,father_id:null,person_id:null,case_id:null,category_id:null},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/app/modules/sponsorship/controllers/CategoriesSponsorshipFormController.js',
                            '/app/modules/common/services/CategoriesService.js'
                        ]
                    });
                }]
            }

        })

        /* ********************************* SPONSORSHIPS ********************************* */
        .state('nomination-list', {
            url: '/sponsorship/nomination/:sponsor_id',
            templateUrl: '/app/modules/sponsorship/views/sponsorship/index.html',
            data: {pageTitle: ''},
            params: {sponsor_id:null},
            controller: "SponsorshipsOperationController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/NominationCasesController.js',
                            '/app/modules/organization/services/OrgService.js'

                        ]
                    });
                }]
            }

        })
        .state('nomination-persons', {
            url: '/sponsorship/nomination/persons/:sponsorship_id',
            templateUrl: '/app/modules/sponsorship/views/sponsorship/persons.html',
            data: {pageTitle: ''},
            params:{sponsorship_id:null},
            controller: "NominationCasesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/NominationCasesController.js'


                        ]
                    });
                }]
            }

        })
       /* ********************************* PAYMENTS ********************************* */
        .state('payments-index', {
            url: '/sponsorship/payments/:id',
            templateUrl: '/app/modules/sponsorship/views/payment/index.html',
            data: {pageTitle: ''},
            params: {mode:null,id:null},
            controller: "AllPaymentsController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/payment/payments.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js'

                        ]
                    });
                }]
            }

        })
        .state('payments-persons', {
            url: '/sponsorship/payments/persons/:id',
            templateUrl: '/app/modules/sponsorship/views/payment/persons.html',
            data: {pageTitle: ''},
            params: {id:null},
            controller: "PersonsPaymentsController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/payment/payments.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/setting/services/locationsService.js'

                        ]
                    });
                }]
            }

        })

        .state('archive-cheques', {
            url: '/sponsorship/archive-cheques',
            templateUrl: '/app/modules/sponsorship/views/payment/cheque/archive-cheques.html',
            data: {pageTitle: ''},
            params: {id:null},
            controller: "archiveChequesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/payment/payments.js',
                            '/app/modules/organization/services/OrgService.js'
                        ]
                    });
                }]
            }

        })
        .state('payments-cheques-print', {
            url: '/sponsorship/cheques',
            templateUrl: '/app/modules/sponsorship/views/payment/cheque/merge-cheques.html',
            data: {pageTitle: ''},
            params: {id:null},
            controller: "PaymentsChequesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/payment/payments.js',
                            '/app/modules/organization/services/OrgService.js'
                        ]
                    });
                }]
            }

        })
        .state('payments-import', {
            url: '/sponsorship/payment/import',
            templateUrl: '/app/modules/sponsorship/views/payment/import.html',
            data: {pageTitle: ''},
            params: {id:null},
            controller: "ImportPaymentsController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/sponsorship/controllers/payment/ImportPaymentsController.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/organization/services/OrgService.js'
                        ]
                    });
                }]
            }

        })
        .state('payment-form', {
            url: '/sponsorship/payment',
            templateUrl: '/app/modules/sponsorship/views/payment/form/main.html',
            controller: "BasicPaymentController",
            params:{payment_id:null,action:null,id:null},
            data: {pageTitle: '' ,stepNumber:'1'},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js',
                            '/app/modules/sponsorship/controllers/payment/form/main.js',
                            '/app/modules/setting/services/EntityService.js',
                        ]
                    });
                }]
            }

        })
        .state('payment-form.payment-details', {
            url: '/details/:action/:payment_id?id',
            templateUrl: '/app/modules/sponsorship/views/payment/form/details.html',
            data: {pageTitle: '',stepNumber:'1'},
            params:{payment_id:null,action:null,id:null},
            controller: "AddPaymentController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/payment/form/details.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/organization/services/OrgService.js',




                        ]
                    });
                }]
            }

        })
        .state('payment-form.payment-import-person', {
            url: '/import/:action/:payment_id?id',
            templateUrl: '/app/modules/sponsorship/views/payment/form/import.html',
            data: {pageTitle: '',stepNumber:'2'},
            params:{payment_id:null,action:null,id:null},
            controller: "ImportPaymentPersonController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js',
                            '/app/modules/sponsorship/controllers/payment/form/import.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js'

                        ]
                    });
                }]
            }

        })
        .state('payment-form.payment-cases', {
            url: '/persons/:action/:payment_id?id',
            templateUrl: '/app/modules/sponsorship/views/payment/form/cases.html',
            data: {pageTitle: '' ,stepNumber:'3'},
            params:{payment_id:null,action:null,id:null},
            controller: "AddPaymentPersonController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/app/modules/sponsorship/controllers/payment/form/cases.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js'


                        ]
                    });
                }]
            }

        })
        .state('payment-form.payment-receipt', {
            url: '/cheques/:action/:payment_id?id',
            templateUrl: '/app/modules/sponsorship/views/payment/form/recipients.html',
            params:{payment_id:null,action:null,id:null},
            data: {pageTitle: '' ,stepNumber:'4'},
            controller: "ChequeController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/payment/form/recipients.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js'

                        ]
                    });
                }]
            }

        })
        .state('payment-form.payment-documents', {
            url: '/documents/:action/:payment_id?id',
            templateUrl: '/app/modules/sponsorship/views/payment/form/documents.html',
            params:{payment_id:null,action:null,id:null},
            controller: "PaymentDocumentController",
            data: {pageTitle: '' ,stepNumber:'5'},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/payment/form/documents.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js'

                        ]
                    });
                }]
            }

        })
        .state('chequePrint', {
            url: "/cheque/print/{id}",
            templateUrl: '/app/modules/sponsorship/views/payment/cheque/one-cheque-print.html',
            data: {pageTitle: ' '},
            params:{id:null},
            controller: "chequePrintController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        files: [
                            '/app/modules/sponsorship/controllers/payment/chequePrint.js',
                        ]
                    });
                }]
            }
        })
        .state('paymentChequePrint', {
            url: "/sponsorship/payment/cheque/print/{id}",
            templateUrl: '/app/modules/sponsorship/views/payment/cheque/all-cheque-print.html',
            data: {pageTitle: ' '},
            params:{id:null},
            controller: "paymentChequePrintController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        files: [
                            '/app/modules/sponsorship/controllers/payment/chequePrint.js',
                        ]
                    });
                }]
            }
        })

        .state('cheque-banks-template', {
            url: '/sponsorship/cheque-banks-template',
            templateUrl: '/app/modules/sponsorship/views/payment/cheque/cheque-banks-template.html',
            data: {pageTitle: ''},
            params: {mode:null},
            controller: "ChequeBanksTemplateController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/app/modules/sponsorship/controllers/payment/payment.js',
                            '/app/modules/sponsorship/controllers/PaymentsController.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js',
                            '/app/modules/setting/services/EntityService.js'

                        ]
                    });
                }]
            }

        })
        /* ********************************* CASE FORM  ********************************* */
        .state('form', {
            url: '/sponsorship/form',
            templateUrl: '/app/modules/sponsorship/views/caseForm/add/form.html',
            // controller: "BasicController",
            params:{mode:'add',persons_id:null , father_id:null,mother_id:null , case_id:null,category_id:null },
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/app/modules/sponsorship/controllers/case-form/BasicController.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/common/services/CategoriesService.js'
                        ]
                    });
                }]
            }

        })
        .state('form.parent-info', {
            url: '/parent-info/:person_id?case_id&category_id&father_id&mother_id&guardian_id',
            templateUrl: '/app/modules/sponsorship/views/caseForm/add/parent-info.html',
            params:{mode:'add',persons_id:null , father_id:null,mother_id:null , case_id:null,category_id:null },
            data: {pageTitle: ''},
            controller: "ParentInfoController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/case-form/ParentInfoController.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/CategoriesService.js'

                        ]
                    });
                }]
            }

        })
        .state('form.guardian-info', {
            url: '/guardian-info/:person_id?case_id&category_id&father_id&mother_id&guardian_id',
            templateUrl: '/app/modules/sponsorship/views/caseForm/add/guardian-info.html',
            params:{mode:'add',persons_id:null , case_id:null,category_id:null },
            data: {pageTitle: ''},
            controller: "GuardianInfoController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/case-form/GuardianInfoController.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/common/services/CaseService.js'



                        ]
                    });
                }]
            }
        })
        .state('form.case-info', {
            url: '/case-info/:person_id?case_id&category_id&father_id&mother_id&guardian_id',
            templateUrl: '/app/modules/sponsorship/views/caseForm/add/case-info.html',
            data: {pageTitle: ''},
            params:{mode:'add',persons_id:null , case_id:null,category_id:null },
            controller: "CaseInfoController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/case-form/CaseInfoController.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/common/services/PersonsService.js',


                        ]
                    });
                }]
            }
        })
        .state('form.family-info', {
            url: '/family-info/:person_id?case_id&category_id&father_id&mother_id&guardian_id',
            templateUrl: '/app/modules/sponsorship/views/caseForm/add/family-info.html',
            params:{mode:'add',persons_id:null , case_id:null,category_id:null },
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/case-form/FamilyInfoController.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/common/services/PersonsService.js',

                        ]
                    });
                }]
            }
        })
        .state('form.case-documents', {
            url: '/case-documents/:person_id?case_id&category_id&father_id&mother_id&guardian_id',
            templateUrl: '/app/modules/sponsorship/views/caseForm/add/case-documents.html',
            data: {pageTitle: ''},
            params:{mode:'add',persons_id:null , case_id:null,category_id:null },
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/app/modules/sponsorship/controllers/case-form/CaseDocumentController.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js'

                        ]
                    });
                }]
            }

        })
        .state('form.family-documents', {
            url: '/family-documents/:person_id?case_id&category_id&father_id&mother_id&guardian_id',
            templateUrl: '/app/modules/sponsorship/views/caseForm/add/family-documents.html',
            params:{mode:'add',persons_id:null , case_id:null,category_id:null },
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/app/modules/sponsorship/controllers/case-form/FamilyDocumentController.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js'
                        ]
                    });
                }]
            }

        })
        .state('form.custom-form-data', {
            url: '/custom-form-data/:person_id?case_id&category_id&father_id&mother_id&guardian_id',
            templateUrl: '/app/modules/sponsorship/views/caseForm/add/custom-form-data.html',
            data: {pageTitle: ''},
            params:{mode:'add',persons_id:null , case_id:null,category_id:null },
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/app/modules/sponsorship/controllers/case-form/CustomFormDataController.js',
                            '/app/modules/forms/services/FormElementsService.js',
                            '/app/modules/forms/services/CustomFormsService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/forms/services/FormCaseDataService.js'
                        ]
                    });
                }]
            }

        })
        .state('sub', {
            url: '/sponsorship/sub',
            templateUrl: '/app/modules/sponsorship/views/caseForm/add/sub/sub.html',
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/app/modules/sponsorship/controllers/case-form/sub/subFormController.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/common/services/CategoriesService.js'
                        ]
                    });
                }]
            }

        })
        .state('sub.fm-case-info', {
            url: '/fm-case-info/:id?father_id&category_id&mother_id&guardian_id&case_id',
            templateUrl: '/app/modules/sponsorship/views/caseForm/add/sub/fm-case-info.html',
            params:{mode:'add',id:null , case_id:null,mother_id:null,guardian_id:null,father_id:null,category_id:null },
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsorship/controllers/case-form/sub/subCaseInfoController.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/common/services/CaseService.js'


                        ]
                    });
                }]
            }
        })
        .state('sub.fm-case-documents', {
            url: '/fm-case-documents/:id?father_id&category_id&mother_id&guardian_id&case_id',
            templateUrl: '/app/modules/sponsorship/views/caseForm/add/sub/fm-case-documents.html',
            params:{mode:'add',id:null , case_id:null,mother_id:null,guardian_id:null,father_id:null,category_id:null },
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/app/modules/sponsorship/controllers/case-form/sub/subCaseDocumentController.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js'

                        ]
                    });
                }]
            }

        })
        .state('sub.fm-custom-form-data', {
            url: '/fm-custom-form-data/:id?father_id&category_id&mother_id&guardian_id&case_id',
            templateUrl: '/app/modules/sponsorship/views/caseForm/add/sub/fm-custom-form-data.html',
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorshipModule',
                        files: [
                            '/app/modules/sponsorship/controllers/case-form/sub/subCustomFormDataController.js',
                            '/app/modules/forms/services/FormElementsService.js',
                            '/app/modules/forms/services/CustomFormsService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/forms/services/FormCaseDataService.js'

                        ]
                    });
                }]
            }

        });

});
