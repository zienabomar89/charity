angular.module('SponsorshipService', []);

angular.module('SponsorshipService')
    .factory('sponsor_cases', function ($resource) {
        return $resource('/api/v1.0/sponsorship/sponsor_cases/:operation/:id/:name', {id: '@id',page: '@page'}, {
            getSponsorCases: { method:'POST' ,params:{operation:'getSponsorCases'}, isArray:false },
            getSponsorCaseInfo: { method:'GET' ,params:{operation:'getSponsorCaseInfo'}, isArray:false },
            exportSponsorCase: { method:'POST' ,params:{operation:'exportSponsorCase'}, isArray:false },
        });
    })
    .factory('nominate', function ($resource) {
        return $resource('/api/v1.0/sponsorship/nominate/:operation/:id', {id: '@id',page: '@page'}, {
            list: {method: 'GET',params:{operation:'list'}, isArray:false},
            filterSponsorships:{method:'POST',params:{operation:'filterSponsorships',page: '@page'}},
            persons:{method:'GET',params:{operation:'persons',id:'@id' ,page: '@page'}, isArray:false},
            updateStatus:{method: 'PUT',params:{operation:'updateStatus',id: '@id'}}
        });
    })
    .factory('related', function ($resource) {
        return $resource('/api/v1.0/sponsorship/related/:operation/:id', {id: '@id',page: '@page'}, {
            getFamily:{method:'POST',params:{operation:'getFamily'}, isArray:false},
            updateStatusTemplate:{method:'GET',params:{operation:'updateStatusTemplate'}, isArray:false}
        });
    })
    .factory('casesInfo', function ($resource) {
        return $resource('/api/v1.0/sponsorship/cases-info/:operation/:id/:name', {id: '@id',page: '@page',name:'@name'}, {
            getFamilies  : { method:'POST' ,params:{operation:'getFamilies',page: '@page'}, isArray:false },
            getCasesStatistics  : { method:'POST' ,params:{operation:'getCasesStatistics',page: '@page'}, isArray:false },
            sponsorshipCasesFilter  : { method:'POST' ,params:{operation:'sponsorshipCasesFilter',page: '@page'}, isArray:false },
            getCasesStatusLog:{method:'GET',params:{operation:'getCasesStatusLog',id:'@id'}, isArray:false},
            UpdateCasesStatus  : { method:'POST' ,params:{operation:'UpdateCasesStatus'}, isArray:false },
            UpdateSponsorshipDate:{method: 'PUT'  , params: { operation:'UpdateSponsorshipDate',id: '@id'}, isArray: false},
            updateSponsorNumber:{method:'PUT',params:{operation:'updateSponsorNumber'}, isArray:false},
            UpdateSponsorshipStatus  : { method:'POST' ,params:{operation:'UpdateSponsorshipStatus'}, isArray:false },
        });
    })
    .factory('reports', function ($resource) {
        return $resource('/api/v1.0/sponsorship/reports/:operation/:id/:name', {id: '@id',page: '@page'}, {
            filter  : { method:'POST' ,params:{operation:'filter',page: '@page'}, isArray:false },
            attachments  : { method:'POST' ,params:{operation:'attachments',page: '@page'}, isArray:false },
            caseAttachments  : { method:'POST' ,params:{operation:'caseAttachments',page: '@page'}, isArray:true },
            generateWithOptions  : { method:'POST' ,params:{operation:'generateWithOptions'}, isArray:false },
            setAttachment  : { method:'POST' ,params:{operation:'setAttachment',page: '@page'}, isArray:false },
            getSponsorCasesList:{method: 'GET'  , params: { operation:'getSponsorCasesList',id: '@id'}, isArray: true},
            getCaseInfo:{method: 'GET'  , params: { operation:'getCaseInfo',id: '@id'}, isArray: false}
        });
    })
    .factory('paymentsService', function ($resource) {
        return $resource('/api/v1.0/sponsorship/paymentsService/:operation/:id', {id: '@id',page: '@page'}, {
            exportCustom  : { method:'POST' ,params:{operation:'exportCustom'}, isArray:false },
            map  : { method:'POST' ,params:{operation:'map'}, isArray:false },
            all  : { method:'POST' ,params:{operation:'all'}, isArray:false },
            persons  : { method:'POST' ,params:{operation:'persons'}, isArray:false },
            SponsorPayments  : { method:'POST' ,params:{operation:'SponsorPayments'}, isArray:false },
            SponsorPersons  : { method:'POST' ,params:{operation:'SponsorPersons'}, isArray:false },
            archivedCheques  : { method:'Post' ,params:{operation:'archivedCheques'}, isArray:false },
            statistic: {method: 'GET',params:{operation:'statistic'}, isArray:false},
            updateBeneficiaryStatus :{method: 'PUT',params:{operation:'updateBeneficiaryStatus',id: '@id'}},
            update:{method: 'PUT', params: {id: '@id'}, isArray: false},
            getPaymentCases:{method:'GET',params:{operation:'getPaymentCases',id:'@id'}, isArray:false},
            setPaymentsPerson:{method:'GET',params:{operation:'setPaymentsPerson',id:'@id'}, isArray:false},
            updateStatus :{method: 'PUT',params:{operation:'updateStatus',id: '@id'}},
            getStatistic:{method:'POST',params:{operation:'getStatistic'}, isArray:false},
            savePaymentCases  :{method: 'PUT',params:{operation:'savePaymentCases',id: '@id'}},
            saveCaseRecipient  :{method: 'PUT',params:{operation:'saveCaseRecipient',id: '@id'}},
            updateGuaranteed :{method: 'PUT',params:{operation:'updateGuaranteed',id: '@id'}},
            updateDateRange:{method:'PUT',params:{operation:'updateDateRange',id: '@id'}, isArray:false},
            saveRecipient :{method: 'PUT',params:{operation:'saveRecipient',id: '@id'}},
            updatePaymentsRecipient  :{method: 'PUT',params:{operation:'updatePaymentsRecipient'}},
            updateRecipientStatus :{method: 'PUT',params:{operation:'updateRecipientStatus',id: '@id'}},
            getPaymentDocuments:{method:'GET',params:{operation:'getPaymentDocuments'}, isArray:false},
            storePaymentDocument:{method:'POST',params:{operation:'storePaymentDocument'}, isArray:false},
            deleteDocument   : { method:'DELETE', params: {operation:'deleteDocument'}, isArray: false},
            saveAccount  : { method:'POST' ,params:{operation:'saveAccount'}, isArray:false },
            duplicate:{method:'POST',params:{operation:'duplicate'}, isArray:false},
            list: {method: 'GET',params:{operation:'list'}, isArray:false},
            districtRange  : { method:'Post' ,params:{operation:'districtRange'}, isArray:false },
            result  : { method:'Post' ,params:{operation:'result'}, isArray:false },
            saveCheques  : { method:'Post' ,params:{operation:'saveCheques'}, isArray:false },
            updateChequeNumber:{method:'PUT',params:{operation:'updateChequeNumber',id: '@id'}, isArray:false},
            getPaymentChequeData:{method:'GET',params:{operation:'getPaymentChequeData',id:'@id'}, isArray:false},
            getChequeData:{method:'GET',params:{operation:'getChequeData',id:'@id'}, isArray:false},
            deleteGroup  : { method:'Post' ,params:{operation:'deleteGroup'}, isArray:false },

        });
    })
    .service('popupService', function($window){
        this.showPopup=function(message){
            return $window.confirm(message);
        }
    });
