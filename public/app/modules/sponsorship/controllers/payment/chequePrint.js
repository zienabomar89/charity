

// one cheque
angular.module('SponsorshipModule')
    .controller('chequePrintController', function ($scope,$rootScope,$state,$stateParams,paymentsService,$filter) {

        $scope.today=$filter('date')(new Date(), 'yyyy/MM/dd');
        if($stateParams.id){
            paymentsService.getChequeData({id:$stateParams.id},function (repsonse) {
                $scope.row = repsonse.row;
            });
        }

    });

// payment
angular.module('SponsorshipModule')
    .controller('paymentChequePrintController', function ($scope,$rootScope,$state,$stateParams,paymentsService,$filter) {

        $scope.today=$filter('date')(new Date(), 'yyyy/MM/dd');
        $rootScope.gFlashMessages = false;
        $scope.rows={};
        if($stateParams.id){
            paymentsService.getPaymentChequeData({id:$stateParams.id},function (repsonse) {
                $scope.rows = repsonse;
            });


        }



    });
