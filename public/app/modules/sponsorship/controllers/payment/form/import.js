

angular.module('SponsorshipModule')
    .service('fileUpload', ["$http", "$rootScope", function ($http, $rootScope) {
        this.uploadFileToUrl = function(file,payment_id,action,row){

            $rootScope.clearToastr();
            $rootScope.status ='';
            $rootScope.msg ='';
            var fd = new FormData();
            var fd2 = new FormData();

            fd.append('payment_id', payment_id);
            fd.append('file', file);

            if(!action){
                $rootScope.items=[];
                $rootScope.progressbar_start();
                $http.post('/importExcel', fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined,'Process-Data': false}
                    })
                    .success(function(response){
                        $rootScope.progressbar_complete();
                        if(response.status == 'failed'){
                            $rootScope.toastrMessages('error',response.msg);
                        }else{

                            if(response.map.length != 0){
                                $rootScope.doimportExcel=true;
                                $rootScope.mapitems = true;

                                $rootScope.items = response.map;
                            }else{
                                $rootScope.mapitems = false;
                            }
                        }

                    })
                    .error(function(error){
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);
                    });

            }
            else{
                angular.forEach(row, function(val,key)
                {
                    angular.forEach(val, function(v,k)
                    {
                        fd.append('row['+key+ '][' + k +']', v);
                    });
                });


                $rootScope.progressbar_start();
                $http.post('/doImportExcel', fd, {
                        transformRequest: angular.identity,
                        headers: {'Content-Type': undefined,'Process-Data': false}
                    })
                    .success(function(response){
                        $rootScope.progressbar_complete();
                        if(response.status==true){
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            $rootScope.toggle=false;
                            $rootScope.items=[];
                            $rootScope.mapitems = false;
                            $rootScope.doimportExcel=false;
                            $rootScope.tableContent();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    })
                    .error(function(error){
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);
                    });



            }


        }

    }])
    .controller('ImportPaymentPersonController', function ($http,$filter,$scope ,OAuthToken, FileUploader,$rootScope,$state,$timeout,$stateParams,paymentsService,$uibModal,fileUpload) {

        if($stateParams.action==null){
            $stateParams.action='new';
        }

        $rootScope.action=$scope.action=$stateParams.action;


        if($stateParams.payment_id==null){
            $state.go('payment-form.payment-details',{action:'new'});
        }else
        {

            $scope.CurrentPage=1;
            $rootScope.success =false;
            $rootScope.failed =false;
            $rootScope.error =false;
            $rootScope.model_error_status =false;
            $rootScope.exceed =false;

            $rootScope.doimportExcel=false;
            $rootScope.doimportExcel=false;
            $rootScope.mapitems = false;
            $rootScope.row = [];
            $rootScope.items = [];
            $rootScope.row1=[];
            $rootScope.msg1=[];
            $rootScope.toggle = false;
            $rootScope.beneficiaries=[];
            $rootScope.new_=0;
            $rootScope.old_=0;
            $rootScope.no_payment=0;
            $rootScope.count=0;
            $rootScope.payment_id=$stateParams.payment_id;

            var PaymentCases =function(){
                $rootScope.clearToastr();
                $rootScope.progressbar_start();
                paymentsService.getPaymentCases({'id': $stateParams.payment_id ,'target': 'all',status:$scope.status,'paginate':true,page:$scope.CurrentPage}, function (response) {
                    $rootScope.progressbar_complete();

                    if(response.data.total != 0){
                        $rootScope.beneficiaries= response.data.data;
                        $scope.CurrentPage = response.data.current_page;
                        $scope.TotalItems = response.data.total;
                        $scope.ItemsPerPage = response.data.per_page;
                        // $rootScope.Beneficiary= response.Beneficiary;
                        // $rootScope.new_=response.new_;
                        // $rootScope.old_=response.old_;
                        // $rootScope.no_payment=response.no_payment;
                        // $rootScope.count=response.count;
                    }else{
                        $rootScope.beneficiaries=[];
                        // $rootScope.Beneficiary= 0;
                        // $rootScope.new_=0;
                        // $rootScope.old_=0;
                        // $rootScope.no_payment=0;
                        // $rootScope.count=0;
                    }


                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });
            };

            var loadStatistic =function(){
                $rootScope.progressbar_start();
                paymentsService.statistic({'id': $stateParams.payment_id}, function (response) {
                    $rootScope.progressbar_complete();
                    $rootScope.is_mine= response.is_mine;
                    $rootScope.sponsorship_id= response.sponsorship_id;
                    $rootScope.Beneficiary= response.Beneficiary;
                    $rootScope.new_=response.new_;
                    $rootScope.old_=response.old_;
                    $rootScope.no_payment=response.no_payment;
                    $rootScope.count=response.count;

                });
            };

            loadStatistic();

            $scope.download=function(){
                $rootScope.clearToastr();
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-person-to-payment-template&template=true';
            };
            $scope.allCheckbox=function(value){

                $scope.selectedAll = value == true ? true: false;
                angular.forEach($rootScope.beneficiaries, function(v, k) {
                    v.checked=$scope.selectedAll;
                });
            };
            $rootScope.tableContent = function(){
                // PaymentCases();
                loadStatistic();
            };
            $scope.pageChanged = function (currentPage) {
                $scope.CurrentPage=currentPage;
                PaymentCases();
            };
    

            $scope.set=function(value,action){
                $rootScope.clearToastr();
                var params = {'id':$stateParams.payment_id,action:action ,value:value,selectedStatus:$scope.status};
                var pass=true;
                var notAllow=0;
                var checked=0;
                var target = value == 2 ? 3: 2;

                if(action =='sub'){
                    var cases=[];
                    angular.forEach($rootScope.beneficiaries, function(v, k) {
                        if(v.checked){
                            if(v.status == target){
                                cases.push({'case_id':v.case_id,'sponsor_number':v.sponsor_number});
                            }else{
                                notAllow++;
                            }
                            checked++;
                        }
                    });

                    if(cases.length==0){
                        if(notAllow == checked){
                            pass=false;
                            $rootScope.toastrMessages('error',$filter('translate')('All existing beneficiaries may not be converted to the selected status'));
                        }
                        else if(checked == 0){
                            pass=false;
                            $rootScope.toastrMessages('error',$filter('translate')('You have not selected any beneficiary to change status'));
                        }
                    }else{
                        params.cases=cases;
                    }
                }

                if(pass == true){
                    $rootScope.progressbar_start();
                    paymentsService.updateStatus(params,function (response) {
                        $rootScope.progressbar_complete();
                        $rootScope.clearToastr();
                        if(response.status=='success')
                        {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            loadStatistic();
                            PaymentCases();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                }
            };

            var ResetMsgFlag=function(){
                $rootScope.success =false;
                $rootScope.failed =false;
                $rootScope.error =false;
                $rootScope.model_error_status =false;
                $rootScope.exceed =false;

            };
            $rootScope.close=function(){
                ResetMsgFlag();
            };

            $scope.empty = function () {
                $rootScope.clearToastr();
                $rootScope.progressbar_start();
                var deleted = new paymentsService($rootScope.payment_id);
                deleted.$delete({id:$rootScope.payment_id,action:'empty'})
                    .then(function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status =='success') {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            if(action == 'empty'){
                                loadStatistic();
                                $rootScope.new_=0;
                                $rootScope.old_=0;
                                $rootScope.no_payment=0;
                                $rootScope.count=0;
                                $rootScope.beneficiaries=[];
                            }
                        }else{
                            $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                        }
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
            };
            $scope.setBeneficiary=function(source){
                ResetMsgFlag();
                $rootScope.clearToastr();

                if(source == 'sponsorship' || source == 'sponsor' ){
                    $rootScope.toggle=false;
                    $rootScope.items=[];
                    $rootScope.mapitems = false;
                    $rootScope.doimportExcel=false;

                    $rootScope.progressbar_start();
                    paymentsService.setPaymentsPerson({'id':$rootScope.payment_id ,'source':source}, function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status){
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            loadStatistic();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                }
                else{
                    $rootScope.toggle = $rootScope.toggle === false ? true: false;
                    $rootScope.items=[];
                    $rootScope.mapitems = false;
                    $rootScope.doimportExcel=false;
                    angular.forEach(angular.element("input[type='file']"), function(inputElem) {
                        angular.element(inputElem).val(null);
                    });
                }
            };

            $scope.updateCaseStatus=function(status,item){
                $rootScope.clearToastr();
                $rootScope.progressbar_start();
                paymentsService.updateBeneficiaryStatus({'id':$stateParams.payment_id,status:status,'case_id':item.case_id,'sponsor_number':item.sponsor_number},function (response) {
                    $rootScope.progressbar_complete();
                    $rootScope.clearToastr();
                    if(response.status=='success')
                    {
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        PaymentCases();
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });
            };

            $scope.previous=function(){
                angular.element('#Step1').removeClass("disabled");
                $state.go('payment-form.payment-details',{action:'edit',payment_id:$stateParams.payment_id});

            };
            $scope.next = function () {
                angular.element('#Step2').removeClass("disabled");
                $state.go('payment-form.payment-cases',{action:'edit',payment_id:$stateParams.payment_id});
            };
            $scope.ExportToExcel=function(target){
                $rootScope.clearToastr();
                $rootScope.progressbar_start();
                angular.element('.btn').addClass("disabled");
                $http({
                    url: "/api/v1.0/sponsorship/paymentsService/exportCaseList",
                    method: "POST",
                    data: {target:target, payments_id:$stateParams.payment_id, action:'all'}
                }).then(function (response) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }, function(error) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled");
                    $rootScope.toastrMessages('error',error.data.msg);
                });
            };
            $scope.load=function(status){
                $scope.status = status;
                PaymentCases();

            };

            /// ******************************* ///

            var Uploader = $scope.uploader = new FileUploader({
                url: '/api/v1.0/sponsorship/paymentsService/importExcel',
                headers: {
                    Authorization: OAuthToken.getAuthorizationHeader()
                }
            });

            $scope.fileUpload=function(item){
                $timeout(function() {
                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                        item.formData = [{payment_id:$rootScope.payment_id}];
                    };
                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(response.status==true){
                            $rootScope.toastrMessages('success',response.msg);
                            $rootScope.toggle=false;
                            $rootScope.items=[];
                            $rootScope.mapitems = false;
                            $rootScope.doimportExcel=false;
                            $rootScope.tableContent();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    };
                    Uploader.uploadAll();
                }, 5);
            };
            /// ******************************* ///
            $scope.fileChanged_ = function($event){
                $scope.myFile=$event.target.files[0];
                $rootScope.doimportExcel=false;
                $rootScope.mapitems = false;
                $rootScope.mapitems = false;
                $rootScope.items = [];
                $rootScope.success =false;
                $rootScope.failed =false;
                $rootScope.error =false;
                $rootScope.msg ='';
            };
            $scope.import_ = function(){
                $rootScope.clearToastr();
                if( !angular.isUndefined($scope.myFile)) {
                    var count = 0;

                    if($rootScope.doimportExcel == true){
                        angular.forEach($rootScope.items, function(v,k) {
                            if(!v.column){
                                count++;
                            }
                        });

                        if(count == $rootScope.items.length){
                            $rootScope.toastrMessages('error',$filter('translate')('not specify the database fields corresponding to the column names in the file you selected'));
                        }else{
                            fileUpload.uploadFileToUrl($scope.myFile,$rootScope.payment_id,$rootScope.doimportExcel,$rootScope.items);
                        }
                    }
                    else{
                        $rootScope.mapitems = false;
                        $rootScope.items = [];
                        fileUpload.uploadFileToUrl($scope.myFile,$rootScope.payment_id,$rootScope.doimportExcel,$rootScope.items);
                    }
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('there is no selected file to import'));
                }
            };

        }

        $rootScope.setStep($state.current.data.stepNumber);
    });
