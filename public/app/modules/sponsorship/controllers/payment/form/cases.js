 angular.module('SponsorshipModule')
  .controller('AddPaymentPersonController', function ($http,$filter,$scope,$rootScope,$state,$stateParams,paymentsService,$uibModal) {

        if($stateParams.action==null){
            $stateParams.action='new';
        }

        $rootScope.action=$scope.action=$stateParams.action;
        $scope.payment_id=$rootScope.payment_id=$stateParams.payment_id;

        if($stateParams.payment_id==null){
            $state.go('payment-form.payment-details',{action:'new'});
        }else {
            $rootScope.success =false;
            $rootScope.failed =false;
            $rootScope.error =false;
            $rootScope.model_error_status =false;
            $scope.status1 ='';
            $scope.msg1=[];
            $rootScope.guaranteed=[];
            $scope.CurrentPage=1;
            $rootScope.amount_total = null;
            $rootScope.amount_total_shekel = null;
            $rootScope.totalInShekell = null;
            $rootScope.totalAfterDiscount = null;
            $rootScope.totalInShekellAfterDiscount = null;
            $scope.distribution={how:1, amount:null,months:null,to:null,from:null};

            var round = function(number, precision) {
                var factor = Math.pow(10, precision);
                var tempNumber = number * factor;
                var roundedTempNumber = Math.round(tempNumber);
                return roundedTempNumber / factor;
            };

            var loadPaymentCasesAmount =function(){
                $rootScope.clearToastr();
                $rootScope.progressbar_start();
                paymentsService.getStatistic({'payment_id': $stateParams.payment_id ,'target': 'guaranteed','paginate':true,page:$scope.CurrentPage}, function (response) {
                    $rootScope.progressbar_complete();

                    $scope.payment = response.payment;
                    $rootScope.is_mine= $scope.payment.is_mine;
                    $scope.payment_exchange = $scope.payment.payment_exchange;
                    $scope.organization_share = $scope.payment.organization_share;
                    $scope.exchange_rate = $scope.payment.exchange_rate;
                    $scope.currency_name = $scope.payment.name;
                    $rootScope.sum_case_amount = $scope.payment.sum_case_amount;
                    $rootScope.sum_case_after_org_share_amount = $scope.payment.sum_case_after_org_share_amount;
                    $rootScope.sum_case_before_org_share_shekel_amount = $scope.payment.sum_case_before_org_share_shekel_amount;
                    $rootScope.sum_case_shekel_amount = $scope.payment.sum_case_shekel_amount;

                    $rootScope.total = $scope.payment.amount;
                    $rootScope.totalInShekell = $scope.payment.total_amount_in_shekel;
                    $rootScope.totalAfterDiscount = $scope.payment.total_after_fee_discount;
                    $rootScope.totalInShekellAfterDiscount = $scope.payment.total_after_fee_discount_in_shekel;

                    var share = isNaN($scope.payment.organization_share)? 0 : $scope.payment.organization_share;
                    var fees = isNaN($scope.payment.administration_fees)? 0 : $scope.payment.administration_fees;
                    var exchange_rate = isNaN($scope.payment.exchange_rate)? 0 : $scope.payment.exchange_rate;

                    $rootScope.totalamount = $scope.payment.amount;
                    $rootScope.totalAfterDiscountfee = ($scope.payment.amount - ($scope.payment.amount * share)) - fees;
                    $rootScope.organizationShare = share;
                    $rootScope.administrationFees = fees;
                    $rootScope.totalamountshekel = round($rootScope.totalamount * exchange_rate, 2);
                    $rootScope.organizationSharesk = round(share * exchange_rate, 2);
                    $rootScope.administrationFeessk = round(fees * exchange_rate, 2);
                    $rootScope.totalAfterDiscountfeeshekel = round($rootScope.totalAfterDiscountfee * exchange_rate, 2);


                    if(response.data.total != 0){
                        $rootScope.guaranteed= response.data.data;
                        $scope.CurrentPage = response.data.current_page;
                        $scope.TotalItems = response.data.total;
                        $scope.ItemsPerPage = response.data.per_page;
                    }else{
                        $rootScope.guaranteed=[];
                    }

                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });
            };

            loadPaymentCasesAmount();

            //   Used after pagination
            $rootScope.theDate=function(param){
                var res =param.split('-');
                var odd=["01","03","05","07","08","10","12"];
                var even=["04","06","09","11"];
                var str_to="";

                if(res[1] == "02"){

                    if ( res[2].match(/^.*00$/)|| res[2].match(/^.*0$/)){
                        var mod= parseInt(res[2]) % 400 ;
                    }
                    else{
                        var mod= parseInt(res[2]) % 4 ;
                    }

                    if(mod ==0 ){
                        str_to+="29"+"-";

                    }else{
                        str_to+="28"+"-";

                    }
                }

                if(even.indexOf(res[1]) != -1){
                    str_to+="31"+"-";
                }
                if(odd.indexOf(res[1]) != -1){
                    str_to+="30"+"-";
                }

                str_to+=res[1]+"-"+res[2];

                return str_to;
            };

            $scope.changeAmount=function(value,index){

                $scope.close();
                if(!angular.isUndefined(value)) {
                    if(value != '' && value != null&& value != 0) {
                        $rootScope.guaranteed[index].amount= parseFloat(value) +"";
                        $rootScope.guaranteed[index].amount_after_discount = parseFloat(parseFloat(value) - (parseFloat(value) * $scope.organization_share))+"";

                        $rootScope.guaranteed[index].shekel_amount_before_discount = (parseFloat(value) * $scope.exchange_rate)+"";
                        $rootScope.guaranteed[index].shekel_amount = Math.round(( parseFloat(value) - (parseFloat(value) * $scope.organization_share)) * $scope.exchange_rate)+"";

                    }else{
                        $rootScope.guaranteed[index].amount= 0;
                        $rootScope.guaranteed[index].amount_after_discount= 0;
                        $rootScope.guaranteed[index].shekel_amount_before_discount= 0;
                        $rootScope.guaranteed[index].shekel_amount= 0;
                    }
                }
            };

            $scope.confirmUpdate=function(){

                $rootScope.clearToastr();
                $rootScope.progressbar_start();
                paymentsService.updateGuaranteed({id:$stateParams.payment_id, items:$rootScope.guaranteed},function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status==true){
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                    } else {
                        $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                    }
                });

            };

            $scope.previous=function(){
                angular.element('#Step3').removeClass("disabled");
                $state.go('payment-form.payment-import-person',{action:$stateParams.action,payment_id:$stateParams.payment_id});

            };

            $scope.next = function () {
                angular.element('#Step4').removeClass("disabled");
                $state.go('payment-form.payment-receipt',{action:$stateParams.action,payment_id:$stateParams.payment_id});
            };

            $scope.ExportToExcel=function(){
                $rootScope.clearToastr();
                $rootScope.progressbar_start();
                $http({
                    url: "/api/v1.0/sponsorship/paymentsService/exportCaseList",
                    method: "POST",
                    data: { payments_id:$stateParams.payment_id, action:'guaranteed'}
                }).then(function (response) {
                    $rootScope.progressbar_complete();

                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }, function(error) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled");
                    $rootScope.toastrMessages('error',error.data.msg);
                });
            };

            $scope.distribute=function(action,param){
                $rootScope.clearToastr();
                $scope.close();

                var params = {'id':$stateParams.payment_id,action:action};
                var pass = false;
                if(action =='amount'){
                    params.how=param.how;

                    if(!angular.isUndefined(param.amount)) {
                        if(param.amount != '' && param.amount != null) {
                            params.amount = parseFloat(param.amount)+"";
                            pass=true;
                        }
                    }
                }
                else{
                    if( !angular.isUndefined(param.from)) {
                        var from = $filter('date')(param.from, '01-MM-yyyy');
                    }

                    if( !angular.isUndefined(param.to)) {
                        var to = $filter('date')(param.to, '01-MM-yyyy');
                    }

                    if(new Date(from) > new Date(to)){
                        $rootScope.toastrMessages('error',$filter('translate')('Please select a period start date older than the end date of the period'));
                    }else{

                        pass=true;
                        params.date_from = from;
                        params.date_to = $rootScope.theDate(to);
                    }
                }

                if(pass == true){
                    $rootScope.progressbar_start();
                    paymentsService.savePaymentCases(params,function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            loadPaymentCasesAmount();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);
                    });

                }
            };

            $scope.toggle=function(index,recipient,item){
                $rootScope.clearToastr();
                $scope.close();
                var params = {'id':$stateParams.payment_id,recipient:recipient,case_id:item.case_id,sponsor_number:item.sponsor_number};

                $rootScope.progressbar_start();
                paymentsService.saveCaseRecipient(params,function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status==true) {
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        $rootScope.guaranteed[index].recipient=recipient;
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });

            };

            $scope.UpdateDateRange = function (size, item ,index) {
                $rootScope.clearToastr();
                $scope.close();

                $uibModal.open(
                    {
                        templateUrl: '/app/modules/sponsorship/views/payment/form/model/edit_date_range_form.html',
                        backdrop  : 'static',
                        keyboard  : false,
                        windowClass: 'modal',
                        size: size,
                        controller: function ($rootScope, $scope, $modalInstance, $log) {

                            $scope.range={from:"",to:""};
                            $scope.msg1=[];

                            $scope.confirm = function (param) {
                                $rootScope.progressbar_complete();
                                $rootScope.clearToastr();

                                if( param.to  =="" || param.from  == "") {
                                    $rootScope.toastrMessages('error',$filter('translate')('Please enter both the start and end date of the period'));
                                }else{
                                    if( !angular.isUndefined(param.from) && param.from  !="" ) {
                                        param.from=$filter('date')(param.from, '01-MM-yyyy');
                                    }
                                    if( !angular.isUndefined(param.to) && param.to  !="" ) {
                                        param.to=$filter('date')(param.to, '01-MM-yyyy');
                                    }

                                    if(new Date(param.from) > new Date(param.to)){
                                        $rootScope.toastrMessages('error',$filter('translate')('Please select a period start date older than the end date of the period'));
                                    }else{
                                        param.case_id=item.case_id;
                                        param.sponsor_number=item.sponsor_number;
                                        param.id=$rootScope.payment_id;
                                        param.from = $rootScope.theDate(param.from);
                                        param.to = $rootScope.theDate(param.to);
                                        $rootScope.progressbar_start();
                                        paymentsService.updateDateRange(param,function (response) {
                                            $rootScope.progressbar_complete();
                                            if(response.status==false)
                                            {
                                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                            }
                                            else if(response.status=='failed_valid')
                                            {
                                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                                $scope.status1 =response.status;
                                                $scope.msg1 =response.msg;
                                            }
                                            else{
                                                if(response.status==true)
                                                {
                                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                                    $rootScope.guaranteed[index].date_from =response.from;
                                                    $rootScope.guaranteed[index].date_to =response.to;
                                                }else{
                                                    $rootScope.toastrMessages('error',response.msg);
                                                }
                                                $modalInstance.close();
                                            }
                                        });
                                    }


                                }


                            };
                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };

                            $scope.dateOptions = {
                                formatYear: 'yy',
                                startingDay: 0
                            };
                            $scope.maxDate = new Date();

                            $scope.formats = ['MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                            $scope.format = $scope.formats[0];
                            $scope.today = function() {
                                $scope.dt = new Date();
                            };
                            $scope.today();
                            $scope.clear = function() {
                                $scope.dt = null;
                            };

                            $scope.open1 = function($event) {
                                $event.preventDefault();
                                $event.stopPropagation();
                                $scope.popup1.opened = true;
                            };

                            $scope.open3 = function($event) {
                                $event.preventDefault();
                                $event.stopPropagation();
                                $scope.popup3.opened = true;
                            };
                            $scope.popup3 = {
                                opened: false
                            };

                            $scope.popup1 = {
                                opened: false
                            };
                        }
                    });

            };

            $scope.pageChanged = function (currentPage) {
                $scope.CurrentPage=currentPage;
                loadPaymentCasesAmount();
            };

            
            $scope.close=function(){
                $rootScope.success =false;
                $rootScope.error =false;
                $rootScope.model_error_status =false;
            };

            $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
            $scope.maxDate = new Date();
            $scope.formats = ['MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];
            $scope.today = function() {$scope.dt = new Date();};
            $scope.today();
            $scope.clear = function() {$scope.dt = null;};
            $scope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup1.opened = true;};
            $scope.open3 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup3.opened = true;};
            $scope.popup3 = {opened: false};
            $scope.popup1 = {opened: false};
        }

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 0
        };
        $scope.maxDate = new Date();

        $scope.formats = ['MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();
        $scope.clear = function() {
            $scope.dt = null;
        };

        $scope.open1 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup1.opened = true;
        };

        $scope.open3 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup3.opened = true;
        };
        $scope.popup3 = {
            opened: false
        };

        $scope.popup1 = {
            opened: false
        };

      $rootScope.setStep($state.current.data.stepNumber);
  });
