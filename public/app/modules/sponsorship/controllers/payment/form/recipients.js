angular.module('SponsorshipModule')
    .controller('ChequeController', function ($window,$http,popupService,$filter,$scope,$rootScope,$state,$stateParams,paymentsService,Entity,$uibModal,OAuthToken, FileUploader,$ngBootbox,org_sms_service,cases) {

        if($stateParams.action==null){
            $stateParams.action='new';
        }

        $rootScope.action=$scope.action=$stateParams.action;
        $scope.payment_id=$rootScope.payment_id=$stateParams.payment_id;

        $scope.distribution={cheque_number:"",cheque_date:"",bank_id:""};


        $scope.msg1 ='';

        $scope.toggle = true;
        $rootScope.guardian =true;
        $rootScope.locations =true;

        $scope.print=false;
        $rootScope.exchange_type=null;
        if($stateParams.payment_id==null){
            $rootScope.toastrMessages('error',$filter('translate')('Your are request invalid link , you will be redirected to the vouchers page to search again and request the valid link'));
            $state.go('payment-form.payment-details',{action:'new'});

        }else {

            angular.element('.btn').removeClass("disabled");
            $rootScope.action=$scope.action=$stateParams.action;
            $scope.payment_id=$rootScope.payment_id=$stateParams.payment_id;
            $scope.recipients=[];
            $scope.distribution={cheque_number:null,cheque_date:null,bank_id:null};
            $scope.distributionChequeNumber= false;
            $scope.distributionChequeDate= false;
            $scope.distributionBankId= false;

            $rootScope.success =false;
            $rootScope.failed =false;
            $rootScope.error =false;
            $rootScope.model_error_status =false;
            $scope.status1 ='';
            $scope.msg1 ='';
            $scope.CurrentPage=1;
            $scope.toggle = true;
            $rootScope.guardian =true;
            $rootScope.locations =true;
            $rootScope.success =false;
            $scope.print=false;
            $rootScope.exchange_type=null;
            $scope.notSave= false;


            var PaymentsRecipient=function(){
                $rootScope.progressbar_start();
                paymentsService.getStatistic({'payment_id': $stateParams.payment_id ,'target': 'recipient','paginate':true,page:$scope.CurrentPage}, function (response) {
                    $rootScope.progressbar_complete();
                    $rootScope.payment = response.payment;
                    $rootScope.is_mine= $rootScope.payment.is_mine;
                    $rootScope.exchange_type = response.payment.exchange_type;
                    $rootScope.currency_name = response.payment.name;
                    $rootScope.payment_exchange = response.payment.payment_exchange;

                    if(response.data.total != 0){
                        var temp = response.data.data;
                        var print = 0;

                        angular.forEach(temp, function(v, k) {
                            var saved=0;
                            v.disabled= false;

                            v.status=parseInt(v.status);
                            if(v.bank_id == null || v.cheque_account_number ==null ){
                                print++;
                            }

                            if($rootScope.exchange_type == 3 && ( v.cheque_account_number ==null || v.cheque_date ==null )){
                                saved++;
                            }
                            else if($rootScope.exchange_type != 3 &&(  v.bank_id == null ||v.cheque_account_number ==null || v.cheque_date ==null )){
                                saved++;
                            }

                            if(v.amount != null){
                                // v.shekel_amount=Math.round(parseInt(v.amount)*$rootScope.exchange_rate);
                            }else{
                                v.shekel_amount=0;
                            }

                            if(v.bank_id == null ){
                                v.bank_id="";
                            }else{
                                v.bank_id=v.bank_id  +"";
                            }

                            if(saved != 0){
                                v.disabled= true;
                            }
                        });

                        $scope.recipients= temp;
                        $scope.CurrentPage = response.data.current_page;
                        $scope.TotalItems = response.data.total;
                        $scope.ItemsPerPage = response.data.per_page;
                    }else{
                        $scope.recipients=[];
                    }
                });
            };

            $scope.sendSms=function(type,receipt_id) {

                if(type == 'manual'){
                    var receipt = [];
                    var pass = true;

                    if(receipt_id ==0){
                        angular.forEach($scope.recipients, function (v, k) {
                            if (v.checked) {
                                receipt.push(v.person_id);
                            }
                        });
                        if(receipt.length==0){
                            pass = false;
                            $rootScope.toastrMessages('error',$filter('translate')("you did not select anyone to send them sms message"));
                            return;
                        }
                    }else{
                        receipt.push(receipt_id);
                    }

                    if(pass){
                        org_sms_service.getList(function (response) {
                            $rootScope.Providers = response.Service;
                        });
                        $uibModal.open({
                            templateUrl: '/app/modules/sms/views/modal/sent_sms_from_person_general.html',
                            backdrop  : 'static',
                            keyboard  : false,
                            windowClass: 'modal',
                            size: 'lg',
                            controller: function ($rootScope,$scope, $modalInstance) {
                                $scope.sms={source: 'check' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:"",cont_type:""};

                                $scope.confirm = function (data) {
                                    $rootScope.clearToastr();
                                    var params = angular.copy(data);
                                    params.receipt=receipt;
                                    params.target='person';

                                    $rootScope.progressbar_start();
                                    org_sms_service.sendSmsToTarget(params,function (response) {
                                        $rootScope.progressbar_complete();
                                        if(response.status=='error' || response.status=='failed') {
                                            $rootScope.toastrMessages('error',response.msg);
                                        }
                                        else if(response.status== 'success'){
                                            if(response.download_token){
                                                var file_type='xlsx';
                                                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                            }

                                            $modalInstance.close();
                                            $rootScope.toastrMessages('success',response.msg);
                                        }
                                    }, function(error) {
                                        $rootScope.progressbar_complete();
                                        $rootScope.toastrMessages('error',error.data.msg);
                                    });
                                };

                                $scope.cancel = function () {
                                    $modalInstance.dismiss('cancel');
                                };

                            },
                            resolve: {
                            }
                        });

                    }
                }
                else{
                    org_sms_service.getList(function (response) {
                        $rootScope.Providers = response.Service;
                    });
                    $uibModal.open({
                        templateUrl: '/app/modules/sms/views/modal/sent_sms_from_xlx_general.html',
                        backdrop  : 'static',
                        keyboard  : false,
                        windowClass: 'modal',
                        size: 'lg',
                        controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                            $scope.upload=false;
                            $scope.sms={source: 'file' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:"",cont_type:""};

                            $scope.RestUpload=function(value){
                                if(value ==2){
                                    $scope.upload=false;
                                }
                            };

                            $scope.fileUpload=function(){
                                $scope.uploader.destroy();
                                $scope.uploader.queue=[];
                                $scope.upload=true;
                            };
                            $scope.download=function(){
                                window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-to-send-sms-template&template=true';
                            };

                            var Uploader = $scope.uploader = new FileUploader({
                                url: 'api/v1.0/sms/excel',
                                removeAfterUpload: false,
                                queueLimit: 1,
                                headers: {
                                    Authorization: OAuthToken.getAuthorizationHeader()
                                }
                            });


                            $scope.confirm = function (item) {
                                $scope.spinner=false;
                                $scope.import=false;
                                $rootScope.progressbar_start();
                                Uploader.onBeforeUploadItem = function(item) {

                                    if($scope.sms.same_msg == 0){
                                        $scope.sms.sms_messege = '';
                                    }
                                    item.formData = [$scope.sms];
                                };
                                Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                                    $rootScope.progressbar_complete();
                                    $scope.uploader.destroy();
                                    $scope.uploader.queue=[];
                                    $scope.uploader.clearQueue();
                                    angular.element("input[type='file']").val(null);

                                    if(response.status=='error' || response.status=='failed')
                                    {
                                        $rootScope.toastrMessages('error',response.msg);
                                        $scope.upload=false;
                                        angular.element("input[type='file']").val(null);
                                    }
                                    else if(response.status=='success')
                                    {
                                        if(response.download_token){
                                            var file_type='xlsx';
                                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                        }
                                        $scope.spinner=false;
                                        $scope.upload=false;
                                        angular.element("input[type='file']").val(null);
                                        $scope.import=false;
                                        $rootScope.toastrMessages('success',response.msg);
                                        $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                        $modalInstance.close();
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                        $scope.upload=false;
                                        angular.element("input[type='file']").val(null);
                                        $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                    }

                                };
                                Uploader.uploadAll();
                            };
                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };
                            $scope.remove=function(){
                                angular.element("input[type='file']").val(null);
                                $scope.uploader.clearQueue();
                                angular.forEach(
                                    angular.element("input[type='file']"),
                                    function(inputElem) {
                                        angular.element(inputElem).val(null);
                                    });
                            };

                        }});

                }
            };

            $scope.exportCustom = function(id){
                $rootScope.progressbar_start();
            // , person_id:id
                paymentsService.exportCustom({payment_id:$stateParams.payment_id},function (response) {
                    $rootScope.progressbar_complete();
                    window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token;
                });
            }

            PaymentsRecipient();

            Entity.list({entity:'banks'},function (response) {
                $scope.banks = response;
            });

            $scope.ExportToWord=function(id){
                $rootScope.progressbar_start();
                $rootScope.clearToastr();
                angular.element('.btn').addClass("disabled");
                $http({
                    url: "/api/v1.0/sponsorship/paymentsService/exportCheque",
                    method: "POST",
                    data: {payments_id:$stateParams.payment_id, person_id:id}
                }).then(function (response) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                    var file_type='zip';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }, function(error) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled");
                    $rootScope.toastrMessages('error',error.data.msg);
                });
            };

            $scope.pdf=function(id,index){
                $rootScope.clearToastr();
                var $params={ payments_id:$stateParams.payment_id, person_id:id};
                $rootScope.progressbar_start();
                angular.element('.btn').addClass("disabled");
                $http({
                    url: "/api/v1.0/sponsorship/paymentsService/pdf",
                    method: "POST",
                    data: $params
                }).then(function (response) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                    var file_type='pdf';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }, function(error) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled");
                    $rootScope.toastrMessages('error',error.data.msg);
                });
            };

            $scope.Export=function(action){
                $rootScope.clearToastr();
                var file_type='';
                var url='';
                var params={};
                var url ='';

                if(action =='excel'){
                    file_type='xlsx';
                    url ='/api/v1.0/sponsorship/paymentsService/exportCaseList';
                    params={ExportTo:action,payments_id:$stateParams.payment_id, action:'signature_recipient'};
                }else if(action =='pdf'){
                    file_type='pdf';
                    url ="/api/v1.0/sponsorship/paymentsService/pdf";
                    params={ payments_id:$stateParams.payment_id};
                }else{
                    file_type='zip';
                    url ='/api/v1.0/sponsorship/paymentsService/exportCaseList';
                    params={ExportTo:action,payments_id:$stateParams.payment_id, action:'signature_recipient'}
                }

                angular.element('.btn').addClass("disabled");
                $rootScope.progressbar_start();
                $http({
                    url: url,
                    method: "POST",
                    data: params
                }).then(function (response) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }, function(error) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled");
                    $rootScope.toastrMessages('error',error.data.msg);
                });
            };

            $scope.ExportCases=function(){
                $rootScope.clearToastr();

                angular.element('.btn').addClass("disabled");
                $rootScope.progressbar_start();
                $http({method: "GET",url: '/api/v1.0/sponsorship/paymentsService/exportCaseRecipient/'+$stateParams.payment_id}).
                then(function (response) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                    window. location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token;
                }, function(error) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled");
                    $rootScope.toastrMessages('error',error.data.msg);
                });
            };

            $scope.saveAccount=function(item){
                $rootScope.clearToastr();
                if( $rootScope.model_error_status == true){
                    $rootScope.model_error_status =false;
                }
                if( $rootScope.success == true){
                    $rootScope.success =false;
                }
                if( $rootScope.failed == true){
                    $rootScope.failed =false;
                }
                if( $rootScope.error == true){
                    $rootScope.error =false;
                }

                if(!( item.bank_id == null || item.bank_id == "" || item.cheque_account_number == null|| item.cheque_account_number == "" )){
                    angular.element('.btn').addClass("disabled");
                    var param={'person_id':item.person_id,'bank_id':item.bank_id,'account_number':item.cheque_account_number};
                    $rootScope.progressbar_start();
                    paymentsService.saveAccount(param,function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        if(response.status == true){
                            $rootScope.toastrMessages('success',$filter('translate')('action success'))
                        }else{
                            $rootScope.toastrMessages('error',$filter('translate')('The account is not approved for the recipient'))
                        }

                    });
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('Please check the bank or recipient account number'))
                }


            };

            $scope.confirmUpdate=function(){
                $rootScope.clearToastr();

                angular.element('.btn').addClass("disabled");
                $rootScope.progressbar_start();
                paymentsService.updatePaymentsRecipient({id:$stateParams.payment_id, items:$scope.recipients},function (response) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                    if(response.status==true){
                        $scope.status1 ='';
                        $scope.msg1 =[];
                        $rootScope.toastrMessages('success', $filter('translate')('action success'));
                        PaymentsRecipient();
                    } else {
                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                        $scope.status1 ='failed_valid';
                        $scope.msg1 =response.msg;
                    }
                }, function(error) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled");
                    $rootScope.toastrMessages('error',error.data.msg);
                });

            };

            $rootScope.close=function(){
                $rootScope.success =false;
                $rootScope.sub_success =false;
                $rootScope.failed =false;
                $rootScope.sub_error =false;
                $rootScope.error =false;
                $rootScope.model_error_status =false;
            };

            $scope.previous=function(){
                angular.element('#Step3').removeClass("disabled");
                $state.go('payment-form.payment-cases',{action:$stateParams.action,payment_id:$stateParams.payment_id});

            };

            $scope.next = function () {
                angular.element('#Step5').removeClass("disabled");
                $state.go('payment-form.payment-documents',{action:$stateParams.action,payment_id:$stateParams.payment_id});
            };

            $scope.pageChanged = function (currentPage) {
                $scope.CurrentPage=currentPage;
                PaymentsRecipient();
            };

            $scope.allCheckbox=function(value){
                $scope.selectedAll = value == true ? true: false;
                angular.forEach($scope.recipients, function(v, k) {
                    v.checked=$scope.selectedAll;
                });
            };


            $scope.printChk = function (item) {
                var url = $state.href('chequePrint', {id: item.id});
                window.open(url, '_blank');
            };

            $scope.distribute=function(action,param){
                $rootScope.clearToastr();

                var params = {'id':$stateParams.payment_id,action:action};

                if(action =='bank'){
                    if(!angular.isUndefined(param.bank_id)) {
                        if(param.bank_id != '' && param.bank_id != null) {
                            params.bank_id=param.bank_id;
                        }
                    }
                }
                else if(action =='cheque_number'){
                    if(!angular.isUndefined(param.cheque_number)) {
                        if(param.cheque_number != '' && param.cheque_number != null) {
                            params.cheque_account_number=parseInt(param.cheque_number);
                        }
                    }
                }
                else if(action =='cheque_date'){
                    if( !angular.isUndefined(param.cheque_date)) {
                        params.cheque_date= $filter('date')(param.cheque_date, 'dd-MM-yyyy');
                    }
                }

                $rootScope.progressbar_start();
                angular.element('.btn').addClass("disabled");
                paymentsService.saveRecipient(params,function (response) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                    if(response.status=='success')
                    {
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        PaymentsRecipient();
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                }, function(error) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled");
                    $rootScope.toastrMessages('error',error.data.msg);
                });

            };

            $scope.updateChequeNumber = function (startValue, index) {
                $rootScope.clearToastr();

                if(!(startValue == "" || startValue == null)){
                    $ngBootbox.confirm($filter('translate')('Would you like to edit your cheque number here?'))
                        .then(function() {
                            $rootScope.close();
                            var offSet = (($scope.CurrentPage -1) * $scope.ItemsPerPage ) + ( index );
                            var params = {'id':$stateParams.payment_id,value:startValue,offSet:offSet};
                            angular.element('.btn').addClass("disabled");
                            $rootScope.progressbar_start();
                            paymentsService.updateChequeNumber(params,function (response) {
                                $rootScope.progressbar_complete();
                                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                                if(response.status=='success')
                                {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    PaymentsRecipient();
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                angular.element('.btn').removeClass("disabled");
                                $rootScope.toastrMessages('error',error.data.msg);
                            });
                        });
                }
            };

            $scope.updateStatus=function(status,index,item){
                $rootScope.clearToastr();

                if(( status == 1 && !(  ( item.bank_id == "" || item.cheque_account_number == null|| item.cheque_account_number == "") && $rootScope.exchange_type ==1 ) ||
                    ((item.bank_id == "" || item.cheque_account_number == null|| item.cheque_account_number == ""||
                        item.cheque_date == null|| item.cheque_date == "") && $rootScope.exchange_type ==2 ) ||
                    ((item.cheque_account_number == null|| item.cheque_account_number == ""||
                        item.cheque_date == null|| item.cheque_date == "") && $rootScope.exchange_type ==3)) ||
                    ( status == 0 && !(( item.bank_id == "" || item.cheque_account_number == null|| item.cheque_account_number == "") && $rootScope.exchange_type ==1 ) ||
                        ((item.bank_id == "" || item.cheque_account_number == null|| item.cheque_account_number == ""||
                            item.cheque_date == null|| item.cheque_date == "") && $rootScope.exchange_type ==2 ) ||
                        ((item.cheque_account_number == null|| item.cheque_account_number == ""||
                            item.cheque_date == null|| item.cheque_date == "") && $rootScope.exchange_type ==3 ))
                ){
                    $rootScope.close();
                    var params = {'id':$stateParams.payment_id,action:'sub' ,value:status,selectedStatus:$scope.status,
                               recipients:[item.id]};
                    angular.element('.btn').addClass("disabled");
                    $rootScope.progressbar_start();
                    paymentsService.updateRecipientStatus(params,function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        if(response.status=='success')
                        {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            $scope.recipients[index].status=status;
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled");
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                }


            };

            $scope.set=function(value,action){
                $rootScope.clearToastr();

                var params = {'id':$stateParams.payment_id,action:action ,value:value,selectedStatus:$scope.status};
                var pass=true;
                var notAllow=0;
                var checked=0;

                if(action =='sub'){
                    var recipients=[];
                    angular.forEach($scope.recipients, function(v, k) {
                        if(v.checked){
                            recipients.push(v.id);
                            checked++;
                        }
                    });

                    if(recipients.length==0){
                        pass=false;
                        $rootScope.toastrMessages('error',$filter('translate')('You have not selected any beneficiary to change status'));
                    }else{
                        params.recipients=recipients;
                    }
                }

                if(pass == true){
                    $rootScope.close();
                    angular.element('.btn').addClass("disabled");
                    $rootScope.progressbar_start();
                    paymentsService.updateRecipientStatus(params,function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        if(response.status=='success')
                        {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            PaymentsRecipient();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled");
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                }
            };

            $scope.list=function(person){
                $rootScope.close();
                $uibModal.open({
                    templateUrl: '/app/modules/sponsorship/views/payment/form/model/document_list.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {
                        $scope.Documents =[];

                        var LoadDocuments=function(){
                            $rootScope.progressbar_start();
                            paymentsService.getPaymentDocuments({'id':$stateParams.payment_id,'person_id':person},function(response){
                                $rootScope.progressbar_complete();
                                if(response.data.length != 0){
                                    $scope.Documents =response.data;
                                }else{
                                    $scope.Documents =[];
                                }
                            });
                        };

                        LoadDocuments();

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                        $scope.ChooseAttachment=function(){
                            $rootScope.clearToastr();
                            $rootScope.close();

                            $uibModal.open({
                                templateUrl: '/app/modules/sponsorship/views/payment/form/model/person_upload_documents.html',
                                backdrop  : 'static',
                                keyboard  : false,
                                windowClass: 'modal',
                                size: 'md',
                                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                                    var Uploader = $scope.uploader = new FileUploader({
                                        url: '/api/v1.0/doc/files',
                                        headers: {
                                            Authorization: OAuthToken.getAuthorizationHeader()
                                        }
                                    });
                                    Uploader.onBeforeUploadItem = function(item) {
                                        $rootScope.clearToastr();
                                        item.formData = [{action:"not_check"}];
                                    };
                                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                                        $scope.uploader.destroy();
                                        $scope.uploader.queue=[];
                                        $scope.uploader.clearQueue();
                                        angular.element("input[type='file']").val(null);
                                        fileItem.id = response.id;
                                        var count=0;
                                        $rootScope.progressbar_start();
                                        paymentsService.storePaymentDocument({person_id:person,payment_id:$rootScope.payment_id, document_id:fileItem.id}, function (response) {
                                            $rootScope.progressbar_complete();
                                            if(response.status== 'success'){
                                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                                    LoadDocuments();
                                                    $modalInstance.close();
                                                }else{
                                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                                }
                                            }, function(error) {
                                            $rootScope.progressbar_complete();
                                            $rootScope.toastrMessages('error',error.data.msg);
                                            });

                                    };

                                    $scope.cancel = function () {
                                        LoadDocuments();
                                        $modalInstance.dismiss('cancel');
                                    };

                                }
                            });

                        };
                        $scope.delete=function(size,id) {
                            $rootScope.clearToastr();
                            $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                                .then(function() {
                                    $rootScope.progressbar_start();
                                    paymentsService.deleteDocument({id:id,payment_id:$rootScope.payment_id,person_id:person},function (response) {
                                        $rootScope.progressbar_complete();
                                        if(response.status=='error' || response.status=='failed') {
                                            $rootScope.toastrMessages('error',response.msg);
                                        }
                                        else if(response.status== 'success'){
                                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                            LoadDocuments();
                                        }
                                    }, function(error) {
                                        $rootScope.progressbar_complete();
                                        $rootScope.toastrMessages('error',error.data.msg);
                                    });
                                });




                        };
                    }
                });
            };

            $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
            $scope.formats = [ 'yyyy-MM-dd','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];
            $scope.today = function() {$scope.dt = new Date();};
            $scope.today();
            $scope.clear = function() {$scope.dt = null;};
            $scope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup1.opened = true;};
            $scope.popup1 = {opened: false};

        }

        $rootScope.setStep($state.current.data.stepNumber);
    });

