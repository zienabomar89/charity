

angular.module('SponsorshipModule')
    .controller('PaymentDocumentController', function ($scope,$rootScope, $state,$stateParams ,paymentsService,$uibModal,OAuthToken,FileUploader,$ngBootbox,$filter) {

        if($stateParams.payment_id==null){
            $state.go('payment-form.payment-details',{action:'new'});
        }else {

            $rootScope.action ='';
            $rootScope.status ='';
            $rootScope.msg ='';
            $rootScope.msg2 ='';
            $rootScope.Documents =[];
            $rootScope.sponsorship_cases = [];
            $rootScope.payment_id=$stateParams.payment_id;
            $rootScope.action=$stateParams.action;
            $scope.action=$stateParams.action;

            $rootScope.signature_sheet =false;
            $rootScope.organization_sheet =false;
            $rootScope.receipt_of_receipt =false;

            var LoadDocuments=function(){
                $rootScope.clearToastr();
                $rootScope.progressbar_start();
                paymentsService.getPaymentDocuments({'id':$stateParams.payment_id,person_id:-1},function(response){
                    $rootScope.progressbar_complete();
                    $rootScope.Documents =response.data;
                    $rootScope.signature_sheet =false;
                    $rootScope.organization_sheet =false;
                    $rootScope.receipt_of_receipt =false;
                    angular.forEach($rootScope.Documents, function(v,k) {
                        if( v.attachment_type == '1' ||  v.attachment_type == 1){
                            $rootScope.signature_sheet =true;
                        }
                        if( v.attachment_type == '2' ||  v.attachment_type == 2){
                            $rootScope.organization_sheet =true;
                        }
                        if( v.attachment_type == '3' ||  v.attachment_type == 3){
                            $rootScope.receipt_of_receipt =true;
                        }
                    });
                    $rootScope.is_mine= response.is_mine;
                });
            }

            if($stateParams.action=='edit'){
                LoadDocuments();
            }

        }

        $scope.download=function(target){
            $rootScope.download('GET','/doc/files/download/'+target,'null');
        };

        $scope.ChooseAttachment=function(){
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: '/app/modules/sponsorship/views/payment/form/model/upload_documents.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader,paymentsService) {

                    $scope.attachment_type = '4';
                    $scope.enable_upload = true;

                    $scope.checkAttachmentType = function(type){

                        if(type != 4){
                            angular.forEach($rootScope.Documents, function(v,k) {
                                if( type == v.attachment_type){
                                    $scope.enable_upload = false;
                                    $rootScope.toastrMessages('error',$filter('translate')('There is a file uploaded for the same attachment type, please delete it and then upload again'));
                                    return
                                }
                            });
                        }
                    }

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/doc/files',
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });
                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                        item.formData = [{action:"not_check"}];
                    };
                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);

                        fileItem.id = response.id;
                            var count=0;
                            paymentsService.storePaymentDocument({attachment_type:$scope.attachment_type,
                                payment_id:$rootScope.payment_id,
                                document_id:fileItem.id},function (response) {
                                if(response.status== 'success'){
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    LoadDocuments();
                                    $modalInstance.close();
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);

                                }

                            }, function(error) {
                                $rootScope.toastrMessages('error',error.data.msg);
                            });
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.closeUploader = function () {
                        $modalInstance.dismiss('cancel');
                        LoadDocuments();

                    };

                },
                resolve: {

                }
            });

        };
        $rootScope.getDocument=function(){
            LoadDocuments();
        };

        $rootScope.len=function(arr){
            if(arr.length != 0){
                $rootScope.success =true;
                $rootScope.action ='add';
                $rootScope.getDocument();
             }else{
                $rootScope.model_error_status =true;
                $rootScope.action ='add';
            }
        };

        $scope.deletedoc=function(size,id) {
            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                .then(function() {
                    $rootScope.progressbar_start();
                    paymentsService.deleteDocument({id:id,payment_id:$rootScope.payment_id},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='error' || response.status=='failed') {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status== 'success'){
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    LoadDocuments();
                                }
                            }, function(error) {
                                $rootScope.toastrMessages('error',error.data.msg);
                            });
                    });


        };
        $scope.previous=function(){
            $state.go('payment-form.payment-receipt',{action:$stateParams.action,payment_id:$stateParams.payment_id});
        };

        $scope.next=function(){
            $state.go('payments-index');
        };

        $rootScope.setStep($state.current.data.stepNumber);
    });
