 angular.module('SponsorshipModule')
            .controller('AddPaymentController', function ($filter,$scope,$rootScope,$state,$stateParams,nominate,paymentsService,setting,Entity,Org) {

                if($stateParams.action==null){
                    $stateParams.action='new';
                }

                $rootScope.action=$stateParams.action;
                $scope.action=$stateParams.action;
                $rootScope.is_mine = true;
                $scope.payment={};
                $rootScope.totalamount = 0;
                $rootScope.totalamountshekel = 0;
                $rootScope.totalAfterDiscountfee = 0;
                $rootScope.totalAfterDiscountfeeshekel = 0;
                $rootScope.organizationShare = 0;
                $rootScope.administrationFees = 0;
                $rootScope.organizationSharesk = 0;
                $rootScope.administrationFeessk = 0;

                $scope.msg1=[];
                $rootScope.passed=false;
                
                var round = function(number, precision) {
                    var factor = Math.pow(10, precision);
                    var tempNumber = number * factor;
                    var roundedTempNumber = Math.round(tempNumber);
                    return roundedTempNumber / factor;
                };

                function calcPaymentSummary() {
                    var share = isNaN($scope.payment.organization_share)? 0 : $scope.payment.organization_share;
                    var fees = isNaN($scope.payment.administration_fees)? 0 : $scope.payment.administration_fees;
                    var exchange_rate = isNaN($scope.payment.exchange_rate)? 0 : $scope.payment.exchange_rate;

                    $rootScope.totalamount = $scope.payment.amount;
                    $rootScope.organizationShare_ = share;
                    $rootScope.administrationFees_ = fees;

                    $rootScope.totalAfterDiscountfee = ($scope.payment.amount - ($scope.payment.amount * share)) - fees;

                    $rootScope.organizationShare = share * $rootScope.totalamount;
                    $rootScope.administrationFees = fees * $rootScope.totalamount;

                    $rootScope.totalamountshekel = round($rootScope.totalamount * exchange_rate, 3);
                    $rootScope.organizationSharesk = round(share * $rootScope.totalamountshekel, 3);
                    $rootScope.administrationFeessk = round(fees * $rootScope.totalamountshekel, 3);

                    $rootScope.totalAfterDiscountfeeshekel = round($rootScope.totalAfterDiscountfee * exchange_rate, 2);
                }

                $scope.setExchangeRate=function(rate){
                    if( !angular.isUndefined($scope.msg1.exchange_rate)) {
                        $scope.msg1.exchange_rate = [];
                    }
                    return calcPaymentSummary();
                };
                $scope.setAmount=function(amount){
                    if( !angular.isUndefined($scope.msg1.amount)) {
                        $scope.msg1.amount = [];
                    }
                    return calcPaymentSummary();
                };

                $scope.setOrgShare=function(value){
                    if( !angular.isUndefined($scope.msg1.organization_share)) {
                        $scope.msg1.organization_share = [];
                    }
                    return calcPaymentSummary();
                };

                $scope.setFee=function(value){
                    if( !angular.isUndefined($scope.msg1.administration_fees)) {
                        $scope.msg1.administration_fees = [];
                    }
                    return calcPaymentSummary();
                };

                Org.list({type:'sponsors'},function (response) { $rootScope.sponsorships = response; });
                Entity.get({entity:'entities',c:'currencies,banks,paymentCategory,transferCompany'},function (response) {
                    $scope.currency = response.currencies;
                    $scope.Banks = response.banks;
                    $scope.paymentCategory = response.paymentCategory;
                    $rootScope.transferCompany = response.transferCompany;
                });


                if(!($stateParams.action == "new" && $stateParams.payment_id == null)){
                    if ($stateParams.payment_id != null){
                        paymentsService.get({'id': $stateParams.payment_id}, function (response) {
                            if (response) {
                                $scope.payment = response;
                                $rootScope.is_mine= $scope.payment.is_mine;
                                $scope.currency_name =$scope.payment.name;

                                if ($scope.payment.sponsorship_id != null) {
                                    $scope.payment.sponsorship_id = $scope.payment.sponsorship_id + "";
                                } else {
                                    $scope.payment.sponsorship_id = "";
                                }
                           if ($scope.payment.category_id != null) {
                                    $scope.payment.category_id = $scope.payment.category_id + "";
                                } else {
                                    $scope.payment.category_id = "";
                                }

                                if ($scope.payment.exchange_type != null) {
                                    $scope.payment.exchange_type = $scope.payment.exchange_type + "";

                                    if ($scope.payment.bank_id != null) {
                                        $scope.payment.bank_id = $scope.payment.bank_id + "";
                                    } else {
                                        $scope.payment.bank_id = "";
                                    }

                                } else {
                                    $scope.payment.exchange_type = "";
                                    $scope.payment.bank_id = "";
                                }

                                if ($scope.payment.payment_target != null) {
                                    $scope.payment.payment_target = $scope.payment.payment_target + "";
                                } else {
                                    $scope.payment.payment_target = "";
                                }

                                if ($scope.payment.currency_id != null) {
                                    $scope.payment.currency_id = $scope.payment.currency_id + "";
                                } else {
                                    $scope.payment.currency_id = "";
                                }

                                if ($scope.payment.payment_exchange != null) {
                                    $scope.payment.payment_exchange = $scope.payment.payment_exchange + "";
                                } else {
                                    $scope.payment.payment_exchange = "";
                                }

                                if ($scope.payment.status != null) {
                                    $scope.payment.status = $scope.payment.status + "";
                                } else {
                                    $scope.payment.status = "";
                                }

                                $scope.payment.exchange_rate = parseFloat($scope.payment.exchange_rate, 10);
                                $scope.payment.organization_share = parseFloat($scope.payment.organization_share, 10);
                                $scope.payment.administration_fees = parseFloat($scope.payment.administration_fees, 10);
                                $rootScope.organization_share = $scope.payment.organization_share;
                                $rootScope.exchange_rate = $scope.payment.exchange_rate;

                                $rootScope.organizationShare_ = $scope.payment.organization_share;
                                $rootScope.administrationFees_ = $scope.payment.administration_fees;

                                $rootScope.amount = $scope.payment.amount;
                                $rootScope.total = $rootScope.amount;

                                $rootScope.totalamount = response.total_amount;
                                $rootScope.organizationShare = round($scope.payment.organization_share * $rootScope.totalamount, 2);
                                $rootScope.administrationFees = $scope.payment.administration_fees;
                                $rootScope.totalAfterDiscountfee = response.total_after_org_share_discount;
                                $rootScope.totalAfterDiscountPayments = response.total_after_fee_discount;

                                $rootScope.totalamountshekel = response.total_amount_in_shekel;
                                $rootScope.organizationSharesk = round($scope.payment.organization_share * $rootScope.totalamountshekel, 2);
                                $rootScope.administrationFeessk = round($rootScope.administrationFees * $rootScope.exchange_rate, 2);
                                $rootScope.totalAfterDiscountfeeshekel = response.total_after_org_share_discount_in_shekel;
                                $rootScope.totalAfterDiscountPaymentsshekel = response.total_after_fee_discount_in_shekel;

                                if ($scope.payment.sponsor_id != null) {
                                    $scope.payment.sponsor_id = $scope.payment.sponsor_id + "";
                                    nominate.list({'id': $scope.payment.sponsor_id}, function (response) {
                                        if (response.s != 0) {
                                            $rootScope.sponsorshipList = response.list;
                                        } else {
                                            $rootScope.sponsorshipList = [];
                                        }
                                    });

                                } else {
                                    $scope.payment.sponsor_id = "";
                                    $rootScope.sponsorshipList = [];
                                }
                                calcPaymentSummary();
                            }
                        });
                }
            }


                $scope.getsponsorshipsList = function(sponsor_id){

                    if( !angular.isUndefined($scope.msg1.sponsor_id)) {
                        $scope.msg1.sponsor_id = [];
                    }
                    nominate.list({'id':sponsor_id},function(response){
                        if(response.s!= 0){
                            $rootScope.sponsorshipList = response.list;
                        }else{
                            $rootScope.sponsorshipList = [];
                        }
                    });

                    $scope.payment.sponsorship_id="";

                };
                $scope.resetBank = function(){

                    if( !angular.isUndefined($scope.msg1.exchange_type)) {
                        $scope.msg1.exchange_type = [];
                    }
                   if( !angular.isUndefined($scope.payment.bank_id)) {
                       $scope.payment.bank_id="";
                   }

                };
                $scope.resetTransfer = function(value){

                  if( !angular.isUndefined($scope.msg1.transfer)) {
                        $scope.msg1.transfer = [];
                   }

                   if( !angular.isUndefined($scope.msg1.transfer_company_id)) {
                        $scope.msg1.transfer_company_id = [];
                   }
                    $scope.payment.transfer_company_id = null;

                };
                $scope.changeCurrency = function(changed,value){

                  if(changed ==1){
                      if( !angular.isUndefined($scope.payment.currency_id)) {
                          $scope.msg1.currency_id = [];
                      }
                  }
                };
                $scope.forward=function(params) {

                    $rootScope.clearToastr();

                    if($rootScope.action == "new" && $stateParams.payment_id != null) {
                        params.duplicated=params.id;
                        delete params.id;
                    }

                    if( !angular.isUndefined(params.sponsorship_id)) {
                        if( params.sponsorship_id == '' ||  params.sponsorship_id == ' ') {
                            params.sponsorship_id = null;
                        }
                    }

                    if( !angular.isUndefined(params.bank_id)) {
                        if( params.bank_id == '' ||  params.bank_id == ' ') {
                            params.bank_id = null;
                        }
                    }

                    if( !angular.isUndefined(params.exchange_date)) {
                        params.exchange_date=$filter('date')(params.exchange_date, 'dd-MM-yyyy')
                    }
                    if( !angular.isUndefined(params.payment_date)) {
                        params.payment_date= $filter('date')(params.payment_date, 'dd-MM-yyyy')
                    }

                    if($stateParams.action =='new'){
                        $rootScope.progressbar_start();
                        paymentsService.save(params,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed') {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid') {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                if(response.status=='success')
                                {
                                    $rootScope.model_error_status =false;
                                    angular.element('#Step2').removeClass("disabled");
                                    $state.go('payment-form.payment-import-person',{action:'new',payment_id:response.payment_id});
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }
                        });
                    }else{

                        if($rootScope.is_mine == false){
                            $state.go('payment-form.payment-import-person',{action:'edit',payment_id:params.id});
                        }else{
                            $rootScope.progressbar_start();
                            paymentsService.update(params,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed') {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid') {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    if(response.status=='success')
                                    {
                                        angular.element('#Step2').removeClass("disabled");
                                        $state.go('payment-form.payment-import-person',{action:'edit',payment_id:params.id});
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                }

                            });
                        }

                    }
                };


                $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 0
                };
                $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                $scope.format = $scope.formats[0];
                $scope.today = function() {$scope.dt = new Date();};
                $scope.today();
                $scope.clear = function() {$scope.dt = null};
                $scope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup1.opened = true;};
                $scope.open3 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup3.opened = true;};
                $scope.popup3 = {opened: false};
                $scope.popup1 = {opened: false};

                $rootScope.setStep($state.current.data.stepNumber);
});
