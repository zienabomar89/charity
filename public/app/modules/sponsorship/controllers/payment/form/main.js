angular.module('SponsorshipModule')
    .controller('BasicPaymentController', function ($scope,$rootScope,$state,$stateParams) {

        $rootScope.fetchPayment = function(target){
            if(target ==1){
                $rootScope.setStep(1);
                angular.element('#Step1').removeClass("disabled");
                  $state.go('payment-form.payment-details',{action:$stateParams.action,payment_id:$stateParams.payment_id});
            }else if(target ==2){
                $rootScope.setStep(2);
                angular.element('#Step2').removeClass("disabled");
                  $state.go('payment-form.payment-import-person',{action:$stateParams.action,payment_id:$stateParams.payment_id});
            }else if(target ==3){
                $rootScope.setStep(3);
                angular.element('#Step3').removeClass("disabled");
                  $state.go('payment-form.payment-cases',{action:$stateParams.action,payment_id:$stateParams.payment_id});
            }else if(target ==4){
                  $rootScope.setStep(4);
                  angular.element('#Step4').removeClass("disabled");
                  $state.go('payment-form.payment-receipt',{action:$stateParams.action,payment_id:$stateParams.payment_id});
            }else if(target ==5){
                  $rootScope.setStep(5);
                  angular.element('#Step5').removeClass("disabled");
                  $state.go('payment-form.payment-documents',{action:$stateParams.action,payment_id:$stateParams.payment_id});
            }
        };

        if($stateParams.payment_id){
            var sec=['#Step1','#Step2','#Step3','#Step4'];
            angular.forEach(sec, function(val,key)
            {
                angular.element(val).removeClass("disabled");
            });
            angular.element('.step').removeClass("disabled");
            $('.step').removeClass("disabled");

        }


        $rootScope.setStep = function (curStep) {

            var curStepName = '#Step'+curStep;
            $rootScope.stepNumber=curStep;

            var sec=['#Step1','#Step2','#Step3','#Step4','#Step5'];
            angular.forEach(sec, function(val,key)
            {
                if(val != curStepName){
                    $(val).css({"background-color":"white","border-bottom":"none"});
                }else{
                    $(curStepName).css({"background-color":"aliceblue","border-bottom":"2px solid #337ab7"});
                }
            });
            angular.element('.progress-bar').css('width', (curStep * 20)+'%');
        }

    });
