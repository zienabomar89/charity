angular.module('SponsorshipModule')
    .directive("ngUploadChange",function($parse){
        return{
            scope:{
                ngUploadChange:"&"
            },
            link:function($scope, $element, $attrs){
                $element.on("change",function(event){
                    $scope.ngUploadChange({$event: event})
                });
                $scope.$on("$destroy",function(){
                    $element.off();
                });
            }
        }
    })
    .service('ImportPayment', ["$http", "$rootScope","$filter", function ($http, $rootScope,$filter) {
        this.uploadFileToUrl = function(file,payment,action,row){

            $rootScope.status ='';
            $rootScope.msg ='';
            var fd = new FormData();

            angular.forEach(payment, function(val,key) {
                if(key == 'exchange_date' || key == 'payment_date'){
                    fd.append(key,$filter('date')(val, 'yyyy/MM/dd'));
                }else{
                    fd.append(key,val);
                }
             });

            fd.append('file', file);

            $rootScope.clearToastr();
            action = true;
            if(!action){
                $rootScope.items=[];
                $rootScope.progressbar_start();
                angular.element('.btn').addClass("disabled");
                $http.post('/importPaymentExcel', fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                })
                    .success(function(response){
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled");
                        if(response.status == 'failed'){
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        }else{
                            if(response.map.length != 0){
                                $rootScope.doimportExcel=true;
                                $rootScope.mapitems = true;

                                $rootScope.items = response.map;
                            }else{
                                $rootScope.mapitems = false;
                            }
                        }

                    })
                    .error(function(error){
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled");
                        $rootScope.toastrMessages('error',error.data.msg);
                    });

            }
            else{
                angular.forEach(row, function(val,key)
                {
                    angular.forEach(val, function(v,k)
                    {
                        fd.append('row['+key+ '][' + k +']', v);
                    });
                });


                angular.element('.btn').addClass("disabled");
                $rootScope.progressbar_start();
                $http.post('/doImportPaymentExcel', fd, {
                    transformRequest: angular.identity,
                    headers: {'Content-Type': undefined,'Process-Data': false}
                })
                    .success(function(response){
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled");
                        // $rootScope.exceed = response.exceed;
                        if(response.status==true){
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            $rootScope.statistic = response.statistic;
                            $rootScope.toggle=false;
                            $rootScope.items=[];
                            $rootScope.mapitems = false;
                            $rootScope.doimportExcel=false;

                            $rootScope.payment={'sponsorship_id':null,'category_id':null,
                                'sponsor_id':null,'organization_id':null,
                                'payment_date':null,'amount':null,'currency_id':null,
                                'transfer':null,'transfer_company_id':null,
                                'payment_exchange':null,'bank_id':null,
                                'payment_target':null,'exchange_rate':null,'exchange_date':null,'organization_share':null,
                                'administration_fees':null,'status':null,'exchange_type':null};
                        }else{
                            $rootScope.statistic=[];
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    })
                    .error(function(error){
                        $rootScope.progressbar_complete();
                        $rootScope.statistic=[];
                        angular.element('.btn').removeClass("disabled");
                        $rootScope.toastrMessages('error',error.data.msg);
                    });



            }


        }

    }])
    .controller('ImportPaymentsController', function ($http,$filter,$scope ,OAuthToken, FileUploader,$timeout,$rootScope,$state,$stateParams,paymentsService,$uibModal,ImportPayment,Org,Entity,nominate) {

        $rootScope.clearToastr();
        $state.current.data.pageTitle = $filter('translate')('payment form') +' : ' + $filter('translate')('import payments');

        $rootScope.payment={'sponsorship_id':null,'category_id':null,
                        'sponsor_id':null,'organization_id':null,
                        'payment_date':null,'amount':null,'currency_id':null,
                        'transfer':null,'transfer_company_id':null,
                        'payment_exchange':null,'bank_id':null,
                        'payment_target':null,'exchange_rate':null,'exchange_date':null,'organization_share':null,
                        'administration_fees':null,'status':null,'exchange_type':null};


        $rootScope.msg =' ';
        $scope.msg1=[];

            $rootScope.doimportExcel=false;
            $rootScope.mapitems = false;
            $rootScope.row = [];
            $rootScope.items = [];
            $rootScope.row1=[];
            $rootScope.msg1=[];
            $rootScope.toggle = false;
            $rootScope.beneficiaries=[];
            $rootScope.new_=0;
            $rootScope.old_=0;
            $rootScope.no_payment=0;
            $rootScope.count=0;

        Org.list({type:'sponsors'},function (response) { $rootScope.Sponsor = response; });

        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        Entity.get({entity:'entities',c:'currencies,banks,paymentCategory,transferCompany'},function (response) {
            $scope.currency = response.currencies;
            $rootScope.transferCompany = response.transferCompany;
            $scope.Banks = response.banks;
            $scope.paymentCategory = response.paymentCategory;
        });

        $scope.getSponsorshipsList = function(sponsor_id){

            if( !angular.isUndefined($scope.msg1.sponsor_id)) {
                $scope.msg1.sponsor_id = [];
            }
            nominate.list({'id':sponsor_id},function(response){
                if(response.s!= 0){
                    $rootScope.sponsorshipList = response.list;
                }else{
                    $rootScope.sponsorshipList = [];
                }
            });

            $rootScope.payment.sponsorship_id="";

        };
        $scope.resetBank = function(){

            if( !angular.isUndefined($scope.msg1.exchange_type)) {
                $scope.msg1.exchange_type = [];
            }
            if( !angular.isUndefined($rootScope.payment.bank_id)) {
                $rootScope.payment.bank_id="";
            }

        };
        $scope.changeCurrency = function(changed,value){

            if(changed ==1){
                if( !angular.isUndefined($rootScope.payment.currency_id)) {
                    $scope.msg1.currency_id = [];
                }
            }
        };

        $scope.download=function(){
            angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();

            $rootScope.clearToastr();
            $http({
                url: "/api/v1.0/sponsorship/paymentsService/importPaymentTemplate",
                method: "GET"
            }).then(function (response) {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token+'&template=true';
            }, function(error) {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };
        $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {$scope.dt = new Date();};
        $scope.today();
        $scope.clear = function() {$scope.dt = null;};
        $scope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup1.opened = true;};
        $scope.open3 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup3.opened = true;};
        $scope.popup3 = {opened: false};
        $scope.popup1 = {opened: false};


        /// ******************************* ///
        var Uploader = $scope.uploader = new FileUploader({
            url: '/api/v1.0/sponsorship/paymentsService/ImportPaymentExcel',
            headers: {
                Authorization: OAuthToken.getAuthorizationHeader()
            }
        });
        $scope.fileUpload=function(item){
            $timeout(function() {
                Uploader.onBeforeUploadItem = function(item) {
                    $rootScope.clearToastr();

                    var params = angular.copy($rootScope.payment);

                    if( !angular.isUndefined(params.exchange_date)) {
                        params.exchange_date=$filter('date')(params.exchange_date, 'dd-MM-yyyy')
                    }
                    if( !angular.isUndefined(params.payment_date)) {
                        params.payment_date= $filter('date')(params.payment_date, 'dd-MM-yyyy')
                    }
                    item.formData = [params];

                };
                $rootScope.progressbar_start();
                Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                    $rootScope.progressbar_complete();
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.uploader.clearQueue();
                    angular.element("input[type='file']").val(null);
                    angular.element('.btn').removeClass("disabled");
                    // $rootScope.exceed = response.exceed;
                    if(response.status==true){
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        $rootScope.statistic = response.statistic;
                        $rootScope.toggle=false;
                        $rootScope.items=[];
                        $rootScope.mapitems = false;
                        $rootScope.doimportExcel=false;

                        $rootScope.payment={'sponsorship_id':null,'category_id':null,
                            'sponsor_id':null,'organization_id':null,
                            'payment_date':null,'amount':null,'currency_id':null,
                            'transfer':null,'transfer_company_id':null,
                            'payment_exchange':null,'bank_id':null,
                            'payment_target':null,'exchange_rate':null,'exchange_date':null,'organization_share':null,
                            'administration_fees':null,'status':null,'exchange_type':null};
                    }else{
                        $rootScope.statistic=[];
                        $rootScope.toastrMessages('error',response.msg);
                    }

                };
                Uploader.uploadAll();
            }, 5);
        };

        /// ******************************* ///
        $scope.setBeneficiary=function(){
            $rootScope.clearToastr();
            $rootScope.toggle = $rootScope.toggle === false ? true: false;
            $rootScope.items=[];
            $rootScope.mapitems = false;
            $rootScope.doimportExcel=false;
            angular.forEach(angular.element("input[type='file']"), function(inputElem) {
                angular.element(inputElem).val(null);
            });
        };
        $scope.fileChanged = function($event){
            $scope.myFile=$event.target.files[0];
            $rootScope.doimportExcel=false;
            $rootScope.mapitems = false;
            $rootScope.mapitems = false;
            $rootScope.items = [];
            $rootScope.clearToastr();
        };
        $scope.import = function(){
            $rootScope.clearToastr();
            if( !angular.isUndefined($scope.myFile)) {
                var count = 0;

                if($rootScope.doimportExcel == true){
                    angular.forEach($rootScope.items, function(v,k) {
                        if(!v.column){
                            count++;
                        }
                    });

                    if(count == $rootScope.items.length){
                        $rootScope.toastrMessages('error',$filter('translate')('not specify the database fields corresponding to the column names in the file you selected'));
                    }else{
                        ImportPayment.uploadFileToUrl($scope.myFile,$rootScope.payment,$rootScope.doimportExcel,$rootScope.items);
                    }
                }
                else{
                    $rootScope.mapitems = false;
                    $rootScope.items = [];
                    ImportPayment.uploadFileToUrl($scope.myFile,$rootScope.payment,$rootScope.doimportExcel,$rootScope.items);
                }
            }else{
                $rootScope.toastrMessages('error',$filter('translate')('there is no selected file to import'));
            }
        };

        $scope.download=function(){
            $rootScope.clearToastr();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-payment-template&template=true';
        };
    });
