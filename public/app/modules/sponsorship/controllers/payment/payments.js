angular.module('SponsorshipModule')
    .controller('AllPaymentsController', function ($http,$filter,$scope,$rootScope,$state,$ngBootbox,$uibModal,$stateParams,nominate,paymentsService,Org,Entity,OAuthToken, FileUploader,setting) {

        $state.current.data.pageTitle = $filter('translate')('payments list');
        $rootScope.settings.layout.pageSidebarClosed = true;
        $rootScope.CurrentPage = 1;
        $scope.Sponsorpayment=true;
        $rootScope.model_error_status =false;
        $rootScope.has_cheque_template=false;
        $scope.custom = true;
        $scope.master=false;
        $scope.sponsor=true;
        $scope.status ='';
        $rootScope.msg ='';
        $rootScope.payment={};
        $scope.items=[];
        $scope.sortKeyArr=[];
        $scope.sortKeyArr_rev=[];
        $scope.sortKeyArr_un_rev=[];
        $scope.itemsCount = 50;



        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });


        var resetSearchFilter =function(){
            $rootScope.payment={
                'page':1,
                "all_organization":false,
                'organization_id':"",
                'sponsor_id':"",
                'sponsorship_id':"",
                'category_id':"",
                'max_amount':"",
                'min_amount':"",
                'currency_id':"",
                'status':"",
                'payment_status':"",
                'start_exchange_date':"",
                'end_exchange_date':"",
                'start_payment_date':"",
                'end_payment_date':"",
                'min_beneficiary_no':"",
                'max_beneficiary_no':"",
                'itemsCount':50
            };

            if($stateParams.id){
                $rootScope.payment.sponsor_id=$stateParams.id;
                $scope.sponsor=true;
            }else{
                $scope.sponsor=false;
            }

        };
        var LoadPayments =function(params){
            params.sortKeyArr_rev = $scope.sortKeyArr_rev;
            params.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;
            // $rootScope.clearToastr();
            // $scope.items = [];
            $rootScope.progressbar_start();
            paymentsService.all(params,function (response) {
                $rootScope.progressbar_complete();
                if( !angular.isUndefined(params.all_organization)) {
                    if(params.all_organization == true ||params.all_organization == 'true'){
                        $scope.master=true
                    }else if(params.all_organization == false ||params.all_organization == 'false') {
                        $scope.master=false
                    }
                }
                // $scope.master=response.master;
                $scope.items= response.Payments.data;
                $scope.CurrentPage = response.Payments.current_page;
                $scope.TotalItems = response.Payments.total;
                $scope.ItemsPerPage = response.Payments.per_page;
                $scope.TotalPayments=response.total;

            });

        };
        var RestTemplate= function () {
            setting.getSettingById({'id':'default-cheque-template'},function (response) {
                $rootScope.has_cheque_template=response.status;
                if(response.status == true){
                    $rootScope.has_cheque_template=response.Setting.value +"";
                }
            });
        };
        var resetPaymentsTable = function () {
            $rootScope.payment.action ='filter';
            $rootScope.payment.page =$rootScope.CurrentPage;
            $rootScope.payment.itemsCount =$scope.itemsCount;
            if($stateParams.id){
                $rootScope.payment.sponsor_id=$stateParams.id;
            }

            LoadPayments( $rootScope.payment);
        };

        resetSearchFilter();
        RestTemplate();
        resetPaymentsTable();

        Entity.get({entity:'entities',c:'currencies,banks,paymentCategory,transferCompany'},function (response) {
            $scope.currency = response.currencies;
            $scope.Banks = response.banks;
            $scope.paymentCategory = response.paymentCategory;
            $rootScope.transferCompany = response.transferCompany;
        });
        Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

        if($stateParams.id){
            Org.get({type:'sponsors', id: $stateParams.id}, function(response) {
                $scope.sponsor_name=response.name;
            });
            $scope.sponsor=true;
        }
        else{
            $scope.sponsor=false;
            Org.list({type:'sponsors'},function (response) { $scope.Sponsors = response; });
            nominate.list({'id':-1},function(response){
                if(response.s!= 0){
                    $scope.sponsorshipList = response.list;
                }else{
                    $scope.sponsorshipList = [];
                }
            });
        }

        $scope.getsponsorshipsList = function(sponsor_id){
            if(sponsor_id!=""){
                nominate.list({'id':sponsor_id},function(response){
                    if(response.s!= 0){
                        $scope.sponsorshipList = response.list;
                    }else{
                        $scope.sponsorshipList = [];
                    }
                });
            }else{
                $scope.sponsorshipList = [];
            }
        };
        $scope.pageChanged = function (currentPage) {
            $rootScope.CurrentPage = currentPage;
            resetPaymentsTable();
        };

        $scope.itemsPerPage_ = function (itemsCount) {
            $scope.itemsCount = itemsCount;
            $rootScope.CurrentPage = 1;
            resetPaymentsTable();
        };       


        $scope.toggleCustom = function() {
            $scope.custom = $scope.custom === false ? true: false;
            if($scope.custom == true) {

            }else{
                resetSearchFilter();
            }
        };

        $scope.sort_ = function(keyname){
            $scope.sortKey_ = keyname;
            var reverse_ = false;

            if (
                 ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
                 ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
                ) {
                reverse_ = true;
            }

            var data = angular.copy($rootScope.payment);
            data.action ='filter';

            if (reverse_) {
                if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                    $scope.sortKeyArr_rev=[];
                    $scope.sortKeyArr_un_rev=[];
                    $scope.sortKeyArr_rev.push(keyname);
                }

                // angular.forEach($scope.sortKeyArr_un_rev, function(val,key) {
                //     if(val == keyname){
                //         $scope.sortKeyArr_un_rev.splice(key,1);
                //         return ;
                //     }
                // });

            }
            else {
                if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                    $scope.sortKeyArr_rev=[];
                    $scope.sortKeyArr_un_rev=[];
                    $scope.sortKeyArr_un_rev.push(keyname);
                }
                // angular.forEach($scope.sortKeyArr_rev, function(val,key) {
                //     if(val == keyname){
                //         $scope.sortKeyArr_rev.splice(key,1);
                //         return ;
                //     }
                // });
            }

            data.sortKeyArr_rev = $scope.sortKeyArr_rev;
            data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;

            $scope.Search(data,'filter');

        };


        $scope.resetTransfer = function(value){

            $scope.payment.transfer_company_id = null;
        };

        $scope.Search=function(params,action) {
            $rootScope.clearToastr();

            var data = angular.copy(params);
            data.action = action;
            data.organization_ids = [];
            if( !angular.isUndefined(data.all_organization)) {
                if(data.all_organization == true ||data.all_organization == 'true'){
                    angular.forEach(data.organization_id, function(v, k) {
                        data.organization_ids.push(v.id);
                    });
                    delete data.organization_id;
                    $scope.master=true
                }else if(data.all_organization == false ||data.all_organization == 'false') {
                    data.organization_ids=[];
                    $scope.master=false
                }

            }
            if( !angular.isUndefined(data.start_exchange_date)) {
                data.start_exchange_date=$filter('date')(data.start_exchange_date, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(data.end_exchange_date)) {
                data.end_exchange_date= $filter('date')(data.end_exchange_date, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(data.start_payment_date)) {
                data.start_payment_date=$filter('date')(data.start_payment_date, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(data.end_payment_date)) {
                data.end_payment_date= $filter('date')(data.end_payment_date, 'dd-MM-yyyy')
            }

            if(action == 'export'){
                $rootScope.progressbar_start();
                $http({
                    url: "/api/v1.0/sponsorship/paymentsService/all",
                    method: "POST",
                    data: data
                }).then(function (response) {
                    $rootScope.progressbar_complete();
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;

                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });


            }else{
                data.page=1;
                LoadPayments(data);
            }
        };
        $scope.ChooseTemplate=function(id,action){

            $rootScope.clearToastr();

            $uibModal.open({
                templateUrl: '/app/modules/sponsorship/views/payment/model/template_uploaded_form.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/sponsorship/related/cheque-template-upload',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                    };

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(!response){
                            $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                        }else{
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            RestTemplate();
                        }

                        $modalInstance.close();

                    };


                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });


        };
        $rootScope.download=function(action,id){

            $rootScope.clearToastr();
            var url='';
            var type='docx';

            if(action ==='default_template'){
                url='/api/v1.0/sponsorship/related/cheque-template/-1';
            }else if(action ==='template_instruction'){
                url='/api/v1.0/common/banks/templateInstruction';
                type='pdf';
            }

            $rootScope.progressbar_start();
            $http({
                url:url,
                method: "GET"
            }).then(function (response) {
                $rootScope.progressbar_complete();
                window.location = '/api/v1.0/common/files/'+type+'/export?download_token='+response.data.download_token+'&template=true';
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };


        $scope.duplicate=function (id) {
            $rootScope.clearToastr();
            $uibModal.open({

                templateUrl: 'app/modules/sponsorship/views/payment/model/duplicate.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                animation: true,
                size: 'sm',
                controller: function ($rootScope,$scope, $modalInstance, $log) {

                    $scope.confirm = function (check) {
                        $rootScope.progressbar_start();
                        paymentsService.duplicate({payments_id:id , check : check},function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status =='success') {
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                LoadPayments( $rootScope.payment);
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            $modalInstance.dismiss('cancel');

                        });
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }});
        };


        $scope.manage=function (id,action) {

            var msg = $filter('translate')('are you want to confirm delete');

            if(action =='empty'){
                msg = $filter('translate')('are you confirm empty payment');
            }

            $ngBootbox.confirm(msg)
                .then(function() {
                    $rootScope.progressbar_start();
                    paymentsService.delete({id:id,action:action},function (response) {

                        $rootScope.progressbar_complete();
                        if(response.status =='success') {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));

                            if(action == 'delete' || action == 'empty'){
                                resetPaymentsTable();
                            }
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                });

            $rootScope.clearToastr();
        };

        $scope.export=function(id,target){
            $rootScope.clearToastr();
            var params={payments_id:id};

            if(target == 4){
                params.action='guaranteed';
            }else if(target == 5){
                params.ExportTo ='excel';
                params.action='signature_recipient';
            }else if(target == 6){
                params.ExportTo ='excel';
                params.action='signature_sheet';
            }else{
                params.target=target;
                params.action='all';
            }

            $rootScope.progressbar_start();
            $http({
                url: "/api/v1.0/sponsorship/paymentsService/exportCaseList",
                method: "POST",
                data: params
            }).then(function (response) {
                $rootScope.progressbar_complete();
                var file_type='xlsx';
                if(response.data.status == 'false' || response.data.status == false){
                    $rootScope.toastrMessages('error', $filter('translate')('There are no cases to export file'));
                }else{
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }

            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            }); };

        $scope.exportCustom = function(id){
            paymentsService.exportCustom({payment_id:id},function (response) {
                window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token;
            });
        }

        $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {$scope.dt = new Date();};
        $scope.today();
        $scope.clear = function() {$scope.dt = null;};
        $scope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup1.opened = true;};
        $scope.open3 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup3.opened = true;};
        $scope.open7 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup7.opened = true;};
        $scope.open8 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup8.opened = true;};
        $scope.open9 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup9.opened = true;};
        $scope.popup8 = {opened: false};
        $scope.popup9 = {opened: false};
        $scope.popup3 = {opened: false};
        $scope.popup7 = {opened: false};
        $scope.popup1 = {opened: false};

    });

angular.module('SponsorshipModule')
    .controller('PersonsPaymentsController', function ($http,$filter,$rootScope,$scope,$state,$uibModal,$stateParams,paymentsService,Org,locations,Entity) {



        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        // set sidebar closed and body solid layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = true;
        $state.current.data.pageTitle = $filter('translate')('Persons Payments');

        $scope.success =false;
        $scope.failed =false;
        $scope.error =false;
        $scope.Sponsorpayment=true;
        $scope.model_error_status =false;
        $scope.custom = true;
        $scope.master=false;
        $scope.sponsor=true;
        $scope.status ='';
        $scope.msg ='';
        $scope.filter={};
        $scope.items=[];
        $scope.sortKeyArr=[];
        $scope.sortKeyArr_rev=[];
        $scope.sortKeyArr_un_rev=[];
        $scope.CurrentPage = 1;
        $scope.itemsCount='50';


        $scope.close=function(){
            $scope.success =false;
            $scope.failed =false;
            $scope.error =false;
            $scope.model_error_status =false;
        };

        var resetPersonStatisticsFilter =function(){
            $scope.filter={
                "page":1,
                "all_organization":false,
                'payment_status':"",
                'status': "",
                'first_name': "",
                'second_name': "",
                'third_name': "",
                'last_name': "",
                'location_id': "",
                'category_id': "",
                'organization_id': "",
                'sponsor_id': "",
                'id_card_number': "",
                'g_id_card_number': "",
                'guardian_first_name': "",
                'guardian_second_name': "",
                'guardian_third_name': "",
                'guardian_last_name': "",
                'max_amount': "",
                'min_amount': "",
                'begin_date_from':"",
                'end_date_from':"",
                'begin_date_to':"",
                'end_date_to':"" ,
                'itemsCount':50
            };
            if($stateParams.id){
                $scope.filter.sponsor_id=$stateParams.id;
                $scope.sponsor=true;
            }else{
                $scope.sponsor=false;
            }

        };

        $scope.sort_ = function(keyname){
            $scope.sortKey_ = keyname;
            var reverse_ = false;
            if (
                 ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
                 ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
                ) {
                reverse_ = true;
            }
            var data = angular.copy($scope.filter);
            data.action ='filter';

            if (reverse_) {
                if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                    $scope.sortKeyArr_rev=[];
                    $scope.sortKeyArr_un_rev=[];
                    $scope.sortKeyArr_rev.push(keyname);
                }
            }
            else {
                if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                    $scope.sortKeyArr_rev=[];
                    $scope.sortKeyArr_un_rev=[];
                    $scope.sortKeyArr_un_rev.push(keyname);
                }
           
            }

            data.sortKeyArr_rev = $scope.sortKeyArr_rev;
            data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;

            $scope.Search(data,'filter');
        };
        var LoadPersonStatistics =function(params){
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            paymentsService.persons(params,function (response) {
                $rootScope.progressbar_complete();
                $scope.items= response.cases.data;
                $scope.CurrentPage = response.cases.current_page;
                $scope.TotalItems = response.cases.total;
                $scope.ItemsPerPage = response.cases.per_page;
                // $scope.master=response.master;
                $scope.TotalPayments=response.total;

                if( !angular.isUndefined(params.all_organization)) {
                    if(params.all_organization == true ||params.all_organization == 'true'){
                        $scope.master=true
                    }else if(params.all_organization == false ||params.all_organization == 'false') {
                        $scope.master=false
                    }
                }

            });
        };
        Entity.get({entity:'entities',c:'currencies,banks,paymentCategory,transferCompany'},function (response) {
            $scope.currency = response.currencies;
            $scope.Banks = response.banks;
            $scope.paymentCategory = response.paymentCategory;
            $rootScope.transferCompany = response.transferCompany;
        });

             var resetTableStatistics = function () {
                $scope.filter.action ='filter';
                $scope.filter.page =$rootScope.CurrentPage;
                $scope.filter.itemsCount =$scope.itemsCount;
                LoadPersonStatistics( $scope.filter);
            };
            if($stateParams.id){
                Org.get({type:'sponsors', id: $stateParams.id}, function(response) {
                    $scope.sponsor_name=response.name;
                });
                $scope.sponsor=true;

            }
            else{
                $scope.sponsor=false;
                Org.list({type:'sponsors'},function (response) { $scope.Sponsor = response; });
            }

            Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

            locations.get_type_location( {id:3}, function(response) {
                $scope.location=  response;
            });

            $scope.pageChanged = function (currentPage) {
                $scope.CurrentPage = currentPage;
                resetTableStatistics();
            };

        $scope.itemsPerPage_ = function (itemsCount) {
                $scope.itemsCount = itemsCount;
                $scope.CurrentPage = 1;
                resetTableStatistics();
            };

            $scope.toggleCustom = function() {
                $scope.custom = $scope.custom === false ? true: false;
                if($scope.custom === true) {
                }else{
                    resetPersonStatisticsFilter();
                }
            };
            $scope.Search=function(params,action) {

                $rootScope.clearToastr();

                var data = angular.copy(params);
                data.action = action;
                data.organization_ids = [];

                if( !angular.isUndefined(data.all_organization)) {
                    if(data.all_organization == true ||data.all_organization == 'true'){
                        angular.forEach(data.organization_id, function(v, k) {
                            data.organization_ids.push(v.id);
                        });
                        delete data.organization_id;
                        $scope.master=true
                    }else if(data.all_organization == false ||data.all_organization == 'false') {
                        data.organization_ids=[];
                        $scope.master=false
                    }

                }
                if( !angular.isUndefined(data.start_cheque_date)) {
                    data.start_cheque_date=$filter('date')(data.start_cheque_date, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(data.end_cheque_date)) {
                    data.end_cheque_date= $filter('date')(data.end_cheque_date, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(data.start_payment_date)) {
                    data.start_payment_date=$filter('date')(data.start_payment_date, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(data.end_payment_date)) {
                    data.end_payment_date= $filter('date')(data.end_payment_date, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(data.begin_date_from)) {
                    data.begin_date_from=$filter('date')(data.begin_date_from, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(data.end_date_from)) {
                    data.end_date_from=$filter('date')(data.end_date_from, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(data.begin_date_to)) {
                    data.begin_date_to=$filter('date')(data.begin_date_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(data.end_date_to)) {
                    data.end_date_to=$filter('date')(data.end_date_to, 'dd-MM-yyyy')
                }

                data.action=action;
                if(action == 'export'){
                    $rootScope.progressbar_start();
                    $http({
                        url: "/api/v1.0/sponsorship/paymentsService/persons",
                        method: "POST",
                        data: data
                    }).then(function (response) {
                        $rootScope.progressbar_complete();
                        var file_type='xlsx';
                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                        $rootScope.toastrMessages('success', $filter('translate')('downloading'));

                    }, function(error) {
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);
                    });


                }else{
                    data.page=1;
                    LoadPersonStatistics(data);
                }
            };

            $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
            $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
            $scope.format = $scope.formats[0];
            $scope.today = function() {$scope.dt = new Date();};
            $scope.today();
            $scope.clear = function() {$scope.dt = null;};
            $scope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup1.opened = true;};
            $scope.open3 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup3.opened = true;};
            $scope.open7 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup7.opened = true;};
            $scope.open8 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup8.opened = true;};
            $scope.open9 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup9.opened = true;};
            $scope.open25 = function($event)  {$event.preventDefault();$event.stopPropagation();$scope.popup25.opened = true;};
            $scope.open26 = function($event)  {$event.preventDefault();$event.stopPropagation();$scope.popup26.opened = true;};
            $scope.open215 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup215.opened = true;};
            $scope.open216 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup216.opened = true;};
            $scope.popup8 = {opened: false};
            $scope.popup9 = {opened: false};
            $scope.popup3 = {opened: false};
            $scope.popup7 = {opened: false};
            $scope.popup1 = {opened: false};
            $scope.popup215 = {opened: false};
            $scope.popup216 = {opened: false};
            $scope.popup25 = {opened: false};
            $scope.popup26 = {opened: false};

            resetPersonStatisticsFilter();
            resetTableStatistics();

        });

angular.module('SponsorshipModule')
    .controller('archiveChequesController', function ($http,$scope,$rootScope,$state,$stateParams,paymentsService,$filter) {



        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        $rootScope.clearToastr();
        $state.current.data.pageTitle = $filter('translate')('archive payments Cheque');
        // $rootScope.settings.layout.pageSidebarClosed = true;
        $scope.items=[];

        var LoadArchivedCheques =function(params){
            $rootScope.progressbar_start();
            paymentsService.archivedCheques(params,function (response) {
                $rootScope.progressbar_complete();

                if(response.items.data.length != 0){
                    $scope.items= response.items.data;
                    $scope.CurrentPage = response.items.current_page;
                    $scope.TotalItems = response.items.total;
                    $scope.ItemsPerPage = response.items.per_page;
                }else{
                    $scope.items=[];
                }
            });
        };

        $scope.pageChanged = function (currentPage) {
            LoadArchivedCheques({page:currentPage});
        };


        $rootScope.search_ = function (name) {
            LoadArchivedCheques({type: $rootScope.orgType,'page':$rootScope.CurrentPage,'name':name});
        };

        LoadArchivedCheques({page:1});

        $scope.download=function(id){
            $rootScope.clearToastr();

            var type='pdf';
            $rootScope.progressbar_start();
            $http({
                url:'api/v1.0/sponsorship/paymentsService/archivedCheques/download/'+id,
                method: "GET"
            }).then(function (response) {
                $rootScope.progressbar_complete();
                window.location = '/api/v1.0/common/files/pdf/export?download_token='+response.data.download_token+'&cheques=true';
            }, function(error) {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled");
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };

    });

angular.module('SponsorshipModule')
    .filter('strReplace', function () {
        return function (input, from, to) {
            input = input || '';
            from = from || '';
            to = to || '';
            return input.replace(new RegExp(from, 'g'), to);
        };
    })
    .controller('PaymentsChequesController', function ($http,$scope,$rootScope,$state,$uibModal,$stateParams,paymentsService,Org,$filter) {



        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        angular.element('.btn').removeClass("disabled");
        $rootScope.clearToastr();

        $state.current.data.pageTitle = $filter('translate')('cheque print');
        $rootScope.settings.layout.pageSidebarClosed = true;
        $scope.CurrentPage = 1;
        $scope.saved=false;
        $scope.do=false;
        $scope.filter={'sponsor_id':"",'currency_id':"",'payment_ids':[]};
        $scope.options=[];
        $scope.distribution={cheque_number : '', cheque_date : ''};
        $scope.items=[];
        $scope.NumberDistribute=false;
        $scope.DateDistribute=false;
        $scope.range=[];
        $scope.list=[];
        $scope.itemsCount = 50;
        $scope.filter.itemsCount = 50 ;


        Org.list({type:'sponsors'}, function(response) {
            $scope.options.sponsors=  response;
            $scope.options.payments=  [];
        });
        $scope.getOptions=function (target) {
            var params={id:$scope.filter.sponsor_id};
            if(target =='list'){
                params.currency_id=$scope.filter.currency_id;
                $scope.filter.payment_ids=[];
            }else{
                $scope.options.currency=[];
                $scope.filter.currency_id="";
                params.target=target;
            }
            $scope.options.payments=[];

            paymentsService.list( params,function (response) {

                if(target =='list'){
                    $scope.options.payments=  response.list;
                }else{
                    $scope.options.currency=  response.list;
                }

            })
        };
        $scope.result=function (options) {
            $rootScope.clearToastr();
            $scope.list=[];
            options.page = $scope.CurrentPage;
            angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();
            paymentsService.result(options,function (response) {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled");
                if(response.data.total != 0){
                    var temp = response.data;
                    if($scope.saved == false){
                        angular.forEach(temp, function(v, k) {
                            v.cheque_date='';
                            v.cheque_account_number='';
                        });
                    }
                    $scope.items = temp;
                    $scope.CurrentPage = response.current_page;
                    $scope.TotalItems = response.total;
                    $scope.ItemsPerPage = response.per_page;
                    $scope.list=new Array($scope.TotalItems);
                    // range($scope.TotalItems);

                }else{
                    $scope.items=[];
                }
            });
        };
        $scope.pageChanged = function (currentPage) {
            $scope.CurrentPage=currentPage;
            $scope.result($scope.filter);
        };

        $scope.itemsCount = '50';
        $scope.itemsPerPage_ = function (itemsCount) {
            $scope.itemsCount = itemsCount;
            $scope.filter.itemsCount = itemsCount ;
            $scope.filter.page = 1 ;
            $scope.CurrentPage=1;
            $scope.result($scope.filter);
        };  

        $scope.distribute=function(action,param){

            $rootScope.clearToastr();
            var params = {'ids':$scope.filter.payment_ids,action:action};
            var save=false;
            if(action =='cheque_number'){
                if(!angular.isUndefined(param.cheque_number)) {
                    if(param.cheque_number != '' && param.cheque_number != null) {
                        params.cheque_account_number=parseInt(param.cheque_number);
                        save=true;
                    }
                }
            }
            else if(action =='cheque_date'){
                if( !angular.isUndefined(param.cheque_date)) {
                    params.cheque_date= $filter('date')(param.cheque_date, 'dd-MM-yyyy');
                    save=true;
                }
            }

            if(save){
                angular.element('.btn').addClass("disabled");
                $rootScope.progressbar_start();
                paymentsService.saveCheques(params,function (response) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled");
                    if(response.status=='success')
                    {
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        $scope.saved=true;
                        $scope.districts=response.districts;
                        $scope.total_amount=response.total_amount;
                        $scope.total_shekel_amount=response.total_shekel_amount;
                        if(action =='cheque_number'){
                            if(!angular.isUndefined(param.cheque_number)) {
                                if(param.cheque_number != '' && param.cheque_number != null) {
                                    $scope.NumberDistribute=true;
                                }
                            }
                        }
                        else if(action =='cheque_date'){
                            if( !angular.isUndefined(param.cheque_date)) {
                                $scope.DateDistribute=true;
                            }
                        }

                        $scope.result($scope.filter);
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                }, function(error) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled");
                    $rootScope.toastrMessages('error',error.data.msg);
                });
            }else{
                $rootScope.toastrMessages('error',$filter('translate')('Check the value entered'));
            }
        };
        $scope.getRange=function (district_id) {

            if(angular.isUndefined(district_id) || district_id ==''|| district_id ==' '|| district_id ==null) {
                $scope.list=new Array($scope.TotalItems);
            }else{
                $rootScope.clearToastr();
                var params = angular.copy($scope.filter);
                params.district_id=district_id;
                paymentsService.districtRange(params,function (response) {
                    $scope.list=new Array(response.size);
                });

            }



        };
        $scope.close=function(){
            $scope.success =false;
            $scope.error =false;
        };
        $scope.merge=function () {
            $rootScope.clearToastr();
            var params = angular.copy($scope.distribution);
            params.ids=$scope.filter.payment_ids;
            params.cheque_date = $filter('date')(params.cheque_date, 'yyyy-MM-dd');
            $rootScope.progressbar_start();
            paymentsService.merge(params,function (response) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                $scope.saved=true;
                $scope.districts=response.districts;
            })
        };
        $scope.export=function (params,target) {

            params.group=true;
            $rootScope.clearToastr();
            if(params.to - params.from > 500){
                $rootScope.toastrMessages('error',$filter('translate')('Please modify the period (from - to) because the maximum number of bonds exported is 500 bonds'))
            }else {
                var file_type='xlsx';
                if(target == 'pdf') {
                    file_type='pdf';
                }else{
                }

                params.target = target;
                $rootScope.progressbar_start();
                paymentsService.map(params,function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status == true){
                        if(response.download_token){
                            if(target == 'pdf') {
                                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token+'&deleted=false&cheques=true';
                            }else{
                                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                            }
                        }
                        $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('There are no recipients within the specified period'));
                    }
                });
            }
        };
        $scope.exportOne=function (id) {
            $rootScope.clearToastr();

            var params = {
                payment_ids:$scope.filter.payment_ids,
                target:'pdf',
                payment_exchange:$scope.filter.payment_exchange,
                person_id:id,
                group:false
            };
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            $http({url: "/api/v1.0/sponsorship/paymentsService/export", method: "POST", data: params})
                .then(function (response) {
                    $rootScope.progressbar_complete();
                    if(response.data.status == true){
                        if(response.data.download_token){
                            var file_type='pdf';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token+'&deleted=false&cheques=true';
                        }
                        $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('There are no recipients within the specified period'));
                    }
                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });
        };
        $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $scope.formats = [ 'yyyy-MM-dd','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {$scope.dt = new Date();};
        $scope.today();
        $scope.clear = function() {
            $scope.dt = null;
        };
        $scope.open1 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup1.opened = true;
        };
        $scope.popup1 = {
            opened: false
        };

    });

angular.module('SponsorshipModule')
    .controller('ChequeBanksTemplateController', function ($http,$filter,$scope,$rootScope,$state,$uibModal,$stateParams,OAuthToken, FileUploader,BankChequeTemplate) {

        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        $state.current.data.pageTitle = $filter('translate')('cheque-banks-template');
        $scope.success =false;
        $scope.failed =false;
        $scope.error =false;
        $scope.custom = true;
        $scope.status ='';
        $scope.msg ='';
        $scope.entries=[];
        $scope.close=function(){
            $scope.success =false;
            $scope.failed =false;
            $scope.error =false;
        };

        function list() {
            $scope.entries = BankChequeTemplate.query();
        }


        list();

        $scope.save=function(item){
            $rootScope.clearToastr();

            $rootScope.row={};
            $rootScope.action  ='new';
            if(item.bank_id != null){
                var row = angular.copy(item);
                delete row['template'];
                delete row['name'];
                $rootScope.action  = 'edit';
                $rootScope.row=row;
            }else{
                $rootScope.row={'bank_id':item.id};
            }

            $uibModal.open({
                templateUrl: 'app/modules/sponsorship/views/payment/model/cheque_banks_template_setting.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,BankChequeTemplate) {

                    $scope.confirm=function (row) {

                        $rootScope.clearToastr();
                        if($rootScope.action == 'new'){
                            $rootScope.progressbar_start();
                            var target = new BankChequeTemplate(row);
                            target.$save()
                                .then(function (response) {
                                    $rootScope.progressbar_complete();
                                    if(response.status=='error' || response.status=='failed')
                                    {
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    else if(response.status=='failed_valid')
                                    {
                                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                        $scope.status1 =response.status;
                                        $scope.msg1 =response.msg;
                                    }
                                    else{
                                        if(response.status=='success')
                                        {
                                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                            list();
                                        }else{
                                            $rootScope.toastrMessages('error',response.msg);
                                        }

                                        $modalInstance.close();
                                    }

                                },function(error) {
                                    $rootScope.progressbar_complete();
                                    $rootScope.toastrMessages('error',error.data.msg);
                                    $modalInstance.close();

                                });
                        }else{
                            $rootScope.progressbar_start();
                            var target = new BankChequeTemplate(row);
                            target.$update()
                                .then(function (response) {
                                    $rootScope.progressbar_complete();
                                    if(response.status=='error' || response.status=='failed')
                                    {
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    else if(response.status=='failed_valid')
                                    {
                                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                        $scope.status1 =response.status;
                                        $scope.msg1 =response.msg;
                                    }
                                    else{
                                        $modalInstance.close();
                                        if(response.status=='success')
                                        {
                                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                            list();
                                        }else{
                                            $rootScope.toastrMessages('error',response.msg);
                                        }
                                    }
                                },function(error) {
                                    $rootScope.progressbar_complete();
                                    $rootScope.toastrMessages('error',error.data.msg);
                                    $modalInstance.close();
                                });
                        }

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });


        };

        $scope.ChooseTemplate=function(id,action){

            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'app/modules/sponsorship/views/payment/model/cheque_banks_template_setting.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/common/banks/template-upload/'+id,
                        removeAfterUpload: false, queueLimit: 1,
                        headers: {Authorization: OAuthToken.getAuthorizationHeader()}});
                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                    };

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(!response){
                            $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                        }else{
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            list({page:$rootScope.CurrentPage});
                        }

                        $modalInstance.close();

                    };


                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });


        };
        $scope.download=function(){

            $rootScope.clearToastr();
            var url='/api/v1.0/common/BankChequeTemplate/pdf/1';
            var type='pdf';


            $rootScope.progressbar_start();
            $http({
                url:url,
                method: "GET"
            }).then(function (response) {
                $rootScope.progressbar_complete();
                window.location = '/api/v1.0/common/files/'+type+'/export?download_token='+response.data.download_token+'&template=true';
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });

        };

    });




