

angular.module('CommonModule')
    .controller('CaseReportsAttachmentsController', function ($scope,$rootScope,$uibModal,$timeout,$http,$state,$stateParams, FileUploader, OAuthToken,reports,$filter) {

        $rootScope.settings.layout.pageSidebarClosed = true;
        $state.current.data.pageTitle = $filter('translate')('case reports attachments');
        $rootScope.msg ='';
        $scope.items=[];
        $rootScope.filters={'sponsorship_cases_id':$stateParams.id, "report_type":"", "category_id":"", "date_from":"", "date_to":"","page":1};

        if($stateParams.id==null){
            $rootScope.toastrMessages('error',$filter('translate')('There is a bug in loading the page you will be redirected to the sponsored ratings page to re-request the correct page link'));
            $timeout(function() {
                $state.go('sponsorship-cases-statistics');
            }, 3000);
        }




        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });


        $rootScope.case =reports.getCaseInfo({id:$stateParams.id});

        var LoadCaseReportsAttachment=function(params){
            $rootScope.clearToastr();
            params.sponsorship_cases_id=$stateParams.id;
            angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();
            reports.caseAttachments(params,function (response) {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                $scope.items= response;
            });
        };
        var resetTable = function () {

            var params = angular.copy($rootScope.filters);
            params.action='paginate';

            if( !angular.isUndefined(params.date_from)) {
                params.date_from=$filter('date')(params.date_from, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(params.date_to)) {
                params.date_to=$filter('date')(params.date_to, 'dd-MM-yyyy')
            }

            params.page=$rootScope.CurrentPage;
            LoadCaseReportsAttachment(params);
        };
        $scope.searchCases = function (item,action) {

            $rootScope.clearToastr();
            var params = angular.copy(item);
            params.action=action;

            if( !angular.isUndefined(params.date_from)) {
                params.date_from=$filter('date')(params.date_from, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(params.date_to)) {
                params.date_to=$filter('date')(params.date_to, 'dd-MM-yyyy')
            }

            if(action == 'export' || action == 'exportToExcel'){
                angular.element('.btn').addClass("disabled");
                $rootScope.progressbar_start();
                $http({
                    url: "/api/v1.0/sponsorship/reports/caseAttachments",
                    method: "POST",
                    data: params
                }).then(function (response) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                    var file_type='zip';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });
            }else{
                params.page=$rootScope.CurrentPage;
                LoadCaseReportsAttachment(params);
            }
        };
        $scope.ChooseAttachment=function(id){
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'myModalContent2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,cases,OAuthToken, FileUploader) {

                    $scope.fileupload = function () {
                        if($scope.uploader.queue.length){
                            var image = $scope.uploader.queue.slice(1);
                            $scope.uploader.queue = image;
                        }
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/doc/files',
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });
                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                        item.formData = [{action:"not_check"}];
                    };
                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        fileItem.id = response.id;
                        reports.setAttachment({attach_id:id,document_id:fileItem.id},function (subResponse) {
                            if(subResponse.status=='error' || subResponse.status=='failed') {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(subResponse.status== 'success'){
                                $rootScope.toastrMessages('success',subResponse.msg);
                                $modalInstance.close();
                                resetTable();
                            }

                        }, function(error) {
                            $rootScope.toastrMessages('error',error.data.msg);
                        });
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

        $scope.upload=function(item,action){
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'myModalContent2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,cases,OAuthToken, FileUploader) {

                    $scope.fileupload = function () {
                        if($scope.uploader.queue.length){
                            var image = $scope.uploader.queue.slice(1);
                            $scope.uploader.queue = image;
                        }
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/doc/files',
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });
                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                        item.formData = [{action:"not_check"}];
                    };
                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);

                        var params = item;
                        params.document_id = response.id;
                        params.attach_id=null;
                        delete params.id;
                        if( !angular.isUndefined(params.date_from)) {
                            params.date_from=$filter('date')(params.date_from, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.date_to)) {
                            params.date_to=$filter('date')(params.date_to, 'dd-MM-yyyy')
                        }

                        reports.setAttachment(params,function (subResponse) {
                            if(subResponse.status=='error' || subResponse.status=='failed') {
                                $rootScope.toastrMessages('error',subResponse.msg);
                            }
                            else if(subResponse.status== 'success'){
                                $rootScope.toastrMessages('success',subResponse.msg);
                                $modalInstance.close();
                                resetTable();
                            }
                        }, function(error) {
                            $rootScope.toastrMessages('error',error.data.msg);
                        });
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

        $scope.generate = function () {

            $rootScope.clearToastr();
            var params = angular.copy($rootScope.filters);

            if( !angular.isUndefined(params.date_from)) {
                params.date_from=$filter('date')(params.date_from, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(params.date_to)) {
                params.date_to=$filter('date')(params.date_to, 'dd-MM-yyyy')
            }
            angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();
            reports.generateWithOptions(params,function (response) {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                if(response.status=='failed') {
                    $rootScope.toastrMessages('error',response.msg);
                }
                else if(response.status== 'success'){
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                }
            });
        };

        $scope.attachmentInstruction=function(){
            $rootScope.clearToastr();
            var url='/api/v1.0/common/attachmentInstruction';
            var type='pdf';

            $rootScope.progressbar_start();
            $http({
                url:url,
                method: "GET"
            }).then(function (response) {
                $rootScope.progressbar_complete();
                window.location = '/api/v1.0/common/files/'+type+'/export?download_token='+response.data.download_token+'&template=true';
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });

        };
        //-----------------------------------------------------------------------------------------------------------
        var today = new Date();
        $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
        $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};
        $scope.popup={0:{},1:{opened:false},2:{opened:false}};
        $scope.open=function (target,$event) {
            $event.preventDefault();
            $event.stopPropagation();
            if(target == 1){ $scope.popup[1].opened = true;  }
            else if(target == 2) { $scope.popup[2].opened = true; }
        };
    });
