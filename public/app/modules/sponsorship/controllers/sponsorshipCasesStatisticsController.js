angular.module('SponsorshipModule')
    .controller('sponsorshipCasesStatisticsController', function ($filter,$scope,$rootScope,$uibModal,related, $http,sponsor_cases,$timeout,$state,$stateParams,nominate,FileUploader, OAuthToken,casesInfo,Org,reports) {

        $rootScope.settings.layout.pageSidebarClosed = true;
        $state.current.data.pageTitle = $filter('translate')('cases-statistics');

        $scope.temp='';
        $scope.items=[];
        $scope.itemsCount = 50;
        $scope.CurrentPage = 1;
        $scope.sortKeyArr=[];
        $scope.sortKeyArr_rev=[];
        $scope.sortKeyArr_un_rev=[];

        $scope.sponsor=true;
        $rootScope.person_restrict=false;
        $scope.mode =$stateParams.mode;

        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        $scope.closeRestricted=function(){
            if( $rootScope.person_restrict == true){
                $rootScope.person_restrict =false;
            }
            if( $rootScope.card_restrict == true){
                $rootScope.card_restrict =false;
            }
            $rootScope.restricted=[];
            $rootScope.invalid_cards=[];

        };

        var resetFiltersData =function(){
            $rootScope.DataFilter={
                "all_organization":false,
                // 'organization_id':"",
                'status':"",
                'first_name':"",
                'second_name':"",
                'third_name':"",
                'last_name':"",
                'id_card_number':"",
                'guardian_id_card_number':"",
                'guardian_first_name':"",
                'guardian_second_name':"",
                'guardian_third_name':"",
                'guardian_last_name':"",
                'mother_id_card_number':"",
                'mother_first_name':"",
                'mother_second_name':"",
                'mother_third_name':"",
                'mother_last_name':"",
                'father_id_card_number':"",
                'father_first_name':"",
                'father_second_name':"",
                'father_third_name':"",
                'father_last_name':"",
                'sponsor_id':"",
                'category_id':"",
                'from_sponsorship_date':"",
                'to_sponsorship_date':"",
                'to_guaranteed_date':"",
                'from_guaranteed_date':"",
                'action':'filters',

                'itemsCount':50
            };
            if($stateParams.id){
                $rootScope.DataFilter.sponsor_id= $stateParams.id;
            }
        };
        $scope.sponsor= $rootScope.sponsor=null;
        $scope.export='false';

        Org.list({type:'organizations'},function (response) {  $scope.Org = response; });


        var loadCasesStatistics = function(filters) {
            // $rootScope.clearToastr();
            var data = angular.copy(filters);

            if($stateParams.id){
                data.sponsor_id= $stateParams.id;
            }

            if( !angular.isUndefined(data.from_sponsorship_date)) {
                data.from_sponsorship_date=$filter('date')(data.from_sponsorship_date, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(data.to_sponsorship_date)) {
                data.to_sponsorship_date=$filter('date')(data.to_sponsorship_date, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(data.to_guaranteed_date)) {
                data.to_guaranteed_date=$filter('date')(data.to_guaranteed_date, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(data.from_guaranteed_date)) {
                data.from_guaranteed_date=$filter('date')(data.from_guaranteed_date, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(data.all_organization)) {
                if(data.all_organization == true ||data.all_organization == 'true'){
                    var orgs=[];
                    angular.forEach(data.organization_id, function(v, k) {
                        orgs.push(v.id);
                    });

                    data.organization_ids=orgs;

                }
                else if(data.all_organization == false ||data.all_organization == 'false') {
                    data.organization_ids=[];
                    $scope.master=false
                }
            }

            $rootScope.progressbar_start();
            casesInfo.getCasesStatistics(data ,function (response) {
                $rootScope.progressbar_complete();
                
                if(data.action == 'export' || data.action == 'ExportToWord'|| 
                   data.action == 'FamilyMember'|| data.action == 'pdf'){
                    
                    if(response.status == 'false'){
                        $rootScope.toastrMessages('error',$filter('translate')('there is no cases to export'));
                    }else if(data.action == 'FamilyMember' && response.status == false){
                        $rootScope.toastrMessages('error',$filter('translate')('there is no family member to export'));
                    }else{
                        var file_type='xlsx';
                        if(data.action == 'ExportToWord') {
                            file_type='zip';
                        }
                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                    }
                }else{
                    
                    if(response.cases.data.length != 0){
                        $rootScope.result='true';
                        $scope.export='true';
                        var temp= response.cases.data;
                        angular.forEach(temp, function(v, k) {
                            v.flag=parseInt(v.flag);
                        });
                        $scope.items= temp;
                        $scope.master=response.master;
                        $scope.CurrentPage = response.cases.current_page;
                        $rootScope.TotalItems = response.cases.total;
                        $rootScope.ItemsPerPage = response.cases.per_page;
                        $scope.All=false;
                        $scope.selectedAll($scope.All);
                    }else{
                        $rootScope.result='false';
                        $scope.export='false';
                        $scope.items=[];
                    }
                }

            });
        };
        var resetTableContent=function () {
            var params = angular.copy($rootScope.DataFilter);
            params.action='filters';
            params.page=$scope.CurrentPage;
            if($stateParams.id){
                params.sponsor_id=$rootScope.sponsor_id;
            }
            params.organization_ids=[];

            if( !angular.isUndefined(params.all_organization)) {
                if(params.all_organization == true ||params.all_organization == 'true'){
                    var orgs=[];
                    angular.forEach(params.organization_id, function(v, k) {
                        orgs.push(v.id);
                    });

                    params.organization_ids=orgs;

                }
                else if(params.all_organization == false ||params.all_organization == 'false') {
                    params.organization_ids=[];
                    $scope.master=false
                }
            }

            loadCasesStatistics(params);
        };

        if($stateParams.id){
            $rootScope.sponsor_id= $rootScope.sponsor_id=$stateParams.id;
            Org.get({type: 'sponsors', id: $stateParams.id}, function(response) {
                $rootScope.target = response;
                $rootScope.sponsor_name=$scope.target.name;
            });
            $rootScope.sponsor=true;

            Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

        }else{
            $rootScope.sponsor=false;
            Org.list({type:'sponsors'},function (response) { $rootScope.Sponsors = response; });
        }
        resetFiltersData();
        resetTableContent();
        $rootScope.download=function(target,id){
            $rootScope.clearToastr();
            if(target == 'candidate'){
                $uibModal.open({
                    size: 'md',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    templateUrl:'/app/modules/sponsorship/views/model/select_lang.html',
                    controller: function ($rootScope,$scope,$modalInstance) {

                        $scope.confirm = function (lang) {
                            $rootScope.progressbar_start();
                            sponsor_cases.exportSponsorCase({id_:id, target:target,lang:lang},function(response) {
                                $rootScope.progressbar_complete();
                                if(response.download_token){
                                    window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token;
                                    $modalInstance.close();
                                }else{
                                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                                }
                            });
                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    }});

            }else{
                $rootScope.progressbar_start();
                related.updateStatusTemplate(function(response) {
                    $rootScope.progressbar_complete();
                    if(response.download_token){
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token+'&template=true';
                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                    }
                });
            }
        };
        $scope.pageChanged = function (CurrentPage) {
            var params =$rootScope.DataFilter;
            params.page=CurrentPage;
            params.action='filters';            
            loadCasesStatistics(params);
        };
        $rootScope.itemsPerPage_ = function (itemsCount) {
            $scope.DataFilter.page = 1 ;
            $scope.DataFilter.itemsCount = itemsCount ;
            $scope.itemsCount = itemsCount ;
            var params =$rootScope.DataFilter;
            params.action='filters';
            loadCasesStatistics(params);
        
        };
        $scope.restMsg = function (index) {

            if( !angular.isUndefined($rootScope.msg1[index])) {
                $rootScope.msg1[index]=[];
            }
        }
        $scope.SaveChanged = function (id,sponsor_number,index) {
            $rootScope.clearToastr();
            $rootScope.msg1=[];
            if( !angular.isUndefined(sponsor_number)) {
                if( sponsor_number != "" && sponsor_number != 'null'&& sponsor_number != null) {
                    $rootScope.progressbar_start();
                    var target = new casesInfo({id:id,sponsor_number:sponsor_number});
                    target.$updateSponsorNumber()
                        .then(function (response) {

                            $rootScope.progressbar_complete();
                            if(response.status!='theSame'){
                                if(response.status=='failed_valid') {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $rootScope.status1 =response.status;
                                    $rootScope.msg1[index]=response.msg;
                                    $scope.items[index].sponsor_number='';
                                }
                                else if(response.status=='success') {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }
                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                        });
                }
            }
        };
        $scope.SavePrev = function (sponsor_number) {
            $scope.temp=sponsor_number;
        };

        //-----------------------------------------------------------------------------------------------------------
        $scope.custom = true;
        $scope.toggleCustom = function() {
            $scope.custom = $scope.custom === false ? true: false;

            if($scope.custom !== true) {
                $scope.export='false';
            }

            resetFiltersData();
        };
        $scope.selectedAll = function(value){
            if(value){
                $scope.selectAll = true;
            }
            else{
                $scope.selectAll =false;

            }
            angular.forEach($scope.items, function(val,key)
            {
                val.caseCheck=$scope.selectAll;
            });
        };
        $scope.statusLog=function(id){
            $rootScope.clearToastr();
            $uibModal.open({
                size: 'lg',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                templateUrl: 'statusLog.html',
                controller: function ($rootScope,$scope,$modalInstance) {
                    $scope.statusItems=[];


                    var loadStatusLog = function(params) {
                        $rootScope.clearToastr();
                        $rootScope.progressbar_start();
                        casesInfo.getCasesStatusLog(params ,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.items.data.length != 0){
                                $scope.statusItems= response.items.data;
                                $scope.CurrentPage = response.items.current_page;
                                $scope.TotalItems = response.items.total;
                                $scope.ItemsPerPage = response.items.per_page;
                            }else{
                                $scope.statusItems=[];
                            }
                        });
                    };

                    $scope.statusPageChanged = function (CurrentPage) {
                        loadStatusLog({page:CurrentPage,id:id});
                    };

                    loadStatusLog({page:1,id:id});

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }});

        };
        $scope.delete = function(size,id){
            $rootScope.clearToastr();
            $uibModal.open(
                {
                    templateUrl: 'deleteSpons.html',
                    size: size,
                    controller: function ($rootScope,$scope,$modalInstance, $log) {

                        $scope.confirmDelete = function () {
                            $rootScope.clearToastr();
                            $rootScope.progressbar_start();
                            nominate.delete({'id':id}, function (response) {
                                $rootScope.progressbar_complete();

                                if(response.status == 'success')
                                {
                                    resetTableContent();
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                $modalInstance.close();
                            });

                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };


                    }
                });

        };
        $scope.reports=function(){
            $rootScope.clearToastr();

            $uibModal.open({
                size: 'lg',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                templateUrl: 'reports.html',
                controller: function ($rootScope,$scope,$modalInstance) {
                    $scope.msg1=[];
                    $scope.options_cases=[];
                    $scope.report={all:true};

                    $scope.sponsorCases= function (id,category_id) {

                        $rootScope.clearToastr();
                        if( !angular.isUndefined($scope.msg1.sponsor_id)) {
                            $scope.msg1.sponsor_id=[];
                        }
                        if( !angular.isUndefined($scope.msg1.category_id)) {
                            $scope.msg1.category_id=[];
                        }

                        if(!angular.isUndefined($scope.report.sponsor_id) && !angular.isUndefined($scope.report.category_id)) {
                            $scope.options_cases=reports.getSponsorCasesList({id:id,category_id:category_id});
                        }

                    };
                    $rootScope.close=function(){
                        $rootScope.model_error_status =false;
                        $rootScope.msg2 ='';
                    };

                    $scope.confirm = function (row) {
                        $rootScope.clearToastr();
                        $rootScope.model_error_status =false;
                        $rootScope.msg2 ='';

                        var item = angular.copy(row);

                        if( !angular.isUndefined(item.date_from)) {
                            item.date_from=$filter('date')(item.date_from, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(item.date_to)) {
                            item.date_to=$filter('date')(item.date_to, 'dd-MM-yyyy')
                        }

                        $rootScope.progressbar_start();
                        reports.save(item,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                $modalInstance.close();
                                if(response.status=='success')
                                {
                                    window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token+'&reports=true';
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }
                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                            $modalInstance.close();
                        });

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    var today = new Date();
                    $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
                    $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};
                    $scope.popup={0:{},1:{opened:false},2:{opened:false}};
                    $scope.open=function (target,$event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        if(target == 1){ $scope.popup[1].opened = true;  }
                        else if(target == 2) { $scope.popup[2].opened = true; }
                    };
                }});

        };
        $scope.SearchCases=function(selected , data,action,target){

            $rootScope.clearToastr();            
            data.action=action;            
            data.items=[];

            if(selected == true || selected == 'true' ){
            
                var items =[];
                 angular.forEach($scope.items, function(v, k){
                      if(v.caseCheck){
                             items.push(v.id);
                         }
                 });

                 if(items.length==0){
                     $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                     return ;
                 }
                 data.items=items;
            }
             
            if(action === 'ExportToWord'){
               data.template_target=target;
           }
           data.page=1;
           loadCasesStatistics(data);

        };
        $scope.sortKeyArr=[];
        $scope.sortKeyArr_rev=[];
        $scope.sortKeyArr_un_rev=[];
        $scope.sort_ = function(keyname){
            $scope.sortKey_ = keyname;
            var reverse_ = false;
            if (
                ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
                ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
            ) {
                reverse_ = true;
            }
            var data = angular.copy($rootScope.DataFilter);
            data.action ='filters';

            if (reverse_) {
                if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                    $scope.sortKeyArr_rev=[];
                    $scope.sortKeyArr_un_rev=[];
                    $scope.sortKeyArr_rev.push(keyname);
                }
            }
            else {
                if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                    $scope.sortKeyArr_rev=[];
                    $scope.sortKeyArr_un_rev=[];
                    $scope.sortKeyArr_un_rev.push(keyname);
                }

            }

            data.sortKeyArr_rev = $scope.sortKeyArr_rev;
            data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;
            $scope.SearchCases(data,'filters','null');
        };
        $scope.updateStatustoAll=function(status){
            $rootScope.clearToastr();
            if(status == -1){
                $uibModal.open({
                    templateUrl: 'myModalContent2.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'md',
                    controller: function ($rootScope,$scope, $modalInstance, $log, FileUploader, OAuthToken,Org) {

                        Org.list({type:'sponsors'},function (response) { $scope.Sponsors = response; });
                        $scope.upload=false;
                        $scope.FormData={using:1};

                        $scope.RestUpload=function(value){
                            if(value ==2){
                                $scope.upload=false;
                            }
                        };

                        $scope.fileupload=function(){
                            if($scope.FormData.using==1 && $scope.FormData.sponsor_id != ""&& $scope.FormData.category_id != ""){
                                $scope.upload=true;
                            }else{

                            }
                            //$scope.upload=true;
                        };

                        var Uploader = $scope.uploader = new FileUploader({
                            url: '/api/v1.0/sponsorship/cases-info/UpdateSponsorshipStatus',
                            removeAfterUpload: false,
                            queueLimit: 1,
                            headers: {
                                Authorization: OAuthToken.getAuthorizationHeader()
                            }
                        });

                        if( $rootScope.person_restrict == true){
                            $rootScope.person_restrict =false;
                        }

                         $scope.confirm = function (item) {
                            $rootScope.clearToastr();
                             $rootScope.progressbar_start();
                             Uploader.onBeforeUploadItem = function(i) {
                                $rootScope.clearToastr();
                                if(item.using ==1){
                                    i.formData = [{using: item.using,sponsor_id: item.sponsor_id,category_id: item.category_id,status: item.status}];
                                }else{
                                    i.formData = [{using: item.using,sponsorship_id: item.sponsorship_id,status: item.status}];
                                }
                            };
                            Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                                $rootScope.progressbar_complete();
                                $scope.uploader.destroy();
                                $scope.uploader.queue=[];
                                $scope.uploader.clearQueue();
                                angular.element("input[type='file']").val(null);

                                if(response.result){
                                    if(response.result.length !=0){
                                        $rootScope.person_restrict=true;
                                        $rootScope.restricted=response.result;
                                    }
                                }

                                if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $rootScope.status1 =response.status;
                                    $rootScope.msg1 =response.msg;
                                }
                                else{
                                    if(response.status=='success')
                                    {
                                        $rootScope.toastrMessages('success',response.msg);
                                        resetTableContent();
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    $modalInstance.close();
                                }
                            };
                            Uploader.uploadAll();
                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    }
                });
            }
            else{
                var cases = [];

                angular.forEach($scope.items, function(val,key){
                    if(val.caseCheck){
                        cases.push( {'id':val.id,'status':val.flag})
                    }
                });
                if(cases.length==0){
                    $rootScope.toastrMessages('error',$filter('translate')('cases-statistics'));
                }else {

                    if(status == 3|| status == 4|| status == 5 ){
                        $rootScope.model_case=status;
                        $uibModal.open({
                            templateUrl: 'UpdateSponsorshipData.html',
                            backdrop: 'static',
                            keyboard: false,
                            windowClass: 'modal',
                            size: 'sm',
                            controller: function ($filter, $rootScope, $scope, $modalInstance, $log) {
                                $scope.model_error = false;
                                $scope.action = 'group';

                                $scope.data = {};
                                $scope.confirm = function (row) {

                                    $rootScope.clearToastr();
                                    $rootScope.progressbar_complete();
                                    if(status==3){
                                        if( !angular.isUndefined(row.sponsorship_date)) {
                                            row.sponsorship_date=$filter('date')(row.sponsorship_date, 'dd-MM-yyyy')
                                        }

                                        data={'action':'group','status':status, 'sponsorship_date':row.sponsorship_date, 'sponsorship_number':row.sponsorship_number ,cases:cases};
                                    } else{
                                        if( !angular.isUndefined(row.date)) {
                                            row.date=$filter('date')(row.date, 'dd-MM-yyyy')
                                        }
                                        data={'action':'group','status':status, 'date':row.date, 'reason':row.reason ,cases:cases};
                                    }

                                    var target = new casesInfo(data);
                                    target.$UpdateCasesStatus()
                                        .then(function (response) {
                                            $rootScope.progressbar_start();
                                            if(response.status=='success')
                                            {
                                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                                resetTableContent();
                                            }else{
                                                $rootScope.toastrMessages('error',response.msg);
                                            }
                                            $modalInstance.close();
                                        }, function(error) {
                                            $rootScope.progressbar_complete();
                                            $rootScope.toastrMessages('error',error.data.msg);
                                        });
                                };

                                $scope.cancel = function () {
                                    $modalInstance.dismiss('cancel');
                                };


                                $scope.dateOptions = {
                                    formatYear: 'yy',
                                    startingDay: 0
                                };
                                $scope.formats = ['dd-MM-yyyy', 'dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                                $scope.format = $scope.formats[0];
                                $scope.today = function () {
                                    $scope.dt = new Date();
                                };
                                $scope.today();
                                $scope.clear = function () {
                                    $scope.dt = null;
                                };

                                $scope.open44 = function ($event) {
                                    $event.preventDefault();
                                    $event.stopPropagation();
                                    $scope.popup44.opened = true;
                                };

                                $scope.popup44 = {
                                    opened: false
                                };
                                $scope.open444 = function ($event) {
                                    $event.preventDefault();
                                    $event.stopPropagation();
                                    $scope.popup444.opened = true;
                                };

                                $scope.popup444 = {
                                    opened: false
                                };


                            }
                        });
                    }
                    else{
                        $rootScope.progressbar_start();
                        var target = new casesInfo({'action':'group',status:status,cases:cases});
                        target.$UpdateCasesStatus()
                            .then(function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status == 'success')
                                {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    resetTableContent();
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                            });

                    }
                }

            }

        };
        $scope.UpdateSponsorshipDate=function(id,date){

            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'UpdateSponsorshipDate.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'sm',
                controller: function ($filter,$rootScope,$scope, $modalInstance, $log) {
                    $scope.model_error =false;
                    $scope.data="";
                    if(date != null){
                        $scope.data={sponsorship_date:new Date(date)};
                    }
                    //$rootScope.voucher.voucher_date=new Date($rootScope.voucher.voucher_date);

                    $scope.confirm = function (item) {

                        $rootScope.clearToastr();
                        $rootScope.progressbar_start();
                        if( !angular.isUndefined(item.sponsorship_date)) {
                            item.sponsorship_date=$filter('date')(item.sponsorship_date, 'dd-MM-yyyy')
                        }

                        var target = new casesInfo({id:id,sponsorship_date:item.sponsorship_date});
                        target.$UpdateSponsorshipDate()
                            .then(function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $rootScope.status1 =response.status;
                                    $rootScope.msg1 =response.msg;
                                }
                                else{
                                    if(response.status=='success')
                                    {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        resetTableContent();
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    $modalInstance.close();
                                }

                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                            });


                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };


                    $scope.dateOptions = {
                        formatYear: 'yy',
                        startingDay: 0
                    };
                    $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                    $scope.format = $scope.formats[0];
                    $scope.today = function() {
                        $scope.dt = new Date();
                    };
                    $scope.today();
                    $scope.clear = function() {
                        $scope.dt = null;
                    };

                    $scope.open44 = function($event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        $scope.popup44.opened = true;
                    };

                    $scope.popup44 = {
                        opened: false
                    };


                }
            });

        };
        $scope.exportRestricted = function (restricted) {
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            $http({
                url: "/api/v1.0/common/cases/exportRestricted",
                method: "POST",
                data: {exported:restricted}
            }).then(function (response) {
                $rootScope.progressbar_complete();
                window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token;

            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };
        $scope.updateCaseStatus=function(status,item){
            $rootScope.clearToastr();

            if(status !=3 && status!=5 && status !=4){
                var params = {'action':'single',status:status,cases:[{'id':item.id,'status':item.flag}]};
                $rootScope.progressbar_start();
                casesInfo.UpdateCasesStatus(params,function (response) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                    if(response.status=='success')
                    {
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        resetTableContent();
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                });

            }
            else if(status==3|| status==5||status==4){
                $rootScope.model_case=status;
                $uibModal.open({
                    templateUrl: 'UpdateSponsorshipData.html',
                    backdrop: 'static',
                    keyboard: false,
                    windowClass: 'modal',
                    size: 'sm',
                    controller: function ($filter, $rootScope, $scope, $modalInstance, $log) {
                        $scope.model_error = false;
                        $scope.action = 'single';
                        $scope.data = {};
                        $scope.confirm = function (row) {

                            $rootScope.progressbar_start();
                            $rootScope.clearToastr();
                            if(status==3){
                                if( !angular.isUndefined(row.sponsorship_date)) {
                                    row.sponsorship_date=$filter('date')(row.sponsorship_date, 'dd-MM-yyyy')
                                }

                                data={'action':'single','status':status, cases:[{'id':item.id,'status':item.flag, 'sponsorship_date':row.sponsorship_date, 'sponsorship_number':row.sponsorship_number}]};
                            } else{
                                if( !angular.isUndefined(row.date)) {
                                    row.date=$filter('date')(row.date, 'dd-MM-yyyy')
                                }
                                data={'action':'single','status':status, cases:[{'id':item.id ,'status':item.flag,'date':row.date, 'reason':row.reason}]};
                            }
                            var target = new casesInfo(data);
                            target.$UpdateCasesStatus()
                                .then(function (response) {
                                    $rootScope.progressbar_complete();
                                    if(response.status=='failed_valid') {
                                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                        $rootScope.status1 =response.status;
                                        $rootScope.msg1 =response.msg;
                                    }else{
                                        if(response.status=='success')
                                        {
                                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                            resetTableContent();
                                        }else{
                                            $rootScope.toastrMessages('error',response.msg);
                                        }
                                        $modalInstance.close();
                                    }
                                }, function(error) {
                                    $rootScope.progressbar_complete();
                                    $rootScope.toastrMessages('error',error.data.msg);
                                });
                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };


                        $scope.dateOptions = {
                            formatYear: 'yy',
                            startingDay: 0
                        };
                        $scope.formats = ['dd-MM-yyyy', 'dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                        $scope.format = $scope.formats[0];
                        $scope.today = function () {
                            $scope.dt = new Date();
                        };
                        $scope.today();
                        $scope.clear = function () {
                            $scope.dt = null;
                        };

                        $scope.open44 = function ($event) {
                            $event.preventDefault();
                            $event.stopPropagation();
                            $scope.popup44.opened = true;
                        };

                        $scope.popup44 = {
                            opened: false
                        };
                        $scope.open444 = function ($event) {
                            $event.preventDefault();
                            $event.stopPropagation();
                            $scope.popup444.opened = true;
                        };

                        $scope.popup444 = {
                            opened: false
                        };


                    }
                });
            }
            else if(status==100) {
                $uibModal.open({
                    templateUrl: 'UpdateSponsorshipData.html',
                    backdrop: 'static',
                    keyboard: false,
                    windowClass: 'modal',
                    size: 'sm',
                    controller: function ($filter, $rootScope, $scope, $modalInstance, $log) {
                        $scope.model_error = false;
                        $scope.data = {};

                        $scope.confirm = function (item) {

                            $rootScope.progressbar_start();
                            $rootScope.clearToastr();
                            if (!angular.isUndefined(item.sponsorship_date)) {
                                item.sponsorship_date = $filter('date')(item.sponsorship_date, 'dd-MM-yyyy')
                            }

                            var target = new casesInfo({id: id, sponsorship_date: item.sponsorship_date});
                            target.$UpdateSponsorshipDate()
                                .then(function (response) {
                                    $rootScope.progressbar_complete();
                                    if (response.status == 'failed') {
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    else if (response.status == 'failed_valid') {
                                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                        $rootScope.status1 = response.status;
                                        $rootScope.msg1 = response.msg;
                                    }
                                    else {
                                        if (response.status == 'success') {
                                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                            resetTableContent();
                                        } else {
                                            $rootScope.toastrMessages('error',response.msg);
                                        }
                                        $modalInstance.close();
                                    }

                                }, function (error) {
                                    $rootScope.progressbar_complete();
                                    $rootScope.toastrMessages('error',error.data.msg);
                                });


                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };


                        $scope.dateOptions = {
                            formatYear: 'yy',
                            startingDay: 0
                        };
                        $scope.formats = ['dd-MM-yyyy', 'dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                        $scope.format = $scope.formats[0];
                        $scope.today = function () {
                            $scope.dt = new Date();
                        };
                        $scope.today();
                        $scope.clear = function () {
                            $scope.dt = null;
                        };

                        $scope.open44 = function ($event) {
                            $event.preventDefault();
                            $event.stopPropagation();
                            $scope.popup44.opened = true;
                        };

                        $scope.popup44 = {
                            opened: false
                        };


                    }
                });
            }
        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 0
        };
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();
        $scope.clear = function() {
            $scope.dt = null;
        };

        $scope.open1 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup1.opened = true;
        };

        $scope.open2 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup2.opened = true;
        };

        $scope.open3 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup3.opened = true;
        };
        $scope.open4 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup4.opened = true;
        };

        $scope.popup1 = {
            opened: false
        };
        $scope.popup2 = {
            opened: false
        };

        $scope.popup3 = {
            opened: false
        };

        $scope.popup4 = {
            opened: false
        };



    });
