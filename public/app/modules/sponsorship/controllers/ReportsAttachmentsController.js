

angular.module('CommonModule')
.controller('ReportsAttachmentsController', function ($scope,category,$rootScope,$uibModal,Org,$http,$state,$stateParams,Entity, FileUploader, OAuthToken,reports,$filter) {

    $rootScope.settings.layout.pageSidebarClosed = true;
    $state.current.data.pageTitle = $filter('translate')('reports attachments');
    $scope.custom = true;
    $scope.items=[];
    $scope.CurrentPage = 1;
    $rootScope.itemsCount = 50 ;
    $scope.filters={page:1};


    $rootScope.sponsorCategories_=[];
        category.getSectionForEachCategory({'type':'sponsorships'},function (response) {
            if(!angular.isUndefined(response)){
                $rootScope.sponsorCategories_firstStep_map =[];
                $rootScope.sponsorCategories = response.Categories;
                if($rootScope.sponsorCategories.length > 0){
                    for (var i in $rootScope.sponsorCategories) {
                        var item = $rootScope.sponsorCategories[i];
                        $rootScope.sponsorCategories_firstStep_map[item.id] = item.FirstStep;
                    }
                     var sponsorCategories_=[];
                    angular.forEach($rootScope.sponsorCategories, function(v, k) {
                        if ($rootScope.lang == 'ar'){
                            sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
                        }
                        else{
                            sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
                        }
                    });

                    $rootScope.sponsorCategories_ = sponsorCategories_ ;
                }

            }
        });

    Org.list({type:'sponsors'},function (response) { $rootScope.Sponsors = response; });

    Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

    var resetFilter =function(){
        $scope.CurrentPage = 1;
        $scope.filters={
            "all_organization":false,
            'sponsor_id':"",
            'title':"",
            "category_id":"",
            "report_type":"",
            "category":"",
            "date_from":"",
            "date_to":"",
            "first_name":"",
            "second_name":"",
            "third_name":"",
            "last_name":"",
            "status":"",
            "action":"paginate",
             "id_card_number":"",
            "page":1
        };
    };

    var LoadReportsAttachment=function(params){
        $rootScope.clearToastr();

        if( !angular.isUndefined(params.date_from)) {
            params.date_from=$filter('date')(params.date_from, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(params.date_to)) {
            params.date_to=$filter('date')(params.date_to, 'dd-MM-yyyy')
        }
        angular.element('.btn').addClass("disabled");
        $rootScope.progressbar_start();
        reports.attachments(params,function (response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if(params.action == 'export' || params.action == 'exportToExcel'){
                if(response.status == false){
                    $rootScope.toastrMessages('error',$filter('translate')('no attachment to export'));
                }else{
                    var file_type='xlsx';
                    if(params.action == 'export') {
                        file_type='zip';
                    }
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }
            }else{
                $scope.items= response.data;
                $scope.CurrentPage = response.current_page;
                $rootScope.TotalItems = response.total;
                $rootScope.ItemsPerPage = response.per_page;                
            }
        });
    };

    resetFilter();

    $scope.close=function(){

        $rootScope.failed =false;
        ;
        $rootScope.model_error_status =false;
        $rootScope.model_error_status2 =false;
    };
    $scope.toggleCustom = function() {
        $scope.custom = $scope.custom == false ? true: false;
        $scope.items=[];
        resetFilter();
    };
    $scope.pageChanged = function (CurrentPage) {
        $scope.filters.page=CurrentPage;
        $scope.filters.action='paginate';
        LoadReportsAttachment($scope.filters);
    };

    $rootScope.itemsPerPage_ = function (itemsCount) {
        $scope.filters.page = 1 ;
        $scope.filters.itemsCount = itemsCount ;
        $scope.filters.action='paginate';
        $scope.CurrentPage = 1;
        LoadReportsAttachment($scope.filters);
    };

    $scope._reset = function() {
        $scope.items=[];
        resetFilter();
    };

    $scope.selectedAll = function(value){
            if(value){
                $scope.selectAll = true;
            }
            else{
                $scope.selectAll =false;

            }
            angular.forEach($scope.items, function(val,key)
            {
                val.check=$scope.selectAll;
            });
    };
        
    $scope.searchCases = function (selected,item,action) {

        $rootScope.clearToastr();

        var params = angular.copy(item);
        params.action=action;
        params.items=[];
        
        if(selected == true || selected == 'true' ){
                    var items=[];
                     angular.forEach($scope.items, function(v, k){                
                         if(v.check){
                             items.push(v.id);
                         }
                     });

                     if(items.length==0){
                         $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                         return ;
                     }
                     params.items=items;
            }

        if( !angular.isUndefined(params.all_organization)) {
            if(params.all_organization == true ||params.all_organization == 'true'){
                var orgs=[];
                angular.forEach(params.organization_id, function(v, k) {
                    orgs.push(v.id);
                });

                params.organization_ids=orgs;
                delete params.organization_id;

            }else if(params.all_organization == false ||params.all_organization == 'false') {
                params.organization_ids=[];
                $scope.master=false
            }
        }

        if( !angular.isUndefined(params.date_from)) {
            params.date_from=$filter('date')(params.date_from, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(params.date_to)) {
            params.date_to=$filter('date')(params.date_to, 'dd-MM-yyyy')
        }
      
        params.page=$scope.CurrentPage;
        LoadReportsAttachment(params);
        
    };
    $scope.download = function (id) {
        $rootScope.clearToastr();
        var params = angular.copy($scope.filters);
        params.sponsorship_cases_id=id;
        params.action='export';

        if( !angular.isUndefined(params.date_from)) {
            params.date_from=$filter('date')(params.date_from, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(params.date_to)) {
            params.date_to=$filter('date')(params.date_to, 'dd-MM-yyyy')
        }
        angular.element('.btn').addClass("disabled");
        $rootScope.progressbar_start();
        $http({
            url: "/api/v1.0/sponsorship/reports/attachments",
            method: "POST",
            data: params
        }).then(function (response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            var file_type='zip';
            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;

        }, function(error) {
            $rootScope.progressbar_complete();
            $rootScope.toastrMessages('error',error.data.msg);
        });

    };

        //-----------------------------------------------------------------------------------------------------------
        // Bulk Export and Import
        $scope.import = {};
        var Uploader = $scope.uploader = new FileUploader({
            url: "/api/v1.0/sponsorship/reports/attachments/import",
            removeAfterUpload: true,
            queueLimit: 1,
            headers: {
                Authorization: OAuthToken.getAuthorizationHeader()
            }
        });
        Uploader.onBeforeUploadItem = function(item) {
            $rootScope.progressbar_start();
            $rootScope.clearToastr();

            var params = angular.copy($scope.filters);
            var orgs=[];
            params.action='export';

            if( !angular.isUndefined(params.all_organization)) {
                if(params.all_organization == true ||params.all_organization == 'true'){
                    angular.forEach(params.organization_id, function(v, k) {
                        orgs.push(v.id);
                    });
                }
            }

            params.organization_ids=orgs;
            item.formData =  [
            {organization_ids: orgs},
            {all_organization: $scope.filters.all_organization},
            {sponsor_id: $scope.filters.sponsor_id},
            {category_id: $scope.filters.category_id},
            {first_name: $scope.filters.first_name},
            {second_name: $scope.filters.second_name},
            {third_name: $scope.filters.third_name},
            {last_name: $scope.filters.last_name},
            {status: $scope.filters.status},
            {doc_category_id: $scope.filters.doc_category_id}
            ];
            if( !angular.isUndefined($scope.filters.date_from)) {
                item.formData.push({date_from :$filter('date')($scope.filters.date_from, 'dd-MM-yyyy')});
            }

            if( !angular.isUndefined($scope.filters.date_to)) {
                item.formData.push({date_to :$filter('date')($scope.filters.date_to, 'dd-MM-yyyy')});
            }

        };

        Uploader.filters.push({
            name: 'zipFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|zip|x-zip-compressed|'.indexOf(type) !== -1;
            }
        });
        Uploader.onCompleteItem = function(fileItem, response, status, headers) {

            $rootScope.progressbar_complete();
            $scope.uploader.destroy();
            $scope.uploader.queue=[];
            $scope.uploader.clearQueue();
            angular.element("input[type='file']").val(null);
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if(response.total > 0){
                $rootScope.toastrMessages('success',$filter('translate')('reports attachments') + $filter('translate')('Imported')+response.total+$filter('translate')('file')+response.insert+$filter('translate')('new file')+response.update+$filter('translate')('updated file'));
            }else{
                $rootScope.toastrMessages('error',$filter('translate')('not Imported') +":"+ $filter('translate')('total') +":" +response.total+$filter('translate')('file')+response.insert+$filter('translate')('new file')+response.update+$filter('translate')('updated file'));
            }
            LoadReportsAttachment($scope.filters)
        };
        $scope.prepare = function(selected) {
            $rootScope.clearToastr();
            var params = angular.copy($scope.filters);
            params.action='export';
            
            params.items=[];
        
           if(selected == true || selected == 'true' ){
                    var items=[];
                     angular.forEach($scope.items, function(v, k){                
                         if(v.check){
                             items.push(v.id);
                         }
                     });

                     if(items.length==0){
                         $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                         return ;
                     }
                     params.items=items;
            }
            
            $rootScope.progressbar_start();

            if( !angular.isUndefined(params.all_organization)) {
                if(params.all_organization == true ||params.all_organization == 'true'){
                    var orgs=[];
                    angular.forEach(params.organization_id, function(v, k) {
                        orgs.push(v.id);
                    });

                    params.organization_ids=orgs;
                    delete params.organization_id;

                }else if(params.all_organization == false ||params.all_organization == 'false') {
                    params.organization_ids=[];
                    $scope.master=false
                }
            }

            if( !angular.isUndefined(params.date_from)) {
                params.date_from=$filter('date')(params.date_from, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(params.date_to)) {
                params.date_to=$filter('date')(params.date_to, 'dd-MM-yyyy')
            }
            $http({
                url: "/api/v1.0/sponsorship/reports/attachments/prepare",
                method: "POST",
                data: params
            }).then(function (response) {
                $rootScope.progressbar_complete();
                
                if(response.status == false){
                    $rootScope.toastrMessages('error',$filter('translate')('no attachment to export'));
                }else{
                    window.location = '/api/v1.0/common/cases-documents/export?download_token='+response.data.download_token;
                }
                
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };

        //-----------------------------------------------------------------------------------------------------------
        var today = new Date();
        $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
        $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};
        $scope.popup={0:{},1:{opened:false},2:{opened:false}};
        $scope.open=function (target,$event) {
            $event.preventDefault();
            $event.stopPropagation();
            if(target == 1){ $scope.popup[1].opened = true;  }
            else if(target == 2) { $scope.popup[2].opened = true; }
        };


        $scope.open_image=function (id) {

            $rootScope.id = id ;
            $rootScope.clearToastr();
            $uibModal.open({

                templateUrl: 'app/modules/sponsorship/views/model/open_image.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                animation: true,
                controller: function ($rootScope,$scope, $modalInstance, $log) {
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                }});
        };


        $scope.open_list_image=function (ids) {


            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'app/modules/sponsorship/views/model/open_image_list.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                animation: true,
                controller: function ($rootScope,$scope, $modalInstance, $log) {
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.image_list = ids.split(',');
                    $scope.show_image = function(id){
                        $rootScope.id =parseInt($scope.image_list[id]) ;
                   }
                   $scope.show_image(0);

                   
                   

               }});

        };

        

    });


