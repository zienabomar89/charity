angular.module('SponsorshipModule')
    .controller('NominationCasesController', function ($http,$timeout,$scope,$rootScope,$uibModal ,$state,$stateParams,nominate,$filter,$ngBootbox) {



        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        $state.current.data.pageTitle = $filter('translate')('nomination_list');
        $rootScope.nominationCases=[];
        $scope.itemsCount='50';

        if($stateParams.sponsorship_id==null){
            $rootScope.toastrMessages('error',$filter('translate')('There was a bug during your link request, you will be redirected to the nominations page so that you can search again and request the correct link'));
            $timeout(function() {
                $state.go('nomination-list');
            }, 3000);
        }
        else{
            $scope.linkEnabled = false;

            $scope.sponsorship_id=$stateParams.sponsorship_id;

            nominate.get({'id':$stateParams.sponsorship_id}, function (response) {
                $rootScope.sponsor_name =response.sponsor_name;
                $rootScope.is_mine =response.is_mine;
                $rootScope.sponsorship_id =response.id;
                $rootScope.sponsor_id =response.sponsor_id;
                $rootScope.sponsorship_number =response.sponsorship_number;

            });

            var getNomination=function(id,page,itemsCount){
                $rootScope.clearToastr();
                $rootScope.progressbar_start();
                nominate.persons({'id':id,'page':page,'itemsCount':itemsCount}, function (response) {
                    $rootScope.progressbar_complete();
                    if(response.data.length != 0){
                        $rootScope.result='true';
                        $rootScope.nominationCases =response.data;
                        $rootScope.CurrentPage = response.current_page;
                        $rootScope.TotalItems = response.total;
                        $rootScope.ItemsPerPage = response.per_page;
                        //$scope.linkEnabled = true;
                    }else{
                        $rootScope.result='false';
                        //$scope.linkEnabled = false;
                    }

                });
            };

            getNomination($stateParams.sponsorship_id,1,50);

            $rootScope.pageChanged = function (CurrentPage) {
                getNomination($stateParams.sponsorship_id,CurrentPage,$scope.itemsCount);
            };

            $scope.itemsPerPage_ = function (itemsCount) {
             $scope.CurrentPage = 1 ;
             $scope.itemsCount = itemsCount ;
             getNomination($stateParams.sponsorship_id,$scope.CurrentPage,itemsCount);
            };


            $scope.download=function(id){
                $rootScope.clearToastr();
                $rootScope.progressbar_start();
                $http({
                    url: '/api/v1.0/sponsorship/nominate/export/'+$scope.sponsorship_id,
                    method: "GET"
                }).then(function (response) {
                    $rootScope.progressbar_complete();
                    window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token;
                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });
            };
            $scope.delete = function(size,id){
                $rootScope.clearToastr();
                $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                    .then(function() {
                        $rootScope.progressbar_start();
                        nominate.delete({'id':id}, function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status == 'success')
                            {
                                getNomination($stateParams.sponsorship_id,$rootScope.CurrentPage);
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                        });

                    });

            };
        }
    });

angular.module('SponsorshipModule')
    .controller('SponsorshipsOperationController', function ($http,$filter ,$scope,$rootScope,$uibModal ,$state,$stateParams,$ngBootbox,
                                                             Org,nominate) {


        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        $rootScope.SponsorNomination=false;
        $state.current.data.pageTitle = $filter('translate')('nomination-cases-list');
        $rootScope.filters={"all_organization":false};

        $scope.mode =$stateParams.mode;

        $rootScope.status ='';
        $rootScope.msg ='';
        $scope.items=[];
        $scope.itemsCount='50';
        $scope.CurrentPage = 1 ;
        var getList=function(param){
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            nominate.filterSponsorships( param ,function (response) {
                $rootScope.progressbar_complete();
                $scope.selectedAll=true;
                var temp =response.data;
                angular.forEach(temp, function (v, k) {
                    if (v.nominationCount == 0) {
                        v.linkEnabled=false;
                    }else{
                        v.linkEnabled=true;
                    }
                });

                $rootScope.result='true';
                $scope.items =temp;
                $scope.CurrentPage = response.current_page;
                $rootScope.TotalItems = response.total;
                $rootScope.ItemsPerPage = response.per_page;
            });
        };

        $scope.itemsPerPage_ = function (itemsCount) {
         $scope.itemsCount = itemsCount ;
         if($stateParams.sponsor_id){
             getList({page:1,itemsCount:$scope.itemsCount,sponsor_id:$stateParams.sponsor_id});
         }else {
             getList({page:1,itemsCount:$scope.itemsCount});
         }
     };

        if($stateParams.sponsor_id){
            $rootScope.sponsor_id=$stateParams.sponsor_id;
            $rootScope.SponsorNomination=true;
            getList({page:1,sponsor_id:$stateParams.sponsor_id,itemsCount :$scope.itemsCount});
            Org.get({type: 'sponsors', id: $stateParams.sponsor_id}, function(response) {
                $scope.target = response;
                $rootScope.sponsor_name=response.name;
            });
        }else{
            getList({page:1,itemsCount:$scope.itemsCount});
        }

        $scope.pageChanged = function (CurrentPage) {
            $rootScope.filters.page=CurrentPage;
            if($stateParams.sponsor_id){
                getList({page:CurrentPage,sponsor_id:$stateParams.sponsor_id,itemsCount:$scope.itemsCount});
                $rootScope.filters.sponsor_id=$stateParams.sponsor_id;
                $rootScope.SponsorNomination=true;
            }else{
                getList({page:CurrentPage,itemsCount:$scope.itemsCount});
            }
        };

        Org.list({type:'organizations'},function (response) {  $scope.Org = response; });


        Org.list({type:'sponsors'},function (response) { $rootScope.sponsorships = response; });

        $scope.getsponsorshipsList = function(sponsor_id){
            $rootScope.clearToastr();

            if(sponsor_id!=""){
                nominate.list({'id':sponsor_id},function(response){
                    if(response.s!= 0){
                        $rootScope.hassponsorships=true;
                        $rootScope.sponsorshipList = response.list;


                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('No nominations'));
                        $rootScope.hassponsorships=false;
                        $rootScope.sponsorshipList = [];
                    }
                });
            }else{
                $rootScope.hassponsorships=false;
            }

        };

        $scope.custom1 = true;
        $scope.toggleCustom = function() {
            $rootScope.filters={};
            $scope.custom1 = $scope.custom1 === false ? true: false;
            if($scope.custom1 === true){


                if($stateParams.sponsor_id){
                    $parameter={page:$scope.CurrentPage,sponsor_id:$stateParams.sponsor_id};
                    $rootScope.SponsorNomination=true;
                    Org.get({type: 'sponsors', id: $stateParams.sponsor_id}, function(response) {
                        $scope.target = response;
                        $rootScope.sponsor_name=response.name;
                    });

                }else{
                    $parameter={page:$scope.CurrentPage};
                }




            }
        };

        $scope.download=function(id){
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            $http({
                url: '/api/v1.0/sponsorship/nominate/export/'+id,
                method: "GET"
            }).then(function (response) {
                $rootScope.progressbar_complete();
                window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token;
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };


        $scope.allCheckbox=function(value) {
            if (value) {
                $scope.selectedAll = true;
            } else {
                $scope.selectedAll = false;
            }

            angular.forEach($scope.items, function (v, k) {
                v.check = $scope.selectedAll;
            });
        };

        $scope.updatestatustoAll=function(status) {

            $rootScope.clearToastr();
            sponsorships = [];
            angular.forEach($scope.items, function (v, k) {
                if (v.check) {
                    sponsorships.push(v.id);
                }
            });


            if(sponsorships.length==0){
                $rootScope.toastrMessages('error',$filter('translate')('not specific any sponsorship to update it'));
            }else{
                $rootScope.progressbar_start();
                nominate.updateStatus({id:0, status:status, sponsorship:sponsorships},function (response) {
                    $rootScope.progressbar_complete();
                    $rootScope.mode =' ';

                    if(response.status=='error' || response.status=='failed')
                    {
                        $rootScope.toastrMessages('error',response.msg);
                    }
                    else if(response.status=='failed_some')
                    {
                        $rootScope.toastrMessages('error',response.msg);
                        if($stateParams.sponsor_id){
                            $rootScope.SponsorNomination=true;
                            getList({page:1,sponsor_id:$rootScope.sponsor_id,itemsCount:$scope.itemsCount});
                        }else{
                            getList({page:1,itemsCount:$scope.itemsCount});
                        }
                    }
                    else{
                        $rootScope.All =false;
                        if(response.status=='success')
                        {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            if($stateParams.sponsor_id){
                                $rootScope.SponsorNomination=true;
                                getList({page:1,sponsor_id:$rootScope.sponsor_id,itemsCount:$scope.itemsCount});
                            }else{
                                getList({page:1,itemsCount:$scope.itemsCount});
                            }

                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    }
                }, function(error) {
                    $rootScope.toastrMessages('error',error.data.msg);
                });

            }

        };


        $scope.Search=function(params,action){
            $rootScope.clearToastr();
            var data = angular.copy(params);

            if( !angular.isUndefined(data.all_organization)) {
                if(data.all_organization == true ||data.all_organization == 'true'){
                    var orgs=[];
                    angular.forEach(data.organization_id, function(v, k) {
                        orgs.push(v.id);
                    });

                    data.organization_ids=orgs;
                }else if(data.all_organization == false ||data.all_organization == 'false') {
                    data.organization_ids=[];
                }

                delete data.organization_id;
            }

            if( !angular.isUndefined(data.end_sponsorship_date)) {
                data.end_sponsorship_date=$filter('date')(data.end_sponsorship_date, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(data.start_sponsorship_date)) {
                data.start_sponsorship_date=$filter('date')(data.start_sponsorship_date, 'dd-MM-yyyy')
            }
            data.page=1;
            data.action=action;
            data.itemsCount = $scope.itemsCount;
            if($stateParams.sponsor_id){
                $rootScope.SponsorNomination=true;
                data.sponsor_id=$rootScope.sponsor_id;
            }else{
                getList({page:1,itemsCount:$scope.itemsCount});
            }
            getList(data);

        };

        $scope.dateOptions = {
            formatYear: 'yy',
            startingDay: 0
        };
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {
            $scope.dt = new Date();
        };
        $scope.today();
        $scope.clear = function() {
            $scope.dt = null;
        };

        $scope.open11 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup11.opened = true;
        };

        $scope.popup11 = {
            opened: false
        };

        $scope.open12 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup12.opened = true;
        };

        $scope.popup12 = {
            opened: false
        };



    });

