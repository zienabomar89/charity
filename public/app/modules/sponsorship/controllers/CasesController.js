angular.module('SponsorshipModule')
    .controller('sponsorshipCasesController', function($filter,$stateParams,$rootScope, $scope,$uibModal, $http, $timeout,$state,setting,Org,category,Entity,custom_forms,cases,persons,casesInfo,nominate,FileUploader, OAuthToken,org_sms_service) {

        var AuthUser =localStorage.getItem("charity_LoggedInUser");
        $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
        $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
        if($rootScope.charity_LoggedInUser.organization != null) {
            $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
        }
        $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;

        $rootScope.settings.layout.pageSidebarClosed = true;
        $rootScope.type=$scope.type=$stateParams.type == 'deleted'? 'deleted' : 'all';
        $state.current.data.pageTitle = $stateParams.type == 'deleted'? $filter('translate')('deleted cases') :
                                                                        $filter('translate')('sponsorships cases');

        $rootScope.settings.layout.pageSidebarClosed = true;

        $rootScope.failed =false;
        ;
        $rootScope.model_error_status =false;
        $rootScope.model_error_status2 =false;
        $scope.custom = true;
        $scope.export = '';
        $rootScope.items=[];
        $scope.master=false;
        $rootScope.status ='';
        $rootScope.msg ='';
        $rootScope.DataFilter= {};
        $rootScope.person_restrict=false;
        $rootScope.card_restrict=false;
        $rootScope.restricted=[];
        $rootScope.invalid_cards=[];

        $scope.caseCollapsed =  $scope.personCollapsed =
            $scope.fatherCollapsed= $scope.motherCollapsed = $scope.guardianCollapsed = true;
        $scope.sortKeys={name:false,gender:false,category:false,guardian:false,rank:false,status:false};
        if($rootScope.type == 'deleted'){
            $rootScope.deleted=$scope.deleted=true;
        }else{
            $rootScope.deleted=$scope.deleted=false;
        }


        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        $scope.mode =$stateParams.mode;
        $rootScope.status ='';
        $rootScope.msg ='';
        $rootScope.sponsor={};

        $scope.collapsed = function(key){
            if(key =='case'){
                $scope.caseCollapsed = $scope.caseCollapsed == false ? true: false;
            }else if(key =='father'){
                $scope.fatherCollapsed = $scope.fatherCollapsed == false ? true: false;
            }else if(key =='mother'){
                $scope.motherCollapsed = $scope.motherCollapsed == false ? true: false;
            }else if(key =='guardian'){
                $scope.guardianCollapsed = $scope.guardianCollapsed == false ? true: false;
            }
        };
        $scope.resetFilters=function(){
            $rootScope.DataFilter={"all_organization":false};
        };
        $scope.closeRestricted=function(){
            if( $rootScope.person_restrict == true){
                $rootScope.person_restrict =false;
            }
        };

        $scope.changeCategory=function(size,case_id,category_id){

            $rootScope.row={count:1,id:case_id,pre_category_id:category_id,category_id:category_id};
            $uibModal.open({
                templateUrl: 'changeCategory2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope, $modalInstance, $log) {

                    $scope.confirm = function (params) {
                        $rootScope.clearToastr();
                        if(params.pre_category_id == params.category_id){
                            $rootScope.toastrMessages('error',$filter('translate')("You do not change the status label"));
                        }else{
                            params.type='sponsorships';
                            $rootScope.progressbar_start();
                            cases.changeCategory(params,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else{
                                    $modalInstance.close();
                                    if(response.status=='success')
                                    {
                                        $rootScope.DataFilter.page=$rootScope.CurrentPage;
                                        getCases($rootScope.DataFilter);
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();
                            });
                        }
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };


                }});
        };

        $scope.sponsor_list=function(case_id,status){

            cases.getSponsorList({case_id:case_id,status:status,type:'sponsorships'},function (response) {
                $rootScope.sponsorList = response.items;
            });
            $uibModal.open({
                templateUrl: '/app/modules/sponsorship/views/cases/model/sponsor_list.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log) {

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                }});
        };


        $scope.sendSms=function(type,receipt_id) {

            if(type == 'manual'){
                var receipt = [];
                var pass = true;

                if(receipt_id ==0){
                    angular.forEach($rootScope.items, function (v, k) {
                        if (v.caseCheck) {
                            receipt.push(v.person_id);
                        }
                    });

                    if(receipt.length==0){
                        pass = false;
                        $rootScope.toastrMessages('error',$filter('translate')("you did not select anyone to send them sms message"));
                        return;
                    }
                }else{
                    receipt.push(receipt_id);
                }

                if(pass){
                    org_sms_service.getList(function (response) {
                        $rootScope.Providers = response.Service;
                    });
                    $uibModal.open({
                        templateUrl: '/app/modules/sms/views/modal/sent_sms_from_person_general.html',
                        backdrop  : 'static',
                        keyboard  : false,
                        windowClass: 'modal',
                        size: 'lg',
                        controller: function ($rootScope,$scope, $modalInstance) {
                            $scope.sms={source: 'check' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:"",cont_type:""};
                            $scope.confirm = function (data) {
                                $rootScope.clearToastr();
                                var params = angular.copy(data);
                                params.receipt=receipt;
                                params.target='person';

                                $rootScope.progressbar_start();
                                org_sms_service.sendSmsToTarget(params,function (response) {
                                    $rootScope.progressbar_complete();
                                    if(response.status=='error' || response.status=='failed') {
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    else if(response.status== 'success'){
                                        if(response.download_token){
                                            var file_type='xlsx';
                                              window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                        }

                                        $modalInstance.close();
                                        $rootScope.toastrMessages('success',response.msg);
                                    }
                                }, function(error) {
                                    $rootScope.progressbar_complete();
                                    $rootScope.toastrMessages('error',error.data.msg);
                                });
                            };


                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };

                        },
                        resolve: {
                        }
                    });

                }
            }
            else{
                org_sms_service.getList(function (response) {
                    $rootScope.Providers = response.Service;
                });
                $uibModal.open({
                    templateUrl: '/app/modules/sms/views/modal/sent_sms_from_xlx_general.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                        $scope.upload=false;
                        $scope.sms={source: 'file' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:"",cont_type:""};

                        $scope.RestUpload=function(value){
                            if(value ==2){
                                $scope.upload=false;
                            }
                        };

                        $scope.fileUpload=function(){
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.upload=true;
                        };
                        $scope.download=function(){
                              window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-to-send-sms-template&template=true';
                        };

                        var Uploader = $scope.uploader = new FileUploader({
                            url: 'api/v1.0/sms/excel',
                            removeAfterUpload: false,
                            queueLimit: 1,
                            headers: {
                                Authorization: OAuthToken.getAuthorizationHeader()
                            }
                        });


                        $scope.confirm = function (item) {
                            $scope.spinner=false;
                            $scope.import=false;
                            $rootScope.progressbar_start();
                            Uploader.onBeforeUploadItem = function(item) {

                                if($scope.sms.same_msg == 0){
                                    $scope.sms.sms_messege = '';
                                }
                                item.formData = [$scope.sms];
                            };
                            Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                                $rootScope.progressbar_complete();
                                $scope.uploader.destroy();
                                $scope.uploader.queue=[];
                                $scope.uploader.clearQueue();
                                angular.element("input[type='file']").val(null);

                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                    $scope.upload=false;
                                    angular.element("input[type='file']").val(null);
                                }
                                else if(response.status=='success')
                                {
                                    if(response.download_token){
                                        var file_type='xlsx';
                                          window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                    }
                                    $scope.spinner=false;
                                    $scope.upload=false;
                                    $scope.import=false;
                                    $rootScope.toastrMessages('success',response.msg);
                                    $modalInstance.close();
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                    $scope.upload=false;
                                    angular.element("input[type='file']").val(null);
                                }

                            };
                            Uploader.uploadAll();
                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                        $scope.remove=function(){
                            angular.element("input[type='file']").val(null);
                            $scope.uploader.clearQueue();
                            angular.forEach(
                                angular.element("input[type='file']"),
                                function(inputElem) {
                                    angular.element(inputElem).val(null);
                                });
                        };

                    }});

            }
        };


        $scope.changeCategoryToGroup=function(size){
            $rootScope.clearToastr();
            var persons=[];
            angular.forEach($rootScope.items, function(v, k) {
                if(v.caseCheck){
                    persons.push({'case_id':v.case_id,'category_id':v.category_id});
                }
            });

            if(persons.length==0){
                $rootScope.toastrMessages('error',$filter('translate')('All the cases you selected have the same type of help specified'));
            }else{
                $rootScope.row={category_id:""};
                $uibModal.open({
                    templateUrl: 'changeCategory2.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: size,
                    controller: function ($rootScope,$scope, $modalInstance, $log) {

                        $rootScope.msg='';
                        $scope.confirm = function (params) {

                            $rootScope.clearToastr();
                            var FinalCases=[];
                            angular.forEach(persons, function(v, k) {
                                if(v.category_id != params.category_id){
                                    FinalCases.push(v.case_id);
                                }
                            });

                            if(FinalCases.length ==0){
                                $rootScope.toastrMessages('error',$filter('translate')('All the cases you selected have the same type of help specified'));
                            }else {
                                params.cases =FinalCases;
                                params.id = params.category_id;
                                params.restricted = persons.length - FinalCases.length;
                                params.type='sponsorships';
                                $rootScope.progressbar_start();
                                cases.changeCategoryToGroup(params,function (response) {
                                    $rootScope.progressbar_complete();
                                    $modalInstance.close();
                                    if(response.status=='success')
                                    {
                                        $rootScope.DataFilter.page=$rootScope.CurrentPage;
                                        getCases($rootScope.DataFilter);
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                }, function(error) {
                                    $rootScope.progressbar_complete();
                                    $rootScope.toastrMessages('error',error.data.msg);
                                    $modalInstance.close();
                                });
                            }


                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };


                    }});

            }



        };


        var getCases =function(params){

            // $rootScope.clearToastr();
            params.deleted=$rootScope.deleted;
            params.category_type=1;
            if(angular.isUndefined(params.action)){
                params.action ='filter';
            }
            if(!angular.isUndefined(params.sponsor)){
                var sponsor =[];
                angular.forEach(params.sponsor, function(v, k) {
                    sponsor.push(v.id);
                });

                params.sponsor=sponsor;
            }

            if(params.action && angular.isUndefined(params.page) ){
                params.page =1;
            }

            if( !angular.isUndefined(params.all_organization)) {
                if(params.all_organization == true ||params.all_organization == 'true'){
                    var orgs=[];
                    angular.forEach(params.organization_id, function(v, k) {
                        orgs.push(v.id);
                    });

                    params.organization_ids=orgs;

                }else if(params.all_organization == false ||params.all_organization == 'false') {
                    params.organization_ids=[];
                    $scope.master=false
                }
            }

            if($rootScope.type == 'deleted'){
                if( !angular.isUndefined(params.deleted_to)) {
                    params.deleted_to=$filter('date')(params.deleted_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.deleted_from)) {
                    params.deleted_from= $filter('date')(params.deleted_from, 'dd-MM-yyyy')
                }

            }
            else{
                if( !angular.isUndefined(params.birthday_to)) {
                    params.birthday_to=$filter('date')(params.birthday_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.birthday_from)) {
                    params.birthday_from= $filter('date')(params.birthday_from, 'dd-MM-yyyy')
                }
           
                 if( !angular.isUndefined(params.date_to)) {
                    params.date_to=$filter('date')(params.date_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.date_from)) {
                    params.date_from= $filter('date')(params.date_from, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(params.father_birthday_to)) {
                    params.father_birthday_to=$filter('date')(params.father_birthday_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.father_birthday_from)) {
                    params.father_birthday_from= $filter('date')(params.father_birthday_from, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(params.father_death_date_to)) {
                    params.father_death_date_to=$filter('date')(params.father_death_date_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.father_death_date_from)) {
                    params.father_death_date_from= $filter('date')(params.father_death_date_from, 'dd-MM-yyyy')
                }


                if( !angular.isUndefined(params.mother_birthday_to)) {
                    params.mother_birthday_to=$filter('date')(params.mother_birthday_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.mother_birthday_from)) {
                    params.mother_birthday_from= $filter('date')(params.mother_birthday_from, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(params.mother_death_date_to)) {
                    params.mother_death_date_to=$filter('date')(params.mother_death_date_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.mother_death_date_from)) {
                    params.mother_death_date_from= $filter('date')(params.mother_death_date_from, 'dd-MM-yyyy')
                }

            }

            $rootScope.progressbar_start();
            $http({
                url: "/api/v1.0/common/sponsorships/cases/filterCases",
                method: "POST",
                data: params
            })
                .then(function (response) {

                    $rootScope.progressbar_complete();
                    if(params.action != 'filter'){
                        if(response.data.status == 'false'){
                            $rootScope.toastrMessages('error',$filter('translate')('there is no cases to export'));
                        }else if(params.action == 'FamilyMember' && response.data.status == false){
                            $rootScope.toastrMessages('error',$filter('translate')('there is no family member to export'));
                        }else{
                            var file_type='zip';
                            if(params.action == 'ExportToExcel' || params.action == 'Basic' || params.action == 'FamilyMember') {
                                file_type='xlsx';
                            }
                              window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                        }
                    }else{

                        if(params.all_organization == true ||params.all_organization == 'true'){
                            $scope.master=true
                        }

                        if(response.data.Cases.data.length != 0){
                            $rootScope.result='true';
                            $scope.master=response.data.master;
//         $scope.export = 'done';
                            $scope.organization_id=response.data.organization_id;
                            var temp= response.data.Cases.data;

                            if($rootScope.type != 'deleted'){
                                angular.forEach(temp, function(v, k) {
                                    if($rootScope.type=='all'){
                                        v.rank=parseInt(v.rank);
                                    }
                                    v.organization_id=parseInt(v.organization_id);
                                });
                            }

                            $rootScope.items= temp;
                            $rootScope.CurrentPage = response.data.Cases.current_page;
                            $scope.TotalItems = response.data.Cases.total;
                            $scope.ItemsPerPage = response.data.Cases.per_page;
                        }else{
                            //         $scope.export = '';
                            $rootScope.result='false';
                            $rootScope.items=[];
                        }
                    }

                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });
        };


        if($rootScope.type == 'deleted'){
            getCases({page:1,'action':'filter','order_by_rank':1,'all_organization':false});
        }


        $scope.resetFilters();

        if($rootScope.type == 'all'){
            var entities='deathCauses,furnitureStatus,houseStatus,propertyTypes,roofMaterials,educationAuthorities,educationStages,diseases,maritalStatus,deathCauses,countries,workJobs';
            Entity.get({entity:'entities',c:entities},function (response) {
                $scope.DeathCauses = response.deathCauses;
                $scope.furnitureStatus = response.furnitureStatus;
                $scope.houseStatus = response.houseStatus;
                $scope.PropertyTypes = response.propertyTypes;
                $scope.RoofMaterials = response.roofMaterials;
                $scope.EduAuthorities = response.educationAuthorities;
                $scope.EduStages = response.educationStages;
                $scope.Diseases = response.diseases;
                $scope.MaritalStatus = response.maritalStatus;
                $scope.DeathCauses = response.deathCauses;
                $scope.Country = response.countries;
                $scope.WorkJob = response.workJobs;
            });
            Org.list({type:'sponsors'},function (response) { $rootScope.Sponsor = response; });
        }

        Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

        //-----------------------------------------------------------------------------------------------------------
        $rootScope.selectCustomForm = function (size) {

            $rootScope.clearToastr();
            $rootScope.Forms =  custom_forms.getCustomFormsList();

            $rootScope.formdata2=[];
            setting.getCustomForm({'id':'sponsorship_custom_form'},function (response) {
                $rootScope.has_custom_form=response.status;
                if(response.status){
                    $rootScope.formdata2.form_id=response.Setting.value +"";
                }
            });

            $uibModal.open({
                templateUrl: 'myModalContent.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope, $modalInstance, $log,setting) {

                    $scope.model_error =false;
                    $scope.confirm = function (item) {
                        $rootScope.clearToastr();
                        $rootScope.progressbar_start();
                        setting.setCustomForm({form_id:item['form_id'],setting_id:'sponsorship_custom_form'},function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                $modalInstance.close();
                                if(response.status=='success')
                                {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }

                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                            $modalInstance.close();
                        });


                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                },
                resolve: {

                }
            });
        };
        $scope.searchCases = function (selected ,params,action) {
            $rootScope.clearToastr();
            $scope.sortKeys={};
            if( !angular.isUndefined($rootScope.DataFilter.sortKeys)) {
                delete $rootScope.DataFilter.sortKeys;
            }

            var filters = angular.copy(params);
            filters.person = [];
            if(selected == true || selected == 'true' ){
               var persons=[];
                angular.forEach($rootScope.items, function(v, k){

                    if(v.caseCheck){
                        persons.push(v.case_id);
                    }
                });

                if(persons.length==0){
                    $rootScope.toastrMessages('error',$filter('translate')('You have not select people to export'));
                    return ;
                }
                filters.persons=persons;
            }
            filters.action=action;
            getCases(filters);
        };

        $scope.sortKeyArr=[];
        $scope.sortKeyArr_rev=[];
        $scope.sortKeyArr_un_rev=[];
        $scope.sort_ = function(keyname){
            $scope.sortKey_ = keyname;
            var reverse_ = false;
            if (
                ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
                ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
            ) {
                reverse_ = true;
            }
            var data = angular.copy($rootScope.DataFilter);
            data.action ='filters';

            if (reverse_) {
                if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                    $scope.sortKeyArr_rev=[];
                    $scope.sortKeyArr_un_rev=[];
                    $scope.sortKeyArr_rev.push(keyname);
                }
            }
            else {
                if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                    $scope.sortKeyArr_rev=[];
                    $scope.sortKeyArr_un_rev=[];
                    $scope.sortKeyArr_un_rev.push(keyname);
                }

            }

            data.sortKeyArr_rev = $scope.sortKeyArr_rev;
            data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;
            $scope.searchCases(data,'filter');
        };

        $rootScope.pageChanged = function (CurrentPage) {
            // $scope.sortKeys={};
            // if( !angular.isUndefined($rootScope.DataFilter.sortKeys)) {
            //     delete $rootScope.DataFilter.sortKeys;
            // }n

            $rootScope.DataFilter.page=CurrentPage;
            $rootScope.DataFilter.action='filter';

            if($stateParams.id){
                $rootScope.DataFilter.sponsor_id=$stateParams.id;
            }
            getCases($rootScope.DataFilter);

        };

         $rootScope.itemsPerPage_ = function (itemsCount) {
            $rootScope.DataFilter.itemsCount = itemsCount ;
            $rootScope.CurrentPage = 1 ;
            $rootScope.DataFilter.page = 1;
            getCases($rootScope.DataFilter);
        };

        $rootScope.close=function(){

            $rootScope.failed =false;
            ;
            $rootScope.model_error_status =false;
            $rootScope.model_error_status2 =false;
        };
        $scope.get = function(_person,target,value,parant){

            if(value != null && value != "" && value != " " ) {

                Entity.listChildren({entity:target,'id':value,'parent':parant},function (response) {

                    if(target =='branches'){
                        $scope.Branches = response;
                        $rootScope.DataFilter.branch_name="";
                    }else{
                        if(_person =='case'){
                            if(target =='districts'){
                                $scope.governarate = response;
                                $scope.mosques = [];
                                $scope.city = [];
                                $scope.nearlocation =[];
                                $rootScope.DataFilter.governarate="";
                            }
                            else if(target =='cities'){
                                $scope.city = response;
                                $scope.mosques = [];
                                $scope.nearlocation =[];
                                $rootScope.DataFilter.city="";
                            }
                            else if(target =='neighborhoods'){
                                $scope.nearlocation = response;
                                $scope.mosques = [];
                                $rootScope.DataFilter.location_id="";
                            }else if(target =='mosques'){
                                $scope.mosques = response;
                                $rootScope.DataFilter.mosques_id="";
                            }
                        }else{
                            if(target =='districts'){
                                $scope.guardian_governarate = response;
                                $scope.guardian_city = [];
                                $scope.guardian_mosques = [];
                                $scope.guardian_nearlocation =[];
                                $rootScope.DataFilter.guardian_governarate="";
                            }else if(target =='cities'){
                                $scope.guardian_city = response;
                                $scope.guardian_mosques = [];
                                $scope.guardian_nearlocation =[];
                                $rootScope.DataFilter.guardian_city="";
                            }else if(target =='neighborhoods'){
                                $scope.guardian_nearlocation = response;
                                $scope.guardian_nearlocation =[];
                                $rootScope.DataFilter.guardian_location_id="";
                            }else if(target =='mosques'){
                                $scope.mosques = response;
                                $rootScope.DataFilter.guardian_mosques_id="";
                            }
                        }

                    }


                });
            }


        };
        $scope.visitorNote=function(id){

            $rootScope.Notes='';
            $rootScope.clearToastr();

            $rootScope.progressbar_start();
            cases.getCaseReports({'action':'filters','target':'info','case_id':id,'visitor_note':true, 'mode':'show'},function(response) {
                $rootScope.progressbar_complete();
                if(response.visitor_note_status==false)
                {
                    $rootScope.toastrMessages('error',$filter('translate')('You have not select any case to filtering'));
                }else{
                    $rootScope.Notes = response.visitor_note;
                    $uibModal.open({
                        templateUrl: 'VisitorNote.html',
                        backdrop  : 'static',
                        keyboard  : false,
                        windowClass: 'modal',
                        size: 'lg',
                        controller: function ($rootScope,$scope, $modalInstance, $log) {
                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };

                        },
                        resolve: {
                        }
                    });
                }
            });


        };
        $scope.exportRestricted = function (restricted) {
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            $http({
                url: "/api/v1.0/common/cases/exportRestricted",
                method: "POST",
                data: {exported:restricted}
            }).then(function (response) {
                $rootScope.progressbar_complete();
                  window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token;

            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };
        $scope.download = function (target,id) {

            $rootScope.clearToastr();
            var url ='';
            var file_type ='zip';
            var  params={};
            var method='';

            if(target =='word'){
                method='POST';
                params={'case_id':id,
                    'target':'info',
                    'action':'filters',
                    'mode':'show',
                    'category_type' : 1,
                    'person' :true,
                    'persons_i18n' : true,
                    'category' : true,
                    'work' : true,
                    'contacts' : true,
                    'health' : true,
                    'default_bank' : true,
                    'education' : true,
                    'islamic' : true,
                    'banks' : true,
                    'residence' : true,
                    'persons_documents' : true,
                    'family_member' : true,
                    'father_id' : true,
                    'mother_id' : true,
                    'guardian_id' : true,
                    'guardian_kinship' : true,
                    'parent_detail':true,
                    'guardian_detail':true,
                    'visitor_note':true
                };
                url = "/api/v1.0/common/sponsorships/cases/word";
            }
            else if(target =='pdf'){
                method='POST';
                params={'case_id':id,
                    'target':'info',
                    'action':'filters',
                    'mode':'show',
                    'category_type' : 1,
                    'person' :true,
                    'persons_i18n' : true,
                    'category' : true,
                    'work' : true,
                    'contacts' : true,
                    'health' : true,
                    'default_bank' : true,
                    'education' : true,
                    'islamic' : true,
                    'banks' : true,
                    'residence' : true,
                    'persons_documents' : true,
                    'family_member' : true,
                    'father_id' : true,
                    'mother_id' : true,
                    'guardian_id' : true,
                    'guardian_kinship' : true,
                    'parent_detail':true,
                    'guardian_detail':true,
                    'visitor_note':true
                };
                url = "/api/v1.0/common/sponsorships/cases/pdf";
            }
            else{
                method='GET';
                url = "/api/v1.0/common/sponsorships/cases/exportCaseDocument/"+id;
            }

            $rootScope.progressbar_start();
            $http({
                url:url,
                method: method,
                data:params
            }).then(function (response) {

                $rootScope.progressbar_complete();
                if(target !='word' && response.data.status == false){
                    $rootScope.toastrMessages('error',$filter('translate')('no attachment to export'));
                }else{
                      window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }

            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };
        $scope.selectedAll = function(value){
            if(value){
                $scope.selectAll = true;
            }
            else{
                $scope.selectAll =false;

            }
            angular.forEach($rootScope.items, function(val,key)
            {
                val.caseCheck=$scope.selectAll;
            });
        };
        $scope.nominationToaSponsor = function(size,id) {

            $rootScope.clearToastr();

            var selected = [];
            angular.forEach($rootScope.items, function(val,key){
                if(val.caseCheck){
                    selected.push( {'id':val.person_id,'name':val.full_name,'case_id':val.case_id,'l_person_id':val.l_person_id,'category_id':val.category_id,'father_id':val.father_id,'id_card_number':val.id_card_number});
                }
            });

            if(selected.length==0){
                $rootScope.toastrMessages('error',$filter('translate')('You have not select any case to filtering'));
            }else {

                nominate.list({'id':-1},function(response){
                    if(response.s!= 0){
                        $rootScope.sponsorshipList2= response.list;
                    }else{
                        $rootScope.sponsorshipList2 = [];
                    }

                });

                $uibModal.open({

                    size: size,
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    templateUrl: 'myModalContent1.html',
                    controller: function ($rootScope,$scope,$modalInstance) {
                        $scope.msg1=[];
                        $rootScope.sponsor={};
                        $scope.getsponsorshipsList = function(sponsor_id){
                            $rootScope.clearToastr();

                            if( !angular.isUndefined($scope.msg1.sponsor_id)) {
                                $scope.msg1.sponsor_id = [];
                            }
                            nominate.list({'id':sponsor_id},function(response){
                                if(response.s!= 0){
                                    $rootScope.sponsorshipList = response.list;
                                }else{
                                    $rootScope.sponsorshipList = [];
                                }
                            });

                        };




                        $scope.add = function (row) {

                            $rootScope.clearToastr();
                            var params = angular.copy(row);
                            params.sponsorship_cases=selected;

                            if(params.sponsor_id){
                                var sponsors = [];
                                angular.forEach(params.sponsor_id, function(v, k) {
                                    sponsors.push(v.id);
                                });
                                params.sponsor_id =  sponsors
                            }

                            $rootScope.progressbar_start();
                            var new_sponsor = new casesInfo(params);
                            new_sponsor.$save().then(function (response) {

                                $rootScope.progressbar_complete();
                                if(response.result){
                                    if(response.result.length !=0){
                                        $rootScope.person_restrict=true;
                                        $rootScope.restricted=response.result;
                                    }
                                }


                                if(response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                    $modalInstance.close();
                                }
                                else if(response.status=='blocked')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    $modalInstance.close();
                                    if(response.status=='success')
                                    {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }


                                }
                            }, function (error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                            });




                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    }});


            }

        };
        $scope.toggleCustom = function() {
            $scope.custom = $scope.custom === false ? true: false;
            $rootScope.DataFilter={"all_organization":false};
            $scope.export = '';
        };
        $scope.updateStatus= function($case_id,status,card){

            if($rootScope.check_id(card)){
                $rootScope.clearToastr();
            $uibModal.open({

                size: 'sm',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                templateUrl: 'detail.html',
                controller: function ($rootScope,$scope,$modalInstance) {

                    $scope.confirm = function (params) {
                        $rootScope.clearToastr();
                        if( !angular.isUndefined(params.date)) {
                            params.date=$filter('date')(params.date, 'dd-MM-yyyy')
                        }

                        params.id=$case_id;
                        params.status=status;
                        params.type='sponsorships';
                        $rootScope.progressbar_start();
                        cases.update(params,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                $modalInstance.close();
                                if(response.status=='success') {
                                    // $rootScope.items[index].status= params.status;
                                    $rootScope.DataeFilter.page=$rootScope.CurrentPage;
                                    getCases($rootScope.DataFilter);
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }

                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                        });

                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }});

        }else{
            $rootScope.toastrMessages('error',$filter('translate')("You cannot modify the case because the ID number is incorrect"));

        }
        };
        $scope.statusLog=function(id){
            $rootScope.clearToastr();
            $uibModal.open({
                size: 'lg',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                templateUrl: 'statusLog.html',
                controller: function ($rootScope,$scope,$modalInstance) {

                    loadStatusLog = function(params) {
                        $rootScope.clearToastr();
                        params.type='sponsorships';
                        $rootScope.progressbar_start();
                        cases.getStatusLogs(params ,function (response) {
                            $rootScope.progressbar_complete();
                            $rootScope.items= response.data;
                            $scope.CurrentPage = response.current_page;
                            $scope.TotalItems = response.total;
                            $scope.ItemsPerPage = response.per_page;
                        });
                    };

                    $scope.statusPageChanged = function (CurrentPage) {
                        loadStatusLog({page:CurrentPage,id:id});
                    };

                    loadStatusLog({page:1,id:id});
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }});

        };
        $scope.restore = function (size,id) {
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'restore.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope, $modalInstance, $log) {
                    $scope.confirm = function () {
                        $rootScope.clearToastr();
                        $rootScope.progressbar_start();
                        cases.restore({id:id,type:'sponsorships'},function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status== 'success'){
                                $modalInstance.close();
                                $rootScope.DataFilter.page=$rootScope.CurrentPage;
                                getCases($rootScope.DataFilter);
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            }

                        }, function(error) {
                            $rootScope.toastrMessages('error',error.data.msg);
                            $modalInstance.close();
                        });
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };
        $scope.delete = function (size,id) {
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'delete.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope, $modalInstance, $log) {

                    $scope.data={};
                    $scope.confirmDelete = function (data) {
                        $rootScope.clearToastr();
                        data.id=id;
                        data.type='sponsorships';
                        $rootScope.progressbar_start();
                        cases.delete(data,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status== 'success'){
                                $modalInstance.close();
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                $rootScope.DataFilter.page=$rootScope.CurrentPage;
                                getCases($rootScope.DataFilter);
                            }

                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                            $modalInstance.close();
                        });
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

        $scope.update=function(type){

            $rootScope.clearToastr();
            if(type == 'xlx')
                $uibModal.open({
                    templateUrl: '/app/modules/aid/views/cases/updateData.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'md',
                    controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                        $scope.upload=false;
                        $scope.row={category_id:"",status:""};

                        $scope.RestUpload=function(value){
                            if(value ==2){
                                $scope.upload=false;
                            }
                        };


                        $scope.fileUpload=function(){
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.upload=true;
                        };
                        $scope.download=function(){
                              window.location = '/api/v1.0/common/files/xlsx/export?download_token=card-check-template&template=true';
                        };

                        var Uploader = $scope.uploader = new FileUploader({
                            url: '/api/v1.0/common/sponsorships/cases/goverment_update',
                            removeAfterUpload: false,
                            queueLimit: 1,
                            headers: {
                                Authorization: OAuthToken.getAuthorizationHeader()
                            }
                        });

                        Uploader.onBeforeUploadItem = function(item) {
                            item.formData = [{source: 'file'}];
                        };

                        $scope.confirm = function (item) {
                            // angular.element('.btn').addClass("disabled");

                            $scope.spinner=false;
                            $scope.import=false;

                            $rootScope.progressbar_start();
                            Uploader.onBeforeUploadItem = function(i) {
                                i.formData = [{source:'file'}];
                            };
                            Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                                $rootScope.progressbar_complete();
                                $scope.uploader.destroy();
                                $scope.uploader.queue=[];
                                $scope.uploader.clearQueue();
                                angular.element("input[type='file']").val(null);
                                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                                if(response.download_token){
                                      window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token;
                                }

                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                    $scope.upload=false;
                                    angular.element("input[type='file']").val(null);
                                }
                                else if(response.status=='success')
                                {
                                    $scope.spinner=false;
                                    $scope.upload=false;
                                    angular.element("input[type='file']").val(null);
                                    $scope.import=false;
                                    $rootScope.toastrMessages('success',response.msg);
                                    $rootScope.DataFilter.page=$scope.CurrentPage;
                                    getCases($rootScope.DataFilter);
                                    $modalInstance.close();
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                    $scope.upload=false;
                                    angular.element("input[type='file']").val(null);
                                    $rootScope.DataFilter.page=$scope.CurrentPage;
                                    getCases($rootScope.DataFilter);
                                    $modalInstance.close();
                                }

                            };
                            Uploader.uploadAll();
                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                        $scope.remove=function(){
                            angular.element("input[type='file']").val(null);
                            $scope.uploader.clearQueue();
                            angular.forEach(
                                angular.element("input[type='file']"),
                                function(inputElem) {
                                    angular.element(inputElem).val(null);
                                });
                        };

                    }});
            else{
                $rootScope.cases=[];
                angular.forEach($rootScope.items, function(v, k) {
                    if(v.check){
                        $rootScope.cases.push(v.id_card_number);
                    }
                });
                if($rootScope.cases.length==0){
                    $rootScope.toastrMessages('error',$filter('translate')('You have not select any case'));
                }else{
                    $rootScope.progressbar_start();
                    cases.goverment_update({'type':'sponsorships' , 'source':'cards' , cases:$rootScope.cases},function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        if(response.download_token){
                              window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token;
                        }

                        if(response.status=='error' || response.status=='failed')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else if(response.status=='success')
                        {
                            $rootScope.toastrMessages('success',response.msg);
                            $rootScope.DataFilter.page=$scope.CurrentPage;
                            getCases($rootScope.DataFilter);
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                            $rootScope.DataFilter.page=$scope.CurrentPage;
                            getCases($rootScope.DataFilter);
                        }

                    });
                }
            }
        };

        $scope.sortBy = function (keyname) {
            $scope.sortKeyis = keyname;
            // $scope.sortKeys={name:false,gender:false,category_name:false,guardian_name:false,rank:false,status:false};
            $scope.sortKeysValue={name:0,gender:0,category_name:0,guardian_name:0,rank:0,status:0};

            if(keyname == 'gender'){
                $scope.sortKeys.gender = !$scope.sortKeys.gender;
                $scope.sortKeysValue.gender = !$scope.sortKeys.gender;
            }else if(keyname == 'name'){
                $scope.sortKeys.name = !$scope.sortKeys.name;
                $scope.sortKeysValue.name = !$scope.sortKeys.name;
            }else if(keyname == 'category_name'){
                $scope.sortKeys.category = !$scope.sortKeys.category;
                $scope.sortKeysValue.category = !$scope.sortKeys.category;
            }else if(keyname == 'guardian_name'){
                $scope.sortKeys.guardian = !$scope.sortKeys.guardian;
                $scope.sortKeysValue.guardian = !$scope.sortKeys.guardian;
            }else if(keyname == 'rank'){
                $scope.sortKeys.rank = !$scope.sortKeys.rank;
                $scope.sortKeysValue.rank = !$scope.sortKeys.rank;
            }else if(keyname == 'status'){
                $scope.sortKeys.status = !$scope.sortKeys.status;
                $scope.sortKeysValue.status = !$scope.sortKeys.status;
            }else if(keyname == 'organization_name'){
                $scope.sortKeys.organization_name = !$scope.sortKeys.organization_name;
                $scope.sortKeysValue.organization_name = !$scope.sortKeys.organization_name;
            }

            $rootScope.DataFilter.page=$rootScope.CurrentPage;
            $rootScope.DataFilter.action='filter';
            $rootScope.DataFilter.keyname=keyname;
            $rootScope.DataFilter.sortKeys=$scope.sortKeys;
            $rootScope.DataFilter.sortKeysValue=$scope.sortKeys;
            if($stateParams.id){
                $rootScope.DataFilter.sponsor_id=$stateParams.id;
            }

            getCases($rootScope.DataFilter);

        };

        $scope.import=function(){
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'importCases.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'md',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {
                    $scope.upload=false;
                    $scope.row={category_id:""};
                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };

                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };
                    $scope.download=function(){

                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                        $http({
                            url:'/api/v1.0/common/sponsorships/cases/import-template',
                            method: "GET"
                        }).then(function (response) {
                            $rootScope.progressbar_complete();
                              window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token+'&template=true';
                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                        });
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/common/sponsorships/cases/import',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.clearToastr();
                        item.formData = [{source: 'file'}];
                    };

                    $scope.confirm = function (item) {
                        angular.element('.btn').addClass("disabled");
                        $rootScope.clearToastr();
                        $scope.spinner=false;
                        $scope.import=false;
                        $rootScope.progressbar_start();

                        Uploader.onBeforeUploadItem = function(i) {
                            $rootScope.clearToastr();
                            i.formData = [{category_id: item.category_id}];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);

                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');

                            if(response.download_token){
                                // $rootScope.card_restrict=true;
                                // $rootScope.invalid_cards=response.invalid_cards;
                                  window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token;
                            }

                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            else if(response.status=='success')
                            {
                                $scope.spinner=false;
                                $scope.import=false;
                                $rootScope.toastrMessages('success',response.msg);
                                $rootScope.DataFilter.page=$rootScope.CurrentPage;
                                getCases($rootScope.DataFilter);
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $modalInstance.close();
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});

        };
        $scope.importFamily=function(){
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'importCasesFamily.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'md',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;
                    $scope.row={};

                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };
                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };
                    $scope.download=function(){
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                        $http({
                            url:'/api/v1.0/common/sponsorships/cases/import-family-template',
                            method: "GET"
                        }).then(function (response) {
                            $rootScope.progressbar_complete();
                              window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token+'&template=true';
                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                        });
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/common/sponsorships/cases/import-family',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });
                    Uploader.onBeforeUploadItem = function(item) {
                        item.formData = [{source: 'file'}];
                    };

                    $scope.confirm = function (item) {
                        $rootScope.clearToastr();
                        $rootScope.progressbar_start();

                        $scope.spinner=false;
                        $scope.import=false;

                        Uploader.onBeforeUploadItem = function(i) {
                            $rootScope.clearToastr();
                            // i.formData = [{category_id: item.category_id}];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            else if(response.status=='success')
                            {
                                $scope.spinner=false;
                                $scope.import=false;
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                $rootScope.DataFilter.page=$rootScope.CurrentPage;
                                getCases($rootScope.DataFilter);
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $modalInstance.close();
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});

        };
        $scope.check = function(){
            $rootScope.clearToastr();

            $uibModal.open({
                templateUrl: '/app/modules/common/views/reports/model/checkPerson.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'md',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {
                    $scope.upload=false;

                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };

                    $scope.download=function(){
                        $rootScope.progressbar_start();
                        angular.element('.btn').addClass("disabled");

                        $rootScope.clearToastr();
                        $http({
                            url:'/api/v1.0/common/reports/template',
                            method: "GET"
                        }).then(function (response) {
                            $rootScope.progressbar_complete();
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                              window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token+'&template=true';
                        }, function(error) {
                            $rootScope.progressbar_complete();
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $rootScope.toastrMessages('error',error.data.msg);
                        });
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url:'/api/v1.0/common/sponsorships/cases/check',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    $scope.confirm = function () {
                        $rootScope.clearToastr();
                        $scope.spinner=false;
                        $scope.import=false;

                        angular.element('.btn').addClass("disabled");

                        $rootScope.progressbar_start();
                        Uploader.onBeforeUploadItem = function(item) {
                            $rootScope.clearToastr();
                            item.formData = [];
                        };

                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);
                            if(response.status=='success')
                            {
                                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                                $scope.spinner=false;
                                $scope.import=false;
                                $rootScope.toastrMessages('success',response.msg);
                            }else{
                                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            if(response.download_token){
                                var file_type='xlsx';
                                  window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                            }
                            $modalInstance.close();

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});
        };

        $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $rootScope.format = $rootScope.formats[0];
        $rootScope.today = function() {$rootScope.dt = new Date();};
        $rootScope.today();
        $rootScope.clear = function() {$rootScope.dt = null;};

        $rootScope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup1.opened = true;};
        $rootScope.open2 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup2.opened = true;};
        $rootScope.open44 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup44.opened = true;};
        $rootScope.open3 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup3.opened = true;};
        $rootScope.open33 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup33.opened = true;};
        $rootScope.open333 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup333.opened = true;};
        $rootScope.open3333 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup3333.opened = true;};
        $rootScope.open11 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup11.opened = true;};
        $rootScope.open111 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup111.opened = true;};
        $rootScope.open1111 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup1111.opened = true;};
        $rootScope.open11111 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup11111.opened = true;};
        $rootScope.open4 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup4.opened = true;};
        $rootScope.open5 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup5.opened = true;};
        $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
        $scope.open88 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup88.opened = true;};

        $rootScope.popup1 = {opened: false};
        $rootScope.popup2 = {opened: false};
        $rootScope.popup33 = {opened: false};
        $rootScope.popup44 = {opened: false};
        $rootScope.popup3 = {opened: false};
        $rootScope.popup333 = {opened: false};
        $rootScope.popup3333 = {opened: false};
        $rootScope.popup11 = {opened: false};
        $rootScope.popup111 = {opened: false};
        $rootScope.popup1111 = {opened: false};
        $rootScope.popup11111 = {opened: false};
        $rootScope.popup4 = {opened: false};
        $rootScope.popup5 = {opened: false};
        $scope.popup99 = {opened: false};
        $scope.popup88 = {opened: false};

        $scope.exportRestrictedCard = function (restricted) {

            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            $http({
                url: "/api/v1.0/common/cases/exportRestrictedCard",
                method: "POST",
                data: {exported:restricted}
            }).then(function (response) {
                $rootScope.progressbar_complete();
                  window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token;

            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };
        $scope.ImportCasesToUpdate=function(type){

            $rootScope.clearToastr();
            if(type == 'xlx')
                $uibModal.open({
                templateUrl: 'ImportCasesToUpdate.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'md',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;

                    $scope.row={category_id:"",status:""};
                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };

                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };
                    $scope.download=function(){
                          window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-to-update-template&template=true';
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/common/sponsorships/cases/importToUpdate',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.clearToastr();
                        item.formData = [{source: 'file'}];
                    };

                    $scope.confirm = function (item) {
                        $rootScope.clearToastr();
                        $rootScope.progressbar_start();
                        $scope.spinner=false;
                        $scope.import=false;

                        Uploader.onBeforeUploadItem = function(i) {
                            $rootScope.clearToastr();
                            i.formData = [{category_id: item.category_id,status:item.status}];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $scope.uploader.destroy();
                            $rootScope.progressbar_complete();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);
                            if(response.invalid_cards){
                                if(response.invalid_cards.length !=0){
                                    $rootScope.card_restrict=true;
                                    $rootScope.invalid_cards=response.invalid_cards;
                                }
                            }

                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            else if(response.status=='success')
                            {
                                $scope.spinner=false;
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $scope.import=false;
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                $rootScope.DataFilter.page=$rootScope.CurrentPage;
                                getCases($rootScope.DataFilter);
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $rootScope.DataFilter.page=$rootScope.CurrentPage;
                                getCases($rootScope.DataFilter);
                                $modalInstance.close();
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});
            else{

                $uibModal.open({
                    templateUrl: 'updateResone.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'md',
                    controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken,cases) {
                        $scope.row={category_id:"",status:"",reason:"",cases:[]};
                        $scope.confirm = function (item) {
                            $scope.row.type = 'sponsorships';

                            $rootScope.cases=[];
                            angular.forEach($rootScope.items, function(v, k) {
                                if(v.check &&
                                    (v.category_id == item.category_id) &&
                                    (v.is_mine == '1' || v.is_mine == 1) && v.status != item.status){
                                    if( (v.status == 0 ) || (  v.status == 1 && v.active_case_count == 0 )){
                                        $rootScope.cases.push(v.case_id);
                                    }
                                }
                            });
                            if($rootScope.cases.length==0){
                                if($scope.row.status == 0){
                                    $rootScope.toastrMessages('error',$filter('translate')('You have not select any case , may be not in your organization or active in other'));

                                }else{
                                    $rootScope.toastrMessages('error',$filter('translate')('You have not select any case_'));
                                }
                                $modalInstance.close();
                            }else{
                                $rootScope.progressbar_start();
                                cases.updateStatusAndCategories($scope.row,function (response) {
                                    $rootScope.progressbar_complete();
                                    if(response.status=='success')
                                    {
                                        getCases($rootScope.DataFilter);
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    $modalInstance.close();
                                });
                            }


                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    }});

            }  
        };
        $scope.closeRestricted=function(){
            if( $rootScope.person_restrict == true){
                $rootScope.person_restrict =false;
            }
            if( $rootScope.card_restrict == true){
                $rootScope.card_restrict =false;
            }
            $rootScope.restricted=[];
            $rootScope.invalid_cards=[];

        };

    });


angular.module('SponsorshipModule')
    .controller('familiesController', function ($filter,$http,$scope,$rootScope,$uibModal,$state,$stateParams,OAuthToken,casesInfo,Org) {

        $rootScope.settings.layout.pageSidebarClosed = true;
        $state.current.data.pageTitle = $filter('translate')('families');

        $rootScope.items=[];
        $rootScope.status ='';
        $rootScope.msg ='';
        $scope.export='false';

        $scope.mode =$stateParams.mode;
        $rootScope.status ='';
        $rootScope.msg ='';

        $rootScope.failed =false;
        $scope.CurrentPage = 1 ;

        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });
        $rootScope.model_error_status =false;

        var resetDataFilter =function(){
            $scope.filter={
                "all_organization":false,
                "organization_ids":[],
                "organization_id":"",
                'father_second_name':"",
                'father_third_name':"",
                'father_last_name':"",
                'mother_first_name':"",
                'mother_second_name':"",
                'mother_third_name':"",
                'mother_last_name':"",
                'guardian_first_name':"",
                'guardian_second_name':"",
                'guardian_third_name':"",
                'guardian_last_name':"",
                'father_id_card_number':"",
                'mother_id_card_number':"",
                'guardian_id_card_number':"",
                'exceed':"",
                'category_id':"",
                'min_children_number':"",
                'max_children_number':"",
                'max_total_amount_of_payments':"",
                'min_total_amount_of_payments':"",
                'min_total_this_year':"",
                'max_total_this_year':"" ,
                'itemsCount':50 ,
            };
        };

        Org.list({type:'organizations'},function (response) {  $scope.Org = response; });


        
        var LoadFamilies =function(params){
            $rootScope.clearToastr();
            casesInfo.getFamilies(params,function (response) {
                if(response.families.data.length != 0){
                    $scope.result='true';
                    $scope.export='true';

                    var temp= response.families.data;
                    var Final=[];
                    angular.forEach(temp, function(v, k) {
                        if(v.father != null || v.guardian !=null || v.mother != null){
                            Final.push(v);
                        }
                    });
                    $rootScope.items= Final;


                    //$rootScope.items= response.families.data;
                    $scope.CurrentPage = response.families.current_page;
                    $rootScope.TotalItems = response.families.total;
                    $rootScope.ItemsPerPage = response.families.per_page;
                }else{
                    $rootScope.result='false';
                    $scope.export='false';
                    $rootScope.items=[];
                }
            });

        };

        LoadFamilies({action:'filters',page:1,all_organization:false});
        resetDataFilter();
        $scope.pageChanged = function (currentPage) {
            $scope.filter.page=currentPage;
            $scope.filter.action='filters';
            LoadFamilies($scope.filter);

        };

        $scope.itemsPerPage_ = function (itemsCount) {
            $scope.filter.itemsCount = itemsCount ;
            $scope.filter.action='filters';
            $scope.filter.page = 1;
            LoadFamilies($scope.filter);
        };

        //-----------------------------------------------------------------------------------------------------------
        $scope.custom = true;
        $scope.toggleCustom = function() {
            $rootScope.clearToastr();
            $scope.custom = $scope.custom === false ? true: false;

            if($scope.custom === true) {
                resetDataFilter();
                //LoadFamilies({action:'filters',page:1});
            }else{
                $scope.export='false';
                resetDataFilter();
            }
        };
        $scope.selectedAll = function(value){
            if(value){
                $scope.selectAll = true;
            }
            else{
                $scope.selectAll =false;
            }
            angular.forEach($rootScope.items, function(val,key) {
                val.check=$scope.selectAll;
            });
        };

        $scope.Search = function(selected , data,action){

            $rootScope.clearToastr();
            if( !angular.isUndefined(data.all_organization)) {
                if(data.all_organization == true ||data.all_organization == 'true'){
                    var orgs=[];
                    angular.forEach(data.organization_id, function(v, k) {
                        orgs.push(v.id);
                    });

                    data.organization_ids=orgs;

                }else if(data.all_organization == false ||data.all_organization == 'false') {
                    data.organization_ids=[];
                    $scope.master=false
                }
            }
            data.items=[];
            
             if(selected == true || selected == 'true' ){
               var persons=[];
                angular.forEach($rootScope.items, function(v, k){

                    if(v.check){
                        persons.push(v.id);
                    }
                });

                if(persons.length==0){
                    $rootScope.toastrMessages('error',$filter('translate')('You have not select records to export'));
                    return ;
                }
                data.persons=persons;
            }
            
            data.action=action;
            if(action == 'export'){
                $http({
                    url: "/api/v1.0/sponsorship/cases-info/getFamilies",
                    method: "POST",
                    data: data
                }).then(function (response) {
                    var file_type='xlsx';
                    if(action === 'ExportToWord') {
                        file_type='zip';
                    }
                      window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;

                }, function(error) {
                    $rootScope.toastrMessages('error',error.data.msg);
                });


            }else{
                data.page=1;
                LoadFamilies(data);
            }

        };
    });


angular.module('SponsorshipModule')
    .controller('organizationsCasesController', function($filter,$stateParams,$rootScope, $scope,$uibModal, $http, $timeout,$state,setting,Org,category,Entity,custom_forms,cases,persons,casesInfo,nominate,FileUploader, OAuthToken,org_sms_service) {

        var AuthUser =localStorage.getItem("charity_LoggedInUser");
        $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
        $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
        if($rootScope.charity_LoggedInUser.organization != null) {
            $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
        }
        $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;

        $rootScope.settings.layout.pageSidebarClosed = true;
        $rootScope.type= 'all';
        $state.current.data.pageTitle = $stateParams.type == 'deleted'? $filter('translate')('deleted cases') :
                                                                        $filter('translate')('sponsorships cases');

        $rootScope.settings.layout.pageSidebarClosed = true;

        $rootScope.failed =false;
        ;
        $rootScope.model_error_status =false;
        $rootScope.model_error_status2 =false;
        $scope.custom = true;
        $scope.export = '';
        $rootScope.items=[];
        $scope.master=false;
        $rootScope.status ='';
        $rootScope.msg ='';
        $rootScope.DataFilter= {};
        $rootScope.person_restrict=false;
        $rootScope.card_restrict=false;
        $rootScope.restricted=[];
        $rootScope.invalid_cards=[];

        $scope.caseCollapsed =  $scope.personCollapsed =
            $scope.fatherCollapsed= $scope.motherCollapsed = $scope.guardianCollapsed = true;
        $scope.sortKeys={name:false,gender:false,category:false,guardian:false,rank:false,status:false};


        $scope.mode =$stateParams.mode;
        $rootScope.status ='';
        $rootScope.msg ='';
        $rootScope.sponsor={};



        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });



        $scope.collapsed = function(key){
            if(key =='case'){
                $scope.caseCollapsed = $scope.caseCollapsed == false ? true: false;
            }else if(key =='father'){
                $scope.fatherCollapsed = $scope.fatherCollapsed == false ? true: false;
            }else if(key =='mother'){
                $scope.motherCollapsed = $scope.motherCollapsed == false ? true: false;
            }else if(key =='guardian'){
                $scope.guardianCollapsed = $scope.guardianCollapsed == false ? true: false;
            }
        };
        $scope.resetFilters=function(){
            $rootScope.DataFilter={"all_organization":false};
        };

        $scope.sendSms=function(type,receipt_id,size) {

            if(type == 'manual'){
                var receipt = [];
                var pass = true;

                if(receipt_id ==0){
                    angular.forEach($rootScope.items, function (v, k) {
                        if (v.caseCheck) {
                            receipt.push(v.person_id);
                        }
                    });

                    if(receipt.length==0){
                        pass = false;
                        $rootScope.toastrMessages('error',$filter('translate')("you did not select anyone to send them sms message"));
                        return;
                    }
                }else{
                    receipt.push(receipt_id);
                }

                if(pass){
                    org_sms_service.getList(function (response) {
                        $rootScope.Providers = response.Service;
                    });
                    $uibModal.open({
                        templateUrl: '/app/modules/sms/views/modal/sent_sms_from_person_general.html',
                        backdrop  : 'static',
                        keyboard  : false,
                        windowClass: 'modal',
                        size: 'lg',
                        controller: function ($rootScope,$scope, $modalInstance) {
                            $scope.sms={source: 'check' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:"",cont_type:""};
                            $scope.confirm = function (data) {
                                $rootScope.clearToastr();
                                var params = angular.copy(data);
                                params.receipt=receipt;
                                params.target='person';

                                org_sms_service.sendSmsToTarget(params,function (response) {
                                    if(response.status=='error' || response.status=='failed') {
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    else if(response.status== 'success'){
                                        if(response.download_token){
                                            var file_type='xlsx';
                                              window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                        }

                                        $modalInstance.close();
                                        $rootScope.toastrMessages('success',response.msg);
                                    }
                                }, function(error) {
                                    $rootScope.toastrMessages('error',error.data.msg);
                                });
                            };


                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };

                        },
                        resolve: {
                        }
                    });

                }
            }
            else{
                org_sms_service.getList(function (response) {
                    $rootScope.Providers = response.Service;
                });
                $uibModal.open({
                    templateUrl: '/app/modules/sms/views/modal/sent_sms_from_xlx_general.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                        $scope.upload=false;
                        $scope.sms={source: 'file' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:"",cont_type:""};

                        $scope.RestUpload=function(value){
                            if(value ==2){
                                $scope.upload=false;
                            }
                        };

                        $scope.fileUpload=function(){
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.upload=true;
                        };
                        $scope.download=function(){
                              window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-to-send-sms-template&template=true';
                        };

                        var Uploader = $scope.uploader = new FileUploader({
                            url: 'api/v1.0/sms/excel',
                            removeAfterUpload: false,
                            queueLimit: 1,
                            headers: {
                                Authorization: OAuthToken.getAuthorizationHeader()
                            }
                        });


                        $scope.confirm = function (item) {
                            $scope.spinner=false;
                            $scope.import=false;
                            $rootScope.progressbar_start();
                            Uploader.onBeforeUploadItem = function(item) {

                                if($scope.sms.same_msg == 0){
                                    $scope.sms.sms_messege = '';
                                }
                                item.formData = [$scope.sms];
                            };
                            Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                                $rootScope.progressbar_complete();
                                $scope.uploader.destroy();
                                $scope.uploader.queue=[];
                                $scope.uploader.clearQueue();
                                angular.element("input[type='file']").val(null);

                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                    $scope.upload=false;
                                    angular.element("input[type='file']").val(null);
                                }
                                else if(response.status=='success')
                                {
                                    if(response.download_token){
                                        var file_type='xlsx';
                                          window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                    }
                                    $scope.spinner=false;
                                    $scope.upload=false;
                                    angular.element("input[type='file']").val(null);
                                    $scope.import=false;
                                    $rootScope.toastrMessages('success',response.msg);
                                    $modalInstance.close();
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                    $scope.upload=false;
                                    angular.element("input[type='file']").val(null);
                                }

                            };
                            Uploader.uploadAll();
                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                        $scope.remove=function(){
                            angular.element("input[type='file']").val(null);
                            $scope.uploader.clearQueue();
                            angular.forEach(
                                angular.element("input[type='file']"),
                                function(inputElem) {
                                    angular.element(inputElem).val(null);
                                });
                        };

                    }});

            }
        };

        var getCases =function(params){
            params.category_type=1;
            if(angular.isUndefined(params.action)){
                params.action ='filter';
            }
            if(!angular.isUndefined(params.sponsor)){
                var sponsor =[];
                angular.forEach(params.sponsor, function(v, k) {
                    sponsor.push(v.id);
                });

                params.sponsor=sponsor;
            }

            if(params.action && angular.isUndefined(params.page) ){
                params.page =1;
            }

            if( !angular.isUndefined(params.all_organization)) {
                if(params.all_organization == true ||params.all_organization == 'true'){
                    var orgs=[];
                    angular.forEach(params.organization_id, function(v, k) {
                        orgs.push(v.id);
                    });

                    params.organization_ids=orgs;

                }else if(params.all_organization == false ||params.all_organization == 'false') {
                    params.organization_ids=[];
                    $scope.master=false
                }
            }

                if( !angular.isUndefined(params.birthday_to)) {
                    params.birthday_to=$filter('date')(params.birthday_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.birthday_from)) {
                    params.birthday_from= $filter('date')(params.birthday_from, 'dd-MM-yyyy')
                }
           
                 if( !angular.isUndefined(params.date_to)) {
                    params.date_to=$filter('date')(params.date_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.date_from)) {
                    params.date_from= $filter('date')(params.date_from, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(params.father_birthday_to)) {
                    params.father_birthday_to=$filter('date')(params.father_birthday_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.father_birthday_from)) {
                    params.father_birthday_from= $filter('date')(params.father_birthday_from, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(params.father_death_date_to)) {
                    params.father_death_date_to=$filter('date')(params.father_death_date_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.father_death_date_from)) {
                    params.father_death_date_from= $filter('date')(params.father_death_date_from, 'dd-MM-yyyy')
                }


                if( !angular.isUndefined(params.mother_birthday_to)) {
                    params.mother_birthday_to=$filter('date')(params.mother_birthday_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.mother_birthday_from)) {
                    params.mother_birthday_from= $filter('date')(params.mother_birthday_from, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(params.mother_death_date_to)) {
                    params.mother_death_date_to=$filter('date')(params.mother_death_date_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.mother_death_date_from)) {
                    params.mother_death_date_from= $filter('date')(params.mother_death_date_from, 'dd-MM-yyyy')
                }

           
            params.organizationsCases = true;
            
            $http({
                url: "/api/v1.0/common/sponsorships/cases/filterCases",
                method: "POST",
                data: params
            })
                .then(function (response) {

                    if(params.action != 'filter'){
                        if(response.data.status == 'false'){
                            $rootScope.toastrMessages('error',$filter('translate')('there is no cases to export'));
                        }else if(params.action == 'FamilyMember' && response.data.status == false){
                            $rootScope.toastrMessages('error',$filter('translate')('there is no family member to export'));
                        }else{
                            var file_type='zip';
                            if(params.action == 'ExportToExcel' || params.action == 'Basic' || params.action == 'FamilyMember') {
                                file_type='xlsx';
                            }
                              window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                        }
                    }else{
                        if(response.data.Cases.data.length != 0){
                            $rootScope.result='true';
                            $scope.master=response.data.master;
                            $scope.organization_id=response.data.organization_id;
                            var temp= response.data.Cases.data;
                            angular.forEach(temp, function(v, k) {
                                if($rootScope.type=='all'){
                                    v.rank=parseInt(v.rank);
                                }
                                v.organization_id=parseInt(v.organization_id);
                            });
                            $rootScope.items= temp;
                            $rootScope.CurrentPage = response.data.Cases.current_page;
                            $scope.TotalItems = response.data.Cases.total;
                            $scope.ItemsPerPage = response.data.Cases.per_page;
                        }else{
                            $rootScope.result='false';
                            $rootScope.items=[];
                        }
                    }

                }, function(error) {
                    $rootScope.toastrMessages('error',error.data.msg);
                });
        };

        $scope.resetFilters();

        if($rootScope.type == 'all'){
            var entities='deathCauses,furnitureStatus,houseStatus,propertyTypes,roofMaterials,educationAuthorities,educationStages,diseases,maritalStatus,deathCauses,countries,workJobs';
            Entity.get({entity:'entities',c:entities},function (response) {
                $scope.DeathCauses = response.deathCauses;
                $scope.furnitureStatus = response.furnitureStatus;
                $scope.houseStatus = response.houseStatus;
                $scope.PropertyTypes = response.propertyTypes;
                $scope.RoofMaterials = response.roofMaterials;
                $scope.EduAuthorities = response.educationAuthorities;
                $scope.EduStages = response.educationStages;
                $scope.Diseases = response.diseases;
                $scope.MaritalStatus = response.maritalStatus;
                $scope.DeathCauses = response.deathCauses;
                $scope.Country = response.countries;
                $scope.WorkJob = response.workJobs;
            });
            Org.list({type:'sponsors'},function (response) { $rootScope.Sponsor = response; });

        }

        Org.list({type:'organizations'},function (response) {  $scope.Org = response; });


        $scope.searchCases = function (selected , params,action) {
            $rootScope.clearToastr();
            $scope.sortKeys={};
            if( !angular.isUndefined($rootScope.DataFilter.sortKeys)) {
                delete $rootScope.DataFilter.sortKeys;
            }

            var filters = angular.copy(params);
            filters.action=action;
             filters.persons=[];
             
             if(selected == true || selected == 'true' ){
                    var persons=[];
                     angular.forEach($rootScope.items, function(v, k){                
                         if(v.caseCheck){
                             persons.push(v.case_id);
                         }
                     });

                     if(persons.length==0){
                         $rootScope.toastrMessages('error',$filter('translate')('You have not select people to export'));
                         return ;
                     }
                     filters.persons=persons;
                 }
            getCases(filters);
        };

        $scope.sortKeyArr=[];
        $scope.sortKeyArr_rev=[];
        $scope.sortKeyArr_un_rev=[];
        $scope.sort_ = function(keyname){
            $scope.sortKey_ = keyname;
            var reverse_ = false;
            if (
                ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
                ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
            ) {
                reverse_ = true;
            }
            var data = angular.copy($rootScope.DataFilter);
            data.action ='filters';

            if (reverse_) {
                if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                    $scope.sortKeyArr_rev=[];
                    $scope.sortKeyArr_un_rev=[];
                    $scope.sortKeyArr_rev.push(keyname);
                }
            }
            else {
                if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                    $scope.sortKeyArr_rev=[];
                    $scope.sortKeyArr_un_rev=[];
                    $scope.sortKeyArr_un_rev.push(keyname);
                }

            }

            data.sortKeyArr_rev = $scope.sortKeyArr_rev;
            data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;
            $scope.searchCases(false ,data,'filter');
        };

        $rootScope.pageChanged = function (CurrentPage) {
            $rootScope.DataFilter.page=CurrentPage;
            $rootScope.DataFilter.action='filter';

            if($stateParams.id){
                $rootScope.DataFilter.sponsor_id=$stateParams.id;
            }
            getCases($rootScope.DataFilter);

        };

        $rootScope.itemsPerPage_ = function (itemsCount) {
            $rootScope.DataFilter.itemsCount = itemsCount ;
            $rootScope.CurrentPage = 1 ;
            $rootScope.DataFilter.page = 1;
            getCases($rootScope.DataFilter);
        };

        $rootScope.close=function(){

            $rootScope.failed =false;
            ;
            $rootScope.model_error_status =false;
            $rootScope.model_error_status2 =false;
        };
        $scope.get = function(_person,target,value,parant){

            if(value != null && value != "" && value != " " ) {

                Entity.listChildren({entity:target,'id':value,'parent':parant},function (response) {

                    if(target =='branches'){
                        $scope.Branches = response;
                        $rootScope.DataFilter.branch_name="";
                    }else{
                        if(_person =='case'){
                            if(target =='districts'){
                                $scope.governarate = response;
                                $scope.mosques = [];
                                $scope.city = [];
                                $scope.nearlocation =[];
                                $rootScope.DataFilter.governarate="";
                            }
                            else if(target =='cities'){
                                $scope.city = response;
                                $scope.mosques = [];
                                $scope.nearlocation =[];
                                $rootScope.DataFilter.city="";
                            }
                            else if(target =='neighborhoods'){
                                $scope.nearlocation = response;
                                $scope.mosques = [];
                                $rootScope.DataFilter.location_id="";
                            }else if(target =='mosques'){
                                $scope.mosques = response;
                                $rootScope.DataFilter.mosques_id="";
                            }
                        }else{
                            if(target =='districts'){
                                $scope.guardian_governarate = response;
                                $scope.guardian_city = [];
                                $scope.guardian_mosques = [];
                                $scope.guardian_nearlocation =[];
                                $rootScope.DataFilter.guardian_governarate="";
                            }else if(target =='cities'){
                                $scope.guardian_city = response;
                                $scope.guardian_mosques = [];
                                $scope.guardian_nearlocation =[];
                                $rootScope.DataFilter.guardian_city="";
                            }else if(target =='neighborhoods'){
                                $scope.guardian_nearlocation = response;
                                $scope.guardian_nearlocation =[];
                                $rootScope.DataFilter.guardian_location_id="";
                            }else if(target =='mosques'){
                                $scope.mosques = response;
                                $rootScope.DataFilter.guardian_mosques_id="";
                            }
                        }

                    }


                });
            }


        };
        
        $scope.download = function (target,id) {

            $rootScope.clearToastr();
            var url ='';
            var file_type ='zip';
            var  params={};
            var method='';

            if(target =='word'){
                method='POST';
                params={'case_id':id,
                    'target':'info',
                    'action':'filters',
                    'mode':'show',
                    'category_type' : 1,
                    'person' :true,
                    'persons_i18n' : true,
                    'category' : true,
                    'work' : true,
                    'contacts' : true,
                    'health' : true,
                    'default_bank' : true,
                    'education' : true,
                    'islamic' : true,
                    'banks' : true,
                    'residence' : true,
                    'persons_documents' : true,
                    'family_member' : true,
                    'father_id' : true,
                    'mother_id' : true,
                    'guardian_id' : true,
                    'guardian_kinship' : true,
                    'parent_detail':true,
                    'guardian_detail':true,
                    'visitor_note':true
                };
                url = "/api/v1.0/common/sponsorships/cases/word";
            }
            else if(target =='pdf'){
                method='POST';
                params={'case_id':id,
                    'target':'info',
                    'action':'filters',
                    'mode':'show',
                    'category_type' : 1,
                    'person' :true,
                    'persons_i18n' : true,
                    'category' : true,
                    'work' : true,
                    'contacts' : true,
                    'health' : true,
                    'default_bank' : true,
                    'education' : true,
                    'islamic' : true,
                    'banks' : true,
                    'residence' : true,
                    'persons_documents' : true,
                    'family_member' : true,
                    'father_id' : true,
                    'mother_id' : true,
                    'guardian_id' : true,
                    'guardian_kinship' : true,
                    'parent_detail':true,
                    'guardian_detail':true,
                    'visitor_note':true
                };
                url = "/api/v1.0/common/sponsorships/cases/pdf";
            }
            else{
                method='GET';
                url = "/api/v1.0/common/sponsorships/cases/exportCaseDocument/"+id;
            }

            $http({
                url:url,
                method: method,
                data:params
            }).then(function (response) {

                if(target !='word' && response.data.status == false){
                    $rootScope.toastrMessages('error',$filter('translate')('no attachment to export'));
                }else{
                      window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }

            }, function(error) {
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };
        $scope.selectedAll = function(value){
            if(value){
                $scope.selectAll = true;
            }
            else{
                $scope.selectAll =false;

            }
            angular.forEach($rootScope.items, function(val,key)
            {
                val.caseCheck=$scope.selectAll;
            });
        };
       
        $scope.toggleCustom = function() {
            $scope.custom = $scope.custom === false ? true: false;
            $rootScope.DataFilter={"all_organization":false};
            $scope.export = '';
        };
    
        $scope.sortBy = function (keyname) {
            $scope.sortKeyis = keyname;
            $scope.sortKeysValue={name:0,gender:0,category_name:0,guardian_name:0,rank:0,status:0};

            if(keyname == 'gender'){
                $scope.sortKeys.gender = !$scope.sortKeys.gender;
                $scope.sortKeysValue.gender = !$scope.sortKeys.gender;
            }else if(keyname == 'name'){
                $scope.sortKeys.name = !$scope.sortKeys.name;
                $scope.sortKeysValue.name = !$scope.sortKeys.name;
            }else if(keyname == 'category_name'){
                $scope.sortKeys.category = !$scope.sortKeys.category;
                $scope.sortKeysValue.category = !$scope.sortKeys.category;
            }else if(keyname == 'guardian_name'){
                $scope.sortKeys.guardian = !$scope.sortKeys.guardian;
                $scope.sortKeysValue.guardian = !$scope.sortKeys.guardian;
            }else if(keyname == 'rank'){
                $scope.sortKeys.rank = !$scope.sortKeys.rank;
                $scope.sortKeysValue.rank = !$scope.sortKeys.rank;
            }else if(keyname == 'status'){
                $scope.sortKeys.status = !$scope.sortKeys.status;
                $scope.sortKeysValue.status = !$scope.sortKeys.status;
            }else if(keyname == 'organization_name'){
                $scope.sortKeys.organization_name = !$scope.sortKeys.organization_name;
                $scope.sortKeysValue.organization_name = !$scope.sortKeys.organization_name;
            }

            $rootScope.DataFilter.page=$rootScope.CurrentPage;
            $rootScope.DataFilter.action='filter';
            $rootScope.DataFilter.keyname=keyname;
            $rootScope.DataFilter.sortKeys=$scope.sortKeys;
            $rootScope.DataFilter.sortKeysValue=$scope.sortKeys;
            if($stateParams.id){
                $rootScope.DataFilter.sponsor_id=$stateParams.id;
            }

            getCases($rootScope.DataFilter);

        };

        $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $rootScope.format = $rootScope.formats[0];
        $rootScope.today = function() {$rootScope.dt = new Date();};
        $rootScope.today();
        $rootScope.clear = function() {$rootScope.dt = null;};

        $rootScope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup1.opened = true;};
        $rootScope.open2 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup2.opened = true;};
        $rootScope.open44 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup44.opened = true;};
        $rootScope.open3 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup3.opened = true;};
        $rootScope.open33 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup33.opened = true;};
        $rootScope.open333 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup333.opened = true;};
        $rootScope.open3333 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup3333.opened = true;};
        $rootScope.open11 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup11.opened = true;};
        $rootScope.open111 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup111.opened = true;};
        $rootScope.open1111 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup1111.opened = true;};
        $rootScope.open11111 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup11111.opened = true;};
        $rootScope.open4 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup4.opened = true;};
        $rootScope.open5 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup5.opened = true;};
        $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
        $scope.open88 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup88.opened = true;};

        $rootScope.popup1 = {opened: false};
        $rootScope.popup2 = {opened: false};
        $rootScope.popup33 = {opened: false};
        $rootScope.popup44 = {opened: false};
        $rootScope.popup3 = {opened: false};
        $rootScope.popup333 = {opened: false};
        $rootScope.popup3333 = {opened: false};
        $rootScope.popup11 = {opened: false};
        $rootScope.popup111 = {opened: false};
        $rootScope.popup1111 = {opened: false};
        $rootScope.popup11111 = {opened: false};
        $rootScope.popup4 = {opened: false};
        $rootScope.popup5 = {opened: false};
        $scope.popup99 = {opened: false};
        $scope.popup88 = {opened: false};

        

    });

