angular.module('SponsorshipModule')
    .controller('ReportsController', function ($http,$filter,$scope,$rootScope,category,$state,$uibModal,$stateParams,reports,Org) {

        
        $rootScope.sponsorCategories_=[];
        category.getSectionForEachCategory({'type':'sponsorships'},function (response) {
            if(!angular.isUndefined(response)){
                $rootScope.sponsorCategories_firstStep_map =[];
                $rootScope.sponsorCategories = response.Categories;
                if($rootScope.sponsorCategories.length > 0){
                    for (var i in $rootScope.sponsorCategories) {
                        var item = $rootScope.sponsorCategories[i];
                        $rootScope.sponsorCategories_firstStep_map[item.id] = item.FirstStep;
                    }
                     var sponsorCategories_=[];
                    angular.forEach($rootScope.sponsorCategories, function(v, k) {
                        if ($rootScope.lang == 'ar'){
                            sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
                        }
                        else{
                            sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
                        }
                    });

                    $rootScope.sponsorCategories_ = sponsorCategories_ ;
                }

            }
        });

        $state.current.data.pageTitle = $filter('translate')('reports');
        $rootScope.settings.layout.pageSidebarClosed = true;
        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');

        var disableSponsor = true;
        var resetSearchFilter =function(){
            $scope.filters={ 'action':"filter",
                'sponsor_id':"",
                'category_id':"",
                'category':"",
                'report_type':"",
                'currency_id':"",
                'title':"",
                'notes':"",
                'created_at_to':"",
                'created_at_from':"",
                'begin_date_from':"",
                'end_date_from':"",
                'begin_date_to':"",
                'end_date_to':""
            };

            if($stateParams.id){
                $scope.filters.sponsor_id=parseInt($stateParams.id);
                disableSponsor=true;
            }

        };


        $rootScope.failed =false;
        ;
        $rootScope.model_error_status =false;
        $scope.custom = true;
        $scope.status ='';
        $rootScope.msg ='';
        $scope.filters={};
        $scope.items=[];
        $scope.CurrentPage=1;

        if($stateParams.id){
            $scope.filters.sponsor_id=parseInt($stateParams.id);
            disableSponsor=true;
        }

        var LoadReports=function(params){
            $rootScope.clearToastr();
            if( !angular.isUndefined(params.begin_date_from)) {
                params.begin_date_from=$filter('date')(params.begin_date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.end_date_from)) {
                params.end_date_from= $filter('date')(params.end_date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.begin_date_to)) {
                params.begin_date_to=$filter('date')(params.begin_date_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.end_date_to)) {
                params.end_date_to= $filter('date')(params.end_date_to, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(params.created_at_to)) {
                params.created_at_to=$filter('date')(params.created_at_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.created_at_from)) {
                params.created_at_from= $filter('date')(params.created_at_from, 'dd-MM-yyyy')
            }

            angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();
            reports.filter(params,function (response) {
                $rootScope.progressbar_complete();
                 if(params.action == 'export'){
                    if(response.download_token){
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        var file_type='xlsx';
                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;

                    }else{
                         $rootScope.toastrMessages('error',$filter('translate')('There is no data to export'));
                    }
                
                }else{
                    angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                    $scope.items= response.data;
                    $scope.CurrentPage = response.current_page;
                    $scope.TotalItems = response.total;
                    $scope.ItemsPerPage = response.per_page; 
                }
               
            });

        };

        Org.list({type:'sponsors'},function (response) { $rootScope.Sponsor = response; });

        var reLoadReports = function () {
            $scope.filters.page=$scope.CurrentPage;
            $scope.filters.action='filter';
            $scope.filters.itemsCount= $scope.itemsCount ;
            LoadReports($scope.filters);
        };
        $scope.pageChanged = function (currentPage) {
            $scope.CurrentPage=currentPage;
            reLoadReports();
        };

        $rootScope.itemsPerPage_ = function (itemsCount) {
           $scope.CurrentPage=1;
           $scope.itemsCount = itemsCount ;
           reLoadReports();
        };


        $scope.toggleCustom = function() {
            $scope.custom = $scope.custom === false ? true: false;
            resetSearchFilter();
        };
        
         $scope.selectedAll = function(value){
            if(value){
                $scope.selectAll = true;
            }
            else{
                $scope.selectAll =false;

            }
            angular.forEach($scope.items, function(val,key)
            {
                val.check=$scope.selectAll;
            });
        };
        $scope.Search=function(selected , data,action) {
            $rootScope.clearToastr();
            data.action=action;
            data.items=[];
            
            if(selected == true || selected == 'true' ){
                    var items=[];
                     angular.forEach($scope.items, function(v, k){                
                         if(v.check){
                             items.push(v.id);
                         }
                     });

                     if(items.length==0){
                         $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                         return ;
                     }
                     data.items=items;
            }
            
            data.page=1;
            LoadReports(data);
        };
        $scope.download=function(id){

            $rootScope.clearToastr();
            angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();
            $http({
                url:'/api/v1.0/sponsorship/reports/download/'+id,
                method: "GET"
            }).then(function (response) {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                   window.location = '/api/v1.0/common/files/zip/export?download_token='+response.data.download_token+'&reports=true';
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };
        $scope.reports=function(){
            $rootScope.clearToastr();

            $uibModal.open({
                size: 'lg',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                templateUrl: 'createReports.html',
                controller: function ($rootScope,$scope,$modalInstance) {
                    $scope.msg1=[];
                    $scope.options_cases=[];
                    $scope.report={all:true};

                    $scope.sponsorCases= function (id,category_id) {

                        if( !angular.isUndefined($scope.msg1.sponsor_id)) {
                            $scope.msg1.sponsor_id=[];
                        }
                        if( !angular.isUndefined($scope.msg1.category_id)) {
                            $scope.msg1.category_id=[];
                        }

                        if(!angular.isUndefined($scope.report.sponsor_id) && !angular.isUndefined($scope.report.category_id)) {
                            $scope.options_cases=reports.getSponsorCasesList({id:id,category_id:category_id});
                        }

                    };
                    $scope.confirm = function (row) {
                        $rootScope.clearToastr();

                        var item = angular.copy(row);

                        if( !angular.isUndefined(item.date_from)) {
                            item.date_from=$filter('date')(item.date_from, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(item.date_to)) {
                            item.date_to=$filter('date')(item.date_to, 'dd-MM-yyyy')
                        }


                        $rootScope.progressbar_start();
                        reports.save(item,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                $modalInstance.close();
                                if(response.status=='success')
                                {
                                    if(response.download_token){
                                        window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token+'&reports=true';
                                    }
                                    $rootScope.toastrMessages('success',response.msg);
                                    reLoadReports();
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }
                        }, function(error) {
                            $rootScope.toastrMessages('error',error.data.msg);
                            $modalInstance.close();
                        });

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    var today = new Date();
                    $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
                    $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};
                    $scope.popup={0:{},1:{opened:false},2:{opened:false}};
                    $scope.open=function (target,$event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        if(target == 1){ $scope.popup[1].opened = true;  }
                        else if(target == 2) { $scope.popup[2].opened = true; }
                    };
                }});

        };
        $scope.generate=function(id){
            $rootScope.clearToastr();
            angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();
            $http({
                url:'/api/v1.0/sponsorship/reports/generate/'+id,
                method: "GET"
            }).then(function (response) {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                   window.location = '/api/v1.0/common/files/zip/export?download_token='+response.data.download_token+'&reports=true';
            }, function() {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };

        // POP UP CALENDER
        $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {$scope.dt = new Date();};
        $scope.today();
        $scope.clear = function() {$scope.dt = null;};
        $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
        $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};

        $scope.popup={0:{},1:{opened:false},2:{opened:false},3:{opened:false},4:{opened:false},5:{opened:false},6:{opened:false}};
        $scope.open=function (target,$event) {
            $event.preventDefault();
            $event.stopPropagation();
            if(target == 1){ $scope.popup[1].opened = true;  }
            else if(target == 2) { $scope.popup[2].opened = true; }
            else if(target == 3) { $scope.popup[3].opened = true; }
            else if(target == 4) { $scope.popup[4].opened = true; }
            else if(target == 5) { $scope.popup[5].opened = true; }
            else if(target == 6) { $scope.popup[6].opened = true; }
        };

        reLoadReports();

    });



