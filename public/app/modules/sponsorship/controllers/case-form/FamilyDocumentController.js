
angular.module('SponsorshipModule')
    .controller('FamilyDocumentController', function ($scope,$rootScope,$state,$stateParams ,$uibModal,OAuthToken,cases,FileUploader,category) {


        $rootScope.failed =false;
        ;
        $rootScope.model_error_status =false;

        $rootScope.close=function(){

            $rootScope.failed =false;
            ;
            $rootScope.model_error_status =false;
        };

        $rootScope.status ='';
        $rootScope.msg ='';
        $rootScope.msg2 ='';

        $scope.category_id=$stateParams.category_id;
        $rootScope.category_id=$stateParams.category_id;
        $rootScope.person_id     = $stateParams.person_id;
        $rootScope.case_id     = $stateParams.case_id;
        $rootScope.guardian_id = $stateParams.guardian_id;
        $rootScope.father_id   = $stateParams.father_id;
        $rootScope.mother_id   = $stateParams.mother_id;

        $rootScope.ResetForm($rootScope.category_id, 'add','familyDocuments');
        $rootScope.TheCurrentStep= $rootScope.FamilyDocumentStepNumber;
        $rootScope.documents=[];

        var loadCommonDocuments=function(){
            category.getCategoryDocuments({  category_id : $rootScope.category_id ,type : 'sponsorships',target : 'common' ,person: [
                { scope:'1' , id:$rootScope.father_id} ,
                { scope:'2' , id:$rootScope.mother_id} ,
                { scope:'4' , id:$rootScope.guardian_id}
            ]

            },function (response){
                $rootScope.documents =response.documents;
                $rootScope.result='true';
             });
        };


        loadCommonDocuments();

        $scope.forward=function(){
            if(angular.isUndefined($rootScope.guardian_id)|| $rootScope.guardian_id == null || $rootScope.guardian_id == ""){
                var next =$rootScope.FormSponsorshipSection[$rootScope.TheCurrentStep].id;
                angular.element('#'+next).removeClass("disabled");
                $rootScope.Next();
            }else{
                $rootScope.Next();
            }
        };


        $scope.showImage=function(file){
            $rootScope.clearToastr();
            $rootScope.target=file;
            $uibModal.open({
                templateUrl: 'showImage.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance) {

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                },
                resolve: {

                }
            });
        };
        $scope.ChooseAttachment=function(item,action){
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'myModalContent2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,cases,OAuthToken, FileUploader) {

                    $scope.fileupload = function () {
                        if($scope.uploader.queue.length){
                            var image = $scope.uploader.queue.slice(1);
                            $scope.uploader.queue = image;
                        }
                    };
                    var uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/doc/files',
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });
                    uploader.onBeforeUploadItem = function(item) {
                        $rootScope.clearToastr();
                    };
                    uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(response.status=='error')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else{
                        fileItem.id = response.id;
                        cases.setAttachment({id:item.person_id,type:'sponsorships',action:action,document_type_id:item.document_type_id,document_id:fileItem.id},function (response1) {
                            if(response1.status=='error' || response1.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response1.msg);
                            }
                            else if(response1.status== 'success'){
                                $rootScope.toastrMessages('success',response1.msg);
                                $modalInstance.close();
                                loadCommonDocuments();
                            }

                        }, function(error) {
                            $rootScope.toastrMessages('error',error.data.msg);
                        });
                    }};

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };


    });