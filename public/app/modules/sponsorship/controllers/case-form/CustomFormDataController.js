
angular.module('SponsorshipModule')
    .directive("dynamicName",function($compile){
        return {
            restrict:"A",
            terminal:true,
            priority:1000,
            link:function(scope,element,attrs){
                element.attr('name', scope.$eval(attrs.dynamicName));
                element.removeAttr("dynamic-name");
                $compile(element)(scope);
            }
        }
    })
    .controller('CustomFormDataController', function ($scope,$rootScope,$state,$stateParams,forms_case_data) {

        $rootScope.error=false;
        $rootScope.category_id=$stateParams.category_id;
        $rootScope.person_id     = $stateParams.person_id;
        $rootScope.case_id     = $stateParams.case_id;
        $rootScope.guardian_id = $stateParams.guardian_id;
        $rootScope.father_id   = $stateParams.father_id;
        $rootScope.mother_id   = $stateParams.mother_id;
        $rootScope.result_1 = 'true';
        $rootScope.result = 'true';

        $rootScope.ResetForm($rootScope.category_id, 'add','customFormData');
        $rootScope.TheCurrentStep= $rootScope.CustomFormDataStepNumber;

        forms_case_data.get({id: $stateParams.case_id,module:'sponsorship',category_id:$stateParams.category_id},function (response) {

            if( !angular.isUndefined(response.status)){
                if(response.status === false) {
                    $rootScope.result_1 = 'false';
                }
            }

            if( !angular.isUndefined(response.status_2)){
                if(response.status_2 === false) {
                    $rootScope.result = 'false';
                }
            }
            if(angular.isUndefined(response.status_2) && angular.isUndefined(response.status)){
                    if (response.data.length !== 0) {
                        var temp = response.data;

                        angular.forEach(temp, function(v, k) {
                            v.data=v.value;
                            if (v.options.length !== 0) {
                                if(v.value !== null && v.value !== 'null'){
                                    v.data=parseInt(v.value) +"";
                                }
                            }
                        });
                        $scope.fields=temp;
                    }
                }



            });


        $scope.forward=function(fields){
            $rootScope.clearToastr();
            var target = new forms_case_data({case_id:$stateParams.case_id,'fieldsData':fields});
                    target.$save()
                        .then(function (response) {
                            if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else {
                                $rootScope.Next();
                            }
                        },function(error) {
                            $rootScope.toastrMessages('error',error.data.msg);
                        });
        };


    });