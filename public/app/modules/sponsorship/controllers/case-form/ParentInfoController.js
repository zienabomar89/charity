angular.module('SponsorshipModule')
    .controller('ParentInfoController', function ($scope,$rootScope,$state,$stateParams,$filter,$uibModal,category,Entity,persons) {

        $rootScope.error=false;
        $rootScope.success_wife=false;
        $rootScope.success=false;
        $rootScope.mother_error=false;
        $rootScope.disable_=false;
        $rootScope.msg='';
        $scope.msg1={};
        $rootScope.items=[];
        $rootScope.msg2={};
        $rootScope.result = 'false';
        $rootScope.mode='';
        $rootScope.category_id=$stateParams.category_id;
        $rootScope.person_id     = $stateParams.person_id;
        $rootScope.case_id     = $stateParams.case_id;
        $rootScope.guardian_id = $stateParams.guardian_id;
        $rootScope.father_id   = $stateParams.father_id;
        $rootScope.mother_id   = $stateParams.mother_id;
        $rootScope.ResetForm($rootScope.category_id, 'add','ParentData');
        $rootScope.TheCurrentStep=$rootScope.ParentStepNumber;
        $rootScope.success_1=false;
        $rootScope.success_2=false;
        $rootScope.success_3=false;
        $rootScope.success_4=false;
        $rootScope.load_not_inserted= true;

        $scope.father={};
        $rootScope.mother={};
        $rootScope.close=function(){
            $rootScope.failed =false;
            $rootScope.model_error_status =false;
            $rootScope.success_1=false;
            $rootScope.success_2=false;
            $rootScope.success_3=false;
            $rootScope.success_4=false;
        };

        var setWives=function(items){
            var len =items.length;
            angular.forEach(items, function(val,key) {
                if(len ==1 ){
                    val.check=true;
                    //$rootScope.mother_id= val.id;
                }else{
                    if( !angular.isUndefined($rootScope.mother_id)) {
                        if($rootScope.mother_id != '' || $rootScope.mother_id !=  null) {
                            if(val.id == $rootScope.mother_id){
                                val.check=true;
                                //$rootScope.mother_id= val.id;
                            }else{
                                val.check=false;
                            }
                        }
                    }
                    else{
                        val.check=false;
                    }
                }
            });
            $rootScope.items=items;
        };
        var loadWife=function(){

            var params ={'action':'filters','target':'info','person_id':$rootScope.father_id, 'mode':'edit', 'wives' :true};

            if($rootScope.has_Wives != false && $rootScope.load_not_inserted == true){
                params.citizen_wives = true;
            }

            persons.getPersonReports(params, function(response) {
                if($rootScope.has_Wives != false && $rootScope.load_not_inserted == true){
                    $rootScope.wives_not_inserted = response.wives_not_inserted;
                }

                setWives(response.wives);
            });
        };


        var find=function(target,action,id){
            $rootScope.clearToastr();

            var params={ 'mode':'edit', 'person' :true, 'persons_i18n' : true, 'education' : true,'health' : true, 'work' : true};

            // , 'wives' :true

            if(target=='father'){
                params.wives=true;
            }
            if(action =='find'){
                params.id_card_number=id;
                if($rootScope.check_id(id)){
                    $rootScope.progressbar_start();
                    persons.find(params,function(response){
                        $rootScope.progressbar_complete();
                        if(response.status == true) {
                        if(target=='mother'){
                            $rootScope.mother=response.person;
                            // $rootScope.mother=response.person;
                            if($rootScope.has_mother !=false) {
                                $rootScope.mother_id=$rootScope.mother.id;
                            }

                            if($rootScope.mother.death_date == null){
                                $rootScope.mother.alive = 0+"";
                                $rootScope.mother.death_cause_id ="";
                                $rootScope.mother.death_date="";
                            }else{
                                $rootScope.mother.alive =1+"";
                                $rootScope.mother.death_cause_id = $rootScope.mother.death_cause_id+"";
                            }

                            if($rootScope.mother.monthly_income==null){
                                $rootScope.mother.monthly_income = "";
                            }


                            if($rootScope.mother.study == null){
                                $rootScope.mother.study = "";
                                $rootScope.mother.stage="";
                            }else{
                                $rootScope.mother.study= $rootScope.mother.study +"";

                                if($rootScope.mother.study == 0){
                                    $rootScope.mother.stage="";
                                }else{
                                    if($rootScope.mother.stage==null){
                                        $rootScope.mother.stage="";
                                    } else{
                                        $rootScope.mother.stage = $rootScope.mother.stage+"";
                                    }
                                }
                            }

                            if($rootScope.mother.working == null ){
                                $rootScope.mother.working ="";
                                $rootScope.mother.work_job_id="";
                                $rootScope.mother.work_location="";
                            }else{
                                $rootScope.mother.working = $rootScope.mother.working+"";
                                $rootScope.mother.working = $rootScope.mother.working+"";
                                if($rootScope.mother.working == 1){
                                    if( $rootScope.mother.work_job_id==null){
                                        $rootScope.mother.work_job_id="";
                                    }
                                    else{
                                        $rootScope.mother.work_job_id = $rootScope.mother.work_job_id+"";
                                    }
                                }else{
                                    $rootScope.mother.work_job_id="";
                                    $rootScope.mother.work_location="";
                                }
                            }




                            if($rootScope.mother.marital_status_id==null){
                                $rootScope.mother.marital_status_id="";
                            }
                            else{
                                $rootScope.mother.marital_status_id = $rootScope.mother.marital_status_id+"";
                            }

                            if($rootScope.mother.condition == null ){
                                $rootScope.mother.condition="";
                                $rootScope.mother.disease_id="";
                                $rootScope.mother.details="";
                            }else{
                                $rootScope.mother.condition=$rootScope.mother.condition+"";
                                if($rootScope.mother.condition == 2 || $rootScope.mother.condition == 3 ||
                                    $rootScope.mother.condition === '2' || $rootScope.mother.condition === '3' ){

                                    if( $rootScope.mother.disease_id==null){
                                        $rootScope.mother.disease_id="";
                                    }
                                    else{
                                        $rootScope.mother.disease_id = $rootScope.mother.disease_id+"";
                                    }
                                }else{
                                    $rootScope.mother.disease_id="";
                                    $rootScope.mother.details="";
                                }
                            }

                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else{
                            $scope.father=response.person;
                            $rootScope.father_id=$scope.father.id;

                            if($scope.father.death_cause_id !=null){
                                $scope.father.death_cause_id = $scope.father.death_cause_id+"";
                            }

                            if( $scope.father.study == null){
                                $scope.father.study = "";
                                $scope.father.degree="";
                                $scope.father.specialization="";
                            }else{
                                $scope.father.study = $scope.father.study+"";

                                if($scope.father.study == 0){

                                    $scope.father.degree="";
                                    $scope.father.specialization="";
                                }else{
                                    if($scope.father.degree==null){
                                        $scope.father.degree="";
                                    }
                                    else{
                                        $scope.father.degree = $scope.father.degree+"";
                                    }
                                }
                            }

                            if($scope.father.working == null ){
                                $scope.father.working="";
                                $scope.father.work_job_id="";
                                $scope.father.work_location="";
                            }else{
                                $scope.father.working=$scope.father.working+"";
                                if($scope.father.working == 1){
                                    $scope.father.work_job_id = $scope.father.work_job_id+"";
                                }else{
                                    $scope.father.work_location="";
                                }
                            }

                            if($scope.father.condition == null ){
                                $scope.father.condition="";
                                $scope.father.disease_id="";
                                $scope.father.details="";
                            }else{
                                $scope.father.condition=$scope.father.condition+"";
                                if($scope.father.condition == 2 || $scope.father.condition == 3 ||
                                    $scope.father.condition === '2' || $scope.father.condition === '3' ){
                                    if( $scope.father.disease_id==null){
                                        $scope.father.disease_id="";
                                    }
                                    else{
                                        $scope.father.disease_id = $scope.father.disease_id+"";
                                    }
                                }else{
                                    $scope.father.disease_id="";
                                    $scope.father.details="";
                                }
                            }
                            setWives($scope.father.wives);
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    }else{

                        if(target=='mother') {
                            if(response.person){
                                $rootScope.mother    = response.person;

                                if($rootScope.mother.birthday =="0000-00-00" || $rootScope.mother.birthday == null){
                                    $rootScope.mother.birthday    = new Date();
                                }else{
                                    $rootScope.mother.birthday    = new Date($rootScope.mother.birthday);
                                }
                            }else{
                                $rootScope.mother    ={};
                                $rootScope.mother.id_card_number =id;
                                $rootScope.mother.id=null;
                                $rootScope.mother.person_id=null;
                                $rootScope.mother_error=false;
                                $rootScope.msg10="";
                            }

                            if(response.msg){
                                $rootScope.toastrMessages('error',response.msg);
                            }


                        }

                        else{
                            if(response.person){
                                $scope.father    = response.person;

                                if($scope.father.birthday =="0000-00-00" || $scope.father.birthday == null){
                                    $scope.father.birthday    = new Date();
                                }else{
                                    $scope.father.birthday    = new Date($scope.father.birthday);
                                }
                            }else{
                                $scope.father    ={};
                                $scope.father.id_card_number ='';
                            }
                            if(response.msg){
                                $rootScope.toastrMessages('error',response.msg);
                            }else{
                                $rootScope.clearToastr();
                            }

                            $scope.father.id=null;
                            $scope.father.person_id=null;


                        }
                    }


                });
                }
                else{
                    if(target=='mother') {
                        $rootScope.mother    ={};
                        $rootScope.mother.id_card_number =id;
                        $rootScope.mother.id=null;
                        $rootScope.mother.person_id=null;
                        $rootScope.mother_error=false;
                        $rootScope.msg10="";
                    }
                    else{
                        $scope.father    ={};
                        $scope.father.id=null;
                        $scope.father.person_id=null;
                    }
                    $rootScope.toastrMessages('error',$filter('translate')('invalid card number'));
                }
             }
            else{
                params.person_id=id;
                params.target='info';

                $rootScope.progressbar_start();
                persons.getPersonReports(params,function(response){
                    $rootScope.progressbar_complete();
                    if(target=='mother'){
                        $rootScope.mother=response;
                        if($rootScope.has_mother !=false) {
                            $rootScope.mother_id=$rootScope.mother.id;
                        }

                        if($rootScope.mother.death_date == null){
                            $rootScope.mother.alive = 0+"";
                            $rootScope.mother.death_cause_id ="";
                            $rootScope.mother.death_date="";
                        }else{
                            $rootScope.mother.alive =1+"";
                            $rootScope.mother.death_cause_id = $rootScope.mother.death_cause_id+"";
                        }

                        if($rootScope.mother.monthly_income==null){
                            $rootScope.mother.monthly_income = "";
                        }


                        if($rootScope.mother.stage==null){
                            $rootScope.mother.stage="";
                        }
                        else{
                            $rootScope.mother.stage = $rootScope.mother.stage+"";
                        }


                        $rootScope.mother.working = $rootScope.mother.working+"";
                        if($rootScope.mother.working == 1){
                            if( $rootScope.mother.work_job_id==null){
                                $rootScope.mother.work_job_id="";
                            }
                            else{
                                $rootScope.mother.work_job_id = $rootScope.mother.work_job_id+"";
                            }
                        }else{
                            $rootScope.mother.work_job_id="";
                            $rootScope.mother.work_location="";
                        }

                        if($rootScope.mother.marital_status_id==null){
                            $rootScope.mother.marital_status_id="";
                        }
                        else{
                            $rootScope.mother.marital_status_id = $rootScope.mother.marital_status_id+"";
                        }

                        if($rootScope.mother.condition == null ){
                            $rootScope.mother.condition="";
                            $rootScope.mother.disease_id="";
                            $rootScope.mother.details="";
                        }else{
                            $rootScope.mother.condition=$rootScope.mother.condition+"";
                            if($rootScope.mother.condition == 2 || $rootScope.mother.condition == 3 ||
                                $rootScope.mother.condition === '2' || $rootScope.mother.condition === '3' ){

                                if( $rootScope.mother.disease_id==null){
                                    $rootScope.mother.disease_id="";
                                }
                                else{
                                    $rootScope.mother.disease_id = $rootScope.mother.disease_id+"";
                                }
                            }else{
                                $rootScope.mother.disease_id="";
                                $rootScope.mother.details="";
                            }
                        }
                    }else{
                        $scope.father=response;
                        $rootScope.father_id=$scope.father.id;

                        if($scope.father.death_cause_id !=null){
                            $scope.father.death_cause_id = $scope.father.death_cause_id+"";
                        }

                        if($scope.father.degree==null){
                            $scope.father.degree="";
                        }
                        else{
                            $scope.father.degree = $scope.father.degree+"";
                        }

                        if($scope.father.working == null ){
                            $scope.father.working="";
                            $scope.father.work_job_id="";
                            $scope.father.work_location="";
                        }else{
                            $scope.father.working=$scope.father.working+"";
                            if($scope.father.working == 1){
                                $scope.father.work_job_id = $scope.father.work_job_id+"";
                            }else{
                                $scope.father.work_location="";
                            }
                        }

                        if( $scope.father.study == null){
                            $scope.father.study = "";
                            $scope.father.degree="";
                            $scope.father.specialization="";
                        }else{
                            $scope.father.study = $scope.father.study+"";

                            if($scope.father.study == 0){
                                $scope.father.degree="";
                                $scope.father.specialization="";
                            }else{
                                if($scope.father.degree==null){
                                    $scope.father.degree="";
                                }
                                else{
                                    $scope.father.degree = $scope.father.degree+"";
                                }
                            }
                        }
                        if($scope.father.condition == null ){
                            $scope.father.condition="";
                            $scope.father.disease_id="";
                            $scope.father.details="";
                        }else{
                            $scope.father.condition=$scope.father.condition+"";
                            if($scope.father.condition == 2 || $scope.father.condition == 3 ||
                                $scope.father.condition === '2' || $scope.father.condition === '3' ){
                                if( $scope.father.disease_id==null){
                                    $scope.father.disease_id="";
                                }
                                else{
                                    $scope.father.disease_id = $scope.father.disease_id+"";
                                }
                            }else{
                                $scope.father.disease_id="";
                                $scope.father.details="";
                            }
                        }
                        setWives($scope.father.wives);
                    }
                });
            }
        };

        if( !angular.isUndefined($rootScope.father_id)) {
            if($rootScope.father_id != '' && $rootScope.father_id !=  null&& $rootScope.father_id !=  'null'){
                find('father','get',$rootScope.father_id);
                if($rootScope.has_Wives == false){
                    if( !angular.isUndefined($rootScope.mother_id)) {
                        if($rootScope.mother_id != '' || $rootScope.mother_id !=  null){
                            find('mother','get',$rootScope.mother_id);

                        }
                    }
                }
            }
        }

        category.getPriority({'type':'sponsorships','category_id':$rootScope.category_id ,'target':'father'},function (response) {
            $scope.fpriority=response.priority;
        });
        category.getPriority({'type':'sponsorships','category_id':$rootScope.category_id ,'target':'mother'},function (response) {
            $rootScope.mpriority=response.priority;
        });

        Entity.get({'entity':'entities' , 'c':'educationDegrees,educationStages,workJobs,diseases,maritalStatus,deathCauses,countries'},function (response){
            $rootScope.EduDegrees = response.educationDegrees;
            $rootScope.EduStages = response.educationStages;
            $rootScope.WorkJob = response.workJobs;
            $rootScope.Diseases = response.diseases;
            $rootScope.MaritalStatus = response.maritalStatus;
            $rootScope.DeathCauses = response.deathCauses;
            $rootScope.Country = response.countries;
        });
// -----------------------------------------------------------------------------------------------------
        $scope.save=function(action,data){
            $rootScope.clearToastr();
            $rootScope.success_1=false;
            $rootScope.success_2=false;
            $rootScope.success_3=false;
            $rootScope.success_4=false;

            if( !angular.isUndefined(data.death_date)) {
                data.death_date=$filter('date')(data.death_date, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(data.birthday)) {
                data.birthday= $filter('date')(data.birthday, 'dd-MM-yyyy')
            }
            if(action =='father'){
                data.descendant= true;
                data.target='father';
                data.category_id= $rootScope.category_id;
                // if($rootScope.father_dead == true){
                //     data.live=1;
                // }else{
                //     data.live=0;
                // }
                data.priority=$scope.fpriority;

                if(data.id){
                    data.person_id=data.id;
                    delete data.id;
                }

                $rootScope.progressbar_start();
                persons.save(data,function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status=='failed_valid')
                    {
                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                        $scope.status1 =response.status;
                        $scope.msg1 =response.msg;
                    } else if(response.status== 'success'){
                        $scope.father.id   = response.person.id;
                        $rootScope.father_id   = response.person.id;
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        if($rootScope.has_Wives != false && $rootScope.load_not_inserted == true){
                            loadWife();
                        }
                    }
                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
               });

            }
            else{
                data.husband_id=$rootScope.father_id;
                data.target='single';
                data.priority=$rootScope.mpriority;

                if(data.id){
                    data.person_id=data.id;
                    delete data.id;
                }

                $rootScope.progressbar_start();
                persons.save(data,function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status=='failed_valid')
                    {
                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                        $scope.status1 =response.status;
                        $scope.msg2 =response.msg;
                    } else if(response.status== 'success'){
                        $rootScope.mother.id   = response.person.id;
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        if($rootScope.has_mother ==true){
                            $rootScope.mother_id   = response.person.id;
                        }
                    }
                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });
            }
        };
        $scope.forward=function(){
            $rootScope.clearToastr();
            var get=-1;
            if($rootScope.has_mother ==false){
                if( $rootScope.father_id != null && $rootScope.father_id != "") {
                    $rootScope.clearToastr();
                    get =$rootScope.FormSponsorshipSection[$rootScope.TheCurrentStep].id;
                    angular.element('#'+get).removeClass("disabled");
                    $rootScope.Next();

                }else{
                    $rootScope.toastrMessages('error', $filter('translate')('mother or father data not saved You must enter at least father data'));

                }
            }else{
                if( $rootScope.father_id != null && $rootScope.mother_id != null && $rootScope.father_id != "" && $rootScope.mother_id != "") {
                    $rootScope.clearToastr();
                    angular.element('#'+$rootScope.FormSponsorshipSection[$rootScope.TheCurrentStep-1].id).removeClass("disabled");
                    angular.element('#'+$rootScope.FormSponsorshipSection[$rootScope.TheCurrentStep].id).removeClass("disabled");
                    $rootScope.Next();

                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('mother or father Data Not Reserved: You must enter the parent data and specify the mother if necessary before proceeding to the next step'));

                }
            }
        };
// -----------------------------------------------------------------------------------------------------
        $scope.isExists = function(whoIs,id_card_number){
            $rootScope.clearToastr();
            if(whoIs =='father'){
                $rootScope.wives_not_inserted =[];
                $rootScope.items=[];
                if( !angular.isUndefined($scope.msg1.id_card_number)) {
                   $scope.msg1.id_card_number = [];
               }
            }else{
               if( !angular.isUndefined($scope.msg2.id_card_number)) {
                   $scope.msg2.id_card_number = [];
               }
           }

           if(!angular.isUndefined(id_card_number)) {
                if(id_card_number.length==9) {
                    find(whoIs,'find',id_card_number);
                }
            }
        };
// -----------------------------------------------------------------------------------------------------
        $scope.Wife =function(size,id){
            $rootScope.clearToastr();
            $rootScope.success_1=false;
            $rootScope.success_2=false;
            $rootScope.success_3=false;
            $rootScope.success_4=false;

            $uibModal.open({
                templateUrl: 'addWife.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size ,
                controller: function ($rootScope,$scope,$stateParams, $modalInstance) {
                    $rootScope.mother_error=false;
                    $rootScope.msg10="";

                    var setWife=function(data){
                        $rootScope.mother=data;
                        if($rootScope.mother.death_date == null){
                            $rootScope.mother.alive = 0+"";
                            $rootScope.mother.death_cause_id ="";
                            $rootScope.mother.death_date="";
                        }else{
                            $rootScope.mother.alive =1+"";
                            $rootScope.mother.death_cause_id = $rootScope.mother.death_cause_id+"";
                        }
                        if($rootScope.mother.monthly_income==null){
                            $rootScope.mother.monthly_income = "";
                        }

                        if($rootScope.mother.study == null){
                            $rootScope.mother.study = "";
                            $rootScope.mother.stage="";
                            $rootScope.mother.specialization ="";
                        }else{
                            $rootScope.mother.study = $rootScope.mother.study+"";

                            if($rootScope.mother.study == 0){
                                $rootScope.mother.stage="";
                                $rootScope.mother.specialization ="";
                            }else{
                                if($rootScope.mother.stage==null){
                                    $rootScope.mother.stage="";
                                } else{
                                    $rootScope.mother.stage = $rootScope.mother.stage+"";
                                }
                            }
                        }

                        $rootScope.mother.working = $rootScope.mother.working+"";
                        if($rootScope.mother.working == 1){
                            if( $rootScope.mother.work_job_id==null){
                                $rootScope.mother.work_job_id="";
                            }
                            else{
                                $rootScope.mother.work_job_id = $rootScope.mother.work_job_id+"";
                            }
                        }else{
                            $rootScope.mother.work_job_id="";
                            $rootScope.mother.work_location="";
                        }
                        if($rootScope.mother.marital_status_id==null){
                            $rootScope.mother.marital_status_id="";
                        } else{
                            $rootScope.mother.marital_status_id = $rootScope.mother.marital_status_id+"";
                        }
                        if($rootScope.mother.condition == null ){
                            $rootScope.mother.condition="";
                            $rootScope.mother.disease_id="";
                            $rootScope.mother.details="";
                        }else{
                            $rootScope.mother.condition=$rootScope.mother.condition+"";
                            if($rootScope.mother.condition == 2 || $rootScope.mother.condition == 3 ||
                                $rootScope.mother.condition === '2' || $rootScope.mother.condition === '3' ){

                                if( $rootScope.mother.disease_id==null){
                                    $rootScope.mother.disease_id="";
                                }
                                else{
                                    $rootScope.mother.disease_id = $rootScope.mother.disease_id+"";
                                }
                            }else{
                                $rootScope.mother.disease_id="";
                                $rootScope.mother.details="";
                            }
                        }
                    };
                    if(id ==-1){
                        $scope.action ='add';
                        $rootScope.mother={};
                    }else{
                        $scope.action ='edit';
                        persons.getPersonReports({'person_id':id ,'target':'info', 'mode':'edit','person' :true, 'persons_i18n' : true,
                                                  'health' : true,  'education' : true, 'work' : true},function (response) {
                            setWife(response);
                        });
                    }

                    $scope.isExists = function(whoIs,id_card_number){
                        $rootScope.clearToastr();
                        if( !angular.isUndefined($scope.msg2.id_card_number)) {
                            $scope.msg2.id_card_number = [];
                        }

                        if(!angular.isUndefined(id_card_number)) {
                            if(id_card_number.length==9) {
                                if($rootScope.check_id(id_card_number)){
                                    var params={'id_card_number':id_card_number,'mode':'edit', 'person' :true, 'persons_i18n' : true, 'education' : true,'health' : true, 'work' : true};
                                    $rootScope.progressbar_start();
                                    persons.find(params,function(response){
                                    $rootScope.progressbar_complete();
                                    if(response.status == true) {
                                        $rootScope.toastrMessages('error',response.msg);
                                        $scope.action ='edit';
                                        setWife(response.person);
                                    }else{
                                        $scope.action ='add';
                                        if(response.person){
                                            $rootScope.mother    = response.person;

                                            if($rootScope.mother.birthday =="0000-00-00" || $rootScope.mother.birthday == null){
                                                $rootScope.mother.birthday    = new Date();
                                            }else{
                                                $rootScope.mother.birthday    = new Date($rootScope.mother.birthday);
                                            }
                                        }

                                        if(response.msg){
                                            $rootScope.toastrMessages('error',response.msg);
                                            $rootScope.mother    ={};
                                            $rootScope.mother.id_card_number =id_card_number;
                                        }else{
                                            $rootScope.mother_error=false;
                                            $rootScope.msg10="";
                                        }
                                        $rootScope.mother.id=null;
                                        $rootScope.mother.person_id=null;
                                    }


                                });
                                }else{
                                    $rootScope.mother    ={};
                                    $rootScope.mother.id_card_number =id_card_number;

                                    $rootScope.toastrMessages('error',$filter('translate')('invalid card number'));
                                }
                            }
                        }
                    };
                    $scope.save = function (data) {

                        $rootScope.clearToastr();
                        if( !angular.isUndefined(data.death_date)) {
                            data.death_date=$filter('date')(data.death_date, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(data.birthday)) {
                            data.birthday= $filter('date')(data.birthday, 'dd-MM-yyyy')
                        }
                        data.husband = $rootScope.father_id;
                        data.target='wives';
                        data.priority=$rootScope.mpriority;

                        if(data.id == ""){
                            delete data.id
                        }

                        data.husband_id=$rootScope.father_id;
                        data.target='single';
                        data.priority=$rootScope.mpriority;

                        if(data.id){
                            data.person_id=data.id;
                            delete data.id;
                        }

                        $rootScope.progressbar_start();
                        persons.save(data,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg2 =response.msg;
                            } else if(response.status== 'success'){
                                $rootScope.mode= $scope.action;

                                if($scope.action == 'add'){
                                    $rootScope.toastrMessages('success',$filter('translate')('The row is inserted to db'));
                                }else{
                                    $rootScope.toastrMessages('success',$filter('translate')('The row is updated'));
                                }
                                $rootScope.mother.id   = response.mother_id;
                                loadWife();
                                $modalInstance.close();
                            }
                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                        });



                       };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.checkDates  = function(source,target,birthday,death_date){

                        var curDateFilter=false;

                        if(source =='father'){
                            if(target == 'birthday'){
                                if( !angular.isUndefined($scope.msg1.birthday)) {
                                    $scope.msg1.birthday = [];
                                }
                            }else{
                                if( !angular.isUndefined($scope.msg1.death_date)) {
                                    $scope.msg1.death_date = [];
                                }
                            }
                        }else{
                            if(target == 'birthday'){
                                if( !angular.isUndefined($scope.msg2.birthday)) {
                                    $scope.msg2.birthday = [];
                                }
                            }else{
                                if( !angular.isUndefined($scope.msg2.death_date)) {
                                    $scope.msg2.death_date = [];
                                }
                            }
                        }

                        if(target == 'birthday'){
                            if(!angular.isUndefined(death_date) && death_date != null){
                                if(new Date(birthday) > new Date(death_date) ){

                                    if(source =='father') {
                                        $scope.msg1.birthday = {0 :$filter('translate')('The date of birth must be older than the date of death')};
                                    }else{
                                        $scope.msg2.birthday = {0 :$filter('translate')('The date of birth must be older than the date of death')};
                                    }

                                    $scope.status1 ='failed_valid';
                                }else{
                                    if(source =='father') {
                                        $scope.msg1.death_date = {};
                                    }else{
                                        $scope.msg2.death_date = {};
                                    }
                                }
                            }
                            if(angular.isUndefined(death_date) || death_date == null){
                                if(new Date(birthday) > new Date() ){
                                    if(source =='father') {
                                        $scope.msg1.birthday = {0 :$filter('translate')('The date of birth must be older than current date')};
                                    }else{
                                        $scope.msg2.birthday = {0 :$filter('translate')('The date of birth must be older than current date')};
                                    }
                                    $scope.status1 ='failed_valid';
                                }else{
                                    if(source =='father') {
                                        $scope.msg1.death_date = {};
                                    }else{
                                        $scope.msg2.death_date = {};
                                    }
                                }
                            }
                        }else if(target == 'death_date'){
                            if(angular.isUndefined(birthday) || birthday == null){
                                if(new Date() < new Date(death_date) ){
                                    if(source =='father') {
                                        $scope.msg1.death_date = {0: $filter('translate')('The date of death must be older than current date')};
                                    }else{
                                        $scope.msg2.death_date = {0: $filter('translate')('The date of death must be older than current date')};
                                    }
                                    $scope.status1 ='failed_valid';
                                }else{
                                    if(source =='father') {
                                        $scope.msg1.death_date = {};
                                    }else{
                                        $scope.msg2.death_date = {};
                                    }
                                }
                            }
                            else if(!angular.isUndefined(birthday) && birthday != null){
                                if(new Date(birthday) > new Date(death_date) ){
                                    if(source =='father') {
                                        $scope.msg1.death_date = {0 :$filter('translate')('The date of birth must be older than the date of death')};
                                    }else{
                                        $scope.msg2.death_date = {0 :$filter('translate')('The date of birth must be older than the date of death')};
                                    }
                                    $scope.status1 ='failed_valid';
                                }else{
                                    if(source =='father') {
                                        $scope.msg1.death_date = {};
                                    }else{
                                        $scope.msg2.death_date = {};
                                    }
                                }
                            }
                        }
                    };
                    $scope.checkBirthday  = function(target,birthday){
                        if( !angular.isUndefined($scope.msg1.birthday)) {
                            $scope.msg1.birthday = [];
                        }

                        var death_date=$rootScope.mother.death_date;

                        if( !angular.isUndefined(death_date)) {
                            if(new Date(birthday) > new Date(death_date)){
                                $scope.msg2.birthday = {0 :$filter('translate')('The date of birth must be older than the date of death')};
                                $scope.status1 ='failed_valid';
                            }else{
                                $scope.msg2.birthday = {};
                            }
                        }else{
                            var curDate = new Date();
                            if(new Date(birthday) > curDate){
                                $scope.msg2.birthday = {0 :$filter('translate')('The specified date must be older than the current date')};
                                $scope.status1 ='failed_valid';
                            }else{
                                $scope.msg2.birthday = {};
                            }

                        }

                    };

                }});
        };
        $scope.confirmAdd =function(row,index,id){

            $rootScope.clearToastr();
            $rootScope.success_1=false;
            $rootScope.success_2=false;
            $rootScope.success_3=false;
            $rootScope.success_4=false;
            $rootScope.disable_=true;

            if(id == null){
                var data = angular.copy(row);
                if( !angular.isUndefined(data.death_date)) {
                    data.death_date=$filter('date')(data.death_date, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(data.birthday)) {
                    data.birthday= $filter('date')(data.birthday, 'dd-MM-yyyy')
                }

                if(data.death_date == null){
                    data.alive = 0+"";
                    data.death_cause_id ="";
                    data.death_date="";
                }else{
                    data.alive =1+"";
                    data.death_cause_id = data.death_cause_id+"";
                }

                if(data.marital_status_id==null){
                    data.marital_status_id="";
                } else{
                    data.marital_status_id = data.marital_status_id+"";
                }

            }
            $uibModal.open({
                templateUrl: 'addWife.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg' ,
                controller: function ($rootScope,$scope,$stateParams, $modalInstance) {
                    $rootScope.mother_error=false;
                    $rootScope.msg10="";
                    $rootScope.mother={};

                    var setWife=function(data){
                        $rootScope.mother=data;
                        if($rootScope.mother.death_date == null){
                            $rootScope.mother.alive = 0+"";
                            $rootScope.mother.death_cause_id ="";
                            $rootScope.mother.death_date="";
                        }else{
                            $rootScope.mother.alive =1+"";
                            $rootScope.mother.death_cause_id = $rootScope.mother.death_cause_id+"";
                        }
                        if($rootScope.mother.monthly_income==null){
                            $rootScope.mother.monthly_income = "";
                        }

                        if($rootScope.mother.study == null){
                            $rootScope.mother.study = "";
                            $rootScope.mother.stage="";
                            $rootScope.mother.specialization ="";
                        }else{
                            $rootScope.mother.study = $rootScope.mother.study+"";

                            if($rootScope.mother.study == 0){
                                $rootScope.mother.stage="";
                                $rootScope.mother.specialization ="";
                            }else{
                                if($rootScope.mother.stage==null){
                                    $rootScope.mother.stage="";
                                } else{
                                    $rootScope.mother.stage = $rootScope.mother.stage+"";
                                }
                            }
                        }

                        $rootScope.mother.working = $rootScope.mother.working+"";
                        if($rootScope.mother.working == 1){
                            if( $rootScope.mother.work_job_id==null){
                                $rootScope.mother.work_job_id="";
                            }
                            else{
                                $rootScope.mother.work_job_id = $rootScope.mother.work_job_id+"";
                            }
                        }else{
                            $rootScope.mother.work_job_id="";
                            $rootScope.mother.work_location="";
                        }
                        if($rootScope.mother.marital_status_id==null){
                            $rootScope.mother.marital_status_id="";
                        } else{
                            $rootScope.mother.marital_status_id = $rootScope.mother.marital_status_id+"";
                        }
                        if($rootScope.mother.condition == null ){
                            $rootScope.mother.condition="";
                            $rootScope.mother.disease_id="";
                            $rootScope.mother.details="";
                        }else{
                            $rootScope.mother.condition=$rootScope.mother.condition+"";
                            if($rootScope.mother.condition == 2 || $rootScope.mother.condition == 3 ||
                                $rootScope.mother.condition === '2' || $rootScope.mother.condition === '3' ){

                                if( $rootScope.mother.disease_id==null){
                                    $rootScope.mother.disease_id="";
                                }
                                else{
                                    $rootScope.mother.disease_id = $rootScope.mother.disease_id+"";
                                }
                            }else{
                                $rootScope.mother.disease_id="";
                                $rootScope.mother.details="";
                            }
                        }
                    };
                    if(id == null){
                        $rootScope.mother=data;
                        $scope.action ='add';
                    }else{
                        $scope.action ='edit';
                        persons.getPersonReports({'person_id':id ,'target':'info', 'mode':'edit','person' :true, 'persons_i18n' : true,
                            'health' : true,  'education' : true, 'work' : true},function (response) {
                            setWife(response);
                        });
                    }

                    $scope.save = function (data) {
                        $rootScope.clearToastr();

                        if( !angular.isUndefined(data.death_date)) {
                            data.death_date=$filter('date')(data.death_date, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(data.birthday)) {
                            data.birthday= $filter('date')(data.birthday, 'dd-MM-yyyy')
                        }
                        data.husband = $rootScope.father_id;
                        data.husband_id=$rootScope.father_id;
                        data.target='wives';
                        // data.target='single';

                        data.priority=$rootScope.mpriority;

                        if(data.id){
                            data.person_id=data.id;
                            delete data.id;
                        }

                        $rootScope.progressbar_start();
                        persons.save(data,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg2 =response.msg;
                            } else if(response.status== 'success'){
                                $rootScope.mode= $scope.action;
                                if($scope.action == 'add'){
                                    $rootScope.toastrMessages('success',$filter('translate')('The row is inserted to db'));
                                }else{
                                    $rootScope.toastrMessages('success',$filter('translate')('The row is updated'));
                                }
                                $rootScope.mother.id   = response.mother_id;
                                $rootScope.wives_not_inserted.splice(index, 1);
                                $rootScope.load_not_inserted= false;
                                loadWife();
                                $modalInstance.close();
                            }
                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                        });



                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.checkDates  = function(source,target,birthday,death_date){

                        var curDateFilter=false;

                        if(source =='father'){
                            if(target == 'birthday'){
                                if( !angular.isUndefined($scope.msg1.birthday)) {
                                    $scope.msg1.birthday = [];
                                }
                            }else{
                                if( !angular.isUndefined($scope.msg1.death_date)) {
                                    $scope.msg1.death_date = [];
                                }
                            }
                        }else{
                            if(target == 'birthday'){
                                if( !angular.isUndefined($scope.msg2.birthday)) {
                                    $scope.msg2.birthday = [];
                                }
                            }else{
                                if( !angular.isUndefined($scope.msg2.death_date)) {
                                    $scope.msg2.death_date = [];
                                }
                            }
                        }

                        if(target == 'birthday'){
                            if(!angular.isUndefined(death_date) && death_date != null){
                                if(new Date(birthday) > new Date(death_date) ){

                                    if(source =='father') {
                                        $scope.msg1.birthday = {0 :$filter('translate')('The date of birth must be older than the date of death')};
                                    }else{
                                        $scope.msg2.birthday = {0 :$filter('translate')('The date of birth must be older than the date of death')};
                                    }

                                    $scope.status1 ='failed_valid';
                                }else{
                                    if(source =='father') {
                                        $scope.msg1.death_date = {};
                                    }else{
                                        $scope.msg2.death_date = {};
                                    }
                                }
                            }
                            if(angular.isUndefined(death_date) || death_date == null){
                                if(new Date(birthday) > new Date() ){
                                    if(source =='father') {
                                        $scope.msg1.birthday = {0 :$filter('translate')('The date of birth must be older than current date')};
                                    }else{
                                        $scope.msg2.birthday = {0 :$filter('translate')('The date of birth must be older than current date')};
                                    }
                                    $scope.status1 ='failed_valid';
                                }else{
                                    if(source =='father') {
                                        $scope.msg1.death_date = {};
                                    }else{
                                        $scope.msg2.death_date = {};
                                    }
                                }
                            }
                        }else if(target == 'death_date'){
                            if(angular.isUndefined(birthday) || birthday == null){
                                if(new Date() < new Date(death_date) ){
                                    if(source =='father') {
                                        $scope.msg1.death_date = {0: $filter('translate')('The date of death must be older than current date')};
                                    }else{
                                        $scope.msg2.death_date = {0: $filter('translate')('The date of death must be older than current date')};
                                    }
                                    $scope.status1 ='failed_valid';
                                }else{
                                    if(source =='father') {
                                        $scope.msg1.death_date = {};
                                    }else{
                                        $scope.msg2.death_date = {};
                                    }
                                }
                            }
                            else if(!angular.isUndefined(birthday) && birthday != null){
                                if(new Date(birthday) > new Date(death_date) ){
                                    if(source =='father') {
                                        $scope.msg1.death_date = {0 :$filter('translate')('The date of birth must be older than the date of death')};
                                    }else{
                                        $scope.msg2.death_date = {0 :$filter('translate')('The date of birth must be older than the date of death')};
                                    }
                                    $scope.status1 ='failed_valid';
                                }else{
                                    if(source =='father') {
                                        $scope.msg1.death_date = {};
                                    }else{
                                        $scope.msg2.death_date = {};
                                    }
                                }
                            }
                        }
                    };
                    $scope.checkBirthday  = function(target,birthday){
                        if( !angular.isUndefined($scope.msg1.birthday)) {
                            $scope.msg1.birthday = [];
                        }

                        var death_date=$rootScope.mother.death_date;

                        if( !angular.isUndefined(death_date)) {
                            if(new Date(birthday) > new Date(death_date)){
                                $scope.msg2.birthday = {0 :$filter('translate')('The date of birth must be older than the date of death')};
                                $scope.status1 ='failed_valid';
                            }else{
                                $scope.msg2.birthday = {};
                            }
                        }else{
                            var curDate = new Date();
                            if(new Date(birthday) > curDate){
                                $scope.msg2.birthday = {0 :$filter('translate')('The specified date must be older than the current date')};
                                $scope.status1 ='failed_valid';
                            }else{
                                $scope.msg2.birthday = {};
                            }

                        }

                    };

                }});
        };
        $scope.setMother  = function(status,index,id){
            var l=-1;
            var pr=-1;
            angular.forEach($rootScope.items, function(val,key) {
                if(val.check){
                    l=key;
                    pr= l.r_person_id;
                }
            });

            if(pr !=-1){
                if(pr != id){
                    $rootScope.items[l].check=false;
                    $rootScope.items[index].check=true;
                    //$rootScope.mother_id=id;
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                }
            }else{
                $rootScope.items[index].check=true;
                //$rootScope.mother_id=id;
                $rootScope.toastrMessages('success',$filter('translate')('action success'));
            }


        };
// -----------------------------------------------------------------------------------------------------
        $scope.checkDates  = function(source,target,birthday,death_date){

            var curDateFilter=false;

            if(source =='father'){
                if(target == 'birthday'){
                    if( !angular.isUndefined($scope.msg1.birthday)) {
                       $scope.msg1.birthday = [];
                    }
                }else{
                    if( !angular.isUndefined($scope.msg1.death_date)) {
                        $scope.msg1.death_date = [];
                    }
                }
            }else{
                if(target == 'birthday'){
                    if( !angular.isUndefined($scope.msg2.birthday)) {
                        $scope.msg2.birthday = [];
                    }
                }else{
                    if( !angular.isUndefined($scope.msg2.death_date)) {
                        $scope.msg2.death_date = [];
                    }
                }
            }

            if(target == 'birthday'){
               if(!angular.isUndefined(death_date) && death_date != null){
                    if(new Date(birthday) > new Date(death_date) ){

                        if(source =='father') {
                            $scope.msg1.birthday = {0 :$filter('translate')('The date of birth must be older than the date of death')};
                        }else{
                            $scope.msg2.birthday = {0 :$filter('translate')('The date of birth must be older than the date of death')};
                        }

                        $scope.status1 ='failed_valid';
                    }else{
                        if(source =='father') {
                            $scope.msg1.death_date = {};
                        }else{
                            $scope.msg2.death_date = {};
                        }
                    }
                }
                if(angular.isUndefined(death_date) || death_date == null){
                    if(new Date(birthday) > new Date() ){
                        if(source =='father') {
                            $scope.msg1.birthday = {0 :$filter('translate')('The date of birth must be older than current date')};
                        }else{
                            $scope.msg2.birthday = {0 :$filter('translate')('The date of birth must be older than current date')};
                        }
                        $scope.status1 ='failed_valid';
                    }else{
                        if(source =='father') {
                            $scope.msg1.death_date = {};
                        }else{
                            $scope.msg2.death_date = {};
                        }
                    }
                }
            }else if(target == 'death_date'){
                if(angular.isUndefined(birthday) || birthday == null){
                    if(new Date() < new Date(death_date) ){
                        if(source =='father') {
                            $scope.msg1.death_date = {0: $filter('translate')('The date of death must be older than current date')};
                        }else{
                            $scope.msg2.death_date = {0: $filter('translate')('The date of death must be older than current date')};
                        }
                        $scope.status1 ='failed_valid';
                    }else{
                        if(source =='father') {
                            $scope.msg1.death_date = {};
                        }else{
                            $scope.msg2.death_date = {};
                        }
                    }
                }
                else if(!angular.isUndefined(birthday) && birthday != null){
                    if(new Date(birthday) > new Date(death_date) ){
                        if(source =='father') {
                            $scope.msg1.death_date = {0 :$filter('translate')('The date of birth must be older than the date of death')};
                        }else{
                            $scope.msg2.death_date = {0 :$filter('translate')('The date of birth must be older than the date of death')};
                        }
                        $scope.status1 ='failed_valid';
                    }else{
                        if(source =='father') {
                            $scope.msg1.death_date = {};
                        }else{
                            $scope.msg2.death_date = {};
                        }
                    }
                }
            }
        };
        $rootScope.rest = function(via,target,value){

            if(target =='father'){

                if(via=='working') {
                    if( !angular.isUndefined($scope.msg1.working)) {
                        $scope.msg1.working = [];
                    }
                    if( !angular.isUndefined($scope.msg1.work_job_id)) {
                        $scope.msg1.work_job_id = [];
                    }
                    if( !angular.isUndefined($scope.msg1.work_location)) {
                        $scope.msg1.work_location = [];
                    }
                    if( !angular.isUndefined($scope.msg1.monthly_income)) {
                        $scope.msg1.monthly_income = [];
                    }
                    $scope.father.work_job_id="";
                    $scope.father.work_location="";
                    $scope.father.monthly_income="";
                }
                else if( via =='study'){
                    if( !angular.isUndefined($scope.msg1.study)) {
                        $scope.msg1.study = [];
                    }

                    if(value == 0){
                        if( !angular.isUndefined($scope.msg1.stage)) {
                            $scope.msg1.stage = [];
                        }

                        if( !angular.isUndefined($scope.msg1.specialization)) {
                            $scope.msg1.specialization = [];
                        }

                        if( !angular.isUndefined($scope.msg1.degree)) {
                            $scope.msg1.degree = [];
                        }

                        $scope.father.stage="";
                        $scope.father.degree="";
                        $scope.father.specialization="";

                    }
                }
                else if( via =='health_status'){
                    if( !angular.isUndefined($scope.msg1.condition)) {
                        $scope.msg1.condition = [];
                    }

                    if (value != 2 || value != 3) {
                        if (!angular.isUndefined($scope.msg1.disease_id)) {
                            $scope.msg1.disease_id = [];
                        }
                        if (!angular.isUndefined($scope.msg1.details)) {
                            $scope.msg1.details = [];
                        }

                        $scope.father.disease_id = "";
                        $scope.father.details = "";
                    }
                }
                else if( via =='alive'){
                    if (!angular.isUndefined($scope.msg1.alive)) {
                        $scope.msg1.alive = [];
                    }

                    if (value == 0) {
                        if (!angular.isUndefined($scope.msg1.death_date)) {
                            $scope.msg1.death_date = [];
                        }
                        if (!angular.isUndefined($rootScope.msg2.death_cause_id)) {
                            $scope.msg1.death_cause_id = [];
                        }

                        $scope.father.death_date = "";
                        $scope.father.death_cause_id = "";
                    }else{

                        if( !angular.isUndefined($scope.msg1.condition)) {
                            $scope.msg1.condition = [];
                        }
                        if (!angular.isUndefined($scope.msg1.disease_id)) {
                            $scope.msg1.disease_id = [];
                        }
                        if (!angular.isUndefined($scope.msg1.details)) {
                            $scope.msg1.details = [];
                        }

                        $scope.father.condition = "";
                        $scope.father.disease_id = "";
                        $scope.father.details = "";
                    }

                }
            }else{
                if(via=='working') {
                    if( !angular.isUndefined($scope.msg2.working)) {
                        $scope.msg2.working = [];
                    }
                    if( !angular.isUndefined($scope.msg2.work_job_id)) {
                        $scope.msg2.work_job_id = [];
                    }
                    if( !angular.isUndefined($scope.msg2.work_location)) {
                        $scope.msg2.work_location = [];
                    }
                    if( !angular.isUndefined($scope.msg2.monthly_income)) {
                        $scope.msg2.monthly_income = [];
                    }
                    $rootScope.mother.work_job_id="";
                    $rootScope.mother.work_location="";
                    $rootScope.mother.monthly_income="";
                }
                else if( via =='study'){
                    if( !angular.isUndefined($scope.msg2.study)) {
                        $scope.msg2.study = [];
                    }

                    if(value == 0){
                        if( !angular.isUndefined($scope.msg2.stage)) {
                            $scope.msg2.stage = [];
                        }

                        if( !angular.isUndefined($scope.msg2.specialization)) {
                            $scope.msg2.specialization = [];
                        }

                        if( !angular.isUndefined($scope.msg2.degree)) {
                            $scope.msg2.degree = [];
                        }

                        $rootScope.mother.stage="";
                        $rootScope.mother.degree="";
                        $rootScope.mother.specialization="";

                    }
                }
                else if( via =='health_status'){
                    if( !angular.isUndefined($scope.msg2.condition)) {
                        $scope.msg2.condition = [];
                    }

                    if (value != 2 || value != 3) {
                        if (!angular.isUndefined($scope.msg2.disease_id)) {
                            $scope.msg2.disease_id = [];
                        }
                        if (!angular.isUndefined($scope.msg2.details)) {
                            $scope.msg2.details = [];
                        }

                        $rootScope.mother.disease_id = "";
                        $rootScope.mother.details = "";
                    }
                }
                else if( via =='alive'){
                    if (!angular.isUndefined($scope.msg2.alive)) {
                        $scope.msg2.alive = [];
                    }

                    if (value == 0) {
                        if (!angular.isUndefined($scope.msg2.death_date)) {
                            $scope.msg2.death_date = [];
                        }
                        if (!angular.isUndefined($scope.msg2.death_cause_id)) {
                            $scope.msg2.death_cause_id = [];
                        }

                        $rootScope.mother.death_date = "";
                        $rootScope.mother.death_cause_id = "";
                    }else{

                        if( !angular.isUndefined($scope.msg2.condition)) {
                            $scope.msg2.condition = [];
                        }
                        if (!angular.isUndefined($scope.msg2.disease_id)) {
                            $scope.msg2.disease_id = [];
                        }
                        if (!angular.isUndefined($scope.msg2.details)) {
                            $scope.msg2.details = [];
                        }

                        $rootScope.mother.condition = "";
                        $rootScope.mother.disease_id = "";
                        $rootScope.mother.details = "";
                    }

                }

            }

        };
// -----------------------------------------------------------------------------------------------------

        $rootScope.dateOptions = {
            formatYear: 'yy',
            startingDay: 0
        };
        $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $rootScope.format = $rootScope.formats[0];
        $rootScope.today = function() {$rootScope.dt = new Date();};
        $rootScope.today();
        $rootScope.clear = function() {$rootScope.dt = null;};
        $rootScope.popup1 = {opened: false};
        $rootScope.popup2 = {opened: false};
        $rootScope.popup4 = {opened: false};
        $rootScope.popup3 = {opened: false};
        $rootScope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup1.opened = true;};
        $rootScope.open2 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup2.opened = true;};
        $rootScope.open3 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup3.opened = true;};
        $rootScope.open4 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup4.opened = true;};
      });