
angular.module('SponsorshipModule')
    .controller('showCaseController', function ($http,$filter,$scope,$rootScope,$state,$stateParams,$timeout,$uibModal,persons,cases,Org,forms_case_data,casesInfo) {

        $rootScope.settings.layout.pageSidebarClosed = true;
        $scope.target=$stateParams.target;
        $scope.id=$stateParams.id;

        $scope.case_payments = true;
        $scope.family_payments = true;
        $scope.guardian_payments= true;
        $scope.case_sponsorships= true;
        $scope.case_guardians= true;

        $scope.case_payments_filters = $scope.family_payments_filters = $scope.guardian_payments_filters =
        $scope.sponsorships_filters  = $scope.guardians_filters={};

        $scope.master=false;
        $scope.success =false;
        $scope.failed =false;
        $scope.error =false;
        $scope.model_error_status =false;
        $scope.model_error_status2 =false;

        $scope.itemsCount0 =50;
        $scope.itemsCount1 =50;
        $scope.itemsCount2 =50;
        $scope.itemsCount3 =50;
        $scope.itemsCount4 =50;

        $scope.model_error_status2 =false;

        $rootScope.update_success =false;
        $rootScope.update_error =false;
        $scope.close=function(){
            $scope.success =false;
            $scope.failed =false;
            $scope.error =false;
            $rootScope.update_success =false;
            $rootScope.update_error =false;
            $scope.model_error_status =false;
            $scope.model_error_status2 =false;
        };

        if($scope.id != null){
            cases.getCaseReports({'action':'filters','target':'info','case_id':$scope.id, 'mode':'show', 'full_name' :true, 'mother_id' :true, 'category_type' :1,'category' :true,'father_id' :true, 'guardian_id' :true},function(response) {
                $scope.category_name = response.category_name;
                $scope.name = response.full_name;
                $scope.category_id = response.category_id;
                $scope.guardian_id = response.guardian_id;
                $scope.father_id = response.father_id;
                $scope.mother_id = response.mother_id;
                $scope.person_id = response.person_id;
                $rootScope.organization_id = response.organization_id;
                $rootScope.user_organization_id = response.user_organization_id;
                $rootScope.gender = response.gender;
            });
        }

        Org.list({type:'sponsors'},function (response) { $scope.Sponsor = response; });

        var LoadCaseContents =function(params){
            $rootScope.clearToastr();
            $scope.close();
            var target = params.target ;
            if(target =='case_payments'){
                params.itemsCount = $scope.itemsCount;
            }else if(target =='family_payments'){
                params.itemsCount = $scope.itemsCount1;
            }else if(target =='guardian_payments'){
                params.itemsCount = $scope.itemsCount2;
            }else if(target =='case_sponsorships'){
                params.itemsCount = $scope.itemsCount0;
            }else if(target =='guardians'){
                params.itemsCount = $scope.itemsCount5;
            }
            
            params.case_id=$scope.id;
            params.action='filters';
            params.mode='show';

            if( $scope.case_payments === true    || $scope.guardian_payments === true  ||
                $scope.family_payments === true  || $scope.case_sponsorships === true  ||$scope.case_guardians === true ){

                if(params.target =='case_payments'|| params.target =='family_payments'|| params.target =='guardian_payments'){
                    if( !angular.isUndefined(params.begin_date_from)) {
                        params.begin_date_from=$filter('date')(params.begin_date_from, 'dd-MM-yyyy')
                    }
                    if( !angular.isUndefined(params.end_date_from)) {
                        params.end_date_from=$filter('date')(params.end_date_from, 'dd-MM-yyyy')
                    }

                    if( !angular.isUndefined(params.begin_date_to)) {
                        params.begin_date_to=$filter('date')(params.begin_date_to, 'dd-MM-yyyy')
                    }
                    if( !angular.isUndefined(params.end_date_to)) {
                        params.end_date_to=$filter('date')(params.end_date_to, 'dd-MM-yyyy')
                    }

                    if( !angular.isUndefined(params.start_payment_date)) {
                        params.start_payment_date=$filter('date')(params.start_payment_date, 'dd-MM-yyyy')
                    }
                    if( !angular.isUndefined(params.end_payment_date)) {
                        params.end_payment_date= $filter('date')(params.end_payment_date, 'dd-MM-yyyy')
                    }
                }
                else if(params.target =='case_sponsorships'){
                    if( !angular.isUndefined(params.to_guaranteed_date)) {
                        params.to_guaranteed_date=$filter('date')(params.to_guaranteed_date, 'dd-MM-yyyy')
                    }
                    if( !angular.isUndefined(params.from_guaranteed_date)) {
                        params.from_guaranteed_date=$filter('date')(params.from_guaranteed_date, 'dd-MM-yyyy')
                    }

                }
                else if(params.target =='guardians'){

                    if( !angular.isUndefined(params.to_birthday)) {
                        params.from_birthday=$filter('date')(params.to_birthday, 'dd-MM-yyyy')
                    }

                    if( !angular.isUndefined(params.from_birthday)) {
                        params.from_birthday=$filter('date')(params.from_birthday, 'dd-MM-yyyy')
                    }

                    if( !angular.isUndefined(params.to_guardian_date)) {
                        params.to_guardian_date=$filter('date')(params.to_guardian_date, 'dd-MM-yyyy')
                    }

                    if( !angular.isUndefined(params.from_guardian_date)) {
                        params.from_guardian_date=$filter('date')(params.from_guardian_date, 'dd-MM-yyyy')
                    }

                }
            }

            params.family_member= true;

            $rootScope.progressbar_start();
            cases.getCaseReports(params,function(response) {
                $rootScope.progressbar_complete();
                if(params.target =='case_payments'){


                    if(response.payment.total == 0){
                        $scope.case_payments_rows='false';
                    }else{
                        $scope.case_payments_rows='true';
                    }
                    $scope.CurrentPage = response.payment.current_page;
                    $scope.TotalItems = response.payment.total;
                    $scope.ItemsPerPage = response.payment.per_page;
                    $scope.casePaymentsList= response.payment.data;

                    if($scope.CurrentPage == 1){
                        $scope.payment_total= response.total;
                    }
                }
                else if(params.target =='family_payments'){

                    if(response.payment.total == 0){
                        $scope.family_payments_rows='false';
                    }else{
                        $scope.family_payments_rows='true';
                    }
                    $scope.CurrentPage1 = response.payment.current_page;
                    $scope.TotalItems1 = response.payment.total;
                    $scope.ItemsPerPage1 = response.payment.per_page;
                    $scope.familyPaymentsList= response.payment.data;

                    if($scope.CurrentPage1 == 1){
                        $scope.fpayment_total= response.total;
                    }

                }
                else if(params.target =='guardian_payments'){

                    if(response.payment.total == 0){
                        $scope.guardian_payments_rows='false';
                    }else{
                        $scope.guardian_payments_rows='true';
                    }


                    $scope.guardian_name= response.guardian_name;

                    $scope.CurrentPage2 = response.payment.current_page;
                    $scope.TotalItems2 = response.payment.total;
                    $scope.ItemsPerPage2 = response.payment.per_page;
                    $scope.guardianPaymentsList= response.payment.data;
                    if($scope.CurrentPage2 == 1){
                        $scope.gpayment_total= response.total;
                    }

                }
                else if(params.target =='case_sponsorships') {

                    if(response.total == 0){
                        $scope.sponsorships_rows='false';
                    }else{
                        $scope.sponsorships_rows='true';
                    }

                    $scope.CurrentPage0 = response.current_page;
                    $scope.TotalItems0 = response.total;
                    $scope.ItemsPerPage0 = response.per_page;
                    $rootScope.sponsorshipList= response.data;


                }
                else if(params.target =='guardians'){
                    if(response.total == 0){
                        $scope.guardians_rows='false';
                    }else{
                        $scope.guardians_rows='true';
                    }

                    $scope.CurrentPage5 = response.current_page;
                    $scope.TotalItems5 = response.total;
                    $scope.ItemsPerPage5 = response.per_page;
                    $scope.guardiansList= response.data;

                }
                else if(params.target =='info'){
                        $scope.case=response;
                }
            });
        };
        var resetSearchFilters =function(target){

            if(target  =='case_payments'){
                $scope.case_payments_filters={
                    'guardian_id_card_number':"",
                    'guardian_first_name':"",
                    'guardian_second_name':"",
                    'guardian_third_name':"",
                    'guardian_last_name':"",
                    'max_amount':"",
                    'min_amount':"",
                    'begin_date_from':"",
                    'end_date_from':"",
                    'begin_date_to':"",
                    'end_date_to':"",
                    'start_payment_date':"",
                    'end_payment_date':""
                };

            }
            else if(target =='family_payments'){

                $scope.family_payments_filters={
                    'id_card_number':"",
                    'first_name':"",
                    'second_name':"",
                    'third_name':"",
                    'last_name':"",
                    'guardian_id_card_number':"",
                    'guardian_first_name':"",
                    'guardian_second_name':"",
                    'guardian_third_name':"",
                    'guardian_last_name':"",
                    'max_amount':"",
                    'min_amount':"",
                    'begin_date_from':"",
                    'end_date_from':"",
                    'begin_date_to':"",
                    'end_date_to':"",
                    'start_payment_date':"",
                    'end_payment_date':""
                };

            }
            else if(target =='guardian_payments'){

                $scope.guardian_payments_filters={
                    'id_card_number':"",
                    'first_name':"",
                    'second_name':"",
                    'third_name':"",
                    'last_name':"",
                    'max_amount':"",
                    'min_amount':"",
                    'begin_date_from':"",
                    'end_date_from':"",
                    'begin_date_to':"",
                    'end_date_to':"",
                    'start_payment_date':"",
                    'end_payment_date':""
                };            }
            else if(target =='case_sponsorships'){

                $scope.sponsorships_filters={
                    'sponsor_id':"",
                    'category_id':"",
                    'to_guaranteed_date':"",
                    'from_guaranteed_date':""
                };
            }
            else if(target =='case_guardians'){

                $scope.guardians_filters={
                    'first_name':"",
                    'second_name':"",
                    'third_name':"",
                    'last_name':"",
                    'id_card_number':"",
                    'to_birthday':"",
                    'from_birthday':"" ,
                    'to_guardian_date':"",
                    'from_guardian_date':""
                };
            }

        };

        if($scope.target == 'sponsorships') {
            LoadCaseContents({'target':'case_sponsorships','page':1});
        }else if($scope.target == 'guardians') {
            LoadCaseContents({'target':'guardians','page':1});
        }

        $scope.toggleSearch = function(target) {

            if(target =='case_payments'){
                $scope.case_payments = $scope.case_payments === false ? true: false;
            }else if(target =='family_payments'){
                $scope.family_payments = $scope.family_payments === false ? true: false;
            }else if(target =='guardian_payments'){
                $scope.guardian_payments = $scope.guardian_payments === false ? true: false;
            }else if(target =='case_sponsorships'){
                $scope.case_sponsorships = $scope.case_sponsorships === false ? true: false;
            }else if(target =='case_guardians'){
                $scope.case_guardians = $scope.case_guardians === false ? true: false;
            }
            resetSearchFilters(target);
        };
        $scope.fetch=function(target){
            if($scope.target == 'payments'){
              var param={};
                if(target =='case_payments'){
                    $scope.case_payments =true;
                    if(angular.isUndefined($rootScope.CurrentPage)) {
                        param.page=1;
                    }else{
                        param.page=$rootScope.CurrentPage;
                    }
                }
                else if(target =='family_payments'){
                    $scope.family_payments =true;
                    if(angular.isUndefined($rootScope.CurrentPage1)) {
                        param.page=1;
                    }else{
                        param.page=$rootScope.CurrentPage1;
                    }
                }
                else if(target =='guardian_payments'){
                    $scope.guardian_payments =true;
                    if(angular.isUndefined($rootScope.CurrentPage2)) {
                        param.page=1;
                    }else{
                        param.page=$rootScope.CurrentPage2;
                    }
                }
                 else if(target =='case_sponsorships'){
                    $scope.case_sponsorships =true;
                    if(angular.isUndefined($rootScope.CurrentPage0)) {
                        param.page=1;
                    }else{
                        param.page=$rootScope.CurrentPage0;
                    }
                }
                param.target=target;
                LoadCaseContents(param);
            }
        };
        $scope.get=function(target){
            $rootScope.clearToastr();
            if($scope.target != 'sponsorships' &&  $scope.target != 'guardians'&&  $scope.target != 'payments'){
              var param={};
                if(target =='case_payments'){
                    $scope.case_payments =true;
                    if(angular.isUndefined($rootScope.CurrentPage)) {
                        param.page=1;
                    }else{
                        param.page=$rootScope.CurrentPage;
                    }
                }
                else if(target =='family_payments'){
                    $scope.family_payments =true;
                    if(angular.isUndefined($rootScope.CurrentPage1)) {
                        param.page=1;
                    }else{
                        param.page=$rootScope.CurrentPage1;
                    }
                }
                else if(target =='guardian_payments'){
                    $scope.guardian_payments =true;
                    if(angular.isUndefined($rootScope.CurrentPage2)) {
                        param.page=1;
                    }else{
                        param.page=$rootScope.CurrentPage2;
                    }
                }
                else if(target =='case_sponsorships'){
                    $scope.case_sponsorships =true;
                    if(angular.isUndefined($rootScope.CurrentPage0)) {
                        param.page=1;
                    }else{
                        param.page=$rootScope.CurrentPage0;
                    }
                }
                else if(target =='guardians'){
                    $scope.case_guardians =true;
                    if(angular.isUndefined($rootScope.CurrentPage5)) {
                        param.page=1;
                    }else{
                        param.page=$rootScope.CurrentPage5;
                    }
                }
                else if(target =='info'){
                    param={'person' :true, 'persons_i18n' : true, 'contacts' : true,  'education' : true,'health' : true,'islamic' : true,'category_type':1,
                        'work' : true, 'residence' : true, 'banks' : true ,'father_id' : true,'mother_id' : true,'guardian_id' : true };
                }

                if(target =='case_payments' || target =='guardians' || target =='case_sponsorships' || target =='family_payments' || target =='guardian_payments' ||target =='info'){
                    param.target=target;
                    LoadCaseContents(param);
                }
                if(target =='family'){
                    $rootScope.progressbar_start();
                    cases.getCaseReports({'case_id':$scope.id,
                        'target':'info',
                        'action':'filters',
                        'category_type':1,
                        'mode':'edit',
                        'family_member' : true,
                        'father_id' : true,
                        'mother_id' : true,
                        'guardian_id' : true,
                        'category' : true},function(response) {
                        $rootScope.progressbar_complete();
                        $rootScope.father_id =response.father_id;
                        $rootScope.mother_id =response.mother_id;
                        $rootScope.guardian_id =response.guardian_id;
                        $rootScope.l_person_id =response.l_person_id;
                        $rootScope.items =response.family_member.fm;
                        $rootScope.Caseitems=response.family_member.fm_cases;

                    });
                }
                if(target =='custom-data'){
                    $rootScope.progressbar_start();
                    forms_case_data.get({id: $scope.id,module:'sponsorship',category_id:$scope.category_id},function (response) {
                        $rootScope.progressbar_complete();

                        if( !angular.isUndefined(response.status)){
                            if(response.status == false) {
                                $rootScope.curesult_1 = 'false';
                            }
                        }

                        if( !angular.isUndefined(response.status_2)){
                            if(response.status_2 == false) {
                                $rootScope.curesult = 'false';
                            }
                        }

                        if(angular.isUndefined(response.status_2) && angular.isUndefined(response.status)) {
                            if (response.data.length != 0) {
                                var temp = response.data;

                                angular.forEach(temp, function (v, k) {

                                    if(v.value == null ) {
                                        v.data ='-';
                                    }else{
                                        if (v.options.length != 0) {
                                            angular.forEach(v.options, function (vv, kk) {
                                                if (v.value == vv.id) {
                                                    v.data = vv.label;
                                                }
                                            })
                                        }else{
                                            v.data = v.value;

                                        }
                                    }
                                });
                                $scope.fields = temp;
                            }else{
                                $scope.fields=[];
                            }
                        }else{
                            $scope.fields=[];
                        }

                    },function(error) {
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                }
                if(target =='dcouments'){
                    $rootScope.progressbar_start();
                    cases.getCaseReports({'action':'filters','target':'info','case_id':$scope.id, 'mode':'edit', 'persons_documents' :true,  'category' :true ,'guardian_id' :true, 'father_id' :true, 'mother_id' :true},function(response) {
                        $rootScope.progressbar_complete();
                        $scope.documents =response.persons_documents;
                        $scope.result='true';
                    });

                }
                if(target =='parents'){
                    if($scope.father_id){
                        $rootScope.progressbar_start();
                        persons.getPersonReports({'target':'info','person_id':$scope.father_id, 'action':'filters','mode':'show', 'person' :true, 'persons_i18n' : true, 'education' : true,'health' : true, 'work' : true},function(response){
                            $rootScope.progressbar_complete();
                            $scope.father=response;
                        });
                     }

                     if($scope.mother_id){
                         $rootScope.progressbar_start();
                         persons.getPersonReports({'target':'info','person_id':$scope.mother_id, 'action':'filters','mode':'show', 'person' :true, 'persons_i18n' : true, 'education' : true,'health' : true, 'work' : true},function(response2){
                             $rootScope.progressbar_complete();
                             $scope.mother=response2;
                        });
                     }
                }
                if(target =='guardian'){
                    if($scope.guardian_id){
                        param={'person_id':$scope.guardian_id,'target':'info','mode':'show',
                               'person' :true, 'persons_i18n' : true, 'contacts' : true,  'education' : true,'health' : true,'islamic' : true,
                               'work' : true, 'residence' : true, 'banks' : true };

                        $rootScope.progressbar_start();
                        persons.getPersonReports(param,function(response){
                            $rootScope.progressbar_complete();
                            $scope.guardian=response;
                        });
                    }

                }



            }
        };

        $scope.showImage=function(file){
            $rootScope.clearToastr();
            $rootScope.target=file;

            $uibModal.open({
                templateUrl: 'showImage.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance) {

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                },
                resolve: {

                }
            });
        };
        $scope.pageChanged = function (target,CurrentPage) {
            var param={'target':target};
            if(target =='case_payments'){
                param=$scope.payment;
            }else if(target =='family_payments'){
                param=$scope.fpayment;
            }else if(target =='guardian_payments'){
                param=$scope.gpayment;
            }else if(target =='case_sponsorships'){
                param=$scope.sponsorships_filters;
            }else if(target =='guardians'){
                param=$scope.guardians_filters;
            }
            if(angular.isUndefined(CurrentPage)) {
                CurrentPage=1;
            }

            param.target=target;
            param.page=CurrentPage;
            LoadCaseContents(param);

        };

        $rootScope.itemsPerPage_ = function (target ,itemsCount) {
            var param={'target':target ,'itemsCount' :itemsCount ,'page' :1};
            if(target =='case_payments'){
                $scope.CurrentPage = 1 ;
                $scope.itemsCount = itemsCount ; 
            }else if(target =='family_payments'){
                $scope.CurrentPage1 = 1 ;
                $scope.itemsCount1 = itemsCount ; 
            }else if(target =='guardian_payments'){
                $scope.CurrentPage2 = 1 ;
                $scope.itemsCount2 = itemsCount ; 
            }else if(target =='case_sponsorships'){
                $scope.CurrentPage0 = 1 ;
                $scope.itemsCount0 = itemsCount ; 
            }else if(target =='guardians'){
                $scope.CurrentPage5 = 1 ;
                $scope.itemsCount5 = itemsCount ; 
            }

            LoadCaseContents(param);
        };


        $scope.Search=function(target,data,action) {
            $rootScope.clearToastr();

            data.action=action;
            data.target=target;

            if(action == 'export'){

                data.case_id=$scope.id;
                data.mode='show';
                if(target =='case_payments'|| target =='family_payments'|| target =='guardian_payments'){
                    if( !angular.isUndefined(data.begin_date_from)) {
                        data.begin_date_from=$filter('date')(data.begin_date_from, 'dd-MM-yyyy')
                    }
                    if( !angular.isUndefined(data.end_date_from)) {
                        data.end_date_from=$filter('date')(data.end_date_from, 'dd-MM-yyyy')
                    }

                    if( !angular.isUndefined(data.begin_date_to)) {
                        data.begin_date_to=$filter('date')(data.begin_date_to, 'dd-MM-yyyy')
                    }
                    if( !angular.isUndefined(data.end_date_to)) {
                        data.end_date_to=$filter('date')(data.end_date_to, 'dd-MM-yyyy')
                    }

                    if( !angular.isUndefined(data.start_payment_date)) {
                        data.start_payment_date=$filter('date')(data.start_payment_date, 'dd-MM-yyyy')
                    }
                    if( !angular.isUndefined(data.end_payment_date)) {
                        data.end_payment_date= $filter('date')(data.end_payment_date, 'dd-MM-yyyy')
                    }
                }
                else if(target =='case_sponsorships'){
                    if( !angular.isUndefined(data.to_guaranteed_date)) {
                        data.to_guaranteed_date=$filter('date')(data.to_guaranteed_date, 'dd-MM-yyyy')
                    }
                    if( !angular.isUndefined(data.from_guaranteed_date)) {
                        data.from_guaranteed_date=$filter('date')(data.from_guaranteed_date, 'dd-MM-yyyy')
                    }

                }
                else{

                    if( !angular.isUndefined(data.to_birthday)) {
                        data.to_birthday=$filter('date')(data.to_birthday, 'dd-MM-yyyy')
                    }

                    if( !angular.isUndefined(data.from_birthday)) {
                        data.from_birthday=$filter('date')(data.from_birthday, 'dd-MM-yyyy')
                    }

                    if( !angular.isUndefined(data.to_guardian_date)) {
                        data.to_guardian_date=$filter('date')(data.to_guardian_date, 'dd-MM-yyyy')
                    }

                    if( !angular.isUndefined(data.from_guardian_date)) {
                        data.from_guardian_date=$filter('date')(data.from_guardian_date, 'dd-MM-yyyy')
                    }

                }

                $rootScope.progressbar_start();
                $http({
                    url: "/api/v1.0/common/cases/getCaseReports",
                    method: "POST",
                    data: data
                }).then(function (response) {
                    $rootScope.progressbar_complete();
                    if( !angular.isUndefined(response.data.status)) {
                        $rootScope.toastrMessages('error',$filter('translate')('There are no records to export to Excel'));
                    }

                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });


            }else{
                data.page=1;
                LoadCaseContents(data);
            }

        };

        $scope.show = function(size,id) {
            $rootScope.clearToastr();

            $uibModal.open({
                templateUrl: 'show.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope,$stateParams, $modalInstance,persons) {

                    $scope.msg1={};
                    $scope.persons={};
                    $scope.model_error=false;
                    $scope.msg32='';

                    if(id != -1){

                        var params={'person_id':id, 'mode':'show','target':'info', 'person' :true, 'persons_i18n' : true,'work' : true,'education' : true,'health' : true,'l_person_id':$rootScope.l_person_id};
                        persons.getPersonReports(params,function(response){
                            $scope.persons=response;
                        });
                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };
        $scope.export = function() {

            $rootScope.clearToastr();
            var params={'case_id':$scope.id,
                'target':'info',
                'sub_target':'family_member',
                'action':'export',
                'category_type':1,
                'mode':'show',
                'family_member' : true,
                'father_id' : true,
                'mother_id' : true,
                'guardian_id' : true,
                'category' : true};
            $rootScope.progressbar_start();
            $http({
                url: "/api/v1.0/common/cases/getCaseReports",
                method: "POST",
                data: params
            }).then(function (response) {
                $rootScope.progressbar_complete();
                var file_type='xlsx';
                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });

        }

        $scope.visitorNote=function(id){

            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'VisitorNote.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log) {
                    $rootScope.Notes='';
                    $rootScope.model_error_status =false;

                    $rootScope.progressbar_start();
                    cases.getCaseReports({'action':'filters','target':'info','case_id':id,'visitor_note':true, 'mode':'show'},function(response) {
                        $rootScope.progressbar_complete();
                        if(response.visitor_note_status==false)
                        {
                            $rootScope.toastrMessages('error',$filter('translate')("The researcher's opinion is not written in the settings"));
                        }else{
                            $rootScope.Notes = response.visitor_note;
                        }
                    });
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                },
                resolve: {
                }
            });
        };
        $scope.download = function (target) {

            $rootScope.clearToastr();
            var url ='';
            var file_type ='zip';
            var  params={};
            var method='';

            if(target =='word'){
                method='POST';
                params={'case_id':$scope.id,
                    'target':'info',
                    'action':'filters',
                    'mode':'show',
                    'category_type' : 1,
                    'person' :true,
                    'persons_i18n' : true,
                    'category' : true,
                    'work' : true,
                    'contacts' : true,
                    'health' : true,
                    'default_bank' : true,
                    'education' : true,
                    'islamic' : true,
                    'banks' : true,
                    'residence' : true,
                    'persons_documents' : true,
                    'family_member' : true,
                    'father_id' : true,
                    'mother_id' : true,
                    'guardian_id' : true,
                    'guardian_kinship' : true,
                    'parent_detail':true,
                    'guardian_detail':true,
                    'visitor_note':true
                };
                url = "/api/v1.0/common/sponsorships/cases/word";
            }
            else if(target =='pdf'){
                method='POST';
                params={'case_id':$scope.id,
                    'target':'info',
                    'action':'filters',
                    'mode':'show',
                    'category_type' : 1,
                    'person' :true,
                    'persons_i18n' : true,
                    'category' : true,
                    'work' : true,
                    'contacts' : true,
                    'health' : true,
                    'default_bank' : true,
                    'education' : true,
                    'islamic' : true,
                    'banks' : true,
                    'residence' : true,
                    'persons_documents' : true,
                    'family_member' : true,
                    'father_id' : true,
                    'mother_id' : true,
                    'guardian_id' : true,
                    'guardian_kinship' : true,
                    'parent_detail':true,
                    'guardian_detail':true,
                    'visitor_note':true
                };
                url = "/api/v1.0/common/sponsorships/cases/pdf";
            }
            else{
                method='GET';
                url = "/api/v1.0/common/sponsorships/cases/exportCaseDocument/"+id;
            }

            $rootScope.progressbar_start();
            $http({
                url:url,
                method: method,
                data:params
            }).then(function (response) {
                $rootScope.progressbar_complete();

                if(target !='word' && response.data.status == false){
                    $rootScope.toastrMessages('error',$filter('translate')('no attachment to export'));
                }else{
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }

            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };
        $scope.updateStatus=function(id,status,index,action){
            $rootScope.clearToastr();
            var new_status=5;

            if(action ==1){
                new_status=1;
                $rootScope.progressbar_start();
                var target = new casesInfo({'sub':true,'action':'single','status':new_status, cases:[{'id':id,'status':status}]});
                target.$UpdateCasesStatus()
                    .then(function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success')
                        {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            $rootScope.sponsorshipList[index].status=$filter('translate')('nominate');
                            $rootScope.sponsorshipList[index].sponsorship_status=1;
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
            }else{
                $uibModal.open({
                    templateUrl: 'updateStatus4.html',
                    backdrop: 'static',
                    keyboard: false,
                    windowClass: 'modal',
                    size: 'sm',
                    controller: function ($filter, $rootScope, $scope, $modalInstance, $log) {
                        $scope.model_error = false;
                        $rootScope.update_success =false;
                        $rootScope.update_error =false;
                        $scope.action = 'group';

                        $scope.data = {};
                        $scope.confirm = function (row) {

                            $rootScope.progressbar_start();
                            $rootScope.clearToastr();
                            if( !angular.isUndefined(row.date)) {
                                row.date=$filter('date')(row.date, 'dd-MM-yyyy')
                            }

                            var target = new casesInfo({'sub':true,'action':'single','status':new_status, 'date':row.date, 'reason':row.reason ,cases:[{'id':id,'status':status}]});
                            target.$UpdateCasesStatus()
                                .then(function (response) {
                                    $rootScope.progressbar_complete();
                                    if(response.status=='success')
                                    {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        $rootScope.sponsorshipList[index].status=$filter('translate')('nominate');
                                        $rootScope.sponsorshipList[index].sponsorship_status=1;
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    $modalInstance.close();
                                }, function(error) {
                                    $rootScope.progressbar_complete();
                                    $rootScope.toastrMessages('error',error.data.msg);
                                });
                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };


                        $scope.dateOptions = {
                            formatYear: 'yy',
                            startingDay: 0
                        };
                        $scope.formats = ['dd-MM-yyyy', 'dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                        $scope.format = $scope.formats[0];
                        $scope.today = function () {
                            $scope.dt = new Date();
                        };
                        $scope.today();
                        $scope.clear = function () {
                            $scope.dt = null;
                        };

                        $scope.open44 = function ($event) {
                            $event.preventDefault();
                            $event.stopPropagation();
                            $scope.popup44.opened = true;
                        };

                        $scope.popup44 = {
                            opened: false
                        };
                        $scope.open444 = function ($event) {
                            $event.preventDefault();
                            $event.stopPropagation();
                            $scope.popup444.opened = true;
                        };

                        $scope.popup444 = {
                            opened: false
                        };


                    }
                });
            }
        };

        $scope.dateOptions = { formatYear: 'yy',  startingDay: 0 };
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.today = function() { $scope.dt = new Date(); };
        $scope.clear = function() { $scope.dt = null; };

        $scope.format = $scope.formats[0];
        $scope.today();

        $scope.open1 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup1.opened = true; };
        $scope.open2 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup2.opened = true; };
        $scope.open3 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup3.opened = true; };
        $scope.open4 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup4.opened = true; };
        $scope.open5 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup5.opened = true; };
        $scope.open6 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup6.opened = true; };
        $scope.open7 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup7.opened = true; };
        $scope.open8 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup8.opened = true; };
        $scope.open9 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup9.opened = true;};
        $scope.open10 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup10.opened = true; };
        $scope.open11 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup11.opened = true; };
        $scope.open12 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup12.opened = true; };
        $scope.open13 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup13.opened = true; };
        $scope.open14 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup14.opened = true; };
        $scope.open15 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup15.opened = true; };
        $scope.open16 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup16.opened = true; };
        $scope.open17 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup17.opened = true; };
        $scope.open18 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup18.opened = true; };
        $scope.open19 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup19.opened = true; };
        $scope.open20 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup20.opened = true; };
        $scope.open21 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup21.opened = true; };
        $scope.open22 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup22.opened = true; };
        $scope.open23 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup23.opened = true; };
        $scope.open24 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup24.opened = true; };
        $scope.open25 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup25.opened = true; };
        $scope.open26 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup26.opened = true; };
        $scope.open215 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup215.opened = true;};
        $scope.open216 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup216.opened = true;};

        $scope.popup1  = {opened: false};
        $scope.popup2  = {opened: false};
        $scope.popup3  = {opened: false};
        $scope.popup4  = {opened: false};
        $scope.popup5  = {opened: false};
        $scope.popup6  = {opened: false};
        $scope.popup7  = {opened: false};
        $scope.popup8  = {opened: false};
        $scope.popup9  = {opened: false};
        $scope.popup10 = {opened: false};
        $scope.popup11 = {opened: false};
        $scope.popup12 = {opened: false};
        $scope.popup13 = {opened: false};
        $scope.popup14 = {opened: false};
        $scope.popup15 = {opened: false};
        $scope.popup16 = {opened: false};
        $scope.popup17 = {opened: false};
        $scope.popup18 = {opened: false};
        $scope.popup19 = {opened: false};
        $scope.popup20 = {opened: false};
        $scope.popup21 = {opened: false};
        $scope.popup22 = {opened: false};
        $scope.popup23 = {opened: false};
        $scope.popup24 = {opened: false};
        $scope.popup25 = {opened: false};
        $scope.popup26 = {opened: false};

    });