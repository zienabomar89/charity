
angular.module('SponsorshipModule')
    .controller('subCaseInfoController',function($http,$filter,$scope,$rootScope,$state,$stateParams,category,$uibModal,cases,persons,Entity) {

        $rootScope.TheStep=1;
            $rootScope.person_id=$stateParams.id;
            $rootScope.category_id=$stateParams.category_id;
            $rootScope.case_id     = $stateParams.case_id;
            $rootScope.father_id     = $stateParams.father_id;
            $rootScope.mother_id     = $stateParams.mother_id;
            $rootScope.guardian_id     = $stateParams.guardian_id;
        $scope.success_frm=false;
        $rootScope.success_msg="";

        $scope.priority={};
        $rootScope.persons={banks:[]};
            $scope.msg='';
            $rootScope.msg1={};
            $rootScope.status1='';
            $scope.model_error_status=false;
            $rootScope.action='add';


            var setPersons=function(data){
                $rootScope.persons=data;
                $rootScope.case_id=$rootScope.persons.case_id;
                $rootScope.person_id=$rootScope.persons.person_id;

                if($rootScope.persons.kinship_id !=null){
                    $rootScope.persons.kinship_id = $rootScope.persons.kinship_id+"";
                }else{
                    $rootScope.persons.kinship_id="";
                }

                if($rootScope.persons.birthday =="0000-00-00" || $rootScope.persons.birthday == null){
                    $rootScope.persons.birthday    = new Date();
                }else{
                    $rootScope.persons.birthday    = new Date($rootScope.persons.birthday);
                }


                if($rootScope.persons.gender ==0){
                    $rootScope.persons.gender="";
                }else{
                    $rootScope.persons.gender=$rootScope.persons.gender+"";
                }

                if($rootScope.persons.nationality==null){
                    $rootScope.persons.nationality="";
                } else{
                    $rootScope.persons.nationality=$rootScope.persons.nationality+"";
                }

                if($rootScope.persons.birth_place==null){
                    $rootScope.persons.birth_place="";
                } else{
                    $rootScope.persons.birth_place=$rootScope.persons.birth_place+"";
                }

                if($rootScope.persons.monthly_income==null){
                    $rootScope.persons.monthly_income = "";
                }else{
                    $rootScope.persons.monthly_income = $rootScope.persons.monthly_income+"";
                }

                if($rootScope.persons.country !=null){
                    $rootScope.persons.country = $rootScope.persons.country+"";
                    $scope.governarate = Entity.listChildren({entity:'districts','id':$rootScope.persons.country,'parent':'countries'});
                }else{
                    $rootScope.persons.country ="";

                }

                if($rootScope.persons.governarate !=null){
                    $rootScope.persons.governarate = $rootScope.persons.governarate+"";

                    $scope.city = Entity.listChildren({parent:'districts','id':$rootScope.persons.governarate,entity:'cities'});
                }else{
                    $rootScope.persons.governarate ="";

                }

                if($rootScope.persons.city !=null){
                    $rootScope.persons.city = $rootScope.persons.city+"";
                    $scope.nearlocation = Entity.listChildren({entity:'neighborhoods','id':$rootScope.persons.city,'parent':'cities'});
                }else{
                    $rootScope.persons.city ="";
                }

                if($rootScope.persons.location_id !=null){
                    $rootScope.persons.location_id = $rootScope.persons.location_id+"";
                }else{
                    $rootScope.persons.location_id ="";
                }


                if($rootScope.persons.study == null){
                    $rootScope.persons.study = "";
                    $rootScope.persons.type="";
                    $rootScope.persons.authority="";
                    $rootScope.persons.grade="";
                    $rootScope.persons.stage="";
                    $rootScope.persons.level="";
                    $rootScope.persons.year="";
                    $rootScope.persons.school="";
                    $rootScope.persons.points="";
                }
                else{
                    $rootScope.persons.study= $rootScope.persons.study +"";

                    if($rootScope.persons.study == 0){
                        $rootScope.persons.stage="";
                    }else{
                        if($rootScope.persons.type==null){
                            $rootScope.persons.type="";
                        } else{
                            $rootScope.persons.type=$rootScope.persons.type+"";
                        }
                        if($rootScope.persons.authority==null){
                            $rootScope.persons.authority="";
                        } else{
                            $rootScope.persons.authority=$rootScope.persons.authority+"";
                        }
                        if($rootScope.persons.grade==null){
                            $rootScope.persons.grade="";
                        } else{
                            $rootScope.persons.grade=$rootScope.persons.grade+"";
                        }
                        if($rootScope.persons.stage==null){
                            $rootScope.persons.stage="";
                        } else{
                            $rootScope.persons.stage=$rootScope.persons.stage+"";
                        }
                        if($rootScope.persons.level==null){
                            $rootScope.persons.level="";
                        } else{
                            $rootScope.persons.level=$rootScope.persons.level+"";
                        }

                    }
                }
                if($rootScope.persons.condition ==null){
                    $rootScope.persons.condition="";
                    $rootScope.persons.disease_id="";
                    $rootScope.persons.details="";
                }
                else{
                    $rootScope.persons.condition=$rootScope.persons.condition+"";
                    if($rootScope.persons.condition == 2 || $rootScope.persons.condition == 3 ||
                        $rootScope.persons.condition === '2' || $rootScope.persons.condition === '3' ){

                        if($rootScope.persons.disease_id==null){
                            $rootScope.persons.disease_id="";
                        }
                        else{
                            $rootScope.persons.disease_id=$rootScope.persons.disease_id+"";
                        }
                    }
                }




                if($rootScope.persons.quran_center==null){
                    $rootScope.persons.quran_center="";
                    $rootScope.persons.quran_reason="";
                }
                else{
                    if($rootScope.persons.quran_center==0){
                        $rootScope.persons.quran_center=0+"";
                    }
                    else{
                        $rootScope.persons.quran_center=1+"";
                        $rootScope.persons.quran_reason="";
                    }
                }
                if($rootScope.persons.prayer==null){
                    $rootScope.persons.prayer="";
                }
                else{
                    $rootScope.persons.prayer =$rootScope.persons.prayer +"";
                }

                if($rootScope.persons.quran_chapters==null || $rootScope.persons.quran_parts==null){
                    $rootScope.persons.save_quran=1+"";
                    $rootScope.persons.quran_parts==null;
                    $rootScope.persons.quran_chapters==null;
                }
                else {
                    $rootScope.persons.save_quran=0+"";
                }



            }
            if($stateParams.id == null){
                $rootScope.action='add';
            }
            else{
                $rootScope.action='edit';
                var param={'target':'info','action':'filters','case_id':$stateParams.case_id, 'mode':'edit','person' :true, 'persons_i18n' : true,  'education' : true,'health' : true,'islamic' : true,'category_type':1,
                           'banks' : true ,'father_id' : true,'mother_id' : true,'guardian_id' : true };
                cases.getCaseReports(param,function(response) {
                    setPersons(response);
                });


            }
            category.getPriority({'type':'sponsorships','category_id':$rootScope.category_id ,'target':'fm-case'},function (response) {
            $scope.priority=response.priority;
            });

        if($stateParams.father_id != null && $stateParams.id == null){
            persons.getPersonReports({'person_id':$rootScope.father_id,'target':'info','persons_i18n' : true},function (response) {
                $rootScope.persons.second_name=response.first_name;
                $rootScope.persons.third_name=response.second_name;
                $rootScope.persons.last_name=response.last_name;
                $rootScope.persons.en_second_name=response.en_first_name;
                $rootScope.persons.en_third_name=response.en_second_name;
                $rootScope.persons.en_last_name=response.en_last_name;
            });
        }

        Entity.get({'entity':'entities' , 'c':'maritalStatus,educationStages,educationAuthorities,countries,diseases,kinship'},function (response){
            $rootScope.MaritalStatus = response.maritalStatus;
            $rootScope.EduStages = response.educationStages;
            $scope.EduAuthorities = response.educationAuthorities;
            $rootScope.Diseases = response.diseases;
            $scope.Kinship = response.kinship;
            $rootScope.Country = response.countries;
            $scope.PropertyTypes = response.propertyTypes;
            $scope.RoofMaterials = response.roofMaterials;
        });

        $scope.forward=function(data){
            $rootScope.clearToastr();
            angular.element('#subcaseDocuments').removeClass("disabled");

                if( !angular.isUndefined(data.birthday)) {
                    data.birthday= $filter('date')(data.birthday, 'dd-MM-yyyy')
                }
                data.priority=$scope.priority;
                data.father_id=$rootScope.father_id;
                data.mother_id=$rootScope.mother_id;
                data.guardian_id=$rootScope.guardian_id;
                data.category_id=$rootScope.category_id;
                data.target='case';
                data.guardian_update=true;

                 $http({
                     method: 'POST',
                     url: '/api/v1.0/common/sponsorships/cases',
                     data:data
                 }).then(function successCallback(response) {

                     if(response.data.status=='failed_valid')
                     {
                         $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                         $scope.status1 =response.data.status;
                         $rootScope.msg1 =response.data.msg;
                     }
                     else if(response.data.status=='failed')
                     {
                         $rootScope.toastrMessages('error',response.data.msg);
                         $scope.model_error_status=true;
                     }
                     else if(response.data.status== 'success'){
                         $rootScope.case_id=response.data.case_id ;
                         $rootScope.person_id=response.data.person_id ;

                         $state.go('sub.fm-case-documents', {
                             id:$rootScope.person_id,
                             guardian_id:$rootScope.guardian_id,
                             case_id:$rootScope.case_id,
                             father_id:$rootScope.father_id,
                             mother_id: $rootScope.mother_id,
                             category_id:$rootScope.category_id
                         });
                     }
                 });


            };
        $scope.isPersonExists = function(id_card_number){
            $rootScope.clearToastr();
            if( !angular.isUndefined($rootScope.msg1.id_card_number)) {
                $rootScope.msg1.id_card_number = [];
            }
            if(!angular.isUndefined($rootScope.persons.id_card_number)){
                if(id_card_number.length==9 ) {

                    if($rootScope.check_id(id_card_number)){
                        var params={ 'mode':'edit' ,'id_card_number' :id_card_number, 'category_id' : $rootScope.category_id, 'l_person_id':$rootScope.guardian_id,
                        'person' :true, 'persons_i18n' : true, 'education' : true, 'islamic' : true, 'health' : true,
                        'banks' : true
                    };

                    persons.find(params,function (response) {
                        if(response.status ==true ) {
                            $rootScope.toastrMessages('error',response.msg);

                            setPersons(response.person);
                        }
                        else{
                            if( !angular.isUndefined(response.blocked)) {
                                if(response.blocked){
                                    $rootScope.toastrMessages('error',response.msg);
                                    $rootScope.persons = [];
                                }else{
                                    $scope.model_error_status=false;
                                    $scope.msg='';
                                    $rootScope.persons.id="";
                                    $rootScope.persons.person_id=null;
                                }
                            }else{
                                $scope.model_error_status=false;
                                $scope.msg='';
                                $rootScope.persons.id="";
                                $rootScope.persons.person_id=null;
                            }

                            if(response.person){
                                $rootScope.persons    = response.person;

                                if($rootScope.persons.birthday =="0000-00-00" || $rootScope.persons.birthday == null){
                                    $rootScope.persons.birthday    = new Date();
                                }else{
                                    $rootScope.persons.birthday    = new Date($rootScope.persons.birthday);
                                }
                            }

                            $rootScope.persons.id="";
                            $rootScope.persons.person_id=null;
                        }
                    });

                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('invalid card number'));
                    }
                }
            }
        };
        $scope.add=function(size){
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'addBank.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size ,
                controller: function ($rootScope,$scope,$stateParams, $modalInstance,Entity) {

                    $scope.action='add';
                    $scope.error_frm=false;
                    $scope.msg3='';
                    $scope.msg4={};
                    $scope.detail={};
                    $scope.getBranches=function(id){
                        if( !angular.isUndefined($rootScope.msg1.bank_id)) {
                            $scope.msg4.bank_id = [];
                        }
                        $scope.detail.branch_name = "";

                        Entity.listChildren({parent:'banks','id':id,'entity':'branches'},function (response) {
                            $scope.Branches = response;
                        });
                    };

                    Entity.list({entity:'banks'},function (response) {
                        $scope.Banks = response;
                    });

                    $scope.save = function (data) {
                        $rootScope.clearToastr();
                        data.id=-1;
                        $scope.error_frm=false;
                        $scope.msg3='';

                        var flag =-1;
                        angular.forEach($rootScope.persons.banks, function(val,key) {
                            if(val.bank_id == data.bank_id){
                                flag = 1;
                                return;
                            }
                        });

                        if(flag == -1){

                            cases.checkAccount({'id':data.bank_id,'account_number':data.account_number},function(response){

                                if(response.status == true){
                                    angular.forEach($scope.Banks, function(val,key) {
                                        if(val.id == data.bank_id){
                                            data.bank_name =val.name;
                                            return;
                                        }
                                    });
                                    angular.forEach($scope.Branches, function(val,key) {
                                        if(val.id == data.branch_name){
                                            data.branch =val.name;
                                            return;
                                        }
                                    });
                                    if(angular.isUndefined($rootScope.persons.banks)) {
                                        $rootScope.persons.banks=new Array();
                                        data.check=true;
                                    }else{
                                        if($rootScope.persons.banks.length ==0 ){
                                            data.check=true;
                                        }else{
                                            data.check=false;
                                        }
                                    }

                                        $rootScope.persons.banks.push(data);


                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));

                                    $rootScope.no_banks='true';
                                    $rootScope.status1 ='failed_valid';
                                    $rootScope.msg1.banks=[];
                                    $modalInstance.close();
                                }else
                                {

                                    if($rootScope.person_id == null){
                                        $rootScope.toastrMessages('error',$filter ('translate') ('this account previously in use of other person'));
                                    }else{

                                        if($rootScope.person_id == response.person_id){
                                            angular.forEach($scope.Banks, function(val,key) {
                                                if(val.id == data.bank_id){
                                                    data.bank_name =val.name;
                                                    return;
                                                }
                                            });
                                            angular.forEach($scope.Branches, function(val,key) {
                                                if(val.id == data.branch_name){
                                                    data.branch =val.name;
                                                    return;
                                                }
                                            });
                                            if(angular.isUndefined($rootScope.persons.banks)) {
                                                $rootScope.persons.banks=new Array();
                                                data.check=true;
                                            }else{
                                                if($rootScope.persons.banks.length ==0 ){
                                                    data.check=true;
                                                }else{
                                                    data.check=false;
                                                }
                                            }

                                            $rootScope.persons.banks.push(data);

                                            $scope.success_frm=true;
                                            $rootScope.success_msg=$filter ('translate')('action success');

                                            $rootScope.status1 ='failed_valid';
                                            $rootScope.msg1.banks=[];
                                            $modalInstance.close();

                                        }else{
                                            $rootScope.toastrMessages('error',$filter ('translate') ('this account previously in use of other person'));
                                        }
                                    }


                                }
                            });

                        }else{
                            $rootScope.toastrMessages('error',$filter('translate')('the account has been entered for the person in the same bank. Please delete the account or modify it'));
                        }


                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }


            });
        };
        $scope.update=function(size,index,item){
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'addBank.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size ,
                controller: function ($rootScope,$scope,$stateParams, $modalInstance,Entity) {

                    $scope.error_frm=false;
                    $scope.msg3='';
                    $scope.msg4={};
                    $scope.action='edit';
                    $scope.getBranches=function(id){
                        if( !angular.isUndefined($scope.msg4.bank_id)) {
                            $scope.msg4.bank_id = [];
                        }
                        $scope.detail.branch_name = "";

                        Entity.listChildren({parent:'banks',id:id,entity:'branches'},function (response) {
                            $scope.Branches = response;

                            if($scope.Branches.length ==0){
                                $scope.detail.branch_name = "";
                            }
                        });
                    };

                    Entity.list({entity:'banks'},function (response) {
                        $scope.Banks = response;
                    });
                    $scope.detail=item;
                    if( $scope.detail.bank_id == null) {
                        $scope.detail.bank_id  = "";
                        $scope.detail.branch_name = "";
                        $scope.detail.account_number = null;
                        $scope.detail.account_owner = null;
                    }else{
                        var branch_name =$scope.detail.branch_name;
                        $scope.getBranches($scope.detail.bank_id);
                        $scope.detail.bank_id = $scope.detail.bank_id +"";
                        if( $scope.detail.branch_name == null) {
                            $scope.detail.branch_name = "";
                        }else{
                            $scope.detail.branch_name = branch_name +"";
                        }

                    }

                    $scope.save = function (data) {
                        $rootScope.clearToastr();
                        $scope.error_frm=false;
                        $scope.msg3='';
                        data.id=-1;

                         cases.checkAccount({'id':data.bank_id,'account_number':data.account_number,'person_id':$rootScope.person_id},function(response){


                            if(response.status == true){
                                angular.forEach($scope.Banks, function(val,key) {
                                    if(val.id == data.bank_id){
                                        data.bank_name =val.name;
                                        return;
                                    }
                                });
                                angular.forEach($scope.Branches, function(val,key) {
                                    if(val.id == data.branch_name){
                                        data.branch =val.name;
                                        return;
                                    }
                                });
                                if(angular.isUndefined($rootScope.persons.banks)) {
                                    $rootScope.persons.banks=new Array();
                                    data.check=true;
                                }else{
                                    if($rootScope.persons.banks.length ==0 ){
                                        data.check=true;
                                    }else{
                                        data.check=false;
                                    }
                                }

                                $rootScope.persons.banks[index]=data;
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                $rootScope.no_banks='true';
                                $rootScope.status1 ='failed_valid';
                                $rootScope.msg1.banks=[];
                                $modalInstance.close();
                            }else
                                {

                                if($rootScope.person_id == null){
                                    $rootScope.toastrMessages('error',$filter ('translate') ('this account previously in use of other person'));
                                }
                                else{

                                    if($rootScope.person_id == response.person_id){
                                        angular.forEach($scope.Banks, function(val,key) {
                                            if(val.id == data.bank_id){
                                                data.bank_name =val.name;
                                                return;
                                            }
                                        });
                                        angular.forEach($scope.Branches, function(val,key) {
                                            if(val.id == data.branch_name){
                                                data.branch =val.name;
                                                return;
                                            }
                                        });
                                        if($rootScope.persons.banks.length ==0 ){
                                            data.check=true;
                                        }else{
                                            data.check=false;
                                        }
                                        $rootScope.persons.banks[index]=data;
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        $rootScope.status1 ='failed_valid';
                                        $rootScope.msg1.banks=[];
                                        $modalInstance.close();

                                    }else{
                                        $rootScope.toastrMessages('error',$filter ('translate') ('this account previously in use of other person'));
                                    }
                                }


                            }
                        });


                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }


            });
        };
        $scope.delete = function (index) {
            $rootScope.clearToastr();
            $rootScope.persons.banks.splice(index,1);
            if($rootScope.persons.banks.length==0){
                $rootScope.no_banks='false';
            }
            $scope.success_frm=true;
            $rootScope.toastrMessages('success',$filter('translate')('The row is deleted from db'));

        };

        $scope.setOrgDefault  = function(status,index,item){
                var l=-1;
                var bk=-1;
                angular.forEach($rootScope.persons.banks, function(val,key) {
                    if(val.check){
                        l=key;
                        bk=val;
                    }
                });
                if(bk != -1){
                    if( l!=index ){
                        $rootScope.persons.banks[l].check=false;
                        $rootScope.persons.banks[index].check=true;
                        $rootScope.default_bank=item.bank_id;
                    }
                }else{
                    $rootScope.persons.banks[index].check=true;
                    $rootScope.default_bank=item.bank_id;
                }


            };

        $scope.restValue = function(toggle,value){

                if(toggle == 'condition'){
                    if( !angular.isUndefined($rootScope.msg1.condition)) { $rootScope.msg1.condition = []; }
                    if( !angular.isUndefined($rootScope.msg1.disease_id)) { $rootScope.msg1.disease_id = []; }
                    if( !angular.isUndefined($rootScope.msg1.details)) { $rootScope.msg1.details = []; }

                    if(value == 2 || value == 3){

                        if($rootScope.persons.disease_id==null){
                            $rootScope.persons.disease_id="";
                        }
                        else{
                            $rootScope.persons.disease_id=$rootScope.persons.disease_id+"";
                        }
                    }else{
                        $rootScope.persons.disease_id="";
                        $rootScope.persons.details="";
                    }

                }
                else if(toggle == 'study'){

                    if( !angular.isUndefined($rootScope.msg1.study)) { $rootScope.msg1.study = []; }
                    if( !angular.isUndefined($rootScope.msg1.type)) { $rootScope.msg1.type = []; }
                    if( !angular.isUndefined($rootScope.msg1.authority)) { $rootScope.msg1.authority = []; }
                    if( !angular.isUndefined($rootScope.msg1.grade)) { $rootScope.msg1.grade = []; }
                    if( !angular.isUndefined($rootScope.msg1.stage)) { $rootScope.msg1.stage = []; }
                    if( !angular.isUndefined($rootScope.msg1.level)) { $rootScope.msg1.level = []; }
                    if( !angular.isUndefined($rootScope.msg1.points)) { $rootScope.msg1.points = []; }
                    if( !angular.isUndefined($rootScope.msg1.year)) { $rootScope.msg1.year = []; }
                    if( !angular.isUndefined($rootScope.msg1.school)) { $rootScope.msg1.school = []; }

                    if(value.study == 0){
                        $rootScope.persons.type="";
                        $rootScope.persons.authority="";
                        $rootScope.persons.grade="";
                        $rootScope.persons.stage="";
                        $rootScope.persons.level="";
                        $rootScope.persons.points="";
                        $rootScope.persons.year="";
                        $rootScope.persons.school="";
                    }else{
                        if($rootScope.persons.type==null){
                            $rootScope.persons.type="";
                        } else{
                            $rootScope.persons.type=$rootScope.persons.type+"";
                        }
                        if($rootScope.persons.authority==null){
                            $rootScope.persons.authority="";
                        } else{
                            $rootScope.persons.authority=$rootScope.persons.authority+"";
                        }
                        if($rootScope.persons.grade==null){
                            $rootScope.persons.grade="";
                        } else{
                            $rootScope.persons.grade=$rootScope.persons.grade+"";
                        }
                        if($rootScope.persons.stage==null){
                            $rootScope.persons.stage="";
                        } else{
                            $rootScope.persons.stage=$rootScope.persons.stage+"";
                        }
                        if($rootScope.persons.level==null){
                            $rootScope.persons.level="";
                        } else{
                            $rootScope.persons.level=$rootScope.persons.level+"";
                        }

                    }
                }
                else if(toggle == 'prayer'){
                if( !angular.isUndefined($rootScope.msg1.prayer)) { $rootScope.msg1.prayer = []; }
                if( !angular.isUndefined($rootScope.msg1.disease_id)) { $rootScope.msg1.disease_id = []; }
                if( !angular.isUndefined($rootScope.msg1.details)) { $rootScope.msg1.details = []; }

                    if (!angular.isUndefined($rootScope.msg1.prayer)) {
                        $rootScope.msg1.prayer = [];
                    }

                    if (!angular.isUndefined($rootScope.msg1.prayer_reason)) {
                        $rootScope.msg1.prayer_reason = [];
                    }

                    if (value != 1) {
                        $rootScope.persons.prayer_reason = "";
                    }

                }
                else if(toggle=='save_quran') {

                    if (!angular.isUndefined($rootScope.msg1.save_quran)) {
                        $rootScope.msg1.save_quran = [];
                    }

                    if (value == 1){
                        if (!angular.isUndefined($rootScope.msg1.quran_parts)) {
                            $rootScope.msg1.quran_parts = [];
                        }
                        if (!angular.isUndefined($rootScope.msg1.quran_chapters )) {
                            $rootScope.msg1.quran_chapters = [];
                        }

                        $rootScope.persons.quran_parts = "";
                        $rootScope.persons.quran_chapters  = "";

                    }

                }
                else if(toggle=='quran_center') {

                    if (!angular.isUndefined($rootScope.msg1.quran_center)) {
                        $rootScope.msg1.quran_center = [];
                    }

                    if (value == 1){
                        if (!angular.isUndefined($rootScope.msg1.quran_reason)) {
                            $rootScope.msg1.quran_reason = [];
                        }
                        $rootScope.persons.quran_reason = "";

                    }
                }
            };
    
            $scope.checkBirthday  = function(birthday){
                if( !angular.isUndefined($rootScope.msg1.birthday)) {
                    $rootScope.msg1.birthday = [];
                }
    
                var curDate = new Date();
    
                if(new Date(birthday) > curDate){
                    $rootScope.msg1.birthday = {0 :$filter('translate')('The specified date must be older than the current date')};
                    $rootScope.status1 ='failed_valid';
                }
    
            };
            $rootScope.dateOptions = {
                formatYear: 'yy',
                startingDay: 0
            };
            $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
            $rootScope.format = $rootScope.formats[0];
            $rootScope.today = function()
            {
                $rootScope.dt = new Date();
            };
    
            $rootScope.today();
            $rootScope.clear = function()
            {
                $rootScope.dt = null;
            };
    
            $rootScope.open1 = function($event) {
                $event.preventDefault();
                $event.stopPropagation();
                $rootScope.popup1.opened = true;
            };
            $rootScope.popup1 = {
                opened: false
            };

        if($stateParams.id){

            $rootScope.setStepSub('#CaseData');

        }
    });