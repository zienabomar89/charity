
angular.module('SponsorshipModule')
    .directive("dynamicName",function($compile){
        return {
            restrict:"A",
            terminal:true,
            priority:1000,
            link:function(scope,element,attrs){
                element.attr('name', scope.$eval(attrs.dynamicName));
                element.removeAttr("dynamic-name");
                $compile(element)(scope);
            }
        }
    })
    .controller('subCustomFormDataController', function ($scope,$rootScope,$state,$stateParams,form_elements,custom_forms,setting,forms_case_data) {

        $rootScope.TheStep=3;
        
        $rootScope.failed =false;
        ;
        $rootScope.model_error_status =false;
        $rootScope.fields=[];
        $rootScope.close=function(){
            
            $rootScope.failed =false;
            ;
            $rootScope.model_error_status =false;
        };

        $rootScope.status ='';
        $rootScope.msg ='';
        $rootScope.msg2 ='';

        $rootScope.person_id=$stateParams.id;
        $rootScope.category_id=$stateParams.category_id;
        $rootScope.case_id     = $stateParams.case_id;
        $rootScope.father_id     = $stateParams.father_id;
        $rootScope.mother_id     = $stateParams.mother_id;
        $rootScope.guardian_id     = $stateParams.guardian_id;
        $rootScope.result_1 = 'true';
        $rootScope.result = 'true';

        forms_case_data.get({id: $stateParams.case_id,module:'sponsorship',category_id:$stateParams.category_id},function (response) {

            if( !angular.isUndefined(response.status)){
                if(response.status === false) {
                    $rootScope.result_1 = 'false';
                }
            }

            if( !angular.isUndefined(response.status_2)){
                if(response.status_2 === false) {
                    $rootScope.result = 'false';
                }
            }

            if(angular.isUndefined(response.status_2) && angular.isUndefined(response.status)){
                    if (response.data.length !== 0) {
                        var temp = response.data;

                        angular.forEach(temp, function(v, k) {
                            v.data=v.value;
                            if (v.options.length !== 0) {
                                if(v.value !== null && v.value !== 'null'){
                                    v.data=parseInt(v.value) +"";
                                }
                            }
                        });
                        $scope.fields=temp;
                    }
                }


        });
        $scope.forward=function(fields,step){
            $rootScope.clearToastr();
            var target = new forms_case_data({case_id:$stateParams.case_id,'fieldsData':fields});
            target.$save()
                .then(function (response) {
                    if(response.status ==='failed_valid')
                    {
                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                        $scope.status1 =response.status;
                        $scope.msg1 =response.msg;
                    }
                    else {
                        if(step == 'next'){
                            $state.go('form.family-info',{category_id:$rootScope.category_id,'father_id':$rootScope.father_id,
                                mother_id:$rootScope.mother_id,'guardian_id':$rootScope.guardian_id});
                        }else{
                            $rootScope.getTheSubTarget('caseDocuments');

                        }
                    }
                },function(error) {
                    $rootScope.toastrMessages('error',error.data.msg);
                });
        };

    });