
angular.module('SponsorshipModule')
    .controller('subFormController', function ($scope,$rootScope,$state,$stateParams,setting,category) {

        $rootScope.cat_has_custom_form= false;
        setting.getCustomForm({'id':'sponsorship_custom_form'},function (response) {
            $rootScope.cat_has_custom_form=response.status;

            if(response.status){
                $rootScope.StepCount=3;
            }else{
                $rootScope.StepCount=2;

            }
        });

        $rootScope.case_id     = $stateParams.case_id;
            $rootScope.CaseStep=false;
            $rootScope.CustomFormStep=false;
            $rootScope.CaseDocumentsStep=false;


         category.get({'id':$stateParams.category_id,'type':'sponsorships'},function (response) {
             if ($rootScope.lang == 'ar'){
                 $rootScope.CategoryName=response.name;
             }
             else{
                 $rootScope.CategoryName=response.en_name;
             }
         });

        Target = function(name){
            $state.go('sub.'+name, {
                id:$rootScope.person_id,
                guardian_id:$rootScope.guardian_id,
                case_id:$rootScope.case_id,
                father_id:$rootScope.father_id,
                mother_id: $rootScope.mother_id,
                category_id:$rootScope.category_id
            });

        };


        $rootScope.getTheSubTarget= function(target) {
            if(target == 'case'){
                Target('fm-case-info');
            } else if(target == 'customFormData'){
                Target('fm-custom-form-data');
            }else if(target == 'caseDocuments'){
                Target('fm-case-documents');
            }

        };


        $rootScope.setStepSub = function (curStep) {

            if($stateParams.id){
                var sec=['#CaseData','#subcaseDocuments','#subcustomForm'];
                angular.forEach(sec, function(val,key)
                {
                    angular.element(val).removeClass("disabled");
                });
                angular.element('.step').removeClass("disabled");
                $('.step').removeClass("disabled");

            }

            var curStepName = curStep;
            var width = 1;

            var sec=['#CaseData','#subcaseDocuments','#subcustomForm'];
            angular.forEach(sec, function(val,key)
            {
                if(val != curStepName){
                    $(val).css({"background-color":"white","border-bottom":"none"});
                }else{
                    width = key+1;
                    $(curStepName).css({"background-color":"aliceblue","border-bottom":"2px solid #337ab7"});
                }
            });
            angular.element('.progress-bar').css('width', (width * 30)+'%');
        }
    });