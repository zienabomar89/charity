
angular.module('SponsorshipModule')
    .controller('subCaseDocumentController', function ($scope,$rootScope,$state,$stateParams ,$uibModal,OAuthToken,category, FileUploader,cases) {

        $rootScope.TheStep=2;
        $rootScope.person_id=$stateParams.id;

        $rootScope.failed =false;
        ;
        $rootScope.model_error_status =false;

        $rootScope.close=function(){

            $rootScope.failed =false;
            ;
            $rootScope.model_error_status =false;
        };

        $rootScope.status ='';
        $rootScope.msg ='';
        $rootScope.msg2 ='';

        $rootScope.person_id=$stateParams.id;
        $rootScope.category_id=$stateParams.category_id;
        $rootScope.case_id     = $stateParams.case_id;
        $rootScope.father_id     = $stateParams.father_id;
        $rootScope.mother_id     = $stateParams.mother_id;
        $rootScope.guardian_id     = $stateParams.guardian_id;


       var loadDocuments=function(){
            category.getCategoryDocuments({case_id:$rootScope.case_id,target:'case',scope:3,type: 'sponsorships'},function (response){
                    $rootScope.documents =response.documents;
                    $rootScope.result='true';
                });
        };

        loadDocuments();

        $scope.Previous=function(){
            $rootScope.getTheSubTarget('case');
        };

        $scope.forward=function(){
            $rootScope.clearToastr();
            if($rootScope.cat_has_custom_form){
                angular.element('#subcustomForm').removeClass("disabled");
                $state.go('sub.fm-custom-form-data', {
                    id:$rootScope.person_id,
                    guardian_id:$rootScope.guardian_id,
                    case_id:$rootScope.case_id,
                    father_id:$rootScope.father_id,
                    mother_id: $rootScope.mother_id,
                    category_id:$rootScope.category_id
                });            }else{
                $state.go('form.family-info',{category_id:$rootScope.category_id,'father_id':$rootScope.father_id,
                    mother_id:$rootScope.mother_id,'guardian_id':$rootScope.guardian_id});

            }

        };
        $scope.showImage=function(file){
            $rootScope.clearToastr();
            $rootScope.target=file;
            $uibModal.open({
                templateUrl: 'showImage.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance) {

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                },
                resolve: {

                }
            });
        };

        $scope.ChooseAttachment=function(item,action){
            $rootScope.clearToastr();


            $uibModal.open({
                templateUrl: 'myModalContent2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,cases,OAuthToken, FileUploader) {

                    $scope.fileupload = function () {
                        if($scope.uploader.queue.length){
                            var image = $scope.uploader.queue.slice(1);
                            $scope.uploader.queue = image;
                        }
                    };
                    var uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/doc/files',
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });
                    uploader.onBeforeUploadItem = function(item) {
                        $rootScope.clearToastr();
                    };
                    uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(response.status=='error')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else{
                        fileItem.id = response.id;
                        cases.setAttachment({id:item.person_id,type:'sponsorships',action:action,document_type_id:item.document_type_id,document_id:fileItem.id},function (response1) {
                            if(response1.status=='error' || response1.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response1.msg);
                            }
                            else if(response1.status== 'success'){

                                $modalInstance.close();
                                $rootScope.toastrMessages('success',response1.msg);
                                loadDocuments();
                            }

                        }, function(error) {
                            $rootScope.toastrMessages('error',error.data.msg);
                        });
                    }};

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

    });