
angular.module('SponsorshipModule')
    .controller('FamilyInfoController', function ($scope,$rootScope,$state,$stateParams,$uibModal,$filter, related,Entity,persons,category) {

        $rootScope.clearToastr();
        $rootScope.category_id=$stateParams.category_id;
        $rootScope.person_id     = $stateParams.person_id;
        $rootScope.case_id     = $stateParams.case_id;
        $rootScope.guardian_id = $stateParams.guardian_id;
        $rootScope.father_id   = $stateParams.father_id;
        $rootScope.mother_id   = $stateParams.mother_id;
        $scope.msg1=[];
        $rootScope.items=[];

        $rootScope.failed =false;
        ;
        $rootScope.model_error_status =false;
        $rootScope.result='false';
        $rootScope.result_1='false';

        $rootScope.close=function(){

            $rootScope.failed =false;
            ;
            $rootScope.model_error_status =false;
        };

        $rootScope.status ='';
        $rootScope.msg ='';
        $rootScope.msg2 ='';

        $rootScope.ResetForm($rootScope.category_id, 'add','FamilyData');
        $rootScope.TheCurrentStep=$rootScope.FamilyStepNumber;


        category.getPriority({'type':'sponsorships','category_id':$rootScope.category_id ,'target':'fm','category':true},function (response) {
            $rootScope.priority=response.priority;
            $rootScope.case_is=response.category.case_is;
            $rootScope.family_structure=response.category.family_structure;
            $rootScope.has_mother=response.category.has_mother;
        });

        var loadFamily=function(){

           var param={};


            if(!angular.isUndefined($rootScope.person_id) && $rootScope.person_id != null && $rootScope.person_id != "") {
                param={'l_person_id' :$rootScope.person_id};
                }else {
                $rootScope.person_id = null;

                if(!angular.isUndefined($rootScope.mother_id) && $rootScope.mother_id != null && $rootScope.mother_id != "") {
                    param={'l_person_id' :$rootScope.father_id,'mother_id': $rootScope.mother_id};
                }else{
                    param={'l_person_id' :$rootScope.guardian_id};
                }
            }
            param.category_id = $rootScope.category_id;

            related.getFamily(param,function (response) {
                if(response.fm.length != 0){
                    $rootScope.result='true';
                }else{
                    $rootScope.result='false';
               }

                $rootScope.items=response.fm;

                if(response.fm_cases.length != 0){
                    $rootScope.result_1='true';
                }else{
                    $rootScope.result_1='false';
                }
                $rootScope.Caseitems=response.fm_cases;
            }, function(error) {
                $rootScope.toastrMessages('error',error.data.msg);
            });

        }


        Entity.get({'entity':'entities' , 'c':'educationStages,workJobs,maritalStatus,kinship'},function (response){
            $rootScope.EduStages = response.educationStages;
            $rootScope.Kinship = response.kinship;
            $rootScope.WorkJob = response.workJobs;
            $rootScope.MaritalStatus = response.maritalStatus;
        });

        $scope.delete = function(size,id) {
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'myModalContent2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope,$stateParams, $modalInstance) {

                    $scope.msg1={};
                    $scope.persons={};
                    $scope.model_error=false;

                    $scope.confirmDelete = function (data) {
                        $rootScope.clearToastr();
                        persons.delete({id:id,l_person_id:$rootScope.l_person_id},function (response) {

                            if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            } else {
                                if(response.status == 'success'){
                                    loadFamily();
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                $modalInstance.close();
                            }
                        }, function(error) {
                            $modalInstance.close();
                            $rootScope.toastrMessages('error',error.data.msg);
                        });

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };
        $scope.add = function(size,action,id) {
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'newPerson.html',
                size: size,
                controller: function ($rootScope,$scope,$stateParams, $modalInstance,$filter,category) {

                    $scope.msg1={};
                    $scope.persons={};
                    $scope.model_error=false;
                    $scope.msg32='';
                    $scope.action=action;

                    $scope.setName=function(value){
                        if( !angular.isUndefined($scope.msg1.kinship_id)) {
                            $scope.msg1.kinship_id = [];
                        }
                        if(value ==13 || value ==14 || value ==3 || value ==4){
                            var param={};
                            if(!angular.isUndefined($rootScope.person_id) && $rootScope.person_id != null && $rootScope.person_id != "") {
                                $rootScope.l_person_id=$rootScope.person_id;
                            }else {
                                $rootScope.person_id = null;
                                if(!angular.isUndefined($rootScope.mother_id) && $rootScope.mother_id != null && $rootScope.mother_id != "") {
                                    $rootScope.l_person_id=$rootScope.father_id;
                                }else{
                                    $rootScope.l_person_id=$rootScope.guardian_id;
                                }
                            }
                            // if($rootScope.case_is == 1 && $rootScope.family_structure ==2){
                            //     // if($rootScope.case_is == 1 && $rootScope.family_structure ==2 &&  $rootScope.has_mother ==2){
                            //     $rootScope.l_person_id=$rootScope.father_id;
                            // }
                            //
                            // if($rootScope.case_is == 1 && $rootScope.family_structure == 1){
                            //     $rootScope.l_person_id=$rootScope.person_id;
                            // }
                            //
                            // if($rootScope.case_is == 2 && $rootScope.family_structure == 1){
                            //     $rootScope.l_person_id=$rootScope.guardian_id;
                            // }
                            //
                            var params={ 'mode':'edit','target':'info', 'person' :true, 'persons_i18n' : true,'person_id':$rootScope.l_person_id};
                            persons.getPersonReports(params,function(response){
                                if(value ==1) {
                                    $scope.persons.first_name=response.second_name;
                                    $scope.persons.second_name=response.third_name;
                                    $scope.persons.last_name=response.last_name;

                                    $scope.persons.en_first_name=response.en_second_name;
                                    $scope.persons.en_second_name=response.en_third_name;
                                    $scope.persons.en_last_name=response.en_last_name;
                                }

                                if(value ==13 || value ==14) {
                                    $scope.persons.second_name=response.first_name;
                                    $scope.persons.third_name=response.second_name;
                                    $scope.persons.last_name=response.last_name;
                                    $scope.persons.en_second_name=response.en_first_name;
                                    $scope.persons.en_third_name=response.en_second_name;
                                    $scope.persons.en_last_name=response.en_last_name;
                                }

                                if(value ==3 || value ==4) {
                                    $scope.persons.second_name=response.second_name;
                                    $scope.persons.third_name=response.third_name;
                                    $scope.persons.last_name=response.last_name;
                                    $scope.persons.en_second_name=response.en_second_name;
                                    $scope.persons.en_third_name=response.en_third_name;
                                    $scope.persons.en_last_name=response.en_last_name;
                                }
                            }, function(error) {
                                $rootScope.toastrMessages('error',error.data.msg);
                            });


                        }
                    };
                    if(id != -1){
                        var params={'mode':'edit','person' :true, 'persons_i18n' : true, 'health' : true,  'education' : true, 'work' : true};

                        // if(!angular.isUndefined($rootScope.person_id) && $rootScope.person_id != null && $rootScope.person_id != "") {
                        //     params.l_person_id=$rootScope.person_id;
                        // }else {
                        //     $rootScope.person_id = null;
                        //     if(!angular.isUndefined($rootScope.mother_id) && $rootScope.mother_id != null && $rootScope.mother_id != "") {
                        //         params.l_person_id=$rootScope.father_id;
                        //         params.mother_id=$rootScope.mother_id;
                        //     }else{
                        //         params.l_person_id=$rootScope.guardian_id;
                        //     }
                        //
                        // }
                        if($rootScope.case_is == 1 && $rootScope.family_structure ==2){
                            // if($rootScope.case_is == 1 && $rootScope.family_structure ==2 &&  $rootScope.has_mother ==2){
                            params.father_id= $rootScope.father_id;
                            params.mother_id= $rootScope.mother_id;
                            params.l_person_id=$rootScope.father_id;
                        }

                        if($rootScope.case_is == 1 && $rootScope.family_structure == 1){
                            params.l_person_id=$rootScope.person_id;
                        }

                        if($rootScope.case_is == 2 && $rootScope.family_structure == 1){
                            params.l_person_id=$rootScope.guardian_id;
                        }
                        params.person_id=id;
                        params.target='info';
                        persons.getPersonReports(params,function (response) {
                              $scope.persons=response;
                              if($scope.persons.marital_status_id==null){
                                  $scope.persons.marital_status_id="";
                              } else{
                                  $scope.persons.marital_status_id = $scope.persons.marital_status_id+"";
                              }

                              if($scope.persons.marital_status_id==null){
                                  $scope.persons.marital_status_id="";
                              } else{
                                  $scope.persons.marital_status_id = $scope.persons.marital_status_id+"";
                              }


                              if($scope.persons.gender ==0 || $scope.persons.gender ==null){
                                  $scope.persons.gender="";
                              }else{
                                  $scope.persons.gender=$scope.persons.gender+"";
                              }

                              if($scope.persons.kinship_id==null){
                                  $scope.persons.kinship_id="";
                              } else{
                                  $scope.persons.kinship_id=$scope.persons.kinship_id+"";
                              }

                            if($scope.persons.study == null){
                                $scope.persons.study = "";
                            }else{
                                $scope.persons.study= $scope.persons.study +"";

                                if($scope.persons.study == 0){
                                    $scope.persons.stage="";
                                }else{
                                    if($scope.persons.stage==null){
                                        $scope.persons.stage="";
                                    } else{
                                        $scope.persons.stage=$scope.persons.stage+"";
                                    }
                                }
                            }



                              if($scope.persons.working == null ){
                                  $scope.persons.working="";
                                  $scope.persons.work_job_id="";
                              }else{
                                  $scope.persons.working=$scope.persons.working+"";
                                  if($scope.persons.working == 1){
                                      $scope.persons.work_job_id = $scope.persons.work_job_id+"";
                                  }else{
                                      $scope.persons.work_job_id="";
                                  }
                              }

                              if($scope.persons.condition == null ){
                                  $scope.persons.condition="";
                              }else{
                                  $scope.persons.condition=$scope.persons.condition+"";
                              }
                          });
                    }


                    $scope.isPersonExists = function(id_card_number){
                        $rootScope.clearToastr();
                        if( !angular.isUndefined($scope.msg1.id_card_number)) {
                            $scope.msg1.id_card_number = [];
                        }
                        if($scope.action =='add'){
                            if(!angular.isUndefined(id_card_number)){
                                if(id_card_number.length==9 ) {

                                    if($rootScope.check_id(id_card_number)){
                                        var params={'id_card_number':id_card_number,'person':true,'persons_i18n':true,'health':true,'education':true,'work':true};

                                    // if(!angular.isUndefined($rootScope.person_id) && $rootScope.person_id != null && $rootScope.person_id != "") {
                                    //     params.l_person_id=$rootScope.person_id;
                                    // }else {
                                    //     $rootScope.person_id = null;
                                    //     if(!angular.isUndefined($rootScope.mother_id) && $rootScope.mother_id != null && $rootScope.mother_id != "") {
                                    //         params.l_person_id=$rootScope.father_id;
                                    //         params.mother_id=$rootScope.mother_id;
                                    //     }else{
                                    //         params.l_person_id=$rootScope.guardian_id;
                                    //     }
                                    //
                                    // }

                                    if($rootScope.case_is == 1 && $rootScope.family_structure ==2){
                                        // if($rootScope.case_is == 1 && $rootScope.family_structure ==2 &&  $rootScope.has_mother ==2){
                                        params.father_id= $rootScope.father_id;
                                        params.mother_id= $rootScope.mother_id;
                                        params.l_person_id=$rootScope.father_id;
                                    }

                                    if($rootScope.case_is == 1 && $rootScope.family_structure == 1){
                                        params.l_person_id=$rootScope.person_id;
                                    }

                                    if($rootScope.case_is == 2 && $rootScope.family_structure == 1){
                                        params.l_person_id=$rootScope.guardian_id;
                                    }
                                    persons.find(params,function (response) {

                                        if(response.status ==true){
                                            $rootScope.toastrMessages('error',response.msg);
                                            $scope.persons=response.person;

                                            if($scope.persons.marital_status_id==null){
                                                $scope.persons.marital_status_id="";
                                            } else{
                                                $scope.persons.marital_status_id = $scope.persons.marital_status_id+"";
                                            }

                                            if($scope.persons.marital_status_id==null){
                                                $scope.persons.marital_status_id="";
                                            } else{
                                                $scope.persons.marital_status_id = $scope.persons.marital_status_id+"";
                                            }

                                            if($scope.persons.gender ==0 || $scope.persons.gender ==null){
                                                $scope.persons.gender="";
                                            }else{
                                                $scope.persons.gender=$scope.persons.gender+"";
                                            }

                                            if($scope.persons.kinship_id==null){
                                                $scope.persons.kinship_id="";
                                            } else{
                                                $scope.persons.kinship_id=$scope.persons.kinship_id+"";
                                            }

                                            if($scope.persons.study == null){
                                                $scope.persons.study = "";
                                            }else{
                                                $scope.persons.study= $scope.persons.study +"";

                                                if($scope.persons.study == 0){
                                                    $scope.persons.stage="";
                                                }else{
                                                    if($scope.persons.stage==null){
                                                        $scope.persons.stage="";
                                                    } else{
                                                        $scope.persons.stage=$scope.persons.stage+"";
                                                    }
                                                }
                                            }

                                            if($scope.persons.working == null ){
                                                $scope.persons.working="";
                                                $scope.persons.work_job_id="";
                                            }else{
                                                $scope.persons.working=$scope.persons.working+"";
                                                if($scope.persons.working == 1){
                                                    $scope.persons.work_job_id = $scope.persons.work_job_id+"";
                                                }else{
                                                    $scope.persons.work_job_id="";
                                                }
                                            }

                                            if($scope.persons.condition == null ){
                                                $scope.persons.condition="";
                                            }else{
                                                $scope.persons.condition=$scope.persons.condition+"";
                                            }
                                        }
                                        else{

                                            if(response.person){
                                                $scope.persons    = response.person;

                                                if($scope.persons.birthday =="0000-00-00" || $scope.persons.birthday == null){
                                                    $scope.persons.birthday    = new Date();
                                                }else{
                                                    $scope.persons.birthday    = new Date($scope.persons.birthday);
                                                }
                                            }
                                            if(response.msg){
                                                $rootScope.toastrMessages('error',response.msg);
                                                $scope.persons={};
                                            }else{
                                                $scope.model_error=false;
                                                $scope.msg32='';
                                            }



                                            $scope.persons.id=null;
                                           $scope.persons.person_id=null;
                                        }
                                    }, function(error) {
                                        $rootScope.toastrMessages('error',error.data.msg);
                                    });

                                    }else{
                                        $rootScope.toastrMessages('error',$filter('translate')('invalid card number'));
                                    }
                                }
                            }
                        }
                    };
                    $scope.save = function (data) {
                        $rootScope.clearToastr();
                        if( !angular.isUndefined(data.birthday)) {
                            data.birthday= $filter('date')(data.birthday, 'dd-MM-yyyy')
                        }

                        // WHEN ($rootScope.case_is = 1 && $rootScope.family_structure=2) THEN (c.id,c.father_id ,c.mother_id)
                        // WHEN ($rootScope.case_is = 1 && $rootScope.family_structure=1) THEN (c.id,null,null)
                        // WHEN ($rootScope.case_is = 2 && $rootScope.family_structure=1) THEN (c.id,char_guardians.guardian_id,null)

                        if($rootScope.case_is == 1 && $rootScope.family_structure ==2){
                        // if($rootScope.case_is == 1 && $rootScope.family_structure ==2 &&  $rootScope.has_mother ==2){
                            data.father_id= $rootScope.father_id;
                            data.mother_id= $rootScope.mother_id;
                            data.l_person_id=$rootScope.father_id;
                        }

                        if($rootScope.case_is == 1 && $rootScope.family_structure == 1){
                            data.l_person_id=$rootScope.person_id;
                        }

                       if($rootScope.case_is == 2 && $rootScope.family_structure == 1){
                            data.l_person_id=$rootScope.guardian_id;
                        }

                        // if(!angular.isUndefined($rootScope.guardian_id) && $rootScope.guardian_id != null && $rootScope.guardian_id != "") {
                        //     data.l_person_id=$rootScope.guardian_id;
                        //     // data.guardian_id=$rootScope.guardian_id;
                        // }else if(!angular.isUndefined($rootScope.person_id) && $rootScope.person_id != null && $rootScope.person_id != "") {
                        //     data.l_person_id=$rootScope.person_id;
                        //     // data.guardian_id=$rootScope.person_id;
                        // }
                        // data.father_id= $rootScope.father_id;
                        // data.mother_id= $rootScope.mother_id;
                        // data.guardian_id= $rootScope.guardian_id;
                        data.l_person_update= true;
                        data.priority= $rootScope.priority;


                        if(data.id){
                            data.person_id=data.id;
                            delete data.id;
                        }

                        persons.save(data,function (response) {
                            if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            } else {

                                if(response.status){
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    loadFamily();
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }

                                $modalInstance.close();
                            }
                        }, function(error) {
                            $rootScope.toastrMessages('error',error.data.msg);
                        });
                    };
                    $scope.restValue = function(via,value){

                        if(via=='working') {
                            if( !angular.isUndefined($scope.msg1.working)) {
                                $scope.msg1.working = [];
                            }
                            if( !angular.isUndefined($scope.msg1.work_job_id)) {
                                $scope.msg1.work_job_id = [];
                            }
                            $scope.persons.work_job_id="";
                        }
                        else if( via =='study'){
                            if( !angular.isUndefined($scope.msg1.study)) {
                                $scope.msg1.study = [];
                            }

                            if(value == 0){
                                if( !angular.isUndefined($scope.msg1.stage)) {
                                    $scope.msg1.stage = [];
                                }

                                $scope.persons.stage="";

                            }
                        }

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };
        $scope.case= function (id,case_id) {
            if(id == -1){
                $state.go('sub.fm-case-info',{category_id:$rootScope.category_id,'father_id':$rootScope.father_id,
                    mother_id:$rootScope.mother_id,'guardian_id':$rootScope.guardian_id});
            }else{
                $state.go('sub.fm-case-info',{case_id:case_id,id:id,category_id:$rootScope.category_id,'father_id':$rootScope.father_id,
                    mother_id:$rootScope.mother_id,'guardian_id':$rootScope.guardian_id});

            }

        }
        $rootScope.dateOptions = {
            formatYear: 'yy',
            startingDay: 0
        };
        $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $rootScope.format = $rootScope.formats[0];
        $rootScope.today = function() {
            $rootScope.dt = new Date();
        };

        $rootScope.today();
        $rootScope.clear = function()
        {
            $rootScope.dt = null;
        };

        $rootScope.open1 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $rootScope.popup1.opened = true;
        };
        $rootScope.popup1 = {
            opened: false
        };

        loadFamily();

    });