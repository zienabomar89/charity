
angular.module('SponsorshipModule')
    .controller('GuardianInfoController', function ($scope,$filter,$rootScope,$state,cases,$uibModal,$stateParams,category,Entity,persons) {

        $rootScope.msg1={};
        $scope.persons={};
        $scope.Wives={};
        $rootScope.guardian={banks:[]};
        $scope.success_frm=false;
        $rootScope.success_msg="";
        $rootScope.gpriority=null;
        $rootScope.category_id=$stateParams.category_id;
        $rootScope.person_id     = $stateParams.person_id;
        $rootScope.case_id     = $stateParams.case_id;
        $rootScope.guardian_id = $stateParams.guardian_id;
        $rootScope.father_id   = $stateParams.father_id;
        $scope.persons_id   = $stateParams.mother_id;

        $rootScope.ResetForm($rootScope.category_id, 'add','providerData');
        $rootScope.TheCurrentStep=$rootScope.GuardianStepNumber;

  // -----------------------------------------------------------------------------------------------------

        Entity.get({'entity':'entities' , 'c':'educationStages,workJobs,maritalStatus,countries,propertyTypes,kinship,roofMaterials,furnitureStatus,houseStatus'},function (response){
            $scope.EduStages = response.educationStages;
            $scope.WorkJob = response.workJobs;
            $scope.MaritalStatus = response.maritalStatus;
            $scope.Country = response.countries;
            $scope.Kinship = response.kinship;
            $scope.PropertyTypes = response.propertyTypes;
            $scope.RoofMaterials = response.roofMaterials;
            $scope.houseStatus = response.houseStatus;
            $scope.furnitureStatus = response.furnitureStatus;
            });


// -----------------------------------------------------------------------------------------------------
        var setProvider=function(item,choice){
            $rootScope.guardian= item;

            if($rootScope.guardian.banks.length ==1){
                $rootScope.guardian.banks[0].check=true;
            }

            if(item.id == $rootScope.person_id){
                choice = -2;
            }
            else if(item.id == $rootScope.father_id){
              choice = -5;
            }
            else if(item.id == $rootScope.mother_id){
                choice = -3;
           }else{
                choice = -1;
                }


            if($rootScope.has_Wives == 'true'){
                angular.forEach($scope.Wives, function(v, k) {
                    if(item.id = v.id){
                        choice =  v.id;
                        return ;
                    }
                });
            }

            $rootScope.guardian.WhoIsGuardian = choice + "";

            if(angular.isUndefined($rootScope.guardian.kinship_id)){
                $rootScope.guardian.kinship_id = "";
            }
            else{
                if($rootScope.guardian.kinship_id==null){
                    $rootScope.guardian.kinship_id = "";
                }else{
                    $rootScope.guardian.kinship_id = $rootScope.guardian.kinship_id +"";
                }
            }

                    if($rootScope.guardian.nationality==null){
                        $rootScope.guardian.nationality = "";
                    }else{
                        $rootScope.guardian.nationality = $rootScope.guardian.nationality+"";
                    }

                    if($rootScope.guardian.monthly_income==null){
                            $rootScope.guardian.monthly_income = "";
                        }else{
                        $rootScope.guardian.monthly_income = $rootScope.guardian.monthly_income+"";
                    }



            if($rootScope.guardian.study == null){
                $rootScope.guardian.study = "";
                $rootScope.guardian.stage="";
            }else{
                $rootScope.guardian.study = $scope.guardian.study+"";

                if($rootScope.guardian.study == 0){
                    $rootScope.guardian.stage="";
                    $rootScope.guardian.specialization ="";
                }else{
                    if($rootScope.guardian.stage==null){
                        $rootScope.guardian.stage="";
                    }
                    else{
                        $rootScope.guardian.stage = $rootScope.guardian.stage+"";
                    }
                }
            }


            if($rootScope.guardian.marital_status_id==null){
                        $rootScope.guardian.marital_status_id="";
                    } else{
                        $rootScope.guardian.marital_status_id = $rootScope.guardian.marital_status_id+"";
                    }

                    if($rootScope.guardian.death_cause_id !=null){
                        $rootScope.guardian.death_cause_id = $rootScope.guardian.death_cause_id+"";
                    }else{
                        $rootScope.guardian.death_cause_id = "";

                    }

                if($rootScope.guardian.country !=null){
                    $rootScope.guardian.country = $rootScope.guardian.country+"";
                    Entity.listChildren({entity:'districts','id':$rootScope.guardian.country,'parent':'countries'},function (response) {
                        $scope.governarate = response;
                    });
                }else{
                    $rootScope.guardian.country ="";
                }

                if($rootScope.guardian.governarate !=null){
                    $rootScope.guardian.governarate = $rootScope.guardian.governarate+"";

                    Entity.listChildren({parent:'districts','id':$rootScope.guardian.governarate,entity:'cities'},function (response) {
                        $scope.city = response;
                    });
                }else{
                    $rootScope.guardian.governarate ="";
                }

                if($rootScope.guardian.city !=null){
                    $rootScope.guardian.city = $rootScope.guardian.city+"";
                    Entity.listChildren({entity:'neighborhoods','id':$rootScope.guardian.city,'parent':'cities'},function (response) {
                        $scope.nearlocation = response;
                    });
                }else{
                    $rootScope.guardian.city ="";
                }


            if($rootScope.guardian.location_id !=null){
                $rootScope.guardian.location_id = $rootScope.guardian.location_id+"";
                Entity.listChildren({entity:'mosques','id':$rootScope.guardian.location_id,'parent':'neighborhoods'},function (response) {
                    $scope.mosques = response;
                });
            }else{
                $rootScope.guardian.location_id ="";
            }

            if($rootScope.guardian.mosques_id !=null){
                $rootScope.guardian.mosques_id = $rootScope.guardian.mosques_id+"";
            }else{
                $rootScope.guardian.mosques_id ="";
            }


                if($rootScope.guardian.working == null) {
                    $rootScope.guardian.working = "";
                    $rootScope.guardian.work_job_id    = "";
                    $rootScope.guardian.work_location  = "";

                }else{
                    $rootScope.guardian.working = $rootScope.guardian.working +"";

                    if($rootScope.guardian.working == 1) {
                        $rootScope.guardian.work_job_id    = $rootScope.guardian.work_job_id +"";
                    }else{
                        $rootScope.guardian.work_job_id    = "";
                        $rootScope.guardian.work_location  = "";
                    }

                }

                if($rootScope.guardian.property_type_id ==null){
                    $rootScope.guardian.property_type_id="";
                }
                else{
                    $rootScope.guardian.property_type_id =$rootScope.guardian.property_type_id +"";
                }
                if($rootScope.guardian.roof_material_id ==null){
                    $rootScope.guardian.roof_material_id="";
                }
                else{
                    $rootScope.guardian.roof_material_id =$rootScope.guardian.roof_material_id +"";
                }

                if($rootScope.guardian.residence_condition ==null){
                    $rootScope.guardian.residence_condition="";
                }
                else{
                    $rootScope.guardian.residence_condition =$rootScope.guardian.residence_condition +"";
                }

                if($rootScope.guardian.indoor_condition ==null){
                    $rootScope.guardian.indoor_condition="";
                }
                else{
                    $rootScope.guardian.indoor_condition =$rootScope.guardian.indoor_condition +"";
                }


        };
        $scope.find=function(action,id,check,choice){

            $rootScope.clearToastr();
            $rootScope.msg1={};
            var params={'mode':'edit','person' :true, 'persons_i18n' : true, 'contacts' : true,  'education' : true,
                'work' : true, 'residence' : true, 'banks' : true };
            // ,'individual_id': $rootScope.father_id

            if($rootScope.father_dead == false){
                params.individual_id=$rootScope.father_id;
                // params.r_person_id=$rootScope.father_id;
            }

            if(action =='find') {
                if($rootScope.check_id(id)){
                    params.id_card_number=id;
                    params.category_id=$rootScope.category_id;
                    params.target='guardian';

                    $rootScope.progressbar_start();
                    persons.find(params, function (response) {
                        $rootScope.progressbar_complete();

                    if(response.status == true){
                        if(action =='find') {
                            $rootScope.toastrMessages('error',response.msg);
                        }

                        var record = response.person;

                        if(record.id == $rootScope.person_id){
                            choice = -2;
                        }
                        else if(record.id == $rootScope.father_id){
                            choice = -5;
                        }
                        else if(record.id == $rootScope.mother_id){
                            choice = -3;
                        }else{
                            choice = -1;
                        }


                        if($rootScope.has_Wives == 'true'){
                            angular.forEach($scope.Wives, function(v, k) {
                                if(record.id = v.id){
                                    choice =  v.id;
                                    return ;
                                }
                            });
                        }
                        if($rootScope.has_Wives){
                            angular.forEach($scope.Wives, function(v, k) {
                                if(record.id = v.id){
                                    choice =  v.id;
                                    return ;
                                }
                            });
                        }

                        setProvider(record,choice);

                    }else{
                        if( !angular.isUndefined(response.blocked)) {

                            if(response.blocked){
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.persons = [];
                                $rootScope.guardian.banks = [];
                            }else{
                                $rootScope.guardian.banks = [];
                                $rootScope.guardian.id="";
                                $rootScope.clearToastr();
                            }

                        }else{
                            $rootScope.guardian.banks = [];
                            $rootScope.guardian.id="";
                            $rootScope.clearToastr();
                            $scope.msg="";
                        }

                        if(response.person){
                            $rootScope.guardian    = response.person;

                            if($rootScope.guardian.birthday =="0000-00-00" || $rootScope.guardian.birthday == null){
                                $rootScope.guardian.birthday    = new Date();
                            }else{
                                $rootScope.guardian.birthday    = new Date($rootScope.guardian.birthday);
                            }
                            $rootScope.guardian.banks = [];
                            $rootScope.guardian.id="";
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }

                    }
                }, function(error) {
                    $rootScope.toastrMessages('error',error.data.msg);
                });
                }else{
                    $rootScope.guardian.banks = [];
                    $rootScope.guardian.id="";
                    $rootScope.toastrMessages('error',$filter('translate')('invalid card number'));
                }

            }
            else{
                params.person_id=id;
                params.target='info';
                persons.getPersonReports(params, function (response) {
                    setProvider(response,choice);
                });
            }


        };
        if( !angular.isUndefined($rootScope.guardian_id)) {
            if($rootScope.guardian_id != '' && $rootScope.guardian_id !=  null&& $rootScope.guardian_id !=  'null') {

                category.getPriority({'type':'sponsorships','category_id':$rootScope.category_id ,'target':'guardian','category':true},function (response) {
                    $rootScope.gpriority=response.priority;
                    if(response.category.father_dead ==0){
                        $rootScope.father_dead=false;
                    }else {
                        $rootScope.father_dead=true;
                    }
                    $scope.find('get',$rootScope.guardian_id,false,null);
                });
            }
        }
        if( !angular.isUndefined($rootScope.father_id)) {
            if($rootScope.father_id != '' && $rootScope.father_id !=  null&& $rootScope.father_id !=  'null') {
                //if($rootScope.has_mother == false){
                persons.getPersonReports({'action':'filters','target':'info','person_id':$rootScope.father_id, 'mode':'edit', 'wives' :true},function(response) {
                    $scope.Wives=response.wives;
                });
                //}
            }
        }

        if(!$rootScope.gpriority){
            category.getPriority({'type':'sponsorships','category_id':$rootScope.category_id ,'target':'guardian','category':true},function (response) {
                $rootScope.gpriority=response.priority;
                if(response.category.father_dead ==0){
                    $rootScope.father_dead=false;
                }else {
                    $rootScope.father_dead=true;
                }
            });
        }
//----------------------------------------------------------------------------------------------------------------------
        $scope.SetGuardian = function(choice){

            $rootScope.clearToastr();
            $scope.error=false;
            $scope.msg="";


            if( !angular.isUndefined($rootScope.msg1.WhoIsGuardian)) {
                $rootScope.msg1.WhoIsGuardian = [];
            }

            if(choice == -1 ){
                $scope.persons={};
                $rootScope.guardian.id_card_number="";
                $rootScope.guardian.banks=[];
                $rootScope.guardian.WhoIsGuardian=-1+"";
            }else{
                var id=null;
                if(choice==-2){
                   if(($rootScope.who_is_case && ! (!$rootScope.has_mother && $rootScope.has_Wives)) && $rootScope.person_id != null){
                        id=$rootScope.person_id;
                   }

                }
                else if(choice==-5){
                    if((!$rootScope.has_mother && $rootScope.has_Wives && !$rootScope.who_is_case) && $rootScope.father_id != null){
                        id=$rootScope.father_id;
                   }
                }
                else if(choice==-3){

                    if($rootScope.has_mother && $rootScope.mother_id != null){
                        id=$rootScope.mother_id;
                    }else {
                        if($rootScope.who_is_case &&  $rootScope.person_id != null){
                            // GET MOTHER
                            $rootScope.toastrMessages('error','GET MOTHER')
                        }
                    }
                }
                else if(choice==-4){
                    if($rootScope.who_is_case &&  $rootScope.person_id != null){
                        // GET MOTHER
                        $rootScope.toastrMessages('error','GET FATHER')
                    }
                }
                else{
                    id=choice;
                }

                if(id != null){
                    $scope.find('get',id,true,choice);
                }

             }


        };
        $scope.isExists= function(target,id_card_number){
            $rootScope.clearToastr();
            var choice=-1;
            var check= false;

            if( !angular.isUndefined($rootScope.guardian.WhoIsGuardian)) {
                choice=$rootScope.guardian.WhoIsGuardian;
            }

            if(!angular.isUndefined($rootScope.guardian.id_card_number)) {
                if(id_card_number.length==9) {
                    if(choice == -1){
                        check = true;
                    }
                    $scope.find('find',id_card_number,check,choice);
                }
            }
        };

        var  saveGuardian= function (params,step) {

            $rootScope.clearToastr();
            $scope.error=false;
            $rootScope.msg='';

            angular.element('#'+$rootScope.FormSponsorshipSection[$rootScope.TheCurrentStep].id).removeClass("disabled");
            angular.element('#'+$rootScope.FormSponsorshipSection[$rootScope.TheCurrentStep-2].id).removeClass("disabled");


            if( !angular.isUndefined(params.birthday)) {
                params.birthday= $filter('date')(params.birthday, 'dd-MM-yyyy')
            }

            if($rootScope.father_dead == false){
                params.individual_id=$rootScope.father_id;
                params.individual_update=true;
                // params.r_person_id=$rootScope.father_id;
            }

            // data.father_id=$rootScope.father_id;
            params.priority=$rootScope.gpriority;
            params.target='guardian';
            params.category_id=$rootScope.category_id;
            params.category_type=1;

            if(params.id){
                params.person_id=params.id;
                delete params.id;
            }

            $rootScope.progressbar_start();
            persons.save(params,function (response) {
                $rootScope.progressbar_complete();
                if(response.status=='failed_valid')
                {
                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                    $scope.status1 =response.status;
                    $rootScope.msg1 =response.msg;
                } else if(response.status== 'success'){
                    $rootScope.guardian.person_id   = response.person.id;
                    $rootScope.guardian_id   = response.person.id;
                    if( !angular.isUndefined(response.person.case_id)) {
                        $rootScope.case_id   = response.person.case_id ;
                    }
                    if(step =='next'){
                        $rootScope.Next();
                    }else{
                        $rootScope.Previous();
                    }

                }
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };

        $scope.forward=function(data,step){

            $rootScope.clearToastr();
            //
            // if($rootScope.has_mother){
            //     if($rootScope.father_id == null ||$rootScope.mother_id == null || $rootScope.mother_id == "" || $scope.persons_id == null || $rootScope.father_id == "" || $scope.persons_id == "" ){
            //         $rootScope.msg='يجب عليك ادخال بيانات الأب و الام وتحديد الأم ان لزم قبل الانتقال للخطوة التالية';
            //     }
            //     else{
            //         saveGuardian(data,step);
            //     }
            // }else{
            //     if($rootScope.father_id == null || $rootScope.father_id == "" ){
            //         $rootScope.msg='يجب عليك ادخال بيانات الأب و الام وتحديد الأم ان لزم قبل الانتقال للخطوة التالية';
            //     }
            //     else{
            //         saveGuardian(data,step);
            //     }
            //
            // }

            if(step == 'previous'){
                var r = confirm($filter('translate')('Do you want to return to the previous page without saving?'));
                if (r == true) {
                    $rootScope.Previous();
                }else {
                    saveGuardian(data,step);
                }
            }else{
                saveGuardian(data,step);
            }

        };
        $scope.add=function(size){
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'addBank.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size ,
                controller: function ($rootScope,$scope,$stateParams, $modalInstance,Entity) {

                    $scope.action='add';
                    $scope.error_frm=false;
                    $scope.msg3='';
                    $scope.msg4={};
                    $scope.detail={};
                    $scope.getBranches=function(id){
                        if( !angular.isUndefined($rootScope.msg1.bank_id)) {
                            $scope.msg4.bank_id = [];
                        }

                        Entity.listChildren({parent:'banks','id':id,'entity':'branches'},function (response) {
                            $scope.Branches = response;
                        });
                    };


                    Entity.list({entity:'banks'},function (response) {
                        $scope.Banks = response;
                    });


                    $scope.save = function (data) {
                        $rootScope.clearToastr();
                        data.id=-1;
                        $scope.error_frm=false;
                        $scope.msg3='';

                        var flag =-1;
                        angular.forEach($rootScope.guardian.banks, function(val,key) {
                            if(val.bank_id == data.bank_id){
                                flag = 1;
                                return;
                            }
                        });

                        if(flag == -1){
                            $rootScope.progressbar_start();
                            cases.checkAccount({'id':data.bank_id,'account_number':data.account_number},function(response){
                                $rootScope.progressbar_complete();
                                if(response.status == true){
                                    angular.forEach($scope.Banks, function(val,key) {
                                        if(val.id == data.bank_id){
                                            data.bank_name =val.name;
                                            return;
                                        }
                                    });
                                    angular.forEach($scope.Branches, function(val,key) {
                                        if(val.id == data.branch_name){
                                            data.branch =val.name;
                                            return;
                                        }
                                    });
                                    if(angular.isUndefined($rootScope.guardian.banks)) {
                                        $rootScope.guardian.banks=new Array();
                                        data.check=true;
                                    }else{
                                        if($rootScope.guardian.banks.length ==0 ){
                                            data.check=true;
                                        }else{
                                            data.check=false;
                                        }
                                    }

                                    $rootScope.guardian.banks.push(data);
                                    $rootScope.status1 ='failed_valid';
                                    $rootScope.msg1.banks=[];
                                    $modalInstance.close();
                                }else{
                                    $rootScope.toastrMessages('error',$filter ('translate') ('this account previously in use of other person'));
                                }
                            });

                        }else{
                            $rootScope.toastrMessages('error',$filter('translate')('the account has been entered for the person in the same bank. Please delete the account or modify it'));
                        }


                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }


            });
        };
        $scope.update=function(size,index,item){
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'addBank.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size ,
                controller: function ($rootScope,$scope,$stateParams, $modalInstance,Entity) {

                    $scope.error_frm=false;
                    $scope.msg3='';
                    $scope.msg4={};
                    $scope.action='edit';
                    $scope.getBranches=function(id){
                        if( !angular.isUndefined($scope.msg4.bank_id)) {
                            $scope.msg4.bank_id = [];
                        }
                        Entity.listChildren({parent:'banks',id:id,entity:'branches'},function (response) {
                            $scope.Branches = response;

                            if($scope.Branches.length ==0){
                                $scope.detail.branch_name = "";
                            }
                        });
                    };

                    Entity.list({entity:'banks'},function (response) {
                        $scope.Banks = response;
                    });

                    $scope.detail=item;
                    if( $scope.detail.bank_id == null) {
                        $scope.detail.bank_id  = "";
                        $scope.detail.branch_name = "";
                        $scope.detail.account_number = null;
                        $scope.detail.account_owner = null;
                    }else{
                        var branch_name =$scope.detail.branch_name;
                        $scope.getBranches($scope.detail.bank_id);
                        $scope.detail.bank_id = $scope.detail.bank_id +"";
                        if( $scope.detail.branch_name == null) {
                            $scope.detail.branch_name = "";
                        }else{
                            $scope.detail.branch_name = branch_name +"";
                        }
                    }

                    $scope.save = function (data) {
                        $rootScope.clearToastr();
                        $scope.error_frm=false;
                        $scope.msg3='';
                        data.id=-1;

                        $rootScope.progressbar_start();
                        cases.checkAccount({'id':data.bank_id,'account_number':data.account_number},function(response){
                            $rootScope.progressbar_complete();
                            if(response.status == true){
                                angular.forEach($scope.Banks, function(val,key) {
                                    if(val.id == data.bank_id){
                                        data.bank_name =val.name;
                                        return;
                                    }
                                });
                                angular.forEach($scope.Branches, function(val,key) {
                                    if(val.id == data.branch_name){
                                        data.branch =val.name;
                                        return;
                                    }
                                });
                                if(angular.isUndefined($rootScope.guardian.banks)) {
                                    $rootScope.guardian.banks=new Array();
                                    data.check=true;
                                }else{
                                    if($rootScope.guardian.banks.length ==0 ){
                                        data.check=true;
                                    }else{
                                        data.check=false;
                                    }
                                }

                                $rootScope.guardian.banks[index]=data;
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                $rootScope.no_banks='true';
                                $rootScope.status1 ='failed_valid';
                                $rootScope.msg1.banks=[];
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',$filter ('translate') ('this account previously in use of other person'));
                            }
                        });


                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }


            });
        };
        $scope.delete = function (index) {
            $rootScope.clearToastr();
            $rootScope.guardian.banks.splice(index,1);
            if($rootScope.guardian.banks.length==0){
                $rootScope.no_banks='false';
            }
            $rootScope.toastrMessages('success',$filter('translate')('The row is deleted from db'));

        };
        $scope.setOrgDefault  = function(status,index,item){
            $rootScope.clearToastr();
            var l=-1;
            var bk=-1;
            angular.forEach($rootScope.guardian.banks, function(val,key) {
                    if(val.check){
                        l=key;
                        bk=val;
                    }
            });
            if(bk != -1){
                if( l!=index ){
                    $rootScope.guardian.banks[l].check=false;
                    $rootScope.guardian.banks[index].check=true;
                    $rootScope.default_bank=item.bank_id;
                }
            }else{
                $rootScope.guardian.banks[index].check=true;
                $rootScope.default_bank=item.bank_id;
            }


        };
        $scope.get = function(target,value,parant){

            if(value != null && value != "" && value != " " ) {

                Entity.listChildren({entity:target,'id':value,'parent':parant},function (response) {

                    if(target =='districts'){
                        if( !angular.isUndefined($rootScope.msg1.country)) {
                            $rootScope.msg1.country = [];
                        }

                        $scope.governarate = response;
                        $scope.city = [];
                        $scope.nearlocation =[];
                        $scope.mosques = [];
                        $rootScope.guardian.governarate="";

                    }
                    else if(target =='cities'){
                        if( !angular.isUndefined($rootScope.msg1.governarate)) {
                            $rootScope.msg1.governarate = [];
                        }
                        $scope.city = response;
                        $scope.nearlocation =[];
                        $scope.mosques = [];
                        $rootScope.guardian.city="";
                        $rootScope.guardian.location_id="";
                        $rootScope.guardian.mosques_id="";

                    }
                    else if(target =='neighborhoods'){
                        if( !angular.isUndefined($rootScope.msg1.city)) {
                            $rootScope.msg1.city = [];
                        }

                        $scope.nearlocation = response;
                        $scope.mosques = [];
                        $rootScope.guardian.location_id="";
                        $rootScope.guardian.mosques_id="";

                    }
                    else if(target =='mosques'){
                        if( !angular.isUndefined($rootScope.msg1.location_id)) {
                            $rootScope.msg1.location_id = [];
                        }

                        $scope.mosques = response;
                        $rootScope.guardian.mosques_id="";
                    }

                });
            }


        };
        $scope.rest = function(via,value){
            if(via=='working') {
                if( !angular.isUndefined($rootScope.msg1.working)) {
                    $rootScope.msg1.working = [];
                }
                if( !angular.isUndefined($rootScope.msg1.work_job_id)) {
                    $rootScope.msg1.work_job_id = [];
                }
                if( !angular.isUndefined($rootScope.msg1.work_location)) {
                    $rootScope.msg1.work_location = [];
                }
                if( !angular.isUndefined($rootScope.msg1.monthly_income)) {
                    $rootScope.msg1.monthly_income = [];
                }
                $rootScope.guardian.work_job_id="";
                $rootScope.guardian.work_location="";
                $rootScope.guardian.monthly_income="";
            }
            else if( via =='study'){
                if( !angular.isUndefined($rootScope.msg1.study)) {
                    $rootScope.msg1.study = [];
                }

                if(value == 0){
                    if( !angular.isUndefined($rootScope.msg1.stage)) {
                        $rootScope.msg1.stage = [];
                    }
                    $rootScope.guardian.stage="";

                }
            }
            else if( via =='health_status'){
                if( !angular.isUndefined($rootScope.msg1.condition)) {
                    $rootScope.msg1.condition = [];
                }

                if (value != 2 || value != 3) {
                    if (!angular.isUndefined($rootScope.msg1.disease_id)) {
                        $rootScope.msg1.disease_id = [];
                    }
                    if (!angular.isUndefined($rootScope.msg1.details)) {
                        $rootScope.msg1.details = [];
                    }

                    $rootScope.guardian.disease_id = "";
                    $rootScope.guardian.details = "";
                }
            }

        };
        $scope.checkBirthday  = function(birthday){
            if( !angular.isUndefined($rootScope.msg1.birthday)) {
                $rootScope.msg1.birthday = [];
            }

            var curDate = new Date();

            if(new Date(birthday) > curDate){
                $rootScope.msg1.birthday = {0 :$filter('translate')('The specified date must be older than the current date')};
                $rootScope.status1 ='failed_valid';
            }

        };
// -----------------------------------------------------------------------------------------------------

        $rootScope.close=function(){

            $rootScope.failed =false;
            $scope.error =false;
            $rootScope.model_error_status =false;
        };

        $rootScope.dateOptions = {
            formatYear: 'yy',
            startingDay: 0
        };
        $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $rootScope.format = $rootScope.formats[0];
        $rootScope.today = function()
        {
            $rootScope.dt = new Date();
        };

        $rootScope.today();
        $rootScope.clear = function()
        {
            $rootScope.dt = null;
        };

        $rootScope.open1 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $rootScope.popup1.opened = true;
        };
        $rootScope.popup1 = {
            opened: false
        };
    });

