
angular.module('SponsorshipModule')
    .controller('CaseDocumentController', function ($scope,$rootScope,$state,$stateParams ,$uibModal,OAuthToken, category,FileUploader,cases) {


        $rootScope.failed =false;
        $rootScope.model_error_status =false;

        $rootScope.close=function(){
            $rootScope.failed =false;
            $rootScope.model_error_status =false;
        };

        $rootScope.documents=[];

        $rootScope.status ='';
        $rootScope.msg ='';
        $rootScope.msg2 ='';

        $rootScope.category_id=$stateParams.category_id;
        $rootScope.id     = $stateParams.case_id;

        $rootScope.ResetForm($rootScope.category_id, 'add','caseDocuments');
        $rootScope.TheCurrentStep= $rootScope.CaseDocumentStepNumber;

        var loadDocument=function(){
            $rootScope.clearToastr();
            cases.getCaseReports({'action':'filters','target':'info','case_id':$rootScope.id, 'mode':'edit', 'persons_documents' :true,  'category' :true},function(response) {
                $rootScope.documents =response.persons_documents;
            });

            };

        loadDocument();


        $scope.forward=function(){
            var next =$rootScope.FormSponsorshipSection[$rootScope.TheCurrentStep].id;
            angular.element('#'+next).removeClass("disabled");
        };

        $scope.ChooseAttachment=function(item,action){
            $rootScope.clearToastr();


            $uibModal.open({
                templateUrl: 'myModalContent2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,cases,OAuthToken, FileUploader) {

                    $scope.fileupload = function () {
                        if($scope.uploader.queue.length){
                            var image = $scope.uploader.queue.slice(1);
                            $scope.uploader.queue = image;
                        }
                    };
                    var uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/doc/files',
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });
                    uploader.onBeforeUploadItem = function(item) {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                    };
                    uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(response.status=='error')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else{
                            fileItem.id = response.id;
                            cases.setAttachment({id:item.person_id,type:'aids',action:action,document_type_id:item.document_type_id,document_id:fileItem.id},function (response2) {
                            if(response2.status=='error' || response2.status=='failed') {
                                $rootScope.toastrMessages('error',response2.msg);
                            }
                            else if(response2.status== 'success'){

                                $modalInstance.close();
                                $rootScope.toastrMessages('success',response2.msg);
                                loadDocument();

                            }

                        }, function(error) {
                            $rootScope.toastrMessages('error',error.data.msg);
                        });
                    }};

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };
        $scope.showImage=function(file){
            $rootScope.clearToastr();
            $rootScope.target=file;
            $uibModal.open({
                templateUrl: 'showImage.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance) {

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                },
                resolve: {

                }
            });
        };



    });