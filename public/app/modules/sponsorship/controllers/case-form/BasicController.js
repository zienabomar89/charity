
angular.module('SponsorshipModule')
    .controller('BasicController', function ($scope,$rootScope,$state,$stateParams,setting,category) {

        $rootScope.Step=null;
        Steps=[];
        setting.getCustomForm({'id':'sponsorship_custom_form'},function (response) {
            $rootScope.has_custom_form=response.status;
        });
        $rootScope.category_id=$state.params.category_id;
//-----------------------------------------------------------------------------------------------------
        $rootScope.ResetForm= function(category_id,mode,step) {
                $rootScope.ParentStep=false;
                $rootScope.CaseStep=false;
                $rootScope.GuardianStep=false;
                $rootScope.CustomFormStep=false;
                $rootScope.FamilyStep=false;
                $rootScope.CaseDocumentsStep=false;
                $rootScope.FamilyDocumentsStep=false;
                $rootScope.caseSponsorshipListStep=false;
                $rootScope.casePaymentListStep=false;
                $rootScope.father_dead=true;
                $rootScope.has_Wives=false;
                $rootScope.has_mother=false;
                $rootScope.classified_family=false;
                $rootScope.who_is_case=false;

            category.getCategoryFormsSections({'id':category_id ,'mode':mode,'type':'sponsorships'},function (response){
                        $rootScope.FormSponsorshipSection=response.FormSection;
                        if($rootScope.FormSponsorshipSection.length !=0){
                           
                            $rootScope.StepCount=$rootScope.FormSponsorshipSection.length;
                            angular.forEach($rootScope.FormSponsorshipSection, function(v, k) {
                                if(v.id == 'ParentData'){
                                    $rootScope.ParentStep=true;
                                    $rootScope.ParentStepNumber=k+1;
                                    Steps.push({'ParentData' : k+1});
                                }else if(v.id == 'CaseData'){
                                    $rootScope.CaseStep=true;
                                    $rootScope.CaseStepNumber=k+1;
                                    Steps.push({'CaseData' : k+1});
                                }else if(v.id == 'providerData'){
                                    $rootScope.GuardianStep=true;
                                    $rootScope.GuardianStepNumber=k+1;
                                    Steps.push({'providerData' : k+1});
                                }else if(v.id == 'FamilyData'){
                                    $rootScope.FamilyStep=true;
                                    $rootScope.FamilyStepNumber=k+1;
                                    Steps.push({'FamilyData' : k+1});
                                }else if(v.id == 'customFormData'){
                                    $rootScope.CustomFormStep=true;
                                    $rootScope.CustomFormDataStepNumber=k+1;
                                    Steps.push({'customFormData' : k+1});
                                }
                                else if(v.id == 'caseSponsorshipList'){
                                    $rootScope.caseSponsorshipListStep=true;
                                    $rootScope.caseSponsorshipListStepNumber=k+1;
                                }else if(v.id == 'casePaymentList'){
                                    $rootScope.casePaymentListStep=true;
                                    $rootScope.casePaymentListStepNumber=k+1;
                                }else if(v.id == 'caseDocuments'){
                                    $rootScope.CaseDocumentsStep=true;
                                    $rootScope.CaseDocumentStepNumber=k+1;
                                    Steps.push({'caseDocuments' : k+1});
                                }else if(v.id == 'familyDocuments'){
                                    $rootScope.FamilyDocumentsStep=true;
                                    $rootScope.FamilyDocumentStepNumber=k+1;
                                    Steps.push({'familyDocuments' : k+1});
                                }
                            });
                            $rootScope.getTheTarget(step);
                        }
                    }
                    , function(error) {
                    $rootScope.toastrMessages('error',error.data.msg);
                });

            category.get({'id':category_id,'type':'sponsorships'},function (response2){
                    if ($rootScope.lang == 'ar'){
                        $rootScope.CategoryName=response2.name;
                    }
                    else{
                        $rootScope.CategoryName=response2.en_name;
                    }

                    if(response2.father_dead ==0){
                        $rootScope.father_dead=false;
                    }else {
                        $rootScope.father_dead=true;
                    }

                    if(response2.has_Wives ==0){
                        $rootScope.has_Wives=false;
                    }else {
                        $rootScope.has_Wives=true;
                    }

                    if(response2.has_mother ==0){
                        $rootScope.has_mother=false;
                    }else {
                        $rootScope.has_mother=true;
                    }

                   if(response2.case_is ==2){
                        // 1 -person 2- guardian
                        $rootScope.who_is_case=false;
                    }else {
                        $rootScope.who_is_case=true;
                    }

                        if(response2.family_structure ==1){
                        // 1 not 2 classified
                        $rootScope.classified_family=false;
                    }else {
                        $rootScope.classified_family=true;
                    }
                    }
                    , function(error) {
                    $rootScope.toastrMessages('error',error.data.msg);
                });
        };
//-----------------------------------------------------------------------------------------------------
        $rootScope.person_id     = $stateParams.person_id;
        $rootScope.case_id     = $stateParams.case_id;
        $rootScope.guardian_id = $stateParams.guardian_id;
        $rootScope.father_id   = $stateParams.father_id;
        $rootScope.mother_id   = $stateParams.mother_id;
//-----------------------------------------------------------------------------------------------------
        State = function(name){
            $state.go('form.'+name, {
                guardian_id:$rootScope.guardian_id,
                person_id:$rootScope.person_id,
                case_id:$rootScope.case_id,
                father_id:$rootScope.father_id,
                mother_id: $rootScope.mother_id,
                category_id:$rootScope.category_id,
                Step:$rootScope.Step
            });

        };
        $rootScope.getTheTarget= function(target) {
            var $target_1='';
            if(target == 'ParentData'){
                angular.element('#ParentData').removeClass("disabled");
                $rootScope.TheCurrentStep=$rootScope.ParentStepNumber;
                $target_1='parent-info';
            }else if(target == 'CaseData'){
                angular.element('#CaseData').removeClass("disabled");
                $rootScope.TheCurrentStep=$rootScope.CaseStepNumber;
                $target_1='case-info';
            }else if(target == 'providerData'){
                angular.element('#providerData').removeClass("disabled");
                $rootScope.TheCurrentStep=$rootScope.GuardianStepNumber;
                $target_1='guardian-info';
            } else if(target == 'customFormData'){
                angular.element('#customFormData').removeClass("disabled");
                $rootScope.TheCurrentStep=$rootScope.CustomFormDataStepNumber;
                $target_1='custom-form-data';
            }if(target == 'FamilyData'){
                angular.element('#FamilyData').removeClass("disabled");
                $rootScope.TheCurrentStep=$rootScope.FamilyStepNumber;
                $target_1='family-info';
            }else if(target == 'caseDocuments'){
                angular.element('#caseDocuments').removeClass("disabled");
                $rootScope.TheCurrentStep= $rootScope.CaseDocumentStepNumber;
                $target_1='case-documents';
            }else if(target == 'familyDocuments'){
                angular.element('#familyDocuments').removeClass("disabled");
                $rootScope.TheCurrentStep= $rootScope.FamilyDocumentStepNumber;
                $target_1='family-documents';
            }
                State($target_1);
          };
        $rootScope.Next= function() {
            var next=null;
            next=$rootScope.TheCurrentStep;
            if($rootScope.StepCount !=$rootScope.TheCurrentStep ){
                var target =$rootScope.FormSponsorshipSection[next].id;
                $rootScope.getTheTarget(target);
            }else{
                $state.go('sponsorshipCases',{'status':'success',mode:'add'});
            }

        };
        $rootScope.Previous= function() {
            var pre=-null;
            pre=$rootScope.TheCurrentStep-2;
             var t =$rootScope.FormSponsorshipSection[pre].id;
            $rootScope.getTheTarget(t);
        };
        $rootScope.getState=function(name){
            State(name);
        }

    });