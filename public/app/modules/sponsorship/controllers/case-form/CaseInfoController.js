
angular.module('SponsorshipModule')
    .controller('CaseInfoController', function ($http,$uibModal,$scope,$rootScope,$state,$stateParams,Entity,category,$filter,cases,persons) {

            $rootScope.category_id=$stateParams.category_id;
            $rootScope.person_id     = $stateParams.person_id;
            $rootScope.case_id     = $stateParams.case_id;
            $rootScope.guardian_id = $stateParams.guardian_id;
            $rootScope.father_id   = $stateParams.father_id;
            $rootScope.mother_id   = $stateParams.mother_id;

            $scope.priority={};
            $rootScope.persons={banks:[]};
        /*         $scope.msg='';
                 $scope.success_frm=false;
                 $rootScope.success_msg="";
                 $rootScope.msg1={};
                 $rootScope.status1='';
                 $scope.model_error_status=false;
                 $rootScope.bankslist=[];
                 $rootScope.ResetForm($rootScope.category_id, 'add','CaseData');
                 $rootScope.TheCurrentStep=$rootScope.CaseStepNumber;

                 category.getPriority({'type':'sponsorships','category_id':$rootScope.category_id ,'target':'fm-case'},function (response) {
                     $scope.priority=response.priority;
                 });

                 Entity.get({'entity':'entities' , 'c':'maritalStatus,educationStages,educationAuthorities,countries,diseases,kinship,propertyTypes,roofMaterials,furnitureStatus,houseStatus'},function (response){
                     $rootScope.MaritalStatus = response.maritalStatus;
                     $rootScope.EduStages = response.educationStages;
                     $scope.EduAuthorities = response.educationAuthorities;
                     $rootScope.Diseases = response.diseases;
                     $scope.Kinship = response.kinship;
                     $rootScope.Country = response.countries;
                     $scope.houseStatus = response.houseStatus;
                     $scope.furnitureStatus = response.furnitureStatus;
                     $scope.PropertyTypes = response.propertyTypes;
                     $scope.RoofMaterials = response.roofMaterials;
                 });

                 var  setCase =function (data){
                     $rootScope.persons=data;
                     $rootScope.case_id= $rootScope.persons.case_id;
                     $rootScope.person_id=$rootScope.persons.person_id;

                     if($rootScope.persons.monthly_income==null){
                         $rootScope.persons.monthly_income = "";
                     }
                     if($rootScope.persons.death_cause_id !=null){
                         $rootScope.persons.death_cause_id = $rootScope.persons.death_cause_id+"";
                     }

                     if($rootScope.persons.death_date =="0000-00-00" || $rootScope.persons.death_date == null){
                         $rootScope.persons.death_date    = new Date();
                     }else{
                         $rootScope.persons.death_date    = new Date($rootScope.persons.death_date);
                     }

                     if($rootScope.persons.birthday =="0000-00-00" || $rootScope.persons.birthday == null){
                         $rootScope.persons.birthday    = new Date();
                     }else{
                         $rootScope.persons.birthday    = new Date($rootScope.persons.birthday);
                     }
                     if($rootScope.persons.country !=null){
                         $rootScope.persons.country = $rootScope.persons.country+"";
                         Entity.listChildren({entity:'districts','id':$rootScope.persons.country,'parent':'countries'},function (response) {
                             $scope.governarate = response;
                         });
                     }else{
                         $rootScope.persons.country ="";

                     }
                     if($rootScope.persons.governarate !=null){
                         $rootScope.persons.governarate = $rootScope.persons.governarate+"";

                         Entity.listChildren({parent:'districts','id':$rootScope.persons.governarate,entity:'cities'},function (response) {
                             $scope.city = response;
                         });
                     }else{
                         $rootScope.persons.governarate ="";

                     }
                     if($rootScope.persons.city !=null){
                         $rootScope.persons.city = $rootScope.persons.city+"";
                         Entity.listChildren({entity:'neighborhoods','id':$rootScope.persons.city,'parent':'cities'},function (response) {
                             $scope.nearlocation = response;
                         });
                     }else{
                         $rootScope.persons.city ="";
                     }
                     if($rootScope.persons.location_id !=null){
                         $rootScope.persons.location_id = $rootScope.persons.location_id+"";
                         Entity.listChildren({entity:'mosques','id':$rootScope.persons.location_id,'parent':'neighborhoods'},function (response) {
                             $scope.mosques = response;
                         });
                     }else{
                         $rootScope.persons.location_id ="";
                     }

                     if($rootScope.persons.mosques_id !=null){
                         $rootScope.persons.mosques_id = $rootScope.persons.mosques_id+"";
                     }else{
                         $rootScope.persons.mosques_id ="";
                     }

                     if($rootScope.persons.gender ==0){
                         $rootScope.persons.gender="";
                     }else{
                         $rootScope.persons.gender=parseInt($rootScope.persons.gender) +"";
                     }
                     if($rootScope.persons.nationality==null){
                         $rootScope.persons.nationality="";
                     } else{
                         $rootScope.persons.nationality=parseInt($rootScope.persons.nationality) +"";
                     }
                     if($rootScope.persons.birth_place==null){
                         $rootScope.persons.birth_place="";
                     } else{
                         $rootScope.persons.birth_place=$rootScope.persons.birth_place +"";
                     }
                     if($rootScope.persons.type==null){
                         $rootScope.persons.type="";
                     } else{
                         $rootScope.persons.type=$rootScope.persons.type+"";
                     }
                     if($rootScope.persons.authority==null){
                         $rootScope.persons.authority="";
                     } else{
                         $rootScope.persons.authority=$rootScope.persons.authority+"";
                     }

                     if($rootScope.persons.grade==null){
                         $rootScope.persons.grade="";
                     } else{
                         $rootScope.persons.grade=$rootScope.persons.grade+"";
                     }

                     if($rootScope.persons.stage==null){
                         $rootScope.persons.stage="";
                     }
                     else{
                         $rootScope.persons.stage=$rootScope.persons.stage+"";
                     }


                     if($rootScope.persons.level==null){
                         $rootScope.persons.level="";
                     }
                     else{
                         $rootScope.persons.level=$rootScope.persons.level+"";
                     }



                     if($rootScope.persons.condition == null ){
                         $rootScope.persons.health_condition="";
                         $rootScope.persons.disease_id="";
                         $rootScope.persons.details="";
                     }else{
                         $rootScope.persons.health_condition=$rootScope.persons.condition+"";
                         if($rootScope.persons.health_condition == 2 || $rootScope.persons.health_condition == 3 ||
                             $rootScope.persons.health_condition === '2' || $rootScope.persons.health_condition === '3' ){

                             if($rootScope.persons.disease_id==null){
                                 $rootScope.persons.disease_id="";
                             } else{
                                 $rootScope.persons.disease_id=$rootScope.persons.disease_id+"";
                             }
                         }else{
                             $rootScope.persons.disease_id="";
                             $rootScope.persons.details="";

                         }
                     }

                     if($rootScope.persons.property_type_id ==null){
                         $rootScope.persons.property_type_id="";
                     }
                     else{
                         $rootScope.persons.property_type_id =$rootScope.persons.property_type_id +"";
                     }
                     if($rootScope.persons.roof_material_id ==null){
                         $rootScope.persons.roof_material_id="";
                     }
                     else{
                         $rootScope.persons.roof_material_id =$rootScope.persons.roof_material_id +"";
                     }

                     if($rootScope.persons.residence_condition ==null){
                         $rootScope.persons.residence_condition="";
                     }
                     else{
                         $rootScope.persons.residence_condition =$rootScope.persons.residence_condition +"";
                     }

                     if($rootScope.persons.indoor_condition ==null){
                         $rootScope.persons.indoor_condition="";
                     }
                     else{
                         $rootScope.persons.indoor_condition =$rootScope.persons.indoor_condition +"";
                     }



                     if($rootScope.persons.prayer==null){
                         $rootScope.persons.prayer="";
                     }
                     else{
                         $rootScope.persons.prayer =$rootScope.persons.prayer +"";
                     }

                     if($rootScope.persons.quran_center==null){
                         $rootScope.persons.quran_center="";
                         $rootScope.persons.quran_reason="";
                     }
                     else{

                         if($rootScope.persons.quran_center !=0){
                             $rootScope.persons.quran_reason="";
                         }
                         $rootScope.persons.quran_center =$rootScope.persons.quran_center +"";

                     }

                     if($rootScope.persons.quran_chapters==0){
                         $rootScope.persons.save_quran=1+"";
                     }
                     else if($rootScope.persons.quran_chapters==null){
                         $rootScope.persons.save_quran="";
                     }
                     else {
                         $rootScope.persons.save_quran=0+"";

                     }

                     if($rootScope.persons.quran_parts==null){
                         $rootScope.persons.save_quran =1+"";
                     }
                     else{
                         $rootScope.persons.save_quran =0+"";
                     }

                 }
                if( !angular.isUndefined($rootScope.case_id)) {
                     if($rootScope.case_id != '' && $rootScope.case_id !=  null&& $rootScope.case_id !=  'null'){
                         cases.getCaseReports({'target':'info','action':'filters','mode':'edit','case_id':$rootScope.case_id ,
                             'person' :true, 'persons_i18n' : true, 'contacts' : true,  'education' : true, 'work' : true, 'residence' : true,
                             'banks' : true ,'health' : true,'islamic' : true,'category_type':1},function(response) {
                             setCase(response);
                         });


                     }
                 }else{
                    $rootScope.persons = {};
                    $rootScope.persons.banks =[]

                }

                 $scope.forward=function(data){

                     $rootScope.clearToastr();
                     var next =$rootScope.FormSponsorshipSection[$rootScope.TheCurrentStep].id;
                           angular.element('#'+next).removeClass("disabled");

                         if( !angular.isUndefined(data.birthday)) {
                             data.birthday= $filter('date')(data.birthday, 'dd-MM-yyyy')
                         }


                     if( !angular.isUndefined(data.health_condition)) {
                         data.condition=data.health_condition
                     }


                         if( !angular.isUndefined(data.birthday)) {
                             data.birthday= $filter('date')(data.birthday, 'dd-MM-yyyy')
                         }

                         data.priority=$scope.priority;
                         data.case_id=$rootScope.case_id;
                         data.category_id=$rootScope.category_id;
                         data.target='case';
                         data.no_kinship=true;

                         if( !angular.isUndefined(data.priority.type)   || !angular.isUndefined(data.priority.authority) ||
                             !angular.isUndefined(data.priority.stage)  || !angular.isUndefined(data.priority.grade) ||
                             !angular.isUndefined(data.priority.year)   || !angular.isUndefined(data.priority.school)||
                             !angular.isUndefined(data.priority.points) || !angular.isUndefined(data.priority.level)
                         ) {
                             data.study = 1
                         }

                        $http({
                         method: 'POST',
                         url: '/api/v1.0/common/sponsorships/cases',
                         data:data
                     }).then(function successCallback(response) {

                         if(response.data.status=='failed_valid') {
                             $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                             $scope.status1 =response.data.status;
                             $rootScope.msg1 =response.data.msg;
                         }
                         else if(response.data.status=='failed')
                         {
                             $rootScope.toastrMessages('error',response.data.msg);
                         }
                         else if(response.data.status== 'success'){
                             $rootScope.case_id=response.data.case_id ;
                             $rootScope.person_id=response.data.person_id ;
                             $rootScope.Next();
                         }
                     });
                     };
                 $scope.isPersonExists = function(id_card_number){
                     $rootScope.clearToastr();
                     if( !angular.isUndefined($rootScope.msg1.id_card_number)) {
                         $rootScope.msg1.id_card_number = [];
                     }
                         if(!angular.isUndefined(id_card_number)){
                             if(id_card_number.length==9 ) {
                                 if($rootScope.check_id(id_card_number)){
                                     var params={ 'mode':'edit' ,'id_card_number' :id_card_number, 'category_id' : $rootScope.category_id,
                                     'person' :true, 'persons_i18n' : true, 'contacts' : true,  'education' : true, 'islamic' : true, 'health' : true,
                                     'work' : true, 'residence' : true, 'banks' : true
                                 };

                                 persons.find(params,function (response) {
                                         if(response.status ==true ) {
                                             $rootScope.toastrMessages('error',response.msg);
                                             setCase(response.person);
                                         }
                                         else{
                                             if( !angular.isUndefined(response.blocked)) {
                                                 if(response.blocked){
                                                     $rootScope.toastrMessages('error',response.msg);
                                                     $rootScope.persons = [];
                                                 }else{
                                                     $scope.model_error_status=false;
                                                     $scope.msg='';
                                                     $rootScope.persons.id=""
                                                 }
                                             }else{
                                                 $scope.model_error_status=false;
                                                 $scope.msg='';
                                                 $rootScope.persons.id=""
                                             }

                                             if(response.msg){
                                                 $rootScope.toastrMessages('error',response.msg);
                                                 $scope.persons={};
                                             }else{
                                                 $scope.model_error=false;
                                                 $scope.msg32='';
                                             }

                                             if(response.person){
                                                 $rootScope.persons    = response.person;

                                                 if($rootScope.persons.birthday =="0000-00-00" || $rootScope.persons.birthday == null){
                                                     $rootScope.persons.birthday    = new Date();
                                                 }else{
                                                     $rootScope.persons.birthday    = new Date($rootScope.persons.birthday);
                                                 }

                                                 if($rootScope.persons.death_date =="0000-00-00" || $rootScope.persons.death_date == null){
                                                     $rootScope.persons.death_date    = new Date();
                                                 }else{
                                                     $rootScope.persons.death_date    = new Date($rootScope.persons.death_date);
                                                 }
                                                 $rootScope.persons.id=""
                                             }
                                     }
                                 });
                             }else{
                                 $rootScope.toastrMessages('error',$filter('translate')('invalid card number'));
                             }
                             }
                         }
                 };
                 $scope.add=function(size){
                     $uibModal.open({
                         templateUrl: 'addBank.html',
                         backdrop  : 'static',
                         keyboard  : false,
                         windowClass: 'modal',
                         size: size ,
                         controller: function ($rootScope,$scope,$stateParams, $modalInstance,Entity) {

                             $scope.action='add';
                             $scope.error_frm=false;
                             $scope.msg3='';
                             $scope.msg4={};
                             $scope.detail={};
                             $scope.getBranches=function(id){
                                 if( !angular.isUndefined($rootScope.msg1.bank_id)) {
                                     $scope.msg4.bank_id = [];
                                 }
                                 $scope.detail.branch_name="";
                                 Entity.listChildren({parent:'banks','id':id,'entity':'branches'},function (response) {
                                     $scope.Branches = response;
                                 });
                             };


                             Entity.list({entity:'banks'},function (response) {
                                 $scope.Banks = response;
                             });


                             $scope.save = function (data) {
                                 data.id=-1;
                                 $scope.error_frm=false;
                                 $scope.msg3='';

                                 var flag =-1;
                                 angular.forEach($rootScope.persons.banks, function(val,key) {
                                     if(val.bank_id == data.bank_id){
                                         flag = 1;
                                         return;
                                     }
                                 });

                                 if(flag == -1){
                                     cases.checkAccount({'id':data.bank_id,'account_number':data.account_number},function(response){

                                         if(response.status == true){
                                             angular.forEach($scope.Banks, function(val,key) {
                                                 if(val.id == data.bank_id){
                                                     data.bank_name =val.name;
                                                     return;
                                                 }
                                             });
                                             angular.forEach($scope.Branches, function(val,key) {
                                                 if(val.id == data.branch_name){
                                                     data.branch =val.name;
                                                     return;
                                                 }
                                             });
                                             if($rootScope.persons.banks.length ==0 ){
                                                 data.check=true;
                                             }else{
                                                 data.check=false;
                                             }
                                             // $rootScope.persons.banks[index]=data;
                                             $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                             $rootScope.persons.banks.push(data);
                                             $rootScope.no_banks='true';
                                             $rootScope.status1 ='failed_valid';
                                             $rootScope.msg1.banks=[];
                                             $modalInstance.close();
                                         }
                                         else{


                                             if($rootScope.person_id == null){
                                                 $rootScope.toastrMessages('error',$filter ('translate') ('this account previously in use of other person'));
                                             }
                                             else{

                                                 if($rootScope.person_id == response.person_id){
                                                     angular.forEach($scope.Banks, function(val,key) {
                                                         if(val.id == data.bank_id){
                                                             data.bank_name =val.name;
                                                             return;
                                                         }
                                                     });
                                                     angular.forEach($scope.Branches, function(val,key) {
                                                         if(val.id == data.branch_name){
                                                             data.branch =val.name;
                                                             return;
                                                         }
                                                     });
                                                     if($rootScope.persons.banks.length ==0 ){
                                                         data.check=true;
                                                     }else{
                                                         data.check=false;
                                                     }
                                                     $rootScope.persons.banks.push(data);

                                                     $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                                     $rootScope.status1 ='failed_valid';
                                                     $rootScope.msg1.banks=[];
                                                     $modalInstance.close();

                                                 }else{
                                                     $rootScope.toastrMessages('error',$filter ('translate') ('this account previously in use of other person'));
                                                 }
                                             }

                                         }
                                     });

                                 }else{
                                     $rootScope.toastrMessages('error',$filter('translate')('the account has been entered for the person in the same bank. Please delete the account or modify it'));
                                 }


                             };
                             $scope.cancel = function () {
                                 $modalInstance.dismiss('cancel');
                             };
                         }


                     });
                 };
                 $scope.update=function(size,index,item){
                     $uibModal.open({
                         templateUrl: 'addBank.html',
                         backdrop  : 'static',
                         keyboard  : false,
                         windowClass: 'modal',
                         size: size ,
                         controller: function ($rootScope,$scope,$stateParams, $modalInstance,Entity) {

                             $scope.error_frm=false;
                             $scope.msg3='';
                             $scope.msg4={};
                             $scope.action='edit';
                             $scope.getBranches=function(id){
                                 if( !angular.isUndefined($scope.msg4.bank_id)) {
                                     $scope.msg4.bank_id = [];
                                 }
                                 $scope.detail.branch_name = "";
                                 Entity.listChildren({parent:'banks',id:id,entity:'branches'},function (response) {
                                     $scope.Branches = response;

                                     if($scope.Branches.length ==0){
                                     }
                                 });
                             };

                             Entity.list({entity:'banks'},function (response) {
                                 $scope.Banks = response;
                             });

                             $scope.detail=item;
                             if( $scope.detail.bank_id == null) {
                                 $scope.detail.bank_id  = "";
                                 $scope.detail.branch_name = "";
                                 $scope.detail.account_number = null;
                                 $scope.detail.account_owner = null;
                             }else{
                                 var branch_name =$scope.detail.branch_name;
                                 $scope.getBranches($scope.detail.bank_id);
                                 $scope.detail.bank_id = $scope.detail.bank_id +"";
                                 if( $scope.detail.branch_name == null) {
                                     $scope.detail.branch_name = "";
                                 }else{
                                     $scope.detail.branch_name = branch_name +"";
                                 }

                             }

                             $scope.save = function (data) {
                                 $scope.error_frm=false;
                                 $scope.msg3='';
                                 data.id=-1;

                                 cases.checkAccount({'id':data.bank_id,'account_number':data.account_number},function(response){
                                     if(response.status == true){
                                         angular.forEach($scope.Banks, function(val,key) {
                                             if(val.id == data.bank_id){
                                                 data.bank_name =val.name;
                                                 return;
                                             }
                                         });
                                         angular.forEach($scope.Branches, function(val,key) {
                                             if(val.id == data.branch_name){
                                                 data.branch =val.name;
                                                 return;
                                             }
                                         });
                                         if($rootScope.persons.banks.length ==0 ){
                                             data.check=true;
                                         }else{
                                             data.check=false;
                                         }
                                         $rootScope.persons.banks[index]=data;
                                         $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                         $rootScope.no_banks='true';
                                         $rootScope.status1 ='failed_valid';
                                         $rootScope.msg1.banks=[];
                                         $modalInstance.close();
                                     }
                                     else{


                                         if($rootScope.person_id == null){
                                             $rootScope.toastrMessages('error',$filter ('translate') ('this account previously in use of other person'));
                                         }
                                         else{

                                             if($rootScope.person_id == response.person_id){
                                                 angular.forEach($scope.Banks, function(val,key) {
                                                     if(val.id == data.bank_id){
                                                         data.bank_name =val.name;
                                                         return;
                                                     }
                                                 });
                                                 angular.forEach($scope.Branches, function(val,key) {
                                                     if(val.id == data.branch_name){
                                                         data.branch =val.name;
                                                         return;
                                                     }
                                                 });
                                                 if($rootScope.persons.banks.length ==0 ){
                                                     data.check=true;
                                                 }else{
                                                     data.check=false;
                                                 }
                                                 $rootScope.persons.banks[index]=data;
                                                 $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                                 $rootScope.status1 ='failed_valid';
                                                 $rootScope.msg1.banks=[];
                                                 $modalInstance.close();

                                             }else{
                                                 $rootScope.toastrMessages('error',$filter ('translate') ('this account previously in use of other person'));
                                             }
                                         }

                                     }
                                 });


                             };
                             $scope.cancel = function () {
                                 $modalInstance.dismiss('cancel');
                             };
                         }


                     });
                 };
                 $scope.delete = function (index) {
                     $rootScope.clearToastr();
                     $rootScope.persons.banks.splice(index,1);
                 if($rootScope.persons.banks.length==0){
                     $rootScope.no_banks='false';
                 }
                $rootScope.toastrMessages('success',$filter('translate')('The row is deleted from db'));

             };
                 $scope.setOrgDefault  = function(status,index,item){
                         var l=-1;
                         var bk=-1;
                         angular.forEach($rootScope.persons.banks, function(val,key) {
                             if(val.check){
                                 l=key;
                                 bk=val;
                             }
                         });
                         if(bk != -1){
                             if( l!=index ){
                                 $rootScope.persons.banks[l].check=false;
                                 $rootScope.persons.banks[index].check=true;
                                 $rootScope.default_bank=item.bank_id;
                             }
                         }else{
                             $rootScope.persons.banks[index].check=true;
                             $rootScope.default_bank=item.bank_id;
                         }


                     };
                 $scope.get = function(target,value,parant){

                     if(value != null && value != "" && value != " " ) {

                         Entity.listChildren({entity:target,'id':value,'parent':parant},function (response) {

                             if(target =='districts'){
                                 if( !angular.isUndefined($rootScope.msg1.country)) {
                                     $rootScope.msg1.country = [];
                                 }

                                 $scope.governarate = response;
                                 $scope.city = [];
                                 $scope.mosques = [];
                                 $scope.nearlocation =[];
                                 $rootScope.persons.governarate="";
                                 $rootScope.persons.city="";
                                 $rootScope.persons.location_id="";
                                 $rootScope.persons.mosques_id="";
                             }
                             else if(target =='cities'){
                                  if( !angular.isUndefined($rootScope.msg1.governarate)) {
                                     $rootScope.msg1.governarate = [];
                                  }
                                 $scope.city = response;
                                 $scope.mosques = [];
                                 $scope.nearlocation =[];
                                 $rootScope.persons.city="";
                                 $rootScope.persons.location_id="";
                                 $rootScope.persons.mosques_id="";

                             }
                             else if(target =='neighborhoods'){
                                 if( !angular.isUndefined($rootScope.msg1.city)) {
                                     $rootScope.msg1.city = [];
                                 }

                                 $scope.nearlocation = response;
                                 $scope.mosques = [];
                                 $rootScope.persons.location_id="";
                                 $rootScope.persons.mosques_id="";

                             }
                             else if(target =='mosques'){
                                  if( !angular.isUndefined($rootScope.msg1.location_id)) {
                                     $rootScope.msg1.location_id = [];
                                  }

                                 $scope.mosques = response;
                                 $rootScope.persons.mosques_id="";
                             }

                         });
                     }


                 };
                 $scope.restValue = function(i,value){


                     if(i==1) {
                         if (!angular.isUndefined($rootScope.msg1.condition)) {
                             $rootScope.msg1.condition = [];
                         }

                         if(value ==1){
                             if( !angular.isUndefined($rootScope.msg1.disease_id)) {
                                 $rootScope.msg1.disease_id = [];
                             }
                             if( !angular.isUndefined($rootScope.msg1.details)) {
                                 $rootScope.msg1.details = [];
                             }

                             $rootScope.persons.disease_id="";
                             $rootScope.persons.details="";

                         }
                     }
                     else if(i==2) {

                         if (!angular.isUndefined($rootScope.msg1.prayer)) {
                             $rootScope.msg1.prayer = [];
                         }

                         if (value != 1) {
                             if (!angular.isUndefined($rootScope.msg1.prayer_reason)) {
                                 $rootScope.msg1.prayer_reason = [];
                             }
                             $rootScope.persons.prayer_reason = "";
                         }

                     }
                     else if(i==3) {

                         if (!angular.isUndefined($rootScope.msg1.save_quran)) {
                             $rootScope.msg1.save_quran = [];
                         }

                         if (value == 1){
                             if (!angular.isUndefined($rootScope.msg1.quran_parts)) {
                                 $rootScope.msg1.quran_parts = [];
                             }
                             if (!angular.isUndefined($rootScope.msg1.quran_chapters )) {
                                 $rootScope.msg1.quran_chapters = [];
                             }

                             $rootScope.persons.quran_parts = "";
                             $rootScope.persons.quran_chapters  = "";

                         }

                     }
                     else if(i==4) {

                         if (!angular.isUndefined($rootScope.msg1.quran_center)) {
                             $rootScope.msg1.quran_center = [];
                         }

                         if (value == 1) {
                             if (!angular.isUndefined($rootScope.msg1.quran_reason)) {
                                 $rootScope.msg1.quran_reason = [];
                             }
                             $rootScope.persons.quran_reason = "";
                         }

                     }

                 };
                 $scope.checkBirthday  = function(birthday){
                     if( !angular.isUndefined($rootScope.msg1.birthday)) {
                         $rootScope.msg1.birthday = [];
                     }

                     var curDate = new Date();

                     if(new Date(birthday) > curDate){
                         $rootScope.msg1.birthday = {0 :$filter('translate')('the enter date must be less than current date')};
                         $rootScope.status1 ='failed_valid';
                     }

                 };
                 $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0};
                 $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                 $rootScope.format = $rootScope.formats[0];
                 $rootScope.today = function() {$rootScope.dt = new Date();};
                 $rootScope.today();
                 $rootScope.clear = function() {$rootScope.dt = null;};

                 $rootScope.open1 = function($event) {
                     $event.preventDefault();
                     $event.stopPropagation();
                     $rootScope.popup1.opened = true;
                 };
                 $rootScope.popup1 = {opened: false};
         */
        $rootScope.persons = {};
        $rootScope.persons.banks =[]

    });