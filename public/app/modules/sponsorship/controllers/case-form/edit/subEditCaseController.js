angular.module('SponsorshipModule')
   .controller('subEditCaseController', function ($http,$filter,$scope,$rootScope,$state,$stateParams,$timeout,$uibModal,persons,cases,Entity,category) {

       $rootScope.settings.layout.pageSidebarClosed = true;

        $rootScope.target=$scope.target=$stateParams.target;
        $rootScope.action=$scope.action=$stateParams.action;
        $rootScope.id=$scope.id=$stateParams.id;
        $scope.success =false;
        $scope.failed =false;
        $scope.error =false;
       $rootScope.has_error=false;

       $rootScope.priority={};
        $rootScope.person={'banks':[]};

       $rootScope.msg='';
       $rootScope.msg1={};
        $rootScope.status1='';

        $rootScope.error=false;
        $rootScope.success=false;
        $scope.category_name = '';
        $scope.name = '';

        $scope.category_id = $rootScope.category_id = $scope.guardian_id =$scope.father_id = $scope.mother_id = $rootScope.person_id = null;

        $scope.close=function(){
            $scope.success =false;
            $scope.failed =false;
            $scope.error =false;
            $scope.model_error_status =false;
            $scope.model_error_status2 =false;
        };
        

        if($scope.id != null){
            var entities='educationDegrees,educationStages,workJobs,diseases,maritalStatus,deathCauses,countries';
            if($rootScope.target =='guardian'){
                entities=entities+',propertyTypes,kinship,roofMaterials,furnitureStatus,houseStatus';
            }

            Entity.get({'entity':'entities' , 'c':entities},function (response){
                $rootScope.EduDegrees = response.educationDegrees;
                $rootScope.EduStages = response.educationStages;
                $rootScope.WorkJob = response.workJobs;
                $rootScope.Diseases = response.diseases;
                $rootScope.MaritalStatus = response.maritalStatus;
                $rootScope.DeathCauses = response.deathCauses;
                $rootScope.Country = response.countries;
                if($rootScope.target =='guardian') {
                    $scope.Kinship = response.kinship;
                    $scope.PropertyTypes = response.propertyTypes;
                    $scope.RoofMaterials = response.roofMaterials;
                    $scope.houseStatus = response.houseStatus;
                    $scope.furnitureStatus = response.furnitureStatus;
                }

            });


            cases.getCaseReports({'action':'filters','target':'info','case_id':$scope.id, 'mode':'show', 'category_type' : 1, 'gender' :true,'full_name' :true, 'mother_id' :true, 'father_id' :true, 'guardian_id' :true},function(response) {
                $rootScope.category_id = response.category_id;
                   $scope.category_id = response.category_id;
                   $scope.category_name = response.category_name;
                   $rootScope.person_id = response.person_id;
                   $rootScope.mother_id = response.mother_id;
                   $rootScope.father_id = response.father_id;
                   $rootScope.guardian_id = response.guardian_id;
                   $rootScope.organization_id = response.organization_id;
                   $rootScope.user_organization_id = response.user_organization_id;
                   $rootScope.gender = response.gender;
                   $scope.full_name = response.full_name;

                var priority={};
                var mother_sub_str= ["mother_condition","mother_death_cause","mother_death_date","mother_details",
                    "mother_disease","mother_marital_status","mother_nationality","mother_prev_family_name","mother_stage"];
                var father_sub_str= ["father_condition"];
                var guardian_sub_str= ["guardian_address","guardian_banks","guardian_indoor_condition","guardian_kinship","guardian_marital_status","guardian_phone","guardian_primary_mobile","guardian_property_type","guardian_residence_condition","guardian_roof_material","guardian_rooms","guardian_secondary_mobile","guardian_stage"];

                if($rootScope.target=='mother'){
                    $rootScope.mother={};
                    category.getPriority({'type':'sponsorships','category_id': $scope.category_id ,'target':'mother'},function (response) {
                        priority = response.priority;
                        for (var key in priority) {
                            if(mother_sub_str.indexOf(key) !== -1) {
                                var value =priority[key];
                                delete  priority[key];
                                priority[key.substr(7)]=value;
                            }
                        }
                        $rootScope.priority=priority;
                    });

                }
                else if($rootScope.target=='father'){
                    $rootScope.father={};
                    category.getPriority({'type':'sponsorships','category_id': $scope.category_id ,'target':'father'},function (response) {
                        priority = response.priority;
                        for (var key in priority) {
                            var temp=[];
                            if(father_sub_str.indexOf(key) !== -1) {
                                // if(key ==  'father_condition') {
                                var value =priority[key];
                                delete  priority[key];
                                priority[key.substr(7)]=value;
                            }
                        }
                        priority.alive=4;
                        priority.death_date=4;
                        priority.death_cause=4;
                        $rootScope.priority=priority;
                    });
                }
                else if($rootScope.target=='guardian'){
                    $rootScope.father={};
                    category.getPriority({'type':'sponsorships','category_id': $scope.category_id ,'target':'guardian'},function (response) {
                        priority = response.priority;
                        for (var key in priority) {
                            var temp=[];
                            if(guardian_sub_str.indexOf(key) !== -1) {
                                // if(key ==  'father_condition') {
                                var value =priority[key];
                                delete  priority[key];
                                priority[key.substr(9)]=value;
                            }
                        }
                        $rootScope.priority=priority;
                    });
                }
                if($rootScope.action =='edit'){
                    var params={'mode':'edit','target':'info','person' :true, 'persons_i18n' : true, 'education' : true,'work' : true};

                    if($rootScope.target == 'father' || $rootScope.target =='mother'){
                        params.health=true;

                        if($rootScope.target == 'mother'){ params.person_id=$rootScope.mother_id;}
                        if($rootScope.target =='father'){ params.person_id=$rootScope.father_id;}

                    }else{
                        params.person_id=$rootScope.guardian_id;
                        params.banks=true;
                        params.residence=true;
                        params.contacts=true;
                        params.individual_id=$rootScope.person_id;
                    }
                    persons.getPersonReports(params, function (response) {
                        setItem(response,null);
                    });
                }
            });
         }


       var setItem= function (item,to) {
           $rootScope.person=item;
           if($rootScope.person.marital_status_id==null){
               $rootScope.person.marital_status_id="";
           } else{
               $rootScope.person.marital_status_id = $rootScope.person.marital_status_id+"";
           }
           if($rootScope.person.study == null){
               $rootScope.person.study = "";
               $rootScope.person.degree="";
               $rootScope.person.specialization="";
               $rootScope.person.stage="";
           } else{
               $rootScope.person.study = $rootScope.person.study+"";

               if($rootScope.person.study == 0){
                   $rootScope.person.degree="";
                   $rootScope.person.specialization="";
               }else{
                   if($rootScope.person.degree==null){
                       $rootScope.person.degree="";
                   }
                   else{
                       $rootScope.person.degree = $rootScope.person.degree+"";
                   }

                   if($rootScope.person.stage==null){
                       $rootScope.person.stage="";
                   }
                   else{
                       $rootScope.person.stage = $rootScope.person.stage+"";
                   }
               }
           }
           if($rootScope.person.working == null ){
               $rootScope.person.working="";
               $rootScope.person.work_job_id="";
               $rootScope.person.work_location="";
               $rootScope.person.monthly_income="";
           }else{
               $rootScope.person.working=$rootScope.person.working+"";
               if($rootScope.person.working == 1){
                   $rootScope.person.work_job_id = $rootScope.person.work_job_id+"";
               }else{
                   $rootScope.person.work_location="";
               }
           }
           if($rootScope.target == 'guardian') {
               if($rootScope.person.country !=null){
                   $rootScope.person.country = $rootScope.person.country+"";
                   Entity.listChildren({entity:'districts','id':$rootScope.person.country,'parent':'countries'},function (response) {
                       $scope.governarate = response;
                   });
               }else{
                   $rootScope.person.country ="";
               }
               if($rootScope.person.governarate !=null){
                   $rootScope.person.governarate = $rootScope.person.governarate+"";

                   Entity.listChildren({parent:'districts','id':$rootScope.person.governarate,entity:'cities'},function (response) {
                       $scope.city = response;
                   });
               }else{
                   $rootScope.person.governarate ="";
               }
               if($rootScope.person.city !=null){
                   $rootScope.person.city = $rootScope.person.city+"";
                   Entity.listChildren({entity:'neighborhoods','id':$rootScope.person.city,'parent':'cities'},function (response) {
                       $scope.nearlocation = response;
                   });
               }else{
                   $rootScope.person.city ="";
               }
               if($rootScope.person.location_id !=null){
                   $rootScope.person.location_id = $rootScope.person.location_id+"";
                   Entity.listChildren({entity:'mosques','id':$rootScope.person.location_id,'parent':'neighborhoods'},function (response) {
                       $scope.mosques = response;
                   });
               }else{
                   $rootScope.person.location_id ="";
               }
               if($rootScope.person.mosques_id !=null){
                   $rootScope.person.mosques_id = $rootScope.person.mosques_id+"";
               }else{
                   $rootScope.person.mosques_id ="";
               }
               if($rootScope.person.property_type_id ==null){
                   $rootScope.person.property_type_id="";
               } else{
                   $rootScope.person.property_type_id =$rootScope.person.property_type_id +"";
               }
               if($rootScope.person.roof_material_id ==null){
                   $rootScope.person.roof_material_id="";
               } else{
                   $rootScope.person.roof_material_id =$rootScope.person.roof_material_id +"";
               }
               if($rootScope.person.residence_condition ==null){
                   $rootScope.person.residence_condition="";
               } else{
                   $rootScope.person.residence_condition =$rootScope.person.residence_condition +"";
               }
               if($rootScope.person.indoor_condition ==null){
                   $rootScope.person.indoor_condition="";
               } else{
                   $rootScope.person.indoor_condition =$rootScope.person.indoor_condition +"";
               }
               if($rootScope.person.kinship_id ==null){
                   $rootScope.person.kinship_id="";
               } else{
                   $rootScope.person.kinship_id =$rootScope.person.kinship_id +"";
               }

           }else {
               if($rootScope.person.death_date == null){
                   $rootScope.person.alive = 0+"";
                   $rootScope.person.death_cause_id ="";
                   $rootScope.person.death_date="";
                   if($rootScope.person.condition == null ){
                       $rootScope.person.condition="";
                       $rootScope.person.disease_id="";
                       $rootScope.person.details="";
                   }
                   else{
                       $rootScope.person.condition = $rootScope.person.condition+"";

                       if($rootScope.person.condition == 2 || $rootScope.person.condition == 3 ||
                           $rootScope.person.condition === '2' || $rootScope.person.condition === '3' ){
                           if( $rootScope.person.disease_id==null){
                               $rootScope.person.disease_id="";
                           }
                           else{
                               $rootScope.person.disease_id = $rootScope.person.disease_id+"";
                           }
                       }else{
                           $rootScope.person.disease_id="";
                           $rootScope.person.details="";
                       }
                   }

               }else{
                   $rootScope.person.alive =1+"";

                   if($rootScope.person.death_cause_id !=null){
                       $rootScope.person.death_cause_id = $rootScope.person.death_cause_id+"";
                   }
                   $rootScope.person.condition="";
                   $rootScope.person.disease_id="";
                   $rootScope.person.details="";

               }
           }

       }
       $scope.confirm = function (item) {

           $rootScope.clearToastr();
           if(item.id){
               item.person_id=item.id;
               delete item.id;
           }

           if($rootScope.target =='father' || $rootScope.target =='mother'){
               item.child_id    = $rootScope.person_id ;
               item.child_gender= $rootScope.gender;
               item.save=$rootScope.target;
               item.category_id=$rootScope.category_id;
           }

           if($rootScope.target =='guardian' ){
               item.individual_id    = $rootScope.person_id ;
               item.individual_update= true;
           }


           item.outerEdit = true;
           item.priority = $rootScope.priority;
           item.target    = $rootScope.target ;

           persons.save(item,function (response) {
               if(response.status=='failed_valid')
               {
                   $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                   $scope.status1 =response.status;
                   $scope.msg1 =response.msg;
               } else if(response.status== 'success'){
                   $state.go('sponsorshipCases',{'status':'success',mode:$rootScope.action});
               }
           }, function(error) {
               $rootScope.toastrMessages('error',error.data.msg);
           });

       };
       $scope.rest = function(via,value){
           if(via=='working') {
               if( !angular.isUndefined($scope.msg1.working)) {
                   $scope.msg1.working = [];
               }
               if( !angular.isUndefined($scope.msg1.work_job_id)) {
                   $scope.msg1.work_job_id = [];
               }
               if( !angular.isUndefined($scope.msg1.work_location)) {
                   $scope.msg1.work_location = [];
               }
               if( !angular.isUndefined($scope.msg1.monthly_income)) {
                   $scope.msg1.monthly_income = [];
               }
               $rootScope.person.work_job_id="";
               $rootScope.person.work_location="";
               $rootScope.person.monthly_income="";
           }
           else if( via =='study'){
               if( !angular.isUndefined($scope.msg1.study)) {
                   $scope.msg1.study = [];
               }

               if(value == 0){
                   if( !angular.isUndefined($scope.msg1.stage)) {
                       $scope.msg1.stage = [];
                   }

                   if( !angular.isUndefined($scope.msg1.specialization)) {
                       $scope.msg1.specialization = [];
                   }

                   if( !angular.isUndefined($scope.msg1.degree)) {
                       $scope.msg1.degree = [];
                   }

                   $rootScope.person.stage="";
                   $rootScope.person.degree="";
                   $rootScope.person.specialization="";

               }
           }
           else if( via =='health_status'){
               if( !angular.isUndefined($scope.msg1.condition)) {
                   $scope.msg1.condition = [];
               }

               if (value != 2 || value != 3) {
                   if (!angular.isUndefined($scope.msg1.disease_id)) {
                       $scope.msg1.disease_id = [];
                   }
                   if (!angular.isUndefined($scope.msg1.details)) {
                       $scope.msg1.details = [];
                   }

                   $rootScope.person.disease_id = "";
                   $rootScope.person.details = "";
               }
           }
           else if( via =='alive'){
               if (!angular.isUndefined($scope.msg1.alive)) {
                   $scope.msg1.alive = [];
               }

               if (value == 0) {
                   if (!angular.isUndefined($scope.msg1.death_date)) {
                       $scope.msg1.death_date = [];
                   }
                   if (!angular.isUndefined($scope.msg1.death_cause_id)) {
                       $scope.msg1.death_cause_id = [];
                   }

                   $rootScope.person.death_date = "";
                   $rootScope.person.death_cause_id = "";
               }else{

                   if( !angular.isUndefined($scope.msg1.condition)) {
                       $scope.msg1.condition = [];
                   }
                   if (!angular.isUndefined($scope.msg1.disease_id)) {
                       $scope.msg1.disease_id = [];
                   }
                   if (!angular.isUndefined($scope.msg1.details)) {
                       $scope.msg1.details = [];
                   }

                   $rootScope.person.condition = "";
                   $rootScope.person.disease_id = "";
                   $rootScope.person.details = "";
               }

           }
       };
       $scope.isExists = function(id_card_number){
           $rootScope.clearToastr();
           $rootScope.has_error=false;
           if($rootScope.action != 'edit'){
               if( !angular.isUndefined($scope.msg1.id_card_number)) {
                   $scope.msg1.id_card_number = [];
               }

               if(!angular.isUndefined(id_card_number)) {
                   if(id_card_number.length==9) {
                       if($rootScope.check_id(id_card_number)){
                           var params={'id_card_number':id_card_number,'mode':'edit','target':$rootScope.target,'person' :true, 'persons_i18n' : true, 'education' : true,'work' : true};

                       if($rootScope.target == 'father' || $rootScope.target =='mother'){
                           params.health=true;
                       }else{
                           params.banks=true;
                           params.outer=true;
                           params.residence=true;
                           params.contacts=true;
                           params.category_id=$rootScope.category_id;
                           params.individual_id=$rootScope.person_id;
                       }
                       persons.find(params,function(response){
                           if(response.status == true) {
                               $rootScope.toastrMessages('error',response.msg);
                               $scope.action ='edit';
                               setItem(response.person,null);
                           }else{
                               if(response.person){
                                   $rootScope.person    = response.person;
                                   if($rootScope.person.birthday =="0000-00-00" || $rootScope.person.birthday == null){
                                       $rootScope.person.birthday    = new Date();
                                   }else{
                                       $rootScope.person.birthday    = new Date($rootScope.person.birthday);
                                   }
                               }

                               $rootScope.person.id=null;
                               $rootScope.person.person_id=null;
                               $rootScope.person.banks=[];
                               $rootScope.sub_error=false;
                               $rootScope.msg="";
                           }


                       });
                       }else{
                           $rootScope.toastrMessages('error',$filter('translate')('invalid card number'));
                       }
                   }
               }
           }
       };
       $scope.download = function (target) {
           $rootScope.clearToastr();

           var url ='';
           var file_type ='zip';
           var  params={};
           var method='';

           if(target =='word'){
               method='POST';
               params={'case_id':$scope.id,
                   'target':'info',
                   'action':'filters',
                   'mode':'show',
                   'category_type' : 1,
                   'person' :true,
                   'persons_i18n' : true,
                   'category' : true,
                   'work' : true,
                   'contacts' : true,
                   'health' : true,
                   'default_bank' : true,
                   'education' : true,
                   'islamic' : true,
                   'banks' : true,
                   'residence' : true,
                   'persons_documents' : true,
                   'family_member' : true,
                   'father_id' : true,
                   'mother_id' : true,
                   'guardian_id' : true,
                   'guardian_kinship' : true,
                   'parent_detail':true,
                   'guardian_detail':true,
                   'visitor_note':true
               };
               url = "/api/v1.0/common/sponsorships/cases/word";
           }
           else if(target =='pdf'){
               method='POST';
               params={'case_id':$scope.id,
                   'target':'info',
                   'action':'filters',
                   'mode':'show',
                   'category_type' : 1,
                   'person' :true,
                   'persons_i18n' : true,
                   'category' : true,
                   'work' : true,
                   'contacts' : true,
                   'health' : true,
                   'default_bank' : true,
                   'education' : true,
                   'islamic' : true,
                   'banks' : true,
                   'residence' : true,
                   'persons_documents' : true,
                   'family_member' : true,
                   'father_id' : true,
                   'mother_id' : true,
                   'guardian_id' : true,
                   'guardian_kinship' : true,
                   'parent_detail':true,
                   'guardian_detail':true,
                   'visitor_note':true
               };
               url = "/api/v1.0/common/sponsorships/cases/pdf";
           }
           else{
               method='GET';
               url = "/api/v1.0/common/sponsorships/cases/exportCaseDocument/"+id;
           }

           $http({
               url:url,
               method: method,
               data:params
           }).then(function (response) {

               if(target !='word' && response.data.status == false){
                   $rootScope.toastrMessages('error',$filter('translate')('no attachment to export'));
               }else{
                   window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
               }

           }, function(error) {
               $rootScope.toastrMessages('error',error.data.msg);
           });
       };
       $scope.download9 = function (target) {
           $rootScope.clearToastr();
           var url ='';
           var file_type ='zip';
           var  params={};
           var method='';

           if(target =='word'){
               method='POST';
               params={'case_id':$scope.id,
                   'target':'info',
                   'action':'filters',
                   'mode':'show',
                   'category_type' : 1,
                   'person' :true,
                   'persons_i18n' : true,
                   'category' : true,
                   'work' : true,
                   'contacts' : true,
                   'health' : true,
                   'default_bank' : true,
                   'education' : true,
                   'islamic' : true,
                   'banks' : true,
                   'residence' : true,
                   'persons_documents' : true,
                   'family_member' : true,
                   'father_id' : true,
                   'mother_id' : true,
                   'guardian_id' : true,
                   'parent_detail':true,
                   'guardian_detail':true,
                   'visitor_note':true
               };
               url = "/api/v1.0/common/sponsorships/cases/word";
           }else{
               method='GET';
               url = "/api/v1.0/common/sponsorships/cases/exportCaseDocument/"+$scope.id;
           }

           $http({
               url:url,
               method: method,
               data:params
           }).then(function (response) {

               if(target !='word' && response.data.status == false){
                   $rootScope.toastrMessages('error',$filter('translate')('no attachment to export'));
               }else{
                   window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
               }

           }, function(error) {
               $rootScope.toastrMessages('error',error.data.msg);
           });
       };

// Banks Account
       $scope.add=function(size){
           $rootScope.clearToastr();
           $rootScope.success_frm=false;
           $uibModal.open({
               templateUrl: 'addBank.html',
               backdrop  : 'static',
               keyboard  : false,
               windowClass: 'modal',
               size: size ,
               controller: function ($rootScope,$scope,$stateParams, $modalInstance,Entity) {
                   $scope.action='add';
                   $scope.error_frm=false;
                   $scope.msg3='';
                   $scope.msg4={};
                   $scope.detail={};
                   $scope.getBranches=function(id){
                       if( !angular.isUndefined($scope.msg4.bank_id)) {
                           $scope.msg4.bank_id = [];
                       }

                       Entity.listChildren({parent:'banks',id:id,entity:'branches'},function (response) {
                           $scope.Branches = response;
                       });
                   };

                   Entity.list({entity:'banks'},function (response) {
                       $scope.Banks = response;
                   });


                   $scope.save = function (data) {

                       $rootScope.clearToastr();
                       $scope.error_frm=false;
                       $scope.msg3='';
                       data.id=-1;

                       var flag =-1;
                       angular.forEach($rootScope.person.banks, function(val,key) {
                           if(val.bank_id == data.bank_id){
                               flag = 1;
                               return;
                           }
                       });

                       if(flag == -1){

                           cases.checkAccount({'id':data.bank_id,'account_number':data.account_number},function(response){
                               if(response.status == true){
                                   angular.forEach($scope.Banks, function(val,key) {
                                       if(val.id == data.bank_id){
                                           data.bank_name =val.name;
                                           return;
                                       }
                                   });
                                   angular.forEach($scope.Branches, function(val,key) {
                                       if(val.id == data.branch_name){
                                           data.branch =val.name;
                                           return;
                                       }
                                   });

                                   if(angular.isUndefined($rootScope.person.banks)) {
                                       $rootScope.person.banks=new Array();
                                       data.check=true;
                                   }else{
                                       if($rootScope.person.banks.length ==0 ){
                                           data.check=true;
                                       }else{
                                           data.check=false;
                                       }
                                   }

                                   $rootScope.person.banks.push(data);
                                   $rootScope.no_banks='true';
                                   $rootScope.status1 ='failed_valid_1';
                                   // $rootScope.msg1.banks=[];
                                   $modalInstance.close();
                               }else{
                                   $rootScope.toastrMessages('error',$filter ('translate') ('this account previously in use of other person'));
                               }
                           });

                       }else{
                           $rootScope.toastrMessages('error',$filter('translate')('the account has been entered for the person in the same bank. Please delete the account or modify it'));
                       }
                   };
                   $scope.cancel = function () {
                       $modalInstance.dismiss('cancel');
                   };
               }


           });
       };
       $scope.update=function(size,index,item){
           $rootScope.clearToastr();
           $rootScope.success_frm=false;

           $uibModal.open({
               templateUrl: 'addBank.html',
               backdrop  : 'static',
               keyboard  : false,
               windowClass: 'modal',
               size: size ,
               controller: function ($rootScope,$scope,$stateParams, $modalInstance,Entity) {

                   $scope.error_frm=false;
                   $scope.msg3='';
                   $scope.msg4={};
                   $scope.action='edit';
                   $scope.getBranches=function(id){
                       if( !angular.isUndefined($scope.msg4.bank_id)) {
                           $scope.msg4.bank_id = [];
                       }
                       Entity.listChildren({parent:'banks',id:id,entity:'branches'},function (response) {
                           $scope.Branches = response;

                           if($scope.Branches.length ==0){
                               $scope.detail.branch_name = "";
                           }
                       });
                   };

                   Entity.list({entity:'banks'},function (response) {
                       $scope.Banks = response;
                   });

                   $scope.detail=item;
                   if( $scope.detail.bank_id == null) {
                       $scope.detail.bank_id  = "";
                       $scope.detail.branch_name = "";
                       $scope.detail.account_number = null;
                       $scope.detail.account_owner = null;
                   }else{
                       var branch_name =$scope.detail.branch_name;
                       $scope.getBranches($scope.detail.bank_id);
                       $scope.detail.bank_id = $scope.detail.bank_id +"";
                       if( $scope.detail.branch_name == null) {
                           $scope.detail.branch_name = "";
                       }else{
                           $scope.detail.branch_name = branch_name +"";
                       }

                   }

                   $scope.save = function (data) {
                       $rootScope.clearToastr();
                       $rootScope.success_frm=false;
                       $scope.error_frm=false;
                       $scope.msg3='';
                       data.id=-1;

                       cases.checkAccount({'id':data.bank_id,'account_number':data.account_number},function(response){
                           if(response.status == true){
                               angular.forEach($scope.Banks, function(val,key) {
                                   if(val.id == data.bank_id){
                                       data.bank_name =val.name;
                                       return;
                                   }
                               });
                               angular.forEach($scope.Branches, function(val,key) {
                                   if(val.id == data.branch_name){
                                       data.branch =val.name;
                                       return;
                                   }
                               });
                               if(angular.isUndefined($rootScope.person.banks)) {
                                   $rootScope.person.banks=new Array();
                                   data.check=true;
                               }else{
                                   if($rootScope.person.banks.length ==0 ){
                                       data.check=true;
                                   }else{
                                       data.check=false;
                                   }
                               }

                               $rootScope.person.banks[index]=data;
                               $rootScope.toastrMessages('success',$filter('translate')('action success'));
                               $rootScope.no_banks='true';
                               $rootScope.status1 ='failed_valid_1';
                               // $rootScope.msg1.banks=[];
                               $modalInstance.close();
                           }else{
                               $rootScope.toastrMessages('error',$filter ('translate') ('this account previously in use of other person'));
                           }
                       });


                   };
                   $scope.cancel = function () {
                       $modalInstance.dismiss('cancel');
                   };
               }


           });
       };
       $scope.delete = function (index) {
           $rootScope.clearToastr();
           $rootScope.success_frm=false;

           $rootScope.person.banks.splice(index,1);
           if($rootScope.person.banks.length == 0){
               $rootScope.no_banks='false';
           }
           $rootScope.toastrMessages('success',$filter('translate')('The row is deleted from db'));

       };
       $scope.setOrgDefault  = function(status,index,item){
           $rootScope.success_frm=false;
           var l=-1;
           var bk=-1;
           angular.forEach($rootScope.person.banks, function(val,key) {
               if(val.check){
                   l=key;
                   bk=val;
               }
           });
           if(bk != -1){
               if( l!=index ){
                   $rootScope.person.banks[l].check=false;
                   $rootScope.person.banks[index].check=true;
                   $rootScope.default_bank=item.bank_id;
               }
           }else{
               $rootScope.person.banks[index].check=true;
               $rootScope.default_bank=item.bank_id;
           }


       };

        $scope.checkBirthday  = function(birthday){
            if( !angular.isUndefined($rootScope.msg1.birthday)) {
                $rootScope.msg1.birthday = [];
            }

            var curDate = new Date();
            if(new Date(birthday) > curDate){
                $rootScope.msg1.birthday = {0 :$filter('translate')('the enter date must be less than current date')};
                $rootScope.status1 ='failed_valid';
            }

        };
        $scope.getLocation = function(entity,parent,value){
            if(!angular.isUndefined(value)) {
                if (value !== null && value !== "" && value !== " ") {
                    Entity.listChildren({'entity': entity, 'id': value, 'parent': parent}, function (response) {

                        if (entity == 'districts') {
                            if (!angular.isUndefined($rootScope.msg1.country)) {
                                $rootScope.msg1.country = [];
                            }

                            $scope.governarate = response;
                            $scope.city = [];
                            $scope.nearlocation =[];
                            $scope.mosques = [];
                            $rootScope.person.governarate = "";
                            $rootScope.person.city = "";
                            $rootScope.person.location_id = "";

                        }
                        else if (entity == 'cities') {
                            if (!angular.isUndefined($rootScope.msg1.governarate)) {
                                $rootScope.msg1.governarate = [];
                            }

                            $scope.city = response;
                            $scope.nearlocation = [];
                            $scope.mosques = [];
                            $rootScope.person.city = "";
                            $rootScope.person.location_id = "";

                        }
                        else if (entity == 'neighborhoods') {
                            if (!angular.isUndefined($rootScope.msg1.city)) {
                                $rootScope.msg1.city = [];
                            }
                            $scope.nearlocation = response;
                            $scope.mosques = [];
                            $rootScope.person.location_id = "";
                            $rootScope.person.mosques_id = "";

                        } else if(entity =='mosques'){
                            if( !angular.isUndefined($rootScope.msg1.location_id)) {
                                $rootScope.msg1.location_id = [];
                            }

                            $scope.mosques = response;
                            $rootScope.person.mosques_id="";
                        }


                    });
                }
            }
        };

        $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0 };
        $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $rootScope.format = $rootScope.formats[0];
        $rootScope.today = function() { $rootScope.dt = new Date(); };
        $rootScope.today();
        $rootScope.clear = function() { $rootScope.dt = null; };
        $rootScope.open3 = function($event) {$event.preventDefault(); $event.stopPropagation(); $rootScope.popup3.opened = true; };
        $rootScope.open4 = function($event) {$event.preventDefault(); $event.stopPropagation(); $rootScope.popup4.opened = true; };
        $rootScope.popup3= { opened: false };
        $rootScope.popup4= { opened: false};
    });