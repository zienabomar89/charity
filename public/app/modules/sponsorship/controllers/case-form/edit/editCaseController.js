angular.module('SponsorshipModule')
    .directive("dynamicName",function($compile){
        return {
            restrict:"A",
            terminal:true,
            priority:1000,
            link:function(scope,element,attrs){
                element.attr('name', scope.$eval(attrs.dynamicName));
                element.removeAttr("dynamic-name");
                $compile(element)(scope);
            }
        }
    }) .controller('editCaseController', function ($http,$filter,$scope,$rootScope,$state,$stateParams,$timeout,$uibModal,persons,cases,Entity,forms_case_data,category,OAuthToken,FileUploader) {

    $rootScope.settings.layout.pageSidebarClosed = true;
        $rootScope.target=$scope.target=$stateParams.target;
        $rootScope.id=$scope.id=$stateParams.id;

        $scope.documents=$scope.items=$scope.Caseitems=$scope.fields=[];
        
        $scope.success =false;
        $scope.failed =false;
        $scope.error =false;
        $scope.model_error_status =false;

        $rootScope.result_1 = 'true';
        $rootScope.result = 'true';

        $scope.priority={};
        $rootScope.persons={'banks':[]};

        $rootScope.msg1={};
        $scope.msg5={};
        $rootScope.status1='';
        $rootScope.error=false;
        $rootScope.success=false;
        $scope.category_name = '';
        $scope.name = '';
        $scope.category_id = $rootScope.category_id = $scope.guardian_id =$scope.father_id = $scope.mother_id = $rootScope.person_id = null;

        $rootScope.msg='';
        $rootScope.msg2={};
        $scope.success_frm=false;
        $scope.classified_family =false;
        $scope.success_msg="";

        $scope.close=function(){
            $scope.success =false;
            $scope.failed =false;
            $scope.error =false;
            $scope.model_error_status =false;
            $scope.model_error_status2 =false;
        };

        if($scope.id != null){
            cases.getCaseReports({'action':'filters','target':'info','case_id':$scope.id, 'mode':'show', 'category_type' : 1, 'full_name' :true, 'mother_id' :true, 'father_id' :true, 'guardian_id' :true},function(response) {
                $scope.category_name = response.category_name;
                $scope.case_is = response.category_name;
                $scope.organization_id = response.organization_id;

                if(response.case_is == 1 && response.family_structure == 2){
                    $scope.classified_family =true;
                }


                $scope.category_name = response.category_name;
                $scope.name = response.full_name;
                $scope.guardian_id = response.guardian_id;
                $scope.father_id = response.father_id;
                $scope.father_id = response.father_id;
                $rootScope.category_id = response.category_id;
                $scope.mother_id = response.mother_id;
                $rootScope.person_id = response.person_id;
                $rootScope.user_organization_id = response.user_organization_id;
                $rootScope.gender = response.gender;

                if($scope.target !='custom-data' && $scope.target !='dcouments' ) {

                    var params = {'type': 'sponsorships', 'category_id': response.category_id, 'target': 'fm'};
                    if ($scope.target == 'family') {
                        params.target = 'fm';
                    } else {
                        params.target = 'fm-case';
                    }

                    category.getPriority(params, function (pResponse) {

                        if ($scope.target == 'family') {
                            $rootScope.priority = pResponse.priority;
                        } else {
                            $scope.priority = pResponse.priority;
                        }
                    });

                }

                });
        }

        if($scope.target !='custom-data' && $scope.target !='dcouments' ){
            Entity.get({'entity':'entities' , 'c':'workJobs,kinship,maritalStatus,educationStages,educationAuthorities,countries,diseases,kinship,propertyTypes,roofMaterials,furnitureStatus,houseStatus'},function (response){
                $rootScope.WorkJob = response.workJobs;
                $rootScope.MaritalStatus = response.maritalStatus;
                $rootScope.EduStages = response.educationStages;
                $scope.EduAuthorities = response.educationAuthorities;
                $rootScope.Diseases = response.diseases;
                $rootScope.Kinship = response.kinship;
                $rootScope.Country = response.countries;
                $scope.houseStatus = response.houseStatus;
                $scope.furnitureStatus = response.furnitureStatus;
                $scope.PropertyTypes = response.propertyTypes;
                $scope.RoofMaterials = response.roofMaterials;
            });
        }

        var loadPageContents=function () {

            if($scope.target =='family'){
                $rootScope.progressbar_start();
                cases.getCaseReports({'case_id':$scope.id,
                    'target':'info',
                    'action':'filters',
                    'category_type':1,
                    'mode':'edit',
                    'family_member' : true,
                    'father_id' : true,
                    'mother_id' : true,
                    'guardian_id' : true,
                    'category' : true},function(response) {
                    $rootScope.progressbar_complete();
                    $rootScope.father_id =response.father_id;
                    $rootScope.mother_id =response.mother_id;
                    $rootScope.guardian_id =response.guardian_id;
                    $rootScope.l_person_id =response.l_person_id;
                    $rootScope.unCaseMem =response.family_member.fm;
                    $rootScope.CaseMem=response.family_member.fm_cases;
                });

            }
            else if($scope.target =='custom-form'){
                forms_case_data.get({id: $stateParams.id,module:'sponsorship',category_id:$rootScope.category_id},function (response) {

                    if( !angular.isUndefined(response.status)){
                        if(response.status === false) {
                            $rootScope.result_1 = 'false';
                        }
                    }

                    if( !angular.isUndefined(response.status_2)){
                        if(response.status_2 === false) {
                            $rootScope.result = 'false';
                        }
                    }

                    if(angular.isUndefined(response.status_2) && angular.isUndefined(response.status)){
                        if (response.data.length !== 0) {
                            var temp = response.data;

                            angular.forEach(temp, function(v, k) {

                                if(v.value !== null && v.value !== 'null'){
                                    if (v.options.length !== 0) {
                                        v.data=parseInt(v.value) +"";
                                    }else{
                                        v.data=v.value;
                                    }
                                }else{
                                    v.data="";
                                }


                            });
                            $scope.fields=temp;
                        }
                    }

                });
            }
            else if($scope.target =='documents'){

                cases.getCaseReports({'action':'filters','target':'info','case_id':$scope.id, 'mode':'edit', 'persons_documents' :true,  'category' :true ,'guardian_id' :true, 'father_id' :true, 'mother_id' :true},function(response) {
                    $scope.documents =response.persons_documents;
                    $scope.result='true';
                });


            }
            else{
                var param={'target':'info','action':'filters','case_id':$scope.id, 'mode':'edit','person' :true, 'persons_i18n' : true, 'contacts' : true,  'education' : true,'health' : true,'islamic' : true,'category_type':1,
                    'work' : true, 'residence' : true, 'banks' : true ,'father_id' : true,'mother_id' : true,'guardian_id' : true };
                cases.getCaseReports(param,function(response) {
                    $rootScope.persons=response;
                    $rootScope.persons.secondary_mobile=response.secondery_mobile;
                    // $scope.priority=response.priority;

                    if($rootScope.persons.monthly_income==null){
                        $rootScope.persons.monthly_income = "";
                    }
                    if($rootScope.persons.death_cause_id !=null){
                        $rootScope.persons.death_cause_id = $rootScope.persons.death_cause_id+"";
                    }

                    if($rootScope.persons.marital_status_id !=null){
                        $rootScope.persons.marital_status_id = $rootScope.persons.marital_status_id+"";
                    }else{
                        $rootScope.persons.marital_status_id = "";
                    }

                    if($rootScope.persons.country !=null){
                        $rootScope.persons.country = $rootScope.persons.country+"";
                        Entity.listChildren({entity:'districts','id':$rootScope.persons.country,'parent':'countries'},function (response) {
                            $scope.governarate = response;
                        });
                    }else{
                        $rootScope.persons.country ="";

                    }
                    if($rootScope.persons.governarate !=null){
                        $rootScope.persons.governarate = $rootScope.persons.governarate+"";

                        Entity.listChildren({parent:'districts','id':$rootScope.persons.governarate,entity:'cities'},function (response) {
                            $scope.city = response;
                        });
                    }else{
                        $rootScope.persons.governarate ="";
                    }
                    if($rootScope.persons.city !=null){
                        $rootScope.persons.city = $rootScope.persons.city+"";
                        Entity.listChildren({entity:'neighborhoods','id':$rootScope.persons.city,'parent':'cities'},function (response) {
                            $scope.nearlocation = response;
                        });
                    }else{
                        $rootScope.persons.city ="";
                    }
                    if($rootScope.persons.location_id !=null){
                        $rootScope.persons.location_id = $rootScope.persons.location_id+"";
                        Entity.listChildren({entity:'mosques','id':$rootScope.persons.location_id,'parent':'neighborhoods'},function (response) {
                            $scope.mosques = response;
                        });
                    }else{
                        $rootScope.persons.location_id ="";
                    }
                    if($rootScope.persons.mosques_id !=null){
                        $rootScope.persons.mosques_id = $rootScope.persons.mosques_id+"";
                    }else{
                        $rootScope.persons.mosques_id ="";
                    }
                    if($rootScope.persons.gender ==0){
                        $rootScope.persons.gender="";
                    }else{
                        $rootScope.persons.gender=parseInt($rootScope.persons.gender) +"";
                    }
                    if($rootScope.persons.nationality==null){
                        $rootScope.persons.nationality="";
                    } else{
                        $rootScope.persons.nationality=parseInt($rootScope.persons.nationality) +"";
                    }
                    if($rootScope.persons.birth_place==null){
                        $rootScope.persons.birth_place="";
                    } else{
                        $rootScope.persons.birth_place=$rootScope.persons.birth_place +"";
                    }
                    if($rootScope.persons.study==null || $rootScope.persons.study == 0 || $rootScope.persons.study =='0'){
                        $rootScope.persons.study=0+"";
                        $rootScope.persons.type="";
                        $rootScope.persons.authority="";
                        $rootScope.persons.grade="";
                        $rootScope.persons.stage="";
                        $rootScope.persons.level="";
                        $rootScope.persons.school="";
                        $rootScope.persons.year="";
                        $rootScope.persons.points="";
                    } else{
                        $rootScope.persons.study=$rootScope.persons.study+"";

                        if($rootScope.persons.type==null){
                            $rootScope.persons.type="";
                        } else{
                            $rootScope.persons.type=$rootScope.persons.type+"";
                        }
                        if($rootScope.persons.authority==null){
                            $rootScope.persons.authority="";
                        } else{
                            $rootScope.persons.authority=$rootScope.persons.authority+"";
                        }
                        if($rootScope.persons.grade==null){
                            $rootScope.persons.grade="";
                        } else{
                            $rootScope.persons.grade=$rootScope.persons.grade+"";
                        }
                        if($rootScope.persons.stage==null){
                            $rootScope.persons.stage="";
                        } else{
                            $rootScope.persons.stage=$rootScope.persons.stage+"";
                        }
                        if($rootScope.persons.level==null){
                            $rootScope.persons.level="";
                        } else{
                            $rootScope.persons.level=$rootScope.persons.level+"";
                        }


                    }
                    if($rootScope.persons.condition == null || $rootScope.persons.health_condition == 1 || $rootScope.persons.health_condition == '1' ){
                        $rootScope.persons.condition="";
                        $rootScope.persons.disease_id="";
                        $rootScope.persons.details="";
                    }else{
                        $rootScope.persons.condition=$rootScope.persons.condition+"";
                        if($rootScope.persons.disease_id==null){
                            $rootScope.persons.disease_id="";
                        } else{
                            $rootScope.persons.disease_id=$rootScope.persons.disease_id+"";
                        }
                    }
                    if($rootScope.persons.property_type_id ==null){
                        $rootScope.persons.property_type_id="";
                    } else{
                        $rootScope.persons.property_type_id =$rootScope.persons.property_type_id +"";
                    }
                    if($rootScope.persons.roof_material_id ==null){
                        $rootScope.persons.roof_material_id="";
                    } else{
                        $rootScope.persons.roof_material_id =$rootScope.persons.roof_material_id +"";
                    }
                    if($rootScope.persons.residence_condition ==null){
                        $rootScope.persons.residence_condition="";
                    } else{
                        $rootScope.persons.residence_condition =$rootScope.persons.residence_condition +"";
                    }
                    if($rootScope.persons.indoor_condition ==null){
                        $rootScope.persons.indoor_condition="";
                    } else{
                        $rootScope.persons.indoor_condition =$rootScope.persons.indoor_condition +"";
                    }
                    if($rootScope.persons.quran_center==null){
                        $rootScope.persons.quran_center="";
                    } else{
                        if($rootScope.persons.quran_center==0){
                            $rootScope.persons.quran_center=0+"";
                        }
                        else{
                            $rootScope.persons.quran_center=1+"";
                        }
                    }
                    if($rootScope.persons.prayer==null){
                        $rootScope.persons.prayer="";
                    } else{
                        $rootScope.persons.prayer =$rootScope.persons.prayer +"";
                    }
                    if($rootScope.persons.quran_chapters==0){
                        $rootScope.persons.save_quran=1+"";
                    } else if($rootScope.persons.quran_chapters==null){
                        $rootScope.persons.save_quran="";
                    } else {
                        $rootScope.persons.save_quran=0+"";

                    }
                    if($rootScope.persons.quran_parts==null){
                        $rootScope.persons.save_quran =1+"";
                    } else{
                        $rootScope.persons.save_quran =0+"";
                    }

                });
            }
        };

        loadPageContents();

        $scope.forward=function(data){

            $rootScope.clearToastr();
            if($scope.target =='custom-form'){
                var target = new forms_case_data({case_id:$stateParams.id,'fieldsData':data});
                target.$save()
                    .then(function (response) {
                        if(response.status=='failed_valid')
                        {
                            $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                            $scope.status5 =response.status;
                            $scope.msg5 =response.msg;
                        }
                        else {
                            $state.go('sponsorshipCases',{"mode": 'edit',"status" :'success'});
                        }
                    },function(error) {
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
            }
            else{
                $rootScope.clearToastr();
                if( !angular.isUndefined(data.birthday)) {
                    data.birthday= $filter('date')(data.birthday, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(data.health_condition)) {
                                        data.condition=data.health_condition
                 }
                data.action='JustUpdate';

                if ($scope.target == 'family') {
                    data.priority = $rootScope.priority;
                } else {
                    data.priority = $scope.priority;
                }

                // data.priority=$scope.priority;
                data.case_id=$scope.id;
                data.target='case';
                delete  data.id;

                $http({
                    method: 'POST',
                    url: '/api/v1.0/common/sponsorships/cases',
                    data:data
                }).then(function successCallback(response) {
                    if(response.data.status=='failed_valid'){
                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                        $rootScope.status1 = response.data.status;
                        $rootScope.msg1 = response.data.msg;
                    }  if(response.data.status== 'success'){
                        $state.go('sponsorshipCases',{"mode": 'edit',"status" :'success'});
                    }
                });
            }
        };


    $scope.case= function (id,case_id) {
        if(id == -1){
            $state.go('sub.fm-case-info',{category_id:$rootScope.category_id,'father_id':$rootScope.father_id,
                mother_id:$rootScope.mother_id,'guardian_id':$rootScope.guardian_id});
        }else{
            $state.go('sub.fm-case-info',{case_id:case_id,id:id,category_id:$rootScope.category_id,'father_id':$rootScope.father_id,
                mother_id:$rootScope.mother_id,'guardian_id':$rootScope.guardian_id});

        }

    };
    $scope.personOp = function(size,action,id) {
        $rootScope.clearToastr();


            $uibModal.open({
                templateUrl: 'newPerson.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope,$stateParams, $modalInstance,$filter) {

                    $scope.msg1={};
                    $scope.persons={};
                    $scope.model_error=false;
                    $scope.msg32='';
                    $scope.action=action;
                    $scope.isPersonExists = function(id_card_number){
                        $rootScope.clearToastr();
                        if( !angular.isUndefined($scope.msg1.id_card_number)) {
                            $scope.msg1.id_card_number = [];
                        }
                        if(action =='add'){
                            if(!angular.isUndefined($scope.persons.id_card_number)){
                                if(id_card_number.length==9 ) {

                                    if($rootScope.check_id(id_card_number)){
                                        var params={ 'mode':'edit','l_person_id' :$rootScope.l_person_id,'id_card_number':id_card_number,
                                        'person' :true, 'persons_i18n' : true, 'education' : true, 'work' : true, 'health' : true};

                                    if(!angular.isUndefined($rootScope.mother_id) && $rootScope.mother_id != null && $rootScope.mother_id != "") {
                                        // params.mother_id=$rootScope.mother_id;
                                    }

                                    persons.find(params,function(response){
                                        if(response.status == true){
                                            $rootScope.toastrMessages('error',response.msg);
                                            $scope.persons=response.person;

                                            if($scope.persons.marital_status_id==null){
                                                $scope.persons.marital_status_id="";
                                            } else{
                                                $scope.persons.marital_status_id = $scope.persons.marital_status_id+"";
                                            }

                                            if($scope.persons.marital_status_id==null){
                                                $scope.persons.marital_status_id="";
                                            } else{
                                                $scope.persons.marital_status_id = $scope.persons.marital_status_id+"";
                                            }


                                            if($scope.persons.gender ==0 || $scope.persons.gender ==null){
                                                $scope.persons.gender="";
                                            }else{
                                                $scope.persons.gender=$scope.persons.gender+"";
                                            }

                                            // if($scope.persons.kinship_id==null){
                                            //     $scope.persons.kinship_id="";
                                            // } else{
                                            //     $scope.persons.kinship_id=$scope.persons.kinship_id+"";
                                            //     // $scope.persons.kinship=$scope.persons.kinship_id+"";
                                            // }

                                            if($scope.persons.kinship_id==null){
                                                $scope.persons.kinship_id="";
                                            } else{
                                                $scope.persons.kinship_id=$scope.persons.kinship_id+"";
                                            }

                                            if($scope.persons.study == null){
                                                $scope.persons.study = "";
                                            }else{
                                                $scope.persons.study= $scope.persons.study +"";

                                                if($scope.persons.study == 0){
                                                    $scope.persons.stage="";
                                                }else{
                                                    if($scope.persons.stage==null){
                                                        $scope.persons.stage="";
                                                    } else{
                                                        $scope.persons.stage=$scope.persons.stage+"";
                                                    }
                                                }
                                            }
                                            if($scope.persons.working == null ){
                                                $scope.persons.working="";
                                                $scope.persons.work_job_id="";
                                            }else{
                                                $scope.persons.working=$scope.persons.working+"";
                                                if($scope.persons.working == 1){
                                                    $scope.persons.work_job_id = $scope.persons.work_job_id+"";
                                                }else{
                                                    $scope.persons.work_job_id="";
                                                }
                                            }

                                            if($scope.persons.condition == null ){
                                                $scope.persons.condition="";
                                            }else{
                                                $scope.persons.condition=$scope.persons.condition+"";
                                            }
                                        }else{
                                            $scope.model_error=false;
                                            $scope.msg32='';
                                            if(response.person){
                                                $scope.persons    = response.person;

                                                if($scope.persons.birthday =="0000-00-00" || $scope.persons.birthday == null){
                                                    $scope.persons.birthday    = new Date();
                                                }else{
                                                    $scope.persons.birthday    = new Date($scope.persons.birthday);
                                                }
                                            }
                                            $scope.persons.id=null;
                                        }

                                    }, function(error) {
                                        $rootScope.toastrMessages('error',error.data.msg);
                                    });
                                }else{
                                    $rootScope.toastrMessages('error',$filter('translate')('invalid card number'));
                                }
                            }
                            }


                        }
                    };

                    $scope.setName=function(value){
                        if( !angular.isUndefined($scope.msg1.kinship)) {
                            $scope.msg1.kinship = [];
                        }
                        if(value ==13 || value ==14 || value ==3 || value ==4){
                            var params={ 'mode':'edit','target':'info', 'person' :true, 'persons_i18n' : true,'person_id':$rootScope.l_person_id};
                            persons.getPersonReports(params,function(response){
                                if(value ==1) {
                                    $scope.persons.first_name=response.second_name;
                                    $scope.persons.second_name=response.third_name;
                                    $scope.persons.last_name=response.last_name;

                                    $scope.persons.en_first_name=response.en_second_name;
                                    $scope.persons.en_second_name=response.en_third_name;
                                    $scope.persons.en_last_name=response.en_last_name;
                                }

                                if(value ==13 || value ==14) {
                                    $scope.persons.second_name=response.first_name;
                                    $scope.persons.third_name=response.second_name;
                                    $scope.persons.last_name=response.last_name;
                                    $scope.persons.en_second_name=response.en_first_name;
                                    $scope.persons.en_third_name=response.en_second_name;
                                    $scope.persons.en_last_name=response.en_last_name;
                                }

                                if(value ==3 || value ==4) {
                                    $scope.persons.second_name=response.second_name;
                                    $scope.persons.third_name=response.third_name;
                                    $scope.persons.last_name=response.last_name;
                                    $scope.persons.en_second_name=response.en_second_name;
                                    $scope.persons.en_third_name=response.en_third_name;
                                    $scope.persons.en_last_name=response.en_last_name;
                                }
                            }, function(error) {
                                $rootScope.toastrMessages('error',error.data.msg);
                            });

                        }
                    };

                    if(action =='edit'){
                        var params={ 'mode':'edit','target':'info','l_person_id' :$rootScope.l_person_id,'person_id':id,
                            'person' :true, 'persons_i18n' : true, 'education' : true, 'work' : true, 'health' : true};

                        if(!angular.isUndefined($rootScope.mother_id) && $rootScope.mother_id != null && $rootScope.mother_id != "") {
                            params.mother_id=$rootScope.mother_id;
                        }

                        persons.getPersonReports(params,function(response){
                            $scope.persons=response;

                            if($scope.persons.marital_status_id==null){
                                $scope.persons.marital_status_id="";
                            } else{
                                $scope.persons.marital_status_id = $scope.persons.marital_status_id+"";
                            }

                            if($scope.persons.marital_status_id==null){
                                $scope.persons.marital_status_id="";
                            } else{
                                $scope.persons.marital_status_id = $scope.persons.marital_status_id+"";
                            }


                            if($scope.persons.gender ==0 || $scope.persons.gender ==null){
                                $scope.persons.gender="";
                            }else{
                                $scope.persons.gender=$scope.persons.gender+"";
                            }

                            if($scope.persons.kinship_id==null){
                                $scope.persons.kinship_id="";
                            } else{
                                $scope.persons.kinship_id=$scope.persons.kinship_id+"";
                            }


                            if($scope.persons.study == null){
                                $scope.persons.study = "";
                            }else{
                                $scope.persons.study= $scope.persons.study +"";

                                if($scope.persons.study == 0){
                                    $scope.persons.stage="";
                                }else{
                                    if($scope.persons.stage==null){
                                        $scope.persons.stage="";
                                    } else{
                                        $scope.persons.stage=$scope.persons.stage+"";
                                    }
                                }
                            }

                            if($scope.persons.working == null ){
                                $scope.persons.working="";
                                $scope.persons.work_job_id="";
                            }else{
                                $scope.persons.working=$scope.persons.working+"";
                                if($scope.persons.working == 1){
                                    $scope.persons.work_job_id = $scope.persons.work_job_id+"";
                                }else{
                                    $scope.persons.work_job_id="";
                                }
                            }

                            if($scope.persons.condition == null ){
                                $scope.persons.condition="";
                            }else{
                                $scope.persons.condition=$scope.persons.condition+"";
                            }
                        }, function(error) {
                            $rootScope.toastrMessages('error',error.data.msg);
                        });
                    }

                    $scope.save = function (data) {
                        $rootScope.clearToastr();
                        if( !angular.isUndefined(data.birthday)) {
                            data.birthday= $filter('date')(data.birthday, 'dd-MM-yyyy')
                        }

                        data.father_id= $rootScope.father_id;
                        data.mother_id= $rootScope.mother_id;
                        data.guardian_id= $rootScope.guardian_id;
                        data.l_person_id=$rootScope.l_person_id;

                        data.priority= $scope.priority;

                       if(data.id){
                            data.person_id=data.id;
                            delete data.id;
                        }


                        persons.save(data,function (response) {
                            if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            } else {
                                if(response.status){
                                    $rootScope.toastrMessages('error',response.msg);
                                    loadPageContents();
                                }else{
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }
                                $modalInstance.close();
                            }

                        }, function(error) {
                            $rootScope.toastrMessages('error',error.data.msg);
                        });

         };
                    $scope.Reset=function(value){
                        if( !angular.isUndefined($scope.msg1.study)) {
                            $scope.msg1.study = [];
                        }

                        if(value == 0){
                            if( !angular.isUndefined($scope.msg1.stage)) {
                                $scope.msg1.stage = [];
                            }

                            $scope.persons.stage="";

                        }


                    };
                    $scope.restValue = function(i,value){

                        if(i==1) {
                            if( !angular.isUndefined($scope.msg1.fm_working)) {
                                $scope.msg1.fm_working = [];
                            }
                            if( !angular.isUndefined($scope.msg1.work_job_id)) {
                                $scope.msg1.work_job_id = [];
                            }
                            $scope.persons.work_job_id="";
                        }

                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };
        $scope.deletePerson = function(size,id) {
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'deletePerson.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope,$stateParams, $modalInstance) {

                    $scope.msg1={};
                    $scope.persons={};
                    $scope.model_error=false;

                    $scope.confirmDelete = function (data) {
                        persons.delete({id:id,l_person_id:$rootScope.l_person_id},function (response) {

                            if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            } else {
                                if(response.status == 'success'){
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    loadPageContents();
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                $modalInstance.close();
                            }
                        }, function(error) {
                            $modalInstance.close();
                            $rootScope.toastrMessages('error',error.data.msg);
                        });
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };
        $scope.visitorNote=function(id){

            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'VisitorNote.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log) {
                    $rootScope.Notes='';
                    cases.getCaseReports({'action':'filters','target':'info','case_id':id,'visitor_note':true, 'mode':'show'},function(response) {
                        if(response.visitor_note_status==false)
                        {
                            $rootScope.toastrMessages('error',$filter('translate')("The researcher's opinion is not written in the settings"));
                        }else{
                            $rootScope.Notes = response.visitor_note;
                        }
                    });
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                },
                resolve: {
                }
            });
        };
       $scope.download = function (target) {

        $rootScope.clearToastr();
        var url ='';
        var file_type ='zip';
        var  params={};
        var method='';

        if(target =='word'){
            method='POST';
            params={'case_id':$scope.id,
                'target':'info',
                'action':'filters',
                'mode':'show',
                'category_type' : 1,
                'person' :true,
                'persons_i18n' : true,
                'category' : true,
                'work' : true,
                'contacts' : true,
                'health' : true,
                'default_bank' : true,
                'education' : true,
                'islamic' : true,
                'banks' : true,
                'residence' : true,
                'persons_documents' : true,
                'family_member' : true,
                'father_id' : true,
                'mother_id' : true,
                'guardian_id' : true,
                'parent_detail':true,
                'guardian_detail':true,
                'visitor_note':true
            };
            url = "/api/v1.0/common/sponsorships/cases/word";
        }
        else if(target =='pdf'){
            method='POST';
            params={'case_id':$scope.id,
                'target':'info',
                'action':'filters',
                'mode':'show',
                'category_type' : 1,
                'person' :true,
                'persons_i18n' : true,
                'category' : true,
                'work' : true,
                'contacts' : true,
                'health' : true,
                'default_bank' : true,
                'education' : true,
                'islamic' : true,
                'banks' : true,
                'residence' : true,
                'persons_documents' : true,
                'family_member' : true,
                'father_id' : true,
                'mother_id' : true,
                'guardian_id' : true,
                'parent_detail':true,
                'guardian_detail':true,
                'visitor_note':true
            };
            url = "/api/v1.0/common/sponsorships/cases/pdf";
        }
        else{
            method='GET';
            url = "/api/v1.0/common/sponsorships/cases/exportCaseDocument/"+$scope.id;
        }

        $http({
            url:url,
            method: method,
            data:params
        }).then(function (response) {

            if(target !='word' && response.data.status == false){
                $rootScope.toastrMessages('error',$filter('translate')('no attachment to export'));
            }else{
                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
            }

        }, function(error) {
            $rootScope.toastrMessages('error',error.data.msg);
        });
    };

    $scope.add=function(size){
        $rootScope.clearToastr();
        $uibModal.open({
                templateUrl: 'addBank.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size ,
                controller: function ($rootScope,$scope,$stateParams, $modalInstance,Entity) {

                    $scope.action='add';
                    $scope.error_frm=false;
                    $scope.msg3='';
                    $scope.msg4={};
                    $scope.detail={};
                    $scope.getBranches=function(id){
                        if( !angular.isUndefined($rootScope.msg1.bank_id)) {
                            $scope.msg4.bank_id = [];
                        }

                        Entity.listChildren({parent:'banks','id':id,'entity':'branches'},function (response) {
                            $scope.Branches = response;
                        });
                    };


                    Entity.list({entity:'banks'},function (response) {
                        $scope.Banks = response;
                    });


                    $scope.save = function (data) {
                        $rootScope.clearToastr();
                        data.id=-1;
                        $scope.error_frm=false;
                        $scope.msg3='';

                        var flag =-1;
                        angular.forEach($rootScope.persons.banks, function(val,key) {
                            if(val.bank_id == data.bank_id){
                                flag = 1;
                                return;
                            }
                        });

                        if(flag == -1){
                            cases.checkAccount({'id':data.bank_id,'account_number':data.account_number},function(response){
                                if(response.status != true && response.person_id != $rootScope.person_id){
                                    $rootScope.toastrMessages('error',$filter ('translate') ('this account previously in use of other person'));
                                }
                                else{
                                    angular.forEach($scope.Banks, function(val,key) {
                                        if(val.id == data.bank_id){
                                            data.bank_name =val.name;
                                            return;
                                        }
                                    });
                                    angular.forEach($scope.Branches, function(val,key) {
                                        if(val.id == data.branch_name){
                                            data.branch =val.name;
                                            return;
                                        }
                                    });
                                    if($rootScope.persons.banks.length ==0){
                                        data.check=true;
                                    }else{
                                        data.check=false;
                                    }

                                    $rootScope.persons.banks.push(data);
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    $rootScope.no_banks='true';
                                    $rootScope.status1 ='failed_valid';
                                    $rootScope.msg1.banks=[];
                                    $modalInstance.close();
                                }

                            });

                        }else{
                            $rootScope.toastrMessages('error',$filter('translate')('the account has been entered for the person in the same bank. Please delete the account or modify it'));
                        }


                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }


            });
        };
        $scope.update=function(size,index,item){
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'addBank.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size ,
                controller: function ($rootScope,$scope,$stateParams, $modalInstance,Entity) {

                    $scope.error_frm=false;
                    $scope.msg3='';
                    $scope.msg4={};
                    $scope.action='edit';
                    $scope.getBranches=function(id){
                        if( !angular.isUndefined($scope.msg4.bank_id)) {
                            $scope.msg4.bank_id = [];
                        }
                        Entity.listChildren({parent:'banks',id:id,entity:'branches'},function (response) {
                            $scope.Branches = response;

                            if($scope.Branches.length ==0){
                                $scope.detail.branch_name = "";
                            }
                        });
                    };

                    Entity.list({entity:'banks'},function (response) {
                        $scope.Banks = response;
                    });

                    $scope.detail=item;
                    if( $scope.detail.bank_id == null) {
                        $scope.detail.bank_id  = "";
                        $scope.detail.branch_name = "";
                        $scope.detail.account_number = null;
                        $scope.detail.account_owner = null;
                    }else{
                        var branch_name =$scope.detail.branch_name;
                        $scope.getBranches($scope.detail.bank_id);
                        $scope.detail.bank_id = $scope.detail.bank_id +"";
                        if( $scope.detail.branch_name == null) {
                            $scope.detail.branch_name = "";
                        }else{
                            $scope.detail.branch_name = branch_name +"";
                        }

                    }

                    $scope.save = function (data) {
                        $rootScope.clearToastr();
                        $scope.error_frm=false;
                        $scope.msg3='';

                        cases.checkAccount({'id':data.bank_id,'account_number':data.account_number},function(response){

                            if(response.status != true && response.person_id != $rootScope.person_id){
                                $rootScope.toastrMessages('error',$filter ('translate') ('this account previously in use of other person'));
                            }
                            else{
                                angular.forEach($scope.Banks, function(val,key) {
                                    if(val.id == data.bank_id){
                                        data.bank_name =val.name;
                                        return;
                                    }
                                });
                                angular.forEach($scope.Branches, function(val,key) {
                                    if(val.id == data.branch_name){
                                        data.branch =val.name;
                                        return;
                                    }
                                });

                                if($rootScope.persons.banks.length ==1){
                                    data.check=true;
                                }

                                $rootScope.persons.banks[index]=data;
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                $rootScope.no_banks='true';
                                $rootScope.status1 ='failed_valid';
                                $rootScope.msg1.banks=[];
                                $modalInstance.close();
                            }

                        });


                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }


            });
        };
        $scope.delete = function (index) {
            $rootScope.clearToastr();
            $rootScope.persons.banks.splice(index,1);
            if($rootScope.persons.banks.length==0){
                $rootScope.no_banks='false';
            }
            $rootScope.toastrMessages('success',$filter('translate')('The row is deleted from db'));

        };
        $scope.setOrgDefault  = function(status,index,item){
            var l=-1;
            var bk=-1;
            angular.forEach($rootScope.persons.banks, function(val,key) {
                if(val.check){
                    l=key;
                    bk=val;
                }
            });
            if(bk != -1){
                if( l!=index ){
                    $rootScope.persons.banks[l].check=false;
                    $rootScope.persons.banks[index].check=true;
                    $rootScope.default_bank=item.bank_id;
                }
            }else{
                $rootScope.persons.banks[index].check=true;
                $rootScope.default_bank=item.bank_id;
            }


        };
        $scope.checkBirthday  = function(birthday){
            if( !angular.isUndefined($rootScope.msg1.birthday)) {
                $rootScope.msg1.birthday = [];
            }

            var curDate = new Date();
            if(new Date(birthday) > curDate){
                $rootScope.msg1.birthday = {0 :$filter('translate')('the enter date must be less than current date')};
                $rootScope.status1 ='failed_valid';
            }

        };
        $scope.toggleValue  = function(toggle,value){


            if(toggle =='study'){
                if (!angular.isUndefined($rootScope.msg1.study)) {
                    $rootScope.msg1.study = [];
                }

                if (value != 1) {
                    if (!angular.isUndefined($rootScope.msg1.authority)) { $rootScope.msg1.authority = [];  }
                    if (!angular.isUndefined($rootScope.msg1.type))   { $rootScope.msg1.type   = [];  }
                    if (!angular.isUndefined($rootScope.msg1.grade))  { $rootScope.msg1.grade  = [];  }
                    if (!angular.isUndefined($rootScope.msg1.stage))  { $rootScope.msg1.stage  = [];  }
                    if (!angular.isUndefined($rootScope.msg1.level))  { $rootScope.msg1.level  = [];  }
                    if (!angular.isUndefined($rootScope.msg1.school)) { $rootScope.msg1.school = [];  }
                    if (!angular.isUndefined($rootScope.msg1.year))   { $rootScope.msg1.year   = [];  }
                    if (!angular.isUndefined($rootScope.msg1.points)) { $rootScope.msg1.points = [];  }

                    $rootScope.persons.type="";
                    $rootScope.persons.authority="";
                    $rootScope.persons.grade="";
                    $rootScope.persons.stage="";
                    $rootScope.persons.level="";
                    $rootScope.persons.school="";
                    $rootScope.persons.year="";
                    $rootScope.persons.points="";
                }
            }
            else if(toggle =='prayer'){
                if (!angular.isUndefined($rootScope.msg1.prayer)) {
                    $rootScope.msg1.prayer = [];
                }

                if (value != 1) {
                    if (!angular.isUndefined($rootScope.msg1.prayer_reason)) {
                        $rootScope.msg1.prayer_reason = [];
                    }
                    $scope.persons.prayer_reason = "";
                }
            }
            else if(toggle=='health_condition'){
                if (!angular.isUndefined($rootScope.msg1.health_condition)) {
                    $rootScope.msg1.health_condition = [];
                }
                if(value ==1){
                    if( !angular.isUndefined($rootScope.msg1.disease_id)) {
                        $rootScope.msg1.disease_id = [];
                    }
                    if( !angular.isUndefined($rootScope.msg1.details)) {
                        $rootScope.msg1.details = [];
                    }

                    $scope.persons.disease_id="";
                    $scope.persons.details="";
                }
            }
            else if(toggle=='save_quran') {

                if (!angular.isUndefined($rootScope.msg1.save_quran)) {
                    $rootScope.msg1.save_quran = [];
                }

                if (value == 1){
                    if (!angular.isUndefined($rootScope.msg1.quran_parts)) {
                        $rootScope.msg1.quran_parts = [];
                    }
                    if (!angular.isUndefined($rootScope.msg1.quran_chapters )) {
                        $rootScope.msg1.quran_chapters = [];
                    }

                    $scope.persons.quran_parts = "";
                    $scope.persons.quran_chapters  = "";

                }
            }
            else if(toggle=='quran_center') {

                if (!angular.isUndefined($rootScope.msg1.quran_center)) {
                    $rootScope.msg1.quran_center = [];
                }

                if (value == 1) {
                    if (!angular.isUndefined($rootScope.msg1.quran_reason)) {
                        $rootScope.msg1.quran_reason = [];
                    }
                    $scope.persons.quran_reason = "";
                }

            }

        };
        $scope.getLocation = function(entity,parent,value){
            if(!angular.isUndefined(value)) {
                if (value !== null && value !== "" && value !== " ") {
                    Entity.listChildren({'entity': entity, 'id': value, 'parent': parent}, function (response) {

                        if (entity == 'districts') {
                            if (!angular.isUndefined($rootScope.msg1.country)) {
                                $rootScope.msg1.country = [];
                            }

                            $scope.governarate = response;
                            $scope.city = [];
                            $scope.nearlocation = [];
                            $scope.mosques = [];
                            $rootScope.persons.governarate = "";
                            $rootScope.persons.city = "";
                            $rootScope.persons.location_id = "";
                            $rootScope.persons.mosques_id = "";

                        }
                        else if (entity == 'cities') {
                            if (!angular.isUndefined($rootScope.msg1.governarate)) {
                                $rootScope.msg1.governarate = [];
                            }

                            $scope.city = response;
                            $scope.nearlocation = [];
                            $scope.mosques = [];
                            $rootScope.persons.city = "";
                            $rootScope.persons.location_id = "";
                            $rootScope.persons.mosques_id = "";
                        }
                        else if (entity == 'neighborhoods') {
                            if (!angular.isUndefined($rootScope.msg1.city)) {
                                $rootScope.msg1.city = [];
                            }
                            $scope.nearlocation = response;
                            $scope.mosques = [];
                            $rootScope.persons.location_id = "";
                            $rootScope.persons.mosques_id = "";

                        } else if(entity =='mosques'){
                            if( !angular.isUndefined($rootScope.msg1.location_id)) {
                                $rootScope.msg1.location_id = [];
                            }

                            $scope.mosques = response;
                            $rootScope.persons.mosques_id="";
                        }
                    });
                }
            }
        };

        $scope.showImage=function(file){
            $rootScope.clearToastr();
            $rootScope.target=file;

            $uibModal.open({
                templateUrl: 'showImage.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance) {

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                },
                resolve: {

                }
            });
        };

        $scope.ChooseAttachment=function(item,action){

            $rootScope.clearToastr();

        $uibModal.open({
            templateUrl: 'myModalContent2.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance, $log,cases,OAuthToken, FileUploader) {

                $scope.fileupload = function () {
                    if($scope.uploader.queue.length){
                        var image = $scope.uploader.queue.slice(1);
                        $scope.uploader.queue = image;
                    }
                };
                var uploader = $scope.uploader = new FileUploader({
                    url: '/api/v1.0/doc/files',
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });
                uploader.onBeforeUploadItem = function(item) {
                    $rootScope.clearToastr();
                };
                uploader.onCompleteItem = function(fileItem, response, status, headers) {
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.uploader.clearQueue();
                    angular.element("input[type='file']").val(null);
                    if(response.status=='error')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else{
                            fileItem.id = response.id;
                            cases.setAttachment({id:item.person_id,type:'sponsorships',action:action,document_type_id:item.document_type_id,document_id:fileItem.id},function (response1) {
                            if(response1.status=='error' || response1.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response1.msg);
                            }
                            else if(response1.status== 'success'){

                                $modalInstance.close();
                                $rootScope.toastrMessages('success',response1.msg);
                                loadPageContents();

                            }
                    }, function(error) {
                                $modalInstance.close();
                                $rootScope.toastrMessages('error',error.data.msg);
                    });
                }};

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }
        });
    };

    $rootScope.dateOptions = {
            formatYear: 'yy',
            startingDay: 0
        };
        $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $rootScope.format = $rootScope.formats[0];
        $rootScope.today = function() {
            $rootScope.dt = new Date();
        };
        $rootScope.today();
        $rootScope.clear = function() {
            $rootScope.dt = null;
        };
        $rootScope.open1 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $rootScope.popup1.opened = true;
        };
        $rootScope.popup1 = {
            opened: false
        };

    });