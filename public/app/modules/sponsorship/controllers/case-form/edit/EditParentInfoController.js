
angular.module('SponsorshipModule')
    .controller('EditParentInfoController', function (Entity,$scope,$rootScope,$state,$stateParams,$http,$filter,$uibModal,casesInfo,mothers,fathers,category) {

        $rootScope.error=false;
        $rootScope.success_1=false;
        $rootScope.success_2=false;
        $rootScope.success=false;
        $rootScope.father_dead=false;
        $rootScope.msg='';
        $scope.msg1={};
        $rootScope.msg2={};
        $rootScope.id=null;
        $scope.mother={};
        $scope.father={};
        $scope.mpriority=$scope.fpriority=$scope.father=$scope.mother={};
        $rootScope.category_id=null;
        $rootScope.id=null;
        $rootScope.id   = $stateParams.id;

        var setFather=function(item){
            $scope.father= item;
             if($scope.father.death_cause_id !=null){
                    $scope.father.death_cause_id = $scope.father.death_cause_id+"";
                }

                if($scope.father.degree==null){
                    $scope.father.degree="";
                }
                else{
                    $scope.father.degree = $scope.father.degree+"";
                }

                if($scope.father.working == null ){
                    $scope.father.working="";
                    $scope.father.work_job_id="";
                    $scope.father.work_location="";
                }else{
                    $scope.father.working=$scope.father.working+"";
                    if($scope.father.working == 1){
                        $scope.father.work_job_id = $scope.father.work_job_id+"";
                    }else{
                        $scope.father.work_location="";
                    }
                }

                if($scope.father.condition == null ){
                    $scope.father.condition="";
                    $scope.father.disease_id="";
                    $scope.father.details="";
                }else{
                    $scope.father.condition=$scope.father.condition+"";
                    if($scope.father.condition == 2 || $scope.father.condition == 3 ||
                        $scope.father.condition === '2' || $scope.father.condition === '3' ){
                        if( $rootScope.father.disease_id==null){
                            $rootScope.father.disease_id="";
                        }
                        else{
                            $scope.father.disease_id = $scope.father.disease_id+"";
                        }
                    }else{
                        $scope.father.disease_id="";
                        $scope.father.details="";
                    }
                }
            };

        setMother=function(item){
            $scope.mother=item;
             if($scope.mother.death_date == null){
                    $scope.mother.alive = 0+"";
                    $scope.mother.death_cause_id ="";
                    $scope.mother.death_date="";
                }else{
                    $scope.mother.alive =1+"";
                    $scope.mother.death_cause_id = $scope.mother.death_cause_id+"";
                }

                if($scope.mother.monthly_income==null){
                    $scope.mother.monthly_income = "";
                }


                if($scope.mother.stage==null){
                    $scope.mother.stage="";
                }
                else{
                    $scope.mother.stage = $scope.mother.stage+"";
                }


                $scope.mother.working = $scope.mother.working+"";
                if($scope.mother.working == 1){
                    if( $scope.mother.work_job_id==null){
                        $scope.mother.work_job_id="";
                    }
                    else{
                        $scope.mother.work_job_id = $scope.mother.work_job_id+"";
                    }
                }else{
                    $scope.mother.work_job_id="";
                    $scope.mother.work_location="";
                }



                if($scope.mother.marital_status_id==null){
                    $scope.mother.marital_status_id="";
                }
                else{
                    $scope.mother.marital_status_id = $scope.mother.marital_status_id+"";
                }

                if($scope.mother.condition == null ){
                    $scope.mother.condition="";
                    $scope.mother.disease_id="";
                    $scope.mother.details="";
                }else{
                    $scope.mother.condition=$scope.mother.condition+"";
                    if($scope.mother.condition == 2 || $scope.mother.condition == 3 ||
                        $scope.mother.condition === '2' || $scope.mother.condition === '3' ){

                        if( $scope.mother.disease_id==null){
                            $scope.mother.disease_id="";
                        }
                        else{
                            $scope.mother.disease_id = $scope.mother.disease_id+"";
                        }
                    }else{
                        $scope.mother.disease_id="";
                        $scope.mother.details="";
                    }
                }
            


        };

        if( angular.isUndefined($rootScope.id) ||$rootScope.id !=  null&& $rootScope.id !=  'null'){
            casesInfo.loadCase({id:$rootScope.id,target:'parent',mode:'edit'},function(response){

                $rootScope.fatherId=response.father_id;
                $rootScope.motherId=response.mother_id;

                category.getPriority({'type':'sponsorships','category_id':response.category_id ,'target':'father'},function (fResponse) {
                    $scope.fpriority=fResponse.priority;
                });
                category.getPriority({'type':'sponsorships','category_id':response.category_id ,'target':'mother'},function (mResponse) {
                    $scope.mpriority=mResponse.priority;
                });

                $rootScope.category_id =response.category_id;

                if(response.category.father_dead ==0){
                    $rootScope.father_dead=false;
                }else {
                    $rootScope.father_dead=true;
                }
                setFather(response.father);
                setMother(response.mother);

            });
        }else{
            $state.go('sponsorshipCases');
        }

// -----------------------------------------------------------------------------------------------------
        Entity.get({'entity':'entities' , 'c':'educationDegrees,educationStages,workJobs,diseases,maritalStatus,deathCauses,countries'},function (response){
            $rootScope.EduDegrees = response.educationDegrees;
            $rootScope.EduStages = response.educationStages;
            $rootScope.WorkJob = response.workJobs;
            $rootScope.Diseases = response.diseases;
            $rootScope.MaritalStatus = response.maritalStatus;
            $rootScope.DeathCauses = response.deathCauses;
            $rootScope.Country = response.countries;
        });
// -----------------------------------------------------------------------------------------------------
        $scope.save=function(action,data){
            $rootScope.clearToastr();
            if( !angular.isUndefined(data.death_date)) {
                data.death_date=$filter('date')(data.death_date, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(data.birthday)) {
                data.birthday= $filter('date')(data.birthday, 'dd-MM-yyyy')
            }
            // if(action =='father'){
            //     if($rootScope.father_dead == true){
            //         data.live=1;
            //     }else{
            //         data.live=0;
            //     }
            //     data.action='JustUpdate';
            //     data.priority=$scope.fpriority;
            //     var item = new fathers(data);
            //     if(data.id && data.id!=""){
            //         item.$update().
            //         then(function (response) {
            //             if(response.status=='failed_valid')
            //             {
            // $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
            //                 $scope.status1 =response.status;
            //                 $scope.msg1 =response.msg;
            //             } else if(response.status== 'success'){
            //                 $rootScope.success_1=true;
            //             }
            //
            //
            //         }, function (error) {
            //                  $rootScope.toastrMessages('error',error.data.msg);
            //       });
            //
            //     }
            //
            // }
            // else{
            //     data.action='JustUpdate';
            //     data.priority=$scope.mpriority;
            //     data.husband=$rootScope.father_id;
            //     data.target='single';
            //     var item = new mothers(data);
            //     if(data.id){
            //         item.$update().
            //         then(function (response) {
            //             if(response.status=='failed_valid')
            //             {
            // $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
//                 $scope.status1 =response.status;
            //                 $scope.msg2 =response.msg;
            //             } else if(response.status== 'success'){
            //                 $rootScope.success_2=true;
            //             }
            //
            //
            //         }, function (error) {
            // $rootScope.toastrMessages('error',error.data.msg);

            //         });
            //
            //     }
            //
            // }
        };
// -----------------------------------------------------------------------------------------------------
        $scope.checkBirthday  = function(target,birthday){
            if( !angular.isUndefined($scope.msg1.birthday)) {
                $scope.msg1.birthday = [];
            }

            if(target =='father'){
                var death_date=$scope.father.death_date
            }else{
                var death_date=$scope.mother.death_date;
            }
            if( !angular.isUndefined(death_date)) {
                if(new Date(birthday) > new Date(death_date)){
                    if(target =='father'){
                        $scope.msg1.birthday = {0 :$filter('translate')('The date of birth must be older than the date of death')};
                    }else{
                        $rootScope.msg2.birthday = {0 :$filter('translate')('The date of birth must be older than the date of death')};
                    }
                    $scope.status1 ='failed_valid';
                }else{
                    if(target =='father'){
                        $scope.msg1.birthday = {};
                    }else{
                        $rootScope.msg2.birthday = {};
                    }
                }
            }else{
                var curDate = new Date();
                if(new Date(birthday) > curDate){
                    if(target =='father'){
                        $scope.msg1.birthday = {0 :$filter('translate')('The specified date must be older than the current date')};
                    }else{
                        $rootScope.msg2.birthday = {0 :$filter('translate')('The specified date must be older than the current date')};
                    }
                    $scope.status1 ='failed_valid';
                }else{
                    if(target =='father'){
                        $scope.msg1.birthday = {};
                    }else{
                        $rootScope.msg2.birthday = {};
                    }
                }

            }

        };
        $scope.checkDeathDate  = function(birthday,DeathDate){
            if( !angular.isUndefined($scope.msg1.death_date)) {
                $scope.msg1.death_date = [];
            }

            if( !angular.isUndefined(birthday)) {
                if(new Date(birthday) > new Date(DeathDate)){
                    $scope.msg1.death_date = {0 :$filter('translate')('The date of birth must be older than the date of death')};
                    $scope.status1 ='failed_valid';
                }else{
                    $scope.msg1.death_date = {};
                }

            }else{
                var curDate = new Date();

                if(curDate < new Date(DeathDate)){
                    $scope.msg1.death_date = {0 : $filter('translate')('The date of death must be older than current date')};
                    $scope.status1 ='failed_valid';
                }else{
                    $scope.msg1.death_date = {};
                }

            }
        };
        $scope.restValue = function(i,value){

            if(i==1) {
                if( !angular.isUndefined($rootScope.msg2.working)) {
                    $rootScope.msg2.working = [];
                }
                if( !angular.isUndefined($rootScope.msg2.work_job_id)) {
                    $rootScope.msg2.work_job_id = [];
                }

                if( !angular.isUndefined($rootScope.msg2.monthly_income)) {
                    $rootScope.msg2.monthly_income = [];
                }

                if( !angular.isUndefined($rootScope.msg2.work_location)) {
                    $rootScope.msg2.work_location = [];
                }

                $scope.mother.work_job_id="";
                $scope.mother.monthly_income="";
                $scope.mother.work_location="";
            }
            else if(i==2) {

                if (!angular.isUndefined($rootScope.msg2.condition)) {
                    $rootScope.msg2.condition = [];
                }

                if (value != 2 || value != 3) {
                    if (!angular.isUndefined($rootScope.msg2.disease_id)) {
                        $rootScope.msg2.disease_id = [];
                    }
                    if (!angular.isUndefined($rootScope.msg2.details)) {
                        $rootScope.msg2.details = [];
                    }

                    $scope.mother.disease_id = "";
                    $scope.mother.details = "";
                }

            }
            else if(i==3) {

                if (!angular.isUndefined($rootScope.msg2.alive)) {
                    $rootScope.msg2.alive = [];
                }

                if (value == 0) {
                    if (!angular.isUndefined($rootScope.msg2.death_date)) {
                        $rootScope.msg2.death_date = [];
                    }
                    if (!angular.isUndefined($rootScope.msg2.death_cause_id)) {
                        $rootScope.msg2.death_cause_id = [];
                    }

                    $scope.mother.death_date = "";
                    $scope.mother.death_cause_id = "";
                }

            }
            else if(i==4) {

                if( !angular.isUndefined($scope.msg1.working)) {
                    $scope.msg1.working = [];
                }
                //if(value == 2){
                if( !angular.isUndefined($scope.msg1.work_job_id)) {
                    $scope.msg1.work_job_id = [];
                }

                if( !angular.isUndefined($scope.msg1.monthly_income)) {
                    $scope.msg1.monthly_income = [];
                }

                if( !angular.isUndefined($scope.msg1.work_location)) {
                    $scope.msg1.work_location = [];
                }

                $scope.father.work_job_id="";
                $scope.father.monthly_income="";
                $scope.father.work_location="";
                //}

            }
            else if(i==5) {
                if (!angular.isUndefined($scope.msg1.condition)) {
                    $scope.msg1.condition = [];
                }

                if (value != 2 || value != 3) {
                    if (!angular.isUndefined($scope.msg1.disease_id)) {
                        $scope.msg1.disease_id = [];
                    }
                    if (!angular.isUndefined($scope.msg1.details)) {
                        $scope.msg1.details = [];
                    }

                    $scope.father.disease_id = "";
                    $scope.father.details = "";
                }

            }

        };
        $scope.download = function (target) {
            $rootScope.clearToastr();

            var url ='';
            var file_type ='zip';
            var  params={};
            var method='';

            if(target =='word'){
                method='POST';
                params={'case_id':$scope.id,
                    'target':'info',
                    'action':'filters',
                    'mode':'show',
                    'category_type' : 1,
                    'person' :true,
                    'persons_i18n' : true,
                    'category' : true,
                    'work' : true,
                    'contacts' : true,
                    'health' : true,
                    'default_bank' : true,
                    'education' : true,
                    'islamic' : true,
                    'banks' : true,
                    'residence' : true,
                    'persons_documents' : true,
                    'family_member' : true,
                    'father_id' : true,
                    'mother_id' : true,
                    'guardian_id' : true,
                    'guardian_kinship' : true,
                    'parent_detail':true,
                    'guardian_detail':true,
                    'visitor_note':true
                };
                url = "/api/v1.0/common/sponsorships/cases/word";
            }
            else if(target =='pdf'){
                method='POST';
                params={'case_id':$scope.id,
                    'target':'info',
                    'action':'filters',
                    'mode':'show',
                    'category_type' : 1,
                    'person' :true,
                    'persons_i18n' : true,
                    'category' : true,
                    'work' : true,
                    'contacts' : true,
                    'health' : true,
                    'default_bank' : true,
                    'education' : true,
                    'islamic' : true,
                    'banks' : true,
                    'residence' : true,
                    'persons_documents' : true,
                    'family_member' : true,
                    'father_id' : true,
                    'mother_id' : true,
                    'guardian_id' : true,
                    'guardian_kinship' : true,
                    'parent_detail':true,
                    'guardian_detail':true,
                    'visitor_note':true
                };
                url = "/api/v1.0/common/sponsorships/cases/pdf";
            }
            else{
                method='GET';
                url = "/api/v1.0/common/sponsorships/cases/exportCaseDocument/"+id;
            }

            $http({
                url:url,
                method: method,
                data:params
            }).then(function (response) {

                if(target !='word' && response.data.status == false){
                    $rootScope.toastrMessages('error',$filter('translate')('no attachment to export'));
                }else{
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }

            }, function(error) {
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };
// -----------------------------------------------------------------------------------------------------
        $rootScope.close=function(){
            
            $rootScope.failed =false;
            ;
            $rootScope.model_error_status =false;
        };
        $rootScope.dateOptions = {
            formatYear: 'yy',
            startingDay: 0
        };
        $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $rootScope.format = $rootScope.formats[0];
        $rootScope.today = function() {
            $rootScope.dt = new Date();
        };
        $rootScope.today();
        $rootScope.clear = function() {
            $rootScope.dt = null;
        };
        $rootScope.open1 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $rootScope.popup1.opened = true;
        };
        $rootScope.popup1 = {
            opened: false
        };
        $rootScope.open2 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $rootScope.popup2.opened = true;
        };
        $rootScope.popup2 = {
            opened: false
        };
        $rootScope.open3 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $rootScope.popup3.opened = true;
        };
        $rootScope.popup3 = {
            opened: false
        };
        $rootScope.open4 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $rootScope.popup4.opened = true;
        };
        $rootScope.popup4 = {
            opened: false
        };

    });