

angular.module('SponsorshipModule')
    .controller('CategoriesSponsorshipFormController', function( $state,$stateParams,$rootScope, $scope, $http, $timeout,category) {

        $rootScope.Formcat=[];



        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        $scope.GotoForm= function(Formcat){
        $rootScope.clearToastr();
        $rootScope.mode=$stateParams.mode;
        $rootScope.child1_id   = $stateParams.child1_id;
        $rootScope.guardian_id = $stateParams.guardian_id;
        $rootScope.father_id   = $stateParams.father_id;
        $rootScope.l_person_id = $stateParams.person_id;
        $rootScope.person_id   = $stateParams.person_id;
        $rootScope.case_id     = $stateParams.case_id;
        $rootScope.mother_id   = $stateParams.mother_id;
        $rootScope.brother_id  = $stateParams.brother_id;
        $rootScope.category_id=Formcat.category_id;

        $rootScope.fatherDataStep=false;
        $rootScope.motherDataStep=false;
        $rootScope.childDataStep=false;
        $rootScope.providerDataStep=false;
        $rootScope.customFormDataStep=false;
        $rootScope.caseSponsorshipListStep=false;
        $rootScope.childFamilyDataStep=false;
        $rootScope.caseDocumentsStep=false;



        category.getCategoryFormsSections({'id':$rootScope.category_id ,'mode':$rootScope.mode,'type':'sponsorships'})
            .then(function (response){
                    $rootScope.FormSponsorshipSection=response.FormSection;

                    if($rootScope.FormSponsorshipSection.length !=0){
                        $rootScope.TotalOfFormStep=$rootScope.FormSponsorshipSection.length;
                        angular.forEach($rootScope.FormSponsorshipSection, function(v, k) {
                            if(v.id == 'fatherData'){
                                $rootScope.fatherDataStep=true;
                                $rootScope.fatherDataStepNumber=k+1;
                            }else if(v.id == 'motherData'){
                                $rootScope.motherDataStep=true;
                                $rootScope.motherDataStepNumber=k+1;
                            }else if(v.id == 'childData'){
                                $rootScope.childDataStep=true;
                                $rootScope.childDataStepNumber=k+1;
                            }else if(v.id == 'providerData'){
                                $rootScope.providerDataStep=true;
                                $rootScope.providerDataStepNumber=k+1;
                            } else if(v.id == 'customFormData'){
                                $rootScope.customFormDataStep=true;
                                $rootScope.customFormSponStepNumber=k+1;
                            }else if(v.id == 'caseSponsorshipList'){
                                $rootScope.caseSponsorshipListStep=true;
                                $rootScope.caseSponsorshipListStepNumber=k+1;
                            }else if(v.id == 'childFamilyData'){
                                $rootScope.childFamilyDataStep=true;
                                $rootScope.childFamilyDataNumber=k+1;
                            }else if(v.id == 'caseDocuments'){
                                $rootScope.caseDocumentsStep=true;
                                $rootScope.caseDocumentsStepNumber=k+1;
                            }
                        });
                        $rootScope.getTheTarget($rootScope.FormSponsorshipSection[0].id);
                    }
                }
                , function(error) {
                    $rootScope.toastrMessages('error',error.data.msg);
                });

        target.$getCategoryName({'id':$rootScope.category_id ,'mode':$rootScope.mode})
            .then(function (response2){
                    $rootScope.CategoryName=response2.CategoryName;
                }
                , function(error) {
                    $rootScope.toastrMessages('error',error.data.msg);
                });



    };


//-----------------------------------------------------------------------------------------------------
        $rootScope.getTheTarget= function(target) {
            if(target == 'fatherData'){
                $rootScope.go1();
            }else if(target == 'motherData'){
                $rootScope.go2();
            }else if(target == 'childData'){
                $rootScope.go3();
            }else if(target == 'providerData'){
                $rootScope.go4();
            } else if(target == 'customFormData'){
                $rootScope.gotoSponsorCustomForm();
            }else if(target == 'caseSponsorshipList'){
                $rootScope.go7();
            }else if(target == 'childFamilyData'){
                $rootScope.go5();
            }else if(target == 'caseDocuments'){
                $rootScope.go6();
            }

        };
//-----------------------------------------------------------------------------------------------------

    });
