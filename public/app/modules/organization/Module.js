

var OrganizationModule = angular.module("OrganizationModule", []);

OrganizationModule.config(function ($stateProvider) {
    $stateProvider
        .state('org-organizations', {
            url: '/organizations/:type',
            templateUrl: '/app/modules/organization/views/organizations/index.html',
            data: {pageTitle: ''},
            params:{type: null,action:null,status:null,level:null},
            controller: "OrganizationsController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'OrganizationsController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                '/app/modules/sms/services/SmsService.js',
                                '/app/modules/organization/controllers/OrganizationsController.js',
                                '/app/modules/organization/services/OrgService.js',
                                '/app/modules/setting/services/EntityService.js',
                                '/app/modules/setting/services/EntityService.js'
                            ]
                        });
                    }]
            }
        })
        .state('org-organizations-trashed', {
            url: '/organizations/:type/:level',
            templateUrl: '/app/modules/organization/views/organizations/trash.html',
            data: {pageTitle: ''},
            params:{type: null,action:null,status:null,level:null},
            controller: "OrganizationsTrashedController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'OrganizationsTrashedController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                '/app/modules/organization/controllers/OrganizationsController.js',
                                '/app/modules/organization/services/OrgService.js',
                                '/app/modules/setting/services/EntityService.js'
                            ]
                        });
                    }]
            }
        })
        .state('org-organizations-edit', {
            url: '/organizations/:type/:action/:id',
            templateUrl: '/app/modules/organization/views/organizations/edit.html',
            data: {pageTitle: ''},
            controller: "OrganizationsEditController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'OrganizationsEditController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                '/app/modules/organization/controllers/OrganizationsEditController.js',
                                '/app/modules/organization/services/OrgService.js',
                                '/app/modules/document/directives/thumb.js',
                                '/app/modules/document/directives/fileDownload.js',
                                '/app/modules/setting/services/EntityService.js'
                            ]
                        });
                    }]
            }
        })
        .state('org-organizations-sponsors', {
            url: '/organization/sponsors/:id',
            templateUrl: '/app/modules/organization/views/organizations/organization-sponsors.html',
            data: {pageTitle: ''},
            controller: "OrganizationsSponsorsController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'OrganizationsSponsorsController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/app/modules/organization/controllers/OrganizationsSponsorsController.js',
                                '/app/modules/organization/services/OrgService.js'
                            ]
                        });
                    }]
            }
        })
        .state('org-organizations-locations', {
        url: '/organization/locations/:id',
        templateUrl: '/app/modules/organization/views/organizations/organization-locations.html',
            data: {pageTitle: ''},
            controller: "OrganizationsLocationsController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'OrganizationsLocationsController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/themes/metronic/global/plugins/jstree/dist/themes/default/style.min.css',
                                '/themes/metronic/global/plugins/jstree/dist/jstree.min.js',
                                '/themes/metronic/global/plugins/angularjs/plugins/ngJsTree/ngJsTree.min.js',
                                '/app/modules/organization/controllers/OrganizationsLocationsController.js',
                                '/app/modules/organization/services/OrgService.js'
                            ]
                        });
                    }]
            }
        })
        .state('sponsor-templates', {
                url: '/sponsors/sponsor-templates/:id',
               templateUrl: '/app/modules/organization/views/organizations/sponsor-template.html',
               data: {pageTitle: ''},
                params:{id:null},
                controller: "SponsorTemplateController",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: [
                                '/app/modules/organization/controllers/SponsorTemplateController.js',
                                '/app/modules/common/services/CategoriesService.js',
                                '/app/modules/organization/services/OrgService.js',
                                '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                '/app/modules/document/directives/fileDownload.js',
                                '/app/modules/setting/services/SettingsService.js'

                            ]
                        });
                    }]
                }

            })
        .state('sponsor-reports', {
                url: '/sponsors/sponsor-reports/:id',
               templateUrl: '/app/modules/organization/views/organizations/sponsor-reports.html',
               data: {pageTitle: ''},
                params:{id:null,type:null},
                controller: "SponsorReportsController",
                resolve: {
                    deps: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            files: [
                                '/app/modules/organization/controllers/SponsorReportsController.js',
                                '/app/modules/common/services/CategoriesService.js',
                                '/app/modules/organization/services/OrgService.js',
                                '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                '/app/modules/document/directives/fileDownload.js',
                                '/app/modules/setting/services/SettingsService.js'

                            ]
                        });
                    }]
                }

            })
        .state('organizations-search', {
            url: '/organizations-search',
            templateUrl: '/app/modules/organization/views/organizations/organizations-search.html',
            data: {pageTitle: ''},
            params:{},
            controller: "OrganizationsSearchController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'OrganizationsController',
                        files: [
                            '/app/modules/organization/controllers/OrganizationsSearchController.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/EntityService.js'
                        ]
                    });
                }]
            }
        })
        .state('org-projects', {
            url: '/organizations-projects',
            templateUrl: '/app/modules/organization/views/projects/index.html',
            data: {pageTitle: ''},
            params:{id:null,success_msg:null},
            controller: "organizationProjectsController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/organization/controllers/organizationProjectsController.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/app/modules/aid/services/VouchersCategoriesService.js',
                            '/app/modules/setting/services/SettingsService.js'
                        ]
                    });
                }]
            }

        })
        .state('trashed-org-projects', {
            url: '/trashed-organizations-projects',
            templateUrl: '/app/modules/organization/views/projects/trashed.html',
            data: {pageTitle: ''},
            params:{id:null,success_msg:null},
            controller: "trashedOrganizationProjectsController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/organization/controllers/organizationProjectsController.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/app/modules/aid/services/VouchersCategoriesService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/setting/services/SettingsService.js'
                        ]
                    });
                }]
            }

        })
        .state('org-project-form', {
            url: '/organizations-project/:id',
            templateUrl: '/app/modules/organization/views/projects/form.html',
            data: {pageTitle: ''},
            params:{id:null,msg:null},
            controller: "organizationProjectFormController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/organization/controllers/organizationProjectsController.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/setting/services/SettingsService.js'
                        ]
                    });
                }]
            }

        })
        .state('org-project-show', {
            url: '/organizations-project/show/:id',
            templateUrl: '/app/modules/organization/views/projects/show.html',
            data: {pageTitle: ''},
            params:{id:null,msg:null},
            controller: "organizationProjectShowFormController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/organization/controllers/organizationProjectsController.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/setting/services/SettingsService.js'
                        ]
                    });
                }]
            }

        })
        .state('org-project-attachments', {
            url: '/organizations-project/attachments/:id',
            templateUrl: '/app/modules/organization/views/projects/attachments.html',
            data: {pageTitle: ''},
            params:{id:null,success_msg:null,msg:null},
            controller: "organizationProjectAttachmentController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/organization/controllers/organizationProjectsController.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/setting/services/SettingsService.js'
                        ]
                    });
                }]
            }

        })

        // ******************************************************************* //
        .state('administrative-report', {
            url: '/administrative-reports',
            templateUrl: '/app/modules/organization/views/reports/reports.html',
            controller: "AdministrativeReportController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'FormsModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/organization/services/ReportService.js',
                            '/app/modules/organization/controllers/report/AdministrativeReportController.js'
                        ]
                    });
                }]
            }

        })
        .state('administrative-report-tab', {
            url: '/administrative-report/tabs/:id',
            templateUrl: '/app/modules/organization/views/reports/tab.html',
            params:{id: null,mode:null,status:null,element:null},
            controller: "TabController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'FormsModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/organization/services/ReportService.js',
                            '/app/modules/organization/controllers/report/AdministrativeReportController.js'
                        ]
                    });
                }]
            }

        })
        .state('administrative-report-show', {
            url: '/administrative-report/:id',
            templateUrl: '/app/modules/organization/views/reports/show.html',
            data: {pageTitle: ''},
            controller: "ShowReportController",
            params:{id: null},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'FormsModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/organization/services/ReportService.js',
                            '/app/modules/organization/controllers/report/AdministrativeReportController.js'
                        ]
                    });
                }]
            }

        })
        .state('custom-administrative-report-show', {
            url: '/custom-administrative-report/:id',
            templateUrl: '/app/modules/organization/views/reports/show-custom.html',
            data: {pageTitle: ''},
            controller: "ShowReportController",
            params:{id: null},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'FormsModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/organization/services/ReportService.js',
                            '/app/modules/organization/controllers/report/AdministrativeReportController.js'
                        ]
                    });
                }]
            }

        })
        .state('administrative-report-show-organizations', {
            url: '/administrative-report/organizations/:id',
            templateUrl: '/app/modules/organization/views/reports/report-organizations.html',
            data: {pageTitle: ''},
            controller: "ReportOrgController",
            params:{id: null},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'FormsModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/organization/services/ReportService.js',
                            '/app/modules/organization/controllers/report/AdministrativeReportController.js'
                        ]
                    });
                }]
            }
        })
        .state('administrative-report-show-organization-data', {
            url: '/administrative-report/organizations/:id/:organization_id',
            templateUrl: '/app/modules/organization/views/reports/organization-form.html',
            data: {pageTitle: ''},
            controller: "ShowOrgReportController",
            params:{id: null},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'FormsModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/organization/services/ReportService.js',
                            '/app/modules/organization/controllers/report/AdministrativeReportController.js'
                        ]
                    });
                }]
            }
        })

        // ******************************************************************* //
        .state('org-administrative-report', {
            url: '/organization/administrative-reports',
            templateUrl: '/app/modules/organization/views/reports/organization/index.html',
            controller: "OrgReportController",
            data: {pageTitle: ''},
            params:{msg:null},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'FormsModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/organization/services/ReportService.js',
                            '/app/modules/organization/controllers/report/OrgReportController.js'
                        ]
                    });
                }]
            }

        })
        .state('org-administrative-report-show', {
            url: '/organization/administrative-reports/:id',
            templateUrl: '/app/modules/organization/views/reports/organization/form.html',
            data: {pageTitle: ''},
            controller: "OrgReportFormController",
            params:{id: null},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'FormsModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/organization/services/ReportService.js',
                            '/app/modules/organization/controllers/report/OrgReportController.js'
                        ]
                    });
                }]
            }

        })
        .state('org-administrative-custom-report-show', {
            url: '/organization/custom-administrative-reports/:id',
            templateUrl: '/app/modules/organization/views/reports/organization/custom-form.html',
            data: {pageTitle: ''},
            controller: "OrgReportFormController",
            params:{id: null},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'FormsModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/organization/services/ReportService.js',
                            '/app/modules/organization/controllers/report/OrgReportController.js'
                        ]
                    });
                }]
            }

        })
        .state('org-administrative-report-show-organizations', {
            url: '/organization/administrative-reports/organizations/:id',
            templateUrl: '/app/modules/organization/views/reports/organization/report-organizations.html',
            data: {pageTitle: ''},
            controller: "OrgReportListController",
            params:{id: null},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'FormsModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/organization/services/ReportService.js',
                            '/app/modules/organization/controllers/report/OrgReportController.js'
                        ]
                    });
                }]
            }
        })


});

OrganizationModule.directive('imgBlobUrl', ['$http',
    function ($http) {
        return {
            restrict: 'A',
            link: function (scope, elem, attrs) {
                    $http({
                        method: 'GET',
                        url: attrs.url,
                        responseType: 'arraybuffer',
                        cache: false
                    }).then(function successCallback(response) {
                        var blob = new Blob([response.data], {type: response.headers('Content-Type')});
                        url = (window.URL || window.webkitURL).createObjectURL(blob);
                        scope.logo = url;
                    }, function errorCallback(response) {
                    });
            }
        };
    }]);
