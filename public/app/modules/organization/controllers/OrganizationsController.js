OrganizationModule = angular.module('OrganizationModule');

OrganizationModule.controller('OrganizationsController', function ($filter,$state, $stateParams, $rootScope, $scope, $uibModal, Org, Entity,$ngBootbox,org_sms_service ) {

    var AuthUser =localStorage.getItem("charity_LoggedInUser");
    $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
    $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
    if($rootScope.charity_LoggedInUser.organization != null) {
        $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
    }
    $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;

    $rootScope.Organizations=[];
    $rootScope.action = $stateParams.action;
    $rootScope.level = $stateParams.level;

    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;
        $scope.reverse = !$scope.reverse;
    };

    $scope.search_form = false;
    $scope.simple_form = true;
    $scope.name ='';
    $scope.target = {action:'paginate',items:[]};
    $scope.CurrentPage = 1;
    $scope.itemsCount = 50;
    
    $scope.location= function(entity,value,parent){
            if($stateParams.type =='organizations'){
                if(value != null && value != "" && value != " " ) {
                    Entity.listChildren({entity: entity, 'id': value, 'parent': parent}, function (response) {

                        if (entity == 'districts') {
                            $scope.districts = response;
                            $scope.target.district_id=undefined;
                            $scope.target.city_id=undefined;
                            $scope.target.location_id=undefined;
                            $scope.city = [];
                            $scope.nearlocation =[];
                        }
                        if (entity == 'cities') {
                            $scope.city = response;
                            $scope.target.city_id=undefined;
                            $scope.target.location_id=undefined;
                            $scope.nearlocation =[];
                        }
                        if (entity == 'neighborhoods') {
                            $scope.nearlocation = response;
                            $scope.target.location_id=undefined;
                        }
                    });
                }
            }
        };

    $rootScope.orgType =$rootScope.orgType = $stateParams.type == 'sponsors'? 'sponsors' : 'organizations';

    $state.current.data.pageTitle = $stateParams.type == 'sponsors'? $filter('translate')('all sponsor') :
                                                             
                                                             
    Org.query({type: $scope.orgType}, function (response) {
            $scope.Organizations_ = response.data;
    });

    Entity.list({entity:'banks'},function (response) {
        $scope.Banks = response;
    });

    Entity.list({entity:'organizations-category'},function (response) {
            $rootScope.categoriesList = response;
    });
    
    var LoadOrgs=function(params){
        var parameters = angular.copy(params);

        parameters.type = $rootScope.orgType ;
        parameters.page = $scope.CurrentPage;
        parameters.itemsCount = $scope.itemsCount;
        
        if( !angular.isUndefined(parameters.created_at_from)) {
            parameters.created_at_from=$filter('date')(parameters.created_at_from, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(parameters.created_at_to)) {
            parameters.created_at_to=$filter('date')(parameters.created_at_to, 'dd-MM-yyyy')
        }
            
        $rootScope.progressbar_start();
        Org.paginate(parameters, function (response) {
            $rootScope.close();
            $rootScope.progressbar_complete();
            if(parameters.action == 'paginate'){
                $rootScope.Organizations = response.data;
                $scope.CurrentPage = response.current_page;
                $rootScope.TotalItems = response.total;
                $rootScope.ItemsPerPage = response.per_page;   
           }else{
                if(response.download_token){
                   var file_type='xlsx';
                   window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
               }else{
                   $rootScope.toastrMessages('error',$filter('translate')('no records to download'));
               }
           }
           
       });
   };

    $rootScope.search_ = function (name) {
        var parameters = angular.copy($scope.target);
        parameters.name = name;
        LoadOrgs(parameters);
    };

    $rootScope.Search = function (selected , target,action) {
        var parameters = angular.copy(target);
        parameters.action = action
        parameters.items=[];

          if(selected == true || selected == 'true' ){
            
                var items =[];
                 angular.forEach($rootScope.Organizations, function(v, k){
                     if(v.check){
                             items.push(v.id);
                         }
                 });

                 if(items.length==0){
                     $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                     return ;
                 }
                 parameters.items=items;
            }        
        LoadOrgs(parameters);
    };

    $rootScope.refresh_page = function () {
        var parameters = angular.copy($scope.target);
        parameters.action = 'paginate';        
        LoadOrgs(parameters);
    };

    $rootScope.pageChanged = function (CurrentPage) {
        $scope.CurrentPage = CurrentPage;
        $rootScope.refresh_page();    
    };

    $rootScope.itemsPerPage_ = function (itemsCount) {
        $scope.itemsCount = itemsCount;
        $rootScope.refresh_page();
    };

    $scope.toggle = function () {
        $scope.target = {};
        $scope.search_form = !$scope.search_form;
        $scope.simple_form = !$scope.simple_form;
        $scope.name ='';
    };
    
    $scope.resetFilter_ = function () {
        $scope.target = {};
        $scope.target.email='';
        $scope.name ='';
    };
    
    $rootScope.delete= function ( id) {
        $rootScope.clearToastr();
        $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
            .then(function() {
                $rootScope.clearToastr();
                $rootScope.progressbar_start();
                Org.delete({id: id,type: $rootScope.orgType},function (response) {
                    $rootScope.progressbar_complete();
                    if (response.status == 'success') {
                            $rootScope.refresh_page();
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        } else {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
               });
            });
    };

    $scope.selectedAll = function(value){
        if(value){
            $scope.selectAll = true;
        }
        else{
            $scope.selectAll =false;

        }
        angular.forEach($rootScope.Organizations, function(val,key)
        {
            val.check=$scope.selectAll;
        });
    };

    $scope.sendSms=function(type,organization_id) {
        $rootScope.clearToastr();
        if(type == 'manual'){
            var receipt = [];
            var pass = true;

            if(organization_id ==0){
                angular.forEach($rootScope.Organizations, function (v, k) {
                    if (v.check) {
                        receipt.push(v.id);
                    }
                });
                if(receipt.length==0){
                    pass = false;
                    $rootScope.toastrMessages('error',$filter('translate')("you did not select anyone to send them sms message"));
                    return;
                }
            }else{
                receipt.push(organization_id);
            }
            if(pass){
                org_sms_service.getList(function (response) {
                    $rootScope.Providers = response.Service;
                });
                $uibModal.open({
                    templateUrl: '/app/modules/sms/views/modal/send_sms.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance) {

                        $scope.sms={source: 'check' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:"",cont_type:"",org_pers:""};

                        $scope.confirm = function (data) {
                            $rootScope.clearToastr();
                            var params = angular.copy(data);
                            params.target='organization';
                            params.receipt=receipt;
                            $rootScope.progressbar_start();

                            org_sms_service.sendSmsToTarget(params,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed') {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status== 'success'){
                                    if(response.download_token){
                                        var file_type='xlsx';
                                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                    }

                                    $modalInstance.close();
                                    $rootScope.toastrMessages('success',response.msg);
                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                            });
                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    }
                });

            }
        }
        else{
            org_sms_service.getList(function (response) {
                $rootScope.Providers = response.Service;
            });
            $uibModal.open({
                templateUrl: '/app/modules/sms/views/modal/sent_sms_from_xlx.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;
                    $scope.sms={source: 'file' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:""};

                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };

                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };
                    $scope.download=function(){
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-to-send-sms-template&template=true';
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: 'api/v1.0/sms/sent_sms/excelWithMobiles',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });


                    $scope.confirm = function (item) {

                        $rootScope.progressbar_start();
                        $scope.spinner=false;
                        $scope.import=false;

                        Uploader.onBeforeUploadItem = function(item) {

                            if($scope.sms.same_msg == 0){
                                $scope.sms.sms_messege = '';
                            }
                            item.formData = [$scope.sms];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();

                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            else if(response.status=='success')
                            {
                                if(response.download_token){
                                    var file_type='xlsx';
                                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                } 
                                $scope.spinner=false;
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $scope.import=false;
                                $rootScope.toastrMessages('success',response.msg);
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                //$modalInstance.close();
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});

        }
    };

 // *************** DATE PIKER ***************
    $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0};
    $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $rootScope.format = $scope.formats[0];
    $rootScope.today = function() {$rootScope.dt = new Date();};
    $rootScope.today();
    $rootScope.clear = function() {$rootScope.dt = null;};

    $scope.open1 = function($event) { $event.preventDefault();  $event.stopPropagation();$scope.popup1.opened = true;};
    $scope.popup1 = {opened: false};
    $scope.open2 = function($event) {  $event.preventDefault();  $event.stopPropagation();  $scope.popup2.opened = true; };
    $scope.popup2 = {  opened: false };
    
    $rootScope.refresh_page();

});

OrganizationModule.controller('OrganizationsTrashedController', function ($filter,$state, $stateParams, $rootScope, $scope, $uibModal, $ngBootbox,Org, Entity) {

    var AuthUser =localStorage.getItem("charity_LoggedInUser");
    $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
    $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
    if($rootScope.charity_LoggedInUser.organization != null) {
        $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
    }
    $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;

    $rootScope.Organizations=[];
    $rootScope.action = $stateParams.action;
    $rootScope.level = $stateParams.level;
    $scope.name ='';
    $scope.target = {action:'paginate',items:[]};
    $scope.CurrentPage = 1;
    $scope.itemsCount = 50;
    
    
    $scope.sort = function (keyname) {
        $scope.sortKey = keyname;
        $scope.reverse = !$scope.reverse;
    };

    $rootScope.orgType =$rootScope.orgType = $stateParams.type == 'sponsors'? 'sponsors' : 'organizations';

    $state.current.data.pageTitle = $stateParams.type == 'sponsors'? $filter('translate')('all  deleted sponsor') :
                                                                     $filter('translate')('all  deleted organization');
                                             
    Org.query({type: $rootScope.orgType}, function (response) {
            $scope.Organizations_ = response.data;
    });
    
    var LoadTrashedOrgs=function(params){
        var parameters = angular.copy(params);
        parameters.type = $rootScope.orgType ;
        parameters.page = $scope.CurrentPage;
        parameters.itemsCount = $scope.itemsCount;
        
        if( !angular.isUndefined(parameters.created_at_from)) {
            parameters.created_at_from=$filter('date')(parameters.created_at_from, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(parameters.created_at_to)) {
            parameters.created_at_to=$filter('date')(parameters.created_at_to, 'dd-MM-yyyy')
        }
         
        if( !angular.isUndefined(parameters.deleted_at_from)) {
            parameters.deleted_at_from=$filter('date')(parameters.deleted_at_from, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(parameters.deleted_at_to)) {
            parameters.deleted_at_to=$filter('date')(parameters.deleted_at_to, 'dd-MM-yyyy')
        }
                    
        $rootScope.progressbar_start();
        Org.trash(parameters, function (response) {
            $rootScope.close();
            $rootScope.progressbar_complete();
            if(parameters.action == 'paginate'){
                $rootScope.Organizations = response.data;
                $scope.CurrentPage = response.current_page;
                $rootScope.TotalItems = response.total;
                $rootScope.ItemsPerPage = response.per_page;   
           }else{
                if(response.download_token){
                   var file_type='xlsx';
                   window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
               }else{
                   $rootScope.toastrMessages('error',$filter('translate')('no records to download'));
               }
           }
           
       });
   };

    $rootScope.search_ = function (name) {
        var parameters = angular.copy($scope.target);
        parameters.name = name;
        LoadTrashedOrgs(parameters);
    };

    $rootScope.Search = function (selected ,target,action) {
        var parameters = angular.copy(target);
        parameters.action = action;
        parameters.items=[];

          if(selected == true || selected == 'true' ){
            
                var items =[];
                 angular.forEach($rootScope.Organizations, function(v, k){
                     if(v.check){
                             items.push(v.id);
                         }
                 });

                 if(items.length==0){
                     $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                     return ;
                 }
                 parameters.items=items;
            }        
            
        LoadTrashedOrgs(parameters);
    };

    $rootScope.refresh_page = function () {
        var parameters = angular.copy($scope.target);
        parameters.action = 'paginate';        
        LoadTrashedOrgs(parameters);
    };

    $rootScope.pageChanged = function (CurrentPage) {
        $scope.CurrentPage = CurrentPage;
        $rootScope.refresh_page();    
    };

    $rootScope.itemsPerPage_ = function (itemsCount) {
        $scope.itemsCount = itemsCount;
        $rootScope.refresh_page();
    };

    $scope.toggle = function () {
        $scope.target = {};
        $scope.search_form = !$scope.search_form;
        $scope.simple_form = !$scope.simple_form;
        $scope.name ='';
    };
    
    $scope.resetFilter_ = function () {
        $scope.target = {};
        $scope.target.email='';
        $scope.name ='';
    };
        
    $scope.restore = function (size,id) {

        var msg = $filter('translate')('restore_organization_form');

        if($rootScope.orgType =='sponsors'){
            msg = $filter('translate')('restore_sponsor_form');
        }

        $rootScope.clearToastr();

        $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
            .then(function() {
                $rootScope.progressbar_start();
                Org.restore({id:id,type: $rootScope.orgType},function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status== 'success'){
                        $rootScope.refresh_page();
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                    } else {
                        $rootScope.toastrMessages('error',response.msg);
                    }
                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });
            });

    };

    $scope.selectedAll = function(value){
        if(value){
            $scope.selectAll = true;
        }
        else{
            $scope.selectAll =false;

        }
        angular.forEach($rootScope.Organizations, function(val,key)
        {
            val.check=$scope.selectAll;
        });
    };

 // *************** DATE PIKER ***************
    $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0};
    $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $rootScope.format = $scope.formats[0];
    $rootScope.today = function() {$rootScope.dt = new Date();};
    $rootScope.today();
    $rootScope.clear = function() {$rootScope.dt = null;};

    $scope.open1 = function($event) { $event.preventDefault();  $event.stopPropagation();$scope.popup1.opened = true;};
    $scope.popup1 = {opened: false};
    $scope.open2 = function($event) {  $event.preventDefault();  $event.stopPropagation();  $scope.popup2.opened = true; };
    $scope.popup2 = {  opened: false };
    $scope.open3 = function($event) { $event.preventDefault();  $event.stopPropagation();$scope.popup3.opened = true;};
    $scope.popup3 = {opened: false};
    $scope.open4 = function($event) {  $event.preventDefault();  $event.stopPropagation();  $scope.popup4.opened = true; };
    $scope.popup4 = {  opened: false };
    
    
    $rootScope.refresh_page();


});

