OrganizationModule = angular.module('OrganizationModule');

OrganizationModule
    .controller('OrganizationsEditController', function ($filter,$stateParams, $state, $rootScope, $scope, Org, FileUploader, OAuthToken,Entity) {


        $rootScope.clearToastr();
        var AuthUser =localStorage.getItem("charity_LoggedInUser");
        $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
        $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
        if($rootScope.charity_LoggedInUser.organization != null) {
            $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
        }
        $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;
        $rootScope.orgType =$rootScope.orgType = $stateParams.type == 'sponsors'? 'sponsors' : 'organizations';

        $rootScope.failed = false;
        $rootScope.error = false;
        $scope.upload_logo= false;
        $rootScope.status = '';
        $rootScope.msg = '';
        $rootScope.msg1 = [];

        $rootScope.model_error_status = false;
        $rootScope.no_org = false;
        $rootScope.close = function () {
            
            $rootScope.failed = false;
            $rootScope.error = false;
            $rootScope.model_error_status = false;
        };

        var logoUploader = $scope.logoUploader = new FileUploader({
            url: '/api/v1.0/org/' + $scope.orgType + '/logo-upload',
            removeAfterUpload: false,
            queueLimit: 1,
            headers: {
                Authorization: OAuthToken.getAuthorizationHeader()
            }
        });
        function uploadFiles(value) {
            $rootScope.progressbar_start();
            $scope.target.id = value.id;
            
            logoUploader.onBeforeUploadItem = function(item) {
                $rootScope.clearToastr();
                item.formData = [{organization_id: value.id,action:"check"}];
            };
            logoUploader.onCompleteItem = function(fileItem, response, status, headers) {
                $rootScope.progressbar_complete();
                // $scope.logoUploader.destroy();
                $scope.logoUploader.queue=[];
                logoUploader.queue=[];
               // $scope.logoUploader.clearQueue();
                angular.element("input[type='file']").val(null);

                if(response.status=='error')
                {
                    $rootScope.toastrMessages('error',response.msg);
                }
                else{
                    $scope.target.logo = response.logo;
                    $scope.upload_logo =true ;
                    ReturnBack();
                }
            };
            logoUploader.uploadAll();
        }
        function ReturnBack(){
            if($scope.upload_logo){
                $state.go('org-organizations',{"type": $stateParams.type,"action": $scope.action,"status": true});
            }
            $rootScope.toastrMessages('success',$filter('translate')('action success'));
        }

        $scope.save = function() {

            $rootScope.clearToastr();

            $scope.target.type = $scope.orgType;
            var CurOrg = angular.copy($scope.target);

            if ($action == 'edit') {
                var target = new Org(CurOrg);
                $rootScope.progressbar_start();
                Org.update(CurOrg,function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status=='error' || response.status=='failed') {
                        $rootScope.toastrMessages('error',response.msg);
                    } else if(response.status=='failed_valid') {
                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                        $rootScope.status1 ='failed_valid';
                        $rootScope.msg1 =response.msg;
                    }else if(response.status =='success')
                    {
                        CurOrg=response.data;
                        $scope.id=response.data.id;

                        if(logoUploader.queue.length == 0){
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            $state.go('org-organizations',{"type": $stateParams.type,"action": $scope.action,"status": true});
                        }else{
                            uploadFiles({'id':$scope.id});
                        }

                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });
            }
            else{

                $rootScope.progressbar_start();
                Org.save(CurOrg,function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status=='error' || response.status=='failed') {
                        $rootScope.toastrMessages('error',response.msg);
                    } else if(response.status=='failed_valid') {
                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                        $rootScope.status1 =response.status;
                        $rootScope.msg1 =response.msg;
                    } else if(response.status=='success')
                    {
                                
                       $scope.target.id = response.id ;
                       $action = 'edit';
                       $scope.id=response.id;
                         if(logoUploader.queue.length == 0){
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            $state.go('org-organizations',{"type": $stateParams.type,"action": $scope.action,"status": true});
                        }else{
                            uploadFiles({'id':$scope.id});
                        }
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                }, function(error) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('error',error.data.msg);
                });


            }
        };
        $scope.location= function(entity,value,parent){
            if($stateParams.type =='organizations'){
                if(value != null && value != "" && value != " " ) {
                    Entity.listChildren({entity: entity, 'id': value, 'parent': parent}, function (response) {

                        if (entity == 'districts') {
                            if( !angular.isUndefined($rootScope.msg1.country)) {
                                $rootScope.msg1.country = [];
                            }
                            $scope.districts = response;
                            $scope.target.district_id=undefined;
                            $scope.target.city_id=undefined;
                            $scope.target.location_id=undefined;
                            $scope.city = [];
                            $scope.nearlocation =[];
                        }
                        if (entity == 'cities') {
                            if( !angular.isUndefined($rootScope.msg1.district)) {
                                $rootScope.msg1.district = [];
                            }
                            $scope.city = response;
                            $scope.target.city_id=undefined;
                            $scope.target.location_id=undefined;
                            $scope.nearlocation =[];
                        }
                        if (entity == 'neighborhoods') {
                            if( !angular.isUndefined($rootScope.msg1.city)) {
                                $rootScope.msg1.city = [];
                            }
                            $scope.nearlocation = response;
                            $scope.target.location_id=undefined;
                        }
                    });
                }
            }else{
                if( !angular.isUndefined($rootScope.msg1.country)) {
                    $rootScope.msg1.country = [];
                }
            }
        };

      $rootScope.orgType =$scope.orgType = $stateParams.type == 'sponsors'? 'sponsors' : 'organizations';
        var $action = $scope.action=$stateParams.action;
        if ($action == 'edit' && !$stateParams.id) {
            $stateParams.action = $action = 'new';
            $state.current.data.pageTitle = $stateParams.type == 'sponsors'? $filter('translate')('edit sponsor') :
                $filter('translate')('edit organization');

        }else{
            $state.current.data.pageTitle = $stateParams.type == 'sponsors'? $filter('translate')('add_new_sponsor') :
                $filter('translate')('add_new_organization');
        }
        Org.query({type: $scope.orgType}, function (response) {
            $scope.Organizations = response.data;
        });

        Entity.list({entity:'organizations-category'},function (response) {
            $rootScope.categoriesList = response;
        });

        Entity.list({entity:'countries'},function (response) {
            $rootScope.Country = response;
        });


        if ($action == 'edit') {
            $rootScope.progressbar_start();
            Org.get({type: $scope.orgType, id: $stateParams.id}, function(response) {
                $rootScope.progressbar_complete();
                $scope.target = response;

                if( $rootScope.orgType == 'organizations'){
                    if($scope.target.country != null && $scope.target.country != ""){
                        // $scope.location('districts',$scope.target.country,'countries');
                        Entity.listChildren({entity: 'districts', 'id': $scope.target.country, 'parent': 'countries'}, function (response) {
                            $scope.districts = response;
                        });
                    }
                    if($scope.target.district_id != null && $scope.target.district_id != ""){
                        // $scope.location('cities',$scope.target.district_id,'districts');
                        Entity.listChildren({entity: 'cities', 'id': $scope.target.district_id, 'parent': 'districts'}, function (response) {
                            $scope.city = response;
                        });
                   }

                    if($scope.target.city_id != null && $scope.target.city_id != ""){
                        // $scope.location('neighborhoods',$scope.target.city_id,'cities');
                        Entity.listChildren({entity: 'neighborhoods', 'id': $scope.target.city_id, 'parent': 'cities'}, function (response) {
                            $scope.nearlocation = response;
                        });
                    }
                }
            });
        } else {
            $scope.target = new Org();
        }
    });

