OrganizationModule = angular.module('OrganizationModule');

angular.module('OrganizationModule')
    .controller('organizationProjectsController' ,function($state,$stateParams,$rootScope,$ngBootbox,$scope,$http,$timeout,$uibModal,$log,organizationProjects,$filter,Org, Entity,cs_persons_vouchers,vouchers_categories) {


        if($stateParams.success_msg){
            $rootScope.toastrMessages('success',$stateParams.success_msg);
        }else{
            $rootScope.clearToastr();
        }
        $state.current.data.pageTitle = $filter('translate')('organizations-projects');

        var AuthUser =localStorage.getItem("charity_LoggedInUser");
        $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
        $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
        if($rootScope.charity_LoggedInUser.organization != null) {
            $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
            $rootScope.UserOrgType = $rootScope.charity_LoggedInUser.organization.type;
        }
        $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;
        $rootScope.connectWithSponsor = $rootScope.charity_LoggedInUser.connect_with_sponsor;

        $scope.master=false;
        $rootScope.items=[];
        $scope.items=[];
        $scope.CurrentPage=1;
        $scope.itemsCount = '50';

        $scope.filters={"all_organization":false, 'page':1,'action':"paginate",'title':"",
                        'notes':"", 'created_at_to':"", 'created_at_from':"",'has_voucher':"",
                        'begin_date_from':"", 'end_date_from':"", 'begin_date_to':"", 'end_date_to':""};

        Entity.get({entity:'entities',c:'countries,ProjectActivities,transferCompany,ProjectCategory,ProjectType,ProjectBeneficiaryCategory,currencies,ProjectRegion'},function (response) {
            $rootScope.countries = response.countries;
            $rootScope.currencies = response.currencies;
            $rootScope.transferCompany = response.transferCompany;
            $rootScope.ProjectActivities = response.ProjectActivities;
            $rootScope.ProjectBeneficiaryCategory = response.ProjectBeneficiaryCategory;
            $rootScope.ProjectRegion = response.ProjectRegion;
            $rootScope.ProjectType = response.ProjectType;
            $rootScope.ProjectCategory = response.ProjectCategory;
        });


        $scope.subList=function(id){
            Entity.listChildren({parent:'project-category',id:id,entity:'sub'},function (response) {
                $rootScope.SubProjectCategory = response;
            });
        };
        Org.list({type:'sponsors'},function (response) { $rootScope.Sponsors = response; });


        var LoadProjects=function(params){
            // $rootScope.clearToastr();
            if( !angular.isUndefined(params.begin_date_from)) {
                params.begin_date_from=$filter('date')(params.begin_date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.end_date_from)) {
                params.end_date_from= $filter('date')(params.end_date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.begin_date_to)) {
                params.begin_date_to=$filter('date')(params.begin_date_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.end_date_to)) {
                params.end_date_to= $filter('date')(params.end_date_to, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(params.created_at_to)) {
                params.created_at_to=$filter('date')(params.created_at_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.created_at_from)) {
                params.created_at_from= $filter('date')(params.created_at_from, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(params.all_organization)) {
                if(params.all_organization == true ||params.all_organization == 'true'){
                    var orgs=[];
                    angular.forEach(params.organization_id, function(v, k) {
                        orgs.push(v.id);
                    });

                    params.organization_ids=orgs;
                    $scope.master=true;

                }else if(params.all_organization == false ||params.all_organization == 'false') {
                    params.organization_ids=[];
                    $scope.master=false
                }
            }

            // angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();
            params.trashed = false;
            organizationProjects.filter(params,function (response) {
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');

                $rootScope.progressbar_complete();
                if(params.action == 'paginate'){
                    $scope.items= response.items.data;
                    $scope.CurrentPage = response.items.current_page;
                    $rootScope.TotalItems = response.items.total;
                    $rootScope.ItemsPerPage = response.items.per_page;
                }else{
                    if(!angular.isUndefined(response.download_token)) {
                        $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                        var file_type='xlsx';
                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                    }
                }
            });

        };

        var ReloadProjects = function () {
            var params = angular.copy($scope.filters);
            params.page=$scope.CurrentPage;
            params.action='paginate';
            params.itemsCount= $scope.itemsCount ;
            LoadProjects(params);
        };

        Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

        $scope.resetSearchFilter =function(){
            $scope.filters={"all_organization":false,
                'page':1,'action':"paginate",'title':"", 'notes':"", 'created_at_to':"", 'created_at_from':"",'has_voucher':"",
                'begin_date_from':"", 'end_date_from':"", 'begin_date_to':"", 'end_date_to':""};
        };

        $scope.toggleStatus = false;
        $scope.toggle = function() {
            $rootScope.clearToastr();
            $scope.toggleStatus = $scope.toggleStatus == false ? true: false;
            $scope.resetSearchFilter();
            if($scope.toggleStatus == true) {
                $scope.filter.page =1;
                // resetTable();
            }
        };

        $scope.pageChanged = function (currentPage) {
            $scope.CurrentPage=currentPage;
            ReloadProjects();
        };

        $scope.itemsPerPage_ = function (itemsCount) {
            $scope.CurrentPage=1;
            $scope.itemsCount = itemsCount ;
            ReloadProjects();
        };

        $scope.selectedAll = false;
        $scope.selectAll=function(value){
                if (value) {
                    $scope.selectedAll = true;
                } else {
                    $scope.selectedAll = false;
                }

                angular.forEach($scope.items, function(v, k) {
                    v.check=$scope.selectedAll;
                });
        };

        $scope.searchReports = function (selected,filter,action) {
            $scope.CurrentPage=1;
            filter.action = action;
            filter.items=[];

            if(selected == true || selected == 'true' ){
            
                var items =[];
                 angular.forEach($scope.items, function(v, k){

                      if(action == 'ExportToWord' || action == 'ExportToPDF'){
                         if(v.check && v.beneficiary > 0){
                             items.push(v.id);
                         } 
                      }else{
                          if(v.check){
                             items.push(v.id);
                         }
                      }


                 });

                 if(items.length==0){
                     $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                     return ;
                 }
                 filter.items=items;
            }
            filter.page = 1;
            LoadProjects(filter);
        };

        $scope.delete = function (id) {
            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                .then(function() {
                    $rootScope.progressbar_start();
                    organizationProjects.delete({id:id},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            ReloadProjects();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                });

        };


        $scope.sortKeyArr=[];
        $scope.sortKeyArr_rev=[];
        $scope.sortKeyArr_un_rev=[];

        $scope.sort_ = function(keyname){

            $scope.sortKey_ = keyname;
            var reverse_ = false;
            if (
                ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
                ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
            ) {
                reverse_ = true;
            }
            var data = angular.copy($scope.filters);
            data.action ='paginate';

            if (reverse_) {
                if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                    $scope.sortKeyArr_rev=[];
                    $scope.sortKeyArr_un_rev=[];
                    $scope.sortKeyArr_rev.push(keyname);
                }
            }
            else {
                if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                    $scope.sortKeyArr_rev=[];
                    $scope.sortKeyArr_un_rev=[];
                    $scope.sortKeyArr_un_rev.push(keyname);
                }

            }

            data.sortKeyArr_rev = $scope.sortKeyArr_rev;
            data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;

            $scope.searchReports(false ,data,'paginate');
        };
        //-----------------------------------------------------------------------------------------------------------
        $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {$scope.dt = new Date();};
        $scope.today();
        $scope.clear = function() {$scope.dt = null;};
        $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
        $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};

        $scope.popup={0:{},1:{opened:false},2:{opened:false},3:{opened:false},4:{opened:false},5:{opened:false},6:{opened:false}
                         ,7:{opened:false},8:{opened:false},9:{opened:false},10:{opened:false}};
        $scope.open=function (target,$event) {
            $event.preventDefault();
            $event.stopPropagation();
            if(target == 1){ $scope.popup[1].opened = true;  }
            else if(target == 2) { $scope.popup[2].opened = true; }
            else if(target == 3) { $scope.popup[3].opened = true; }
            else if(target == 4) { $scope.popup[4].opened = true; }
            else if(target == 5) { $scope.popup[5].opened = true; }
            else if(target == 6) { $scope.popup[6].opened = true; }
            else if(target == 7) { $scope.popup[7].opened = true; }
            else if(target == 8) { $scope.popup[8].opened = true; }
            else if(target == 9) { $scope.popup[9].opened = true; }
            else if(target == 10) { $scope.popup[10].opened = true; }
        };

        $rootScope.Choices = vouchers_categories.List();
        $scope.newVoucher=function(action,organization_project_id,project){
            $rootScope.clearToastr();
            $rootScope.reanOnlyInput=false;

            let oValue = 0;

            if (project.beneficiary_count > 0 && project.amount > 0){
                oValue = Math.round((project.amount / project.beneficiary_count));
            }

            let vType = '1';
            if (project.distribute == 1 || project.distribute == '1' ){
                vType = '2';
            }

            if(action == 0){

                $rootScope.voucher={center:'0' ,
                                    un_serial :'' ,
                                    type : vType ,
                                    title : project.title ,
                                    content  : project.description ,
                                    sponsor_id  : project.sponsor_id ,
                                    currency_id  : project.currency_id ,
                                    count : project.beneficiary_count ,
                                    value  : oValue ,
                                    notes  : project.notes  ,
                                    voucher_date : new Date(project.project_date) ,
                };

                $uibModal.open({
                    templateUrl: 'newVoucher.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance, $log,cs_persons_vouchers) {

                        $scope.mode='add';

                        $scope.confirm = function (params) {
                            $rootScope.clearToastr();
                            if( !angular.isUndefined(params.voucher_date)) {
                                params.voucher_date=$filter('date')(params.voucher_date, 'dd-MM-yyyy')
                            }
                            $rootScope.progressbar_start();
                            params.organization_project_id = organization_project_id;
                            cs_persons_vouchers.save(params,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    $modalInstance.close();
                                    if(response.status=='success')
                                    {
                                        ReloadProjects();
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                }
                            });

                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                        $scope.open = function($event) {
                            $event.preventDefault();
                            $event.stopPropagation();
                            $scope.popup.opened = true;
                        };

                        $scope.popup = {
                            opened: false
                        };

                    }});

            }
            else{
                $rootScope.voucher={add_cases:false,center:'0',collected:'0',un_serial:null,
                                    title : project.title ,
                                    content  : project.description ,
                                    sponsor_id  : project.sponsor_id ,
                                    count : project.beneficiary_count ,
                                    value  : oValue ,
                                    type : vType ,
                                    currency_id : project.currency_id ,
                                    voucher_date : new Date(project.project_date) ,
                                    notes  : project.notes  ,
                                    organization_project_id:organization_project_id};

                $uibModal.open({
                    templateUrl: 'importFromExcel.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance, $log,cs_persons_vouchers,OAuthToken, FileUploader) {
                        $scope.remove=function(){
                            $scope.uploader.clearQueue();
                            angular.forEach(
                                angular.element("input[type='file']"),
                                function(inputElem) {
                                    angular.element(inputElem).val(null);
                                });
                        };


                        $scope.upload=false;
                        $scope.mode='add';

                        $scope.RestUpload=function(value){
                            if(value ==2){
                                $scope.upload=false;
                            }
                        };
                        $scope.setAdd=function(){
                            if($rootScope.voucher.center == 1 || $rootScope.voucher.center == '1' ){
                                $rootScope.voucher.add_cases=true;
                            }else{
                                $rootScope.voucher.add_cases=false;
                            }
                        };

                        $scope.fileupload=function(){
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.upload=true;
                        };

                        var Uploader = $scope.uploader = new FileUploader({
                            url: '/api/v1.0/aids/cs_persons_vouchers/storePersonsVoucher',
                            removeAfterUpload: false,
                            queueLimit: 1,
                            headers: {
                                Authorization: OAuthToken.getAuthorizationHeader()
                            }
                        });

                        Uploader.onBeforeUploadItem = function(item) {
                            item.formData = [{source: 'file'}];
                        };

                        $scope.confirm = function (item) {
                            $rootScope.progressbar_start();
                            Uploader.onBeforeUploadItem = function(i) {
                                let center = 0;

                                if($rootScope.can('aid.voucher.center')){
                                    center = 1;
                                }

                                i.formData = [{title: item.title,
                                    center: center,
                                    beneficiary: 1,
                                    organization_project_id: item.organization_project_id,
                                    notes: item.notes,
                                    currency_id: item.currency_id,
                                    exchange_rate: item.exchange_rate,
                                    transfer: item.transfer,
                                    add_cases: item.add_cases,
                                    un_serial: item.un_serial,
                                    collected: item.collected,
                                    allow_day: item.allow_day,
                                    transfer_company_id: item.transfer_company_id,
                                    category_id: item.category_id,
                                    case_category_id: item.case_category_id,
                                    voucher_date: $filter('date')(item.voucher_date, 'dd-MM-yyyy'),
                                    source:'file',
                                    type: item.type,
                                    sponsor_id: item.sponsor_id}];
                            };


                            Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                                $rootScope.progressbar_complete();
                                $scope.uploader.destroy();
                                $scope.uploader.queue=[];
                                $scope.uploader.clearQueue();
                                angular.element("input[type='file']").val(null);

                                if(response.download_token){
                                    var file_type='xlsx';
                                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;

                                }
                                $modalInstance.close();
                                if(response.status=='true' || response.status==true)
                                {
                                    ReloadProjects();
                                    $rootScope.toastrMessages('success',response.msg);
                                }else if(response.status=='inserted_error')
                                {
                                    ReloadProjects();
                                    $rootScope.toastrMessages('error',response.msg);
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            };
                            Uploader.uploadAll();
                        };
                        $scope.confirm2 = function (params) {
                            $rootScope.clearToastr();
                            $rootScope.progressbar_start();
                            if( !angular.isUndefined(params.voucher_date)) {
                                params.voucher_date=$filter('date')(vouchers.voucher_date, 'dd-MM-yyyy')
                            }
                            params.organization_project_id = organization_project_id;
                            var target = new cs_persons_vouchers(params);
                            target.$save()
                                .then(function (response) {
                                    $rootScope.progressbar_complete();
                                    if(response.status=='error' || response.status=='failed')
                                    {
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    else if(response.status=='failed_valid')
                                    {
                                        $scope.status1 =response.status;
                                        $scope.msg1 =response.msg;
                                    }
                                    else{
                                        $modalInstance.close();
                                        if(response.status=='success')
                                        {
                                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                            ReloadProjects();
                                        }else{
                                            $rootScope.toastrMessages('error',response.msg);
                                        }
                                    }
                                }, function(error) {
                                    $rootScope.progressbar_complete();
                                    $rootScope.toastrMessages('error',error.data.msg);
                                    $modalInstance.close();
                                });

                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                        $scope.open = function($event) {
                            $event.preventDefault();
                            $event.stopPropagation();
                            $scope.popup.opened = true;
                        };

                        $scope.popup = {
                            opened: false
                        };

                    }});


            }
        };

        $scope.showVoucher=function(voucher_id){
            $rootScope.clearToastr();
            $rootScope.reanOnlyInput=false;
            cs_persons_vouchers.get({id:voucher_id},function (response) {
                $rootScope.voucher=response;
                $rootScope.voucher.voucher_date=new Date($rootScope.voucher.voucher_date);
                $rootScope.voucher.category_id=$rootScope.voucher.category_id +"";
                $rootScope.voucher.type=$rootScope.voucher.type +"";
                // $rootScope.voucher.sponsor_id=$rootScope.voucher.sponsor_id +"";
                if($rootScope.voucher.status == 1){
                    $rootScope.reanOnlyInput=true;
                }
                if($rootScope.voucher.project_id != null){
                    $rootScope.reanOnlyInput=true;
                }

                $uibModal.open({
                    templateUrl: 'showVoucher.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance, $log,cs_persons_vouchers) {
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                        $scope.open = function($event) {
                            $event.preventDefault();
                            $event.stopPropagation();
                            $scope.popup.opened = true;
                        };

                        $scope.popup = {
                            opened: false
                        };

                    } });

            });
        };

        $scope.voucherFiles = function (id) {

            $rootScope.clearToastr();
            $rootScope.progressbar_start();

            cs_persons_vouchers.getAttachments({id:id},function (response) {
                $rootScope.progressbar_complete();
                $rootScope.voucherStatus= response.status;
                $rootScope.progressbar_complete();
                if(response.status == false){
                    $rootScope.toastrMessages('error',$filter('translate')('no attachment to export'));
                }else{
                    window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token;
                }
            });
        };

        // ReloadProjects();

    });

angular.module('OrganizationModule')
    .controller('trashedOrganizationProjectsController' ,function($state,$stateParams,$rootScope,$ngBootbox,$scope,$http,$timeout,$uibModal,$log,organizationProjects,$filter,Org, Entity) {

        var AuthUser =localStorage.getItem("charity_LoggedInUser");
        $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
        $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
        if($rootScope.charity_LoggedInUser.organization != null) {
            $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
            $rootScope.UserOrgType = $rootScope.charity_LoggedInUser.organization.type;
        }
        $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;
        $rootScope.connectWithSponsor = $rootScope.charity_LoggedInUser.connect_with_sponsor;

        if($stateParams.success_msg){
            $rootScope.toastrMessages('success',$stateParams.success_msg);
        }else{
            $rootScope.clearToastr();
        }
        $state.current.data.pageTitle = $filter('translate')('Trashed organizations-projects');

        $scope.master=false;
        $rootScope.items=[];
        $scope.items=[];
        $scope.CurrentPage=1;
        $scope.itemsCount = '50';

        $scope.filters={"all_organization":false, 'page':1,'action':"paginate",'title':"",
                        'notes':"", 'created_at_to':"", 'created_at_from':"", 'deleted_at_to':"", 'deleted_at_from':"",'has_voucher':"",
                        'begin_date_from':"", 'end_date_from':"", 'begin_date_to':"", 'end_date_to':""};

        Entity.get({entity:'entities',c:'countries,ProjectActivities,ProjectCategory,ProjectType,ProjectBeneficiaryCategory,currencies,ProjectRegion'},function (response) {
            $rootScope.countries = response.countries;
            $rootScope.currencies = response.currencies;
            $rootScope.ProjectActivities = response.ProjectActivities;
            $rootScope.ProjectBeneficiaryCategory = response.ProjectBeneficiaryCategory;
            $rootScope.ProjectRegion = response.ProjectRegion;
            $rootScope.ProjectType = response.ProjectType;
            $rootScope.ProjectCategory = response.ProjectCategory;
        });


        $scope.subList=function(id){
            Entity.listChildren({parent:'project-category',id:id,entity:'sub'},function (response) {
                $rootScope.SubProjectCategory = response;
            });
        };

        Org.list({type:'sponsors'},function (response) { $rootScope.Sponsors = response; });

        var LoadProjects=function(params){
            // $rootScope.clearToastr();

            if( !angular.isUndefined(params.deleted_at_to)) {
                params.deleted_at_to=$filter('date')(params.deleted_at_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.deleted_at_from)) {
                params.deleted_at_from= $filter('date')(params.deleted_at_from, 'dd-MM-yyyy')
            }
            // angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();

            if( !angular.isUndefined(params.all_organization)) {
                if(params.all_organization == true ||params.all_organization == 'true'){
                    var orgs=[];
                    angular.forEach(params.organization_id, function(v, k) {
                        orgs.push(v.id);
                    });

                    params.organization_ids=orgs;
                    $scope.master=true;

                }else if(params.all_organization == false ||params.all_organization == 'false') {
                    params.organization_ids=[];
                    $scope.master=false
                }
            }
            params.trashed = true;
            organizationProjects.filter(params,function (response) {
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');

                $rootScope.progressbar_complete();
                if(params.action == 'paginate'){
                    $scope.items= response.items.data;
                    $scope.CurrentPage = response.items.current_page;
                    $rootScope.TotalItems = response.items.total;
                    $rootScope.ItemsPerPage = response.items.per_page;
                }else{
                    if(!angular.isUndefined(response.download_token)) {
                        $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                        var file_type='xlsx';
                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                    }
                }
            });

        };

        var ReloadProjects = function () {
            var params = angular.copy($scope.filters);
            params.page=$scope.CurrentPage;
            params.action='paginate';
            params.itemsCount= $scope.itemsCount ;
            LoadProjects(params);
        };

        Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

        $scope.resetSearchFilter =function(){
            $scope.filters={"all_organization":false,
                'page':1,'action':"paginate",'title':"", 'notes':"", 'created_at_to':"", 'created_at_from':"",'has_voucher':"",
                'begin_date_from':"", 'end_date_from':"", 'begin_date_to':"", 'end_date_to':""};
        };

        $scope.pageChanged = function (currentPage) {
            $scope.CurrentPage=currentPage;
            ReloadProjects();
        };

        $scope.itemsPerPage_ = function (itemsCount) {
            $scope.CurrentPage=1;
            $scope.itemsCount = itemsCount ;
            ReloadProjects();
        };

      $scope.selectedAll = false;
        $scope.selectAll=function(value){
                if (value) {
                    $scope.selectedAll = true;
                } else {
                    $scope.selectedAll = false;
                }

                angular.forEach($scope.items, function(v, k) {
                    v.check=$scope.selectedAll;
                });
        };

        $scope.searchReports = function (selected,filter,action) {
            $scope.CurrentPage=1;
            filter.action = action;
            filter.items=[];


            if(selected == true || selected == 'true' ){
            
                var items =[];
                 angular.forEach($scope.items, function(v, k){

                      if(action == 'ExportToWord' || action == 'ExportToPDF'){
                         if(v.check && v.beneficiary > 0){
                             items.push(v.id);
                         } 
                      }else{
                          if(v.check){
                             items.push(v.id);
                         }
                      }


                 });

                 if(items.length==0){
                     $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                     return ;
                 }
                 filter.items=items;
            }
            filter.page = 1;
            filter.action = action;
            LoadProjects(filter);
        };
        // restore project by id
        $scope.restore = function (id) {
            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to confirm restore') )
                .then(function() {
                    $rootScope.progressbar_start();
                    organizationProjects.restore({id: id}, function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            ReloadProjects();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                });
        };


        $scope.sortKeyArr=[];
        $scope.sortKeyArr_rev=[];
        $scope.sortKeyArr_un_rev=[];
        $scope.sort_ = function(keyname){
            $scope.sortKey_ = keyname;
            var reverse_ = false;
            if (
                ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
                ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
            ) {
                reverse_ = true;
            }
            var data = angular.copy($scope.filters);
            data.action ='paginate';

            if (reverse_) {
                if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                    $scope.sortKeyArr_rev=[];
                    $scope.sortKeyArr_un_rev=[];
                    $scope.sortKeyArr_rev.push(keyname);
                }
            }
            else {
                if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                    $scope.sortKeyArr_rev=[];
                    $scope.sortKeyArr_un_rev=[];
                    $scope.sortKeyArr_un_rev.push(keyname);
                }

            }

            data.sortKeyArr_rev = $scope.sortKeyArr_rev;
            data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;

            $scope.searchReports(false ,data,'paginate');
        };

        //-----------------------------------------------------------------------------------------------------------
        $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {$scope.dt = new Date();};
        $scope.today();
        $scope.clear = function() {$scope.dt = null;};
        $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
        $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};

        $scope.popup={0:{},1:{opened:false},2:{opened:false},3:{opened:false},4:{opened:false},5:{opened:false},6:{opened:false}
                      ,7:{opened:false},8:{opened:false},9:{opened:false},10:{opened:false}};

        $scope.open=function (target,$event) {
            $event.preventDefault();
            $event.stopPropagation();
            if(target == 1){ $scope.popup[1].opened = true;  }
            else if(target == 2) { $scope.popup[2].opened = true; }
            else if(target == 3) { $scope.popup[3].opened = true; }
            else if(target == 4) { $scope.popup[4].opened = true; }
            else if(target == 5) { $scope.popup[5].opened = true; }
            else if(target == 6) { $scope.popup[6].opened = true; }
            else if(target == 7) { $scope.popup[7].opened = true; }
            else if(target == 8) { $scope.popup[8].opened = true; }
            else if(target == 9) { $scope.popup[9].opened = true; }
            else if(target == 10) { $scope.popup[10].opened = true; }
        };

        // ReloadProjects();

    });

angular.module('OrganizationModule')
    .controller('organizationProjectFormController' ,function($state,$stateParams,$rootScope,$ngBootbox,$scope,$http,$timeout,$uibModal,$log,organizationProjects,$filter,Org, Entity) {


        var AuthUser =localStorage.getItem("charity_LoggedInUser");
        $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
        $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
        if($rootScope.charity_LoggedInUser.organization != null) {
            $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
            $rootScope.UserOrgType = $rootScope.charity_LoggedInUser.organization.type;
        }
        $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;
        $rootScope.connectWithSponsor = $rootScope.charity_LoggedInUser.connect_with_sponsor;

        $scope.action = 'new';
        $scope.title = '';
        $scope.row={};
        $scope.status1 ='';
        $scope.msg1=[];

        Entity.get({entity:'entities',c:'countries,ProjectActivities,ProjectCategory,ProjectType,ProjectBeneficiaryCategory,currencies,ProjectRegion'},function (response) {
            $rootScope.countries = response.countries;
            $rootScope.currencies = response.currencies;
            $rootScope.ProjectActivities = response.ProjectActivities;
            $rootScope.ProjectBeneficiaryCategory = response.ProjectBeneficiaryCategory;
            $rootScope.ProjectRegion = response.ProjectRegion;
            $rootScope.ProjectType = response.ProjectType;
            $rootScope.ProjectCategory = response.ProjectCategory;
        });
        Org.list({type:'sponsors'},function (response) { $rootScope.Sponsors = response; });

        $scope.subList=function(id){
            if( !angular.isUndefined($scope.msg1.category_id)) {
                $scope.msg1.category_id = [];
            }

            Entity.listChildren({parent:'project-category',id:id,entity:'sub'},function (response) {
                $rootScope.SubProjectCategory = response;
                $scope.row.sub_category_id = "";
            });
        };

        if($stateParams.id){
            $scope.action = 'edit';
            $rootScope.progressbar_start();
            organizationProjects.get({id:$stateParams.id},function (response) {
                $rootScope.progressbar_complete();
                $scope.row= response;
                $scope.title = $scope.row.title;
            });

        }



        $scope.confirm = function (item) {
            $rootScope.clearToastr();

            var row = angular.copy(item);

            if( !angular.isUndefined(row.date)) {
                row.date=$filter('date')(row.date, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(row.date_from)) {
                row.date_from=$filter('date')(row.date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(row.date_to)) {
                row.date_to= $filter('date')(row.date_to, 'dd-MM-yyyy')
            }

            $rootScope.progressbar_start();
            if(row.id){
                organizationProjects.update(row,function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status=='error' || response.status=='failed')
                    {
                        $rootScope.toastrMessages('error',response.msg);
                    }
                    else if(response.status=='failed_valid')
                    {
                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                        $scope.status1 =response.status;
                        $scope.msg1 =response.msg;
                    }
                    else{
                        if(response.status=='success') {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            $uibModal.open({
                                templateUrl: 'redirect.html',
                                backdrop  : 'static',
                                keyboard  : false,
                                windowClass: 'modal',
                                size: 'md',
                                controller: function ($rootScope,$scope, $modalInstance, $log) {
                                    $scope.yes = function () {
                                        $modalInstance.close();
                                        $state.go('org-project-attachments',{"success_msg":$filter('translate')('action success'),
                                            "id":response.row.id});
                                    };


                                    $scope.no = function () {
                                        $modalInstance.close();
                                        $state.go('org-projects',{"success_msg":$filter('translate')('action success')});
                                    };
                                }
                            });
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }

                    }
                });
            }else {
                organizationProjects.save(row,function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status=='error' || response.status=='failed')
                    {
                        $rootScope.toastrMessages('error',response.msg);
                    }
                    else if(response.status=='failed_valid')
                    {
                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                        $scope.status1 =response.status;
                        $scope.msg1 =response.msg;
                    }
                    else{
                        if(response.status=='success') {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            $uibModal.open({
                                templateUrl: 'redirect.html',
                                backdrop  : 'static',
                                keyboard  : false,
                                windowClass: 'modal',
                                size: 'md',
                                controller: function ($rootScope,$scope, $modalInstance, $log) {
                                    $scope.yes = function () {
                                        $modalInstance.close();
                                        $state.go('org-project-attachments',{"success_msg":$filter('translate')('action success'),
                                            "id":response.row.id});
                                    };


                                    $scope.no = function () {
                                        $modalInstance.close();
                                        $state.go('org-projects',{"success_msg":$filter('translate')('action success')});
                                    };
                                }
                            });
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }

                    }
                });
            }

        };


        //-----------------------------------------------------------------------------------------------------------
        $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {$scope.dt = new Date();};
        $scope.today();
        $scope.clear = function() {$scope.dt = null;};
        $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
        $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};

        $scope.popup={0:{},1:{opened:false},2:{opened:false},3:{opened:false},4:{opened:false},5:{opened:false},6:{opened:false}};
        $scope.open=function (target,$event) {
            $event.preventDefault();
            $event.stopPropagation();
            if(target == 1){ $scope.popup[1].opened = true;  }
            else if(target == 2) { $scope.popup[2].opened = true; }
            else if(target == 3) { $scope.popup[3].opened = true; }
            else if(target == 4) { $scope.popup[4].opened = true; }
            else if(target == 5) { $scope.popup[5].opened = true; }
            else if(target == 6) { $scope.popup[6].opened = true; }
        };

    });

angular.module('OrganizationModule')
    .controller('organizationProjectShowFormController' ,function($state,$stateParams,$rootScope,$ngBootbox,$scope,$http,$timeout,$uibModal,$log,organizationProjects,$filter,Org, Entity) {

        var AuthUser =localStorage.getItem("charity_LoggedInUser");
        $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
        $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
        if($rootScope.charity_LoggedInUser.organization != null) {
            $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
            $rootScope.UserOrgType = $rootScope.charity_LoggedInUser.organization.type;
        }
        $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;
        $rootScope.connectWithSponsor = $rootScope.charity_LoggedInUser.connect_with_sponsor;

        $scope.action = 'show';
        $scope.title = '';
        $scope.row={};

        if($stateParams.id){
            $scope.action = 'edit';
            $rootScope.progressbar_start();
            organizationProjects.fetch({id:$stateParams.id},function (response) {
                $rootScope.progressbar_complete();
                $scope.row= response;
                $scope.title = $scope.row.title;
            });

        }

    });

angular.module('OrganizationModule')
    .controller('organizationProjectAttachmentController' ,function($state,$stateParams,$rootScope,$ngBootbox,$scope,$http,$timeout,$uibModal,$log,organizationProjects,$filter,Org, Entity ,$filter,$stateParams,$rootScope, $scope,$ngBootbox,$uibModal, $http, FileUploader, OAuthToken,$timeout) {

        var AuthUser =localStorage.getItem("charity_LoggedInUser");
        $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
        $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
        if($rootScope.charity_LoggedInUser.organization != null) {
            $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
            $rootScope.UserOrgType = $rootScope.charity_LoggedInUser.organization.type;
        }
        $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;
        $rootScope.connectWithSponsor = $rootScope.charity_LoggedInUser.connect_with_sponsor;

        $scope.title = '';
        $rootScope.row={attachments:[]};

        if($stateParams.id){
            $rootScope.progressbar_start();
            
            $rootScope.project_id= $stateParams.id;

            var LoadProjectAttachments =function(){
                $rootScope.progressbar_start();
                organizationProjects.get({id:$stateParams.id},function (response) {
                    $rootScope.progressbar_complete();
                    $rootScope.row= response;
                    $scope.title = $scope.row.title;
                });
            };

            $scope.select=function () {

                $uibModal.open({
                    templateUrl: 'myModalContent2.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                        var Uploader = $scope.uploader = new FileUploader({
                            url: '/api/v1.0/doc/files',
                            headers: {
                                Authorization: OAuthToken.getAuthorizationHeader()
                            }
                        });
                        Uploader.onBeforeUploadItem = function(item) {
                            $rootScope.clearToastr();
                            item.formData = [{action:"not_check"}];
                            $rootScope.progressbar_start();
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);
                            fileItem.id = response.id;
                            organizationProjects.saveAttachments({id:$stateParams.id, document_id:fileItem.id},function (response) {
                                    if(response.status== 'success'){
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        $rootScope.row.attachments.push(response.attachment);
                                        $modalInstance.close();
                                    }
                                }, function(error) {
                                $rootScope.toastrMessages('error',error.data.msg);
                            });

                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                        $scope.closeuploader = function () {
                            $modalInstance.dismiss('cancel');
                            LoadDocuments();

                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };
                    }
                });

            };
            $scope.delete=function (id,index) {
                $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                    .then(function() {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                        organizationProjects.deleteAttachments({id:id},function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status== 'success'){
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            LoadProjectAttachments();

                        });
                    });



                };

            LoadProjectAttachments();
        }




       
    });
