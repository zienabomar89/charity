
OrganizationModule = angular.module('OrganizationModule');

OrganizationModule.controller('OrganizationsSearchController', function ($filter,$state, $stateParams, $rootScope, $scope, $uibModal, Org, Entity) {

    $rootScope.clearToastr();
    $rootScope.details=[];
    $scope.CurrentPage= 1;
    $scope.itemsCount= 50;
    $state.current.data.pageTitle = $filter('translate')('organization search');

    var LoadResult=function($param){
        $rootScope.progressbar_start();
        Org.filter($param, function (response) {
            $rootScope.progressbar_complete();

            if($param.action == 'xlsx'){
                if(!angular.isUndefined(response.download_token)) {
                    $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('there is no data to show'));
                }
            }else{
                if(response.total > 0){
                    $rootScope.details = response.data;
                    $scope.CurrentPage = response.current_page;
                    $rootScope.TotalItems = response.total;
                    $rootScope.ItemsPerPage = response.per_page;
                }else{
                    $rootScope.details=[];
                    $rootScope.toastrMessages('error',$filter('translate')('there is no data to show'));
                }
            }
        });

    };

    $rootScope.export = function (name) {
        LoadResult({'itemsCount':$scope.itemsCount ,'name':name ,'action':'xlsx'});
    };

    $rootScope.search_ = function (name) {
        LoadResult({'itemsCount':$scope.itemsCount,'page':$scope.CurrentPage ,'name':name ,'action':'paginate'});
    };

    $rootScope.pageChanged = function (CurrentPage) {
        LoadResult({'itemsCount':$scope.itemsCount,'page':CurrentPage ,'name':name ,'action':'paginate'});
    };

    $rootScope.itemsPerPage_ = function (itemsCount) {
        LoadResult({'itemsCount':itemsCount,'page':$scope.CurrentPage ,'name':name ,'action':'paginate'});
    };

});


