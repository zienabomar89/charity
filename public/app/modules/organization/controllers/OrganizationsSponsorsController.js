OrganizationModule = angular.module('OrganizationModule');

OrganizationModule
    .controller('OrganizationsSponsorsController', function ($filter,$stateParams, $state, $rootScope,
                                                             $scope, Org, OAuthToken,$timeout) {

        var AuthUser =localStorage.getItem("charity_LoggedInUser");
        $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
        $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
        if($rootScope.charity_LoggedInUser.organization != null) {
            $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
        }
        $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;
        $rootScope.row={id:$stateParams.id,list:[]};

       $rootScope.close = function () {

            $rootScope.failed = false;
            $rootScope.error = false;
            $rootScope.model_error_status = false;
        };

        $state.current.data.pageTitle = $filter('translate')('connected sponsors list');

        if($stateParams.id){
            $rootScope.progressbar_start();
            Org.getSponsors({id: $stateParams.id},function (response) {
                $rootScope.progressbar_complete();
                if( ($rootScope.superAdmin == 1 || $rootScope.superAdmin == '1' || $rootScope.UserOrgLevel == 1)
                    ||
                    ( $rootScope.can('org.organizations.connectSponsor') && ($rootScope.UserOrgLevel ==2 && response.level == 3))
                ){
                    $rootScope.row=response;
                    if($rootScope.row.list.length == 0 ){
                        $rootScope.toastrMessages('error',$filter('translate')("The parent organization has not been connect to any sponsors. Please check that with your parent or super admin system"));
                        $timeout(function() {
                            $state.go('org-organizations',{"type": 'organizations'});
                        }, 3000);
                    }
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')("you don't have permission to do this action"));
                    $timeout(function() {
                        $state.go('org-organizations',{"type": 'organizations'});
                    }, 3000);
                }
            });
            $scope.confirm = function () {
                $rootScope.clearToastr();
                var params={id:$stateParams.id,sponsors:[]};

                angular.forEach($rootScope.row.list, function(v, k) {
                    if(v.selected){
                        if(params.sponsors.indexOf(v.sponsor_id) == -1){
                            params.sponsors.push(v.sponsor_id);
                        }
                    }
                });

                $rootScope.progressbar_start();
                Org.resetSponsors(params,function (response) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                    $state.go('org-organizations',{"type": 'organizations',"action": 'edit',"status": response.status});
                });
            };
        }else{
            $rootScope.toastrMessages('error',$filter('translate')('Your are request invalid link , you will be redirected to the Organizations page to search again and request the valid link'));
            $timeout(function() {
                $state.go('org-organizations',{"type": 'organizations'});
            }, 3000);
        }
    });

