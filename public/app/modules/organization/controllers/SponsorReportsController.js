
angular.module('OrganizationModule')
    .controller('SponsorReportsController' ,function($stateParams,$rootScope, $scope, $http ,setting,$timeout, $uibModal, $log,category,Org,FileUploader, OAuthToken,$filter,$state) {

        $rootScope.clearToastr();

        $rootScope.id =$scope.id = $stateParams.id;
        $state.current.data.pageTitle =$filter('translate')('sponsor-reports');

        Org.get({type: 'sponsors', id: $stateParams.id}, function(response) {
            $scope.target = response;
            $scope.name=$scope.target.name;
        });

        var RestTemplate= function () {

            $rootScope.progressbar_start();
            setting.getSettingById({id:'Sponsorship-default-template'},function (response) {
                $rootScope.progressbar_complete();
                $rootScope.has_default_template=response.status;
                if(response.status){
                    $rootScope.default_template=response.Setting.value +"";
                }
            });
        };

        RestTemplate();

        $scope.ChooseTemplate=function(id,action,other,lang){

            $rootScope.clearToastr();
            $rootScope.category_id=id;
            $rootScope.action=action;
            $rootScope.other=other;
            $scope.f=false;
            $uibModal.open({
                templateUrl: 'myModalContent2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                        var Uploader = $scope.uploader = new FileUploader({
                            url: '/api/v1.0/common/sponsorships/category/template-upload',
                            removeAfterUpload: false,
                            queueLimit: 1,
                            headers: {
                                Authorization: OAuthToken.getAuthorizationHeader()
                            }
                        });


                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                        item.formData = [{category_id: $rootScope.category_id,
                            lang: lang,
                            sponsor: $rootScope.id,template:'reports','other':$rootScope.other,action:"not_check"}];
                    };
                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        SponsorTemplates();
                        $modalInstance.close();

                    };


                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                },
                resolve: {

                }
            });


        };

        function SponsorTemplates(){
                category.getSponsorReports({'id':$stateParams.id,'type':'sponsorships'},function (response) {
                    $scope.Category = response;
                });
        }

        $rootScope.downloadFile=function(target,id,other,lang){
            var url='';

            $rootScope.progressbar_start();

            if(target =='template-instructions'){
                url='/api/v1.0/common/sponsorships/category/templateInstruction';
                file_type='pdf';
            }else{
                url='/api/v1.0/common/sponsorships/category/template/'+id+'?sponsor='+$rootScope.id+'&other='+other+'&lang='+lang;
                file_type='docx';
            }


            $http({
                url:url,
                method: "GET"
            }).then(function (response) {
                $rootScope.progressbar_complete();
                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token+'&template=true';
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };

        SponsorTemplates();

    });

