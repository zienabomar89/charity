
angular.module('OrganizationModule')
    .controller('SponsorTemplateController' ,function($stateParams,$rootScope, $scope, $http ,setting,$timeout, $uibModal, $log,category,Org,FileUploader, OAuthToken,$filter,$state) {

        $rootScope.clearToastr();
        $scope.Category = [];

        $rootScope.id =$scope.id = $stateParams.id;
        $state.current.data.pageTitle =  $filter('translate')('sponsor-template') ;

        Org.get({type: 'sponsors', id: $stateParams.id}, function(response) {
            $scope.target = response;
            $scope.name=$scope.target.name;
        });

        function RestTemplate() {
            $rootScope.progressbar_start();
            setting.getSettingById({id:'Sponsorship-default-template'},function (response) {
                $rootScope.progressbar_complete();
                $rootScope.has_default_template=response.status;
                if(response.status){
                    $rootScope.default_template=response.Setting.value +"";
                }
            });
        }
        function SponsorTemplates(id){
            // template_type:'candidate',
            category.getSponsorTemplates({'id':$stateParams.id,'type':'sponsorships'},function (response) {
                $scope.Category = response;
            });
        }

        SponsorTemplates();
        RestTemplate();

        $scope.ChooseTemplate=function(id,action,lang){
            $rootScope.category_id=id;
            $rootScope.action=action;

            $scope.f=false;
            $uibModal.open({
                templateUrl: 'myModalContent2.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/common/sponsorships/category/template-upload',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                        item.formData = [{category_id: $rootScope.category_id,
                            lang: lang,
                            sponsor: $rootScope.id,
                            template:'candidate',action:"not_check"}];
                    };

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        SponsorTemplates();
                        $modalInstance.close();

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };
        $rootScope.downloadFile=function(target,id,lang){
            var url='';
            var file_type='';

            if(target =='template-instructions'){
                url='/api/v1.0/common/sponsorships/category/templateInstruction';
                var file_type='pdf';
            }else{
                // target
                url='/api/v1.0/common/sponsorships/category/template/'+id+'?sponsor='+$rootScope.id+'&lang='+lang;
                file_type='docx';
            }

            $http({
                url:url,
                method: "GET"
            }).then(function (response) {
                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token+'&template=true';
            }, function() {
                $rootScope.toastrMessages('error',error.data.msg);
            });
        };

    });

