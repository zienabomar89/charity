OrganizationModule = angular.module('OrganizationModule');

OrganizationModule
    .controller('OrganizationsLocationsController', function ($filter,$stateParams, $state, $rootScope,
                                                             $scope, Org, OAuthToken,$timeout) {

        var AuthUser =localStorage.getItem("charity_LoggedInUser");
        $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
        $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
        if($rootScope.charity_LoggedInUser.organization != null) {
            $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
        }
        $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;

        $rootScope.clearToastr();
        $rootScope.row={id:$stateParams.id,connected:[]};
        $scope.ignoreChanges = false;
        $scope.newId = 1;
        $scope.newNode = {};
        $scope.originalData = [];
        $scope.treeData = null;
        $scope.indexs = [];

        $rootScope.close = function () {

            $rootScope.failed = false;
            $rootScope.error = false;
            $rootScope.model_error_status = false;
        };

        $state.current.data.pageTitle = $filter('translate')('connected sponsors list');
        if($stateParams.id) {
            $rootScope.progressbar_start();
            Org.getLocations({id:$stateParams.id},function (response) {
                $rootScope.progressbar_complete();

                if(($rootScope.superAdmin == 1 || $rootScope.superAdmin == '1' || $rootScope.UserOrgLevel == 1 ||
                    ( $rootScope.can('org.organizations.connectLocations') && ($rootScope.UserOrgLevel ==2 && response.level == 3))
                )){
                    $rootScope.row=response;
                    for (var i = 0; i < response.districts.length; i++) {
                        var parent = '#';
                        $scope.originalData.push({
                            id: 'district' + response.districts[i].id,
                            parent: parent,
                            text: response.districts[i].name,
                            state: {opened: false}
                        });

                    }
                    for (var i = 0; i < response.regions.length; i++) {
                        var parent = 'district' + response.regions[i].parent_id;
                        if (response.regions[i].parent_id)
                            parent = 'district' + response.regions[i].parent_id;
                        $scope.originalData.push({
                            id: 'region' + response.regions[i].id,
                            parent: parent,
                            text: response.regions[i].name,
                            state: {opened: false}
                        });
                    }
                    for (var i = 0; i < response.neighborhoods.length; i++) {
                        var parent = 'region' + response.neighborhoods[i].parent_id;
                        if (response.neighborhoods[i].parent_id)
                            parent = 'region' + response.neighborhoods[i].parent_id;
                        $scope.originalData.push({
                            id: 'neighborhood' + response.neighborhoods[i].id,
                            parent: parent,
                            text: response.neighborhoods[i].name,
                            state: {opened: false}
                        });
                    }
                    for (var i = 0; i < response.squares.length; i++) {
                        var parent = 'neighborhood' + response.squares[i].parent_id;
                        if (response.squares[i].parent_id)
                            parent = 'neighborhood' + response.squares[i].parent_id;
                        $scope.originalData.push({
                            id: 'square' + response.squares[i].id,
                            parent: parent,
                            text: response.squares[i].name,
                            state: {opened: false}
                        });
                    }
                    for (var i = 0; i < response.mosques.length; i++) {
                        var parent = 'square' + response.mosques[i].parent_id;
                        if (response.mosques[i].parent_id)
                            parent = 'square' + response.mosques[i].parent_id;
                        $scope.originalData.push({
                            id: 'mosque' + response.mosques[i].id,
                            parent: parent,
                            text: response.mosques[i].name,
                            // state: {opened: false}
                        });

                        if(response.connected.length > 0){
                            for (var j = 0; j < response.connected.length; j++) {
                                if (response.mosques[i].id == response.connected[j].location_id) {
                                    var objIdx = $scope.originalData.indexOf('mosque' + response.mosques[i].id);
                                    $scope.originalData.splice(objIdx, 1);
                                    $scope.originalData.push({
                                        id: 'mosque' + response.mosques[i].id,
                                        parent: parent,
                                        text:response.mosques[i].name,
                                        state: {selected: true}
                                    });
                                }
                            }
                        }
                    }
                    $scope.treeData = [];
                    angular.copy($scope.originalData,$scope.treeData);
                }else {
                    $rootScope.toastrMessages('error',$filter('translate')("you don't have permission to do this action"));
                    $timeout(function() {
                        $state.go('org-organizations',{"type": 'organizations'});
                    }, 1000);
                }
            });

            $scope.confirm = function () {
                $rootScope.clearToastr();

                var mosques = [];
                var selected_nodes = angular.element('#myTreeId').jstree(true).get_selected();

                for (var i = 0; i < selected_nodes.length; i++) {
                    var substring = 6;
                   if (selected_nodes[i].indexOf('mosque') !== -1)
                        mosques.push(selected_nodes[i].substring(substring));
                }

                $rootScope.progressbar_start();
                Org.resetLocations({mosques: mosques, id: $stateParams.id},function (response) {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                   $state.go('org-organizations',{"type": 'organizations',"action": 'edit',"status": response.status});
                });
            };

            // Tree Config
            $scope.treeConfig = {
                core: {
                    multiple: true,
                    // animation: true,
                    error: function (error) {
                        // $log.error('treeCtrl: error from js tree - ' + angular.toJson(error));
                    },
                    check_callback: true,
                    worker: true,
                    "themes": {
                        "icons": false
                    }
                },
                types: {
                    default: {
                        icon: 'glyphicon glyphicon-flash'
                    },
                    star: {
                        icon: 'glyphicon glyphicon-star'
                    },
                    cloud: {
                        icon: 'glyphicon glyphicon-cloud'
                    }
                },
                version: 1,
                plugins: ['types', 'checkbox']
            };
            $scope.reCreateTree = function () {
                $scope.ignoreChanges = true;
                angular.copy($scope.originalData, $scope.treeData);
                $scope.treeConfig.version++;
            };
            $scope.simulateAsyncData = function () {
                $scope.promise = $timeout(function () {
                    $scope.treeData.push({id: (newId++).toString(), parent: $scope.treeData[0].id, text: 'Async Loaded'})
                }, 3000);
            };
            $scope.addNewNode = function () {
                $scope.treeData.push({id: (newId++).toString(), parent: $scope.newNode.parent, text: vm.newNode.text});
            };
            $scope.setNodeType = function () {
                var item = _.findWhere($scope.treeData, {id: this.selectedNode});
                item.type = this.newType;
                // console.log( 'Changed the type of node ' + this.selectedNode);

            };
            $scope.readyCB = function () {
                $timeout(function () {
                    $scope.ignoreChanges = false;
                    // console.log( ' Js Tree issued the ready event' );
                });
            };
            $scope.createCB = function (e, item) {
                $timeout(function () {
                    // console.log( 'Added new node with the text ' + item.node.text );
                });
            };
            $scope.applyModelChanges = function () {
                return !$scope.ignoreChanges;
            };

        }
        else{
            $rootScope.toastrMessages('error',$filter('translate')('Your are request invalid link , you will be redirected to the Organizations page to search again and request the valid link'));
            $timeout(function() {
                $state.go('org-organizations',{"type": 'organizations'});
            }, 3000);
        }

    });

