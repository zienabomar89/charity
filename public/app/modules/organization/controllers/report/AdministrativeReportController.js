
angular.module('OrganizationModule')
    .controller('AdministrativeReportController' ,function($state,$rootScope, $ngBootbox,$scope, $http, $timeout, $uibModal, $log,AdministrativeReport,$filter,FileUploader, OAuthToken,Org) {

        $rootScope.clearToastr();
        $state.current.data.pageTitle = $filter('translate')('custom forms');

        $scope.master=false;
        $rootScope.items=[];
        $scope.items=[];
        $scope.CurrentPage=1;
        $scope.itemsCount = 50;
        $scope.filters={ "all_organization":false, 'page':1, 'action':"paginate", 'report_type':"", 'title':"", 'notes':"", 'created_at_to':"", 'created_at_from':"",
                        'begin_date_from':"", 'end_date_from':"", 'begin_date_to':"", 'end_date_to':""};

        Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

        $rootScope.years = [];
        var d = new Date();
        var OffsetYear = 2000;
        var CurYear = d.getFullYear();
        var diff = CurYear - OffsetYear ;
        var i;
        var Offset = 2000;
        for (i = 0; i <= diff; i++) {
            $scope.years[i]= { id: Offset , name : "" +Offset+ ""};
            Offset++;
        }

        $rootScope.months = [];
        var months = [ "January", "February", "March", "April", "May", "June", "July", "September", "August", "October", "November", "December"];
        var z;
        var Offset_ = 1;
        for (z = 0; z < 12; z++) {
            $scope.months[z]= { id: Offset_ , name : $filter('translate')(months[z])};
            Offset_++;
        }

        var LoadAdministrativeReport=function(params_){
            // $rootScope.clearToastr();
            var params = angular.copy(params_);
            if( !angular.isUndefined(params.begin_date_from)) {
                params.begin_date_from=$filter('date')(params.begin_date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.end_date_from)) {
                params.end_date_from= $filter('date')(params.end_date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.begin_date_to)) {
                params.begin_date_to=$filter('date')(params.begin_date_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.end_date_to)) {
                params.end_date_to= $filter('date')(params.end_date_to, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(params.created_at_to)) {
                params.created_at_to=$filter('date')(params.created_at_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.created_at_from)) {
                params.created_at_from= $filter('date')(params.created_at_from, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(params.all_organization)) {
                if(params.all_organization == true ||params.all_organization == 'true'){
                    var orgs=[];
                    angular.forEach(params.organization_id, function(v, k) {
                        orgs.push(v.id);
                    });

                    params.organization_ids=orgs;
                    delete params.organization_id;
                    $scope.master=true;

                }else if(params.all_organization == false ||params.all_organization == 'false') {
                    params.organization_ids=[];
                    $scope.master=false
                }
            }

            // angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();
            AdministrativeReport.filter(params,function (response) {
                // angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                $rootScope.progressbar_complete();

                if(params.action == 'paginate'){
                    $scope.items= response.items.data;
                    $scope.CurrentPage = response.items.current_page;
                    $rootScope.TotalItems = response.items.total;
                    $rootScope.ItemsPerPage = response.items.per_page;
                }else{
                    if(response.status=='success') {
                        var file_type='zip';
                          window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                }
            });

        };

        var reLoadReports = function () {
            var params = angular.copy($scope.filters);
            params.page=$scope.CurrentPage;
            params.action='paginate';
            params.itemsCount= $scope.itemsCount ;
            LoadAdministrativeReport(params);
        };

        $scope.resetSearchFilter =function(){
            $scope.filters={ "all_organization":false,'page':1,'action':"paginate",'title':"", 'notes':"", 'created_at_to':"", 'created_at_from':"",
                'begin_date_from':"", 'end_date_from':"", 'begin_date_to':"", 'end_date_to':""
            };
        };

        $scope.pageChanged = function (currentPage) {
            $scope.CurrentPage=currentPage;
            reLoadReports();
        };

        $scope.itemsPerPage_ = function (itemsCount) {
            $scope.CurrentPage=1;
            $scope.itemsCount = itemsCount ;
            reLoadReports();
        };

        $scope.selectedAll = false;
        $scope.selectAll=function(value){
                if (value) {
                    $scope.selectedAll = true;
                } else {
                    $scope.selectedAll = false;
                }

                angular.forEach($scope.items, function(v, k) {
                    v.check=$scope.selectedAll;
                });
        };
        
        $scope.searchReports = function (selected ,filter,action) {
            $scope.CurrentPage=1;
            filter.action = action;
            filter.items=[];

            if(selected == true || selected == 'true' ){
            
                var items =[];
                 angular.forEach($scope.items, function(v, k){

                      if(action == 'ExportToWord' || action == 'ExportToPDF'){
                         if(v.check && v.beneficiary > 0){
                             items.push(v.id);
                         } 
                      }else{
                          if(v.check){
                             items.push(v.id);
                         }
                      }


                 });

                 if(items.length==0){
                     $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                     return ;
                 }
                 filter.items=items;
            }
            LoadAdministrativeReport(filter);
        };

        $scope.delete = function (id) {
            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                .then(function() {
                    $rootScope.progressbar_start();
                    AdministrativeReport.delete({id:id},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            reLoadReports();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                });

        };

        $scope.createOrUpdate = function (item) {

            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: '/app/modules/organization/views/reports/model/form.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,AdministrativeReport) {

                    // Org.list({type:'organizations'},function (response) {  $scope.Org = response; });
                    AdministrativeReport.Org(function (response) {  $scope.organizations = response.organizations; });

                    $scope.row={};

                    if(item == 'null' || item != null){
                        $scope.row = angular.copy(item);
                        // $scope.row.organization_id = $scope.row.organizations;
                   }

                    $scope.confirm = function (item_) {
                        $rootScope.clearToastr();

                        var row = angular.copy(item_);

                        var organizations=[];
                        if( !angular.isUndefined(row.organizations)) {
                            angular.forEach(row.organizations, function(v, k) {
                                organizations.push(v.id);
                            });
                        }

                        row.target_organizations= organizations;

                        if( !angular.isUndefined(row.date_from)) {
                            row.date_from=$filter('date')(row.date_from, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(row.date_to)) {
                            row.date_to= $filter('date')(row.date_to, 'dd-MM-yyyy')
                        }

                        if(row.id){
                            $rootScope.progressbar_start();
                            AdministrativeReport.update(row,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                    {
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    else if(response.status=='failed_valid')
                                    {
                                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                        $scope.status1 =response.status;
                                        $scope.msg1 =response.msg;
                                    }
                                    else{
                                        $modalInstance.close();
                                        if(response.status=='success') {
                                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                            reLoadReports();
                                        }else{
                                            $rootScope.toastrMessages('error',response.msg);
                                        }

                                    }
                                }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                    $modalInstance.close();

                                });  
                        }else {
                            $rootScope.progressbar_start();
                            AdministrativeReport.save(row,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    $modalInstance.close();
                                    if(response.status=='success') {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        reLoadReports();
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }

                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();

                            });
                        }
                        
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    var today = new Date();
                    $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
                    $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};
                    $scope.popup={0:{},1:{opened:false},2:{opened:false}};
                    $scope.open=function (target,$event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        if(target == 1){ $scope.popup[1].opened = true;  }
                        else if(target == 2) { $scope.popup[2].opened = true; }
                    };
                }
            });
        };

        $scope.show = function (item) {
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: '/app/modules/organization/views/reports/model/show.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,AdministrativeReport) {

                    // Org.list({type:'organizations'},function (response) {  $scope.Org = response; });
                    AdministrativeReport.Org(function (response) {  $scope.organizations = response.organizations; });

                    $scope.row={};

                    if(item == 'null' || item != null){
                        $scope.row = angular.copy(item);
                    }

                    $scope.confirm = function (item_) {
                        $rootScope.clearToastr();

                        var row = angular.copy(item_);

                        var organizations=[];
                        if( !angular.isUndefined(row.organizations)) {
                            angular.forEach(row.organizations, function(v, k) {
                                organizations.push(v.id);
                            });
                        }

                        row.target_organizations= organizations;

                        if( !angular.isUndefined(row.date_from)) {
                            row.date_from=$filter('date')(row.date_from, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(row.date_to)) {
                            row.date_to= $filter('date')(row.date_to, 'dd-MM-yyyy')
                        }

                        if(row.id){
                            $rootScope.progressbar_start();
                            AdministrativeReport.update(row,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                    {
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                    else if(response.status=='failed_valid')
                                    {
                                        $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                        $scope.status1 =response.status;
                                        $scope.msg1 =response.msg;
                                    }
                                    else{
                                        $modalInstance.close();
                                        if(response.status=='success') {
                                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                            reLoadReports();
                                        }else{
                                            $rootScope.toastrMessages('error',response.msg);
                                        }

                                    }
                                }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                    $modalInstance.close();

                                });
                        }else {
                            $rootScope.progressbar_start();
                            AdministrativeReport.save(row,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    $modalInstance.close();
                                    if(response.status=='success') {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        reLoadReports();
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }

                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();

                            });
                        }

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    var today = new Date();
                    $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
                    $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};
                    $scope.popup={0:{},1:{opened:false},2:{opened:false}};
                    $scope.open=function (target,$event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        if(target == 1){ $scope.popup[1].opened = true;  }
                        else if(target == 2) { $scope.popup[2].opened = true; }
                    };
                }
            });
        };

        $scope.duplicate = function (item) {
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: '/app/modules/organization/views/reports/model/duplicate.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,AdministrativeReport) {

                    AdministrativeReport.Org(function (response) {  $scope.organizations = response.organizations; });

                    $scope.row = angular.copy(item);

                    $scope.confirm = function (item_) {

                        var row = angular.copy(item_);
                        $rootScope.clearToastr();
                        if(row.id){
                            row.duplicate_id=row.id;
                            delete row.id;
                        }

                        var organizations=[];
                        if( !angular.isUndefined(row.organizations)) {
                            angular.forEach(row.organizations, function(v, k) {
                                organizations.push(v.id);
                            });
                        }

                        row.target_organizations= organizations;


                        if( !angular.isUndefined(row.date_from)) {
                            row.date_from=$filter('date')(row.date_from, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(row.date_to)) {
                            row.date_to= $filter('date')(row.date_to, 'dd-MM-yyyy')
                        }

                        $rootScope.progressbar_start();
                        AdministrativeReport.save(row,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                $modalInstance.close();
                                if(response.status=='success') {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    reLoadReports();
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }

                            }
                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                            $modalInstance.close();

                        });
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    var today = new Date();
                    $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
                    $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};
                    $scope.popup={0:{},1:{opened:false},2:{opened:false}};
                    $scope.open=function (target,$event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        if(target == 1){ $scope.popup[1].opened = true;  }
                        else if(target == 2) { $scope.popup[2].opened = true; }
                    };
                }
            });
        };

        $scope.instruction = function (id) {
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            AdministrativeReport.instruction({'id':id},function (response) {
                $rootScope.progressbar_complete();
                if(response.status=='success') {
                    var file_type='xlsx';
                      window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',response.msg);
                }
            });
        };

        $scope.template = function (id,template) {
            $rootScope.clearToastr();

            if(!angular.isUndefined(template)) {
                if( !(template == null || template == 'null' || template == ' ' || template == '') ) {
                    var sub = template.split(".");
                    var str = sub[0];
                    var token = str.split("/");
                    var download_token = token[1];
                    var type='docx';
                    window.location = '/api/v1.0/common/files/'+type+'/export?download_token='+download_token+'&template=true';
                }
            }


        };

        $scope.excel = function (id) {
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            AdministrativeReport.excel({id:id},function (response) {
                $rootScope.progressbar_complete();
                if(response.status=='success') {
                    var file_type='zip';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',response.msg);
                }
            });
        };

        $scope.word = function (id) {
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            AdministrativeReport.word({'id':id},function (response) {
                $rootScope.progressbar_complete();
                if(response.status=='success') {
                    var file_type='docx';
                      window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',response.msg);
                }
            });
        };

        $scope.ChooseTemplate=function(id,action){
            $rootScope.clearToastr();
            $rootScope.action=action;
            $uibModal.open({
                templateUrl: '/app/modules/organization/views/reports/model/upload_template.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/org/reports/template-upload',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });

                    Uploader.onBeforeUploadItem = function(item) {
                        $rootScope.progressbar_start();
                        item.formData = [{report_id: id}];
                    };

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(!response){
                            $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                        }else{
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            reLoadReports();
                        }
                        $modalInstance.close();

                    };


                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });

        };

        $scope.status=function(field,id,status,index) {
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            AdministrativeReport.status({'action':'single',target:id,status:status ,field:field },function (response) {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                if(response.status == 'success') {
                    if(field == 'status'){
                        $scope.items[index].status = status;
                    }else{
                        $scope.items[index].data_status = status;
                    }
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }

            });
        };


        //-----------------------------------------------------------------------------------------------------------
        $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {$scope.dt = new Date();};
        $scope.today();
        $scope.clear = function() {$scope.dt = null;};
        $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
        $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};

        $scope.popup={0:{},1:{opened:false},2:{opened:false},3:{opened:false},4:{opened:false},5:{opened:false},6:{opened:false}};
        $scope.open=function (target,$event) {
            $event.preventDefault();
            $event.stopPropagation();
            if(target == 1){ $scope.popup[1].opened = true;  }
            else if(target == 2) { $scope.popup[2].opened = true; }
            else if(target == 3) { $scope.popup[3].opened = true; }
            else if(target == 4) { $scope.popup[4].opened = true; }
            else if(target == 5) { $scope.popup[5].opened = true; }
            else if(target == 6) { $scope.popup[6].opened = true; }
        };

        reLoadReports();

    });

angular.module('OrganizationModule')
    .controller('TabController' ,function($state,$stateParams,$rootScope, $ngBootbox,AdministrativeReport,$scope, $http, $timeout, $uibModal, $log,ReportTab,$filter,TabOptions,TabColumns) {

        $state.current.data.pageTitle = $filter('translate')('report tabs');
        $rootScope.row={'tabs':[]};

        $scope.report_category = null;
        $scope.report_id = $stateParams.id;

        var LoadReportTab=function(){
            $rootScope.clearToastr();
            // angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();
            AdministrativeReport.get({id:$stateParams.id},function (response) {
                $rootScope.progressbar_complete();
                // angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                $rootScope.report_category = response.category;
                $rootScope.row= response;
            });

        };

        $scope.delete = function (id) {
            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                .then(function() {
                    $rootScope.progressbar_start();
                    ReportTab.delete({id:id},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            LoadReportTab();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                });

        };

        $scope.excel = function () {
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            AdministrativeReport.excel({'id':$stateParams.id},function (response) {
                $rootScope.progressbar_complete();
                if(response.status=='success') {
                    var file_type='zip';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',response.msg);
                }
            });
        };

        $scope.word = function () {
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            AdministrativeReport.word({'id':$stateParams.id},function (response) {
                $rootScope.progressbar_complete();
                if(response.status=='success') {
                    var file_type='docx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',response.msg);
                }
            });
        };

        $scope.createOrUpdate = function (item) {
            $rootScope.clearToastr();

            var templateUrl =  '/app/modules/organization/views/reports/model/tab-form.html';
            var size =  'lg';
            if($rootScope.report_category == 2 || $rootScope.report_category == '2' ){
                templateUrl =  '/app/modules/organization/views/reports/model/tab-form-custom.html';
                size =  'md';
            }
            $uibModal.open({
                templateUrl: templateUrl ,
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope, $modalInstance, $log,ReportTab) {

                    $scope.row_={};

                    if(item != 'null' || item != null){
                        $scope.row_ = angular.copy(item);
                    }

                    $scope.confirm = function (row) {
                        row.report_id = $stateParams.id;
                        if($rootScope.report_category == 2 || $rootScope.report_category == '2' ){
                            row.type  = 4 ;
                        }
                        $rootScope.clearToastr();
                        if(row.id){
                            $rootScope.progressbar_start();
                            ReportTab.update(row,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    $modalInstance.close();
                                    if(response.status=='success') {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        LoadReportTab();
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }

                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();

                            });
                        }else {
                            $rootScope.progressbar_start();
                            ReportTab.save(row,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    $modalInstance.close();
                                    if(response.status=='success') {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        LoadReportTab();
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }

                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();

                            });
                        }

                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    var today = new Date();
                    $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
                    $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};
                    $scope.popup={0:{},1:{opened:false},2:{opened:false}};
                    $scope.open=function (target,$event) {
                        $event.preventDefault();
                        $event.stopPropagation();
                        if(target == 1){ $scope.popup[1].opened = true;  }
                        else if(target == 2) { $scope.popup[2].opened = true; }
                    };
                }
            });
        };
        $scope.show = function (item) {
            $rootScope.clearToastr();

            $uibModal.open({
                templateUrl: '/app/modules/organization/views/reports/model/show-tab-form.html' ,
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size:  'md',
                controller: function ($rootScope,$scope, $modalInstance, $log,ReportTab) {

                    $scope.row_={};
                    $scope.row_ = angular.copy(item);


                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };

        $scope.elements = function (tab,tab_id,type) {
            $rootScope.clearToastr();

            $uibModal.open({
                templateUrl: '/app/modules/organization/views/reports/model/tab-element.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log) {

                    $scope.tabType=type;
                    $scope.curIndex=null;
                    $scope.showForm = false ;
                    $scope.temp_item = {} ;
                    $scope.item ={};
                    $scope.msg1 ={};
                    $scope.elements =[];

                    $scope.has_option = false;
                    $scope.Options =[];
                    $scope.option = {};
                    $scope.OptionValue =[];
                    $scope.RemovedOptions = [];

                    $scope.tab_elements = function () {
                        ReportTab.elements({id:tab_id},function (data) {
                            $scope.elements=data.elements;
                        });
                    };

                    if($scope.tabType == 3 || $scope.tabType == '3'){
                        $scope.options_ =[
                                      { id: '1',name:$filter('translate')('element_type_number_integer')},
                                      { id: '4',name:$filter('translate')('element_type_number_decimal')}
                            ];
                    }else{
                        $scope.options_ =[
                            { id: '1',name:$filter('translate')('element_type_text')},
                            { id: '2',name:$filter('translate')('element_type_select')},
                            { id: '3',name:$filter('translate')('element_type_textarea')}
                        ];
                    }

                    $scope.create_element = function () {
                        $scope.curIndex=null;
                        $scope.showForm = true;
                        $scope.has_option = false;
                        $scope.action =1;
                        $scope.temp_item = {} ;
                        $scope.item ={};
                        $scope.option = {};

                        if($rootScope.report_category == 2 || $rootScope.report_category == '2' ){
                            $scope.temp_item = {type:'1'} ;
                            $scope.item = {type:'1'} ;
                        }
                    };

                    $scope.edit_element = function (x,index) {
                       $scope.curIndex=index;
                        $scope.showForm = true;
                        x.type = parseInt(x.type);
                        $scope.item = angular.copy(x);
                        $scope.temp_item = angular.copy(x);
                        $scope.action =2;
                        $scope.option = {};

                        $scope.Options = [];
                        if(x.type == 2 ||x.type == '2'){
                            $scope.Options = angular.copy(x.options);
                        }

                        $scope.checkType($scope.item.type);
                    };

                    $scope.cancel_element = function () {
                        $scope.curIndex=null;
                        $scope.has_option = false;
                        $scope.option = {};
                        $scope.showForm = false;
                        $scope.temp_item = {} ;
                        $scope.item ={};
                    };

                    $scope.delete_element = function (option_id) {
                        $rootScope.clearToastr();
                        $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                            .then(function() {
                                $rootScope.progressbar_start();
                                TabOptions.delete({id:option_id},function (response) {
                                    $rootScope.progressbar_complete();
                                    if(response.status=='success') {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        $scope.tab_elements();
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                });
                            });
                    };

                    $scope.checkType = function (type) {
                        if( !angular.isUndefined($scope.msg1.type)) {
                            $scope.msg1.type = [ ];
                        }

                        if(type == 2 || type == '2'){
                            $scope.has_option = true;
                        }else{
                            $scope.has_option = false;
                        }
                    };

                    $scope.newOption = function (option) {
                        $rootScope.clearToastr();
                        var founded=false;
                        angular.forEach($scope.Options, function(v, k) {
                            if(v.value==option.value) {
                                founded=true;
                                return;
                            }
                        });

                        if(founded){
                            $rootScope.toastrMessages('error',$filter('translate')('The value is already used'));
                            $scope.OptionValue='';
                        }else{
                            $scope.Options.push(option);
                            $scope.count= $scope.count+1;
                            $scope.option = {};
                        }

                    };

                    $scope.removeOption = function(index){
                        if($rootScope.action=='edit'){
                            var temp_id=$scope.Options[index].id;
                            var op={id: temp_id };
                            $scope.RemovedOptions.push(op);
                            $scope.Options.splice(index,1);
                        }else{
                            $scope.Options.splice(index,1);
                            $scope.count= $scope.count-1;
                        }
                    };

                    $scope.editOption=function(index){
                        $scope.option = $scope.Options[index];
                        $scope.temp_index=index;
                        $scope.mode=true;
                    };

                    $scope.ConfirmEditOption=function(option){
                        $rootScope.clearToastr();
                        var flag = true;
                        angular.forEach($scope.Options, function(val,key) {
                            if( val.value ==  option.value && $scope.temp_index != key){
                                flag = false;
                                return;
                            }
                        });

                        if(flag){
                            $scope.Options[$scope.temp_index].label=$scope.option.value;
                            $scope.Options[$scope.temp_index].weight=$scope.option.weight;
                            $scope.Options[$scope.temp_index].value=$scope.option.value;
                            $scope.mode=false;
                            $scope.option = {};

                        }else{
                            $rootScope.toastrMessages('error',$filter('translate')('The value to modify is already used'));
                        }
                    };

                    $scope.save_element = function (row) {

                        $rootScope.clearToastr();

                        if( !angular.isUndefined(row.name)) {
                            if(!OnlyLetter(row.name)) {
                                $rootScope.toastrMessages('error',$filter('translate')('The Name Must Only English Letter'));
                                return ;
                            }
                        }

                        var flag = true;
                        if($scope.tabType == 3 || $scope.tabType == '3'){
                            angular.forEach($scope.elements, function(val,key) {
                                if( val.type !=  row.type){
                                    flag = false;
                                    return;
                                }
                            });
                        }

                        if(flag == false){
                            $rootScope.toastrMessages('error',$filter('translate')('all option must be has the same type'));
                            return ;
                        }

                        row.tab_id = tab_id;

                        if($scope.Options.length==0 && ( row.type == 2 || row.type == '2')){
                            $rootScope.toastrMessages('error',$filter('translate')('The is no elements option'));
                            return ;
                        }

                        if($scope.Options.length > 0 && ( row.type == 2 || row.type == '2')){
                            row.options  = $scope.Options ;
                        }


                        if(row.id){
                            if($scope.RemovedOptions.length !=0 && ( row.type == 2 || row.type == 5 ||row.type == 4)){
                                row.Removed_options  = $scope.RemovedOptions ;
                            }

                            $rootScope.progressbar_start();
                            TabOptions.update(row,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    if(response.status=='success') {
                                        $scope.cancel_element();
                                        $scope.tab_elements();
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }

                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();

                            });
                        }else {
                            $rootScope.progressbar_start();
                            TabOptions.save(row,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    if(response.status=='success') {
                                        $scope.cancel_element();
                                        $scope.tab_elements();
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }

                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();
                            });
                        }

                    };

                    $scope.tab_elements();
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };


                }
            });
        };

        $scope.columns = function (tab,tab_id,type) {
            $rootScope.clearToastr();

            $uibModal.open({
                templateUrl: '/app/modules/organization/views/reports/model/tab-columns.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log) {

                    $scope.tabType=type;
                    $scope.curIndex=null;
                    $scope.showForm = false ;
                    $scope.temp_item = {} ;
                    $scope.item ={};
                    $scope.msg1 ={};
                    $scope.columns =[];

                    $scope.has_option = false;
                    $scope.Options =[];
                    $scope.option = {};
                    $scope.OptionValue =[];
                    $scope.RemovedOptions = [];

                    $scope.tab_columns = function () {
                        ReportTab.columns({id:tab_id},function (data) {
                            $scope.columns=data.columns;
                        });
                    };

                    $scope.options_ =[{ id: '1',name:$filter('translate')('element_type_number_integer')},
                                     { id: '2',name:$filter('translate')('element_type_number_decimal')},
                                     { id: '3',name:$filter('translate')('element_type_text')},
                                     { id: '4',name:$filter('translate')('element_type_textarea')},
                                     { id: '6',name:$filter('translate')('element_type_date')},
                                     { id: '5',name:$filter('translate')('element_type_select')}
                                     ];

                    $scope.create_column = function () {
                        $scope.msg1 ={};
                        $scope.curIndex=null;
                        $scope.showForm = true;
                        $scope.has_option = false;
                        $scope.action =1;
                        $scope.temp_item = {} ;
                        $scope.item ={};
                        $scope.option = {};
                        $scope.Options =[];
                    };

                    $scope.edit_column = function (x,index) {
                        $scope.msg1 ={};
                        $scope.curIndex=index;
                        $scope.showForm = true;
                        x.type = parseInt(x.type);
                        $scope.item = angular.copy(x);
                        $scope.temp_item = angular.copy(x);
                        $scope.action =2;
                        $scope.option = {};

                        $scope.Options = [];
                        if(x.type == 5 ||x.type == '5'){
                            $scope.Options = angular.copy(x.options);
                        }
                        $scope.checkType($scope.item.type);
                    };

                    $scope.cancel_column = function () {
                        $scope.curIndex=null;
                        $scope.msg1 ={};
                        $scope.has_option = false;
                        $scope.showForm = false;
                        $scope.temp_item = {} ;
                        $scope.item ={};
                        $scope.Options =[];
                        $scope.option = {};
                    };

                    $scope.delete_column = function (option_id) {
                        $rootScope.clearToastr();
                        $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                            .then(function() {
                                $rootScope.progressbar_start();
                                TabColumns.delete({id:option_id},function (response) {
                                    $rootScope.progressbar_complete();
                                    if(response.status=='success') {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        $scope.tab_columns();
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }
                                });
                            });
                    };

                    $scope.checkType = function (type) {
                        if( !angular.isUndefined($scope.msg1.type)) {
                            $scope.msg1.type = [ ];
                        }

                        if(type == 5 || type == '5'){
                            $scope.has_option = true;
                        }else{
                            $scope.has_option = false;
                        }
                    };

                    $scope.newOption = function (option) {
                        $rootScope.clearToastr();
                        var founded=false;
                        angular.forEach($scope.Options, function(v, k) {
                            if(v.value==option.value) {
                                founded=true;
                                return;
                            }
                        });

                        if(founded){
                            $rootScope.toastrMessages('error',$filter('translate')('The value is already used'));
                            $scope.OptionValue='';
                        }else{
                            $scope.Options.push(option);
                            $scope.count= $scope.count+1;
                            $scope.option = {};
                        }

                    };

                    $scope.removeOption = function(index){
                        if($rootScope.action=='edit'){
                            var temp_id=$scope.Options[index].id;
                            var op={id: temp_id };
                            $scope.RemovedOptions.push(op);
                            $scope.Options.splice(index,1);
                        }else{
                            $scope.Options.splice(index,1);
                            $scope.count= $scope.count-1;
                        }
                    };

                    $scope.editOption=function(index){
                        $scope.option = $scope.Options[index];
                        $scope.temp_index=index;
                        $scope.mode=true;
                    };

                    $scope.ConfirmEditOption=function(option){
                        $rootScope.clearToastr();
                        var flag = true;
                        angular.forEach($scope.Options, function(val,key) {
                            if( val.value ==  option.value && $scope.temp_index != key){
                                flag = false;
                                return;
                            }
                        });

                        if(flag){
                            $scope.Options[$scope.temp_index].label=$scope.option.value;
                            $scope.Options[$scope.temp_index].weight=$scope.option.weight;
                            $scope.Options[$scope.temp_index].value=$scope.option.value;
                            $scope.mode=false;
                            $scope.option = {};

                        }else{
                            $rootScope.toastrMessages('error',$filter('translate')('The value to modify is already used'));
                        }
                    };

                    $scope.save_column = function (row) {

                        $rootScope.clearToastr();

                        if( !angular.isUndefined(row.name)) {
                            if(!OnlyLetter(row.name)) {
                                $rootScope.toastrMessages('error',$filter('translate')('The Name Must Only English Letter'));
                                return ;
                            }
                        }

                        row.tab_id = tab_id;

                        if($scope.Options.length==0 && ( row.type == 5 || row.type == '5')){
                            $rootScope.toastrMessages('error',$filter('translate')('The is no elements option'));
                            return ;
                        }

                        if($scope.Options.length > 0 && ( row.type == 5 || row.type == '5')){
                            row.options  = $scope.Options ;
                        }

                        if(row.id){
                            $rootScope.progressbar_start();
                            TabColumns.update(row,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    if(response.status=='success') {
                                        $scope.cancel_column();
                                        $scope.tab_columns();
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }

                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();

                            });
                        }else {
                            $rootScope.progressbar_start();
                            TabColumns.save(row,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    if(response.status=='success') {
                                        $scope.cancel_column();
                                        $scope.tab_columns();
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }

                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();
                            });
                        }

                    };

                    $scope.tab_columns();
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };


                }
            });
        };

        LoadReportTab();

        $scope.status=function(id,status,index) {
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            AdministrativeReport.OrgStatus({'action':'single',target:id,status:status },function (response) {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                if(response.status == 'success') {
                    $scope.items[index].status = status;
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }

            });
        };
    });

angular.module('OrganizationModule')
    .controller('ShowReportController' ,function($state,$stateParams,$rootScope, $ngBootbox,AdministrativeReport,$scope, $http, $timeout, $uibModal, $log,ReportTab,$filter) {

        $state.current.data.pageTitle = $filter('translate')('report tabs');
        $scope.row={};

        $scope.report_id = $stateParams.id;
        var LoadReportTab=function(){
            $rootScope.clearToastr();
            // angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();
            AdministrativeReport.get({id:$stateParams.id},function (response) {
                $rootScope.progressbar_complete();
                // angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                $scope.row= response;
            });
        };
        LoadReportTab();

    });

angular.module('OrganizationModule')
    .controller('ReportOrgController' ,function($state,$stateParams,$rootScope, $ngBootbox,AdministrativeReport,$scope, $http, $timeout, $uibModal, $log,ReportTab,$filter) {


        $state.current.data.pageTitle = $filter('translate')('show_report_data');
        $scope.row={};

        $scope.report_id = $stateParams.id;
        $rootScope.clearToastr();
        // angular.element('.btn').addClass("disabled");
        $rootScope.progressbar_start();

        var loadOrganization= function(){
            AdministrativeReport.organizations({id:$stateParams.id},function (response) {
                $rootScope.progressbar_complete();
                // angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                $scope.row= response;
            });
        }

        $scope.allCheckbox=function(value){
            if (value) {
                $scope.selectedAll = true;
            } else {
                $scope.selectedAll = false;
            }

            angular.forEach($scope.row.organizations, function (v, k) {
                v.check=$scope.selectedAll;
            });
        };

        $scope.excel = function (target,id) {

            $rootScope.clearToastr();
            if(target == 'main'){
                $rootScope.progressbar_start();
                AdministrativeReport.excel({'id':$stateParams.id},function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status=='success') {
                        window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token;
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                });
            }else if(target == 'single'){
                AdministrativeReport.excelOrg({'report_id':$stateParams.id,'target':'group','organizations':[id]},function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status=='success') {
                        window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token;
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                });
            }else{

                var organizations  =[];

                angular.forEach($scope.row.organizations, function (v, k) {
                    if (v.check) {
                        organizations.push(v.organization_id);
                    }
                });
                if(organizations.length==0){
                    $rootScope.toastrMessages('error',$filter('translate')("you did not select organizations to export file"));
                    return;
                }else{
                    AdministrativeReport.excelOrg({'report_id':$stateParams.id,'target':'group','organizations':organizations},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                            window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token;
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                }

            }

        };

        $scope.word = function (target,id) {

            $rootScope.clearToastr();
            if(target == 'main'){
                $rootScope.progressbar_start();
                AdministrativeReport.word({'id':$stateParams.id},function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status=='success') {
                        var file_type='docx';
                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                });
            }else if(target == 'single'){
                AdministrativeReport.wordOrg({'report_id':$stateParams.id,'target':'group','organizations':[id]},function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status=='success') {
                        var file_type='zip';
                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                });
            }else{

                var organizations  =[];

                angular.forEach($scope.row.organizations, function (v, k) {
                    if (v.check) {
                        organizations.push(v.organization_id);
                    }
                });
                if(organizations.length==0){
                    $rootScope.toastrMessages('error',$filter('translate')("you did not select organizations to export file"));
                    return;
                }else{
                    AdministrativeReport.wordOrg({'report_id':$stateParams.id,'target':'group','organizations':organizations},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                            var file_type='zip';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                }
            }

        };

        $scope.changeOrgUser=function(user_id,organization_id){
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: '/app/modules/organization/views/reports/model/change_user.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'md',
                controller: function ($rootScope,$scope, $modalInstance, $log,AdministrativeReport) {

                    $scope.row={};
                    $scope.users =[];
                    AdministrativeReport.usersList({report_id:$stateParams.id ,user_id:user_id ,organization_id:organization_id} ,
                        function (response) {
                        $scope.users = response.users;
                    });

                    $scope.confirm = function () {

                        $rootScope.progressbar_start();
                        AdministrativeReport.setUser({report_id:$stateParams.id ,user_id:$scope.row.user,organization_id:organization_id},function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else{
                                $modalInstance.close();
                                if(response.status=='success') {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    loadOrganization();
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }

                            }
                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                            $modalInstance.close();
                        });
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                }
            });
        };
        loadOrganization();

    });

angular.module('OrganizationModule')
    .controller('ShowOrgReportController' ,function($state,$stateParams,$rootScope, $ngBootbox,AdministrativeReport,$scope, $http, $timeout, $uibModal, $log,ReportTab,$filter) {

        $state.current.data.pageTitle = $filter('translate')('report tabs');
        $scope.row={};

        $scope.report_id = $stateParams.id;
        $scope.organization_id = $stateParams.organization_id;
        $rootScope.clearToastr();
        // angular.element('.btn').addClass("disabled");
        $rootScope.progressbar_start();
        AdministrativeReport.getTargetOrgData({id:$stateParams.id,organization_id:$stateParams.organization_id},function (response) {
            $rootScope.progressbar_complete();
            // angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            $scope.row= response;
        });

    });
