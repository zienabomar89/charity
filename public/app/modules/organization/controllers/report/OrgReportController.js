
angular.module('OrganizationModule')
    .controller('OrgReportController' ,function($state,$stateParams,$rootScope, $ngBootbox,$scope, $http, $timeout, $uibModal, $log,AdministrativeReport,$filter,Org) {

        $state.current.data.pageTitle = $filter('translate')('custom forms');

        $scope.master=false;
        $rootScope.items=[];
        $scope.items=[];
        $scope.CurrentPage=1;
        $scope.itemsCount = 50;
        $scope.filters={ "all_organization":false, 'page':1, 'action':"paginate", 'report_type':"", 'title':"", 'notes':"", 'created_at_to':"", 'created_at_from':"",
            'begin_date_from':"", 'end_date_from':"", 'begin_date_to':"", 'end_date_to':""};

        Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

        $rootScope.years = [];
        var d = new Date();
        var OffsetYear = 2000;
        var CurYear = d.getFullYear();
        var diff = CurYear - OffsetYear ;
        var i;
        var Offset = 2000;
        for (i = 0; i <= diff; i++) {
            $scope.years[i]= { id: Offset , name : "" +Offset+ ""};
            Offset++;
        }

        $rootScope.months = [];
        var months = [ "January", "February", "March", "April", "May", "June", "July", "September", "August", "October", "November", "December"];
        var z;
        var Offset_ = 1;
        for (z = 0; z < 12; z++) {
            $scope.months[z]= { id: Offset_ , name : $filter('translate')(months[z])};
            Offset_++;
        }

        if(!$stateParams.msg || ($stateParams.msg == null || $stateParams.msg == 'null')){
            $rootScope.clearToastr();
        }

        var LoadOrgReport=function(params_){

            var params = angular.copy(params_);
            if( !angular.isUndefined(params.begin_date_from)) {
                params.begin_date_from=$filter('date')(params.begin_date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.end_date_from)) {
                params.end_date_from= $filter('date')(params.end_date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.begin_date_to)) {
                params.begin_date_to=$filter('date')(params.begin_date_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.end_date_to)) {
                params.end_date_to= $filter('date')(params.end_date_to, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(params.created_at_to)) {
                params.created_at_to=$filter('date')(params.created_at_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(params.created_at_from)) {
                params.created_at_from= $filter('date')(params.created_at_from, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(params.all_organization)) {
                if(params.all_organization == true ||params.all_organization == 'true'){
                    var orgs=[];
                    angular.forEach(params.organization_id, function(v, k) {
                        orgs.push(v.id);
                    });

                    params.organization_ids=orgs;
                    delete params.organization_id;
                    $scope.master=true;

                }else if(params.all_organization == false ||params.all_organization == 'false') {
                    params.organization_ids=[];
                    $scope.master=false
                }
            }

            // angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();
            AdministrativeReport.OrgReport(params,function (response) {
                $rootScope.progressbar_complete();
                // angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                $scope.items= response.items.data;
                $scope.CurrentPage = response.items.current_page;
                $rootScope.TotalItems = response.items.total;
                $rootScope.ItemsPerPage = response.items.per_page;
            });

        };

        var reLoadReports = function () {
            var params = angular.copy($scope.filters);
            params.page=$scope.CurrentPage;
            params.action='paginate';
            params.itemsCount= $scope.itemsCount ;
            LoadOrgReport(params);
        };

        $scope.resetSearchFilter =function(){
            $scope.filters={ "all_organization":false, 'page':1, 'action':"paginate", 'report_type':"", 'title':"", 'notes':"", 'created_at_to':"", 'created_at_from':"",
                'begin_date_from':"", 'end_date_from':"", 'begin_date_to':"", 'end_date_to':""};

        };

        $scope.pageChanged = function (currentPage) {
            $scope.CurrentPage=currentPage;
            $rootScope.clearToastr();
            reLoadReports();
        };

        $rootScope.itemsPerPage_ = function (itemsCount) {
            $scope.CurrentPage=1;
            $scope.itemsCount = itemsCount ;
            reLoadReports();
        };

        $rootScope.searchReports = function (filter) {
            $scope.CurrentPage=1;
            LoadOrgReport(filter);
        };

        $scope.delete = function (id) {
            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                .then(function() {
                    $rootScope.progressbar_start();
                    AdministrativeReport.delete({id:id},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            reLoadReports();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                });

        };

        $scope.word = function (id) {
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            AdministrativeReport.wordOrg({'report_id':id,'target':'user'},function (response) {
                $rootScope.progressbar_complete();
                if(response.status=='success') {
                    var file_type='zip';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',response.msg);
                }
            });
        };

        $scope.excel = function (id) {
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            AdministrativeReport.excelOrg({report_id:id,'target':'user'},function (response) {
                $rootScope.progressbar_complete();
                if(response.status=='success') {
                    var file_type='zip';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',response.msg);
                }
            });
        };

        reLoadReports();

        $scope.status=function(id,status,index) {
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            AdministrativeReport.OrgStatus({'action':'single',target:id,status:status},function (response) {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                if(response.status == 'success') {
                    $scope.items[index].report_organization_status = status;
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }

            });
        };

        //-----------------------------------------------------------------------------------------------------------
        $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {$scope.dt = new Date();};
        $scope.today();
        $scope.clear = function() {$scope.dt = null;};
        $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
        $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};

        $scope.popup={0:{},1:{opened:false},2:{opened:false},3:{opened:false},4:{opened:false},5:{opened:false},6:{opened:false}};
        $scope.open=function (target,$event) {
            $event.preventDefault();
            $event.stopPropagation();
            if(target == 1){ $scope.popup[1].opened = true;  }
            else if(target == 2) { $scope.popup[2].opened = true; }
            else if(target == 3) { $scope.popup[3].opened = true; }
            else if(target == 4) { $scope.popup[4].opened = true; }
            else if(target == 5) { $scope.popup[5].opened = true; }
            else if(target == 6) { $scope.popup[6].opened = true; }
        };

    });

angular.module('OrganizationModule')
    .directive("formatDate", function(){
        return {
            require: 'ngModel',
            link: function(scope, elem, attr, modelCtrl) {
                modelCtrl.$formatters.push(function(modelValue){
                    return new Date(modelValue);
                })
            }
        }
    })
    .controller('OrgReportFormController' ,function($state,$stateParams,$rootScope, $ngBootbox,AdministrativeReport,$scope, $http, $timeout, $uibModal, $log,ReportTab,$filter) {

        $state.current.data.pageTitle = $filter('translate')('report tabs');
        $scope.curIndex=0;
        $scope.row={};
        $scope.status1 ='';
        $scope.msg1={};

        $scope.popup={0:{}};

        var today = new Date();
        $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {$scope.dt = new Date();};
        $scope.today();
        $scope.clear = function() {$scope.dt = null;};
        $rootScope.dateOptions1 = {formatYear: 'yy', startingDay: 0};
        $rootScope.dateOptions2 = {formatYear: 'yy', startingDay: 0};

        $scope.select=function (date) {
            alert('ddddd');

        }
        $scope.popup_open=function (col,option) {
            col = col + "" ;
            option = option + "" ;
            index = col + '-'+ option  ;
            return $scope.popup[index].opened;
        };

        $scope.open=function (col,option,$event) {
            $event.preventDefault();
            $event.stopPropagation();
            col = col + "" ;
            option = option + "" ;
            index = col + '-'+ option  ;
            $scope.popup[index].opened = true;
        };
        var floatRound = function(number, precision) {

            if(number != 0 || number != '0'){
                var factor = Math.pow(10, precision);
                var tempNumber = number * factor;
                var roundedTempNumber = Math.round(tempNumber);
                return roundedTempNumber / factor;
            }
            return 0;

        };
        var LoadReportTab=function(){
            $rootScope.clearToastr();
            // angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();
            AdministrativeReport.getOrgData({id:$stateParams.id},function (response) {
                $rootScope.progressbar_complete();
                // angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                var temp = response;

                angular.forEach(temp.tabs, function(val,key) {
                    if(val.options.length > 0 ){
                        var tp = val.options[0].type;

                        if(tp == 1 ){
                            val.input_type = $filter('translate')('element_type_number_integer');
                        }else{
                            val.input_type = $filter('translate')('element_type_number_decimal');
                        }
                        val.input_type_id = tp;
                    }

                    if(val.columns.length > 0 ){
                        angular.forEach(val.columns, function(val_,key_) {
                            if(val_.type == 6){
                                angular.forEach(val_.data, function(vl_,ky_) {
                                    if(val_.type == 6){
                                        if(!(vl_ == null || vl_ == 'null')){
                                            var tp = new Date(vl_);
                                            vl_ = tp;
                                        }
                                        col_index = val_.id +"" ;
                                        field_index = ky_  +"" ;
                                        index = col_index +'-'+ field_index  +"" ;
                                        $scope.popup[index]={opened:false};
                                    }
                                });
                            }
                        });
                    }
                });

                $scope.row = temp;
            });
        };
        LoadReportTab();


        $scope.save=function(index,options,col,tab_type){

            $rootScope.clearToastr();
            $scope.status1 ='';
            $scope.msg1={};
            $rootScope.progressbar_start();

            if($scope.row.category == 2 || $scope.row.category == '2'){
                var temp_columns = angular.copy(col);
                angular.forEach(temp_columns, function(val_,key_) {
                    if(val_.type == 6){
                        angular.forEach(val_.data, function(vl_,ky_) {
                            if(val_.type == 6){
                                if(!(vl_ == null || vl_ == 'null')){
                                    dt = new Date(vl_);
                                    tp = $filter('date')(dt, 'yyyy/MM/dd');
                                    val_.data[ky_] = tp;
                                }
                            }else if (val_.type == 1 ||val_.type == 2){
                                if((vl_ == null || vl_ == 'null' || vl_ == '' || vl_ == ' ')){
                                    vl_ = 0;
                                }
                            }
                        });
                    }
                });
                columns=temp_columns;
            }else{
                columns=[];
            }

            AdministrativeReport.OrgData({'report_id':$stateParams.id,'options':options,'columns':columns,'category':$scope.row.category,'tab_type':tab_type},function (response) {
                $rootScope.progressbar_complete();
                if(response.status=='failed_valid') {
                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                    if($scope.row.category == 1 || $scope.row.category == '1'){
                        $scope.row.tabs[index].options =  response.options;
                    }else{
                        $scope.columns_required =  response.columns;

                    }
                }
                else {
                    if(response.status=='success') {
                        $rootScope.toastrMessages('success',$filter('translate')('The row is save successfully'));

                        var step_diff = $scope.row.tabs.length - index ;
                        if(step_diff == 1){
                            $timeout(function() {
                            $state.go('org-administrative-report',{"msg":"true"});
                            },1000);
                        }else{
                            $scope.fetch (index + 1);

                        }
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }

                }
            });
        };

        $scope.fetch=function(index){

            $timeout(function() {
                var data = angular.copy($scope.row.tabs);

                if(!angular.isUndefined(data[index])) {
                    data[$scope.curIndex].selected =  false;
                    data[$scope.curIndex].disable =  false;

                    data[index].selected =  true;
                    data[index].disable =  false;
                    $scope.row.tabs = data;
                    $scope.curIndex = index;
                }
            });


        };

        $scope.UpdateTotal = function (target,field,value,tb) {

            if(angular.isUndefined(field.value)) {
                field.value = 0;
            }else{
                if(isNaN(field.value) || field.value == '' || field.value == ' ' ||
                   field.value == null || field.value == 'null'){
                    field.value = 0;

                }
            }

            if(angular.isUndefined(field.amount)) {
                field.amount = 0;
            }else{
                if(isNaN(field.amount) || field.amount == '' || field.amount == ' ' ||
                   field.amount == null || field.amount == 'null'){
                    field.amount = 0;

                }
            }

            var sum_value = 0;
            var sum_amount = 0;

            angular.forEach(tb.options, function (v, k) {
                sum_value += parseFloat(v.value);
                sum_amount += floatRound(v.amount,2);
                // sum_amount += parseFloat(v.amount).toFixed(2);
            });
            tb.sum_value = parseFloat(sum_value);
            tb.sum_amount = floatRound(sum_amount,2);
        }

        $scope.UpdateSum = function (column) {

            var sum = 0;
            angular.forEach(column.data, function (v, k) {
                if(angular.isUndefined(v)) {
                    v = 0;
                }else{
                    if(isNaN(v) || v == '' || v == ' ' ||
                        v == null || v == 'null'){
                        v = 0;
                    }
                }

                if(column.type == 1){
                    sum += parseInt(v);
                }else {
                    sum += parseFloat(v);
                }

            });

            column.sum = sum;

        }


    });


angular.module('OrganizationModule')
    .controller('OrgReportListController' ,function($state,$stateParams,$rootScope, $ngBootbox,AdministrativeReport,$scope, $http, $timeout, $uibModal, $log,ReportTab,$filter) {

        $state.current.data.pageTitle = $filter('translate')('show_report_data');
        $scope.row={};

        $scope.report_id = $stateParams.id;
        $rootScope.clearToastr();
        // angular.element('.btn').addClass("disabled");
        $rootScope.progressbar_start();
        AdministrativeReport.related({id:$stateParams.id},function (response) {
            $rootScope.progressbar_complete();
            // angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            $scope.row= response;
        });

        $scope.allCheckbox=function(value){
            if (value) {
                $scope.selectedAll = true;
            } else {
                $scope.selectedAll = false;
            }

            angular.forEach($scope.row.organizations, function (v, k) {
                v.check=$scope.selectedAll;
            });
        };

        $scope.excel = function (target,id) {

            $rootScope.clearToastr();
            if(target == 'main'){
                $rootScope.progressbar_start();
                AdministrativeReport.excelRelated({'id':$stateParams.id},function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status=='success') {
                        window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token;
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                });
            }else if(target == 'single'){
                AdministrativeReport.excelOrg({'report_id':$stateParams.id,'target':'group','organizations':[id]},function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status=='success') {
                        window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token;
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                });
            }else{

                var organizations  =[];

                angular.forEach($scope.row.organizations, function (v, k) {
                    if (v.check) {
                        organizations.push(v.organization_id);
                    }
                });
                if(organizations.length==0){
                    $rootScope.toastrMessages('error',$filter('translate')("you did not select organizations to export file"));
                    return;
                }else{
                    AdministrativeReport.excelOrg({'report_id':$stateParams.id,'target':'group','organizations':organizations},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                            window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token;
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                }

            }

        };

        $scope.word = function (target,id) {

            $rootScope.clearToastr();
            if(target == 'main'){
                $rootScope.progressbar_start();
                AdministrativeReport.wordRelated({'id':$stateParams.id},function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status=='success') {
                        var file_type='docx';
                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                });
            }else if(target == 'single'){
                AdministrativeReport.wordOrg({'report_id':$stateParams.id,'target':'group','organizations':[id]},function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status=='success') {
                        var file_type='zip';
                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                    }else{
                        $rootScope.toastrMessages('error',response.msg);
                    }
                });
            }else{

                var organizations  =[];

                angular.forEach($scope.row.organizations, function (v, k) {
                    if (v.check) {
                        organizations.push(v.organization_id);
                    }
                });
                if(organizations.length==0){
                    $rootScope.toastrMessages('error',$filter('translate')("you did not select organizations to export file"));
                    return;
                }else{
                    AdministrativeReport.wordOrg({'report_id':$stateParams.id,'target':'group','organizations':organizations},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                            var file_type='zip';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });
                }
            }

        };

    });
