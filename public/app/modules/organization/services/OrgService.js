OrganizationModule = angular.module('OrganizationModule');

OrganizationModule
    .factory('Org', function ($resource) {
    return $resource('/api/v1.0/org/:type/:id', {id: '@id', type: '@type'}, {
        query: {
            method: 'GET',
            isArray: false
        },
        all: {
            url: "/api/v1.0/org/organizations/all",
            method: 'GET',
            isArray: true
        },
        filter: {
            url: "/api/v1.0/org/organizations/filter",
            method: 'POST',
            isArray: false
        },
        paginate: {
            url: "/api/v1.0/org/:type/paginate",
            method: 'POST',
            isArray: false
        },        
        trash: {
            method: 'POST',
            url: "/api/v1.0/org/:type/trash/:id",
            isArray: false
        },
        restore: {
            method: 'PUT',
            url: "/api/v1.0/org/:type/restore/:id",
            isArray: false
        },
        Org: {
            method: 'GET',
            url: "/api/v1.0/org/organizations/Org",
            isArray: true
        },
        compositeList: {
            method: 'GET',
            url: "/api/v1.0/org/:type/compositeList",
            params: {type: '@type'},
            isArray: true
        },
        list: {
            method: 'GET',
            url: "/api/v1.0/org/:type/list",
            params: {type: '@type'},
            isArray: true
        },
        update: {
            method: 'PUT'
        },
        updateRepositoryShare: {
            method: 'PUT',
            url: "/api/v1.0/org/organizations/repository-share"
        },
        descendants: {
            method: 'GET',
            url: "/api/v1.0/org/:type/descendants",
            params: {type: '@type'},
            isArray: true
        },
        containerShare: {
            method: 'GET',
            url: "/api/v1.0/org/organizations/:id/containerShare",
            params: {type: '@type'},
            isArray: false
        },
        getSponsors: {
            method: 'GET',
            url: "/api/v1.0/org/organizations/:id/getSponsors",
            params: {},
            isArray: false
        },
        resetSponsors: {
            method: 'PUT',
            url: "/api/v1.0/org/organizations/:id/resetSponsors",
            params: {},
            isArray: false
        },

        getLocations: {
            method: 'GET',
            url: "/api/v1.0/org/organizations/:id/getLocations",
            params: {},
            isArray: false
        },
        resetLocations: {
            method: 'PUT',
            url: "/api/v1.0/org/organizations/:id/resetLocations",
            params: {},
            isArray: false
        },
        ListOfCategory: {
            method: 'Post',
            url: "/api/v1.0/org/organizations/ListOfCategory",
            params: {},
            isArray: false
        }
    });
})

    .factory('organizationProjects', function ($resource) {
        return $resource('/api/v1.0/org/projects/:operation/:id', {id: '@id',page: '@page'}, {
            filter: { method:'POST' ,params:{operation:'filter'}, isArray:false },
            update:{method: 'PUT'  , params: {id: '@id'}, isArray: false},
            fetch   :{method:'GET' ,params:{operation:'fetch'}, isArray: false},
            restore   :{method:'PUT' ,params:{operation:'restore'}, isArray: false},
            attachments   :{method:'GET' ,params:{operation:'attachments'}, isArray: false},
            saveAttachments:{method: 'PUT'  , params: {operation:'saveAttachments'}, isArray: false},
            deleteAttachments   :{method:'GET' ,params:{operation:'deleteAttachments'}, isArray: false},            
        });
    })  ;
    