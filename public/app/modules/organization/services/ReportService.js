OrganizationModule = angular.module('OrganizationModule');

OrganizationModule
    .factory('AdministrativeReport', function ($resource) {
        return $resource('/api/v1.0/org/reports/:operation/:id', {id: '@id',page: '@page'}, {
            OrgReport: { method:'POST' ,params:{operation:'OrgReport'}, isArray:false },
            filter:    { method:'POST' ,params:{operation:'filter'}, isArray:false },
            OrgData:   { method:'POST' ,params:{operation:'OrgData'}, isArray:false },
            status:    { method:'POST' ,params:{operation:'status'}, isArray:false },
            OrgStatus:    { method:'POST' ,params:{operation:'OrgStatus'}, isArray:false },

            excel:    { method:'GET' ,params:{operation:'excel'}, isArray:false },
            excelOrg: { method:'POST' ,params:{operation:'excelOrg'}, isArray:false },
            excelRelated:    { method:'GET' ,params:{operation:'excelRelated'}, isArray:false },

            word:     { method:'GET' ,params:{operation:'word'}, isArray:false },
            wordOrg:  { method:'POST' ,params:{operation:'wordOrg'}, isArray:false },
            wordRelated:  { method:'GET' ,params:{operation:'wordRelated'}, isArray:false },

            tabs:   {method:'GET' ,params:{operation:'tabs'}, isArray:false },
            Org: { method:'GET' ,params:{operation:'Org'}, isArray:false },

            getOrgData: { method:'GET' ,params:{operation:'getOrgData'}, isArray:false },
            instruction: { method:'GET' ,params:{operation:'instruction'}, isArray:false },
            organizations: { method:'GET' ,params:{operation:'organizations'}, isArray:false },
            getTargetOrgData: { method:'GET' ,params:{operation:'getTargetOrgData'}, isArray:false },

            related: { method:'GET' ,params:{operation:'related'}, isArray:false },

            update: {method: 'PUT'  , params: {id: '@id'}, isArray: false},

            usersList:  { method:'POST' ,params:{operation:'usersList'}, isArray:false },
            setUser:  { method:'POST' ,params:{operation:'setUser'}, isArray:false },

        });
    })
    .factory('ReportTab', function ($resource) {
        return $resource('/api/v1.0/org/reports/tabs/:operation/:id', {id: '@id',page: '@page'}, {
            update:{method: 'PUT'  , params: {id: '@id'}, isArray: false},
            elements: { method:'GET' ,params:{operation:'elements'}, isArray:false },
            columns: { method:'GET' ,params:{operation:'columns'}, isArray:false }
        });
    })
    .factory('TabOptions', function ($resource) {
        return $resource('/api/v1.0/org/reports/options/:operation/:id', {id: '@id',page: '@page'}, {
            update:{method: 'PUT'  , params: {id: '@id'}, isArray: false}
        });
    })
    .factory('TabColumns', function ($resource) {
        return $resource('/api/v1.0/org/reports/columns/:operation/:id', {id: '@id',page: '@page'}, {
            update:{method: 'PUT'  , params: {id: '@id'}, isArray: false}
        })
    });
