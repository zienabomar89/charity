

angular.module('FormsModule')
    .factory('form_elements', function ($resource) {
        return $resource('/api/v1.0/forms/form_elements/:operation/:id/', {page: '@page',id: '@id'}, {
            getFormElements:{ method:'GET',params:{operation:'getFormElements',id: '@id'}, isArray:false },
            getElements:{ method:'GET',params:{operation:'getElements',id: '@id'}, isArray:false },
            update:{method: 'PUT'  , params: {id: '@id'}, isArray: false},
            getElementName     : { method:'GET' ,params:{operation:'getElementName',id: '@id'}, isArray:false },
            deleteOption:{method:'DELETE', params: {operation:'deleteOption',id: '@id'}, isArray: false}

        });
    });