

angular.module('FormsModule')
    .factory('forms_case_data', function ($resource) {
        return $resource('/api/v1.0/forms/forms_case_data/:operation/:id', {id: '@id',page: '@page',form_id: '@form_id'}, {
            update:{method: 'PUT'  , params: {id: '@id'}, isArray: false},
            showFormCaseData     : { method:'GET' ,params:{operation:'showFormCaseData',id: '@id',form_id: '@form_id'}, isArray:false },
            updateFormCaseData:{method: 'PUT'  , params: {operation:'updateFormCaseData',id: '@id'}, isArray: false},
            saveCaseData:   {method: 'POST' , params: {operation:'saveCaseData'}, isArray: false}
        });
    });
