
angular.module('FormsModule')
    .factory('custom_forms', function ($resource) {
        return $resource('/api/v1.0/forms/custom_forms/:operation/:id', {id: '@id',page: '@page'}, {
            update:{method: 'PUT'  , params: {id: '@id'}, isArray: false},
            filter     : { method:'POST' ,params:{operation:'filter',page: '@page'}, isArray:false },
            getCustomFormsList     : { method:'GET' ,params:{operation:'getCustomFormsList'}, isArray:true },
            restore     : { method:'GET' ,params:{operation:'restore',id: '@id'}, isArray:false },
            getFormName     : { method:'GET' ,params:{operation:'getFormName',id: '@id'}, isArray:false }

        });
    });
