

angular.module('FormsModule')
    .controller('CustomFormsController' ,function($state,$stateParams,$rootScope, $ngBootbox,$scope, $http, $timeout, $uibModal, $log,custom_forms,$filter) {

        $state.current.data.pageTitle = $filter('translate')('custom forms');

        $scope.master=false;
        $rootScope.items=[];

        $scope.type ='main';

        if($stateParams.type){
            $scope.type =$stateParams.type;
        }

        var LoadCustomForm=function($param){
            $param.type = $scope.type;
            $rootScope.progressbar_start();
            custom_forms.filter($param,function (response) {
                $rootScope.progressbar_complete();
                $scope.master=response.master;
                var temp= response.result.data;
                angular.forEach(temp, function(v, k) {
                    v.status=parseInt(v.status);
                });

                $rootScope.items= temp;
                $scope.CurrentPage = response.result.current_page;
                $rootScope.TotalItems = response.result.total;
                $rootScope.ItemsPerPage = response.result.per_page;
            });

        };


        LoadCustomForm({page:1});

        $rootScope.pageChanged = function (CurrentPage) {
            LoadCustomForm({page:CurrentPage});
        };

        $rootScope.itemsPerPage_ = function (itemsCount) {
            LoadCustomForm({page: 1,itemsCount:itemsCount});
        };


        $rootScope.showElements=function(id){
            $state.go('form-elements',{"form_id": id});
        };

        $scope.insertCustomForms = function (size) {
            $rootScope.clearToastr();
            $uibModal.open({
                templateUrl: 'myModalContent.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope, $modalInstance, $log,custom_forms) {
                    $scope.confirmInsert = function (customformdata) {
                        $rootScope.clearToastr();
                      //  customformdata.page=$rootScope.CurrentPage;
                        $rootScope.progressbar_start();
                        custom_forms.save( customformdata , function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed')
                                {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status=='failed_valid')
                                {
                                    $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                    $scope.status1 =response.status;
                                    $scope.msg1 =response.msg;
                                }
                                else{
                                    $modalInstance.close();
                                    if(response.status=='success') {
                                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                        $timeout(function() {
                                            $state.go('form-elements',{"form_id": response.form_id});
                                        }, 3000);
                                    }else{
                                        $rootScope.toastrMessages('error',response.msg);
                                    }

                                }
                            }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                                $modalInstance.close();

                            });
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };


        var resetForms=function(){
            LoadCustomForm({page:$scope.CurrentPage});
        };

        $rootScope.deleteCustomForms = function (size,id) {
            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                .then(function() {
                    $rootScope.progressbar_start();
                    custom_forms.delete({id:id},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                LoadCustomForm({page:$scope.CurrentPage});
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                    });
                });

        };

        $rootScope.restore = function (size,id) {
            $rootScope.clearToastr();
            $ngBootbox.confirm($filter('translate')('are you want to confirm restore') )
                .then(function() {
                    $rootScope.progressbar_start();
                    custom_forms.restore({id:id},function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                LoadCustomForm({page:$scope.CurrentPage});
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                    });
                });

        };

        $rootScope.updateCustomForms= function (size,id) {
            $rootScope.clearToastr();

            var target= new custom_forms(id);
            target.$get({id:id})
                .then(function (response) {
                    $rootScope.target=response;
                    $rootScope.target.status= $rootScope.target.status +"";
                    $uibModal.open({
                        templateUrl: 'myModalContent1.html',
                        backdrop  : 'static',
                        keyboard  : false,
                        windowClass: 'modal',
                        size: size,
                        controller: function ($rootScope,$scope, $modalInstance, $log,custom_forms) {

                            $scope.confirmUpdate = function (customformsdata) {
                                $rootScope.clearToastr();
                                customformsdata.page=$scope.CurrentPage;
                                $rootScope.progressbar_start();
                                custom_forms.update( customformsdata , function (response) {
                                    $rootScope.progressbar_complete();
                                    if(response.status=='error' || response.status=='failed')
                                        {
                                            $rootScope.toastrMessages('error',response.msg);
                                        }
                                        else if(response.status=='failed_valid')
                                        {

                                            $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                            $scope.status1 =response.status;
                                            $scope.msg1 =response.msg;
                                        }
                                        else{
                                            $modalInstance.close();
                                            if(response.status=='success')
                                            {
                                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                                resetForms();
                                            }else{
                                                $rootScope.toastrMessages('error',response.msg);
                                            }

                                        }
                                    }, function(error) {
                                        $rootScope.progressbar_complete();
                                        $rootScope.toastrMessages('error',error.data.msg);
                                        $modalInstance.close();
                                    });
                            };
                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };
                        }
                    });
                });
        };
    });

