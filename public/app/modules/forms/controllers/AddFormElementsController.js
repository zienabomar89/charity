
angular.module('FormsModule')
    .controller('AddFormElementsController' ,function($state,$stateParams,$rootScope, $scope, $http, $timeout, $uibModal, $log,form_elements,custom_forms,$filter) {

        
        $rootScope.failed =false;
        $rootScope.model_error_status =false;
        $rootScope.msg1 ='';

        $rootScope.status ='';
        $rootScope.msg ='';

        $scope.Optionlabel='';
        $scope.Optionweight='';
        $scope.OptionValue='';
        $scope.mode=false;
        $scope.enabled=false;

        $scope.count= 0;
        $scope.Options = [];
        $scope.RemovedOptions = [];
        $rootScope.fstatus= null;
        $rootScope.form_id=$stateParams.form_id;
        $rootScope.action=$stateParams.mode;
        $rootScope.status ='';
        $rootScope.msg ='';

        if($stateParams.form_id == null){
            $rootScope.formName=' ';
            $rootScope.no_form =true;
            $timeout(function() {
                $state.go('custom-forms');
            }, 3000);
        }
        else{
            custom_forms.getFormName({id:$rootScope.form_id},function (response) {
                $rootScope.formName=response.name;
                $rootScope.form=response.id;
                $rootScope.fstatus=response.status;
            });

            if($stateParams.element != null){
                if($stateParams.mode=='duplicate'){
                    $rootScope.action='add';
                    $state.current.data.pageTitle = $filter('translate')('add_new_element');
                }else{
                    $state.current.data.pageTitle = $filter('translate')('edit_element');
                }
                $rootScope.progressbar_start();
                form_elements.getElementName({id:$stateParams.element},function (response) {
                    $rootScope.progressbar_complete();
                    $scope.Element=response.Element;
                    $scope.Element.type=$scope.Element.type +"";
                    $scope.Element.priority=$scope.Element.priority +"";
                    $rootScope.elementName=$scope.Element.label;
                    $rootScope.Element_id=$scope.Element.id;

                    if($scope.Element.type == 2){
                        $rootScope.has_option = true;
                        $scope.Options=$scope.Element.options;
                    }
                });
            }else{
                $rootScope.action='add';
                $state.current.data.pageTitle = $filter('translate')('add_new_element');

            }
        }

        $scope.checkType = function (type) {
            if( !angular.isUndefined($rootScope.msg1.type)) {
                $rootScope.msg1.type = [ ];
            }

            if(type == 2 || type == 4 ||type == 5){
                $rootScope.has_option = true;
            }else{
                  $rootScope.has_option = false;
            }
        };
        $scope.newOption = function () {
            $rootScope.clearToastr();
            var founded=false;
            angular.forEach($scope.Options, function(v, k) {
                if(v.value==$scope.OptionValue) {
                    founded=true;
                    return;
                }
            });
            if(founded){
                $rootScope.toastrMessages('error',$filter('translate')('The value is already used'));
                $scope.OptionValue='';
            }else{
                $scope.Options.push({label: $scope.Optionlabel, weight: $scope.Optionweight, value: $scope.OptionValue});
                $scope.count= $scope.count+1;
                $scope.Optionlabel='';
                $scope.Optionweight='';
                $scope.OptionValue='';

            }

        };
        $scope.removeOption = function(index){
            if($rootScope.action=='edit'){
                var temp_id=$scope.Options[index].id;
                op={id: temp_id };
                $scope.RemovedOptions.push(op);
                $scope.Options.splice(index,1);
            }else{
                $scope.Options.splice(index,1);
                $scope.count= $scope.count-1;
            }

        };
        $scope.editOption=function(index){
            $scope.Optionlabel=$scope.Options[index].label;
            $scope.Optionweight=$scope.Options[index].weight;
            $scope.OptionValue=$scope.Options[index].value;
            $scope.temp_index=index;
            $scope.mode=true;
        };
        $scope.ConfirmEditOption=function(){

            $rootScope.clearToastr();
            var flag = true;
            angular.forEach($scope.Options, function(val,key)
            {
                if( val.value ==  $scope.OptionValue && $scope.temp_index != key){
                    flag = false;
                    return;
                }
            });

            if(flag){
                $scope.Options[$scope.temp_index].label=$scope.Optionlabel;
                $scope.Options[$scope.temp_index].weight=$scope.Optionweight;
                $scope.Options[$scope.temp_index].value=$scope.OptionValue;
                $scope.mode=false;
                $scope.Optionlabel='';
                $scope.Optionweight='';
                $scope.OptionValue ='';
            }else{
                $rootScope.toastrMessages('error',$filter('translate')('The value to modify is already used'));
            }
        };
        $scope.confirmInsert = function (Element) {
            $rootScope.clearToastr();
            Element.form_id=$rootScope.form;

            if( !angular.isUndefined(Element.name)) {
                if(!OnlyLetter(Element.name)) {
                    $rootScope.toastrMessages('error',$filter('translate')('The Name Must Only English Letter'));
                    return ;
                }
            }

            
            var Elementdata= {
                Element : Element,
                Element_Options : $scope.Options
            };
            if($scope.Options.length==0 && ( Element.type == 2 || Element.type == 5 ||Element.type == 4)){
                $rootScope.toastrMessages('error',$filter('translate')('The is no elements option'));
            }else{

                if($rootScope.action=='edit'){
                    Elementdata.id=Elementdata.Element.id;
                    Elementdata.RemovedOptions=$scope.RemovedOptions;
                    $rootScope.progressbar_start();
                    form_elements.update( Elementdata , function (response) {
                        $rootScope.progressbar_complete();

                            if(response.status=='error' || response.status=='failed') {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid') {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                if(response.status=='success') {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    $timeout(function() {
                                        $state.go('form-elements',{"form_id": $rootScope.form,"mode": "edit","status": true});
                                    }, 3000);
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }
                    });
                }
                else if($rootScope.action=='add'){
                    $rootScope.progressbar_start();
                    form_elements.save( Elementdata ,function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='error' || response.status=='failed') {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid') {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                if(response.status=='success') {
                                    $rootScope.toastrMessages('success',$filter('translate')('The row is inserted to db'));
                                    $timeout(function() {
                                        $state.go('form-elements',{"form_id": $rootScope.form,"mode": "add","status": true});
                                    }, 3000);

                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }
                    });
                }
            }
        };


    });

