
angular.module('FormsModule')
    .controller('ShowFormElementsController' ,function($state,$stateParams,$rootScope, $scope, $http, $timeout, $uibModal, $log,form_elements,custom_forms,$filter) {

        $rootScope.fields=[];
        $rootScope.no_form =false;

        if($stateParams.form_id){
            $rootScope.form_id=$stateParams.form_id;
            $rootScope.visitmode=$stateParams.mode;

            $rootScope.progressbar_start();
            custom_forms.getFormName({id:$stateParams.form_id},function (response) {
                $rootScope.progressbar_complete();
                $rootScope.formName=response.name;
                $rootScope.form_id=response.id;
                $state.current.data.pageTitle = $filter('translate')('show_custom_forms_element') + ' ' + $rootScope.formName;
            });

            $rootScope.progressbar_start();
            form_elements.getFormElements({id:$stateParams.form_id},function (response) {
                $rootScope.progressbar_complete();
                $rootScope.fields=response.fields;
            });
        }else{
            $rootScope.formName=$filter('translate')('demo form') ;
            $rootScope.no_form =true;
            $rootScope.toastrMessages('error',$filter('translate')('no choice form to show') );
            $timeout(function() {
                $state.go('custom-forms');
            }, 1000);
        }



    });

