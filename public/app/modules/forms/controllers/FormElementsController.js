


angular.module('FormsModule')
    .controller('FormElementsController' ,function($state,$stateParams,$ngBootbox,$rootScope, $scope, $http, $timeout, $uibModal, $log,form_elements,custom_forms,$filter) {

        $state.current.data.pageTitle = $filter('translate')('form elements');

        $rootScope.no_form =false;
        $rootScope.has_option = false;
        $rootScope.status ='';
        $rootScope.msgg ='';
        $rootScope.msg1 ='';
        $rootScope.fields=[];

        $rootScope.form_id=$stateParams.form_id;

        if($stateParams.form_id){
            $rootScope.progressbar_start();
            custom_forms.get({id:$stateParams.form_id},function (response) {
                $rootScope.progressbar_complete();
                $rootScope.formName=response.name;
                $rootScope.form=response.id;
                $rootScope.fstatus=response.status;
                
                form_elements.getFormElements({id:$rootScope.form},function (response) {
                    $rootScope.fields=response.fields;
                });
            });

            $scope.backtoforms = function () {
                $state.go('custom-forms');
            };

            $rootScope.insertElement= function () {
                $state.go('operation-form-elements',{"form_id": $stateParams.form_id,"mode":"add"});
            };

            $rootScope.redirect= function (id,action) {
                $state.go('form-elements-operation',{"form_id": $stateParams.form_id,"element":id,"mode":action});
            };



            $scope.deleteElement = function (size,id) {
                $rootScope.clearToastr();
                $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
                    .then(function() {
                        $rootScope.progressbar_start();
                        form_elements.delete({id:id},function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='success')
                                {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    if(response.FormElement.length != 0){
                                        $rootScope.fields=response.FormElement;
                                    }else{
                                        $rootScope.fields=[];
                                    }
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            });
                    });

            };

        }else{
            $rootScope.formName=$filter('translate')('demo form') ;
            $rootScope.no_form =true;
            $rootScope.toastrMessages('error',$filter('translate')('no choice form to show') );
            $timeout(function() {
                $state.go('custom-forms');
            }, 1000);
        }

    });

