
var FormsModule = angular.module("FormsModule",[]);

angular.module('FormsModule').config(function ($stateProvider) {

    $stateProvider
        .state('custom-forms', {
            url: '/custom-forms/:type',
            templateUrl: '/app/modules/forms/views/index.html',
            controller: "CustomFormsController",
            params:{type: null},
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'FormsModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/forms/controllers/CustomFormsController.js',
                            '/app/modules/forms/services/CustomFormsService.js'
                        ]
                    });
                }]
            }

        })
        .state('form-elements', {
            url: '/forms/:form_id',
            templateUrl: '/app/modules/forms/views/elements-index.html',
            params:{form_id: null,mode:null,status:null,element:null},
            controller: "FormElementsController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'FormsModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/forms/controllers/FormElementsController.js',
                            '/app/modules/forms/services/FormElementsService.js',
                            '/app/modules/forms/services/CustomFormsService.js'
                        ]
                    });
                }]
            }

        })
        .state('form-elements-show', {
            url: '/forms/elements/:form_id',
            templateUrl: '/app/modules/forms/views/show-elements.html',
            data: { pageParantTitle:'الثوابت والاعدادات ',pageTitle: 'عرض النموذج'},
            controller: "ShowFormElementsController",
            params:{form_id: null},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'FormsModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/forms/controllers/ShowFormElementsController.js',
                            '/app/modules/forms/services/FormElementsService.js',
                            '/app/modules/forms/services/CustomFormsService.js'
                        ]
                    });
                }]
            }

        })
        .state('form-elements-operation', {
            url: '/forms/elements/:form_id/:element?mode',
            templateUrl: '/app/modules/forms/views/elements-form.html',
            params:{form_id: null,mode:null,status:null,element:null},
            controller: "AddFormElementsController",
            data: {pageTitle: ''},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'FormsModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/forms/controllers/AddFormElementsController.js',
                            '/app/modules/forms/services/FormElementsService.js',
                            '/app/modules/forms/services/CustomFormsService.js'
                        ]
                    });
                }]
            }

        });

});
