DocumentModule = angular.module('DocumentModule');
DocumentModule.directive('docFileDownload', ['$http',
    function ($http) {
        return {
            restrict: 'A',
            link: function (scope, elem, attrs) {
                elem.on('click', function (e) {
                    e.preventDefault();
                    $http({
                        method: 'GET',
                        url: attrs.url,
                        //responseType: 'arraybuffer',
                        cache: false
                    }).then(function (response) {
                        var token = response.data.download_token;
                        if (token) {
                            window.location = attrs.url + "?token=" + token;
                            return;
                        }
                        /*var a = document.createElement("a");
                         a.style = "display: none";
                         document.body.appendChild(a);

                         var blob = new Blob([response.data], {type: response.headers('Content-Type')});
                         url = (window.URL || window.webkitURL).createObjectURL(blob);
                         a.href = url;
                         a.download = attrs.download;
                         a.click();
                         (window.URL || window.webkitURL).revokeObjectURL(url);
                         document.body.removeChild(a);*/
                    });
                });
            }
        };
    }]);

