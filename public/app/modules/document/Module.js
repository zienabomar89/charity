angular.module('DocumentModule', [
    "ui.router",
    "ngResource",
    "oc.lazyLoad",
    "pascalprecht.translate"
]);

DocumentModule = angular.module('DocumentModule');

/*DocumentModule.config(function ($translateProvider, $translatePartialLoaderProvider) {
    $translateProvider.useSanitizeValueStrategy(null);
    $translateProvider.preferredLanguage('ar');
    $translateProvider.forceAsyncReload(true);
    
    $translatePartialLoaderProvider.addPart('document');
    $translateProvider.useLoader('$translatePartialLoader', {
        urlTemplate: '/app/modules/document/i18n/{lang}.json'
    });
});*/

DocumentModule.config(function ($stateProvider) {
    $stateProvider.state('doc-files-index', {
        url: '/doc/files',
        templateUrl: '/app/modules/document/views/files/index.html',
        data: {pageTitle: 'Files'},
        controller: 'FilesController',
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'FilesController',
                        files: [
                            '/app/modules/document/controllers/FilesController.js',
                            '/app/modules/document/services/FileService.js',
                            '/app/modules/document/services/CategoryService.js',
                            '/app/modules/document/filters/Filesize.js',
                            '/app/modules/document/directives/fileDownload.js'
                        ]
                    });
                }]
        }
    })
        .state('doc-files-view', {
        url: '/doc/files/view/:id',
        templateUrl: '/app/modules/document/views/files/view.html',
        data: {pageTitle: 'View File'},
        controller: 'FileViewController',
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'FileViewController',
                        files: [
                            '/app/modules/document/services/FileService.js',
                            '/app/modules/document/controllers/FilesController.js'
                        ]
                    });
                }]
        }
    })
        .state('doc-files-new', {
        url: '/doc/files/new',
        templateUrl: '/app/modules/document/views/files/new.html',
        data: {pageTitle: 'New File'},
        controller: 'FileCreateController',
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'FileCreateController',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/controllers/FilesController.js',
                            '/app/modules/document/services/FileService.js',
                            '/app/modules/document/services/CategoryService.js',
                            '/app/modules/document/services/TagService.js',
                            '/app/modules/document/services/AttributeService.js',
                            '/app/modules/document/directives/fileDownload.js'
                        ]
                    });
                }]
        }
    })
        .state('doc-files-edit', {
        url: '/doc/files/edit/:id',
        templateUrl: '/app/modules/document/views/files/edit.html',
        data: {pageTitle: 'Edit File'},
        controller: 'FileEditController',
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'FileEditController',
                        files: [    
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/controllers/FilesController.js',
                            '/app/modules/document/services/FileService.js',
                            '/app/modules/document/services/CategoryService.js',
                            '/app/modules/document/services/TagService.js',
                            '/app/modules/document/services/AttributeService.js',
                            '/app/modules/document/services/RevisionService.js',
                            '/app/modules/document/services/FileTagService.js',
                            '/app/modules/document/filters/Filesize.js',
                            '/app/modules/document/directives/fileDownload.js'                                                    ]
                    });
                }]
        }
    })
        .state('doc-categories-index', {
        url: '/doc/categories',
        templateUrl: '/app/modules/document/views/categories/index.html',
        data: {pageTitle: 'Categories'},
        controller: 'CategoreisController',
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CategoreisController',
                        files: [
                            '/app/modules/document/controllers/CategoriesController.js',
                            '/app/modules/document/services/CategoryService.js',
                        ]
                    });
                }]
        }
    })
        .state('doc-categories-new', {
        url: '/doc/categories/new',
        templateUrl: '/app/modules/document/views/categories/new.html',
        data: {pageTitle: 'New Category'},
        controller: 'CategoryCreateController',
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CategoriesController',
                        files: [
                            '/app/modules/document/controllers/CategoriesController.js',
                            '/app/modules/document/services/CategoryService.js',
                        ]
                    });
                }]
        }
    })
        .state('doc-categories-edit', {
        url: '/doc/categories/edit/:id',
        templateUrl: '/app/modules/document/views/categories/edit.html',
        data: {pageTitle: 'Edit Category'},
        controller: 'CategoryEditController',
        resolve: {
            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CategoriesController',
                        files: [
                            '/app/modules/document/controllers/CategoriesController.js',
                            '/app/modules/document/services/CategoryService.js',
                        ]
                    });
                }]
        }
    });
});

 DocumentModule.run(function($rootScope) {
    $rootScope.fileName = function(file) {
        var ext = (file.filepath).substr((file.filepath).lastIndexOf('.'));
        return file.name + ext;
    };
});