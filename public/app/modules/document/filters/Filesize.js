DocumentModule = angular.module('DocumentModule');

DocumentModule.filter('Filesize', function () {
    return function (size) {
        if (isNaN(size)) {
            return 0;
        }
        $units = ['B', 'KB', 'MB', 'GB', 'TB'];
        $units = ['بايت', 'كيلو بايت', 'ميغا بايت', 'جيجا بايت', 'تيرا بايت'];

        $bytes = Math.max(size, 0); 
        $pow = Math.floor(($bytes ? Math.log($bytes) : 0) / Math.log(1024)); 
        $pow = Math.min($pow, $units.length - 1); 

        $bytes /= Math.pow(1024, $pow);

        return $bytes.toFixed(2) + ' ' + $units[$pow]; 
    };
});