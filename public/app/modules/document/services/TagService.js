TagService = angular.module('TagService', []);

TagService.factory('Tag', function ($resource) {
    return $resource('/api/v1.0/doc/tags/:id', {id: '@id'}, {
        update: {
            method: 'PUT'
        }
    });
});