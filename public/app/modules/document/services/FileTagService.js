TagService = angular.module('FileTagService', []);

TagService.factory('FileTag', function ($resource) {
    return $resource('/api/v1.0/doc/file-tags/:id', {id: '@id'}, {
        update: {
            method: 'PUT'
        }
    });
});