DocumentModule = angular.module('DocumentModule');

DocumentModule.factory('DocumentCategory', function ($resource) {
    return $resource('/api/v1.0/doc/categories/:id', {id: '@id'}, {
        update: {
            method: 'PUT'
        }
    });
});