FileService = angular.module('RevisionService', []);

FileService.factory('Revision', function ($resource) {
    return $resource('/api/v1.0/doc/revisions/:id', {id: '@id'}, {
        update: {
            method: 'PUT'
        }
    });
});