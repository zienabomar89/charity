FileService = angular.module('FileService', []);

FileService.factory('File', function ($resource) {
    return $resource('/api/v1.0/doc/files/:id', {id: '@id'}, {
        query:  {
            method: 'GET',
            isArray: false
        },
        update: {
            method: 'PUT'
        }
    });
});