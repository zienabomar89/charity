AttributeService = angular.module('AttributeService', []);

AttributeService.factory('Attribute', function ($resource) {
    return $resource('/api/v1.0/doc/attributes/:id', {id: '@id'}, {
        update: {
            method: 'PUT'
        }
    });
});