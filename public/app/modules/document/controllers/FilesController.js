DocumentModule = angular.module('DocumentModule');

DocumentModule.controller('FilesController', function ($sce, $rootScope, $scope, $filter, $state, popupService, File, DocumentCategory) {
    $state.current.data.pageTitle = $filter('translate')('files');
    
    loadFiles = function(params) {
        File.query(params, function(response) {
            $scope.files = response.data;

            $scope.totalItems = response.total;
            $scope.currentPage = response.current_page;
            $scope.itemsPerPage = response.per_page;
        });
    };
    
    $scope.pageChanged = function() {
        loadFiles({
            page: $scope.currentPage,
            name: $scope.srchName,
            tag: $scope.srchTag,
            category_id: $scope.srchCategoryId
        });
    };

    $scope.deleteFile = function (file, idx) {
        $rootScope.clearToastr();
        if (popupService.showPopup('Really delete this?')) {
            File.delete({id: file.id}, function () {
                $scope.files.splice(idx, 1);
                $rootScope.toastrMessages('success',$filter('translate')('action success'));
            });
        }
    };
    
    $scope.search = function() {
        loadFiles({
            page: 1,
            name: $scope.srchName,
            tag: $scope.srchTag,
            category_id: $scope.srchCategoryId
        });
    };
    
    $scope.preview = function(url) {
        url = 'http://view.officeapps.live.com/op/view.aspx?src='+window.location.protocol+"//"+window.location.hostname+url;
        $sce.trustAsUrl(url);
        $('#previewer').attr('src', url);
        $('#filePreview').modal('show');
    };
    
    $scope.categories = DocumentCategory.query();
    loadFiles({page: 1});
});
        
DocumentModule.controller('FileViewController', function ($scope, $stateParams, $state, File) {
    $scope.file = File.get({id: $stateParams.id});
    $state.current.data.pageTitle = $scope.file.name;
});
        
DocumentModule.controller('FileCreateController', function ($rootScope, $scope, $state, $filter, FileUploader, DocumentCategory, OAuthToken) {
    $state.current.data.pageTitle = $filter('translate')('new');
    
    $scope.categories = DocumentCategory.query();
    
    $scope.errorClass = function(name) {
        var s = $scope.form[name];
        return s.$invalid && s.$dirty ? "has-error" : "";
    };

    $scope.errorMessage = function(name) {
        result = [];
        angular.forEach($scope.form[name].$error, function (key, value) {
            result.push(value);
        });
        return result.join(", ");
    };

    $scope.submit = function () {
        $rootScope.clearToastr();
        $scope.file.$save($scope.file, function () {
            $state.go('doc-files-index');
            $rootScope.toastrMessages('success',$filter('translate')('action success'));
        }, function (response) {
            angular.forEach(response.data, function (errors, key) {
                angular.forEach(errors, function (e) {
                    $scope.form[key].$dirty = true;
                    $scope.form[key].$setValidity(e, false);
                });
            });
        });
    };
    
    var Uploader = $scope.uploader = new FileUploader({
        url: '/api/v1.0/doc/files',
        headers: {
            Authorization: OAuthToken.getAuthorizationHeader()
        }
    });
    Uploader.onBeforeUploadItem = function(item)
    {
        $rootScope.progressbar_start();
        Uploader.formData = [{category_id: $scope.category_id,action:"not_check"}];
    };
    Uploader.onCompleteItem = function(fileItem, response, status, headers)
    {
        $rootScope.progressbar_complete();
        $scope.uploader.destroy();
        $scope.uploader.queue=[];
        $scope.uploader.clearQueue();
        angular.element("input[type='file']").val(null);
        fileItem.id = response.id;
    };
});

DocumentModule.controller('FileEditController', function ($rootScope, $scope, $state, $stateParams, File, DocumentCategory, Revision, FileUploader, $http, OAuthToken, Tag, FileTag) {
    $scope.errorClass = function(name) {
        var s = $scope.form[name];
        return s.$invalid && s.$dirty ? "has-error" : "";
    };

    $scope.errorMessage = function(name) {
        result = [];
        angular.forEach($scope.form[name].$error, function (key, value) {
            result.push(value);
        });
        return result.join(", ");
    };
    
    $scope.submit = function () {
        $rootScope.clearToastr();
        tags=[];
        angular.forEach($scope.tags, function(v, k) {
            tags.push(v.id);
        });
        
        $scope.file.tags = tags;
        
        $scope.file.$update($scope.file, function () {
            $state.go('doc-files-index');
            $rootScope.toastrMessages('success',$filter('translate')('action success'));
        }, function (response) {
            angular.forEach(response.data, function (errors, key) {
                angular.forEach(errors, function (e) {
                    $scope.form[key].$dirty = true;
                    $scope.form[key].$setValidity(e, false);
                });
            });
        });
    };

    $scope.loadFile = function () {
        $scope.file = File.get({id: $stateParams.id});
        $scope.tags = FileTag.query({file_id: $stateParams.id});
        $scope.categories = DocumentCategory.query();
        $scope.revisions = Revision.query({file_id: $stateParams.id});
    };

    $scope.loadFile();
    $state.current.data.pageTitle = $scope.file.name;
    
    var Uploader = $scope.uploader = new FileUploader({
        url: '/api/v1.0/doc/revisions',
        headers: {
            Authorization: OAuthToken.getAuthorizationHeader()
        }
    });
    Uploader.onBeforeUploadItem = function(item)
    {
        Uploader.formData = [{file_id: $scope.file.id}];
    };
    Uploader.onCompleteItem = function(fileItem, response, status, headers)
    {
        $scope.uploader.destroy();
        $scope.uploader.queue=[];
        $scope.uploader.clearQueue();
        angular.element("input[type='file']").val(null);
        fileItem.id = response.id;
    };
    
    $scope.getTags = function (val) {
        return $http.get('/api/v1.0/doc/tags', {
            params: {
                name: val
            }
        }).then(function (response) {
            return response.data.map(function (item) {
                return item;
            });
        });
    };
    
    $scope.tagSelected = function ($item, $model, $label, $event) {
        $scope.selectedTag = '';
        $scope.tags.push($item);
    };
    $scope.addTag = function (tag) {
        $tag = new Tag();
        $tag.name = tag;
        $tag.$save($tag, function () {
            $scope.noResults = false;
            $scope.tags.push($tag);
        }, function (response) {
        });
    };
    
    $scope.removeTag = function (tag, idx) {
        $scope.tags.splice(idx, 1);
    };
});