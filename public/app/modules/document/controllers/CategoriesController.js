DocumentModule = angular.module('DocumentModule');

DocumentModule.controller('CategoreisController', function ($rootScope, $scope, $state, popupService, $window, DocumentCategory) {
    $rootScope.clearToastr();
    $scope.categories = DocumentCategory.query({files: 1});

    $scope.deleteCategory = function (category, idx) {
        if (popupService.showPopup('Really delete this?')) {
            category.$delete(function () {
                $scope.categories.splice(idx, 1);
                $rootScope.toastrMessages('success',$filter('translate')('action success'));
            });
        }
    };
});
        
DocumentModule.controller('CategoryViewController', function ($scope, $stateParams, DocumentCategory) {
    $scope.user = DocumentCategory.get({id: $stateParams.id});
});
        
DocumentModule.controller('CategoryCreateController', function ($rootScope, $scope, $state, $stateParams, DocumentCategory) {
    $scope.errorClass = function(name) {
        var s = $scope.form[name];
        return s.$invalid && s.$dirty ? "has-error" : "";
    };

    $scope.errorMessage = function(name) {
        result = [];
        angular.forEach($scope.form[name].$error, function (key, value) {
            result.push(value);
        });
        return result.join(", ");
    };
    $scope.categories = DocumentCategory.query();
    $scope.category = new DocumentCategory();

    $scope.submit = function () {
        $rootScope.clearToastr();
        $scope.category.$save($scope.category, function () {
            $state.go('doc-categories-index');
            $rootScope.toastrMessages('success',$filter('translate')('action success'));
        }, function (response) {
            angular.forEach(response.data, function (errors, key) {
                angular.forEach(errors, function (e) {
                    $scope.form[key].$dirty = true;
                    $scope.form[key].$setValidity(e, false);
                });
            });
        });
    };
});
        
DocumentModule.controller('CategoryEditController', function ($rootScope, $scope, $state, $stateParams, DocumentCategory) {
    $scope.errorClass = function(name) {
        var s = $scope.form[name];
        return s.$invalid && s.$dirty ? "has-error" : "";
    };

    $scope.errorMessage = function(name) {
        result = [];
        angular.forEach($scope.form[name].$error, function (key, value) {
            result.push(value);
        });
        return result.join(", ");
    };
    
    $scope.submit = function () {
        $rootScope.clearToastr();
        $scope.category.$update($scope.category, function () {
            $state.go('doc-categories-index');
            $rootScope.toastrMessages('success',$filter('translate')('action success'));
        }, function (response) {
            angular.forEach(response.data, function (errors, key) {
                angular.forEach(errors, function (e) {
                    $scope.form[key].$dirty = true;
                    $scope.form[key].$setValidity(e, false);
                });
            });
        });
    };

    $scope.loadCategory = function () {
        $scope.category = DocumentCategory.get({id: $stateParams.id});
        $scope.categories = DocumentCategory.query({exclude: $stateParams.id});
    };

    $scope.loadCategory();
});

DocumentModule.controller('CategoryAttributeModal', function($scope, $uibModal) {
    $scope.open = function () {
        var modalInstance = $uibModal.open({
            templateUrl: 'myModalContent.html'
        });
    };
});