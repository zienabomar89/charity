
var SponsorModule = angular.module("SponsorModule",[]);

angular.module('SponsorModule').config(function ($stateProvider) {

    $stateProvider
               
        // *************************** sponsorships-cases *************** //

        .state('persons-sponsorship', {
            url: '/persons/sponsorship',
            templateUrl: '/app/modules/sponsor/views/persons/sponsorship/index.html',
            data: {pageTitle: ''},
            params:{},
            controller: "SponsorCasesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsor/controllers/casesCtrl.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/organization/services/OrgService.js',                            
                            '/app/modules/sponsor/services/services.js',
                        ]
                    });
                }]
            }

        })
        .state('persons-sponsorship-detail', {
            url: '/persons/sponsorship/detail/:id',
            templateUrl: '/app/modules/sponsor/views/persons/sponsorship/detail.html',
            controller:"SponsorCasesDetailController",
            data: {pageTitle: ''},
            params:{id:null},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsor/controllers/casesCtrl.js',
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/common/services/CategoriesService.js',                            
                            '/app/modules/sponsor/services/services.js',
                        ]
                    });
                }]
            }
        })
        
        // *************************** payments ************************ //
        
        .state('sponsor-payments', {
            url: '/cases/payments',
            templateUrl: '/app/modules/sponsor/views/payments/index.html',
            data: {pageTitle: ''},
            params:{},
            controller: "SponsorPaymentsController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js',
                            '/app/modules/sponsor/controllers/paymentsCtrl.js',                            
                            '/app/modules/sponsor/services/services.js',
                        ]
                    });
                }]
            }

        })
        .state('sponsor-payments-persons', {
            url: '/cases/payments/persons/:id',
            templateUrl: '/app/modules/sponsor/views/payments/persons.html',
            data: {pageTitle: ''},
            params: {id:null},
            controller: "SponsorPaymentsPersonsController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsor/controllers/paymentsCtrl.js',                            
                            '/app/modules/sponsor/services/services.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/locationsService.js'

                        ]
                    });
                }]
            }

        })
        
        // *************************** aids-cases ********************** //

        .state('persons-aids', {
            url: '/persons/aids',
            templateUrl: '/app/modules/sponsor/views/persons/aids/index.html',
            data: {pageTitle: ''},
            params:{},
            controller: "SponsorAidsCasesController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsor/controllers/casesCtrl.js',   
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/organization/services/OrgService.js',                            
                            '/app/modules/sponsor/services/services.js',
                        ]
                    });
                }]
            }

        })
        .state('persons-aids-detail', {
            url: '/persons/aids/detail/:id',
            templateUrl: '/app/modules/sponsor/views/persons/aids/detail.html',
            controller:"aidCaseCtrl",
            data: {pageTitle: ''},
            params:{id:null},
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsor/controllers/casesCtrl.js',   
                            '/app/modules/common/services/PersonsService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/common/services/CategoriesService.js',                            
                            '/app/modules/sponsor/controllers/casesCtrl.js',   
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/organization/services/OrgService.js',                            
                            '/app/modules/sponsor/services/services.js',
                        ]
                    });
                }]
            }
        })
        
        // *************************** vouchers ************************ //
        .state('sponsor-vouchers', {
            url: '/cases/vouchers',
            templateUrl: '/app/modules/sponsor/views/vouchers/index.html',
            data: {pageTitle: ''},
            params:{},
            controller: "SponsorVouchersController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsor/controllers/vouchersCtrl.js',                          
                            '/app/modules/sponsor/services/services.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/aid/services/VouchersCategoriesService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/document/directives/fileDownload.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/SettingsService.js'
                        ]
                    });
                }]
            }

        })
        .state('sponsor-vouchers-persons', {
            url: '/cases/vouchers/persons/:id',
            templateUrl: '/app/modules/sponsor/views/vouchers/persons.html',
            data: {pageTitle: ''},
            params: {id:null},
            controller: "SponsorVouchersPersonsController",
            resolve: {
                deps: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'SponsorModule',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/sponsor/controllers/vouchersCtrl.js',   
                            '/app/modules/sponsor/services/services.js',
                            '/app/modules/aid/services/VouchersService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/aid/services/VouchersCategoriesService.js',
                            '/app/modules/common/services/CategoriesService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/SettingsService.js',
                            '/app/modules/setting/services/locationsService.js'
                        ]
                    });
                }]
            }

        })

});
