
angular.module('SponsorModule')
        .controller('SponsorVouchersController', function($location,$filter,$stateParams,$rootScope, $scope,$uibModal, $http, $timeout,$state,Entity,setting,Org,FileUploader,$ngBootbox, OAuthToken,vouchers_categories,cs_persons_vouchers,cases,org_sms_service,category,$uibModal,Sponsor) {

    $rootScope.clearToastr();

    $state.current.data.pageTitle = $filter('translate')('vouchers');
    $rootScope.settings.layout.pageSidebarClosed = true;

    var AuthUser = localStorage.getItem("charity_LoggedInUser");
    $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
    $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
    $rootScope.UserType = $rootScope.charity_LoggedInUser.type
    if ($rootScope.charity_LoggedInUser.organization != null) {
        $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
    }
    $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;

    $scope.master=false;
    $rootScope.status ='';
    $scope.CurrentPage = 1;
    $rootScope.msg ='';
    $rootScope.default_template=null;
    $rootScope.has_default_template=false;
    $scope.sortKeyArr=[];
    $scope.sortKeyArr_rev=[];
    $scope.sortKeyArr_un_rev=[];

    $rootScope.filter={};
    $scope.items=[];
    $scope.toggleStatus = true;

    Org.list({type:'organizations'},function (response) {  $scope.Org = response; });
    Org.list({type:'sponsors'},function (response) { $rootScope.Sponsors = response; });

    Entity.get({entity:'entities',c:'currencies,transferCompany'},function (response) {
        $rootScope.currency = response.currencies;
        $rootScope.transferCompany = response.transferCompany;
    });

    $rootScope.aidCategories_=[];
    category.getSectionForEachCategory({'type':'aids'},function (response) {
        if(!angular.isUndefined(response)){
            $rootScope.aidCategories_firstStep_map =[];
            $rootScope.aidCategories = response.Categories;
            if($rootScope.aidCategories.length > 0){
                for (var i in $rootScope.aidCategories) {
                    var item = $rootScope.aidCategories[i];
                    $rootScope.aidCategories_firstStep_map[item.id] = item.FirstStep;
                }
                angular.forEach($rootScope.aidCategories, function(v, k) {
                    if ($rootScope.lang == 'ar'){
                        $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
                    }
                    else{
                        $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
                    }
                });
            }
        }
    });
    
    var resetVouchersFilter =function(){
        $rootScope.filter={
            "page":1,
            "all_organization":false,
            "type":"",
            "voucher_source":"",
            "category_id":"",
            "case_category_id":"",
            "sponsor_id":"",
            "title":"",
            "content":"",
            "notes":"",
            "min_value":"",
            "max_value":"",
            "min_count":"",
            "transfer":"",
            "allow_day":"",
            "transfer_company_id":null,
            "max_count":"",
            "date_to":"",
            "date_from":"",
            "voucher_date_to":"",
            "voucher_date_from":""
        };
    };

    var LoadVouchersList =function($param){
        // $rootScope.clearToastr();
        $param.page =  $scope.CurrentPage ;

        if( !angular.isUndefined($param.transfer)) {
            if($param.transfer == 0 ||$param.transfer == '0'){
                $param.transfer_company_id = null
            }
        }
        $rootScope.progressbar_start();
        // // angular.element('.btn').addClass("disabled");
        Sponsor.vouchers($param,function (response) {
            // angular.element('.btn').removeClass("disabled");
            $rootScope.progressbar_complete();
            if($param.action == 'ExportToExcel'){
                  if(response.status == true){
                       var file_type='xlsx';
                       $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                       window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                  }else{
                       $rootScope.toastrMessages('error',$filter('translate')('no records to download'));
                  }
            }else{
                if(response.Vouchers.data.total != 0){

                    if( !angular.isUndefined($param.all_organization)) {
                        if($param.all_organization == true ||$param.all_organization == 'true'){
                            $scope.master=true
                        }else if($param.all_organization == false ||$param.all_organization == 'false') {
                            $scope.master=false
                        }
                    }

                    // $scope.master=response.master;
                    var temp =response.Vouchers.data;

                    angular.forEach(temp, function (v, k) {
                        if (v.Beneficiary == 0) {
                            v.linkEnabled=false;
                        }else{
                            v.linkEnabled=true;
                        }
                    });
                    $scope.items =temp;
                    $scope.CurrentPage = response.Vouchers.current_page;
                    if($param.page == 1){
                        $rootScope.total_value = response.total;
                    }
                    $rootScope.TotalItems = response.Vouchers.total;
                    $rootScope.ItemsPerPage = response.Vouchers.per_page;
                }else{
                    $scope.items= [];
                    $rootScope.total_value= 0;
                }                
            }
        });

    };

    var resetTable = function () {

        var data = angular.copy($rootScope.filter);
        data.action ='filter';
        data.page =$scope.CurrentPage;
        data.itemsCount =$scope.itemsCount;
        data.organization_ids=[];

        if( !angular.isUndefined(data.all_organization)) {
            if(data.all_organization == true ||data.all_organization == 'true'){
                angular.forEach(data.organization_id, function(v, k) {
                    data.organization_ids.push(v.id);
                });
                delete data.organization_id;
                $scope.master=true
            }else if(data.all_organization == false ||data.all_organization == 'false') {
                data.organization_ids=[];
                $scope.master=false
            }

        }
        LoadVouchersList(data);
    };

    resetVouchersFilter();
    resetTable();

    $rootScope.Choices = vouchers_categories.List();

    $scope.resetVouchersFilter_=function(){
        resetVouchersFilter();
    }
    $scope.export=function(id){
        
        $rootScope.clearToastr();
        $param = {};
        $param.voucher_id =  id ;
        $param.action =  'ExportToExcel' ;

        $rootScope.progressbar_start();
        // // angular.element('.btn').addClass("disabled");
        Sponsor.vouchers($param,function (response) {
            // angular.element('.btn').removeClass("disabled");
            $rootScope.progressbar_complete();
            if(response.status == true){
                var file_type='xlsx';
                $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
            }else{
                $rootScope.toastrMessages('error',$filter('translate')('no records to download'));
            }
        });


    };

    $rootScope.pageChanged = function (CurrentPage) {
        $scope.CurrentPage = CurrentPage;
        resetTable();
    };

    $rootScope.itemsPerPage_ = function (itemsCount) {
        $scope.itemsCount = itemsCount;
        $scope.CurrentPage = 1;
        resetTable();
    };

    $scope.allCheckbox = function(value){
        if(value){
            $scope.selectAll = true;
        }
        else{
            $scope.selectAll =false;

        }
        angular.forEach($scope.items, function(val,key)
        {
            val.check=$scope.selectAll;
        });
    };
    $scope.Search=function(selected , params,action) {

        $rootScope.clearToastr();
        var data = angular.copy(params);
        data.action = action;
        data.organization_ids = [];

        if( !angular.isUndefined(data.organization_id)) {
               angular.forEach(data.organization_id, function(v, k) {
                    data.organization_ids.push(v.id);
                });
        }
                    data.vouchers =[];

       if(selected == true || selected == 'true' ){
           var vouchers=[];
            angular.forEach($scope.items, function(v, k){
               if(!angular.isUndefined(v.check)) {
                  if(v.check){
                      vouchers.push(v.id);
                  }
                }

            });

            if(vouchers.length == 0){
                $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                return ;
            }
            data.vouchers=vouchers;
        }
        if( !angular.isUndefined(data.date_to)) {
            data.date_to=$filter('date')(data.date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.date_from)) {
            data.date_from= $filter('date')(data.date_from, 'dd-MM-yyyy')
        }

       if( !angular.isUndefined(data.voucher_date_to)) {
            data.voucher_date_to=$filter('date')(data.voucher_date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.voucher_date_from)) {
            data.voucher_date_from= $filter('date')(data.voucher_date_from, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(data.transfer)) {
            if(data.transfer == 0 ||data.transfer == '0'){
                data.transfer_company_id = null
            }
        }
        data.page=1;
        data.action=action;
        LoadVouchersList(data);
    };

    $scope.sort_ = function(keyname){
        $scope.sortKey_ = keyname;
        var reverse_ = false;
        if (
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
        ) {
            reverse_ = true;
        }
        var data = angular.copy($rootScope.filter);
        data.action ='filter';

        if (reverse_) {
            if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_rev.push(keyname);
            }
        }
        else {
            if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_un_rev.push(keyname);
            }

        }

        data.sortKeyArr_rev = $scope.sortKeyArr_rev;
        data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;
        $scope.Search(data,'filter');
    };

    $scope.toggle = function() {
        $rootScope.clearToastr();
        $scope.toggleStatus = $scope.toggleStatus == false ? true: false;
        resetVouchersFilter();
        if($scope.toggleStatus == true) {
            $rootScope.filter.page =1;
            resetTable();
        }
    };

    $scope.Export=function(voucher_id){
        $rootScope.progressbar_start();
        Sponsor.persons({target : 1 ,action:'xls' , voucher_id:voucher_id},function (response) {
            $rootScope.progressbar_complete();
            if(response.download_token){
                window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token;
            }
        });

    };

    $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0};
    $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $rootScope.format = $scope.formats[0];
    $rootScope.today = function() {$rootScope.dt = new Date();};
    $rootScope.today();
    $rootScope.clear = function() {$rootScope.dt = null;};
    $scope.popup99 = {opened: false};
    $scope.popup88 = {opened: false};
    $scope.popup999 = {opened: false};
    $scope.popup888 = {opened: false};
    $scope.open88 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup88.opened = true;};
    $scope.open999 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup999.opened = true;};
    $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
    $scope.open888 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup888.opened = true;};

});

angular.module('SponsorModule')
        .controller('SponsorVouchersPersonsController', function( $filter,$stateParams,$rootScope, $scope,$uibModal, $http, $timeout,$state,Entity,setting,Org,FileUploader, OAuthToken,vouchers_categories,cs_persons_vouchers,cases,org_sms_service,category,Sponsor) {

    $rootScope.clearToastr();

    $state.current.data.pageTitle = $filter('translate')('personsVouchers');
    $rootScope.settings.layout.pageSidebarClosed = true;

    $scope.master=false;
    $rootScope.status ='';
    $scope.CurrentPage = 1;
    $scope.itemsCount = 50;
    $rootScope.msg ='';
    $rootScope.default_template=null;
    $rootScope.has_default_template=false;
    $rootScope.filter={};
    $scope.items=[];
    $scope.toggleStatus = true;

    Org.list({type:'organizations'},function (response) {  $scope.Org = response; });
    Org.list({type:'sponsors'},function (response) { $rootScope.Sponsors = response; });
    Entity.get({entity:'entities',c:'currencies,transferCompany'},function (response) {
        $rootScope.currency = response.currencies;
        $rootScope.transferCompany = response.transferCompany;
    });

    $rootScope.aidCategories_=[];
    category.getSectionForEachCategory({'type':'aids'},function (response) {
        if(!angular.isUndefined(response)){
            $rootScope.aidCategories_firstStep_map =[];
            $rootScope.aidCategories = response.Categories;
            if($rootScope.aidCategories.length > 0){
                for (var i in $rootScope.aidCategories) {
                    var item = $rootScope.aidCategories[i];
                    $rootScope.aidCategories_firstStep_map[item.id] = item.FirstStep;
                }
                angular.forEach($rootScope.aidCategories, function(v, k) {
                    if ($rootScope.lang == 'ar'){
                        $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
                    }
                    else{
                        $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
                    }
                });
            }
        }
    });

    var resetVouchersFilter =function(){
        $rootScope.filter={
            "page":1,
            "all_organization":false,
            "type":"",
            "voucher_source":"",
            "category_id":"",
            "sponsor_id":"",
            "title":"",
            "content":"",
            "notes":"",
            "min_value":"",
            "max_value":"",
            "min_count":"",
            "transfer":"",
            "transfer_company_id":null,
            "case_category_id":"",
            "max_count":"",
            "date_to":"",
            "date_from":"",
            "voucher_date_to":"",
            "voucher_date_from":""
        };
    };
    var LoadPersonsVouchersList =function($param){
        // $rootScope.clearToastr();
        $param.page =  $scope.CurrentPage ;
        $param.itemsCount =  $scope.itemsCount ;

        if( !angular.isUndefined($param.transfer)) {
            if($param.transfer == 0 ||$param.transfer == '0'){
                $param.transfer_company_id = null
            }
        }
        $rootScope.progressbar_start();
        
        $param.ctype = 'aids';
        
        if($stateParams.id){
              $param.voucher_id=$stateParams.id;
        }
        Sponsor.beneficiaries($param,function (response) {
            $rootScope.progressbar_complete();
            if($param.action == 'ExportToExcel'){
                  if(response.status == true){
                       var file_type='xlsx';
                       $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                       window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                  }else{
                       $rootScope.toastrMessages('error',$filter('translate')('no records to download'));
                  }
            }else{
                if(response.data.data.total != 0){
                    if( !angular.isUndefined($param.all_organization)) {
                        if($param.all_organization == true ||$param.all_organization == 'true'){
                            $scope.master=true
                        }else if($param.all_organization == false ||$param.all_organization == 'false') {
                            $scope.master=false
                        }
                    }
                    // $scope.master=response.master;
                    $scope.items = response.data.data;
                    $scope.CurrentPage = response.data.current_page;
                    if($param.page == 1){
                        $rootScope.total_value = response.total;
                    }
                    $rootScope.TotalItems = response.data.total;
                    $rootScope.ItemsPerPage = response.data.per_page;
                }else{
                    $scope.items= [];
                    $rootScope.total_value= 0;
                }           
            }
        });

    };

    var resetTable = function () {

        var data = angular.copy($rootScope.filter);
        data.action ='filter';
        data.page =$scope.CurrentPage;
        data.organization_ids=[];

        if( !angular.isUndefined(data.all_organization)) {
            if(data.all_organization == true ||data.all_organization == 'true'){
                angular.forEach(data.organization_id, function(v, k) {
                    data.organization_ids.push(v.id);
                });
                delete data.organization_id;
                $scope.master=true
            }else if(data.all_organization == false ||data.all_organization == 'false') {
                data.organization_ids=[];
                $scope.master=false
            }

        }
        LoadPersonsVouchersList(data);
    };
    resetVouchersFilter();
    resetTable();

    vouchers_categories.List(function (response) {
        $rootScope.Choices = response;
    });

    $scope.resetVouchersFilter_=function(){
        resetVouchersFilter();
    }
    
    $rootScope.pageChanged = function (CurrentPage) {
        $scope.CurrentPage = CurrentPage;
        resetTable();
    };

    $rootScope.itemsPerPage_ = function (itemsCount) {
        $scope.CurrentPage = 1;
        $scope.itemsCount = itemsCount;
        resetTable();
    };
    
    $scope.allCheckbox = function(value){
        if(value){
            $scope.selectAll = true;
        }
        else{
            $scope.selectAll =false;

        }
        angular.forEach($scope.items, function(val,key)
        {
            val.check=$scope.selectAll;
        });
    };
    $scope.Search=function(selected,params,action) {

        $rootScope.clearToastr();
        var data = angular.copy(params);
        data.action = action;
        data.organization_ids = [];
        data.persons = [];

       if(selected == true || selected == 'true' ){
           var persons=[];
            angular.forEach($scope.items, function(v, k){
               if( !angular.isUndefined(v.check)) {
                  if(v.check){
                      persons.push(v.id);
                  }
                }

            });
            
            if(persons.length==0){
                $rootScope.toastrMessages('error',$filter('translate')('You have not select people to export'));
                return ;
            }
            data.persons=persons;
        }
        
        if( !angular.isUndefined(data.all_organization)) {
            if(data.all_organization == true ||data.all_organization == 'true'){
                angular.forEach(data.organization_id, function(v, k) {
                    data.organization_ids.push(v.id);
                });
                delete data.organization_id;
                $scope.master=true
            }else if(data.all_organization == false ||data.all_organization == 'false') {
                data.organization_ids=[];
                $scope.master=false
            }

        }

        if( !angular.isUndefined(data.date_to)) {
            data.date_to=$filter('date')(data.date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.date_from)) {
            data.date_from= $filter('date')(data.date_from, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(data.voucher_date_to)) {
            data.voucher_date_to=$filter('date')(data.voucher_date_to, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.voucher_date_from)) {
            data.voucher_date_from= $filter('date')(data.voucher_date_from, 'dd-MM-yyyy')
        }
        data.action=action;

        if( !angular.isUndefined(angular.transfer)) {
            if(angular.transfer == 0 ||angular.transfer == '0'){
                angular.transfer_company_id = null
            }
        }
        data.page=1;
        LoadPersonsVouchersList(data);
    };

    $scope.sortKeyArr=[];
    $scope.sortKeyArr_rev=[];
    $scope.sortKeyArr_un_rev=[];
    $scope.sort_ = function(keyname){
        $scope.sortKey_ = keyname;
        var reverse_ = false;
        if (
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
            ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
        ) {
            reverse_ = true;
        }
        var data = angular.copy($rootScope.filter);
        data.action ='filter';

        if (reverse_) {
            if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_rev.push(keyname);
            }
        }
        else {
            if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                $scope.sortKeyArr_rev=[];
                $scope.sortKeyArr_un_rev=[];
                $scope.sortKeyArr_un_rev.push(keyname);
            }

        }

        data.sortKeyArr_rev = $scope.sortKeyArr_rev;
        data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;
        $scope.Search(data,'filter');
    };

    $scope.toggle = function() {
        $rootScope.clearToastr();
        $scope.toggleStatus = $scope.toggleStatus == false ? true: false;
        resetVouchersFilter();
        if($scope.toggleStatus == true) {
            $rootScope.filter.page =1;
            resetTable();
        }
    };

    $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0};
    $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $rootScope.format = $scope.formats[0];
    $rootScope.today = function() {$rootScope.dt = new Date();};
    $rootScope.today();
    $rootScope.clear = function() {$rootScope.dt = null;};
    $scope.popup99 = {opened: false};
    $scope.popup88 = {opened: false};
    $scope.popup999 = {opened: false};
    $scope.popup888 = {opened: false};
    $scope.open88 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup88.opened = true;};
    $scope.open999 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup999.opened = true;};
    $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
    $scope.open888 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup888.opened = true;};

});
