angular.module('SponsorModule')
    .controller('SponsorCasesController', function (Sponsor,$filter,$scope,$rootScope,$uibModal, $http,$state,$stateParams,OAuthToken,sponsor_cases,Org,Entity) {


        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        $state.current.data.pageTitle = $filter('translate')('sponsorships') + 
                                        $filter('translate')('cases');
        $rootScope.settings.layout.pageSidebarClosed = true;

        $scope.master=false;
        $rootScope.status ='';
        $rootScope.msg ='';
        $scope.dataFilters= {};
        $scope.items=[];
        $scope.itemsCount='50';
        $scope.CurrentPage = 1 ;
        $scope.caseCollapsed =  $scope.personCollapsed =
        $scope.fatherCollapsed= $scope.motherCollapsed = $scope.guardianCollapsed = true;
        $scope.search_form = false;
        $scope.simple_form = true;        
        
        $scope.download=function(id,target){
            $rootScope.clearToastr();
            $uibModal.open({
                size: 'md',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                templateUrl:'/app/modules/sponsorship/views/model/select_lang.html',
                controller: function ($rootScope,$scope,$modalInstance) {

                    $scope.confirm = function (lang) {
                        $rootScope.progressbar_start();
                        sponsor_cases.exportSponsorCase({id_:id, target:target,lang:lang},function(response) {
                            $rootScope.progressbar_complete();
                            if(response.download_token){
                                 window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token;
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',$filter('translate')('action failed'));

                            }
                        });
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }});

        };

        getCases =function(params){
                $rootScope.progressbar_start();
                params.page = $scope.CurrentPage;
                params.itemsCount = $scope.itemsCount;
                params.ctype = 'sponsorships';

                if( !angular.isUndefined(params.from_guaranteed_date)) {
                    params.from_guaranteed_date=$filter('date')(params.from_guaranteed_date, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.to_guaranteed_date)) {
                    params.to_guaranteed_date=$filter('date')(params.to_guaranteed_date, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(params.from_sponsorship_date)) {
                    params.from_sponsorship_date=$filter('date')(params.from_sponsorship_date, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.to_sponsorship_date)) {
                    params.to_sponsorship_date=$filter('date')(params.to_sponsorship_date, 'dd-MM-yyyy')
                }
            
                if( !angular.isUndefined(params.birthday_to)) {
                    params.birthday_to=$filter('date')(params.birthday_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.birthday_from)) {
                    params.birthday_from= $filter('date')(params.birthday_from, 'dd-MM-yyyy')
                }
           
                 if( !angular.isUndefined(params.date_to)) {
                    params.date_to=$filter('date')(params.date_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.date_from)) {
                    params.date_from= $filter('date')(params.date_from, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(params.father_birthday_to)) {
                    params.father_birthday_to=$filter('date')(params.father_birthday_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.father_birthday_from)) {
                    params.father_birthday_from= $filter('date')(params.father_birthday_from, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(params.father_death_date_to)) {
                    params.father_death_date_to=$filter('date')(params.father_death_date_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.father_death_date_from)) {
                    params.father_death_date_from= $filter('date')(params.father_death_date_from, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(params.mother_birthday_to)) {
                    params.mother_birthday_to=$filter('date')(params.mother_birthday_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.mother_birthday_from)) {
                    params.mother_birthday_from= $filter('date')(params.mother_birthday_from, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(params.mother_death_date_to)) {
                    params.mother_death_date_to=$filter('date')(params.mother_death_date_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.mother_death_date_from)) {
                    params.mother_death_date_from= $filter('date')(params.mother_death_date_from, 'dd-MM-yyyy')
                }
                
                Sponsor.persons(params,function (response) {
                     $rootScope.close();
                     $rootScope.progressbar_complete();
                        if(params.action == 'search'){
                            $scope.items =response.cases.data;
                            $scope.CurrentPage = response.cases.current_page;
                            $scope.TotalItems = response.cases.total;
                            $scope.ItemsPerPage = response.cases.per_page;

                       }else{
                            if(response.download_token){
                               var file_type='zip';
                                if(params.action == 'export' || params.action == 'Basic' ) {
                                    file_type='xlsx';
                                }
                               window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                           }else{
                               $rootScope.toastrMessages('error',$filter('translate')('no records to download'));
                           }
                       }
                });
        };

        //-----------------------------------------------------------------------------------------------------------
        $scope.toggle = function() {
            $scope.search_form = !$scope.search_form;
            $scope.simple_form = !$scope.simple_form;
            $scope.dataFilters={};
        };

        $scope.collapsed = function(key){
            if(key =='case'){
                $scope.caseCollapsed = $scope.caseCollapsed == false ? true: false;
            }else if(key =='father'){
                $scope.fatherCollapsed = $scope.fatherCollapsed == false ? true: false;
            }else if(key =='mother'){
                $scope.motherCollapsed = $scope.motherCollapsed == false ? true: false;
            }else if(key =='guardian'){
                $scope.guardianCollapsed = $scope.guardianCollapsed == false ? true: false;
            }
        };
        
        $scope.resetFilters=function(){
            $scope.dataFilters={"all_organization":false};
        };
        
        $scope.allCheckbox = function(value){
            if(value){
                $scope.selectAll = true;
            }
            else{
                $scope.selectAll =false;

            }
            angular.forEach($scope.items, function(val,key)
            {
                val.check=$scope.selectAll;
            });
        };
        
       //-----------------------------------------------------------------------------------------------------------
        $rootScope.pageChanged = function (CurrentPage) {
           $scope.dataFilters.page=CurrentPage;
           $scope.dataFilters.action = 'search';
           getCases($scope.dataFilters);
        };

        $rootScope.itemsPerPage_ = function (itemsCount) {
           $scope.dataFilters.page = 1 ;
           $scope.CurrentPage = 1 ;
           $scope.dataFilters.action = 'search';
           $scope.itemsCount = itemsCount ;
           $scope.dataFilters.itemsCount = itemsCount ;
           getCases($scope.dataFilters);
        };
        
        $scope.searchCases = function (selected , params,action) {
            $rootScope.clearToastr();
            
            var filters = angular.copy(params);
            filters.persons=[];

            if(selected == true || selected == 'true' ){
               var persons=[];
                angular.forEach($scope.items, function(v, k){
                    if(v.check){
                        persons.push(v.id);
                    }
                });

                if(persons.length==0){
                    $rootScope.toastrMessages('error',$filter('translate')('You have not select people to export'));
                    return ;
                }
                filters.persons=persons;
            }
            filters.action=action;
            getCases(filters);
        };
       //-----------------------------------------------------------------------------------------------------------
        var entities='deathCauses,furnitureStatus,houseStatus,propertyTypes,roofMaterials,educationAuthorities,educationStages,diseases,maritalStatus,deathCauses,countries,workJobs';
            Entity.get({entity:'entities',c:entities},function (response) {
                $scope.DeathCauses = response.deathCauses;
                $scope.furnitureStatus = response.furnitureStatus;
                $scope.houseStatus = response.houseStatus;
                $scope.PropertyTypes = response.propertyTypes;
                $scope.RoofMaterials = response.roofMaterials;
                $scope.EduAuthorities = response.educationAuthorities;
                $scope.EduStages = response.educationStages;
                $scope.Diseases = response.diseases;
                $scope.MaritalStatus = response.maritalStatus;
                $scope.DeathCauses = response.deathCauses;
                $scope.Country = response.countries;
                $scope.WorkJob = response.workJobs;
            });
        Org.all({type:'organizations'},function (response) {  $scope.Org = response; });
       //-----------------------------------------------------------------------------------------------------------
       
        getCases({page:1,'action':'search','itemsCount':50});
        
        //-----------------------------------------------------------------------------------------------------------

        $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $rootScope.format = $rootScope.formats[0];
        $rootScope.today = function() {$rootScope.dt = new Date();};
        $rootScope.today();
        $rootScope.clear = function() {$rootScope.dt = null;};

        $rootScope.open10 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup10.opened = true;};
        $rootScope.open20 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup20.opened = true;};
        $rootScope.open30 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup30.opened = true;};
        $rootScope.open40 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup40.opened = true;};

        $rootScope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup1.opened = true;};
        $rootScope.open2 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup2.opened = true;};
        $rootScope.open44 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup44.opened = true;};
        $rootScope.open3 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup3.opened = true;};
        $rootScope.open33 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup33.opened = true;};
        $rootScope.open333 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup333.opened = true;};
        $rootScope.open3333 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup3333.opened = true;};
        $rootScope.open11 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup11.opened = true;};
        $rootScope.open111 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup111.opened = true;};
        $rootScope.open1111 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup1111.opened = true;};
        $rootScope.open11111 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup11111.opened = true;};
        $rootScope.open4 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup4.opened = true;};
        $rootScope.open5 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup5.opened = true;};
        $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
        $scope.open88 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup88.opened = true;};

        $rootScope.popup10 = {opened: false};
        $rootScope.popup20 = {opened: false};
        $rootScope.popup30 = {opened: false};
        $rootScope.popup40 = {opened: false};
       
        $rootScope.popup1 = {opened: false};
        $rootScope.popup2 = {opened: false};
        $rootScope.popup33 = {opened: false};
        $rootScope.popup44 = {opened: false};
        $rootScope.popup3 = {opened: false};
        $rootScope.popup333 = {opened: false};
        $rootScope.popup3333 = {opened: false};
        $rootScope.popup11 = {opened: false};
        $rootScope.popup111 = {opened: false};
        $rootScope.popup1111 = {opened: false};
        $rootScope.popup11111 = {opened: false};
        $rootScope.popup4 = {opened: false};
        $rootScope.popup5 = {opened: false};
        $scope.popup99 = {opened: false};
        $scope.popup88 = {opened: false};

    });

angular.module('SponsorModule')
    .controller('SponsorCasesDetailController', function (Sponsor,$filter,$http,$scope,$uibModal,$rootScope,$state,$stateParams,$timeout,persons,cases,sponsor_cases,category) {

        $state.current.data.pageTitle = $filter('translate')('sponsorships') + 
                                        $filter('translate')('cases');
                                $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        $scope.id=$rootScope.id=$stateParams.id;
        $scope.case_id=$rootScope.case_id=null;

        $scope.success =false;
        $scope.failed =false;
        $scope.error =false;
        $scope.model_error_status =false;
        $scope.model_error_status2 =false;
        $scope.persons={};
        $scope.father={};
        $scope.mother={};
        $scope.guardian={};
        $scope.documents=[];
        $scope.casePaymentsList=[];
        $scope.brothers=[];
        $scope.case_payments = true;
        $scope.case_payments_filters ={};
        $rootScope.itemsCount = 50 ;


        if($scope.id != null){
            $rootScope.progressbar_start();
            Sponsor.person({sponsorship_case_id:$scope.id,ctype:'sponsorships'},function(response) {
                $rootScope.progressbar_complete();
                $scope.case_id=$rootScope.case_id = response.id;
                $scope.category_id=$rootScope.category_id = response.category_id;
                $rootScope.person_id=response.person_id;
                $rootScope.father_id=response.father_id;
                $rootScope.mother_id=response.mother_id;
                $rootScope.guardian_id=response.guardian_id;
                $rootScope.name=response.full_name;
                $rootScope.category_name=response.category_name;

                if(Object.keys($scope.persons).length === 0 && $scope.persons.constructor === Object){
                    $scope.fetch('case');
                }
            });
        }

        var resetSearchFilters =function(){
            $scope.case_payments_filters={
                'guardian_id_card_number':"",
                'guardian_first_name':"",
                'guardian_second_name':"",
                'guardian_third_name':"",
                'guardian_last_name':"",
                'max_amount':"",
                'min_amount':"",
                'begin_date_from':"",
                'end_date_from':"",
                'begin_date_to':"",
                'end_date_to':"",
                'start_payment_date':"",
                'end_payment_date':""
            };
        };
        var LoadCasePayemtns = function () {
            var param ={
                case_id:$scope.case_id,
                action:'filters',
                mode:'show',
                target:'case_payments',
                itemsCount:$rootScope.itemsCount,
                page:$rootScope.CurrentPage
            };

            if( !angular.isUndefined($scope.case_payments_filters.begin_date_from)) {
                param.begin_date_from=$filter('date')($scope.case_payments_filters.begin_date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined($scope.case_payments_filters.end_date_from)) {
                param.end_date_from=$filter('date')($scope.case_payments_filters.end_date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined($scope.case_payments_filters.begin_date_to)) {
                param.begin_date_to=$filter('date')($scope.case_payments_filters.begin_date_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined($scope.case_payments_filters.end_date_to)) {
                param.end_date_to=$filter('date')($scope.case_payments_filters.end_date_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined($scope.case_payments_filters.start_payment_date)) {
                param.start_payment_date=$filter('date')($scope.case_payments_filters.start_payment_date, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(param.end_payment_date)) {
                param.end_payment_date= $filter('date')(param.end_payment_date, 'dd-MM-yyyy')
            }

            $rootScope.progressbar_start();
            cases.getCaseReports(param,function(response) {
                $rootScope.progressbar_complete();
                
                if(action == 'export'){
                
                    $rootScope.progressbar_complete();
                    if( !angular.isUndefined(response.download_token)) {
                      var file_type='xlsx';
                      window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                     return ;
                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('payments list'));
           
                    }

               
                }else{
                    $scope.payment_total= response.total;
                    if(response.payment.total == 0){
                        $scope.case_payments_rows='false';
                    }else{
                        $scope.case_payments_rows='true';
                    }
                    $scope.CurrentPage = response.payment.current_page;
                    $scope.TotalItems = response.payment.total;
                    $scope.ItemsPerPage = response.payment.per_page;
                    $scope.casePaymentsList= response.payment.data;                
                }
            });
        };

        $scope.toggleSearch = function(target) {

            if(target =='case_payments'){
                $scope.case_payments = $scope.case_payments === false ? true: false;
            }
            resetSearchFilters();
        };


        $scope.fetch=function(target){
            var param={};

            if(target == 'case'){
                if($scope.case_id){
                    if(Object.keys($scope.persons).length === 0 && $scope.persons.constructor === Object){
                        $rootScope.progressbar_start();
                        cases.getCaseReports({'target':'info','mode':'show','case_id':$rootScope.case_id,'action':'filters',
                            'person' :true, 'persons_i18n' : true, 'contacts' : true,  'education' : true,
                            'health' : true,'islamic' : true,'category_type':1,'work' : true, 'residence' : true, 'banks' : true },function(response) {
                            $rootScope.progressbar_complete();
                            $scope.persons=response;
                        });
                    }
                }
            }
            else if(target == 'parent'){
                param={ 'action':'filters','mode':'show', 'person' :true, 'persons_i18n' : true, 'education' : true,'health' : true, 'work' : true};
                if($rootScope.father_id){
                    if(Object.keys($scope.father).length === 0 && $scope.father.constructor === Object) {
                        param.person_id=$rootScope.father_id;
                        param.target='info';

                        $rootScope.progressbar_start();
                        persons.getPersonReports(param,function(response){
                            $rootScope.progressbar_complete();
                            $scope.father=response;
                        });
                    }

                }
                if($rootScope.mother_id){
                    if(Object.keys($scope.mother).length === 0 && $scope.mother.constructor === Object) {
                        param.person_id=$rootScope.mother_id;
                        param.target='info';

                        $rootScope.progressbar_start();
                        persons.getPersonReports(param,function(response){
                            $rootScope.progressbar_complete();
                            $scope.mother=response;
                        });
                    }
                }
            }
            else if(target =='guardian'){
                if($scope.guardian_id){
                    if(Object.keys($scope.guardian).length === 0 && $scope.guardian.constructor === Object) {
                        $rootScope.progressbar_start();
                        persons.getPersonReports({'person_id':$scope.guardian_id,'target':'info','mode':'show',
                            'person' :true, 'persons_i18n' : true, 'contacts' : true,  'education' : true,'health' : true,'islamic' : true,
                            'work' : true, 'residence' : true, 'banks' : true },function(response){
                            $rootScope.progressbar_complete();
                            $scope.guardian=response;
                        });
                    }
                }
            }
            else if(target =='documents'){
                if($scope.case_id){
                    if($scope.documents.length === 0){
                        $rootScope.progressbar_start();
                        category.getCategoryDocuments({ category_id : $scope.category_id,case_id: $scope.case_id, type: 'sponsorships' ,target: 'case'},function (response){
                            $rootScope.progressbar_complete();
                            $scope.documents =response.documents;
                        });
                    }
                }
            }
            else if(target =='payments'){
                if($scope.case_id){
                    $scope.case_payments =true;
                    if(angular.isUndefined($rootScope.CurrentPage)) {
                        $rootScope.CurrentPage=1;
                    }
                    LoadCasePayemtns();
                }
            }
            if(target == 'brothers'){
                if($scope.person_id){
                    if($scope.brothers.length === 0) {
                        $rootScope.progressbar_start();
                        persons.getPersonReports({'person_id':$scope.person_id,'target':target,'mode':'show','action':'filters'},function(response){
                            $rootScope.progressbar_complete();
                            $scope.brothers=response.brothers;
                        });
                    }
                }

            }
        };
        $scope.export=function(target){
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            $http({
                url: "/api/v1.0/sponsorship/show-sponsorship-cases/loadSponsorCase/"+$scope.id,
                method: "POST",
                data: {id:$scope.id,action:'export',target:'payment'}
            }).then(function (response) {
                $rootScope.progressbar_complete();
                var file_type='xlsx';
                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            });

        };
        $scope.show = function(size,id) {

            $uibModal.open({
                templateUrl: 'show.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: size,
                controller: function ($rootScope,$scope,$stateParams, $modalInstance,persons) {

                    $scope.msg1={};
                    $scope.persons={};
                    $scope.model_error=false;
                    $scope.msg32='';

                    if(id != -1){
                        var params={'person_id':id, 'mode':'show','target':'info', 'person' :true, 'persons_i18n' : true,'work' : true,'education' : true,'health' : true};
                        persons.getPersonReports(params,function(response){
                            $scope.persons=response;
                        });
                    }
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }
            });
        };
        $scope.pageChanged = function (target,CurrentPage) {
            $scope.CurrentPage=CurrentPage;
            LoadCasePayemtns(param);
        };

        $scope.itemsPerPage_ = function (target,itemsCount) {
            $scope.CurrentPage=1;
            $rootScope.itemsCount =  itemsCount ;
            LoadCasePayemtns();
        };

        $scope.Search=function(target,data,action) {

            $rootScope.clearToastr();
            data.action=action;
            data.target=target;
            data.page=1;
            LoadCasePayemtns(data);

        };
        $scope.close=function(){
            $scope.success =false;
            $scope.failed =false;
            $scope.error =false;
            $scope.model_error_status =false;
            $scope.model_error_status2 =false;
        };

        $scope.dateOptions = { formatYear: 'yy',  startingDay: 0 };
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.today = function() { $scope.dt = new Date(); };
        $scope.clear = function() { $scope.dt = null; };

        $scope.format = $scope.formats[0];
        $scope.today();

        $scope.open9  = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup9.opened = true;};
        $scope.open10 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup10.opened = true; };
        $scope.open11 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup11.opened = true; };
        $scope.open12 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup12.opened = true; };
        $scope.open13 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup13.opened = true; };
        $scope.open14 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup14.opened = true; };

        $scope.popup9  = {opened: false};
        $scope.popup10 = {opened: false};
        $scope.popup11 = {opened: false};
        $scope.popup12 = {opened: false};
        $scope.popup13 = {opened: false};
        $scope.popup14 = {opened: false};

    });

angular.module('SponsorModule')
    .controller('SponsorAidsCasesController', function (Sponsor,$filter,$scope,$rootScope,$uibModal, $http,$state,$stateParams,OAuthToken,sponsor_cases,Org,Entity,category) {

        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        $state.current.data.pageTitle = $filter('translate')('sponsorships') + 
                                        $filter('translate')('cases');
        $rootScope.settings.layout.pageSidebarClosed = true;

        $rootScope.status ='';
        $rootScope.msg ='';
        $scope.toggleStatus = true;
        
        $scope.dataFilters= {Essential : []};
        $scope.items=[];
        $scope.PropertiesFilter=[];
        $scope.pro_id=[];
        $scope.AidSourceFilter=[];
        $scope.aid_id=[];
        $scope.itemsCount='50';
        $scope.CurrentPage = 1 ;
        $rootScope.aidCategories_=[];
        
        $scope.infoCollapsed  = $scope.banksCollapsed     =
        $scope.healthCollapsed = $scope.worksCollapsed = $scope.contactsCollapsed  =
        $scope.residenceCollapsed = $scope.homeIndoorCollapsed =
        $scope.othersCollapsed= $scope.aidSourceCollapsed=
        $scope.propertiesCollapsed=$scope.vouchersCollapsed=true;
                
        $scope.search_form = false;
        $scope.simple_form = true;        
        
        $scope.download=function(id,target){
            $rootScope.clearToastr();
            $uibModal.open({
                size: 'md',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                templateUrl:'/app/modules/sponsorship/views/model/select_lang.html',
                controller: function ($rootScope,$scope,$modalInstance) {

                    $scope.confirm = function (lang) {
                        $rootScope.progressbar_start();
                        sponsor_cases.exportSponsorCase({id_:id, target:target,lang:lang},function(response) {
                            $rootScope.progressbar_complete();
                            if(response.download_token){
                                 window.location = '/api/v1.0/common/files/zip/export?download_token='+response.download_token;
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',$filter('translate')('action failed'));

                            }
                        });
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }});

        };

        var resetFilter =function(){
            $rootScope.dataFilters={
                    "Properties":[],
                    "all_organization":false,
                    "order_by_rank":"",
                    "status":"",
                    "date_from":"",
                    "date_to":"",
                    "first_name":"",
                    "second_name":"",
                    "third_name":"",
                    "last_name":"",
                    "en_first_name":"",
                    "en_second_name":"",
                    "en_third_name":"",
                    "en_last_name":"",
                    "category_id":"",
                    "id_card_number":"",
                    "gender":"",
                    "marital_status_id":"",
                    "birthday_from":"",
                    "birthday_to":"",
                    "birth_place":"",
                    "nationality":"",
                    "refugee":"",
                    "unrwa_card_number":"",
                    "adscountry_id":"",
                    "adsdistrict_id":"",
                    "adsregion_id":"",
                    "adsneighborhood_id":"",
                    "adssquare_id":"",
                    "adsmosques_id":"",
                    "person_country":"",
                    "person_governarate":"",
                    "person_city":"",
                    "location_id":"",
                    "mosques_id":"",
                    "street_address":"",
                    "monthly_income_from":"",
                    "monthly_income_to":"",
                    "primary_mobile":"",
                    "secondery_mobile":"",
                    "phone":"",
                    "bank_id":"",
                    "branch_name":"",
                    "account_number":"",
                    "account_owner":"",
                    "property_type_id":"",
                    "roof_material_id":"",
                    "residence_condition":"",
                    "indoor_condition":"",
                    "min_rooms":"",
                    "max_rooms":"",
                    "min_area":"",
                    "max_area":"",
                    "habitable":"",
                    "house_condition":"",
                    "rent_value":"",
                    "need_repair":"",
                    "repair_notes":"",
                    "working":"",
                    "can_work":"",
                    "work_reason_id":"",
                    "work_job_id":"",
                    "work_status_id":"",
                    "work_wage_id":"",
                    "work_location":"",
                    "promised":"",
                    "visited_at_from":"",
                    "visited_at_to":"",
                    'notes':"",
                    'visitor':"",
                    'visited_at':"",
                    'visitor_card':"",
                    'visitor_opinion':"",
                    'visitor_evaluation':"",
                    "min_family_count":"",
                    "max_family_count":"",
                    "min_total_vouchers":"",
                    "max_total_vouchers":"",
                    'organization_id':"",
                    'got_voucher':"",
                    'voucher_organization':"",
                    'voucher_sponsors':"",
                    'voucher_to':"",
                    'voucher_from':"",
                    "voucher_ids":"",
                    'has_health_insurance':"",
                    'health_insurance_number':"",
                    'health_insurance_type':"",
                    'has_device':"",
                    'used_device_name':"",
                    'gov_health_details':"",
                };

                $scope.PropertiesFilter=[];
                $scope.pro_id=[];
                $scope.AidSourceFilter=[];
                $scope.aid_id=[];

        };

        getCases =function(params){
                $rootScope.progressbar_start();
                params.page = $scope.CurrentPage;
                params.itemsCount = $scope.itemsCount;
                params.ctype = 'aids';

                if( !angular.isUndefined(params.from_guaranteed_date)) {
                    params.from_guaranteed_date=$filter('date')(params.from_guaranteed_date, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.to_guaranteed_date)) {
                    params.to_guaranteed_date=$filter('date')(params.to_guaranteed_date, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(params.from_sponsorship_date)) {
                    params.from_sponsorship_date=$filter('date')(params.from_sponsorship_date, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.to_sponsorship_date)) {
                    params.to_sponsorship_date=$filter('date')(params.to_sponsorship_date, 'dd-MM-yyyy')
                }
            
                if( !angular.isUndefined(params.birthday_to)) {
                    params.birthday_to=$filter('date')(params.birthday_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.birthday_from)) {
                    params.birthday_from= $filter('date')(params.birthday_from, 'dd-MM-yyyy')
                }
           
                 if( !angular.isUndefined(params.date_to)) {
                    params.date_to=$filter('date')(params.date_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.date_from)) {
                    params.date_from= $filter('date')(params.date_from, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(params.father_birthday_to)) {
                    params.father_birthday_to=$filter('date')(params.father_birthday_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.father_birthday_from)) {
                    params.father_birthday_from= $filter('date')(params.father_birthday_from, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(params.father_death_date_to)) {
                    params.father_death_date_to=$filter('date')(params.father_death_date_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.father_death_date_from)) {
                    params.father_death_date_from= $filter('date')(params.father_death_date_from, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(params.mother_birthday_to)) {
                    params.mother_birthday_to=$filter('date')(params.mother_birthday_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.mother_birthday_from)) {
                    params.mother_birthday_from= $filter('date')(params.mother_birthday_from, 'dd-MM-yyyy')
                }

                if( !angular.isUndefined(params.mother_death_date_to)) {
                    params.mother_death_date_to=$filter('date')(params.mother_death_date_to, 'dd-MM-yyyy')
                }
                if( !angular.isUndefined(params.mother_death_date_from)) {
                    params.mother_death_date_from= $filter('date')(params.mother_death_date_from, 'dd-MM-yyyy')
                }
                
                Sponsor.persons(params,function (response) {
                     $rootScope.close();
                     $rootScope.progressbar_complete();
                        if(params.action == 'search'){
                            $scope.items =response.cases.data;
                            $scope.CurrentPage = response.cases.current_page;
                            $scope.TotalItems = response.cases.total;
                            $scope.ItemsPerPage = response.cases.per_page;

                       }else{
                            if(response.download_token){
                               var file_type='zip';
                                if(params.action == 'export' || params.action == 'Basic' ) {
                                    file_type='xlsx';
                                }
                               window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                           }else{
                               $rootScope.toastrMessages('error',$filter('translate')('no records to download'));
                           }
                       }
                });
        };

        category.getSectionForEachCategory({'type':'aids'},function (response) {
            if(!angular.isUndefined(response)){
                $rootScope.aidCategories_firstStep_map =[];
                $rootScope.aidCategories = response.Categories;
                if($rootScope.aidCategories.length > 0){
                    for (var i in $rootScope.aidCategories) {
                        var item = $rootScope.aidCategories[i];
                        $rootScope.aidCategories_firstStep_map[item.id] = item.FirstStep;
                    }
                     var aidCategories_=[];
                    angular.forEach($rootScope.aidCategories, function(v, k) {
                        if ($rootScope.lang == 'ar'){
                            aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
                        }
                        else{
                            aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
                        }
                    });

                    $rootScope.aidCategories_ = aidCategories_ ;
                }

            }
        });

        //-----------------------------------------------------------------------------------------------------------
        $scope.toggle = function() {
            $scope.search_form = !$scope.search_form;
            $scope.simple_form = !$scope.simple_form;
            $scope.dataFilters={};
        };
        $scope.get = function(target,value,parant){
            $rootScope.clearToastr();

            if (!angular.isUndefined(value)) {
                if(value != null && value != "" && value != " " ) {
                    Entity.listChildren({entity:target,'id':value,'parent':parant},function (response) {

                        if (target == 'sdistricts') {
                            $scope.governarate = response;
                            $scope.regions = [];
                            $scope.nearlocation = [];
                            $scope.squares = [];
                            $scope.mosques = [];

                            $rootScope.dataFilters.adsdistrict_id = "";
                            $rootScope.dataFilters.adsregion_id = "";
                            $rootScope.dataFilters.adsneighborhood_id = "";
                            $rootScope.dataFilters.adssquare_id = "";
                            $rootScope.dataFilters.adsmosques_id = "";

                        }
                        else if (target == 'sregions') {
                            $scope.regions = response;
                            $scope.nearlocation = [];
                            $scope.squares = [];
                            $scope.mosques = [];
                            $rootScope.dataFilters.adsregion_id = "";
                            $rootScope.dataFilters.adsneighborhood_id = "";
                            $rootScope.dataFilters.adssquare_id = "";
                            $rootScope.dataFilters.adsmosques_id = "";
                        }
                        else if (target == 'sneighborhoods') {
                            $scope.nearlocation = response;
                            $scope.squares = [];
                            $scope.mosques = [];
                            $rootScope.dataFilters.adsneighborhood_id = "";
                            $rootScope.dataFilters.adssquare_id = "";
                            $rootScope.dataFilters.adsmosques_id = "";

                        }
                        else if (target == 'ssquares') {
                            $scope.squares = response;
                            $scope.mosques = [];
                            $rootScope.dataFilters.adssquare_id = "";
                            $rootScope.dataFilters.adsmosques_id = "";
                        }
                        else if (target == 'smosques') {
                            $scope.mosques = response;
                            $rootScope.dataFilters.adsmosques_id = "";
                        }else if(target =='branches'){
                            $scope.Branches = response;
                            $rootScope.dataFilters.branch_name="";
                        }

                        // if(target =='districts'){
                        //     $scope.governarate = response;
                        //     $scope.city = [];
                        //     $scope.nearlocation =[];
                        //     $scope.mosques = [];
                        //     $rootScope.dataFilters.person_governarate="";
                        // }else if(target =='cities'){
                        //     $scope.city = response;
                        //     $rootScope.dataFilters.person_city="";
                        // }else if(target =='neighborhoods'){
                        //     $scope.nearlocation = response;
                        //     $rootScope.dataFilters.location_id="";
                        // }else if(target =='mosques'){
                        //     $scope.mosques = response;
                        //     $rootScope.dataFilters.mosques_id="";
                        // }

                    });
                }
            }
        };

        $scope.collapsed = function(key){
            if(key =='case'){
                $scope.caseCollapsed = $scope.caseCollapsed == false ? true: false;
            }else if(key =='father'){
                $scope.fatherCollapsed = $scope.fatherCollapsed == false ? true: false;
            }else if(key =='mother'){
                $scope.motherCollapsed = $scope.motherCollapsed == false ? true: false;
            }else if(key =='guardian'){
                $scope.guardianCollapsed = $scope.guardianCollapsed == false ? true: false;
            }
        };
        
        $scope.resetFilters=function(){
            $scope.dataFilters={"all_organization":false};
        };
        
        $scope.allCheckbox = function(value){
            if(value){
                $scope.selectAll = true;
            }
            else{
                $scope.selectAll =false;

            }
            angular.forEach($scope.items, function(val,key)
            {
                val.check=$scope.selectAll;
            });
        };
        
        $scope.setPropertiesFilter=function(id,value){
            var index = id_index = -1;
            if ($scope.pro_id.indexOf(id) == -1) {
                if (value != "") {
                    $scope.PropertiesFilter.push({'id':id,'exist':value});
                    $scope.pro_id.push(id);
                }
            }else{
                id_index=$scope.pro_id.indexOf(id);
                for (var i = 0, len = $scope.PropertiesFilter.length; i <= len; i++) {
                    if ($scope.PropertiesFilter[i].id == id) {
                        index= i;
                        break;
                    }
                }
            }
            if(index != -1){
                if (value == "") {
                    $scope.PropertiesFilter.splice(index, 1);
                    $scope.pro_id.splice(id_index, 1);
                } else {
                    $scope.PropertiesFilter[index].exist = value;
                }
            }
        };

        $scope.setAidsourceFilter=function(id,value){
            var aindex = aid_index = -1;
            if ($scope.aid_id.indexOf(id) == -1) {
                if (value != "") {
                    $scope.AidSourceFilter.push({'id':id,'aid_take':value});
                    $scope.aid_id.push(id);
                }
            }else{
                aid_index=$scope.aid_id.indexOf(id);
                for (var i = 0, len = $scope.AidSourceFilter.length; i <= len; i++) {
                    if ($scope.AidSourceFilter[i].id == id) {
                        aindex= i;
                        break;
                    }
                }
            }
            if(aindex != -1){
                if (value == "") {
                    $scope.AidSourceFilter.splice(aindex, 1);
                    $scope.aid_id.splice(aid_index, 1);
                } else {
                    $scope.AidSourceFilter[aindex].aid_take = value;
                }
            }
        };

        $scope.goTo=function(action,person,case_id,category_id){
            $rootScope.clearToastr();
            angular.forEach($rootScope.aidCategories, function(v, k) {
                if(v.id ==category_id){
                    $state.go('caseform.'+v.FirstStep,{"mode":action,"id" :case_id,"category_id" :category_id});
                }
            });

        };

       //-----------------------------------------------------------------------------------------------------------
        $rootScope.pageChanged = function (CurrentPage) {
           $scope.dataFilters.page=CurrentPage;
           $scope.dataFilters.action = 'search';
           getCases($scope.dataFilters);
        };

        $rootScope.itemsPerPage_ = function (itemsCount) {
           $scope.dataFilters.page = 1 ;
           $scope.CurrentPage = 1 ;
           $scope.dataFilters.action = 'search';
           $scope.itemsCount = itemsCount ;
           $scope.dataFilters.itemsCount = itemsCount ;
           getCases($scope.dataFilters);
        };
        
        $scope.searchCases = function (selected , params,action) {
            $rootScope.clearToastr();
            
            var filters = angular.copy(params);
           filters.persons=[];

            if(selected == true || selected == 'true' ){
               var persons=[];
                angular.forEach($scope.items, function(v, k){
                    if(v.check){
                        persons.push(v.id);
                    }
                });

                if(persons.length==0){
                    $rootScope.toastrMessages('error',$filter('translate')('You have not select people to export'));
                    return ;
                }
                filters.persons=persons;
            }
            filters.action=action;
            getCases(filters);
        };
       //-----------------------------------------------------------------------------------------------------------
       Entity.get({entity:'entities',c:'diseases,transferCompany,propertyTypes,roofMaterials,buildingStatus,furnitureStatus,houseStatus,habitableStatus,currencies,aidCountries,countries,maritalStatus,workReasons,workStatus,workWages,workJobs,banks,aidSources,properties,essentials'},function (response) {
            $rootScope.transferCompany = response.transferCompany;
            $rootScope.currency = response.currencies;
            $scope.PropertyTypes = response.propertyTypes;
            $scope.RoofMaterials = response.roofMaterials;
            $scope.buildingStatus = response.buildingStatus;
            $scope.furnitureStatus = response.furnitureStatus;
            $scope.houseStatus = response.houseStatus;
            $scope.habitableStatus = response.habitableStatus;
            $scope.Diseases = response.diseases;
            $scope.Country = response.countries;
            $scope.aidCountries = response.aidCountries;
            $scope.MaritalStatus = response.maritalStatus;
            $scope.WorkReasons = response.workReasons;
            $scope.WorkStatus = response.workStatus;
            $scope.WorkWages = response.workWages;
            $scope.WorkJobs = response.workJobs;
            $scope.Banks = response.banks;
            $scope.Properties = response.properties;
            $scope.Aidsource = response.aidSources;
            $scope.Essentials = response.essentials;
        });

        Org.list({type:'organizations'},function (response) {  $scope.Org = response; });
       //-----------------------------------------------------------------------------------------------------------
        $scope.sortKeyArr=[];
        $scope.sortKeyArr_rev=[];
        $scope.sortKeyArr_un_rev=[];
        $scope.sort_ = function(keyname){
            $scope.sortKey_ = keyname;
            var reverse_ = false;
            if (
                ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) == -1) ||
                ( $scope.sortKeyArr_rev.indexOf(keyname) == -1 && $scope.sortKeyArr_un_rev.indexOf(keyname) != -1)
            ) {
                reverse_ = true;
            }
            var data = angular.copy($rootScope.dataFilters);
            data.action ='filter';

            if (reverse_) {
                if ($scope.sortKeyArr_rev.indexOf(keyname) == -1) {
                    $scope.sortKeyArr_rev=[];
                    $scope.sortKeyArr_un_rev=[];
                    $scope.sortKeyArr_rev.push(keyname);
                }
            }
            else {
                if ($scope.sortKeyArr_un_rev.indexOf(keyname) == -1) {
                    $scope.sortKeyArr_rev=[];
                    $scope.sortKeyArr_un_rev=[];
                    $scope.sortKeyArr_un_rev.push(keyname);
                }

            }

            data.sortKeyArr_rev = $scope.sortKeyArr_rev;
            data.sortKeyArr_un_rev = $scope.sortKeyArr_un_rev;
            $scope.Search(data,'filter');
        };
       
        getCases({page:1,'action':'search','itemsCount':50});
        
        // *************** DATE PIKER ***************
        $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $rootScope.format = $scope.formats[0];
        $rootScope.today = function() {$rootScope.dt = new Date();};
        $rootScope.today();
        $rootScope.clear = function() {$rootScope.dt = null;};

        $scope.open1 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup1.opened = true;
        };
        $scope.popup1 = {
            opened: false
        };
        $scope.open2 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup2.opened = true;
        };
        $scope.popup2 = {
            opened: false
        };
        $scope.open8 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup8.opened = true;
        };
        $scope.popup8 = {
            opened: false
        };
        $scope.open3 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup3.opened = true;
        };
        $scope.popup3 = {
            opened: false
        };
        $scope.open4 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup4.opened = true;
        };
        $scope.popup4 = {
            opened: false
        };
        $scope.open23 = function($event) {
            $event.preventDefault();
            $event.stopPropagation();
            $scope.popup23.opened = true;
        };
        $scope.popup23 = {     opened: false };
        $scope.open24 = function($event) {   $event.preventDefault();    $event.stopPropagation(); $scope.popup24.opened = true;};
        $scope.popup24 = {     opened: false };
        $scope.open224 = function($event) {  $event.preventDefault();      $event.stopPropagation();    $scope.popup224.opened = true; };
        $scope.open15 = function($event) {    $event.preventDefault();    $event.stopPropagation();   $scope.popup15.opened = true;  };
        $scope.open25 = function($event) {  $event.preventDefault();   $event.stopPropagation();   $scope.popup25.opened = true; };
        $scope.open99 = function($event) { $event.preventDefault();   $event.stopPropagation();   $scope.popup99.opened = true;  };
        $scope.open88 = function($event) {  $event.preventDefault(); $event.stopPropagation();  $scope.popup88.opened = true; };

        $scope.popup224 = {     opened: false };
        $scope.popup15 = {      opened: false  };
        $scope.popup25 = {     opened: false  };
        $scope.popup99 = {  opened: false };
        $scope.popup88 = {    opened: false   };


    });

angular.module('SponsorModule')
    .controller('aidCaseCtrl', function( Sponsor, $filter,$scope,$rootScope,$uibModal, $http,$state,$stateParams,OAuthToken,sponsor_cases,Org,Entity,category, $timeout,persons,Org,Entity) {

        $rootScope.id=$scope.id=$stateParams.id;

        $scope.case_payments_filters = $scope.sponsorships_filters  = $scope.guardians_filters=$scope.individuals_filters=$scope.voucher_filters={};
        $scope.cases_frm = $scope.case_payments = $scope.case_sponsorships= $scope.case_guardians= $scope.case_vouchers= $scope.case_individuals= true;

        $scope.casePaymentsList= [];
        $scope.CasesList= [];
        $scope.caseVouchersList= [];
        $scope.sponsorshipList= [];
        $scope.guardiansList= [];
        $scope.individualsList= [];

        $scope.close=function(){
            $scope.success =false;
            $scope.failed =false;
            $scope.error =false;
            $scope.model_error_status =false;
            $scope.model_error_status2 =false;
        };


        Entity.get({entity:'entities',c:'currencies,transferCompany'},function (response) {
            $rootScope.currency = response.currencies;
            $rootScope.transferCompany = response.transferCompany;
        });

        $rootScope.aidCategories_=[];
        angular.forEach($rootScope.aidCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });


        if($stateParams.id == null || $stateParams.id =="" ){
            $rootScope.toastrMessages('error',$filter('translate')('Your request for the link has been error'));
            $timeout(function() {
                $state.go('search');
            }, 3000);
        }
        else{

            Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

            $scope.toggleSearch = function(target) {

                if(target =='payments') {
                    $scope.case_payments = $scope.case_payments === false ? true : false;
                }else if(target =='sponsorships'){
                    $scope.case_sponsorships = $scope.case_sponsorships === false ? true: false;
                }else if(target =='guardians'){
                    $scope.case_guardians = $scope.case_guardians === false ? true: false;
                }else if(target =='case_individuals'){
                    $scope.case_individuals = $scope.case_individuals === false ? true: false;
                }else if(target =='vouchers'){
                    $scope.case_vouchers = $scope.case_vouchers === false ? true: false;
                }else if(target =='cases'){
                    $scope.cases_frm = $scope.cases_frm === false ? true: false;
                }
                resetSearchFilters(target);
            };

            var resetSearchFilters =function(target){

                if(target  =='payments'){
                    $scope.case_payments_filters={
                        'id_card_number':"",
                        'first_name':"",
                        'second_name':"",
                        'third_name':"",
                        'last_name':"",
                        'max_amount':"",
                        'min_amount':"",
                        'begin_date_from':"",
                        'end_date_from':"",
                        'begin_date_to':"",
                        'end_date_to':"",
                        'start_payment_date':"",
                        'end_payment_date':""
                    };

                }
                else if(target =='sponsorships'){

                    $scope.sponsorships_filters={
                        'category_id':"",
                        'to_guaranteed_date':"",
                        'from_guaranteed_date':""
                    };
                }
                else if(target =='guardians'){

                    $scope.guardians_filters={
                        'first_name':"",
                        'second_name':"",
                        'third_name':"",
                        'last_name':"",
                        'id_card_number':"",
                        'to_birthday':"",
                        'from_birthday':"" ,
                        'to_guardian_date':"",
                        'from_guardian_date':""
                    };
                }
                else if(target =='vouchers'){
                    $scope.voucher_filters={
                        "organization_id":"",
                        "category_id":"",
                        "voucher_type":"",
                        "title"  :"",
                        "voucher_date_from":"",
                        "voucher_date_to":"",
                        "content":"",
                        "notes":"",
                        "voucher_source":"",
                        "receipt_date_from":"",
                        "receipt_date_to":"",
                        "receipt_location":""
                    };
                }
                else if(target =='cases'){
                    $scope.cases_filters={
                        "organization_id":"",
                        "category_id":"",
                        "visited_at_to":"",
                        "visited_at_from":""
                    };
                }

            };
            var Content =function(params){

                params.person_id=$scope.id;
                // params.person_id=$stateParams.id;
                params.mode='show';
                params.ctype='aids';

                if( $scope.case_payments === true  || $scope.case_sponsorships === true  ||$scope.case_guardians === true||
                    $scope.case_individuals === true || $scope.cases_frm === true || $scope.case_vouchers === true ){
                    if(params.target =='payments'){
                        if( !angular.isUndefined(params.begin_date_from)) {
                            params.begin_date_from=$filter('date')(params.begin_date_from, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(params.end_date_from)) {
                            params.end_date_from=$filter('date')(params.end_date_from, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.begin_date_to)) {
                            params.begin_date_to=$filter('date')(params.begin_date_to, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(params.end_date_to)) {
                            params.end_date_to=$filter('date')(params.end_date_to, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.start_payment_date)) {
                            params.start_payment_date=$filter('date')(params.start_payment_date, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(params.end_payment_date)) {
                            params.end_payment_date= $filter('date')(params.end_payment_date, 'dd-MM-yyyy')
                        }
                    }
                    else if(params.target =='sponsorships'){
                        if( !angular.isUndefined(params.to_guaranteed_date)) {
                            params.to_guaranteed_date=$filter('date')(params.to_guaranteed_date, 'dd-MM-yyyy')
                        }
                        if( !angular.isUndefined(params.from_guaranteed_date)) {
                            params.from_guaranteed_date=$filter('date')(params.from_guaranteed_date, 'dd-MM-yyyy')
                        }

                    }
                    else if(params.target =='guardians' || params.target =='individuals'){

                        if( !angular.isUndefined(params.to_birthday)) {
                            params.from_birthday=$filter('date')(params.to_birthday, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.from_birthday)) {
                            params.from_birthday=$filter('date')(params.from_birthday, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.to_guardian_date)) {
                            params.to_guardian_date=$filter('date')(params.to_guardian_date, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.from_guardian_date)) {
                            params.from_guardian_date=$filter('date')(params.from_guardian_date, 'dd-MM-yyyy')
                        }

                    }
                    else if(params.target =='cases'){
                        if( !angular.isUndefined(params.visited_at_to)) {
                            params.visited_at_to=$filter('date')(params.visited_at_to, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.visited_at_from)) {
                            params.visited_at_from=$filter('date')(params.visited_at_from, 'dd-MM-yyyy')
                        }

                    }
                    else if(params.target =='vouchers'){

                        if( !angular.isUndefined(params.voucher_date_from)) {
                            params.voucher_date_from=$filter('date')(params.voucher_date_from, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.voucher_date_to)) {
                            params.voucher_date_to=$filter('date')(params.voucher_date_to, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.receipt_date_from)) {
                            params.receipt_date_from=$filter('date')(params.receipt_date_from, 'dd-MM-yyyy')
                        }

                        if( !angular.isUndefined(params.receipt_date_to)) {
                            params.receipt_date_to=$filter('date')(params.receipt_date_to, 'dd-MM-yyyy')
                        }

                    }

                }

                $rootScope.progressbar_start();
                Sponsor.person(params,function(response) {
                    $rootScope.progressbar_complete();
                    if(params.action == 'export'){
                        if( !angular.isUndefined(response.status)) {
                            $rootScope.toastrMessages('error',$filter('translate')('There are no records to export to Excel'));
                        }else{
                            var file_type='xlsx';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                            $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                        }
                    }else{
                        if(params.target == 'info') {
                            $scope.person=response;
                            // $rootScope.person_id=response.person_id;
                            // $rootScope.person_id=response.person_id;
                       }
                       else if(params.target =='payments'){
                           $scope.payment_total= response.total;
                           $scope.CurrentPage = response.payment.current_page;
                           $scope.TotalItems = response.payment.total;
                           $scope.ItemsPerPage = response.payment.per_page;
                           $scope.casePaymentsList= response.payment.data;
                       }
                       else if(params.target =='cases'){
                           $scope.CurrentPage10 = response.current_page;
                           $scope.TotalItems10 = response.total;
                           $scope.ItemsPerPage10 = response.per_page;
                           $scope.CasesList= response.data;
                       }
                       else if(params.target =='vouchers'){
                           $scope.CurrentPage11 = response.data.current_page;
                           $scope.TotalItems11 = response.data.total;
                           $scope.ItemsPerPage11 = response.data.per_page;
                           $scope.caseVouchersList= response.data.data;
                           $scope.total= response.total;

                       }
                       else if(params.target =='sponsorships') {
                           $scope.CurrentPage0 = response.current_page;
                           $scope.TotalItems0 = response.total;
                           $scope.ItemsPerPage0 = response.per_page;
                           $scope.sponsorshipList= response.data;


                       }
                       else if(params.target =='guardians'){
                           $scope.CurrentPage5 = response.current_page;
                           $scope.TotalItems5 = response.total;
                           $scope.ItemsPerPage5 = response.per_page;
                           $scope.guardiansList= response.data;
                       }
                       else if(params.target =='individuals'){
                           $scope.CurrentPage50 = response.current_page;
                           $scope.TotalItems50 = response.total;
                           $scope.ItemsPerPage50 = response.per_page;
                           $scope.individualsList= response.data;
                       }   
                    }
                });
            };

            $scope.fetch=function(target){
                var params={'target':target,'action':'filters'};
                if(target == 'info'){
                     params.full_name =true;
                     params.person =true;
                     params.persons_i18n =true;
                     params.residence =true;
                     params.education =true;
                     params.health =true;
                     params.contacts =true;
                     params.islamic =true;
                     params.work =true;
                     params.banks =true;
                }
                else if(target == 'cases'){
                    $scope.cases_frm =true;
                    if(angular.isUndefined($rootScope.CurrentPage10)) {
                        params.page=1;
                    }else{
                        params.page=$rootScope.CurrentPage10;
                    }
                }
                else if(target =='payments'){
                    $scope.case_payments =true;
                    if(angular.isUndefined($rootScope.CurrentPage)) {
                        params.page=1;
                    }else{
                        params.page=$rootScope.CurrentPage;
                    }
                }
                else if(target == 'vouchers'){
                    $scope.case_vouchers =true;
                    if(angular.isUndefined($rootScope.CurrentPage11)) {
                        params.page=1;
                    }else{
                        params.page=$rootScope.CurrentPage11;
                    }
                }
                else if(target == 'individuals'){
                    $scope.case_individuals =true;
                    if(angular.isUndefined($rootScope.CurrentPage10)) {
                        params.page=1;
                    }else{
                        params.page=$rootScope.CurrentPage10;
                    }
                }
                else if(target == 'guardians'){
                    if(angular.isUndefined($rootScope.CurrentPage10)) {
                        params.page=1;
                    }else{
                        params.page=$rootScope.CurrentPage10;
                    }
                }
                else if(target == 'sponsorships'){
                    if(angular.isUndefined($rootScope.CurrentPage0)) {
                        params.page=1;
                    }else{
                        params.page=$rootScope.CurrentPage0;
                    }
                }

                resetSearchFilters(target);

                Content(params);
            };
            $scope.pageChanged = function (target,CurrentPage) {
                var param={};

                if(target  =='payments'){ param =$scope.case_payments_filters; }
                else if(target =='sponsorships'){ param =$scope.sponsorships_filters; }
                else if(target =='guardians'){ param = $scope.guardians_filters; }
                else if(target =='vouchers'){  param = $scope.voucher_filters;  }
                else if(target =='cases'){  param = $scope.cases_filters; }

                if(angular.isUndefined(CurrentPage)) {
                    CurrentPage=1;
                }

                param.target=target;
                param.page=CurrentPage;
                Content(param);
            };

            $scope.Search=function(target,data,action) {
                data.action=action;
                data.target=target;
                data.page=1;
                Content(data);
                
            };

        }

        $scope.dateOptions = { formatYear: 'yy',  startingDay: 0 };
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.today = function() { $scope.dt = new Date(); };
        $scope.clear = function() { $scope.dt = null; };

        $scope.format = $scope.formats[0];
        $scope.today();

        $scope.open1 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup1.opened = true; };
        $scope.open2 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup2.opened = true; };
        $scope.open3 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup3.opened = true; };
        $scope.open4 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup4.opened = true; };
        $scope.open5 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup5.opened = true; };
        $scope.open6 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup6.opened = true; };
        $scope.open7 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup7.opened = true; };
        $scope.open8 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup8.opened = true; };
        $scope.open9 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup9.opened = true;};
        $scope.open10 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup10.opened = true; };
        $scope.open11 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup11.opened = true; };
        $scope.open12 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup12.opened = true; };
        $scope.open13 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup13.opened = true; };
        $scope.open14 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup14.opened = true; };
        $scope.open88 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup88.opened = true; };
        $scope.open888 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup888.opened = true; };
        $scope.open90 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup90.opened = true;};
        $scope.open999 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup999.opened = true;};

        $scope.popup1  = {opened: false};
        $scope.popup2  = {opened: false};
        $scope.popup3  = {opened: false};
        $scope.popup4  = {opened: false};
        $scope.popup5  = {opened: false};
        $scope.popup6  = {opened: false};
        $scope.popup7  = {opened: false};
        $scope.popup8  = {opened: false};
        $scope.popup9  = {opened: false};
        $scope.popup10 = {opened: false};
        $scope.popup11 = {opened: false};
        $scope.popup12 = {opened: false};
        $scope.popup13 = {opened: false};
        $scope.popup14 = {opened: false};
        $scope.popup88  = {opened: false};
        $scope.popup888  = {opened: false};
        $scope.popup90  = {opened: false};
        $scope.popup999  = {opened: false};

    });
