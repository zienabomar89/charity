
angular.module('SponsorModule')
    .controller('SponsorPaymentsController', function ($http,$filter,$scope,$rootScope,$state,$ngBootbox,$uibModal,$stateParams,nominate,paymentsService,Org,Entity,OAuthToken, FileUploader,setting,Sponsor) {

        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        $state.current.data.pageTitle = $filter('translate')('payments list');
        $rootScope.settings.layout.pageSidebarClosed = true;
        $rootScope.CurrentPage = 1;
        $scope.Sponsorpayment=true;
        $rootScope.model_error_status =false;
        $rootScope.has_cheque_template=false;
        $scope.custom = true;
        $scope.master=false;
        $scope.sponsor=true;
        $scope.status ='';
        $rootScope.msg ='';
        $rootScope.payment={};
        $scope.items=[];
        $scope.itemsCount = 50;

        var resetSearchFilter =function(){
            $rootScope.payment={
                'page':1,
                'organization_id':"",
                'sponsor_id':"",
                'sponsorship_id':"",
                'category_id':"",
                'max_amount':"",
                'min_amount':"",
                'currency_id':"",
                'status':"",
                'payment_status':"",
                'start_exchange_date':"",
                'end_exchange_date':"",
                'start_payment_date':"",
                'end_payment_date':"",
                'min_beneficiary_no':"",
                'max_beneficiary_no':"",
                'itemsCount':50
            };
        };
        var LoadPayments =function(params){
            $rootScope.clearToastr();
            if(params.action != 'export'){
                $scope.items = [];              
            }

            $rootScope.progressbar_start();
            Sponsor.payments(params,function (response) {
                $rootScope.progressbar_complete();
                if(params.action == 'export'){
                      if(response.status == true){
                           var file_type='xlsx';
                           $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                           window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                      }else{
                           $rootScope.toastrMessages('error',$filter('translate')('no records to download'));
                      }
                }else{
                    // $scope.master=response.master;
                    $scope.items= response.Payments.data;
                    $scope.CurrentPage = response.Payments.current_page;
                    $scope.TotalItems = response.Payments.total;
                    $scope.ItemsPerPage = response.Payments.per_page;
                    $scope.TotalPayments=response.total;                
                }
            });

        };

        nominate.list({'id':-1},function(response){
            $scope.sponsorshipList = response.list;
        });
        var RestTemplate= function () {
            setting.getSettingById({'id':'default-cheque-template'},function (response) {
                $rootScope.has_cheque_template=response.status;
                if(response.status == true){
                    $rootScope.has_cheque_template=response.Setting.value +"";
                }
            });
        };
        var resetPaymentsTable = function () {
            $rootScope.payment.action ='filter';
            $rootScope.payment.page =$rootScope.CurrentPage;
            $rootScope.payment.itemsCount =$scope.itemsCount;
            LoadPayments( $rootScope.payment);
        };

        resetSearchFilter();
        RestTemplate();
        resetPaymentsTable();

        Entity.get({entity:'entities',c:'currencies,banks,paymentCategory,transferCompany'},function (response) {
            $scope.currency = response.currencies;
            $scope.Banks = response.banks;
            $scope.paymentCategory = response.paymentCategory;
            $rootScope.transferCompany = response.transferCompany;
        });
        Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

        $scope.pageChanged = function (currentPage) {
            $rootScope.CurrentPage = currentPage;
            resetPaymentsTable();
        };

        $scope.itemsPerPage_ = function (itemsCount) {
            $scope.itemsCount = itemsCount;
            $rootScope.CurrentPage = 1;
            resetPaymentsTable();
        };
        $scope.toggleCustom = function() {
            $scope.custom = $scope.custom === false ? true: false;
            if($scope.custom == true) {

            }else{
                resetSearchFilter();
            }
        };

        $scope.resetTransfer = function(value){
            $scope.payment.transfer_company_id = null;
        };


        $scope.allCheckbox = function(value){
            if(value){
                $scope.selectAll = true;
            }
            else{
                $scope.selectAll =false;

            }
            angular.forEach($scope.items, function(val,key)
            {
                val.check=$scope.selectAll;
            });
        };
        $scope.Search=function(selected , params,action) {
            $rootScope.clearToastr();

            var data = angular.copy(params);
            data.action = action;
            data.organization_ids = [];
                       data.payments=[];

            if(selected == true || selected == 'true' ){
            var payments=[];
            angular.forEach($scope.items, function(v, k){
               if(!angular.isUndefined(v.check)) {
                  if(v.check){
                      payments.push(v.id);
                  }
                }

            });

            if(payments.length == 0){
                $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                return ;
            }
            data.payments=payments;
        }
        
            if( !angular.isUndefined(data.organization_id)) {
                angular.forEach(data.organization_id, function(v, k) {
                    data.organization_ids.push(v.id);
                });
                delete data.organization_id;
            }

            if( !angular.isUndefined(data.start_exchange_date)) {
                data.start_exchange_date=$filter('date')(data.start_exchange_date, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(data.end_exchange_date)) {
                data.end_exchange_date= $filter('date')(data.end_exchange_date, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(data.start_payment_date)) {
                data.start_payment_date=$filter('date')(data.start_payment_date, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(data.end_payment_date)) {
                data.end_payment_date= $filter('date')(data.end_payment_date, 'dd-MM-yyyy')
            }

             data.page=1;
            LoadPayments(data);
        };
        $scope.export=function(id,target){
            $rootScope.clearToastr();
            var params={payments_id:id};

            if(target == 4){
                params.action='guaranteed';
            }else if(target == 5){
                params.ExportTo ='excel';
                params.action='signature_recipient';
            }else{
                params.target=target;
                params.action='all';
            }

            $rootScope.progressbar_start();
            $http({
                url: "/api/v1.0/sponsorship/paymentsService/exportCaseList",
                method: "POST",
                data: params
            }).then(function (response) {
                $rootScope.progressbar_complete();
                var file_type='xlsx';
                if(response.data.status == 'false' || response.data.status == false){
                    $rootScope.toastrMessages('error',  $filter('translate')('There are no cases to export file'));
                }else{
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.data.download_token;
                }

            }, function(error) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('error',error.data.msg);
            }); };

        $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {$scope.dt = new Date();};
        $scope.today();
        $scope.clear = function() {$scope.dt = null;};
        $scope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup1.opened = true;};
        $scope.open3 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup3.opened = true;};
        $scope.open7 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup7.opened = true;};
        $scope.open8 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup8.opened = true;};
        $scope.open9 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup9.opened = true;};
        $scope.popup8 = {opened: false};
        $scope.popup9 = {opened: false};
        $scope.popup3 = {opened: false};
        $scope.popup7 = {opened: false};
        $scope.popup1 = {opened: false};

    });

angular.module('SponsorModule')
    .controller('SponsorPaymentsPersonsController', function ($http,$filter,$rootScope,$scope,$state,$uibModal,$stateParams,paymentsService,Org,locations,Entity,Sponsor) {

        $rootScope.sponsorCategories_=[];
        angular.forEach($rootScope.sponsorCategories, function(v, k) {
            if ($rootScope.lang == 'ar'){
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
            }
            else{
                $rootScope.sponsorCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
            }
        });

        // set sidebar closed and body solid layout mode
        $rootScope.settings.layout.pageContentWhite = true;
        $rootScope.settings.layout.pageBodySolid = false;
        $rootScope.settings.layout.pageSidebarClosed = true;
        $state.current.data.pageTitle = $filter('translate')('Persons Payments');

        $scope.success =false;
        $scope.failed =false;
        $scope.error =false;
        $scope.Sponsorpayment=true;
        $scope.model_error_status =false;
        $scope.custom = true;
        $scope.master=false;
        $scope.sponsor=true;
        $scope.status ='';
        $scope.msg ='';
        $scope.filter={};
        $scope.items=[];
        $scope.CurrentPage = 1;
        $scope.itemsCount='50';


        $scope.close=function(){
            $scope.success =false;
            $scope.failed =false;
            $scope.error =false;
            $scope.model_error_status =false;
        };

        var resetPersonStatisticsFilter =function(){
            $scope.filter={
                "page":1,
                'payment_status':"",
                'status': "",
                'first_name': "",
                'second_name': "",
                'third_name': "",
                'last_name': "",
                'location_id': "",
                'category_id': "",
                'organization_id': "",
                'sponsor_id': "",
                'id_card_number': "",
                'g_id_card_number': "",
                'guardian_first_name': "",
                'guardian_second_name': "",
                'guardian_third_name': "",
                'guardian_last_name': "",
                'max_amount': "",
                'min_amount': "",
                'begin_date_from':"",
                'end_date_from':"",
                'begin_date_to':"",
                'end_date_to':"" ,
                'itemsCount':50
            };
        };
        var LoadPersonStatistics =function(params){
            $rootScope.progressbar_start();
            $rootScope.clearToastr();
            params.page =$scope.CurrentPage;
            if($stateParams.id){
               params.payment_id =$stateParams.id;
            }
            params.ctype = 'sponsorships';
            Sponsor.beneficiaries(params,function (response) {
                
                if(params.action == 'export'){
                      if(response.status == true){
                           var file_type='xlsx';
                           $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                           window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                      }else{
                           $rootScope.toastrMessages('error',$filter('translate')('no records to download'));
                      }
                }else{
                   
                $rootScope.progressbar_complete();
                $scope.items= response.cases.data;
                $scope.CurrentPage = response.cases.current_page;
                $scope.TotalItems = response.cases.total;
                $scope.ItemsPerPage = response.cases.per_page;
                // $scope.master=response.master;
                $scope.TotalPayments=response.total; 
                }
                
            });
        };
        Entity.get({entity:'entities',c:'currencies,banks,paymentCategory,transferCompany'},function (response) {
            $scope.currency = response.currencies;
            $scope.Banks = response.banks;
            $scope.paymentCategory = response.paymentCategory;
            $rootScope.transferCompany = response.transferCompany;
        });

        var resetTableStatistics = function () {
            $scope.filter.action ='filter';
            $scope.filter.page =$rootScope.CurrentPage;
            $scope.filter.itemsCount =$scope.itemsCount;
            LoadPersonStatistics( $scope.filter);
        };

        Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

        locations.get_type_location( {id:3}, function(response) {
            $scope.location=  response;
        });

        $scope.pageChanged = function (currentPage) {
            $scope.CurrentPage = currentPage;
            resetTableStatistics();
        };

        $scope.itemsPerPage_ = function (itemsCount) {
            $scope.itemsCount = itemsCount;
            $scope.CurrentPage = 1;
            resetTableStatistics();
        };

        $scope.toggleCustom = function() {
            $scope.custom = $scope.custom === false ? true: false;
            if($scope.custom === true) {
            }else{
                resetPersonStatisticsFilter();
            }
        };
        $scope.Search=function(selected ,params,action) {

            $rootScope.clearToastr();

            var data = angular.copy(params);
            data.action = action;
            data.persons =[];
            data.organization_ids = [];
            if(selected == true || selected == 'true' ){
                var persons=[];
                 angular.forEach($scope.items, function(v, k){
                    if( !angular.isUndefined(v.check)) {
                       if(v.check){
                           persons.push(v.cid);
                       }
                     }
                 });

                 if(persons.length==0){
                     $rootScope.toastrMessages('error',$filter('translate')('You have not select people to export'));
                     return ;
                 }
                 data.persons=persons;
             }
            if( !angular.isUndefined(data.organization_id)) {
                angular.forEach(data.organization_id, function(v, k) {
                    data.organization_ids.push(v.id);
                });
                delete data.organization_id;
            }
            if( !angular.isUndefined(data.start_cheque_date)) {
                data.start_cheque_date=$filter('date')(data.start_cheque_date, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(data.end_cheque_date)) {
                data.end_cheque_date= $filter('date')(data.end_cheque_date, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(data.start_payment_date)) {
                data.start_payment_date=$filter('date')(data.start_payment_date, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(data.end_payment_date)) {
                data.end_payment_date= $filter('date')(data.end_payment_date, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(data.begin_date_from)) {
                data.begin_date_from=$filter('date')(data.begin_date_from, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(data.end_date_from)) {
                data.end_date_from=$filter('date')(data.end_date_from, 'dd-MM-yyyy')
            }

            if( !angular.isUndefined(data.begin_date_to)) {
                data.begin_date_to=$filter('date')(data.begin_date_to, 'dd-MM-yyyy')
            }
            if( !angular.isUndefined(data.end_date_to)) {
                data.end_date_to=$filter('date')(data.end_date_to, 'dd-MM-yyyy')
            }

            data.action=action;
           data.page=1;
                LoadPersonStatistics(data);
        };

        $scope.allCheckbox = function(value){
            if(value){
                $scope.selectAll = true;
            }
            else{
                $scope.selectAll =false;

            }
            angular.forEach($scope.items, function(val,key)
            {
                val.check=$scope.selectAll;
            });
        };
        $scope.dateOptions = {formatYear: 'yy', startingDay: 0};
        $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
        $scope.format = $scope.formats[0];
        $scope.today = function() {$scope.dt = new Date();};
        $scope.today();
        $scope.clear = function() {$scope.dt = null;};
        $scope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup1.opened = true;};
        $scope.open3 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup3.opened = true;};
        $scope.open7 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup7.opened = true;};
        $scope.open8 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup8.opened = true;};
        $scope.open9 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup9.opened = true;};
        $scope.open25 = function($event)  {$event.preventDefault();$event.stopPropagation();$scope.popup25.opened = true;};
        $scope.open26 = function($event)  {$event.preventDefault();$event.stopPropagation();$scope.popup26.opened = true;};
        $scope.open215 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup215.opened = true;};
        $scope.open216 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup216.opened = true;};
        $scope.popup8 = {opened: false};
        $scope.popup9 = {opened: false};
        $scope.popup3 = {opened: false};
        $scope.popup7 = {opened: false};
        $scope.popup1 = {opened: false};
        $scope.popup215 = {opened: false};
        $scope.popup216 = {opened: false};
        $scope.popup25 = {opened: false};
        $scope.popup26 = {opened: false};

        resetPersonStatisticsFilter();
        resetTableStatistics();

    });
