
angular.module('SponsorModule')
    .factory('Sponsor', function ($resource) {
        return $resource('/api/v1.0/org/sponsor/:operation/:id', {id: '@id',page: '@page', ctype: '@ctype'}, {
            persons:     { method: 'POST', url: "/api/v1.0/org/sponsor/:ctype/persons", isArray: false}, 
            person:     { method: 'POST', url: "/api/v1.0/org/sponsor/:ctype/person", isArray: false}, 
            vouchers:    { method:'POST' ,params:{operation:'vouchers'}, isArray:false },
            payments:    { method:'POST' ,params:{operation:'payments'}, isArray:false },
            beneficiaries:     { method: 'POST', url: "/api/v1.0/org/sponsor/:ctype/beneficiaries", isArray: false}, 
        });
    })
