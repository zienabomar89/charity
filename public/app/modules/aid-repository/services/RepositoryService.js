AidRepositoryModule = angular.module('AidRepositoryModule');

AidRepositoryModule.factory('Repository', function ($resource) {
    return $resource('/api/v1.0/aid-repository/repositories/:operation/:id', {id: '@id',page: '@page'}, {
        query:  {method: 'GET', isArray: false},
        filter:    { method:'POST',params:{operation:'filter'}, isArray: false},
        trash:    { method:'POST',params:{operation:'trash'}, isArray: false},
        statistic:    { method:'POST',params:{operation:'statistic'}, isArray: false},
        MainStatistic:    { method:'POST',params:{operation:'MainStatistic'}, isArray: false},
        projectsOptions:    { method:'GET',params:{operation:'projectsOptions'}, isArray: false},
        projects:    { method:'GET',params:{operation:'projects'}, isArray: false},
        update: {method: 'PUT'},
        restore   : { method:'PUT', params: {operation:'restore'}, isArray: false}
        // filterCases:    { method:'POST',params:{operation:'filterCases'}, isArray: false}
        
    });
});

AidRepositoryModule.factory('Central', function ($resource) {
    return $resource('/api/v1.0/aid-repository/repositories/central/:operation/:id', {id: '@id',page: '@page'}, {
        query:  {method: 'GET', isArray: false},
        filter:    { method:'POST',params:{operation:'filter'}, isArray: false},
        statistic:    { method:'POST',params:{operation:'statistic'}, isArray: false},
        MainStatistic:    { method:'POST',params:{operation:'MainStatistic'}, isArray: false},
        projectsOptions:    { method:'GET',params:{operation:'projectsOptions'}, isArray: false},
        projects:    { method:'GET',params:{operation:'projects'}, isArray: false},
        saveOrg: {  method: 'put',params: {operation: 'organization-share'}, isArray: false}
       
    });
});

AidRepositoryModule.factory('Project', function ($resource) {
    return $resource('/api/v1.0/aid-repository/projects/:id/:operation', {id: '@id'}, {
        updateProjectStatus: {
            method:'PUT',
            params: {
                operation:'updateProjectStatus'
                ,id: '@id'
            }, isArray: false}
        , update: {
            method: 'PUT'
        },
        saveOrg: {
            method: 'POST',
            params: {
                operation: 'organization-share'
            }
        },
        getMobilesNumbers: {method: 'POST', params: {operation: 'getMobilesNumbers'}},
        rest_candidates:    { method:'put',params:{operation:'rest_candidates'}, isArray: false},

    });
});

AidRepositoryModule.factory('nominationRules', function ($resource) {
    return $resource('/api/v1.0/aid-repository/nomination-rules/:operation/:id', {page: '@page',id: '@id'}, {
        query:  {method: 'GET', isArray: false},
        group:    { method:'GET',params:{operation:'group'}, isArray: false},
        groups:    { method:'GET',params:{operation:'groups'}, isArray: true},
        statistic:    { method:'POST',params:{operation:'statistic'}, isArray: false},
        rule:    { method:'POST',params:{operation:'rule'}, isArray: false},
        saveNomination:    { method:'POST',params:{operation:'saveNomination'}, isArray: false},
        deleteNomination:    { method:'DELETE',params:{operation:'deleteNomination'}, isArray: false},
        candidates:    { method:'POST',params:{operation:'candidates'}, isArray: false},
        filter:    { method:'POST',params:{operation:'filter'}, isArray: false},
        projects:    { method:'GET',params:{operation:'projects'}, isArray: false},
        update: {method: 'PUT'},
        rest_candidates:    { method:'put',params:{operation:'rest_candidates'}, isArray: false}

    });
});

AidRepositoryModule.factory('organizationProject', function ($resource) {
    return $resource('/api/v1.0/aid-repository/organization/projects/:operation/:id', {page: '@page',id: '@id'}, {
        query:  {method: 'GET', isArray: false},
        filter:    { method:'POST',params:{operation:'filter'}, isArray: false},
        related:    { method:'POST',params:{operation:'related'}, isArray: false},
        projects:    { method:'GET',params:{operation:'projects'}, isArray: false},
        OrgProjects: {method: 'POST', params: {operation: 'OrgProjects'}},
        update: {method: 'PUT'}
    });
});


