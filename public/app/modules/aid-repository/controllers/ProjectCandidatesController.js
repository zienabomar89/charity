AidRepositoryModule = angular.module('AidRepositoryModule');

AidRepositoryModule.controller('ProjectCandidatesController', function ($scope,Org, $stateParams,$uibModal, $http, Project,$state,$filter ,nominationRules, $rootScope,organizationProject ,Repository,org_sms_service) {
    if(!$stateParams.msg){
        $rootScope.clearToastr();

    }
    $scope.project_id = $stateParams.id;
    $scope.nomination_id = $stateParams.nomination_id;

    $state.current.data.pageTitle = $filter('translate')('Projects');
    $scope.project = Project.get({id: $stateParams.id, repository: 1});
    $scope.project_owner=false;
    $scope.master=false;
    $rootScope.CurrentPage = 1;
    $scope.rows=true;
    // $scope.Search={};

    $rootScope.candidates=[];
    $scope.messages={};
    $rootScope.filter={page:1 ,action :'paginate'};
    $scope.statistic={accept: 0, all: 0, candidate:0, quantity: 0, rejected: 0, required: 0, sent: 0};

    $scope.nomination_name =null;
    $rootScope.progressbar_start();
    nominationRules.groups({id: $stateParams.id},function (response) {
        $rootScope.progressbar_complete();
        $scope.groups = response;
        if($stateParams.nomination_id) {
            angular.forEach($scope.groups, function (v, k) {
                if (v.id == $stateParams.nomination_id) {
                    $scope.nomination_name = v.label;
                }
            });
        }
    });

    Org.list({type:'organizations'},function (response) {  $scope.Org = response; });

    var  LoadTheCandidates = function(params){
        params.nomination_id = $stateParams.nomination_id;
        params.project_id = $stateParams.id;
        params.page = $rootScope.CurrentPage;
        params.action = 'paginate';
        params.src = 'main';

        // angular.element('.btn').addClass("disabled");
        $rootScope.progressbar_start();
        organizationProject.filter(params, function(response) {
            $rootScope.progressbar_complete();
            // angular.element('.btn').removeClass("disabled");
            // angular.element('.btn').removeAttr('disabled');
            var temp =  response.data;
            $rootScope.candidates=temp.data;
            $rootScope.CurrentPage = temp.current_page;
            $rootScope.TotalItems = temp.total;
            $rootScope.ItemsPerPage = temp.per_page;
            $scope.project_owner = response.project_owner;
            $scope.organization_id = response.organization_id;
            $scope.master = response.master;
            if(params.page  == 1){
                $scope.statistic = response.statistic;
            }
        });

    };

    LoadTheCandidates($rootScope.filter);

    $scope.pageChanged = function(current_page) {
        $rootScope.CurrentPage = current_page;
        LoadTheCandidates($rootScope.filter);
    };

    $scope.removeCandidate = function(person, idx) {

        $rootScope.clearToastr();
        //if(person.nominated_organization_id != person.organization_id){
        //    $rootScope.toastrMessages('error','دائرة المؤسسات فقط القادرة على حذف هذا الشخص');
        //}else{
        $rootScope.progressbar_start();
        $http({
            method: 'DELETE',
            url: '/api/v1.0/aid-repository/projects/'+$stateParams.id+'/candidates/delete/'+person.person_id
        }).then(function (response) {
            $rootScope.progressbar_complete();
            if (response.data.result) {
                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                $rootScope.candidates.splice(idx, 1);
            } else {
                $rootScope.toastrMessages('error',response.data.msg);
            }
        });
        //}

    };

    $scope.checkAll=function(value){
        var checked = false;
        if (value) {
            checked = true;
        }
        angular.forEach($rootScope.candidates, function(v, k) {
            v.checked=checked;
        });
    };
    $scope.sendSms=function(type,receipt_id,size) {

        if(type == 'manual'){
            var receipt = [];
            var pass = true;

            if(receipt_id ==0){
                angular.forEach($scope.candidates, function (v, k) {
                    if (v.checked) {
                        receipt.push(v.person_id);
                    }
                });
                if(receipt.length==0){
                    pass = false;
                    $rootScope.toastrMessages('error',$filter('translate')("you did not select anyone to send them sms message"));
                    return;
                }
            }else{
                receipt.push(receipt_id);
            }

            if(pass){
                org_sms_service.getList(function (response) {
                    $rootScope.Providers = response.Service;
                });
                $uibModal.open({
                    templateUrl: '/app/modules/sms/views/modal/sent_sms_from_person_general.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance) {
                        $scope.sms={source: 'check' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:"",cont_type:""};
                        $scope.confirm = function (data) {
                            $rootScope.clearToastr();
                            var params = angular.copy(data);
                            params.receipt=receipt;
                            params.target='person';

                            $rootScope.progressbar_start();
                            org_sms_service.sendSmsToTarget(params,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed') {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status== 'success'){
                                    if(response.download_token){
                                        var file_type='xlsx';
                                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                    }

                                    $modalInstance.close();
                                    $rootScope.toastrMessages('success',response.msg);
                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                            });
                        };


                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    },
                    resolve: {
                    }
                });

            }
        }
        else{
            org_sms_service.getList(function (response) {
                $rootScope.Providers = response.Service;
            });
            $uibModal.open({
                templateUrl: '/app/modules/sms/views/modal/sent_sms_from_xlx_general.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;
                    $scope.sms={source: 'file' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:"",cont_type:""};

                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };

                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };
                    $scope.download=function(){
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-to-send-sms-template&template=true';
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: 'api/v1.0/sms/excel',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });


                    $scope.confirm = function (item) {
                        $scope.spinner=false;
                        $scope.import=false;
                        $rootScope.progressbar_start();
                        Uploader.onBeforeUploadItem = function(item) {

                            if($scope.sms.same_msg == 0){
                                $scope.sms.sms_messege = '';
                            }
                            item.formData = [$scope.sms];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);

                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            else if(response.status=='success')
                            {
                                if(response.download_token){
                                    var file_type='xlsx';
                                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                }
                                $scope.spinner=false;
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $scope.import=false;
                                $rootScope.toastrMessages('success',response.msg);
                                $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});

        }
    };

    $scope.updateCandidatesStatus=function(status){

        $rootScope.clearToastr();

        if(status == -1){
            $uibModal.open({
                size: 'sm',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                templateUrl: 'detail.html',
                controller: function ($rootScope,$scope,$modalInstance) {
                    $scope.data = {reason : null};
                    $scope.confirm = function (params) {
                        $rootScope.clearToastr();
                        $rootScope.progressbar_start();
                        $http({
                            method: 'PUT',
                            url: '/api/v1.0/aid-repository/projects/'+$stateParams.id+'/candidates/status',
                            data: {persons: persons, status: status, reason: params.reason}
                        }).then(function (response) {
                            $rootScope.progressbar_complete();
                            if(response.data.status){
                                if(response.data.status == 'error'){
                                    $rootScope.toastrMessages('error',response.data.msg);
                                }else{
                                    $rootScope.toastrMessages('success',response.data.msg);
                                }
                                LoadTheCandidates($rootScope.filter);
                            }else{
                                $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                            }
                        });
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }});
        }else{
            var persons=[];
            angular.forEach($rootScope.candidates, function(v, k){

                if(v.checked){
                    if(parseInt(v.status) != status){
                        if( (parseInt(v.is_mine) == 1)){
                            persons.push({'id':v.person_id});
                        }
                    }
                }

            });
            if(persons.length==0){
                $rootScope.toastrMessages('error',$filter('translate')('You have not selected specific people or people who have the same status to edit or you have selected people who are not registered as an organization'));
            }
            else{
                if(status == 4){
                    $uibModal.open({

                        size: 'sm',
                        backdrop  : 'static',
                        keyboard  : false,
                        windowClass: 'modal',
                        templateUrl: 'detail.html',
                        controller: function ($rootScope,$scope,$modalInstance) {
                            $scope.data = {reason : null};
                            $scope.confirm = function (params) {
                                $rootScope.clearToastr();
                                $rootScope.progressbar_start();
                                $http({
                                    method: 'PUT',
                                    url: '/api/v1.0/aid-repository/projects/'+$stateParams.id+'/candidates/status',
                                    data: {persons: persons, status: status, reason: params.reason}
                                }).then(function (response) {
                                    $rootScope.progressbar_complete();
                                    if(response.data.status){
                                        if(response.data.status == 'error'){
                                            $rootScope.toastrMessages('error',response.data.msg);
                                        }else{
                                            $rootScope.toastrMessages('success',response.data.msg);
                                        }
                                        LoadTheCandidates($rootScope.filter);
                                    }else{
                                        $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                                    }
                                });
                            };
                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };
                        }});
                }
                else{
                    $rootScope.clearToastr();
                    $rootScope.progressbar_start();
                    $http({
                        method: 'PUT',
                        url: '/api/v1.0/aid-repository/projects/'+$stateParams.id+'/candidates/status',
                        data: {persons: persons, status: status, reason: null}
                    }).then(function (response) {
                        $rootScope.progressbar_complete();
                        if(response.data.status){
                            if(response.data.status == 'error'){
                                $rootScope.toastrMessages('error',response.data.msg);
                            }else{
                                $rootScope.toastrMessages('success',response.data.msg);
                            }
                            LoadTheCandidates($rootScope.filter);
                        }else{
                            $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                        }
                    });
                }
            }
        }
    };

    $scope.updateStatus = function(person_id,status, idx) {

        $rootScope.clearToastr();

        if(status == 4){

            $uibModal.open({

                size: 'sm',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                templateUrl: 'detail.html',
                controller: function ($rootScope,$scope,$modalInstance) {

                    $scope.data = {reason : null};
                    $scope.confirm = function (params) {
                        $rootScope.clearToastr();
                        $rootScope.progressbar_start();
                        $http({
                            method: 'PUT',
                            url: '/api/v1.0/aid-repository/projects/'+$stateParams.id+'/candidates/status',
                            data: {persons: [{'id':person_id}], 'status': status , 'reason': params.reason}
                        }).then(function (response) {
                            $rootScope.progressbar_complete();
                            if(response.data.status){
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                $rootScope.candidates[idx].status = status;
                                $rootScope.candidates[idx].status_name = response.data.status_name;

                                if(response.data.status == 'error'){
                                    $rootScope.toastrMessages('error',response.data.msg);
                                }else{
                                    $rootScope.toastrMessages('success',response.data.msg);
                                }
                            }else{
                                $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                            }

                            $modalInstance.close();
                        }, function(error) {
                            $modalInstance.close();
                            $rootScope.toastrMessages('error',error.data.msg);
                        });

                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }});

        }else{
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            $http({
                method: 'PUT',
                url: '/api/v1.0/aid-repository/projects/'+$stateParams.id+'/candidates/status',
                data: {persons: [{'id':person_id}], status: status, reason: null}
            }).then(function (response) {
                $rootScope.progressbar_complete();
                if(response.data.status == 'error'){
                    $rootScope.toastrMessages('error',response.data.msg);
                }else{
                    $rootScope.toastrMessages('success',response.data.msg);
                }
                $rootScope.candidates[idx].status = status;
                $rootScope.candidates[idx].status_name = response.data.status_name;
            });
        }

    };


    $scope.SearchCandidates = function (selected,data,action) {
      
        $rootScope.clearToastr();
        
        var params = angular.copy(data);
            params.persons=[];

        if(selected == true || selected == 'true' ){
           var persons=[];
            angular.forEach($rootScope.candidates, function(v, k){

                if(v.checked){
                    persons.push(v.person_id);
                }

            });
            
            if(persons.length==0){
                $rootScope.toastrMessages('error',$filter('translate')('You have not select people to export'));
                return ;
            }
            params.persons=persons;
        }

        var orgs=[];
        angular.forEach(params.organization_id, function(v, k) {
            orgs.push(v.id);
        });

        params.organization_id=orgs;
        params.action=action;
        params.type=$scope.type;
        params.src = 'main';
        params.nomination_id = $stateParams.nomination_id;
        params.project_id = $stateParams.id;
            
        if(action == 'children' || action == 'wives'){
            
            $rootScope.progressbar_start();
            organizationProject.related(params, function(response) {
                $rootScope.progressbar_complete();
                if(response.status == false){
                        $rootScope.toastrMessages('error',$filter('translate')('There are no names matching the search settings you entered'));
                }else{

                    if(response.status == true){
                       var file_type='zip';
                       $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                       window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                    }else{
                       $rootScope.toastrMessages('error',$filter('translate')('no records to download'));
                    }
               }

            });

        }
        else if(action == 'excel'){

            $rootScope.progressbar_start();
                organizationProject.filter(params, function(response) {
                    $rootScope.progressbar_complete();
                    if(response.status == false){
                        $rootScope.toastrMessages('error',$filter('translate')('There are no names matching the search settings you entered'));
                    }else{

                        if(response.status == true){
                            var file_type='xlsx';
                            if(action == 'ExportToWord' || action == 'export') {
                                file_type='zip';
                            }
                            $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;

                        }else{
                            $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                        }
                    }
            });


        }
        else{
            params.page=$rootScope.CurrentPage;
            LoadTheCandidates(params);
        }
    };

    $stateParams.msg = null;

    $scope.statistic = function(id) {
        $rootScope.clearToastr();
        angular.element('.btn').addClass("disabled");

        $rootScope.progressbar_start();
        Repository.statistic({project_id: $stateParams.id}, function(response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if (response.status===false || response.status==='false') {
                $rootScope.toastrMessages('error',response.msg);
            }else{
                if(!angular.isUndefined(response.download_token)) {
                    $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }
            }

        });

    };

});