AidRepositoryModule = angular.module('AidRepositoryModule');

// list all project of organization
AidRepositoryModule.controller('RepositoryOrganizationProjectsController', function ($filter,$uibModal,$rootScope,$scope,FileUploader,Entity,organizationProject, OAuthToken, $http,Project,cs_persons_vouchers,vouchers_categories,Org,$state) {

    $rootScope.clearToastr();

    $state.current.data.pageTitle = $filter('translate')('Projects');
    $scope.master=true;
    $scope.projects=[];
    $rootScope.CurrentPage = 1;
    $scope.currentPage=1;
    $scope.itemsCount='50';
    $rootScope.action = 'paginate';
    $scope.filters = {action:'paginate' ,sponsor_id:'' ,repository_name:'' , project_name:'' , project_status:'' ,
                      category_id:"", project_date_from:"", project_date_to:"",
                      page : $scope.currentPage , itemsCount:$scope.itemsCount};

    $scope.resetFilters=function(){
        $scope.filters = {action:'paginate' ,sponsor_id:'' ,repository_name:'' , project_name:'' , project_status:'' ,
            category_id:"", project_date_from:"", project_date_to:"",
            page : $scope.currentPage , itemsCount:$scope.itemsCount};
    };

    var floatRound_ = function(number, precision) {
        return number;
        var factor = Math.pow(10, precision);
        var tempNumber = number * factor;
        var roundedTempNumber = Math.round(tempNumber);
        return roundedTempNumber / factor;
    };

    function addDays(date, days) {
        var result = new Date(date);
        result.setDate(result.getDate() + days);
        return result;
    }

    function isDateAfterToday(date) {
        return new Date(date.toDateString()) > new Date(new Date().toDateString());
    }

    var loadProjects = function(){
        
        $rootScope.progressbar_start();
        var params = angular.copy($scope.filters);
        params.page = $rootScope.currentPage ;

        if( !angular.isUndefined(params.project_date_from)) {
            params.project_date_from=$filter('date')(params.project_date_from, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(params.project_date_to)) {
            params.project_date_to= $filter('date')(params.project_date_to, 'dd-MM-yyyy')
        }
        
        organizationProject.OrgProjects(params,function (response) {
            $rootScope.progressbar_complete();
            $scope.total1 = $scope.total2 = $scope.total3= $scope.total4= $scope.total5 = 0;
            $scope.master = response.master;
            $scope.organization_id = response.organization_id;

            var temp =  response.entries.data;
            if (temp.length )
            {
                angular.forEach(temp, function(e, i) {
                    e.not_allow_add = false;
                    if(e.status == 3){
                        e.allow_day_ = e.allow_day;
                        e.allow_day = addDays(new Date(e.date),parseInt(e.allow_day));
                        e.not_allow_add = isDateAfterToday(e.allow_day);
                    }
                    e.quantity = parseInt(e.quantity);
                    e.extra_quantity = parseInt(e.extra_quantity);
                    e.extra_ratio = parseFloat(e.extra_ratio);
                    e.ratio = parseFloat(e.ratio);
                    e.total_quantity_share = floatRound_((e.extra_ratio  + e.ratio),3);

                    $scope.total1 += e.quantity;
                    $scope.total2 += e.extra_quantity;
                    $scope.total3 += e.quantity + e.extra_quantity;
                    $scope.total4 += (e.project_amount  *(e.quantity + e.extra_quantity));
                    $scope.total5 += e.ratio + e.extra_ratio;
                });
            }
            $scope.projects = temp;
            $scope.currentPage = response.entries.current_page;
            $rootScope.TotalItems = response.entries.total;
            $rootScope.ItemsPerPage = response.entries.per_page;
        });
    }
   
    vouchers_categories.List(function (response) {
        $rootScope.Choices = response;
    });
    $rootScope.categories = vouchers_categories.List();
    Entity.get({entity:'entities',c:'currencies,transferCompany'},function (response) {
        $rootScope.currency = response.currencies;
        $rootScope.transferCompany = response.transferCompany;
    });
    Org.list({type: "sponsors"}, function(response) {
        $rootScope.Sponsors = response;
    });
    // check project
    $scope.insert=function(project,size){

        $rootScope.clearToastr();
        //$scope.project = Project.get({id: project.project_id, repository: 1});
        $rootScope.voucher={
            title:project.repository_name +' '+ project.project_name,
            project_id:project.project_id,
            organization_id:project.organization_id,
            case_category_id:project.case_category_id,
            category_id:project.category_id,
            sponsor_id:project.sponsor_id,
            value:project.project_amount,
            count:project.quantity + project.extra_quantity,
            allow_day:project.allow_day_,
            currency_id:project.currency_id,
            voucher_date:new Date(),
            exchange_rate:project.exchange_rate,
            beneficiary:1
        };

        $uibModal.open({
            templateUrl: 'myModalContent.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: size,
            controller: function ($rootScope,$scope, $modalInstance, $log,cs_persons_vouchers,Entity) {
                // receipt_time input
                $scope.sponsor_name = project.sponsor_name;
                $scope.mytime = new Date();
                $scope.displayTime = 'n/a';
                $scope.$watch('mytime', function(newValue, oldValue) {
                    var hour = $scope.mytime.getHours()-($scope.mytime.getHours() >= 12 ? 12 : 0),
                        period = $scope.mytime.getHours() >= 12 ? 'PM' : 'AM';
                    $scope.displayTime = hour+':'+$scope.mytime.getMinutes()+' '+period
                })
                $scope.hstep = 1;
                $scope.mstep = 1;
                $scope.options = {
                    hstep: [1, 2, 3],
                    mstep: [1, 2, 3]
                };
                $scope.ismeridian = true;
                $scope.toggleMode = function() {
                    $scope.ismeridian = !$scope.ismeridian;
                };
                $scope.confirm = function (vouchers) {
                    $rootScope.progressbar_complete();
                    $rootScope.clearToastr();
                    vouchers.receipt_time = $filter('date')( new Date($scope.mytime), 'HH:MM:ss');

                    if( !angular.isUndefined(vouchers.voucher_date)) {
                        vouchers.voucher_date=$filter('date')(vouchers.voucher_date, 'dd-MM-yyyy')
                    }
                    if( !angular.isUndefined(vouchers.receipt_date)) {
                        vouchers.receipt_date=$filter('date')(vouchers.receipt_date, 'dd-MM-yyyy')
                    }

                    if( !angular.isUndefined(vouchers.transfer)) {
                        if(vouchers.transfer == 0 ||vouchers.transfer == '0'){
                            vouchers.transfer_company_id = null
                        }
                    }

                    cs_persons_vouchers.save(vouchers,function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                            }
                            else{
                                loadProjects();
                                $modalInstance.close();
                                if(response.status=='success')
                                {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                            }
                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',error.data.msg);
                            $modalInstance.close();
                        });                       

                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };

                $scope.open = function($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.popup.opened = true;
                };

                $scope.popup = {
                    opened: false
                };

                $scope.open45 = function($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.popup45.opened = true;
                };

                $scope.popup45 = {
                    opened: false
                };

                $scope.dateOptions = {
                    formatYear: 'yy',
                    startingDay: 0
                };
                $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                $scope.format = $scope.formats[0];
                $scope.today = function() {
                    $scope.dt = new Date();
                };
                $scope.today();
                $scope.clear = function() {
                    $scope.dt = null;
                };
            }});

    };

    // check candidates on project by id
    $scope.check = function(id){
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: 'checkPerson_.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'md',
            controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {
                $scope.upload=false;

                $scope.fileUpload=function(){
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.upload=true;
                };

                $scope.download=function(){
                    angular.element('.btn').addClass("disabled");

                    $rootScope.progressbar_start();
                    $rootScope.clearToastr();
                    $http({
                        url:'/api/v1.0/common/reports/template',
                        method: "GET"
                    }).then(function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token+'&template=true';
                    }, function(error) {
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                };

                var Uploader = $scope.uploader = new FileUploader({
                    url:'/api/v1.0/aid-repository/projects/check',
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });

                Uploader.onBeforeUploadItem = function(item) {
                    $rootScope.clearToastr();
                    item.formData = [{source: 'file',project_id:id}];
                };

                $scope.confirm = function () {
                    angular.element('.btn').addClass("disabled");
                    $rootScope.progressbar_start();
                    $rootScope.clearToastr();
                    $scope.spinner=false;
                    $scope.import=false;
                    angular.element('.btn').addClass("disabled");

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(response.status=='success')
                        {
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $scope.spinner=false;
                            $scope.import=false;
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        }else{
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $rootScope.toastrMessages('error',response.msg);
                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                        }
                        if(response.download_token){
                            $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                            var file_type='xlsx';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }
                        $modalInstance.close();

                    };
                    Uploader.uploadAll();
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.remove=function(){
                    angular.element("input[type='file']").val(null);
                    $scope.uploader.clearQueue();
                    angular.forEach(
                        angular.element("input[type='file']"),
                        function(inputElem) {
                            angular.element(inputElem).val(null);
                        });
                };

            }});
    };

    loadProjects();

    $scope.pageChanged = function(current_page) {
        $rootScope.currentPage = current_page;
        loadProjects();
    };

    $scope.itemsPerPage_ = function (itemsCount) {
        $rootScope.currentPage = 1;
        $scope.filters.itemsCount=itemsCount;
        loadProjects();
    };

    $scope.searchWithFilter = function(params ,action) {
        $scope.filters.action = action;
        loadProjects();
    };

    $rootScope.dateOptions = { formatYear: 'yy',  startingDay: 0 };
    $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $rootScope.today = function() { $rootScope.dt = new Date(); };
    $rootScope.clear = function() { $rootScope.dt = null; };

    $rootScope.format = $rootScope.formats[0];
    $rootScope.today();

    $rootScope.popup99 = {opened: false};
    $rootScope.popup88 = {opened: false};
    $rootScope.popup999 = {opened: false};
    $rootScope.popup888 = {opened: false};

    $rootScope.popup880  = {opened: false};
    $rootScope.popup90  = {opened: false};
    $rootScope.popup880  = {opened: false};
    $rootScope.popup900  = {opened: false};

    $scope.open = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup.opened = true;};
    $scope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup1.opened = true;};
    $scope.open2 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup2.opened = true;};
    $scope.open3 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup3.opened = true;};
    $scope.open4 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup4.opened = true;};
    $scope.popup = {opened: false};
    $scope.popup1 = {opened: false};
    $scope.popup2 = {opened: false};
    $scope.popup3 = {opened: false};
    $scope.popup4 = {opened: false};


    $rootScope.open880 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup880.opened = true;};
    $rootScope.open999 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup999.opened = true;};
    $rootScope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup99.opened = true;};
    $rootScope.open888 = function($event) {$event.preventDefault();$event.stopPropagation();$rootScope.popup888.opened = true;};

    $rootScope.open88 = function($event) { $event.preventDefault(); $event.stopPropagation(); $rootScope.popup88.opened = true; };
    $rootScope.open90 = function($event) { $event.preventDefault(); $event.stopPropagation(); $rootScope.popup90.opened = true;};
    $rootScope.open880 = function($event) { $event.preventDefault(); $event.stopPropagation(); $rootScope.popup880.opened = true; };
    $rootScope.open900 = function($event) { $event.preventDefault(); $event.stopPropagation(); $rootScope.popup900.opened = true;};

});

// list all nominations of project organization  using nomination_id & project_id
AidRepositoryModule.controller('RepositoryOrganizationProjectsNominationsListController', function ($rootScope, $scope, $http, $stateParams, Project,$filter,$state,nominationRules,$uibModal,$ngBootbox,Repository) {

    $rootScope.clearToastr();
    $state.current.data.pageTitle = $filter('translate')('Nomination');
    $scope.project_id = $stateParams.id;
    $scope.project = nominationRules.group({id: $stateParams.id});

    $scope.statistic = function() {
        $rootScope.clearToastr();
        angular.element('.btn').addClass("disabled");

        $rootScope.progressbar_start();
        Repository.statistic({project_id: $stateParams.id}, function(response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if (response.status===false || response.status==='false') {
                $rootScope.toastrMessages('error',response.msg);
            }else{
                if(!angular.isUndefined(response.download_token)) {
                    $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }
            }

        });

    };
});

// show nomination rules to organization using nomination_id & project_id
AidRepositoryModule.controller('RepositoryOrganizationProjectsRulesController', function ($filter,$scope, $rootScope, $stateParams, $http, Project,nominationRules) {

    $rootScope.clearToastr();
    $scope.rules = [];
    $rootScope.progressbar_start();
    nominationRules.rule({strict:1 ,project_id: $stateParams.id,nomination_id:$stateParams.nomination_id},function (response) {
        $rootScope.progressbar_complete();
        if(response.status == 'false' || response.status == false){
            $rootScope.toastrMessages('error',$filter('translate')('No nomination conditions'));
            return ;
        }
        $scope.rules = response.rules;
        $scope.nomination = response.nomination;
        $scope.project = response.nomination.project;
    });

});

// show candidates of project to organization using nomination_id & project_id
AidRepositoryModule.controller('RepositoryOrganizationProjectsCandidatesController', function ($scope,Org, $stateParams,$uibModal, $http, Project,$state,$filter , $rootScope,organizationProject,org_sms_service,Repository,nominationRules) {

    $rootScope.clearToastr();
    $state.current.data.pageTitle = $filter('translate')('Projects');
    $scope.project_id = $stateParams.id;
    $scope.nomination_id = $stateParams.nomination_id;

    $scope.project_owner=false;
    $scope.master=false;
    $rootScope.CurrentPage = 1;
    $scope.rows=true;
    $rootScope.candidates=[];
    $rootScope.filter={page:1 ,action :'paginate',type:'out'};
    $scope.statistic={accept: 0, all: 0, candidate:0, quantity: 0, rejected: 0, required: 0, sent: 0};

    $scope.project = Project.get({id: $stateParams.id, repository: 1});

    $scope.nomination_name =null;
    $rootScope.progressbar_start();
    nominationRules.groups({id: $stateParams.id},function (response) {
        $rootScope.progressbar_complete();
        $scope.groups = response;
        if($stateParams.nomination_id) {
            angular.forEach($scope.groups, function (v, k) {
                if (v.id == $stateParams.nomination_id) {
                    $scope.nomination_name = v.label;
                }
            });
        }
    });


    Org.list({type:'organizations'},function (response) {  $scope.Org = response; });
    $scope.sendSms=function(type,receipt_id,size) {

        if(type == 'manual'){
            var receipt = [];
            var pass = true;

            if(receipt_id ==0){
                angular.forEach($scope.candidates, function (v, k) {
                    if (v.checked) {
                        receipt.push(v.person_id);
                    }
                });
                if(receipt.length==0){
                    pass = false;
                    $rootScope.toastrMessages('error',$filter('translate')("you did not select anyone to send them sms message"));
                    return;
                }
            }else{
                receipt.push(receipt_id);
            }

            if(pass){
                org_sms_service.getList(function (response) {
                    $rootScope.Providers = response.Service;
                });
                $uibModal.open({
                    templateUrl: '/app/modules/sms/views/modal/sent_sms_from_person_general.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance) {
                        $scope.sms={source: 'check' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:"",cont_type:""};
                        $scope.confirm = function (data) {
                            $rootScope.clearToastr();
                            var params = angular.copy(data);
                            params.receipt=receipt;
                            params.target='person';

                            $rootScope.progressbar_start();
                            org_sms_service.sendSmsToTarget(params,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed') {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status== 'success'){
                                    if(response.download_token){
                                        var file_type='xlsx';
                                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                    }

                                    $modalInstance.close();
                                    $rootScope.toastrMessages('success',response.msg);
                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                            });
                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    },
                    resolve: {
                    }
                });

            }
        }
        else{
            org_sms_service.getList(function (response) {
                $rootScope.Providers = response.Service;
            });
            $uibModal.open({
                templateUrl: '/app/modules/sms/views/modal/sent_sms_from_xlx_general.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;
                    $scope.sms={source: 'file' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:"",cont_type:""};

                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };

                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };
                    $scope.download=function(){
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-to-send-sms-template&template=true';
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: 'api/v1.0/sms/excel',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });


                    $scope.confirm = function (item) {
                        $scope.spinner=false;
                        $scope.import=false;
                        $rootScope.progressbar_start();
                        Uploader.onBeforeUploadItem = function(item) {

                            if($scope.sms.same_msg == 0){
                                $scope.sms.sms_messege = '';
                            }
                            item.formData = [$scope.sms];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);

                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            else if(response.status=='success')
                            {
                                if(response.download_token){
                                    var file_type='xlsx';
                                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                }
                                $scope.spinner=false;
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $scope.import=false;
                                $rootScope.toastrMessages('success',response.msg);
                                $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $rootScope.aidsCasesfilters.page=$scope.CurrentPage;
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});

        }
    };

    var  LoadTheCandidates = function(params){

        params.nomination_id = $stateParams.nomination_id;
        params.project_id = $stateParams.id;
        params.page = $rootScope.CurrentPage;
        params.action = 'paginate';
        params.src = 'org';
        // angular.element('.btn').addClass("disabled");
        $rootScope.progressbar_start();
        organizationProject.filter(params, function(response) {
            $rootScope.progressbar_complete();
            // angular.element('.btn').removeClass("disabled");
            // angular.element('.btn').removeAttr('disabled');
            var temp =  response.data;
            $rootScope.candidates=temp.data;
            $rootScope.CurrentPage = temp.current_page;
            $rootScope.TotalItems = temp.total;
            $rootScope.ItemsPerPage = temp.per_page;
            $scope.project_owner = response.project_owner;
            $scope.organization_id = response.organization_id;
            $scope.master = response.master;
            if(params.page  == 1){
                $scope.statistic = response.statistic;
            }
        });

    };

    LoadTheCandidates($rootScope.filter);

    $scope.pageChanged = function(current_page) {
        $rootScope.CurrentPage = current_page;
        LoadTheCandidates($rootScope.filter);
    };
    $scope.removeCandidate = function(person, idx) {
        $rootScope.clearToastr();

        //if(person.nominated_organization_id != person.organization_id){
        //    $rootScope.toastrMessages('error','دائرة المؤسسات فقط القادرة على حذف هذا الشخص');
        //}else{
        $rootScope.progressbar_start();
        $http({
            method: 'DELETE',
            url: '/api/v1.0/aid-repository/projects/'+$stateParams.id+'/candidates/delete/'+person.person_id
        }).then(function (response) {
            $rootScope.progressbar_complete();
            if (response.data.result) {
                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                $rootScope.candidates.splice(idx, 1);
            } else {
                $rootScope.toastrMessages('error',$filter('translate')('action failed'));
            }
        });
        //}

    };
    $scope.checkAll=function(value){
        var checked = false;
        if (value) {
            checked = true;
        }
        angular.forEach($rootScope.candidates, function(v, k) {
            v.checked=checked;
        });
    };
    $scope.updateCandidatesStatus=function(status){
        $rootScope.clearToastr();

        if(status == -1){
            $uibModal.open({
                templateUrl: '/app/modules/aid-repository/views/projects/update_candidates_status.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'md',
                controller: function ($rootScope,$scope, $modalInstance, $log, FileUploader, OAuthToken) {

                    $rootScope.download=function(){
                        $rootScope.clearToastr();
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token=update-candidates-status-template&template=true';
                    };

                    $scope.upload=false;
                    $scope.FormData={};

                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };

                    $scope.fileupload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                        if( !angular.isUndefined($scope.FormData.status)) {
                            if($scope.FormData.status != ""){
                                $scope.upload=true;
                            }
                        }
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: '/api/v1.0/aid-repository/projects/UpdateCandidatesStatus',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });


                    $scope.confirm = function (item) {
                        $rootScope.progressbar_start();
                        $rootScope.clearToastr();
                        Uploader.onBeforeUploadItem = function(i) {
                            $rootScope.clearToastr();
                            i.formData = [{status: item.status , project_id: $stateParams.id}];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);
                            if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $rootScope.status1 =response.status;
                                $rootScope.msg1 =response.msg;
                            }
                            else{
                                if(response.status=='success')
                                {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                    LoadTheCandidates($rootScope.filter);
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                $modalInstance.close();
                            }
                        };
                        Uploader.uploadAll();
                    };

                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };

                }
            });
        }else{
            var persons=[];
            angular.forEach($rootScope.candidates, function(v, k){

                if(v.checked){
                    if( ($rootScope.can('aidRepository.repositories.manageCandidates') || parseInt(v.is_mine) == 1)){
                        persons.push({'id':v.person_id});
                    }
                }

            });
            if(persons.length==0){
                $rootScope.toastrMessages('error',$filter('translate')('You have not selected specific people or people who have the same status to edit or you have selected people who are not registered as an organization'));
            }
            else{

                if(status == 4){

                    $rootScope.clearToastr();
                    $uibModal.open({

                        size: 'sm',
                        backdrop  : 'static',
                        keyboard  : false,
                        windowClass: 'modal',
                        templateUrl: 'detail.html',
                        controller: function ($rootScope,$scope,$modalInstance) {
                            $scope.data = {reason : null};
                            $scope.confirm = function (params) {
                                $rootScope.clearToastr();
                                $rootScope.progressbar_start();
                                $http({
                                    method: 'PUT',
                                    url: '/api/v1.0/aid-repository/projects/'+$stateParams.id+'/candidates/status',
                                    data: {persons: persons, status: status, reason: params.reason}
                                }).then(function (response) {
                                    $rootScope.progressbar_complete();
                                    if(response.data.status == 'success'){
                                        $rootScope.toastrMessages('success',response.data.msg );
                                        LoadTheCandidates($rootScope.filter);
                                    }else{
                                        $rootScope.toastrMessages('error',response.data.msg );
                                    }

                                    $modalInstance.close();

                                });
                            };
                            $scope.cancel = function () {
                                $modalInstance.dismiss('cancel');
                            };
                        }});

                }
                else{
                    $rootScope.progressbar_start();
                    $http({
                        method: 'PUT',
                        url: '/api/v1.0/aid-repository/projects/'+$stateParams.id+'/candidates/status',
                        data: {persons: persons, status: status, reason: null}
                    }).then(function (response) {
                        $rootScope.progressbar_complete();
                        if(response.data.status){
                            if(response.data.status == 'success'){
                                $rootScope.toastrMessages('success',response.data.msg );
                                LoadTheCandidates($rootScope.filter);
                            }else{
                                $rootScope.toastrMessages('error',response.data.msg );
                            }
                        }
                    });
                }
            }
        }
    };
    
    var status_name = function(val){
        
    }
    $scope.updateStatus = function(person_id,status, idx) {


        $rootScope.clearToastr();
        if(status == 4){

            $uibModal.open({

                size: 'sm',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                templateUrl: 'detail.html',
                controller: function ($rootScope,$scope,$modalInstance) {

                    $scope.data = {reason : null};
                    $scope.confirm = function (params) {
                        $rootScope.clearToastr();
                        $rootScope.progressbar_start();
                        $http({
                            method: 'PUT',
                            url: '/api/v1.0/aid-repository/projects/'+$stateParams.id+'/candidates/status',
                            data: {persons: [{'id':person_id}], 'status': status , 'reason': params.reason}
                        }).then(function (response) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            $rootScope.candidates[idx].status = status;
                            $rootScope.candidates[idx].status_name = response.data.status_name;
                            $modalInstance.close();
                        }, function(error) {
                            $rootScope.progressbar_complete();
                            $modalInstance.close();
                            $rootScope.toastrMessages('error',error.data.msg);
                        });

                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                }});

        }else{
            $rootScope.clearToastr();
            $rootScope.progressbar_start();
            $http({
                method: 'PUT',
                url: '/api/v1.0/aid-repository/projects/'+$stateParams.id+'/candidates/status',
                data: {persons: [{'id':person_id}], status: status, reason: null}
            }).then(function (response) {
                $rootScope.progressbar_complete();
                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                $rootScope.candidates[idx].status = status;
                $rootScope.candidates[idx].status_name = response.data.status_name;
            });
        }

    };

    $scope.SearchCandidates = function (selected,data,action) {
      
        $rootScope.clearToastr();
        
        var params = angular.copy(data);
            params.persons=[];

        if(selected == true || selected == 'true' ){
           var persons=[];
            angular.forEach($rootScope.candidates, function(v, k){

                if(v.checked){
                    persons.push(v.person_id);
                }

            });
            
            if(persons.length==0){
                $rootScope.toastrMessages('error',$filter('translate')('You have not select people to export'));
                return ;
            }
            params.persons=persons;
        }

        var orgs=[];
        angular.forEach(params.organization_id, function(v, k) {
            orgs.push(v.id);
        });

        params.organization_id=orgs;
        params.action=action;
        params.type=$scope.type;
        params.src = 'org';
        params.nomination_id = $stateParams.nomination_id;
        params.project_id = $stateParams.id;
            
        if(action == 'children' || action == 'wives'){
            
            $rootScope.progressbar_start();
            organizationProject.related(params, function(response) {
                $rootScope.progressbar_complete();
                if(response.status == false){
                        $rootScope.toastrMessages('error',$filter('translate')('There are no names matching the search settings you entered'));
                }else{

                    if(response.status == true){
                        var file_type='zip';
                       $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                       window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                    }else{
                       $rootScope.toastrMessages('error',$filter('translate')('no records to download'));
                    }
               }

            });

        }
        else if(action == 'excel'){

            $rootScope.progressbar_start();
                organizationProject.filter(params, function(response) {
                    $rootScope.progressbar_complete();
                    if(response.status == false){
                        $rootScope.toastrMessages('error',$filter('translate')('There are no names matching the search settings you entered'));
                    }else{

                        if(response.status == true){
                            var file_type='xlsx';
                            if(action == 'ExportToWord' || action == 'export') {
                                file_type='zip';
                            }
                            $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;

                        }else{
                            $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                        }
                    }
            });


        }
        else{
            params.page=$rootScope.CurrentPage;
            LoadTheCandidates(params);
        }
    };

    $scope.statistic_ = function() {
        $rootScope.clearToastr();
        angular.element('.btn').addClass("disabled");

        $rootScope.progressbar_start();
        nominationRules.statistic({project_id: $stateParams.id}, function(response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if (response.status===false || response.status==='false') {
                $rootScope.toastrMessages('error',response.msg);
            }else{
                if(!angular.isUndefined(response.download_token)) {
                    $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }
            }

        });

    };
});