AidRepositoryModule = angular.module('AidRepositoryModule');

AidRepositoryModule.controller('RatioController', function ($stateParams,$scope, $filter, $state,Org,$rootScope) {
    $rootScope.clearToastr();
    $state.current.data.pageTitle = $filter('translate')('Share_Settings');
    $scope.messages = {};
    $scope.organization={descendants_ratio:[]};
    $scope.total_container_share=0;
    $rootScope.clearToastr();
    $scope.id = 1 ;
    if($stateParams.id){
        $scope.id = $stateParams.id ;
    }
    $rootScope.progressbar_start();
    Org.containerShare({id:$scope.id}, function(response) {
        $rootScope.progressbar_complete();
        $scope.organization = response;
        $scope.total_container_share = response.descendants_ratio_total;
        var total_container_share = 0;
        angular.forEach($scope.organization.descendants_ratio, function(v, k) {
            v.container_share = parseFloat(v.container_share);
            total_container_share += v.container_share;
        });
    });

    $scope.save = function() {
        $rootScope.clearToastr();

        var  total_container_share = 0;
        angular.forEach($scope.organization.descendants_ratio, function(v, k) {
            if(angular.isUndefined(v.container_share)) {
                v.container_share=0;
            }
            if(v.container_share == " " || v.container_share == "") {
                v.container_share=0;
            }
            total_container_share += parseFloat(v.container_share);
        });

        if(parseInt(total_container_share)>100){
            $rootScope.toastrMessages('error',$filter('translate')('Total ratios exceeded 100'));


        }else{
            $rootScope.progressbar_start();
            $scope.total_container_share =  parseFloat(total_container_share);
            Org.updateRepositoryShare({organizations: $scope.organization.descendants_ratio}, function(response) {
                $rootScope.progressbar_complete();
                if(response.status){
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                }else{
                    $rootScope.toastrMessages('error',response.msg);
                }
            });
        }


    };
      $scope.adjust = function(index,value) {
          $rootScope.clearToastr();

          if(angular.isUndefined(value) || value == " " || value == "") {
              value=0;
          }

          var  total_container_share = 0;
          angular.forEach($scope.organization.descendants_ratio, function(v, k) {
              if(angular.isUndefined(v.container_share)) {
                  v.container_share=0;
              }
              if(v.container_share == " " || v.container_share == "") {
                  v.container_share=0;
              }
              total_container_share += parseFloat(v.container_share);
          });

          if(parseFloat(total_container_share)>100){
              $scope.total_container_share =  parseFloat(total_container_share - parseFloat(value));
              var remain =  1 - parseFloat(total_container_share - parseFloat(value)) ;
              $scope.organization.descendants_ratio[index].container_share =0;
              //     // $scope.organizations[index].container_share =remain;
              $rootScope.toastrMessages('warning',$filter('translate')('Total ratio exceeded 1, ratio is zeroed for')+  $scope.organization.descendants_ratio[index].name +
                                           $filter('translate')(', So you can add the correct ratio, where the remaining ratio:') + remain);


          }else{
              $scope.total_container_share =  parseFloat(total_container_share);
          }
    };
});