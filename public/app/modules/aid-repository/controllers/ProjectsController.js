AidRepositoryModule = angular.module('AidRepositoryModule');

AidRepositoryModule.controller('RepositoryProjectsController', function ($rootScope, $scope, $stateParams,Entity, Project, Repository, $http,Org,vouchers_categories,$filter,$state,$uibModal,FileUploader, OAuthToken,$ngBootbox,org_sms_service) {

    $rootScope.clearToastr();
    $state.current.data.pageTitle = $filter('translate')('Projects');

    // $scope.$watchCollection('projects', function (newCollection, oldCollection, scope) {
    //     $scope.updateTotals();
    // });


    $scope.updateTotals_ = function () {

        var repository_ = angular.copy($rootScope.repository);
        repository_.total_ratio = 0 ;
        repository_.total_quantity = 0 ;
        repository_.total_amount = 0 ;

        angular.forEach($rootScope.repository.organizations, function(org, o) {
            org.total_ratio = 0 ;
            org.total_quantity = 0 ;
            org.total_amount = 0 ;

            angular.forEach(repository_.projects, function(project, p) {
                org.total_ratio += project.organizations[o].ratio ;
                org.total_quantity += project.organizations[o].quantity ;
                org.total_amount += project.organizations[o].amount ;
            });
            org.total_ratio = floatRound(org.total_ratio /$rootScope.repository.projects.length,0) ;

        });


        angular.forEach(repository_.projects, function(project, p) {
            // total_ratio += floatRound(project.ratio,2) ;
            project.total_ratio = 0 ;
            project.total_quantity = 0 ;
            project.total_amount = 0 ;

            angular.forEach(project.organizations, function(item, key) {
                project.total_ratio += floatRound(item.ratio,2) ;
                project.total_quantity += floatRound(item.quantity,2) ;
                project.total_amount += floatRound(item.amount,2) ;

            });

            repository_.total_ratio += project.total_ratio ;
            repository_.total_quantity += project.total_quantity ;
            repository_.total_amount += project.total_amount ;
        });

        $rootScope.repository = repository_;
    };

    init = function() {
        // For New Peoject
        $rootScope.categories = vouchers_categories.List();
        Entity.get({entity:'entities',c:'currencies'},function (response) {
            $rootScope.currency = response.currencies;
        });
        Org.list({type: "organizations", direct: 1}, function(response) {
            $rootScope.Orgs = response;
        });
        Org.list({type: "sponsors"}, function(response) {
            $rootScope.Sponsors = response;
        });
        Repository.projectsOptions({id: $stateParams.id}, function(response) {
            $rootScope.Orgs_ = response.exe_org;



        });
        Repository.projects({id: $stateParams.id}, function(response) {
            var temp = response;
            angular.forEach(temp.projects, function(v, k) {
                v.status=parseInt(v.status);
            });
            $rootScope.repository = temp;
            // $scope.updateTotals();


        });
    };

    $scope.updateProjectStatus = function (id,status,index) {
        $rootScope.clearToastr();
        $rootScope.progressbar_start();
        Project.updateProjectStatus({id:id,status:status}, function(response) {
            $rootScope.progressbar_complete();
            if(response.status==true){
                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                $rootScope.repository.projects[index].status=status;
            }else{
                $rootScope.toastrMessages('error',response.msg);
           //     $rootScope.toastrMessages('error',$filter('translate')('action failed'));
            }

            if(response.download_token){
                var file_type='xlsx';
                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
            }
        });
    };

    var floatRound = function(number, precision) {

        if(number != 0 || number != '0'){
            var factor = Math.pow(10, precision);
            var tempNumber = number * factor;
            var roundedTempNumber = Math.round(tempNumber);
            return roundedTempNumber / factor;
        }
        return 0;

    };

    init();

    // add or update project
    $scope.project_=function(){
        $rootScope.clearToastr();
        $scope.project={organization_quantity:0,custom_quantity:0};
        $uibModal.open({
            templateUrl: 'project_.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance, $log) {
                $scope.project={organization_quantity:0,custom_quantity:0};
                $scope.msg1={};
                $scope.confirm = function (params) {
                    $rootScope.progressbar_start();
                    $rootScope.clearToastr();
                    var data = angular.copy(params);
                    data.repository_id = $stateParams.id;

                    if( !angular.isUndefined(data.date_from)) {
                        data.date_from=$filter('date')(data.date_from, 'dd-MM-yyyy');
                    }
                    if( !angular.isUndefined(params.date_to)) {
                        data.date_to=$filter('date')(data.date_to, 'dd-MM-yyyy');
                    }

                    Project.save(data, function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='error' || response.status=='failed')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else if(response.status=='failed_valid')
                        {
                            $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                            $scope.status1 =response.status;
                            $scope.msg1 =response.msg;
                        }
                        else{
                            if(response.status==true)
                            {
                                init();
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            }else{
                                $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                            }
                            $modalInstance.close();
                        }
                    });

                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };


                $scope.dateOptions = { formatYear: 'yy',  startingDay: 0 };
                $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                $scope.today = function() { $scope.dt = new Date(); };
                $scope.clear = function() { $scope.dt = null; };

                $scope.format = $scope.formats[0];
                $scope.today();

                $scope.popup99 = {opened: false};
                $scope.popup88 = {opened: false};
                $scope.popup999 = {opened: false};
                $scope.popup888 = {opened: false};

                $scope.popup880  = {opened: false};
                $scope.popup90  = {opened: false};
                $scope.popup880  = {opened: false};
                $scope.popup900  = {opened: false};

                $scope.open880 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup880.opened = true;};
                $scope.open999 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup999.opened = true;};
                $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
                $scope.open888 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup888.opened = true;};

                $scope.open88 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup88.opened = true; };
                $scope.open90 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup90.opened = true;};
                $scope.open880 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup880.opened = true; };
                $scope.open900 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup900.opened = true;};

            }});
    };

    // save changes on page
    $scope.saveProject = function(project) {
        $rootScope.clearToastr();
        if(project.total_quantity != project.quantity){
        // if(project.total_ratio != 100){
            $rootScope.toastrMessages('error',$filter('translate')('Total ratios must be 100 in order to save project'));
        }else {
            var p = angular.copy(project);
            $rootScope.progressbar_start();
            Project.saveOrg(p, function (response) {
                $rootScope.progressbar_complete();
                if (response.success) {
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }
            });
        }
    };

    var remap = function(){
        var repository_ = angular.copy($rootScope.repository);
        repository_.total_ratio = 0 ;
        repository_.total_quantity = 0 ;
        repository_.total_amount = 0 ;
        angular.forEach(repository_.projects, function(project, p) {
            var p_total_ratio = 0 ;
            var p_total_quantity = 0 ;
            var p_total_amount = 0 ;

            angular.forEach(project.organizations, function(item, key) {
                p_total_ratio += floatRound(item.ratio,1) ;
                p_total_quantity += floatRound(item.quantity,2) ;
                p_total_amount += floatRound(item.amount,2) ;
            });

            project.total_ratio = floatRound(p_total_ratio,2) ;
            project.total_quantity = p_total_quantity ;
            project.total_amount = p_total_amount ;

            repository_.total_ratio += project.total_ratio ;
            repository_.total_quantity += project.total_quantity ;
            repository_.total_amount += project.total_amount ;
        });
        repository_.total_ratio = floatRound(repository_.total_ratio /repository_.projects.length,0) ;
        angular.forEach(repository_.organizations, function(org, o) {
            var org_total_ratio = 0 ;
            var org_total_quantity = 0 ;
            var org_total_amount = 0 ;

            angular.forEach(repository_.projects, function(project, p) {
                org_total_ratio += floatRound(project.organizations[o].ratio,1) ;
                org_total_quantity += floatRound(project.organizations[o].quantity,0) ;
                org_total_amount += floatRound(project.organizations[o].amount,0) ;
            });
            org.total_ratio = floatRound(org_total_ratio / repository_.projects.length ,1) ;
            org.total_quantity = org_total_quantity ;
            org.total_amount = org_total_quantity ;
        });
        $rootScope.repository = repository_;
    };

    $scope.distribute = function(project) {
        $rootScope.clearToastr();

        if (project.status ==3 ||project.status ==4) {
            $rootScope.toastrMessages('error',$filter('translate')('No operation can be do on a project under construction or closed'));
        }
        else{
            var total_ratio = 0 ;
            var total_quantity = 0 ;
            var total_amount = 0;

            var quantity = project.quantity - project.organization_quantity ;
            var amount = (project.amount * quantity) * project.exchange_rate;

            if(project.has_subtract_ratio){
                angular.forEach(project.organizations, function(item, key) {
                    if( !( project.custom_organization == 'null' || project.custom_organization == null || 
                           project.custom_organization == '' || project.custom_organization == ' ')) {
                           item.ratio = floatRound((item.container_share - (item.container_share * project.changes_)),1);
                           item.quantity = floatRound(quantity * ((item.container_share - (item.container_share * project.changes_)) / 100),0);
                          item.amount = floatRound(amount * ((item.container_share - (item.container_share * project.changes_)) / 100),0);
                   }else{
                    if(item.organization_id == project.custom_organization){
                        item.ratio = floatRound((project.custom_ratio),1);
                        item.quantity = floatRound((quantity * (project.custom_ratio / 100)),0);
                        item.amount = floatRound((amount * (project.custom_ratio / 100)),0);
                    }else{
                        item.ratio = floatRound((item.container_share - (item.container_share * project.changes_)),1);
                        item.quantity = floatRound(quantity * ((item.container_share - (item.container_share * project.changes_)) / 100),0);
                        item.amount = floatRound(amount * ((item.container_share - (item.container_share * project.changes_)) / 100),0);
                    }                       
                   }

                    total_ratio += floatRound(item.ratio,1) ;
                    total_quantity += floatRound(item.quantity,0) ;
                    total_amount += floatRound(item.amount,0) ;
                });
            }
            else{
                angular.forEach(project.organizations, function(item, key) {
                        item.ratio = floatRound(item.container_share,1);
                        item.quantity = floatRound((quantity * (item.container_share / 100)),0);
                        item.amount = floatRound((amount * (item.container_share / 100)),0);

                    total_ratio += floatRound(item.ratio,1) ;
                    total_quantity += floatRound(item.quantity,0);
                    total_amount += floatRound(item.amount,0);
                });
            }
            project.total_ratio = floatRound(total_ratio,1) ;
            project.total_quantity = floatRound(total_quantity,0);
            project.total_amount = floatRound(total_amount,0);
            project.adjust=0;
            remap();
        }

    };

    $scope.adjust = function(index,project) {
        $rootScope.clearToastr();
        if (project.status == 3 || project.status == 4) {
            $rootScope.toastrMessages('error',$filter('translate')('No operation can be do on a project under construction or closed'));
        }

        else {
            if(project.adjust == 1){
                $rootScope.toastrMessages('error',$filter('translate')('The project cannot be adapted again'));
            }else{
                var quantity = project.quantity - project.organization_quantity;
                var amount = (project.amount * quantity) * project.exchange_rate;
                var total_ratio = 0;
                var total_quantity = 0;
                var total_amount = 0;

                angular.forEach(project.organizations, function(item, key) {

                    var total = floatRound($rootScope.repository.organizations[key].total_ratio,1);

                    if(total < item.container_share) {
                        var diff = floatRound(floatRound(item.container_share ,1) - floatRound($rootScope.repository.organizations[key].total_ratio ,1),1);
                        var new_ = floatRound(item.ratio ,1)  + diff;
                        item.ratio = floatRound(new_,1);
                        $rootScope.repository.organizations[key].total_ratio =floatRound( ($rootScope.repository.organizations[key].total_ratio +  diff) , 3);
                        $rootScope.repository.organizations[key].total_quantity =floatRound( ($rootScope.repository.organizations[key].total_quantity +  (diff * quantity)) , 3);
                        $rootScope.repository.organizations[key].total_amount =floatRound( ($rootScope.repository.organizations[key].total_amount +  (diff * quantity)) , 3);
                        item.quantity = floatRound( quantity * (item.ratio / 100) , 0);
                        item.amount = floatRound( amount *(item.ratio / 100) , 0);

                    }

                    if(total > item.container_share){
                        var diff = (floatRound((($rootScope.repository.organizations[key].total_ratio) - item.container_share),3));
                        var new_ = floatRound(item.ratio ,3)  - floatRound( diff , 1);

                        var val = 0;
                        var diff_0 = 0;
                        if(new_ >= 0){
                            val = new_;
                            diff_0 = diff;
                        }

                        item.ratio = floatRound(val,1);
                        $rootScope.repository.organizations[key].total_ratio =floatRound( ($rootScope.repository.organizations[key].total_ratio - diff_0) , 3);
                        item.quantity = floatRound( quantity * (item.ratio / 100) , 0);
                        item.amount = floatRound( amount *(item.ratio / 100) , 0);


                    }
                    total_ratio += floatRound(item.ratio,1);
                    total_quantity += floatRound(item.quantity,2);
                    total_amount += floatRound(item.amount,2);

                });

                project.total_ratio = floatRound(total_ratio,1) ;
                project.total_quantity = floatRound(total_quantity,2) ;
                project.total_amount = floatRound(total_amount,2) ;
                project.adjust=1;
                remap();
            }


        }
    };

    $scope.updateTotals = function () {
        $rootScope.clearToastr();
        var repository_ = angular.copy($rootScope.repository);
        repository_.total_ratio = 0 ;
        repository_.total_quantity = 0 ;
        repository_.total_amount = 0 ;
        angular.forEach(repository_.projects, function(project, p) {
            var p_total_ratio = 0 ;
            var p_total_quantity = 0 ;
            var quantity = project.quantity - project.organization_quantity;

            var p_total_amount = 0 ;

            angular.forEach(project.organizations, function(item, key) {
                p_total_quantity += floatRound(item.quantity,2) ;
            });

            angular.forEach(project.organizations, function(item, key) {
                var quantity_ = (item.quantity / quantity) * 100 ;
                item.quantity = floatRound(item.quantity,2) ;
                item.ratio = floatRound(quantity_,  1) ;
                item.amount = item.quantity * project.amount ;

                p_total_ratio += floatRound(item.ratio,1) ;
                p_total_amount += floatRound(item.amount,2) ;
            });

            project.total_ratio = floatRound(p_total_ratio,1) ;
            project.total_quantity = p_total_quantity ;
            project.total_amount = p_total_amount ;

            repository_.total_ratio += project.total_ratio ;
            repository_.total_quantity += project.total_quantity ;
            repository_.total_amount += project.total_amount ;
        });

        repository_.total_ratio = floatRound(repository_.total_ratio /repository_.projects.length,0) ;

        angular.forEach(repository_.organizations, function(org, o) {
            var org_total_ratio = 0 ;
            var org_total_quantity = 0 ;
            var org_total_amount = 0 ;

            angular.forEach(repository_.projects, function(project, p) {
                org_total_ratio += floatRound(project.organizations[o].ratio,1) ;
                org_total_quantity += floatRound(project.organizations[o].quantity,0) ;
                org_total_amount += floatRound(project.organizations[o].amount,0) ;
            });
            org.total_ratio = floatRound(org_total_ratio / repository_.projects.length ,1) ;
            org.total_quantity = org_total_quantity ;
            org.total_amount = org_total_quantity ;
        });
        $rootScope.repository = repository_;
    };

    $scope.statistic = function(id) {
        $rootScope.clearToastr();
        angular.element('.btn').addClass("disabled");

        $rootScope.progressbar_start();
        Repository.statistic({project_id: id}, function(response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if (response.status===false || response.status==='false') {
                $rootScope.toastrMessages('error',response.msg);
            }else{
                if(!angular.isUndefined(response.download_token)) {
                    $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }
            }

        });

    };

    $scope.MainStatistic = function() {
        angular.element('.btn').addClass("disabled");

        $rootScope.progressbar_start();
        Repository.MainStatistic({repository_id: $stateParams.id}, function(response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if (response.status===false || response.status==='false') {
                $rootScope.toastrMessages('error',response.msg);
            }else{
                if(!angular.isUndefined(response.download_token)) {
                    $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }
            }

        });

    };

    // check candidates on project by id
    $scope.check = function(id){
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: 'checkPerson_.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'md',
            controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {
                $scope.upload=false;

                $scope.fileUpload=function(){
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.upload=true;
                };

                $scope.download=function(){
                    angular.element('.btn').addClass("disabled");

                    $rootScope.clearToastr();
                    $rootScope.progressbar_start();
                    $http({
                        url:'/api/v1.0/common/reports/template',
                        method: "GET"
                    }).then(function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token+'&template=true';
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                };

                var Uploader = $scope.uploader = new FileUploader({
                    url:'/api/v1.0/aid-repository/projects/check',
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });

                Uploader.onBeforeUploadItem = function(item) {
                    $rootScope.clearToastr();
                    item.formData = [{source: 'file',project_id:id}];
                };

                $scope.confirm = function () {
                    $rootScope.progressbar_start();
                    angular.element('.btn').addClass("disabled");
                    $rootScope.clearToastr();
                    $scope.spinner=false;
                    $scope.import=false;
                    angular.element('.btn').addClass("disabled");

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(response.status=='success')
                        {
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $scope.spinner=false;
                            $scope.import=false;
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        }else{
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $rootScope.toastrMessages('error',response.msg);
                           $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                        }
                        if(response.download_token){
                            var file_type='xlsx';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }
                        $modalInstance.close();

                    };
                    Uploader.uploadAll();
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.remove=function(){
                    angular.element("input[type='file']").val(null);
                    $scope.uploader.clearQueue();
                    angular.forEach(
                        angular.element("input[type='file']"),
                        function(inputElem) {
                            angular.element(inputElem).val(null);
                        });
                };

            }});
    };

    // check candidates on repository by $stateParams.id
    $scope.check_on_repository = function(){
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: 'checkPerson_.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'md',
            controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {
                $scope.upload=false;

                $scope.fileUpload=function(){
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.upload=true;
                };

                $scope.download=function(){
                    angular.element('.btn').addClass("disabled");

                    $rootScope.progressbar_start();
                    $rootScope.clearToastr();
                    $http({
                        url:'/api/v1.0/common/reports/template',
                        method: "GET"
                    }).then(function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token+'&template=true';
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                };

                var Uploader = $scope.uploader = new FileUploader({
                    url:'/api/v1.0/aid-repository/repositories/check',
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });

                Uploader.onBeforeUploadItem = function(item) {
                    $rootScope.clearToastr();
                    item.formData = [{source: 'file',repository_id:$stateParams.id}];
                };

                $scope.confirm = function () {
                    $rootScope.progressbar_start();
                    angular.element('.btn').addClass("disabled");
                    $rootScope.clearToastr();
                    $scope.spinner=false;
                    $scope.import=false;
                    angular.element('.btn').addClass("disabled");

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(response.status=='success')
                        {
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $scope.spinner=false;
                            $scope.import=false;
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        }else{
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $rootScope.toastrMessages('error',response.msg);
                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                        }
                        if(response.download_token){
                            var file_type='xlsx';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }
                        $modalInstance.close();

                    };
                    Uploader.uploadAll();
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.remove=function(){
                    angular.element("input[type='file']").val(null);
                    $scope.uploader.clearQueue();
                    angular.forEach(
                        angular.element("input[type='file']"),
                        function(inputElem) {
                            angular.element(inputElem).val(null);
                        });
                };

            }});
    };

    // delete project by id
    $scope.delete = function (project) {
        $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
            .then(function() {
                $rootScope.progressbar_start();
                Project.delete({id: project.id}, function () {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                    init();
                });
            });
    };

    // edit project
    $scope.edit=function(project){
        $rootScope.clearToastr();
         $uibModal.open({
            templateUrl: 'edit_project.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance, $log) {
                $scope.project = angular.copy(project);
                $scope.project.organization_quantity= parseInt($scope.project.organization_quantity );
                $scope.project.exchange_rate = parseFloat($scope.project.exchange_rate, 10);
                $scope.project.custom_quantity = parseInt($scope.project.custom_quantity );
                $scope.project.allow_day = parseInt($scope.project.allow_day );
                if( !( $scope.project.date_from == null || $scope.project.date_from == 'null')) {
                    $scope.project.date_from=new Date($scope.project.date_from);
                }else
                    $scope.project.date_from='';

                if( !( $scope.project.date_to == null || $scope.project.date_to == 'null')) {
                    $scope.project.date_to=new Date($scope.project.date_to);
                }else
                    $scope.project.date_to='';

                $scope.msg1={};
                $scope.confirm = function (params) {
                    $rootScope.progressbar_start();
                    $rootScope.clearToastr();
                    var data = angular.copy(params);
                    data.repository_id = $stateParams.id;
                    if( !angular.isUndefined(data.date_from)) {
                        data.date_from=$filter('date')(data.date_from, 'dd-MM-yyyy');
                    }
                    if( !angular.isUndefined(params.date_to)) {
                        data.date_to=$filter('date')(data.date_to, 'dd-MM-yyyy');
                    }

                    Project.update(data, function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='error' || response.status=='failed')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else if(response.status=='failed_valid')
                        {
                            $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                            $scope.status1 =response.status;
                            $scope.msg1 =response.msg;
                        }
                        else{
                            if(response.status==true)
                            {
                                init();
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            }else{
                                $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                            }
                            $modalInstance.close();
                        }
                    });

                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };


                $scope.dateOptions = { formatYear: 'yy',  startingDay: 0 };
                $scope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
                $scope.today = function() { $scope.dt = new Date(); };
                $scope.clear = function() { $scope.dt = null; };

                $scope.format = $scope.formats[0];
                $scope.today();

                $scope.popup99 = {opened: false};
                $scope.popup88 = {opened: false};
                $scope.popup999 = {opened: false};
                $scope.popup888 = {opened: false};

                $scope.popup880  = {opened: false};
                $scope.popup90  = {opened: false};
                $scope.popup880  = {opened: false};
                $scope.popup900  = {opened: false};

                $scope.open880 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup880.opened = true;};
                $scope.open999 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup999.opened = true;};
                $scope.open99 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup99.opened = true;};
                $scope.open888 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup888.opened = true;};

                $scope.open88 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup88.opened = true; };
                $scope.open90 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup90.opened = true;};
                $scope.open880 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup880.opened = true; };
                $scope.open900 = function($event) { $event.preventDefault(); $event.stopPropagation(); $scope.popup900.opened = true;};

            }});
    };

    $scope.show=function(project){
        $rootScope.clearToastr();
         $uibModal.open({
            templateUrl: 'show_project.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance, $log) {
                $scope.project = angular.copy(project);
                $scope.project.organization_quantity = parseInt($scope.project.organization_quantity );
                $scope.project.exchange_rate = parseFloat($scope.project.exchange_rate, 10);
                $scope.project.custom_quantity = parseInt($scope.project.custom_quantity );
                $scope.project.allow_day = parseInt($scope.project.allow_day );
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }});
    };

    $scope.selectedAll = function(value){
        if(value){
            $scope.selectAll = true;
        }
        else{
            $scope.selectAll =false;
        }
        angular.forEach($rootScope.repository.projects, function(val,key) {
            val.caseCheck=$scope.selectAll;
        });
    };

    $scope.sendSms =  function(type,project_id,size) {

        if(type == 'manual'){
            $rootScope.clearToastr();

            var receipt = [];
            var pass = true;
            if(project_id ==0){
                angular.forEach($rootScope.repository.projects, function (v, k) {
                    if (v.caseCheck) {
                        receipt.push(v.id);
                    }
                });
                if(receipt.length==0){
                    pass = false;
                    $rootScope.toastrMessages('error',$filter('translate')('No project was selected to send the message'));
                    return;
                }
            }else{
                receipt.push(project_id);
            }

            if(pass){
                org_sms_service.getList(function (response) {
                    $rootScope.Providers = response.Service;
                });
                $uibModal.open({
                    templateUrl: '/app/modules/sms/views/modal/send_sms_in_projects.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance) {
                        $scope.sms={source: 'project' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:"",cont_type:"",org_pers:""};


                        $scope.confirm = function (data) {
                            $rootScope.clearToastr();
                            var params = angular.copy(data);
                            params.target='project';
                            params.receipt=receipt;

                            $rootScope.progressbar_start();
                            org_sms_service.sendSmsToTarget(params,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed') {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status== 'success'){
                                    if(response.download_token){
                                        var file_type='xlsx';
                                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                    }

                                    $modalInstance.close();
                                    $rootScope.toastrMessages('success',response.msg);
                                }
                            }, function(error) {
                                $modalInstance.close();
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                            });
                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    },
                    resolve: {
                    }
                });

            }


        }else{
            org_sms_service.getList(function (response) {
                $rootScope.Providers = response.Service;
            });
            $uibModal.open({
                templateUrl: '/app/modules/sms/views/modal/sent_sms_from_xlx.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;
                    $scope.sms={source: 'file' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:""};

                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };

                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };
                    $scope.download=function(){
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-to-send-sms-template&template=true';
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: 'api/v1.0/sms/sent_sms/excelWithMobiles',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });


                    $scope.confirm = function (item) {
                        $scope.spinner=false;
                        $scope.import=false;
                        $rootScope.progressbar_start();
                        Uploader.onBeforeUploadItem = function(item) {

                            if($scope.sms.same_msg == 0){
                                $scope.sms.sms_messege = '';
                            }
                            item.formData = [$scope.sms];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);

                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            else if(response.status=='success')
                            {
                                if(response.download_token){
                                    var file_type='xlsx';
                                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                } 
                                $scope.spinner=false;
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $scope.import=false;
                                $rootScope.toastrMessages('success',response.msg);
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                //$modalInstance.close();
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});

        }

    };

});

AidRepositoryModule.controller('UrgentRepositoryProjectsController', function ($rootScope, $scope, $stateParams,Entity, Project, Repository, $http,Org,vouchers_categories,$filter,$state,$uibModal,FileUploader, OAuthToken,$ngBootbox,org_sms_service) {

    $rootScope.clearToastr();
    $state.current.data.pageTitle = $filter('translate')('Projects');


    init = function() {
        // For New Peoject
        $rootScope.categories = vouchers_categories.List();
        Entity.get({entity:'entities',c:'currencies'},function (response) {
            $rootScope.currency = response.currencies;
        });
        Org.list({type: "organizations", direct: 1}, function(response) {
            $rootScope.Orgs = response;
        });
        Org.list({type: "sponsors"}, function(response) {
            $rootScope.Sponsors = response;
        });
        Repository.projectsOptions({id: $stateParams.id}, function(response) {
            $rootScope.Orgs_ = response.exe_org;
        });
        $rootScope.progressbar_start();
        Repository.projects({id: $stateParams.id}, function(response) {
            $rootScope.progressbar_complete();
            var temp = response;
            angular.forEach(temp.projects, function(v, k) {
                v.status=parseInt(v.status);
            });
            $rootScope.repository = temp;
        });
    };

    $scope.updateProjectStatus = function (id,status,index) {
        $rootScope.clearToastr();
        $rootScope.progressbar_start();
        Project.updateProjectStatus({id:id,status:status}, function(response) {
            $rootScope.progressbar_complete();
            if(response.status==true){
                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                $rootScope.repository.projects[index].status=status;
            }else{
                $rootScope.toastrMessages('error',response.msg);
                //     $rootScope.toastrMessages('error',$filter('translate')('action failed'));
            }

            if(response.download_token){
                var file_type='xlsx';
                window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
            }
        });
    };

    var floatRound = function(number, precision) {

        if(number != 0 || number != '0'){
            var factor = Math.pow(10, precision);
            var tempNumber = number * factor;
            var roundedTempNumber = Math.round(tempNumber);
            return roundedTempNumber / factor;
        }
        return 0;

    };

    init();

    // add or update project
    $scope.project_=function(){
        $rootScope.clearToastr();
        $scope.project={organization_quantity:0,custom_quantity:0};
        $uibModal.open({
            templateUrl: 'project_.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance, $log) {
                $scope.project={organization_quantity:0,custom_quantity:0};
                $scope.msg1={};
                $scope.confirm = function (params) {
                    $rootScope.clearToastr();
                    var data = angular.copy(params);
                    data.repository_id = $stateParams.id;
                    $rootScope.progressbar_start();
                    Project.save(data, function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='error' || response.status=='failed')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else if(response.status=='failed_valid')
                        {
                            $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                            $scope.status1 =response.status;
                            $scope.msg1 =response.msg;
                        }
                        else{
                            if(response.status==true)
                            {
                                init();
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            }else{
                                $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                            }
                            $modalInstance.close();
                        }
                    });

                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }});
    };

    // add candidates on project by id
    $scope.upload_candidates = function(id){

        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: '/app/modules/aid-repository/views/repositories/urgent-repositories/upload_candidates.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'md',
            controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {
                $scope.upload=false;

                $scope.fileUpload=function(){
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.upload=true;
                };

                $scope.download=function(){
                    window.location = '/api/v1.0/common/files/xlsx/export?download_token=upload_candidates&template=true';
                };

                var Uploader = $scope.uploader = new FileUploader({
                    url:'/api/v1.0/aid-repository/projects/upload_candidates',
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });

                Uploader.onBeforeUploadItem = function(item) {
                    $rootScope.clearToastr();
                    item.formData = [{source: 'file',project_id:id}];
                };

                $scope.confirm = function () {
                    angular.element('.btn').addClass("disabled");
                    $rootScope.clearToastr();
                    $scope.spinner=false;
                    $scope.import=false;
                    angular.element('.btn').addClass("disabled");

                    $rootScope.progressbar_start();
                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(response.status=='success')
                        {
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $scope.spinner=false;
                            $scope.import=false;
                            $rootScope.toastrMessages('success',response.msg);
                        }else{
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $rootScope.toastrMessages('error',response.msg);
                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                        }
                        if(response.download_token){
                            var file_type='xlsx';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }
                        $modalInstance.close();

                    };
                    Uploader.uploadAll();
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.remove=function(){
                    angular.element("input[type='file']").val(null);
                    $scope.uploader.clearQueue();
                    angular.forEach(
                        angular.element("input[type='file']"),
                        function(inputElem) {
                            angular.element(inputElem).val(null);
                        });
                };

            }});
    };

    // save changes on page
    $scope.saveProject = function(project) {
        $rootScope.clearToastr();
        if(project.total_quantity != project.quantity){
            // if(project.total_ratio != 100){
            $rootScope.toastrMessages('error',$filter('translate')('Total ratios must be 100 in order to save project'));
        }else {
            var p = angular.copy(project);
            $rootScope.progressbar_start();
            Project.saveOrg(p, function (response) {
                $rootScope.progressbar_complete();
                if (response.success) {
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }
            });
        }
    };

    $scope.statistic = function(id) {
        $rootScope.clearToastr();
        angular.element('.btn').addClass("disabled");

        $rootScope.progressbar_start();
        Repository.statistic({project_id: id}, function(response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if (response.status===false || response.status==='false') {
                $rootScope.toastrMessages('error',response.msg);
            }else{
                if(!angular.isUndefined(response.download_token)) {
                    $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }
            }

        });

    };

    $scope.MainStatistic = function() {
        angular.element('.btn').addClass("disabled");

        $rootScope.progressbar_start();
        Repository.MainStatistic({repository_id: $stateParams.id}, function(response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if (response.status===false || response.status==='false') {
                $rootScope.toastrMessages('error',response.msg);
            }else{
                if(!angular.isUndefined(response.download_token)) {
                    $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }
            }

        });

    };

    // check candidates on project by id
    $scope.check = function(id){
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: 'checkPerson_.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'md',
            controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {
                $scope.upload=false;

                $scope.fileUpload=function(){
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.upload=true;
                };

                $scope.download=function(){
                    angular.element('.btn').addClass("disabled");

                    $rootScope.progressbar_start();
                    $rootScope.clearToastr();
                    $http({
                        url:'/api/v1.0/common/reports/template',
                        method: "GET"
                    }).then(function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token+'&template=true';
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                };

                var Uploader = $scope.uploader = new FileUploader({
                    url:'/api/v1.0/aid-repository/projects/check',
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });

                Uploader.onBeforeUploadItem = function(item) {
                    $rootScope.clearToastr();
                    item.formData = [{source: 'file',project_id:id}];
                };

                $scope.confirm = function () {
                    $rootScope.progressbar_start();
                    angular.element('.btn').addClass("disabled");
                    $rootScope.clearToastr();
                    $scope.spinner=false;
                    $scope.import=false;
                    angular.element('.btn').addClass("disabled");

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(response.status=='success')
                        {
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $scope.spinner=false;
                            $scope.import=false;
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        }else{
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $rootScope.toastrMessages('error',response.msg);
                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                        }
                        if(response.download_token){
                            var file_type='xlsx';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }
                        $modalInstance.close();

                    };
                    Uploader.uploadAll();
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.remove=function(){
                    angular.element("input[type='file']").val(null);
                    $scope.uploader.clearQueue();
                    angular.forEach(
                        angular.element("input[type='file']"),
                        function(inputElem) {
                            angular.element(inputElem).val(null);
                        });
                };

            }});
    };

    // check candidates on repository by $stateParams.id
    $scope.check_on_repository = function(){
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: 'checkPerson_.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'md',
            controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {
                $scope.upload=false;

                $scope.fileUpload=function(){
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.upload=true;
                };

                $scope.download=function(){
                    angular.element('.btn').addClass("disabled");

                    $rootScope.clearToastr();
                    $rootScope.progressbar_start();
                    $http({
                        url:'/api/v1.0/common/reports/template',
                        method: "GET"
                    }).then(function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token+'&template=true';
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                };

                var Uploader = $scope.uploader = new FileUploader({
                    url:'/api/v1.0/aid-repository/repositories/check',
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });

                Uploader.onBeforeUploadItem = function(item) {
                    $rootScope.clearToastr();
                    item.formData = [{source: 'file',repository_id:$stateParams.id}];
                };

                $scope.confirm = function () {
                    angular.element('.btn').addClass("disabled");
                    $rootScope.clearToastr();
                    $scope.spinner=false;
                    $scope.import=false;
                    angular.element('.btn').addClass("disabled");

                    $rootScope.progressbar_start();
                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(response.status=='success')
                        {
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $scope.spinner=false;
                            $scope.import=false;
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        }else{
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $rootScope.toastrMessages('error',response.msg);
                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                        }
                        if(response.download_token){
                            var file_type='xlsx';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }
                        $modalInstance.close();

                    };
                    Uploader.uploadAll();
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.remove=function(){
                    angular.element("input[type='file']").val(null);
                    $scope.uploader.clearQueue();
                    angular.forEach(
                        angular.element("input[type='file']"),
                        function(inputElem) {
                            angular.element(inputElem).val(null);
                        });
                };

            }});
    };

    // delete project by id
    $scope.delete = function (project) {
        $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
            .then(function() {
                $rootScope.progressbar_start();
                Project.delete({id: project.id}, function () {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                    init();
                });
            });
    };

    // edit project
    $scope.edit=function(project){
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: 'edit_project.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance, $log) {
                $scope.project = angular.copy(project);
                $scope.project.organization_quantity = parseInt($scope.project.organization_quantity );
                $scope.project.exchange_rate = parseFloat($scope.project.exchange_rate, 10);
                $scope.project.custom_quantity = parseInt($scope.project.custom_quantity );
                $scope.project.allow_day = parseInt($scope.project.allow_day );
                $scope.msg1={};
                $scope.confirm = function (params) {
                    $rootScope.clearToastr();
                    var data = angular.copy(params);
                    data.repository_id = $stateParams.id;
                    $rootScope.progressbar_start();
                    Project.update(data, function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='error' || response.status=='failed')
                        {
                            $rootScope.toastrMessages('error',response.msg);
                        }
                        else if(response.status=='failed_valid')
                        {
                            $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                            $scope.status1 =response.status;
                            $scope.msg1 =response.msg;
                        }
                        else{
                            if(response.status==true)
                            {
                                init();
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                            }else{
                                $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                            }
                            $modalInstance.close();
                        }
                    });

                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }});
    };

    $scope.show=function(project){
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: 'show_project.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance, $log) {
                $scope.project = angular.copy(project);
                $scope.project.organization_quantity = parseInt($scope.project.organization_quantity );
                $scope.project.exchange_rate = parseFloat($scope.project.exchange_rate, 10);
                $scope.project.custom_quantity = parseInt($scope.project.custom_quantity );
                $scope.project.allow_day = parseInt($scope.project.allow_day );
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }});
    };

    $scope.selectedAll = function(value){
        if(value){
            $scope.selectAll = true;
        }
        else{
            $scope.selectAll =false;

        }
        angular.forEach($rootScope.repository.projects, function(val,key)
        {
            val.caseCheck=$scope.selectAll;
        });
    };

    $scope.sendSms =  function(type,project_id,size) {

        if(type == 'manual'){
            $rootScope.clearToastr();

            var receipt = [];
            var pass = true;
            if(project_id ==0){
                angular.forEach($rootScope.repository.projects, function (v, k) {
                    if (v.caseCheck) {
                        receipt.push(v.id);
                    }
                });
                if(receipt.length==0){
                    pass = false;
                    $rootScope.toastrMessages('error',$filter('translate')('No project was selected to send the message'));
                    return;
                }
            }else{
                receipt.push(project_id);
            }

            if(pass){
                org_sms_service.getList(function (response) {
                    $rootScope.Providers = response.Service;
                });
                $uibModal.open({
                    templateUrl: '/app/modules/sms/views/modal/send_sms_in_projects.html',
                    backdrop  : 'static',
                    keyboard  : false,
                    windowClass: 'modal',
                    size: 'lg',
                    controller: function ($rootScope,$scope, $modalInstance) {
                        $scope.sms={source: 'project' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:"",cont_type:"",org_pers:""};


                        $scope.confirm = function (data) {
                            $rootScope.clearToastr();
                            var params = angular.copy(data);
                            params.target='project';
                            params.receipt=receipt;

                            $rootScope.progressbar_start();
                            org_sms_service.sendSmsToTarget(params,function (response) {
                                $rootScope.progressbar_complete();
                                if(response.status=='error' || response.status=='failed') {
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                else if(response.status== 'success'){
                                    if(response.download_token){
                                        var file_type='xlsx';
                                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                    }

                                    $modalInstance.close();
                                    $rootScope.toastrMessages('success',response.msg);
                                }
                            }, function(error) {
                                $rootScope.progressbar_complete();
                                $rootScope.toastrMessages('error',error.data.msg);
                            });
                        };

                        $scope.cancel = function () {
                            $modalInstance.dismiss('cancel');
                        };

                    },
                    resolve: {
                    }
                });

            }


        }else{
            org_sms_service.getList(function (response) {
                $rootScope.Providers = response.Service;
            });
            $uibModal.open({
                templateUrl: '/app/modules/sms/views/modal/sent_sms_from_xlx.html',
                backdrop  : 'static',
                keyboard  : false,
                windowClass: 'modal',
                size: 'lg',
                controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {

                    $scope.upload=false;
                    $scope.sms={source: 'file' ,sms_provider:"",is_code:"",sms_messege:"",same_msg:""};

                    $scope.RestUpload=function(value){
                        if(value ==2){
                            $scope.upload=false;
                        }
                    };

                    $scope.fileUpload=function(){
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.upload=true;
                    };
                    $scope.download=function(){
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token=import-to-send-sms-template&template=true';
                    };

                    var Uploader = $scope.uploader = new FileUploader({
                        url: 'api/v1.0/sms/sent_sms/excelWithMobiles',
                        removeAfterUpload: false,
                        queueLimit: 1,
                        headers: {
                            Authorization: OAuthToken.getAuthorizationHeader()
                        }
                    });


                    $scope.confirm = function (item) {
                        $scope.spinner=false;
                        $scope.import=false;
                        $rootScope.progressbar_start();
                        Uploader.onBeforeUploadItem = function(item) {

                            if($scope.sms.same_msg == 0){
                                $scope.sms.sms_messege = '';
                            }
                            item.formData = [$scope.sms];
                        };
                        Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                            $rootScope.progressbar_complete();
                            $scope.uploader.destroy();
                            $scope.uploader.queue=[];
                            $scope.uploader.clearQueue();
                            angular.element("input[type='file']").val(null);
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                            }
                            else if(response.status=='success')
                            {
                                if(response.download_token){
                                    var file_type='xlsx';
                                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                                }
                                $scope.spinner=false;
                                $scope.upload=false;
                                angular.element("input[type='file']").val(null);
                                $scope.import=false;
                                $rootScope.toastrMessages('success',response.msg);
                                $modalInstance.close();
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                                $scope.upload=false;
                                //$modalInstance.close();
                            }

                        };
                        Uploader.uploadAll();
                    };
                    $scope.cancel = function () {
                        $modalInstance.dismiss('cancel');
                    };
                    $scope.remove=function(){
                        angular.element("input[type='file']").val(null);
                        $scope.uploader.clearQueue();
                        angular.forEach(
                            angular.element("input[type='file']"),
                            function(inputElem) {
                                angular.element(inputElem).val(null);
                            });
                    };

                }});

        }

    };

});