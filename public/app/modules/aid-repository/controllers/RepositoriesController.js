AidRepositoryModule = angular.module('AidRepositoryModule');

AidRepositoryModule.controller('RepositoriesController', function ($rootScope, $scope, $filter, $state, $ngBootbox, Org,Repository ,FileUploader, OAuthToken,$uibModal,$http) {
    $state.current.data.pageTitle = $filter('translate')('Repositories');


    if( !angular.isUndefined($rootScope.UserOrgId)) {
        var AuthUser =localStorage.getItem("charity_LoggedInUser");
        $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
        $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
        if($rootScope.charity_LoggedInUser.organization != null) {
            $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
        }
        $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;
    }



    $scope.repositories=[];
    $scope.currentPage=1;
    $scope.itemsCount='50';
    $rootScope.action = 'paginate';
    $rootScope.multiple = false;
    $scope.filters = {action:'paginate' , page : $scope.currentPage , itemsCount:$scope.itemsCount , multiple:false};
    $scope.statusOptions = [
        {id: 1, label: $filter('translate')('Open')},
        {id: 2, label: $filter('translate')('Close')}
    ];

    $scope.levelOptions = [
        {id: 1, label: $filter('translate')('Organization_')},
        {id: 2, label: $filter('translate')('district')},
        {id: 3, label: $filter('translate')('city')},
        {id: 4, label: $filter('translate')('region')},
        {id: 5, label: $filter('translate')('nearlocation_')},
        {id: 5, label: $filter('translate')('square')},
        {id: 5, label: $filter('translate')('mosques')}
    ];

    Org.list({type:'organizations'},function (response) {  $scope.organizations_ = response; });


    function listRepository(params) {
        $rootScope.progressbar_start();
        Repository.filter(params, function(response) {
            $rootScope.progressbar_complete();

            if(params.action == 'paginate'){
                $rootScope.multiple = params.multiple;
                $scope.repositories = response.data;
                $scope.totalItems = response.total;
                $scope.currentPage = response.current_page;
                $scope.itemsPerPage = response.per_page;
            }else{
                if(response.status == true){
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('There is no data to export'));

                }
            }
        });
    }

    function restTable() {

        var data = angular.copy($scope.filters);
        data.action =$rootScope.action;

        if($scope.action == 'paginate'){
            data.items = [];
            data.page = $scope.currentPage;
            data.itemsCount = $scope.itemsCount;
        }

        var orgs = [];

        if( !angular.isUndefined(data.organization_id)) {
            angular.forEach(data.organization_id, function(v, k) {
                orgs.push(v.id);
            });
            data.organization_id = orgs;
            data.multiple = true;
        }else{
            data.multiple = false;
        }

        if( !angular.isUndefined(data.first)) {
            data.first=$filter('date')(data.first, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.last)) {
            data.last= $filter('date')(data.last, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(data.first_created_at)) {
            data.first_created_at=$filter('date')(data.first_created_at, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.last_created_at)) {
            data.last_created_at= $filter('date')(data.last_created_at, 'dd-MM-yyyy')
        }

        listRepository(data);
    }

    $scope.selectAll =false;
    $scope.selectedAll = function(value){
        if(value){
            $scope.selectAll = true;
        }
        else{
            $scope.selectAll =false;
        }
        angular.forEach($scope.repositories, function(val,key) {
            val.check=$scope.selectAll;
        });
    };
    
    // get repositories according search filters
    $scope.searchWithFilter = function(selected,params,action){
        $rootScope.clearToastr();
        $rootScope.action = action;
        var data = angular.copy(params);
        data.action = action;
        data.items = [];
        if(selected == true || selected == 'true' ){
            
                var items =[];
                 angular.forEach($scope.repositories, function(v, k){
                      if(v.check){
                         items.push(v.id);
                      }
                 });

                 if(items.length==0){
                     $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                     return ;
                 }
                 data.items=items;
        }
        
        var orgs = [];
        if( !angular.isUndefined(data.organization_id)) {
            angular.forEach(data.organization_id, function(v, k) {
                orgs.push(v.id);
            });
            data.organization_id = orgs;
            data.multiple = true;
        }else{
            data.multiple = false;
        }

        if( !angular.isUndefined(data.first)) {
            data.first=$filter('date')(data.first, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.last)) {
            data.last= $filter('date')(data.last, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(data.first_created_at)) {
            data.first_created_at=$filter('date')(data.first_created_at, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.last_created_at)) {
            data.last_created_at= $filter('date')(data.last_created_at, 'dd-MM-yyyy')
        }
        listRepository(data);
    };

    // get repositories according selected page and filters
    $scope.pageChanged = function() {
        restTable();
    };

    $scope.itemsCount = '50';


    $scope.itemsPerPage_ = function(itemsCount) {
        $scope.itemsCount = itemsCount;
        $scope.currentPage = 1;
        restTable();
    };

    $scope.EmptyThenSearch = function() {
        $scope.currentPage = 1;
        $scope.filters = {action:'paginate' , page : $scope.currentPage};
        listRepository($scope.filters);

    };

    $scope.statistic = function(id) {
        angular.element('.btn').addClass("disabled");

        $rootScope.progressbar_start();
        Repository.MainStatistic({repository_id: id}, function(response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if (response.status===false || response.status==='false') {
                $rootScope.toastrMessages('error',response.msg);
            }else{
                if(!angular.isUndefined(response.download_token)) {
                    $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }
            }
        });

    };

    // check candidates on repository by id
    $scope.check = function(id){
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: 'checkPerson_.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'md',
            controller: function ($rootScope,$scope, $modalInstance, $log,OAuthToken, FileUploader) {
                $scope.upload=false;

                $scope.fileUpload=function(){
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.upload=true;
                };

                $scope.download=function(){
                    angular.element('.btn').addClass("disabled");

                    $rootScope.clearToastr();
                    $rootScope.progressbar_start();
                    $http({
                        url:'/api/v1.0/common/reports/template',
                        method: "GET"
                    }).then(function (response) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.data.download_token+'&template=true';
                    }, function(error) {
                        $rootScope.progressbar_complete();
                        angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                        $rootScope.toastrMessages('error',error.data.msg);
                    });
                };

                var Uploader = $scope.uploader = new FileUploader({
                    url:'/api/v1.0/aid-repository/repositories/check',
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });

                Uploader.onBeforeUploadItem = function(item) {
                    $rootScope.clearToastr();
                    item.formData = [{source: 'file',repository_id:id}];
                };

                $scope.confirm = function () {
                    $rootScope.progressbar_start();
                    angular.element('.btn').addClass("disabled");
                    $rootScope.clearToastr();
                    $scope.spinner=false;
                    $scope.import=false;
                    angular.element('.btn').addClass("disabled");

                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(response.status=='success')
                        {
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $scope.spinner=false;
                            $scope.import=false;
                            $rootScope.toastrMessages('success',$filter('translate')('action success'));
                        }else{
                            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                            $rootScope.toastrMessages('error',response.msg);
                            $scope.upload=false;
                            angular.element("input[type='file']").val(null);
                        }
                        if(response.download_token){
                            var file_type='xlsx';
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        }
                        $modalInstance.close();

                    };
                    Uploader.uploadAll();
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.remove=function(){
                    angular.element("input[type='file']").val(null);
                    $scope.uploader.clearQueue();
                    angular.forEach(
                        angular.element("input[type='file']"),
                        function(inputElem) {
                            angular.element(inputElem).val(null);
                        });
                };

            }});
    };

    // delete repository by id
    $scope.delete = function (repository, idx) {
        $rootScope.clearToastr();
        $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
            .then(function() {
                $rootScope.progressbar_start();
                Repository.delete({id: repository.id}, function () {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                    $rootScope.action = 'paginate';
                    restTable();
                });
            });
    };

    // add or update repository
    $scope.repository_=function(action,entry){
        $rootScope.clearToastr();

        $rootScope.action=action;
        $rootScope.repository={level:"1"};

        if(action == 'edit'){
            $rootScope.repository = angular.copy(entry);
            $rootScope.repository.date=new Date($rootScope.repository.date);
        }

        $uibModal.open({
            templateUrl: 'repository.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance, $log,Repository) {

                $scope.msg1={};
                $scope.confirm = function (params) {
                    $rootScope.clearToastr();
                    var data = angular.copy(params);
                    if( !angular.isUndefined(data.date)) {
                        data.date=$filter('date')(data.date, 'dd-MM-yyyy')
                    }

                    $rootScope.progressbar_start();
                    if(data.id){
                        Repository.update(data, function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                $modalInstance.close();
                                if(response.status=='success')
                                {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                $rootScope.action = 'paginate';
                                restTable();
                            }
                        }, function (response) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',angular.isArray(response.data)? response.data.join('<br>') : JSON.stringify(response.data));
                        });
                    }else{
                        Repository.save(data, function (response) {
                            $rootScope.progressbar_complete();
                            if(response.status=='error' || response.status=='failed')
                            {
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            else if(response.status=='failed_valid')
                            {
                                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                                $scope.status1 =response.status;
                                $scope.msg1 =response.msg;
                            }
                            else{
                                $modalInstance.close();
                                if(response.status=='success')
                                {
                                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                }else{
                                    $rootScope.toastrMessages('error',response.msg);
                                }
                                $rootScope.action = 'paginate';
                                restTable();
                            }
                        }, function (response) {
                            $rootScope.progressbar_complete();
                            $rootScope.toastrMessages('error',angular.isArray(response.data)? response.data.join('<br>') : JSON.stringify(response.data));
                        });

                    }

                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.open = function($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.popup.opened = true;
                };
                $scope.popup = {opened: false};

            }});


    };
    $scope.show_repository=function(entry){
        $rootScope.clearToastr();
        $rootScope.repository = angular.copy(entry);
        $rootScope.repository.date=new Date($rootScope.repository.date);

        $uibModal.open({
            templateUrl: 'show_repository.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance, $log,Repository) {

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
            }});


    };
    $scope.urgent_repository=function(){
        $rootScope.clearToastr();
        $uibModal.open({
            templateUrl: 'urgent_repository.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance, $log,Repository) {

                $scope.repository={level:"1"};
                $scope.msg1={};

                $scope.download=function(){
                    $rootScope.clearToastr();
                    window.location = '/api/v1.0/common/files/xlsx/export?download_token=urgent-repository-candidates-template&template=true';
                };

                $scope.upload=false;
                $scope.FormData={};

                $scope.RestUpload=function(value){
                    if(value ==2){
                        $scope.upload=false;
                    }
                };

                $scope.fileupload=function(){
                    $scope.uploader.destroy();
                    $scope.uploader.queue=[];
                    $scope.upload=true;
                    if( !angular.isUndefined($scope.FormData.status)) {
                        if($scope.FormData.status != ""){
                            $scope.upload=true;
                        }
                    }
                };

                var Uploader = $scope.uploader = new FileUploader({
                    url: '/api/v1.0/aid-repository/urgent_repository',
                    removeAfterUpload: false,
                    queueLimit: 1,
                    headers: {
                        Authorization: OAuthToken.getAuthorizationHeader()
                    }
                });

                $scope.confirm = function (params) {
                    $rootScope.progressbar_start();
                    $rootScope.clearToastr();
                    var data = angular.copy(params);
                    if( !angular.isUndefined(data.date)) {
                        data.date=$filter('date')(data.date, 'dd-MM-yyyy')
                    }

                    Uploader.onBeforeUploadItem = function(i) {
                        $rootScope.clearToastr();
                        i.formData = [{status: item.status , project_id: $stateParams.id}];
                    };
                    Uploader.onCompleteItem = function(fileItem, response, status, headers) {
                        $rootScope.progressbar_complete();
                        $scope.uploader.destroy();
                        $scope.uploader.queue=[];
                        $scope.uploader.clearQueue();
                        angular.element("input[type='file']").val(null);
                        if(response.status=='failed_valid')
                        {
                            $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                            $rootScope.status1 =response.status;
                            $rootScope.msg1 =response.msg;
                        }
                        else{
                            if(response.status=='success')
                            {
                                $rootScope.toastrMessages('success',$filter('translate')('action success'));
                                LoadTheCandidates($rootScope.filter);
                            }else{
                                $rootScope.toastrMessages('error',response.msg);
                            }
                            $modalInstance.close();
                        }
                    };
                    Uploader.uploadAll();
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.open = function($event) {
                    $event.preventDefault();
                    $event.stopPropagation();
                    $scope.popup.opened = true;
                };
                $scope.popup = {opened: false};

            }});
    };


    // set pop up calender settings
    $scope.open = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup.opened = true;};
    $scope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup1.opened = true;};
    $scope.open2 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup2.opened = true;};
    $scope.open3 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup3.opened = true;};
    $scope.open4 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup4.opened = true;};
    $scope.popup = {opened: false};
    $scope.popup1 = {opened: false};
    $scope.popup2 = {opened: false};
    $scope.popup3 = {opened: false};
    $scope.popup4 = {opened: false};

    // get all repositories
    listRepository({action:'paginate' , page : $scope.currentPage});

});

AidRepositoryModule.controller('RepositoriesTrashController', function ($rootScope, $scope, $filter, $state, $ngBootbox, Org,Repository ,FileUploader, OAuthToken,$uibModal) {

    $rootScope.clearToastr();
    $state.current.data.pageTitle = $filter('translate')('Repositories');


    if( !angular.isUndefined($rootScope.UserOrgId)) {
        var AuthUser =localStorage.getItem("charity_LoggedInUser");
        $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
        $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
        if($rootScope.charity_LoggedInUser.organization != null) {
            $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
        }
        $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;
    }



    $scope.repositories=[];
    $scope.currentPage=1;
    $scope.itemsCount='50';
    $rootScope.action = 'paginate';
    $rootScope.multiple = false;
    $scope.filters = {action:'paginate' , page : $scope.currentPage,itemsCount:$scope.itemsCount , multiple:false};
    $scope.statusOptions = [
        {id: 1, label: $filter('translate')('Open')},
        {id: 2, label: $filter('translate')('Close')}
    ];

    $scope.levelOptions = [
        {id: 1, label: $filter('translate')('Organization_')},
        {id: 2, label: $filter('translate')('district')},
        {id: 3, label: $filter('translate')('city')},
        {id: 4, label: $filter('translate')('region')},
        {id: 5, label: $filter('translate')('nearlocation_')},
        {id: 5, label: $filter('translate')('square')},
        {id: 5, label: $filter('translate')('mosques')}
    ];

    Org.list({type:'organizations'},function (response) {  $scope.organizations_ = response; });

    function listTrashRepository(params) {
        $rootScope.progressbar_start();
        Repository.trash(params, function(response) {
            $rootScope.progressbar_complete();

            if(params.action == 'paginate'){
                $rootScope.multiple = params.multiple;
                $scope.repositories = response.data;
                $scope.totalItems = response.total;
                $scope.currentPage = response.current_page;
                $scope.itemsPerPage = response.per_page;
            }else{
                if(response.status == true){
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('There is no data to export'));

                }
            }
        });
    }

    function restTable() {

        var data = angular.copy($scope.filters);
        data.action =$rootScope.action;
        data.items = [];

        if($scope.action == 'paginate'){
            data.page = $scope.currentPage;
            data.itemsCount = $scope.itemsCount;
        }

        var orgs = [];

        if( !angular.isUndefined(data.organization_id)) {
            angular.forEach(data.organization_id, function(v, k) {
                orgs.push(v.id);
            });
            data.organization_id = orgs;
            data.multiple = true;
        }else{
            data.multiple = false;
        }

        if( !angular.isUndefined(data.first)) {
            data.first=$filter('date')(data.first, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.last)) {
            data.last= $filter('date')(data.last, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(data.first_created_at)) {
            data.first_created_at=$filter('date')(data.first_created_at, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.last_created_at)) {
            data.last_created_at= $filter('date')(data.last_created_at, 'dd-MM-yyyy')
        }

        listTrashRepository(data);
    }

    $scope.selectAll =false;
    $scope.selectedAll = function(value){
        if(value){
            $scope.selectAll = true;
        }
        else{
            $scope.selectAll =false;
        }
        angular.forEach($scope.repositories, function(val,key) {
            val.check=$scope.selectAll;
        });
    };
    
    // get repositories according search filters
    $scope.searchWithFilter = function(selected,params,action){
        $rootScope.clearToastr();
        $rootScope.action = action;
        var data = angular.copy(params);
        data.action = action;
        data.items = [];
        if(selected == true || selected == 'true' ){
            
                var items =[];
                 angular.forEach($scope.repositories, function(v, k){
                      if(v.check){
                         items.push(v.id);
                      }
                 });

                 if(items.length==0){
                     $rootScope.toastrMessages('error',$filter('translate')('You have not select recoreds to export'));
                     return ;
                 }
                 data.items=items;
        }
        var orgs = [];

        if( !angular.isUndefined(data.organization_id)) {
            angular.forEach(data.organization_id, function(v, k) {
                orgs.push(v.id);
            });
            data.organization_id = orgs;
            data.multiple = true;
        }else{
            data.multiple = false;
        }

        if( !angular.isUndefined(data.first)) {
            data.first=$filter('date')(data.first, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.last)) {
            data.last= $filter('date')(data.last, 'dd-MM-yyyy')
        }

        if( !angular.isUndefined(data.first_created_at)) {
            data.first_created_at=$filter('date')(data.first_created_at, 'dd-MM-yyyy')
        }
        if( !angular.isUndefined(data.last_created_at)) {
            data.last_created_at= $filter('date')(data.last_created_at, 'dd-MM-yyyy')
        }
        listTrashRepository(data);
    };

    // get repositories according selected page and filters
    $scope.pageChanged = function() {
        restTable();
    };

    $scope.itemsCount = '50';


    $scope.itemsPerPage_ = function(itemsCount) {
        $scope.itemsCount = itemsCount;
        $scope.currentPage = 1;
        restTable();
    };


    $scope.EmptyThenSearch = function() {
        $scope.currentPage = 1;
        $scope.filters = {action:'paginate' , page : $scope.currentPage};
        listTrashRepository($scope.filters);

    };

    // restore repository by id
    $scope.restore = function (repository, idx) {
        $rootScope.clearToastr();
        $ngBootbox.confirm($filter('translate')('are you want to confirm restore') )
            .then(function() {
                $rootScope.progressbar_start();
                Repository.restore({id: repository.id}, function () {
                    $rootScope.progressbar_complete();
                    $rootScope.toastrMessages('success',$filter('translate')('action success'));
                    $rootScope.action = 'paginate';
                    restTable();
                });
            });
    };

    // set pop up calender settings
    $scope.open = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup.opened = true;};
    $scope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup1.opened = true;};
    $scope.open2 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup2.opened = true;};
    $scope.open3 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup3.opened = true;};
    $scope.open4 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup4.opened = true;};
    $scope.popup = {opened: false};
    $scope.popup1 = {opened: false};
    $scope.popup2 = {opened: false};
    $scope.popup3 = {opened: false};
    $scope.popup4 = {opened: false};

    // get all repositories
    listTrashRepository({action:'paginate' , page : $scope.currentPage});

});