
AidRepositoryModule = angular.module('AidRepositoryModule');

// list all project nomination
AidRepositoryModule.controller('ProjectsNominationsListController', function ($rootScope, $scope, $http, $stateParams, Project,$filter,$state,nominationRules,$uibModal,$ngBootbox,Repository) {

    $rootScope.clearToastr();
    $state.current.data.pageTitle = $filter('translate')('Nomination');
    $scope.project_id = $stateParams.id;
    $scope.project = nominationRules.group({id: $stateParams.id});

    $scope.createForm = false;
    $scope.row = {label:'',project_id :$stateParams.id};
    $scope.tempEntry = {};
    $scope.curAction = 0;

    $scope.createOrUpdate = function (entry,$index) {
        $rootScope.clearToastr();
        if ((entry == null || entry == 'null')) {
            $scope.curAction = 0;
            $scope.createForm = $scope.createForm == false ? true: false;
            $scope.row = {label:'',project_id :$stateParams.id};
            $scope.tempEntry = {label:'',project_id :$stateParams.id};
            $scope.curIndex =  ($scope.project.rules.length + 1);
        }else{
            $scope.curAction = 1;
            $scope.row = angular.copy(entry);
            $scope.tempEntry = angular.copy(entry);
            $scope.createForm = true;
            $scope.curIndex = $index;
        }
    };
    $scope.cancel = function() {
        $scope.createForm = false;
        $scope.row = {label:'',project_id :$stateParams.id};
        $scope.curIndex =  ($scope.project.rules.length + 1);
        if($scope.curAction == 1){
            $scope.project.rules[$scope.curIndex] = $scope.tempEntry;
        }
    };
    $scope.save = function(model) {
        $rootScope.clearToastr();
        var entry = angular.copy(model);

        if( !angular.isUndefined(entry.label)) {

            if(entry.label == '' || entry.label == ' ' ){
                $rootScope.toastrMessages('error',$filter('translate')('please enter all required fields'));
                return ;
            }

            if(entry.id){
                entry.curId =entry.id;
                delete entry.id;
            }

            entry.project_id = $stateParams.id;
            $rootScope.progressbar_start();
            nominationRules.saveNomination(entry, function (response) {
                $rootScope.progressbar_complete();
                if(response.status =='true' || response.status ==true){
                    $scope.createForm = false;
                    $scope.row = {label:'',project_id :$stateParams.id};

                    if($scope.curAction == 1){
                        $scope.project.rules[$scope.curIndex] = response.entry;
                    }else{
                        $scope.project.rules.push(response.entry);
                    }

                    $ngBootbox.confirm($filter('translate')('action success') + ' ' +
                        $filter('translate')('Do you want to go to the Nominations Rules screen?'))
                        .then(function() {
                            $state.go('repository-projects-nominations',{"msg":true,"id" :$stateParams.id,'nomination_id':response.entry.id});
                        });
                }else{
                    $rootScope.toastrMessages('error',response.msg);
                }
            });

        }

    };
    $scope.delete = function (id, idx) {
        $rootScope.clearToastr();
        $ngBootbox.confirm($filter('translate')('are you want to confirm delete') )
            .then(function() {
                $rootScope.progressbar_start();
                nominationRules.deleteNomination({'id':id}, function (response) {
                    $rootScope.progressbar_complete();
                    if(response.status =='true' || response.status ==true){
                        $scope.project.rules.splice(idx, 1);
                        $rootScope.toastrMessages('success',$filter('translate')('action success'));
                    }else{
                        $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                    }
                });
            });
    };
    $scope.statistic = function() {
        $rootScope.clearToastr();
        angular.element('.btn').addClass("disabled");

        $rootScope.progressbar_start();
        Repository.statistic({project_id: $stateParams.id}, function(response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if (response.status===false || response.status==='false') {
                $rootScope.toastrMessages('error',response.msg);
            }else{
                if(!angular.isUndefined(response.download_token)) {
                    $rootScope.toastrMessages('success', $filter('translate')('downloading'));
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }
            }

        });

    };
});

// show nomination rule using nomination_id & project_id
AidRepositoryModule.controller('ShowProjectsRulesController', function ($filter,$scope, $rootScope, $stateParams, $http, Project,nominationRules) {

    $rootScope.clearToastr();
    $scope.rules = [];
    $rootScope.progressbar_start();
    nominationRules.rule({strict:1 ,project_id: $stateParams.id,nomination_id:$stateParams.nomination_id},function (response) {
        $rootScope.progressbar_complete();
        if(response.status == 'false' || response.status == false){
            $rootScope.toastrMessages('error',$filter('translate')('No nomination conditions'));
            return ;
        }
        $scope.rules = response.rules;
        $scope.nomination = response.nomination;
        $scope.project = response.nomination.project;
    });

});

// to set or rest rules and candidate
AidRepositoryModule.controller('NominationsController', function ($rootScope, $scope, $http, $stateParams, Project,$filter,$state,nominationRules) {

    // done
    $rootScope.clearToastr();
    $state.current.data.pageTitle = $filter('translate')('Nomination');
    $scope.entries=[];
    $scope.records=[];
    $rootScope.CurrentPage = 1;
    $rootScope.TotalItems = 0;
    $scope.itemsCount = 50;
    $scope.statistic_flg=false;
    $scope.saved = false;
    $scope.cAll = false;

    $scope.nomination_id = $stateParams.nomination_id;

    $rootScope.progressbar_start();
    nominationRules.rule({project_id: $stateParams.id,nomination_id:$stateParams.nomination_id},function (response) {
        $rootScope.progressbar_complete();
        if(response.status == 'false' || response.status == false){
            $rootScope.toastrMessages('error',$filter('translate')('No nomination conditions'));
            return ;
        }
        $scope.rules = response.rules;
        $scope.nomination = response.nomination;
        $scope.project = response.nomination.project;
    });

    $scope.closeRestricted=function(action){
        $rootScope.clearToastr();
        if(action === 0 || action === '0'){
            if( $scope.statistic_flg == true){
                $scope.statistic_flg =false;
            }
        }else{
            if( $scope.restricted == true){
                $scope.restricted =false;
            }
        }

    };

    $scope.checkAll = function(c) {
        angular.forEach($scope.entries, function (item) {
            item.checked = c;
        });
    };

    $scope.rest_candidates = function() {
        angular.element('.btn').addClass("disabled");
        $rootScope.clearToastr();
        $rootScope.progressbar_start();
        nominationRules.rest_candidates({id:$stateParams.nomination_id}, function(response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if(response.status=='success') {
                $scope.entries =[];
                $rootScope.CurrentPage = 1;
                $rootScope.TotalItems =0;
                $rootScope.ItemsPerPage = 50;
                $rootScope.toastrMessages('success',response.msg);

            }else{
                $rootScope.toastrMessages('error',response.msg);
            }

        });

    };

    $scope.reset = function() {
        $rootScope.clearToastr();
        angular.forEach($scope.rules, function(item) {
            if (item.type == 'options') {
                item.value = []
            }
            if (item.type == "range") {
                item.max = "";
                item.min = "";
            }
        });

    };

    $rootScope.pageChanged = function (CurrentPage) {
        $rootScope.CurrentPage = CurrentPage;
        LoadNominations();
    };

    $scope.itemsPerPage_ = function (itemsCount) {
        $scope.CurrentPage=1;
        $scope.itemsCount = itemsCount ;
        LoadNominations();
    };

    function getRulesData() {
        var data = {
            nomination_id:$stateParams.nomination_id,
            project_id: $scope.project.id,
            rules: {}
        };
        angular.forEach($scope.rules, function(item) {
            data.rules[item.name] = {};
            if (item.hasOwnProperty('min')) {
                data.rules[item.name]['min'] = item.min;
            }
            if (item.hasOwnProperty('max')) {
                data.rules[item.name]['max'] = item.max;
            }
            if (item.hasOwnProperty('value')) {
                data.rules[item.name] = item.value;
            }
        });
        return data;
    }

    $scope.save = function(status) {

        $rootScope.clearToastr();
        var params = getRulesData();
        params.status = status;

        angular.element('.btn').addClass("disabled");
        $rootScope.progressbar_start();
        nominationRules.save(params, function (response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            var msg = response.msg ;
            var has_drop = response.has_drop ;
            var file_type='xlsx';
            if(response.status=='true' || response.status==true)
            {
                if(status == 1){
                    if( response.download_token){
                        msg = msg + ' ' + $filter('translate')(', And a file that meets the requirements and has not been nominated is being downloaded');
                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                        $rootScope.toastrMessages('warning',msg);
                    }else{
                        msg = msg + ' ' + $filter('translate')(", And you're being redirected to the Candidates page");
                        $rootScope.toastrMessages('success',msg);
                        $state.go('repository-projects-candidates',{"msg":true,"id" :$stateParams.id,nomination_id:$stateParams.nomination_id});
                    }
                }
            }else{
                if(!angular.isUndefined(response.download_token)) {
                    msg = msg + ' ' +   $filter('translate')(', And a file that meets the requirements and has not been nominated is being downloaded');
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }
                $rootScope.toastrMessages('error',msg);
            }
        });

    };

    ///////////////////////////////////////////

    $scope.statistic = function() {
        $rootScope.clearToastr();
        angular.element('.btn').addClass("disabled");

        $rootScope.progressbar_start();
        nominationRules.statistic(getRulesData(),function (response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if (response.status===false || response.status==='false') {
                $rootScope.toastrMessages('error',response.msg);
            }else{
                if(!angular.isUndefined(response.download_token)) {
                    $rootScope.toastrMessages('success',$filter('translate')('downloading'));
                    var file_type='xlsx';
                    window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                }else{
                    $rootScope.toastrMessages('error',$filter('translate')('action failed'));
                }
            }
        });
    };


    var  LoadNominations = function(){
        // $rootScope.clearToastr();
        $scope.cAll = false;
        if($rootScope.CurrentPage == 1){
            $scope.entries=[];
        }
        $scope.statistic_flg=false;

        var data = getRulesData();
        data.page =$rootScope.CurrentPage;
        data.itemsCount =$scope.itemsCount;
        angular.element('.btn').addClass("disabled");
        $rootScope.progressbar_start();
        nominationRules.filter(data, function(response) {
            $rootScope.progressbar_complete();
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            var records = response.data;
            $scope.entries =records.data;
            $rootScope.CurrentPage = records.current_page;
            $rootScope.TotalItems = records.total;
            $rootScope.ItemsPerPage = records.per_page;
            if(records.total == 0){
                $rootScope.toastrMessages('error',$filter('translate')('There are no new candidates who meet the specified conditions'));
            }
        });
    };

    $scope.results = function() {
        $rootScope.clearToastr();
        $rootScope.CurrentPage = 1;
        LoadNominations();
    };

    $scope.candidate = function(type) {

        $rootScope.clearToastr();
        var params = {};
        var passed = true;

        if(type == 2){
            var candidates = [];
            angular.forEach($scope.entries, function (item) {
                if (item.checked) {
                    candidates.push({case_id:item.case_id,person_id:item.id,organization_id:item.organization_id});
                }
            });

            if (candidates.length == 0) {
                passed = false;
                $rootScope.toastrMessages('error',$filter('translate')("There are no specific people to nominate"));
                return;
            }
            params.candidates =  candidates;
            if(passed){
                params.project_id =  $scope.project.id;
                params.nomination_id =  $stateParams.nomination_id;
                params.type = type;
                angular.element('.btn').addClass("disabled");
                $rootScope.progressbar_start();
                nominationRules.candidates(params, function(response) {
                    $rootScope.progressbar_complete();
                    angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                    if (response.status==false) {
                        $rootScope.toastrMessages('error',response.msg);
                        $scope.msg=response.msg;
                    }else{
                        $rootScope.toastrMessages('success',response.msg);
                    }

                    if(!angular.isUndefined(response.download_token)) {
                        var file_type='xlsx';
                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                    }

                    $rootScope.CurrentPage = 1;
                    LoadNominations();

                });
            }
        }
        else{
            params = getRulesData();
            params.project_id =  $scope.project.id;
            params.nomination_id =  $stateParams.nomination_id;
            params.type = type;
            angular.element('.btn').addClass("disabled");
            $rootScope.progressbar_start();
            nominationRules.candidates(params, function(response) {
                $rootScope.progressbar_complete();
                angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
                var msg = response.msg ;
                var has_drop = response.has_drop ;
                var file_type='xlsx';
                if(response.status=='true' || response.status==true) {
                    if(status == 1){
                        if( response.download_token){
                            msg = msg +'  ' + $filter('translate')(', And a file that meets the requirements and has not been nominated is being downloaded');
                            window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                            $rootScope.toastrMessages('warning',msg);
                        }else{
                            msg = msg +'  ' +  response.msg  + $filter('translate')(", And you're being redirected to the Candidates page");
                            $rootScope.toastrMessages('success',msg);
                            $state.go('repository-projects-candidates',{"msg":true,"id" :$stateParams.id,nomination_id:$stateParams.nomination_id});
                        }
                    }
                }
                else{
                    if(!angular.isUndefined(response.download_token)) {
                        msg = msg +'  ' +   $filter('translate')(', And a file that meets the requirements and has not been nominated is being downloaded');
                        window.location = '/api/v1.0/common/files/'+file_type+'/export?download_token='+response.download_token;
                    }
                    $rootScope.toastrMessages('error',msg);
                }

                $rootScope.CurrentPage = 1;
                LoadNominations();
            });
        }

    }

});
