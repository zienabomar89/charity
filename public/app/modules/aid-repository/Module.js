angular.module('AidRepositoryModule', [
    "ui.router",
    "ngResource",
    "oc.lazyLoad",
    "pascalprecht.translate"
]);

AidRepositoryModule = angular.module('AidRepositoryModule');

AidRepositoryModule.config(function ($stateProvider) {
    $stateProvider

        .state('repository-organizations', {
            url: '/aid-repository/organizations/{id}',
            templateUrl: '/app/modules/aid-repository/views/organizations.html',
            data: {pageTitle: ' ',id:null},
            controller: 'RatioController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'RatioController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/app/modules/aid-repository/controllers/RatioController.js',
                                '/app/modules/setting/services/EntityService.js',
                                '/app/modules/organization/services/OrgService.js',
                            ]
                        });
                    }]
            }
        })
        .state('repository-repositories-index', {
                url: '/aid-repository/repositories',
                templateUrl: '/app/modules/aid-repository/views/repositories/index.html',
                data: {pageTitle: ' '},
                controller: 'RepositoriesController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'RepositoriesController',
                                files: [
                                    '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                    '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                    '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                    '/app/modules/aid-repository/controllers/RepositoriesController.js',
                                    '/app/modules/setting/services/EntityService.js',
                                    '/app/modules/organization/services/OrgService.js',
                                    '/app/modules/aid-repository/services/RepositoryService.js',
                                ]
                            });
                        }]
                }
            })
        .state('repository-repositories-trash', {
                url: '/aid-repository/repositories/trash',
                templateUrl: '/app/modules/aid-repository/views/repositories/trash.html',
                data: {pageTitle: ' '},
                controller: 'RepositoriesTrashController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'RepositoriesTrashController',
                                files: [
                                    '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                    '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                    '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                    '/app/modules/aid-repository/controllers/RepositoriesController.js',
                                    '/app/modules/setting/services/EntityService.js',
                                    '/app/modules/organization/services/OrgService.js',
                                    '/app/modules/aid-repository/services/RepositoryService.js',
                                ]
                            });
                        }]
                }
            })
        .state('repository-repositories-view', {
            url: '/aid-repository/repositories/view/:id',
            templateUrl: '/app/modules/aid-repository/views/repositories/view.html',
                data: {pageTitle: ' '},
                controller: 'RepositoryViewController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'FileViewController',
                                files: [
                                    '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                    '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                    '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                    '/app/modules/setting/services/EntityService.js',
                                    '/app/modules/aid-repository/services/RepositoryService.js',
                                    '/app/modules/aid-repository/controllers/RepositoriesController.js'
                                ]
                            });
                        }]
                }
            })
        .state('repository-repositories-new', {
            url: '/aid-repository/repositories/new',
            templateUrl: '/app/modules/aid-repository/views/repositories/new.html',
                data: {pageTitle: ' '},
            controller: 'RepositoryCreateController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'RepositoryCreateController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/app/modules/setting/services/EntityService.js',
                                '/app/modules/aid-repository/controllers/RepositoriesController.js',
                                '/app/modules/aid-repository/services/RepositoryService.js'
                            ]
                        });
                    }]
            }
        })
        .state('repository-repositories-edit', {
             url: '/aid-repository/repositories/edit/:id',
             templateUrl: '/app/modules/aid-repository/views/repositories/edit.html',
            data: {pageTitle: ' '},
            controller: 'RepositoryEditController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'RepositoryEditController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/app/modules/setting/services/EntityService.js',
                                '/app/modules/aid-repository/controllers/RepositoriesController.js',
                                '/app/modules/aid-repository/services/RepositoryService.js'
                            ]
                        });
                    }]
            }
        })

        .state('repository-repositories-projects', {
            url: '/aid-repository/repositories/projects/:id',
            templateUrl: '/app/modules/aid-repository/views/repositories/projects.html',
            data: {pageTitle: ' '},
            controller: 'RepositoryProjectsController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'RepositoryProjectsController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/app/modules/aid-repository/controllers/ProjectsController.js',
                                '/app/modules/aid-repository/services/RepositoryService.js',
                                '/app/modules/sms/services/SmsService.js',
                                '/app/modules/aid/services/VouchersCategoriesService.js',
                                '/app/modules/setting/services/EntityService.js',
                                '/app/modules/organization/services/OrgService.js',
                                '/app/modules/sponsorship/services/SponsorshipService.js'

                            ]
                        });
                    }]
            }
        })
        .state('repository-urgent-repositories-projects', {
            url: '/aid-repository/urgent-repositories/projects/:id',
            templateUrl: '/app/modules/aid-repository/views/repositories/urgent-repositories/projects.html',
            data: {pageTitle: ' '},
            controller: 'UrgentRepositoryProjectsController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'UrgentRepositoryProjectsController',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/aid-repository/controllers/ProjectsController.js',
                            '/app/modules/aid-repository/services/RepositoryService.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/aid/services/VouchersCategoriesService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/sponsorship/services/SponsorshipService.js'

                        ]
                    });
                }]
            }
        })

        .state('repository-projects-nominations-list', {
            url: '/aid-repository/repositories/projects/nominations-list/:id',
            templateUrl: '/app/modules/aid-repository/views/repositories/nominations-list.html',
            data: {pageTitle: ' '},
            controller: 'ProjectsNominationsListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'ProjectsNominationsListController',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/app/modules/setting/services/EntityService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/aid-repository/controllers/ProjectsRulesController.js',
                            '/app/modules/aid-repository/services/RepositoryService.js',
                        ]
                    });
                }]
            }
        })
        .state('repository-projects-nominations-rules', {
            url: '/aid-repository/repositories/projects/rules/:id/:nomination_id',
            templateUrl: '/app/modules/aid-repository/views/repositories/rules.html',
            data: {pageTitle: ' '},
            params:{nomination_id: null},
            controller: 'ShowProjectsRulesController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'ShowProjectsRulesController',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/app/modules/setting/services/EntityService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/aid-repository/controllers/ProjectsRulesController.js',
                            '/app/modules/aid-repository/services/RepositoryService.js',
                        ]
                    });
                }]
            }
        })
        .state('repository-projects-nominations', {
            url: '/aid-repository/repositories/projects/nominations/:id/:nomination_id',
            templateUrl: '/app/modules/aid-repository/views/repositories/nominations.html',
            data: {pageTitle: ' '},
            params:{nomination_id: null,id: null,msg:null},
            controller: 'NominationsController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'NominationsController',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/aid-repository/controllers/ProjectsRulesController.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/aid-repository/services/RepositoryService.js',
                        ]
                    });
                }]
            }
        })
        .state('repository-projects-candidates', {
            url: '/aid-repository/repositories/projects/candidates/:id/:nomination_id',
            templateUrl: '/app/modules/aid-repository/views/repositories/candidates.html',
            params:{nomination_id: null,id: null,msg:null},
            data: {pageTitle: ''},
            controller: 'ProjectCandidatesController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'ProjectCandidatesController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                '/app/modules/document/directives/fileDownload.js',
                                '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/app/modules/aid-repository/controllers/ProjectCandidatesController.js',
                                '/app/modules/aid-repository/services/RepositoryService.js',
                                '/app/modules/setting/services/EntityService.js',
                                '/app/modules/sms/services/SmsService.js',
                                '/app/modules/common/services/CaseService.js',
                                '/app/modules/organization/services/OrgService.js',
                                '/app/modules/aid-repository/services/RepositoryService.js',
                            ]
                        });
                    }]
            }
        })

        .state('repository-organization-projects', {
            url: '/aid-repository/organization/projects',
            templateUrl: '/app/modules/aid-repository/views/projects/index.html',
            data: {pageTitle: ' '},
            controller: 'RepositoryOrganizationProjectsController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'RepositoryOrganizationProjectsController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/app/modules/aid-repository/controllers/OrganizationProjectsControllers.js',
                                '/app/modules/aid/services/VouchersService.js',
                                '/app/modules/setting/services/EntityService.js',
                                '/app/modules/aid/services/VouchersCategoriesService.js',
                                '/app/modules/organization/services/OrgService.js',
                                '/app/modules/aid-repository/services/RepositoryService.js',

                            ]
                        });
                    }]
            }
        })
        .state('repository-organization-projects-list', {
            url: '/aid-repository/organization/projects/:id/nominations-list',
            templateUrl: '/app/modules/aid-repository/views/projects/nominations-list.html',
            data: {pageTitle: ' '},
            controller: 'RepositoryOrganizationProjectsNominationsListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'RepositoryOrganizationProjectsNominationsListController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/app/modules/setting/services/EntityService.js',
                                '/app/modules/aid-repository/controllers/OrganizationProjectsControllers.js',
                                '/app/modules/aid-repository/services/RepositoryService.js',
                            ]
                        });
                    }]
            }
        })
        .state('repository-organization-projects-rules', {
            url: '/aid-repository/organization/projects/:id/rules/:nomination_id',
            templateUrl: '/app/modules/aid-repository/views/projects/rules.html',
            params:{nomination_id: null},
            data: {pageTitle: ' '},
            controller: 'RepositoryOrganizationProjectsRulesController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RepositoryOrganizationProjectsRulesController',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/aid-repository/controllers/OrganizationProjectsControllers.js',
                            '/app/modules/aid-repository/services/RepositoryService.js',
                        ]
                    });
                }]
            }
    })
        .state('repository-organization-projects-candidates', {
            url: '/aid-repository/organization/projects/:id/candidates/:nomination_id',
            params:{nomination_id: null},
            templateUrl: '/app/modules/aid-repository/views/projects/candidates.html',
            data: {pageTitle: ' '},
            controller: 'RepositoryOrganizationProjectsCandidatesController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'RepositoryOrganizationProjectsCandidatesController',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/app/modules/document/directives/fileDownload.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/aid-repository/controllers/OrganizationProjectsControllers.js',
                            '/app/modules/aid-repository/services/RepositoryService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/common/services/CaseService.js',
                            '/app/modules/aid-repository/services/RepositoryService.js',
                        ]
                    });
                }]
            }
        })
        
        //******************************* central-repositories ********************************//
         .state('central-repositories-index', {
                url: '/aid-repository/central',
                templateUrl: '/app/modules/aid-repository/views/central/index.html',
                data: {pageTitle: ' '},
                controller: 'CentralController',
                resolve: {
                    deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'CentralController',
                                files: [
                                    '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                    '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                    '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                    '/app/modules/aid-repository/controllers/central/CentralController.js',
                                    '/app/modules/setting/services/EntityService.js',
                                    '/app/modules/organization/services/OrgService.js',
                                    '/app/modules/aid-repository/services/RepositoryService.js',
                                ]
                            });
                        }]
                }
            })
         .state('central-repositories-projects', {
            url: '/aid-repository/central/projects/:id',
            templateUrl: '/app/modules/aid-repository/views/central/projects.html',
            data: {pageTitle: ' '},
            controller: 'CentralProjectsController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'CentralProjectsController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/app/modules/aid-repository/controllers/central/CentralProjectsController.js',
                                '/app/modules/aid-repository/services/RepositoryService.js',
                                '/app/modules/sms/services/SmsService.js',
                                '/app/modules/aid/services/VouchersCategoriesService.js',
                                '/app/modules/setting/services/EntityService.js',
                                '/app/modules/organization/services/OrgService.js',
                                '/app/modules/sponsorship/services/SponsorshipService.js'

                            ]
                        });
                    }]
            }
        })
         .state('central-urgent-repositories-projects', {
            url: '/aid-repository/central/urgent-projects/:id',
            templateUrl: '/app/modules/aid-repository/views/central/urgent-projects.html',
            data: {pageTitle: ' '},
            controller: 'CentralUrgentProjectsController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CentralUrgentProjectsController',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/aid-repository/controllers/central/CentralProjectsController.js',
                            '/app/modules/aid-repository/services/RepositoryService.js',
                            '/app/modules/sms/services/SmsService.js',
                            '/app/modules/aid/services/VouchersCategoriesService.js',
                            '/app/modules/setting/services/EntityService.js',
                            '/app/modules/organization/services/OrgService.js',
                            '/app/modules/sponsorship/services/SponsorshipService.js'

                        ]
                    });
                }]
            }
        })
         .state('central-projects-nominations-list', {
            url: '/aid-repository/central/projects/nominations-list/:id',
            templateUrl: '/app/modules/aid-repository/views/central/nominations-list.html',
            data: {pageTitle: ' '},
            controller: 'ProjectsNominationsListController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'ProjectsNominationsListController',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/app/modules/setting/services/EntityService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/aid-repository/controllers/ProjectsRulesController.js',
                            '/app/modules/aid-repository/services/RepositoryService.js',
                        ]
                    });
                }]
            }
        })
         .state('central-projects-nominations-rules', {
            url: '/aid-repository/central/projects/rules/:id/:nomination_id',
            templateUrl: '/app/modules/aid-repository/views/central/rules.html',
            data: {pageTitle: ' '},
            params:{nomination_id: null},
            controller: 'ShowProjectsRulesController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'ShowProjectsRulesController',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/app/modules/setting/services/EntityService.js',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/app/modules/aid-repository/controllers/ProjectsRulesController.js',
                            '/app/modules/aid-repository/services/RepositoryService.js',
                        ]
                    });
                }]
            }
        })
         .state('central-projects-candidates', {
            url: '/aid-repository/central/projects/candidates/:id/:nomination_id',
            templateUrl: '/app/modules/aid-repository/views/central/candidates.html',
            params:{nomination_id: null,id: null,msg:null},
            data: {pageTitle: ''},
            controller: 'CentralProjectCandidatesController',
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                        return $ocLazyLoad.load({
                            name: 'CentralProjectCandidatesController',
                            files: [
                                '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                '/app/modules/document/directives/fileDownload.js',
                                '/themes/metronic/global/plugins/angularjs/plugins/angular-file-upload/angular-file-upload.min.js',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                                '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                                '/app/modules/aid-repository/controllers/central/CentralProjectCandidatesController.js',
                                '/app/modules/aid-repository/services/RepositoryService.js',
                                '/app/modules/setting/services/EntityService.js',
                                '/app/modules/sms/services/SmsService.js',
                                '/app/modules/common/services/CaseService.js',
                                '/app/modules/organization/services/OrgService.js',
                                '/app/modules/aid-repository/services/RepositoryService.js',
                            ]
                        });
                    }]
            }
        })
;
});