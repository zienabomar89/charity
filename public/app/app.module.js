var CharityApp = angular.module("CharityApp", [
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad",
    "ngSanitize",
    "ngResource",
    "ngBootbox",
    "ngCookies",
    'angularUtils.directives.dirPagination',
    "pascalprecht.translate",
    'DashboardModule',
    'UserModule',
    'ngProgress',
    'DocumentModule',
    'SettingsModule',
    'AidModule',
    'SponsorshipModule',
    'OrganizationModule',
    'CommonModule',
    'FormsModule',
    'FormsModule',
    'SmsModule',
    "AidRepositoryModule",
    'yaru22.angular-timeago',
    "ngTagsInput",
    "ng.ckeditor",
    "LogModule",
    "SponsorModule",
    "chart.js",
    'btorfs.multiselect']);

CharityApp.config(function ($httpProvider) {
    $httpProvider.interceptors.push(function ($q, $rootScope) {
        return {
            responseError: function (rejection) {
                angular.element('.btn').removeClass("disabled");
                angular.element('.btn').removeAttr('disabled');
                $rootScope.clearToastr();
                if (rejection.status === 401) {
                    localStorage.removeItem("charity_LoggedInUser");
                    return window.location = '/account/login';
                } else if (rejection.status === 422) {
                    var msg = rejection.data.msg? rejection.data.msg : rejection.data.error? rejection.data.error : rejection.data;
                    $rootScope.toastrMessages('error',msg);
                } else if (rejection.status === 500) {
                    $rootScope.toastrMessages('error','حدث خطأ داخلي على مستوى الخادم!');
                } else if (rejection.status === 403) {
                    $rootScope.toastrMessages('error','لا تملك الصلاحيات المناسبة لتنفيذ هذا الإجراء');
                }else{
                    var msg = rejection.data.msg? rejection.data.msg : rejection.data.error? rejection.data.error : rejection.data;
                    $rootScope.toastrMessages('error',msg);
                }
                // return $q.reject(rejection);
            }
        };
    });
});

// CharityApp.config(function ($translateProvider) {
//
// });


/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
CharityApp.config(['$ocLazyLoadProvider','$httpProvider', '$ngBootboxConfigProvider','$translateProvider', function ($ocLazyLoadProvider, $httpProvider, $ngBootboxConfigProvider,$translateProvider) {

    $translateProvider.useSanitizeValueStrategy(null);
    $translateProvider.preferredLanguage('ar');



    $translateProvider.useStaticFilesLoader({
        files: [{
            prefix: '/app/i18n/',
            suffix: '.json'
        }
            , {
                prefix: '/app/modules/dashboard/i18n/',
                suffix: '.json'
            }
            , {    prefix: '/app/modules/common/i18n/',
                suffix: '.json'
            }
            ,
            {
                prefix: '/app/modules/user/i18n/',
                suffix: '.json'
            }
            , {
                prefix: '/app/modules/aid/i18n/',
                suffix: '.json'
            }
            , {
                prefix: '/app/modules/sponsorship/i18n/',
                suffix: '.json'
            }, {
                prefix: '/app/modules/document/i18n/',
                suffix: '.json'
            }, {
                prefix: '/app/modules/aid-repository/i18n/',
                suffix: '.json'
            }
            , {
                prefix: '/app/modules/sms/i18n/',
                suffix: '.json'
            }
            , {
                prefix: '/app/modules/forms/i18n/',
                suffix: '.json'
            }
            , {
                prefix: '/app/modules/organization/i18n/',
                suffix: '.json'
            }
            , {
                prefix: '/app/modules/log/i18n/',
                suffix: '.json'
            }
            , {
                prefix: '/app/modules/setting/i18n/',
                suffix: '.json'
            }
        ]
    });

    $ngBootboxConfigProvider.addLocale('ar', {OK: 'موافق', CANCEL: 'الغاء الأمر', CONFIRM: 'موافق'});

    $ocLazyLoadProvider.config({
            modules: [{
                name: 'DashboardModule',
                files: ['/app/modules/dashboard/dashboard.module.js']
            }, {
                name: 'UserModule',
                files: ['/app/modules/user/Module.js']
            }, {
                name: 'DocumentModule',
                files: ['/app/modules/document/Module.js']
            }, {
                name: 'SettingsModule',
                files: ['/app/modules/setting/Module.js']
            }, {
                name: 'AidModule',
                files: ['/app/modules/aid/Module.js']
            }, {
                name: 'CommonModule',
                files: ['/app/modules/common/Module.js']
            }, {
                name: 'SponsorshipModule',
                files: ['/app/modules/sponsorship/Module.js']
            }]
        });
    }]);

//AngularJS v1.3.x workaround for old style controller declarition in HTML
CharityApp.config(['$controllerProvider', function ($controllerProvider) {
        // this option might be handy for migrating old apps, but please don't use it
        // in new ones!
        $controllerProvider.allowGlobals();
    }]);

/* Setup global settings */
CharityApp.factory('settings', ['$rootScope', function ($rootScope) {
        // supported languages
        var settings = {
            layout: {
                pageSidebarClosed: false, // sidebar menu state
                pageContentWhite: true, // set page content layout
                pageBodySolid: false, // solid body color state
                pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
            },
            assetsPath: '/themes/metronic',
            globalPath: '/themes/metronic/global',
            layoutPath: '/themes/metronic/layouts/layout3',
            StoragePath: '/storage/app/'
        };

        $rootScope.settings = settings;

        return settings;
    }]);

/* Setup App Main Controller */
CharityApp.controller('AppController', ['$scope', '$rootScope', '$http','Notification','$timeout','$state','$filter','$ngBootbox','$uibModal', function($scope,$rootScope,$http,Notification,$timeout,$state,$filter,$ngBootbox,$uibModal) {
    $rootScope.do=false;
    $scope.$on('$viewContentLoaded', function() {
        App.initComponents(); // init core components
        //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive 

        if(angular.isUndefined($rootScope.lang)) {
            $rootScope.angular_locale =  '/themes/metronic/global/plugins/angularjs/i18n/angular-locale.js';
        }else {

            if ($rootScope.lang == 'ar'){
                angular_locale = '/themes/metronic/global/plugins/angularjs/i18n/angular-locale_ar-ps.js';
                $rootScope.days = $rootScope.days_ar;
            }else{
                $rootScope.angular_locale =  '/themes/metronic/global/plugins/angularjs/i18n/angular-locale.js';

            }
        }

        /*
         if (langKey == 'ar'){
            $rootScope.lang_tg = 'ع' ;
            $ngBootbox.setLocale($translate.use());
        }
        else{
            $ngBootbox.setLocale('en_US');
            $rootScope.lang_tg = 'ENG' ;
            $rootScope.days = $rootScope.days_en;
        }
         */


    });

    $rootScope.hasMessage = function () {
        $rootScope.progressbar_start();
        Notification.unreadMessages(function (response) {
            $rootScope.progressbar_complete();
            //     $scope.users.splice(idx, 1);
            //     $rootScope.toastrMessages('success',$filter('translate')('action success'));

            $rootScope.hasMessageCont= false;
            if (response.count > 0){
                $rootScope.hasMessageCont= true;
                $rootScope.hasMessageStatus = false;
                var currentLocation = window.location.href;
                let url = currentLocation.split('#')[1];
                var urlStr = url.replace(/\//g, '-');

                if (urlStr.toLowerCase() === '-maillingsystem-inbox' ) {
                    $rootScope.hasMessageTime= 180000; // 5 minutes
                    $ngBootbox.confirm( $filter('translate')('You have unread message,You will be refresh inbox page within 30 seconds'))
                        .then(function() {
                            $timeout(function() {
                                $state.reload();
                            },3000);
                        });


                }else{
                    $rootScope.hasMessageTime= 45000; // 1 minutes
                    $ngBootbox.confirm( $filter('translate')('You have unread message,You will be redirected to the mailing within 30 seconds'))
                        .then(function() {
                            alert('confirm');
                            $timeout(function() {
                                $state.go('mailing-system',{"id" : 'inbox'});
                                // window.location.href = '/#/maillingSystem/inbox';
                            },30000);
                        });
                }

            }else{
                $rootScope.hasMessageStatus = true;
            }


        });
    };

    $rootScope.resetMessageTime = function () {
        var currentLocation = window.location.href;
        let url = currentLocation.split('#')[1];
        var urlStr = url.replace(/\//g, '-');

        if (urlStr.toLowerCase() === '-maillingsystem-inbox' ) {
            $rootScope.hasMessageTime= 300000; // 5 minutes
        }
        else{
            $rootScope.hasMessageTime= 120000; // 2 minutes
        }
    };

    $rootScope.checkOldCard=function() {

        $uibModal.open({
            templateUrl: '/app/tpl/check_old_card.html',
            backdrop  : 'static',
            keyboard  : false,
            windowClass: 'modal',
            size: 'lg',
            controller: function ($rootScope,$scope, $modalInstance) {

                $scope.cards ='';

                $scope.confirm = function (cards) {
                    $rootScope.progressbar_start();
                    Notification.checkCards({cards:cards}, function (response) {
                        $rootScope.progressbar_complete();
                        if(response.status=='success') {
                            $rootScope.toastrMessages('success',response.msg);
                            if(!angular.isUndefined(response.download_token)) {
                                window.location = '/api/v1.0/common/files/xlsx/export?download_token='+response.download_token;
                            }
                            $modalInstance.close();
                        }else{
                            $rootScope.toastrMessages('error',response.msg);
                        }
                    });

                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };

            },
            resolve: {
            }
        });

    };

}]);

/***
Layout Partials.
By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial 
initialization can be disabled and Layout.init() should be called on page load complete as explained above.
***/

/* Setup Layout Part - Header */
CharityApp.controller('HeaderController', ['$scope','$state','$rootScope','$window','$translate','$http','$cookies' ,'Notification','$ngBootbox','Language','$timeout',function($scope,$state,$rootScope, $window,$translate,$http,$cookies,Notification,$ngBootbox,Language,$timeout) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initHeader(); // init header
    });
    $rootScope.lang_tg = 'ع' ;
    $rootScope.unread_notifications_count = 0;
    $rootScope.notifications = [];

    // Notification.header({page:1}, function (response) {
    //     $rootScope.notifications = response.notifications.data;
    //     $rootScope.unread_notifications_count = response.unread_notifications_count;
    // });

        $http({
            method: 'GET',
            url: 'doc/files/userImage',
            responseType: 'arraybuffer',
            cache: false
        }).then(function (response) {
            //     angular.element('#headerImg').attr('src',url);
            //     // angular.element('.userImage').attr('src',url);
            var blob = new Blob([response.data], {type: response.headers('Content-Type')});
            url = (window.URL || window.webkitURL).createObjectURL(blob);
            $rootScope.UserImage =url;
        }, function (response) {
        });

//    }
       $scope.logout = function () {
           // Language.setLocale({id: 'ar'});

           $rootScope.revokeUserStatus = false;
           Notification.logout(function (response) {
               localStorage.removeItem("charity_LoggedInUser");
               return $window.location.href = '/account/login';
           });

           // console.log('HeaderController');
    };

    $scope.read=function (row,$index) {
        if(row.status == 1 || row.status == '1'){
            window.location = row.url;
        }else{
            Notification.read({id:row.id}, function (response) {
                var data = response;
                if(data.status == 'true' || data.status == true){
                    // -- notice cnt + remove class from element
                    $rootScope.unread_notifications_count --;
                    row.status = 1;
                    window.location = row.url;
                }
            });
        }
    };

    $rootScope.changeLanguage = function (langKey) {
        $translate.use(langKey);
        $rootScope.lang  = langKey;
        Language.setLocale({id: langKey});
        $cookies.put('lang', $translate.use());

        if (langKey == 'ar'){
            $rootScope.lang_tg = 'ع' ;
            $ngBootbox.setLocale($translate.use());
            $rootScope.days = $rootScope.days_ar;
        }
        else{
            $ngBootbox.setLocale('en_US');
            $rootScope.lang_tg = 'ENG' ;
            $rootScope.days = $rootScope.days_en;
        }

        $timeout(function () {
            $state.reload();
        },10);
    };

}]);

/* Setup Layout Part - Sidebar */
CharityApp.controller('SidebarController', function($window,$scope, $http, $rootScope,$location,category) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initSidebar(); // init sidebar

        $rootScope.sponsorCategories=[];
        category.getSectionForEachCategory({'type':'sponsorships'},function (response) {
            if(!angular.isUndefined(response)){
                $rootScope.sponsorCategories = response.Categories;
            }
        });


        $rootScope.aidCategories_firstStep_map =[];
        $rootScope.aidCategories_ =[];
        $rootScope.aidCategories=[];
        category.getSectionForEachCategory({'type':'aids'},function (response) {
            if(!angular.isUndefined(response)){
                $rootScope.aidCategories_firstStep_map =[];
                $rootScope.aidCategories = response.Categories;
                if($rootScope.aidCategories.length > 0){
                    for (var i in $rootScope.aidCategories) {
                        var item = $rootScope.aidCategories[i];
                        $rootScope.aidCategories_firstStep_map[item.id] = item.FirstStep;
                    }
                    $rootScope.aidCategories_ =[];
                    angular.forEach($rootScope.aidCategories, function(v, k) {
                        if ($rootScope.lang == 'ar'){
                            $rootScope.aidCategories_.push({'id':v.id,'name':v.name,'FirstStep':v.FirstStep});
                        }
                        else{
                            $rootScope.aidCategories_.push({'id':v.id,'name':v.en_name,'FirstStep':v.FirstStep});
                        }
                    });
                }
            }
        });

    });

    $rootScope.categoriesLang = function(i){
        if ($rootScope.lang == 'ar'){
            return i.name ;
        }
        else{
            return i.en_name ;
        }
    }

});

/* Setup Layout Part - Footer */
CharityApp.controller('FooterController', ['$scope', function($scope) {
    $scope.$on('$includeContentLoaded', function() {
        Layout.initFooter(); // init footer
    });
}]);

/* Setup Rounting For All Pages */
CharityApp.config(['$stateProvider', '$httpProvider', '$urlRouterProvider', function ($stateProvider, $httpProvider, $urlRouterProvider) {
    // Redirect any unmatched url

    $urlRouterProvider.otherwise("/dashboard");



}]);

CharityApp.factory('AppModule', function ($resource) {
    return $resource('/api/v1.0/modules');
});
CharityApp.factory('AppRoute', function ($resource) {
    return $resource('/api/v1.0/routes');
});

/* Init global settings and run the app */
CharityApp.run(["$rootScope", "settings", "$state", "$http", "$interval","$timeout","$location",'$window','Notification' ,'$ngBootbox','$translate','Language','$cookies','ngProgressFactory','$filter',function ($rootScope, settings,$window, $state, $http, $interval,$timeout,$location,Notification,$ngBootbox,$translate,Language,$cookies,ngProgressFactory,$filter) {

    $rootScope.progressbar = ngProgressFactory.createInstance();
    $rootScope.progressbar.setHeight('15px');

    function handlerClickEvents(e){
            e.stopPropagation();
            e.preventDefault();
    }

    $rootScope.progressbar_start = function (){
        $rootScope.progressbar.start();
        angular.element("input[type='button']").addClass("disabled");
        angular.element("input[type='submit']").addClass("disabled");
        // angular.element(".btn").addClass("disabled");
        // angular.element("a").addClass("disabled");
        // angular.element('.btn').addClass("disabled");
        // angular.element('.btn').css('cursor', 'not-allowed');
        $('.btn').addClass('loading');
        document.addEventListener("click",handlerClickEvents,true);

        $('.loader-').removeClass('display-loader');
        // angular.element('.loader-').css('display', 'block');

    };

    $rootScope.RestMsg=function(){

    };

    $rootScope.progressbar_complete = function (){
        $rootScope.progressbar.complete();
        angular.element("input[type='button']").removeClass("disabled");
        angular.element("input[type='button']").removeClass("loading");
        angular.element("input[type='button']").removeAttr("disabled");

        angular.element("a").removeClass("loading");
        angular.element("a").removeClass("disabled");
        angular.element("a").removeAttr("disabled");

        angular.element("input[type='submit']").removeClass("disabled");
        angular.element("input[type='submit']").removeClass("loading");
        angular.element("input[type='submit']").removeAttr("disabled");
                 // angular.element('.loader-').css('display', 'none');
        $('.loader-').addClass('display-loader');
        $('.btn').removeClass('loading');
        $(':button').removeClass('loading');
        $(':button.btn.btn-xs').removeClass('loading');
        $('.fa').removeClass('loading');
        document.removeEventListener("click",handlerClickEvents,true);
        // angular.element('.btn').css('cursor', 'pointer');
        angular.element('.btn').removeClass("loading");
        angular.element('.btn').removeClass("disabled");
        angular.element('.btn').removeAttr('disabled');
    };



    $rootScope.lang  = 'ar';
    Language.getLocale(function (response) {
        var langKey = response.locale;
        $translate.use(langKey);
        $rootScope.lang  = langKey;
        $cookies.put('lang', $translate.use());

        if (langKey == 'ar'){
            $rootScope.lang_tg = 'ع' ;
            $ngBootbox.setLocale($translate.use());
            $rootScope.days = $rootScope.days_ar;
        }
        else{
            $ngBootbox.setLocale('en_US');
            $rootScope.lang_tg = 'ENG' ;
            $rootScope.days = $rootScope.days_en;
        }

    });

    $rootScope.revokeUserStatus = true;
    $rootScope.hasMessageStatus= false;
    $rootScope.hasMessageCont= true;
    $rootScope.hasMessageTime= 60000; // 0.5 minutes => 30000
    $rootScope.$state = $state; // state to be accessed from view
    $rootScope.$settings = settings; // state to be accessed from view
    $rootScope.logLogIn=false;
    $rootScope.ShowCitizenImage=false;
    $rootScope.maxSize=10;
    $rootScope.messages = {success: false, error: false, msg:''};
    $rootScope.validate = {status: false ,msg:{}};
    $rootScope.maxSize=10;
    var d = new Date();
    $rootScope.Curyear = d.getFullYear();

    var AuthUser =localStorage.getItem("charity_LoggedInUser");
    if(AuthUser != null && AuthUser != 'null') {

        $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
        $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin; 
        $rootScope.UserType = $rootScope.charity_LoggedInUser.type;
        $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
       
        if(angular.isUndefined($rootScope.charity_LoggedInUser.organization)) {
            if($rootScope.charity_LoggedInUser.organization != null && $rootScope.charity_LoggedInUser.organization != 'null') {
                $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
            }
        }

        $rootScope.ShowCitizenImage=false;
        if(!angular.isUndefined($rootScope.charity_LoggedInUser.type)) {
            if(!( $rootScope.charity_LoggedInUser.type == null  || $rootScope.charity_LoggedInUser.type == 'null' ||
                $rootScope.charity_LoggedInUser.type == 4 ||$rootScope.charity_LoggedInUser.type == '4' ||
                $rootScope.charity_LoggedInUser.type == 5 ||$rootScope.charity_LoggedInUser.type == '5')) {
                $rootScope.ShowCitizenImage=true;
            }
        }

        $http({
            method: 'GET',
            url: 'doc/files/userImage',
            responseType: 'arraybuffer',
            cache: false
        }).then(function (response_) {
            var blob = new Blob([response_.data], {type: response_.headers('Content-Type')});
            url = (window.URL || window.webkitURL).createObjectURL(blob);
            angular.element('#headerImg').attr('src',url);
            // angular.element('.userImage').attr('src',url);
            $rootScope.UserImage =url;
        });

    }


    $rootScope.close=function () {
        $rootScope.messages = {success: '', error: '' ,msg:''};
        $rootScope.validate = {status: false ,msg:{}};
        $rootScope.subMessages = {success: '', error: '' ,msg:''};
           toastr.remove();
   };
    $rootScope.toastrMessages =function (type,message) {
        angular.element('.btn').removeClass("disabled");
        angular.element('.btn').removeAttr('disabled');
        toastr.remove();
        toastr.options = {
              tapToDismiss: false
            , timeOut: 0
            , extendedTimeOut: 0
            , allowHtml: true
            , preventDuplicates: false
            , preventOpenDuplicates: false
            , newestOnTop: true
            , closeButton: true
            , closeHtml: '<button><i class="icon-off pull-right"></i></button>'
        };
        switch(type) {
            case 'error':
              toastr.error(message);
                break;
            case 'warning':
                toastr.warning(message);
                break;
            default:
                toastr.success(message);
        }
        // toastr.clear();
    };
    $rootScope.clearToastr =function () {
        angular.element('.btn').removeClass("disabled");
        angular.element('.btn').removeAttr('disabled');
        toastr.remove();
        toastr.clear();
        $rootScope.revokeUser();
        $rootScope.resetMessageTime();
    };

    $rootScope.revokeUser = function () {
        if ($rootScope.revokeUserStatus){
            // Notification.revokeUser(function (response) {
            //
            //
            // });
        }
    };


    $rootScope.sort = function (keyname) {
        $rootScope.sortKey = keyname;
        $rootScope.reverse = !$rootScope.reverse;
    };

    $rootScope.download=function (method,url,data) {
        $rootScope.close();
        $http({
            method: method,
            url: url,
            data:data,
            responseType: 'arraybuffer',
            cache: false
        }).then(function successCallback(response) {
            var a = document.createElement("a");
            a.style = "display: none";
            document.body.appendChild(a);
            var blob = new Blob([response.data], {type: response.headers('Content-Type')});
            url = (window.URL || window.webkitURL).createObjectURL(blob);
            a.href = url;
            a.click();
            // (window.URL || window.webkitURL).revokeObjectURL(url);
        }, function errorCallback(response) {
        });
    };


    $rootScope.fixed_dropdownn = function(index , currentPage , ItemsPerPage){

        if((ItemsPerPage - (index  +1)) < 7){
            return 'fixed_dropdown';
        }

        // if(( ((index +1)+((currentPage-1)* ItemsPerPage))%(ItemsPerPage)) == 0 ||
        //     (((index +1)+((currentPage-1)* ItemsPerPage))%(ItemsPerPage-1)) == 0 ||
        //     (((index +1)+((currentPage-1)* ItemsPerPage))%(ItemsPerPage-2)) == 0 ||
        //     (((index +1)+((currentPage-1)* ItemsPerPage))%(ItemsPerPage-3)) == 0){
        //     return 'fixed_dropdown';
        // }
    };

    stop = $interval(function() {
        Notification.header({page:1}, function (response) {
            $rootScope.notifications = response.notifications;
            $rootScope.unread_notifications_count = response.unread_notifications_count;
            localStorage.removeItem("charity_LoggedInUser");
            localStorage.setItem('charity_LoggedInUser', JSON.stringify(response.user));
            $rootScope.charity_LoggedInUse = response.user;
            $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;
            $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
            if(angular.isUndefined($rootScope.charity_LoggedInUser.organization)) {
                if($rootScope.charity_LoggedInUser.organization != null && $rootScope.charity_LoggedInUser.organization != 'null') {
                    $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
                }
            }
        });
    }, 60000); // 60000

    $rootScope.$on('$destroy', function() {
        if (angular.isDefined(stop)) {
            $interval.cancel(stop);
            stop = undefined;
        }
    });

    $rootScope.check_id = function (idno) {
        var identity = idno.toString();
        var lastDig = identity[8];
        var digit1 = identity[0] * 1;
        var digit2 = identity[1] * 2;
        var digit3 = identity[2] * 1;
        var digit4 = identity[3] * 2;
        var digit5 = identity[4] * 1;
        var digit6 = identity[5] * 2;
        var digit7 = identity[6] * 1;
        var digit8 = identity[7] * 2;
        var checkLast = parseInt(digit1) + parseInt(digit3) + parseInt(digit5) + parseInt(digit7);
        if (digit2.toString().length == 2) {
            var digit22 = digit2.toString();
            checkLast += parseInt(digit22[0]);
            checkLast += parseInt(digit22[1]);
        } else {
            checkLast += parseInt(digit2);
        }
        if (digit4.toString().length == 2) {
            var digit44 = digit4.toString();
            checkLast += parseInt(digit44[0]);
            checkLast += parseInt(digit44[1]);
        } else {
            checkLast += parseInt(digit4);
        }
        if (digit6.toString().length == 2) {
            var digit66 = digit6.toString();
            checkLast += parseInt(digit66[0]);
            checkLast += parseInt(digit66[1]);
        } else {
            checkLast += parseInt(digit6);
        }

        if (digit8.toString().length == 2) {
            var digit88 = digit8.toString();
            checkLast += parseInt(digit88[0]);
            checkLast += parseInt(digit88[1]);
        } else {
            checkLast += parseInt(digit8);
        }

        var checkLast_length = checkLast.toString().length;
        if(checkLast.toString()[checkLast_length - 1]==0)
            var lastOne=10;
        else
            lastOne =  checkLast.toString()[checkLast_length - 1];
        var lastDigit_inCheck = 10 - lastOne;

        if (lastDig != lastDigit_inCheck) {
            return false;
        } else {
            return true;
        }

    };

    $rootScope.resetAuthUser = function () {

        $http({
            method: 'GET',
            url: '/api/v1.0/auth/account/user'
        }).then(function(response) {
            response.data.initails = (response.data.username.substr(0, 1)).toUpperCase();
            $rootScope.charity_LoggedInUser = response.data;
            localStorage.setItem("charity_LoggedInUser", JSON.stringify(response.data));
            $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;
            $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;
            if(angular.isUndefined($rootScope.charity_LoggedInUser.organization)) {
                if($rootScope.charity_LoggedInUser.organization != null && $rootScope.charity_LoggedInUser.organization != 'null') {
                    $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
                }
            }

            $rootScope.ShowCitizenImage=false;
            if(!angular.isUndefined($rootScope.charity_LoggedInUser.type)) {
                if(!( $rootScope.charity_LoggedInUser.type == null  || $rootScope.charity_LoggedInUser.type == 'null' ||
                    $rootScope.charity_LoggedInUser.type == 4 ||$rootScope.charity_LoggedInUser.type == '4' ||
                    $rootScope.charity_LoggedInUser.type == 5 ||$rootScope.charity_LoggedInUser.type == '5')) {
                    $rootScope.ShowCitizenImage=true;
                }
            }

            $http({
                method: 'GET',
                url: 'doc/files/userImage',
                responseType: 'arraybuffer',
                cache: false
            }).then(function (response_) {
                var blob = new Blob([response_.data], {type: response_.headers('Content-Type')});
                url = (window.URL || window.webkitURL).createObjectURL(blob);
                angular.element('.userImage').attr('src',url);
                $rootScope.UserImage =url;
            });
        });


    };
    $rootScope.resetAuthUser();

    $rootScope.setAuthUser = function () {
        var AuthUser =localStorage.getItem("charity_LoggedInUser");
        if(AuthUser != null && AuthUser != 'null') {

            $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
            $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;
            $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;

            if(angular.isUndefined($rootScope.charity_LoggedInUser.organization)) {
                if($rootScope.charity_LoggedInUser.organization != null && $rootScope.charity_LoggedInUser.organization != 'null') {
                    $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
                }
            }

            $rootScope.ShowCitizenImage= $rootScope.can('reports.case.photo');


            $http({
                method: 'GET',
                url: 'doc/files/userImage',
                responseType: 'arraybuffer',
                cache: false
            }).then(function (response_) {
                var blob = new Blob([response_.data], {type: response_.headers('Content-Type')});
                url = (window.URL || window.webkitURL).createObjectURL(blob);
                angular.element('#headerImg').attr('src',url);
                // angular.element('.userImage').attr('src',url);
                $rootScope.imgSrc = url;
                $rootScope.UserImage =url;
            });

        }


    };

    $rootScope.can = function (action) {

        var $can = false;

        if(angular.isUndefined($rootScope.charity_LoggedInUser)) {
            // $rootScope.resetAuthUser();

            var AuthUser =localStorage.getItem("charity_LoggedInUser");
            if(AuthUser != null && AuthUser != 'null') {

                $rootScope.charity_LoggedInUser = JSON.parse(AuthUser);
                $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin;
                $rootScope.UserOrgId = $rootScope.charity_LoggedInUser.organization_id;

                if(angular.isUndefined($rootScope.charity_LoggedInUser.organization)) {
                    if($rootScope.charity_LoggedInUser.organization != null && $rootScope.charity_LoggedInUser.organization != 'null') {
                        $rootScope.UserOrgLevel = $rootScope.charity_LoggedInUser.organization.level;
                    }
                }

                $rootScope.ShowCitizenImage= $rootScope.can('reports.case.photo');


                $http({
                    method: 'GET',
                    url: 'doc/files/userImage',
                    responseType: 'arraybuffer',
                    cache: false
                }).then(function (response_) {
                    var blob = new Blob([response_.data], {type: response_.headers('Content-Type')});
                    url = (window.URL || window.webkitURL).createObjectURL(blob);
                    angular.element('#headerImg').attr('src',url);
                    // angular.element('.userImage').attr('src',url);
                    $rootScope.imgSrc = url;
                    $rootScope.UserImage =url;
                });
                $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin ;
                if (parseInt($rootScope.superAdmin) === 1) {
                    $can = true;
                }else{
                    angular.forEach($rootScope.charity_LoggedInUser.permissions, function(item) {
                        if (action === item) {
                            $can = true;
                            return;
                        }
                    });
                }
            }



        }
        else{
            $rootScope.superAdmin = $rootScope.charity_LoggedInUser.super_admin ;
            if (parseInt($rootScope.superAdmin) === 1) {
                $can = true;
            }else{
                angular.forEach($rootScope.charity_LoggedInUser.permissions, function(item) {
                    if (action === item) {
                        $can = true;
                        return;
                    }
                });
            }
        }

        return $can;
    };
    $rootScope.OnlyLetter_  = function (str){
        return OnlyLetter(str)
    };

    var floatRound = function(number, precision) {

        if(number != 0 || number != '0'){
            var factor = Math.pow(10, precision);
            var tempNumber = number * factor;
            var roundedTempNumber = Math.round(tempNumber);
            return roundedTempNumber / factor;
        }
        return 0;

    };
    OnlyLetter = function (str) {

        var letters = /^[A-Za-z]+$/;
        if(str.match(letters)) {
            return true;
        }
        else
        {
            return false;
        }
    }

    $rootScope.labels = {
        "itemsSelected": $filter('translate')('itemsSelected'),
        "selectAll": $filter('translate')('selectAll'),
        "unselectAll": $filter('translate')('unselectAll'),
        "search": $filter('translate')('search'),
        "select": $filter('translate')('select')
    }

    stopRevoke = setInterval(() => {

        if ($rootScope.hasMessageCont){
            $rootScope.hasMessage();
        }


        // Notification.revokeUser(function (response) {
        //
        // });
    },$rootScope.hasMessageTime);


    //  $interval(function() {
    //
    //
    // }, 1000); // 60000


    $rootScope.$on('$destroy', function() {
        if (angular.isDefined(stopRevoke)) {
            $interval.cancel(stopRevoke);
            stopRevoke = undefined;
        }
    });
}]).
run(function ($rootScope, $translate) {
    $rootScope.$on('$translatePartialLoaderStructureChanged', function () {
        $translate.refresh();
    });
});