<?php

$HOST     = "localhost";

 $baheth_NAME     = "test";
 $baheth_USER     = "root";
 $baheth_PASSWORD = "";

// *********************************** baheth ***********************************  //
// $baheth_NAME     = "bahethps_baheth_db";
// $baheth_USER     = "bahethps_baheth_user";
// $baheth_PASSWORD = "7.Uw1Q.H;LOk";

// create connection
 $baheth = mysqli_connect($HOST,$baheth_USER,$baheth_PASSWORD,$baheth_NAME);
 mysqli_query($baheth,"SET CHARACTER SET 'utf8'");
 mysqli_query($baheth,"SET SESSION collation_connection ='utf8_unicode_ci'");

// *********************************** takaful ***********************************  //
$takaful_NAME     = "takaful_db_sys";
$takaful_USER     = "root";
$takaful_PASSWORD = "";

// $takaful_USER     = "takaful_sys_usr";
// $takaful_PASSWORD = "At3{*6]PAS5U";
// $takaful_NAME     = "takaful_db_sys";

 $takaful = mysqli_connect($HOST,$takaful_USER,$takaful_PASSWORD,$takaful_NAME);
 mysqli_query($takaful,"SET CHARACTER SET 'utf8'");
 mysqli_query($takaful,"SET SESSION collation_connection ='utf8_unicode_ci'");

// ******************************************************************************* //

var_dump( importRewards($baheth,$takaful));

mysqli_close($baheth);
mysqli_close($takaful);

function importRewards($baheth,$takaful){

    ini_set('max_execution_time',0);
    set_time_limit(0);
    Ini_set('memory_limit','2048M');

    $ids = [];
    $import = mysqli_query($takaful,"SELECT reward_id FROM `char_social_search` WHERE `reward_id` is not null ");
    if($import) {
        $total = $import->num_rows;
        if ($total > 0) {
            while ($row = mysqli_fetch_assoc($import)) {
                $ids[] = $row['reward_id'];
            }
        }
    }

//    WHERE `status` = 1
    $query = "SELECT * FROM `rewards`";
    if (sizeof($ids) > 0){
        $str = implode(",", $ids);
//        `status` = 1 and
        $query = "SELECT * FROM `rewards` WHERE id not in ( ". $str ." )";
    }

    $list = mysqli_query($baheth,$query);
    $rewards_data =[];
    if($list) {
        $total = $list->num_rows;
        if ($total > 0) {
            $rewardKeys = ['organization_id','category_id','user_id','title','notes','status', 'date','date_to','date_from'];

            while ($row = mysqli_fetch_assoc($list)) {
                $data = rewardMap($row);
                $data['reward_id'] = $row['id'];

                $insert_keys = [];
                $insert_values = [];
                foreach ($data as $key => $value) {
                    $insert_keys []=  "`".$key."`";
                    $insert_values []=  "'".$value."'";
                }

                $insert_keys_ = join(",", $insert_keys);
                $insert_values_ = join(',', $insert_values);

                $query = "INSERT INTO `char_social_search` ( ".$insert_keys_. ") VALUES ( " .$insert_values_ . " ) ";
                mysqli_query($takaful,$query);

                $new_reward_id = mysqli_insert_id($takaful);

                 $data['cases'] = getCases($row['id'],$new_reward_id,$baheth,$takaful);

                 return $data['cases'];
                $rewards_data[] = $data;
                $rewards_data[] = $data;
            }
        }
    }
    return $rewards_data;
}

function rewardMap($data){

    $row = [ 'category_id'=>5 , 'organization_id'=>1 , 'user_id'=>1];

    $_translator = ["name"=>"title" ,"note"=> "notes" ,"status"=>"status" ,"close"=> "close",
        "start_date"=>"date_from" , "end_date"=> "date_to"];

    $dates = ["start_date","date_from","end_date","date_to"];

    foreach ($data as $k=>$v) {
        if(isset($_translator[$k])){
            if(in_array($_translator[$k],$dates)){
                if(!is_null($v) && $v != " "){
                    $row[$_translator[$k]]=date('Y-m-d',strtotime($v));
                }else{
                    $row[$_translator[$k]]=null;
                }
            }
            else{
                $row [$_translator[$k]]=$v;
            }
        }
    }

    $row['date'] = $row['date_from'] ;

    return $row;

}

function getCases($old_reward_id , $new_reward_id,$baheth, $takaful){

    $query = caseQuery($old_reward_id);
    $cases = mysqli_query($baheth,$query);

    $rows = [];
    $caseKeys = caseKeys();
    $keys = join(",", $caseKeys);

    while ($case = mysqli_fetch_assoc($cases)) {
        $case['reward_id'] = $new_reward_id;

        $insert_values = [];
        foreach ($caseKeys as $key){
            if (!($case[$key] == NULL or $case[$key] == 'NULL')){
                $insert_values []=  "'".$case[$key]."'";
            }else{
                $insert_values [] =  'NULL';
            }
        }

        $values = join(',', $insert_values);
        mysqli_query($baheth,"INSERT INTO `rewards_cases` ( ".$keys. ") VALUES ( " .$values . " ) " );
//        mysqli_query($takaful,"INSERT INTO `rewards_cases` ( ".$keys. ") VALUES ( " .$values . " ) " );
        $rows[]=$case;
    }

    // after testing set connection $takaful
    // set district_id
    mysqli_query($takaful,"UPDATE rewards_cases , char_aids_locations_i18n set rewards_cases.district = char_aids_locations_i18n.location_id
                                 where rewards_cases.district = char_aids_locations_i18n.name and char_aids_locations_i18n.language_id = 1 " );


     // set region_id
    mysqli_query($takaful,"UPDATE rewards_cases , char_aids_locations , char_aids_locations_i18n
                                 set    rewards_cases.region = char_aids_locations_i18n.location_id
                                 where  rewards_cases.region = char_aids_locations_i18n.name and 
                                       char_aids_locations.id = char_aids_locations_i18n.location_id  and 
                                       char_aids_locations.parent_id = rewards_cases.district and 
                                       char_aids_locations_i18n.language_id = 1  " );


    // set neighborhood_id
    mysqli_query($takaful,"UPDATE rewards_cases , char_aids_locations , char_aids_locations_i18n
                                 set    rewards_cases.neighborhood = char_aids_locations_i18n.location_id
                                 where  rewards_cases.neighborhood = char_aids_locations_i18n.name and 
                                       char_aids_locations.id = char_aids_locations_i18n.location_id  and 
                                       char_aids_locations.parent_id = rewards_cases.region and 
                                       char_aids_locations_i18n.language_id = 1  " );




    // set square_id
    mysqli_query($takaful,"UPDATE rewards_cases , char_aids_locations , char_aids_locations_i18n
                                 set    rewards_cases.square = char_aids_locations_i18n.location_id
                                 where  rewards_cases.square = char_aids_locations_i18n.name and 
                                       char_aids_locations.id = char_aids_locations_i18n.location_id  and 
                                       char_aids_locations.parent_id = rewards_cases.neighborhood and 
                                       char_aids_locations_i18n.language_id = 1  " );





    return $rows;
}

/////////////////////////////////////////////////////////////////////////////////////////////

function caseKeys(){

    return [
    'id_card_number',
    'card_type',
    'first_name',
    'second_name',
	'third_name',
	'last_name',	
	'prev_family_name',		
	'gender',		
	'marital_status_id',
	'deserted',
	'birthday',
	'birth_place',
	'death_date',
	'death_cause_id',
	'nationality',
	'family_cnt',
	'spouses',
	'male_live',
	'female_live',
    'phone',
	'primary_mobile',
	'secondary_mobile',
	'watanya',
	'country_id',
	'district_id',
	'region_id',
	'neighborhood_id',
	'square_id',
	'mosque_id',
	'street_address',
	'longitude',
	'Latitude',
    'status',
    'reason',
	'created_at',
	'updated_at',
 	'deleted_at'

    ] ;
}

function caseKeys_(){

    return ["id",
//        "family_id",
        "id_card_number",
        "card_type",
        "fullname",
        "fullname_en",
        "first_name",
        "second_name",
        "third_name",
        "last_name",
        "longitude",
        "latitude",
        "prev_family_name",
        "gender",
        "marital_status",
        "deserted",
        "birthday",
        "birth_place",
        "death_date",
        "death_cause",
        "nationality",
        "family_cnt",
        "spouses",
        "male_live",
        "female_live",
        "refugee",
        "unrwa_card_number",
        "is_qualified",
        "qualified_card_number",
        "receivables",
        "receivables_sk_amount",
        "monthly_income",
        "actual_monthly_income",
        "phone",
        "primary_mobile",
        "secondary_mobile",
        "watanya",
//        "charity",
        "country",
        "district",
        "region",
        "neighborhood",
        "square",
        "mosque",
        "street_address",
        "street_address",
        "needs",
        "promised",
        "reconstructions_organization_name",

        "visitor",
        "notes",
        "visitor_card",
        "visited_at",
        "visitor_evaluation",
        "visitor_opinion",

        "status",
        "reason",
        "status_type",
        "has_commercial_record",
        "active_commercial_record",
        "gov_commercial_record_details",
        "working",
        "work_status",
        "work_job",
        "work_wage",
        "work_location",
        "can_work",
        "ability_of_work_reason",
//        "does_have_source_of_income",
//        "work_info",

        "house_condition",
        "indoor_condition",
        "residence_condition",
        "property_type",
        "roof_material",
        "area",
        "rooms",
        "rent_value",

        "need_repair",
        "habitable",
//        "home_weight",

        "health_status",
        "health_status_description",
        "disease",
        "has_health_insurance",
        "health_insurance_number",
        "health_insurance_type",

        "has_device",
        "used_device_name",
        "medical_report_summary",

        "chf",
        "oxform",
        "wfp",
        "family_martyrs",
        "prisoners",
        "world_bank",
        "wounded",
        "permanent_unemployment",
        "social_affairs",
        "benefactor",
        "family_sponsorship",
        "guarantees_of_orphans",
        "neighborhood_committees",
        "Zakat",
        "agency_aid",
        "chf_sample",
        "oxform_sample",
        "wfp_sample",
        "family_martyrs_sample",
        "prisoners_sample",
        "world_bank_sample",
        "wounded_sample",
        "permanent_unemployment_sample",
        "social_affairs_sample",
        "benefactor_sample",
        "family_sponsorship_sample",
        "guarantees_of_orphans_sample",
        "neighborhood_committees_sample",
        "Zakat_sample",
        "agency_aid_sample",
        "lands",
        "cars",
        "property",
        "cattle",
        "doors",
        "safe_lighting",
        "gas_tube",
        "cobble",
        "tv",
        "fridge",
        "solarium",
        "watertank",
        "sweet_watertank",
        "closet",
        "heater",
        "windows",
        "electricity_network",
        "water_network",
        "electric_cooker",
        "stuff_kitchen",
        "gas",
        "washer",
        "mattresses",
        "plastic_chairs",
        "fan",
        "top_ceiling",
        "qualifiedRelationship",
        "abilityOfWorkReasonAnother",
        "wallsOfHouseAnother",
        "solarPower",
        "hasCHF",
        "hasOXFAM",
        "hasZakat",
        "hasGuaranteesOfOrphans",
        "hasFamilySponsorship",
        "hasAgencyAid",
        "hasNeighborhoodCommittees",
        "hasSocialAffairs",
        "hasPermanentUnemployment",
        "hasWounded",
        "hasWorldBank",
        "hasPrisoners",
        "hasFamilyMartyrs",
        "hasWFP",
        "zakatCount",
        "guaranteesOfOrphansCount",
        "familySponsorshipCount",
        "agencyAidCount",
        "neighborhoodCommitteesCount",
        "socialAffairsCount",
        "permanentUnemploymentCount",
        "woundedCount",
        "worldBankCount",
        "prisonersCount",
        "familyMartyrsCount",
        "WFPCount",
        "OXFAMCount",
        "CHFCount",
        "chfSampleCount",
        "oxformSampleCount",
        "wfpSampleCount",
        "familyMartyrsSampleCount",
        "prisonersSampleCount",
        "worldBankSampleCount",
        "woundedSampleCount",
        "permanentUnemploymentSampleCount",
        "socialAffairsSampleCount",
        "neighborhoodCommitteesSampleCount",
        "agencyAidSampleCount",
        "familySponsorshipSampleCount",
        "guaranteesOfOrphansSampleCount",
        "zakatSampleCount",
        "other1HelpType",
        "other2HelpType",
        "hasAnotherHelp",
        "other1HelpCount",
        "other1HelpSource",
        "other1HelpValue",
        "other2HelpValue",
        "other2HelpSource",
        "other2HelpCount",
        "bank1AccountNumber",
        "bank1AccountOwner",
        "bank1AccountBranch",
        "bank1Name",
        "bank2AccountNumber",
        "bank2AccountOwner",
        "bank2AccountBranch",
        "bank2Name",
        "hasCar",
        "hasCattle",
        "hasLands",
        "hasProperty",
        "hasQatarHelp",
        "qatarHelp",
        "qatarHelpCount",
        "qatarHelpSample",
        "qatarHelpSampleCount",
        "home_number",
        "created_at",
        "updated_at",
        "not_qualified_reason",
        "person_present",
        "reward_id",

//        "is_old",
//        "last_update_data",
//        "sync_date",
        "active",
        "activeReason",
        "active_reason_hint",
        "manual",
        "case_status",
        "return_status",
        "operation_case_returned_reason"
    
    
    ] ;
}

function caseQuery($reward_id){

    $query = "SELECT
        opc.id,
        opc.reward_id,
        
        opc.case_status,
        opc.active_reason_hint,
        
        
        
        opc.id_card_number,
        opc.card_type,
        opc.fullname,
        opc.fullname_en,
        opc.first_name,
        opc.second_name,
        opc.third_name,
        opc.last_name,
        
        opc.longitude,
        opc.latitude,
        opc.prev_family_name,
        
        CASE WHEN opc.gender = 1 THEN 1  Else 2  END  AS gender,
        
        opc.marital_status,
        
        CASE WHEN opc.deserted = 1 THEN 1  Else 2  END  AS deserted,
        
        opc.birthday,
        
        (SELECT constants.NAME FROM constants WHERE constants.id =opc.nationality ) AS nationality,
        
        (SELECT constants.NAME FROM constants WHERE constants.id =opc.birth_place ) AS birth_place,
        
        opc.death_date,
        
        (SELECT constants.NAME FROM constants WHERE constants.id =opc.death_cause_id ) AS death_cause,
        
        opc.family_cnt,
        opc.spouses,
        opc.male_live ,
        opc.female_live ,
        
        opc.phone,
        opc.primary_mobile,
        opc.secondary_mobile,
        opc.watanya,
        
        1 AS `country`,
        governorates.`name` AS `district`,
        regoins.`name` AS `region`,
        neighborhoods.`name` AS `neighborhood`,
        squares.`name` AS `square`,
        mosques.`name` AS mosque ,
        (SELECT	mosques.`name`FROM mosques WHERE mosques.id = mosque_id) AS `المسجد`,
        opc.street_address_details AS `street_address`,
        
        opc.status,
        opc.reason,
        opc.status_type,
        
        CASE WHEN opc.refugee = 1 THEN 1  Else 2  END  AS refugee,
        CASE WHEN opc.refugee = 1 THEN opc.unrwa_card_number  Else null  END  AS unrwa_card_number,
        
        CASE WHEN opc.is_qualified = 1 THEN 1  Else 0  END  AS is_qualified,
        CASE WHEN opc.is_qualified = 1 THEN opc.qualified_card_number  Else null END  AS qualified_card_number,
        
        CASE WHEN opc.receivables = 1 THEN 1 Else 0 END AS receivables,
        CASE WHEN opc.receivables = 1 THEN opc.receivables_sk_amount Else 0 END AS receivables_sk_amount,
        
        opc.monthly_income ,
        opc.actual_monthly_income ,
        
        researchers.fullname AS visitor,
        researchers.id_number AS visitor_card,
        CASE opc.visitor_evaluation
				WHEN 4 THEN	'ممتاز'
		    WHEN 3 THEN	'جيد جدا'
            WHEN 2 THEN		'جيد'
            WHEN 1 THEN	'سيء'
           WHEN 0 THEN	'سيء جدا'
        END AS visitor_evaluation,
        opc.researcher_recommendations as notes,
        opc.visitor_opinion,
        opc.visited_at,
        
        opc.needs,
        CASE  WHEN opc.promised = 1 THEN 1 Else 2 END AS promised,
        reconstructions_organization_name,
        
        opc.health_status,
        ( SELECT constants.NAME FROM constants WHERE constants.id = opc.disease ) AS disease,
        opc.health_status_description,
        
        CASE WHEN opc.health_insurance = 1 THEN 1  Else 0  END  AS has_health_insurance,
        CASE WHEN opc.health_insurance_type is null THEN ' ' Else ( SELECT constants.NAME FROM constants WHERE constants.id =opc.health_insurance_type ) END   AS health_insurance_type,
        CASE WHEN opc.health_insurance_number is null THEN ' ' Else opc.health_insurance_number END   AS health_insurance_number,
        
        CASE WHEN opc.device_use = 1 THEN 1 ELSE 0 END AS has_device,
        CASE WHEN opc.device_use = 1 THEN opc.device_use_name ELSE null END AS used_device_name,
        opc.medical_report_summary,
        
        
        CASE WHEN opc.do_working = 1 THEN 1  Else 2  END  AS working,
        ( SELECT constants.NAME FROM constants WHERE constants.id =opc.work_status ) AS work_status,
        ( SELECT constants.NAME FROM constants WHERE constants.id =opc.work_title ) AS work_job,
        opc.Income_by_category AS work_wage,
        opc.work_space AS work_location,
        
        CASE WHEN opc.ability_of_work = 1 THEN 1  Else 2  END  AS can_work,
        ability_of_work_reason AS ability_of_work_reason,
        
        
        CASE opc.home_ownership
                WHEN 0 THEN 'ايجار'WHEN 1 THEN 'ايجار'	WHEN 2 THEN 'ملك'
                WHEN 3 THEN 'دون مقابل'	WHEN 4 THEN 'أراضي حكومية'	WHEN 5 THEN 'مستضاف'
        END AS property_type,
        
        opc.rent_value,
        
        CASE	opc.walls_of_house
              WHEN 942 THEN	'باطون جيد'		WHEN 956 THEN	'كرفان'
              WHEN 943 THEN	'كرميد'	WHEN 944 THEN	'زينكو'
              WHEN 459442 THEN	'باطون جديد'	WHEN 945 THEN	'خيمة'
              WHEN 946 THEN	'أسبست'	WHEN 947 THEN	'بلاستيك'
              WHEN 948 THEN	'أخرى'	WHEN 949 THEN	'صفيح جديد'
              WHEN 459323 THEN	'أسبست'	WHEN 950 THEN	'صفيح قديم'
              WHEN 459525 THEN	'باطون'	WHEN 951 THEN'باطون قديم'
              WHEN 952 THEN'نصف بطون و نصف صفيح'	WHEN 953 THEN	'جزء باطون والاخر زينكو'
              WHEN 954 THEN	'جزء باطون والاخر أسبست'	WHEN 459466 THEN'باطون سيئ'
              WHEN 955 THEN	'جزء زينكو والاخر أسبست'	Else Null
              END AS roof_material,
        
        
        CASE opc.home_status
                WHEN 62 THEN	'قديم'	WHEN 459397 THEN	'سيء جدا'
                WHEN 459431 THEN	'سيء'	WHEN 56 THEN	'ممتاز'
                WHEN 57 THEN	'جيد جدا'	WHEN 58 THEN		'جيد'
                WHEN 459325 THEN	'سيء'	WHEN 59 THEN		'متوسط'
                WHEN 60 THEN	'سيء'	WHEN 61 THEN	'سيء جدا'
            END AS residence_condition,
        
        CASE opc.placement_of_furniture
                WHEN 1216 THEN	'سئ'	WHEN 1217 THEN	'سئ جدا'
                WHEN 1218 THEN	'قديم'	WHEN 459441 THEN	'لا يوجد'
                WHEN 459443 THEN'سئ'	WHEN 459040 THEN'سئ'
                WHEN 459043 THEN'سئ جدا'	WHEN 34 THEN	'ممتاز'
                WHEN 35 THEN'جيد جدا'	WHEN 459324 THEN'سئ'
                WHEN 459465 THEN	'سئ جدا'	WHEN 1215 THEN	'جيد'
        END AS indoor_condition,
        
        CASE	opc.description_of_home_status
                WHEN 315 THEN	'بحاجة لإعمار كلي'
                WHEN 316 THEN	'اعمار جذري'
                WHEN 38 THEN	'لا يحتاج للترميم'
                WHEN 39 THEN	'بحاحة لإعمار جزئي'
            END AS habitable,
        
        CASE	opc.home_assessment
                WHEN 63 THEN	'جديد (1-10سنة)'
                WHEN 64 THEN	'متوسط (11-20سنة)'
                WHEN 65 THEN	'قديم (أكثر من 20سنة)'
                WHEN 459203 THEN	'متوسط (11-20سنة)'
                WHEN 459204 THEN	'جديد (1-10سنة)'
            END AS house_condition,
        
        opc.room_number as rooms,
        opc.house_area_in_meters as area,
        
        CASE WHEN opc.renovate_the_house = 1 THEN 1  Else 2  END  AS need_repair,
        ( SELECT constants.NAME FROM constants WHERE constants.id = description_of_home_status ) AS repair_notes,
        
        
        
        CASE WHEN opc.commercial_record = 1 THEN 1 Else 0 END AS has_commercial_record,
        CASE WHEN opc.commercial_record = 1 THEN opc.commercial_record_active_number Else 0 END AS active_commercial_record,
        CASE WHEN opc.commercial_record = 1 THEN opc.commercial_record_info Else ' ' END AS gov_commercial_record_details,
        
        
        
        opc.chf,
        opc.oxform,
        opc.wfp,
        opc.family_martyrs,
        opc.prisoners,
        opc.world_bank,
        opc.wounded,
        opc.permanent_unemployment,
        opc.social_affairs,
        opc.benefactor,
        opc.family_sponsorship,
        opc.guarantees_of_orphans,
        opc.neighborhood_committees,
        opc.Zakat,
        opc.agency_aid,
        
        
        opc.chf_sample,
        opc.oxform_sample,
        opc.wfp_sample,
        opc.family_martyrs_sample,
        opc.prisoners_sample,
        opc.world_bank_sample,
        opc.wounded_sample,
        opc.permanent_unemployment_sample,
        opc.social_affairs_sample,
        opc.benefactor_sample,
        opc.family_sponsorship_sample,
        opc.guarantees_of_orphans_sample,
        opc.neighborhood_committees_sample,
        opc.Zakat_sample,
        opc.agency_aid_sample,
        
        opc.lands,
        opc.cars,
        opc.property,
        opc.cattle,
        
        
        opc.doors,
        opc.safe_lighting,
        opc.gas_tube,
        opc.cobble,
        opc.tv,
        opc.fridge,
        opc.solarium,
        opc.watertank,
        opc.sweet_watertank,
        opc.closet,
        opc.heater,
        opc.windows,
        opc.electricity_network,
        opc.water_network,
        opc.electric_cooker,
        opc.stuff_kitchen,
        opc.gas,
        opc.washer,
        opc.mattresses,
        opc.plastic_chairs,
        opc.fan,
        opc.top_ceiling,
        opc.solarPower,
        
        opc.qualifiedRelationship,
        opc.abilityOfWorkReasonAnother,
        opc.wallsOfHouseAnother,
        
        opc.hasCHF,
        opc.hasOXFAM,
        opc.hasZakat,
        opc.hasGuaranteesOfOrphans,
        opc.hasFamilySponsorship,
        opc.hasAgencyAid,
        opc.hasNeighborhoodCommittees,
        opc.hasSocialAffairs,
        opc.hasPermanentUnemployment,
        opc.hasWounded,
        opc.hasWorldBank,
        opc.hasPrisoners,
        opc.hasFamilyMartyrs,
        opc.hasWFP,
        
        opc.zakatCount,
        opc.guaranteesOfOrphansCount,
        opc.familySponsorshipCount,
        opc.agencyAidCount,
        opc.neighborhoodCommitteesCount,
        opc.socialAffairsCount,
        opc.permanentUnemploymentCount,
        opc.woundedCount,
        opc.worldBankCount,
        opc.prisonersCount,
        opc.familyMartyrsCount,
        opc.WFPCount,
        opc.OXFAMCount,
        opc.CHFCount,
        opc.chfSampleCount,
        opc.oxformSampleCount,
        opc.wfpSampleCount,
        opc.familyMartyrsSampleCount,
        opc.prisonersSampleCount,
        opc.worldBankSampleCount,
        opc.woundedSampleCount,
        opc.permanentUnemploymentSampleCount,
        opc.socialAffairsSampleCount,
        opc.neighborhoodCommitteesSampleCount,
        opc.agencyAidSampleCount,
        opc.familySponsorshipSampleCount,
        opc.guaranteesOfOrphansSampleCount,
        opc.zakatSampleCount,
        
        opc.other1HelpType,
        opc.other2HelpType,
        opc.hasAnotherHelp,
        
        opc.other1HelpCount,
        opc.other1HelpSource,
        opc.other1HelpValue,
        opc.other2HelpValue,
        opc.other2HelpSource,
        opc.other2HelpCount,
        
        opc.bank1AccountNumber,
        opc.bank1AccountOwner,
        opc.bank1AccountBranch,
        opc.bank1Name,
        
        opc.bank2AccountNumber,
        opc.bank2AccountOwner,
        opc.bank2AccountBranch,
        opc.bank2Name,
        
        opc.hasCar,
        opc.hasCattle,
        opc.hasLands,
        opc.hasProperty,
        opc.hasQatarHelp,
        
        opc.qatarHelp,
        opc.qatarHelpCount,
        opc.qatarHelpSample,
        opc.qatarHelpSampleCount,
        
        opc.home_number,
        
        opc.created_at,
        opc.updated_at,
        
        opc.not_qualified_reason,
        opc.person_present,
        
        
        opc.active,
        opc.activeReason,
        opc.active_reason_hint,
        opc.manual,
        opc.case_status,
        opc.return_status,  
        opc.operation_case_returned_reason
        
        FROM charities
             RIGHT JOIN operation_cases as opc ON charities.id = opc.charity_id
             LEFT JOIN mosques ON opc.mosque_id = mosques.id
             LEFT JOIN squares ON opc.square_id = squares.id
             LEFT JOIN regoins ON opc.region_id = regoins.id
             INNER JOIN neighborhoods ON opc.neighborhood_id = neighborhoods.id
             LEFT JOIN researchers ON opc.researcher_id = researchers.id
             INNER JOIN governorates ON charities.governorate_id = governorates.id
        WHERE opc.reward_id =" .$reward_id."
        ORDER BY `opc`.`id` ASC";

    return $query;
}

?>
