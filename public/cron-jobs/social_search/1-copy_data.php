<?php

$HOST     = "localhost";

// ---------------------------------------baheth-----------------------------------------------  //
$baheth_NAME     = "bahethps_baheth_db";
$baheth_USER     = "bahethps_baheth_user";
$baheth_PASSWORD = "7.Uw1Q.H;LOk";
//$baheth_USER     = "root";
//$baheth_PASSWORD = "";
// create connection
$baheth = mysqli_connect($HOST,$baheth_USER,$baheth_PASSWORD,$baheth_NAME);
mysqli_query($baheth,"SET CHARACTER SET 'utf8'");
mysqli_query($baheth,"SET SESSION collation_connection ='utf8_unicode_ci'");

// ---------------------------------------takaful-----------------------------------------------  //
$takaful_NAME     = "takaful_db_sys";
$takaful_USER     = "takaful_sys_usr";
$takaful_PASSWORD = "At3{*6]PAS5U";
//$takaful_USER     = "root";
//$takaful_PASSWORD = "";

$takaful = mysqli_connect($HOST,$takaful_USER,$takaful_PASSWORD,$takaful_NAME);
mysqli_query($takaful,"SET CHARACTER SET 'utf8'");
mysqli_query($takaful,"SET SESSION collation_connection ='utf8_unicode_ci'");

// --------------------------------------------------------------------------------------  //

var_dump( import($baheth,$takaful));

// --------------------------------------------------------------------------------------  //

mysqli_close($baheth);
mysqli_close($takaful);

// --------------------------------------------------------------------------------------  //

function import($baheth,$takaful){

    ini_set('max_execution_time',0);
    set_time_limit(0);
    Ini_set('memory_limit','2048M');

//    (38,316,39,315)

    //***************************************************************************************************************************************//
    // update location tree
    mysqli_query($takaful,"TRUNCATE char_aids_locations_tree" );
    mysqli_query($takaful,"INSERT INTO `char_aids_locations_tree` ( `id`, `name` ) 
                                SELECT  `location_id`,trim(`name`) 
                                             FROM `char_aids_locations_i18n` 
                                             WHERE language_id = 1
                                             ORDER BY `location_id` ASC");

    mysqli_query($takaful,"UPDATE char_aids_locations_tree , char_aids_locations
                                     SET    char_aids_locations_tree.location_type = char_aids_locations.location_type ,
                                            char_aids_locations_tree.parent_id = char_aids_locations.parent_id
                                     WHERE  char_aids_locations_tree.id = char_aids_locations.id " );

    mysqli_query($takaful,"UPDATE char_aids_locations_tree , char_aids_locations_i18n
                                  SET    char_aids_locations_tree.parent_name = char_aids_locations_i18n.name
                                  WHERE  char_aids_locations_tree.parent_id = char_aids_locations_i18n.location_id and char_aids_locations_i18n.language_id = 1" );

  //  mysqli_query($takaful,"UPDATE `char_aids_locations_tree` SET name = normalize_text_ar(name) WHERE `location_type` > 1 ");

    //***************************************************************************************************************************************//
    $importedIds = [];
    $imported = mysqli_query($takaful,"SELECT reward_id FROM `char_social_search` WHERE `reward_id` is not null ");
    if($imported) {
        $total = $imported->num_rows;
        if ($total > 0) {
            while ($row = mysqli_fetch_assoc($imported)) {
                $importedIds[] = $row['reward_id'];
            }
        }
    }

//    WHERE `status` = 1
    $query = "SELECT * FROM `rewards` where id = 3";
//    if (sizeof($importedIds) > 0){
//        $str = implode(",", $importedIds);
//        $query = "SELECT * FROM `rewards` WHERE id not in ( ". $str ." )";
//    }

    $rewards = mysqli_query($baheth,$query);
    $saved_rewards =0;

    if($rewards) {
        $total = $rewards->num_rows;
        if ($total > 0) {
            while ($reward = mysqli_fetch_assoc($rewards)) {

                $data = rewardMap($reward);
                $data['reward_id'] = $reward['id'];

                $insert_keys = [];
                $insert_values = [];
                foreach ($data as $key => $value) {
                    $insert_keys []=  "`".$key."`";
                    $insert_values []=  "'".$value."'";
                }

                $insert_keys_ = join(",", $insert_keys);
                $insert_values_ = join(',', $insert_values);

                // copy from baheth rewards to  takaful social_search
                $query = "INSERT INTO `char_social_search` ( ".$insert_keys_. ") VALUES ( " .$insert_values_ . " ) ";
                mysqli_query($takaful,$query);

                $last_reward = mysqli_insert_id($takaful);
                // copy from baheth cases to  takaful social_search_cases
                $data['cases'] = copyCases($reward['id'],$last_reward,$baheth,$takaful);
                $saved_rewards++;
            }
        }
    }

    return $saved_rewards;
}

function rewardMap($data){

    $row = [ 'category_id'=>5 , 'organization_id'=>1 , 'user_id'=>1];

    $_translator = ["name"=>"title" ,"note"=> "notes" ,"status"=>"status" ,"close"=> "close",
        "start_date"=>"date_from" , "end_date"=> "date_to"];

    $dates = ["start_date","date_from","end_date","date_to"];

    foreach ($data as $k=>$v) {
        if(isset($_translator[$k])){
            if(in_array($_translator[$k],$dates)){
                if(!is_null($v) && $v != " "){
                    $row[$_translator[$k]]=date('Y-m-d',strtotime($v));
                }else{
                    $row[$_translator[$k]]=null;
                }
            }
            else{
                $row [$_translator[$k]]=$v;
            }
        }
    }

    $row['date'] = $row['date_from'] ;

    return $row;

}
// --------------------------------------------------------------------------------------  //
function caseKeys(){
    return [

        'operation_case_id'  ,
        'family_id' ,
        'id_card_number'   ,
        'card_type'   ,

        'fullname'   ,
        'fullname_en'   ,
        'first_name'   ,
        'second_name'   ,
        'third_name'   ,
        'last_name'   ,
        'prev_family_name'   ,

        'longitude'   ,
        'latitude'   ,

        'gender'    ,

        'marital_status_id'      ,
        'deserted'   ,

        'birthday'   ,
        'birth_place'   ,

        'death_date'   ,
        'death_cause_id'   ,

        'nationality' ,

        'family_cnt'   ,
        'spouses'   ,
        'male_live'   ,
        'female_live'   ,

        'refugee'   ,
        'unrwa_card_number'   ,

        'is_qualified'   ,
        'qualified_card_number'  ,

        'receivables'  ,
        'receivables_sk_amount'   ,
        'monthly_income'   ,
        'actual_monthly_income'   ,

        'has_commercial_records'   ,
        'active_commercial_records'   ,
        'gov_commercial_records_details' ,

        'phone'     ,
        'primary_mobile'      ,
        'secondary_mobile',
        'watanya'      ,

        'charity_id'   ,

        'country_id'   ,
        'district_id',
        'region_id'   ,
        'neighborhood_id',
        'square_id'   ,
        'mosque_id'   ,
        'street_address_details',
        'street_address',

        'needs' ,

        'promised'   ,
        'reconstructions_organization_name' ,

        'visitor' ,
        'visitor_card'   ,
        'visited_at'   ,
        'visitor_evaluation'   ,
        'visitor_opinion' ,
        'notes' ,

        'bank1AccountNumber' ,
        'bank1AccountOwner' ,
        'bank1AccountBranch' ,
        'bank1Name' ,

        'bank2AccountNumber' ,
        'bank2AccountOwner' ,
        'bank2AccountBranch' ,
        'bank2Name' ,

        'working'   ,
        'work_job_id' ,
        'work_status_id'   ,
        'work_wage_id'   ,
        'work_location' ,

        'can_work'   ,
        'work_reason_id' ,
        'abilityOfWorkReasonAnother'   ,

        'has_other_work_resources'   ,

        'property_type_id'   ,
        'roof_material_id'   ,
        'house_condition' ,
        'indoor_condition'   ,
        'residence_condition'   ,
        'area'   ,
        'rooms'   ,
        'rent_value'   ,
        'need_repair'   ,
        'habitable' ,

        'condition'   ,
        'details' ,
        'disease_id' ,

        'has_health_insurance'   ,
        'health_insurance_number'   ,
        'health_insurance_type'   ,

        'has_device'   ,
        'used_device_name'   ,
        'medical_report_summary'   ,

        'hasCHF'  ,
        'hasOXFAM'  ,
        'hasWFP'  ,
        'hasFamilyMartyrs'  ,
        'hasPrisoners'  ,
        'hasWorldBank'  ,
        'hasWounded'  ,
        'hasPermanentUnemployment'  ,
        'hasSocialAffairs'  ,
        'hasFamilySponsorship'  ,
        'hasGuaranteesOfOrphans'  ,
        'hasNeighborhoodCommittees'  ,
        'hasZakat'  ,
        'hasAgencyAid'  ,

        'hasQatarHelp'  ,

        'chf'   ,
        'oxform'   ,
        'wfp'   ,
        'family_martyrs'   ,
        'prisoners'   ,
        'world_bank'   ,
        'wounded'   ,
        'permanent_unemployment'   ,
        'social_affairs'   ,
        'benefactor'   ,
        'family_sponsorship'   ,
        'guarantees_of_orphans'   ,
        'neighborhood_committees'   ,
        'Zakat'   ,
        'agency_aid'   ,
        'qatarHelp'  ,

        'CHFCount' ,
        'OXFAMCount' ,
        'WFPCount' ,
        'familyMartyrsCount' ,
        'prisonersCount' ,
        'worldBankCount' ,
        'woundedCount' ,
        'permanentUnemploymentCount' ,
        'socialAffairsCount' ,
        'familySponsorshipCount' ,
        'guaranteesOfOrphansCount' ,
        'neighborhoodCommitteesCount' ,
        'zakatCount' ,
        'agencyAidCount' ,
        'qatarHelpCount'  ,

        'chf_sample'   ,
        'oxform_sample'   ,
        'wfp_sample'   ,
        'family_martyrs_sample'   ,
        'prisoners_sample'   ,
        'world_bank_sample'   ,
        'wounded_sample'   ,
        'permanent_unemployment_sample'   ,
        'social_affairs_sample'   ,
        'benefactor_sample'   ,
        'family_sponsorship_sample'   ,
        'guarantees_of_orphans_sample'   ,
        'neighborhood_committees_sample'   ,
        'Zakat_sample'   ,
        'agency_aid_sample'   ,
        'qatarHelpSample'  ,

        'chfSampleCount' ,
        'oxformSampleCount' ,
        'wfpSampleCount' ,
        'familyMartyrsSampleCount' ,
        'prisonersSampleCount' ,
        'worldBankSampleCount' ,
        'woundedSampleCount' ,
        'permanentUnemploymentSampleCount' ,
        'socialAffairsSampleCount'  ,
        'familySponsorshipSampleCount' ,
        'guaranteesOfOrphansSampleCount' ,
        'neighborhoodCommitteesSampleCount' ,
        'zakatSampleCount'  ,
        'agencyAidSampleCount'  ,
        'qatarHelpSampleCount'  ,

        'hasCar'  ,
        'cars'   ,

        'hasCattle'  ,
        'cattle'   ,

        'hasLands'  ,
        'lands'   ,

        'hasProperty'  ,
        'property'   ,

        'doors'   ,
        'safe_lighting'   ,
        'gas_tube'   ,
        'cobble'   ,
        'tv'   ,
        'fridge'   ,
        'solarium'   ,
        'watertank'   ,
        'sweet_watertank'   ,
        'closet'   ,
        'heater'   ,
        'windows'   ,
        'electricity_network'   ,
        'water_network'   ,
        'electric_cooker'   ,
        'stuff_kitchen'   ,
        'gas'   ,
        'washer'   ,
        'mattresses'   ,
        'plastic_chairs'   ,
        'fan'   ,
        'top_ceiling'   ,
        'solarPower'    ,

        'qualifiedRelationship'   ,
        'wallsOfHouseAnother'   ,

        'hasAnotherHelp'  ,
        'other1HelpSource' ,

        'other2HelpSource' ,

        'other1HelpValue'  ,
        'other2HelpValue'  ,

        'other1HelpType' ,
        'other2HelpType' ,

        'other1HelpCount'  ,
        'other2HelpCount'  ,


        'home_number'   ,
        'not_qualified_reason' ,
        'person_present' ,
        'status'  ,
        'reason'  ,

        'search_id' ,

        'created_at'   ,
        'updated_at'   ,
        'last_update_data'  ,
        'case_status'
    ] ;
}

function caseQuery($reward_id){

    $query = "SELECT
                operation_cases.id as operation_case_id,
                operation_cases.family_id,
                operation_cases.id_card_number,
                CASE	operation_cases.card_type WHEN 'رقم الهوية' THEN	1   WHEN 'بطاقة التعريف' THEN	2    WHEN 'جواز السفر' THEN	3
                END AS card_type,
                operation_cases.fullname,
                operation_cases.fullname_en,
                operation_cases.first_name,
                operation_cases.second_name,
                operation_cases.third_name,
                operation_cases.last_name,
                operation_cases.prev_family_name,
                operation_cases.longitude,
                operation_cases.latitude,
                CASE WHEN operation_cases.gender = 1 THEN 1  Else 2  END  AS gender,
                CASE	marital_status
                      WHEN 'أرمل' THEN	40  WHEN 'أرمل / ة' THEN	40
                      WHEN 'أعزب / آنسة' THEN	 10
                      WHEN 'متزوج' THEN	 20  WHEN 'متزوج / ة' THEN	 20
                      WHEN 'متعدد الزوجات' THEN	21
                      WHEN 'متوفى / ة' THEN	 4
                      WHEN 'مطلق' THEN	30  WHEN 'مطلق  / ة' THEN	30  WHEN 'مطلق / ة' THEN	30
                END AS marital_status_id,
                CASE WHEN operation_cases.deserted = 1 THEN 1  Else 0  END  AS deserted,
                birthday,
                (SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.birth_place ) AS birth_place,
                death_date,
                death_cause_id,
                (SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.nationality ) AS nationality,
                
                operation_cases.family_cnt,
                operation_cases.spouses,
                operation_cases.male_live ,
                operation_cases.female_live ,
                
                CASE WHEN operation_cases.refugee = 1 THEN 1  Else 2  END  AS refugee,
                CASE WHEN operation_cases.refugee = 2 THEN operation_cases.unrwa_card_number  Else null  END  AS unrwa_card_number,
                
                CASE WHEN operation_cases.is_qualified = 1 THEN 1  Else 0  END  AS is_qualified,
                CASE WHEN operation_cases.is_qualified = 1 THEN operation_cases.qualified_card_number  Else null END  AS qualified_card_number,
                
                CASE WHEN operation_cases.receivables = 1 THEN 1 Else 0 END AS receivables,
                CASE WHEN operation_cases.receivables = 1 THEN operation_cases.receivables_sk_amount Else 0 END AS receivables_sk_amount,
                operation_cases.monthly_income ,
                operation_cases.actual_monthly_income ,
                
                CASE WHEN operation_cases.commercial_record = 1 THEN 1 Else 0 END AS has_commercial_records,
                CASE WHEN operation_cases.commercial_record = 1 THEN operation_cases.commercial_record_active_number Else 0 END AS active_commercial_records,
                CASE WHEN operation_cases.commercial_record = 1 THEN operation_cases.commercial_record_info Else ' ' END AS gov_commercial_records_details,
                
                operation_cases.phone,
                operation_cases.primary_mobile,
                operation_cases.secondary_mobile,
                operation_cases.watanya,
                
                operation_cases.charity_id,
                
                'دولة فلسطين' AS country_id,
                governorates.name AS district_id,
                regoins.name AS region_id,
                neighborhoods.name AS neighborhood_id,
                squares.name AS square_id,
                mosques.name AS mosque_id ,
                operation_cases.street_address_details AS street_address_details,
                operation_cases.street_address AS street_address,
                
                operation_cases.needs,
                CASE  WHEN operation_cases.promised = 1 THEN 1 Else 2 END AS promised,
                operation_cases.reconstructions_organization_name,
                
                researchers.fullname AS visitor,
                researchers.id_number AS visitor_card,
                operation_cases.visited_at,
                CASE operation_cases.visitor_evaluation
                      WHEN 4 THEN	'ممتاز'
                      WHEN 3 THEN	'جيد جدا'
                      WHEN 2 THEN		'جيد'
                      WHEN 1 THEN	'سيء'
                      WHEN 0 THEN	'سيء جدا'
                END AS visitor_evaluation,
                
                operation_cases.visitor_opinion,
                operation_cases.researcher_recommendations as notes,
                
                bank1AccountNumber, bank1AccountOwner, bank1AccountBranch,
                (SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.bank1Name ) AS bank1Name,
                
                bank2AccountNumber, bank2AccountOwner, bank2AccountBranch,
                (SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.bank2Name ) AS bank2Name,
                
                CASE WHEN operation_cases.do_working = 1 THEN 1  Else 2  END  AS working,
                (SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.work_title ) AS work_job_id,
                (SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.work_status ) AS work_status_id,
                operation_cases.Income_by_category AS work_wage_id,
                operation_cases.work_space AS work_location,
                CASE WHEN operation_cases.ability_of_work = 1 THEN 1  Else 2  END  AS can_work,
                operation_cases.ability_of_work_reason AS work_reason_id,
                operation_cases.abilityOfWorkReasonAnother AS abilityOfWorkReasonAnother,
                operation_cases.does_have_source_of_income AS has_other_work_resources,
                
                CASE operation_cases.home_ownership
                      WHEN 0 THEN 'ايجار'WHEN 1 THEN 'ايجار'	WHEN 2 THEN 'ملك'
                      WHEN 3 THEN 'دون مقابل'	WHEN 4 THEN 'أراضي حكومية'	WHEN 5 THEN 'مستضاف'
                  END AS property_type_id,
                
                CASE operation_cases.walls_of_house
                     WHEN 942 THEN	'باطون جيد'		WHEN 956 THEN	'كرفان'
                     WHEN 943 THEN	'كرميد'	WHEN 944 THEN	'زينكو'
                     WHEN 459442 THEN	'باطون جيد'	WHEN 945 THEN	'خيمة'
                     WHEN 946 THEN	'أسبست'	WHEN 947 THEN	'بلاستيك'
                     WHEN 948 THEN	'أخرى'	WHEN 949 THEN	'صفيح جديد'
                     WHEN 459323 THEN	'أسبست'	WHEN 950 THEN	'صفيح قديم'
                     WHEN 459525 THEN	'باطون'	WHEN 951 THEN'باطون قديم'
                     WHEN 952 THEN'نصف بطون و نصف صفيح'	WHEN 953 THEN	'جزء باطون والاخر زينكو'
                     WHEN 954 THEN	'جزء باطون والاخر أسبست'	WHEN 459466 THEN'باطون قديم'
                     WHEN 955 THEN	'جزء زينكو والاخر أسبست'	Else Null
                END AS roof_material_id,
                
                CASE	operation_cases.home_assessment
                        WHEN 63 THEN	'جديد (1-10سنة)'
                          WHEN 64 THEN	'متوسط (11-20سنة)'
                          WHEN 65 THEN	'قديم (أكثر من 20سنة'
                          WHEN 459203 THEN	'متوسط (11-20سنة)'
                          WHEN 459204 THEN	'جديد (1-10سنة)'
                END AS house_condition,
                
                CASE operation_cases.placement_of_furniture
                        WHEN 1216 THEN	'سئ'	WHEN 1217 THEN	'سئ جدا'
                        WHEN 1218 THEN	'قديم'	WHEN 459441 THEN	'لا يوجد'
                        WHEN 459443 THEN'سئ'	WHEN 459040 THEN'سئ'
                        WHEN 459043 THEN'سئ جدا'	WHEN 34 THEN	'ممتاز'
                        WHEN 35 THEN'جيد جدا'	WHEN 459324 THEN'سئ'
                        WHEN 459465 THEN	'سئ جدا'	WHEN 1215 THEN	'جيد'
                END AS indoor_condition,
                
                CASE operation_cases.home_status
                        WHEN 62 THEN	'قديم'	WHEN 459397 THEN	'سيء جدا'
                        WHEN 459431 THEN	'سيء'	WHEN 56 THEN	'ممتاز'
                        WHEN 57 THEN	'جيد جدا'	WHEN 58 THEN		'جيد'
                        WHEN 459325 THEN	'سيء'	WHEN 59 THEN		'متوسط'
                        WHEN 60 THEN	'سيء'	WHEN 61 THEN	'سيء جدا'
                END AS residence_condition,
                
                operation_cases.house_area_in_meters as area,
                operation_cases.room_number as rooms,
                operation_cases.rent_value,
                
                CASE WHEN operation_cases.renovate_the_house = 1 THEN 1  Else 2  END  AS need_repair,
                ( SELECT constants.NAME FROM constants WHERE constants.id = description_of_home_status ) AS habitable,
                
                operation_cases.health_status as `condition`,
                ( SELECT constants.NAME FROM constants WHERE constants.id = operation_cases.disease ) AS disease_id,
                operation_cases.health_status_description AS details,
                
                CASE WHEN operation_cases.health_insurance = 1 THEN 1  Else 0  END  AS has_health_insurance,
                CASE WHEN operation_cases.health_insurance_number is null THEN ' ' Else operation_cases.health_insurance_number END   AS health_insurance_number,
                CASE WHEN operation_cases.health_insurance_type is null THEN ' ' Else ( SELECT constants.NAME FROM constants WHERE constants.id = operation_cases.health_insurance_type ) END   AS health_insurance_type,
                
                CASE WHEN operation_cases.device_use = 1 THEN 1 ELSE 0 END AS has_device,
                CASE WHEN operation_cases.device_use = 1 THEN operation_cases.device_use_name ELSE null END AS used_device_name,
                operation_cases.medical_report_summary,
                
                   hasCHF, hasOXFAM, hasWFP, hasFamilyMartyrs, hasPrisoners, hasWorldBank, hasWounded,
                   hasPermanentUnemployment, hasSocialAffairs, hasFamilySponsorship, hasGuaranteesOfOrphans,
                    hasNeighborhoodCommittees, hasZakat, hasAgencyAid, hasQatarHelp,
                
                   chf, oxform, wfp, family_martyrs, prisoners, world_bank, wounded, permanent_unemployment, social_affairs,
                   benefactor, family_sponsorship, guarantees_of_orphans, neighborhood_committees, Zakat,
                   agency_aid, qatarHelp,
                
                   CHFCount, OXFAMCount, WFPCount, familyMartyrsCount, prisonersCount, worldBankCount,
                   woundedCount, permanentUnemploymentCount, socialAffairsCount, familySponsorshipCount,
                   guaranteesOfOrphansCount, neighborhoodCommitteesCount, zakatCount, agencyAidCount,
                    qatarHelpCount,
                
                    chf_sample, oxform_sample, wfp_sample, family_martyrs_sample,
                     prisoners_sample, world_bank_sample, wounded_sample, permanent_unemployment_sample,
                      social_affairs_sample, benefactor_sample, family_sponsorship_sample,
                      guarantees_of_orphans_sample, neighborhood_committees_sample, Zakat_sample,
                      agency_aid_sample, qatarHelpSample,
                
                chfSampleCount, oxformSampleCount, wfpSampleCount, familyMartyrsSampleCount,
                 prisonersSampleCount, worldBankSampleCount, woundedSampleCount, permanentUnemploymentSampleCount,
                  socialAffairsSampleCount, familySponsorshipSampleCount, guaranteesOfOrphansSampleCount,
                   neighborhoodCommitteesSampleCount, zakatSampleCount, agencyAidSampleCount,
                   qatarHelpSampleCount,
                
                hasCar, cars, hasCattle, cattle, hasLands, lands, hasProperty, property,
                doors, safe_lighting, gas_tube, cobble, tv, fridge, solarium, watertank,
                 sweet_watertank, closet, heater, windows, electricity_network, water_network,
                  electric_cooker, stuff_kitchen, gas, washer, mattresses, plastic_chairs, fan, top_ceiling,
                   solarPower, qualifiedRelationship, wallsOfHouseAnother,
                    hasAnotherHelp, other1HelpSource, other2HelpSource, other1HelpValue,
                     other2HelpValue, other1HelpType, other2HelpType, other1HelpCount,
                     other2HelpCount, home_number, not_qualified_reason, person_present,
                     active as status, 
                     (SELECT constants.NAME FROM constants WHERE constants.id =operation_cases.activeReason  ) AS reason ,
                     reward_id as search_id,
                     operation_cases.created_at,
                operation_cases.updated_at, last_update_data, case_status
                FROM charities
                     RIGHT JOIN operation_cases ON charities.id = operation_cases.charity_id
                     LEFT JOIN mosques ON operation_cases.mosque_id = mosques.id
                     LEFT JOIN squares ON operation_cases.square_id = squares.id
                     LEFT JOIN regoins ON operation_cases.region_id = regoins.id
                     LEFT JOIN neighborhoods ON operation_cases.neighborhood_id = neighborhoods.id
                     LEFT JOIN researchers ON operation_cases.researcher_id = researchers.id
                     INNER JOIN governorates ON charities.governorate_id = governorates.id
                WHERE operation_cases.case_status LIKE 'CONFIRMED' and operation_cases.reward_id =" .$reward_id."
                ORDER BY `operation_cases`.`id` ASC";

    return $query;
}

function copyCases($old_reward_id , $new_reward_id,$baheth, $takaful){

    mysqli_query($takaful,"TRUNCATE rewards_cases" );

    $query = caseQuery($old_reward_id);
    $cases = mysqli_query($baheth,$query);

    $rows = [];
    $caseKeys = caseKeys();

    $caseKeys_ =[];
    foreach ($caseKeys as $key){
        $caseKeys_ []=  "`".$key."`";
    }

    $keys = join(",", $caseKeys_);
    while ($case = mysqli_fetch_assoc($cases)) {
        $case['search_id'] = $new_reward_id;

        $insert_values = [];
        foreach ($caseKeys as $key){
            if (!($case[$key] == NULL or $case[$key] == 'NULL')){
                if ($key == 'visited_at'){
                    $insert_values []=  "'".date('Y-m-d',strtotime($case[$key]))."'";
                }else{
                    $insert_values []=  "'".$case[$key]."'";
                }
            }else{
                $insert_values [] =  'NULL';
            }
        }

        $values = join(',', $insert_values);

        mysqli_query($takaful,"INSERT INTO `rewards_cases` ( ".$keys. ") VALUES ( " .$values . " ) " );
        $rows[]=$case;
    }

    return $rows;

}

// --------------------------------------------------------------------------------------  //

?>
