<?php

$HOST     = "localhost";

// ---------------------------------------baheth-----------------------------------------------  //
$baheth_NAME     = "bahethps_baheth_db";
$baheth_USER     = "bahethps_baheth_user";
$baheth_PASSWORD = "7.Uw1Q.H;LOk";

// create connection
$baheth = mysqli_connect($HOST,$baheth_USER,$baheth_PASSWORD,$baheth_NAME);
mysqli_query($baheth,"SET CHARACTER SET 'utf8'");
mysqli_query($baheth,"SET SESSION collation_connection ='utf8_unicode_ci'");

// ---------------------------------------takaful-----------------------------------------------  //
$takaful_NAME     = "takaful_db_sys";
$takaful_USER     = "takaful_sys_usr";
$takaful_PASSWORD = "At3{*6]PAS5U";

$takaful = mysqli_connect($HOST,$takaful_USER,$takaful_PASSWORD,$takaful_NAME);
mysqli_query($takaful,"SET CHARACTER SET 'utf8'");
mysqli_query($takaful,"SET SESSION collation_connection ='utf8_unicode_ci'");

var_dump( import($baheth,$takaful));

mysqli_close($baheth);
mysqli_close($takaful);

// --------------------------------------------------------------------------------------  //

function import($baheth,$takaful){

    ini_set('max_execution_time',0);
    set_time_limit(0);
    Ini_set('memory_limit','2048M');

    //***************************************************************************************************************************************//

    $query = "SELECT * FROM `rewards` where id = 5";

    $rewards = mysqli_query($baheth,$query);
    $saved =0;
    if($rewards) {
        $total = $rewards->num_rows;
        if ($total > 0) {
            while ($reward = mysqli_fetch_assoc($rewards)) {
                $data['family'] = copyMembers($reward['id'],$baheth,$takaful);
                $saved++;
            }
        }
    }

    return $saved;
}

// --------------------------------------------------------------------------------------  //
function memberKeys(){
    return [
            'id'  ,
            'id_card_number'     ,
            'family_id'     ,
            'operation_case_id'     ,
            'new_operation_case_id'     ,
            'father_id_card'     ,
            'full_name'       ,
            'first_name'       ,
            'second_name'       ,
            'third_name'       ,
            'last_name'       ,
            'gender'     ,
            'birthday'       ,
            'birth_place'       ,
            'monthly_income'       ,
            'marital_status_id'       ,
            'kinship_id'    ,
            'primary_mobile'     ,
            'study'   ,
            'study_type'       ,
            'school'       ,
            'degree'       ,
            'stage'       ,
            'grade',
            'working'       ,
            'work_job_id'       ,
            'condition'       ,
            'disease_id'     ,
            'details'     ,
            'prayer'  ,
            'prayer_reason'     ,
            'quran_center'   ,
            'quran_parts'   ,
            'quran_reason'     ,
            'created_at'    ,
            'updated_at'
        ] ;
}

function memberQuery($reward_id){

    $query = "SELECT
                `id`,
                `id_card_number`,
                `family_id`,
                NULL AS `new_operation_case_id` ,
                `operation_case_id`,
                `father_id_card`,
                `full_name`,
                NULL AS `first_name` ,
                NULL AS `second_name`,
                NULL AS `third_name` ,
                NULL AS `last_name` ,
                CASE WHEN gender = 1 THEN 1  Else 2  END  AS gender,
                `birthday`,
                (SELECT constants.NAME FROM constants WHERE constants.id =birth_place ) AS birth_place,
                `monthly_income`,
                CASE	marital_status
                        WHEN 'أرمل' THEN	40  WHEN 'أرمل / ة' THEN	40
                        WHEN 'أعزب / آنسة' THEN	 10
                        WHEN 'متزوج' THEN	 20  WHEN 'متزوج / ة' THEN	 20
                        WHEN 'متعدد الزوجات' THEN	21
                        WHEN 'متوفى / ة' THEN	 4
                        WHEN 'مطلق' THEN	30  WHEN 'مطلق  / ة' THEN	30  WHEN 'مطلق / ة' THEN	30
                END AS marital_status_id,
                `relation` as kinship_id,
                mobileNumber AS primary_mobile ,
                `study`,
                `type_of_study` as study_type,
                CONCAT(ifnull(name_educational_institution, ' '), ' ' ,ifnull(name_educational_institution, ' ')) AS school,
                `degree`,
                `educational_level` as stage,
                `class` as grade,
                CASE WHEN do_working = 1 THEN 1  Else 2  END  AS working,
                (SELECT constants.NAME FROM constants WHERE constants.id =job_title ) AS work_job_id,
                `health_status` as `condition`,
                `disease` as `disease_id`,
                `health_status_description` as details,
                CASE WHEN doPraying = 1 THEN 0  Else 1  END  AS prayer,
                CASE WHEN doPraying = 1 THEN 'NULL'  Else prayingLeaveReason  END  AS prayer_reason,   
                CASE WHEN mosqueMemberShip = 1 THEN 1  Else 0  END  AS quran_center,
                `quranParts` AS quran_parts,
                `quranLeaveReason`  AS quran_reason,
                `created_at`,
                `updated_at`
                FROM `operation_member_cases`
                WHERE  operation_case_id in ( SELECT operation_cases.id
                                              FROM charities
                                              RIGHT JOIN operation_cases ON charities.id = operation_cases.charity_id
                                              INNER JOIN governorates ON charities.governorate_id = governorates.id
                                              WHERE operation_cases.case_status LIKE 'CONFIRMED' and operation_cases.reward_id =  " .$reward_id." )
                ORDER BY `operation_member_cases`.`id` ASC";

    return $query;
}

function copyMembers($old_reward_id,$baheth, $takaful){

//    mysqli_query($takaful,"TRUNCATE rewards_cases_members" );

    $query = memberQuery($old_reward_id);
    $members = mysqli_query($baheth,$query);

    $rows = [];
    $memberKeys = memberKeys();

    $memberKeys_ =[];
    foreach ($memberKeys as $key){
        $memberKeys_ []=  "`".$key."`";
    }

    $keys = join(",", $memberKeys_);
    while ($member = mysqli_fetch_assoc($members)) {
        $insert_values = [];
        foreach ($memberKeys as $key){
            if (!($member[$key] == NULL or $member[$key] == 'NULL')){
                $insert_values []=  "'".$member[$key]."'";
            }else{
                $insert_values [] =  'NULL';
            }
        }

        $values = join(',', $insert_values);
        mysqli_query($takaful,"INSERT INTO `rewards_cases_members` ( ".$keys. ") VALUES ( " .$values . " ) " );
        $rows[]=$member;
    }

    //fixedMember($takaful);

    return $rows;

}

function fixedMember($connection){

    mysqli_query($connection,"UPDATE rewards_cases_members set full_name = REPLACE(full_name, 'عبد ال', 'عبدال') where full_name like  '%عبد ال%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set full_name = REPLACE(full_name, 'ابو ال', 'ابوال') where full_name like  '%ابو ال%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set full_name = REPLACE(full_name, 'أبو ال', 'أبوال') where full_name like  '%أبو ال%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set full_name = REPLACE(full_name, 'ابو ', 'ابو') where full_name like  '%ابو %'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set full_name = REPLACE(full_name, 'أبو ', 'أبو') where full_name like  '%أبو %'") ;

    mysqli_query($connection,"UPDATE rewards_cases_members set first_name =  SUBSTRING_INDEX(SUBSTRING_INDEX(full_name, ' ', 1), ' ', -1) ,
                                                                     second_name =  SUBSTRING_INDEX(SUBSTRING_INDEX(full_name, ' ', 2), ' ', -1),
                                                                     third_name =  SUBSTRING_INDEX(SUBSTRING_INDEX(full_name, ' ', 3), ' ', -1),
                                                                     last_name =  SUBSTRING_INDEX(SUBSTRING_INDEX(full_name, ' ', 4), ' ', -1)
                                                          where 1 ") ;

    mysqli_query($connection,"UPDATE rewards_cases_members set full_name = REPLACE(full_name, 'عبدال', 'عبد ال') where full_name like  '%عبدال%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set full_name = REPLACE(full_name, 'ابوال', 'ابو ال') where full_name like  '%ابوال%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set full_name = REPLACE(full_name, 'أبوال', 'أبو ال') where full_name like  '%أبوال%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set full_name = REPLACE(full_name, 'ابو', 'ابو ') where full_name like  '%ابو%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set full_name = REPLACE(full_name, 'أبو', 'أبو ') where full_name like  '%أبو%'") ;

    mysqli_query($connection,"UPDATE rewards_cases_members set first_name = REPLACE(first_name, 'عبدال', 'عبد ال') where first_name like  '%عبدال%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set first_name = REPLACE(first_name, 'ابوال', 'ابو ال') where first_name like  '%ابوال%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set first_name = REPLACE(first_name, 'أبوال', 'أبو ال') where first_name like  '%أبوال%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set first_name = REPLACE(first_name, 'ابو', 'ابو ') where first_name like  '%ابو%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set first_name = REPLACE(first_name, 'أبو', 'أبو ') where first_name like  '%أبو%'") ;

    mysqli_query($connection,"UPDATE rewards_cases_members set second_name = REPLACE(second_name, 'عبدال', 'عبد ال') where second_name like  '%عبدال%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set second_name = REPLACE(second_name, 'ابوال', 'ابو ال') where second_name like  '%ابوال%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set second_name = REPLACE(second_name, 'أبوال', 'أبو ال') where second_name like  '%أبوال%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set second_name = REPLACE(second_name, 'ابو', 'ابو ') where second_name like  '%ابو%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set second_name = REPLACE(second_name, 'أبو', 'أبو ') where second_name like  '%أبو%'") ;

    mysqli_query($connection,"UPDATE rewards_cases_members set third_name = REPLACE(third_name, 'عبدال', 'عبد ال') where third_name like  '%عبدال%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set third_name = REPLACE(third_name, 'ابوال', 'ابو ال') where third_name like  '%ابوال%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set third_name = REPLACE(third_name, 'أبوال', 'أبو ال') where third_name like  '%أبوال%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set third_name = REPLACE(third_name, 'ابو', 'ابو ') where third_name like  '%ابو%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set third_name = REPLACE(third_name, 'أبو', 'أبو ') where third_name like  '%أبو%'") ;

    mysqli_query($connection,"UPDATE rewards_cases_members set last_name = REPLACE(last_name, 'عبدال', 'عبد ال') where last_name like  '%عبدال%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set last_name = REPLACE(last_name, 'ابوال', 'ابو ال') where last_name like  '%ابوال%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set last_name = REPLACE(last_name, 'أبوال', 'أبو ال') where last_name like  '%أبوال%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set last_name = REPLACE(last_name, 'ابو', 'ابو ') where last_name like  '%ابو%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set last_name = REPLACE(last_name, 'أبو', 'أبو ') where last_name like  '%أبو%'") ;

    mysqli_query($connection,"UPDATE rewards_cases_members set full_name = REPLACE(full_name, '  ', ' ') where full_name like  '%  %'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set first_name = REPLACE(first_name, '  ', ' ') where first_name like  '%  %'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set second_name = REPLACE(second_name,'  ', ' ') where second_name like  '%  %'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set third_name = REPLACE(third_name, '  ', ' ') where third_name like  '%  %'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set last_name = REPLACE(last_name, '  ', ' ') where last_name like  '%  %'") ;

    mysqli_query($connection,"UPDATE rewards_cases_members set birthday = REPLACE(birthday, '/', '-') where 1") ;

    mysqli_query($connection,"UPDATE rewards_cases_members set primary_mobile = NULL where primary_mobile = '0'") ;

    mysqli_query($connection,"UPDATE rewards_cases_members set `condition` = NULL where `condition` = ''") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set `condition` = NULL where `condition` = ' '") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set `condition` = 'جيدة' where `condition` = NULL") ;

    mysqli_query($connection,"UPDATE rewards_cases_members set `condition`  = 1 where `condition` like  '%جيدة%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set `condition`  = 2 where `condition` like  '%مرض مزمن%'") ;
    mysqli_query($connection,"UPDATE rewards_cases_members set `condition`  = 3 where `condition` like  '%ذوي احتياجات خاصة%'") ;

    //    -- birth_place
    mysqli_query($connection,"UPDATE rewards_cases_members,char_locations_i18n
                                    set    rewards_cases_members.birth_place = char_locations_i18n.location_id
                                    where rewards_cases_members.birth_place = char_locations_i18n.name and char_locations_i18n.language_id = 1 ") ;

    //-- work_job
    mysqli_query($connection,"UPDATE rewards_cases_members,char_work_jobs_i18n
                                    set    rewards_cases_members.work_job_id  = char_work_jobs_i18n.work_job_id
                                    where  rewards_cases_members.work_job_id  = char_work_jobs_i18n.name and char_work_jobs_i18n.language_id = 1  ") ;

    //-- stage
    mysqli_query($connection,"UPDATE rewards_cases_members,char_edu_stages_i18n
                                    set    rewards_cases_members.stage  = char_edu_stages_i18n.edu_stage_id
                                    where  rewards_cases_members.stage  = char_edu_stages_i18n.name and char_edu_stages_i18n.language_id = 1  ") ;

    //-- degree
    mysqli_query($connection,"UPDATE rewards_cases_members,char_edu_degrees_i18n
                                    set    rewards_cases_members.degree  = char_edu_degrees_i18n.edu_degree_id
                                    where  rewards_cases_members.degree  = char_edu_degrees_i18n.name and char_edu_degrees_i18n.language_id = 1  ") ;

    //-- degree
    mysqli_query($connection,"UPDATE rewards_cases_members,char_kinship_i18n
                                    set    rewards_cases_members.kinship_id  = char_kinship_i18n.kinship_id
                                    where  rewards_cases_members.kinship_id  = char_kinship_i18n.name and char_kinship_i18n.language_id = 1  ") ;

    $keys = ['birth_place','disease_id','work_job_id','stage','degree' , 'kinship_id'];

    foreach ($keys as $key){
        mysqli_query($connection,"UPDATE rewards_cases_members  set " . $key ." = NULL
                                 WHERE " . $key ." NOT REGEXP '^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$'") ;
    }


    //-- set new_operation_case_id
    mysqli_query($connection,"UPDATE rewards_cases_members,char_social_search_cases
                                    set    rewards_cases_members.new_operation_case_id  = char_social_search_cases.id
                                    where  rewards_cases_members.operation_case_id  = char_social_search_cases.operation_case_id ") ;

}

?>
