<?php

$HOST     = "localhost";

// ---------------------------------------takaful-----------------------------------------------  //
$takaful_NAME     = "takaful_db_sys";
$takaful_USER     = "takaful_sys_usr";
$takaful_PASSWORD = "At3{*6]PAS5U";
//$takaful_USER     = "root";
//$takaful_PASSWORD = "";

$takaful = mysqli_connect($HOST,$takaful_USER,$takaful_PASSWORD,$takaful_NAME);
mysqli_query($takaful,"SET CHARACTER SET 'utf8'");
mysqli_query($takaful,"SET SESSION collation_connection ='utf8_unicode_ci'");

// --------------------------------------------------------------------------------------  //

var_dump( import($takaful));

// --------------------------------------------------------------------------------------  //

mysqli_close($takaful);

// --------------------------------------------------------------------------------------  //
function import($takaful){

    ini_set('max_execution_time',0);
    set_time_limit(0);
    Ini_set('memory_limit','2048M');

    //***************************************************************************************************************************************//

    $current_cases = mysqli_query($takaful,"SELECT * FROM rewards_cases");

    $saved_rewards =[];

    if($current_cases) {
        $total = $current_cases->num_rows;
        if ($total > 0) {
            while ($case = mysqli_fetch_assoc($current_cases)) {
                saveCase($takaful,$case);
                $saved_rewards++;
            }
        }
    }

    return $saved_rewards;
}

// --------------------------------------------------------------------------------------  //

function insertKeys($type){

    $keys = [];

    if ($type == 'basic'){
        $keys =[
            'search_id',
            'operation_case_id',

            'id_card_number',
            'card_type',

            'first_name',
            'second_name',
            'third_name',
            'last_name',

            'prev_family_name',

            'gender',
            'marital_status_id',

            'deserted',

            'birthday',
            'birth_place',
            'nationality',

            'death_date',
            'death_cause_id',

            'family_cnt',
            'spouses',
            'male_live',
            'female_live',

            'phone',
            'primary_mobile',
            'secondary_mobile',
            'watanya',

            'country_id',
            'district_id',
            'region_id',
            'neighborhood_id',
            'square_id',
            'mosque_id',
            'street_address',

            'longitude',
            'latitude',

            'status',
            'reason',
        ];
    }
    else if ($type == 'details'){
        $keys =[
//            'search_cases_id',
            'refugee',
            'unrwa_card_number',
            'is_qualified',
            'qualified_card_number',
            'receivables',
            'receivables_sk_amount',
            'monthly_income',
            'actual_monthly_income',
            'visitor',
            'visitor_card',
            'visited_at',
            'visitor_evaluation',
            'notes',
            'visitor_opinion',
            'needs',
            'promised',
            'reconstructions_organization_name'
        ];
    }
    else if ($type == 'health') {
        $keys = [
//            'search_cases_id',
            'condition',
            'disease_id',
            'details',
            'has_health_insurance',
            'health_insurance_type',
            'health_insurance_number',
            'has_device',
            'used_device_name'
        ];
    }
    else if ($type == 'work') {
        $keys = [
//            'search_cases_id',
            'working',
            'work_status_id',
            'work_job_id',
            'work_wage_id',
            'work_location',
            'can_work',
            'work_reason_id',
            'has_other_work_resources'
        ];
    }
    else if ($type == 'residence') {
        $keys = [
//            'search_cases_id',
            'property_type_id',
            'rent_value',
            'roof_material_id',
            'residence_condition',
            'indoor_condition',
            'habitable',
            'house_condition',
            'rooms',
            'area',
            'need_repair',
//            'habitable'
        ];
    }

    return $keys;
}

function saveCase($takaful,$data){

    // save social_search_case basic data
    $basicKeys = insertKeys('basic');

    $basicValues = [];
    $basicKeys_ = [];
    foreach ($basicKeys as $key){
        $basicKeys_ []=  "`".$key."`";
        if (!($data[$key] == NULL or $data[$key] == 'NULL')){
            $basicValues []=  "'".$data[$key]."'";
        }else{
            $basicValues [] =  'NULL';
        }
    }

    $keys = join(",", $basicKeys_);
    $values = join(',', $basicValues);
     mysqli_query($takaful,"INSERT INTO `char_social_search_cases` ( ".$keys. ") VALUES ( " .$values . " ) " );
    $search_cases_id = mysqli_insert_id($takaful);
    $data['search_cases_id'] = $search_cases_id;

    // save social_search_case other details data
    $caseDetailsTable = ['details' => 'char_social_search_cases_details' ,
                         'work'  => 'char_social_search_cases_work',
                         'health'  => 'char_social_search_cases_health',
                         'residence' => 'char_social_search_cases_residence'];

    $caseDetailsKeys = ['details' ,'work' , 'health' , 'residence'];

    foreach ($caseDetailsKeys as $key){

        $dataValues = [];
        $dataKeys_ = [];

        $dataKeys_ []=  "`search_cases_id`";
        $dataValues[]=$search_cases_id;

        $dataKeys = insertKeys($key);

        foreach ($dataKeys as $k){
            $dataKeys_ []=  "`".$k."`";
            if (!($data[$k] == NULL or $data[$k] == 'NULL')){
                $dataValues []=  "'".$data[$k]."'";
            }else{
                $dataValues [] =  'NULL';
            }
        }

        $keys = join(",", $dataKeys_);
        $values = join(',', $dataValues);
        mysqli_query($takaful,"INSERT INTO `".$caseDetailsTable[$key]. "` ( ".$keys. ") VALUES ( " .$values . " ) " );
    }

    // insert may rows related banks ----------------------------------------------------------------------------------------

    if (!is_null($data['bank1Name'])){

        $bank_id = is_null($data['bank1Name'])? 'NULL' : (int) $data['bank1Name'] ;
        $account_number = is_null($data['bank1AccountNumber'])? ' ' : (int) $data['bank1AccountNumber'] ;
        $account_owner = is_null($data['bank1AccountOwner'])? ' ' : (int) $data['bank1AccountOwner'] ;
        $branch_name = is_null($data['bank1AccountBranch'])? 'NULL' : (int) $data['bank1AccountBranch'] ;

        $bank_query = "INSERT INTO `char_social_search_cases_banks`(`search_cases_id`, `bank_id`, `account_number`, `account_owner`, `branch_name`) VALUES ( ".
            $search_cases_id . "," . $bank_id . "," . $account_number . "," . $account_owner . "," . $branch_name .")";

        mysqli_query($takaful, $bank_query );

    }
    if (!is_null($data['bank2Name'])){

        $bank_id = is_null($data['bank2Name'])? 'NULL' : (int) $data['bank2Name'] ;
        $account_number = is_null($data['bank2AccountNumber'])? ' ' : (int) $data['bank2AccountNumber'] ;
        $account_owner = is_null($data['bank2AccountOwner'])? ' ' : (int) $data['bank2AccountOwner'] ;
        $branch_name = is_null($data['bank2AccountBranch'])? 'NULL' : (int) $data['bank2AccountBranch'] ;

        $bank_query = "INSERT INTO `char_social_search_cases_banks`(`search_cases_id`, `bank_id`, `account_number`, `account_owner`, `branch_name`) VALUES ( ".
            $search_cases_id . "," . $bank_id . "," . $account_number . "," . $account_owner . "," . $branch_name .")";

        mysqli_query($takaful, $bank_query );
    }

    // insert may rows related essentials ----------------------------------------------------------------------------------------
    $essentials =[
        "doors" => 25,
        "safe_lighting" => 26,
        "gas_tube" => 10,
        "cobble" => 7,
        "tv" => 31,
        "fridge" => 11,
        "solarium" =>3 ,
        "watertank" =>4 ,
        "sweet_watertank" =>28 ,
        "closet" => 17,
        "heater" => 14,
        "windows" => 5,
        "electricity_network" => 1,
        "water_network" => 2,
        "electric_cooker" => 13,
        "stuff_kitchen" => 8,
        "gas" => 9,
        "washer" => 12,
        "mattresses" => 18,
        "plastic_chairs" => 24,
        "fan" => 15 ,
        "top_ceiling" => 27,
        "solarPower" =>32,
    ];
    foreach ($essentials  as $key=>$essential_id){
        $condition = is_null($data[$key])? 0 : (int) $data[$key] ;
        $essential_query = "INSERT INTO `char_social_search_cases_essentials`(`search_cases_id`, `essential_id`, `condition`, `needs`) VALUES ( ".
            $search_cases_id . "," . $essential_id . "," . $condition . ", NULL )";
        mysqli_query($takaful, $essential_query );
    }

    // insert may rows related properties ----------------------------------------------------------------------------------------

    //  property_id = 1
    $hasCar = is_null($data['hasCar'])? 0 : (int) $data['hasCar'] ;
    $cars = is_null($data['cars'])? 0 : (int) $data['cars'] ;
    $car_query = "INSERT INTO `char_social_search_cases_properties`(`search_cases_id`, `property_id`, `has_property`, `quantity`, `notes`) VALUES ( ".
                              $search_cases_id . ", 1 ," . $hasCar . "," . $cars . ", 'NULL' )";
    mysqli_query($takaful, $car_query );

    //  property_id = 2
    $hasCattle = is_null($data['hasCattle'])? 0 : (int) $data['hasCattle'] ;
    $cattle = is_null($data['cattle'])? 0 : (int) $data['cattle'] ;
    $cattle_query = "INSERT INTO `char_social_search_cases_properties`(`search_cases_id`, `property_id`, `has_property`, `quantity`, `notes`) VALUES ( ".
                              $search_cases_id . ", 2 ," . $hasCattle . "," . $cattle . ", 'NULL' )";
    mysqli_query($takaful, $cattle_query );

    //  property_id = 3
    $hasProperty = is_null($data['hasProperty'])? 0 : (int) $data['hasProperty'] ;
    $property = is_null($data['property'])? 0 : (int) $data['property'] ;

    $cattle_query = "INSERT INTO `char_social_search_cases_properties`(`search_cases_id`, `property_id`, `has_property`, `quantity`, `notes`) VALUES ( ".
        $search_cases_id . ", 13 ," . $hasProperty . "," . $property . ", 'NULL' )";
    mysqli_query($takaful, $cattle_query );

    //  property_id = 4
    $hasLands = is_null($data['hasLands'])? 0 : (int) $data['hasLands'] ;
    $lands = is_null($data['lands'])? 0 : (int) $data['lands'] ;

    $cattle_query = "INSERT INTO `char_social_search_cases_properties`(`search_cases_id`, `property_id`, `has_property`, `quantity`, `notes`) VALUES ( ".
        $search_cases_id . ", 4 ," . $hasLands . "," . $lands . ", 'NULL' )";
    mysqli_query($takaful, $cattle_query );


    // insert may rows related aids ----------------------------------------------------------------------------------------------

//    aid_type == 1 نقدية
    $aid_value = is_null($data['chf'])? 0 : (int) $data['chf'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
                           $search_cases_id . ", 1 ,1 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['oxform'])? 0 : (int) $data['oxform'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 2 ,1 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['wfp'])? 0 : (int) $data['wfp'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 3 ,1 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['family_sponsorship'])? 0 : (int) $data['family_sponsorship'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 4 ,1 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['family_martyrs'])? 0 : (int) $data['family_martyrs'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 5 ,1 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['prisoners'])? 0 : (int) $data['prisoners'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 6 ,1 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['world_bank'])? 0 : (int) $data['world_bank'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 7 ,1 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['wounded'])? 0 : (int) $data['wounded'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 8 ,1 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['permanent_unemployment'])? 0 : (int) $data['permanent_unemployment'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 9 ,1 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['social_affairs'])? 0 : (int) $data['social_affairs'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 10 ,1 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['agency_aid'])? 0 : (int) $data['agency_aid'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 11 ,1 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['neighborhood_committees'])? 0 : (int) $data['neighborhood_committees'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 12 ,1 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['guarantees_of_orphans'])? 0 : (int) $data['guarantees_of_orphans'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 13 ,1 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['benefactor'])? 0 : (int) $data['benefactor'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 14 ,1 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['Zakat'])? 0 : (int) $data['Zakat'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 15 ,1 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['qatarHelp'])? 0 : (int) $data['qatarHelp'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 17 ,1 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    //    aid_type == 2 عينية
    $aid_value = is_null($data['chf_sample'])? 0 : (int) $data['chf_sample'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 1 ,2 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['oxform_sample'])? 0 : (int) $data['oxform_sample'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 2 ,2 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['wfp_sample'])? 0 : (int) $data['wfp_sample'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 3 ,2 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['family_sponsorship_sample'])? 0 : (int) $data['family_sponsorship_sample'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 4 ,2 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['family_martyrs_sample'])? 0 : (int) $data['family_martyrs_sample'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 5 ,2 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['prisoners_sample'])? 0 : (int) $data['prisoners_sample'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 6 ,2 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['world_bank_sample'])? 0 : (int) $data['world_bank_sample'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 7 ,2 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['wounded_sample'])? 0 : (int) $data['wounded_sample'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 8 ,2 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['permanent_unemployment_sample'])? 0 : (int) $data['permanent_unemployment_sample'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 9 ,2 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['social_affairs_sample'])? 0 : (int) $data['social_affairs_sample'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 10 ,2 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['agency_aid_sample'])? 0 : (int) $data['agency_aid_sample'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 11 ,2 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['neighborhood_committees_sample'])? 0 : (int) $data['neighborhood_committees_sample'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 12 ,2 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['guarantees_of_orphans_sample'])? 0 : (int) $data['guarantees_of_orphans_sample'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 13 ,2 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['benefactor_sample'])? 0 : (int) $data['benefactor_sample'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 14 ,2 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['Zakat_sample'])? 0 : (int) $data['Zakat_sample'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 15 ,2 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );

    $aid_value = is_null($data['qatarHelpSample'])? 0 : (int) $data['qatarHelpSample'] ;
    $aid_take = $aid_value > 0 ? 0 : 1 ;
    $aid_query = "INSERT INTO `char_social_search_cases_aids`
                         (`search_cases_id`, `aid_source_id`, `aid_type`, `aid_take`, `aid_value`, `currency_id`) VALUES ( ".
        $search_cases_id . ", 17 ,2 ," . $aid_take . "," . $aid_value . ", 4 )";
    mysqli_query($takaful, $aid_query );


}

// --------------------------------------------------------------------------------------  //

?>
