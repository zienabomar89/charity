<?php

$DB_HOST     = "localhost";
$DB_USER     = "takaful_sys_usr";
$DB_PASSWORD = "At3{*6]PAS5U";
$DB_NAME     = "takaful_db_sys";

// CREATE DB CONNECTION
$connection = mysqli_connect($DB_HOST,$DB_USER,$DB_PASSWORD,$DB_NAME);

mysqli_query($connection,"SET CHARACTER SET 'utf8'");
mysqli_query($connection,"SET SESSION collation_connection ='utf8_unicode_ci'");

var_dump( cronJob($connection));

mysqli_close($connection);

function cronJob($conn){

    ini_set('max_execution_time', 0);
    set_time_limit(0);
    Ini_set('memory_limit','2048M');

    mysqli_query($conn,"SET CHARACTER SET 'utf8'");
    mysqli_query($conn,"SET SESSION collation_connection ='utf8_unicode_ci'");

    $char_persons = "SELECT  char_persons.id, char_persons.first_name,char_persons.second_name,char_persons.third_name,char_persons.last_name, 
                                     prev_family_name, id_card_number,card_type,gender,marital_status_id,birthday,death_date,
                                     father_id, mother_id,spouses, family_cnt,male_live,female_live ,
                                     i18n.language_id ,i18n.first_name as en_first_name ,i18n.second_name  as en_second_name ,
                                     i18n.third_name as en_third_name ,i18n.last_name  as en_last_name 
                             FROM char_persons
                             left join char_persons_i18n i18n on i18n.person_id = char_persons.id and i18n.language_id = 2
                             where notes is null and id not IN (SELECT distinct person_id FROM char_cases)
                            LIMIT 0, 5000";//order by id limit 2";


    $result_data = mysqli_query($conn,$char_persons);
    $person_update=[];
    $total= 0;

    if($result_data){
        $total = $result_data->num_rows;
        if ($total > 0) {
            while ($record = mysqli_fetch_assoc($result_data)){
                $card = $record['id_card_number'];
                $person_id = $record['id'];
                $marital_status_id = $record['marital_status_id'];
                if(checkCard($card)) {
                    $row = getCitizenInfo($card);
                    if(!is_null($row)) {
                        $map = getPersonMap($row);
                        $en = ['en_first_name' , 'en_second_name' ,'en_third_name','en_last_name'];
                        $language_id = $record['language_id'];

                        if(is_null($language_id)){
                            $I18n=['en_first_name' => NULL , 'en_second_name'  => NULL ,'en_third_name' => NULL ,'en_last_name' => NULL ];

                        }else{
                            $I18n=['en_first_name' => $record['en_first_name'] , 'en_second_name'=> $record['en_second_name'] ,
                                'en_third_name' => $record['en_third_name'] , 'en_last_name' => $record['en_last_name']  ];
                        }

                        foreach ($en as $ino){
                            if ($map[$ino] != $record[$ino]) {
                                if(isset($map[$ino])){
                                    $I18n[$ino] =$map[$ino];
                                }
                            }
                        }

                        if(sizeof($I18n) == 4){
                            $PersonI18n ="REPLACE INTO `char_persons_i18n` 
                                                              (`person_id`,`language_id`,`first_name`,`second_name`,`third_name`,`last_name`)
                                                               VALUES
                                                              (".$person_id.",2,'".$I18n['en_first_name']."','". $I18n['en_second_name']."','". $I18n['en_third_name']."','". $I18n['en_last_name']."')";

                            mysqli_query($conn,$PersonI18n);
                        }

                        $update_query = "";
                        $to_update = array("prev_family_name", "marital_status_id", "birthday", "death_date");

                        foreach ($to_update as $key) {
                            if (isset($map[$key])) {
                                if ($map[$key] != $record[$key]) {
                                    if ($update_query != "")
                                        $update_query .= ', ';

                                    $update_query .= $key . "='" . $map[$key] . "'";
                                }
                            }
                        }

                        if($marital_status_id == 10){
                            $spouses = 0 ;
                            $female_live = 0 ;
                            $male_live = 0 ;

                            $family_cnt = ($male_live + $female_live + $spouses) + 1;

                            if($record['family_cnt']  != $family_cnt  ||  $record['spouses']  != $spouses ||
                                $record['female_live'] != $female_live ||  $record['male_live'] != $male_live ){
                                if ($update_query != ""){
                                    $update_query .=  ", family_cnt ='".$family_cnt."'" .", spouses ='".$spouses."'" .", female_live ='".$female_live."'".", male_live ='".$male_live."'";
                                }else{
                                    $update_query .=  ", family_cnt ='".$family_cnt."'" .", spouses ='".$spouses."'" .", female_live ='".$female_live."'".", male_live ='".$male_live."'";
                                }
                            }
                        }

                        if ($update_query != ""){
                            $update_query .= ', ';
                        }

                        $update_query .=  "notes ='updated'";
                        $update_query .= ', ';
                        $update_query .=  "updated_at ='".date("Y-m-d H:i:s")."'";
                        $person = "UPDATE char_persons SET " . $update_query . " WHERE id=".$person_id;
                        mysqli_query($conn, $person);

                        $person_update[] = $person_id;
                    }
                }
            }
        }
    }
    return ['total'=>$total , 'updated'=>$person_update];
}

function checkCard($card)
{
    if (strlen($card) == 9) {
        $identity = $card;
        $lastDig = $identity[8];
        $digit1 = $identity[0] * 1;
        $digit2 = $identity[1] * 2;
        $digit3 = $identity[2] * 1;
        $digit4 = $identity[3] * 2;
        $digit5 = $identity[4] * 1;
        $digit6 = $identity[5] * 2;
        $digit7 = $identity[6] * 1;
        $digit8 = $identity[7] * 2;
        $checkLast = (int)($digit1) + (int)($digit3) + (int)($digit5) + (int)($digit7);

        $digit2 = (string) $digit2;
        if (strlen($digit2) == 2) {
            $digit22 = (string) $digit2;
            $checkLast += (int)($digit22[0]);
            $checkLast += (int)($digit22[1]);
        } else {
            $checkLast += (int)($digit2);
        }

        $digit4 = (string) $digit4;
        if (strlen($digit4) == 2) {
            $digit44 = (string) $digit4;
            $checkLast += (int)($digit44[0]);
            $checkLast += (int)($digit44[1]);
        } else {
            $checkLast += (int)($digit4);
        }

        $digit6 = (string) $digit6;
        if (strlen($digit6) == 2) {
            $digit66 = (string) $digit6;
            $checkLast += (int)($digit66[0]);
            $checkLast += (int)($digit66[1]);
        } else {
            $checkLast += (int)($digit6);
        }

        $digit8 = (string) $digit8;
        if (strlen($digit8) == 2) {
            $digit88 = (string) $digit8;
            $checkLast += (int)($digit88[0]);
            $checkLast += (int)($digit88[1]);
        } else {
            $checkLast += (int)($digit8);
        }

        $checkLast = (string)$checkLast;
        $checkLast_length = strlen($checkLast);

        if($checkLast[$checkLast_length - 1]==0)
            $lastOne=10;
        else
            $lastOne =  $checkLast[$checkLast_length - 1];

        $lastDigit_inCheck = 10 - $lastOne;

        if ($lastDig != $lastDigit_inCheck) {
            return false;
        }
        return true;

    }

    return false;
}

function GetCitizenInfo($card){

    $result = null;
    $jsonData = [
        'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
        'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
        'DATA_IN' => [
            "package" => "MOI_GENERAL_PKG",
            "procedure" => "CITZN_INFO",
            "ID" => $card
        ],
        'WB_AUDIT_IN' => [
            "ip" => "10.12.0.32",
            "pc" => "hima-pc",
        ]
    ];
    $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";

    $jsonDataEncoded = json_encode($jsonData);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    $response = curl_exec($ch);

    if (!FALSE === $response) {
        $response = json_decode($response);
        $row = $response->DATA;
        if ($response->DATA && @count($response->DATA)) {
            $result = $row[0] ;
        }
    }
    return $result;
}

function getPersonMap($data){

    $row = array();
    $_translator =array(
        "IDNO" =>  "id_card_number",
        "FNAME_ARB" =>  "first_name",
        "SNAME_ARB" =>  "second_name",
        "TNAME_ARB" => "third_name",
        "LNAME_ARB" =>  "last_name",
        "PREV_LNAME_ARB" =>  "prev_family_name",
        "ENG_NAME" =>  "en_name",
        "SEX_CD" => "gender",
        "SOCIAL_STATUS_CD" =>  "marital_status_id",
        "BIRTH_DT" =>  "birthday",
        "DETH_DT" =>  "death_date",
        "MOBILE" =>  "primary_mobile",
        "TEL" =>  "phone",
    );

    $dates = ['birthday' , 'death_date'];
    foreach ($data as $k=>$v) {

        if(isset($_translator[$k])){
            if($k == 'ENG_NAME'){
                $parts = explode(" ",$v);
                $row['en_first_name'] = $parts[0];
                $row['en_second_name'] = $parts[1];
                $row['en_third_name'] = $parts[2];
                $row['en_last_name'] = $parts[3];
            }
            elseif(in_array($_translator[$k],$dates)){
                if(!is_null($v) && $v != " "){
                    $row[$_translator[$k]]=date('Y-m-d',strtotime(str_replace("/","-",$v)));
                }
            }
            else{
                $row [$_translator[$k]]=$v;
            }
        }
    }
    return $row;}

?>
