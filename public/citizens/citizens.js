
var CitizensApp = angular.module("CitizensApp", ["ui.router", "ui.bootstrap", "oc.lazyLoad", "ngSanitize",
    "ngResource", "ngBootbox", "ngCookies",
    'angularUtils.directives.dirPagination', "pascalprecht.translate",
    'yaru22.angular-timeago', "ngTagsInput", "ng.ckeditor", 'btorfs.multiselect']);

CitizensApp.factory('Citizens', ['$resource', function ($resource) {
    return $resource('/api/v1.0/citizens/' + ':operation/:id', {id: '@id'}, {
            check: {method: 'POST', params: {operation: 'check'}},
            voucher: {method: 'POST', params: {operation: 'voucher'}},
            // save: {method: 'POST', params: {operation: 'check'}},
        });
    }]);

CitizensApp.factory('Entity', function ($resource) {
    return $resource('/api/v1.0/citizensConst/:entity/:parent_id/:action/:id', {entity: '@entity', parent_id: '@parent_id', id: '@id', action: '@action'}, {
        query: {
            method: 'GET',
            isArray: false
        },
        update: {
            method: 'PUT'
        },
        translations: {
            method: 'GET',
            isArray: true,
            url: "/api/v1.0/citizensConst/:entity/:id/translations"
        },
        saveTranslations: {
            method: 'POST',
            isArray: true,
            url: "/api/v1.0/citizensConst/:entity/:id/translations"
        },
        list: {
            method: 'GET',
            isArray: true,
            url: "/api/v1.0/citizensConst/:entity/list"
        },
        listChildren: {
            method: 'GET',
            isArray: true,
            // :parent The parent entity, :id The parent ID, :entity The child entity
            url: "/api/v1.0/citizensConst/:parent/:id/:entity/list"
        }
    });
});

CitizensApp.config(function ($translateProvider) {
    $translateProvider.useSanitizeValueStrategy(null);
    $translateProvider.preferredLanguage('ar');
    $translateProvider.useStaticFilesLoader({files: [{    prefix: '/citizens/i18n/', suffix: '.json'}]});
});




/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
CitizensApp.config(['$ocLazyLoadProvider','$httpProvider', '$ngBootboxConfigProvider', function ($ocLazyLoadProvider, $httpProvider, $ngBootboxConfigProvider) {

    $ngBootboxConfigProvider.addLocale('ar', {OK: 'موافق', CANCEL: 'الغاء الأمر', CONFIRM: 'موافق'});
}]);

//AngularJS v1.3.x workaround for old style controller declarition in HTML
CitizensApp.config(['$controllerProvider', function ($controllerProvider) {
    // this option might be handy for migrating old apps, but please don't use it
    // in new ones!
    $controllerProvider.allowGlobals();
}]);


/* Setup global settings */
CitizensApp.factory('settings', ['$rootScope', function ($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: false, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        assetsPath: '/themes/metronic',
        globalPath: '/themes/metronic/global',
        layoutPath: '/themes/metronic/layouts/layout3',
        StoragePath: '/storage/app/'
    };

    $rootScope.settings = settings;

    return settings;
}]);



CitizensApp.directive('numericOnly', [function(){
    return {
        restrict: 'A',
        link: function (scope, element, attrs, ctrl) {
            element.on('cut copy paste', function (e) {
                return true;
            });
            element.on('keydown', function (event) {
                var ew =event.which;
                if (ew == 64 || ew == 16) {
                    return false;
                }
                if (((event.keyCode == 65 || event.keyCode == 86 || event.keyCode == 67|| event.keyCode == 88) &&
                    (event.ctrlKey === true || event.metaKey === true)) ||
                    (event.keyCode >= 35 && event.keyCode <= 40) ||
                    ([8, 13, 27, 37, 38, 39, 40, 110,9].indexOf(ew) > -1) ||(ew >= 48 && ew <= 57) || (ew >= 96 && ew <= 105) ) {
                    return true;
                } else {
                    event.preventDefault();
                    return false;
                }
            });
        }
    }
}]);

/* Setup App Main Controller */
CitizensApp.controller('AppController', ['$scope', '$rootScope', '$http', function($scope,$rootScope,$http) {
    $rootScope.do=false;
    $scope.$on('$viewContentLoaded', function() {
        App.initComponents(); // init core components
        //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive
    });

}]);

/* Setup Rounting For All Pages */
CitizensApp.config(['$stateProvider', '$httpProvider', '$urlRouterProvider', function ($stateProvider, $httpProvider, $urlRouterProvider) {
    // Redirect any unmatched url

    $urlRouterProvider.otherwise("/login.html");
    $stateProvider
    // Dashboard
        .state('login', {
            url: "/login.html",
            templateUrl: "/citizens/views/login.html",
            data: {pageTitle: 'الصفحة الرئيسية'},
            controller: "loginController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CitizensApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            '/citizens/js/loginController.js'
                        ]
                    });
                }]
            }
        })

        .state('request', {
            url: "/request.html",
            templateUrl: "/citizens/views/request.html",
            data: {pageTitle: 'تقديم الطلبات'},
            controller: "RequestController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CitizensApp',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/citizens/js/RequestController.js'
                        ]
                    });
                }]
            }
        })

        .state('check', {
            url: "/check/:token",
            templateUrl: "/citizens/views/check.html",
            params: {token:null},
            data: {pageTitle: 'فحص تلقي مساعدة'},
            controller: "CheckController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'CitizensApp',
                        files: [
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            '/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js' ,
                            '/citizens/js/CheckController.js'
                        ]
                    });
                }]
            }
        });

}]);

