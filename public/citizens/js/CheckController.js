angular.module('CitizensApp').controller("CheckController",function ($rootScope,$scope, Citizens, $filter ,Entity,$state, $stateParams) {

    $state.current.data.pageTitle = 'فحص تلقي مساعدة ';
    $rootScope.pageTitle = 'فحص تلقي مساعدة';

    var d = new Date();
    $rootScope.year = d.getFullYear();

    $scope.row = {};
    $scope.validate ={'status':null,'msg':null};
    $scope.details = null;
    $scope.value = 0;
    $scope.shekel_value = 0;

    $scope.clearToast =function () {
        angular.element('.btn').removeClass("disabled");
        angular.element('.btn').removeAttr('disabled');
        toastr.remove();
        toastr.clear();
    };
    $scope.toastMessages =function (type,message) {
        angular.element('.btn').removeClass("disabled");
        angular.element('.btn').removeAttr('disabled');
        toastr.remove();
        toastr.options = {
            tapToDismiss: false
            , timeOut: 0
            , extendedTimeOut: 0
            , allowHtml: true
            , preventDuplicates: false
            , preventOpenDuplicates: false
            , newestOnTop: true
            , closeButton: true
            , closeHtml: '<button><i class="icon-off pull-right"></i></button>'
        };
        switch(type) {
            case 'error':
                toastr.error(message);
                break;
            case 'warning':
                toastr.warning(message);
                break;
            default:
                toastr.success(message);
        }
        // toastr.clear();
    };

    $scope.check = function (row) {
        $scope.details = null;
        $scope.value = 0;
        $scope.shekel_value = 0;
        $scope.validate ={'status':null,'msg':null};
        $scope.clearToast();
        var params = angular.copy(row);

        if( !angular.isUndefined(params.birthday)) {
            params.birthday=$filter('date')(params.birthday, 'dd-MM-yyyy')
        }
        params.token = $stateParams.token;
        angular.element('.btn').addClass("disabled");
        Citizens.voucher(params,function (response) {
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if(response.status=='failed_valid') {
                $scope.toastMessages ('error','الرجاء إدخال كافة الحقول المطلوبة');
                $scope.validate ={'status':response.status,'msg':response.msg};
            }
            else if(response.status == 'false' || response.status == false){
                $scope.toastMessages ('error',response.msg);
                if(response.redirect){
                    $state.go('login');
                }
            }else{
                response.person.name = response.person.FNAME_ARB + " " +  response.person.SNAME_ARB + " " + response.person.TNAME_ARB + ' ' +  response.person.LNAME_ARB;
                $scope.person = response.person;
                $scope.details = response.details;
                $scope.value = response.value;
                $scope.shekel_value = response.shekel_value;
            }
        });

    };

    // *************** DATE PIKER ***************
    $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0};
    $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $rootScope.format = $scope.formats[0];
    $rootScope.today = function() {$rootScope.dt = new Date();};
    $rootScope.today();
    $rootScope.clear = function() {$rootScope.dt = null;};
    $scope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup1.opened = true;};
    $scope.popup1 = {opened: false};

    $scope.clearToast();

});
