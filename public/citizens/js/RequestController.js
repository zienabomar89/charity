angular.module('CitizensApp').controller("RequestController",function ($rootScope,$scope, Citizens, $filter ,Entity,$state) {

    $state.current.data.pageTitle = 'تقديم الطلبات ';
    $rootScope.pageTitle = 'تقديم الطلبات';
    $scope.row = {};
    $scope.person = {};
    $scope.can_make_request = false;
    $scope.request = {type:'',notes:'' , date : $filter('date')(new Date(), 'dd-MM-yyyy') , approve:false};
    $scope.validate ={'status':null,'msg':null};

    var d = new Date();
    $rootScope.year = d.getFullYear();

    Entity.get({entity:'entities',c:'aidCountries,workReasons,workStatus,workWages,workJobs,diseases'},function (response) {
        $scope.countries = response.aidCountries;
    });

    $scope.getLocation = function(entity,parent,value){
        if(!angular.isUndefined(value)) {
            if (value !== null && value !== "" && value !== " ") {
                Entity.listChildren({'entity': entity, 'id': value, 'parent': parent}, function (response) {

                    if (entity == 'sdistricts') {
                        if (!angular.isUndefined($scope.validate.msgadscountry_id)) {
                            $scope.validate.msgadscountry_id = [];
                        }

                        $scope.governarate = response;
                        $scope.regions = [];
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];

                        $scope.request.adsdistrict_id = "";
                        $scope.request.adsregion_id = "";
                        $scope.request.adsneighborhood_id = "";
                        $scope.request.adssquare_id = "";
                        $scope.request.adsmosques_id = "";

                    }
                    else if (entity == 'sregions') {
                        if (!angular.isUndefined($scope.validate.msgadsdistrict_id)) {
                            $scope.validate.msgadsdistrict_id = [];
                        }

                        $scope.regions = response;
                        $scope.nearlocation = [];
                        $scope.squares = [];
                        $scope.mosques = [];
                        $scope.request.adsregion_id = "";
                        $scope.request.adsneighborhood_id = "";
                        $scope.request.adssquare_id = "";
                        $scope.request.adsmosques_id = "";
                    }
                    else if (entity == 'sneighborhoods') {
                        if (!angular.isUndefined($scope.validate.msgadsregion_id)) {
                            $scope.validate.msgadsregion_id = [];
                        }
                        $scope.nearlocation = response;
                        $scope.squares = [];
                        $scope.mosques = [];
                        $scope.request.adsneighborhood_id = "";
                        $scope.request.adssquare_id = "";
                        $scope.request.adsmosques_id = "";

                    }
                    else if (entity == 'ssquares') {
                        if (!angular.isUndefined($scope.validate.msgadssquare_id)) {
                            $scope.validate.msgcity = [];
                        }
                        $scope.squares = response;
                        $scope.mosques = [];
                        $scope.request.adssquare_id = "";
                        $scope.request.adsmosques_id = "";
                    }
                    else if (entity == 'smosques') {
                        if (!angular.isUndefined($scope.validate.msgadssquare_id)) {
                            $scope.validate.msgadssquare_id = [];
                        }
                        $scope.mosques = response;
                        $scope.request.adsmosques_id = "";
                    }
                });
            }
        }
    };

    $scope.clearToast =function () {
        angular.element('.btn').removeClass("disabled");
        angular.element('.btn').removeAttr('disabled');
        toastr.remove();
        toastr.clear();
    };
    $scope.toastMessages =function (type,message) {
        angular.element('.btn').removeClass("disabled");
        angular.element('.btn').removeAttr('disabled');
        toastr.remove();
        toastr.options = {
            tapToDismiss: false
            , timeOut: 0
            , extendedTimeOut: 0
            , allowHtml: true
            , preventDuplicates: false
            , preventOpenDuplicates: false
            , newestOnTop: true
            , closeButton: true
            , closeHtml: '<button><i class="icon-off pull-right"></i></button>'
        };
        switch(type) {
            case 'error':
                toastr.error(message);
                break;
            case 'warning':
                toastr.warning(message);
                break;
            default:
                toastr.success(message);
        }
        // toastr.clear();
    };
    $scope.check = function (row) {
        $scope.can_make_request = false;
        $scope.clearToast();
        var params = angular.copy(row);

        if( !angular.isUndefined(params.birthday)) {
            params.birthday=$filter('date')(params.birthday, 'dd-MM-yyyy')
        }
        angular.element('.btn').addClass("disabled");
        Citizens.check(params,function (response) {
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if(response.status=='failed_valid') {
                $scope.toastMessages ('error','الرجاء إدخال كافة الحقول المطلوبة');
                $scope.validate ={'status':response.status,'msg':response.msg};
            }
            else if(response.status == 'false' || response.status == false){
                $scope.toastMessages ('error',response.msg);
            }else{
                response.person.name = response.person.FNAME_ARB + " " +  response.person.SNAME_ARB + " " + response.person.TNAME_ARB + ' ' +  response.person.LNAME_ARB;
                $scope.person = response.person;
                $scope.request = {id_card_number :response.person.IDNO , type:'',notes:'' , date : $filter('date')(new Date(), 'yyyy-MM-dd'), approve:false};
                $scope.can_make_request = true;
            }
        });

    };

    $scope.save = function (row) {
        $scope.clearToast();

        var validate = $scope.req_from.$valid;
        var params = angular.copy(row);
        params.pass = 'valid';

        if(validate == true || validate == 'true'){
            var approve = $scope.request.approve ;
            if(approve == false || approve == 'false'){
                params.pass = 'invalid';
                $scope.toastMessages ('error','الرجاء التأكيد على ان كافة المعلومات المدخلة في الطلب حقيقية');
                return ;
            }
        }else{
            $scope.toastMessages ('error','الرجاء إدخال كافة الحقول المطلوبة');
        }

        if( !angular.isUndefined($scope.row.birthday)) {
            params.birthday=$filter('date')($scope.row.birthday, 'dd-MM-yyyy')
        }
        angular.element('.btn').addClass("disabled");
        Citizens.save(params,function (response) {
            angular.element('.btn').removeClass("disabled"); angular.element('.btn').removeAttr('disabled');
            if(response.status=='failed_valid') {
                $scope.toastMessages ('error','الرجاء إدخال كافة الحقول المطلوبة');
                $scope.validate ={'status':response.status,'msg':response.msg};
            }
            else if(response.status == 'false' || response.status == false){
                $scope.toastMessages ('error',response.msg);
            }else{
                $scope.toastMessages ('success',response.msg);
                $state.go('login');
            }
        });

    };

    // *************** DATE PIKER ***************
    $rootScope.dateOptions = {formatYear: 'yy', startingDay: 0};
    $rootScope.formats = ['dd-MM-yyyy','dd-MMMM-yyyy', 'dd.MM.yyyy', 'shortDate'];
    $rootScope.format = $scope.formats[0];
    $rootScope.today = function() {$rootScope.dt = new Date();};
    $rootScope.today();
    $rootScope.clear = function() {$rootScope.dt = null;};
    $scope.open1 = function($event) {$event.preventDefault();$event.stopPropagation();$scope.popup1.opened = true;};
    $scope.popup1 = {opened: false};
    $scope.clearToast();
});
