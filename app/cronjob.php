<?php
    $DB_HOST     = "localhost";
    $DB_USER     = "takaful_sys_usr";
    $DB_PASSWORD = "At3{*6]PAS5U";
    $DB_NAME     = "takaful_db_sys";

    //  mysql_query("SET character_set_results = 'utf8', character_set_client = 'utf8',
    //  character_set_connection = 'utf8', character_set_database = 'utf8',
    //  character_set_server = 'utf8'", $this->links);
    //
    // CREATE DB CONNECTION
    $connection = mysqli_connect($DB_HOST,$DB_USER,$DB_PASSWORD,$DB_NAME);

    mysqli_query($connection,"SET CHARACTER SET 'utf8'");
    mysqli_query($connection,"SET SESSION collation_connection ='utf8_unicode_ci'");

    cronJob($connection);

    mysqli_close($connection);

    function cronJob($conn){

        mysqli_query($conn,"SET CHARACTER SET 'utf8'");
        mysqli_query($conn,"SET SESSION collation_connection ='utf8_unicode_ci'");

        $char_persons = "SELECT  id,id_card_number,gender FROM char_persons where id = 1";//order by id limit 2";
/*      $char_persons = "SELECT id,id_card_number,gender
                         FROM char_persons
                         where id IN (SELECT distinct person_id
                                      FROM char_cases
                                      where char_cases.organization_id < 150)
                         limit 500";//order by id limit 2";
*/

        $result_data = mysqli_query($conn,$char_persons);
        if($result_data){
            $total = $result_data->num_rows;
            if ($total > 0) {

                $father_kinship_id = null;
                $father_kinship_query = mysqli_query($conn,"SELECT value FROM char_settings where id ='father_kinship' ");
                if($father_kinship_query){
                    $out =  mysqli_fetch_assoc($father_kinship_query);
                    if(!is_null($out['value']) and $out['value'] != "") {
                        $father_kinship_id =$out['value'];
                    }
                }

                $mother_kinship_id = null;
                $mother_kinship_query = mysqli_query($conn,"SELECT value FROM char_settings where id ='mother_kinship' ");
                if($mother_kinship_query){
                    $out =  mysqli_fetch_assoc($mother_kinship_query);
                    if(!is_null($out['value']) and $out['value'] != "") {
                        $mother_kinship_id =$out['value'];
                    }
                }

                $husband_kinship_id = null;
                $husband_kinship_query = mysqli_query($conn,"SELECT value FROM char_settings where id ='husband_kinship' ");
                if($husband_kinship_query){
                    $out =  mysqli_fetch_assoc($husband_kinship_query);
                    if(!is_null($out['value']) and $out['value'] != "") {
                        $husband_kinship_id =$out['value'];
                    }
                }

                $wife_kinship_id = null;
                $wife_kinship_query = mysqli_query($conn,"SELECT value FROM char_settings where id ='wife_kinship' ");
                if($wife_kinship_query){
                    $out =  mysqli_fetch_assoc($wife_kinship_query);
                    if(!is_null($out['value']) and $out['value'] != "") {
                        $wife_kinship_id =$out['value'];
                    }
                }

                $daughter_kinship_id = null;
                $daughter_kinship_query = mysqli_query($conn,"SELECT value FROM char_settings where id ='daughter_kinship' ");
                if($daughter_kinship_query){
                    $out =  mysqli_fetch_assoc($daughter_kinship_query);
                    if(!is_null($out['value']) and $out['value'] != "") {
                        $daughter_kinship_id =$out['value'];
                    }
                }

                $son_kinship_id = null;
                $son_kinship_query = mysqli_query($conn,"SELECT value FROM char_settings where id ='son_kinship' ");
                if($son_kinship_query){
                    $out =  mysqli_fetch_assoc($son_kinship_query);
                    if(!is_null($out['value']) and $out['value'] != "") {
                        $son_kinship_id =$out['value'];
                    }
                }

                while ($row1 = mysqli_fetch_assoc($result_data)){
                    $card = $row1['id_card_number'];
                    $person_id = $row1['id'];
                    $gender = $row1['gender'];
                    if(checkCard($card)) {
                        $father_id = null;
                        $mother_id = null;
                        $row = getCitizenInfo($card);
                        if($row['status'] == true) {
                            $map = getPersonMap($row['row']);
                            $gender = $row1['gender'];
                            $update_query = "";
                            $to_update = array("prev_family_name", "marital_status_id", "birthday", "death_date");
                            $utf8 = ['first_name', 'second_name', 'third_name', 'last_name', 'prev_family_name'];
                            foreach ($to_update as $key) {
                                if (isset($map[$key])) {
                                    if ($update_query != "")
                                        $update_query .= ', ';

                                    if (in_array($key, $utf8)) {
//                                        $insert[$var] =$input[$var];
//                                        $ut8_ = iconv("UTF-8", "ISO-8859-1", $map[$key]);
//                                        $update_query .= $key . "='" . $ut8_ . "'";
                                        $update_query .= $key . "='" . $map[$key] . "'";
                                    } else {
                                        $update_query .= $key . "='" . $map[$key] . "'";
                                    }
                                }
                            }

                            $spouses = 0 ;
                            $female_live = 0 ;
                            $male_live = 0 ;

                            $members = getAllPersonRelations($card);
                            foreach ($members as $member){

                                $map = inputsMap($member);
                                $is_mother = false;
                                $is_father = false;

                                $increment = true;
                                if(isset( $map['death_date'])){
                                    $increment = false;
                                }

                                if($map['kinship'] == 286 && $map['gender'] == 1){
                                    if($gender == 1){
                                        $map['father_id']= $person_id;
                                    }else{
                                        $map['mother_id']= $person_id;
                                    }
                                    $map['kinship_id']= $son_kinship_id;
                                    $map['l_person_id']= $person_id;
                                    $map['l_person_update']= true;
                                    if($increment && $map['marital_status_id'] == 10 ) $male_live++;
                                } elseif($map['kinship'] == 286 && $map['gender'] == 2){
                                    $map['kinship_id']= $daughter_kinship_id;
                                    $map['l_person_id']= $person_id;
                                    $map['l_person_update']= true;
                                    if($gender == 1){
                                        $map['father_id']= $person_id;
                                    }else{
                                        $map['mother_id']= $person_id;
                                    }
                                    if($increment && $map['marital_status_id'] == 10 ) $female_live++;
                                } elseif($map['kinship'] == 287 && $map['gender'] == 1){
                                    $map['kinship_id']= $husband_kinship_id;
                                    $map['l_person_id']= $person_id;
                                    $map['l_person_update']= true;
                                    if($increment) $spouses++;
                                } elseif($map['kinship'] == 287 && $map['gender'] == 2){
                                    $map['kinship_id']= $wife_kinship_id;
                                    $map['l_person_id']= $person_id;
                                    $map['l_person_update']= true;
                                    if($increment) $spouses++;
                                } elseif($map['kinship'] == 284){
                                    $is_father = true;
                                    $map['kinship_id']= $father_kinship_id;
                                    $map['l_person_id']= $person_id;
                                    $map['l_person_update']= true;
                                }elseif($map['kinship'] == 285){
                                    $is_mother = true;
                                    $map['kinship_id']= $mother_kinship_id;
                                    $map['l_person_id']= $person_id;
                                    $map['l_person_update']= true;
                                }

                                $map['update_en']= true;
                                unset($map['kinship']);
                                $record = savePerson($map, $conn);

                                if($is_father){
                                    $father_id = $record['id'];
                                }

                                if($is_mother){
                                    $mother_id = $record['id'];
                                }
                            }

                            if(!is_null($father_id)){
                                $update_query .=" father_id='" . $father_id . "'";

                            }
                            if(!is_null($mother_id)){
                                $update_query .=" mother_id='" . $mother_id . "'";
                            }

                            $family_cnt = ($male_live + $female_live + $spouses) + 1;
                            if ($update_query != "")
                                $update_query .=  ",";

                            $update_query .=  "family_cnt ='".$family_cnt."'" .", spouses ='".$spouses."'" .", female_live ='".$female_live."'".", male_live ='".$male_live."'";
                            $update_query .= ', ';
                            $update_query .=  "updated_at ='".date("Y-m-d H:i:s")."'";

                            $person = "UPDATE char_persons SET " . $update_query . " WHERE id=".$person_id;
                            mysqli_query($conn, $person);

                            // ******************************************************************************************* //
                            // Citizen Name I18n
                            $en = ['en_first_name' , 'en_second_name' ,'en_third_name','en_last_name'];
                            $I18n=[];
                            foreach ($en as $ino){
                                if(isset($map[$ino])){
                                    $I18n[$ino] =$map[$ino];
                                }
                            }

                            if(sizeof($I18n) == 4){
                                $PersonI18n ="REPLACE INTO `char_persons_i18n` 
                                                  (`person_id`,`language_id`,`first_name`,`second_name`,`third_name`,`last_name`)
                                                   VALUES
                                                  (".$person_id.",2,'".$I18n['en_first_name']."','". $I18n['en_second_name']."','". $I18n['en_third_name']."','". $I18n['en_last_name']."')";

                                mysqli_query($conn,$PersonI18n);
                            }

                            // ******************************************************************************************* //
                            // Citizen Health Info
                            /*
                            $health_info = getPersonHealthInfo($card);
                            $health_sql = "SELECT * from char_persons_health  where person_id =".$person_id;
                            $health_result = mysqli_query($conn, $health_sql);
                            $person_health = mysqli_fetch_array($health_result) ;

                            $details = $person_health?$person_health['details']:NULL;
                            $condition = $person_health?$person_health['condition']:1;

                            if($health_info['status'] != false){
                                $health_details = $health_info['row'];
                                $details = $health_details->LOC_NAME_AR.' - '.$health_details->MR_DIAGNOSIS_AR;
                                $condition = ($health_details->MR_DIAGNOSIS_AR || $health_details->LOC_NAME_AR) ? 2 : 1 ;
                            }

                            $person_health_ ="REPLACE INTO `char_persons_health`  (`person_id`, `condition`, `details`)
                                              VALUES
                                              (".$person_id.",".$condition.",'". $details."')";

                            mysqli_query($conn, $person_health_);
                            */
                            // ******************************************************************************************* //
                            // Citizen Work Info
                            /*
                            $employment_data = getPersonFinancialInfo($card);
                            $work_sql = "SELECT * from char_persons_work  where person_id =".$person_id;
                            $work_result = mysqli_query($conn, $work_sql);
                            $person_work = mysqli_fetch_array($work_result) ;

                            $can_work = 'NULL';
                            $work_reason_id = 'NULL';
                            $work_status_id ='NULL';
                            $work_job_id = 'NULL';
                            $work_wage_id = 'NULL';
                            $work_location = '';
                            $working = 2;

                            if($person_work != 0) {
                                if(!is_null($person_work['can_work'])){
                                    $can_work = $person_work['can_work'];
                                }

                                if(!is_null($person_work['can_work'])){
                                    $work_reason_id = $person_work['work_reason_id'];
                                }
                            }
                            if($employment_data['status'] != false){
                                $employment=$employment_data['row'];
                                $working = 1;
                                $work_location = $employment->MINISTRY_NAME.' - '.$employment->EMP_STATE_DESC;

                                if($person_work != 0) {
                                    $work_status_id = ( !is_null($person_work['work_status_id']) ) ? 'NULL' : $person_work['work_status_id'] ;
                                    $work_job_id = ( !is_null($person_work['work_job_id']) ) ? 'NULL' : $person_work['work_job_id'] ;
                                    $work_wage_id = ( !is_null($person_work['work_wage_id']) ) ? 'NULL' : $person_work['work_wage_id'] ;
                                }
                            }

                            $person_work_ ="REPLACE INTO `char_persons_work`
                                            (`person_id`, `working`, `can_work`, `work_status_id`, `work_reason_id`, `work_job_id`, `work_wage_id`, `work_location`)
                                            VALUES
                                            (".$person_id.",".$working.",". $can_work.",".$work_status_id .",". $work_reason_id.",".$work_job_id.",". $work_wage_id.",'
                                            ".$work_location."')";
                            mysqli_query($conn, $person_work_);
                            */
                        }
                    }
                }
            }
        }
        return;
    }

    function checkCard($card)
    {
        if (strlen($card) == 9) {
            $identity = $card;
            $lastDig = $identity[8];
            $digit1 = $identity[0] * 1;
            $digit2 = $identity[1] * 2;
            $digit3 = $identity[2] * 1;
            $digit4 = $identity[3] * 2;
            $digit5 = $identity[4] * 1;
            $digit6 = $identity[5] * 2;
            $digit7 = $identity[6] * 1;
            $digit8 = $identity[7] * 2;
            $checkLast = (int)($digit1) + (int)($digit3) + (int)($digit5) + (int)($digit7);

            $digit2 = (string) $digit2;
            if (strlen($digit2) == 2) {
                $digit22 = (string) $digit2;
                $checkLast += (int)($digit22[0]);
                $checkLast += (int)($digit22[1]);
            } else {
                $checkLast += (int)($digit2);
            }

            $digit4 = (string) $digit4;
            if (strlen($digit4) == 2) {
                $digit44 = (string) $digit4;
                $checkLast += (int)($digit44[0]);
                $checkLast += (int)($digit44[1]);
            } else {
                $checkLast += (int)($digit4);
            }

            $digit6 = (string) $digit6;
            if (strlen($digit6) == 2) {
                $digit66 = (string) $digit6;
                $checkLast += (int)($digit66[0]);
                $checkLast += (int)($digit66[1]);
            } else {
                $checkLast += (int)($digit6);
            }

            $digit8 = (string) $digit8;
            if (strlen($digit8) == 2) {
                $digit88 = (string) $digit8;
                $checkLast += (int)($digit88[0]);
                $checkLast += (int)($digit88[1]);
            } else {
                $checkLast += (int)($digit8);
            }

            $checkLast = (string)$checkLast;
            $checkLast_length = strlen($checkLast);

            if($checkLast[$checkLast_length - 1]==0)
                $lastOne=10;
            else
                $lastOne =  $checkLast[$checkLast_length - 1];

            $lastDigit_inCheck = 10 - $lastOne;

            if ($lastDig != $lastDigit_inCheck) {
                return false;
            }
            return true;

        }

        return false;
    }

    function GetCitizenInfo($card)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package" => "MOI_GENERAL_PKG",
                "procedure" => "CITZN_INFO",
                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";

        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = 'can not connected to api service';
        } else {
            $response = json_decode($response);
            $row = $response->DATA;
            if ($response->DATA && @count($response->DATA)) {
                $result = ['row'=> $row[0] ,'status' => true];
            }else{
                $result['message'] = 'not register card';
            }
        }
        return $result;
    }

    function savePerson($input = array(),$conn)
    {

        mysqli_query($conn,"SET CHARACTER SET 'utf8'");
        mysqli_query($conn,"SET SESSION collation_connection ='utf8_unicode_ci'");

        $person_Input = ['id_card_number','birthday','gender','marital_status_id','death_date','father_id','mother_id',
            'first_name','second_name','third_name','last_name','prev_family_name'];

        $utf8 = ['first_name', 'second_name', 'third_name', 'last_name','prev_family_name' ];

        $IntegerInput =  ['marital_status_id'];

        $insert=[];
        foreach ($person_Input as $var) {
            if(isset($input[$var])){
                if($input[$var] != null && $input[$var] != "") {
                    if(in_array($var, $IntegerInput)){
                        $insert[$var] =(int)$input[$var];
                    }else if(in_array($var, $utf8)){
    //                        $insert[$var] =iconv("UTF-8", "ISO-8859-1", $input[$var]);
                        $insert[$var] =$input[$var];
                    }else{

                        if ($var == 'birthday' || $var == 'death_date') {
                            $insert[$var] = date('Y-m-d', strtotime($input[$var]));
                        }else{
                            $insert[$var] =$input[$var];
                        }
                    }

                }else{
                    $insert[$var] = null;
                }
            }
        }



        $return=array();
        $person=null;
        $person_id = null;


        if(sizeof($insert) != 0){
            $person = mysqli_query($conn,"SELECT * from char_persons  where id_card_number = ".(int) $input['id_card_number']);
            $rows = $person->num_rows;

            if($rows == 0){
                $person_keys = [];
                $person_values =[];

                foreach ($insert as $key => $value) {
                    $person_keys []=  "`".$key."`";
                    $person_values []=  "'".$value."'";
                }

                $set = ['prev_family_name','mother_id','father_id'];
                foreach ($set as $key) {
                    if(!isset($insert[$key])){
                        $person_keys []=  "`".$key."`";
                        $person_values []=   'NULL';
                    }
                }

    //              join(', ', array_reverse($person_values))
                $person_keys_ = join(",", $person_keys);
                $person_values_ = join(',', $person_values);
                $query = mysqli_query($conn,"INSERT INTO `char_persons` ( ".$person_keys_. ") VALUES ( " .$person_values_ . " ) " );
            }
            else{
                $person_updates = [];

                unset($insert['id_card_number']);
                foreach ($insert as $key => $value) {
                    $person_updates [] =  "`".$key."`"." = '".$value."'";
                }


                $set = ['prev_family_name','mother_id','father_id'];
                foreach ($set as $key) {
                    if(!isset($insert[$key])){
                        $person_updates [] =  "`".$key."`"." = 'NULL'";
                    }
                }
                $person_updates [] =  "`updated_at`"." = '".date("Y-m-d H:i:s")."'";
                $person_update_ = join(',', $person_updates);
                $query = mysqli_query($conn,"UPDATE char_persons SET " . $person_update_ . " WHERE id_card_number=".$input['id_card_number']);
            }
        }

        $get = mysqli_query($conn,"SELECT * from char_persons  where id_card_number = ".(int) $input['id_card_number']);

        if ($get) {

            $person = mysqli_fetch_array($get);

            $person_id = $person['id'];
            $return['id']= $person['id'];
            $return['id_card_number']= $person['id_card_number'];
            $return['name'] =((is_null($person['first_name']) || $person['first_name'] == ' ' ) ? ' ' : $person['first_name']) . ' ' .
                ((is_null($person['second_name']) || $person['second_name'] == ' ' ) ? ' ' : $person['second_name']) . ' ' .
                ((is_null($person['third_name']) || $person['third_name'] == ' ' ) ? ' ' : $person['third_name']) . ' ' .
                ((is_null($person['last_name']) || $person['last_name'] == ' ' ) ? ' ' : $person['last_name']) ;

            $return['adscountry_id']= $person['adscountry_id'];
            $return['adsregion_id']= $person['adsregion_id'];
            $return['adsdistrict_id']= $person['adsdistrict_id'];
            $return['adsneighborhood_id']= $person['adsneighborhood_id'];
            $return['adssquare_id']= $person['adssquare_id'];
            $return['adsmosques_id']= $person['adsmosques_id'];

            if(isset($input['en_first_name']) || isset($input['en_second_name']) || isset($input['en_third_name']) || isset($input['en_last_name'])) {

                $en = ['en_first_name' , 'en_second_name' ,'en_third_name','en_last_name'];
                $I18n=[];
                foreach ($en as $ino){
                    if(isset($input[$ino])){
                        $I18n[$ino] =$input[$ino];
                    }
                }

                if(sizeof($I18n) == 4){
                    $PersonI18n ="REPLACE INTO `char_persons_i18n` 
                                                      (`person_id`,`language_id`,`first_name`,`second_name`,`third_name`,`last_name`)
                                                       VALUES
                                                      (".$person_id.",2,'".$I18n['en_first_name']."','". $I18n['en_second_name']."','". $I18n['en_third_name']."','". $I18n['en_last_name']."')";

                    mysqli_query($conn,$PersonI18n);
                }
            }

            if (isset($input['kinship_id'])) {
                if (isset($input['l_person_id'])  && isset($input['l_person_update'])) {
                    if (($input['l_person_id'] != null && $input['l_person_id'] != "")) {

                        $person_kin_ ="REPLACE INTO `char_persons_kinship` (`l_person_id`, `r_person_id`, `kinship_id`)
                                                  VALUES (".$input['l_person_id'].",".$person_id.",". $input['kinship_id'].")";

                        mysqli_query($conn, $person_kin_);

                    }
                }
            }

        }

        return $return;
    }

    function getPersonHealthInfo($card)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package" => "MOH_GENERAL_PKG",
                "procedure" => "MEDICAL_REPORTS_GET_PR",
                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";

        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = 'can not connected to api service';
        } else {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $row = $response->DATA;
                $result = ['row'=> $row[0] ,'status' => true];
            }else{
                $result['message'] = 'not register card';
            }
        }
        return $result;
    }

    function getPersonFinancialInfo($card)
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = ['row'=> [] ,'status' => false];
        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package" => "LAB_EMP_FILE_PKG",
                "procedure" => "GET_EMP_INFO_BYID_PR",



                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";

        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $response = curl_exec($ch);

        if (FALSE === $response) {
            $result['message'] = 'can not connected to api service';
        } else {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $row = $response->DATA;
                $result = ['row'=> $row[0] ,'status' => true];
            }else{
                $result['message'] = 'not register card';
            }
        }
        return $result;
    }

    function getAllPersonRelations($card)
    {

        $result = [];

        $url = "http://eservices.mtit.gov.ps/ws/gov-services/ws/getData";
        $jsonData = [
            'WB_USER_NAME_IN' => 'AIDCOMM4GOVDATA',
            'WB_USER_PASS_IN' => 'DBDFB0FAD032E0518471E658FED26FED',
            'DATA_IN' => [
                "package" => "MOI_GENERAL_NEW_PKG",
                "procedure" => "MOI_REL_CTZN_INFO_PR",
                "ID" => $card
            ],
            'WB_AUDIT_IN' => [
                "ip" => "10.12.0.32",
                "pc" => "hima-pc",
            ]
        ];
        $jsonDataEncoded = json_encode($jsonData);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        $response = curl_exec($ch);

        if (!(FALSE === $response)) {
            $response = json_decode($response);
            if ($response->DATA && @count($response->DATA)) {
                $result =  $response->DATA;
            }
        }

        return $result;
    }

    function inputsMap($data)
    {

        $row = array("death_date"=>null,'card_type'=>"1");
        $_translator =array(
            "IDNO_RELATIVE" =>  "id_card_number",
            "FNAME_ARB" =>  "first_name",
            "SNAME_ARB" =>  "second_name",
            "TNAME_ARB" => "third_name",
            "LNAME_ARB" =>  "last_name",
            "PREV_LNAME_ARB" =>  "prev_family_name",
            "ENG_NAME" =>  "en_name",
            "SEX_CD" => "gender",
            "SOCIAL_STATUS_CD" =>  "marital_status_id",
            "BIRTH_DT" =>  "birthday",
            "DETH_DT" =>  "death_date",
            "RELATIVE_CD" =>  "kinship",
            "MOBILE" =>  "primary_mobile",
            "TEL" =>  "phone",
        );

        $dates = ['birthday' , 'death_date'];
        $SEX_CD_MAP = ['531'=> 1 ,'532'=> 2];
        $SOCIAL_STATUS_MAP = ['526'=> 20 ,'525'=> 10 ,'528' => 30 , '529'=>40 , '527' =>21];

        foreach ($data as $k=>$v) {

            if(isset($_translator[$k])){
                if($k == 'ENG_NAME'){
                    $parts = explode(" ",$v);
                    $row['en_first_name'] = $parts[0];
                    $row['en_second_name'] = $parts[1];
                    $row['en_third_name'] = $parts[2];
                    $row['en_last_name'] = $parts[3];
                }
                elseif($k == 'SEX_CD'){
                    if(isset($SEX_CD_MAP[$v])){
                        $row [$_translator[$k]]=$SEX_CD_MAP[$v];
                    }else{
                        $row [$_translator[$k]]=$v;
                    }
                }
                elseif($k == 'SOCIAL_STATUS_CD'){
                    $row [$_translator[$k]]=$SOCIAL_STATUS_MAP[$v];
                }
                elseif(in_array($_translator[$k],$dates)){
                    if(!is_null($v) && $v != " "){
                        //                    $row[$_translator[$k]]=date('Y-m-d',strtotime($v));
                        $row[$_translator[$k]]=date('Y-m-d',strtotime(str_replace("/","-",$v)));
                    }
                }
                else{
                    $row [$_translator[$k]]=$v;
                }
            }
        }

        if(isset($row['DETH_DT'])){
            if(!is_null($row['DETH_DT'])){
                $row ['marital_status_id']=4;
            }
        }

        return $row;
    }

    function getPersonMap($data)
    {

        $row = array('child' => array() , 'wife' =>  array());
        $_translator =array(
            "IDNO" =>  "id_card_number",
            "FNAME_ARB" =>  "first_name",
            "SNAME_ARB" =>  "second_name",
            "TNAME_ARB" => "third_name",
            "LNAME_ARB" =>  "last_name",
            "PREV_LNAME_ARB" =>  "prev_family_name",
            "ENG_NAME" =>  "en_name",
            "SEX_CD" => "gender",
            "SOCIAL_STATUS_CD" =>  "marital_status_id",
            "BIRTH_DT" =>  "birthday",
            "DETH_DT" =>  "death_date",
            "CI_CITY" =>  "governarate_name",
            "CI_REGION" => "city_name",
            "MOBILE" =>  "primary_mobile",
            "TEL" =>  "phone",
        );

        $dates = ['birthday' , 'death_date'];
        foreach ($data as $k=>$v) {

            if(isset($_translator[$k])){
                if($k == 'ENG_NAME'){
                    $parts = explode(" ",$v);
                    $row['en_first_name'] = $parts[0];
                    $row['en_second_name'] = $parts[1];
                    $row['en_third_name'] = $parts[2];
                    $row['en_last_name'] = $parts[3];
                }
                elseif(in_array($_translator[$k],$dates)){
                    if(!is_null($v) && $v != " "){
                        $row[$_translator[$k]]=date('Y-m-d',strtotime(str_replace("/","-",$v)));
                    }
                }
                else{
                    $row [$_translator[$k]]=$v;
                }
            }
        }

        return $row;
    }

?>
