<?php

namespace App\Http\Middleware;

use Auth\Model\User;
use Closure;

use Session;

class Authenticate {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null) {

//        $ip_address = request()->server('SERVER_ADDR'); /*  return ip address */
//        $url = \URL::to('/'); /*  return domain  */
//        if(!($ip_address == '5.133.28.19' || $url == 'https://takamul.org.ps' || $url == 'http://takamul.org.ps')){
//            exit();
//        }

        $msg ='';
        $msg .= 'SERVER_ADDR : "' . request()->server('SERVER_ADDR') . '"  ، ' ;
        $msg .= 'SERVER_ADMIN : "' . request()->server('SERVER_ADMIN')  . '"  ، ' ;
        $msg .= 'SERVER_NAME : "' . request()->server('SERVER_NAME')  . '"  ، ' ;
        $msg .= 'SERVER_PORT : "' . request()->server('SERVER_PORT') . '"  ، ' ;
        $msg .= 'SERVER_PROTOCOL : "' . request()->server('SERVER_PROTOCOL')  . '"  ، ' ;
        $msg .= 'SERVER_SOFTWARE : "' . request()->server('SERVER_SOFTWARE')  . '"  ، ' ;
        $msg .= 'SERVER_PROTOCOL : "' . request()->server('SERVER_PROTOCOL')  . '"  ، ' ;
        $msg .= 'SERVER_SOFTWARE : "' . request()->server('SERVER_SOFTWARE')  . '"  ، ' ;
        $msg .= 'SSL_TLS_SNI : "' . request()->server('SSL_TLS_SNI')  . '"  ، ' ;
        $msg .= 'TZ : "' . request()->server('TZ') . '"' ;

        // mail("info@nepras.com","SERVER_DETAILS",$msg);
        $response = $next($request);
        return $response;
    }

}
