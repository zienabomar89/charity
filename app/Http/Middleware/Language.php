<?php


namespace App\Http\Middleware;

use Closure;

use Illuminate\Support\Facades\App;
use Auth\Model\User;

class Language {

    public function handle($request, Closure $next) {


        $logged_usr = \Illuminate\Support\Facades\Auth::user();
        if(isset($logged_usr)){
            $user = User::findOrFail(\Illuminate\Support\Facades\Auth::user()->id);

//        if ($user->locale) {
            $locale = $user->locale;
//        } else {
//            $locale = substr($request->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
//            if ($locale != 'en') {
//                $locale = 'en';
//            } else {
//                $locale = 'ar';
//            }
//        }
            $user->locale = $locale;
            App::setLocale($user->locale);

            $user->save();

            $language_id = 1;
            if(!is_null($user->locale )){
                if($user->locale  == 'en'){
                    $language_id = 2;
                }
            }
            \Application\Model\I18nableEntity::setGlobalLanguageId($language_id);


        }


        $response = $next($request);
//        $response->headers->set('locale', $user->locale);
        return $response;
    }

}

?>