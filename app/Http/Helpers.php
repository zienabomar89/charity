<?php

namespace App\Http;
use Auth\Model\User;
use App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Lang;


class Helpers
{
    protected static $validator;

    public static function getFromTable($table, $key, $value, $where = "", $soft_delete = false) {
        if ($where) {
            $rs = \DB::table($table)->select($key, $value);
            for ($i = 0; $i < @count($where); $i++)
                $rs->where($where[$i][0], $where[$i][1], $where[$i][2]);
            if ($soft_delete)
                $rs = $rs->whereNull('deleted_at');
            $rs = $rs->get();
        }
        else {
            $rs = \DB::table($table)->select($key, $value);
            if ($soft_delete)
                $rs = $rs->whereNull('deleted_at');
            $rs = $rs->get();
        }
        return $rs;
    }

    public static function isValid($attributes,$rules){

        self::$validator = \Validator::make($attributes,$rules) ;

        if(!self::$validator->passes()){
            return ['status'=>'failed_valid' ,'error_type' =>'validate_inputs' ,'msg' => self::$validator->messages()];
        }

        return null;
    }

    public static function recordsPerPage($recordsCount , $all){

        $records_per_page = config('constants.records_per_page');
        if($recordsCount){
            if ($recordsCount == '-1') {
                $records_per_page = $all;
            }else{
                $records_per_page = (int)($recordsCount);
            }
        }

        return $records_per_page;
    }

    public static function deleteFile($id)
    {
        $file = \Document\Model\File::findOrFail($id);
        \Illuminate\Support\Facades\Storage::delete($file->filepath);
        foreach (\Document\Model\FileRevision::allforFile($file->id) as $revision) {
            \Illuminate\Support\Facades\Storage::delete($revision->filepath);
        }
        $file->delete();
        return $file;
    }

    public static function getLocale(){

        $language_id = 1;

        $user = User::findOrFail(\Illuminate\Support\Facades\Auth::user()->id);

        if(!is_null($user->locale )){
            if($user->locale  == 'en'){
                $language_id = 2;
            }
        }
        \Application\Model\I18nableEntity::setGlobalLanguageId($language_id);

        return $language_id;
    }

    public static function getFormatedDate($date){

                return $date;
       $output = null ;
        $explode = explode('/',$date);
        if(sizeof($explode) == 3){
            $str = $explode[2] .'-' .$explode[1] .'-' . $explode[0] ;
            $output = date('Y-m-d', strtotime($str));
        }
        return $output;
    }

    public static function sheetFound( $sheet , $list){

        $sheetFound = false;
        foreach ($list as $item) {
            if ( $item == $sheet ) {
                $sheetFound = true;
                break;
            }
        }
        return $sheetFound;
    }

    public static function getDistrictCode ($organization_id ,$id){


        if ($organization_id == 1)
            return 'C';

        $CodeArray = [
            249 => 'N', // north
            250 => 'W', // west
            251 => 'E', // east
            252 => 'S', // south
            253 => 'R', //  rafah
            254 => 'KH', // khan
            255 => 'M', // Wosta
        ];

        if ( isset($CodeArray[$id]) ) {
            return $CodeArray[$id];
        }
        return '';
    }

    public static function normalize_text_ar ($p_text){
        $p_text = str_replace('ـ', '',$p_text);
        $p_text = str_replace('ً', '',$p_text);
        $p_text = str_replace('ٌ', '',$p_text);
        $p_text = str_replace('ٍ', '',$p_text);
        $p_text = str_replace('َ', '',$p_text);
        $p_text = str_replace('ُ', '',$p_text);
        $p_text = str_replace('ِ', '',$p_text);
        $p_text = str_replace('ّ', '',$p_text);
        $p_text = str_replace('ْ', '',$p_text);
        $p_text = str_replace( 'ؤ', 'و',$p_text);
        $p_text = str_replace('ئ', 'ى',$p_text);
        $p_text = str_replace('آ', 'ا',$p_text);
        $p_text = str_replace('أ', 'ا',$p_text);
        $p_text = str_replace('إ', 'ا',$p_text);
        $p_text = str_replace('ٔ', 'ا',$p_text);
        $p_text = str_replace('ٕ', 'ا',$p_text);
        $p_text = str_replace('ﻻ', 'لَا',$p_text);
        $p_text = str_replace('ﻷ', 'لأ',$p_text);
        $p_text = str_replace('ﻹ', 'لأ',$p_text);
        $p_text = str_replace('ﻵ', 'لإ',$p_text);
        $p_text = str_replace('ة','ه',$p_text);

        while(strpos($p_text, '  ') > 0) {
            $p_text = str_replace('  ', ' ',$p_text);
        }

        RETURN trim($p_text);

    }

}
