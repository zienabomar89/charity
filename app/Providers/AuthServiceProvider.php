<?php

namespace App\Providers;

use Laravel\Passport\Passport;
// use Carbon\Carbon;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        $curDay = date('Y-m-d');

        $curDayToExpireIn = new \DateTime($curDay);
        $curDayToExpireIn->modify('+15 day');
        $curDayToExpireIn->format('Y-m-d H:i:s');

        $curDayToRefresh = new \DateTime($curDay);
        $curDayToRefresh->modify('+30 day');
        $curDayToRefresh->format('Y-m-d H:i:s');

        Passport::tokensExpireIn($curDayToExpireIn);
        Passport::refreshTokensExpireIn($curDayToRefresh);
        Passport::pruneRevokedTokens();
/*
                Passport::tokensExpireIn(Carbon::now()->addDays(15));
        Passport::refreshTokensExpireIn(Carbon::now()->addDays(30));
        Passport::pruneRevokedTokens();
  */
  }
}
