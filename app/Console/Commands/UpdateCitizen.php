<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Common\Model\Person;
use Common\Model\GovServices;

class UpdateCitizen extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'UpdateCitizen:update_citizen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update citizen data from governments services';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function  handle (){

//        $cards = \DB::table('card_to_update')->get();
//
//        if (sizeof($cards) > 0) {
//            foreach ($cards as $key =>$value) {
//                $card = $value->card;
//                $row = GovServices::byCard($value->card);
//                if($row['status'] == true) {
//                    $person_ = Person::where(function ($q) use ($card){
//                                        $q->where('id_card_number',$card);
//                                    })->first();
//
//                    if($person_){
//                        $person_id = $person_->id;
//                        $map = GovServices::personDataMap($row['row']);
//                        $to_update =array("prev_family_name",'last_name', "marital_status_id", "birthday", "death_date",);
//
//                        foreach ($to_update as $key){
//                            if(isset($map[$key])){
//                                $person_->$key = $map[$key];
//                            }
//                        }
//
//                        $en = ['en_first_name' , 'en_second_name' ,'en_third_name','en_last_name'];
//                        $per_en=[];
//                        foreach ($en as $ino){
//                            if(isset($map[$ino])){
//                                $per_en[$ino] =$map[$ino];
//                            }
//                        }
//
//                        if(sizeof($per_en) == 4){
//                            if(\Common\Model\PersonModels\PersonI18n::where(['person_id' => $person_->id,'language_id' => 2])->first()){
//                                \Common\Model\PersonModels\PersonI18n::where(['person_id' => $person_->id,'language_id' => 2])
//                                    ->update(['first_name'  => $per_en['en_first_name'], 'second_name' => $per_en['en_second_name'],
//                                        'third_name'  => $per_en['en_third_name'], 'last_name'   => $per_en['en_last_name']
//                                    ]);
//                            }else{
//                                \Common\Model\PersonModels\PersonI18n::create(['person_id' => $person_->id,'language_id' => 2,
//                                    'first_name'  => $per_en['en_first_name'], 'second_name' => $per_en['en_second_name'],
//                                    'third_name'  => $per_en['en_third_name'], 'last_name'   => $per_en['en_last_name']
//                                ]);
//                            }
//                        }
//
//                        $person_->spouses = 0 ;
//                        $person_->female_live = 0 ;
//                        $person_->male_live = 0 ;
//
//                        $related = GovServices::getPersonRelatedCard($card);
//
//                        if(sizeof($related['row']) > 0 ) {
//                            $kinships =$related['kinship'];
//                            foreach ($related['row'] as $crd){
//                                if(!is_null($crd)){
//                                    $row_ = GovServices::byCard($crd);
//                                    $child_ = \Common\Model\Person::PersonMap($row_['row']);
//                                    $child_['l_person_id']=$person_->id;
//                                    $child_['update_en']=true;
//                                    $child_['l_person_update']=true;
//                                    $child_['person_id']=null;
//                                    $child_['card_type']='1';
//                                    $child_['id_card_number']=$crd;
//
//                                    if (substr($crd, 0, 1) === '7') {
//                                        $child_['card_type']='2';
//                                    }
//
//                                    $ifExist = \Common\Model\Person::where(function ($q) use ($crd){
//                                        $q->where('id_card_number',$crd);
//                                    })->first();
//
//                                    if($ifExist){
//                                        $child_['person_id']=$ifExist->id;
//                                    }
//
//                                    $rel =$kinships[$crd];
//                                    if($rel =='wife'){
//                                        $wife_kinship=\Setting\Model\Setting::where('id','wife_kinship')->first();
//                                        if($wife_kinship){
//                                            if(!is_null($wife_kinship->value) and $wife_kinship->value != "") {
//                                                $child_['kinship_id'] =$wife_kinship->value;
//                                                $child_['husband_id']=$person_id;
//                                                $child_['husband']=$person_id;
//                                                $person_->spouses ++;
//                                            }
//                                        }
//                                    }
//                                    else if($rel =='son' && $child_['gender'] == 2){
//                                        $daughter_kinship = \Setting\Model\Setting::where('id', 'daughter_kinship')->first();
//                                        if($daughter_kinship){
//                                            if(!is_null($daughter_kinship->value) and $daughter_kinship->value != "") {
//                                                $person_0['father_id'] =$person_id;
//                                                $child_['kinship_id'] =$daughter_kinship->value;
//                                                $person_->female_live++;
//                                            }
//                                        }
//                                    }
//                                    else {
//                                        $son_kinship = \Setting\Model\Setting::where('id', 'son_kinship')->first();
//                                        if($son_kinship){
//                                            if(!is_null($son_kinship->value) and $son_kinship->value != "") {
//                                                $person_0['father_id'] =$person_id;
//                                                $child_['kinship_id'] =$son_kinship->value;
//                                                $person_->male_live++;
//                                            }
//                                        }
//                                    }
//
//                                    $child_['update_en'] = true;
//                                    \Common\Model\Person::savePerson($child_);
//                                }
//                            }
//                            $person_->family_cnt = ( $person_->male_live + $person_->female_live +  $person_->spouses) + 1;
//                        }
//
//                        $person_->save();
//                    }
//                }
//            }
//        }

        return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);

    }


}