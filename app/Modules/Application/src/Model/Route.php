<?php

namespace Application\Model;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'app_routes';
    
    public $timestamps = false;
    
    protected static $rules;

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'code'  => 'required|max:45',
                'name'  => 'required|max:255',
            );
        }
        
        return self::$rules;
    }
    
    public static function getUserRoutes($id)
    {
        $query = 'SELECT r.* FROM app_routes r WHERE r.permission_id IN (
            SELECT DISTINCT p.permission_id
            FROM char_users_roles u
            INNER JOIN char_roles_permissions p ON u.role_id = p.role_id
            WHERE u.user_id = :user_id
        )
        ORDER BY p.parent_id, p.id';
        
        $entries = array();
        $results = DB::select($query, ['user_id' => $id]);
        foreach ($results as $row) {
            $route = new Route(get_object_vars($row));
            $entries[] = $route;
        }
        
        return $entries;
    }
}