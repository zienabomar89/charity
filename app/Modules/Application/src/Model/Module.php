<?php

namespace Application\Model;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'app_modules';
    
    public $timestamps = false;
    
    protected static $rules;

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'code'  => 'required|max:45',
                'name'  => 'required|max:255',
            );
        }
        
        return self::$rules;
    }
}