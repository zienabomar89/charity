<?php

namespace Application\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class I18nableEntity extends Model implements I18nableInterface
{
    /**
     *
     * @var I18nInterface
     */
    protected $i18nModel;
    
    /**
     *
     * @var array
     */
    protected $i18nAttributes = array();
    
    /**
     *
     * @var int
     */
    protected $languageId;
    
    protected static $globalLanguageId;


    /**
     * 
     * @return I18nInterface
     */
    public function getI18nModel()
    {
        if ($this->i18nModel === null) {
            $this->i18nModel = new \Application\Model\I18nEntity();
        }
        
        return $this->i18nModel;
    }
    
    public function findI18nModel($language_id = null)
    {
        if (null === $language_id) {
            $language_id = $this->getLanguageId();
        }
        
        $model = $this->getI18nModel()->where(array(
            $this->getI18nModel()->getEntityIdColumn()   => $this->id,
            $this->getI18nModel()->getLanguageIdColumn() => $language_id,
        ))->first();
        
        return $model;
    }
    

    /**
     * 
     * @param array $attributes
     * @return I18nableInterface
     */
    public function setI18nAttributes($attributes)
    {
        $this->i18nAttributes = $attributes;
        return $this;
    }
    
    /**
     * 
     * @return array
     */
    public function getI18nAttributes()
    {
        return $this->i18nAttributes;
    }

    /**
     * 
     * @param int $id
     * @return I18nableInterface
     */
    public function setLanguageId($id)
    {
        $this->languageId = $id;
        return $this;
    }
    
    /**
     * 
     * @return int
     */
    public function getLanguageId()
    {
        if ($this->languageId === null) {
            return self::$globalLanguageId;
        }
        
        return $this->languageId;
    }

    /**
     * 
     * @param int $id
     */
    public static function setGlobalLanguageId($id)
    {
        self::$globalLanguageId = $id;
    }
    
    public static function getGlobalLanguageId()
    {
        return self::$globalLanguageId;
    }
    
    public function save(array $options = array())
    {
        static::saved(function ($entity) {
            $entity->getI18nModel()->updateOrCreate(array(
                $entity->getI18nModel()->getEntityIdColumn() => $entity->id,
                $entity->getI18nModel()->getLanguageIdColumn() => $entity->getLanguageId(),
            ), $entity->i18nAttributes);
        });
        
        return parent::save($options);
        
        /*DB::beginTransaction();
        
        try {
            parent::save($options);
            
            $this->getI18nModel()->updateOrCreate(array(
                $this->getI18nModel()->getEntityIdColumn() => $this->id,
                $this->getI18nModel()->getLanguageIdColumn() => $this->getLanguageId(),
            ), $this->i18nAttributes);
            
        } catch (\Exception $ex) {
            echo $ex->getTraceAsString();
            DB::rollBack();
        }
        DB::commit();*/
    }
    
    public function translations($columns = [])
    {
        $id = $this->getKey();
        
        $query = \Setting\Model\Language::query();
        $i18nTable = $this->getI18nModel()->getTable();
        $idColumn = $i18nTable . '.' . $this->getI18nModel()->getEntityIdColumn();
        $languageIdColumn = $i18nTable . '.' . $this->getI18nModel()->getLanguageIdColumn();
        
        if (!$columns) {
            $columns = array_keys($this->i18nAttributes);
        }
        foreach ($columns as $value) {
            $attributes[] = $i18nTable . '.' . $value;
        }
        
        $query->leftJoin($i18nTable, function($join) use ($id, $idColumn, $languageIdColumn, $attributes) {
            $join->on('char_languages.id', '=', $languageIdColumn)
                     ->where($idColumn, '=', $id);
            })
            ->select(array_merge(array(
                'char_languages.id AS language_id',
                'char_languages.name AS language_name'
            ), $attributes));
        
        return $query->get();
    }
    
    public function i18ns($options = array())
    {
        $defaults = array(
            'with_language_name' => true,
        );
        
        $options = array_merge($defaults, $options);
        
        $i18nTable = $this->getI18nModel()->getTable();
        $idColumn = $i18nTable . '.' . $this->getI18nModel()->getEntityIdColumn();
        $languageIdColumn = $i18nTable . '.' . $this->getI18nModel()->getLanguageIdColumn();
        
        $query = static::query();
        $query->where($idColumn, $this->getKey());
                
        if ($options['with_language_name']) {
            $query->join('char_languages', 'char_languages.id', '=', $languageIdColumn)
                  ->select("{$i18nTable}.*", 'char_languages.name as language_name');
        }
        
        return $query->get();
    }
       
    /**
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeI18n($query, $language_id = null)
    {
        if (null === $language_id) {
            $language_id = $this->getLanguageId();
        }
        $i18nModel = $this->getI18nModel();
        $i18nTable = $i18nModel->getTable();
        $fk = $i18nTable . '.' . $i18nModel->getEntityIdColumn();
        $languageColumn = $i18nTable . '.' . $i18nModel->getLanguageIdColumn();
        
        return $query->join($i18nTable, $this->getQualifiedKeyName(), '=', $fk)
                     ->where($languageColumn, '=', $language_id);
    }
}
