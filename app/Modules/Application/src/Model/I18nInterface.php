<?php

namespace Application\Model;

interface I18nInterface
{
    public function getLanguageIdColumn();
    
    public function getEntityIdColumn();
}