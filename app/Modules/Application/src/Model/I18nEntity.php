<?php

namespace Application\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class I18nEntity extends Model implements I18nInterface
{
    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    public function getLanguageIdColumn()
    {
        return 'language_id';
    }
    
    public function getEntityIdColumn()
    {
        return 'id';
    }
    
    /**
     * Find a model by its primary key.
     *
     * @param  mixed  $id
     * @param  array  $columns
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Collection|static[]|static|null
     */
    public function find($id, $columns = ['*'])
    {
        $builder = $this->newQueryWithoutScopes();
        if (is_array($id)) {
            return $this->findMany($id, $columns);
        }
        $builder->query->where($builder->model->getEntityIdColumn(), '=', $id)
                       ->where($builder->model->getLanguageIdColumn(), '=', $this->getAttribute($this->getLanguageIdColumn()));
        return $builder->first($columns);
    }
    
    /**
     * Find multiple models by their primary keys.
     *
     * @param  array  $ids
     * @param  array  $columns
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function findMany($ids, $columns = ['*'])
    {
        $builder = $this->newQueryWithoutScopes();
        if (empty($ids)) {
            return $builder->model->newCollection();
        }

        $builder->query->whereIn($this->model->getEntityIdColumn(), $ids)
                       ->where($builder->model->getLanguageIdColumn(), '=', $this->getAttribute($this->getLanguageIdColumn()));

        return $builder->get($columns);
    }

    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(Builder $query)
    {
        $query->where($this->getEntityIdColumn(), '=', $this->getAttribute($this->getEntityIdColumn()))
              ->where($this->getLanguageIdColumn(), '=', $this->getAttribute($this->getLanguageIdColumn()));

        return $query;
    }
}
