<?php

namespace Application\Model;

interface I18nableInterface
{    
    public function setLanguageId($id);
    
    public function getI18nModel();
    
    public function setI18nAttributes($attributes);
}

