<?php

namespace Application\Controller;

use App\Http\Controllers\Controller;
use Application\Model\Route as RouteModel;

class RoutesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $user = \Illuminate\Support\Facades\Auth::user();
        if ($user->id == 4) {
            return response()->json(RouteModel::all());
        }
        return response()->json(RouteModel::getUserRoutes($user->id));
    }
}