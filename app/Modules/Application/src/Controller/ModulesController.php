<?php

namespace Application\Controller;

use App\Http\Controllers\Controller;
use Application\Model\Module as ModuleModel;

class ModulesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    public function index()
    {
        return response()->json(ModuleModel::all());
    }
}