<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js" data-ng-app="MetronicApp"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js" data-ng-app="MetronicApp"> <![endif]-->
<!--[if !IE]><!-->
<html data-ng-app="CharityApp" dir="{{lang=='ar' ? 'rtl' : 'ltr'}}" lang="{{lang=='ar' ? 'ar_AR' : 'en'}}">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <title data-ng-bind="'CharityApp | ' + $state.current.data.pageTitle"></title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <noscript>
        <meta http-equiv="refresh" content="0;URL='/no-javascript.html'" />
        </noscript>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link ng-href="/themes/metronic/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link ng-href="/themes/metronic/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css">
        <link ng-href="/themes/metronic/global/plugins/bootstrap/css/bootstrap{{lang=='ar' ? '-rtl' : ''}}.min.css" rel="stylesheet" type="text/css">
        <link ng-href="/themes/metronic/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css">
        <link ng-href="/themes/metronic/global/plugins/bootstrap-switch/css/bootstrap-switch{{lang=='ar' ? '-rtl' : ''}}.min.css" rel="stylesheet" type="text/css">
        <link ng-href="/themes/metronic/global/plugins/bootstrap-toastr/toastr{{lang=='ar' ? '-rtl' : ''}}.css" rel="stylesheet" type="text/css">
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN DYMANICLY LOADED CSS FILES(all plugin and page related styles must be loaded between GLOBAL and THEME css files ) -->
        <link id="ng_load_plugins_before">
        <!-- END DYMANICLY LOADED CSS FILES -->
        <!-- BEGIN THEME STYLES -->
        <!-- DOC: To use 'rounded corners' style just load 'components-rounded.css' stylesheet instead of 'components.css' in the below style tag -->
        <link ng-href="/themes/metronic/global/css/components-rounded{{lang=='ar' ? '-rtl' : ''}}.min.css" id="style_components" rel="stylesheet" type="text/css">
        <link ng-href="/themes/metronic/global/css/plugins{{lang=='ar' ? '-rtl' : ''}}.min.css" rel="stylesheet" type="text/css">
        <link ng-href="/themes/metronic/layouts/layout/css/layout{{lang=='ar' ? '-rtl' : ''}}.min.css" rel="stylesheet" type="text/css">
        <link ng-href="/themes/metronic/layouts/layout/css/themes/darkblue{{lang=='ar' ? '-rtl' : ''}}.min.css" rel="stylesheet" type="text/css" id="style_color">
        <link ng-href="/themes/metronic/layouts/layout/css/custom{{lang=='ar' ? '-rtl' : ''}}.min.css" rel="stylesheet" type="text/css">
        <link ng-href="/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.css" rel="stylesheet" type="text/css">
        <link ng-href="/themes/metronic/global/plugins/jstree/dist/themes/default/style.min.css" rel="stylesheet" type="text/css">
        <link ng-href='/themes/metronic/global/plugins/ng-tag/tag.css' rel="stylesheet" type="text/css">
        <link ng-href="/themes/metronic/global/plugins/angularjs/plugins/ngprogress/ngProgress.css" rel="stylesheet" type="text/css">
        <link ng-href="/app/css/style{{lang=='ar' ? '-rtl' : ''}}.css" rel="stylesheet" type="text/css" />
        <link ng-href="/themes/metronic/global/css/fontawesome.min.css"  rel="stylesheet" type="text/css" >
        <link ng-href="/themes/metronic/global/plugins/angularjs/plugins/angular-chart/angular-chart.css"  rel="stylesheet" type="text/css">
        <!-- END THEME STYLES -->
        <link rel="shortcut icon" href="/logo.png">

        <style>

            .well > span {
                font-size: 11px !important;
                color: black;
                line-height: 8px !important;
            }

            .well {
                min-height: 20px;
                padding: 19px !important;
                margin-bottom: 20px;
                background-color: #f3f3f3 !important;
                border: 1px solid #f3f3f3 !important;
                border-radius: 4px !important;
                -webkit-box-shadow: inset 0 1px 1px rgb(0 0 0 / 5%) !important;
                box-shadow: inset 0 1px 1px rgb(0 0 0 / 5%) !important;
            }

            .page-header.navbar {
                background-color: #292f38!important;
            }

            .page-sidebar, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover {
                background-color: #292f38!important;
            }

            .page-footer {
                background: #292f38!important;
            }

            .page-footer .page-footer-inner {
                color: #b4bcc8!important;
            }

            .page-content-wrapper{
                background: #292f38!important;
            }
            .breadcrumb {
                padding: 8px 15px;
                margin-bottom: 20px;
                list-style: none;
                background-color: #f3f3f3!important;
                border-radius: 4px;
            }

            .breadcrumb > li > a {
                color: #000000!important;
            }

            .font-new-style {
                color: #b3b334!important;
            }
            .page-sidebar .page-sidebar-menu>li.active.open>a:hover, .page-sidebar .page-sidebar-menu>li.active>a:hover, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu>li.active.open>a:hover, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu>li.active>a:hover {
                background: #b3b334 !important;
                font-weight: bolder;
            }
            .page-sidebar .page-sidebar-menu>li.active.open>a, .page-sidebar .page-sidebar-menu>li.active>a, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu>li.active.open>a, .page-sidebar-closed.page-sidebar-fixed .page-sidebar:hover .page-sidebar-menu>li.active>a {
                background: #b3b334 !important;
                border-top-color: transparent;
                color: #fff;
            }

            #ngProgress {
                height: 15px;
                background-color: #b3b334 !important;
                color: #b3b334 !important;
                width: 0%;
                opacity: 1;
            }
                /*#ngProgress {*/
                    /*height: 15px;*/
                    /*background-color: #36c6d3 !important;*/
                    /*color: #36c6d3 !important;*/
                    /*width: 0%;*/
                    /*opacity: 1;*/
                /*}*/

                /* style.css */
                .loading {
                    cursor: wait; /* busy cursor feedback */
                }

                .loading * {
                    /* disable all mouse events on subElements */
                    pointer-events: none;
                }

               

                  .loader1{
                     background: #3498db;
                     /*height:100%;
                     width:100%;
                     background:url(/themes/loader.gif) no-repeat center center;
                     */
                    z-index: 9999999 !important;
                    /*position: absolute;*/
                    /*position: absolute;*/
                  /*  left: 60%;
                    top: 35%;*/
                     display: inline-block;
                     width: 50px;
                    height: 50px;
                    /*margin: -35px 0 0 -75px;*/
                    border: 3px solid #f3f3f3;
                    border-radius: 50% !important; 
                    /*border-top: 3px solid #3498db;
                    border-right: 3px solid #63ae45;
                    border-bottom: 3px solid #ed2024;
                    border-left: 3px solid #CDDC39;*/
                   /* -webkit-animation: spin 2s linear infinite !important;
                    animation: spin 2s linear infinite !important;*/

                   /*-webkit-animation: bounceDelay 1.4s infinite ease-in-out;
                    animation: bounceDelay 1.4s infinite ease-in-out;
                    -webkit-animation-fill-mode: both;
                    animation-fill-mode: both;*/
                }

                
                 .loader2{
                     /*height:100%;
                     width:100%;
                     background:red;
                     background:url(/themes/loader.gif) no-repeat center center;
                     */
                    z-index: 9999999 !important;
                    background: #63ae45;
                    /*position: absolute;*/
                  /*  left: 54%;
                    top: 35%;*/
                    display: inline-block;
                    width: 50px;
                    height: 50px;
                    /*margin: -35px 0 0 -75px;*/
                    border: 3px solid #f3f3f3;
                    border-radius: 50% !important;                    
                   /* border-top: 6px solid #3498db;
                    border-right: 6px solid #63ae45;
                    border-bottom: 6px solid #ed2024;
                    border-left: 6px solid #CDDC39;*/
                  /*  -webkit-animation: spin 2s linear infinite !important;
                    animation: spin 2s linear infinite !important;*/
              
                 /*  -webkit-animation: bounceDelay 1.4s infinite ease-in-out;
                    animation: bounceDelay 1.4s infinite ease-in-out;
                    -webkit-animation-fill-mode: both;
                    animation-fill-mode: both;*/
                 }

                .loader3{
                    z-index: 9999999 !important;
                    background: #ed2024;
                    /*position: absolute;*/
                   /* left: 48%;
                    top: 35%;*/
                    display: inline-block;
                    width: 50px;
                    height: 50px;
                    /*margin: -35px 0 0 -75px;*/
                    border: 3px solid #f3f3f3;
                    border-radius: 50% !important;                    
                   /* border-top: 6px solid #3498db;
                    border-right: 6px solid #63ae45;
                    border-bottom: 6px solid #ed2024;
                    border-left: 6px solid #CDDC39;*/
                  /*  -webkit-animation: spin 2s linear infinite !important;
                    animation: spin 2s linear infinite !important;*/

                  /* -webkit-animation: bounceDelay 1.4s infinite ease-in-out;
                    animation: bounceDelay 1.4s infinite ease-in-out;
                    -webkit-animation-fill-mode: both;
                    animation-fill-mode: both;*/
                }


              .loader4{
                    z-index: 9999999 !important;
                    background: #CDDC39;
                    /*position: absolute;*/
                   /* left: 48%;
                    top: 35%;*/
                    display: inline-block;
                    width: 50px;
                    height: 50px;
                    /*margin: -35px 0 0 -75px;*/
                    border: 3px solid #f3f3f3;
                    border-radius: 50% !important;                    
                   /* border-top: 6px solid #3498db;
                    border-right: 6px solid #63ae45;
                    border-bottom: 6px solid #ed2024;
                    border-left: 6px solid #CDDC39;*/
                  /*  -webkit-animation: spin 2s linear infinite !important;
                    animation: spin 2s linear infinite !important;*/

                  /* -webkit-animation: bounceDelay 1.4s infinite ease-in-out;
                    animation: bounceDelay 1.4s infinite ease-in-out;
                    -webkit-animation-fill-mode: both;
                    animation-fill-mode: both;*/
                }
                
               .loader5{
                    z-index: 9999999 !important;
                    background: #dd6c2a;
                    /*position: absolute;*/
                   /* left: 48%;
                    top: 35%;*/
                    display: inline-block;
                    width: 50px;
                    height: 50px;
                    /*margin: -35px 0 0 -75px;*/
                    border: 3px solid #f3f3f3;
                    border-radius: 50% !important;                    
                   /* border-top: 6px solid #3498db;
                    border-right: 6px solid #63ae45;
                    border-bottom: 6px solid #ed2024;
                    border-left: 6px solid #CDDC39;*/
                  /*  -webkit-animation: spin 2s linear infinite !important;
                    animation: spin 2s linear infinite !important;*/

                  /* -webkit-animation: bounceDelay 1.4s infinite ease-in-out;
                    animation: bounceDelay 1.4s infinite ease-in-out;
                    -webkit-animation-fill-mode: both;
                    animation-fill-mode: both;*/
                }

                 .loader6{
                    z-index: 9999999 !important;
                    background: #9e9999;
                    /*position: absolute;*/
                   /* left: 48%;
                    top: 35%;*/
                    display: inline-block;
                    width: 50px;
                    height: 50px;
                    /*margin: -35px 0 0 -75px;*/
                    border: 3px solid #f3f3f3;
                    border-radius: 50% !important;                    
                   /* border-top: 6px solid #3498db;
                    border-right: 6px solid #63ae45;
                    border-bottom: 6px solid #ed2024;
                    border-left: 6px solid #CDDC39;*/
                  /*  -webkit-animation: spin 2s linear infinite !important;
                    animation: spin 2s linear infinite !important;*/

                  /* -webkit-animation: bounceDelay 1.4s infinite ease-in-out;
                    animation: bounceDelay 1.4s infinite ease-in-out;
                    -webkit-animation-fill-mode: both;
                    animation-fill-mode: both;*/
                }
                
                .display-loader{
                    display: none !important;
                }


                @keyframes mymove {
                  from {left: 0px;}
                  to {left: 200px;}
                }

        </style>
     </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
    <!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
    <!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
    <!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
    <!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
    <!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
    <!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
    <!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->

    <body ng-controller="AppController" class="page-header-fixed page-sidebar-closed-hide-logo page-on-load" ng-class="{'page-content-white': settings.layout.pageContentWhite,'page-container-bg-solid': settings.layout.pageBodySolid, 'page-sidebar-closed': settings.layout.pageSidebarClosed}">
        <!-- BEGIN PAGE SPINNER -->
        <div ng-spinner-bar class="page-spinner-bar">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
        <!-- END PAGE SPINNER -->
        <!-- BEGIN HEADER -->
        <div data-ng-include="'/app/tpl/header.html'" data-ng-controller="HeaderController" class="page-header navbar navbar-fixed-top"> </div>
        <!-- END HEADER -->
        <div class="clearfix"> </div>
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div data-ng-include="'/app/tpl/sidebar'" data-ng-controller="SidebarController" class="page-sidebar-wrapper"> </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <div class="page-content" >
                    <div class="loader- display-loader"
                         style="position: fixed; display: none;
                                z-index: 9999999 !important; width: 230px;
                                animation-duration: 2s;
                                animation-timing-function: ease-in-out;
                                animation-delay: 0s;
                                animation-iteration-count: infinite;
                                animation-direction: normal;
                                animation-fill-mode: both;
                                animation-play-state: running;
                                animation-name: bounceDelay;
                                -webkit-animation-fill-mode: both;
                                animation-fill-mode: both;

                                top: 40%;  right: 50%; margin-right: -55px; padding: 30px !important;
                                text-align: center; background: #2196f330 !important; border-radius: 50%;">
                           
                                 <div class="loader1 " ></div>
                                 <div class="loader2 " ></div>
                              
                                  <br>
                                   <div class="loader3 " ></div>
                                   <div class="loader4 " ></div>
                                   <div class="loader5 " ></div>
                                  <br>

                                  <div class="loader5 " ></div>
                                  <div class="loader6 " ></div>

                    </div>


    <!-- BEGIN ACTUAL CONTENT -->
                         <div ui-view class="fade-in-up"> </div>
                    <!-- END ACTUAL CONTENT -->
                </div>
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div data-ng-include="'/app/tpl/footer.html'" data-ng-controller="FooterController" class="page-footer"> </div>
        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
        <!-- BEGIN CORE JQUERY PLUGINS -->
        <!--[if lt IE 9]>
	<script src="/themes/metronic/global/plugins/respond.min.js"></script>
	<script src="/themes/metronic/global/plugins/excanvas.min.js"></script> 
	<![endif]-->
        <script src="/themes/metronic/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/bootstrap-toastr/toastr.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>

        <!-- END CORE JQUERY PLUGINS -->
        <script src="/app/scripts/query-string/query-string.js" type="text/javascript"></script>
        <!-- BEGIN CORE ANGULARJS PLUGINS -->
        <script src="/themes/metronic/global/plugins/angularjs/angular.min.js" type="text/javascript"></script>

        <script src="/themes/metronic/global/plugins/angularjs/angular-sanitize.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/angular-touch.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/plugins/angular-ui-router.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/plugins/ocLazyLoad.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/plugins/ui-bootstrap-tpls.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/plugins/angular-resource.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/plugins/dirPagination.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/angular-cookies.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/plugins/angular-oauth2.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/i18n/angular-locale_ar-ps.js"></script>
        <script src="/themes/metronic/global/plugins/angularjs/plugins/angular-animate.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/plugins/angular-translate/angular-translate.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/plugins/angular-translate/angular-translate-loader-static-files.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/plugins/ui-select/select.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/plugins/angular-bootstrap-multiselect/dist/angular-bootstrap-multiselect.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/jstree/dist/jstree.min.js"></script>
        <script src="/themes/metronic/global/plugins/angularjs/plugins/ngJsTree/ngJsTree.min.js"></script>   <!-- END CORE ANGULARJS PLUGINS -->
        <script src="/themes/metronic/global/plugins/angularjs/plugins/angular-timeago-master/dist/angular-timeago.min.js"></script>   <!-- end timeago  -->
        <script src="/themes/metronic/global/plugins/angularjs/plugins/ng-ckeditor/dist/ckeditor/ckeditor.js"></script>
        <script src="/themes/metronic/global/plugins/angularjs/plugins/ng-ckeditor/dist/ng-ckeditor.min.js"></script>       
        <script src='/themes/metronic/global/plugins/ng-tag/tag.js'></script>   <!-- end timeago  -->
        <script src="/themes/metronic/global/plugins/angularjs/plugins/angular-timeago-master/src/languages/time-ago-language-ar_AR.js"></script>   <!-- end timeago  -->
        <script src="/themes/metronic/global/plugins/angularjs/plugins/angular-timeago-master/src/languages/time-ago-language-ar_AR.js"></script>   <!-- end timeago  -->
        <script src="/themes/metronic/global/plugins/angularjs/plugins/angular-timeago-master/src/languages/time-ago-language-en_US.js"></script>   <!-- end timeago  -->
        <script src="/themes/metronic/global/plugins/angularjs/plugins/ngBootbox-master/ngBootbox.js" type="text/javascript"></script>
        <script src="/themes/metronic/global/plugins/angularjs/plugins/ngprogress/ngprogress.min.js" type="text/javascript"></script>

        <script type="text/javascript"  src="/themes/metronic/global/plugins/chart/chart.js"></script>
        <script type="text/javascript" src="/themes/metronic/global/plugins/angularjs/plugins/angular-chart/angular-chart.js"></script>

<!--        <script type="text/javascript" src="https://rawgit.com/nnnick/Chart.js/8b840ce56be51b61dfc2d4e40e56304c880c541b/Chart.js"></script>-->
<!--        <script type="text/javascript" src="https://rawgit.com/jtblin/angular-chart.js/a0b1ce650d5dce9b7ccdd86843d83f27b0d7bc03/dist/angular-chart.js"></script>-->

        <!-- BEGIN APP LEVEL ANGULARJS SCRIPTS -->
        <script src="/app/app.module.js" type="text/javascript"></script>
        <script src="/app/directives.js" type="text/javascript"></script>
        <script src="/app/modules/dashboard/dashboard.module.js" type="text/javascript"></script>
        <script src="/app/modules/user/Module.js" type="text/javascript"></script>
        <script src="/app/modules/user/services/AuthUserService.js" type="text/javascript"></script>
        <script src="/app/modules/document/Module.js" type="text/javascript"></script>
        <script src="/app/modules/setting/Module.js" type="text/javascript"></script>
        <script src="/app/modules/aid/Module.js" type="text/javascript"></script>
        <script src="/app/modules/sponsorship/services/SponsorshipService.js" type="text/javascript"></script>
        <script src="/app/modules/sponsorship/Module.js" type="text/javascript"></script>
        <script src="/app/modules/sms/Module.js" type="text/javascript"></script>
        <script src="/app/modules/forms/Module.js" type="text/javascript"></script>
        <script src="/app/modules/organization/Module.js" type="text/javascript"></script>
        <script src="/app/modules/aid-repository/Module.js" type="text/javascript"></script>
        <script src="/app/modules/log/Module.js" type="text/javascript"></script>
        <script src="/app/modules/log/services/NotificationsService.js" type="text/javascript"></script>
        <script src="/app/modules/common/Module.js" type="text/javascript"></script>
        <script src="/app/modules/common/services/CategoriesService.js" type="text/javascript"></script>
        <script src="/app/modules/sponsor/Module.js" type="text/javascript"></script>
        <!-- END APP LEVEL ANGULARJS SCRIPTS -->
        <!-- BEGIN APP LEVEL JQUERY SCRIPTS -->
        <script src="/themes/metronic/global/scripts/app.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="/themes/metronic/layouts/layout/scripts/demo.min.js" type="text/javascript"></script>
        <!-- END APP LEVEL JQUERY SCRIPTS -->
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->

</html>