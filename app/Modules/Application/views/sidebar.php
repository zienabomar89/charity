<?php
$user = \Illuminate\Support\Facades\Auth::user();
?>

<div class="page-sidebar navbar-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->
    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" ng-class="{'page-sidebar-menu-closed': settings.layout.pageSidebarClosed}">
        <li class="start nav-item">
            <a href="#/dashboard" >
                <i class="glyphicon glyphicon-home"></i>
                <span class="title">{{ 'Dashboard' | translate}}</span>
            </a>
        </li>

        <li class="nav-item">
            <a href="#/maillingSystem/inbox" >
                <i class="glyphicon glyphicon-envelope"></i>
                <span class="title">{{ 'manage' | translate}} {{ 'Mailing' | translate}}</span>
            </a>
        </li>
<!--        <li class="nav-item">-->
<!--            <a href="javascript:;" class="nav-link nav-toggle">-->
<!--                <i class="glyphicon glyphicon-cog"></i>-->
<!--                <span class="title">  {{ 'manage' | translate}} {{ 'Mailing' | translate}} </span>-->
<!--                <span class="arrow "></span>-->
<!--            </a>-->
<!--            <ul class="sub-menu">-->
<!--                <li>-->
<!--                    <a href="#/maillingSystem/compose" class="iconify">-->
<!--                        <i class="fa fa-plus"></i>-->
<!--                        {{ 'compose' | translate}}-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <a href="#/maillingSystem/inbox" class="iconify">-->
<!--                        <i class="fa fa-level-up"></i>-->
<!--                        {{ 'inbox' | translate}}-->
<!--                    </a>-->
<!--                </li>-->
<!--                <li>-->
<!--                    <a href="#/maillingSystem/sent" class="iconify">-->
<!--                        <i class="fa fa-level-down"></i>-->
<!--                        {{ 'sent' | translate}}-->
<!--                    </a>-->
<!--                </li>-->
<!--            </ul>-->
<!--        </li>-->

        <li class="nav-item" ng-if="can('org.AdministrativeReport.manage') || can('org.AdministrativeReport.manageOrg')">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-circle-o-notch"></i>
                <span class="title">  {{ 'manage' | translate}} {{ 'administrative-reports' | translate}} </span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li ng-if="can('org.AdministrativeReport.manage')">
                    <a href="#/administrative-reports" class="iconify">
                        <i class="glyphicon glyphicon-cog"></i>
                        {{ 'add-administrative-reports' | translate}}
                    </a>
                </li>
                <li ng-if="can('org.AdministrativeReport.manageOrg')">
                    <a href="#/organization/administrative-reports" class="iconify">
                        <i class="fa fa-circle-o-notch"></i>
                        {{ 'administrative-reports' | translate}}
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item"
            ng-if="can('org.SocialSearch.manage') || can('org.SocialSearch.manageCases') ||
                     can('org.SocialSearch.templates') " >

            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-sticky-note-o"></i>
                <span class="title"> {{ 'manage' | translate}}  {{ 'Social Search' | translate}}</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">

                <li ng-if="can('org.SocialSearch.manage')">
                    <a href="#/aids/social-search" class="iconify">
                        <i class="glyphicon glyphicon-cog"></i>
                        {{ 'manage' | translate}}  {{ 'Social Search' | translate}}
                    </a>
                </li>

                <li ng-if="can('org.SocialSearch.manage')">
                    <a href="#/aids/social-search/trashed" class="iconify">
                        <i class="fa fa-trash"></i>
                        {{ 'Trashed Social Search' | translate}}
                    </a>
                </li>

                <li ng-if="can('org.SocialSearch.templates')">
                    <a href="#/aids/social-search/templates" class="iconify">
                        <i class="fa fa-file-word-o"></i>  {{ 'categories-template'| translate}}</a>
                </li>

                <li ng-if="can('org.SocialSearch.manageCases')">
                    <a href="#/aids/social-search/data/" class="iconify">
                        <i class="fa fa-circle-o-notch"></i>
                        {{ 'Social Search Report' | translate}}
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item" ng-if="can('org.organizationsProjects.manage') || can('org.organizationsProjects.trashed')">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-map-pin"></i>
                <span class="title"> {{ 'manage' | translate}}  {{ 'organizations-projects' | translate}}</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li  ng-if="can('org.organizationsProjects.manage')">
                    <a href="#/organizations-projects" class="iconify">
                        <i class="fa fa-hdd-o"></i>
                        {{ 'manage' | translate}}  {{ 'organizations-projects' | translate}}
                    </a>
                </li>

                <li  ng-if="can('org.organizationsProjects.trashed')">
                    <a href="#/trashed-organizations-projects" class="iconify">
                        <i class="fa fa-trash"></i>
                        {{ 'Trashed organizations-projects' | translate}}
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item"  ng-if="can('reports.CitizenRequest.manage') ||can('reports.relays.manage') || can('reports.relays.manageConfirm') ||
                                         can('aid.case.manage') || can('sponsorship.case.manage') || can('reports.case.generalSearch') ||
                                         can('reports.case.generalSearch') || can('reports.case.guardiansSearch') ||
                                         can('reports.case.voucherSearch') || can('reports.case.sponsorshipSearch') ||
                                         can('reports.organization.orgSearch')  || can('reports.case.citizenRepository') ||
                                         can('reports.case.socialAffairs') || can('reports.case.socialAffairsReceipt') ||
                                         can('reports.case.aidsCommittee') ||
                                        can('reports.case.islamicCommitmentStatistic') || can('reports.case.charityAct') ||
                                        can('reports.case.vouchersStatistic') || can('reports.case.sponsorshipsStatistic') ||
                                         can('reports.case.paymentsStatistic') || can('sponsorship.blockIDCard.manage') || can('aid.blockIDCard.manage')">

            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-bar-chart"></i>
                <span class="title">{{ 'reports and queries' | translate }}</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item" ng-if="can('reports.CitizenRequest.manage')">
                    <a href="#/citizens/request" >
                        <i class="fa fa-bullhorn"></i>
                        <span class="title">{{ 'CitizenRequest' | translate}}</span>
                    </a>
                </li>
                <li class="nav-item" ng-if="can('reports.transfers.manage') || can('reports.transfers.manageConfirm')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-map-marker"></i>  {{ 'transfer request' | translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="#/transfers" class="iconify">
                                <i class="fa fa-filter"></i>  {{ 'transfer request' | translate}} </a>
                        </li>
                        <li>
                            <a href="#/accepted-transfers" class="iconify">
                                <i class="fa fa-exchange"></i> {{ 'transfer request' | translate}}  {{ 'accepted transfers' | translate}} </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item" ng-if="can('reports.relays.manage') || can('reports.relays.manageConfirm')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-exchange"></i>  {{ 'relays' | translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item" ng-if="can('reports.relays.manage')">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-filter"></i>  {{ 'not confirm relays'| translate}}
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="#/relays/aids" class="iconify">
                                        <i class="icon-support"></i>  {{ 'aids'| translate}} </a>
                                </li>
                                <li>
                                    <a href="#/relays/sponsorships" class="iconify">
                                        <i class="icon-present"></i>  {{ 'sponsorships'| translate}}  </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item" ng-if="can('reports.relays.manageConfirm')">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-check"></i>  {{ 'confirm relays'| translate}}
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="#/relay/confirm/aids" class="iconify">
                                        <i class="icon-support"></i>  {{ 'aids'| translate}} </a>
                                </li>
                                <li>
                                    <a href="#/relay/confirm/sponsorships" class="iconify">
                                        <i class="icon-present"></i>  {{ 'sponsorships'| translate}}  </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item" ng-if="can('aid.case.manage') || can('sponsorship.case.manage')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-building-o"></i>  {{ 'organizations_cases' | translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li  ng-if=" can('sponsorship.case.manage')">
                            <a href="#/sponsorship/organizations" class="iconify"><i class="icon-present"></i>  {{ 'sponsorships'| translate}}  </a>
                        </li>
                        <li  ng-if="can('aid.case.manage')">
                            <a href="#/aids/organizations" class="iconify"><i class="icon-support"></i>  {{ 'aids'| translate}} </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item"  ng-if="can('reports.case.generalSearch') || can('reports.case.guardiansSearch') ||
                                   can('reports.case.voucherSearch') || can('reports.case.sponsorshipSearch') ||
                                    can('reports.organization.orgSearch') ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-filter"></i>   {{ 'search'| translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li class="nav-item"
                            ng-if="can('reports.case.generalSearch') || can('reports.case.guardiansSearch') ||
                                               can('reports.case.voucherSearch') || can('reports.case.sponsorshipSearch')"
                        >
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-users"></i>     {{ 'persons' |translate}}
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li ng-if="can('reports.case.generalSearch')">
                                    <a href="#/search/persons" class="iconify">
                                        <i class="fa fa-sitemap"></i>  {{ 'general' |translate}}  </a>
                                </li>

                                <li ng-if="can('reports.case.guardiansSearch')">
                                    <a href="#/search/guardians" class="iconify">
                                        <i class="fa fa-paw"></i>  {{'guardians_data' | translate}} </a>
                                </li>

                                <li ng-if="can('reports.case.voucherSearch')">
                                    <a href="#/vouchers-search" class="iconify">
                                        <i class="icon-support"></i>  {{'vouchers_data' | translate}} </a>
                                </li>

                                <li ng-if="can('reports.case.sponsorshipSearch')">
                                    <a href="#/sponsorship-search" class="iconify">
                                        <i class="icon-present"></i>  {{'sponsorships_' | translate}}</a>
                                </li>

                            </ul>
                        </li>
                        <li ng-if="can('reports.organization.orgSearch')">
                            <a href="#/organizations-search" class="iconify">
                                <i class="fa fa-bank"></i>  {{ 'organizations_' |translate}}  </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item" ng-if="can('reports.case.citizenRepository') ||
                                                    can('reports.case.socialAffairsReceipt') ||
                                                    can('reports.case.socialAffairs') ||
                                                    can('reports.case.aidsCommittee')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-question"></i>   {{'search_on' | translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-if="can('reports.case.citizenRepository') &&  can('reports.case.socialAffairs') && can('reports.case.aidsCommittee')">
                            <a href="#/reports/searchAll" class="iconify">
                                <i class="fa fa-arrows-alt"></i>  {{'general' | translate}}  </a>
                        </li>
                        <li ng-if="can('reports.case.citizenRepository')">
                            <a href="#/reports/citizenRepository" class="iconify">
                                <i class="fa fa-sticky-note"></i> {{'civil_registry' | translate}} </a>
                        </li>
                        <li ng-if="can('reports.case.socialAffairs')">
                            <a href="#/reports/socialAffairs" class="iconify">
                                <i class="icon-wallet"></i>  {{'SOCIAL_AFFAIRS' | translate}} </a>
                        </li>
                        <li ng-if="can('reports.case.socialAffairsReceipt')">
                            <a href="#/reports/socialAffairsReceipt" class="iconify">
                                <i class="icon-list"></i>  {{'social_affairs_receipt_' | translate}} </a>
                        </li>
                        <li ng-if="can('reports.case.aidsCommittee')">
                            <a href="#/reports/aidsCommittee" class="iconify">
                                <i class="icon-support"></i>  {{'aids committee' | translate}}  </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item"
                    ng-if="can('reports.case.islamicCommitmentStatistic') || can('reports.case.charityAct') ||
                                   can('reports.case.vouchersStatistic') || can('reports.case.sponsorshipsStatistic') ||
                                   can('reports.case.paymentsStatistic')"
                >
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-bar-chart"></i>  {{ 'statistic and reports'|translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li  ng-if="can('reports.case.islamicCommitmentStatistic') ">
                            <a href="#/statistic/islamic-commitment" class="iconify">
                                <i class="fa fa-bar-chart"></i>  {{ 'islamic-commitment'| translate}}</a>
                        </li>
                        <li  ng-if="can('reports.case.charityAct') ">
                            <a href="#/statistic/charity-act" class="iconify">
                                <i class="icon-wallet"></i>  {{ 'charity-act'| translate}}  </a>
                        </li>
                        <li  ng-if="can('reports.case.vouchersStatistic') ">
                            <a href="#/statistic/vouchers-sponsor" class="iconify">
                                <i class="icon-support"></i>  {{'sponsor-voucher'|translate}}</a>
                        </li>
                        <li  ng-if="can('reports.case.sponsorshipsStatistic') ">
                            <a href="#/statistic/sponsorships-sponsor" class="iconify">
                                <i class="icon-present"></i>  {{ 'sponsorships_authorities'| translate}} </a>
                        </li>
                        <li  ng-if="can('reports.case.paymentsStatistic') ">
                            <a href="#/statistic/payments" class="iconify">
                                <i class="fa fa-money"></i>  {{ 'payments' | translate}} </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item"  ng-if="can('sponsorship.blockIDCard.manage') || can('aid.blockIDCard.manage')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-ban"></i>  {{ 'blocked id cards number' | translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-if="can('sponsorship.blockIDCard.manage')">
                            <a href="#/sponsorships/blocked" class="iconify">
                                <i class="icon-present"></i>  {{ 'sponsorships_report' | translate}} </a>
                        </li>
                        <li  ng-if="can('aid.blockIDCard.manage')">
                            <a href="#/aids/blocked" class="iconify">
                                <i class="icon-support"></i>  {{ 'aids_report' | translate}} </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>

        <li class="nav-item"
            ng-if="can('sponsorship.case.create') || can('sponsorship.reportsCasesDocuments.manage') || can('sponsorship.reports.manage') ||
                       can('sponsorship.case.manage') || can('sponsorship.sponsorshipCases.manage')  ||  can('sponsorship.category.forms')  ||
                       can('sponsorship.category.manage') || can('sponsorship.category.upload') ||
                       can('sponsorship.sponsorshipCategoriesPolicy.manage') ||
                       can('sponsorship.sponsorshipVisitorNotes.manage')
                                                    ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-present"></i>
                <span class="title">{{'sponsorships management' | translate}}</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item" ng-if="can('sponsorship.case.create')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-plus"></i>  {{'add_new_case' | translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-repeat="cat in sponsorCategories">
                            <a href="#/sponsorship/form/{{cat.FirstStep}}/?category_id={{cat.id}}" class="iconify">
                                <i class="fa fa-plus"></i>  {{ categoriesLang(cat)}}
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item" ng-if="can('sponsorship.reportsCasesDocuments.manage') || can('sponsorship.reports.manage') ||
                                                    can('sponsorship.case.manage') || can('sponsorship.sponsorshipCases.manage') ">

                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>  {{'cases_manage' | translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li  ng-if="can('sponsorship.case.manage')">
                            <a href="#/sponsorship/all/cases" class="iconify">
                                <i class="fa fa-table"></i>  {{'sponsorships cases' | translate }} </a>
                        </li>
                        <li  ng-if="can('sponsorship.case.manage')">
                            <a href="#/sponsorships/cases/attachments" class="iconify">
                                <i class="fa fa-paperclip"></i>  {{'cases attachments' | translate}}</a>
                        </li>
                        <li  ng-if="can('sponsorship.case.manage')">
                            <a href="#/sponsorship/cases/incomplete-data" class="iconify">
                                <i class="fa fa-exclamation-triangle"></i>  {{ 'case-incomplete-data'|translate }}</a>
                        </li>
                        <li  ng-if="can('sponsorship.case.manage')">
                            <a href="#/sponsorship/families" class="iconify">
                                <i class="fa fa-sitemap"></i> {{ 'families'|translate }} </a>
                        </li>
                        <li ng-if="can('sponsorship.sponsorshipCases.manage')">
                            <a href="#/sponsorship/cases/statistics/" class="iconify">
                                <i class="fa fa-tags"></i>  {{'cases-statistics' | translate }}</a>
                        </li>
                        <li ng-if="can('sponsorship.case.manage')">
                            <a href="#/sponsorship/deleted/cases" class="iconify">
                                <i class="fa fa-trash-o"></i>  {{ 'deleted cases'|translate }} </a>
                        </li>
                        <li class="nav-item" ng-if="can('sponsorship.reportsCasesDocuments.manage') || can('sponsorship.reports.manage')">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-check-square"></i>  {{'reports_' | translate}}
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li ng-if=" can('sponsorship.reports.manage')">
                                    <a href="#/sponsorship/reports/" class="iconify">
                                        <i class="fa fa-sticky-note"></i>  {{'archived reports' | translate}}</a>
                                </li>
                                <li ng-if="can('sponsorship.reportsCasesDocuments.manage')">
                                    <a href="#/sponsorship/report/attachments" class="iconify">
                                        <i class="fa fa-paperclip"></i>  {{'attachments reports' | translate}} </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="nav-item" ng-if="can('sponsorship.category.manage') || can('sponsorship.category.upload') ||
                                                    can('sponsorship.sponsorshipCategoriesPolicy.manage') ||
                                                    can('sponsorship.sponsorshipVisitorNotes.manage') ||
                                                    can('sponsorship.category.forms')
                                                    ">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-check-square-o"></i> {{ 'categories-sponsorships'| translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-if="can('sponsorship.category.manage')">
                            <a href="#/sponsorships/categories" class="iconify">
                                <i class="fa fa-table"></i>  {{ 'categories-sponsorships'| translate}}</a>
                        </li>
                        <li ng-if="can('sponsorship.category.upload')">
                            <a href="#/sponsorships/categories/templates" class="iconify">
                                <i class="fa fa-file-word-o"></i>  {{ 'categories-template'| translate}}</a>
                        </li>
                        <li ng-if="can('sponsorship.sponsorshipCategoriesPolicy.manage')">
                            <a href="#/sponsorships/policy" class="iconify">
                                <i class="fa fa-ban"></i>  {{ 'policy' | translate}}</a>
                        </li>
                        <li ng-if="can('sponsorship.sponsorshipVisitorNotes.manage')">
                            <a href="#/sponsorships/visitor-notes" class="iconify">
                                <i class="fa fa-sticky-note-o"></i>  {{ 'visitor note' | translate}}   </a>
                        </li>
                        <li ng-if="can('sponsorship.category.forms')">
                            <a href="#/sponsorships/custom-forms" class="iconify">
                                <i class="fa fa-star"></i>  {{ 'custom-forms' | translate}}   </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item" ng-if="can('sponsorship.category.manage') || can('sponsorship.updateSetting.manage')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="glyphicon glyphicon-cog"></i>  {{ 'settings_' | translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-if="can('sponsorship.category.manage')">
                            <a href="#/sponsorships/general-setting" class="iconify">
                                <i class="glyphicon glyphicon-cog"></i>  {{ 'sponsorships_administrations' | translate}} </a>
                        </li>
                        <li ng-if="can('sponsorship.updateSetting.manage')">
                            <a href="#/updating-data-setting" class="iconify">
                                <i class="fa fa-cog"></i>  {{ 'update-data' | translate}} </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item" ng-if="can('sponsorship.sponsorships.manage')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-table"></i>
                        {{ 'nomination-cases' | translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-if="can('sponsorship.sponsorships.manage')">
                            <a href="#/sponsorship/nomination/" class="iconify">
                                <i class="fa fa-tags"></i>  {{ 'all-nomination-cases' | translate}}</a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item" ng-if="can('sponsorship.payments.manage') || can('sponsorship.paymentsCases.manage')" >
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-money"></i>  {{ 'payments' |translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-if="can('sponsorship.payments.manage')">
                            <a href="#/sponsorship/payments/" class="iconify">
                                <i class="fa fa-table"></i>  {{ 'payments-manage' |translate}}</a>
                        </li>
                        <li ng-if="can('sponsorship.paymentsCases.manage')">
                            <a href="#/sponsorship/payments/persons/" class="iconify">
                                <i class="fa fa-users"></i>  {{ 'Persons Payments' |translate}} </a>
                        </li>
                        <li class="nav-item"  ng-if="can('sponsorship.payments.manage')">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-sitemap"></i>  {{ 'cheques-' |translate}}
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="#/sponsorship/cheques" class="iconify">
                                        <i class="fa fa-code-fork"></i> {{ 'print-cheques' |translate}}   </a>
                                </li>
                                <li>
                                    <a href="#/sponsorship/archive-cheques" class="iconify">
                                        <i class="fa fa-history"></i>  {{ 'archive-cheques-' |translate}} </a>
                                </li>

                            </ul>
                        </li>

                    </ul>
                </li>
            </ul>
        </li>

        <li class="nav-item" ng-if="can('sponsorship.SponsorCase.manage')">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-users"></i>
                <span class="title">  {{ 'cases_manage' | translate}} </span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">

                <li class="nav-item" ng-if="can('sponsorship.SponsorCase.manage')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-present"></i>
                        <span class="title">  {{ 'sponsorships' | translate}} </span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="#/persons/sponsorship" class="iconify">
                                <i class="fa fa-users"></i>  {{'cases'|translate}}
                            </a>
                        </li>
                        <li class="nav-item" ng-if="can('sponsorship.SponsorCase.manage')">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-present"></i>
                                <span class="title">  {{ 'payments' | translate}} </span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="#/cases/payments" class="iconify">
                                        <i class="icon-present"></i>  {{'payments'|translate}}
                                    </a>
                                </li>
                                <li>
                                    <a href="#/cases/payments/persons/" class="iconify">
                                        <i class="fa fa-users"></i>  {{ 'recipient-report'| translate}}
                                    </a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </li>

                <li class="nav-item" ng-if="can('sponsorship.SponsorCase.manage')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-support"></i>
                        <span class="title">  {{ 'aids' | translate}} </span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="#/persons/aids" class="iconify">
                                <i class="fa fa-users"></i>  {{'cases'|translate}}
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-support"></i>
                                <span class="title">  {{ 'vouchers-data' | translate}} </span>
                                <span class="arrow "></span>
                            </a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="#/cases/vouchers" class="iconify">
                                        <i class="icon-support"></i> {{'vouchers-data' | translate}}</a>
                                </li>
                                <li>
                                    <a href="#/cases/vouchers/persons/" class="iconify">
                                        <i class="fa fa-users"></i>  {{ 'recipient-report' |translate}}</a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>




        <li class="nav-item"
            ng-if="can('aid.case.create') || can('aid.case.manage') ||
                        can('aid.voucher.manage') ||can('aid.voucher.trashed') || can('aid.voucherCategory.manage') || can('aid.category.manage') ||
                        can('aid.category.manage') || can('aid.category.upload') ||
                                                    can('aid.aidsCategoriesPolicy.manage') || can('aid.aidsVisitorNotes.manage') ||
                                                     can('aid.category.forms')">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-support"></i>
                <span class="title">{{ 'aids management'| translate}}</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item" ng-if="can('aid.case.create')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-plus"></i>  {{ 'add_new_case'| translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-repeat="cat in aidCategories">
                            <a ng-href="#/aids/caseform/{{cat.FirstStep}}/{{cat.id}}/new/" class="iconify">
                                <i class="fa fa-plus"></i>  {{ categoriesLang(cat)}}
                            </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item" ng-if="can('aid.case.manage')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>  {{'cases_manage' | translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="#/aids/all/cases" class="iconify">
                                <i class="fa fa-users"></i> {{'all aid cases'| translate}}  </a>
                        </li>
                        <li>
                            <a href="#/aids/cases/individual" class="iconify">
                                <i class="icon-users"></i>  {{'cases-individual' | translate}}  </a>
                        </li>
                        <li>
                            <a href="#/aids/cases/attachments" class="iconify">
                                <i class="fa fa-paperclip"></i>  {{'cases attachments' | translate}}</a>
                        </li>
                        <li>
                            <a href="#/aids/cases/incomplete-data" class="iconify">
                                <i class="fa fa-exclamation-triangle"></i>  {{ 'case-incomplete-data'|translate }}</a>
                        </li>
                        <li>
                            <a href="#/aids/deleted/cases" class="iconify">
                                <i class="fa fa-trash-o"></i> {{ 'deleted cases'|translate }} </a>
                        </li>
                    </ul>
                </li>


                <li class="nav-item" ng-if="can('aid.category.manage') || can('aid.category.upload') ||
                                                    can('aid.aidsCategoriesPolicy.manage') || can('aid.aidsVisitorNotes.manage') ||
                                                     can('aid.category.forms')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-check-square-o"></i>  {{ 'categories-aid'|translate }}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-if="can('aid.category.manage')">
                            <a href="#/aids/categories" class="iconify">
                                <i class="fa fa-table"></i>  {{ 'categories-aid'|translate }}</a>
                        </li>
                        <li ng-if="can('aid.category.upload')">
                            <a href="#/aids/categories/templates" class="iconify">
                                <i class="fa fa-file-word-o"></i>  {{ 'categories-template'| translate}}</a>
                        </li>
                        <li ng-if="can('aid.aidsCategoriesPolicy.manage')">
                            <a href="#/aids/policy" class="iconify">
                                <i class="fa fa-ban"></i>  {{ 'policy' | translate}}</a>
                        </li>
                        <li ng-if="can('aid.aidsVisitorNotes.manage')">
                            <a href="#/aids/visitor-notes" class="iconify">
                                <i class="fa fa-sticky-note-o"></i>  {{ 'visitor note' | translate}}  </a>
                        </li>
                        <li ng-if="can('aid.category.forms')">
                            <a href="#/aids/custom-forms" class="iconify">
                                <i class="fa fa-star"></i>  {{ 'custom-forms' | translate}}  </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item" ng-if="can('aid.category.manage')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="glyphicon glyphicon-cog"></i>  {{ 'settings_' | translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="#/aids/general-setting" class="iconify">
                                <i class="glyphicon glyphicon-cog"></i>  {{'aids_administrations' | translate}} </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item" ng-if="can('aid.voucher.manage') ||can(' aid.voucher.trashed') || can('aid.voucherCategory.manage')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-support"></i>  {{'vouchers-data' | translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-if="can('aid.voucher.manage')">
                            <a href="#/aids/vouchers" class="iconify">
                                <i class="fa fa-tags"></i> {{'vouchers-data' | translate}}</a>
                        </li>
                        <li ng-if="can('aid.voucher.manage')">
                            <a href="#/aids/Vouchers/persons" class="iconify">
                                <i class="fa fa-users"></i>  {{ 'personsVouchers' |translate}}</a>
                        </li>
                        <li ng-if="can(' aid.voucher.trashed')">
                            <a href="#/aids/trashed-vouchers" class="iconify">
                                <i class="fa fa-trash"></i> {{'trashed-vouchers' | translate}}</a>
                        </li>
                        <li ng-if="can('aid.voucherCategory.manage')">
                            <a href="#/aids/vouchers/categories" class="iconify">
                                <i class="fa fa-sitemap"></i> {{'vouchers-categories'| translate}}</a>
                        </li>
                    </ul>
                </li>

            </ul>
        </li>


        <li class="nav-item"  ng-if="can('setting.locations.manage') || can('setting.documentTypes.manage') ||
                         can('setting.ProjectCategory.manage') || can('setting.OrganizationsCategory.manage') ||can('setting.ProjectType.manage') || can('setting.ProjectRegion.manage') ||
                         can('setting.ProjectBeneficiaryCategory.manage') || can('setting.ProjectActivities.manage') ||
                         can('setting.currency.manage') || can('setting.banks.manage') || can('setting.aidSources.manage') ||
                         can('setting.transferCompany.manage') || can('setting.paymentCategory.manage') ||
                         can('setting.workJob.manage') || can('setting.workWages.manage') ||
                         can('setting.workStatus.manage') || can('setting.workReason.manage') ||
                         can('setting.kinship.manage') || can('setting.diseases.manage') ||
                         can('setting.deathCauses.manage') || can('setting.maritalStatus.manage') ||
                         can('setting.eduAuthorities.manage') || can('setting.eduDegrees.manage') ||
                         can('setting.eduStages.manage') || can('setting.essentials.manage') || can('setting.roofMaterials.manage') ||
                         can('setting.PropertyTypes.manage') ||  can('setting.properties.manage')||
                         can('setting.houseStatus.manage') ||  can('setting.buildingStatus.manage')||
                         can('setting.furnitureStatus.manage') ||  can('setting.habitableStatus.manage')">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="glyphicon glyphicon-cog"></i>
                <span class="title"> {{'system constants'| translate}}</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">

                <li class="nav-item"  ng-if="can('setting.essentials.manage') || can('setting.roofMaterials.manage') ||
                                                    can('setting.PropertyTypes.manage') ||  can('setting.properties.manage')||
                                                    can('setting.houseStatus.manage') ||  can('setting.buildingStatus.manage')||
                                                    can('setting.furnitureStatus.manage') ||  can('setting.habitableStatus.manage')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-home"></i>
                        <span class="title">{{'home data constants'| translate}}</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-if="can('setting.essentials.manage')">
                            <a href="#/common/entities/essentials" class="iconify">
                                <i class="fa fa-table"></i>  {{'Necessities and home furniture'| translate}}</a>
                        </li>
                        <li ng-if="can('setting.roofMaterials.manage')">
                            <a href="#/common/entities/roof-materials" class="iconify">
                                <i class="fa fa-table"></i> {{'Roof_Materials'| translate}} </a>
                        </li>
                        <li ng-if="can('setting.PropertyTypes.manage')">
                            <a href="#/common/entities/property-types" class="iconify">
                                <i class="fa fa-table"></i> {{'Property_Types'| translate}} </a>
                        </li>
                        <li ng-if="can('setting.properties.manage')">
                            <a href="#/common/entities/properties" class="iconify">
                                <i class="fa fa-table"></i> {{'properties'| translate}}  </a>
                        </li>
                        <li ng-if="can('setting.houseStatus.manage')">
                            <a href="#/common/entities/house-status" class="iconify">
                                <i class="fa fa-table"></i>  {{'houseStatus'| translate}}</a>
                        </li>
                        <li ng-if="can('setting.buildingStatus.manage')">
                            <a href="#/common/entities/building-status" class="iconify">
                                <i class="fa fa-table"></i>  {{'buildingStatus'| translate}}</a>
                        </li>
                        <li ng-if="can('setting.furnitureStatus.manage')">
                            <a href="#/common/entities/furniture-status" class="iconify">
                                <i class="fa fa-table"></i>  {{'furnitureStatus'| translate}}</a>
                        </li>
                        <li ng-if="can('setting.habitableStatus.manage')">
                            <a href="#/common/entities/habitable-status" class="iconify">
                                <i class="fa fa-table"></i> {{'habitableStatus'| translate}}</a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item" ng-if="can('setting.eduAuthorities.manage') || can('setting.eduDegrees.manage') ||
                                                    can('setting.eduStages.manage')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-list"></i>
                        <span class="title">{{'constants'| translate}} {{'qualification'| translate}} </span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-if="can('setting.eduAuthorities.manage')">
                            <a href="#/common/entities/education-authorities" class="iconify">
                                <i class="fa fa-table"></i> {{'Education_Authorities'| translate}} </a>
                        </li>
                        <li ng-if="can('setting.eduDegrees.manage')">
                            <a href="#/common/entities/education-degrees" class="iconify">
                                <i class="fa fa-table"></i> {{'Education_Degrees'|translate}}</a>
                        </li>
                        <li ng-if="can('setting.eduStages.manage')">
                            <a href="#/common/entities/education-stages" class="iconify">
                                <i class="fa fa-table"></i> {{'Education_Stages'|translate}}</a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item" ng-if="can('setting.kinship.manage') || can('setting.diseases.manage') ||
                                                    can('setting.deathCauses.manage') || can('setting.maritalStatus.manage')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i>
                        <span class="title"> {{'constants'|translate}} {{'Social data'|translate}}</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-if="can('setting.kinship.manage')">
                            <a href="#/common/entities/kinship" class="iconify">
                                <i class="fa fa-table"></i>  {{'kinship map'|translate}}</a>
                        </li>
                        <li ng-if="can('setting.diseases.manage')">
                            <a href="#/common/entities/diseases" class="iconify">
                                <i class="fa fa-table"></i>  {{'Diseases'|translate}}</a>
                        </li>
                        <li ng-if="can('setting.deathCauses.manage')">
                            <a href="#/common/entities/death-causes" class="iconify">
                                <i class="fa fa-table"></i>  {{'Death_Causes'|translate}}</a>
                        </li>
                        <li ng-if="can('setting.maritalStatus.manage')">
                            <a href="#/common/entities/marital-status" class="iconify">
                                <i class="fa fa-table"></i> {{'Marital_Status'|translate}}</a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item"  ng-if="can('setting.workJob.manage') || can('setting.workWages.manage') ||
                                                     can('setting.workStatus.manage') || can('setting.workReason.manage')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-check"></i>
                        <span class="title"> {{'constants'|translate}} {{'work_'|translate}}</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-if="can('setting.workWages.manage')">
                            <a href="#/common/entities/work-wages" class="iconify">
                                <i class="fa fa-table"></i>  {{'workWages'|translate}}</a>
                        </li>
                        <li ng-if="can('setting.workStatus.manage')">
                            <a href="#/common/entities/work-status" class="iconify">
                                <i class="fa fa-table"></i> {{'work_status'|translate}}</a>
                        </li>
                        <li ng-if="can('setting.workReason.manage')">
                            <a href="#/common/entities/work-reasons" class="iconify">
                                <i class="fa fa-table"></i>  {{'Work_Reasons'|translate}}</a>
                        </li>
                        <li ng-if="can('setting.workJob.manage')">
                            <a href="#/common/entities/work-jobs" class="iconify">
                                <i class="fa fa-table"></i>  {{'Work_Jobs'|translate}}</a>
                        </li>

                    </ul>
                </li>

                <li class="nav-item" ng-if="can('setting.currency.manage') || can('setting.banks.manage') || can('setting.aidSources.manage') ||
                                   can('setting.transferCompany.manage') || can('setting.paymentCategory.manage')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-bank"></i>
                        <span class="title">{{'constants'|translate}} {{ 'Financial' | translate}}</span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-if="can('setting.currency.manage')">
                            <a href="#/common/entities/currencies" class="iconify">
                                <i class="fa fa-money"></i>  {{ 'currencies' | translate}}</a>
                        </li>
                        <li ng-if="can('setting.banks.manage')">
                            <a href="#/common/entities/banks" class="iconify">
                                <i class="fa fa-bank"></i> {{ 'Banking organizations' | translate}}</a>
                        </li>
                        <li ng-if="can('setting.aidSources.manage')">
                            <a href="#/common/entities/aid-sources" class="iconify">
                                <i class="fa fa-table"></i> {{ 'aid_sources' | translate}}</a>
                        </li>
                        <li ng-if="can('setting.transferCompany.manage')">
                            <a href="#/common/entities/transfer-company" class="iconify">
                                <i class="fa fa-table"></i> {{ 'Transfer_Company' | translate}}</a>
                        </li>
                        <li ng-if="can('setting.paymentCategory.manage')">
                            <a href="#/common/entities/payment-category" class="iconify">
                                <i class="fa fa-list"></i>  {{ 'Payment_Category' | translate }}</a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item" ng-if="can('setting.ProjectCategory.manage') || can('setting.ProjectType.manage') || can('setting.ProjectRegion.manage') ||
                                can('setting.ProjectBeneficiaryCategory.manage') || can('setting.ProjectActivities.manage')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-wallet"></i>
                        <span class="title">{{ 'Project constant' | translate}} </span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-if="can('setting.ProjectCategory.manage')">
                            <a href="#/common/entities/project-category" class="iconify">
                                <i class="fa fa-paperclip"></i> {{ 'project-category' | translate}}</a>
                        </li>
                        <li ng-if="can('setting.ProjectType.manage')">
                            <a href="#/common/entities/project-type" class="iconify">
                                <i class="fa fa-paperclip"></i> {{ 'project-type' | translate}}</a>
                        </li>
                        <li ng-if="can('setting.ProjectBeneficiaryCategory.manage')">
                            <a href="#/common/entities/project-beneficiary-category" class="iconify">
                                <i class="fa fa-paperclip"></i> {{ 'project-beneficiary-category' | translate}}</a>
                        </li>
                        <li ng-if="can('setting.ProjectActivities.manage')">
                            <a href="#/common/entities/project-activities" class="iconify">
                                <i class="fa fa-paperclip"></i> {{ 'project-activities' | translate}}</a>
                        </li>
                        <li ng-if="can('setting.ProjectRegion.manage')">
                            <a href="#/common/entities/project-region" class="iconify">
                                <i class="fa fa-paperclip"></i> {{ 'project-region' | translate}}</a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item"  ng-if="can('setting.locations.manage') || can('setting.documentTypes.manage')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-list"></i>
                        <span class="title">{{ 'Others' | translate}} </span>
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-if="can('setting.locations.manage')">
                            <a href="#/common/entities/countries" class="iconify">
                                <i class="fa fa-map-marker"></i>  {{ 'addresses' | translate}} ( {{ 'General distribution' | translate}} ) </a>
                        </li>
                        <li ng-if="can('setting.locations.manage')">
                            <a href="#/common/entities/scountries" class="iconify">
                                <i class="fa fa-map-marker"></i> {{ 'addresses' | translate}} ( {{ 'Addressing aid' | translate}} )  </a>
                        </li>
                        <li ng-if="can('setting.OrganizationsCategory .manage')">
                            <a href="#/common/entities/organizations-category" class="iconify">
                                <i class="fa fa-tags"></i> {{ 'organizations_category' | translate}}</a>
                        </li>
                        <li ng-if="can('setting.documentTypes.manage')">
                            <a href="#/common/entities/document-types" class="iconify">
                                <i class="fa fa-paperclip"></i> {{ 'documentTypes' | translate}}</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>

        <li class="nav-item"  ng-if="can('forms.customForms.manage')">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-star"></i>
                <span class="title">{{ 'manage' | translate}} {{ 'custom forms' | translate}}</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li>
                    <a href="#/custom-forms/" class="iconify">
                        <i class="fa fa-table"></i> {{ 'custom forms' | translate}} </a>
                </li>
                <li>
                    <a href="#/custom-forms/trash" class="iconify">
                        <i class="fa fa-trash-o"></i>  {{ 'trashed custom forms' | translate}} </a>
                </li>
            </ul>
        </li>

        <li class="nav-item"  ng-if="can('org.organizations.manage') || can('org.organizations.create') || can('org.sponsors.manage')">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-sitemap"></i>
                <span class="title"> {{ 'organizations manage' | translate }}</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item" ng-if="can('org.organizations.manage') || can('org.organizations.create')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-sitemap"></i>  {{ 'organization' | translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-if="can('org.organizations.create')">
                            <a href="#/organizations/organizations/new/0" class="iconify">
                                <i class="fa fa-plus"></i>  {{ 'add organization' | translate}}</a>
                        <li>
                        <li ng-if="can('org.organizations.manage')">
                            <a href="#/organizations/organizations" class="iconify">
                                <i class="fa fa-sitemap"></i>  {{ 'organization' | translate}}</a>
                        </li>
                        <li ng-if="can('org.organizations.manage')">
                            <a href="#/organizations/organizations/trashed" class="iconify">
                                <i class="fa fa-trash-o"></i>  {{ 'all deleted organization' | translate}} </a>
                        </li>

                    </ul>
                </li>
                <li class="nav-item" ng-if="can('org.sponsors.manage')" >
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-support"></i> {{ 'sponsorship_authorities' | translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li ng-if="can('org.sponsors.manage')">
                            <a href="#/organizations/sponsors" class="iconify">
                                <i class="icon-support"></i> {{ 'sponsorship_authorities' | translate}}</a>
                        </li>
                        <li ng-if="can('org.sponsors.manage')">
                            <a href="#/organizations/sponsors/trashed" class="iconify">
                                <i class="fa fa-trash-o"></i> {{ 'all deleted sponsor' | translate}} </a>
                        </li>
                        <li>
                            <a href="#/blocked/sponsor" class="iconify">
                                <i class="fa fa-ban"></i> {{ 'Blocked Sponsors' | translate}}</a>
                        </li>

                    </ul>
                </li>
            </ul>
        </li>

        <li class="nav-item" ng-if="can('sms.smsProviders.manage') || can('sms.smsProviders.sentSms') || can('sms.organizationsSmsProviders.manage') ">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-mobile"></i>
                <span class="title">{{ 'manage' | translate}} {{ 'sms' | translate}}</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li ng-if="can('sms.smsProviders.manage')">
                    <a href="#/sms/providers" class="iconify">
                        <i class="fa fa-table"></i> {{ 'sms-providers' | translate}} </a>
                </li>
                <li ng-if="can('sms.smsProviders.sentSms')">
                    <a href="#/sms/sent-sms" class="iconify">
                        <i class="glyphicon glyphicon-cog"></i> {{ 'send sms' | translate}}</a>
                </li>
                <li ng-if="can('sms.organizationsSmsProviders.manage') ">
                    <a href="#/sms/settings" class="iconify">
                        <i class="glyphicon glyphicon-cog"></i>  {{ 'Settings' | translate}}</a>
                </li>
            </ul>
        </li>

        <li class="nav-item" ng-if="can('auth.suggestion.manage') || can('auth.suggestion.manageSuggestion')">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-lightbulb-o"></i>
                <span class="title">{{ 'manage-suggestions' | translate}}</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li ng-if="can('auth.suggestion.manage')">
                    <a href="#/suggestions" class="iconify">
                        <i class="fa fa-lightbulb-o"></i>  {{ 'my-suggestions' | translate}} </a>
                </li>
                <li ng-if="can('auth.suggestion.manageSuggestion')">
                    <a href="#/suggestions/manage" class="iconify">
                        <i class="fa fa-check-square-o"></i> {{ 'manage-suggestions' | translate}} </a>
                </li>
            </ul>
        </li>

        <li class="nav-item" ng-if="can('aidRepository.repositories.manage') || can('aidRepository.organizationProjects.manage')">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="icon-wallet"></i>
                <span class="title">{{ 'Manage_Repository' | translate}}</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item" ng-if="can('aidRepository.repositories.manage')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="icon-wallet"></i>  {{ 'Repositories' | translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu" ng-if="can('aidRepository.repositories.manage')">
                        <li>
                            <a href="#/aid-repository/repositories" class="iconify">
                                <i class="icon-wallet"></i> {{ 'Repositories' | translate}} </a>
                        </li>
                        <li>
                            <a href="#/aid-repository/repositories/trash" class="iconify">
                                <i class="fa fa-trash-o"></i> {{ 'Trash_Repositories' | translate}}</a>
                        </li>

                    </ul>
                </li>

                <li ng-if="can('aidRepository.repositories.manageCentral')">
                    <a href="#/aid-repository/central" class="iconify">
                        <i class="fa fa-share-alt"></i>  {{ 'central_repositories' | translate}}</a>
                </li>
                <li ng-if="can('aidRepository.organizationProjects.manage')">
                    <a href="#/aid-repository/organization/projects" class="iconify">
                        <i class="fa fa-tags"></i>  {{ 'Projects' | translate}}</a>
                </li>
                <li class="nav-item" ng-if="can('aidRepository.repositories.manage')">
                    <a href="#/aid-repository/organizations/" class="iconify">
                        <i class="glyphicon glyphicon-cog"></i>  {{ 'Share_Settings' | translate }}</a>
                </li>
            </ul>
        </li>

        <li ng-if="can('auth.users.manage') || can('auth.users.trashed') ||
                     can('auth.roles.manage')"
            class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-users"></i>
                <span class="title">{{ 'users_and_permissions'| translate}}</span>
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item" ng-if="can('auth.users.manage') || can('auth.users.trashed')">
                    <a href="javascript:;" class="nav-link nav-toggle">
                        <i class="fa fa-users"></i> {{ 'users' | translate}}
                        <span class="arrow "></span>
                    </a>
                    <ul class="sub-menu">
                        <li>
                            <a href="#/auth/users" class="iconify">
                                <i class="fa fa-users"></i> {{ 'users' | translate}}</a>
                        </li>
                        <li>
                            <a href="#/auth/users/trashed" class="iconify">
                                <i class="fa fa-trash-o"></i> {{ 'Trashed_Users' | translate}} </a>
                        </li>
                        <li>
                            <a href="#/auth/online-users" class="iconify">
                                <i class="fa fa-lastfm"></i> {{ 'online-users' | translate}}</a>
                        </li>

                    </ul>
                </li>
                <li ng-if="can('auth.roles.manage')">
                    <a href="#/auth/roles" class="iconify">
                        <i class="icon-lock"></i> {{'roles' | translate}}</a>
                </li>
<!--                <li ng-if="can('log.logs.manage')">-->
<!--                    <a href="#/log/logs" class="iconify">-->
<!--                        <i class="fa fa-history"></i>  {{'Logs' | translate}}</a>-->
<!--                </li>-->

<!--                <li ng-if="can('auth.settings.manage')">-->
<!--                    <a href="#/auth/settings" class="iconify">-->
<!--                        <i class="glyphicon glyphicon-cog"></i>  {{'Settings' | translate}}</a>-->
<!--                </li>-->
            </ul>
        </li>

        <li class="nav-item" ng-if="can('log.logs.manage')">
            <a href="#/log/logs" >
                <i class="fa fa-history"></i>
                <span class="title">{{ 'Logs' | translate}}</span>
            </a>
        </li>

        <!--'manage', \Setting\Model\BackupVersions::class -->
        <li class="nav-item" ng-if="can('auth.BackupVersions.manage')">
            <a href="#/backup" >
                <i class="fa fa-hdd-o"></i>
                <span class="title">{{ 'backup' | translate}}</span>
            </a>
        </li>

        <li class="nav-item" ng-if="can('auth.settings.manage')">
            <a href="#/auth/settings" >
                <i class="glyphicon glyphicon-cog"></i>
                <span class="title">{{ 'Settings' | translate}}</span>
            </a>
        </li>
    </ul>
    <!-- END SIDEBAR MENU -->
</div>