<?php

namespace App\Modules\Application;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function boot()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/config/routes.php';
        }
        $this->loadViewsFrom(__DIR__ . '/views', 'application');
        $this->loadTranslationsFrom(__DIR__ . '/languages', 'application');
        
        require __DIR__ . '/config/autoload.php';
    }

    public function register()
    {
        /*$this->mergeConfigFrom(
            __DIR__ . '/config/config.php', 'user'
        );*/
    }

}
