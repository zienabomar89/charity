<?php

$loader = new Composer\Autoload\ClassLoader();
$loader->addPsr4('Sms\\', __DIR__ . '/../src');
$loader->register();