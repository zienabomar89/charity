<?php
Route::group(['middleware' =>['api','lang','auth.check'],'prefix' => 'api/v1.0/sms' ,'namespace' => 'Sms\Controller'], function() {

    Route::post('excel','SmsServiceProviderController@excel');
    Route::group(['prefix' => 'sent_sms'], function() {
        Route::post('sendSms','SmsServiceProviderController@sendSms');
        Route::post('excelWithMobiles','SmsServiceProviderController@excelWithMobiles');
    });
    //----------------------------------------------------------------------------------------
    Route::group(['prefix' => 'providers'], function() {
        Route::get ('getActiveProvidersList','SmsServiceProviderController@getList');
        Route::put('updateStatus/{id}','SmsServiceProviderController@updateStatus');
    });
    Route::resource('providers', 'SmsServiceProviderController');
    //----------------------------------------------------------------------------------------
    Route::group(['prefix' => 'settings'], function() {
        Route::post('sendSmsToTarget','SmsServiceProviderController@sendSmsToTarget');
        Route::get ('getList','OrganizationsSmsProvidersController@getList');
        Route::put('updateActive/{id}','OrganizationsSmsProvidersController@updateActive');
        Route::put('updateDefault/{id}','OrganizationsSmsProvidersController@updateDefault');
    });
    Route::resource('settings', 'OrganizationsSmsProvidersController');
});
?>
