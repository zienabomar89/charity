<?php

return array(
                'sms.smsProviders.manage'=>'إدارة مزودي خدمة الرسائل القصيرة',
                'sms.organizationsSmsProviders.manage'=>'إدارة بيانات مزودي خدمة الرسائل',
                'sms.smsProviders.create'=>'إنشاء مزود خدمة جديد',
                'sms.organizationsSmsProviders.create'=>'إنشاء مزود خدمة رسائل جديد',
                'sms.smsProviders.update'=>'تحرير بيانات مزود خدمة الرسائل القصيرة',
                'sms.organizationsSmsProviders.update'=>'تحرير بيانات مزود خدمة',
                'sms.smsProviders.delete'=>'حذف مزود خدمة',
                'sms.organizationsSmsProviders.delete'=>'حذف مزود خدمة رسائل',
                'sms.smsProviders.view'=>'عرض بيانات مزود خدمة الرسائل',
                'sms.organizationsSmsProviders.view'=>'عرض بيانات مزود خدمة رسائل قصير',
                'sms.smsProviders.sentSms'=>'إرسال رسائل قصيرة'
            );


