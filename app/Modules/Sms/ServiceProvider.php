<?php

namespace App\Modules\Sms;

use Sms\Model\OrganizationsSmsProviders;
use Sms\Model\SmsProviders;
use Sms\Policy\OrganizationsSmsProvidersPolicy;
use Sms\Policy\SmsProvidersPolicy;

class ServiceProvider extends \App\Providers\AuthServiceProvider
{


    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [ SmsProviders::class => SmsProvidersPolicy::class,
                            OrganizationsSmsProviders::class => OrganizationsSmsProvidersPolicy::class
                        ];
    public function boot()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/config/routes.php';
        }
        $this->loadViewsFrom(__DIR__ . '/views', 'sms');
        $this->loadTranslationsFrom(__DIR__.'/languages', 'sms');
        
        require __DIR__ . '/config/autoload.php';
        $this->registerPolicies();
    }

    public function register()
    {
        /*$this->mergeConfigFrom(
            __DIR__ . '/config/config.php', 'user'
        );*/
    }

}
