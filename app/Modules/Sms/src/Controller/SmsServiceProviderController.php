<?php
namespace Sms\Controller;

use Aid\Model\VoucherPersons;
use App\Http\Controllers\Controller;
use Auth\Model\User;
use Citizens\Model\Citizen;
use Illuminate\Http\Request;
use Organization\Model\Organization;
use Sms\Model\SmsProviders;
use App\Http\Helpers;
use Maatwebsite\Excel\Facades\Excel;
use Common\Model\Person;
use Common\Model\PersonModels\PersonContact;

class SmsServiceProviderController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // paginate SmsProviders
    public function index(Request $request)
    {
        $this->authorize('manage', SmsProviders::class);
        $itemsCount = $request->input('itemsCount');
        $records_per_page = Helpers::recordsPerPage($itemsCount,SmsProviders::orderBy('Label')->count());
        return response()->json( SmsProviders::orderBy('Label')->paginate($records_per_page));
    }

    // list SmsProviders of Organization to login user organization
    public function getList()
    {
        return response()->json( ['Providers' => SmsProviders::where('status',1)->get()] );
    }

    // create new SmsProviders model
    public function store(Request $request){
        $response = array();

        $this->authorize('create', SmsProviders::class);

        $rules= ['Label'   => 'required', 'api_sms_balance_url'    => 'required', 'api_url'    => 'required'];

        $input=[
            'Label'     => strip_tags($request->Label),
            'api_sms_balance_url' => $request->api_sms_balance_url,
            'api_url' => $request->api_url
        ];

        $error = \App\Http\Helpers::isValid($input,$rules);
        if($error)
            return response()->json($error);

        $input['status']= 1;
        $providers = new SmsProviders($input);

        if($providers->save()){
            \Log\Model\Log::saveNewLog('SMS_PROVIDERS_CREATED', trans('sms::application.He added a new service provider named')  . ' ' .$input['Label']);
            $response["status"]= 'success';
            $response["msg"]= trans('sms::application.The row is inserted to db');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sms::application.The row is not insert to db');

        }
        return response()->json($response);
    }

    // get exist SmsProviders object model by id
    public function show($id)
    {
        $entry = SmsProviders::findOrFail($id);
        $this->authorize('view',  $entry);
        return response()->json(['Providers' => $entry] );
    }

    // update exist SmsProviders object model by id
    public function update(Request $request, $id)
    {
        $entry = SmsProviders::findOrFail($id);
        $this->authorize('update',$entry);

        $rules= ['Label'   => 'required', 'api_sms_balance_url'    => 'required', 'api_url'    => 'required'];

        $input=[
            'Label'     => strip_tags($request->Label),
            'api_sms_balance_url' => $request->api_sms_balance_url,
            'api_url' => $request->api_url];

        $error = \App\Http\Helpers::isValid($input,$rules);
        if($error)
            return response()->json($error);

        if($entry->update($input)) {
            \Log\Model\Log::saveNewLog('SMS_PROVIDERS_UPDATED', trans('sms::application.Edit the messages provider data')  . ' " '.$entry['Label']  . ' " ');
            $response["status"]= 'success';
            $response["msg"]= trans('sms::application.The row is updated');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sms::application.There is no update');
        }
        return response()->json($response);

    }

    // update active status of SmsProviders
    public function updateStatus(Request $request, $id)
    {
        $entry = SmsProviders::findOrFail($id);
        $this->authorize('update',  $entry);

        if($entry->update(['status' => $request->get('status')]))
        {
            \Log\Model\Log::saveNewLog('SMS_PROVIDERS_UPDATED', trans('sms::application.Edit the message provider status data')  . ' " ' .$entry->Label . ' " ');
            $response["status"]= 'success';
            $response["msg"]= trans('sms::application.The row is updated');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sms::application.There is no update');
        }
        return response()->json($response);

    }

    // delete exist SmsProviders model by id
    public function destroy($id,Request $request)
    {

        $response = array();
        $entry=SmsProviders::findorfail($id);
        $this->authorize('delete', $entry);
        $Label=$entry->Label;
        if($entry->delete()) {
            \Log\Model\Log::saveNewLog('SMS_PROVIDERS_DELETED', trans('sms::application.Deleted a service provider')  . ' " ' .$Label . ' " ');
            $response["status"]= 'success';
            $response["msg"]= trans('sms::application.The row is deleted from db');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sms::application.The row is not deleted from db');

        }
        return response()->json($response);

    }

    // send sms by excelSheet using card number or mobile number
    public function excel(Request $request){
        $this->authorize('sentSms', SmsProviders::class);
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $sent_using = $request->sent_using;  // 1 for mobile 2 for id_card_number
        $cont_type = $request->cont_type;  // 0 for primary_mobile 1 for wataniya
        $validFile=true;

        $send=[];
        $invalid_card = 0;
        $not_has_mob_no = 0;
        $processed=[];

        $importFile=$request->file ;
        if ($importFile->isValid()) {
            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){
                        $validFile = false;
                        foreach ($keys as $item) {
                            if ( $item == 'rkm_alhoy' ) {
                                $validFile = true;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);
                            if ($total > 0) {
                                $response = [];
                                $has_voucher = false;
                                if ($request->voucher_id){
                                    $has_voucher = true;
                                }
                                foreach ($records as $key =>$value) {
                                    if($sent_using == 0 || $sent_using =='0'){
                                        $id_card_number =$value['rkm_alhoy'];
                                        if(strlen($value['rkm_alhoy'])  <= 9) {
                                            $person = Person::where(function ($q) use ($id_card_number){
                                                                    $q->where('id_card_number',$id_card_number);
                                                                })->first();
                                            if(is_null($person)) {
                                                $result[]=['name' =>'','id_card_number' => $id_card_number,'reason'=>trans('aid::application.invalid_card')];
                                                $invalid_card++;
                                            }else{
                                                $contact_type = 'primary_mobile';
                                                if($cont_type == 0 || $cont_type == '0'){
                                                    $contact_type = 'wataniya';
                                                    $phone_msg = trans('aid::application.invalid_wataniya_for_this_person');
                                                }else{
                                                    $contact_type = 'primary_mobile';
                                                    $phone_msg = trans('aid::application.invalid_jawal_for_this_person');
                                                }
                                                $personContact = PersonContact::where(['person_id'=>$person->id,'contact_type' => $contact_type])->first();

                                                if(is_null($personContact)){
                                                    $result[]=['name' => $person->fullname,'id_card_number' => $id_card_number,'reason'=> $phone_msg];
                                                    $invalid_card++;
                                                }else{
                                                    $request->mobile_number = ltrim($personContact->contact_value, "0");

                                                    if($request->same_msg == 0 || $request->same_msg == '0')
                                                        $request->sms_messege = $value['alrsal'];

                                                    if($request->get('is_code')){
                                                        $user_name = $person['first_name'] ;
                                                        $request->sms_messege = str_replace("name", $user_name, $request->sms_messege );
                                                        $request->sms_messege  = str_replace("card", $id_card_number,$request->sms_messege );

                                                        if ($has_voucher){
                                                            $voucher = VoucherPersons::beneficiaryDetails($request->voucher_id,$person->id);
                                                            if ($voucher){
                                                                $request->sms_messege = str_replace("title", $voucher->voucher_title , $request->sms_messege );
                                                                $request->sms_messege = str_replace("location", $voucher->receipt_location , $request->sms_messege );
                                                                $request->sms_messege = str_replace("sh_value", $voucher->sh_value , $request->sms_messege );
                                                            }
                                                        }
                                                    }

                                                    $request->sms_messege = str_replace("\n",' ' ,  $request->sms_messege);

                                                    $sms =$this->sendMessage($request);
                                                    if($sms['status'] == 'error'){
                                                        $result[]=['name' => $person->fullname,'id_card_number' => $id_card_number,'reason'=>$sms['msg']];
                                                        $invalid_card++;
                                                    }
                                                    $processed[]=$value['rkm_alhoy'];

                                                }
                                            }
                                        }else{
                                            $result[]=['name' => ' ','id_card_number' => $id_card_number,'reason'=>'invalid_card'];
                                            $invalid_card++;
                                        }
                                    }else{

                                        $request->mobile_number = ltrim($value['rkm_aljoal'], "0");
                                        if($request->same_msg == 0 || $request->same_msg == '0'){
                                            $request->sms_messege = $value['alrsal'];
                                        }
                                        $sms =$this->sendMessage($request);

                                        if($sms['status'] == 'error'){
                                            $result[]=['mobile_number' => $value['rkm_aljoal'],'reason'=>$sms['msg']];
                                            $invalid_card++;
                                        }
                                    }
                                }

                                $token = md5(uniqid());
                                \Excel::create('export_' . $token, function($excel) use($result) {
                                    $excel->setTitle(trans('aid::application.sendSms'));
                                    $excel->setDescription(trans('aid::application.sendSms'));
                                    $excel->sheet(trans('sms::application.Message access record'), function($sheet) use($result) {

                                        $sheet->setRightToLeft(true);
                                        $sheet->setAllBorders('thin');
                                        $sheet->setfitToWidth(true);
                                        $sheet->setHeight(1,40);
                                        $sheet->getDefaultStyle()->applyFromArray([
                                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                        ]);
                                        $sheet->getStyle("A1:C1")->applyFromArray(['font' => ['bold' => true]]);
                                        $sheet->fromArray($result);
                                    });
                                })->store('xlsx', storage_path('tmp/'));
                                return response()->json(['download_token' => $token,'status'=>'success','msg'=>trans('sms::application.action success ,A file was downloaded with the numbers that the messages did not receive and the reason for not reaching')]);
                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }

    // send sms to group just by mobiles number like in (organizations and users)
    public function excelWithMobiles(Request $request){

        $this->authorize('sentSms', SmsProviders::class);
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $validFile=true;
        $invalid_card = 0;

        $importFile=$request->file ;
        if ($importFile->isValid()) {
            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){
                        $validFile = false;
                        foreach ($keys as $item) {
                            if ( $item == 'rkm_alhoy' ) {
                                $validFile = true;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);
                            if ($total > 0) {

                                foreach ($records as $key =>$value) {
                                    $request->mobile_number = ltrim($value['rkm_aljoal'], "0");
                                    if($request->same_msg == 0 || $request->same_msg == '0'){
                                        $request->sms_messege = $value['alrsal'];
                                    }

                                    $sms =$this->sendMessage($request);

                                    if($sms['status'] == 'error'){
                                        $result[]=['mobile_number' => $value['rkm_aljoal'],'reason'=>$sms['msg']];
                                        $invalid_card++;
                                    }

                                }

                                $token = md5(uniqid());
                                \Excel::create('export_' . $token, function($excel) use($result) {
                                    $excel->setTitle(trans('aid::application.sendSms'));
                                    $excel->setDescription(trans('aid::application.sendSms'));
                                    $excel->sheet(trans('sms::application.Message access record'), function($sheet) use($result) {

                                        $sheet->setRightToLeft(true);
                                        $sheet->setAllBorders('thin');
                                        $sheet->setfitToWidth(true);
                                        $sheet->setHeight(1,40);
                                        $sheet->getDefaultStyle()->applyFromArray([
                                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                        ]);
                                        $sheet->getStyle("A1:C1")->applyFromArray(['font' => ['bold' => true]]);
                                        $sheet->fromArray($result);
                                    });
                                })->store('xlsx', storage_path('tmp/'));
                                return response()->json(['download_token' => $token,'status'=>'success','msg'=> trans('aid::application.action success ,A file was downloaded with the numbers that the messages did not receive and the reason for not reaching') ]);
                            }


                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }

    // send sms to group just by mobiles number like in (  organization , users , project[organization,users] , citizens , person)
    public function sendSmsToTarget(Request $request){

            $this->authorize('sentSms', \Sms\Model\SmsProviders::class);

            $response = array();
            $rules= ['sms_provider'=> 'required', 'sms_messege'    => 'required'];

            $error = \App\Http\Helpers::isValid($request->all(),$rules);
            if($error)
                return response()->json($error);

            $provider = \Sms\Model\SmsProviders::getProvider($request->sms_provider);

            if(is_null($provider->api_url) || $provider->api_url =="" || $provider->api_url ==" "){
                return response()->json(array('status' => 'error' , 'msg' =>trans('sms::application.There is no link to send messages by the service provider you have selected')));
            }

            if(is_null($provider->sender) || $provider->sender =="" || $provider->sender ==" "){
                return response()->json(array('status' => 'error' , 'msg' =>trans('sms::application.There is no sender name for sending messages by the service provider you have chosen')));
            }

            if(is_null($provider->api_username) || $provider->api_username =="" || $provider->api_username ==" "){
                return response()->json(array('status' => 'error' , 'msg' =>'There is no user name to send messages from the service provider you have selected'));
            }

            $sender = $provider->sender;
            $username=$provider->api_username;
            $password=$provider->api_password;
            $api_url=$provider->api_url;
            $api_sms_balance_url=$provider->api_sms_balance_url;
            $api_url = str_replace('$username',$username, $api_url);
            $api_url = str_replace('$password',$password, $api_url);
            $api_url = str_replace('$sender',$sender, $api_url);

            $cont_type = $request->cont_type;  // 0 for primary_mobile 1 for wataniya
            if($cont_type == 0 || $cont_type == '0'){
                $contact_type = 'wataniya';
            }else{
                $contact_type = 'primary_mobile';
            }

            $target = $request->target;
            $sentToPerson = false;
            if($target == 'project' || $target == 'central_project'){
                if($request->org_pers == 1){
                    $items = Organization::getMobileNumbers($cont_type,$target,$request->receipt);
                }else{
                    $sentToPerson = true;
                    $items = PersonContact::getMobileNumbers($contact_type,$request->receipt,$target);
                }

            }
            elseif($target == 'organization'){
                $items = Organization::getMobileNumbers($cont_type,$target,$request->receipt);
            }elseif($target == 'users'){
                $items = User::getMobileNumbers($cont_type,$target,$request->receipt);
            }elseif($target == 'citizens'){
                $sentToPerson = true;
                $items = Citizen::getMobileNumbers($cont_type,$target,$request->receipt);
            }else{
                $sentToPerson = true;
                $target = 'person';
                $items = PersonContact::getMobileNumbers($contact_type,$request->receipt,$target);
            }

            if(sizeof($items) == 0 ){
                return response()->json(array('status' => 'error' , 'msg' =>trans('sms::application.There are no registered numbers to who send the message from them')));
            }

            $has_voucher = false;
            if ($request->voucher_id){
                $has_voucher = true;
            }

            $sent=0;
            $not_sent = 0;
            $returns = [];

            foreach ($items as $key => $value) {
                if(is_null($value->mobile)){
                    $returns[]= ['name' => $value->name , 'reason' => trans('sms::application.There is no number to send the message to')];
                }else{
                    if($request->is_code){
                        $msg = str_replace("name", $value->name,  $request->sms_messege);


                        if ($has_voucher){
                            $voucher = VoucherPersons::beneficiaryDetails($request->voucher_id,$value->person_id);
                            if ($voucher){
                                $msg = str_replace("title", $voucher->voucher_title , $msg );
                                $msg = str_replace("location", $voucher->receipt_location , $msg );
                                $msg = str_replace("sh_value", $voucher->sh_value , $msg );
                            }
                        }

                        if($sentToPerson){
                            $final_msg = str_replace("card", $value ->id_card_number,  $msg);
                        }else{
                            $final_msg = $msg;
                        }

                    }else{
                        $final_msg = $request->sms_messege;
                    }

                    $final_msg = str_replace("\n",' ' ,  $final_msg);

                    $sub_api_url = str_replace('$mobile_number', '972'.ltrim($value->mobile, "0") , $api_url);
                    $sub_api_url = str_replace('$message',urlencode($final_msg), $sub_api_url);

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_URL, $sub_api_url);
                    $sms = curl_exec($ch);

                    if ($sms === 'error') {
                        $returns[]= ['name' => $value->name , 'reason' => trans('sms::application.There is not enough credit to send')];
                    }
                    else{
                        if ($sms == -110){
                            $returns[]= ['name' => $value->name , 'reason' => trans('sms::application.There is not enough credit to send')];
                        }
                        else if ($sms == -100){
                            $returns[]= ['name' => $value->name , 'reason' =>trans('sms::application.There is not enough credit to send')];
                        }
                        else if ($sms == -115){
                            $returns[]= ['name' => $value->name , 'reason' => trans('sms::application.There is not enough credit to send')];
                        }
                        else if ($sms == -116){
                            $returns[]= ['name' => $value->name , 'reason' => trans('sms::application.There is not enough credit to send')];
                        }
                        else if ($sms == -113){
                            $not_sent++;
                            $returns[]= ['name' => $value->name , 'reason' => trans('sms::application.There is not enough credit to send')];
                        }
                        else {
                            $return = explode(":",$sms);
                            if($return['0'] == '1'){
                                $sent++;
                                \Log\Model\Log::saveNewLog('SENT_SMS', trans('sms::application.Has sent SMS messages to') . $value->name );
                            }elseif($return['0'] == '-2'){
                                $not_sent++;
                                $returns[]= ['name' => $value->name , 'reason' => trans('sms::application.The service provider is not sending the message to your country')];
                            }elseif($return['0'] == '999'){
                                $not_sent++;
                                $returns[]= ['name' => $value->name , 'reason' => trans('sms::application.the bug has occurred at the service provider')];
                            }
                        }
                    }
                }

            }

            if($sent == sizeof($request->receipt)){
                return response()->json(array('status' => 'success' ,$api_url, 'msg' =>trans('sms::application.sent succesfully')));
            }
            if(sizeof($request->receipt) == 1 && sizeof($returns) == 1){
                return response()->json(array('status' => 'error' , 'msg' => $returns[0]['reason']));
            }
            if(sizeof($returns) > 0){

                $token = md5(uniqid());
                $data=array();
                foreach($returns as $key =>$value){
                    $data[$key][trans('aid::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        $data[$key][trans('aid::application.' . $k)]= $v;
                    }
                }
                \Excel::create('export_' . $token, function($excel) use($data) {
                    $excel->setTitle(trans('aid::application.sendSms'));
                    $excel->setDescription(trans('aid::application.sendSms'));
                    $excel->sheet(trans('sms::Message access record'), function($sheet) use($data) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);
                        $sheet->getStyle("A1:C1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->fromArray($data);
                    });
                })->store('xlsx', storage_path('tmp/'));

                if($sent > 0){
                    return response()->json(['download_token' => $token,
                        'status'=>'success','msg'=>trans('sms::application.action success ,A file was downloaded with the numbers that the messages did not receive and the reason for not reaching')]);

                }else{
                    return response()->json(['download_token' => $token,
                        'status'=>'success','msg'=>trans('sms::application.action faild ,A file was downloaded with the numbers that the messages did not receive and the reason for not reaching')]);

                }
            }
            return response()->json(array('status' => 'error' , 'msg' =>trans('sms::application.An unexpected error occurred')));
    }

    // send sms to group just by mobiles number
    public function sendSms(Request $request){

        try {
            $this->authorize('sentSms', SmsProviders::class);
            $response = array();

            $rules= [
                'sms_provider'   => 'required',
                'sms_messege'    => 'required',
                'mobile_number'    => 'required'
            ];
            $input=[
                'sms_provider'     => strip_tags($request->get('sms_provider')),
                'sms_messege' => strip_tags($request->get('sms_messege')),
                'mobile_number' => $request->get('mobile_number')
            ];

            $error = \App\Http\Helpers::isValid($input,$rules);
            if($error)
                return response()->json($error);

            $numbers = implode(',', $input['mobile_number']);
            $provider = \Sms\Model\SmsProviders::getProvider($input['sms_provider']);

            $sender = $provider->sender;
            $username=$provider->api_username;
            $password=$provider->api_password;
            $api_url=$provider->api_url;
            $api_sms_balance_url=$provider->api_sms_balance_url;

            $api_url = str_replace('$username',$username, $api_url);
            $api_url = str_replace('$password',$password, $api_url);
            $api_url = str_replace('$mobile_number',$numbers, $api_url);
            $api_url = str_replace('$sender',$sender, $api_url);
            $api_url = str_replace('$message',urlencode($input['sms_messege']), $api_url);
//            $api_url = str_replace(' ', '%20', $api_url);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $api_url);
            $sms = curl_exec($ch);
//            $sms= @file_get_contents($api_url);

            if ($sms === 'error') {
                return response()->json(array('status' => 'error' , 'msg' =>trans('sms::application.Invalid scan link')));
            }
            else{
                if ($sms == -110){
                    return response()->json(array('status' => 'error' , 'msg' => trans('sms::application.Cannot send There is an error in the username or password')));
                }
                else if ($sms == -100){
                    return response()->json(array('status' => 'error' , 'msg' => trans('sms::application.The link is incorrect because there is no important data in the send link')));
                } else if ($sms == -113){
                    return response()->json(array('status' => 'error' , 'msg'=>trans('sms::application.There is not enough credit to send')));
                } else if ($sms == -115){
                    return response()->json(array('status' => 'error' , 'msg' => trans('sms::application.The sender is not enabled')));
                }else if ($sms == -116){
                    return response()->json(array('status' => 'error' , 'msg' => trans("sms::application.Cannot send because the sender name does not exist")));
                }else {
                    $return = explode(":",$sms);
                    if($return['0'] == '1'){
                        \Log\Model\Log::saveNewLog('SENT_SMS',trans('sms::application.Has sent SMS messages to'));
                        $api_sms_balance_url = str_replace('$username',$username, $api_sms_balance_url);
                        $api_sms_balance_url = str_replace('$password',$password, $api_sms_balance_url);
                        $sms_balance= @file_get_contents($api_sms_balance_url);
                        return response()->json(array('status' => 'success' , 'msg' => trans('sms::application.sent succesfully') ,'sms_balance'=>$sms_balance));
                    }elseif($return['0'] == '-2'){
                        return response()->json(array('status' => 'error' , 'msg' => trans('sms::application.The service provider is not sending the message to your country')));
                    }elseif($return['0'] == '999'){
                        return response()->json(array('status' => 'error' , 'msg' => trans('sms::application.the bug has occurred at the service provider')));
                    }
                }
            }

            return response()->json(array('status' => 'error' , 'msg' =>trans('sms::application.An unexpected error occurred')));

        }
        catch (Exception $e) {
            $response["status"] = 'error';
            $response["error_code"] = $e->getCode();
            $response["error_message"] = $e->getMessage();
            return response()->json($response);
        }

    }

    // send sms to group just by mobiles number
    public function sendMessage(Request $request){

        try {

            $this->authorize('sentSms', SmsProviders::class);
            $response = array();

            $rules= [
                'sms_provider'   => 'required',
                'sms_messege'    => 'required',
                'mobile_number'    => 'required'
            ];
            $input=[
                'sms_provider'     => strip_tags($request->sms_provider),
                'sms_messege' => strip_tags($request->sms_messege),
                'mobile_number' => $request->mobile_number
            ];

            $error = \App\Http\Helpers::isValid($input,$rules);
            if($error)
                return response()->json($error);

            $mobilenumberList = $input['mobile_number'];

            $provider = \Sms\Model\SmsProviders::getProvider($input['sms_provider']);

            $sender = $provider->sender;
            $username=$provider->api_username;
            $password=$provider->api_password;
            $api_url=$provider->api_url;
            $api_sms_balance_url=$provider->api_sms_balance_url;

            $api_url = str_replace('$username',$username, $api_url);
            $api_url = str_replace('$password',$password, $api_url);
            $api_url = str_replace('$mobile_number',$mobilenumberList, $api_url);
            $api_url = str_replace('$sender',$sender, $api_url);
            $api_url = str_replace('$message',urlencode($input['sms_messege']), $api_url);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_URL, $api_url);
            $sms = curl_exec($ch);

            if ($sms === 'error') {
                return array('status' => 'error' , 'msg' =>trans('sms::application.Invalid scan link'));
            }
            else{
                if ($sms == -110){
                    return array('status' => 'error' , 'msg' => trans('sms::application.Cannot send There is an error in the username or password'));
                }
                else if ($sms == -100){
                    return array('status' => 'error' , 'msg' =>trans('sms::application.Cannot send There is an error in the username or password'));
                } else if ($sms == -113){
                    return array('status' => 'error' , 'msg'=> trans('sms::application.There is not enough credit to send'));
                } else if ($sms == -115){
                    return array('status' => 'error' , 'msg' => trans('sms::application.The sender is not enabled'));
                }else if ($sms == -116){
                    return array('status' => 'error' , 'msg' => trans("sms::application.Cannot send because the sender name does not exist"));
                }else {
                    $return = explode(":",$sms);

                    if($return['0'] == '1'){
                        \Log\Model\Log::saveNewLog('SENT_SMS',trans('sms::application.Has sent SMS messages to'));
                        $api_sms_balance_url = str_replace('$username',$username, $api_sms_balance_url);
                        $api_sms_balance_url = str_replace('$password',$password, $api_sms_balance_url);
                        $sms_balance= @file_get_contents($api_sms_balance_url);
                        return array('status' => 'success' , 'msg' => trans('sms::application.sent succesfully') ,'sms_balance'=>$sms_balance);
                    }elseif($return['0'] == '-2'){
                        return array('status' => 'error' , 'msg' => trans('sms::application.The service provider is not sending the message to your country'));
                    }elseif($return['0'] == '999'){
                        return array('status' => 'error' , 'msg' => trans('sms::application.the bug has occurred at the service provider'));
                    }
                }
            }

            return array('status' => 'error' , 'msg' =>trans('sms::application.An unexpected error occurred'));

        }
        catch (Exception $e) {
            $response["status"] = 'error';
            $response["error_code"] = $e->getCode();
            $response["error_message"] = $e->getMessage();
            return $response;
        }

    }

}
