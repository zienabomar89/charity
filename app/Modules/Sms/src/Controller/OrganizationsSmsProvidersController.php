<?php

namespace Sms\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Sms\Model\OrganizationsSmsProviders;

class OrganizationsSmsProvidersController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get SmsProvidersSetting of Organization to login user organization
    public function index(Request $request)
    {
        $this->authorize('manage', OrganizationsSmsProviders::class);
        $user = \Auth::user();
        $query = OrganizationsSmsProviders::getSmsProviders($user->organization_id,$request->page,$request->itemsCount);
        $query->map(function ($q) {
            $q->sms_balance= trans('sms::application.The scan link is not registered') ;
            $api_sms_balance_url = $q->api_sms_balance_url;
            $username = $q->api_username;
            $password = $q->api_password;
            if(!(empty($api_sms_balance_url) || is_null($api_sms_balance_url) || ($api_sms_balance_url == ''))){

                $api_sms_balance_url = str_replace('$username',$username, $api_sms_balance_url);
                $api_sms_balance_url = str_replace('$password',$password, $api_sms_balance_url);

                $sms= @file_get_contents($api_sms_balance_url);

                if ($sms === false) {
                    $q->sms_balance=  trans('sms::application.Invalid scan link');
                }else{
                    if ($sms == -110){
                        $q->sms_balance = trans('sms::application.Cannot send There is an error in the username or password');
                    }
                    else if ($sms == -100){
                        $q->sms_balance =  trans('sms::application.The link is incorrect because there is no important data in the send link');

                    } else if ($sms == -113){
                        $q->sms_balance = '';

                    } else if ($sms == -115){
                        $q->sms_balance =  trans('sms::application.The sender is not enabled');

                    }else if ($sms == -116){
                     $q->sms_balance = trans("sms::application.Cannot send because the sender name does not exist");
                 }else {
                    $q->sms_balance = $sms ;
                }
            }
        }

        return $q;
    });

        return response()->json($query);
    }

    // list SmsProvidersSetting of Organization to login user organization
    public function getList(Request $request)
    {
        $user = \Auth::user();
        return response()->json(['Service'=>OrganizationsSmsProviders::getSmsService($user->organization_id)]);
    }

    // create new SmsProvidersSetting of Organization to login user organization
    public function store(Request $request){
        $response = array();
        $this->authorize('create', OrganizationsSmsProviders::class);
        $user = \Auth::user();
        $rules= ['provider_id'   => 'required', 'api_password'   => 'required','api_username'    => 'required'];
        $input=[
            'provider_id'     => strip_tags($request->get('sms_provider')),
            'api_password'     => strip_tags($request->get('api_password')),
            'api_username' => strip_tags($request->api_username)
        ];

        if(OrganizationsSmsProviders::where(['provider_id'=> strip_tags($request->get('sms_provider')), 'organization_id'=> $user->organization_id])->first()){
            $response["status"]= 'failed_valid';
            $response["msg"]['sms_provider'][0]= trans('sms::application.Setup has already been listed for this provider');
            return response()->json($response);
        }

        $error = \App\Http\Helpers::isValid($input,$rules);
        if($error)
            return response()->json($error);


        $input['is_active']=1;
        $input['organization_id']=$user->organization_id;
        if(OrganizationsSmsProviders::Create($input)){
            \Log\Model\Log::saveNewLog( 'ORGANIZATIONS_SMS_PROVIDERS_CREATED', trans('sms::application.He added a service provider setting with a name').' ' .$input['api_username']);
            $response["status"]= 'success';
            $response["msg"]= trans('sms::application.The row is inserted to db');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sms::application.The row is not insert to db');

        }
        return response()->json($response);

    }

    // get SmsProvidersSetting of Organization to login user organization using id
    public function show($id)
    {
        $user = \Auth::user();
        $entry = OrganizationsSmsProviders::where(['provider_id' =>$id, 'organization_id' => $user->organization_id])->first();
        $this->authorize('view', $entry);
        return response()->json(['Providers' => $entry] );
    }

    // update data of SmsProvidersSetting of Organization to login user organization
    public function update(Request $request, $id)
    {
        $this->authorize('update',  OrganizationsSmsProviders::class);
        $rules= ['api_password'   => 'required', 'api_username'    => 'required'];

        $input=[
            'api_password'     => strip_tags($request->get('api_password')),
            'api_username' => strip_tags($request->api_username)];

            $error = \App\Http\Helpers::isValid($input,$rules);
            if($error)
                return response()->json($error);


            $user = \Auth::user();
            $update_1= OrganizationsSmsProviders::where(['provider_id'=>$id,'organization_id'=> $user ->organization_id])
            ->update(['api_username' => $input['api_username'] ,
              'api_password' => $input['api_password'] ]);
            if( $update_1)
            {
                \Log\Model\Log::saveNewLog('ORGANIZATIONS_SMS_PROVIDERS_UPDATED',trans('sms::application.He edited the SQL Server setup data as') .' ' .$input['api_username']);
                $response["status"]= 'success';
                $response["msg"]= trans('sms::application.The row is updated');
            }else{
                $response["status"]= 'failed';
                $response["msg"]= trans('sms::application.There is no update');
            }
            return response()->json($response);

        }

    // set default of SmsProvidersSetting of Organization to login user organization
    public function updateDefault(Request $request, $id)
    {
            $this->authorize('update',  OrganizationsSmsProviders::class);

            $if_has_default=null;
            $is_default=$request->get('status');
            $user = \Auth::user();

            if($is_default==1){

                if(OrganizationsSmsProviders::where(['provider_id'=>$id,'organization_id'=> $user ->organization_id])->update(['is_default' => 0]))
                {
                    \Log\Model\Log::saveNewLog('ORGANIZATIONS_SMS_PROVIDERS_UPDATED',trans('sms::application.Edit the default service provider'));
                    $response["status"]= 'success';
                    $response["msg"]= trans('sms::application.The row is updated');
                }else{
                    $response["status"]= 'failed';
                    $response["msg"]= trans('sms::application.There is no update');
                }

            }else{
                $if_has_default=OrganizationsSmsProviders::where('is_default','=' , 1 )
                ->where('organization_id', '=', $user ->organization_id)
                ->where('provider_id', '!=', $id)->first();

                if($if_has_default){
                    $response["status"]= 'alert';
                    $response["msg"]= trans('sms::application. have default providers');
                }else{

                    if(OrganizationsSmsProviders::where(['provider_id'=>$id,'organization_id'=> $user ->organization_id])->update(['is_default' => 1]))
                    {
                        \Log\Model\Log::saveNewLog('ORGANIZATIONS_SMS_PROVIDERS_UPDATED', trans('Edit the default service provider'));
                        $response["status"]= 'success';
                        $response["msg"]= trans('sms::application.The row is updated');
                    }else{
                        $response["status"]= 'failed';
                        $response["msg"]= trans('sms::application.There is no update');
                    }
                }
            }

            return response()->json($response);

    }

    // update active status of SmsProvidersSetting of Organization to login user organization
    public function updateActive(Request $request, $id)
    {
            $this->authorize('update',  OrganizationsSmsProviders::class);
            $user = \Auth::user();
            $organization_id=$user->organization_id;

            if(OrganizationsSmsProviders::where(['provider_id' =>$id, 'organization_id'=>$organization_id,
               'api_username'=>$request->api_username]
           )->update(['is_active' => $request->get('status')]))
            {
                \Log\Model\Log::saveNewLog('ORGANIZATIONS_SMS_PROVIDERS_UPDATED',trans('sms::application.Edit the service provider status'));
                $response["status"]= 'success';
                $response["msg"]= trans('sms::application.The row is updated');
            }else{
                $response["status"]= 'failed';
                $response["msg"]= trans('sms::application.There is no update');
            }
            
            return response()->json($response);

    }

    // delete exist SmsProvidersSetting model by provider_id,api_username
    public function destroy($id,Request $request)
    {

        $this->authorize('delete', OrganizationsSmsProviders::class);
        $user = \Auth::user();
        $organization_id=$user->organization_id;

        $entry =  OrganizationsSmsProviders::where(['provider_id' =>$id, 'organization_id'=>$organization_id, 'api_username'=>$request->api_username])->delete();
        if($entry)
        {
            \Log\Model\Log::saveNewLog('ORGANIZATIONS_SMS_PROVIDERS_DELETED',trans('sms::application.Deleted the SQL Server setup data for') .' ' .$request->api_username);
            $response["status"]= 'success';
            $response["msg"]= trans('sms::application.The row is deleted from db');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sms::application.The row is not deleted from db');

        }
        return response()->json($response);

    }

}
