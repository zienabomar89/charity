<?php

namespace Sms\Policy;
use Auth\Model\User;
use Sms\Model\OrganizationsSmsProviders;

class OrganizationsSmsProvidersPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('sms.organizationsSmsProviders.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('sms.organizationsSmsProviders.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user,OrganizationsSmsProviders $OrganizationsSmsProviders = null)
    {
        if ($user->hasPermission('sms.organizationsSmsProviders.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, OrganizationsSmsProviders $OrganizationsSmsProviders = null)
    {
        if ($user->hasPermission('sms.organizationsSmsProviders.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, OrganizationsSmsProviders $OrganizationsSmsProviders = null)
    {
        if ($user->hasPermission(['sms.organizationsSmsProviders.view','sms.organizationsSmsProviders.update'])) {
            return true;
        }

        return false;
    }

}

