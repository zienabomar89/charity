<?php

namespace Sms\Policy;

use Auth\Model\User;
use Sms\Model\SmsProviders;
class SmsProvidersPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('sms.smsProviders.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('sms.smsProviders.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, SmsProviders $smsProviders = null)
    {
        if ($user->hasPermission('sms.smsProviders.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, SmsProviders $smsProviders = null)
    {
        if ($user->hasPermission('sms.smsProviders.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, SmsProviders $smsProviders = null)
    {
        if ($user->hasPermission(['sms.smsProviders.view','sms.smsProviders.update'])) {
            return true;
        }

        return false;
    }
    public function sentSms(User $user)
    {
        if ($user->hasPermission('sms.smsProviders.sentSms')) {
            return true;
        }

        return false;
    }

}

