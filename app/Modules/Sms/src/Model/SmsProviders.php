<?php

namespace Sms\Model;

class SmsProviders extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_sms_providers';
    protected $fillable = ['name','Label','api_sms_balance_url','api_url','status'];


    public $timestamps = false;

    public function OrganizationsSmsProviders()
    {
        return $this->hasOne('Sms\Model\OrganizationsSmsProviders','provider_id', 'id');
    }

    public static function getProvider($sms_provider)
    {

        return \DB::table('char_sms_providers')
            ->join('char_organizations_sms_providers', 'char_organizations_sms_providers.provider_id', '=', 'char_sms_providers.id')
            ->where('char_organizations_sms_providers.provider_id','=',$sms_provider)
            ->select('char_sms_providers.Label as sender',
                     'char_sms_providers.api_url as api_url',
                     'char_sms_providers.api_sms_balance_url as api_sms_balance_url',
                     'char_organizations_sms_providers.api_username as api_username',
                     'char_organizations_sms_providers.api_password as api_password')
                ->first();

    }
}

