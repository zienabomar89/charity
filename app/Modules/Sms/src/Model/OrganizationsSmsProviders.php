<?php

namespace Sms\Model;
use App\Http\Helpers;


class OrganizationsSmsProviders extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_organizations_sms_providers';
    protected $fillable = ['provider_id','organization_id','api_username','api_password','is_active'];
    
    public $timestamps = false;
    
    public $incrementing = false;

    public function SmsProviders()
    {
        return $this->belongsTo('Sms\Model\SmsProviders', 'provider_id', 'id');
    }

    public static function getSmsProviders($organization_id,$page,$itemsCount=50)
    {

        $active = trans('auth::application.active');
        $inactive = trans('auth::application.inactive');

        $result=
          \DB::table('char_sms_providers')
                ->join('char_organizations_sms_providers', 'char_organizations_sms_providers.provider_id', '=', 'char_sms_providers.id')
                ->where('char_organizations_sms_providers.organization_id','=',$organization_id)
                ->orderBy('char_sms_providers.Label')
              ->selectRaw("char_sms_providers.status ,char_sms_providers.Label ,char_organizations_sms_providers.*,                        
                            char_sms_providers.api_sms_balance_url as api_sms_balance_url,
                            CASE
                                  WHEN char_organizations_sms_providers.is_active = 1 THEN '$active'
                                  WHEN char_organizations_sms_providers.is_active = 0 THEN '$inactive'
                             END
                             AS active,
                             CASE
                                  WHEN char_sms_providers.status = 1 THEN '$active'
                                  WHEN char_sms_providers.status = 0 THEN '$inactive'
                             END
                             AS pstatus");
                            
        $records_per_page = Helpers::recordsPerPage($itemsCount,$result->count());
        return $result->paginate($records_per_page);
    }

    public static function getSmsService($organization_id)
    {
        $result=
          \DB::table('char_organizations_sms_providers')
              ->join('char_sms_providers', function($q) {
                  $q->on('char_sms_providers.id', '=', 'char_organizations_sms_providers.provider_id');
                  $q->where('char_sms_providers.status',1);
              })
                ->where('char_organizations_sms_providers.organization_id','=',$organization_id)
                ->where('char_organizations_sms_providers.is_active','=',1)
                ->selectRaw("CONCAT(ifnull(char_sms_providers.Label,' ') , ' ' , ifnull(char_organizations_sms_providers.api_username, ' ')) AS Label,
                             char_sms_providers.id,char_organizations_sms_providers.*")

                ->get();

        return $result;
    }

}

