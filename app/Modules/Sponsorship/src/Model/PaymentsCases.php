<?php
namespace Sponsorship\Model;

use Common\Model\PersonModels\Guardian;
use App\Http\Helpers;

class PaymentsCases  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_payments_cases';
    protected $fillable = ['case_id','sponsor_number','amount','shekel_amount','amount_after_discount','shekel_amount_before_discount','guardian_id','payment_id','date_to','date_from','status','recipient'];

    protected $hidden = ['created_at','updated_at'];


    public function caseData()
    {
        return $this->belongsTo('Common\Model\CaseModel','case_id', 'id');
    }

    public function payment()
    {
        return $this->belongsTo('Sponsorship\Model\Payments','payment_id', 'id');
    }

    public static function getDistrictBeneficiary($payment_id,$district_id){

        $list =\DB::table('char_cases')
            ->join('char_persons as c','c.id',  '=', 'char_cases.person_id')
            ->join('char_payments_cases as p','p.case_id','=','char_cases.id')
            ->join('char_payments','char_payments.id','=','p.payment_id')
            ->leftjoin('char_payment_category_i18n', function($join) {
                $join->on('char_payment_category_i18n.payment_category_id', '=', 'char_payments.category_id')
                    ->where('char_payment_category_i18n.language_id', '=', 1);
            })
            ->leftjoin('char_persons AS g','g.id','=','p.guardian_id')
            ->where(function ($q) use ($district_id,$payment_id) {
                $q->whereRaw("(( c.governarate = $district_id and p.recipient = 0 ) or ( g.governarate = $district_id and p.recipient = 1 ))")
                    ->whereIn('char_payments.id',$payment_id)
                    ->where('p.amount','!=',0);
            });

        $list= $list->selectRaw("
                         p.sponsor_number,
                         CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ', ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' ')) AS case_name,
                         c.id_card_number, char_payment_category_i18n.name as payment_category,
                         g.id_card_number As guardian_card,
                         CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',
                                                        ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' ')) AS guardian_name,
                         p.date_from,
                         p.date_to,
                         p.amount,
                         p.shekel_amount,
                         p.amount_after_discount,
                         p.shekel_amount_before_discount,
                         CASE WHEN p.recipient is null THEN '-'
                             WHEN p.recipient = 0 THEN 
                                  CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ', ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))
                             WHEN p.recipient = 1 THEN 
                                  CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ', ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' '))
                         END  AS recipients_name
                   ");

        $list->orderBy('g.first_name')->orderBy('g.second_name')->orderBy('g.third_name')->orderBy('g.last_name');
        return $list->get();

    }
    public static function saveRecipient($payment_id,$case_id,$sponsor_number,$status)
    {

        $case=PaymentsCases::where(["payment_id"=>$payment_id,"case_id"=>$case_id,"sponsor_number"=>$sponsor_number])->first();
        $payment=\DB::table('char_payments')->where('char_payments.id',$payment_id)->first();
        $recipient=$case->recipient;
        $case= \DB::table('char_cases')
            ->join('char_persons','char_persons.id',  '=', 'char_cases.person_id')
            ->where('char_cases.id','=',$case_id)
            ->selectRaw("char_cases.organization_id,char_cases.person_id,char_persons.father_id,char_persons.mother_id")
            ->first();

        $recipient_id=null;
        $amount=0;

        if($status == 3){
            if($recipient ==0) {
                $recipient_id=$case->person_id;
                $rows= \DB::table('char_cases')
                    ->join('char_persons', function($q) {
                        $q->on('char_persons.id','=','char_cases.person_id');
                    })
                    ->join('char_payments_cases', function($q) {
                        $q->on('char_payments_cases.case_id','=','char_cases.id');
                        $q->where('char_payments_cases.status','!=',3);
                    })
                    ->where('char_payments_cases.payment_id','=',$payment_id)
                    ->where('char_cases.person_id',$recipient_id)
                    ->where('char_payments_cases.sponsor_number','!=',$sponsor_number)
                    ->selectRaw("count(char_payments_cases.sponsor_number) as rows_count,char_persons.father_id,char_persons.mother_id")
                    ->first();

                $count=$rows->rows_count;
                $father_id=$rows->father_id;
                $mother_id=$rows->mother_id;

                if($count != 0){
                    $total= \DB::select("select get_payments_recipient_amount ( ?, 'person', ? ) AS amount", [$recipient_id,$payment_id]);
                    if($total){
                        $amount=$total[0]->amount;
                    }
                }

            }
            else{

                $the_recipient=Guardian::where(['individual_id' =>$case->person_id,'organization_id'=>$case->organization_id,'status'=>1])->first();
                if($the_recipient){
                    $recipient_id=$the_recipient->guardian_id;
                }

                $rows= \DB::table('char_payments_cases')
                    ->join('char_cases','char_cases.id',  '=', 'char_payments_cases.case_id')
                    ->join('char_persons', function($q) {
                        $q->on('char_persons.id','=','char_cases.person_id');
                    })
                    ->where('char_payments_cases.status','!=',3)
                    ->where('char_payments_cases.payment_id','=',$payment_id)
                    ->where('char_payments_cases.guardian_id',$recipient_id)
                    ->where('char_payments_cases.sponsor_number','!=',$sponsor_number)
                    ->selectRaw("count(char_payments_cases.sponsor_number) as rows_count,char_persons.father_id,char_persons.mother_id")
                    ->first();

                $count=$rows->rows_count;
                $father_id=$rows->father_id;
                $mother_id=$rows->mother_id;

                if($count != 0){
                    $total= \DB::select("select get_payments_recipient_amount ( ?, 'guardian', ? ) AS amount", [$recipient_id,$payment_id]);
                    if($total){
                        $amount=$total[0]->amount;
                    }
                }

            }

            if($count == 0){
                PaymentsRecipient::where(["payment_id"=>$payment_id,"person_id"=>$recipient_id,'mother_id'=>$mother_id,'father_id'=>$father_id])->delete();
            }else{
                PaymentsRecipient::where(["payment_id"=>$payment_id,"person_id"=>$recipient_id,'mother_id'=>$mother_id,'father_id'=>$father_id])->update(['amount'=>$amount]);
            }
        }
        if($status == 2){
            if($recipient ==0) {
                $recipient_id=$case->person_id;
                $total= \DB::select("select get_payments_recipient_amount ( ?, 'person', ? ) AS amount", [$recipient_id,$payment_id]);
                if($total){
                    $amount=$total[0]->amount;
                }
            }
            else{
                $the_recipient=Guardian::where(['individual_id' =>$case->person_id,'organization_id'=>$case->organization_id,'status'=>1])->first();
                if($the_recipient){
                    $recipient_id=$the_recipient->guardian_id;
                    $total= \DB::select("select get_payments_recipient_amount ( ?, 'guardian', ? ) AS amount", [$recipient_id,$payment_id]);
                    if($total){
                        $amount=$total[0]->amount;
                    }
                }
            }

            $the_recipient= PaymentsRecipient::where(["payment_id"=>$payment_id,"person_id"=>$recipient_id,'father_id'=>$case->father_id,'mother_id'=>$case->mother_id])->first();
            if(!$the_recipient){
                $insert=['payment_id' => $payment_id, 'person_id' => $recipient_id, 'status' => 0,'amount' =>$amount,'father_id'=>$case->father_id,'mother_id'=>$case->mother_id];
                if ($payment->exchange_type == 2) {
                    $insert['bank_id']= $payment->bank_id;
                }else{
                    if ($payment->bank_id != null && $payment->exchange_type == 1) {
                        $insert['bank_id'] = $payment->bank_id;
                        $bank_account =\Common\Model\PersonModels\PersonBank::where(["person_id"=>$recipient_id,'bank_id'=>$payment->bank_id ])->first();
                        if($bank_account){
                            $insert['cheque_account_number'] = $bank_account->account_number;
                        }
                    }
                }
                PaymentsRecipient::create($insert);
            }
        }

        return true;
    }
    public static function exportCaseRecipientWithOptions($options){

        $shekel=trans('sponsorship::application.shekel');

        $language_id =  \App\Http\Helpers::getLocale();
        $return =\DB::table('char_cases')
            ->join('char_categories as ca','ca.id',  '=', 'char_cases.category_id')
            ->join('char_payments_cases as p', function($q)  {
                $q->on('p.case_id',  '=', 'char_cases.id');
                $q->whereIn('p.status',[1,2]);
            })
            ->join('char_payments', function($q)  {
                $q->on('char_payments.id','=','p.payment_id');
                $q->on('char_payments.organization_id','=','char_cases.organization_id');
            })
            ->leftjoin('char_payment_category_i18n', function($join) use ($language_id){
                $join->on('char_payments.category_id', '=', 'char_payment_category_i18n.payment_category_id')
                    ->where('char_payment_category_i18n.language_id', '=', $language_id);
            })
            ->join('char_currencies','char_currencies.id',  '=', 'char_payments.currency_id')
            ->join('char_organizations as org','org.id',  '=', 'char_payments.organization_id')
            ->join('char_organizations as sponsor','sponsor.id',  '=', 'char_payments.sponsor_id')
            ->join('char_persons as c','c.id',  '=', 'char_cases.person_id')
            ->leftjoin('char_persons AS g','g.id','=','p.guardian_id')
            ->leftjoin('char_guardians', function($q) {
                $q->on('char_cases.person_id','=','char_guardians.individual_id');
                $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                $q->where('char_guardians.status','=',1);
            })
            ->leftjoin('char_persons AS guardian','guardian.id','=','char_guardians.guardian_id')
            ->leftjoin('char_locations_i18n As mosques_name', function($join) use ($language_id)  {
                $join->on('guardian.mosques_id', '=','mosques_name.location_id' )
                    ->where('mosques_name.language_id', $language_id);
            })
            ->leftJoin('char_locations_i18n As person_location', function($join) use ($language_id){
                $join->on('guardian.location_id', '=','person_location.location_id' )
                    ->where('person_location.language_id',$language_id);
            })
            ->leftjoin('char_locations_i18n As person_city',function($join) use ($language_id){
                $join->on('guardian.city', '=','person_city.location_id' )
                    ->where('person_city.language_id',$language_id);
            })
            ->leftjoin('char_locations_i18n As person_district', function($join) use ($language_id){
                $join->on('guardian.governarate', '=','person_district.location_id' )
                    ->where('person_district.language_id',$language_id);
            })
            ->leftjoin('char_locations_i18n As person_country', function($join) use ($language_id){
                $join->on('guardian.country', '=','person_country.location_id' )
                    ->where('person_country.language_id',$language_id);
            })
            ->leftjoin('char_persons_contact as cont1', function($q) {
                $q->on('cont1.person_id', '=', 'c.id');
                $q->where('cont1.contact_type','phone');
            })
            ->leftjoin('char_persons_contact as cont2', function($q) {
                $q->on('cont2.person_id', '=', 'c.id');
                $q->where('cont2.contact_type','primary_mobile');
            })
            ->leftjoin('char_persons_contact as cont3', function($q) {
                $q->on('cont3.person_id', '=', 'c.id');
                $q->where('cont3.contact_type','secondery_mobile');
            })
            ->leftjoin('char_persons_contact as guardian_cont1', function($q) {
                $q->on('guardian_cont1.person_id', '=', 'guardian.id');
                $q->where('guardian_cont1.contact_type','phone');
            })
            ->leftjoin('char_persons_contact as guardian_cont2', function($q) {
                $q->on('guardian_cont2.person_id', '=', 'guardian.id');
                $q->where('guardian_cont2.contact_type','primary_mobile');
            })
            ->leftjoin('char_persons_contact as guardian_cont3', function($q) {
                $q->on('guardian_cont3.person_id', '=', 'guardian.id');
                $q->where('guardian_cont3.contact_type','secondery_mobile');
            })
            ->whereIn('char_payments.id',$options['payment_ids'])
            ->selectRaw(" CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ', ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' ')) AS case_name,
                           c.id_card_number,
                           char_payment_category_i18n.name as category_name,
                           org.name as organizations_name,
                           sponsor.name as sponsor_name,
                           p.sponsor_number as sponsor_number,
                           CONCAT(ifnull(guardian.first_name, ' '), ' ' ,ifnull(guardian.second_name, ' '),' ',
                                  ifnull(guardian.third_name, ' '),' ', ifnull(guardian.last_name,' ')) AS guardian_name,
                           g.id_card_number as guardian_id_card_number,
                           CASE WHEN guardian.country is null THEN '-' Else person_country.name  END   AS country_name,
                             CASE WHEN guardian.governarate is null THEN '-' Else person_district.name  END   AS governarate_name,
                             CASE WHEN guardian.city is null THEN '-' Else person_city.name  END   AS city_name,
                             CASE WHEN guardian.location_id is null THEN '-' Else person_location.name  END   AS location_name,
                             CASE WHEN guardian.mosques_id is null THEN '-' Else mosques_name.name  END   AS mosque_name,
                             CASE WHEN guardian.street_address is null THEN '-' Else guardian.street_address  END   AS street_address,
                         char_payments.payment_date,
                           CASE  WHEN char_payments.payment_exchange = 0 THEN char_currencies.name Else '$shekel' END AS currency_name,
                           CASE WHEN p.amount is null THEN '-' Else p.amount  END  AS amount,
                           CASE WHEN p.amount_after_discount is null THEN '-' Else p.amount_after_discount  END  AS amount_after_discount,
                           CASE WHEN p.shekel_amount_before_discount is null THEN '-' Else p.shekel_amount_before_discount  END  AS shekel_amount_before_discount,
                           CASE WHEN p.shekel_amount is null THEN '-' Else p.shekel_amount  END  AS shekel_amount,
                           CASE WHEN p.date_from is null THEN '-' Else p.date_from  END  AS date_from,
                           CASE WHEN p.date_to is null THEN '-' Else p.date_to  END  AS date_to,
                           CASE WHEN p.recipient is null THEN '-'
                                WHEN p.recipient = 0 THEN 
                                     CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',  ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))
                                WHEN p.recipient = 1 THEN 
                                     CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ', ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' '))
                           END  AS recipients_name,
                           CASE WHEN p.recipient is null THEN '-'
                                WHEN p.recipient = 0 THEN g.id_card_number
                                WHEN p.recipient = 1 THEN c.id_card_number
                     
                           END  AS recipient_id_card_number,
                           CASE WHEN p.recipient is null THEN '-'
                                WHEN p.recipient = 0 THEN guardian.id
                                WHEN p.recipient = 1 THEN char_cases.person_id
                           END  AS recipients_id,
                           CASE WHEN cont1.contact_value is null THEN '-' Else cont1.contact_value END   AS phone,
                           CASE WHEN cont2.contact_value is null THEN '-' Else cont2.contact_value END   AS primary_mobile,
                           CASE WHEN cont3.contact_value is null THEN '-' Else cont3.contact_value END   AS secondary_mobile,
                           CASE WHEN guardian_cont1.contact_value is null THEN '-' Else guardian_cont1.contact_value END   AS guardian_phone,
                           CASE WHEN guardian_cont2.contact_value is null THEN '-' Else guardian_cont2.contact_value END   AS guardian_primary_mobile,
                           CASE WHEN guardian_cont3.contact_value is null THEN '-' Else guardian_cont3.contact_value END   AS guardian_secondary_mobile
                   ")
            ->orderBy('p.created_at','desc');

        $query=$return->get();

        $ids=$options['payment_ids'];
        $pIds = join(",",$ids);
        $payment_exchange=$options['payment_exchange'];
        $shekel=trans('sponsorship::application.shekel');

        $get= trans('sponsorship::application.get');
        $not_get= trans('sponsorship::application.not get');

        if(sizeof($query) >0 ){

            foreach ($query as $key => $value){

                $recipients_id=$value->recipients_id;
                $value->recipient_amount =0;
                $value->recipient_amount_after_discount =0;
                $value->recipient_shekel_amount =0;
                $value->recipient_shekel_amount_before_discount =0;
                $value->cheque_account_number=null;
                $value->cheque_date=null;
                $value->rec_status=null;
                if($recipients_id){

                    $TheRecipient= \DB::select("select char_payments_recipient.*,
                                                CASE  WHEN char_payments_recipient.status = 0 THEN '$not_get' WHEN char_payments_recipient.status = 1 THEN '$get'  END AS status   
                                                FROM `char_payments_recipient`
                                                where ( (`char_payments_recipient`.`payment_id` IN($pIds)) and char_payments_recipient.`person_id` = $recipients_id) ");

                    if($TheRecipient){
                        $value->cheque_account_number=$TheRecipient[0]->cheque_account_number;
                        $value->cheque_date=$TheRecipient[0]->cheque_date;
                        $value->rec_status=$TheRecipient[0]->status;
                    }

                    $total= \DB::select("select SUM(`amount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where  (`char_payments_cases`.`payment_id` IN($pIds)) and
                                        ((char_cases.`person_id` = $recipients_id and recipient = 0) or( `guardian_id` = $recipients_id and recipient = 1 )) ");

                    if($total){ $value->recipient_amount=$total[0]->amount; }
//
                    $amount_after_discount= \DB::select("select SUM(`amount_after_discount`) as amount   FROM `char_payments_cases`
                                     join char_cases on char_cases .id = char_payments_cases.case_id
                                      where  (`char_payments_cases`.`payment_id` IN($pIds)) and
                                            ((char_cases.`person_id` = $recipients_id and recipient = 0) or( `guardian_id` = $recipients_id and recipient = 1 )) ");

                    if($amount_after_discount){$value->recipient_amount_after_discount =$amount_after_discount[0]->amount;}


                    $total_shekel_amount= \DB::select("select SUM(`shekel_amount`) as amount   FROM `char_payments_cases`
                                     join char_cases on char_cases .id = char_payments_cases.case_id
                                      where  (`char_payments_cases`.`payment_id` IN($pIds)) and
                                            ((char_cases.`person_id` = $recipients_id and recipient = 0) or( `guardian_id` = $recipients_id and recipient = 1 )) ");

                    if($total_shekel_amount){$value->recipient_shekel_amount=$total_shekel_amount[0]->amount;}


                    $shekel_amount_before_discount= \DB::select("select SUM(`shekel_amount_before_discount`) as amount   FROM `char_payments_cases`
                                     join char_cases on char_cases .id = char_payments_cases.case_id
                                      where  (`char_payments_cases`.`payment_id` IN($pIds)) and
                                            ((char_cases.`person_id` = $recipients_id and recipient = 0) or( `guardian_id` = $recipients_id and recipient = 1 )) ");

                    if($shekel_amount_before_discount){
                        $value->recipient_shekel_amount_before_discount =$shekel_amount_before_discount[0]->amount;
                    }
                }

            }
        }

        return $query;

    }
    public static function exportCaseRecipient($id){

        $shekel=trans('sponsorship::application.shekel');

        $return =\DB::table('char_cases')
            ->join('char_categories as ca','ca.id',  '=', 'char_cases.category_id')
            ->join('char_payments_cases as p', function($q)  {
                $q->on('p.case_id',  '=', 'char_cases.id');
                $q->whereIn('p.status',[1,2]);
            })
            ->join('char_payments', function($q)  {
                $q->on('char_payments.id','=','p.payment_id');
                $q->on('char_payments.organization_id','=','char_cases.organization_id');
            })
            ->join('char_currencies','char_currencies.id',  '=', 'char_payments.currency_id')
            ->join('char_organizations as org','org.id',  '=', 'char_payments.organization_id')
            ->join('char_organizations as sponsor','sponsor.id',  '=', 'char_payments.sponsor_id')
            ->join('char_persons as c','c.id',  '=', 'char_cases.person_id')
            ->leftjoin('char_persons AS g','g.id','=','p.guardian_id')
            ->leftjoin('char_guardians', function($q) {
                $q->on('char_cases.person_id','=','char_guardians.individual_id');
                $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                $q->where('char_guardians.status','=',1);
            })
            ->leftjoin('char_persons AS guardian','guardian.id','=','char_guardians.guardian_id')
            ->leftjoin('char_persons_contact as cont1', function($q) {
                $q->on('cont1.person_id', '=', 'c.id');
                $q->where('cont1.contact_type','phone');
            })
            ->leftjoin('char_persons_contact as cont2', function($q) {
                $q->on('cont2.person_id', '=', 'c.id');
                $q->where('cont2.contact_type','primary_mobile');
            })
            ->leftjoin('char_persons_contact as cont3', function($q) {
                $q->on('cont3.person_id', '=', 'c.id');
                $q->where('cont3.contact_type','secondery_mobile');
            })
            ->leftjoin('char_persons_contact as guardian_cont1', function($q) {
                $q->on('guardian_cont1.person_id', '=', 'guardian.id');
                $q->where('guardian_cont1.contact_type','phone');
            })
            ->leftjoin('char_persons_contact as guardian_cont2', function($q) {
                $q->on('guardian_cont2.person_id', '=', 'guardian.id');
                $q->where('guardian_cont2.contact_type','primary_mobile');
            })
            ->leftjoin('char_persons_contact as guardian_cont3', function($q) {
                $q->on('guardian_cont3.person_id', '=', 'guardian.id');
                $q->where('guardian_cont3.contact_type','secondery_mobile');
            })
            ->where('char_payments.id',$id)
            ->selectRaw(" CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ', ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' ')) AS case_name,
                           c.id_card_number,
                           ca.name as category_name,
                           org.name as organizations_name,
                           sponsor.name as sponsor_name,
                           p.sponsor_number as sponsor_number,
                           CONCAT(ifnull(guardian.first_name, ' '), ' ' ,ifnull(guardian.second_name, ' '),' ',
                                  ifnull(guardian.third_name, ' '),' ', ifnull(guardian.last_name,' ')) AS guardian_name,
                           c.id_card_number as guardian_id_card_number,
                           char_payments.payment_date,
                           CASE  WHEN char_payments.payment_exchange = 0 THEN char_currencies.name Else '$shekel' END AS currency_name,
                           CASE WHEN p.amount is null THEN '-' Else p.amount  END  AS amount,
                           CASE WHEN p.amount_after_discount is null THEN '-' Else p.amount_after_discount  END  AS amount_after_discount,
                           CASE WHEN p.shekel_amount_before_discount is null THEN '-' Else p.shekel_amount_before_discount  END  AS shekel_amount_before_discount,
                           CASE WHEN p.shekel_amount is null THEN '-' Else p.shekel_amount  END  AS shekel_amount,
                           CASE WHEN p.date_from is null THEN '-' Else p.date_from  END  AS date_from,
                           CASE WHEN p.date_to is null THEN '-' Else p.date_to  END  AS date_to,
                           CASE WHEN p.recipient is null THEN '-'
                                WHEN p.recipient = 0 THEN 
                                     CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',  ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))
                                WHEN p.recipient = 1 THEN 
                                     CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ', ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' '))
                           END  AS recipients_name,
                           CASE WHEN p.recipient is null THEN '-'
                                WHEN p.recipient = 0 THEN g.id_card_number
                                WHEN p.recipient = 1 THEN c.id_card_number
                     
                           END  AS recipient_id_card_number,
                           CASE WHEN p.recipient is null THEN '-'
                                WHEN p.recipient = 0 THEN guardian.id
                                WHEN p.recipient = 1 THEN char_cases.person_id
                           END  AS recipients_id,
                           CASE WHEN cont1.contact_value is null THEN '-' Else cont1.contact_value END   AS phone,
                           CASE WHEN cont2.contact_value is null THEN '-' Else cont2.contact_value END   AS primary_mobile,
                           CASE WHEN cont3.contact_value is null THEN '-' Else cont3.contact_value END   AS secondary_mobile,
                           CASE WHEN guardian_cont1.contact_value is null THEN '-' Else guardian_cont1.contact_value END   AS guardian_phone,
                           CASE WHEN guardian_cont2.contact_value is null THEN '-' Else guardian_cont2.contact_value END   AS guardian_primary_mobile,
                           CASE WHEN guardian_cont3.contact_value is null THEN '-' Else guardian_cont3.contact_value END   AS guardian_secondary_mobile
                   ")
            ->orderBy('p.created_at','desc');

        $query=$return->get();


        $get= trans('sponsorship::application.get');
        $not_get= trans('sponsorship::application.not get');

        if(sizeof($query) >0 ){

            foreach ($query as $key => $value){

                $recipients_id=$value->recipients_id;
                $value->recipient_amount =0;
                $value->recipient_amount_after_discount =0;
                $value->recipient_shekel_amount =0;
                $value->recipient_shekel_amount_before_discount =0;
                $value->cheque_account_number=null;
                $value->cheque_date=null;
                $value->rec_status=null;
                if($recipients_id){

                    $TheRecipient= \DB::select("select char_payments_recipient.*,CASE  WHEN char_payments_recipient.status = 0 THEN '$not_get'
                                WHEN char_payments_recipient.status = 1 THEN '$get'
                         END AS status   FROM `char_payments_recipient`
                                  where (char_payments_recipient.`payment_id`= $id and char_payments_recipient.`person_id` = $recipients_id) ");

                    if($TheRecipient){
                        $value->cheque_account_number=$TheRecipient[0]->cheque_account_number;
                        $value->cheque_date=$TheRecipient[0]->cheque_date;
                        $value->rec_status=$TheRecipient[0]->status;
                    }

                    $total= \DB::select("select SUM(`amount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where (`payment_id`= $id) and
                                        ((char_cases.`person_id` = $recipients_id and recipient = 0) or( `guardian_id` = $recipients_id and recipient = 1 )) ");

                    if($total){ $value->recipient_amount=$total[0]->amount; }

                    $amount_after_discount= \DB::select("select SUM(`amount_after_discount`) as amount   FROM `char_payments_cases`
                                     join char_cases on char_cases .id = char_payments_cases.case_id
                                      where (`payment_id`= $id) and
                                            ((char_cases.`person_id` = $recipients_id and recipient = 0) or( `guardian_id` = $recipients_id and recipient = 1 )) ");

                    if($amount_after_discount){$value->recipient_amount_after_discount =$amount_after_discount[0]->amount;}

                    $total_shekel_amount= \DB::select("select SUM(`shekel_amount`) as amount   FROM `char_payments_cases`
                                     join char_cases on char_cases .id = char_payments_cases.case_id
                                      where (`payment_id`= $id) and
                                            ((char_cases.`person_id` = $recipients_id and recipient = 0) or( `guardian_id` = $recipients_id and recipient = 1 )) ");

                    if($total_shekel_amount){$value->recipient_shekel_amount=$total_shekel_amount[0]->amount;}


                    $shekel_amount_before_discount= \DB::select("select SUM(`shekel_amount_before_discount`) as amount   FROM `char_payments_cases`
                                     join char_cases on char_cases .id = char_payments_cases.case_id
                                      where (`payment_id`= $id) and
                                            ((char_cases.`person_id` = $recipients_id and recipient = 0) or( `guardian_id` = $recipients_id and recipient = 1 )) ");

                    if($shekel_amount_before_discount){
                        $value->recipient_shekel_amount_before_discount =$shekel_amount_before_discount[0]->amount;
                    }
                }

            }
        }

        return $query;

    }
    // ***********************************************//
    public static function savePaymentCases($id,$inputs)
    {

        $action = $inputs['action'];

        $response=['status' => true];
        $cheque_number =null;
        $updated =[];

        $update=true;
        $required=false;

        if($action =='range'){
            if(isset($inputs['date_from'])){
                if($inputs['date_from'] != null || $inputs['date_from'] != ""){
                    $updated["date_from"]= date('Y-m-d',strtotime($inputs['date_from']));
                    $required=true;
                }
            }

            if(isset($inputs['date_to'])){
                if($inputs['date_to'] != null || $inputs['to'] != ""){
                    $updated["date_to"]= date('Y-m-d',strtotime($inputs['date_to']));
                    $required=true;
                }
            }


        }
        else if($action =='amount'){
            if($inputs['amount'] != null || $inputs['amount'] != ""){
                $amount =$inputs['amount'];
                $payment=\DB::table('char_payments')->where('char_payments.id',$id)->first();

                $count = \DB::table('char_payments_cases')
                    ->where(function($q) use($id) {
                        $q->where('char_payments_cases.status','!=',3);
                        $q->where('char_payments_cases.payment_id','=',$id);
                    })
                    ->count();
                $max_allowed_amount = $payment->amount - ($payment->amount * $payment->organization_share) - $payment->administration_fees;

                if($inputs['how']==1){
                    $new_amount = $count * ($amount - ($amount * $payment->organization_share));
                    $updated["amount"] =$inputs['amount'];
                }else{
                    $updated["amount"] =$inputs['amount']/$count;
                    $new_amount = $amount - ($amount * $payment->organization_share);
                }

                if($new_amount > $max_allowed_amount ){
                    return ["status" => "failed" ,"msg" => trans('sponsorship::application.The amount exceed the allowed amount')];
                }

                $updated['amount_after_discount']=$updated["amount"] - ($updated["amount"] * $payment->organization_share);
                $updated['shekel_amount_before_discount']= $updated["amount"] * $payment->exchange_rate;
                $updated['shekel_amount']=round( ( $updated["amount"] - ($updated["amount"] * $payment->organization_share)) * $payment->exchange_rate);
                $required=true;
            }
        }

        if($required == true){
            $items=\DB::table('char_payments_cases')
                ->where(function($q) use($id) {
                    $q->where('char_payments_cases.status','!=',3);
                    $q->where('char_payments_cases.payment_id','=',$id);
                })
                ->selectRaw("char_payments_cases.*")
                ->get();

            foreach($items as $k => $v){
                self::where(['case_id'=>$v->case_id,'sponsor_number'=>$v->sponsor_number,"payment_id"=>$id])->update($updated);
            }
            PaymentsRecipient::setRecipient($id);

            \Log\Model\Log::saveNewLog('PAYMENTS_CASES_UPDATED', trans('sponsorship::application.edited cases of beneficiaries of payment') . $id);
        }

        if($update) {
            $response["status"]= 'success';
            $response["msg"]= trans('sponsorship::application.The row is updated');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sponsorship::application.There is no update');
        }
        return $response;

    }
    // ******************************************* //
    public static function filter($page,$organization_id,$filters){
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $amount = 0;
        $condition = [];
        $c = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number','location_id'];
        $g = ['g_first_name', 'g_second_name', 'g_third_name', 'g_last_name', 'g_id_card_number'];
        $p = ['status'];
//        'organization_id',
        $char_payments = ['sponsor_id','sponsorship_id','amount', 'currency_id','status'];
        $char_cases = ['category_id'];

        foreach ($filters as $key => $value) {

            if(in_array($key, $c)) {
                if ( $value != "" ) {
                    $data = ['c.' . $key => $value];
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $char_cases)) {
                if ( $value != "" ) {
                    $data = ['char_cases.' . $key => $value];
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $p)) {
                if ( $value != "" ) {
                    $data = ['p.' . $key => $value];
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $g)) {

                if ( $value != "" ) {
                    $data = ['g.' . substr($key, 2) => $value];
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $char_payments)) {

                if ( $value != "" ) {
                    $data = ['char_payments.' . $key => $value];
                    array_push($condition, $data);
                }
            }

        }

        $list =\DB::table('char_cases')
            ->join('char_categories as ca','ca.id',  '=', 'char_cases.category_id')
            ->join('char_payments_cases as p', function($q)  {
                $q->on('p.case_id','=','char_cases.id');
//                $q->wherein('p.status',[1,2]);
            })
            ->join('char_payments', function($q)  {
                $q->on('char_payments.id','=','p.payment_id');
                $q->on('char_payments.organization_id','=','char_cases.organization_id');
            })
            ->join('char_currencies','char_currencies.id',  '=', 'char_payments.currency_id')
            ->join('char_organizations as org','org.id',  '=', 'char_payments.organization_id')
            ->join('char_organizations as sponsor','sponsor.id',  '=', 'char_payments.sponsor_id')
            ->join('char_persons as c','c.id',  '=', 'char_cases.person_id')
//            ->join('char_sponsorship_cases', function($q){
//                $q->on('char_sponsorship_cases.case_id','=','char_cases.id');
////                $q->on('char_sponsorship_cases.sponsor_id','=','char_payments.sponsor_id');
////                $q->on('char_sponsorship_cases.sponsor_number','=','p.sponsor_number');
//            })
            ->leftjoin('char_persons AS g','g.id','=','p.guardian_id');


        if (count($condition) != 0) {
            $list =  $list
                ->where(function ($q) use ($condition) {
                    $names = ['c.first_name', 'c.second_name', 'c.third_name', 'c.last_name',
                        'g.first_name', 'g.second_name', 'g.third_name', 'g.last_name'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $names)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });


        }
        $all_organization=null;
        if(isset($filters['all_organization'])){
            if($filters['all_organization'] ===true){
                $all_organization=0;
            }elseif($filters['all_organization'] ===false){
                $all_organization=1;

            }
        }

        if($all_organization ==0){

            $organizations=[];
            if(isset($filters['organization_ids']) && $filters['organization_ids'] !=null && $filters['organization_ids'] !="" && $filters['organization_ids'] !=[] && sizeof($filters['organization_ids']) != 0) {
                if($filters['organization_ids'][0]==""){
                    unset($filters['organization_ids'][0]);
                }
                $organizations =$filters['organization_ids'];
            }

            if(!empty($organizations)){
                $list->wherein('char_cases.organization_id',$organizations);
            }else{

                $user = \Auth::user();

                if($user->type == 2) {
                    $list->where(function ($anq) use ($organization_id,$user) {
                        $anq->where('char_cases.organization_id',$organization_id);
                        $anq->orwherein('char_cases.organization_id', function($decq) use($user) {
                            $decq->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });

                }else{

                    $list->where(function ($anq) use ($organization_id) {
                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                        $anq->wherein('char_cases.organization_id', function($decq) use($organization_id) {
                            $decq->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $organization_id);
                        });
                    });
                }

            }
        }
        elseif($all_organization ==1){
            $list->where('char_cases.organization_id',$organization_id);

        }

        $max_amount=null;
        $min_amount=null;

        if(isset($filters['max_amount']) && $filters['max_amount'] !=null && $filters['max_amount'] !=""){
            $max_amount=$filters['max_amount'];
        }
        if(isset($filters['min_amount']) && $filters['min_amount'] !=null && $filters['min_amount'] !=""){
            $min_amount=$filters['min_amount'];
        }

        if($max_amount != null && $min_amount != null) {
            $list = $list->whereRaw(" $min_amount <= (p.amount)");
            $list = $list->whereRaw(" $max_amount >= (p.amount)");
        }elseif($max_amount != null && $min_amount == null) {
            $list = $list->whereRaw(" $max_amount >= (p.amount)");
        }elseif($max_amount == null && $min_amount != null) {
            $list = $list->whereRaw(" $min_amount <= (p.amount)");
        }


        $end_date_from=null;
        $begin_date_from=null;

        if(isset($filters['end_date_from']) && $filters['end_date_from'] !=null){
            $end_date_from=date('Y-m-d',strtotime($filters['end_date_from']));
        }
        if(isset($filters['begin_date_from']) && $filters['begin_date_from'] !=null){
            $begin_date_from=date('Y-m-d',strtotime($filters['begin_date_from']));
        }
        if($begin_date_from != null && $end_date_from != null) {
            $list = $list->whereBetween( 'p.date_from', [ $begin_date_from, $end_date_from]);
        }elseif($begin_date_from != null && $end_date_from == null) {
            $list = $list->whereDate('p.date_from', '>=', $begin_date_from);
        }elseif($begin_date_from == null && $end_date_from != null) {
            $list = $list->whereDate('p.date_from', '<=', $end_date_from);
        }

        $begin_date_to=null;
        $end_date_to=null;

        if(isset($filters['begin_date_to']) && $filters['begin_date_to'] !=null){
            $begin_date_to=date('Y-m-d',strtotime($filters['begin_date_to']));
        }

        if(isset($filters['end_date_to']) && $filters['end_date_to'] !=null){
            $end_date_to=date('Y-m-d',strtotime($filters['end_date_to']));
        }

        if($begin_date_to != null && $end_date_to != null) {
            $list = $list->whereBetween( 'p.date_to', [ $begin_date_to, $end_date_to]);
        }elseif($begin_date_to != null && $end_date_to == null) {
            $list = $list->whereDate('p.date_to', '>=', $begin_date_to);
        }elseif($begin_date_to == null && $end_date_to != null) {
            $list = $list->whereDate('p.date_to', '<=', $end_date_to);
        }

        $end_payment_date=null;
        $start_payment_date=null;

        if(isset($filters['end_payment_date']) && $filters['end_payment_date'] !=null){
            $end_payment_date=date('Y-m-d',strtotime($filters['end_payment_date']));
        }
        if(isset($filters['start_payment_date']) && $filters['start_payment_date'] !=null){
            $start_payment_date=date('Y-m-d',strtotime($filters['start_payment_date']));
        }

        if($start_payment_date != null && $end_payment_date != null) {
            $list = $list->whereBetween( 'char_payments.payment_date', [ $start_payment_date, $end_payment_date]);
        }elseif($start_payment_date != null && $end_payment_date == null) {
            $list = $list->whereDate('char_payments.payment_date', '>=', $start_payment_date);
        }elseif($list == null && $end_payment_date != null) {
            $list = $list->whereDate('char_payments.payment_date', '<=', $end_payment_date);
        }

        $deposit = trans('common::application.Bank deposit');
        $check = trans('common::application.Bank check');
        $card = trans('common::application.ID card');
        $Internal_check = trans('common::application.Internal check');
        $finish = trans('common::application.finish');
        $done = trans('common::application.done');
        $in_progress = trans('common::application.in progress');

        $finish = trans('common::application.finish');
        $done = trans('common::application.done');
        $in_progress = trans('common::application.in progress');

        if($filters['action'] =='export'){
            $shekel=trans('sponsorship::application.shekel');

            $return= $list ->leftjoin('char_guardians', function($q) {
                $q->on('char_cases.person_id','=','char_guardians.individual_id');
                $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                $q->where('char_guardians.status','=',1);
            })
                ->leftjoin('char_persons AS guardian','guardian.id','=','char_guardians.guardian_id')
                ->leftjoin('char_persons_contact as cont1', function($q) {
                    $q->on('cont1.person_id', '=', 'c.id');
                    $q->where('cont1.contact_type','phone');
                })
                ->leftjoin('char_persons_contact as cont2', function($q) {
                    $q->on('cont2.person_id', '=', 'c.id');
                    $q->where('cont2.contact_type','primary_mobile');
                })
                ->leftjoin('char_persons_contact as cont3', function($q) {
                    $q->on('cont3.person_id', '=', 'c.id');
                    $q->where('cont3.contact_type','secondery_mobile');
                })
                ->leftjoin('char_persons_contact as guardian_cont1', function($q) {
                    $q->on('guardian_cont1.person_id', '=', 'guardian.id');
                    $q->where('guardian_cont1.contact_type','phone');
                })
                ->leftjoin('char_persons_contact as guardian_cont2', function($q) {
                    $q->on('guardian_cont2.person_id', '=', 'guardian.id');
                    $q->where('guardian_cont2.contact_type','primary_mobile');
                })
                ->leftjoin('char_persons_contact as guardian_cont3', function($q) {
                    $q->on('guardian_cont3.person_id', '=', 'guardian.id');
                    $q->where('guardian_cont3.contact_type','secondery_mobile');
                })
                ->leftjoin('char_persons_contact as pre_guardian_cont1', function($q) {
                    $q->on('pre_guardian_cont1.person_id', '=', 'g.id');
                    $q->where('pre_guardian_cont1.contact_type','phone');
                })
                ->leftjoin('char_persons_contact as pre_guardian_cont2', function($q) {
                    $q->on('pre_guardian_cont2.person_id', '=', 'g.id');
                    $q->where('pre_guardian_cont2.contact_type','primary_mobile');
                })
                ->leftjoin('char_persons_contact as pre_guardian_cont3', function($q) {
                    $q->on('pre_guardian_cont3.person_id', '=', 'g.id');
                    $q->where('pre_guardian_cont3.contact_type','secondery_mobile');
                })
                ->selectRaw(" p.sponsor_number as sponsor_num,
                            CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ', ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' ')) AS case_name,
                            CONCAT(ifnull(guardian.first_name, ' '), ' ' ,ifnull(guardian.second_name, ' '),' ',
                                   ifnull(guardian.third_name, ' '),' ', ifnull(guardian.last_name,' ')) AS guardian_name,
                                         c.id_card_number,
                                         ca.name as category_name,
                                         org.name as organizations_name,
                                         sponsor.name as sponsor_name,
                                         char_payments.payment_date,
                                         CASE  WHEN char_payments.payment_exchange = 0 THEN p.amount
                                                WHEN char_payments.payment_exchange = 1 THEN p.shekel_amount
                                         END AS amount,
                                         CASE  WHEN char_payments.payment_exchange = 0 THEN char_currencies.name
                                                WHEN char_payments.payment_exchange = 1 THEN '$shekel'
                                         END AS currency,
                                         CASE WHEN p.shekel_amount is null THEN '-' Else p.shekel_amount  END  AS shekel_amount,
                                         CASE WHEN p.date_from is null THEN '-' Else p.date_from  END  AS date_from,
                                         CASE WHEN p.date_to is null THEN '-' Else p.date_to  END  AS date_to,
                                         CASE WHEN char_payments.status is null THEN '-'
                                               WHEN char_payments.status = 1 THEN '$done'
                                               WHEN char_payments.status = 2 THEN '$finish'
                                               WHEN char_payments.status = 3 THEN '$in_progress'
                                          END  AS status,
                                          CASE  WHEN char_payments.exchange_type is  null THEN ' '
                                                WHEN char_payments.exchange_type = 1 THEN '$deposit'
                                                WHEN char_payments.exchange_type = 2 THEN '$check'
                                                WHEN char_payments.exchange_type = 4 THEN '$card'
                                                WHEN char_payments.exchange_type = 3 THEN '$Internal_check'
                                           END
                                              AS exchange_type,
                                         CASE WHEN p.recipient is null THEN '-'
                                              WHEN p.recipient = 0 THEN 
                                                    CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',
                                                        ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))
                                              WHEN p.recipient = 1 THEN 
                                              CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ', ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' '))
                                         END  AS recipients_name,
                                         CASE WHEN cont1.contact_value is null THEN '-' Else cont1.contact_value END   AS phone,
                                         CASE WHEN cont2.contact_value is null THEN '-' Else cont2.contact_value END   AS primary_mobile,
                                         CASE WHEN cont3.contact_value is null THEN '-' Else cont3.contact_value END   AS secondary_mobile,
                                         CASE WHEN guardian_cont1.contact_value is null THEN '-' Else guardian_cont1.contact_value END   AS guardian_phone,
                                         CASE WHEN guardian_cont2.contact_value is null THEN '-' Else guardian_cont2.contact_value END   AS guardian_primary_mobile,
                                         CASE WHEN guardian_cont3.contact_value is null THEN '-' Else guardian_cont3.contact_value END   AS guardian_secondary_mobile,
                                         CASE WHEN pre_guardian_cont1.contact_value is null THEN '-' Else pre_guardian_cont1.contact_value END   AS pre_guardian_phone,
                                         CASE WHEN pre_guardian_cont2.contact_value is null THEN '-' Else pre_guardian_cont2.contact_value END   AS pre_guardian_primary_mobile,
                                         CASE WHEN pre_guardian_cont3.contact_value is null THEN '-' Else pre_guardian_cont3.contact_value END   AS pre_guardian_secondary_mobile,
                                         CASE WHEN p.recipient is null THEN '-'
                                              WHEN p.recipient = 0 THEN cont1.contact_value
                                              WHEN p.recipient = 1 THEN pre_guardian_cont1.contact_value
                                         END  AS recipient_phone,
                                         CASE WHEN p.recipient is null THEN '-'
                                               WHEN p.recipient = 0 THEN cont2.contact_value
                                              WHEN p.recipient = 1 THEN pre_guardian_cont2.contact_value
                                        END  AS recipient_primary_mobile,
                                         CASE WHEN p.recipient is null THEN '-'
                                              WHEN p.recipient = 0 THEN cont3.contact_value
                                              WHEN p.recipient = 1 THEN pre_guardian_cont3.contact_value
                                      END  AS recipient_secondary_mobile

                   ")->orderBy('p.created_at','desc')->get();

        }else{
//        CASE WHEN char_sponsorship_cases.sponsor_number is null
//                              THEN '-'
//                         Else char_sponsorship_cases.sponsor_number
//                         END
//                         AS sponsor_num,
            $list= $list->selectRaw("

                         ca.name as category_name,
                         CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ', ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' ')) AS case_name,
                         c.id_card_number,
                         org.name as organizations_name,
                         p.sponsor_number as sponsor_num,
                         p.date_from,
                         p.date_to,
                         sponsor.name as sponsor_name,
                         p.status,
                         char_currencies.name as currency,
                         p.amount,
                         p.shekel_amount,
                         CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',
                                                        ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' ')) AS guardian_name,
                        CASE WHEN p.recipient is null THEN '-'
                                              WHEN p.recipient = 0 THEN 
                                                    CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',
                                                        ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))
                                              WHEN p.recipient = 1 THEN 
                                              CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ', ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' '))
                                         END  AS recipients_name,
                         char_payments.exchange_rate,
                         char_payments.payment_exchange,
                         char_payments.payment_date,
                         c.id,
                                                  char_payments.id,

                         char_cases.id as case_id
                   ");
            $map = [
                "sponsor_number" => 'p.sponsor_number',
                "category_name" => 'ca.name',
                "id_card_number" => 'c.id_card_number',
                "organizations_name" => 'org.name',
                "sponsor_name" => 'sponsor.name',
                "payment_exchange" => 'char_payments.payment_exchange',
                "exchange_rate" => 'char_payments.exchange_rate',
                "amount" => 'p.amount',
                "amount_in_shekel" => 'p.shekel_amount',
                "case_name" => 'c.first_name',
                "guardian_name" => 'g.first_name',
                "recipients_name" => 'c.first_name',
                "date_from" => 'p.date_from',
                "date_to" => 'p.date_to',
                "status" => 'p.status'
            ];
            $order = false;

            if (isset($filters['sortKeyArr_rev']) && $filters['sortKeyArr_rev'] != null && $filters['sortKeyArr_rev'] != "" &&
                $filters['sortKeyArr_rev'] != [] && sizeof($filters['sortKeyArr_rev']) != 0
            ) {

                $order = true;

                foreach ($filters['sortKeyArr_rev'] as $key_) {

                    if($key_ == 'case_name'){
                        $list->orderBy('c.first_name','desc');
                        $list->orderBy('c.second_name','desc');
                        $list->orderBy('c.third_name','desc');
                        $list->orderBy('c.last_name','desc');
                    }elseif($key_ == 'guardian_name'){
                        $list->orderBy('g.first_name','desc');
                        $list->orderBy('g.second_name','desc');
                        $list->orderBy('g.third_name','desc');
                        $list->orderBy('g.last_name','desc');
                    }elseif($key_ == 'recipients_name'){
                        $list->orderBy('char_persons.first_name','desc');
                        $list->orderBy('char_persons.second_name','desc');
                        $list->orderBy('char_persons.third_name','desc');
                        $list->orderBy('char_persons.last_name','desc');
                    }else{
                        $list->orderBy($map[$key_],'desc');
                    }


                }
            }


            if (isset($filters['sortKeyArr_un_rev']) && $filters['sortKeyArr_un_rev'] != null && $filters['sortKeyArr_un_rev'] != "" &&
                $filters['sortKeyArr_un_rev'] != [] && sizeof($filters['sortKeyArr_un_rev']) != 0
            ) {

                $order = true;
                foreach ($filters['sortKeyArr_un_rev'] as $key_) {
                    if($key_ == 'case_name'){
                        $list->orderBy('c.first_name','asc');
                        $list->orderBy('c.second_name','asc');
                        $list->orderBy('c.third_name','asc');
                        $list->orderBy('c.last_name','asc');
                    }elseif($key_ == 'guardian_name'){
                        $list->orderBy('g.first_name','asc');
                        $list->orderBy('g.second_name','asc');
                        $list->orderBy('g.third_name','asc');
                        $list->orderBy('g.last_name','asc');
                    }elseif($key_ == 'recipients_name'){
                        $list->orderBy('char_persons.first_name','asc');
                        $list->orderBy('char_persons.second_name','asc');
                        $list->orderBy('char_persons.third_name','asc');
                        $list->orderBy('char_persons.last_name','asc');
                    }else{
                        $list->orderBy($map[$key_],'asc');
                    }
                }
            }


            if(!$order){
                $list->orderBy('char_payments.created_at','desc');
            }

             $itemsCount = isset($filters['itemsCount'])? $filters['itemsCount']:0;
             $records_per_page = Helpers::recordsPerPage($itemsCount,$list->count());
             $paginate =$list->orderBy('p.created_at','desc')->paginate($records_per_page);

            if($page == 1){
                $total    = $list->selectRaw("sum((p.shekel_amount)) as total")->first();
                $amount = $total->total;
            }
            return ['list' => $paginate, 'total' =>round($amount,0)];
        }

        return $return;

    }
    // ******************************************* //
    public static function filterForSponsor($page,$sponsor_id,$filters){

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $condition = [];
        $c = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number','location_id'];
        $g = ['g_first_name', 'g_second_name', 'g_third_name', 'g_last_name', 'g_id_card_number'];
        $p = ['status','payment_id'];
//        'organization_id',
        $char_payments = ['sponsorship_id','amount', 'currency_id','status'];
        $char_cases = ['category_id'];

        foreach ($filters as $key => $value) {

            if(in_array($key, $c)) {
                if ( $value != "" ) {
                    $data = ['c.' . $key => $value];
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $char_cases)) {
                if ( $value != "" ) {
                    $data = ['char_cases.' . $key => $value];
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $p)) {
                if ( $value != "" ) {
                    $data = ['p.' . $key => $value];
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $g)) {

                if ( $value != "" ) {
                    $data = ['g.' . substr($key, 2) => $value];
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $char_payments)) {

                if ( $value != "" ) {
                    $data = ['char_payments.' . $key => $value];
                    array_push($condition, $data);
                }
            }

        }
        $list =\DB::table('char_cases')
            ->join('char_categories as ca','ca.id',  '=', 'char_cases.category_id')
            ->join('char_payments_cases as p', function($q)  {
                $q->on('p.case_id','=','char_cases.id');
//                $q->wherein('p.status',[1,2]);
            })
            ->join('char_payments', function($q) use ($sponsor_id) {
                $q->on('char_payments.id','=','p.payment_id');
                $q->where('char_payments.sponsor_id','=',$sponsor_id);
            })
            ->join('char_currencies','char_currencies.id',  '=', 'char_payments.currency_id')
            ->join('char_organizations as org','org.id',  '=', 'char_payments.organization_id')
            ->join('char_organizations as sponsor','sponsor.id',  '=', 'char_payments.sponsor_id')
            ->join('char_persons as c','c.id',  '=', 'char_cases.person_id')
//            ->join('char_sponsorship_cases', function($q){
//                $q->on('char_sponsorship_cases.case_id','=','char_cases.id');
////                $q->on('char_sponsorship_cases.sponsor_id','=','char_payments.sponsor_id');
////                $q->on('char_sponsorship_cases.sponsor_number','=','p.sponsor_number');
//            })
            ->leftjoin('char_persons AS g','g.id','=','p.guardian_id');


        
        
        if (count($condition) != 0) {
            $list =  $list
                ->where(function ($q) use ($condition) {
                    $names = ['c.first_name', 'c.second_name', 'c.third_name', 'c.last_name',
                        'g.first_name', 'g.second_name', 'g.third_name', 'g.last_name'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $names)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });


        }

        if(isset($filters['organization_ids']) && $filters['organization_ids'] !=null && $filters['organization_ids'] !="" && $filters['organization_ids'] !=[] && sizeof($filters['organization_ids']) != 0) {
            if($filters['organization_ids'][0]==""){
                unset($filters['organization_ids'][0]);
            }
            $organizations =$filters['organization_ids'];
            if(!empty($organizations)){
                $list->wherein('char_cases.organization_id',$organizations);
            }
        }

        $max_amount=null;
        $min_amount=null;

        if(isset($filters['max_amount']) && $filters['max_amount'] !=null && $filters['max_amount'] !=""){
            $max_amount=$filters['max_amount'];
        }
        if(isset($filters['min_amount']) && $filters['min_amount'] !=null && $filters['min_amount'] !=""){
            $min_amount=$filters['min_amount'];
        }

        if($max_amount != null && $min_amount != null) {
            $list = $list->whereRaw(" $min_amount <= (p.amount)");
            $list = $list->whereRaw(" $max_amount >= (p.amount)");
        }elseif($max_amount != null && $min_amount == null) {
            $list = $list->whereRaw(" $max_amount >= (p.amount)");
        }elseif($max_amount == null && $min_amount != null) {
            $list = $list->whereRaw(" $min_amount <= (p.amount)");
        }


        $end_date_from=null;
        $begin_date_from=null;

        if(isset($filters['end_date_from']) && $filters['end_date_from'] !=null){
            $end_date_from=date('Y-m-d',strtotime($filters['end_date_from']));
        }
        if(isset($filters['begin_date_from']) && $filters['begin_date_from'] !=null){
            $begin_date_from=date('Y-m-d',strtotime($filters['begin_date_from']));
        }
        if($begin_date_from != null && $end_date_from != null) {
            $list = $list->whereBetween( 'p.date_from', [ $begin_date_from, $end_date_from]);
        }elseif($begin_date_from != null && $end_date_from == null) {
            $list = $list->whereDate('p.date_from', '>=', $begin_date_from);
        }elseif($begin_date_from == null && $end_date_from != null) {
            $list = $list->whereDate('p.date_from', '<=', $end_date_from);
        }

        $begin_date_to=null;
        $end_date_to=null;

        if(isset($filters['begin_date_to']) && $filters['begin_date_to'] !=null){
            $begin_date_to=date('Y-m-d',strtotime($filters['begin_date_to']));
        }

        if(isset($filters['end_date_to']) && $filters['end_date_to'] !=null){
            $end_date_to=date('Y-m-d',strtotime($filters['end_date_to']));
        }

        if($begin_date_to != null && $end_date_to != null) {
            $list = $list->whereBetween( 'p.date_to', [ $begin_date_to, $end_date_to]);
        }elseif($begin_date_to != null && $end_date_to == null) {
            $list = $list->whereDate('p.date_to', '>=', $begin_date_to);
        }elseif($begin_date_to == null && $end_date_to != null) {
            $list = $list->whereDate('p.date_to', '<=', $end_date_to);
        }

        $end_payment_date=null;
        $start_payment_date=null;

        if(isset($filters['end_payment_date']) && $filters['end_payment_date'] !=null){
            $end_payment_date=date('Y-m-d',strtotime($filters['end_payment_date']));
        }
        if(isset($filters['start_payment_date']) && $filters['start_payment_date'] !=null){
            $start_payment_date=date('Y-m-d',strtotime($filters['start_payment_date']));
        }

        if($start_payment_date != null && $end_payment_date != null) {
            $list = $list->whereBetween( 'char_payments.payment_date', [ $start_payment_date, $end_payment_date]);
        }elseif($start_payment_date != null && $end_payment_date == null) {
            $list = $list->whereDate('char_payments.payment_date', '>=', $start_payment_date);
        }elseif($list == null && $end_payment_date != null) {
            $list = $list->whereDate('char_payments.payment_date', '<=', $end_payment_date);
        }
        $deposit = trans('common::application.Bank deposit');
        $check = trans('common::application.Bank check');
        $card = trans('common::application.ID card');
        $Internal_check = trans('common::application.Internal check');
        $finish = trans('common::application.finish');
        $done = trans('common::application.done');
        $in_progress = trans('common::application.in progress');

        if($filters['action'] =='export'){
            
            if(isset($filters['persons'])){
            if(sizeof($filters['persons']) > 0 ){
               $list->whereIn('p.id',$filters['persons']);
            } 
        }  
            $shekel=trans('sponsorship::application.shekel');

            $return= $list ->leftjoin('char_guardians', function($q) {
                $q->on('char_cases.person_id','=','char_guardians.individual_id');
                $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                $q->where('char_guardians.status','=',1);
            })
                ->leftjoin('char_persons AS guardian','guardian.id','=','char_guardians.guardian_id')
                ->leftjoin('char_persons_contact as cont1', function($q) {
                    $q->on('cont1.person_id', '=', 'c.id');
                    $q->where('cont1.contact_type','phone');
                })
                ->leftjoin('char_persons_contact as cont2', function($q) {
                    $q->on('cont2.person_id', '=', 'c.id');
                    $q->where('cont2.contact_type','primary_mobile');
                })
                ->leftjoin('char_persons_contact as cont3', function($q) {
                    $q->on('cont3.person_id', '=', 'c.id');
                    $q->where('cont3.contact_type','secondery_mobile');
                })
                ->leftjoin('char_persons_contact as guardian_cont1', function($q) {
                    $q->on('guardian_cont1.person_id', '=', 'guardian.id');
                    $q->where('guardian_cont1.contact_type','phone');
                })
                ->leftjoin('char_persons_contact as guardian_cont2', function($q) {
                    $q->on('guardian_cont2.person_id', '=', 'guardian.id');
                    $q->where('guardian_cont2.contact_type','primary_mobile');
                })
                ->leftjoin('char_persons_contact as guardian_cont3', function($q) {
                    $q->on('guardian_cont3.person_id', '=', 'guardian.id');
                    $q->where('guardian_cont3.contact_type','secondery_mobile');
                })
                ->leftjoin('char_persons_contact as pre_guardian_cont1', function($q) {
                    $q->on('pre_guardian_cont1.person_id', '=', 'g.id');
                    $q->where('pre_guardian_cont1.contact_type','phone');
                })
                ->leftjoin('char_persons_contact as pre_guardian_cont2', function($q) {
                    $q->on('pre_guardian_cont2.person_id', '=', 'g.id');
                    $q->where('pre_guardian_cont2.contact_type','primary_mobile');
                })
                ->leftjoin('char_persons_contact as pre_guardian_cont3', function($q) {
                    $q->on('pre_guardian_cont3.person_id', '=', 'g.id');
                    $q->where('pre_guardian_cont3.contact_type','secondery_mobile');
                })
                ->selectRaw(" p.sponsor_number as sponsor_num,
                            CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ', ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' ')) AS case_name,
                            CONCAT(ifnull(guardian.first_name, ' '), ' ' ,ifnull(guardian.second_name, ' '),' ',
                                   ifnull(guardian.third_name, ' '),' ', ifnull(guardian.last_name,' ')) AS guardian_name,
                                         c.id_card_number,
                                         ca.name as category_name,
                                         org.name as organizations_name,
                                         sponsor.name as sponsor_name,
                                         char_payments.payment_date,
                                         CASE  WHEN char_payments.payment_exchange = 0 THEN p.amount
                                                WHEN char_payments.payment_exchange = 1 THEN p.shekel_amount
                                         END AS amount,
                                         CASE  WHEN char_payments.payment_exchange = 0 THEN char_currencies.name
                                                WHEN char_payments.payment_exchange = 1 THEN '$shekel'
                                         END AS currency,
                                         CASE WHEN p.shekel_amount is null THEN '-' Else p.shekel_amount  END  AS shekel_amount,
                                         CASE WHEN p.date_from is null THEN '-' Else p.date_from  END  AS date_from,
                                         CASE WHEN p.date_to is null THEN '-' Else p.date_to  END  AS date_to,
                                         CASE WHEN char_payments.status is null THEN '-'
                                              WHEN char_payments.status = 1 THEN '$done'
                                               WHEN char_payments.status = 2 THEN '$finish'
                                               WHEN char_payments.status = 3 THEN '$in_progress'
                                          END  AS status,
                                          CASE WHEN char_payments.exchange_type is  null THEN ' '
                                                WHEN char_payments.exchange_type = 1 THEN '$deposit'
                                                WHEN char_payments.exchange_type = 2 THEN '$check'
                                                WHEN char_payments.exchange_type = 4 THEN '$card'
                                                WHEN char_payments.exchange_type = 3 THEN '$Internal_check'
                                           END
                                              AS exchange_type,
                                              CASE WHEN p.recipient is null THEN '-'
                                              WHEN p.recipient = 0 THEN 
                                                    CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',
                                                        ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))
                                              WHEN p.recipient = 1 THEN 
                                              CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ', ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' '))
                                         END  AS recipients_name,
                                         CASE WHEN cont1.contact_value is null THEN '-' Else cont1.contact_value END   AS phone,
                                         CASE WHEN cont2.contact_value is null THEN '-' Else cont2.contact_value END   AS primary_mobile,
                                         CASE WHEN cont3.contact_value is null THEN '-' Else cont3.contact_value END   AS secondary_mobile,
                                         CASE WHEN guardian_cont1.contact_value is null THEN '-' Else guardian_cont1.contact_value END   AS guardian_phone,
                                         CASE WHEN guardian_cont2.contact_value is null THEN '-' Else guardian_cont2.contact_value END   AS guardian_primary_mobile,
                                         CASE WHEN guardian_cont3.contact_value is null THEN '-' Else guardian_cont3.contact_value END   AS guardian_secondary_mobile,
                                         CASE WHEN pre_guardian_cont1.contact_value is null THEN '-' Else pre_guardian_cont1.contact_value END   AS pre_guardian_phone,
                                         CASE WHEN pre_guardian_cont2.contact_value is null THEN '-' Else pre_guardian_cont2.contact_value END   AS pre_guardian_primary_mobile,
                                         CASE WHEN pre_guardian_cont3.contact_value is null THEN '-' Else pre_guardian_cont3.contact_value END   AS pre_guardian_secondary_mobile,
                                         CASE WHEN p.recipient is null THEN '-'
                                              WHEN p.recipient = 0 THEN cont1.contact_value
                                              WHEN p.recipient = 1 THEN pre_guardian_cont1.contact_value
                                         END  AS recipient_phone,
                                         CASE WHEN p.recipient is null THEN '-'
                                               WHEN p.recipient = 0 THEN cont2.contact_value
                                              WHEN p.recipient = 1 THEN pre_guardian_cont2.contact_value
                                        END  AS recipient_primary_mobile,
                                         CASE WHEN p.recipient is null THEN '-'
                                              WHEN p.recipient = 0 THEN cont3.contact_value
                                              WHEN p.recipient = 1 THEN pre_guardian_cont3.contact_value
                                      END  AS recipient_secondary_mobile

                   ")->orderBy('p.created_at','desc')->get();

        }else{
//        CASE WHEN char_sponsorship_cases.sponsor_number is null
//                              THEN '-'
//                         Else char_sponsorship_cases.sponsor_number
//                         END
//                         AS sponsor_num,
            $list= $list->selectRaw("

                         ca.name as category_name,
                         CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ', ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' ')) AS case_name,
                         c.id_card_number,
                         org.name as organizations_name,
                         p.sponsor_number as sponsor_num,
                         p.date_from,
                         p.date_to,
                         sponsor.name as sponsor_name,
                         p.status,
                         char_currencies.name as currency,
                         p.amount,
                         p.id as cid,
                         p.shekel_amount,
                         CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',
                                                        ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' ')) AS guardian_name,
                        CASE WHEN p.recipient is null THEN '-'
                                              WHEN p.recipient = 0 THEN 
                                                    CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',
                                                        ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))
                                              WHEN p.recipient = 1 THEN 
                                              CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ', ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' '))
                                         END  AS recipients_name,
                         char_payments.exchange_rate,
                         char_payments.payment_exchange,
                         char_payments.payment_date,
                         c.id,
                         char_payments.id,

                         char_cases.id as case_id
                   ");
            $itemsCount = isset($filters['itemsCount'])? $filters['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$list->count());
            $paginate =$list->orderBy('p.created_at','desc')->paginate($records_per_page);
            $amount = 0;
            if($page == 1 || $page == '1'){
                $total    = $list->selectRaw("sum((p.shekel_amount)) as total")->first();
                if(!is_null($total)){
                    $amount = $total->total;
                }
            }
            return ['list' => $paginate, 'total' =>round($amount,0)];
        }

        return $return;

    }
    // ******************************************* //

    public static function totalCaseAmountByTarget($id,$target){

        $return = \DB::table('char_payments_cases')
            ->where('char_payments_cases.payment_id',$id);

        $sum = 0;

            if($target == 1){
               $sum = $return->sum('char_payments_cases.amount');
            }else if($target == 2){
                $sum = $return->sum('char_payments_cases.amount_after_discount');
            }else if($target == 3){
                $sum = $return->sum('char_payments_cases.shekel_amount_before_discount');
            }else if($target == 4){
                $sum = $return->sum('char_payments_cases.shekel_amount');
            }

        return $sum;
    }

    public static function getCasesTotalAmount($id){
        $return=\DB::table('char_payments_cases')->where('char_payments_cases.payment_id',$id)->selectRaw("sum(char_payments_cases.amount) as total")->first();
        return $return->total;
    }
    public static function fetch($target,$id,$status,$paginate)
    {
        $query=\DB::table('char_payments_cases')
            ->join('char_cases','char_cases.id','char_payments_cases.case_id')
            ->leftjoin('char_categories As cat','cat.id', '=', 'char_cases.category_id')
            ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
            ->leftjoin('char_persons as f', 'char_persons.father_id', '=', 'f.id')
            ->leftjoin('char_persons as m', 'char_persons.mother_id', '=', 'm.id')
            ->leftjoin('char_guardians', function($q) {
                $q->on('char_guardians.individual_id','=','char_cases.person_id');
                $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                $q->where('char_guardians.status','=',1);
            })
            ->leftjoin('char_persons as g', 'char_guardians.guardian_id', '=', 'g.id');

        if($target=='guaranteed'){

            $query=$query->where(function($q) use($id) {
                $q->where('char_payments_cases.status','!=',3);
                $q->where('char_payments_cases.payment_id','=',$id);
            });

        }else{
            $query=$query->where(function($q) use($id,$status) {
                $filter = [ 1,2,3];
                if(in_array($status,$filter)){
                    $q->where('char_payments_cases.status',$status);
                }
                $q->where('char_payments_cases.payment_id','=',$id);
            });
        }

        $query=$query
            ->orderBy('char_persons.first_name')
            ->orderBy('char_persons.second_name')
            ->orderBy('char_persons.third_name')
            ->orderBy('char_persons.last_name')
            ->groupBy('char_payments_cases.case_id','char_payments_cases.sponsor_number')
            ->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                        ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                                 CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',
                                        ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' ')) AS guardian_name,
                                 CONCAT(ifnull(f.first_name, ' '), ' ' ,ifnull(f.second_name, ' '),' ',
                                        ifnull(f.third_name, ' '),' ', ifnull(f.last_name,' ')) AS father_name,
                                 CONCAT(ifnull(m.first_name, ' '), ' ' ,ifnull(m.second_name, ' '),' ',
                                        ifnull(m.third_name, ' '),' ', ifnull(m.last_name,' ')) AS mother_name,
                                 cat.name as category_name ,
                                 char_persons.id as person_id ,
                                 char_persons.father_id,
                                 char_persons.mother_id,
                                 g.id as guardian_id,
                                 char_cases.id as case_id ,
                                 char_persons.id_card_number,char_persons.old_id_card_number,
                                 char_payments_cases.*");

        if($paginate){
            return  $query->paginate(config('constants.records_per_page'));
        }else{
            return  $query->get();
        }

    }
    public static function get_($target,$payment_id,$payment_exchange)
    {
        $query=\DB::table('char_payments_cases')
            ->join('char_cases', function($q) {
                $q->on('char_cases.id','=','char_payments_cases.case_id');
            })
            ->join('char_categories As cat','cat.id', '=', 'char_cases.category_id')
            ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
            ->leftjoin('char_persons as f', 'char_persons.father_id', '=', 'f.id')
            ->leftjoin('char_persons as m', 'char_persons.mother_id', '=', 'm.id')
            ->leftjoin('char_guardians', function($q) {
                $q->on('char_guardians.individual_id','=','char_cases.person_id');
                $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                $q->where('char_guardians.status','=',1);
            })
            ->leftjoin('char_persons as g', 'char_guardians.guardian_id', '=', 'g.id');


        if($target=='guaranteed'){
            $query=$query->where('char_payments_cases.status','!=',3);
        }

        $query=$query->where('char_payments_cases.payment_id','=',$payment_id)
            ->orderBy('char_persons.first_name')
            ->orderBy('char_persons.second_name')
            ->orderBy('char_persons.third_name')
            ->orderBy('char_persons.last_name')
            ->groupBy('char_payments_cases.sponsor_number')
            ->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                         CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',
                                ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' ')) AS guardian_name,
                         CONCAT(ifnull(f.first_name, ' '), ' ' ,ifnull(f.second_name, ' '),' ',
                                ifnull(f.third_name, ' '),' ', ifnull(f.last_name,' ')) AS father_name,
                         CONCAT(ifnull(m.first_name, ' '), ' ' ,ifnull(m.second_name, ' '),' ',
                                ifnull(m.third_name, ' '),' ', ifnull(m.last_name,' ')) AS mother_name,
                         cat.name as category_name ,
                         char_persons.id as person_id ,
                         char_persons.father_id,
                         char_persons.mother_id,
                         g.id as guardian_id,
                         char_cases.id as case_id ,
                         char_persons.id_card_number,char_persons.old_id_card_number,
                         char_payments_cases.recipient ,
                         char_payments_cases.date_from ,
                         char_payments_cases.date_to ,
                         char_payments_cases.amount ,
                         char_payments_cases.shekel_amount ,
                         char_payments_cases.sponsor_number ,
                         char_payments_cases.status")
            ->get();

        return $query;
    }
    public static function getSub($action,$target,$payment_id)
    {
        $shekel=trans('sponsorship::application.shekel');
        $old_guaranteed=trans('sponsorship::application.old_guaranteed');
        $new_guaranteed=trans('sponsorship::application.new_guaranteed');
        $no_case_payment_amount=trans('sponsorship::application.no_case_payment_amount');

        $query=\DB::table('char_payments_cases')
            ->join('char_cases', function($q) {
                $q->on('char_cases.id','=','char_payments_cases.case_id');
            })
            ->join('char_categories As cat','cat.id', '=', 'char_cases.category_id')
            ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
            ->leftjoin('char_guardians', function($q) {
                $q->on('char_guardians.individual_id','=','char_cases.person_id');
                $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                $q->where('char_guardians.status','=',1);
            })
            ->leftjoin('char_persons as g', 'char_payments_cases.guardian_id', '=', 'g.id')
            ->join('char_payments','char_payments.id','=','char_payments_cases.payment_id')
            ->leftjoin('char_currencies','char_currencies.id',  '=', 'char_payments.currency_id')

            ->where('char_payments_cases.payment_id','=',$payment_id)
            ->orderBy('char_persons.first_name')
            ->orderBy('char_persons.second_name')
            ->orderBy('char_persons.third_name')
            ->orderBy('char_persons.last_name')
            ->groupBy('char_payments_cases.sponsor_number')
            ->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name,
                        cat.name as category_name ,
                        char_persons.id_card_number,char_persons.old_id_card_number,
                        char_payments_cases.sponsor_number,
                        CASE WHEN g.first_name is null THEN '-' Else  CONCAT(ifnull(g.first_name, ' '), ' ' ,ifnull(g.second_name, ' '),' ',
                                                        ifnull(g.third_name, ' '),' ', ifnull(g.last_name,' '))  END   AS guardian_name,
                        CASE WHEN g.id is null THEN '-' Else g.id_card_number  END   AS guardian_id_card_number,
                        CASE WHEN g.id is null THEN '-' Else g.old_id_card_number  END   AS guardian_old_id_card_number,
                        CASE WHEN char_payments_cases.date_from is null THEN '-' Else char_payments_cases.date_from  END  AS date_from,
                        CASE WHEN char_payments_cases.date_to is null THEN '-' Else char_payments_cases.date_to  END  AS date_to,
                        CASE  WHEN char_payments.currency_id is null THEN '-'   Else char_currencies.name END AS currency,
                        CASE WHEN char_payments_cases.amount is null THEN '0.0' Else char_payments_cases.amount  END  AS amount,
                        CASE WHEN char_payments_cases.shekel_amount is null THEN '0.0' Else char_payments_cases.shekel_amount  END  AS amount_in_shekel,
                        CASE  WHEN char_payments.payment_exchange = 0 THEN char_currencies.name
                              WHEN char_payments.payment_exchange = 1 THEN '$shekel'
                        END AS payment_currency,
                        CASE  WHEN char_payments.payment_exchange = 0 THEN char_payments_cases.amount
                              WHEN char_payments.payment_exchange = 1 THEN char_payments_cases.shekel_amount
                        END AS amount_in_payments_currency,
                        CASE WHEN char_payments_cases.status is null THEN '-'
                                           WHEN char_payments_cases.status = 1 THEN '$new_guaranteed'
                                           WHEN char_payments_cases.status = 2 THEN '$old_guaranteed'
                                           WHEN char_payments_cases.status = 3 THEN '$no_case_payment_amount'
                                      END  AS status
                       ");
//    CASE WHEN char_payments_cases.shekel_amount is null THEN '0.0' Else char_payments_cases.shekel_amount  END  AS shekel_amount,
//    CASE WHEN char_payments_cases.amount is null THEN '0.0' Else char_payments_cases.amount  END  AS amount_in_payments_currency,
//
        if($action =='guaranteed'){
            $query=$query->whereIn('char_payments_cases.status',[1,2]);
        }
        else{
            if((int)$target== 1){ $query=$query->where('char_payments_cases.status','=',1);}
            if((int)$target== 2){ $query=$query->where('char_payments_cases.status','=',2);}
            if((int)$target== 3){ $query=$query->where('char_payments_cases.status','=',3);}
            if((int)$target== 0){
//                $old_guaranteed=trans('sponsorship::application.old_guaranteed');
//                $new_guaranteed=trans('sponsorship::application.new_guaranteed');
//                $no_case_payment_amount=trans('sponsorship::application.no_case_payment_amount');
//                $query=$query->selectRaw("CASE WHEN char_payments_cases.status is null THEN '-'
//                                           WHEN char_payments_cases.status = 1 THEN '$new_guaranteed'
//                                           WHEN char_payments_cases.status = 2 THEN '$old_guaranteed'
//                                           WHEN char_payments_cases.status = 3 THEN '$no_case_payment_amount'
//                                      END  AS status
//                                      ");
            }
        }

        return $query->get();

    }

}

