<?php
namespace Sponsorship\Model;

class PaymentsDocuments  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_payments_documents';
    protected $fillable = ['document_id','payment_id','person_id','attachment_type','created_at'];
    public $timestamps = false;

    public function File()
    {
        return $this->belongsTo('Document\Model\File', 'document_id', 'id');
    }

    public static function getDocument($id,$person_id)
    {
        $query=\DB::table('char_payments_documents')
                ->join('doc_files as doc','doc.id',  '=', 'char_payments_documents.document_id')
                ->where('char_payments_documents.payment_id','=',$id);

        if($person_id != -1){
            $query=$query->where('char_payments_documents.person_id','=',$person_id);
        }else{
            $query=$query->whereNull('char_payments_documents.person_id');

        }

        $query=$query->select( 'doc.id as file_id','doc.name as name' ,'char_payments_documents.attachment_type')->get();

        foreach($query as $key => $item) {
            $parts = explode('.', $item->name);
            $start=$parts[0];
            $item->name=$start;
        }

        return $query;

    }
}


