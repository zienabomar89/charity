<?php
namespace Sponsorship\Model;

class SponsorshipsPerson  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_sponsorships_persons';

    protected $primaryKey = 'id';
    protected $fillable = ['sponsorship_id','person_id','status'];
    protected $hidden = ['created_at','updated_at'];

    public function Cases()
    {
        return $this->hasMany('Sponsorship\Model\Cases', 'person_id', 'id');
    }

    public function Sponsorships()
    {
        return $this->hasOne('Sponsorship\Model\Sponsorships', 'sponsor_id', 'sponsorship_id');
    }
    public function Person()
    {
        return $this->hasOne('Common\Model\Person', 'id', 'person_id');
    }

    public static function isPaassed($case_id,$sponsorship_id,$sponsor_id,$organization_id)
    {


        $details =\DB::table('char_cases')
                 ->join('char_categories as ca','ca.id',  '=', 'char_cases.category_id')
                 ->join('char_persons as c','c.id',  '=', 'char_cases.person_id')
                ->leftjoin('char_guardians', function($q){
                    $q->on('char_cases.person_id','=','char_guardians.individual_id');
                    $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                    $q->where('char_guardians.status','=',1);

                })
                ->where('char_cases.id','=',$case_id)
                ->selectRaw("
                                                     char_cases.organization_id,
                                                     char_cases.category_id,
                                                     char_cases.id,
                                                     c.id person_id,
                                                     c.father_id,
                                                     c.mother_id,
                                                     CASE
                                                       WHEN (ca.case_is = 1 and ca.family_structure= 2) THEN c.father_id
                                                       WHEN (ca.case_is = 1 and ca.family_structure=1) THEN c.id
                                                       WHEN (ca.case_is = 2 and ca.family_structure=1) THEN char_guardians.guardian_id
                                                     END
                                                     AS l_person_id,
                                                     c.id_card_number,
                                          CASE WHEN (ca.case_is = 1 and ca.family_structure= 2) THEN char_get_family_count ( 'left',c.father_id,null,c.id )
                                               WHEN (ca.case_is = 1 and ca.family_structure=1) THEN char_get_family_count ( 'left',c.id,null,c.id )
                                               WHEN (ca.case_is = 2 and ca.family_structure=1) THEN char_get_family_count ( 'left',char_guardians.guardian_id,null,c.id )
                                         END AS family_count")
                                                    ->first();

        $category_id=$details->category_id;
        $person_id=$details->person_id;
        $family_count=$details->family_count;
        $l_person_id=$details->l_person_id;

        $is_nominated =\DB::table('char_sponsorships_persons')
            ->where('char_sponsorships_persons.sponsorship_id','=',$sponsorship_id)
            ->where('char_sponsorships_persons.person_id','=',$person_id)
            ->first();


        if($is_nominated){
            return array('status'=>false,'reason'=>'nominated');
        }

        $is_nominated =\DB::table('char_sponsorship_cases')
            ->where('char_sponsorship_cases.sponsor_id','=',$sponsor_id)
            ->where('char_sponsorship_cases.sponsorship_id','=',$sponsorship_id)
            ->where('char_sponsorship_cases.case_id','=',$case_id)
            ->where('char_sponsorship_cases.sponsor_number','!=',$case_id)
            ->first();

        if($is_nominated){
            return array('status'=>false,'reason'=>'nominated');
        }

        $is_blocked= \DB::table('char_block_id_card_number')
            ->join('char_block_categories', function($q) use($category_id){
                $q->on('char_block_categories.block_id', '=', 'char_block_id_card_number.id');
                $q->where('char_block_categories.category_id', '=', $category_id);
            })
            ->where(function($q) use($details){
                $q->where('char_block_id_card_number.type','=',2);
                $q->where('char_block_id_card_number.id_card_number','=',$details->id_card_number);
            })

            ->first();

        if($is_blocked){
            return array('status'=>false,'reason'=>'blocked');
        }

        $ancestor_id = \Organization\Model\Organization::getAncestor($organization_id);

        $max_total_amount_of_family=\DB::table('char_settings')
            ->where('char_settings.id','=','max_total_amount_of_payments')
            ->where('char_settings.organization_id',$ancestor_id)
            ->first();

        $max_total=0;

        if($max_total_amount_of_family){
            $max_total=$max_total_amount_of_family->value;
        }

        $person_policy_restricted= \DB::table('char_categories_policy')
            ->where('char_categories_policy.category_id','=',$category_id)
            ->where('char_categories_policy.level','=',1)
            ->selectRaw("char_categories_policy.*")
            ->first();


        if($person_policy_restricted && $person_id != null){
            $payment_persons_cnt= \DB::table('char_persons')
                ->where('char_persons.id','=',$person_id)
                ->selectRaw("char_get_person_average_payments ( '$person_id' ,'$person_policy_restricted->started_at', '$person_policy_restricted->ends_at') as amount                             ")
                ->first();


            if($payment_persons_cnt){

                if($max_total_amount_of_family){
                    if($max_total <= ($payment_persons_cnt->amount)){
                        return array('status'=>false,'reason'=>'policy_restricted','sub'=>'restricted_total_payment_persons');
                    }
                }

                if($person_policy_restricted->count_of_family != 0){
                    if($person_policy_restricted->count_of_family <= $family_count){
                        return array('status'=>false,'reason'=>'policy_restricted','sub'=>'restricted_count_of_family');
                    }
                }

                if($person_policy_restricted->amount != null && $person_policy_restricted->amount != 0){

                    if($person_policy_restricted->amount <= ($payment_persons_cnt->amount)){
                        return array('status'=>false,'reason'=>'policy_restricted','sub'=>'restricted_amount');
                    }
                }
            }
        }

        $family_policy_restricted= \DB::table('char_categories_policy')
            ->where('char_categories_policy.category_id','=',$category_id)
            ->where('char_categories_policy.level','=',2)
            ->selectRaw("char_categories_policy.*")
            ->first();

        if($family_policy_restricted){

            if($details->father_id != null){
                $payment_family_cnt= \DB::table('char_persons')->where('char_persons.id','=',$person_id)
                    ->selectRaw(" char_get_family_total_payments ( '$details->father_id'  ,'$family_policy_restricted->started_at','$family_policy_restricted->ends_at') as amount                                ")
                    ->first();

                if($payment_family_cnt){
                    if($family_policy_restricted->count_of_family != 0){
                        if($family_policy_restricted->count_of_family <= $family_count){
                            return array('status'=>false,'reason'=>'policy_restricted','sub'=>'restricted_count_of_family');
                        }
                    }

                    if($family_policy_restricted->amount != null && $family_policy_restricted->amount != null){
                        if($family_policy_restricted->amount <= ($payment_family_cnt->amount)){
                            return array('status'=>false,'reason'=>'policy_restricted','sub'=>'restricted_amount');
                        }
                    }

                    if($max_total_amount_of_family){
                        if($max_total <= ($payment_family_cnt->amount)){
                            return array('status'=>false,'reason'=>'policy_restricted','sub'=>'restricted_total_payment_persons');
                        }
                    }
                }
            }
        }

        return array('status'=> true);
    }

    public static function fetch($id)
    {
        $language_id =  \App\Http\Helpers::getLocale();
        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $refugee = trans('common::application.refugee');
        $citizen = trans('common::application.citizen');

        $not_exist = trans('common::application.not_exist');
        $exist = trans('common::application.exist');

        $not_work = trans('common::application.not_work');
        $work = trans('common::application.work');

        $is_alive = trans('common::application.is_alive');
        $deceased = trans('common::application.deceased');

        $vocational = trans('common::application.vocational');
        $academic = trans('common::application.academic');

        $perfect = trans('common::application.perfect');
        $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
        $special_needs = trans('common::application.special_needs');

        $excellent = trans('common::application.excellent');
        $acceptable = trans('common::application.acceptable');

        $good = trans('common::application.good');
        $very_good = trans('common::application.very_good');
        $weak = trans('common::application.weak');
        $very_bad_condition = trans('common::application.very_bad_condition');
        $bad_condition = trans('common::application.bad_condition');

        $the_first  = trans('common::application.the_first') ;
        $the_second = trans('common::application.the_second') ;
        $the_third  = trans('common::application.the_third') ;
        $the_fourth = trans('common::application.the_fourth') ;
        $the_Fifth  = trans('common::application.the_Fifth');
        $the_Sixth  = trans('common::application.the_Sixth');
        $the_Seventh = trans('common::application.the_Seventh');
        $the_Eighth= trans('common::application.the_Eighth');
        $the_Ninth = trans('common::application.the_Ninth');
        $the_tenth = trans('common::application.the_tenth');
        $the_eleventh = trans('common::application.the_eleventh');
        $the_twelveth= trans('common::application.the_twelveth');

        $special_studies= trans('common::application.special_studies');
        $vocational_training= trans('common::application.vocational_training');
        $first_year_of_university= trans('common::application.first_year_of_university');
        $second_year_of_university= trans('common::application.second_year_of_university');
        $third_year_of_university= trans('common::application.third_year_of_university');
        $fourth_year_of_university= trans('common::application.fourth_year_of_university');
        $fifth_year_of_university= trans('common::application.fifth_year_of_university');

        $regularly = trans('common::application.regularly');
        $does_not_pray = trans('common::application.does_not_pray');
        $sometimes = trans('common::application.sometimes');

        $is_alive = trans('common::application.is_alive');
        $deceased = trans('common::application.deceased');

        $is_can_work = trans('common::application.is_can_work');
        $not_can_work = trans('common::application.not_can_work');

        return \DB::table('char_sponsorships_persons')
            ->join('char_persons as c','c.id','=','char_sponsorships_persons.person_id')
            ->leftjoin('char_persons AS f','f.id','=','c.father_id')
            ->leftjoin('char_persons AS m','m.id','=','c.mother_id')
            ->leftjoin('char_locations_i18n As L', function($q) use ($language_id){
                $q->on('L.location_id', '=', 'c.birth_place');
                $q->where('L.language_id',$language_id);
            })
            ->leftjoin('char_locations_i18n As L0', function($q) use ($language_id){
                $q->on('L0.location_id', '=', 'c.nationality');
                $q->where('L0.language_id',$language_id);
            })
            ->leftjoin('char_locations_i18n As L1', function($q) use ($language_id){
                $q->on('L1.location_id', '=', 'c.location_id');
                $q->where('L1.language_id',$language_id);
            })
            ->leftjoin('char_locations_i18n As L2', function($q) use ($language_id){
                $q->on('L2.location_id', '=', 'c.city');
                $q->where('L2.language_id',$language_id);
            })
            ->leftjoin('char_locations_i18n As L3', function($q) use ($language_id){
                $q->on('L3.location_id', '=', 'c.governarate');
                $q->where('L3.language_id',$language_id);
            })
            ->leftjoin('char_locations_i18n As L4', function($q) use ($language_id){
                $q->on('L4.location_id', '=', 'c.country');
                $q->where('L4.language_id',$language_id);
            })
            ->leftjoin('char_persons_contact as phone_num', function($q) {
                $q->on('phone_num.person_id', '=', 'c.id');
                $q->where('phone_num.contact_type','phone');
            })
            ->leftjoin('char_persons_contact as primary_mobile_num', function($q) {
                $q->on('primary_mobile_num.person_id', '=', 'c.id');
                $q->where('primary_mobile_num.contact_type','primary_mobile');
            })
            ->leftjoin('char_persons_contact as secondary_mobile_num', function($q) {
                $q->on('secondary_mobile_num.person_id', '=', 'c.id');
                $q->where('secondary_mobile_num.contact_type','secondary_mobile');
            })
            ->leftjoin('char_persons_i18n', function($q) {
                $q->on('char_persons_i18n.person_id', '=', 'c.id');
                $q->where('char_persons_i18n.language_id','2');
            })
            ->leftjoin('char_persons_health','char_persons_health.person_id', '=', 'c.id')
            ->leftjoin('char_diseases_i18n', function($q) use ($language_id){
                $q->on('char_diseases_i18n.disease_id', '=', 'char_persons_health.disease_id');
                $q->where('char_diseases_i18n.language_id',$language_id);
            })
            ->leftjoin('char_residence','char_residence.person_id', '=', 'c.id')
            ->leftjoin('char_property_types_i18n', function($q) use ($language_id){
                $q->on('char_property_types_i18n.property_type_id', '=', 'char_residence.property_type_id');
                $q->where('char_property_types_i18n.language_id',$language_id);
            })
            ->leftjoin('char_roof_materials_i18n', function($q) use ($language_id){
                $q->on('char_roof_materials_i18n.roof_material_id', '=', 'char_residence.roof_material_id');
                $q->where('char_roof_materials_i18n.language_id',$language_id);
            })
            ->leftjoin('char_persons_islamic_commitment','char_persons_islamic_commitment.person_id', '=', 'c.id')
            ->leftjoin('char_persons_education','char_persons_education.person_id', '=', 'c.id')
            ->leftjoin('char_edu_stages_i18n as char_edu_stages', function($q) use ($language_id){
                $q->on('char_edu_stages.edu_stage_id', '=', 'char_persons_education.stage');
                $q->where('char_edu_stages.language_id',$language_id);
            })
            ->leftjoin('char_edu_authorities_i18n as char_edu_authorities', function($q) use ($language_id){
                $q->on('char_edu_authorities.edu_authority_id', '=', 'char_persons_education.authority');
                $q->where('char_edu_authorities.language_id',$language_id);
            })
            ->leftjoin('char_edu_degrees_i18n as char_edu_degrees', function($q) use ($language_id){
                $q->on('char_edu_degrees.edu_degree_id', '=', 'char_persons_education.degree');
                $q->where('char_edu_degrees.language_id',$language_id);
            })

            ->leftjoin('char_death_causes_i18n as father_death_causes', function($q) use ($language_id){
                $q->on('father_death_causes.death_cause_id', '=', 'f.death_cause_id');
                $q->where('father_death_causes.language_id',$language_id);
            })
            ->leftjoin('char_persons_work as father_work','father_work.person_id','=','f.id')
            ->leftjoin('char_work_jobs_i18n as father_work_jobs', function($q) use ($language_id){
                $q->on('father_work_jobs.work_job_id', '=', 'father_work.work_job_id');
                $q->where('father_work_jobs.language_id',$language_id);
            })
            ->leftjoin('char_persons_education as father_education','father_education.person_id','=','f.id')
            ->leftjoin('char_edu_degrees_i18n as father_edu_degrees', function($q) use ($language_id){
                $q->on('father_edu_degrees.edu_degree_id', '=', 'father_education.degree');
                $q->where('father_edu_degrees.language_id',$language_id);
            })
            ->leftjoin('char_persons_health as father_health','father_health.person_id','=','f.id')
            ->leftjoin('char_diseases_i18n as father_diseases', function($q) use ($language_id){
                $q->on('father_diseases.disease_id', '=', 'father_health.disease_id');
                $q->where('father_diseases.language_id',$language_id);
            })
            ->leftjoin('char_death_causes_i18n as mother_death_causes', function($q) use ($language_id){
                $q->on('mother_death_causes.death_cause_id', '=', 'm.death_cause_id');
                $q->where('mother_death_causes.language_id',$language_id);
            })
            ->leftjoin('char_persons_work as mother_work','mother_work.person_id','=','m.id')
            ->leftjoin('char_work_jobs_i18n as mother_work_jobs', function($q) use ($language_id){
                $q->on('mother_work_jobs.work_job_id', '=', 'mother_work.work_job_id');
                $q->where('mother_work_jobs.language_id',$language_id);
            })
            ->leftjoin('char_persons_education as mother_education','mother_education.person_id','=','m.id')
            ->leftjoin('char_edu_stages_i18n as mother_edu_stages', function($q) use ($language_id){
                $q->on('mother_edu_stages.edu_stage_id', '=', 'mother_education.stage');
                $q->where('mother_edu_stages.language_id',$language_id);
            })
            ->leftjoin('char_marital_status_i18n as mother_marital_status', function($q) use ($language_id){
                $q->on('mother_marital_status.marital_status_id', '=', 'm.marital_status_id');
                $q->where('mother_marital_status.language_id',$language_id);
            })
            ->leftjoin('char_persons_health as mother_health','mother_health.person_id','=','m.id')
            ->leftjoin('char_diseases_i18n as mother_diseases', function($q) use ($language_id){
                $q->on('mother_diseases.disease_id', '=', 'mother_health.disease_id');
                $q->where('mother_diseases.language_id',$language_id);
            })
            ->where('char_sponsorships_persons.sponsorship_id', '=', $id)
                                     ->selectRAW("CONCAT(c.first_name, ' ' , c.second_name, ' ', c.third_name, ' ' , c.last_name) AS name,
                                                  CONCAT(ifnull(char_persons_i18n.first_name, ' '), ' ' ,ifnull(char_persons_i18n.second_name, ' '),' ',
                                                        ifnull(char_persons_i18n.third_name, ' '),' ', ifnull(char_persons_i18n.last_name,' ')) AS en_name,
                                                  CASE WHEN c.id_card_number is null THEN '-' Else c.id_card_number  END  AS id_card_number,
                                                  CASE WHEN c.old_id_card_number is null THEN '-' Else c.old_id_card_number  END  AS old_id_card_number,
                                                  CASE WHEN c.mother_id is null THEN '-' Else CONCAT(m.first_name, ' ', m.second_name, ' ', m.third_name, ' ', m.last_name) END   AS mother_name,
                                                  CASE WHEN c.father_id is null THEN '-' Else CONCAT(f.first_name, ' ', f.second_name, ' ', f.third_name, ' ', f.last_name) END   AS father_name,
                                                  CASE WHEN c.birthday is null THEN '-' Else DATE_FORMAT(c.birthday,'%Y/%m/%d')  END  AS birthday,
                                                  CASE WHEN c.gender is null THEN '-' WHEN c.gender = 1 THEN '$male' WHEN c.gender = 2 THEN '$female' END AS the_gender,
                                                  CASE WHEN c.birth_place is null THEN '-' Else L.name END AS birth_place,
                                                  CASE WHEN c.nationality is null THEN '-' Else L0.name END AS nationality,
                                                  CASE WHEN c.Location_id is null THEN '-'
                                                      Else CONCAT(L4.name, ' - ',L3.name, ' - ',L2.name, ' - ', L1.name, ' - ', c.street_address)
                                                  END AS address,
                                                  CASE WHEN phone_num.contact_value is null THEN '-' Else phone_num.contact_value END   AS phone,
                                                  CASE WHEN primary_mobile_num.contact_value is null THEN '-' Else primary_mobile_num.contact_value END   AS primary_mobile,
                                                  CASE WHEN secondary_mobile_num.contact_value is null THEN '-' Else secondary_mobile_num.contact_value END   AS secondary_mobile,
                                                  CASE WHEN char_persons_health.condition is null THEN '-'
                                                       WHEN char_persons_health.condition = 1 THEN '$perfect'
                                                       WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                       WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                                     END
                                                     AS health_status,
                                                     CASE WHEN char_persons_health.details is null THEN '-' Else char_persons_health.details END   AS details,
                                                     CASE WHEN char_diseases_i18n.name is null THEN '-' Else char_diseases_i18n.name END   AS disease_id,
                                                     CASE WHEN char_property_types_i18n.name is null THEN '-' Else char_property_types_i18n.name END   AS property_type,
                                                     CASE WHEN char_roof_materials_i18n.name is null THEN '-' Else char_roof_materials_i18n.name END   AS roof_material,
                                                     CASE WHEN char_residence.area is null THEN '-' Else char_residence.area  END  AS area,
                                                     CASE WHEN char_residence.rooms is null THEN '-' Else char_residence.rooms  END  AS rooms,
                                                     CASE
                                                               WHEN char_residence.residence_condition is null THEN '-'
                                                               WHEN char_residence.residence_condition = 1 THEN '$excellent'
                                                               WHEN char_residence.residence_condition = 2 THEN '$very_good'
                                                               WHEN char_residence.residence_condition = 3 THEN '$good'
                                                               WHEN char_residence.residence_condition = 4 THEN '$bad_condition'
                                                               WHEN char_residence.residence_condition = 5 THEN '$very_bad_condition'
                                                     END
                                                     AS residence_condition,
                                                     CASE
                                                               WHEN char_residence.indoor_condition is null THEN '-'
                                                               WHEN char_residence.indoor_condition = 1 THEN '$excellent'
                                                               WHEN char_residence.indoor_condition = 2 THEN '$very_good'
                                                               WHEN char_residence.indoor_condition = 3 THEN '$good'
                                                               WHEN char_residence.indoor_condition = 4 THEN '$bad_condition'
                                                               WHEN char_residence.indoor_condition = 5 THEN '$very_bad_condition '
                                                     END
                                                     AS indoor_condition,
                                                      CASE
                                                          WHEN char_persons_education.study is null THEN '-'
                                                          WHEN char_persons_education.study = 1 THEN '$yes'
                                                          WHEN char_persons_education.study = 0 THEN '$no'
                                                     END
                                                     AS study,
                                                     CASE
                                                               WHEN char_persons_education.type is null THEN '-'
                                                               WHEN char_persons_education.type = 1 THEN '$vocational'
                                                               WHEN char_persons_education.type = 2 THEN '$academic'
                                                     END
                                                     AS type,
                                                     CASE
                                                               WHEN char_persons_education.level is null THEN '-'
                                                               WHEN char_persons_education.level = 1 THEN '$weak'
                                                               WHEN char_persons_education.level = 2 THEN '$acceptable'
                                                               WHEN char_persons_education.level = 3 THEN '$good'
                                                               WHEN char_persons_education.level = 4 THEN '$very_good'
                                                               WHEN char_persons_education.level = 5 THEN '$excellent'
                                                     END
                                                     AS level,
                                                    CASE
                                                               WHEN char_persons_education.grade is null THEN '-'
                                                               WHEN char_persons_education.grade = 1 THEN '$the_first '
                                                               WHEN char_persons_education.grade = 2 THEN '$the_second '
                                                               WHEN char_persons_education.grade = 3 THEN '$the_third '
                                                               WHEN char_persons_education.grade = 4 THEN '$the_fourth '
                                                               WHEN char_persons_education.grade = 5 THEN '$the_Fifth '
                                                               WHEN char_persons_education.grade = 6 THEN '$the_Sixth '
                                                               WHEN char_persons_education.grade = 7 THEN '$the_Seventh '
                                                               WHEN char_persons_education.grade = 8 THEN '$the_Eighth '
                                                               WHEN char_persons_education.grade = 9 THEN '$the_Ninth '
                                                               WHEN char_persons_education.grade = 10 THEN '$the_tenth'
                                                               WHEN char_persons_education.grade = 11 THEN '$the_eleventh'
                                                               WHEN char_persons_education.grade = 12 THEN '$the_twelveth'
                                                               WHEN char_persons_education.grade = 13 THEN '$special_studies'
                                                               WHEN char_persons_education.grade = 14 THEN '$vocational_training'
                                                               WHEN char_persons_education.grade = 15 THEN '$first_year_of_university'
                                                               WHEN char_persons_education.grade = 16 THEN '$second_year_of_university'
                                                               WHEN char_persons_education.grade = 17 THEN '$third_year_of_university'
                                                               WHEN char_persons_education.grade = 18 THEN '$fourth_year_of_university'
                                                               WHEN char_persons_education.grade = 19 THEN '$fifth_year_of_university' 
                                                     END
                                                     AS grade,
                                                     CASE WHEN char_persons_education.year is null THEN '-'   Else char_persons_education.year END  AS year,
                                                     CASE WHEN char_persons_education.points is null THEN '-' Else char_persons_education.points END  AS points,
                                                     CASE WHEN char_persons_education.school is null THEN '-' Else char_persons_education.school END  AS school,
                                                     CASE WHEN char_edu_authorities.name is null THEN '-' Else char_edu_authorities.name END as authority,
                                                     CASE WHEN char_edu_stages.name is null THEN '-' Else char_edu_stages.name END as stage,
                                                     CASE WHEN char_edu_degrees.name is null THEN '-' Else char_edu_degrees.name END as degree,
                                                     CASE
                                                               WHEN char_persons_islamic_commitment.prayer is null THEN '-'
                                                           WHEN char_persons_islamic_commitment.prayer = 0 THEN '$regularly'
                                                             WHEN char_persons_islamic_commitment.prayer = 1 THEN '$does_not_pray'
                                                             WHEN char_persons_islamic_commitment.prayer = 2 THEN '$sometimes'
                                                    END
                                                     AS prayer,
                                                      CASE
                                                               WHEN char_persons_islamic_commitment.quran_parts is null THEN '-'
                                                               WHEN char_persons_islamic_commitment.quran_parts = 0 THEN '$no'
                                                               Else '$yes'
                                                    END
                                                     AS save_quran,
                                                      CASE
                                    WHEN char_persons_islamic_commitment.quran_center is null THEN '-'
                                    WHEN char_persons_islamic_commitment.quran_center = 0 THEN '$no'
                                    Else '$yes'
                                    END
                                    AS quran_center,
                                    
                                                     CASE WHEN char_persons_islamic_commitment.prayer_reason is null THEN '-' Else  char_persons_islamic_commitment.prayer_reason END AS prayer_reason,
                                                     CASE WHEN char_persons_islamic_commitment.quran_reason is null  THEN '-' Else char_persons_islamic_commitment.quran_reason END AS quran_reason,
                                                     CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN '-' Else char_persons_islamic_commitment.quran_parts END  AS quran_parts,
                                                     CASE WHEN char_persons_islamic_commitment.quran_chapters is null  THEN '-' Else char_persons_islamic_commitment.quran_chapters END AS quran_chapters,
                                                     CASE WHEN f.birthday is null THEN '-' Else  DATE_FORMAT(f.birthday,'%Y/%m/%d')  END  AS father_birthday,
                                                     CASE WHEN f.spouses is null THEN  '-' Else  f.spouses END AS father_spouses,
                                                     CASE WHEN f.death_date is null THEN  '$is_alive' Else  '$deceased' END AS father_alive,
                                                     CASE WHEN f.death_date is null THEN '-'  Else f.death_date END AS father_death_date,
                                                     CASE WHEN f.death_date is null THEN '-'  Else father_death_causes.name END AS father_death_cause,
                                                     CASE
                                                            WHEN father_health.condition is null THEN '-'
                                                            WHEN father_health.condition = 1 THEN '$perfect'
                                                            WHEN father_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                            WHEN father_health.condition = 3 THEN '$special_needs'
                                                         END
                                                     AS father_health_status,
                                                     CASE WHEN father_health.details is null THEN '-' Else father_health.details END   AS father_details,
                                                     CASE WHEN father_diseases.name is null THEN '-' Else father_diseases.name END   AS father_disease_id,
                                                     CASE WHEN father_education.specialization is null THEN '-'  Else father_education.specialization END  AS father_Specialization,
                                                     CASE WHEN father_edu_degrees.name is null THEN '-' Else father_edu_degrees.name END as father_degree,
                                                     CASE
                                                          WHEN father_work.working is null THEN '-'
                                                          WHEN father_work.working = 1 THEN '$yes'
                                                          WHEN father_work.working = 2 THEN '$no'
                                                       END
                                                     AS father_working,
                                                     CASE
                                                           WHEN father_work.working is null THEN '-'
                                                           WHEN father_work.working = 1 THEN father_work_jobs.name
                                                           WHEN father_work.working = 2 THEN '-'
                                                     END
                                                     AS father_work_type,
                                                     CASE
                                                       WHEN father_work.working is null THEN '-'
                                                       WHEN father_work.working = 1 THEN f.monthly_income
                                                       WHEN father_work.working = 2 THEN '-'
                                                     END
                                                     AS father_monthly_income,
                                                     CASE
                                                           WHEN father_work.working is null THEN '-'
                                                           WHEN father_work.working = 1 THEN father_work.work_location
                                                           WHEN father_work.working = 2 THEN '-'
                                                     END
                                                     AS father_work_location,
                                                     CASE WHEN m.birthday is null THEN '-' Else  DATE_FORMAT(m.birthday,'%Y/%m/%d') END  AS mother_birthday,
                                                     CASE WHEN mother_marital_status.name is null THEN '-' Else mother_marital_status.name END as mother_marital_status,
                                                     CASE WHEN m.death_date is null THEN  '$is_alive' Else  '$deceased' END AS mother_alive,
                                                     CASE WHEN m.death_date is null THEN '-'  Else f.death_date END AS mother_death_date,
                                                     CASE WHEN m.death_date is null THEN '-'  Else father_death_causes.name END AS mother_death_cause"
            )
            ->get();
    }

    public static function paginatePerson($id,$page)
    {
        $male = trans('common::application.male');
        $female = trans('common::application.female');

        return \DB::table('char_sponsorships_persons')
            ->join('char_persons','char_persons.id','=','char_sponsorships_persons.person_id')
            ->where('char_sponsorships_persons.sponsorship_id', '=', $id)
            ->selectRAW("char_persons.id,char_sponsorships_persons.sponsorship_id,
                         CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name,
                         CASE WHEN char_persons.id_card_number is null THEN '-' Else char_persons.id_card_number  END  AS id_card_number,
                         CASE WHEN char_persons.old_id_card_number is null THEN '-' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                         CASE WHEN char_persons.gender is null THEN '-' WHEN char_persons.gender = 1 THEN '$male' WHEN char_persons.gender = 2 THEN '$female' END AS gender
                       ")->paginate(config('constants.records_per_page'));
    }

}
