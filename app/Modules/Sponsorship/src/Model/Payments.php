<?php
namespace Sponsorship\Model;
use App\Http\Helpers;

class Payments  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_payments';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['sponsorship_id','category_id','sponsor_id','organization_id','payment_date','amount','currency_id','payment_exchange','bank_id','payment_target'
                           ,'exchange_rate','exchange_date','notes','organization_share','administration_fees','status','exchange_type', 'transfer_company_id' , 'transfer'];

    protected static $rules;

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'exchange_type' => 'required|integer',
                'sponsor_id' => 'required|integer',
                'category_id' => 'required',
                'bank_id' => 'required_if:exchange_type,1|required_if:exchange_type,2',
                'payment_exchange' => 'required|integer',
                'currency_id' => 'required|integer',
                'status' => 'required|integer',
                'payment_date' => 'required|date',
                'amount' => 'required',
//                'payment_target' => 'required',
                'exchange_rate' => 'required|numeric|min:0.0001',
                'exchange_date' => 'required|date',
                'organization_share' => 'required',
                'transfer' => 'required',
                'transfer_company_id' => 'required_if:transfer,1'
            );
        }

        return self::$rules;
    }
    public static function getFields()
    {

        return ['sponsorship_id','category_id','sponsor_id','organization_id','payment_date','amount','currency_id','payment_exchange','bank_id','payment_target'
            ,'exchange_rate','exchange_date','notes','organization_share','administration_fees','status','exchange_type', 'transfer_company_id' , 'transfer'];

    }

    public function Sponsorships()
    {
        return $this->belongsTO('Sponsorship\Model\Sponsorships', 'sponsorship_id', 'id');
    }
    public function sponsor()
    {
        return $this->belongsTO('Organization\Model\Sponsor', 'sponsor_id', 'id');
    }
    public function category()
    {
        return $this->belongsTo('Setting\Model\PaymentCategoryI18n','category_id','payment_category_id');
    }

// ***************************************************************************** //
    public static function detail($id){

        $language_id =  \App\Http\Helpers::getLocale();

        return \DB::table('char_payments')
            ->join('char_organizations as org','org.id',  '=', 'char_payments.organization_id')
            ->join('char_organizations as sponsor','sponsor.id',  '=', 'char_payments.sponsor_id')
            ->leftjoin('char_currencies', function($q) {
                $q->on('char_payments.currency_id','=','char_currencies.id');
            })
            ->leftjoin('char_payment_category_i18n', function($join) use ($language_id) {
                $join->on('char_payments.category_id', '=', 'char_payment_category_i18n.payment_category_id')
                    ->where('char_payment_category_i18n.language_id', '=', $language_id);
            })
            ->where('char_payments.id',$id)
            ->selectRaw("char_payments.*, 
                         (char_payments.amount * char_payments.exchange_rate) as shekel_amount,
                         CASE  WHEN $language_id = 1 THEN char_currencies.name Else char_currencies.en_name END  AS name,
                         CASE  WHEN $language_id = 1 THEN sponsor.name Else sponsor.en_name END  AS sponsor_name,
                         CASE  WHEN $language_id = 1 THEN org.name Else org.en_name END  AS organization_name,
                         sponsor.logo as sponsor_logo,
                         org.logo as organization_logo,
                         char_payment_category_i18n.name as payment_category")
            ->first();

    }

    public static function getDistrictPayment($payment_id)
    {

        $language_id =  \App\Http\Helpers::getLocale();
        $query = \DB::table('char_locations')
            ->join('char_locations_i18n', 'char_locations_i18n.location_id', '=', 'char_locations.id')
            ->where(function ($q) use ($language_id){
                $q->where('char_locations.location_type','=',1)
                    ->where('char_locations.parent_id','=',188)
                    ->where('char_locations_i18n.language_id','=',$language_id);
            })
            ->orderBy('char_locations_i18n.name')
            ->selectRaw("char_locations.id as id , char_locations_i18n.name as name")->get();


        $districts = array();
        foreach ($query as $key => $value) {
            $value->shekel_amount = 0;
            $value->ben_count = 0;
            $district = $value->id;

            $total= \DB::select("SELECT SUM(`shekel_amount`) as amount   
                        FROM char_payments_cases
                        JOIN char_cases   on char_cases.id   = char_payments_cases.case_id
                        JOIN char_persons on char_persons.id = char_cases.person_id
                        LEFT JOIN char_persons g on g.id = char_payments_cases.guardian_id
                        WHERE (
                                 (char_payments_cases.payment_id= '$payment_id') and 
                                 ( 
                                   ( char_persons.governarate= '$district' and recipient = 0 ) or 
                                   ( g.governarate= '$district' and recipient = 1 )
                                 )
                              )");

            if($total){
                $value->shekel_amount=$total[0]->amount;
                if($value->shekel_amount != 0 ){
                    $total_count= \DB::select("SELECT COUNT(`sponsor_number`) as count_   
                        FROM char_payments_cases
                        JOIN char_cases   on char_cases.id   = char_payments_cases.case_id
                        JOIN char_persons on char_persons.id = char_cases.person_id
                        LEFT JOIN char_persons g on g.id = char_payments_cases.guardian_id
                        WHERE ((char_payments_cases.payment_id= '$payment_id') and 
                               (char_payments_cases.amount != 0 ) and 
                               ( 
                                   ( char_persons.governarate= '$district' and char_payments_cases.recipient = 0 ) or 
                                   ( g.governarate= '$district' and char_payments_cases.recipient = 1 )
                                 )
                               )");
                    if($total){
                        $value->ben_count=$total_count[0]->count_;
                    }

                    $value->beneficiary = PaymentsCases::getDistrictBeneficiary([$payment_id],$district);

                    $districts[]=$value;
                }

            }
        }

        return $districts;

    }

    public static function getDistrictPaymentGroup($payment_ids)
    {

        $query = \DB::table('char_locations')
            ->join('char_locations_i18n', 'char_locations_i18n.location_id', '=', 'char_locations.id')
            ->where(function ($q) {
                $q->where('char_locations.location_type','=',1)
                    ->where('char_locations.parent_id','=',188)
                    ->where('char_locations_i18n.language_id','=',1);
            })
            ->orderBy('char_locations_i18n.name')
            ->selectRaw("char_locations.id as id , 
                                 char_locations_i18n.name as name")->get();

        $join_ =  implode(',', $payment_ids) ;
        $districts = array();
        foreach ($query as $key => $value) {
            $value->shekel_amount = 0;
            $value->ben_count = 0;
            $district = $value->id;

            $total= \DB::select("SELECT SUM(shekel_amount) as amount   
                        FROM char_payments_cases
                        JOIN char_cases   on char_cases.id   = char_payments_cases.case_id
                        JOIN char_persons on char_persons.id = char_cases.person_id
                        LEFT JOIN char_persons g on g.id = char_payments_cases.guardian_id
                        WHERE (
                                 (char_payments_cases.payment_id in ($join_) ) and 
                                 ( 
                                   ( char_persons.governarate= $district and recipient = 0 ) or 
                                   ( g.governarate= $district and recipient = 1 )
                                 )
                              )");

            if($total){
                $value->shekel_amount=$total[0]->amount;
                if($value->shekel_amount != 0 ){
                    $total_count= \DB::select("SELECT COUNT(sponsor_number) as count_   
                        FROM char_payments_cases
                        JOIN char_cases   on char_cases.id   = char_payments_cases.case_id
                        JOIN char_persons on char_persons.id = char_cases.person_id
                        LEFT JOIN char_persons g on g.id = char_payments_cases.guardian_id
                        WHERE ((char_payments_cases.payment_id in ( $join_)) and 
                               (char_payments_cases.amount != 0 ) and 
                               ( 
                                   ( char_persons.governarate= '$district' and char_payments_cases.recipient = 0 ) or 
                                   ( g.governarate= '$district' and char_payments_cases.recipient = 1 )
                                 )
                               )");

                    if($total){
                        $value->ben_count=$total_count[0]->count_;
                    }

                    $value->beneficiary = PaymentsCases::getDistrictBeneficiary($payment_ids,$district);

                    $districts[]=$value;
                }

            }
        }

        return $districts;

    }

    public static function statistic($ids)
    {

        $language_id =  \App\Http\Helpers::getLocale();
        $categories = \DB::table('char_payments')
            ->join('char_payment_category_i18n', function($join) use ($language_id){
                $join->on('char_payments.category_id', '=', 'char_payment_category_i18n.payment_category_id')
                    ->where('char_payment_category_i18n.language_id', '=', $language_id);
            })
            ->whereIn('char_payments.id',$ids)
            ->selectRaw('char_payments.category_id,char_payment_category_i18n.name as category_name , 0 as amount , 0 as shekel_amount')
            ->groupBy('char_payments.category_id')
            ->get();

        $pIds = join(",",$ids);

        foreach ($categories as $key=>$value){

            $id = $value->category_id;
            $value->amount =0;
            $value->amount_after_discount =0;
            $value->shekel_amount =0;
            $value->shekel_amount_before_discount =0;

            $total= \DB::select("select SUM(`char_payments_cases`.`amount`) as amount   FROM `char_payments_cases`
                                 join char_payments on char_payments .id = char_payments_cases.payment_id
                                  where (`payment_id` IN($pIds)) and (char_payments.`category_id` = $id)");

            if($total){ $value->amount=$total[0]->amount; }

            $amount_after_discount= \DB::select("select SUM(`char_payments_cases`.`amount_after_discount`) as amount   FROM `char_payments_cases`
                                         join char_payments on char_payments .id = char_payments_cases.payment_id
                                          where (`payment_id` IN($pIds)) and (char_payments.`category_id` = $id)");

            if($amount_after_discount){$value->amount_after_discount =$amount_after_discount[0]->amount;}

            $total_shekel_amount= \DB::select("select SUM(`char_payments_cases`.`shekel_amount`) as amount  FROM `char_payments_cases`
                                         join char_payments on char_payments .id = char_payments_cases.payment_id
                                          where (`payment_id` IN($pIds)) and (char_payments.`category_id` = $id)");

            if($total_shekel_amount){$value->shekel_amount=$total_shekel_amount[0]->amount;}

            $shekel_amount_before_discount= \DB::select("select SUM(`char_payments_cases`.`shekel_amount_before_discount`) as amount   FROM `char_payments_cases`
                                             join char_payments on char_payments .id = char_payments_cases.payment_id
                                          where (`payment_id` IN($pIds)) and (char_payments.`category_id` = $id)");

            if($shekel_amount_before_discount){
                $value->shekel_amount_before_discount_amount =$shekel_amount_before_discount[0]->amount;
            }



        }
        return $categories;

//        $result=\DB::table('char_payments')
//            ->join('char_currencies','char_currencies.id',  '=', 'char_payments.currency_id')
//            ->leftjoin('char_payment_category_i18n', function($join) {
//                $join->on('char_payments.category_id', '=', 'char_payment_category_i18n.payment_category_id')
//                    ->where('char_payment_category_i18n.language_id', '=', 1);
//            })
//            ->whereIn('char_payments.id',$ids);
//
//
//
//        return $return;
    }
// ***************************************************************************** //
    public static function filter($page,$organization_id,$filters)
    {
        $condition = [];
//        ,'organization_id'
        $char_payments_int = ['sponsorship_id','sponsor_id', 'amount', 'category_id','status',
                              'currency_id', 'payment_exchange','bank_id','payment_target','exchange_type', 'transfer_company_id' , 'transfer'];

        foreach ($filters as $key => $value) {
            if(in_array($key, $char_payments_int)) {
                if ($value != "" ) {
                    $data = ['char_payments.' . $key => (int) $value];
                    array_push($condition, $data);
                }
            }
        }

        $language_id =  \App\Http\Helpers::getLocale();

        $result=\DB::table('char_payments')
            ->join('char_currencies','char_currencies.id',  '=', 'char_payments.currency_id')
            ->leftJoin('char_transfer_company', 'char_transfer_company.id', '=', 'char_payments.transfer_company_id')
            ->join('char_organizations as owner','owner.id',  '=', 'char_payments.organization_id')
            ->join('char_organizations as org','org.id',  '=', 'char_payments.sponsor_id')
            ->leftjoin('char_payment_category_i18n', function($join) use ($language_id){
                $join->on('char_payments.category_id', '=', 'char_payment_category_i18n.payment_category_id')
                    ->where('char_payment_category_i18n.language_id', '=', $language_id);
            })
//            ->where('char_payments.organization_id',$organization_id)
        ;

        $all_organization=null;
        if(isset($filters['all_organization'])){
            if($filters['all_organization'] ===true){
                $all_organization=0;
            }elseif($filters['all_organization'] ===false){
                $all_organization=1;

            }
        }

        if($all_organization ==0){

            $organizations=[];
            if(isset($filters['organization_ids']) && $filters['organization_ids'] !=null && $filters['organization_ids'] !="" && $filters['organization_ids'] !=[] && sizeof($filters['organization_ids']) != 0) {
                if($filters['organization_ids'][0]==""){
                    unset($filters['organization_ids'][0]);
                }
                $organizations =$filters['organization_ids'];
            }

            if(!empty($organizations)){
                $result->wherein('char_payments.organization_id',$organizations);
            }else{

                $user = \Auth::user();

                if($user->type == 2) {
                    $result->where(function ($anq) use ($organization_id,$user) {
                        $anq->where('char_payments.organization_id',$organization_id);
                        $anq->orwherein('char_payments.organization_id', function($decq) use($user) {
                            $decq->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });

                }else{
                    $result->where(function ($anq) use ($organization_id) {
                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                        $anq->wherein('char_payments.organization_id', function($decq) use($organization_id) {
                            $decq->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $organization_id);
                        });
                    });
                }

            }
        }
        elseif($all_organization ==1){
            $result->where('char_payments.organization_id',$organization_id);

        }

        $end_exchange_date=null;
        $start_exchange_date=null;
        $end_payment_date=null;
        $start_payment_date=null;


        if(isset($filters['end_exchange_date']) && $filters['end_exchange_date'] !=null){
            $end_exchange_date=date('Y-m-d',strtotime($filters['end_exchange_date']));
        }
        if(isset($filters['start_exchange_date']) && $filters['start_exchange_date'] !=null){
            $start_exchange_date=date('Y-m-d',strtotime($filters['start_exchange_date']));
        }
        if(isset($filters['end_payment_date']) && $filters['end_payment_date'] !=null){
            $end_payment_date=date('Y-m-d',strtotime($filters['end_payment_date']));
        }
        if(isset($filters['start_payment_date']) && $filters['start_payment_date'] !=null){
            $start_payment_date=date('Y-m-d',strtotime($filters['start_payment_date']));
        }

        if($start_exchange_date != null && $end_exchange_date != null) {
            $result = $result->whereBetween( 'char_payments.exchange_date', [ $start_exchange_date, $end_exchange_date]);
        }elseif($start_exchange_date != null && $end_exchange_date == null) {
            $result = $result->whereDate('char_payments.exchange_date', '>=', $start_exchange_date);
        }elseif($start_exchange_date == null && $end_exchange_date != null) {
            $result = $result->whereDate('char_payments.exchange_date', '<=', $end_exchange_date);
        }

        if($start_payment_date != null && $end_payment_date != null) {
            $result = $result->whereBetween( 'char_payments.payment_date', [ $start_payment_date, $end_payment_date]);
        }elseif($start_payment_date != null && $end_payment_date == null) {
            $result = $result->whereDate('char_payments.payment_date', '>=', $start_payment_date);
        }elseif($start_payment_date == null && $end_payment_date != null) {
            $result = $result->whereDate('char_payments.payment_date', '<=', $end_payment_date);
        }

        if (count($condition) != 0) {
            $result =  $result
                ->where(function ($q) use ($condition) {
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            $q->where($key, '=', $value);
                        }
                    }
                });
        }

        $max_beneficiary_no=null;
        $min_beneficiary_no=null;


        if(isset($filters['max_beneficiary_no']) && $filters['max_beneficiary_no'] !=null && $filters['max_beneficiary_no'] !=""){
            $max_beneficiary_no=$filters['max_beneficiary_no'];
        }
        if(isset($filters['min_beneficiary_no']) && $filters['min_beneficiary_no'] !=null && $filters['min_beneficiary_no'] !=""){
            $min_beneficiary_no=$filters['min_beneficiary_no'];
        }

        if($max_beneficiary_no != null && $min_beneficiary_no != null) {
            $result = $result->whereRaw(" $min_beneficiary_no <= (SELECT COUNT(*) FROM char_payments_cases
                                                                  WHERE (char_payments_cases.payment_id = char_payments.id) and (char_payments_cases.status = 2 or char_payments_cases.status = 1))");
            $result = $result->whereRaw(" $max_beneficiary_no >= (SELECT COUNT(*) FROM char_payments_cases
                                                                  WHERE (char_payments_cases.payment_id = char_payments.id) and (char_payments_cases.status = 2 or char_payments_cases.status = 1))");
        }elseif($max_beneficiary_no != null && $min_beneficiary_no == null) {
            $result = $result->whereRaw(" $max_beneficiary_no >= (SELECT COUNT(*) FROM char_payments_cases
                                                                  WHERE (char_payments_cases.payment_id = char_payments.id) and (char_payments_cases.status = 2 or char_payments_cases.status = 1))");
        }elseif($max_beneficiary_no == null && $min_beneficiary_no != null) {
            $result = $result->whereRaw(" $min_beneficiary_no <= ((SELECT COUNT(*) FROM char_payments_cases
                                                                  WHERE (char_payments_cases.payment_id = char_payments.id) and (char_payments_cases.status = 2 or char_payments_cases.status = 1))");
        }

        $max_amount=null;
        $min_amount=null;


        if(isset($filters['max_amount']) && $filters['max_amount'] !=null && $filters['max_amount'] !=""){
            $max_amount=$filters['max_amount'];
        }
        if(isset($filters['min_amount']) && $filters['min_amount'] !=null && $filters['min_amount'] !=""){
            $min_amount=$filters['min_amount'];
        }

        if($max_amount != null && $min_amount != null) {
            $result = $result->whereRaw(" $min_amount <= (char_payments.amount * char_payments.exchange_rate)");
            $result = $result->whereRaw(" $max_amount >= (char_payments.amount * char_payments.exchange_rate)");
//            $q->whereRaw(' ( char_payments.amount * char_payments.exchange_rate between ? and  ?)', array($f, $t));

        }elseif($max_amount != null && $min_amount == null) {
            $result = $result->whereRaw(" $max_amount >= (char_payments.amount * char_payments.exchange_rate)");
        }elseif($max_amount == null && $min_amount != null) {
            $result = $result->whereRaw(" $min_amount <= (char_payments.amount * char_payments.exchange_rate)");
        }



        $currency=trans('sponsorship::application.currency');
        $shekel= trans('sponsorship::application.shekel');
        $person= trans('sponsorship::application.person') ;
        $guardian =trans('sponsorship::application.guardian');
        $finish = trans('common::application.finish');
        $done = trans('common::application.done');
        $in_progress = trans('common::application.in progress');

        $transfer_company = trans('aid::application.transfer_company');
        $direct = trans('aid::application.direct');

        if($filters['action'] =='filter'){
            $result->selectRaw("
                                     CASE  WHEN $language_id = 1 THEN owner.name Else owner.en_name END  AS organization_name,
                                     CASE  WHEN $language_id = 1 THEN org.name Else org.en_name END  AS sponsor_name,
                                       get_payments_beneficiary_count(char_payments.id , 'all') as Beneficiary,
                                       get_payments_beneficiary_count(char_payments.id , 'guaranteed') as guaranteed,
                                       char_get_payment_document_type_count(char_payments.id , 1) as signature_sheet,
                                       char_get_payment_document_type_id(char_payments.id , 1) as signature_sheet_id,
                                       
                                       char_get_payment_document_type_count(char_payments.id , 2) as organization_sheet,
                                       char_get_payment_document_type_id(char_payments.id , 2) as organization_sheet_id,
                                       
                                       char_get_payment_document_type_count(char_payments.id , 3) as receipt_of_receipt,
                                       char_get_payment_document_type_id(char_payments.id , 3) as receipt_of_receipt_id,

                                       char_payments.*,
                                       char_payments.payment_date as payment_date,
                                       char_payments.amount,
                                        CASE  WHEN $language_id = 1 THEN char_currencies.name Else char_currencies.en_name END  AS currency,
                                        char_payments.amount * char_payments.exchange_rate as shekel_amount,
                                           CASE WHEN char_payments.category_id is null THEN '-'
                                               Else  char_payment_category_i18n.name
                                           END  AS payment_category,
                                           CASE
                                                 WHEN char_payments.transfer = 0 THEN '$direct'
                                                 WHEN char_payments.transfer = 1 THEN '$transfer_company'
                                              END
                                             AS transfer_name,
                                             char_payments.transfer,
                                             char_payments.transfer_company_id,
                                             CASE
                                                  WHEN char_payments.transfer_company_id is null THEN '-'
                                                  Else char_transfer_company.name
                                             END  AS transfer_company_name,
                                       char_payments.administration_fees * char_payments.exchange_rate as administration_fees,
                                                                        char_payments.notes   ");


            $map = [
                "administration_fees" => 'char_payments.administration_fees',
                "amount" => 'char_payments.amount',
                "organization_share" => 'char_payments.organization_share',
                "exchange_date" => 'char_payments.exchange_date',
                "payment_date" => 'char_payments.payment_date',
                "shekel_amount" => 'char_payments.amount',
                "payment_category" => 'char_payment_category_i18n.name',
                "organizations_name" => 'owner.name',
                "sponsor_name" => 'org.name'
            ];
            $order = false;

            if (isset($filters['sortKeyArr_rev']) && $filters['sortKeyArr_rev'] != null && $filters['sortKeyArr_rev'] != "" &&
                $filters['sortKeyArr_rev'] != [] && sizeof($filters['sortKeyArr_rev']) != 0
            ) {

                $order = true;

                foreach ($filters['sortKeyArr_rev'] as $key_) {
                    $result->orderBy($map[$key_],'desc');
                }
            }


            if (isset($filters['sortKeyArr_un_rev']) && $filters['sortKeyArr_un_rev'] != null && $filters['sortKeyArr_un_rev'] != "" &&
                $filters['sortKeyArr_un_rev'] != [] && sizeof($filters['sortKeyArr_un_rev']) != 0
            ) {

                $order = true;
                foreach ($filters['sortKeyArr_un_rev'] as $key_) {
                    $result->orderBy($map[$key_],'asc');
                }
            }


            if(!$order){
                $result->orderBy('char_payments.created_at','desc');
            }
            $itemsCount = isset($filters['itemsCount'])? $filters['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$result->count());
            $list = $result;

            $paginate =$list->paginate($records_per_page);
            $amount = 0;
            if($page == 1 || $page == '1'){
                $total=$list->selectRaw("sum((char_payments.amount * char_payments.exchange_rate)) as total")->first();
                if(!is_null($total)){
                    $amount = $total->total;
                }
            }
            return ['list' => $paginate, 'total' =>round($amount,0)];
        }else{

            $result  ->leftjoin('char_banks', function($q) {
                $q->on('char_banks.id', '=', 'char_payments.bank_id');
            })
                ->selectRaw("             CASE  WHEN $language_id = 1 THEN owner.name Else owner.en_name END  AS organization_name,
                                          CASE  WHEN $language_id = 1 THEN org.name Else org.en_name END  AS sponsor_name,
                                          get_payments_beneficiary_count(char_payments.id , 'all') as cases_total,
                                           get_payments_beneficiary_count(char_payments.id , 'guaranteed') as guaranteed_total,
                                        CASE  WHEN $language_id = 1 THEN char_currencies.name Else char_currencies.en_name END  AS currency,
                                           CASE WHEN char_banks.name is null THEN '-'
                                               Else  char_banks.name
                                           END  AS bank_name,
                                           char_payments.amount as amount_in_currency,
                                           char_payments.amount * char_payments.exchange_rate as amount_in_shekel,
                                           char_payments.exchange_rate,
                                           char_payments.organization_share,
                                           char_payments.organization_share * char_payments.amount * char_payments.exchange_rate as organization_share_in_shekel,
                                           char_payments.administration_fees,
                                           char_payments.administration_fees * char_payments.exchange_rate as administration_fees_in_shekel,
                                           char_payments.payment_date as payment_date,
                                           char_payments.exchange_date as exchange_date,
                                           CASE WHEN char_payments.payment_exchange is null THEN '-'
                                               WHEN char_payments.payment_exchange = 0 THEN '$currency'
                                               WHEN char_payments.payment_exchange = 1 THEN '$shekel'
                                          END  AS payment_exchange,
                                           CASE WHEN char_payments.payment_target is null THEN '-'
                                               WHEN char_payments.payment_target = 0 THEN '$person'
                                               WHEN char_payments.payment_target = 1 THEN '$guardian'
                                          END  AS payment_target,
                                           CASE WHEN char_payments.status is null THEN '-'
                                                WHEN char_payments.status = 1 THEN '$done'
                                               WHEN char_payments.status = 2 THEN '$finish'
                                               WHEN char_payments.status = 3 THEN '$in_progress'
                                          END  AS status,
                                          CASE WHEN char_payments.category_id is null THEN '-'
                                               Else  char_payment_category_i18n.name
                                           END  AS payment_category,
                                           CASE
                                                 WHEN char_payments.transfer = 0 THEN '$direct'
                                                 WHEN char_payments.transfer = 1 THEN '$transfer_company'
                                              END
                                             AS transfer,
                                             CASE
                                                  WHEN char_payments.transfer_company_id is null THEN '-'
                                                  Else char_transfer_company.name
                                             END  AS transfer_company_name,  
                                           char_payments.notes
                                         ");

            $map = ["administration_fees" => 'char_payments.administration_fees',
                "amount" => 'char_payments.amount',
                "organization_share" => 'char_payments.organization_share',
                "exchange_date" => 'char_payments.exchange_date',
                "payment_date" => 'char_payments.payment_date',
                "shekel_amount" => 'char_payments.amount',
                "payment_category" => 'char_payment_category_i18n.name',
                "organizations_name" => 'owner.name',
                "sponsor_name" => 'org.name'
            ];
            $order = false;

            if (isset($filters['sortKeyArr_rev']) && $filters['sortKeyArr_rev'] != null && $filters['sortKeyArr_rev'] != "" &&
                $filters['sortKeyArr_rev'] != [] && sizeof($filters['sortKeyArr_rev']) != 0
            ) {

                $order = true;

                foreach ($filters['sortKeyArr_rev'] as $key_) {
                    $result->orderBy($map[$key_],'desc');
                }
            }


            if (isset($filters['sortKeyArr_un_rev']) && $filters['sortKeyArr_un_rev'] != null && $filters['sortKeyArr_un_rev'] != "" &&
                $filters['sortKeyArr_un_rev'] != [] && sizeof($filters['sortKeyArr_un_rev']) != 0
            ) {

                $order = true;
                foreach ($filters['sortKeyArr_rev'] as $key_) {
                    $result->orderBy($map[$key_],'asc');
                }
            }

            if(!$order){
                $result->orderBy('char_payments.created_at','desc');
            }

            $return= $result->get();
        }

        return $return;
    }
// ***************************************************************************** //
    public static function filterForSponsor($page,$sponsor_id,$filters)
    {
        $condition = [];
        $char_payments_int = ['sponsorship_id', 'amount', 'category_id','status',
            'currency_id', 'payment_exchange','bank_id','payment_target','exchange_type', 'transfer_company_id' , 'transfer'];

        foreach ($filters as $key => $value) {
            if(in_array($key, $char_payments_int)) {
                if ($value != "" ) {
                    $data = ['char_payments.' . $key => (int) $value];
                    array_push($condition, $data);
                }
            }
        }

        $result=\DB::table('char_payments')
            ->join('char_currencies','char_currencies.id',  '=', 'char_payments.currency_id')
            ->leftJoin('char_transfer_company', 'char_transfer_company.id', '=', 'char_payments.transfer_company_id')
            ->join('char_organizations as owner','owner.id',  '=', 'char_payments.organization_id')
            ->join('char_organizations as org','org.id',  '=', 'char_payments.sponsor_id')
            ->leftjoin('char_payment_category_i18n', function($join) {
                $join->on('char_payments.category_id', '=', 'char_payment_category_i18n.payment_category_id')
                    ->where('char_payment_category_i18n.language_id', '=', 1);
            });


           $result->where('char_payments.sponsor_id',$sponsor_id);

        if(isset($filters['organization_ids']) && $filters['organization_ids'] !=null && $filters['organization_ids'] !="" && $filters['organization_ids'] !=[] && sizeof($filters['organization_ids']) != 0) {
            if($filters['organization_ids'][0]==""){
                unset($filters['organization_ids'][0]);
            }
            $organizations =$filters['organization_ids'];
            if(!empty($organizations)){
                $result->wherein('char_payments.organization_id',$organizations);
            }
        }
        
        
        if(isset($filters['payments'])){
            if($filters['payments'] > 0 ){
               $result->whereIn('char_payments.id',$filters['payments']);
            } 
        }  

        $end_exchange_date=null;
        $start_exchange_date=null;
        $end_payment_date=null;
        $start_payment_date=null;


        if(isset($filters['end_exchange_date']) && $filters['end_exchange_date'] !=null){
            $end_exchange_date=date('Y-m-d',strtotime($filters['end_exchange_date']));
        }
        if(isset($filters['start_exchange_date']) && $filters['start_exchange_date'] !=null){
            $start_exchange_date=date('Y-m-d',strtotime($filters['start_exchange_date']));
        }
        if(isset($filters['end_payment_date']) && $filters['end_payment_date'] !=null){
            $end_payment_date=date('Y-m-d',strtotime($filters['end_payment_date']));
        }
        if(isset($filters['start_payment_date']) && $filters['start_payment_date'] !=null){
            $start_payment_date=date('Y-m-d',strtotime($filters['start_payment_date']));
        }

        if($start_exchange_date != null && $end_exchange_date != null) {
            $result = $result->whereBetween( 'char_payments.exchange_date', [ $start_exchange_date, $end_exchange_date]);
        }elseif($start_exchange_date != null && $end_exchange_date == null) {
            $result = $result->whereDate('char_payments.exchange_date', '>=', $start_exchange_date);
        }elseif($start_exchange_date == null && $end_exchange_date != null) {
            $result = $result->whereDate('char_payments.exchange_date', '<=', $end_exchange_date);
        }

        if($start_payment_date != null && $end_payment_date != null) {
            $result = $result->whereBetween( 'char_payments.payment_date', [ $start_payment_date, $end_payment_date]);
        }elseif($start_payment_date != null && $end_payment_date == null) {
            $result = $result->whereDate('char_payments.payment_date', '>=', $start_payment_date);
        }elseif($start_payment_date == null && $end_payment_date != null) {
            $result = $result->whereDate('char_payments.payment_date', '<=', $end_payment_date);
        }

        if (count($condition) != 0) {
            $result =  $result
                ->where(function ($q) use ($condition) {
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            $q->where($key, '=', $value);
                        }
                    }
                });
        }

        $max_beneficiary_no=null;
        $min_beneficiary_no=null;


        if(isset($filters['max_beneficiary_no']) && $filters['max_beneficiary_no'] !=null && $filters['max_beneficiary_no'] !=""){
            $max_beneficiary_no=$filters['max_beneficiary_no'];
        }
        if(isset($filters['min_beneficiary_no']) && $filters['min_beneficiary_no'] !=null && $filters['min_beneficiary_no'] !=""){
            $min_beneficiary_no=$filters['min_beneficiary_no'];
        }

        if($max_beneficiary_no != null && $min_beneficiary_no != null) {
            $result = $result->whereRaw(" $min_beneficiary_no <= (SELECT COUNT(*) FROM char_payments_cases
                                                                  WHERE (char_payments_cases.payment_id = char_payments.id) and (char_payments_cases.status = 2 or char_payments_cases.status = 1))");
            $result = $result->whereRaw(" $max_beneficiary_no >= (SELECT COUNT(*) FROM char_payments_cases
                                                                  WHERE (char_payments_cases.payment_id = char_payments.id) and (char_payments_cases.status = 2 or char_payments_cases.status = 1))");
        }elseif($max_beneficiary_no != null && $min_beneficiary_no == null) {
            $result = $result->whereRaw(" $max_beneficiary_no >= (SELECT COUNT(*) FROM char_payments_cases
                                                                  WHERE (char_payments_cases.payment_id = char_payments.id) and (char_payments_cases.status = 2 or char_payments_cases.status = 1))");
        }elseif($max_beneficiary_no == null && $min_beneficiary_no != null) {
            $result = $result->whereRaw(" $min_beneficiary_no <= ((SELECT COUNT(*) FROM char_payments_cases
                                                                  WHERE (char_payments_cases.payment_id = char_payments.id) and (char_payments_cases.status = 2 or char_payments_cases.status = 1))");
        }

        $max_amount=null;
        $min_amount=null;


        if(isset($filters['max_amount']) && $filters['max_amount'] !=null && $filters['max_amount'] !=""){
            $max_amount=$filters['max_amount'];
        }
        if(isset($filters['min_amount']) && $filters['min_amount'] !=null && $filters['min_amount'] !=""){
            $min_amount=$filters['min_amount'];
        }

        if($max_amount != null && $min_amount != null) {
            $result = $result->whereRaw(" $min_amount <= (char_payments.amount * char_payments.exchange_rate)");
            $result = $result->whereRaw(" $max_amount >= (char_payments.amount * char_payments.exchange_rate)");
//            $q->whereRaw(' ( char_payments.amount * char_payments.exchange_rate between ? and  ?)', array($f, $t));

        }elseif($max_amount != null && $min_amount == null) {
            $result = $result->whereRaw(" $max_amount >= (char_payments.amount * char_payments.exchange_rate)");
        }elseif($max_amount == null && $min_amount != null) {
            $result = $result->whereRaw(" $min_amount <= (char_payments.amount * char_payments.exchange_rate)");
        }

        $currency=trans('sponsorship::application.currency');
        $shekel= trans('sponsorship::application.shekel');
        $person= trans('sponsorship::application.person') ;
        $guardian =trans('sponsorship::application.guardian');
        $finish = trans('common::application.finish');
        $done = trans('common::application.done');
        $in_progress = trans('common::application.in progress');
        $transfer_company = trans('aid::application.transfer_company');
        $direct = trans('aid::application.direct');

        if($filters['action'] =='filter'){
            $result->selectRaw("owner.name as organizations_name,
                                       org.name as sponsor_name,
                                       get_payments_beneficiary_count(char_payments.id , 'all') as Beneficiary,
                                       get_payments_beneficiary_count(char_payments.id , 'guaranteed') as guaranteed,
                                       char_get_payment_document_type_count(char_payments.id , 1) as signature_sheet,
                                       char_get_payment_document_type_id(char_payments.id , 1) as signature_sheet_id,
                                       
                                       char_get_payment_document_type_count(char_payments.id , 2) as organization_sheet,
                                       char_get_payment_document_type_id(char_payments.id , 2) as organization_sheet_id,
                                       
                                       char_get_payment_document_type_count(char_payments.id , 3) as receipt_of_receipt,
                                       char_get_payment_document_type_id(char_payments.id , 3) as receipt_of_receipt_id,

                                       char_payments.*,
                                       char_payments.payment_date as payment_date,
                                       char_payments.amount,
                                       char_currencies.name as currency,
                                       char_payments.amount * char_payments.exchange_rate as shekel_amount,
                                           CASE WHEN char_payments.category_id is null THEN '-'
                                               Else  char_payment_category_i18n.name
                                           END  AS payment_category,
                                           CASE
                                                 WHEN char_payments.transfer = 0 THEN '$direct'
                                                 WHEN char_payments.transfer = 1 THEN '$transfer_company'
                                              END
                                             AS transfer_name,
                                             char_payments.transfer,
                                             char_payments.transfer_company_id,
                                             CASE
                                                  WHEN char_payments.transfer_company_id is null THEN '-'
                                                  Else char_transfer_company.name
                                             END  AS transfer_company_name,
                                       char_payments.administration_fees * char_payments.exchange_rate as administration_fees,
                                                                        char_payments.notes   ");

            if(isset($filters['order_by_payment_date']) && $filters['order_by_payment_date'] !=null && $filters['order_by_payment_date'] !=""){
                if((int)$filters['order_by_payment_date']==1){
                    $result->orderBy('char_payments.payment_date','desc');
                }elseif((int)$filters['order_by_payment_date']== 2){
                    $result->orderBy('char_payments.payment_date','asc');
                }
            }else{
                $result->orderBy('char_payments.created_at','desc');
            }

            $itemsCount = isset($filters['itemsCount'])? $filters['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$result->count());
            $list = $result;

            $paginate =$list->paginate($records_per_page);
            $amount = 0;
            if($page == 1 || $page == '1'){
                $total=$list->selectRaw("sum((char_payments.amount * char_payments.exchange_rate)) as total")->first();
                if(!is_null($total)){
                    $amount = $total->total;
                }
            }
            return ['list' => $paginate, 'total' =>round($amount,0)];

        }else{



            $result  ->leftjoin('char_banks', function($q) {
                $q->on('char_banks.id', '=', 'char_payments.bank_id');
            })
                ->selectRaw("owner.name as organizations_name,
                                           org.name as sponsor_name,
                                           get_payments_beneficiary_count(char_payments.id , 'all') as cases_total,
                                           get_payments_beneficiary_count(char_payments.id , 'guaranteed') as guaranteed_total,
                                           char_currencies.name as currency,
                                           CASE WHEN char_banks.name is null THEN '-'
                                               Else  char_banks.name
                                           END  AS bank_name,
                                           char_payments.amount as amount_in_currency,
                                           char_payments.amount * char_payments.exchange_rate as amount_in_shekel,
                                           char_payments.exchange_rate,
                                           char_payments.organization_share,
                                           char_payments.organization_share * char_payments.amount * char_payments.exchange_rate as organization_share_in_shekel,
                                           char_payments.administration_fees,
                                           char_payments.administration_fees * char_payments.exchange_rate as administration_fees_in_shekel,
                                           char_payments.payment_date as payment_date,
                                           char_payments.exchange_date as exchange_date,
                                           CASE WHEN char_payments.payment_exchange is null THEN '-'
                                               WHEN char_payments.payment_exchange = 0 THEN '$currency'
                                               WHEN char_payments.payment_exchange = 1 THEN '$shekel'
                                          END  AS payment_exchange,
                                           CASE WHEN char_payments.payment_target is null THEN '-'
                                               WHEN char_payments.payment_target = 0 THEN '$person'
                                               WHEN char_payments.payment_target = 1 THEN '$guardian'
                                          END  AS payment_target,
                                           CASE WHEN char_payments.status is null THEN '-'
                                              WHEN char_payments.status = 1 THEN '$done'
                                               WHEN char_payments.status = 2 THEN '$finish'
                                               WHEN char_payments.status = 3 THEN '$in_progress'
                                          END  AS status,
                                          CASE WHEN char_payments.category_id is null THEN '-'
                                               Else  char_payment_category_i18n.name
                                           END  AS payment_category,
                                           CASE
                                                 WHEN char_payments.transfer = 0 THEN '$direct'
                                                 WHEN char_payments.transfer = 1 THEN '$transfer_company'
                                              END
                                             AS transfer,
                                             CASE
                                                  WHEN char_payments.transfer_company_id is null THEN '-'
                                                  Else char_transfer_company.name
                                             END  AS transfer_company_name,  
                                           char_payments.notes
                                         ");

            if(isset($filters['order_by_payment_date']) && $filters['order_by_payment_date'] !=null && $filters['order_by_payment_date'] !=""){
                if((int)$filters['order_by_payment_date']==1){
                    $result->orderBy('char_payments.payment_date','desc');
                }elseif((int)$filters['order_by_payment_date']== 2){
                    $result->orderBy('char_payments.payment_date','asc');
                }
            }else{
                $result->orderBy('char_payments.created_at','desc');
            }

            $return= $result->get();
        }

        return $return;
    }
// ***************************************************************************** //
    public static function fetch($id,$mode){


        $language_id =  \App\Http\Helpers::getLocale();

        $query = self::query()
                     ->leftJoin('char_currencies','char_currencies.id',  '=', 'char_payments.currency_id')
                     ->join('char_organizations as org','org.id',  '=', 'char_payments.organization_id')
                     ->leftJoin('char_organizations as sponsor','sponsor.id',  '=', 'char_payments.sponsor_id')
                     ->leftjoin('char_payment_category_i18n', function($join) use ($language_id) {
                           $join->on('char_payments.category_id', '=', 'char_payment_category_i18n.payment_category_id')
                               ->where('char_payment_category_i18n.language_id', '=', $language_id);
                     })
                     ->where('char_payments.id',$id)
                     ->selectRaw("char_payments.*,org.name as organization_name, sponsor.name as sponsor_name,
                                  org.logo as organization_logo,
                                  CASE WHEN char_payments.payment_exchange is null THEN ' '
                                       WHEN char_payments.payment_exchange = 0 THEN char_currencies.name
                                       WHEN char_payments.payment_exchange = 1 THEN ' '
                                  END  AS currency,
                                  char_payment_category_i18n.name as payment_category,
                                  sponsor.logo as sponsor_logo")
            ->first();

        if($query){
            $query->total_amount=$query->amount;
            $query->total_after_org_share_discount=round($query->amount- ( $query->amount * $query->organization_share) - $query->administration_fees,0);

            $query->total_shekel_amount=$query->amount * $query->exchange_rate;
            $query->total_after_org_share_discount_shekel_amount=$query->total_after_org_share_discount * $query->exchange_rate;

            $query->sum_case_amount = PaymentsCases::totalCaseAmountByTarget($id,1);
            $query->sum_case_after_org_share_amount = PaymentsCases::totalCaseAmountByTarget($id,2);
            $query->sum_case_before_org_share_shekel_amount = PaymentsCases::totalCaseAmountByTarget($id,3);
            $query->sum_case_shekel_amount = PaymentsCases::totalCaseAmountByTarget($id,4);

            $query->sum_after_case_amount = round($query->total_amount - $query->sum_case_amount,0);
            $query->total_amount_in_shekel=round($query->amount * $query->exchange_rate,0);
            $query->total_after_org_share_discount_in_shekel=round($query->total_after_org_share_discount * $query->exchange_rate,0);
            $query->total_after_fee_discount_in_shekel=round($query->total_after_fee_discount * $query->exchange_rate,0);



            if($mode =='show'){
                $query->name='-';
                if($query->currency_id){
                    $currency=\Setting\Model\Currency::findorfail($query->currency_id);
                    if($currency){
                        $query->name=$currency->name;
                    }
                }
            }
        }
        return $query;
    }

    public static function fetchGroup($id,$payment_exchange){


        $language_id =  \App\Http\Helpers::getLocale();

        $query = self::query()
                     ->leftJoin('char_currencies','char_currencies.id',  '=', 'char_payments.currency_id')
                     ->join('char_organizations as org','org.id',  '=', 'char_payments.organization_id')
                     ->leftJoin('char_organizations as sponsor','sponsor.id',  '=', 'char_payments.sponsor_id')
                     ->leftjoin('char_payment_category_i18n', function($join) use ($language_id) {
                           $join->on('char_payments.category_id', '=', 'char_payment_category_i18n.payment_category_id')
                               ->where('char_payment_category_i18n.language_id', '=', $language_id);
                     })
                     ->whereIn('char_payments.id',$id)
                     ->selectRaw("
                                  char_payments.*,
                                  sponsor.name as sponsor_name,
                                  org.name as organization_name,
                                  org.logo as organization_logo,
                                  CASE WHEN $payment_exchange is null THEN ' '
                                       WHEN $payment_exchange = 0 THEN char_currencies.name
                                       WHEN $payment_exchange = 1 THEN ' '
                                  END  AS currency,
                                  char_payment_category_i18n.name as payment_category,
                                  sponsor.logo as sponsor_logo")
            ->get();

        if(sizeof($query) > 0){
            $query_=['sponsor_name'=>'','sponsor_logo'=>'','organization_name'=>'','organization_logo'=>'','currency'=>'','payment_category'=>[],'exchange_type'=>''];

            foreach ($query as $key=>$value){
                $query_['exchange_type']=$value->exchange_type;
                $query_['sponsor_name']=$value->sponsor_name;
                $query_['sponsor_logo']=$value->sponsor_logo;
                $query_['organization_name']=$value->organization_name;
                $query_['organization_logo']=$value->organization_logo;
                $query_['currency']=$value->currency;
                $query_['payment_category'][] = $value->payment_category;
            }

            if(sizeof($query_['payment_category'])>0){
                $payment_category = $query_['payment_category'];
                $query_['payment_category'] = implode(" , ",$payment_category);
            }
        }
        return $query_;
    }

    public static function GroupDetail($ids){

        $join_ =  implode(',', $ids) ;
        $details = ['sponsor_name'=> '' , 'shekel_amount'=> 0 , 'amount'=> 0 ,'category_map'=>[]];

        $sponsor = \DB::table('char_organizations')
            ->whereIn('id',function ($q) use ($ids){
                $q->select('sponsor_id')
                    ->from('char_payments')
                    ->whereIn('id', $ids);
            })->first();

        if(!is_null($sponsor)){
            $details['name'] = $sponsor->name;
        }

        $amount= \DB::select("SELECT SUM(amount) as amount   
                             FROM char_payments_cases
                             JOIN char_cases on char_cases .id = char_payments_cases.case_id
                             WHERE char_payments_cases.payment_id in( '$join_' ) and char_payments_cases.status != 3 ");

        if($amount)
            $details['amount'] = $amount[0]->amount;


        $shekel_amount= \DB::select("SELECT SUM(shekel_amount) as amount   
                                     FROM char_payments_cases
                                     JOIN char_cases on char_cases .id = char_payments_cases.case_id
                                     WHERE payment_id in( '$join_' )  and char_payments_cases.status != 3 ");

        if($shekel_amount)
            $details['shekel_amount'] = $shekel_amount[0]->amount;


        $details['category_map'] = \DB::table('char_payment_category_i18n')
            ->where(function ($q) use ($ids){
                $q->where('language_id', '=', 1);
                $q->whereIn('payment_category_id',function ($q) use ($ids){
                    $q->select('category_id')
                        ->from('char_payments')
                        ->whereIn('id', $ids);
                });
            })
            ->get();

        foreach ($details['category_map'] as $key=>$value){

            $value->amount = 0;
            $value->shekel_amount = 0;

            $payments =  \DB::table('char_payments')
                ->where(function ($q) use ($ids,$value){
                    $q->whereIn('id',$ids);
                    $q->where('category_id', '=', $value->payment_category_id);
                })
                ->get();


            $CatPay = [];
            foreach ($payments as  $k=>$v){
                $CatPay[]=$v->id;
            }

            $catJoin =  implode(',', $ids) ;

            if(sizeof($CatPay) > 0 ){

                $value->ben_cases = \DB::table('char_payments_cases')
                    ->whereRaw("status  !=  1 and amount != 0 and payment_id in ( '$catJoin' ) " )
                    ->count();
                $amount= \DB::select("select SUM(amount) as amount   FROM char_payments_cases
                                     join char_cases on char_cases .id = char_payments_cases.case_id
                                     where (payment_id in ( '$catJoin' ) and char_payments_cases.status != 3)  ");

                if($amount)
                    $value->amount = $amount[0]->amount;


                $shekel_amount= \DB::select("SELECT SUM(shekel_amount) as amount   
                                             FROM char_payments_cases
                                             JOIN char_cases on char_cases .id = char_payments_cases.case_id
                                             WHERE payment_id in( '$catJoin' )  and char_payments_cases.status != 3 ");

                if($shekel_amount)
                    $value->shekel_amount = $amount[0]->amount;

            }

        }

        return $details;
    }

}

