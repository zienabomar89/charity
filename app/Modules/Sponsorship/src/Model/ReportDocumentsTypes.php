<?php

namespace Sponsorship\Model;

class ReportDocumentsTypes  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_report_document_types';
    protected $primaryKey = 'id';
    protected $fillable = ['name','multiple'];

}
