<?php
namespace Sponsorship\Model;
use Illuminate\Support\Facades\App;
use Setting\Model\Setting;
use Common\Model\PersonDocument;
use App\Http\Helpers;

class Reports  extends \Illuminate\Database\Eloquent\Model
{

   protected $table = 'char_reports';
   protected $primaryKey = 'id';
   protected $hidden = ['created_at','updated_at'];
   protected $fillable = ['sponsor_id','category_id','organization_id','category','report_type','title','date','date_from','date_to','notes','folder','target','language'];

   static $rules =array('sponsor_id'   => 'required',
                         'category_id'  => 'required',
                         'category'     => 'required',
                         'all'  => '',
                         'language'  => 'required',
                         'report_type'  => 'required',
                         'title'        => 'required|max:255',
                         'cases'        => 'required_if:all,false',
                         'date_from'    => 'required',
                         'date_to'      => 'required');

// ***************************************************************************** //
    public static function filter($options)
    {

        $Annual = trans('organization::application.Annual');
        $mid_Annual = trans('organization::application.mid Annual');
        $quarter_Annual = trans('organization::application.quarter Annual');
        $monthly = trans('organization::application.monthly');
        $with_details = trans('sponsorship::application.with details');
        $without_details = trans('sponsorship::application.without details');
        $ar = trans('common::application.ar');
        $en = trans('common::application.en');

        $condition = [];
        $user = \Auth::user();
        $organization_id = $user->organization_id;
        $filters = ['sponsor_id','category_id','organization_id','category','report_type','title','notes','language'];

        foreach ($options as $key => $value) {
            if(in_array($key,$filters)) {
                if ($value != "" ) {
                    $data = ['char_reports.' . $key => (int) $value];
                    array_push($condition,$data);
                }
            }
        }

        $query=\DB::table('char_reports')
                ->join('char_categories as ca','ca.id', 'char_reports.category_id')
                ->join('char_organizations','char_organizations.id', '=','char_reports.sponsor_id')
                ->where('char_reports.organization_id',$organization_id);

        
       if(isset($options['items'])){
               if(sizeof($options['items']) > 0 ){
                $query->whereIn('char_reports.id',$options['items']);  
              }      
            } 

        //        'date','date_from','date_to'
        $begin_date_from=null;
        $begin_date_to=null;

        if(isset($options['begin_date_from']) && $options['begin_date_from'] !=null){
            $begin_date_from=date('Y-m-d',strtotime($options['begin_date_from']));
        }
        if(isset($options['begin_date_to']) && $options['begin_date_to'] !=null){
            $begin_date_to=date('Y-m-d',strtotime($options['begin_date_to']));
        }

        if($begin_date_from != null && $begin_date_to != null) {
            $query =  $query->whereBetween( 'char_reports.date_from', [ $begin_date_from, $begin_date_to]);
        }elseif($begin_date_from != null && $begin_date_to == null) {
            $query =  $query->whereDate('char_reports.date_from', '>=', $begin_date_from);
        }elseif($begin_date_from == null && $begin_date_to != null) {
            $query =  $query->whereDate('char_reports.date_from', '<=', $begin_date_to);
        }

        $end_date_from=null;
        $end_date_to=null;

        if(isset($options['end_date_from']) && $options['end_date_from'] !=null){
            $end_date_from=date('Y-m-d',strtotime($options['end_date_from']));
        }

        if(isset($options['end_date_to']) && $options['end_date_to'] !=null){
            $end_date_to=date('Y-m-d',strtotime($options['end_date_to']));
        }

        if($end_date_from != null && $end_date_to != null) {
            $query =  $query->whereBetween( 'char_reports.date_to', [ $end_date_from, $end_date_to]);
        }elseif($end_date_from != null && $end_date_to == null) {
            $query =  $query->whereDate('char_reports.date_to', '>=', $end_date_from);
        }elseif($end_date_from == null && $end_date_to != null) {
            $query =  $query->whereDate('char_reports.date_to', '<=', $end_date_to);
        }

        $created_at_to=null;
        $created_at_from=null;
        if(isset($options['created_at_from']) && $options['created_at_from'] !=null){
            $created_at_from=date('Y-m-d',strtotime($options['created_at_from']));
        }
        if(isset($options['created_at_to']) && $options['created_at_to'] !=null){
            $created_at_to=date('Y-m-d',strtotime($options['created_at_to']));
        }

        if($created_at_from != null && $created_at_to != null) {
            $query =  $query->whereBetween('char_reports.created_at', [ $created_at_from, $created_at_to]);
        }elseif($created_at_from != null && $created_at_to == null) {
            $query =  $query->whereDate('char_reports.created_at', '>=', $created_at_from);
        }elseif($created_at_from == null && $created_at_to != null) {
            $query =  $query->whereDate('char_reports.created_at', '<=', $created_at_to);
        }

        if (count($condition) != 0) {
            $query =  $query->where(function ($q) use ($condition) {
                                for ($i = 0; $i < count($condition); $i++) {
                                    foreach ($condition[$i] as $key => $value) {
                                        $q->where($key,'=',$value);
                                    }
                                }
                            });
        }

        $query->orderBy('char_reports.created_at','desc');

        if($options['action'] =='export') {

            return $query->selectRaw("char_reports.title AS report_title ,
                                      char_organizations.name as sponsor_name,
                                      ca.name as category_name,
                                      CASE WHEN char_reports.category is null THEN '-'
                                           WHEN char_reports.category = 1 THEN '$Annual'
                                           WHEN char_reports.category = 2 THEN '$mid_Annual'
                                           WHEN char_reports.category = 2 THEN '$quarter_Annual'
                                           WHEN char_reports.category = 3 THEN '$monthly'
                                      END  AS report_category,
                                      CASE WHEN char_reports.language is null THEN '-'
                                           WHEN char_reports.language = 'ar' THEN '$ar'
                                           WHEN char_reports.language = 'en' THEN '$en'
                                      END  AS report_language,
                                      CASE WHEN char_reports.category is null THEN '-'
                                           WHEN char_reports.category = 1 THEN '$Annual'
                                           WHEN char_reports.category = 2 THEN '$mid_Annual'
                                           WHEN char_reports.category = 2 THEN '$quarter_Annual'
                                           WHEN char_reports.category = 3 THEN '$monthly'
                                      END  AS report_category,
                                      CASE WHEN char_reports.report_type is null THEN '-'
                                           WHEN char_reports.report_type = 1 THEN '$with_details'
                                           WHEN char_reports.report_type = 2 THEN '$without_details'
                                      END  AS report_type,
                                      char_reports.date_from,
                                      char_reports.date_to,
                                      char_reports.notes                                    
                                     ")
                        ->get();
        }

        $query->selectRaw("char_reports.*,
                             CASE WHEN char_reports.category is null THEN '-'
                                  WHEN char_reports.category = 1 THEN '$Annual'
                                  WHEN char_reports.category = 2 THEN '$mid_Annual'
                                  WHEN char_reports.category = 2 THEN '$quarter_Annual'
                                  WHEN char_reports.category = 3 THEN '$monthly'
                             END  AS report_category_name,
                               CASE WHEN char_reports.language is null THEN '-'
                                           WHEN char_reports.language = 'ar' THEN '$ar'
                                           WHEN char_reports.language = 'en' THEN '$en'
                                      END  AS report_language,
                                    
                              CASE WHEN char_reports.report_type is null THEN '-'
                                  WHEN char_reports.report_type = 1 THEN '$with_details'
                                  WHEN char_reports.report_type = 2 THEN '$without_details'
                             END  AS report_type_name,
                             ca.name as category_name,
                             char_organizations.name as sponsor_name");

        $itemsCount = isset($options['itemsCount'])? $options['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
         return $query=$query->paginate($records_per_page);    


    }

    public static function casesReportData($report_id,$category_id,$sponsor_id){

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $report_id=1;
        $query= \DB::table('char_sponsorship_cases')
            ->join('char_cases', function($q) use ($category_id){
                $q->on('char_cases.id', '=', 'char_sponsorship_cases.case_id');
                $q->where('char_cases.category_id',$category_id);
            })
            ->join('char_organizations', 'char_organizations.id', '=', 'char_sponsorship_cases.sponsor_id')
            ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
            ->leftjoin('char_guardians', function ($q){
                $q->on('char_guardians.individual_id', '=', 'char_guardians.individual_id');
                $q->on('char_guardians.organization_id', 'char_cases.organization_id');
                $q->where('char_guardians.status','=',1);
            })
            ->where('sponsor_id',$sponsor_id)
            ->selectRaw('CONCAT(ifnull(char_persons.first_name, " "), " " ,ifnull(char_persons.second_name, " ")," ",ifnull(char_persons.third_name," ")," ", ifnull(char_persons.last_name," "))as name,
                         char_organizations.name as sponsor_name,
                         char_sponsorship_cases.id,
                         char_sponsorship_cases.sponsor_number,
                         char_sponsorship_cases.case_id,
                         char_cases.person_id,
                         char_persons.id_card_number,char_persons.old_id_card_number,
                         char_persons.father_id,
                         char_persons.mother_id,
                         char_guardians.guardian_id
                       ')->get();

        $DocTypeIds=[1,2,3,4,5,6,7];
        if(sizeof($query) > 0 ){
            foreach ($query as $k =>$v){
                $get=array('action'=>'show','person'=>true,'persons_i18n'=>true,'health'=>true,'education'=>true,'work'=>true);
                if($v->father_id != null){
                    $get['id'] = $v->father_id;
                    $v->father =\Common\Model\Person::fetch($get);
                }
                if($v->mother_id != null) {
                    $get['id'] = $v->mother_id;
                    $v->mother = \Common\Model\Person::fetch($get);
                }
                if($v->guardian_id != null) {
                    $v->guardian =\Common\Model\Person::fetch(array('action'=>'show', 'id' => $v->guardian_id,'person' =>true, 'persons_i18n' => true, 'contacts' => true,  'education' => true,
                        'work' => true, 'residence' => true, 'banks' => true));
                }

                // Get Attachments
                $v->files = [];
                 foreach ($DocTypeIds as $item) {
                        $files =\DB::table('char_sponsorship_cases_documents')
                            ->join('doc_files', 'doc_files.id', '=', 'char_sponsorship_cases_documents.document_id')
                            ->where(function ($q)use($report_id,$v,$item){
                                $q->where('document_type_id',$item);
                                $q->where('sponsorship_cases_id',$v->id);
                                $q->where('report_id',$report_id);
                            })
                            ->addSelect(['doc_files.filepath']);

                        if($item == 7){
                            $file=$files->get();
                            $path=[];
                            if(sizeof($file)>0){
                                foreach ($file as $k1=>$v2) {
                                    $path[]=$v2->filepath;
                                }
                                $v->files[]=$path;
                            }
                        }else{
                            $file=$files->first();
                            if($file){
                                $v->files[]=$file->filepath;
                            }
                        }
                    }
            }
        }

        return $query;

    }

    public static function CasesData($options){

        return ['status' => true ,'items' => []];

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $lang = $options['language'];
        $return= array();
        $return['status']=false;

        $organization_id=$options['organization_id'];
        $category_id= $options['category_id'];
        $sponsor_id= $options['sponsor_id'];
        $date_from = date('Y-m-d',strtotime($options['date_from']));
        $date_to = date('Y-m-d',strtotime($options['date_to']));
        $category = $options['category'];

        $casesList=[];
        if($options['all'] == false){
            if(isset($options['cases'])){
              if(is_array($options['cases']) ){
                  if(sizeof($options['cases'])>0){
                        foreach ($options['cases'] as $k=>$value){
                            $casesList[]=$value['id'];
                        }
                  }
              }
            }
        }
        if(isset($options['finalCases'])){
            if(is_array($options['finalCases']) ){
                if(sizeof($options['finalCases'])>0){
                    foreach ($options['finalCases'] as $value){
                        $casesList[]=(int)$value;
                    }
               }
            }
        }

        $query= \DB::table('char_sponsorship_cases')
                ->join('char_cases', function($q) use ($category_id,$organization_id){
                    $q->on('char_cases.id', '=', 'char_sponsorship_cases.case_id');
                    $q->where('char_cases.category_id',$category_id);
                    $q->where('char_cases.organization_id',$organization_id);
                })
                ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
                ->where(function ($q)use ($casesList,$sponsor_id){
                    $q->where('char_sponsorship_cases.sponsor_id',$sponsor_id);
                    if(sizeof($casesList) > 0){
                        $q->whereIn('char_sponsorship_cases.id', $casesList);
                    }
                })
                ->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as full_name,
                             char_persons.id_card_number,char_persons.old_id_card_number,                        
                             char_sponsorship_cases.sponsor_number,
                             char_sponsorship_cases.id,
                             char_sponsorship_cases.case_id,
                             char_cases.person_id,
                             char_get_sponsorship_cases_documents_file_path(char_sponsorship_cases.id,1,'$category','$date_from','$date_to') as thanks_message,
                             char_get_sponsorship_cases_documents_file_path(char_sponsorship_cases.id,2,'$category','$date_from','$date_to') as school_certificate,
                             char_get_sponsorship_cases_documents_file_path(char_sponsorship_cases.id,3,'$category','$date_from','$date_to') as long_photo,
                             char_get_sponsorship_cases_documents_file_path(char_sponsorship_cases.id,4,'$category','$date_from','$date_to') as per_photo,
                             char_get_sponsorship_cases_documents_file_path(char_sponsorship_cases.id,5,'$category','$date_from','$date_to') as single_document,
                             char_get_sponsorship_cases_documents_file_path(char_sponsorship_cases.id,6,'$category','$date_from','$date_to') as medical_report,
                             char_get_sponsorship_cases_documents_file_path(char_sponsorship_cases.id,7,'$category','$date_from','$date_to') as photo_album
                ")
                ->groupBy('char_sponsorship_cases.id')
                ->get();

        if(sizeof($query) ==0 ){
            return ['status' => false ,'items' => []];
        }

        $required=['thanks_message','school_certificate','long_photo'];
        $files=['thanks_message','school_certificate','long_photo','per_photo','single_document','medical_report','photo_album'];
        $multiple =['medical_report','photo_album'];

        foreach ($query as $key => $value) {
            if( is_null($value->long_photo) || is_null($value->per_photo) || is_null($value->single_document)){
                return ['status' => false ];
                break;
            }else{
                $value->hasFiles = false ;
                $value->files=[];
                $value->documents=[];
                foreach ($files as $k){
                    if(!is_null($value->$k)){
                        if($k == 'photo_album' || $k == 'medical_report'){
                            $docs =explode(",",$value->$k);
                            $value->files[$k]=$docs;
                            foreach ($docs as $doc) {
                                $value->documents[]=$doc;
                            }
                        }else{
                            $value->files[$k]=$value->$k;
                            $value->documents[]=$value->$k;
                        }
                    }
                }
            }
        }

        $return=[];
        foreach($query as $k =>$v){
            if(sizeof($v->documents)){
                $sub=\Common\Model\CaseModel::fetch(array(
                    'action' => 'show',
                    'category'      => true,
                    'full_name'     => true,
                    'family_member' => true,
                    'person'        => true,
                    'persons_i18n'  => true,
                    'contacts'      => true,
                    'education'     => true,
                    'islamic'       => true,
                    'health'        => true,
                    'work'          => true,
                    'residence'     => true,
                    'default_bank'  => true,
                    'banks'         => true,
                    'persons_documents'  => false,
                    'organization_name'      => true,
                    'father_id'     => true,
                    'mother_id'     => true,
                    'guardian_id'   => true,
                    'visitor_note'  => true,
                    'lang'  => $lang
                ),$v->case_id);
                $sub->documents=$v->documents;
                $sub->files=$v->files;
                $sub->sponsor_number=$v->sponsor_number;
                $get=array('action'=>'show','person'=>true,'persons_i18n'=>true,'health'=>true,'education'=>true,'work'=>true);
                if($sub->father_id != null){
                    $get['id'] = $sub->father_id;
                    $sub->father =\Common\Model\Person::fetch($get);
                }
                if($sub->mother_id != null) {
                    $get['id'] = $sub->mother_id;
                    $sub->mother = \Common\Model\Person::fetch($get);
                }
                if($sub->guardian_id != null) {
                    $sub->guardian =\Common\Model\Person::fetch(array('action'=>'show', 'id' => $sub->guardian_id,'person' =>true, 'persons_i18n' => true, 'contacts' => true,  'education' => true,
                        'work' => true, 'residence' => true, 'banks' => true));
                }
                $return[]=$sub;
            }

        }
        if(sizeof($return) ==0 ){
            return ['status' => false ,'items' => []];
        }

        return ['status' => true ,'items' => $return];
    }

    public static function Regenerate($report){

        $items = self::CasesData($report);
        if($items['status'] == true){
            unlink(storage_path('app/reports').'/'.$report->folder .'.zip');
            self::createZip($report,$items['items']);
            return true;
        }
        return false;
    }

    public static function createZip($report,$records)
    {
        $filename = storage_path('app/reports/' . $report->folder . '.zip');
        $zip = new \ZipArchive();
        $zip->open($filename,\ZipArchive::CREATE);
        $dirName = base_path('storage/app/reports').'/'.$report->folder;
        if (!is_dir($dirName)) {
            @mkdir($dirName,0777,true);
        }
        $lang = $report->language;

        $template=\Common\Model\Template::where(['template'=>'reports',
                                                 'category_id'=>$report->category_id,'organization_id'=>$report->sponsor_id])->first();
        if($template){

            if($report->report_type == 2) {
                App::setLocale($lang);
                if($lang == 'en'){
                    $path = base_path('storage/app/'.$template->en_other_filename);
                }else{
                    $path = base_path('storage/app/'.$template->other_filename);
                }

            }else{
                if($lang == 'en'){
                    $path = base_path('storage/app/'.$template->en_filename);
                }else{
                    $path = base_path('storage/app/'.$template->filename);
                }
            }

        }else{
            $default_template=Setting::where(['id'=>'Sponsorship-default-template','organization_id'=>$report->organization_id])->first();
            if($default_template){
                $path = base_path('storage/app/'.$default_template->value);
            }else{
                $path = base_path('storage/app/templates/Sponsorship-default-template.docx');
            }
        }

        foreach ($records as $k =>$v){
            $folder = $v->id_card_number . '_' . $v->full_name;
            $zip->addEmptyDir($folder);
            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);

            $personalImagePath=PersonDocument::personalImagePath($v->person_id,1);
            if(!is_null($personalImagePath)){
                $templateProcessor->setImg('img', array('src'=> $personalImagePath,'size'=>[200,350]));
            }else{
                $templateProcessor->setImg('img', array('src'=> base_path('storage/app/emptyUser.png'),'size'=>[200,350]));

            }
            $templateProcessor->setValue('date',date('y-m-d'));
            $templateProcessor->setValue('category_name',$v->category_name);
            $templateProcessor->setValue('first_name',$v->first_name);
            $templateProcessor->setValue('second_name',$v->second_name);
            $templateProcessor->setValue('third_name',$v->third_name );
            $templateProcessor->setValue('last_name',$v->last_name );
            $templateProcessor->setValue('name',$v->full_name);
            $templateProcessor->setValue('en_first_name',$v->en_first_name);
            $templateProcessor->setValue('en_second_name',$v->en_second_name);
            $templateProcessor->setValue('en_third_name',$v->en_third_name);
            $templateProcessor->setValue('en_last_name',$v->en_last_name);
            $templateProcessor->setValue('en_name',$v->en_name);
            $templateProcessor->setValue('id_card_number',$v->id_card_number);
            $templateProcessor->setValue('birthday',$v->birthday);
            $templateProcessor->setValue('gender',$v->gender);
            $templateProcessor->setValue('birth_place',$v->birth_place);
            $templateProcessor->setValue('nationality',$v->nationality);
            $templateProcessor->setValue('refugee',$v->refugee);
            $templateProcessor->setValue('unrwa_card_number',$v->unrwa_card_number);
            $templateProcessor->setValue('marital_status_name',$v->marital_status_name);
            $templateProcessor->setValue('monthly_income',$v->monthly_income);
            $templateProcessor->setValue('address',$v->address);
            $templateProcessor->setValue('country',$v->country);
            $templateProcessor->setValue('governarate',$v->governarate);
            $templateProcessor->setValue('city',$v->city);
            $templateProcessor->setValue('location_id',$v->nearLocation);
            $templateProcessor->setValue('street_address',$v->street_address);
            $templateProcessor->setValue('individual_count',$v->individual_count);
            $templateProcessor->setValue('family_count',$v->family_count);
            $templateProcessor->setValue('male_count',$v->male_count);
            $templateProcessor->setValue('female_count',$v->female_count);
            $templateProcessor->setValue('phone',$v->phone);
            $templateProcessor->setValue('primary_mobile',$v->primary_mobile);
            $templateProcessor->setValue('secondary_mobile',$v->secondary_mobile );
            $templateProcessor->setValue('property_types',$v->property_types);
            $templateProcessor->setValue('rent_value',$v->rent_value);
            $templateProcessor->setValue('roof_materials',$v->roof_materials);
            $templateProcessor->setValue('habitable',$v->habitable);
            $templateProcessor->setValue('house_condition',$v->house_condition);
            $templateProcessor->setValue('residence_condition',$v->residence_condition);
            $templateProcessor->setValue('indoor_condition',$v->indoor_condition);
            $templateProcessor->setValue('rooms',$v->rooms);
            $templateProcessor->setValue('area',$v->area);
            $templateProcessor->setValue('working',$v->working);
            $templateProcessor->setValue('works',$v->works);
            $templateProcessor->setValue('can_work',$v->can_work);
            $templateProcessor->setValue('can_works',$v->can_works);
            $templateProcessor->setValue('work_job',$v->work_job);
            $templateProcessor->setValue('work_reason',$v->work_reason);
            $templateProcessor->setValue('work_status',$v->work_status);
            $templateProcessor->setValue('work_wage',$v->work_wage);
            $templateProcessor->setValue('work_location',$v->work_location);
            $templateProcessor->setValue('needs',$v->needs);
            $templateProcessor->setValue('promised',$v->promised);
            $templateProcessor->setValue('reconstructions_organization_name',$v->reconstructions_organization_name);
            $templateProcessor->setValue('visitor',$v->visitor);
            $templateProcessor->setValue('notes',$v->notes);
            $templateProcessor->setValue('visited_at',$v->visited_at);
            $templateProcessor->setValue('health_status',$v->health_status);
            $templateProcessor->setValue('health_details',$v->health_details);
            $templateProcessor->setValue('diseases_name',$v->diseases_name);
            $templateProcessor->setValue('study',$v->study);
            $templateProcessor->setValue('type',$v->type);
            $templateProcessor->setValue('level',$v->level);
            $templateProcessor->setValue('year',$v->year);
            $templateProcessor->setValue('points',$v->points);
            $templateProcessor->setValue('school',$v->school);
            $templateProcessor->setValue('grade',$v->grade);
            $templateProcessor->setValue('authority',$v->authority);
            $templateProcessor->setValue('stage',$v->stage);
            $templateProcessor->setValue('degree',$v->degree);
            $templateProcessor->setValue('prayer',$v->prayer);
            $templateProcessor->setValue('prayer_reason',$v->prayer_reason);
            $templateProcessor->setValue('quran_reason',$v->quran_reason);
            $templateProcessor->setValue('quran_parts',$v->quran_parts);
            $templateProcessor->setValue('quran_chapters',$v->quran_chapters);
            $templateProcessor->setValue('save_quran',$v->save_quran);
            $templateProcessor->setValue('visitor_note',$v->visitor_note);
            $templateProcessor->setValue('sponsor_name',$v->sponsor_name);
            $templateProcessor->setValue('guardian_kinship',$v->guardian_kinship);

            $setting_id = 'sponsorship_custom_form';
            $CaseCustomData =\Forms\Model\FormsCasesData::getCaseCustomData($v->case_id,$item->category_id,$v->organization_id,$setting_id);

            if (sizeof($CaseCustomData)!=0){
                try{
                    $templateProcessor->cloneRow('row_custom_name', sizeof($CaseCustomData));
                    $z=1;
                    foreach($CaseCustomData as $cValue) {
                        $templateProcessor->setValue('row_custom_name#' . $z, $cValue['label']);
                        $templateProcessor->setValue('row_custom_value#' . $z, $cValue['value']);
                        $z++;
                    }
                }catch (\Exception $e){

                }
            }
            else{
                try{
                    $templateProcessor->cloneRow('row_custom_name',1);
                    $templateProcessor->setValue('row_custom_name#' . 1, '-');
                    $templateProcessor->setValue('row_custom_value#' . 1, '-');
                }catch (\Exception $e){  }
            }
            if (sizeof($v->banks)!=0){
                try{
                    $templateProcessor->cloneRow('r_bank_name',sizeof($v->banks));
                    $z=1;
                    foreach($v->banks as $rec) {
                        $templateProcessor->setValue('r_bank_name#' . $z,$rec->bank_name);
                        $templateProcessor->setValue('r_branch_name#' . $z,$rec->branch);
                        $templateProcessor->setValue('r_account_owner#' . $z,$rec->account_owner);
                        $templateProcessor->setValue('r_account_number#' . $z,$rec->account_number);
                        if($rec->check == true){
                            $templateProcessor->setValue('r_default#' . $z,trans('aid::application.yes') );
                        }else{
                            $templateProcessor->setValue('r_default#' . $z,trans('aid::application.no') );
                        }
                        $z++;
                    }
                }catch (\Exception $e){}
            }else{
                try{
                    $templateProcessor->cloneRow('r_bank_name',1);
                    $templateProcessor->setValue('r_bank_name#' . 1,'-');
                    $templateProcessor->setValue('r_branch_name#' . 1,'-');
                    $templateProcessor->setValue('r_account_owner#' . 1,'-');
                    $templateProcessor->setValue('r_account_number#' . 1,'-');
                    $templateProcessor->setValue('r_default#' . 1,'-');
                }catch (\Exception $e){  }
            }
            if (sizeof($v->home_indoor)!=0){
                try{
                    $templateProcessor->cloneRow('row_es_name',sizeof($v->home_indoor));
                    $z=1;
                    foreach($v->home_indoor as $record) {
                        $templateProcessor->setValue('row_es_name#' . $z,$record->name);
                        $templateProcessor->setValue('row_es_if_exist#' . $z,$record->exist);
                        $templateProcessor->setValue('row_es_needs#' . $z,$record->needs);
                        $templateProcessor->setValue('row_es_condition#' . $z,$record->essentials_condition);
                        $z++;
                    }
                }catch (\Exception $e){  }
            }else{
                try{
                    $templateProcessor->cloneRow('row_es_name',1);
                    $templateProcessor->setValue('row_es_name#' . 1,'-');
                    $templateProcessor->setValue('row_es_if_exist#' . 1,'-');
                    $templateProcessor->setValue('row_es_needs#' . 1,'-');
                    $templateProcessor->setValue('row_es_condition#' . 1,'-');

                }catch (\Exception $e){  }
            }
            if (sizeof($v->persons_properties)!=0){
                try{
                    $templateProcessor->cloneRow('row_pro_name',sizeof($v->persons_properties));
                    $z2=1;
                    foreach($v->persons_properties as $sub_record) {
                        $templateProcessor->setValue('row_pro_name#' . $z2,$sub_record->name);
                        $templateProcessor->setValue('row_pro_if_exist#' . $z2,$sub_record->has_property);
                        $templateProcessor->setValue('row_pro_count#' . $z2,$sub_record->quantity);
                        $z2++;
                    }
                }catch (\Exception $e){  }
            }else{
                try{
                    $templateProcessor->cloneRow('row_pro_name',1);
                    $templateProcessor->setValue('row_pro_name#' . 1,'-');
                    $templateProcessor->setValue('row_pro_if_exist#' . 1,'-');
                    $templateProcessor->setValue('row_pro_count#' . 1,'-');
                }catch (\Exception $e){  }
            }
            if (sizeof($v->financial_aid_source)!=0){
                try{
                    $templateProcessor->cloneRow('row_fin_name',sizeof($v->financial_aid_source));
                    $z3=1;
                    foreach($v->financial_aid_source as $record) {
                        $templateProcessor->setValue('row_fin_name#' . $z3,$record->name);
                        $templateProcessor->setValue('row_fin_if_exist#' . $z3,$record->aid_take);
                        $templateProcessor->setValue('row_fin_count#' . $z3,$record->aid_value);
                        $templateProcessor->setValue('row_fin_currency#' . $z3,$record->currency_name);
                        $z3++;
                    }
                }catch (\Exception $e){  }
            }else{
                try{
                    $templateProcessor->cloneRow('row_fin_name',1);
                    $templateProcessor->setValue('row_fin_name#' . 1,'-');
                    $templateProcessor->setValue('row_fin_if_exist#' . 1,'-');
                    $templateProcessor->setValue('row_fin_count#' . 1,'-');
                    $templateProcessor->setValue('row_fin_currency#' . 1,'-');
                }catch (\Exception $e){  }
            }
            if (sizeof($v->non_financial_aid_source)!=0){
                try{
                    $templateProcessor->cloneRow('row_nonfin_name',sizeof($v->non_financial_aid_source));
                    $z3=1;
                    foreach($v->non_financial_aid_source as $record) {
                        $templateProcessor->setValue('row_nonfin_name#' . $z3,$record->name);
                        $templateProcessor->setValue('row_nonfin_if_exist#' . $z3,$record->aid_take);
                        $templateProcessor->setValue('row_nonfin_count#' . $z3,$record->aid_value);
                        $z3++;
                    }
                }catch (\Exception $e){  }
            } else{
                try{
                    $templateProcessor->cloneRow('row_nonfin_name',1);
                    $templateProcessor->setValue('row_nonfin_name#' . 1,'-');
                    $templateProcessor->setValue('row_nonfin_if_exist#' . 1,'-');
                    $templateProcessor->setValue('row_nonfin_count#' . 1,'-');
                }catch (\Exception $e){  }
            }
            if (sizeof($v->family_member)!=0){
                try{
                    $templateProcessor->cloneRow('fm_nam',sizeof($v->family_member));
                    $z=1;
                    foreach($v->family_member as $record) {
                        $templateProcessor->setValue('fm_nam#' . $z,$record->name);
                        $templateProcessor->setValue('fm_bd#' . $z,$record->birthday);
                        $templateProcessor->setValue('fm_gen#' . $z,$record->gender);
                        $templateProcessor->setValue('fm_idc#' . $z,$record->id_card_number);
                        $templateProcessor->setValue('fm_ms#' . $z,$record->marital_status);
                        $templateProcessor->setValue('fm_kin#' . $z,$record->kinship_name);
                        $templateProcessor->setValue('fm_stg#' . $z,$record->stage);
                        $templateProcessor->setValue('fm_hs#' . $z,$record->health_status);
                        $z++;
                    }
                }catch (\Exception $e){  }
            }else{
                try{
                    $templateProcessor->cloneRow('fm_nam',1);
                    $templateProcessor->setValue('fm_nam#' . 1,'-');
                    $templateProcessor->setValue('fm_bd#' . 1,'-');
                    $templateProcessor->setValue('fm_gen#' . 1,'-');
                    $templateProcessor->setValue('fm_idc#' . 1,'-');
                    $templateProcessor->setValue('fm_ms#' . 1,'-');
                    $templateProcessor->setValue('fm_kin#' . 1,'-');
                    $templateProcessor->setValue('fm_stg#' . 1,'-');
                    $templateProcessor->setValue('fm_hs#' . 1,'-');
                }catch (\Exception $e){  }
            }
            if($v->father_id != null){
                $templateProcessor->setValue('father_first_name',$v->father->first_name);
                $templateProcessor->setValue('father_second_name',$v->father->second_name);
                $templateProcessor->setValue('father_third_name',$v->father->third_name );
                $templateProcessor->setValue('father_last_name',$v->father->last_name );
                $templateProcessor->setValue('father_name',$v->father->full_name);
                $templateProcessor->setValue('father_spouses',$v->father->spouses);
                $templateProcessor->setValue('father_id_card_number',$v->father->id_card_number);
                $templateProcessor->setValue('father_birthday',$v->father->birthday);
                $templateProcessor->setValue('father_en_first_name',$v->father->en_first_name);
                $templateProcessor->setValue('father_en_second_name',$v->father->en_second_name);
                $templateProcessor->setValue('father_en_third_name',$v->father->en_third_name);
                $templateProcessor->setValue('father_en_last_name',$v->father->en_last_name);
                $templateProcessor->setValue('father_health_status',$v->father->health_status);
                $templateProcessor->setValue('father_health_details',$v->father->health_details);
                $templateProcessor->setValue('father_diseases_name',$v->father->diseases_name);
                $templateProcessor->setValue('father_specialization',$v->father->specialization);
                $templateProcessor->setValue('father_degree',$v->father->degree);
                $templateProcessor->setValue('father_alive',$v->father->alive);
                $templateProcessor->setValue('father_is_alive',$v->father->is_alive);
                $templateProcessor->setValue('father_death_date',$v->father->death_date);
                $templateProcessor->setValue('father_death_cause_name',$v->father->death_cause_name);
                $templateProcessor->setValue('father_working',$v->father->working);
                $templateProcessor->setValue('father_work_job',$v->father->work_job);
                $templateProcessor->setValue('father_work_location',$v->father->work_location);
                $templateProcessor->setValue('father_monthly_income',$v->father->monthly_income);
            }
            else{
                $templateProcessor->setValue('father_first_name','-');
                $templateProcessor->setValue('father_second_name','-');
                $templateProcessor->setValue('father_third_name','-');
                $templateProcessor->setValue('father_last_name','-');
                $templateProcessor->setValue('father_name','-');
                $templateProcessor->setValue('father_spouses','-');
                $templateProcessor->setValue('father_id_card_number','-');
                $templateProcessor->setValue('father_birthday','-');
                $templateProcessor->setValue('father_en_first_name','-');
                $templateProcessor->setValue('father_en_second_name','-');
                $templateProcessor->setValue('father_en_third_name','-');
                $templateProcessor->setValue('father_en_last_name','-');
                $templateProcessor->setValue('father_health_status','-');
                $templateProcessor->setValue('father_health_details','-');
                $templateProcessor->setValue('father_diseases_name','-');
                $templateProcessor->setValue('father_specialization','-');
                $templateProcessor->setValue('father_degree','-');
                $templateProcessor->setValue('father_alive','-');
                $templateProcessor->setValue('father_is_alive','-');
                $templateProcessor->setValue('father_death_date','-');
                $templateProcessor->setValue('father_death_cause_name','-');
                $templateProcessor->setValue('father_working','-');
                $templateProcessor->setValue('father_work_job','-');
                $templateProcessor->setValue('father_work_location','-');
                $templateProcessor->setValue('father_monthly_income','-');
            }
            if($v->mother_id != null){
                $templateProcessor->setValue('mother_prev_family_name',$v->mother->prev_family_name);
                $templateProcessor->setValue('mother_first_name',$v->mother->first_name);
                $templateProcessor->setValue('mother_second_name',$v->mother->second_name);
                $templateProcessor->setValue('mother_third_name',$v->mother->third_name);
                $templateProcessor->setValue('mother_last_name',$v->mother->last_name);
                $templateProcessor->setValue('mother_name',$v->mother->full_name);
                $templateProcessor->setValue('mother_id_card_number',$v->mother->id_card_number);
                $templateProcessor->setValue('mother_birthday',$v->mother->birthday);
                $templateProcessor->setValue('mother_marital_status_name',$v->mother->marital_status_name);
                $templateProcessor->setValue('mother_nationality',$v->mother->nationality);
                $templateProcessor->setValue('mother_en_first_name',$v->mother->en_first_name);
                $templateProcessor->setValue('mother_en_second_name',$v->mother->en_second_name);
                $templateProcessor->setValue('mother_en_third_name',$v->mother->en_third_name);
                $templateProcessor->setValue('mother_en_last_name',$v->mother->en_last_name);
                $templateProcessor->setValue('mother_health_status',$v->mother->health_status);
                $templateProcessor->setValue('mother_health_details',$v->mother->health_details);
                $templateProcessor->setValue('mother_diseases_name',$v->mother->diseases_name);
                $templateProcessor->setValue('mother_specialization',$v->mother->specialization);
                $templateProcessor->setValue('mother_stage',$v->mother->stage);
                $templateProcessor->setValue('mother_alive',$v->mother->alive);
                $templateProcessor->setValue('mother_is_alive',$v->mother->is_alive);
                $templateProcessor->setValue('mother_death_date',$v->mother->death_date);
                $templateProcessor->setValue('mother_death_cause_name',$v->mother->death_cause_name);
                $templateProcessor->setValue('mother_working',$v->mother->working);
                $templateProcessor->setValue('mother_work_job',$v->mother->work_job);
                $templateProcessor->setValue('mother_work_location',$v->mother->work_location);
                $templateProcessor->setValue('mother_monthly_income',$v->mother->monthly_income);
            }
            else{
                $templateProcessor->setValue('mother_prev_family_name','-');
                $templateProcessor->setValue('mother_first_name','-');
                $templateProcessor->setValue('mother_second_name','-');
                $templateProcessor->setValue('mother_third_name','-');
                $templateProcessor->setValue('mother_last_name','-');
                $templateProcessor->setValue('mother_name','-');
                $templateProcessor->setValue('mother_id_card_number','-');
                $templateProcessor->setValue('mother_birthday','-');
                $templateProcessor->setValue('mother_marital_status_name','-');
                $templateProcessor->setValue('mother_nationality','-');
                $templateProcessor->setValue('mother_en_first_name','-');
                $templateProcessor->setValue('mother_en_second_name','-');
                $templateProcessor->setValue('mother_en_third_name','-');
                $templateProcessor->setValue('mother_en_last_name','-');
                $templateProcessor->setValue('mother_health_status','-');
                $templateProcessor->setValue('mother_health_details','-');
                $templateProcessor->setValue('mother_diseases_name','-');
                $templateProcessor->setValue('mother_specialization','-');
                $templateProcessor->setValue('mother_stage','-');
                $templateProcessor->setValue('mother_alive','-');
                $templateProcessor->setValue('mother_is_alive','-');
                $templateProcessor->setValue('mother_death_date','-');
                $templateProcessor->setValue('mother_death_cause_name','-');
                $templateProcessor->setValue('mother_working','-');
                $templateProcessor->setValue('mother_work_job','-');
                $templateProcessor->setValue('mother_work_location','-');
                $templateProcessor->setValue('mother_monthly_income','-');
            }
            if($v->guardian_id != null){
                $templateProcessor->setValue('guardian_first_name',$v->guardian->first_name);
                $templateProcessor->setValue('guardian_second_name',$v->guardian->second_name);
                $templateProcessor->setValue('guardian_third_name',$v->guardian->third_name );
                $templateProcessor->setValue('guardian_last_name',$v->guardian->last_name );
                $templateProcessor->setValue('guardian_name',$v->guardian->full_name);
                $templateProcessor->setValue('guardian_id_card_number',$v->guardian->id_card_number);
                $templateProcessor->setValue('guardian_birthday',$v->guardian->birthday);
                $templateProcessor->setValue('guardian_birth_place',$v->guardian->birth_place);
                $templateProcessor->setValue('guardian_nationality',$v->guardian->nationality);
                $templateProcessor->setValue('guardian_country',$v->guardian->country);
                $templateProcessor->setValue('guardian_governarate',$v->guardian->governarate);
                $templateProcessor->setValue('guardian_city',$v->guardian->city);
                $templateProcessor->setValue('guardian_location_id',$v->guardian->location_id);
                $templateProcessor->setValue('guardian_street_address',$v->guardian->street_address);
                $templateProcessor->setValue('guardian_address',$v->guardian->address);
                $templateProcessor->setValue('guardian_phone',$v->guardian->phone);
                $templateProcessor->setValue('guardian_primary_mobile',$v->guardian->primary_mobile);
                $templateProcessor->setValue('guardian_secondery_mobile',$v->guardian->secondary_mobile );
                $templateProcessor->setValue('guardian_en_first_name',$v->guardian->en_first_name);
                $templateProcessor->setValue('guardian_en_second_name',$v->guardian->en_second_name);
                $templateProcessor->setValue('guardian_en_third_name',$v->guardian->en_third_name);
                $templateProcessor->setValue('guardian_en_last_name',$v->guardian->en_last_name);
                $templateProcessor->setValue('guardian_property_types',$v->guardian->property_types);
                $templateProcessor->setValue('guardian_roof_materials',$v->guardian->roof_materials);
                $templateProcessor->setValue('guardian_area',$v->guardian->area);
                $templateProcessor->setValue('guardian_rooms',$v->guardian->rooms);
                $templateProcessor->setValue('guardian_residence_condition',$v->guardian->residence_condition);
                $templateProcessor->setValue('guardian_indoor_condition',$v->guardian->indoor_condition);
                $templateProcessor->setValue('guardian_stage',$v->guardian->stage);
                $templateProcessor->setValue('guardian_working',$v->guardian->working);
                $templateProcessor->setValue('guardian_work_job',$v->guardian->work_job);
                $templateProcessor->setValue('guardian_work_location',$v->guardian->work_location);
                $templateProcessor->setValue('guardian_monthly_income',$v->guardian->monthly_income);
            }
            else{

                $templateProcessor->setValue('guardian_first_name','-');
                $templateProcessor->setValue('guardian_second_name','-');
                $templateProcessor->setValue('guardian_third_name','-');
                $templateProcessor->setValue('guardian_last_name','-');
                $templateProcessor->setValue('guardian_name','-');
                $templateProcessor->setValue('guardian_id_card_number','-');
                $templateProcessor->setValue('guardian_birthday','-');
                $templateProcessor->setValue('guardian_birth_place','-');
                $templateProcessor->setValue('guardian_nationality','-');
                $templateProcessor->setValue('guardian_country','-');
                $templateProcessor->setValue('guardian_governarate','-');
                $templateProcessor->setValue('guardian_city','-');
                $templateProcessor->setValue('guardian_location_id','-');
                $templateProcessor->setValue('guardian_street_address','-');
                $templateProcessor->setValue('guardian_address','-');
                $templateProcessor->setValue('guardian_phone','-');
                $templateProcessor->setValue('guardian_primary_mobile','-');
                $templateProcessor->setValue('guardian_secondery_mobile','-');
                $templateProcessor->setValue('guardian_en_first_name','-');
                $templateProcessor->setValue('guardian_en_second_name','-');
                $templateProcessor->setValue('guardian_en_third_name','-');
                $templateProcessor->setValue('guardian_en_last_name','-');
                $templateProcessor->setValue('guardian_property_types','-');
                $templateProcessor->setValue('guardian_roof_materials','-');
                $templateProcessor->setValue('guardian_area','-');
                $templateProcessor->setValue('guardian_rooms','-');
                $templateProcessor->setValue('guardian_residence_condition','-');
                $templateProcessor->setValue('guardian_indoor_condition','-');
                $templateProcessor->setValue('guardian_stage','-');
                $templateProcessor->setValue('guardian_working','-');
                $templateProcessor->setValue('guardian_work_job','-');
                $templateProcessor->setValue('guardian_work_location','-');
                $templateProcessor->setValue('guardian_monthly_income','-');
            }

            if (sizeof($v->files)!=0){
                try{
                    $files = $v->files;

                    if(isset($files['thanks_message'])){
                        $templateProcessor->setImg('thanks_message',array('src'=> base_path('storage/app/').$v->files['thanks_message'],'swh'=>'650'));
                    }

                    if(isset($files['school_certificate'])){
                        $templateProcessor->setImg('school_certificate',array('src'=> base_path('storage/app/').$v->files['school_certificate'],'swh'=>'650'));
                    }

                    if(isset($files['long_photo'])){
                        $templateProcessor->setImg('long_photo',array('src'=> base_path('storage/app/').$v->files['long_photo'],'swh'=>'650'));
                    }

                    if(isset($files['per_photo'])){
                        $templateProcessor->setImg('per_photo',array('src'=> base_path('storage/app/').$v->files['per_photo'],'swh'=>'650'));
                    }

                    if(isset($files['single_document'])){
                        $templateProcessor->setImg('single_document',array('src'=> base_path('storage/app/').$v->files['single_document'],'swh'=>'650'));
                    }

                    if(isset($files['medical_report'])){
                        $medical_reports = $files['medical_report'];
                        $i=0;
                        $templateProcessor->cloneRow('medical_report',sizeof($medical_reports));
                        foreach ($medical_reports as $photo){
                            $templateProcessor->setImg('medical_report#'.$i,array('src'=> base_path('storage/app/').$photo,'swh'=>'650'));
                            $i++;
                        }
                    }
                    else{
                        try{
                            $templateProcessor->cloneRow('medical_report',1);
                            $templateProcessor->setImg('medical_report#' . 1 , array('src'=> base_path('storage/app/emptyUser.png'),'swh'=>'650'));
                        }catch (\Exception $e){  }

                    }

                    if(isset($files['photo_album'])){
                        try{
                            $photo_albums = $files['photo_album'];
                            $i=0;
                            $templateProcessor->cloneRow('photo_album',sizeof($photo_albums));
                            foreach ($photo_albums as $photo){
                                $templateProcessor->setImg('photo_album#'.$i,array('src'=> base_path('storage/app/').$photo,'swh'=>'650'));
                                $i++;
                            }
                        }catch (\Exception $e){  }
                    }
                    else{
                        try{
                            $templateProcessor->cloneRow('photo_album',1);
                            $templateProcessor->setImg('photo_album#' . 1 , array('src'=> base_path('storage/app/emptyUser.png'),'swh'=>'650'));
                        }catch (\Exception $e){  }

                    }



                }catch (\Exception $e){  }
            } else{

            }

            $templateProcessor->saveAs($dirName.'/'.$v->id_card_number.'.docx');
            $zip->addFile($dirName.'/'.$v->id_card_number.'.docx',$folder .'/'."التقرير".".docx");

            if($report->report_type == 2 && sizeof($v->files) > 0 ){
                $subFolder = $folder .'/'.'المرفقات';
                foreach ($v->files as $key => $value) {
                    if($key == 'photo_album' || $key == 'medical_report'){
                        $dir= $subFolder . '/'.trans('sponsorship::application.'. $key) ;
                        $zip->addEmptyDir($dir);
                        $i=1;
                        foreach ($value as $photo){
                            $photoPath = storage_path('app/').$photo;
                            $ext = explode(".",$photo);
                            $zip->addFile($photoPath,$subFolder .'/'.trans('sponsorship::application.'. $key).'/' .trans('sponsorship::application.'. $key)."_".$i.".".end($ext));
                            $i++;
                        }
                    }else{
                        $photoPath = storage_path('app/').$value;
                        $ext = explode(".",$value);
                        $zip->addFile($photoPath,$subFolder .'/' .trans('sponsorship::application.'. $key).".".end($ext));
                    }
                }
            }
        }
        $zip->close();

        App::setLocale('ar');

        $dir = $dirName;
        $it = new \RecursiveDirectoryIterator($dir,\RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new \RecursiveIteratorIterator($it,\RecursiveIteratorIterator::CHILD_FIRST);
        foreach($files as $file) {
            if ($file->isDir()){
                rmdir($file->getRealPath());
            } else {
                unlink($file->getRealPath());
            }
        }
        rmdir($dir);

    }

    }

