<?php
namespace Sponsorship\Model;

use Setting\Model\LocationI18n;
use App\Http\Helpers;

class PaymentsRecipient  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_payments_recipient';
    protected $primaryKey = 'id';
    protected $fillable = ['payment_id','person_id','mother_id','father_id','amount','shekel_amount','amount_after_discount','shekel_amount_before_discount','bank_id','cheque_account_number','cheque_date','status'];
    protected $hidden = ['created_at','updated_at'];

    public function individual()
    {
        return $this->hasMany('Sponsorship\Model\PaymentsCases', 'guardian_id', 'person_id');
    }

    public function person()
    {
        return $this->belongsTo('Common\Model\Person','person_id', 'id');
    }

    public function payment()
    {
        return $this->belongsTo('Sponsorship\Model\Payments','payment_id', 'id');
    }

    public static function setOneRecipient ($payment_id,$new_recipient,$case_id,$amount,$shekel_amount,$recipient){

        $payment=\DB::table('char_payments')->where('char_payments.id',$payment_id)->first();
        $payment_bank= $payment->bank_id;
        $account_number= null;
        $item=\DB::table('char_cases')
            ->join('char_persons as c','c.id',  '=', 'char_cases.person_id')
            ->leftjoin('char_persons_banks', function($q) use($payment_bank){
                $q->on('char_cases.person_id','=','char_persons_banks.person_id');
                $q->where('char_persons_banks.bank_id','=',$payment_bank);
            })
            ->where('char_cases.id','=',$case_id)
            ->selectRaw("c.father_id, c.mother_id,char_persons_banks.account_number")->first();

        if($payment_bank !=null && $payment->exchange_type == 1 && $recipient ==1){
            $bank_account =\DB::table('char_persons_banks')
                ->where('char_persons_banks.bank_id','=',$payment_bank)
                ->where('char_persons_banks.person_id','=',$new_recipient)
                ->first();

            if($bank_account){
                $account_number= $bank_account->account_number;
            }
        }

        if($payment_bank !=null && $payment->exchange_type == 1 && $recipient ==0){
            $account_number= $item->account_number;
        }

        $insert = PaymentsRecipient::create(['payment_id' => $payment_id,
            'person_id' =>$new_recipient,
            'mother_id'=>$item->mother_id,
            'father_id'=>$item->father_id,
            'cheque_account_number'=>$account_number,
            'amount'=>$amount,
            'shekel_amount' =>$shekel_amount,'bank_id'=>$payment_bank, 'status'=>0]);

        return $insert;

    }
    public static function getRecord($id,$status,$page)
    {
        $query=\DB::table('char_payments_recipient')
            ->join('char_persons', 'char_payments_recipient.person_id', '=', 'char_persons.id')
            ->where('char_payments_recipient.payment_id','=',$id)
            ->where('char_payments_recipient.amount','>',0)
            ->orderBy('char_persons.country')
            ->orderBy('char_persons.governarate')
            ->orderBy('char_persons.city')
            ->orderBy('char_persons.location_id')
            ->orderBy('char_persons.first_name')
            ->orderBy('char_persons.second_name')
            ->orderBy('char_persons.third_name')
            ->orderBy('char_persons.last_name')
            ->selectRaw("char_payments_recipient.* , char_persons.id_card_number,char_persons.old_id_card_number,
                            CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                            ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name
                 ")
            ->paginate(config('constants.records_per_page'));

        return $query;

    }
    public static function saveRecipient($id,$inputs)
    {


        $action = $inputs['action'];

        $response=['status' => true];
        $cheque_number =null;
        $updated =[];

        $update=true;
        $required=false;

        if($action =='bank'){
            if(isset($inputs['bank_id'])){
                if($inputs['bank_id'] != null || $inputs['bank_id'] != ""){
                    $updated["bank_id"]= $inputs['bank_id'];
                    $required=true;
                }
            }
        }
        else if($action =='cheque_date'){
            if(isset($inputs['cheque_date'])){
                if($inputs['cheque_date'] != null || $inputs['cheque_date'] != ""){
                    $updated["cheque_date"]= date('Y-m-d',strtotime($inputs['cheque_date']));
                    $required=true;
                }
            }
        }
        else if($action =='cheque_number'){
            if($inputs['cheque_account_number'] != null || $inputs['cheque_account_number'] != ""){
                $cheque_number =$inputs['cheque_account_number'];
                $required=true;
            }
        }

        if($required == true){
            $items=\DB::table('char_payments_recipient')
                ->join('char_persons', 'char_payments_recipient.person_id', '=', 'char_persons.id')
                ->where('char_payments_recipient.payment_id','=',$id)
                ->orderBy('char_persons.country')
                ->orderBy('char_persons.governarate')
                ->orderBy('char_persons.city')
                ->orderBy('char_persons.location_id')
                ->orderBy('char_persons.first_name')
                ->orderBy('char_persons.second_name')
                ->orderBy('char_persons.third_name')
                ->orderBy('char_persons.last_name')
                ->selectRaw("char_payments_recipient.id")
                ->get();
            foreach($items as $k => $v){

                if(!is_null($cheque_number)){
                    $updated["cheque_account_number"]= $cheque_number;
                }

                self::where(["id"=>$v->id])->update($updated);

                if(!is_null($cheque_number)){
                    $cheque_number++;
                }

            }
            \Log\Model\Log::saveNewLog('PAYMENTS_RECIPIENT_UPDATED', trans('sponsorship::application.update on payment recipients data') . ' : ' . $id);
        }

        if($update) {
            $response["status"]= 'success';
            $response["msg"]= trans('sponsorship::application.The row is updated');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sponsorship::application.There is no update');

        }
        return $response;

    }
    public static function saveCheques($filter)
    {

        $action = $filter['action'];

        $response=['status' => true];
        $cheque_number =null;
        $updated =[];
        $district =[];

        $update=true;
        $required=false;

        if($action =='cheque_date'){
            if(isset($filter['cheque_date'])){
                if($filter['cheque_date'] != null || $filter['cheque_date'] != ""){
                    $updated["cheque_date"]= date('Y-m-d',strtotime($filter['cheque_date']));
                    $required=true;
                }
            }
        }
        else if($action =='cheque_number'){
            if($filter['cheque_account_number'] != null || $filter['cheque_account_number'] != ""){
                $cheque_number =$filter['cheque_account_number'];
                $required=true;
            }
        }

        if($required == true){
            $ids=$filter['ids'];
            $pIds = "".join(",",$ids)."";

            $items = \DB::table('char_payments_recipient')
                ->join('char_persons','char_persons.id',  '=', 'char_payments_recipient.person_id')
                ->join('char_payments', 'char_payments.id', '=', 'char_payments_recipient.payment_id')
                ->join('char_organizations as sponsor', 'char_payments.sponsor_id', '=', 'sponsor.id')
                ->join('char_currencies','char_payments.currency_id','=','char_currencies.id')
                ->leftjoin('char_persons_contact', function($join)   {
                    $join->on('char_payments_recipient.person_id', '=','char_persons_contact.person_id')
                        ->where('char_persons_contact.contact_type','primary_mobile');
                })
                ->leftJoin('char_locations_i18n As location_name', function($join)   {
                    $join->on('char_persons.location_id', '=','location_name.location_id' )
                        ->where('location_name.language_id',1);
                })
                ->leftjoin('char_locations_i18n As city_name', function($join)   {
                    $join->on('char_persons.city', '=','city_name.location_id' )
                        ->where('city_name.language_id',1);
                })
                ->leftjoin('char_locations_i18n As district_name', function($join)   {
                    $join->on('char_persons.governarate', '=','district_name.location_id' )
                        ->where('district_name.language_id',1);
                })
                ->leftjoin('char_locations_i18n As country_name', function($join)   {
                    $join->on('char_persons.country', '=','country_name.location_id' )
                        ->where('country_name.language_id',1);
                })
                ->whereIn('payment_id',$ids)
                ->orderBy('char_persons.country')
                ->orderBy('char_persons.governarate')
                ->orderBy('char_persons.city')
                ->orderBy('char_persons.location_id')
                ->orderBy('char_persons.first_name')
                ->orderBy('char_persons.second_name')
                ->orderBy('char_persons.third_name')
                ->orderBy('char_persons.last_name')
                ->groupBy('char_payments_recipient.person_id')
                ->selectRaw("char_payments_recipient.person_id,char_persons.governarate")
                ->get();

            foreach($items as $k => $v){
                $pEId=$v->person_id;

                if(!is_null($cheque_number)){
                    $updated["cheque_account_number"]= $cheque_number;
                }

                self::where(function($subQuery) use($pEId,$ids)  {
                    $subQuery->where("person_id",$pEId);
                    $subQuery->whereIn("payment_id",$ids);
                })->update($updated);

                if(!is_null($cheque_number)){
                    $cheque_number++;
                }

                if(!is_null($v->governarate)){
                    if(!in_array($v->governarate,$district)){
                        $district[]=$v->governarate;
                    }
                }
            }
            \Log\Model\Log::saveNewLog('PAYMENTS_RECIPIENT_UPDATED', trans('sponsorship::application.update on payment recipients data') . ' : ' . $pIds);
        }
        $response["total_amount"]=self::filterAmount($ids,null,'amount');
        $response["total_shekel_amount"]=self::filterAmount($ids,null,'shekel_amount');

        $language_id =  \App\Http\Helpers::getLocale();

        if(sizeof($district) >0 ){
            $districts=LocationI18n::where(function ($q)use ($district,$language_id){
                $q->where('language_id',$language_id);
                $q->whereIn('location_id',$district);
            })->get();
            foreach ($districts as $k=>$v){
                $v->amount =self::filterAmount($ids,$v->location_id,'amount');
                $v->amount_after_discount =self::filterAmount($ids,$v->location_id,'amount_after_discount');
                $v->shekel_amount =self::filterAmount($ids,$v->location_id,'shekel_amount');
                $v->shekel_amount_before_discount =self::filterAmount($ids,$v->location_id,'shekel_amount_before_discount');
            }

            $response["districts"]=$districts;
        }

        if($update) {
            $response["status"]= 'success';
            $response["msg"]= trans('sponsorship::application.The row is updated');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sponsorship::application.There is no update');

        }
        return $response;

    }
    public static function filterAmount($pIds,$governarate,$target){

        $list =\DB::table('char_cases')
            ->join('char_payments_cases as p', 'p.case_id','=','char_cases.id')
            ->join('char_payments','char_payments.id','=','p.payment_id')
            ->join('char_persons as c','c.id',  '=', 'char_cases.person_id')
            ->leftjoin('char_persons AS g','g.id','=','p.guardian_id')
            ->whereIn('payment_id',$pIds);


        if($governarate != null){
            $list->whereRaw("((p.recipient = 0 AND c.governarate = '$governarate' )or  (p.recipient = 1 AND g.governarate = '$governarate'))");
        }

        if($target == 'amount'){
            $total=$list->selectRaw("sum( p.amount) as total")->first();
        }
        elseif($target == 'amount_after_discount'){
            $total=$list->selectRaw("sum( p.amount_after_discount) as total")->first();
        }
        elseif($target == 'shekel_amount_before_discount'){
            $total=$list->selectRaw("sum( p.shekel_amount_before_discount) as total")->first();
        }
        else{
            $total=$list->selectRaw("sum( p.shekel_amount) as total")->first();
        }

        if($total){
            return $total->total;
        }
        return 0;
    }
    public static function OfSetExport($options){

        $ids=$options['payment_ids'];
        $pIds = join(",",$ids);
        $payment_exchange=$options['payment_exchange'];
        $shekel=trans('sponsorship::application.shekel');

        $query=\DB::table('char_payments_recipient')
            ->join('char_persons','char_persons.id',  '=', 'char_payments_recipient.person_id')
            ->join('char_payments', 'char_payments.id', '=', 'char_payments_recipient.payment_id')
            ->join('char_organizations as sponsor', 'char_payments.sponsor_id', '=', 'sponsor.id')
            ->join('char_currencies', function($q) {
                $q->on('char_payments.currency_id','=','char_currencies.id');
            })
            ->leftjoin('char_persons_contact', function($join)   {
                $join->on('char_payments_recipient.person_id', '=','char_persons_contact.person_id')
                    ->where('char_persons_contact.contact_type','primary_mobile');
            })
            ->leftJoin('char_locations_i18n As location_name', function($join)   {
                $join->on('char_persons.location_id', '=','location_name.location_id' )
                    ->where('location_name.language_id',1);
            })
            ->leftjoin('char_locations_i18n As city_name', function($join)   {
                $join->on('char_persons.city', '=','city_name.location_id' )
                    ->where('city_name.language_id',1);
            })
            ->leftjoin('char_locations_i18n As district_name', function($join)   {
                $join->on('char_persons.governarate', '=','district_name.location_id' )
                    ->where('district_name.language_id',1);
            })
            ->leftjoin('char_locations_i18n As country_name', function($join)   {
                $join->on('char_persons.country', '=','country_name.location_id' )
                    ->where('country_name.language_id',1);
            })
            ->whereIn('payment_id',$ids);


        if(!isset($options['person_id'])) {

            if(isset($options['district_id'])) {
                if(!is_null($options['district_id']) && $options['district_id'] != "" ){
                    $query=$query->where('char_persons.governarate',$options['district_id']);
                }
            }

            $query = $query->orderBy('char_persons.country')
                ->orderBy('char_persons.governarate')
                ->orderBy('char_persons.city')
                ->orderBy('char_persons.location_id')
                ->orderBy('char_persons.first_name')
                ->orderBy('char_persons.second_name')
                ->orderBy('char_persons.third_name')
                ->orderBy('char_persons.last_name');
        }
        else{
            if(!is_null($options['person_id']) && $options['person_id'] != "" ){
                $query=$query->where('char_payments_recipient.person_id',$options['person_id']);
            }
        }

        $query=$query->groupBy('char_payments_recipient.person_id')
            ->selectRaw("char_payments_recipient.* , 
                          ifnull(sponsor.name, ' - ') as sponsor_name,
                          ifnull(char_persons.id_card_number, ' - ') as id_card_number,
                          ifnull(char_persons.old_id_card_number, ' - ') as old_id_card_number,
                          ifnull(char_persons_contact.contact_value, ' - ') as mobile,
                          CASE  WHEN char_payments.payment_exchange = 0 THEN char_currencies.name
                                WHEN char_payments.payment_exchange = 1 THEN '$shekel'
                          END AS currency,
                          CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                          ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name,
                          country_name.name as country,
                          district_name.name as district,
                          city_name.name as city,
                          location_name.name as location,
                          char_persons.street_address,
                          CONCAT(ifnull(country_name.name, ' '), ' - ' ,ifnull(district_name.name, ' '),' - ',
                                 ifnull(city_name.name, ' '),' - ', ifnull(location_name.name,' '),' - ', ifnull(char_persons.street_address,' '))  AS address
                 ");


        if(!isset($options['person_id'])){
            if(isset($options['from']) && isset($options['to'])){
                $offset=$options['from'];
                $limit=($options['to']-$options['from'])+1;
                $query->offset($offset)->take($limit);
            }
        }

        $query=$query->get();
        if(sizeof($query) >0 ){

            foreach ($query as $key => $value){
                $amount=0;
                $total_sh_mot=0;
                $id=$value->person_id;
                $query[$key]->individual = self::individuals($options,$id);
               $value->amount =0;
                $value->amount_after_discount =0;
                $value->shekel_amount =0;
                $value->shekel_amount_before_discount =0;

                $total= \DB::select("select SUM(amount) as amount   FROM char_payments_cases
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where (payment_id IN($pIds)) and 
                                        ((char_cases.person_id = $id and recipient = 0)or( guardian_id = $id and recipient = 1 ))  ");

                if($total){ $value->amount=$total[0]->amount; }

                $amount_after_discount= \DB::select("select SUM(amount_after_discount) as amount   FROM char_payments_cases
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                 where (payment_id IN($pIds)) and 
                                        ((char_cases.person_id = $id and recipient = 0)or( guardian_id = $id and recipient = 1 ))  ");

                if($amount_after_discount){$value->amount_after_discount =$amount_after_discount[0]->amount;}

                $total_shekel_amount= \DB::select("select SUM(shekel_amount) as amount  FROM char_payments_cases
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                 where (payment_id IN($pIds)) and 
                                        ((char_cases.person_id = $id and recipient = 0)or( guardian_id = $id and recipient = 1 ))   ");

                if($total_shekel_amount){$value->shekel_amount=$total_shekel_amount[0]->amount;}

                $shekel_amount_before_discount= \DB::select("select SUM(shekel_amount_before_discount) as amount   FROM char_payments_cases
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                 where (payment_id IN($pIds)) and 
                                        ((char_cases.person_id = $id and recipient = 0)or( guardian_id = $id and recipient = 1 ))  ");

                if($shekel_amount_before_discount){
                    $value->shekel_amount_before_discount_amount =$shekel_amount_before_discount[0]->amount;
                }

                $query[$key]->payment_value =$amount;
                $query[$key]->payment_value_shekel =$total_sh_mot;
            }
        }

        return $query;
    }
    public static function exportRecipients($options){

        $ids=$options['payment_ids'];
        $pIds = join(",",$ids);
        $payment_exchange=$options['payment_exchange'];
        $shekel=trans('sponsorship::application.shekel');

        $language_id =  \App\Http\Helpers::getLocale();
        $query=\DB::table('char_payments_recipient')
            ->join('char_persons','char_persons.id',  '=', 'char_payments_recipient.person_id')
            ->join('char_payments', 'char_payments.id', '=', 'char_payments_recipient.payment_id')
            ->join('char_organizations as sponsor', 'char_payments.sponsor_id', '=', 'sponsor.id')
            ->join('char_currencies', function($q) {
                $q->on('char_payments.currency_id','=','char_currencies.id');
            })
            ->leftjoin('char_persons_contact', function($join)   {
                $join->on('char_payments_recipient.person_id', '=','char_persons_contact.person_id')
                    ->where('char_persons_contact.contact_type','primary_mobile');
            })
            ->leftjoin('char_locations_i18n As mosques_name', function($join) use ($language_id)  {
                $join->on('char_persons.mosques_id', '=','mosques_name.location_id' )
                    ->where('mosques_name.language_id', $language_id);
            })
            ->leftJoin('char_locations_i18n As person_location', function($join)   {
                $join->on('char_persons.location_id', '=','person_location.location_id' )
                    ->where('person_location.language_id',1);
            })
            ->leftjoin('char_locations_i18n As person_city', function($join)   {
                $join->on('char_persons.city', '=','person_city.location_id' )
                    ->where('person_city.language_id',1);
            })
            ->leftjoin('char_locations_i18n As person_district', function($join)   {
                $join->on('char_persons.governarate', '=','person_district.location_id' )
                    ->where('person_district.language_id',1);
            })
            ->leftjoin('char_locations_i18n As person_country', function($join)   {
                $join->on('char_persons.country', '=','person_country.location_id' )
                    ->where('person_country.language_id',1);
            })
            ->whereIn('payment_id',$ids);


        if(isset($options['district_id'])) {
            if(!is_null($options['district_id']) && $options['district_id'] != "" ){
                $query=$query->where('char_persons.governarate',$options['district_id']);
            }
        }

        $query = $query->orderBy('char_persons.country')
            ->orderBy('char_persons.governarate')
            ->orderBy('char_persons.city')
            ->orderBy('char_persons.location_id')
            ->orderBy('char_persons.first_name')
            ->orderBy('char_persons.second_name')
            ->orderBy('char_persons.third_name')
            ->orderBy('char_persons.last_name');

        $query=$query->groupBy('char_payments_recipient.person_id')
            ->selectRaw("
                         CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                               ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name,
                         CASE WHEN char_persons.id_card_number is null THEN '-' Else char_persons.id_card_number  END   AS id_card_number,
                         CASE WHEN char_persons.old_id_card_number is null THEN '-' Else char_persons.old_id_card_number  END   AS old_id_card_number,
                         CASE WHEN char_persons_contact.contact_value is null THEN '-' Else char_persons_contact.contact_value  END   AS mobile,
                         CASE WHEN char_persons.country is null THEN '-' Else person_country.name  END   AS country,
                         CASE WHEN char_persons.governarate is null THEN '-' Else person_district.name  END   AS district,
                         CASE WHEN char_persons.city is null THEN '-' Else person_city.name  END   AS city,
                         CASE WHEN char_persons.location_id is null THEN '-' Else person_location.name  END   AS location,
                         CASE WHEN char_persons.mosques_id is null THEN '-' Else mosques_name.name  END   AS mosques,
                         CASE WHEN char_persons.street_address is null THEN '-' Else char_persons.street_address  END   AS street_address,
                         CASE  WHEN '$payment_exchange' = 0 THEN char_currencies.name
                               WHEN '$payment_exchange' = 1 THEN '$shekel'
                         END AS currency,char_payments_recipient.person_id,
                         CASE WHEN char_payments_recipient.cheque_account_number is null THEN '-' 
                              Else char_payments_recipient.cheque_account_number  END  AS cheque_account_number,
                              CASE WHEN char_payments_recipient.cheque_date is null THEN '-' Else char_payments_recipient.cheque_date  END  AS cheque_date
                        
                 ");

        if(isset($options['from']) && isset($options['to'])){
            $offset=$options['from'];
            $limit=($options['to']-$options['from'])+1;
            $query->offset($offset)->take($limit);
        }

        $query=$query->get();
        if(sizeof($query) >0 ){

            foreach ($query as $key => $value){
                $id=$value->person_id;

                $value->amount =0;
//                $value->amount_after_discount =0;
//                $value->shekel_amount =0;
//                $value->shekel_amount_before_discount =0;

//                $total= \DB::select("select SUM(amount) as amount   FROM char_payments_cases
//                                 join char_cases on char_cases .id = char_payments_cases.case_id
//                                  where (payment_id IN($pIds)) and
//                                        ((char_cases.person_id = $id and recipient = 0)or( guardian_id = $id and recipient = 1 ))  ");
//
//                if($total){ $value->amount=$total[0]->amount; }
//
//                $amount_after_discount= \DB::select("select SUM(amount_after_discount) as amount   FROM char_payments_cases
//                                 join char_cases on char_cases .id = char_payments_cases.case_id
//                                 where (payment_id IN($pIds)) and
//                                        ((char_cases.person_id = $id and recipient = 0)or( guardian_id = $id and recipient = 1 ))  ");
//
//                if($amount_after_discount){$value->amount_after_discount =$amount_after_discount[0]->amount;}

                $total_shekel_amount= \DB::select("select SUM(shekel_amount) as amount  FROM char_payments_cases
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                 where (payment_id IN($pIds)) and 
                                        ((char_cases.person_id = $id and recipient = 0)or( guardian_id = $id and recipient = 1 ))   ");

                if($total_shekel_amount){$value->amount=$total_shekel_amount[0]->amount;}

//                $shekel_amount_before_discount= \DB::select("select SUM(shekel_amount_before_discount) as amount   FROM char_payments_cases
//                                 join char_cases on char_cases .id = char_payments_cases.case_id
//                                 where (payment_id IN($pIds)) and
//                                        ((char_cases.person_id = $id and recipient = 0)or( guardian_id = $id and recipient = 1 ))  ");
//
//                if($shekel_amount_before_discount){
//                    $value->shekel_amount_before_discount =$shekel_amount_before_discount[0]->amount;
//                }
            }
        }

        return $query;
    }
    public static function updateChequeNumber($id,$offset,$cheque_number)
    {

        $response=['status' => 'success'];
        $updated =[];
        $update=true;
        if($cheque_number!= null || $cheque_number != ""){

            $count = \DB::table('char_payments_recipient')->where('char_payments_recipient.payment_id','=',$id)->count();
            $limit = $count - $offset;

            $items=\DB::table('char_payments_recipient')
                ->join('char_persons', 'char_payments_recipient.person_id', '=', 'char_persons.id')
                ->where('char_payments_recipient.payment_id','=',$id)
                ->orderBy('char_persons.country')
                ->orderBy('char_persons.governarate')
                ->orderBy('char_persons.city')
                ->orderBy('char_persons.location_id')
                ->orderBy('char_persons.first_name')
                ->orderBy('char_persons.second_name')
                ->orderBy('char_persons.third_name')
                ->orderBy('char_persons.last_name')
                ->selectRaw("char_payments_recipient.id")
                ->limit($limit)->offset($offset)->get();

            foreach($items as $k => $v){
                if(!is_null($cheque_number)){
                    $updated["cheque_account_number"]= $cheque_number;
                }
                self::where(["id"=>$v->id])->update($updated);

                if(!is_null($cheque_number)){
                    $cheque_number++;
                }
            }

            \Log\Model\Log::saveNewLog('PAYMENTS_RECIPIENT_UPDATED', trans('sponsorship::application.update on payment recipients data')  . ' : ' . $id);

        }

        if($update) {
            $response["status"]= 'success';
            $response["msg"]= trans('sponsorship::application.The row is updated');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sponsorship::application.There is no update');

        }
        return $response;

    }

    // ********************************************************* //

    public static function result($filter){


        $ids=$filter['payment_ids'];
        $pIds = "".join(",",$ids)."";
        $shekel=trans('sponsorship::application.shekel');

        $query=\DB::table('char_payments_recipient')
            ->join('char_persons','char_persons.id',  '=', 'char_payments_recipient.person_id')
            ->join('char_payments', 'char_payments.id', '=', 'char_payments_recipient.payment_id')
            ->join('char_organizations as sponsor', 'char_payments.sponsor_id', '=', 'sponsor.id')
            ->join('char_currencies','char_payments.currency_id','=','char_currencies.id')
            ->leftjoin('char_persons_contact', function($join)   {
                $join->on('char_payments_recipient.person_id', '=','char_persons_contact.person_id')
                    ->where('char_persons_contact.contact_type','primary_mobile');
            })
            ->leftJoin('char_locations_i18n As location_name', function($join)   {
                $join->on('char_persons.location_id', '=','location_name.location_id' )
                    ->where('location_name.language_id',1);
            })
            ->leftjoin('char_locations_i18n As city_name', function($join)   {
                $join->on('char_persons.city', '=','city_name.location_id' )
                    ->where('city_name.language_id',1);
            })
            ->leftjoin('char_locations_i18n As district_name', function($join)   {
                $join->on('char_persons.governarate', '=','district_name.location_id' )
                    ->where('district_name.language_id',1);
            })
            ->leftjoin('char_locations_i18n As country_name', function($join)   {
                $join->on('char_persons.country', '=','country_name.location_id' )
                    ->where('country_name.language_id',1);
            })
            ->whereIn('payment_id',$ids)
            ->orderBy('char_persons.country')
            ->orderBy('char_persons.governarate')
            ->orderBy('char_persons.city')
            ->orderBy('char_persons.location_id')
            ->orderBy('char_persons.first_name')
            ->orderBy('char_persons.second_name')
            ->orderBy('char_persons.third_name')
            ->orderBy('char_persons.last_name')
            ->groupBy('char_payments_recipient.person_id')
            ->selectRaw("char_payments_recipient.* ,
                         get_total_recipient_amount (char_payments_recipient.person_id,'$pIds',0) as total_amount,
                         get_total_recipient_amount (char_payments_recipient.person_id,'$pIds',1) as total_shekel_amount,
                        ifnull(sponsor.name, ' - ') as sponsor_name,
                        ifnull(char_persons.id_card_number,' - ') as id_card_number,
                        ifnull(char_persons.old_id_card_number, ' - ') as old_id_card_number,
                        ifnull(char_persons_contact.contact_value, ' - ') as mobile,
                          CASE  WHEN char_payments.payment_exchange = 0 THEN char_currencies.name
                                WHEN char_payments.payment_exchange = 1 THEN '$shekel'
                          END AS currency,
                          CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                          ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name,
                          country_name.name as country,
                          district_name.name as district,
                          city_name.name as city,
                          location_name.name as location,
                          char_persons.street_address,
                          CONCAT(ifnull(country_name.name, ' '), ' - ' ,ifnull(district_name.name, ' '),' - ',
                                 ifnull(city_name.name, ' '),' - ', ifnull(location_name.name,' '),' - ', ifnull(char_persons.street_address,' '))  AS address
                 ");
            $itemsCount = isset($filter['itemsCount'])? $filter['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
            return $query->paginate($records_per_page);

//        if(sizeof($query) >0 ){
//
//            foreach ($query as $key => $value){
//                $amount=0;
//                $id=$value->person_id;
//                $query[$key]->individual = self::individuals($filter,$id);
//
//                if($payment_exchange ==0 ){
//                    $total= \DB::select("select SUM(amount) as amount   FROM char_payments_recipient where payment_id IN($pIds) and person_id = $id  ");
//                }else{
//                    $total= \DB::select("select SUM(shekel_amount) as amount   FROM char_payments_recipient where payment_id IN($pIds) and person_id = $id  ");
//                }
//                if($total){
//                    $amount=$total[0]->amount;
//                }
//
//                $query[$key]->payment_value =$amount;
//            }
//        }
//


        /*
         *
         *  CASE  WHEN char_payments.payment_exchange = 0 THEN get_total_recipient_amount(char_persons.id,'$pIds',0)
                                  WHEN char_payments.payment_exchange = 1 THEN get_total_recipient_amount(char_persons.id,'$pIds',1)
                            END AS payment_exchange_amount,

        $query = PaymentsRecipient::with(['person',
                                          'person.country',
                                          'person.city',
                                          'person.district',
                                          'person.location',
                                          'person.contacts'=> function ($q) use ($ids) {
                                                        $q->where('contact_type', 'primary_mobile');
                                          },'individual'=> function ($q) use ($ids) {
                                                        $q->whereIn('payment_id', $ids);
            },
                                           'individual.caseData.person' =>function ($q){
                                               $q->orderBy('char_persons.first_name','asc');
                                               $q->orderBy('char_persons.second_name','asc');
                                               $q->orderBy('char_persons.third_name','asc');
                                               $q->orderBy('char_persons.last_name','asc');
                                           },
                                           'individual.payment.sponsor',
                                           'individual.payment.category'])
                                        ->whereIn('payment_id',$request->payment_ids)
                                        ->get();
*/
//                return \DB::select("select get_total_recipient_amount ($id,$pIds)",[$id,$pIds]);

//
//        $items=\DB::table('char_payments_recipient_view')->whereIn('payment_id',$request->payment_ids)->get();
//        foreach ($items as $key => $value) {
//            $temp=$value;
//            $person=$value->person_id;
//            return \DB::table('char_payments_cases_view')->whereRaw("recipient == 0 and guardian_id == '$person'")->get();
//
//
//        }

    }
    public static function PaymentOfSetExport($options){

        $payment_id=$options['payment_ids'][0];
        $payment_exchange=$options['payment_exchange'];
        $shekel=trans('sponsorship::application.shekel');

        $query=\DB::table('char_payments_recipient')
            ->join('char_persons','char_persons.id',  '=', 'char_payments_recipient.person_id')
            ->join('char_payments', 'char_payments.id', '=', 'char_payments_recipient.payment_id')
            ->join('char_organizations as sponsor', 'char_payments.sponsor_id', '=', 'sponsor.id')
            ->join('char_currencies', function($q) {
                $q->on('char_payments.currency_id','=','char_currencies.id');
            })
            ->leftjoin('char_persons_contact', function($join)   {
                $join->on('char_payments_recipient.person_id', '=','char_persons_contact.person_id')
                    ->where('char_persons_contact.contact_type','primary_mobile');
            })
            ->leftJoin('char_locations_i18n As location_name', function($join)   {
                $join->on('char_persons.location_id', '=','location_name.location_id' )
                    ->where('location_name.language_id',1);
            })
            ->leftjoin('char_locations_i18n As city_name', function($join)   {
                $join->on('char_persons.city', '=','city_name.location_id' )
                    ->where('city_name.language_id',1);
            })
            ->leftjoin('char_locations_i18n As district_name', function($join)   {
                $join->on('char_persons.governarate', '=','district_name.location_id' )
                    ->where('district_name.language_id',1);
            })
            ->leftjoin('char_locations_i18n As country_name', function($join)   {
                $join->on('char_persons.country', '=','country_name.location_id' )
                    ->where('country_name.language_id',1);
            })
            ->where('payment_id',$payment_id);


        if(!isset($options['person_id'])) {

            if(isset($options['district_id'])) {
                if(!is_null($options['district_id']) && $options['district_id'] != "" ){
                    $query=$query->where('char_persons.governarate',$options['district_id']);
                }
            }

            $query = $query->orderBy('char_persons.country')
                ->orderBy('char_persons.governarate')
                ->orderBy('char_persons.city')
                ->orderBy('char_persons.location_id')
                ->orderBy('char_persons.first_name')
                ->orderBy('char_persons.second_name')
                ->orderBy('char_persons.third_name')
                ->orderBy('char_persons.last_name');
        }
        else{
            if(!is_null($options['person_id']) && $options['person_id'] != "" ){
                $query=$query->where('char_payments_recipient.person_id',$options['person_id']);
            }
        }

        $query=$query->groupBy('char_payments_recipient.person_id')
            ->selectRaw("char_payments_recipient.* , 
                          ifnull(sponsor.name, ' - ') as sponsor_name,
                          ifnull(char_persons.id_card_number,' - ') as id_card_number,
                          ifnull(char_persons.old_id_card_number, ' - ') as old_id_card_number,
                          ifnull(char_persons_contact.contact_value, ' - ') as mobile,
                          CASE  WHEN char_payments.payment_exchange = 0 THEN char_currencies.name
                                WHEN char_payments.payment_exchange = 1 THEN '$shekel'
                          END AS currency,
                          CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                          ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name,
                          country_name.name as country,
                          district_name.name as district,
                          city_name.name as city,
                          location_name.name as location,
                          char_persons.street_address,
                          CONCAT(ifnull(country_name.name, ' '), ' - ' ,ifnull(district_name.name, ' '),' - ',
                                 ifnull(city_name.name, ' '),' - ', ifnull(location_name.name,' '),' - ', ifnull(char_persons.street_address,' '))  AS address
                 ");


        if(!isset($options['person_id'])){
            if(isset($options['from']) && isset($options['to'])){
                $offset=$options['from'];
                $limit=($options['to']-$options['from'])+1;
                $query->offset($offset)->take($limit);
            }
        }

        $query=$query->get();

        if(sizeof($query) >0 ){

            foreach ($query as $key => $value){

                $id=$value->person_id;
                $query[$key]->individual = self::individuals($options,$id);

                $value->amount =0;
                $value->amount_after_discount =0;
                $value->shekel_amount =0;
                $value->shekel_amount_before_discount =0;

                $total= \DB::select("select SUM(amount) as amount   FROM char_payments_cases
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where (payment_id= $payment_id) and 
                                        ((char_cases.person_id = $id and recipient = 0) or( guardian_id = $id and recipient = 1 )) ");

                if($total){ $value->amount=$total[0]->amount; }

                $amount_after_discount= \DB::select("select SUM(amount_after_discount) as amount   FROM char_payments_cases
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where (payment_id= $payment_id) and 
                                        ((char_cases.person_id = $id and recipient = 0) or( guardian_id = $id and recipient = 1 )) ");

                if($amount_after_discount){$value->amount_after_discount =$amount_after_discount[0]->amount;}

                $total_shekel_amount= \DB::select("select SUM(shekel_amount) as amount  FROM char_payments_cases
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                            where (payment_id= $payment_id) and 
                                        ((char_cases.person_id = $id and recipient = 0) or( guardian_id = $id and recipient = 1 )) ");

                if($total_shekel_amount){$value->shekel_amount=$total_shekel_amount[0]->amount;}

                $shekel_amount_before_discount= \DB::select("select SUM(shekel_amount_before_discount) as amount   FROM char_payments_cases
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                                  where (payment_id= $payment_id) and 
                                        ((char_cases.person_id = $id and recipient = 0) or( guardian_id = $id and recipient = 1 )) ");

                if($shekel_amount_before_discount){
                    $value->shekel_amount_before_discount_amount =$shekel_amount_before_discount[0]->amount;
                }
            }
        }

        return $query;
    }
    public static function districtRange($filter){

        $ids=$filter['payment_ids'];
        $district = $filter['district_id'];
        $query=\DB::table('char_payments_recipient')
            ->join('char_persons', function($join) use($district)   {
                $join->on('char_persons.id', '=','char_payments_recipient.person_id' )
                    ->where('char_persons.governarate',$district);
            })
            ->whereIn('payment_id',$ids)
            ->groupBy('char_payments_recipient.person_id')
//                ->selectRaw("char_persons.id")
            ->get();

        return $query;



    }

    // ********************************************************* //
    public static function setRecipient($payment_id){

        $payment=\DB::table('char_payments')->where('char_payments.id',$payment_id)->first();

        $person=self::getRows($payment_id,0,$payment->bank_id,$payment->exchange_type);
        $guardians=self::getRows($payment_id,1,$payment->bank_id,$payment->exchange_type);

        foreach($person as $k=>$v){

            $v->amount =0;
            $v->amount_after_discount =0;
            $v->shekel_amount =0;
            $v->shekel_amount_before_discount =0;
            $id = $v->person_id;

            $total= \DB::select("select SUM(amount) as amount   FROM char_payments_cases
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where payment_id= $payment_id and 
                                        ((char_cases.person_id = $id and recipient = 0) or( guardian_id = $id and recipient = 1 ))  ");

            if($total){ $v->amount=$total[0]->amount; }

            $amount_after_discount= \DB::select("select SUM(amount_after_discount) as amount   FROM char_payments_cases
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where payment_id= $payment_id and 
                                        ((char_cases.person_id = $id and recipient = 0) or( guardian_id = $id and recipient = 1 ))  ");

            if($amount_after_discount){$v->amount_after_discount =$amount_after_discount[0]->amount;}

            $total_shekel_amount= \DB::select("select SUM(shekel_amount) as amount  FROM char_payments_cases
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where payment_id = $payment_id and 
                                        ((char_cases.person_id = $id and recipient = 0) or( guardian_id = $id and recipient = 1 ))   ");

            if($total_shekel_amount){$v->shekel_amount=$total_shekel_amount[0]->amount;}

            $shekel_amount_before_discount= \DB::select("select SUM(shekel_amount_before_discount) as amount   FROM char_payments_cases
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where payment_id= $payment_id and 
                                        ((char_cases.person_id = $id and recipient = 0) or( guardian_id = $id and recipient = 1 ))  ");

            if($shekel_amount_before_discount){
                $v->shekel_amount_before_discount_amount =$shekel_amount_before_discount[0]->amount;
            }

            if (PaymentsRecipient::where(['payment_id'=>$payment_id,'person_id'=>$v->person_id,'father_id'=>$v->father_id,'mother_id'=>$v->mother_id])->exists()) {
                PaymentsRecipient::where(['payment_id'=>$payment_id,'person_id'=>$v->person_id,'father_id'=>$v->father_id,'mother_id'=>$v->mother_id])
                    ->update([
                        'amount'=>$v->amount,
                        'amount_after_discount'=> ($v->amount - ($v->amount * $payment->organization_share)),
                        'shekel_amount_before_discount'=>($v->amount * $payment->exchange_rate),
                        'shekel_amount'=>round( ( $v->amount - ($v->amount * $payment->organization_share)) * $payment->exchange_rate)
                    ]);

            }else{

                $v->amount_after_discount =($v->amount - ($v->amount * $payment->organization_share));
                $v->shekel_amount_before_discount  = ($v->amount * $payment->exchange_rate);
                $v->shekel_amount  =  round( ( $v->amount - ($v->amount * $payment->organization_share)) * $payment->exchange_rate);

                if($v->father_id ==null){
                    unset($v->father_id);
                }

                if($v->mother_id ==null){
                    unset($v->mother_id);
                }

                if($payment->exchange_type ==3){
                    unset($v->bank_id);
                    unset($v->cheque_account_number);
                }

                $item=(array)$v;
                $item['status']=0;
                $item['payment_id']=$payment_id;
                PaymentsRecipient::create($item);
            }
        }
        foreach($guardians as $k=>$v){

            $v->amount =0;
            $v->amount_after_discount =0;
            $v->shekel_amount =0;
            $v->shekel_amount_before_discount =0;
            $id = $v->person_id;

            $total= \DB::select("select SUM(amount) as amount   FROM char_payments_cases
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where payment_id= $payment_id and 
                                        ((char_cases.person_id = $id and recipient = 0) or( guardian_id = $id and recipient = 1 ))  ");

            if($total){ $v->amount=$total[0]->amount; }

            $amount_after_discount= \DB::select("select SUM(amount_after_discount) as amount   FROM char_payments_cases
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where payment_id= $payment_id and 
                                        ((char_cases.person_id = $id and recipient = 0) or( guardian_id = $id and recipient = 1 ))  ");

            if($amount_after_discount){$v->amount_after_discount =$amount_after_discount[0]->amount;}

            $total_shekel_amount= \DB::select("select SUM(shekel_amount) as amount  FROM char_payments_cases
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where payment_id = $payment_id and 
                                        ((char_cases.person_id = $id and recipient = 0) or( guardian_id = $id and recipient = 1 ))   ");

            if($total_shekel_amount){$v->shekel_amount=$total_shekel_amount[0]->amount;}

            $shekel_amount_before_discount= \DB::select("select SUM(shekel_amount_before_discount) as amount   FROM char_payments_cases
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where payment_id= $payment_id and 
                                        ((char_cases.person_id = $id and recipient = 0) or( guardian_id = $id and recipient = 1 ))  ");

            if($shekel_amount_before_discount){
                $v->shekel_amount_before_discount_amount =$shekel_amount_before_discount[0]->amount;
            }

            if (PaymentsRecipient::where(['payment_id'=>$payment_id,'person_id'=>$v->person_id,'father_id'=>$v->father_id,'mother_id'=>$v->mother_id])->exists()) {
                PaymentsRecipient::where(['payment_id'=>$payment_id,'person_id'=>$v->person_id,'father_id'=>$v->father_id,'mother_id'=>$v->mother_id])
                    ->update([
                        'amount'=>$v->amount,
                        'amount_after_discount'=> ($v->amount - ($v->amount * $payment->organization_share)),
                        'shekel_amount_before_discount'=>($v->amount * $payment->exchange_rate),
                        'shekel_amount'=>round( ( $v->amount - ($v->amount * $payment->organization_share)) * $payment->exchange_rate)
                    ]);
            }else{

                $v->amount_after_discount =($v->amount - ($v->amount * $payment->organization_share));
                $v->shekel_amount_before_discount  = ($v->amount * $payment->exchange_rate);
                $v->shekel_amount  =  round( ( $v->amount - ($v->amount * $payment->organization_share)) * $payment->exchange_rate);

                if($v->father_id ==null){
                    unset($v->father_id);
                }

                if($v->mother_id ==null){
                    unset($v->mother_id);
                }

                if($payment->exchange_type ==3){
                    unset($v->bank_id);
                    unset($v->cheque_account_number);
                }


                $item=(array)$v;
                $item['status']=0;
                $item['payment_id']=$payment_id;
                PaymentsRecipient::create($item);
            }
        }
    }
    public static function getRows($payment_id,$recipient,$payment_bank,$exchange_type)
    {
            $query=\DB::table('char_payments_cases')
                      ->join('char_cases','char_cases.id',  '=', 'char_payments_cases.case_id')
                      ->join('char_persons as c','c.id',  '=', 'char_cases.person_id')
                      ->leftjoin('char_guardians', function($q) {
                        $q->on('char_guardians.individual_id','=','char_cases.person_id')
                          ->on('char_guardians.organization_id','=','char_cases.organization_id')
                          ->where('char_guardians.status','=',1);
                     })
                     ->where('char_payments_cases.status','!=',3)
                    ->where('char_payments_cases.recipient',$recipient)
                    ->where('char_payments_cases.payment_id','=',$payment_id)
                    ->groupBy('c.father_id','c.mother_id');

            if($exchange_type == 2 || $exchange_type == 1) {
                $query=$query->selectRaw(" '$payment_bank' as bank_id") ;
            }

            if($payment_bank !=null && $exchange_type == 1){
                if($recipient==0){
                    $query=$query->leftjoin('char_persons_banks', function($q) use($payment_bank){
                        $q->on('char_cases.person_id','=','char_persons_banks.person_id');
                        $q->where('char_persons_banks.bank_id','=',$payment_bank);
                    })
                        ->selectRaw("char_persons_banks.account_number as cheque_account_number") ;
                }else{
                    $query=$query
                        ->leftjoin('char_persons_banks as guardian_banks', function($q) use($payment_bank){
                            $q->on('char_guardians.guardian_id','=','guardian_banks.person_id');
                            $q->where('guardian_banks.bank_id','=',$payment_bank);
                        })
                        ->selectRaw("guardian_banks.account_number as cheque_account_number") ;
                }
            }

            if($recipient ==0 ){
                $query=$query->leftjoin('char_payments_recipient', function($q) {
                    $q->on('char_payments_recipient.payment_id','=','char_payments_cases.payment_id');
                    $q->on('c.father_id','=','char_payments_recipient.father_id');
                    $q->on('c.mother_id','=','char_payments_recipient.mother_id');
                    $q->on('char_cases.person_id','=','char_payments_recipient.person_id');
                })
               ->groupBy('c.father_id','c.mother_id','char_cases.person_id');
            }else{
                $query=$query->leftjoin('char_payments_recipient', function($q) {
                    $q->on('char_payments_recipient.payment_id','=','char_payments_cases.payment_id');
                    $q->on('c.father_id','=','char_payments_recipient.father_id');
                    $q->on('c.mother_id','=','char_payments_recipient.mother_id');
                    $q->on('char_guardians.guardian_id','=','char_payments_recipient.person_id');
                })
                ->groupBy('c.father_id','c.mother_id','char_guardians.guardian_id');
            }

            return $query->selectRaw("c.father_id,
                                      char_payments_recipient.person_id as recipient_id,
                                      c.mother_id,
                                      CASE WHEN char_payments_cases.recipient is null THEN char_cases.person_id
                                           WHEN char_payments_cases.recipient =0 THEN char_cases.person_id
                                      Else char_guardians.guardian_id  END  AS person_id,
                                      CASE WHEN SUM(char_payments_cases.amount) is null THEN 0 Else SUM(char_payments_cases.amount) END  AS amount,
                                      CASE WHEN SUM(char_payments_cases.shekel_amount) is null THEN 0 Else SUM(char_payments_cases.shekel_amount) END  AS shekel_amount")
             ->get();
/*
        if($recipient ==0 ){

            return \DB::select("SELECT char_cases.person_id, char_persons.mother_id, char_persons.father_id,char_persons_banks.account_number, COALESCE(SUM(p.amount),0)
                                FROM   char_payments_cases AS p
                                JOIN   char_cases ON char_cases.id = p.case_id
                                JOIN   char_persons ON char_persons.id = char_cases.person_id
                                LEFT JOIN   char_persons_banks ON ((char_persons_banks.person_id = char_cases.person_id)and(char_persons_banks.bank_id = '$payment_bank') )
                                WHERE  (p.recipient = 0) AND (p.payment_id = '$payment_id') AND (p.status != 3)
                                GROUP BY char_cases.person_id, char_persons.mother_id, char_persons.father_id");

        }else{
           return \DB::select("SELECT char_guardians.guardian_id as person_id, char_persons.mother_id, char_persons.father_id,char_persons_banks.account_number, COALESCE(SUM(p.amount),0)
                               FROM   char_payments_cases AS p
                               JOIN   char_cases ON char_cases.id = p.case_id
                               JOIN   char_persons ON char_persons.id = char_cases.person_id
                               JOIN   char_guardians ON char_persons.id = char_guardians.individual_id
                               LEFT JOIN   char_persons_banks ON ((char_persons_banks.person_id = char_guardians.guardian_id)and(char_persons_banks.bank_id = 1) )
                               WHERE  (p.recipient = 1) AND (p.payment_id = '$payment_id') AND (p.status != 3)
                               GROUP BY char_guardians.guardian_id, char_persons.mother_id, char_persons.father_id");

        }
*/

    }
    public static function updateRow($payment_id,$case_id,$sponsor_number)
    {

        $query=\DB::table('char_payments_cases')
            ->join('char_cases','char_cases.id',  '=', 'char_payments_cases.case_id')
            ->join('char_persons as c','c.id',  '=', 'char_cases.person_id')
            ->where('char_payments_cases.status','!=',3)
            ->where('char_payments_cases.case_id','=',$case_id)
            ->where('char_payments_cases.sponsor_number','=',$sponsor_number)
            ->where('char_payments_cases.payment_id','=',$payment_id)
            ->selectRaw("c.father_id,
                         c.mother_id,
                         char_payments_cases.payment_id,
                         char_cases.person_id,
                         CASE WHEN char_payments_cases.recipient is null THEN
                                   get_payments_recipient_amount(char_cases.person_id,'person',char_payments_cases.payment_id,c.father_id,c.mother_id)
                              WHEN char_payments_cases.recipient =0 THEN
                                   get_payments_recipient_amount(char_cases.person_id,'person',char_payments_cases.payment_id,c.father_id,c.mother_id)
                               Else
                                    get_payments_recipient_amount(char_payments_cases.guardian_id,'person',char_payments_cases.payment_id,c.father_id,c.mother_id)
                         END  AS amount,
                         CASE WHEN char_payments_cases.recipient is null THEN
                                   get_payments_recipient_amount(char_cases.person_id,'guardian',char_payments_cases.payment_id,c.father_id,c.mother_id)
                              WHEN char_payments_cases.recipient =0 THEN
                                   get_payments_recipient_amount(char_cases.person_id,'guardian',char_payments_cases.payment_id,c.father_id,c.mother_id)
                              Else
                                   get_payments_recipient_amount(char_payments_cases.guardian_id,'guardian',char_payments_cases.payment_id,c.father_id,c.mother_id)
                         END  AS guardian_amount,
                         CASE WHEN char_payments_cases.recipient is null THEN char_cases.person_id
                              WHEN char_payments_cases.recipient =0 THEN char_cases.person_id
                              Else char_payments_cases.guardian_id  END  AS recipient_id
                         ")
            ->first();

        if($query){
            $recipient_id=$query->recipient_id;

            $payments_recipient=\DB::table('char_payments_recipient')
                        ->where('char_payments_recipient.person_id','=',$query->person_id)
                        ->where('char_payments_recipient.mother_id','=',$query->mother_id)
                        ->where('char_payments_recipient.father_id','=',$query->father_id)
                        ->where('char_payments_recipient.payment_id','=',$payment_id)
                        ->first();
            $amount=0;

            if($query->amount){
                $amount=$amount+$query->amount;
            }

            if($query->guardian_amount){
                $amount=$amount+$query->guardian_amount;
            }

            if($payments_recipient) {
                return
                    ['cheque_account_number'=>$payments_recipient->cheque_account_number,'bank_id'=>$payments_recipient->bank_id,
                     'cheque_date'=>$payments_recipient->cheque_date,'status'=>$payments_recipient->status,
                      'person_id'=>$query->recipient_id,'mother_id'=>$query->mother_id, 'father_id'=>$query->father_id,'payment_id'=>$payment_id,'amount'=>$amount];
            }
            else{

                $payment=\DB::table('char_payments')->where('char_payments.id',$payment_id)->selectRaw("char_payments.*")->first();
                $payment_bank=$payment->bank_id;
                $exchange_type=$payment->exchange_type;
                $recipient=['person_id'=>$query->recipient_id,
                            'mother_id'=>$query->mother_id,
                            'father_id'=>$query->father_id,
                            'payment_id'=>$payment_id,
                            'amount'=>$amount,
                            'status'=>0];

                if($exchange_type == 2 || $exchange_type == 1) {
                    $recipient['bank_id']=$payment_bank;
                }

                if($payment_bank !=null && $exchange_type == 1){
                    $bank_account=\DB::table('char_persons_banks')
                            ->where('char_persons_banks.bank_id',$payment_bank)
                            ->where('char_persons_banks.person_id',$recipient_id)
                            ->selectRaw("char_persons_banks.account_number")
                            ->first();
                    if($bank_account){
                         $recipient['cheque_account_number']=$bank_account->account_number;
                    }
               }
                return $recipient;
//                    PaymentsRecipient::create($recipient);
            }

        }

        return true;
    }
    public static function export($exportTo,$payment_id,$exchange_type,$payment_exchange,$address)
    {

        $language_id =  \App\Http\Helpers::getLocale();
        $query=\DB::table('char_payments_recipient')
            ->join('char_persons', 'char_payments_recipient.person_id', '=', 'char_persons.id')
            ->leftjoin('char_persons_contact', function($q) {
                $q->on('char_persons_contact.person_id', '=', 'char_persons.id');
                $q->where('char_persons_contact.contact_type','primary_mobile');
            })
            ->leftjoin('char_locations_i18n As mosques_name', function($join) use ($language_id)  {
                $join->on('char_persons.mosques_id', '=','mosques_name.location_id' )
                    ->where('mosques_name.language_id', $language_id);
            })
            ->leftJoin('char_locations_i18n As location_name', function($join) use ($language_id)  {
                $join->on('char_persons.location_id', '=','location_name.location_id' )
                    ->where('location_name.language_id',1);
            })
            ->leftjoin('char_locations_i18n As city_name', function($join) use ($language_id)  {
                $join->on('char_persons.city', '=','city_name.location_id' )
                    ->where('city_name.language_id',1);
            })
            ->leftjoin('char_locations_i18n As district_name', function($join) use ($language_id)  {
                $join->on('char_persons.governarate', '=','district_name.location_id' )
                    ->where('district_name.language_id',1);
            })
            ->leftjoin('char_locations_i18n As country_name', function($join)  use ($language_id)  {
                $join->on('char_persons.country', '=','country_name.location_id' )
                    ->where('country_name.language_id',1);
            });
        if($exchange_type == 1 || $exchange_type == 2){
            $query=$query
                ->leftjoin('char_banks', function($q) {
                    $q->on('char_banks.id', '=', 'char_payments_recipient.bank_id');
                })->selectRaw("char_banks.name as bank_name");
        };


        $get= trans('sponsorship::application.get');
        $not_get= trans('sponsorship::application.not get');

        $query=$query->where('char_payments_recipient.payment_id','=',$payment_id);

        if($exportTo =='excel'){

            $query
                ->orderBy('country_name.name')
                ->orderBy('district_name.name')
                ->orderBy('city_name.name')
                ->orderBy('location_name.name')
                ->orderBy('char_persons.first_name')
                ->orderBy('char_persons.second_name')
                ->orderBy('char_persons.third_name')
                ->orderBy('char_persons.last_name')
                ->selectRaw("
                         CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name,
                          char_persons.id_card_number,char_persons.old_id_card_number,
                          char_persons.family_cnt,
                          CASE WHEN char_persons_contact.contact_value is null THEN '-'  Else char_persons_contact.contact_value END AS mobile_no,
                          CASE WHEN char_payments_recipient.cheque_account_number is null THEN '-' Else char_payments_recipient.cheque_account_number  END  AS cheque_account_number,
                          CASE  WHEN '$payment_exchange' = 0 THEN char_payments_recipient.amount
                                WHEN '$payment_exchange' = 1 THEN char_payments_recipient.shekel_amount
                         END AS amount,
                           country_name.name as country,
                          district_name.name as district,
                          city_name.name as city,
                          location_name.name as location,
                          mosques_name.name as mosques,
                          char_persons.street_address
                        ");

            if($address){
                $query
                   ->selectRaw(" CONCAT(ifnull(country_name.name, ' '), ' - ' ,ifnull(district_name.name, ' '),' - ',
                                 ifnull(city_name.name, ' '),' - ', ifnull(location_name.name,' '),' - ', ifnull(mosques_name.name,' '), 
                                 ifnull(char_persons.street_address,' '))  AS address");
            }

            if($exchange_type == 2 || $exchange_type == 3){
                $query=$query->selectRaw("CASE WHEN char_payments_recipient.cheque_date is null THEN '-' Else char_payments_recipient.cheque_date  END  AS cheque_date");
            }


            $query->selectRaw("CASE  WHEN char_payments_recipient.status = 0 THEN '$not_get'
                                WHEN char_payments_recipient.status = 1 THEN '$get'
                         END AS status
                        ");
            return $query->get();
//        CASE WHEN char_payments_recipient.amount is null THEN '-' Else char_payments_recipient.amount  END  AS amount_0,

        }else{

            $query=$query
                ->orderBy('country_name.name')
                ->orderBy('district_name.name')
                ->orderBy('city_name.name')
                ->orderBy('location_name.name')
                ->orderBy('char_persons.first_name')
                ->orderBy('char_persons.second_name')
                ->orderBy('char_persons.third_name')
                ->orderBy('char_persons.last_name')
                ->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name,
                             char_persons.id_card_number,char_persons.old_id_card_number,
                             char_payments_recipient.cheque_account_number,
                             CASE WHEN char_payments_recipient.amount is null THEN 0 Else char_payments_recipient.amount  END  AS amount_0,
                              CASE  WHEN '$payment_exchange' = 0 THEN char_payments_recipient.amount
                                    WHEN '$payment_exchange' = 1 THEN char_payments_recipient.shekel_amount
                             END AS amount,
                              country_name.name as country,
                              district_name.name as district,
                              city_name.name as city,
                              location_name.name as location, mosques_name.name as mosques,char_persons.street_address
                           ")->get();

            $result=[];
            foreach($query as $k=>$v) {
                $pass=false;

                if($exchange_type == 1 || $exchange_type == 2) {
                    if($v->bank_name != null && $v->bank_name == ""){
                        $pass=true;
                    }
                }

                 if($exchange_type == 2 || $exchange_type == 3) {
                    if($v->cheque_date != null && $v->cheque_date == ""){
                        $pass=true;
                    }
                }

                if($v->cheque_account_number != null && $v->cheque_account_number != ""){
                    $pass=true;
                }

                if($pass){
                    array_push($result,$v);
                }
            }
            return $result;

        }


    }
    public static function exportAccorddingBanks($payment_id,$exchange_type,$payment_exchange)
    {

        $banks=\DB::table('char_payments_recipient')->where('char_payments_recipient.payment_id','=',$payment_id)
                                                    ->whereNotNull('bank_id')->distinct()->get(['bank_id']);
        $records=[];

        foreach($banks as $k=>$v){
            $bank= \DB::table('char_banks')->where('id',$v->bank_id)->first();
            $record=[ 'name' =>$bank->name ,'id'=>$v->bank_id ,'template'=>$bank->template];
             $query=\DB::table('char_payments_recipient')
                       ->join('char_persons', 'char_payments_recipient.person_id', '=', 'char_persons.id');

                  if($exchange_type == 2){
                       $query=$query->where('char_payments_recipient.cheque_date','!=',"")
                                    ->selectRaw("CASE WHEN char_payments_recipient.cheque_date is null THEN '-' Else char_payments_recipient.cheque_date  END  AS cheque_date");
                  }
              $record['items']=$query
                              ->where('char_payments_recipient.bank_id','=',$v->bank_id)
                              ->whereNotNull('char_payments_recipient.cheque_account_number')
                              ->where('char_payments_recipient.cheque_account_number','!=',"")
                              ->where('char_payments_recipient.payment_id','=',$payment_id)
                              ->orderBy('char_persons.first_name')
                              ->orderBy('char_persons.second_name')
                              ->orderBy('char_persons.third_name')
                              ->orderBy('char_persons.last_name')
                              ->orderBy('char_persons.location_id')
                              ->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name,
                                           char_persons.id_card_number,char_persons.old_id_card_number,char_payments_recipient.cheque_account_number,char_payments_recipient.bank_id,
                                            CASE  WHEN '$payment_exchange' = 0 THEN char_payments_recipient.amount
                                                WHEN '$payment_exchange' = 1 THEN char_payments_recipient.shekel_amount
                                         END AS amount
                                 ")->get();

//        CASE WHEN char_payments_recipient.amount is null THEN 0 Else char_payments_recipient.amount  END  AS amount

            $records[]=$record;
        }

            return $records;
    }
    public static function exportRecord($person_id,$payment_id,$exchange_type,$payment_exchange)
    {

        $query=\DB::table('char_payments_recipient')
            ->join('char_persons', 'char_payments_recipient.person_id', '=', 'char_persons.id');

        if($exchange_type == 2){
            $query=$query
                ->leftjoin('char_banks', function($q) {
                    $q->on('char_banks.id', '=', 'char_payments_recipient.bank_id');
                })->selectRaw("char_banks.name as bank_name,char_banks.template");
        };

        if($exchange_type == 2 || $exchange_type == 3){
            $query=$query->selectRaw("CASE WHEN char_payments_recipient.cheque_date is null THEN '-' Else char_payments_recipient.cheque_date  END  AS cheque_date");
        }

         return $query->where('char_payments_recipient.payment_id','=',$payment_id)
                      ->where('char_payments_recipient.person_id','=',$person_id)
                      ->selectRaw("char_payments_recipient.bank_id,
                      CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name,
                             char_persons.id_card_number,char_persons.old_id_card_number,char_payments_recipient.cheque_account_number,
                               CASE  WHEN '$payment_exchange' = 0 THEN char_payments_recipient.amount
                                                WHEN '$payment_exchange' = 1 THEN char_payments_recipient.shekel_amount
                                         END AS amount
                           ")->first();

//    CASE WHEN char_payments_recipient.amount is null THEN 0 Else char_payments_recipient.amount  END  AS amount



    }


   public static function individuals($filter,$id){

        $ids=$filter['payment_ids'];
        $pIds = join(",",$ids);
        $payment_exchange=$filter['payment_exchange'];

        return \DB::select("SELECT char_payments_cases.payment_id, 
                                           char_payments_cases.case_id,
                                           char_payments_cases.sponsor_number,
                                           concat(ifnull(char_persons.first_name, ' '),'  ', ifnull(char_persons.second_name, ' '),'  ',
                                                  ifnull(char_persons.third_name, ' '),'  ',ifnull(char_persons.last_name, ' ')) as full_name ,
                                           char_persons.first_name,char_persons.second_name,char_persons.third_name,char_persons.last_name,
                                           char_persons.id_card_number,char_persons.old_id_card_number,
                                           char_payments_cases.date_from, 
                                           char_payments_cases.date_to, 
                                           char_payments_cases.amount, 
                                           char_payments_cases.shekel_amount, 
                                           char_payments_cases.guardian_id, 
                                           char_payments_cases.recipient, 
                                           char_payments_cases.status,
                                           char_payments.payment_date,
                                           category.name as category_name,
                                            CASE  WHEN '$payment_exchange' = 0 THEN char_payments_cases.amount
                                                    WHEN '$payment_exchange' = 1 THEN char_payments_cases.shekel_amount
                                            END AS payment_value
                                            FROM char_payments_cases 
                                            JOIN char_payments ON char_payments.id =   char_payments_cases.payment_id
                                            JOIN char_payment_category_i18n as category On category.payment_category_id = char_payments.category_id
                                            JOIN char_cases ON char_cases.id =   char_payments_cases.case_id
                                            JOIN char_persons ON char_persons.id =   char_cases.person_id
                                            Where (char_payments_cases.payment_id IN ($pIds)and
                                                    ((char_payments_cases.guardian_id = $id and char_payments_cases.recipient = 1 )or
                                                    (char_cases.person_id = $id  and char_payments_cases.recipient = 0)))
                                            Group By  char_payments_cases.payment_id, char_payments_cases.case_id, char_payments_cases.sponsor_number       
                                            ORDER BY char_persons.first_name ASC ,char_persons.second_name ASC,char_persons.third_name ASC,char_persons.last_name ASC");

    }

}

