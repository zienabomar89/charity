<?php
namespace Sponsorship\Model;

class ArchivedCheques  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_archive_cheques';
    protected $fillable = ['title','file_id'];
    public $timestamps = false;

    public function File()
    {
        return $this->belongsTo('Document\Model\File', 'file', 'id');
    }

}


