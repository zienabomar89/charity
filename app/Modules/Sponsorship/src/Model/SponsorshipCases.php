<?php

namespace Sponsorship\Model;
use App\Http\Helpers;
use Common\Model\SponsorshipsCases;
use Organization\Model\Organization;

class SponsorshipCases  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_sponsorship_cases';
    protected $primaryKey = 'id';
    protected $fillable = ['case_id', 'sponsor_id','status','last_status_date','sponsor_number','sponsorship_id'];

//    public $timestamps = false;
    protected $hidden = ['created_at','updated_at'];

    public static function getNominated($sponsor_number,$payment_bank,$exchange_type,$sponsor_id)
    {

        $user = \Auth::user();
        $organization_id = $user->organization_id;

        $return=\DB::table('char_sponsorship_cases')
            ->join('char_cases', function($q) use($organization_id){
                $q->on('char_cases.id','=','char_sponsorship_cases.case_id');
                $q->where('char_cases.organization_id','=',$organization_id);
            })
            ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
            ->leftjoin('char_guardians', function($q) {
                $q->on('char_cases.person_id','=','char_guardians.individual_id');
                $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                $q->where('char_guardians.status','=',1);
            })
            ->leftjoin('char_payments_cases', function($q) {
                $q->on('char_payments_cases.case_id','=','char_sponsorship_cases.case_id');
                $q->on('char_payments_cases.sponsor_number','=','char_sponsorship_cases.sponsor_number');
            });

        if($payment_bank !=null && $exchange_type == 1){
            $return=$return
                ->leftjoin('char_persons_banks', function($q) use($payment_bank){
                    $q->on('char_cases.person_id','=','char_persons_banks.person_id');
                    $q->where('char_persons_banks.bank_id','=',$payment_bank);
                })
                ->leftjoin('char_persons_banks as guardian_banks', function($q) use($payment_bank){
                    $q->on('char_guardians.guardian_id','=','guardian_banks.person_id');
                    $q->where('guardian_banks.bank_id','=',$payment_bank);
                })
                ->selectRaw("CASE WHEN char_payments_cases.recipient is null THEN char_persons_banks.bank_id
                                      WHEN char_payments_cases.recipient =0 THEN char_persons_banks.bank_id
                                      Else guardian_banks.bank_id  END  AS bank_id,
                                 CASE WHEN char_payments_cases.recipient is null THEN char_persons_banks.account_number
                                      WHEN char_payments_cases.recipient =0 THEN char_persons_banks.account_number
                                      Else guardian_banks.account_number  END  AS account_number");

        }

        return
            $return->where(function ($q) use($sponsor_id,$sponsor_number){
                $q->where('char_sponsorship_cases.sponsor_number','=',$sponsor_number);
                $q->where('char_sponsorship_cases.sponsor_id','=',$sponsor_id);
            })
                ->selectRaw("CASE WHEN char_guardians.guardian_id is null THEN 0 
                                  WHEN char_guardians.guardian_id = char_cases.person_id THEN 0
                                  Else 1  END  AS recipient,
                             CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                    ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                             char_cases.id as case_id ,
                             char_cases.person_id ,
                             get_payments_count(char_cases.id,char_sponsorship_cases.sponsor_number,char_sponsorship_cases.sponsor_id) as payments_count,
                             char_guardians.guardian_id,
                             char_sponsorship_cases.sponsor_number,
                             char_sponsorship_cases.sponsorship_id")
                ->first();


    }

    // ********* //
    public static function getSponsorList($case_id,$status){

        $char_sponsorship_cases=['status','sponsor_id'];

        $query= \DB::table('char_sponsorship_cases')
            ->join('char_organizations', 'char_organizations.id', '=', 'char_sponsorship_cases.sponsor_id')
            ->join('char_cases', 'char_cases.id', '=', 'char_sponsorship_cases.case_id')
            ->join('char_organizations as org', 'org.id', '=', 'char_cases.organization_id')
            ->join('char_categories as ca','ca.id', 'char_cases.category_id');

        $query->where('char_sponsorship_cases.case_id',$case_id);
        $query->where('char_sponsorship_cases.status',$status);


//              $query->groupBy('char_sponsorship_cases.id');
        $nominate =  trans('sponsorship::application.nominate');
        $sent =  trans('sponsorship::application.sent');
        $guaranteed =  trans('sponsorship::application.guaranteed');
        $stopped =  trans('sponsorship::application.stopped');
        $paused =  trans('sponsorship::application.paused');
        $Default =  trans('sponsorship::application.Default');

        return $query

            ->selectRaw("    ca.name as category_name,
                             char_organizations.name as organization_name,
                             org.name as sponsor_name,
                             char_sponsorship_cases.sponsor_number,
                             CASE  WHEN char_sponsorship_cases.sponsorship_date is null THEN'$Default'
                                   Else char_sponsorship_cases.sponsorship_date
                                   END
                             AS sponsorship_date,
                             CASE WHEN char_sponsorship_cases.status = 1 THEN'$nominate'
                                          WHEN char_sponsorship_cases.status = 2 THEN'$sent'
                                          WHEN char_sponsorship_cases.status = 3 THEN'$guaranteed'
                                          WHEN char_sponsorship_cases.status = 4 THEN'$stopped'
                                          WHEN char_sponsorship_cases.status = 5 THEN '$paused'
                             END AS status
                                                   ") ->get();
    }

    public static function getCasesStatistics($page,$organization_id,$sponsor_id,$filters){

        $language_id =  \App\Http\Helpers::getLocale();

        $char_persons=['first_name','id_card_number','last_name','second_name','third_name'];
        $char_cases=['category_id'];
        $char_sponsorship_cases=['status','sponsor_id'];
        $char_persons_father = ['father_id_card_number','father_first_name','father_second_name','father_third_name','father_last_name'];
        $char_persons_mother = ['mother_id_card_number','mother_first_name','mother_second_name','mother_third_name','mother_last_name'];
        $char_persons_guardian=['guardian_id_card_number','guardian_first_name', 'guardian_second_name','guardian_third_name','guardian_last_name'];
        
        $char_persons_father_cnt = 0;
        $char_persons_mother_cnt = 0;
        $char_persons_guardian_cnt = 0;

        $condition=[];

        foreach ($filters as $key => $value) {
            if(in_array($key, $char_persons)) {
                if ( $value != "" ) {
                    $data = ['char_persons.' .$key => $value];
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $char_cases)) {
                if ( $value != "" ) {
                    $data = ['char_cases.'. $key => $value];
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $char_sponsorship_cases)) {
                if ( $value != "" ) {
                    $data = ['char_sponsorship_cases.'.$key => $value];
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $char_persons_father)) {
                if ( $value != "" ) {
                    $data = ['father.' . substr($key, 7) => (int)$value];
                    array_push($condition, $data);
                    $char_persons_father_cnt++;
                }
            }
            if(in_array($key, $char_persons_mother)) {
                if ( $value != "" ) {
                    $data = ['mother.' . substr($key, 7) => (int)$value];
                    array_push($condition, $data);
                    $char_persons_mother_cnt++;
                }
            }
            if(in_array($key, $char_persons_guardian)) {
                if ( $value != "" ) {
                    $data = ['guardian.' . substr($key, 9) => (int)$value];
                    array_push($condition, $data);
                    $char_persons_guardian_cnt++;
                }
            }
        }


        $query= \DB::table('char_sponsorship_cases')
            ->join('char_organizations', 'char_organizations.id', '=', 'char_sponsorship_cases.sponsor_id')
            ->join('char_cases', 'char_cases.id', '=', 'char_sponsorship_cases.case_id')
            ->join('char_organizations as org', 'org.id', '=', 'char_cases.organization_id')
            ->join('char_categories as ca','ca.id', 'char_cases.category_id')
            ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id');
        

        $all_organization=null;
        if(isset($filters['all_organization'])){
            if($filters['all_organization'] ===true){
                $all_organization=0;
            }elseif($filters['all_organization'] ===false){
                $all_organization=1;

            }
        }
        if($all_organization ==0){
            $organizations=[];
            if(isset($filters['organization_ids']) && $filters['organization_ids'] !=null && $filters['organization_ids'] !="" && $filters['organization_ids'] !=[] && sizeof($filters['organization_ids']) != 0) {
                if($filters['organization_ids'][0]==""){
                    unset($filters['organization_ids'][0]);
                }
                $organizations =$filters['organization_ids'];
            }
            if(sizeof($organizations) > 0 ){
                $query->wherein('char_cases.organization_id',$organizations);
            }else{
                $user = \Auth::user();

                if($user->type == 2) {
                    $query->where(function ($anq) use ($organization_id,$user) {
                        $anq->where('char_cases.organization_id',$organization_id);
                        $anq->orwherein('char_cases.organization_id', function($decq) use($user) {
                            $decq->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });

                }else{

                    $query->where(function ($anq) use ($organization_id) {
                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                        $anq->wherein('char_cases.organization_id', function($decq) use($organization_id) {
                            $decq->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $organization_id);
                        });
                    });
                }

            }
        }
        elseif($all_organization ==1){
            $query->where('char_cases.organization_id',$organization_id);
        }
        if($sponsor_id != null){
            $query=$query->where('char_sponsorship_cases.sponsor_id',$sponsor_id);
        }

        if (count($condition) != 0) {
            $query =  $query
                ->where(function ($q) use ($condition) {
                    $names = ['char_persons.first_name', 'char_persons.second_name', 'char_persons.third_name', 'char_persons.last_name',
                        'father.first_name', 'father.second_name', 'father.third_name', 'father.last_name',
                        'mother.first_name', 'mother.second_name', 'mother.third_name', 'mother.last_name',
                        'guardian.first_name', 'guardian.second_name', 'guardian.third_name', 'guardian.last_name'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $names)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });
        }

        $to_sponsorship_date=null;
        $from_sponsorship_date=null;
        $to_guaranteed_date=null;
        $from_guaranteed_date=null;

        if(isset($filters['to_sponsorship_date']) && $filters['to_sponsorship_date'] !=null){
            $to_sponsorship_date=date('Y-m-d',strtotime($filters['to_sponsorship_date']));
        }
        if(isset($filters['from_sponsorship_date']) && $filters['from_sponsorship_date'] !=null){
            $from_sponsorship_date=date('Y-m-d',strtotime($filters['from_sponsorship_date']));
        }
        if(isset($filters['to_guaranteed_date']) && $filters['to_guaranteed_date'] !=null){
            $to_guaranteed_date=date('Y-m-d',strtotime($filters['to_guaranteed_date']));
        }
        if(isset($filters['from_guaranteed_date']) && $filters['from_guaranteed_date'] !=null){
            $from_guaranteed_date=date('Y-m-d',strtotime($filters['from_guaranteed_date']));
        }

        if($from_sponsorship_date != null && $to_sponsorship_date != null) {
            $query = $query->whereBetween( 'char_sponsorship_cases.created_at', [ $from_sponsorship_date, $to_sponsorship_date]);
        }elseif($from_sponsorship_date != null && $to_sponsorship_date == null) {
            $query = $query->whereDate('char_sponsorship_cases.created_at', '>=', $from_sponsorship_date);
        }elseif($from_sponsorship_date == null && $to_sponsorship_date != null) {
            $query = $query->whereDate('char_sponsorship_cases.created_at', '<=', $to_sponsorship_date);
        }

        if($from_guaranteed_date != null && $to_guaranteed_date != null) {
            $query = $query->whereBetween( 'char_sponsorship_cases.sponsorship_date', [ $from_guaranteed_date, $to_guaranteed_date]);
        }elseif($from_guaranteed_date != null && $to_guaranteed_date == null) {
            $query = $query->whereDate('char_sponsorship_cases.sponsorship_date', '>=', $from_guaranteed_date);
        }elseif($from_guaranteed_date == null && $to_guaranteed_date != null) {
            $query = $query->whereDate('char_sponsorship_cases.sponsorship_date', '<=', $to_guaranteed_date);
        }
        $query->groupBy('char_sponsorship_cases.id');

        $nominate =  trans('sponsorship::application.nominate');
        $sent =  trans('sponsorship::application.sent');
        $guaranteed =  trans('sponsorship::application.guaranteed');
        $stopped =  trans('sponsorship::application.stopped');
        $paused =  trans('sponsorship::application.paused');
        $Default =  trans('sponsorship::application.Default');

        $active = trans('common::application.active');
        $inactive = trans('common::application.inactive');

        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $refugee = trans('common::application.refugee');
        $citizen = trans('common::application.citizen');

        $not_exist = trans('common::application.not_exist');
        $exist = trans('common::application.exist');

        $not_work = trans('common::application.not_work');
        $work = trans('common::application.work');

        $is_alive = trans('common::application.is_alive');
        $deceased = trans('common::application.deceased');

        $vocational = trans('common::application.vocational');
        $academic = trans('common::application.academic');

        $perfect = trans('common::application.perfect');
        $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
        $special_needs = trans('common::application.special_needs');

        $excellent = trans('common::application.excellent');
        $acceptable = trans('common::application.acceptable');

        $good = trans('common::application.good');
        $very_good = trans('common::application.very_good');
        $weak = trans('common::application.weak');
        $very_bad_condition = trans('common::application.very_bad_condition');
        $bad_condition = trans('common::application.bad_condition');

        $the_first  = trans('common::application.the_first') ;
        $the_second = trans('common::application.the_second') ;
        $the_third  = trans('common::application.the_third') ;
        $the_fourth = trans('common::application.the_fourth') ;
        $the_Fifth  = trans('common::application.the_Fifth');
        $the_Sixth  = trans('common::application.the_Sixth');
        $the_Seventh = trans('common::application.the_Seventh');
        $the_Eighth= trans('common::application.the_Eighth');
        $the_Ninth = trans('common::application.the_Ninth');
        $the_tenth = trans('common::application.the_tenth');
        $the_eleventh = trans('common::application.the_eleventh');
        $the_twelveth= trans('common::application.the_twelveth');

        $special_studies= trans('common::application.special_studies');
        $vocational_training= trans('common::application.vocational_training');
        $first_year_of_university= trans('common::application.first_year_of_university');
        $second_year_of_university= trans('common::application.second_year_of_university');
        $third_year_of_university= trans('common::application.third_year_of_university');
        $fourth_year_of_university= trans('common::application.fourth_year_of_university');
        $fifth_year_of_university= trans('common::application.fifth_year_of_university');


        $regularly = trans('common::application.regularly');
        $does_not_pray = trans('common::application.does_not_pray');
        $sometimes = trans('common::application.sometimes');

        if($filters['action'] !='export'){
                                    
        if($char_persons_father_cnt > 0 ){
            $query->leftjoin('char_persons AS father', 'char_persons.father_id', '=', 'father.id');
        }
        
        if($char_persons_mother_cnt > 0 ){
            $query->leftjoin('char_persons AS mother', 'char_persons.mother_id', '=', 'mother.id');
        }
        
        if($char_persons_guardian_cnt > 0 ){
            $query->leftjoin('char_guardians', function($q)  {
                    $q->on('char_guardians.individual_id','=','char_cases.person_id');
                    $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                    $q->where('char_guardians.status','=',1);
                })
                ->leftjoin('char_persons AS guardian', 'char_guardians.guardian_id', '=', 'guardian.id');
        }
            
             
                
        }
        
        if($filters['action'] =='filters'){
            $query->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,
                                      ifnull(char_persons.second_name, ' '),' ',
                                      ifnull(char_persons.third_name,' '),' ', 
                                      ifnull(char_persons.last_name,' '))as name,char_persons.id_card_number,char_persons.old_id_card_number,
                                     char_cases.category_id,
                                     ca.name as category_name,
                                     org.name as organization_name,
                                     char_organizations.name as sponsor_name,
                                     char_sponsorship_cases.sponsor_number,
                                     CASE
                                        WHEN char_sponsorship_cases.sponsorship_date is null THEN '$Default'
                                        Else char_sponsorship_cases.sponsorship_date
                                         END
                                     AS sponsorship_date,
                                    char_sponsorship_cases.sponsorship_date AS spon_date,
                                     char_sponsorship_cases.sponsorship_date as date_flag,
                                     CASE WHEN char_sponsorship_cases.status = 1 THEN '$nominate'
                                          WHEN char_sponsorship_cases.status = 2 THEN '$sent'
                                          WHEN char_sponsorship_cases.status = 3 THEN '$guaranteed'
                                          WHEN char_sponsorship_cases.status = 4 THEN '$stopped'
                                          WHEN char_sponsorship_cases.status = 5 THEN '$paused'
                                     END AS status,
                                     char_sponsorship_cases.status as flag,
                                     char_sponsorship_cases.id,
                                     char_sponsorship_cases.sponsor_id
                                    "
            );

            $map = ["name" => 'char_persons.first_name',
                "organization_name" => 'org.name',
                "id_card_number" => 'char_persons.id_card_number',
                "sponsor_name" => 'char_organizations.name',
                "category_name" => 'ca.name',
                "sponsor_number" => 'char_sponsorship_cases.sponsor_number',
                "status" => 'char_sponsorship_cases.status',
                "sponsorship_date" => 'char_sponsorship_cases.sponsorship_date',
            ];
            $order = false;

            if (isset($filters['sortKeyArr_rev']) && $filters['sortKeyArr_rev'] != null && $filters['sortKeyArr_rev'] != "" &&
                $filters['sortKeyArr_rev'] != [] && sizeof($filters['sortKeyArr_rev']) != 0
            ) {

                $order = true;

                foreach ($filters['sortKeyArr_rev'] as $key_) {
                    $query->orderBy($map[$key_],'desc');
                }
            }


            if (isset($filters['sortKeyArr_un_rev']) && $filters['sortKeyArr_un_rev'] != null && $filters['sortKeyArr_un_rev'] != "" &&
                $filters['sortKeyArr_un_rev'] != [] && sizeof($filters['sortKeyArr_un_rev']) != 0
            ) {

                $order = true;
                foreach ($filters['sortKeyArr_un_rev'] as $key_) {
                    $query->orderBy($map[$key_],'asc');
                }
            }

            if(!$order){
                $query->orderBy('char_sponsorship_cases.created_at','desc');
            }
            $itemsCount = isset($filters['itemsCount'])? $filters['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
            return $query->paginate($records_per_page);

        }
        else if($filters['action'] =='export'){
            
               $query->leftjoin('char_guardians', function($q)  {
                    $q->on('char_guardians.individual_id','=','char_cases.person_id');
                    $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                    $q->where('char_guardians.status','=',1);
                })
                ->leftjoin('char_persons AS father', 'char_persons.father_id', '=', 'father.id')
                ->leftjoin('char_persons AS mother', 'char_persons.mother_id', '=', 'mother.id')
                ->leftjoin('char_persons AS guardian', 'char_guardians.guardian_id', '=', 'guardian.id');
            
             if(isset($filters['items'])){
                if(sizeof($filters['items']) > 0 ){
                 $query->whereIn('char_sponsorship_cases.id',$filters['items']);  
               }      
             } 
            
            return $query
                ->leftjoin('char_locations_i18n As L1', function($q) use ($language_id){
                    $q->on('L1.location_id', '=', 'char_persons.location_id');
                    $q->where('L1.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L2', function($q) use ($language_id){
                    $q->on('L2.location_id', '=', 'char_persons.city');
                    $q->where('L2.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L3', function($q) use ($language_id){
                    $q->on('L3.location_id', '=', 'char_persons.governarate');
                    $q->where('L3.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L4', function($q) use ($language_id){
                    $q->on('L4.location_id', '=', 'char_persons.country');
                    $q->where('L4.language_id',$language_id);
                })
                ->leftjoin('char_persons_contact as cont1', function($q) {
                    $q->on('cont1.person_id', '=', 'char_persons.id');
                    $q->where('cont1.contact_type','phone');
                })
                ->leftjoin('char_persons_contact as cont2', function($q) {
                    $q->on('cont2.person_id', '=', 'char_persons.id');
                    $q->where('cont2.contact_type','primary_mobile');
                })
                ->leftjoin('char_persons_contact as cont3', function($q) {
                    $q->on('cont3.person_id', '=', 'char_persons.id');
                    $q->where('cont3.contact_type','secondery_mobile');
                })
                ->leftjoin('char_persons_health', function($q) {
                    $q->on('char_persons_health.person_id', '=', 'char_persons.id');
                })
                ->leftjoin('char_diseases_i18n', function($q) use ($language_id){
                    $q->on('char_diseases_i18n.disease_id', '=', 'char_persons_health.disease_id');
                    $q->where('char_diseases_i18n.language_id',$language_id);
                })
                ->leftjoin('char_residence', function($q) {
                    $q->on('char_residence.person_id', '=', 'char_persons.id');
                })
                ->leftjoin('char_property_types_i18n', function($q) use ($language_id){
                    $q->on('char_property_types_i18n.property_type_id', '=', 'char_residence.property_type_id');
                    $q->where('char_property_types_i18n.language_id',$language_id);
                })
                ->leftjoin('char_roof_materials_i18n', function($q) use ($language_id){
                    $q->on('char_roof_materials_i18n.roof_material_id', '=', 'char_residence.roof_material_id');
                    $q->where('char_roof_materials_i18n.language_id',$language_id);
                })
                ->leftjoin('char_building_status_i18n', function($q) use ($language_id){
                    $q->on('char_building_status_i18n.building_status_id', '=', 'char_residence.house_condition');
                    $q->where('char_building_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_furniture_status_i18n', function($q) use ($language_id){
                    $q->on('char_furniture_status_i18n.furniture_status_id', '=', 'char_residence.indoor_condition');
                    $q->where('char_furniture_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_habitable_status_i18n', function($q) use ($language_id){
                    $q->on('char_habitable_status_i18n.habitable_status_id', '=', 'char_residence.habitable');
                    $q->where('char_habitable_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_house_status_i18n', function($q) use ($language_id){
                    $q->on('char_house_status_i18n.house_status_id', '=', 'char_residence.residence_condition');
                    $q->where('char_house_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_persons_islamic_commitment', function($q) {
                    $q->on('char_persons_islamic_commitment.person_id', '=', 'char_persons.id');
                })
                ->leftjoin('char_persons_education', function($q) {
                    $q->on('char_persons_education.person_id', '=', 'char_persons.id');
                })
                ->leftjoin('char_edu_stages_i18n as char_edu_stages', function($q) use ($language_id){
                    $q->on('char_edu_stages.edu_stage_id', '=', 'char_persons_education.stage');
                    $q->where('char_edu_stages.language_id',$language_id);
                })
                ->leftjoin('char_edu_authorities_i18n as char_edu_authorities', function($q) use ($language_id){
                    $q->on('char_edu_authorities.edu_authority_id', '=', 'char_persons_education.authority');
                    $q->where('char_edu_authorities.language_id',$language_id);
                })
                ->leftjoin('char_edu_degrees_i18n as char_edu_degrees', function($q) use ($language_id){
                    $q->on('char_edu_degrees.edu_degree_id', '=', 'char_persons_education.degree');
                    $q->where('char_edu_degrees.language_id',$language_id);
                })
                ->leftjoin('char_organization_persons_banks', function($q) {
                    $q->on('char_organization_persons_banks.person_id', '=', 'char_persons.id');
                    $q->on('char_organization_persons_banks.organization_id', '=', 'char_cases.organization_id');
                })
                ->leftjoin('char_persons_banks', function($q) {
                    $q->on('char_persons_banks.id', '=', 'char_organization_persons_banks.bank_id');
                })
                ->leftjoin('char_branches', function($q) {
                    $q->on('char_branches.id', '=', 'char_persons_banks.branch_name');
                })
                ->leftjoin('char_banks', function($q) {
                    $q->on('char_banks.id', '=', 'char_persons_banks.bank_id');
                })
                ->leftjoin('char_persons_contact as guardian_cont1', function($q) {
                    $q->on('guardian_cont1.person_id', '=', 'guardian.id');
                    $q->where('guardian_cont1.contact_type','phone');
                })
                ->leftjoin('char_persons_contact as guardian_cont2', function($q) {
                    $q->on('guardian_cont2.person_id', '=', 'guardian.id');
                    $q->where('guardian_cont2.contact_type','primary_mobile');
                })
                ->leftjoin('char_persons_contact as guardian_cont3', function($q) {
                    $q->on('guardian_cont3.person_id', '=', 'guardian.id');
                    $q->where('guardian_cont3.contact_type','secondery_mobile');
                })
                ->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                             ca.name as category_name,
                             char_persons.id_card_number,char_persons.old_id_card_number,
                             char_organizations.name as organization_name,
                             org.name as sponsor_name,
                             char_sponsorship_cases.sponsor_number,
                             CASE  WHEN char_sponsorship_cases.sponsorship_date is null THEN'$Default'
                                   Else char_sponsorship_cases.sponsorship_date
                                   END
                             AS sponsorship_date,
                             CASE WHEN char_sponsorship_cases.status = 1 THEN'$nominate'
                                          WHEN char_sponsorship_cases.status = 2 THEN'$sent'
                                          WHEN char_sponsorship_cases.status = 3 THEN'$guaranteed'
                                          WHEN char_sponsorship_cases.status = 4 THEN'$stopped'
                                          WHEN char_sponsorship_cases.status = 5 THEN'$paused'
                             END AS status,
                             CASE WHEN char_get_siblings_count(char_cases.person_id,'both',null) is null THEN 0
                                  Else char_get_siblings_count(char_cases.person_id,'both',null) END AS brothers,
                             CASE WHEN char_get_siblings_count(char_cases.person_id,'both',1) is null THEN 0
                                  Else char_get_siblings_count(char_cases.person_id,'both',1) END AS male_brothers,
                             CASE WHEN char_get_siblings_count(char_cases.person_id,'both',2) is null THEN 0
                                  Else char_get_siblings_count(char_cases.person_id,'both',2) END AS female_brothers,
                             CASE WHEN char_persons.birthday is null THEN ' ' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                             CASE WHEN  char_persons.birthday is null THEN ' ' Else  EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(), char_persons.birthday)))) END AS age,
                             CASE
                                 WHEN char_persons.gender is null THEN ' '
                                 WHEN char_persons.gender = 1 THEN '$male'
                                 WHEN char_persons.gender = 2 THEN '$female'
                            END
                               AS the_gender,
                            CASE WHEN char_persons.governarate is null THEN ' ' Else L3.name END   AS district,
                            CASE
                                WHEN char_persons.Location_id is null THEN ' '
                                Else CONCAT(L4.name, ' - ',L3.name, ' - ',L2.name, ' - ', L1.name, ' - ', char_persons.street_address)
                            END
                            AS address,
                                                     CASE WHEN cont1.contact_value is null THEN ' ' Else cont1.contact_value END   AS phone,
                                                     CASE WHEN cont2.contact_value is null THEN ' ' Else cont2.contact_value END   AS primary_mobile,
                                                     CASE WHEN cont3.contact_value is null THEN ' ' Else cont3.contact_value END   AS secondary_mobile,
                                                     CASE
                                                          WHEN char_persons_health.condition is null THEN ' '
                                                          WHEN char_persons_health.condition = 1 THEN '$perfect'
                                                          WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                          WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                                     END
                                                     AS health_status,
                                                     CASE WHEN char_persons_health.details is null THEN ' ' Else char_persons_health.details END   AS details,
                                                     CASE WHEN char_diseases_i18n.name is null THEN ' ' Else char_diseases_i18n.name END   AS disease_id,
                                                     CASE WHEN char_property_types_i18n.name is null THEN ' ' Else char_property_types_i18n.name END   AS property_type,
                                                     CASE WHEN char_roof_materials_i18n.name is null THEN ' ' Else char_roof_materials_i18n.name END   AS roof_material,
                                                     CASE WHEN char_building_status_i18n.name is null THEN ' ' Else char_building_status_i18n.name  END  AS house_condition,
                                                     CASE WHEN char_furniture_status_i18n.name is null THEN ' ' Else char_furniture_status_i18n.name  END  AS indoor_condition,
                                                     CASE WHEN char_habitable_status_i18n.name is null THEN ' ' Else char_habitable_status_i18n.name  END  AS habitable,
                                                     CASE WHEN char_house_status_i18n.name is null     THEN ' ' Else char_house_status_i18n.name  END  AS residence_condition,
                                                     CASE WHEN char_residence.area is null             THEN ' ' Else char_residence.area  END  AS area,
                                                     CASE WHEN char_residence.rooms is null            THEN ' ' Else char_residence.rooms  END  AS rooms,
                                                     CASE WHEN char_residence.rent_value is null       THEN ' ' Else char_residence.rent_value  END  AS rent_value,
                                                      CASE
                                                          WHEN char_persons_education.study is null THEN ' '
                                                          WHEN char_persons_education.study = 1 THEN '$yes'
                                                          WHEN char_persons_education.study = 0 THEN '$no'
                                                              Else ' '
   END
                                                     AS study,
                                                     CASE
                                                               WHEN char_persons_education.type is null THEN ' '
                                                               WHEN char_persons_education.type = 1 THEN '$vocational'
                                                               WHEN char_persons_education.type = 2 THEN '$academic'
                                                                Else ' '
  END
                                                     AS type,
                                                     CASE
                                                               WHEN char_persons_education.level is null THEN ' '
                                                               WHEN char_persons_education.level = 1 THEN '$weak'
                                                               WHEN char_persons_education.level = 2 THEN '$acceptable'
                                                               WHEN char_persons_education.level = 3 THEN '$good'
                                                               WHEN char_persons_education.level = 4 THEN '$very_good'
                                                               WHEN char_persons_education.level = 5 THEN '$excellent'
                                                               Else ' '
                                                     END
                                                     AS level,
                                                    CASE
                                                               WHEN char_persons_education.grade is null THEN ' '
                                                               WHEN char_persons_education.grade = 1 THEN '$the_first '
                                                               WHEN char_persons_education.grade = 2 THEN '$the_second '
                                                               WHEN char_persons_education.grade = 3 THEN '$the_third '
                                                               WHEN char_persons_education.grade = 4 THEN '$the_fourth '
                                                               WHEN char_persons_education.grade = 5 THEN '$the_Fifth '
                                                               WHEN char_persons_education.grade = 6 THEN '$the_Sixth '
                                                               WHEN char_persons_education.grade = 7 THEN '$the_Seventh '
                                                               WHEN char_persons_education.grade = 8 THEN '$the_Eighth '
                                                               WHEN char_persons_education.grade = 9 THEN '$the_Ninth '
                                                               WHEN char_persons_education.grade = 10 THEN '$the_tenth'
                                                               WHEN char_persons_education.grade = 11 THEN '$the_eleventh'
                                                               WHEN char_persons_education.grade = 12 THEN '$the_twelveth'
                                                               WHEN char_persons_education.grade = 13 THEN '$special_studies'
                                                               WHEN char_persons_education.grade = 14 THEN '$vocational_training'
                                                               WHEN char_persons_education.grade = 15 THEN '$first_year_of_university'
                                                               WHEN char_persons_education.grade = 16 THEN '$second_year_of_university'
                                                               WHEN char_persons_education.grade = 17 THEN '$third_year_of_university'
                                                               WHEN char_persons_education.grade = 18 THEN '$fourth_year_of_university'
                                                               WHEN char_persons_education.grade = 19 THEN '$fifth_year_of_university' 
                                                                   Else ' '
                                                     END
                                                     AS grade,
                                                     CASE WHEN char_persons_education.year is null THEN ' '   Else char_persons_education.year END  AS year,
                                                     CASE WHEN char_persons_education.points is null THEN ' ' Else char_persons_education.points END  AS points,
                                                     CASE WHEN char_persons_education.school is null THEN ' ' Else char_persons_education.school END  AS school,
                                                     CASE WHEN char_edu_authorities.name is null THEN ' ' Else char_edu_authorities.name END as authority,
                                                     CASE WHEN char_edu_stages.name is null THEN ' ' Else char_edu_stages.name END as stage,
                                                     CASE WHEN char_edu_degrees.name is null THEN ' ' Else char_edu_degrees.name END as degree,
                                                     CASE
                                                               WHEN char_persons_islamic_commitment.prayer is null THEN ' '
                                                              WHEN char_persons_islamic_commitment.prayer = 0 THEN '$regularly'
                                                             WHEN char_persons_islamic_commitment.prayer = 1 THEN '$does_not_pray'
                                                             WHEN char_persons_islamic_commitment.prayer = 2 THEN '$sometimes'
                                                    END
                                                     AS prayer,
                                                     CASE
                                                               WHEN char_persons_islamic_commitment.quran_parts is null THEN ' '
                                                               WHEN char_persons_islamic_commitment.quran_parts = 0 THEN '$no'
                                                               Else '$yes'
                                                    END
                                                     AS save_quran,
                                                      CASE
                                                        WHEN char_persons_islamic_commitment.quran_center is null THEN ' '
                                                        WHEN char_persons_islamic_commitment.quran_center = 0 THEN '$no'
                                                        Else '$yes'
                                                        END
                                                        AS quran_center,
                                                     CASE WHEN char_persons_islamic_commitment.prayer_reason is null THEN ' ' Else  char_persons_islamic_commitment.prayer_reason END AS prayer_reason,
                                                     CASE WHEN char_persons_islamic_commitment.quran_reason is null  THEN ' ' Else char_persons_islamic_commitment.quran_reason END AS quran_reason,
                                                     CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN ' ' Else char_persons_islamic_commitment.quran_parts END  AS quran_parts,
                                                     CASE WHEN char_persons_islamic_commitment.quran_chapters is null  THEN ' ' Else char_persons_islamic_commitment.quran_chapters END AS quran_chapters,
                                                     CASE WHEN char_banks.name is null THEN ' ' Else char_banks.name END AS bank_name,
                                                     CASE WHEN char_branches.name is null THEN ' ' Else char_branches.name END AS branch_name,
                                                     CASE WHEN char_persons_banks.account_number is null THEN ' ' Else char_persons_banks.account_number END AS account_number,
                                                     CASE WHEN char_persons_banks.account_owner is null THEN ' ' Else char_persons_banks.account_owner END AS account_owner,

                                                     CASE WHEN father.first_name is null THEN ' '
                                                     Else CONCAT(father.first_name, ' ' , father.second_name, ' ', father.third_name, ' ' , father.last_name)
                                                      END
                                                      as father_name,
                                                     CASE WHEN father.id_card_number is null THEN ' ' Else father.id_card_number  END as father_id_card_number,
                                                     CASE WHEN father.birthday is null THEN ' ' Else DATE_FORMAT(father.birthday,'%Y/%m/%d')  END  as father_birthday,
                                                     CASE WHEN father.spouses is null THEN ' ' Else father.spouses  END  as father_spouses,
                                                     CASE WHEN father.death_date is null THEN  '$is_alive' Else  '$deceased' END as father_alive,
                                                     CASE WHEN father.death_date is null THEN ' '  Else father.death_date END as father_death_date,
                                                     CASE WHEN mother.first_name is null THEN ' '
                                                         Else CONCAT(mother.first_name, ' ' , mother.second_name, ' ', mother.third_name, ' ' , mother.last_name)
                                                     END  AS mother_name,
                                                     CASE WHEN mother.id_card_number is null THEN ' ' Else mother.id_card_number  END  AS mother_id_card_number,
                                                    CASE WHEN guardian.first_name is null THEN ' '
                                                    Else CONCAT(guardian.first_name, ' ' , guardian.second_name, ' ', guardian.third_name, ' ' , guardian.last_name)
                                                    END
                                                    As guardian_name,
                                                    CASE WHEN guardian.id_card_number is null THEN ' ' Else guardian.id_card_number  END  As guardian_id_card_number,
                                                    CASE WHEN guardian_cont1.contact_value is null THEN ' ' Else guardian_cont1.contact_value END   As guardian_phone,
                                                    CASE WHEN guardian_cont2.contact_value is null THEN ' ' Else guardian_cont2.contact_value END   As guardian_primary_mobile,
                                                    CASE WHEN guardian_cont3.contact_value is null THEN ' ' Else guardian_cont3.contact_value END   As guardian_secondery_mobile

                                                   ") ->get();


        }
        else if($filters['action'] =='ExportToWord'){

            if(isset($filters['items'])){
                if(sizeof($filters['items']) > 0 ){
                 $query->whereIn('char_sponsorship_cases.id',$filters['items']);  
               }      
            }
             
            $query= $query->selectRaw("char_cases.id,char_sponsorship_cases.sponsor_id,char_organizations.name as sponsor_name" )->get();
            $return=[];

            $lang = 'ar';
            if(isset($filters['lang'])){
                $lang = $filters['lang'];
            }
            foreach($query as $k =>$v){

//                $fetch_array
//                $row=Cases::getCaseInfo($v->id);
                $sub=\Common\Model\CaseModel::fetch(array(
                    'action' => 'show',
                    'category'      => true,
                    'full_name'     => true,
                    'family_member' => true,
                    'person'        => true,
                    'persons_i18n'  => true,
                    'contacts'      => true,
                    'education'     => true,
                    'islamic'       => true,
                    'health'        => true,
                    'work'          => true,
                    'residence'     => true,
                    'default_bank'  => true,
                    'banks'         => true,
                    'persons_documents'  => true,
                    'organization_name'      => true,
                    'father_id'     => true,
                    'mother_id'     => true,
                    'guardian_id'   => true,
                    'visitor_note'  => true,
                    'lang'  => $lang
                ),$v->id);
                $get=array('action'=>'show','person'=>true,'persons_i18n'=>true,'health'=>true,'education'=>true,'work'=>true);

                if($sub->father_id != null){
                    $get['id'] = $sub->father_id;
                    $sub->father =\Common\Model\Person::fetch($get);
                }

                if($sub->mother_id != null) {
                    $get['id'] = $sub->mother_id;
                    $sub->mother = \Common\Model\Person::fetch($get);
                }
                if($sub->guardian_id != null) {
                    $sub->guardian =\Common\Model\Person::fetch(array('action'=>'show', 'id' => $sub->guardian_id,'person' =>true, 'persons_i18n' => true, 'contacts' => true,  'education' => true,
                        'work' => true, 'residence' => true, 'banks' => true));
                }

                $sub->sponsor_id=$v->sponsor_id;
                $sub->sponsor_name=$v->sponsor_name;
                $return[]=$sub;
            }

            return $return;
        }

    }

    public static function getInactive($ids,$payment_id,$sponsor_id,$organization_id)
    {

        $Final=[];
        $count=0;

        $return =\DB::table('char_sponsorship_cases')
            ->join('char_cases', function($q) use($organization_id) {
                $q->on('char_cases.id','=','char_sponsorship_cases.case_id');
                $q->where('char_cases.organization_id','=',$organization_id);
            })
            ->leftjoin('char_guardians', function($q) {
                $q->on('char_cases.person_id','=','char_guardians.individual_id');
                $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                $q->where('char_guardians.status','=',1);
            })
            ->where('char_sponsorship_cases.status', 3)
            ->where('char_sponsorship_cases.sponsor_id', $sponsor_id)
            ->where('char_sponsorship_cases.sponsor_number','!=', "")
            ->whereNotNull('char_sponsorship_cases.sponsor_number')
            ->whereNotIn('char_sponsorship_cases.case_id',$ids)
            ->whereNotIn('char_sponsorship_cases.sponsor_number', function($query)use($payment_id){
                $query->
                select('char_payments_cases.sponsor_number')
                    ->from('char_payments_cases')
                    ->where('char_payments_cases.payment_id', $payment_id);
            })
            ->whereNotIn('char_sponsorship_cases.case_id', function($query)use($payment_id){
                $query->
                select('char_payments_cases.case_id')
                    ->from('char_payments_cases')
                    ->where('char_payments_cases.payment_id', $payment_id);
            })
            ->selectRaw('char_cases.id as case_id ,
                                  char_guardians.guardian_id,
                                  char_sponsorship_cases.sponsor_number,
                                  char_sponsorship_cases.sponsorship_id')
            ->groupBy('char_sponsorship_cases.sponsor_number','char_sponsorship_cases.case_id')
            ->get();

        if(sizeof($return)!=0){
            foreach ($return as $key => $value) {
                PaymentsCases::Create([ 'payment_id' => $payment_id, 'case_id' => $value->case_id, 'sponsor_number' => $value->sponsor_number, 'status' => 3 ,'guardian_id'=>$value->guardian_id ]);
            }

            $count = $count+ sizeof($return);
        }


        $updated =\DB::table('char_sponsorship_cases')
            ->join('char_cases', function($q) use($organization_id) {
                $q->on('char_cases.id','=','char_sponsorship_cases.case_id');
                $q->where('char_cases.organization_id','=',$organization_id);
            })
            ->where('char_sponsorship_cases.status', 3)
            ->where('char_sponsorship_cases.sponsor_id', $sponsor_id)
            ->where('char_sponsorship_cases.sponsor_number','!=', "")
            ->whereNotIn('char_sponsorship_cases.case_id',$ids)
            ->whereIn('char_sponsorship_cases.sponsor_number', function($query)use($payment_id){
                $query->
                select('char_payments_cases.sponsor_number')
                    ->from('char_payments_cases')
                    ->where('char_payments_cases.payment_id', $payment_id);
            })
            ->whereIn('char_sponsorship_cases.case_id', function($query)use($payment_id){
                $query->
                select('char_payments_cases.case_id')
                    ->from('char_payments_cases')
                    ->where('char_payments_cases.payment_id', $payment_id);
            })
            ->selectRaw('char_cases.id as case_id ,char_sponsorship_cases.sponsor_number')->get();

        foreach ($updated as $key => $value) {
            PaymentsCases::where([ 'payment_id' => $payment_id, 'case_id' => $value->case_id, 'sponsor_number' => $value->sponsor_number])->update(['status' => 3]);
        }

        $count = $count+ sizeof($updated);

        return $count;

    }

    public static function getRecord($target,$id,$payment_id,$payment_bank,$payment_target,$exchange_type,$sponsor_id)
    {
        if($target=='sponsor_number'){
            $return=\DB::table('char_sponsorship_cases')
                ->leftjoin('char_cases', function($q){
                    $q->on('char_cases.id','=','char_sponsorship_cases.case_id');
                })
                ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
                ->leftjoin('char_guardians', function($q) {
                    $q->on('char_cases.person_id','=','char_guardians.individual_id');
                    $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                    $q->where('char_guardians.status','=',1);
                })
                ->leftjoin('char_payments_cases', function($q) use($payment_id) {
                    $q->on('char_payments_cases.case_id','=','char_sponsorship_cases.case_id');
                    $q->on('char_payments_cases.sponsor_number','=','char_sponsorship_cases.sponsor_number');
                    $q->where('char_payments_cases.payment_id','=',$payment_id);
                });

            if($payment_bank !=null && $exchange_type == 1){
//                if($payment_target ==1){
                    $return=$return
                        ->leftjoin('char_persons_banks as guardian_banks', function($q) use($payment_bank){
                            $q->on('guardian_banks.person_id','=','char_guardians.guardian_id');
                            $q->where('guardian_banks.bank_id','=',$payment_bank);
                        })
                        ->selectRaw('char_persons_banks.bank_id as bank_id,char_persons_banks.account_number as account_number') ;
//                }else{
//                    $return=$return
//                        ->leftjoin('char_persons_banks', function($q) use($payment_bank){
//                            $q->on('char_persons_banks.person_id','=','char_cases.person_id');
//                            $q->where('char_persons_banks.bank_id','=',$payment_bank);
//                        })
//                        ->selectRaw('char_persons_banks.bank_id as bank_id,char_persons_banks.account_number as account_number') ;
//                }

                $return=$return
                    ->leftjoin('char_persons_banks', function($q) use($payment_bank){
                        $q->on('char_cases.person_id','=','char_persons_banks.person_id');
                        $q->where('char_persons_banks.bank_id','=',$payment_bank);
                    })
                    ->selectRaw('char_persons_banks.bank_id as bank_id,char_persons_banks.account_number as account_number')
                    ->selectRaw("CASE WHEN char_payments_cases.recipient is null THEN char_persons_banks.bank_id
                                      WHEN char_payments_cases.recipient =0 THEN char_persons_banks.bank_id
                                      Else guardian_banks.bank_id  END  AS bank_id,
                                 CASE WHEN char_payments_cases.recipient is null THEN char_persons_banks.account_number
                                      WHEN char_payments_cases.recipient =0 THEN char_persons_banks.account_number
                                      Else guardian_banks.account_number  END  AS account_number") ;

            }

//            if($payment_target ==0){
//                $return=$return
//                    ->leftjoin('char_payments_recipient', function($q) use($payment_id) {
//                        $q->on('char_payments_recipient.person_id','=','char_cases.person_id');
//                        $q->where('char_payments_recipient.payment_id','=',$payment_id);
//                    })->selectRaw('char_payments_recipient.person_id as recipient_id');
//
//            }else{
//                $return=$return
//                    ->leftjoin('char_payments_recipient', function($q) use($payment_id) {
//                        $q->on('char_payments_recipient.person_id','=','char_cases.person_id');
//                        $q->where('char_payments_recipient.payment_id','=',$payment_id);
//                    })
//                    ->selectRaw('char_payments_recipient.person_id as recipient_id');
//            }

            return   $return->where(function ($q) use($sponsor_id,$id){
                          $q->where('char_sponsorship_cases.sponsor_number','=',$id);
                          $q->where('char_sponsorship_cases.sponsor_id','=',$sponsor_id);
                     })
                     ->selectRaw("get_payments_cases_count(char_cases.id,'$payment_id',char_sponsorship_cases.sponsor_number) as payments,
                           CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                                                char_payments_cases.case_id as payment_case_id ,
                                                char_cases.id as case_id ,
                                                char_cases.person_id ,
                                                char_payments_cases.payment_id,
                                                CASE WHEN char_payments_cases.recipient is null THEN 0 Else char_payments_cases.recipient  END  AS recipient,
                                                char_guardians.guardian_id,
                                                char_sponsorship_cases.sponsor_number,
                                                char_sponsorship_cases.sponsorship_id")
                ->get();
        }else{

            $return=\DB::table('char_sponsorship_cases')
                ->join('char_cases', function($q){
                    $q->on('char_cases.id','=','char_sponsorship_cases.case_id');
                })
                ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
                ->leftjoin('char_guardians', function($q) {
                    $q->on('char_cases.person_id','=','char_guardians.individual_id');
                    $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                    $q->where('char_guardians.status','=',1);
                })
                ->leftjoin('char_payments_cases', function($q) use($payment_id) {
                    $q->on('char_payments_cases.case_id','=','char_sponsorship_cases.case_id');
                    $q->on('char_sponsorship_cases.sponsor_number','=','char_payments_cases.sponsor_number');
                    $q->where('char_payments_cases.payment_id','=',$payment_id);
                });

            if($payment_bank !=null && $exchange_type == 1){
//                if($payment_target ==1){
//                    $return=$return
//                        ->leftjoin('char_persons_banks', function($q) use($payment_bank){
//                            $q->on('char_guardians.guardian_id','=','char_persons_banks.person_id');
//                            $q->where('char_persons_banks.bank_id','=',$payment_bank);
//                        })
//                        ->selectRaw('char_persons_banks.bank_id as bank_id,char_persons_banks.account_number as account_number') ;
//                }else{
//                    $return=$return
//                        ->leftjoin('char_persons_banks', function($q) use($payment_bank){
//                            $q->on('char_cases.person_id','=','char_persons_banks.person_id');
//                            $q->where('char_persons_banks.bank_id','=',$payment_bank);
//                        })
//                        ->selectRaw('char_persons_banks.bank_id as bank_id,char_persons_banks.account_number as account_number') ;
//                }

                $return=$return
                    ->leftjoin('char_persons_banks', function($q) use($payment_bank){
                        $q->on('char_cases.person_id','=','char_persons_banks.person_id');
                        $q->where('char_persons_banks.bank_id','=',$payment_bank);
                    })
                    ->leftjoin('char_persons_banks as guardian_banks', function($q) use($payment_bank){
                        $q->on('char_guardians.guardian_id','=','guardian_banks.person_id');
                        $q->where('guardian_banks.bank_id','=',$payment_bank);
                    })
                    ->selectRaw("CASE WHEN char_payments_cases.recipient is null THEN char_persons_banks.bank_id
                                      WHEN char_payments_cases.recipient =0 THEN char_persons_banks.bank_id
                                      Else guardian_banks.bank_id  END  AS bank_id,
                                 CASE WHEN char_payments_cases.recipient is null THEN char_persons_banks.account_number
                                      WHEN char_payments_cases.recipient =0 THEN char_persons_banks.account_number
                                      Else guardian_banks.account_number  END  AS account_number") ;
            }
//
//            if($payment_target ==0){
//                $return=$return
//                    ->leftjoin('char_payments_recipient', function($q) use($payment_id) {
//                        $q->on('char_cases.person_id','=','char_payments_recipient.person_id');
//                        $q->where('char_payments_recipient.payment_id','=',$payment_id);
//                    })->selectRaw('char_payments_recipient.person_id as recipient_id');
//
//            }else{
//                $return=$return
//                    ->leftjoin('char_payments_recipient', function($q) use($payment_id) {
//                        $q->on('char_cases.person_id','=','char_payments_recipient.person_id');
//                        $q->where('char_payments_recipient.payment_id','=',$payment_id);
//                    })
//                    ->selectRaw('char_payments_recipient.person_id as recipient_id');
//            }

//
            return  $return->where('char_persons.id','=',$id)
                ->whereNotNull('char_sponsorship_cases.sponsor_number')
                ->selectRaw(" get_payments_cases_count(char_cases.id,'$payment_id',char_sponsorship_cases.sponsor_number) as payments,
                             CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                                                            char_payments_cases.case_id as payment_case_id ,
                                                            char_cases.id as case_id ,
                                                            char_cases.person_id ,
                                                            char_payments_cases.payment_id,
                                                            CASE WHEN char_payments_cases.recipient is null THEN 0 Else char_payments_cases.recipient  END  AS recipient,
                                                            char_guardians.guardian_id,
                                                            char_sponsorship_cases.sponsor_number,
                                                            char_sponsorship_cases.sponsorship_id")
                ->get();
        }

    }

    public static function getPerson($page,$sponsor_id,$filters){

        $language_id =  \App\Http\Helpers::getLocale();

        $char_sponsorship_cases=['status','sponsor_id'];
        $char_cases=['category_id','organization_id'];
        $char_persons = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number', 'gender',
            'marital_status_id','birth_place','nationality','refugee','unrwa_card_number','location_id','country','governarate','city',
            'street_address','monthly_income','mosques_id',"adscountry_id","is_qualified","qualified_card_number",
            "adsdistrict_id", "adsmosques_id", "card_type", "adsneighborhood_id", "adsregion_id",
            "adssquare_id" ,'actual_monthly_income', 'receivables',  'receivables_sk_amount',
            'has_commercial_records','active_commercial_records', 'gov_commercial_records_details'];
        $char_residence = ['property_type_id', 'roof_material_id','residence_condition','indoor_condition',
            'house_condition','habitable','rent_value','need_repair','repair_notes'];
        $char_persons_i18n = ['en_first_name', 'en_second_name', 'en_third_name', 'en_last_name'];
        $char_persons_work = ['working','can_work','work_status_id','work_reason_id','work_job_id','work_wage_id','work_location',
            'gov_work_details'];
        $char_persons_health = ['condition','disease_id','has_health_insurance', 'health_insurance_number', 'health_insurance_type',
            'has_device', 'used_device_name', 'gov_health_details'];
        $char_persons_education = ['grade', 'authority','type','stage','level','year','school','points'];
        $char_persons_islamic_commitment = ['prayer','quran_parts', 'quran_chapters','quran_center'];
        $reconstructions=['promised','reconstructions_organization_name'];
        $secondary_mobile=['secondary_mobile'];
        $primary_mobile=['primary_mobile'];
        $phone=['phone'];
        $char_organization_persons_banks = ['bank_id'];
        $char_persons_banks = ['bank_id','branch_name','account_number','account_owner'];
        $char_persons_father = ['father_id_card_number','father_card_type','father_death_cause_id','father_first_name', 'father_monthly_income', 'father_second_name', 'father_third_name', 'father_last_name','father_alive'];
        $char_persons_mother = ['mother_id_card_number','mother_card_type','mother_death_cause_id','mother_first_name','mother_marital_status_id' ,'mother_second_name','mother_monthly_income', 'mother_third_name','mother_nationality', 'mother_last_name','mother_alive'];
        $char_persons_guardian=['guardian_id_card_number','guardian_card_type','guardian_first_name', 'guardian_second_name','guardian_third_name',
            'guardian_gender','guardian_marital_status_id','guardian_location_id','guardian_country','guardian_governarate','guardian_city',
            'guardian_monthly_income','guardian_birth_place','guardian_nationality', 'guardian_last_name','guardian_mosques_id'];
        $guardian_residence = ['guardian_property_type_id', 'guardian_residence_condition','guardian_indoor_condition','guardian_roof_material_id'];
        $mother_health = ['mother_condition','mother_disease_id'];
        $father_health = ['father_condition','father_disease_id'];
        $father_work = ['father_working','father_work_job_id','father_work_location'];
        $mother_work = ['mother_working','mother_work_job_id','mother_work_location'];
        $guardian_work = ['guardian_working','guardian_work_job_id','guardian_work_location'];
        $father_education = ['father_grade', 'father_authority','father_type','father_stage','father_level','father_year','father_school','father_points'];
        $mother_education = ['mother_grade', 'mother_authority','mother_type','mother_stage','mother_level','mother_year','mother_school','mother_points'];
        $guardian_education = ['guardian_grade', 'guardian_authority','guardian_type','guardian_stage','guardian_level','guardian_year','guardian_school','guardian_points'];
        $guardian_cont1=['guardian_primary_mobile'];
        $guardian_cont2=['guardian_secondary_mobile'];
        $guardian_cont3=['guardian_phone'];


        $char_persons_health_cnt =
        $char_persons_work_cnt  =
        $char_persons_education_cnt =
        $char_persons_i18n_cnt  =
        $char_residence_cnt =
        $char_persons_work_cnt =
        $primary_num_cnt = $secondary_num_cnt =  $phone_num_cnt = $reconstruct_cnt=
        $char_persons_islamic_commitment_cnt =
        $char_organization_persons_banks_cnt=
        $char_persons_banks_cnt =
        $char_persons_father_cnt =
        $char_persons_mother_cnt =
        $char_persons_guardian_cnt =
        $guardian_residence_cnt =
        $mother_health_cnt =
        $father_health_cnt =
        $father_work_cnt    =
        $mother_work_cnt  =
        $guardian_work_cnt =
        $father_education_cnt =
        $mother_education_cnt =
        $guardian_education_cnt =
        $guardian_cont1_cnt =
        $guardian_cont2_cnt =
        $guardian_cont3_cnt = 0;

        $condition=[];
        foreach ($filters as $key => $value) {
            if(in_array($key, $char_cases)) {
                if ( $value != "" ) {
                    if($key =='case_status'){
                        $data = ['char_cases.status'=> $value];
                    }else{
                        $data = ['char_cases.' . $key => $value];
                    }
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $reconstructions)) {
                if ( $value != "" ) {
                    if($key =='reconstructions_organization_name'){
                        $data = ['char_reconstructions.organization_name'=> $value];
                    }else{
                        $data = ['char_reconstructions.' . $key => $value];
                    }
                    $reconstruct_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons)) {
                if ( $value != "" ) {
                    $data = ['char_persons.' . $key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_residence)) {
                if ( $value != "" ) {
                    $data = ['char_residence.' . $key =>  $value];
                    $char_residence_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_i18n)) {
                if ( $value != "" ) {
                    $data = ['char_persons_i18n.' . substr($key,3)=>$value ];
                    $char_persons_i18n_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_work)) {

                if ( $value != "" ) {
                    $data = ['char_persons_work.' . $key => $value];
                    $char_persons_work_cnt ++;
                    array_push($condition, $data);
                }

            }
            if(in_array($key, $char_persons_health)) {
                if ( $value != "" ) {
                    $data = ['char_persons_health.' . $key =>  $value];
                    $char_persons_health_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_education)) {
                if ( $value != "" ) {
                    $data = ['char_persons_education.' . $key => $value];
                    $char_persons_education_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_islamic_commitment)) {
                if ( $value != "" ) {
                    $data = ['char_persons_islamic_commitment.' . $key => $value];
                    $char_persons_islamic_commitment_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $phone)) {
                if ( $value != "" ) {
                    $data = ['phone_num.contact_value' => $value]; $phone_num_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $primary_mobile)) {
                if ( $value != "" ) {
                    $data = ['primary_num.contact_value' => $value];
                    $primary_num_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $secondary_mobile)) {
                if ( $value != "" ) {
                    $data = ['secondary_num.contact_value' => $value];
                    $secondary_num_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_organization_persons_banks)) {
                if ( $value != "" ) {
                    $data = ['char_organization_persons_banks.'. $key => $value];
                    $char_organization_persons_banks_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_banks)) {
                if ( $value != "" ) {
                    $data = ['char_persons_banks.'. $key => $value];
                    $char_persons_banks_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_father)) {
                if ( $value != "" ) {
                    if($key =="father_alive"){
                        $data = ['f.death_date'  => $value];
                    }else{
                        $data = ['f.' . substr($key, 7) => $value];
                    }
                    $char_persons_father_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_mother)) {
                if ( $value != "" ) {
                    if($key =="mother_alive"){
                        $data = ['m.death_date'  => $value];
                    }else{
                        $data = ['m.' . substr($key, 7) => $value];
                    }
                    $char_persons_mother_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_guardian)) {
                if ( $value != "" ) {
                    $data = ['g.' . substr($key, 9) => $value];
                    $char_persons_guardian_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_residence)) {
                if ( $value != "" ) {
                    $data = ['guardian_residence.' . substr($key, 9) => $value];
                    $guardian_residence_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key,$mother_health)) {
                if ( $value != "" ) {
                    $data = ['mother_health.' . substr($key, 7) => $value];
                    $mother_health_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key,$father_health)) {
                if ( $value != "" ) {
                    $data = ['father_health.' . substr($key, 7) => $value];
                    $father_health_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $father_work)) {

                if ( $value != "" ) {
                    $father_work_cnt   ++;
                    $data = ['father_work.' . substr($key, 7) => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $mother_work)) {

                if ( $value != "" ) {
                    $data = ['mother_work.' . substr($key, 7) => $value];
                    $mother_work_cnt ++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_work)) {

                if ( $value != "" ) {
                    $data = ['guardian_work.' . substr($key, 9) => $value];
                    $guardian_work_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $father_education)) {

                if ( $value != "" ) {
                    $data = ['father_education.' . substr($key, 7) => $value];
                    $father_education_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $mother_education)) {

                if ( $value != "" ) {
                    $data = ['mother_education.' . substr($key, 7) => $value];
                    $mother_education_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_education)) {

                if ( $value != "" ) {
                    $data = ['guardian_education.' . substr($key, 9) => $value];
                    $guardian_education_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_cont1)) {
                if ( $value != "" ) {
                    $data = ['guardian_cont1.contact_value' => $value];
                    $guardian_cont1_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_cont2)) {
                if ( $value != "" ) {
                    $data = ['guardian_cont2.contact_value' => $value];
                    $guardian_cont2_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_cont3)) {
                if ( $value != "" ) {
                    $data = ['guardian_cont3.contact_value' => $value];
                    $guardian_cont3_cnt++;
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $char_sponsorship_cases)) {
                if ( $value != "" ) {
                    $data = ['char_sponsorship_cases.'.$key => $value];
                    array_push($condition, $data);
                }
            }


        }


        $language_id =  \App\Http\Helpers::getLocale();

        $query=
            \DB::table('char_sponsorship_cases')
                ->join('char_cases', 'char_sponsorship_cases.case_id', '=', 'char_cases.id')
                ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
                ->join('char_organizations as org','org.id',  '=', 'char_cases.organization_id')
                ->join('char_categories as ca','ca.id','=','char_cases.category_id')
                ->join('char_marital_status_i18n', function($q) use ($language_id){
                    $q->on('char_marital_status_i18n.marital_status_id', '=', 'char_persons.marital_status_id');
                    $q->where('char_marital_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_persons AS f','f.id','=','char_persons.father_id')
                ->leftjoin('char_persons AS m','m.id','=','char_persons.mother_id')
                ->leftjoin('char_guardians', function($q) use ($language_id){
                    $q->on('char_cases.person_id','=','char_guardians.individual_id');
                    $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                    $q->where('char_guardians.status',1);
                })
                ->leftjoin('char_persons AS g','g.id','=','char_guardians.guardian_id')
                ->leftjoin('char_locations_i18n As L2', function($q) use ($language_id){
                    $q->on('L2.location_id', '=', 'char_persons.city');
                    $q->where('L2.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L3', function($q) use ($language_id) {
                    $q->on('L3.location_id', '=', 'char_persons.governarate');
                    $q->where('L3.language_id',$language_id);
                })
                ->whereIn('char_sponsorship_cases.status',[3,4,5])
                ->where('char_sponsorship_cases.sponsor_id',$sponsor_id)
;

        if (count($condition) != 0) {
            $query->where(function ($q) use ($condition) {
                    $names = ['char_persons.street_address','char_persons.first_name', 'char_persons.second_name',
                        'char_persons.third_name', 'char_persons.last_name',
                        'char_persons_i18n.first_name', 'char_persons_i18n.second_name', 'char_persons_i18n.third_name',
                        'char_persons_i18n.last_name','char_residence.repair_notes',
                        'char_cases.visitor','char_cases.notes','char_cases.visitor_card','char_cases.visitor_opinion',
                        'char_persons_work.work_location','char_persons_banks.account_owner',
                        'm.first_name', 'm.second_name', 'm.third_name', 'm.last_name',
                        'g.first_name', 'g.second_name', 'g.third_name', 'g.last_name',
                        'char_persons_health.health_insurance_number','char_persons_health.used_device_name',
                        'char_persons_health.gov_health_details',
                        'char_persons.gov_commercial_records_details','char_residence.repair_notes',
                        'char_persons_work.work_location', 'char_persons_work.gov_work_details' ,
                        'char_persons_health.used_device_name', 'char_persons_health.gov_health_details'
                    ];


                    $live = ['m.death_date','f.death_date'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $names)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else if(in_array($key, $live)){
                                if($value == 0){
                                    $q->where($key, '=', null);
                                }else{
                                    $q->where($key, '!=', null);
                                }
                            }else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });

        }
        
        if((isset($filters['birthday_to']) && $filters['birthday_to'] !=null) || (isset($filters['birthday_from']) && $filters['birthday_from'] !=null)){

            $birthday_to=null;
            $birthday_from=null;

            if(isset($filters['birthday_to']) && $filters['birthday_to'] !=null){
                $birthday_to=date('Y-m-d',strtotime($filters['birthday_to']));
            }
            if(isset($filters['birthday_from']) && $filters['birthday_from'] !=null){
                $birthday_from=date('Y-m-d',strtotime($filters['birthday_from']));
            }
            if($birthday_from != null && $birthday_to != null) {
                $query->whereBetween( 'char_persons.birthday', [ $birthday_from, $birthday_to]);
            }elseif($birthday_from != null && $birthday_to == null) {
                $query->whereDate('char_persons.birthday', '>=', $birthday_from);
            }elseif($birthday_from == null && $birthday_to != null) {
                $query->whereDate('char_persons.birthday', '<=', $birthday_to);
            }
        }
        
        $to_sponsorship_date=null;
        $from_sponsorship_date=null;
        $to_guaranteed_date=null;
        $from_guaranteed_date=null;
        if(isset($filters['to_sponsorship_date']) && $filters['to_sponsorship_date'] !=null){
            $to_sponsorship_date=date('Y-m-d',strtotime($filters['to_sponsorship_date']));
        }
        if(isset($filters['from_sponsorship_date']) && $filters['from_sponsorship_date'] !=null){
            $from_sponsorship_date=date('Y-m-d',strtotime($filters['from_sponsorship_date']));
        }
        if(isset($filters['to_guaranteed_date']) && $filters['to_guaranteed_date'] !=null){
            $to_guaranteed_date=date('Y-m-d',strtotime($filters['to_guaranteed_date']));
        }
        if(isset($filters['from_guaranteed_date']) && $filters['from_guaranteed_date'] !=null){
            $from_guaranteed_date=date('Y-m-d',strtotime($filters['from_guaranteed_date']));
        }

        if($from_sponsorship_date != null && $to_sponsorship_date != null) {
            $query = $query->whereBetween( 'char_sponsorship_cases.created_at', [ $from_sponsorship_date, $to_sponsorship_date]);
        }elseif($from_sponsorship_date != null && $to_sponsorship_date == null) {
            $query = $query->whereDate('char_sponsorship_cases.created_at', '>=', $from_sponsorship_date);
        }elseif($from_sponsorship_date == null && $to_sponsorship_date != null) {
            $query = $query->whereDate('char_sponsorship_cases.created_at', '<=', $to_sponsorship_date);
        }

        if($from_guaranteed_date != null && $to_guaranteed_date != null) {
            $query = $query->whereBetween( 'char_sponsorship_cases.sponsorship_date', [ $from_guaranteed_date, $to_guaranteed_date]);
        }elseif($from_guaranteed_date != null && $to_guaranteed_date == null) {
            $query = $query->whereDate('char_sponsorship_cases.sponsorship_date', '>=', $from_guaranteed_date);
        }elseif($from_guaranteed_date == null && $to_guaranteed_date != null) {
            $query = $query->whereDate('char_sponsorship_cases.sponsorship_date', '<=', $to_guaranteed_date);
        }
        
        if((isset($filters['mother_birthday_to']) && $filters['mother_birthday_to'] !=null) ||
                (isset($filters['mother_birthday_from']) && $filters['mother_birthday_from'] !=null)) {

                $mother_birthday_to=null;
                $mother_birthday_from=null;

                if(isset($filters['mother_birthday_to']) && $filters['mother_birthday_to'] !=null){
                    $mother_birthday_to=date('Y-m-d',strtotime($filters['mother_birthday_to']));
                }
                if(isset($filters['mother_birthday_from']) && $filters['mother_birthday_from'] !=null){
                    $mother_birthday_from=date('Y-m-d',strtotime($filters['mother_birthday_from']));
                }

                if($mother_birthday_from != null && $mother_birthday_to != null) {
                    $query->whereBetween( 'm.birthday', [ $mother_birthday_from, $mother_birthday_to]);
                }elseif($mother_birthday_from != null && $mother_birthday_to == null) {
                    $query->whereDate('m.birthday', '>=', $mother_birthday_from);
                }elseif($mother_birthday_from == null && $mother_birthday_to != null) {
                    $query->whereDate('m.birthday', '<=', $mother_birthday_to);
                }

            }

            if((isset($filters['mother_death_date_to']) && $filters['mother_death_date_to'] !=null) ||
                (isset($filters['mother_death_date_from']) && $filters['mother_death_date_from'] !=null)) {
                $mother_death_date_to=null;
                $mother_death_date_from=null;

                if(isset($filters['mother_death_date_to']) && $filters['mother_death_date_to'] !=null){
                    $mother_death_date_to=date('Y-m-d',strtotime($filters['mother_death_date_to']));
                }
                if(isset($filters['mother_death_date_from']) && $filters['mother_death_date_from'] !=null){
                    $mother_death_date_from=date('Y-m-d',strtotime($filters['mother_death_date_from']));
                }
                if($mother_death_date_from != null && $mother_death_date_to != null) {
                    $query->whereBetween( 'm.death_date', [ $mother_death_date_from, $mother_death_date_to]);
                }elseif($mother_death_date_from != null && $mother_death_date_to == null) {
                    $query->whereDate('m.death_date', '>=', $mother_death_date_from);
                }elseif($mother_death_date_from == null && $mother_death_date_to != null) {
                    $query->whereDate('m.death_date', '<=', $mother_death_date_to);
                }
            }

            if((isset($filters['father_death_date_to']) && $filters['father_death_date_to'] !=null) ||
                (isset($filters['father_death_date_from']) && $filters['father_death_date_from'] !=null)) {

                $father_death_date_to=null;
                $father_death_date_from=null;

                if(isset($filters['father_death_date_to']) && $filters['father_death_date_to'] !=null){
                    $father_death_date_to=date('Y-m-d',strtotime($filters['father_death_date_to']));
                }
                if(isset($filters['father_death_date_from']) && $filters['father_death_date_from'] !=null){
                    $father_death_date_from=date('Y-m-d',strtotime($filters['father_death_date_from']));
                }
                if($father_death_date_from != null && $father_death_date_to != null) {
                    $query->whereBetween( 'f.death_date', [ $father_death_date_from, $father_death_date_to]);
                }elseif($father_death_date_from != null && $father_death_date_to == null) {
                    $query->whereDate('f.death_date', '>=', $father_death_date_from);
                }elseif($father_death_date_from == null && $father_death_date_to != null) {
                    $query->whereDate('f.death_date', '<=', $father_death_date_to);
                }
            }

            if((isset($filters['father_birthday_to']) && $filters['father_birthday_to'] !=null)||
                (isset($filters['father_birthday_from']) && $filters['father_birthday_from'] !=null)){

                $father_birthday_to=null;
                $father_birthday_from=null;

                if(isset($filters['father_birthday_to']) && $filters['father_birthday_to'] !=null){
                    $father_birthday_to=date('Y-m-d',strtotime($filters['father_birthday_to']));
                }
                if(isset($filters['father_birthday_from']) && $filters['father_birthday_from'] !=null){
                    $father_birthday_from=date('Y-m-d',strtotime($filters['father_birthday_from']));
                }

                if($father_birthday_from != null && $father_birthday_to != null) {
                    $query->whereBetween( 'f.birthday', [ $father_birthday_from, $father_birthday_to]);
                }
                elseif($father_birthday_from != null && $father_birthday_to == null) {
                    $query->whereDate('f.birthday', '>=', $father_birthday_from);
                }
                elseif($father_birthday_from == null && $father_birthday_to != null) {
                    $query->whereDate('f.birthday', '<=', $father_birthday_to);
                }

            }
        

        $nominate =  trans('sponsorship::application.nominate');
        $sent =  trans('sponsorship::application.sent');
        $guaranteed =  trans('sponsorship::application.guaranteed');
        $stopped =  trans('sponsorship::application.stopped');
        $paused =  trans('sponsorship::application.paused');
        
        $active = trans('common::application.active');
        $inactive = trans('common::application.inactive');

        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $refugee = trans('common::application.refugee');
        $citizen = trans('common::application.citizen');

        $not_exist = trans('common::application.not_exist');
        $exist = trans('common::application.exist');

        $not_work = trans('common::application.not_work');
        $work = trans('common::application.work');


        $qualified = trans('common::application.qualified');
        $not_qualified = trans('common::application.not qualified');

        $is_alive = trans('common::application.is_alive');
        $deceased = trans('common::application.deceased');

        $vocational = trans('common::application.vocational');
        $academic = trans('common::application.academic');

        $perfect = trans('common::application.perfect');
        $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
        $special_needs = trans('common::application.special_needs');

        $excellent = trans('common::application.excellent');
        $acceptable = trans('common::application.acceptable');

        $good = trans('common::application.good');
        $very_good = trans('common::application.very_good');
        $weak = trans('common::application.weak');
        $very_bad_condition = trans('common::application.very_bad_condition');
        $bad_condition = trans('common::application.bad_condition');

        $the_first  = trans('common::application.the_first') ;
        $the_second = trans('common::application.the_second') ;
        $the_third  = trans('common::application.the_third') ;
        $the_fourth = trans('common::application.the_fourth') ;
        $the_Fifth  = trans('common::application.the_Fifth');
        $the_Sixth  = trans('common::application.the_Sixth');
        $the_Seventh = trans('common::application.the_Seventh');
        $the_Eighth= trans('common::application.the_Eighth');
        $the_Ninth = trans('common::application.the_Ninth');
        $the_tenth = trans('common::application.the_tenth');
        $the_eleventh = trans('common::application.the_eleventh');
        $the_twelveth= trans('common::application.the_twelveth');

        $special_studies= trans('common::application.special_studies');
        $vocational_training= trans('common::application.vocational_training');
        $first_year_of_university= trans('common::application.first_year_of_university');
        $second_year_of_university= trans('common::application.second_year_of_university');
        $third_year_of_university= trans('common::application.third_year_of_university');
        $fourth_year_of_university= trans('common::application.fourth_year_of_university');
        $fifth_year_of_university= trans('common::application.fifth_year_of_university');

        $regularly = trans('common::application.regularly');
        $does_not_pray = trans('common::application.does_not_pray');
        $sometimes = trans('common::application.sometimes');

        if($filters['action'] =='export') {

       if(isset($filters['persons'])){
            if(sizeof($filters['persons']) > 0 ){
               $query->whereIn('char_sponsorship_cases.id',$filters['persons']);
            } 
        }
            $query->leftjoin('char_persons_health','char_persons_health.person_id', '=', 'char_persons.id')
                ->leftjoin('char_diseases_i18n', function($q) use ($language_id){
                    $q->on('char_diseases_i18n.disease_id', '=', 'char_persons_health.disease_id');
                    $q->where('char_diseases_i18n.language_id',$language_id);
                });


                $query ->leftjoin('char_locations_i18n As mosques_name', function($q) use ($language_id){
                    $q->on('mosques_name.location_id', '=', 'char_persons.mosques_id');
                    $q->where('mosques_name.language_id',$language_id);
                })
                    ->leftjoin('char_locations_i18n As L1', function($q) use ($language_id){
                        $q->on('L1.location_id', '=', 'char_persons.location_id');
                        $q->where('L1.language_id',$language_id);
                    })
                    ->leftjoin('char_locations_i18n As L4', function($q) use ($language_id){
                        $q->on('L4.location_id', '=', 'char_persons.country');
                        $q->where('L4.language_id',$language_id);
                    })
                    ->leftjoin('char_persons_islamic_commitment','char_persons_islamic_commitment.person_id', '=', 'char_persons.id')
                    ->leftjoin('char_persons_education','char_persons_education.person_id', '=', 'char_persons.id')
                    ->leftjoin('char_edu_stages_i18n as char_edu_stages', function($q) use ($language_id){
                        $q->on('char_edu_stages.edu_stage_id', '=', 'char_persons_education.stage');
                        $q->where('char_edu_stages.language_id',$language_id);
                    })
                    ->leftjoin('char_edu_authorities_i18n as char_edu_authorities', function($q) use ($language_id){
                        $q->on('char_edu_authorities.edu_authority_id', '=', 'char_persons_education.authority');
                        $q->where('char_edu_authorities.language_id',$language_id);
                    })
                    ->leftjoin('char_edu_degrees_i18n as char_edu_degrees', function($q) use ($language_id){
                        $q->on('char_edu_degrees.edu_degree_id', '=', 'char_persons_education.degree');
                        $q->where('char_edu_degrees.language_id',$language_id);
                    })
                    ->leftjoin('char_death_causes_i18n as father_death_causes', function($q) use ($language_id){
                        $q->on('father_death_causes.death_cause_id', '=', 'f.death_cause_id');
                        $q->where('father_death_causes.language_id',$language_id);
                    })
                    ->leftjoin('char_persons_work as father_work','father_work.person_id','=','f.id')
                    ->leftjoin('char_work_jobs_i18n as father_work_jobs', function($q) use ($language_id){
                        $q->on('father_work_jobs.work_job_id', '=', 'father_work.work_job_id');
                        $q->where('father_work_jobs.language_id',$language_id);
                    })
                    ->leftjoin('char_persons_education as father_education','father_education.person_id','=','f.id')
                    ->leftjoin('char_edu_degrees_i18n as father_edu_degrees', function($q) use ($language_id){
                        $q->on('father_edu_degrees.edu_degree_id', '=', 'father_education.degree');
                        $q->where('father_edu_degrees.language_id',$language_id);
                    })
                    ->leftjoin('char_persons_health as father_health','father_health.person_id','=','f.id')
                    ->leftjoin('char_diseases_i18n as father_diseases', function($q) use ($language_id){
                        $q->on('father_diseases.disease_id', '=', 'father_health.disease_id');
                        $q->where('father_diseases.language_id',$language_id);
                    })
                    ->leftjoin('char_persons_work as mother_work','mother_work.person_id','=','m.id')
                    ->leftjoin('char_work_jobs_i18n as mother_work_jobs', function($q) use ($language_id){
                        $q->on('mother_work_jobs.work_job_id', '=', 'mother_work.work_job_id');
                        $q->where('mother_work_jobs.language_id',$language_id);
                    })
                    ->leftjoin('char_persons_education as mother_education','mother_education.person_id','=','m.id')
                    ->leftjoin('char_edu_stages_i18n as mother_edu_stages', function($q) use ($language_id){
                        $q->on('mother_edu_stages.edu_stage_id', '=', 'mother_education.stage');
                        $q->where('mother_edu_stages.language_id',$language_id);
                    })
                    ->leftjoin('char_marital_status_i18n as mother_marital_status', function($q) use ($language_id){
                        $q->on('mother_marital_status.marital_status_id', '=', 'm.marital_status_id');
                        $q->where('mother_marital_status.language_id',$language_id);
                    })
                    ->leftjoin('char_persons_health as mother_health','mother_health.person_id','=','m.id')
                    ->leftjoin('char_diseases_i18n as mother_diseases', function($q) use ($language_id){
                        $q->on('mother_diseases.disease_id', '=', 'mother_health.disease_id');
                        $q->where('mother_diseases.language_id',$language_id);
                    })
                    ->leftjoin('char_residence as guardian_residence','guardian_residence.person_id', '=', 'g.id')
                    ->leftjoin('char_property_types_i18n as guardian_property_types', function($q) use ($language_id){
                        $q->on('guardian_property_types.property_type_id', '=', 'guardian_residence.property_type_id');
                        $q->where('guardian_property_types.language_id',$language_id);
                    })
                    ->leftjoin('char_roof_materials_i18n as guardian_roof_materials', function($q) use ($language_id){
                        $q->on('guardian_roof_materials.roof_material_id', '=', 'guardian_residence.roof_material_id');
                        $q->where('guardian_roof_materials.language_id',$language_id);
                    })
                    ->leftjoin('char_furniture_status_i18n as guardian_furniture_status_i18n', function($q) use ($language_id){
                        $q->on('guardian_furniture_status_i18n.furniture_status_id', '=', 'guardian_residence.indoor_condition');
                        $q->where('guardian_furniture_status_i18n.language_id',$language_id);
                    })
                    ->leftjoin('char_house_status_i18n as guardian_house_status_i18n', function($q) use ($language_id){
                        $q->on('guardian_house_status_i18n.house_status_id', '=', 'guardian_residence.residence_condition');
                        $q->where('guardian_house_status_i18n.language_id',$language_id);
                    })
                ->leftjoin('char_locations_i18n As L', function($q) use ($language_id){
                    $q->on('L.location_id', '=', 'char_persons.birth_place');
                    $q->where('L.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L0', function($q) use ($language_id){
                    $q->on('L0.location_id', '=', 'char_persons.nationality');
                    $q->where('L0.language_id',$language_id);
                })
                ->leftjoin('char_persons_i18n', 'char_persons_i18n.person_id', '=', 'char_cases.person_id')
                ->leftjoin('char_residence', 'char_residence.person_id', '=', 'char_persons.id')
                ->leftjoin('char_property_types_i18n', function($q) use ($language_id){
                    $q->on('char_property_types_i18n.property_type_id', '=', 'char_residence.property_type_id');
                    $q->where('char_property_types_i18n.language_id',$language_id);
                })
                ->leftjoin('char_roof_materials_i18n', function($q) use ($language_id){
                    $q->on('char_roof_materials_i18n.roof_material_id', '=', 'char_residence.roof_material_id');
                    $q->where('char_roof_materials_i18n.language_id',$language_id);
                })
                ->leftjoin('char_building_status_i18n', function($q) use ($language_id){
                    $q->on('char_building_status_i18n.building_status_id', '=', 'char_residence.house_condition');
                    $q->where('char_building_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_furniture_status_i18n', function($q) use ($language_id){
                    $q->on('char_furniture_status_i18n.furniture_status_id', '=', 'char_residence.indoor_condition');
                    $q->where('char_furniture_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_habitable_status_i18n', function($q) use ($language_id){
                    $q->on('char_habitable_status_i18n.habitable_status_id', '=', 'char_residence.habitable');
                    $q->where('char_habitable_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_house_status_i18n', function($q) use ($language_id){
                    $q->on('char_house_status_i18n.house_status_id', '=', 'char_residence.residence_condition');
                    $q->where('char_house_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_persons_work', 'char_persons_work.person_id', '=', 'char_persons.id')
                ->leftjoin('char_work_jobs_i18n as work_job', function($q) use ($language_id){
                    $q->on('work_job.work_job_id', '=', 'char_persons_work.work_job_id');
                    $q->where('work_job.language_id',$language_id);
                })
                ->leftjoin('char_work_wages as work_wage', function($q) use ($language_id){
                    $q->on('work_wage.id', '=', 'char_persons_work.work_wage_id');
                })
                ->leftjoin('char_work_status_i18n as work_status', function($q) use ($language_id){
                    $q->on('work_status.work_status_id', '=', 'char_persons_work.work_status_id');
                    $q->where('work_status.language_id',$language_id);
                })
                ->leftjoin('char_work_reasons_i18n as work_reason', function($q) use ($language_id){
                    $q->on('work_reason.work_reason_id', '=', 'char_persons_work.work_reason_id');
                    $q->where('work_reason.language_id',$language_id);
                })
                 ->leftjoin('char_locations_i18n As guardian_L1', function($q) use ($language_id){
                        $q->on('guardian_L1.location_id', '=', 'g.location_id');
                        $q->where('guardian_L1.language_id',$language_id);
                    })
                    ->leftjoin('char_locations_i18n As guardian_L2', function($q) use ($language_id){
                        $q->on('guardian_L2.location_id', '=', 'g.city');
                        $q->where('guardian_L2.language_id',$language_id);
                    })
                    ->leftjoin('char_locations_i18n As guardian_L3', function($q) use ($language_id){
                        $q->on('guardian_L3.location_id', '=', 'g.governarate');
                        $q->where('guardian_L3.language_id',$language_id);
                    })
                ->leftjoin('char_locations_i18n As guardian_L4', function($q) use ($language_id){
                        $q->on('guardian_L4.location_id', '=', 'g.country');
                        $q->where('guardian_L4.language_id',$language_id);
                    });

             return $query->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name, 
                                     char_persons.id_card_number,char_persons.old_id_card_number,
                                         ca.name as category_name,
                                         char_persons.id_card_number,char_persons.old_id_card_number,
                                         org.name as organization_name,
                                         CASE WHEN char_sponsorship_cases.sponsor_number is null THEN ' ' Else char_sponsorship_cases.sponsor_number  END  AS sponsorship_number,
                                         CASE WHEN char_sponsorship_cases.sponsorship_date is null THEN ' ' Else char_sponsorship_cases.sponsorship_date  END  AS sponsorship_date,
                                         CASE
                                                          WHEN char_sponsorship_cases.status is null THEN ' '
                                                          WHEN char_sponsorship_cases.status = 1 THEN '$nominate'
                                                          WHEN char_sponsorship_cases.status = 2 THEN '$sent'
                                                          WHEN char_sponsorship_cases.status = 3 THEN '$guaranteed'
                                                          WHEN char_sponsorship_cases.status = 4 THEN '$stopped'
                                                          WHEN char_sponsorship_cases.status = 5 THEN '$paused'
                                                     END
                                                     AS status,
                                         CASE WHEN m.id is null THEN ' '
                                              Else CONCAT(m.first_name, ' ', m.second_name, ' ', m.third_name, ' ', m.last_name)  END
                                         AS mother_name,
                                        CASE WHEN f.id is null THEN ' '
                                              Else CONCAT(f.first_name, ' ', f.second_name, ' ', f.third_name, ' ', f.last_name)  END
                                         AS father_name,
                                        CASE WHEN g.id is null THEN ' '
                                              Else CONCAT(g.first_name, ' ', g.second_name, ' ', g.third_name, ' ', g.last_name)  END
                                         AS guardian_name,
                                         CASE WHEN (ca.case_is = 1 and ca.family_structure= 2) THEN char_get_family_count ( 'left',char_persons.father_id,null,char_persons.id )
                                               WHEN (ca.case_is = 1 and ca.family_structure=1) THEN char_get_family_count ( 'left',char_persons.id,null,char_persons.id )
                                               WHEN (ca.case_is = 2 and ca.family_structure=1) THEN char_get_family_count ( 'left',char_guardians.guardian_id,null,char_persons.id )
                                         END AS brothers,
                                                     CASE WHEN char_persons.birthday is null THEN ' ' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                                     CASE
                                                          WHEN char_persons.gender is null THEN ' '
                                                          WHEN char_persons.gender = 1 THEN '$male'
                                                          WHEN char_persons.gender = 2 THEN '$female'
                                                     END
                                                     AS the_gender,
                                                     CASE WHEN char_persons.birth_place is null THEN ' ' Else L.name END AS birth_place,
                                                     CASE WHEN char_persons.nationality is null THEN ' ' Else L0.name END AS nationality,
                                                    CASE
                                                          WHEN char_persons.Location_id is null THEN ' '
                                                          Else CONCAT(L4.name, ' - ',L3.name, ' - ',L2.name, ' - ', L1.name, ' - ', char_persons.street_address)
                                                          END
                                                     AS address,
                                                      CASE
                                                          WHEN char_persons_health.condition is null THEN ' '
                                                              WHEN char_persons_health.condition = 1 THEN '$perfect'
                                                          WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                          WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                                  END
                                                     AS health_status,
                                                     CASE WHEN char_persons_health.details is null THEN ' ' Else char_persons_health.details END   AS details,
                                                     CASE WHEN char_diseases_i18n.name is null THEN ' ' Else char_diseases_i18n.name END   AS disease_id,
                                                     CASE WHEN char_property_types_i18n.name is null THEN ' ' Else char_property_types_i18n.name END   AS property_type,
                                                     CASE WHEN char_roof_materials_i18n.name is null THEN ' ' Else char_roof_materials_i18n.name END   AS roof_material,
                                                     CASE WHEN char_furniture_status_i18n.name is null THEN ' ' Else char_furniture_status_i18n.name  END  AS indoor_condition,
                                                     CASE WHEN char_house_status_i18n.name is null     THEN ' ' Else char_house_status_i18n.name  END  AS residence_condition,
                                                     CASE WHEN char_residence.area is null             THEN ' ' Else char_residence.area  END  AS area,
                                                     CASE WHEN char_residence.rooms is null            THEN ' ' Else char_residence.rooms  END  AS rooms,
                                                     CASE WHEN char_residence.rent_value is null       THEN ' ' Else char_residence.rent_value  END  AS rent_value,
                                                     CASE
                                                          WHEN char_persons_education.study is null THEN ' '
                                                          WHEN char_persons_education.study = 1 THEN '$yes'
                                                          WHEN char_persons_education.study = 0 THEN '$no'
                                                     END
                                                     AS study,
                                                     CASE
                                                               WHEN char_persons_education.type is null THEN ' '
                                                                     WHEN char_persons_education.type = 1 THEN '$vocational'
                                                               WHEN char_persons_education.type = 2 THEN '$academic'
                                               END
                                                     AS type,
                                                     CASE
                                                              WHEN char_persons_education.level is null THEN ' '
                                                               WHEN char_persons_education.level = 1 THEN '$weak'
                                                               WHEN char_persons_education.level = 2 THEN '$acceptable'
                                                               WHEN char_persons_education.level = 3 THEN '$good'
                                                               WHEN char_persons_education.level = 4 THEN '$very_good'
                                                               WHEN char_persons_education.level = 5 THEN '$excellent'
                                                               Else ' '
                                                     END
                                                     AS level,
                                                    CASE
                                                               WHEN char_persons_education.grade is null THEN ' '
                                                               WHEN char_persons_education.grade = 1 THEN '$the_first '
                                                               WHEN char_persons_education.grade = 2 THEN '$the_second '
                                                               WHEN char_persons_education.grade = 3 THEN '$the_third '
                                                               WHEN char_persons_education.grade = 4 THEN '$the_fourth '
                                                               WHEN char_persons_education.grade = 5 THEN '$the_Fifth '
                                                               WHEN char_persons_education.grade = 6 THEN '$the_Sixth '
                                                               WHEN char_persons_education.grade = 7 THEN '$the_Seventh '
                                                               WHEN char_persons_education.grade = 8 THEN '$the_Eighth '
                                                               WHEN char_persons_education.grade = 9 THEN '$the_Ninth '
                                                               WHEN char_persons_education.grade = 10 THEN '$the_tenth'
                                                               WHEN char_persons_education.grade = 11 THEN '$the_eleventh'
                                                               WHEN char_persons_education.grade = 12 THEN '$the_twelveth'
                                                               WHEN char_persons_education.grade = 13 THEN '$special_studies'
                                                               WHEN char_persons_education.grade = 14 THEN '$vocational_training'
                                                               WHEN char_persons_education.grade = 15 THEN '$first_year_of_university'
                                                               WHEN char_persons_education.grade = 16 THEN '$second_year_of_university'
                                                               WHEN char_persons_education.grade = 17 THEN '$third_year_of_university'
                                                               WHEN char_persons_education.grade = 18 THEN '$fourth_year_of_university'
                                                               WHEN char_persons_education.grade = 19 THEN '$fifth_year_of_university' 
                                                 
                                                     END
                                                     AS grade,
                                                     CASE WHEN char_persons_education.year is null THEN ' '   Else char_persons_education.year END  AS year,
                                                     CASE WHEN char_persons_education.points is null THEN ' ' Else char_persons_education.points END  AS points,
                                                     CASE WHEN char_persons_education.school is null THEN ' ' Else char_persons_education.school END  AS school,
                                                     CASE WHEN char_edu_authorities.name is null THEN ' ' Else char_edu_authorities.name END as authority,
                                                     CASE WHEN char_edu_stages.name is null THEN ' ' Else char_edu_stages.name END as stage,
                                                     CASE WHEN char_edu_degrees.name is null THEN ' ' Else char_edu_degrees.name END as degree,
                                                     CASE
                                                             WHEN char_persons_islamic_commitment.prayer is null THEN ' '
                                                             WHEN char_persons_islamic_commitment.prayer = 0 THEN '$regularly'
                                                             WHEN char_persons_islamic_commitment.prayer = 1 THEN '$does_not_pray'
                                                             WHEN char_persons_islamic_commitment.prayer = 2 THEN '$sometimes'
                                                      END
                                                     AS prayer,
                                                      CASE
                                                               WHEN char_persons_islamic_commitment.quran_parts is null THEN ' '
                                                               WHEN char_persons_islamic_commitment.quran_parts = 0 THEN '$no'
                                                               Else '$yes'
                                                    END
                                                     AS save_quran,
                                                      CASE
                                    WHEN char_persons_islamic_commitment.quran_center is null THEN ' '
                                    WHEN char_persons_islamic_commitment.quran_center = 0 THEN '$no'
                                    Else '$yes'
                                    END
                                    AS quran_center,
                                     CASE
                                    WHEN char_persons_islamic_commitment.quran_center is null THEN ' '
                                    WHEN char_persons_islamic_commitment.quran_center = 0 THEN '$no'
                                    Else '$yes'
                                    END
                                    AS quran_center,
                                                     CASE WHEN char_persons_islamic_commitment.prayer_reason is null THEN ' ' Else  char_persons_islamic_commitment.prayer_reason END AS prayer_reason,
                                                     CASE WHEN char_persons_islamic_commitment.quran_reason is null  THEN ' ' Else char_persons_islamic_commitment.quran_reason END AS quran_reason,
                                                     CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN ' ' Else char_persons_islamic_commitment.quran_parts END  AS quran_parts,
                                                     CASE WHEN char_persons_islamic_commitment.quran_chapters is null  THEN ' ' Else char_persons_islamic_commitment.quran_chapters END AS quran_chapters,
                                                     CASE WHEN f.birthday is null THEN ' ' Else  DATE_FORMAT(f.birthday,'%Y/%m/%d')  END  AS father_birthday,
                                                     CASE WHEN f.spouses is null THEN  ' ' Else  f.spouses END AS father_spouses,
                                                     CASE WHEN f.death_date is null THEN  '$is_alive' Else  '$deceased' END AS father_alive,
                                                     CASE WHEN f.death_date is null THEN ' '  Else f.death_date END AS father_death_date,
                                                     CASE WHEN f.death_date is null THEN ' '  Else father_death_causes.name END AS father_death_cause,
                                                     CASE
                                                            WHEN father_health.condition is null THEN ' '
                                                                   WHEN father_health.condition is null THEN ' '
                                                            WHEN father_health.condition = 1 THEN '$perfect'
                                                            WHEN father_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                            WHEN father_health.condition = 3 THEN '$special_needs'
                                                   END
                                                     AS father_health_status,
                                                     CASE WHEN father_health.details is null THEN ' ' Else father_health.details END   AS father_details,
                                                     CASE WHEN father_diseases.name is null THEN ' ' Else father_diseases.name END   AS father_disease_id,
                                                     CASE WHEN father_education.specialization is null THEN ' '  Else father_education.specialization END  AS father_Specialization,
                                                     CASE WHEN father_edu_degrees.name is null THEN ' ' Else father_edu_degrees.name END as father_degree,
                                                     CASE
                                                          WHEN father_work.working is null THEN ' '
                                                          WHEN father_work.working = 1 THEN '$yes'
                                                          WHEN father_work.working = 2 THEN '$no'
                                                       END
                                                     AS father_working,
                                                     CASE
                                                           WHEN father_work.working is null THEN ' '
                                                           WHEN father_work.working = 1 THEN father_work_jobs.name
                                                           WHEN father_work.working = 2 THEN ' '
                                                     END
                                                     AS father_work_type,
                                                     CASE
                                                       WHEN father_work.working is null THEN ' '
                                                       WHEN father_work.working = 1 THEN f.monthly_income
                                                       WHEN father_work.working = 2 THEN ' '
                                                     END
                                                     AS father_monthly_income,
                                                     CASE
                                                           WHEN father_work.working is null THEN ' '
                                                           WHEN father_work.working = 1 THEN father_work.work_location
                                                           WHEN father_work.working = 2 THEN ' '
                                                     END
                                                     AS father_work_location,
                                                     CASE WHEN m.birthday is null THEN ' ' Else  DATE_FORMAT(m.birthday,'%Y/%m/%d')  END  AS mother_birthday,
                                                     CASE WHEN mother_marital_status.name is null THEN ' ' Else mother_marital_status.name END as mother_marital_status,
                                                     CASE WHEN m.death_date is null THEN  '$is_alive' Else  '$deceased' END AS mother_alive,
                                                     CASE WHEN m.death_date is null THEN ' '  Else f.death_date END AS mother_death_date,
                                                     CASE WHEN m.death_date is null THEN ' '  Else father_death_causes.name END AS mother_death_cause,
                                                     CASE
                                                            WHEN mother_health.condition is null THEN ' '
                                                               WHEN mother_health.condition = 1 THEN '$perfect'
                                                            WHEN mother_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                            WHEN mother_health.condition = 3 THEN '$special_needs'
                                                     END
                                                     AS mother_health_status,
                                                   CASE WHEN mother_health.details is null THEN ' ' Else mother_health.details END   AS mother_details,
                                                   CASE WHEN mother_diseases.name is null THEN ' ' Else mother_diseases.name END   AS mother_disease_id,
                                                   CASE WHEN mother_education.specialization is null THEN ' '  Else mother_education.specialization END  AS mother_Specialization,
                                                     CASE WHEN mother_edu_stages.name is null THEN ' ' Else mother_edu_stages.name END as mother_stage,
                                                     CASE
                                                          WHEN mother_work.working is null THEN ' '
                                                          WHEN mother_work.working = 1 THEN '$yes'
                                                          WHEN mother_work.working = 2 THEN '$no'
                                                       END
                                                     AS mother_working,
                                                     CASE
                                                           WHEN mother_work.working is null THEN ' '
                                                           WHEN mother_work.working = 1 THEN mother_work_jobs.name
                                                           WHEN mother_work.working = 2 THEN ' '
                                                     END
                                                     AS mother_work_type,
                                                     CASE
                                                       WHEN mother_work.working is null THEN ' '
                                                       WHEN mother_work.working = 1 THEN m.monthly_income
                                                       WHEN mother_work.working = 2 THEN ' '
                                                     END
                                                     AS mother_monthly_income,
                                                     CASE
                                                           WHEN mother_work.working is null THEN ' '
                                                           WHEN mother_work.working = 1 THEN mother_work.work_location
                                                           WHEN mother_work.working = 2 THEN ' '
                                                     END
                                                     AS mother_work_location,
                                                     CASE
                                                          WHEN g.Location_id is null THEN ' '
                                                          Else CONCAT(guardian_L4.name, ' - ',guardian_L3.name, ' - ',guardian_L2.name, ' - ', guardian_L1.name, ' - ', g.street_address)
                                                          END
                                                     AS guardian_address,
                                                     CASE WHEN guardian_property_types.name is null THEN ' ' Else guardian_property_types.name END   AS guardian_property_type,
                                                     CASE WHEN guardian_roof_materials.name is null THEN ' ' Else guardian_roof_materials.name END   AS guardian_roof_material,
                                                     CASE WHEN guardian_furniture_status_i18n.name is null THEN ' ' Else guardian_furniture_status_i18n.name  END  AS indoor_condition,
                                                     CASE WHEN guardian_house_status_i18n.name is null     THEN ' ' Else guardian_house_status_i18n.name  END  AS residence_condition,
                                                     CASE WHEN guardian_residence.area is null             THEN ' ' Else guardian_residence.area  END  AS area,
                                                     CASE WHEN guardian_residence.rooms is null            THEN ' ' Else guardian_residence.rooms  END  AS rooms,
                                                     CASE WHEN guardian_residence.rent_value is null       THEN ' ' Else guardian_residence.rent_value  END  AS rent_value
                                                     
                             ")->get();
        }
        else{
            
            if($char_persons_i18n_cnt != 0){
                $query->leftjoin('char_persons_i18n', 'char_persons_i18n.person_id', '=', 'char_cases.person_id');
            }
            if($char_residence_cnt != 0) {
                $query->leftjoin('char_residence', 'char_residence.person_id', '=', 'char_persons.id');
            }
            if($char_persons_work_cnt != 0){
                $query ->leftjoin('char_persons_work','char_persons_work.person_id', '=', 'char_persons.id');
            }
            if($char_persons_health_cnt!= 0 ){
                $query->leftjoin('char_persons_health','char_persons_health.person_id','=','char_persons.id');
            }
            if($char_persons_education_cnt!= 0 ){
                $query->leftjoin('char_persons_education','char_persons_education.person_id','=','char_persons.id');
            }
            if($char_persons_islamic_commitment_cnt!= 0  || isset($filters['save_quran'])){
                $query->leftjoin('char_persons_islamic_commitment','char_persons_islamic_commitment.person_id','=','char_persons.id');
            }

            if($char_organization_persons_banks_cnt!= 0  || $char_persons_banks_cnt != 0) {
                $query->leftjoin('char_organization_persons_banks', function ($q) {
                    $q->on('char_organization_persons_banks.person_id', '=', 'char_persons.id');
                    $q->on('char_organization_persons_banks.organization_id', '=', 'char_cases.organization_id');
                })
                    ->leftjoin('char_persons_banks', function ($q) {
                        $q->on('char_persons_banks.id', '=', 'char_organization_persons_banks.bank_id');
                    });

            }
            if($primary_num_cnt != 0){
                $query->leftjoin('char_persons_contact as primary_num', function($q) use ($language_id){
                    $q->on('primary_num.person_id', '=', 'char_persons.id');
                    $q->where('primary_num.contact_type','primary_mobile');
                });
            }
            if($secondary_num_cnt != 0){
                $query->leftjoin('char_persons_contact as secondary_num', function($q) use ($language_id){
                    $q->on('secondary_num.person_id', '=', 'char_persons.id');
                    $q->where('secondary_num.contact_type','secondary_mobile');
                });
            }
            if($phone_num_cnt != 0){
                $query->leftjoin('char_persons_contact as phone_num', function($q) use ($language_id){
                    $q->on('phone_num.person_id', '=', 'char_persons.id');
                    $q->where('phone_num.contact_type','phone');
                });
            }

            if($guardian_residence_cnt!= 0 ){
                    $query->leftjoin('char_residence as guardian_residence','guardian_residence.person_id','=','g.id')   ;
                }
                if($guardian_cont1_cnt!= 0 ){
                    $query->leftjoin('char_persons_contact as guardian_cont1', function($q) use ($language_id){
                        $q->on('guardian_cont1.person_id', '=', 'g.id');
                        $q->where('guardian_cont1.contact_type','primary_mobile');
                    })      ;
                }

                if($guardian_cont3_cnt!= 0 ){
                    $query->leftjoin('char_persons_contact as guardian_cont3', function($q) use ($language_id){
                        $q->on('guardian_cont3.person_id', '=', 'g.id');
                        $q->where('guardian_cont3.contact_type','phone');
                    })        ;
                }
                if($guardian_education_cnt!= 0 ){
                    $query->leftjoin('char_persons_education as guardian_education','guardian_education.person_id','=','g.id');
                }
                if($guardian_work_cnt!= 0 ){
                    $query->leftjoin('char_persons_work as guardian_work','guardian_work.person_id','=','g.id')  ;
                }
                
                if($mother_health_cnt!= 0 ){
                        $query->leftjoin('char_persons_health as mother_health','mother_health.person_id','=','m.id');
                    }
                    if($mother_work_cnt != 0 ){
                        $query->leftjoin('char_persons_work as mother_work','mother_work.person_id','=','m.id');
                    }
                    if($mother_education_cnt!= 0 ){
                        $query->leftjoin('char_persons_education as mother_education','mother_education.person_id','=','m.id');
                    }
                    
                if($father_health_cnt!= 0 ){
                    $query->leftjoin('char_persons_health as father_health','father_health.person_id','=','f.id');
                }
                if($father_work_cnt   != 0 ){
                    $query->leftjoin('char_persons_work as father_work','father_work.person_id','=','f.id');
                }
                if($father_education_cnt!= 0 ){
                    $query->leftjoin('char_persons_education as father_education','father_education.person_id','=','f.id');
                }
            
        }
        
        
        if($filters['action'] =='ExportToWord'){
            $return= $query->selectRaw("char_cases.id,char_sponsorship_cases.sponsor_id,org.name as organization_name" )->get();
            $query=[];

            $lang = 'ar';
            if(isset($filters['lang'])){
                $lang = $filters['lang'];
            }

            foreach($return as $k =>$v){
                $sub=\Common\Model\CaseModel::fetch(array(
                    'action' => 'show',
                    'category'      => true,
                    'full_name'     => true,
                    'family_member' => true,
                    'person'        => true,
                    'persons_i18n'  => true,
                    'contacts'      => true,
                    'education'     => true,
                    'islamic'       => true,
                    'health'        => true,
                    'work'          => true,
                    'residence'     => true,
                    'default_bank'  => true,
                    'banks'         => true,
                    'persons_documents'  => true,
                    'organization_name'      => true,
                    'father_id'     => true,
                    'mother_id'     => true,
                    'guardian_id'   => true,
                    'visitor_note'  => true,
                    'lang'  => $lang
                ),$v->id);
                $get=array('action'=>'show','person'=>true,'persons_i18n'=>true,'health'=>true,'education'=>true,'work'=>true);

                if($sub->father_id != null){
                    $get['id'] = $sub->father_id;
                    $sub->father =\Common\Model\Person::fetch($get);
                }

                if($sub->mother_id != null) {
                    $get['id'] = $sub->mother_id;
                    $sub->mother = \Common\Model\Person::fetch($get);
                }
                if($sub->guardian_id != null) {
                    $sub->guardian =\Common\Model\Person::fetch(array('action'=>'show', 'id' => $sub->guardian_id,'person' =>true, 'persons_i18n' => true, 'contacts' => true,  'education' => true,
                        'work' => true, 'residence' => true, 'banks' => true));
                }

                $sub->sponsor_id=$v->sponsor_id;
                $sub->organization_name=$v->organization_name;
                $return[]=$sub;
            }

            return $query;
        }

        else{
        $query->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name,    char_cases.id as case_id,
                             char_persons.id_card_number,char_persons.old_id_card_number,ca.name as category_name,
                             org.name as organization_name,
                             char_sponsorship_cases.id,
                             char_sponsorship_cases.sponsor_number,
                             char_sponsorship_cases.sponsorship_date,
                             char_sponsorship_cases.sponsorship_date as date_flag,
                             char_sponsorship_cases.status,
                             char_sponsorship_cases.status as flag,
                             char_sponsorship_cases.id,
                             char_sponsorship_cases.sponsor_id,
                             CASE
                                  WHEN char_persons.gender = 1 THEN '$male'
                                  WHEN char_persons.gender = 2 THEN '$female'
                             END
                             AS gender,
                             CASE
                                  WHEN char_sponsorship_cases.status = 1 THEN '$nominate'
                                  WHEN char_sponsorship_cases.status = 2 THEN '$sent'
                                  WHEN char_sponsorship_cases.status = 3 THEN '$guaranteed'
                                  WHEN char_sponsorship_cases.status = 4 THEN '$stopped'
                                  WHEN char_sponsorship_cases.status = 5 THEN '$paused'
                             END
                             AS status_name,
                             CASE WHEN char_persons.birthday is null THEN ' ' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  As birthday,  
                             CASE WHEN char_persons.death_date is null THEN ' ' Else DATE_FORMAT(char_persons.death_date,'%Y/%m/%d')  END  As death_date,  
                             char_marital_status_i18n.name as marital_status,                             
                             CASE WHEN char_persons.death_date is null THEN EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(), char_persons.birthday))))
                                 Else EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(), char_persons.death_date))))  
                              END  As age,
                               char_persons.family_cnt AS family_count,char_persons.spouses AS spouses,
                                           char_persons.male_live AS male_count,
                                           char_persons.female_live AS female_count
                             "
            );
        $itemsCount = isset($filters['itemsCount'])? $filters['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
        return $query=$query->paginate($records_per_page);
        }

    }

    public static function getBasicPerson($page,$sponsor_id,$filters){

        $language_id =  \App\Http\Helpers::getLocale();

        $char_sponsorship_cases=['status','sponsor_id'];
        $char_cases=['category_id','organization_id'];
        $char_cases=['category_id','organization_id'];
        $char_persons = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number', 'gender',
            'marital_status_id','birth_place','nationality','refugee','unrwa_card_number','location_id','country','governarate','city',
            'street_address','monthly_income','mosques_id',"adscountry_id","is_qualified","qualified_card_number",
            "adsdistrict_id", "adsmosques_id", "card_type", "adsneighborhood_id", "adsregion_id",
            "adssquare_id" ,'actual_monthly_income', 'receivables',  'receivables_sk_amount',
            'has_commercial_records','active_commercial_records', 'gov_commercial_records_details'];
        $char_residence = ['property_type_id', 'roof_material_id','residence_condition','indoor_condition',
            'house_condition','habitable','rent_value','need_repair','repair_notes'];
        $char_persons_i18n = ['en_first_name', 'en_second_name', 'en_third_name', 'en_last_name'];
        $char_persons_work = ['working','can_work','work_status_id','work_reason_id','work_job_id','work_wage_id','work_location',
            'gov_work_details'];
        $char_persons_health = ['condition','disease_id','has_health_insurance', 'health_insurance_number', 'health_insurance_type',
            'has_device', 'used_device_name', 'gov_health_details'];
        $char_persons_education = ['grade', 'authority','type','stage','level','year','school','points'];
        $char_persons_islamic_commitment = ['prayer','quran_parts', 'quran_chapters','quran_center'];
        $reconstructions=['promised','reconstructions_organization_name'];
        $secondary_mobile=['secondary_mobile'];
        $primary_mobile=['primary_mobile'];
        $phone=['phone'];
        $char_organization_persons_banks = ['bank_id'];
        $char_persons_banks = ['bank_id','branch_name','account_number','account_owner'];
        $char_persons_father = ['father_id_card_number','father_card_type','father_death_cause_id','father_first_name', 'father_monthly_income', 'father_second_name', 'father_third_name', 'father_last_name','father_alive'];
        $char_persons_mother = ['mother_id_card_number','mother_card_type','mother_death_cause_id','mother_first_name','mother_marital_status_id' ,'mother_second_name','mother_monthly_income', 'mother_third_name','mother_nationality', 'mother_last_name','mother_alive'];
        $char_persons_guardian=['guardian_id_card_number','guardian_card_type','guardian_first_name', 'guardian_second_name','guardian_third_name',
            'guardian_gender','guardian_marital_status_id','guardian_location_id','guardian_country','guardian_governarate','guardian_city',
            'guardian_monthly_income','guardian_birth_place','guardian_nationality', 'guardian_last_name','guardian_mosques_id'];
        $guardian_residence = ['guardian_property_type_id', 'guardian_residence_condition','guardian_indoor_condition','guardian_roof_material_id'];
        $mother_health = ['mother_condition','mother_disease_id'];
        $father_health = ['father_condition','father_disease_id'];
        $father_work = ['father_working','father_work_job_id','father_work_location'];
        $mother_work = ['mother_working','mother_work_job_id','mother_work_location'];
        $guardian_work = ['guardian_working','guardian_work_job_id','guardian_work_location'];
        $father_education = ['father_grade', 'father_authority','father_type','father_stage','father_level','father_year','father_school','father_points'];
        $mother_education = ['mother_grade', 'mother_authority','mother_type','mother_stage','mother_level','mother_year','mother_school','mother_points'];
        $guardian_education = ['guardian_grade', 'guardian_authority','guardian_type','guardian_stage','guardian_level','guardian_year','guardian_school','guardian_points'];
        $guardian_cont1=['guardian_primary_mobile'];
        $guardian_cont2=['guardian_secondary_mobile'];
        $guardian_cont3=['guardian_phone'];


        $char_persons_health_cnt =
        $char_persons_work_cnt  =
        $char_persons_education_cnt =
        $char_persons_i18n_cnt  =
        $char_residence_cnt =
        $char_persons_work_cnt =
        $primary_num_cnt = $secondary_num_cnt =  $phone_num_cnt = $reconstruct_cnt=
        $char_persons_islamic_commitment_cnt =
        $char_organization_persons_banks_cnt=
        $char_persons_banks_cnt =
        $char_persons_father_cnt =
        $char_persons_mother_cnt =
        $char_persons_guardian_cnt =
        $guardian_residence_cnt =
        $mother_health_cnt =
        $father_health_cnt =
        $father_work_cnt    =
        $mother_work_cnt  =
        $guardian_work_cnt =
        $father_education_cnt =
        $mother_education_cnt =
        $guardian_education_cnt =
        $guardian_cont1_cnt =
        $guardian_cont2_cnt =
        $guardian_cont3_cnt = 0;

        $condition=[];
        foreach ($filters as $key => $value) {
            if(in_array($key, $char_cases)) {
                if ( $value != "" ) {
                    if($key =='case_status'){
                        $data = ['char_cases.status'=> $value];
                    }else{
                        $data = ['char_cases.' . $key => $value];
                    }
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $reconstructions)) {
                if ( $value != "" ) {
                    if($key =='reconstructions_organization_name'){
                        $data = ['char_reconstructions.organization_name'=> $value];
                    }else{
                        $data = ['char_reconstructions.' . $key => $value];
                    }
                    $reconstruct_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons)) {
                if ( $value != "" ) {
                    $data = ['char_persons.' . $key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_residence)) {
                if ( $value != "" ) {
                    $data = ['char_residence.' . $key =>  $value];
                    $char_residence_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_i18n)) {
                if ( $value != "" ) {
                    $data = ['char_persons_i18n.' . substr($key,3)=>$value ];
                    $char_persons_i18n_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_work)) {

                if ( $value != "" ) {
                    $data = ['char_persons_work.' . $key => $value];
                    $char_persons_work_cnt ++;
                    array_push($condition, $data);
                }

            }
            if(in_array($key, $char_persons_health)) {
                if ( $value != "" ) {
                    $data = ['char_persons_health.' . $key =>  $value];
                    $char_persons_health_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_education)) {
                if ( $value != "" ) {
                    $data = ['char_persons_education.' . $key => $value];
                    $char_persons_education_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_islamic_commitment)) {
                if ( $value != "" ) {
                    $data = ['char_persons_islamic_commitment.' . $key => $value];
                    $char_persons_islamic_commitment_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $phone)) {
                if ( $value != "" ) {
                    $data = ['phone_num.contact_value' => $value]; $phone_num_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $primary_mobile)) {
                if ( $value != "" ) {
                    $data = ['primary_num.contact_value' => $value];
                    $primary_num_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $secondary_mobile)) {
                if ( $value != "" ) {
                    $data = ['secondary_num.contact_value' => $value];
                    $secondary_num_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_organization_persons_banks)) {
                if ( $value != "" ) {
                    $data = ['char_organization_persons_banks.'. $key => $value];
                    $char_organization_persons_banks_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_banks)) {
                if ( $value != "" ) {
                    $data = ['char_persons_banks.'. $key => $value];
                    $char_persons_banks_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_father)) {
                if ( $value != "" ) {
                    if($key =="father_alive"){
                        $data = ['f.death_date'  => $value];
                    }else{
                        $data = ['f.' . substr($key, 7) => $value];
                    }
                    $char_persons_father_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_mother)) {
                if ( $value != "" ) {
                    if($key =="mother_alive"){
                        $data = ['m.death_date'  => $value];
                    }else{
                        $data = ['m.' . substr($key, 7) => $value];
                    }
                    $char_persons_mother_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_guardian)) {
                if ( $value != "" ) {
                    $data = ['g.' . substr($key, 9) => $value];
                    $char_persons_guardian_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_residence)) {
                if ( $value != "" ) {
                    $data = ['guardian_residence.' . substr($key, 9) => $value];
                    $guardian_residence_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key,$mother_health)) {
                if ( $value != "" ) {
                    $data = ['mother_health.' . substr($key, 7) => $value];
                    $mother_health_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key,$father_health)) {
                if ( $value != "" ) {
                    $data = ['father_health.' . substr($key, 7) => $value];
                    $father_health_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $father_work)) {

                if ( $value != "" ) {
                    $father_work_cnt   ++;
                    $data = ['father_work.' . substr($key, 7) => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $mother_work)) {

                if ( $value != "" ) {
                    $data = ['mother_work.' . substr($key, 7) => $value];
                    $mother_work_cnt ++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_work)) {

                if ( $value != "" ) {
                    $data = ['guardian_work.' . substr($key, 9) => $value];
                    $guardian_work_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $father_education)) {

                if ( $value != "" ) {
                    $data = ['father_education.' . substr($key, 7) => $value];
                    $father_education_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $mother_education)) {

                if ( $value != "" ) {
                    $data = ['mother_education.' . substr($key, 7) => $value];
                    $mother_education_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_education)) {

                if ( $value != "" ) {
                    $data = ['guardian_education.' . substr($key, 9) => $value];
                    $guardian_education_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_cont1)) {
                if ( $value != "" ) {
                    $data = ['guardian_cont1.contact_value' => $value];
                    $guardian_cont1_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_cont2)) {
                if ( $value != "" ) {
                    $data = ['guardian_cont2.contact_value' => $value];
                    $guardian_cont2_cnt++;
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $guardian_cont3)) {
                if ( $value != "" ) {
                    $data = ['guardian_cont3.contact_value' => $value];
                    $guardian_cont3_cnt++;
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $char_sponsorship_cases)) {
                if ( $value != "" ) {
                    $data = ['char_sponsorship_cases.'.$key => $value];
                    array_push($condition, $data);
                }
            }


        }

        $language_id =  \App\Http\Helpers::getLocale();

        $query=
            \DB::table('char_sponsorship_cases')
                ->join('char_cases', 'char_sponsorship_cases.case_id', '=', 'char_cases.id')
                ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
                ->join('char_organizations as org','org.id',  '=', 'char_cases.organization_id')
                ->join('char_categories as ca','ca.id','=','char_cases.category_id')
                ->join('char_marital_status_i18n', function($q) use ($language_id){
                    $q->on('char_marital_status_i18n.marital_status_id', '=', 'char_persons.marital_status_id');
                    $q->where('char_marital_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_persons AS f','f.id','=','char_persons.father_id')
                ->leftjoin('char_persons AS m','m.id','=','char_persons.mother_id')
                ->leftjoin('char_guardians', function($q) use ($language_id){
                    $q->on('char_cases.person_id','=','char_guardians.individual_id');
                    $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                    $q->where('char_guardians.status',1);
                })
                ->leftjoin('char_persons AS g','g.id','=','char_guardians.guardian_id')
                ->leftjoin('char_locations_i18n As L2', function($q) use ($language_id){
                    $q->on('L2.location_id', '=', 'char_persons.city');
                    $q->where('L2.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L3', function($q) use ($language_id) {
                    $q->on('L3.location_id', '=', 'char_persons.governarate');
                    $q->where('L3.language_id',$language_id);
                })
                ->leftjoin('char_marital_status_i18n as mother_marital_status', function($q) use ($language_id){
                    $q->on('mother_marital_status.marital_status_id', '=', 'm.marital_status_id');
                    $q->where('mother_marital_status.language_id',$language_id);
                })
                ->whereIn('char_sponsorship_cases.status',[3,4,5])
                ->where('char_sponsorship_cases.sponsor_id',$sponsor_id)
;

        if (count($condition) != 0) {
            $query->where(function ($q) use ($condition) {
                    $names = ['char_persons.street_address','char_persons.first_name', 'char_persons.second_name',
                        'char_persons.third_name', 'char_persons.last_name',
                        'char_persons_i18n.first_name', 'char_persons_i18n.second_name', 'char_persons_i18n.third_name',
                        'char_persons_i18n.last_name','char_residence.repair_notes',
                        'char_cases.visitor','char_cases.notes','char_cases.visitor_card','char_cases.visitor_opinion',
                        'char_persons_work.work_location','char_persons_banks.account_owner',
                        'm.first_name', 'm.second_name', 'm.third_name', 'm.last_name',
                        'g.first_name', 'g.second_name', 'g.third_name', 'g.last_name',
                        'char_persons_health.health_insurance_number','char_persons_health.used_device_name',
                        'char_persons_health.gov_health_details',
                        'char_persons.gov_commercial_records_details','char_residence.repair_notes',
                        'char_persons_work.work_location', 'char_persons_work.gov_work_details' ,
                        'char_persons_health.used_device_name', 'char_persons_health.gov_health_details'
                    ];


                    $live = ['m.death_date','f.death_date'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $names)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else if(in_array($key, $live)){
                                if($value == 0){
                                    $q->where($key, '=', null);
                                }else{
                                    $q->where($key, '!=', null);
                                }
                            }else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });

        }
        
        if((isset($filters['birthday_to']) && $filters['birthday_to'] !=null) || (isset($filters['birthday_from']) && $filters['birthday_from'] !=null)){

            $birthday_to=null;
            $birthday_from=null;

            if(isset($filters['birthday_to']) && $filters['birthday_to'] !=null){
                $birthday_to=date('Y-m-d',strtotime($filters['birthday_to']));
            }
            if(isset($filters['birthday_from']) && $filters['birthday_from'] !=null){
                $birthday_from=date('Y-m-d',strtotime($filters['birthday_from']));
            }
            if($birthday_from != null && $birthday_to != null) {
                $query->whereBetween( 'char_persons.birthday', [ $birthday_from, $birthday_to]);
            }elseif($birthday_from != null && $birthday_to == null) {
                $query->whereDate('char_persons.birthday', '>=', $birthday_from);
            }elseif($birthday_from == null && $birthday_to != null) {
                $query->whereDate('char_persons.birthday', '<=', $birthday_to);
            }
        }
        
        $to_sponsorship_date=null;
        $from_sponsorship_date=null;
        $to_guaranteed_date=null;
        $from_guaranteed_date=null;
        if(isset($filters['to_sponsorship_date']) && $filters['to_sponsorship_date'] !=null){
            $to_sponsorship_date=date('Y-m-d',strtotime($filters['to_sponsorship_date']));
        }
        if(isset($filters['from_sponsorship_date']) && $filters['from_sponsorship_date'] !=null){
            $from_sponsorship_date=date('Y-m-d',strtotime($filters['from_sponsorship_date']));
        }
        if(isset($filters['to_guaranteed_date']) && $filters['to_guaranteed_date'] !=null){
            $to_guaranteed_date=date('Y-m-d',strtotime($filters['to_guaranteed_date']));
        }
        if(isset($filters['from_guaranteed_date']) && $filters['from_guaranteed_date'] !=null){
            $from_guaranteed_date=date('Y-m-d',strtotime($filters['from_guaranteed_date']));
        }

        if($from_sponsorship_date != null && $to_sponsorship_date != null) {
            $query = $query->whereBetween( 'char_sponsorship_cases.created_at', [ $from_sponsorship_date, $to_sponsorship_date]);
        }elseif($from_sponsorship_date != null && $to_sponsorship_date == null) {
            $query = $query->whereDate('char_sponsorship_cases.created_at', '>=', $from_sponsorship_date);
        }elseif($from_sponsorship_date == null && $to_sponsorship_date != null) {
            $query = $query->whereDate('char_sponsorship_cases.created_at', '<=', $to_sponsorship_date);
        }

        if($from_guaranteed_date != null && $to_guaranteed_date != null) {
            $query = $query->whereBetween( 'char_sponsorship_cases.sponsorship_date', [ $from_guaranteed_date, $to_guaranteed_date]);
        }elseif($from_guaranteed_date != null && $to_guaranteed_date == null) {
            $query = $query->whereDate('char_sponsorship_cases.sponsorship_date', '>=', $from_guaranteed_date);
        }elseif($from_guaranteed_date == null && $to_guaranteed_date != null) {
            $query = $query->whereDate('char_sponsorship_cases.sponsorship_date', '<=', $to_guaranteed_date);
        }
        
        if((isset($filters['mother_birthday_to']) && $filters['mother_birthday_to'] !=null) ||
                (isset($filters['mother_birthday_from']) && $filters['mother_birthday_from'] !=null)) {

                $mother_birthday_to=null;
                $mother_birthday_from=null;

                if(isset($filters['mother_birthday_to']) && $filters['mother_birthday_to'] !=null){
                    $mother_birthday_to=date('Y-m-d',strtotime($filters['mother_birthday_to']));
                }
                if(isset($filters['mother_birthday_from']) && $filters['mother_birthday_from'] !=null){
                    $mother_birthday_from=date('Y-m-d',strtotime($filters['mother_birthday_from']));
                }

                if($mother_birthday_from != null && $mother_birthday_to != null) {
                    $query->whereBetween( 'm.birthday', [ $mother_birthday_from, $mother_birthday_to]);
                }elseif($mother_birthday_from != null && $mother_birthday_to == null) {
                    $query->whereDate('m.birthday', '>=', $mother_birthday_from);
                }elseif($mother_birthday_from == null && $mother_birthday_to != null) {
                    $query->whereDate('m.birthday', '<=', $mother_birthday_to);
                }

            }

            if((isset($filters['mother_death_date_to']) && $filters['mother_death_date_to'] !=null) ||
                (isset($filters['mother_death_date_from']) && $filters['mother_death_date_from'] !=null)) {
                $mother_death_date_to=null;
                $mother_death_date_from=null;

                if(isset($filters['mother_death_date_to']) && $filters['mother_death_date_to'] !=null){
                    $mother_death_date_to=date('Y-m-d',strtotime($filters['mother_death_date_to']));
                }
                if(isset($filters['mother_death_date_from']) && $filters['mother_death_date_from'] !=null){
                    $mother_death_date_from=date('Y-m-d',strtotime($filters['mother_death_date_from']));
                }
                if($mother_death_date_from != null && $mother_death_date_to != null) {
                    $query->whereBetween( 'm.death_date', [ $mother_death_date_from, $mother_death_date_to]);
                }elseif($mother_death_date_from != null && $mother_death_date_to == null) {
                    $query->whereDate('m.death_date', '>=', $mother_death_date_from);
                }elseif($mother_death_date_from == null && $mother_death_date_to != null) {
                    $query->whereDate('m.death_date', '<=', $mother_death_date_to);
                }
            }

            if((isset($filters['father_death_date_to']) && $filters['father_death_date_to'] !=null) ||
                (isset($filters['father_death_date_from']) && $filters['father_death_date_from'] !=null)) {

                $father_death_date_to=null;
                $father_death_date_from=null;

                if(isset($filters['father_death_date_to']) && $filters['father_death_date_to'] !=null){
                    $father_death_date_to=date('Y-m-d',strtotime($filters['father_death_date_to']));
                }
                if(isset($filters['father_death_date_from']) && $filters['father_death_date_from'] !=null){
                    $father_death_date_from=date('Y-m-d',strtotime($filters['father_death_date_from']));
                }
                if($father_death_date_from != null && $father_death_date_to != null) {
                    $query->whereBetween( 'f.death_date', [ $father_death_date_from, $father_death_date_to]);
                }elseif($father_death_date_from != null && $father_death_date_to == null) {
                    $query->whereDate('f.death_date', '>=', $father_death_date_from);
                }elseif($father_death_date_from == null && $father_death_date_to != null) {
                    $query->whereDate('f.death_date', '<=', $father_death_date_to);
                }
            }

            if((isset($filters['father_birthday_to']) && $filters['father_birthday_to'] !=null)||
                (isset($filters['father_birthday_from']) && $filters['father_birthday_from'] !=null)){

                $father_birthday_to=null;
                $father_birthday_from=null;

                if(isset($filters['father_birthday_to']) && $filters['father_birthday_to'] !=null){
                    $father_birthday_to=date('Y-m-d',strtotime($filters['father_birthday_to']));
                }
                if(isset($filters['father_birthday_from']) && $filters['father_birthday_from'] !=null){
                    $father_birthday_from=date('Y-m-d',strtotime($filters['father_birthday_from']));
                }

                if($father_birthday_from != null && $father_birthday_to != null) {
                    $query->whereBetween( 'f.birthday', [ $father_birthday_from, $father_birthday_to]);
                }
                elseif($father_birthday_from != null && $father_birthday_to == null) {
                    $query->whereDate('f.birthday', '>=', $father_birthday_from);
                }
                elseif($father_birthday_from == null && $father_birthday_to != null) {
                    $query->whereDate('f.birthday', '<=', $father_birthday_to);
                }

            }
        
       if(isset($filters['persons'])){
            if(sizeof($filters['persons']) > 0 ){
               $query->whereIn('char_sponsorship_cases.id',$filters['persons']);
            } 
        }

        $nominate =  trans('sponsorship::application.nominate');
        $sent =  trans('sponsorship::application.sent');
        $guaranteed =  trans('sponsorship::application.guaranteed');
        $stopped =  trans('sponsorship::application.stopped');
        $paused =  trans('sponsorship::application.paused');
        
        $active = trans('common::application.active');
        $inactive = trans('common::application.inactive');

        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $refugee = trans('common::application.refugee');
        $citizen = trans('common::application.citizen');

        $not_exist = trans('common::application.not_exist');
        $exist = trans('common::application.exist');

        $not_work = trans('common::application.not_work');
        $work = trans('common::application.work');


        $qualified = trans('common::application.qualified');
        $not_qualified = trans('common::application.not qualified');

        $is_alive = trans('common::application.is_alive');
        $deceased = trans('common::application.deceased');

        $vocational = trans('common::application.vocational');
        $academic = trans('common::application.academic');

        $perfect = trans('common::application.perfect');
        $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
        $special_needs = trans('common::application.special_needs');

        $excellent = trans('common::application.excellent');
        $acceptable = trans('common::application.acceptable');

        $good = trans('common::application.good');
        $very_good = trans('common::application.very_good');
        $weak = trans('common::application.weak');
        $very_bad_condition = trans('common::application.very_bad_condition');
        $bad_condition = trans('common::application.bad_condition');

        $the_first  = trans('common::application.the_first') ;
        $the_second = trans('common::application.the_second') ;
        $the_third  = trans('common::application.the_third') ;
        $the_fourth = trans('common::application.the_fourth') ;
        $the_Fifth  = trans('common::application.the_Fifth');
        $the_Sixth  = trans('common::application.the_Sixth');
        $the_Seventh = trans('common::application.the_Seventh');
        $the_Eighth= trans('common::application.the_Eighth');
        $the_Ninth = trans('common::application.the_Ninth');
        $the_tenth = trans('common::application.the_tenth');
        $the_eleventh = trans('common::application.the_eleventh');
        $the_twelveth= trans('common::application.the_twelveth');

        $special_studies= trans('common::application.special_studies');
        $vocational_training= trans('common::application.vocational_training');
        $first_year_of_university= trans('common::application.first_year_of_university');
        $second_year_of_university= trans('common::application.second_year_of_university');
        $third_year_of_university= trans('common::application.third_year_of_university');
        $fourth_year_of_university= trans('common::application.fourth_year_of_university');
        $fifth_year_of_university= trans('common::application.fifth_year_of_university');

        $regularly = trans('common::application.regularly');
        $does_not_pray = trans('common::application.does_not_pray');
        $sometimes = trans('common::application.sometimes');

  if($char_persons_i18n_cnt != 0){
                $query->leftjoin('char_persons_i18n', 'char_persons_i18n.person_id', '=', 'char_cases.person_id');
            }
            if($char_residence_cnt != 0) {
                $query->leftjoin('char_residence', 'char_residence.person_id', '=', 'char_persons.id');
            }
            if($char_persons_work_cnt != 0){
                $query ->leftjoin('char_persons_work','char_persons_work.person_id', '=', 'char_persons.id');
            }
            if($char_persons_health_cnt!= 0 ){
                $query->leftjoin('char_persons_health','char_persons_health.person_id','=','char_persons.id');
            }
            if($char_persons_education_cnt!= 0 ){
                $query->leftjoin('char_persons_education','char_persons_education.person_id','=','char_persons.id');
            }
            if($char_persons_islamic_commitment_cnt!= 0  || isset($filters['save_quran'])){
                $query->leftjoin('char_persons_islamic_commitment','char_persons_islamic_commitment.person_id','=','char_persons.id');
            }

            if($char_organization_persons_banks_cnt!= 0  || $char_persons_banks_cnt != 0) {
                $query->leftjoin('char_organization_persons_banks', function ($q) {
                    $q->on('char_organization_persons_banks.person_id', '=', 'char_persons.id');
                    $q->on('char_organization_persons_banks.organization_id', '=', 'char_cases.organization_id');
                })
                    ->leftjoin('char_persons_banks', function ($q) {
                        $q->on('char_persons_banks.id', '=', 'char_organization_persons_banks.bank_id');
                    });

            }
            if($primary_num_cnt != 0){
                $query->leftjoin('char_persons_contact as primary_num', function($q) use ($language_id){
                    $q->on('primary_num.person_id', '=', 'char_persons.id');
                    $q->where('primary_num.contact_type','primary_mobile');
                });
            }
            if($secondary_num_cnt != 0){
                $query->leftjoin('char_persons_contact as secondary_num', function($q) use ($language_id){
                    $q->on('secondary_num.person_id', '=', 'char_persons.id');
                    $q->where('secondary_num.contact_type','secondary_mobile');
                });
            }
            if($phone_num_cnt != 0){
                $query->leftjoin('char_persons_contact as phone_num', function($q) use ($language_id){
                    $q->on('phone_num.person_id', '=', 'char_persons.id');
                    $q->where('phone_num.contact_type','phone');
                });
            }

            if($guardian_residence_cnt!= 0 ){
                    $query->leftjoin('char_residence as guardian_residence','guardian_residence.person_id','=','g.id')   ;
                }
                if($guardian_cont1_cnt!= 0 ){
                    $query->leftjoin('char_persons_contact as guardian_cont1', function($q) use ($language_id){
                        $q->on('guardian_cont1.person_id', '=', 'g.id');
                        $q->where('guardian_cont1.contact_type','primary_mobile');
                    })      ;
                }

                if($guardian_cont3_cnt!= 0 ){
                    $query->leftjoin('char_persons_contact as guardian_cont3', function($q) use ($language_id){
                        $q->on('guardian_cont3.person_id', '=', 'g.id');
                        $q->where('guardian_cont3.contact_type','phone');
                    })        ;
                }
                if($guardian_education_cnt!= 0 ){
                    $query->leftjoin('char_persons_education as guardian_education','guardian_education.person_id','=','g.id');
                }
                if($guardian_work_cnt!= 0 ){
                    $query->leftjoin('char_persons_work as guardian_work','guardian_work.person_id','=','g.id')  ;
                }
                
                if($mother_health_cnt!= 0 ){
                        $query->leftjoin('char_persons_health as mother_health','mother_health.person_id','=','m.id');
                    }
                    if($mother_work_cnt != 0 ){
                        $query->leftjoin('char_persons_work as mother_work','mother_work.person_id','=','m.id');
                    }
                    if($mother_education_cnt!= 0 ){
                        $query->leftjoin('char_persons_education as mother_education','mother_education.person_id','=','m.id');
                    }
                    
                if($father_health_cnt!= 0 ){
                    $query->leftjoin('char_persons_health as father_health','father_health.person_id','=','f.id');
                }
                if($father_work_cnt   != 0 ){
                    $query->leftjoin('char_persons_work as father_work','father_work.person_id','=','f.id');
                }
                if($father_education_cnt!= 0 ){
                    $query->leftjoin('char_persons_education as father_education','father_education.person_id','=','f.id');
                }

            

             return $query->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name, 
                                     char_persons.id_card_number,char_persons.old_id_card_number,
                                         ca.name as category_name,
                                         char_persons.id_card_number,char_persons.old_id_card_number,
                                         org.name as organization_name,
                                         CASE WHEN char_sponsorship_cases.sponsor_number is null THEN ' ' Else char_sponsorship_cases.sponsor_number  END  AS sponsorship_number,
                                         CASE WHEN char_sponsorship_cases.sponsorship_date is null THEN ' ' Else char_sponsorship_cases.sponsorship_date  END  AS sponsorship_date,
                                         CASE
                                                          WHEN char_sponsorship_cases.status is null THEN ' '
                                                          WHEN char_sponsorship_cases.status = 1 THEN '$nominate'
                                                          WHEN char_sponsorship_cases.status = 2 THEN '$sent'
                                                          WHEN char_sponsorship_cases.status = 3 THEN '$guaranteed'
                                                          WHEN char_sponsorship_cases.status = 4 THEN '$stopped'
                                                          WHEN char_sponsorship_cases.status = 5 THEN '$paused'
                                                     END
                                                     AS status,
                                                    CASE WHEN m.id is null THEN ' '
                                                         Else CONCAT(m.first_name, ' ', m.second_name, ' ', m.third_name, ' ', m.last_name)  END
                                                    AS mother_name,
                                                   CASE WHEN f.id is null THEN ' '
                                                         Else CONCAT(f.first_name, ' ', f.second_name, ' ', f.third_name, ' ', f.last_name)  END
                                                    AS father_name,
                                                   CASE WHEN g.id is null THEN ' '
                                                         Else CONCAT(g.first_name, ' ', g.second_name, ' ', g.third_name, ' ', g.last_name)  END
                                                    AS guardian_name,
                                                    CASE WHEN (ca.case_is = 1 and ca.family_structure= 2) THEN char_get_family_count ( 'left',char_persons.father_id,null,char_persons.id )
                                                          WHEN (ca.case_is = 1 and ca.family_structure=1) THEN char_get_family_count ( 'left',char_persons.id,null,char_persons.id )
                                                          WHEN (ca.case_is = 2 and ca.family_structure=1) THEN char_get_family_count ( 'left',char_guardians.guardian_id,null,char_persons.id )
                                                    END AS brothers,
                                                     CASE WHEN char_persons.birthday is null THEN ' ' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                                     CASE
                                                          WHEN char_persons.gender is null THEN ' '
                                                          WHEN char_persons.gender = 1 THEN '$male'
                                                          WHEN char_persons.gender = 2 THEN '$female'
                                                     END
                                                     AS the_gender,
                                                     CASE WHEN f.birthday is null THEN ' ' Else  DATE_FORMAT(f.birthday,'%Y/%m/%d')  END  AS father_birthday,
                                                     CASE WHEN f.spouses is null THEN  ' ' Else  f.spouses END AS father_spouses,
                                                     CASE WHEN f.death_date is null THEN  '$is_alive' Else  '$deceased' END AS father_alive,
                                                     CASE WHEN f.death_date is null THEN ' '  Else f.death_date END AS father_death_date,                                                    
                                                     CASE WHEN m.birthday is null THEN ' ' Else  DATE_FORMAT(m.birthday,'%Y/%m/%d')  END  AS mother_birthday,
                                                     CASE WHEN mother_marital_status.name is null THEN ' ' Else mother_marital_status.name END as mother_marital_status,
                                                     CASE WHEN m.death_date is null THEN  '$is_alive' Else  '$deceased' END AS mother_alive,
                                                     CASE WHEN m.death_date is null THEN ' '  Else f.death_date END AS mother_death_date
                                                     
                             ")->get();
        

    }

    public static function getRecords($sponsorships,$sponsor_id,$id,$payment_bank,$payment_organization_id,$payment_target,$exchange_type){

        $return=\DB::table('char_cases')
            ->join('char_sponsorship_cases', function($q) use($sponsor_id,$sponsorships,$id) {
                $q->on('char_sponsorship_cases.case_id','=','char_cases.id')
                    ->where('char_sponsorship_cases.sponsor_id','=',$sponsor_id)
                    ->where('char_sponsorship_cases.status', 3)
                    ->wherein('char_sponsorship_cases.sponsorship_id', $sponsorships);
            })
            ->leftjoin('char_guardians', function($q) {
                $q->on('char_cases.person_id','=','char_guardians.individual_id');
                $q->on('char_guardians.organization_id','=','char_cases.organization_id');
                $q->where('char_guardians.status','=',1);
            })
            ->leftjoin('char_payments_cases', function($q) use($id) {
                $q->on('char_payments_cases.case_id','=','char_sponsorship_cases.case_id');
                $q->on('char_payments_cases.sponsor_number','=','char_sponsorship_cases.sponsor_number');
                $q->where('char_payments_cases.payment_id','=',$id);
            });

        if($payment_bank !=null && $exchange_type == 1){
//            if($payment_target ==1){
                $return=$return
                    ->leftjoin('char_persons_banks as guardian_banks', function($q) use($payment_bank){
                        $q->on('char_guardians.guardian_id','=','guardian_banks.person_id');
                        $q->where('guardian_banks.bank_id','=',$payment_bank);
                    })
//                    ->selectRaw('guardian_banks.bank_id as guardian_bank_id,guardian_banks.account_number as guardian_account_number') ;
////            }else{
//                $return=$return
                    ->leftjoin('char_persons_banks', function($q) use($payment_bank){
                        $q->on('char_cases.person_id','=','char_persons_banks.person_id');
                        $q->where('char_persons_banks.bank_id','=',$payment_bank);
                    })
                    ->selectRaw('char_persons_banks.bank_id as bank_id,char_persons_banks.account_number as account_number')
                    ->selectRaw("CASE WHEN char_payments_cases.recipient is null THEN char_persons_banks.bank_id
                                      WHEN char_payments_cases.recipient =0 THEN char_persons_banks.bank_id
                                      Else guardian_banks.bank_id  END  AS bank_id,
                                 CASE WHEN char_payments_cases.recipient is null THEN char_persons_banks.account_number
                                      WHEN char_payments_cases.recipient =0 THEN char_persons_banks.account_number
                                      Else guardian_banks.account_number  END  AS account_number") ;
//            }
        }

//        if($payment_target ==0){
//            $return=$return
//                ->leftjoin('char_payments_recipient', function($q) use($id) {
////                    $q->on('char_payments_recipient.person_id','=','char_cases.person_id');
//                    $q->where('char_payments_recipient.payment_id','=',$id);
//                })->selectRaw('char_payments_recipient.person_id as recipient_id');

//        }else{
//            $return=$return
//                ->leftjoin('char_payments_recipient', function($q) use($id) {
//                    $q->on('char_payments_recipient.person_id','=','char_cases.person_id');
//                    $q->where('char_payments_recipient.payment_id','=',$id);
//                })
//                ->selectRaw('char_payments_recipient.person_id as recipient_id');
//        }

        return $return
            ->where('char_cases.organization_id',$payment_organization_id)
            ->where('char_sponsorship_cases.sponsor_number','!=', "")
            ->whereNotIn('char_sponsorship_cases.sponsor_number', function($query)use($id){
                $query->select('char_payments_cases.sponsor_number')
                    ->from('char_payments_cases')
                    ->where('char_payments_cases.payment_id', $id);
            })
            ->groupBy('char_sponsorship_cases.sponsor_number','char_cases.id')
            ->selectRaw("get_payments_cases_count(char_cases.id,'$id',char_sponsorship_cases.sponsor_number) as payments,
                         char_cases.id as case_id ,
                         char_cases.person_id ,
                         char_guardians.guardian_id,
                         char_payments_cases.case_id as payment_case_id,
                         CASE WHEN char_payments_cases.recipient is null THEN 0 Else char_payments_cases.recipient  END  AS recipient,
                         char_sponsorship_cases.sponsor_number,
                         char_sponsorship_cases.sponsorship_id")
            ->get();

    }

    public static function sponsorshipCasesFilter($master,$page,$organization_id,$sponsor_id,$filters){

        $user = \Auth::user();
        $Org=Organization::findorfail($user->organization_id);
        $master=true;
        if($Org->parent_id){
            $master=false;
        }
        $char_cases=['category_id'];
        $char_sponsorship_cases=['status','sponsor_id'];

        $language_id =  \App\Http\Helpers::getLocale();

        $char_persons=['first_name','id_card_number','old_id_card_number','last_name','second_name','third_name'];
        $char_persons_father = ['father_id_card_number','father_old_id_card_number'];
        $char_persons_mother = ['mother_id_card_number','mother_old_id_card_number'];
        $char_persons_guardian=['guardian_id_card_number','guardian_old_id_card_number'];

        $condition=[];

        foreach ($filters as $key => $value) {
            if(in_array($key, $char_persons)) {
                if ( $value != "" ) {
                    $data = ['char_persons.' .$key => $value];
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $char_cases)) {
                if ( $value != "" ) {
                    $data = ['char_cases.'. $key => $value];
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $char_sponsorship_cases)) {
                if ( $value != "" ) {
                    $data = ['char_sponsorship_cases.'.$key => $value];
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $char_persons_father)) {
                if ( $value != "" ) {
                    $data = ['father.' . substr($key, 7) => (int)$value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_mother)) {
                if ( $value != "" ) {
                    $data = ['mother.' . substr($key, 7) => (int)$value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_persons_guardian)) {
                if ( $value != "" ) {
                    $data = ['guardian.' . substr($key, 9) => (int)$value];
                    array_push($condition, $data);
                }
            }
        }


        $query= \DB::table('char_cases')
            ->join('char_organizations as org', 'org.id', '=', 'char_cases.organization_id')
            ->join('char_categories as ca', function($q) use($organization_id) {
                    $q->on('ca.id','=','char_cases.category_id');
                    $q->where('ca.type','=',1);
                })
            ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
            ->leftjoin('char_persons AS father', 'char_persons.father_id', '=', 'father.id')
            ->leftjoin('char_persons AS mother', 'char_persons.mother_id', '=', 'mother.id')
            ->leftjoin('char_guardians', 'char_cases.person_id', '=', 'char_guardians.individual_id')
            ->leftjoin('char_persons AS guardian', 'char_guardians.guardian_id', '=', 'guardian.id')

            ->leftjoin('char_sponsorship_cases', function($q) use($organization_id) {
                $q->on('char_sponsorship_cases.case_id','=','char_cases.id');
//                $q->on('char_sponsorship_cases.organization_id','=','char_cases.organization_id');
            })
            ->leftjoin('char_sponsorships', 'char_sponsorships.id', '=', 'char_sponsorship_cases.sponsorship_id')
            ->leftjoin('char_organizations', 'char_organizations.id', '=', 'char_sponsorship_cases.sponsor_id');

        if($user->type == 2) {
            $query->where(function ($anq) use ($organization_id,$user) {
                $anq->where('char_cases.organization_id',$organization_id);
                $anq->orwherein('char_cases.organization_id', function($decq) use($user) {
                    $decq->select('organization_id')
                        ->from('char_user_organizations')
                        ->where('user_id', '=', $user->id);
                });
            });

        }else{
            $query->where(function ($anq) use ($organization_id) {
                /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                $anq->wherein('char_cases.organization_id', function($decq) use($organization_id) {
                    $decq->select('descendant_id')
                        ->from('char_organizations_closure')
                        ->where('ancestor_id', '=', $organization_id);
                });
            });
        }

        if (count($condition) != 0) {
            $query =  $query->where(function ($q) use ($condition) {
                    $names = ['char_persons.first_name', 'char_persons.second_name', 'char_persons.third_name', 'char_persons.last_name'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $names)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });
        }
//    CASE WHEN ((char_organizations.name is null or char_sponsorships.organization_id != '$organization_id') and $master != true) THEN ' '
//                                                  Else  char_organizations.name END AS sponsor_name,
//

        $male = trans('common::application.male');
        $female = trans('common::application.female');
        $not_nominate =  trans('sponsorship::application.not_nominated');
        $nominate =  trans('sponsorship::application.nominate');
        $sent =  trans('sponsorship::application.sent');
        $guaranteed =  trans('sponsorship::application.guaranteed');
        $stopped =  trans('sponsorship::application.stopped');
        $paused =  trans('sponsorship::application.paused');


        $query = $query->selectRaw("
                                    CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,
                                    ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))
                                    as name,char_persons.id_card_number,char_persons.old_id_card_number,
                                     char_cases.id as case_id,
                                     char_cases.category_id,
                                     char_cases.organization_id,
                                     char_persons.id,
                                     char_persons.father_id,
                                     char_persons.mother_id,
                                     CASE WHEN $language_id = 1 THEN ca.name Else ca.en_name END  AS category_name ,
                                     char_guardians.guardian_id,
                                     CASE  WHEN $language_id = 1 THEN org.name Else org.en_name END  AS organization_name,
                                      CASE
                                       WHEN char_persons.gender is null THEN ' '
                                       WHEN char_persons.gender = 1 THEN '$male'
                                       WHEN char_persons.gender = 2 THEN '$female'
                                 END
                                 AS gender,
                                     CASE WHEN char_sponsorship_cases.status is null THEN '$not_nominate'
                                          WHEN char_sponsorship_cases.status = 1 THEN '$nominate'
                                          WHEN char_sponsorship_cases.status = 2 THEN '$sent'
                                          WHEN char_sponsorship_cases.status = 3 THEN '$guaranteed'
                                          WHEN char_sponsorship_cases.status = 4 THEN '$stopped'
                                          WHEN char_sponsorship_cases.status = 5 THEN '$paused'
                                     END AS status,
                                      CASE WHEN char_sponsorship_cases.sponsor_number is null THEN ' '
                                         Else sponsor_number
                                     END AS sponsor_number
                                 "
        );

        if($user->super_admin || $master){

            if($language_id == 1){
                $query->selectRaw("CASE  WHEN char_organizations.name is null THEN ' ' Else char_organizations.name END AS sponsor_name");
            }else{
                $query->selectRaw("CASE  WHEN char_organizations.name is null THEN ' ' Else char_organizations.en_name END AS sponsor_name");
            }
        }else{

            if($language_id == 1){

                $query->selectRaw("CASE  WHEN (char_organizations.name is null or char_sponsorships.organization_id != '$organization_id') THEN ' ' Else 
                                     char_organizations.en_name END AS sponsor_name");
            }else{

                $query->selectRaw("CASE  WHEN (char_organizations.name is null or char_sponsorships.organization_id != '$organization_id') THEN ' ' Else 
                                     char_organizations.name END AS sponsor_name");
            }

        }
        $query->orderBy('char_persons.first_name','asc');
        $query->orderBy('char_persons.second_name','asc');
        $query->orderBy('char_persons.third_name','asc');
        $query->orderBy('char_persons.last_name','asc');

        $query->groupBy('char_cases.id','char_sponsorship_cases.sponsor_id','char_sponsorship_cases.sponsor_number');

        if($filters['action'] =='export'){
            return $query->get();
        }
        $itemsCount = isset($filters['itemsCount'])? $filters['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
        return $query->paginate($records_per_page); 

    }

    public static function getCaseInfo($id){

        return \DB::table('char_sponsorship_cases')
                ->join('char_cases','char_cases.id','char_sponsorship_cases.case_id')
                ->join('char_categories','char_categories.id', 'char_cases.category_id')
                ->join('char_organizations','char_organizations.id', '=','char_sponsorship_cases.sponsor_id')
                ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
                ->where('char_sponsorship_cases.id',$id)
                ->selectRaw("
                             char_sponsorship_cases.*,
                             char_organizations.name as sponsor_name,
                             char_categories.name as category_name,
                             CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,
                                    ifnull(char_persons.second_name, ' '),' ',
                                    ifnull(char_persons.third_name,' '),' ', 
                                    ifnull(char_persons.last_name,' '))as 
                                    full_name,
                                    char_persons.id_card_number,char_persons.old_id_card_number,
                                    char_cases.person_id,
                                    char_cases.category_id
                           ")
                ->first();
    }

    public static function getSponsorCasesList($sponsor_id,$category_id){

        $user = \Auth::user();
        $organization_id = $user->organization_id;

        return \DB::table('char_sponsorship_cases')
            ->join('char_cases', function($q) use($organization_id,$category_id){
                $q->on('char_cases.id', '=', 'char_sponsorship_cases.case_id');
                $q->where('char_cases.category_id',$category_id);
                $q->where('char_cases.organization_id',$organization_id);
            })
            ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
            ->where('char_sponsorship_cases.sponsor_id',$sponsor_id)
            ->groupBy('char_sponsorship_cases.id')
         ->selectRaw('CONCAT(ifnull(char_persons.first_name, " "), " " ,ifnull(char_persons.second_name, " ")," ",ifnull(char_persons.third_name," ")," ", ifnull(char_persons.last_name," "))as name,char_persons.id_card_number,char_persons.old_id_card_number,
                                     char_sponsorship_cases.id')->get();

    }

    public static function paginatePerson($id,$page,$itemsCount)
    {
        $nominate =  trans('sponsorship::application.nominate');
        $sent =  trans('sponsorship::application.sent');
        $guaranteed =  trans('sponsorship::application.guaranteed');
        $stopped =  trans('sponsorship::application.stopped');
        $paused =  trans('sponsorship::application.paused');
        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $query= \DB::table('char_sponsorship_cases')
            ->join('char_cases','char_cases.id','=','char_sponsorship_cases.case_id')
            ->join('char_persons','char_persons.id','=','char_cases.person_id')
            ->where('char_sponsorship_cases.sponsorship_id', '=', $id)
            ->selectRAW("char_sponsorship_cases.sponsorship_id,char_sponsorship_cases.id,
                                CASE WHEN char_sponsorship_cases.status = 1 THEN '$nominate'
                                        WHEN char_sponsorship_cases.status = 2 THEN '$sent'
                                        WHEN char_sponsorship_cases.status = 3 THEN '$guaranteed'
                                        WHEN char_sponsorship_cases.status = 4 THEN '$stopped'
                                        WHEN char_sponsorship_cases.status = 5 THEN '$paused'
                                    END AS status,
                                    char_sponsorship_cases.status as flag,
                         CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name,
                         CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
                         CASE WHEN char_persons.gender is null THEN ' ' WHEN char_persons.gender = 1 THEN '$male' WHEN char_persons.gender = 2 THEN '$female' END AS gender
                       ")
            ->orderBy('char_sponsorship_cases.created_at','desc');
            $itemsCount = isset($itemsCount)? $itemsCount:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
            return $query=$query->paginate($records_per_page);    
    }

    public static function checkISponsorNumber($case_id,$sponsor_id,$sponsor_number){

        $founded= \DB::table('char_sponsorship_cases')
            ->join('char_cases', 'char_cases.id', '=', 'char_sponsorship_cases.case_id')
            ->where('char_sponsorship_cases.sponsor_number', $sponsor_number)
            ->where('char_sponsorship_cases.sponsor_id', $sponsor_id)
            ->selectRaw('char_sponsorship_cases.case_id')->first();

        if($founded){
            return ['status' => false];
        }
        return ['status' => true];
    }

}
