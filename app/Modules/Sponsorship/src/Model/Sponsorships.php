<?php
namespace Sponsorship\Model;
use App\Http\Helpers;

class Sponsorships  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_sponsorships';
    protected $primaryKey = 'id';
    protected $fillable = ['organization_id','sponsor_id','status','category_id','user_id'];
    protected $hidden = ['created_at','updated_at'];
//    public $timestamps = false;
    public function category()
    {
        return $this->belongsTo('Setting\Model\Categories','category_id','id');
    }
    public function Organizations()
    {
        return $this->belongsTo('Organization\Model\Organization','sponsor_id','id');
    }
    public function SponsorshipsPerson()
    {
        return $this->hasMany('Sponsorship\Model\SponsorshipsPerson','sponsorship_id','id');
    }
    public function Payments()
    {
        return $this->hasMany('Sponsorship\Model\Payments', 'sponsorship_id', 'id');
    }

    public static function fetch($id)
    {
        return \DB::table('char_sponsorships')
                  ->join('char_organizations as org','org.id','=','char_sponsorships.sponsor_id')
                  ->join('char_categories as cat','cat.id','=','char_sponsorships.category_id')
                  ->selectRaw('char_sponsorships.*,cat.name as category_name,org.name as sponsor_name')
                  ->where('char_sponsorships.id','=',$id)
                  ->first();
    }
    public static function filterSponsorships($organization_id,$filters,$page)
    {
        $condition = [];
        $char_sponsorships = ['sponsorship_number', 'sponsor_id', 'category_id', 'sponsorship_date'];
        $int = ['sponsorship_number', 'sponsor_id', 'category_id'];

        foreach ($filters as $key => $value) {
            $data=[];
            if(in_array($key, $char_sponsorships)) {
                if ($value != "" ) {
                    if(in_array($key, $int)) {
                        array_push($condition, ['char_sponsorships.' . $key => (int) $value]);
                    }

                }
            }
        }

        $draft    = trans('sponsorship::application.draft');
        $finished = trans('sponsorship::application.finished');
        $sent     = trans('sponsorship::application.sent');

        $result = \DB::table('char_sponsorships')
            ->leftJoin('char_sponsorships_persons', 'char_sponsorships.id', '=', 'char_sponsorships_persons.sponsorship_id')
            ->join('char_organizations as org','org.id','=','char_sponsorships.sponsor_id')
            ->join('char_categories as cat','cat.id','=','char_sponsorships.category_id')
            ->selectRaw("char_sponsorships.*,
                                         DATE_FORMAT(char_sponsorships.created_at,'%m-%d-%Y') as date,
                                         org.name as sponsor,
                                         cat.name as sponsorship_type,
                                         CASE
                                              WHEN char_sponsorships.status = 1 THEN '$draft'
                                              WHEN char_sponsorships.status = 2 THEN '$sent'
                                              WHEN char_sponsorships.status = 3 THEN '$finished'
                                         END
                                         AS status,
                                         count(char_sponsorships_persons.person_id) as nominationCount");

        $all_organization=null;
        if(isset($filters['all_organization'])){
            if($filters['all_organization'] ===true){
                $all_organization=0;
            }elseif($filters['all_organization'] ===false){
                $all_organization=1;

            }
        }
        if($all_organization ==0){
            $organizations=[];
            if(isset($filters['organization_ids']) && $filters['organization_ids'] !=null && $filters['organization_ids'] !="" && $filters['organization_ids'] !=[] && sizeof($filters['organization_ids']) != 0) {
                if($filters['organization_ids'][0]==""){
                    unset($filters['organization_ids'][0]);
                }
                $organizations =$filters['organization_ids'];
            }
            if(sizeof($organizations) > 0 ){
                $result->wherein('char_sponsorships.organization_id',$organizations);
            }else{

                $user = \Auth::user();
                if($user->type == 2) {
                    $result->where(function ($anq) use ($organization_id,$user) {
                        $anq->where('char_sponsorships.organization_id',$organization_id);
                        $anq->orwherein('char_sponsorships.organization_id', function($decq) use($user) {
                            $decq->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });

                }else{
                    $result->where(function ($anq) use ($organization_id) {
                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                        $anq->wherein('char_sponsorships.organization_id', function($decq) use($organization_id) {
                            $decq->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $organization_id);
                        });
                    });
                }


            }
        }
        elseif($all_organization ==1){
            $result->where('char_sponsorships.organization_id','=',$organization_id);
        }

        if (count($condition) != 0) {
            $result =  $result
                ->where(function ($q) use ($condition) {
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            $q->where($key, '=', $value);
                        }
                    }
                });
        }

        $start_sponsorship_date=null;
        $end_sponsorship_date=null;


        if(isset($filters['start_sponsorship_date']) && $filters['start_sponsorship_date'] !=null){
            $start_sponsorship_date=date('Y-m-d',strtotime($filters['start_sponsorship_date']));
        }
        if(isset($filters['end_sponsorship_date']) && $filters['end_sponsorship_date'] !=null){
            $end_sponsorship_date=date('Y-m-d',strtotime($filters['end_sponsorship_date']));
        }

        if($start_sponsorship_date != null && $end_sponsorship_date != null) {
            $result = $result->whereBetween( 'char_sponsorships.created_at', [ $start_sponsorship_date, $end_sponsorship_date]);

        }elseif($start_sponsorship_date != null && $end_sponsorship_date == null) {
            $result = $result->whereDate('char_sponsorships.created_at', '>=', $start_sponsorship_date);
        }elseif($start_sponsorship_date == null && $end_sponsorship_date != null) {
            $result = $result->whereDate('char_sponsorships.created_at', '<=', $end_sponsorship_date);
        }


        $max_beneficiary_no=null;
        $min_beneficiary_no=null;

        if(isset($filters['max_beneficiary_no']) && $filters['max_beneficiary_no'] !=null && $filters['max_beneficiary_no'] !=""){
            $max_beneficiary_no=$filters['max_beneficiary_no'];
        }
        if(isset($filters['min_beneficiary_no']) && $filters['min_beneficiary_no'] !=null && $filters['min_beneficiary_no'] !=""){
            $min_beneficiary_no=$filters['min_beneficiary_no'];
        }

        if($max_beneficiary_no != null && $min_beneficiary_no != null) {
            $result = $result->whereRaw("$min_beneficiary_no <= (SELECT COUNT(*) FROM char_sponsorships_persons WHERE char_sponsorships_persons.sponsorship_id = char_sponsorships.id)");
            $result = $result->whereRaw("$max_beneficiary_no >= (SELECT COUNT(*) FROM char_sponsorships_persons WHERE char_sponsorships_persons.sponsorship_id = char_sponsorships.id)");

        }elseif($max_beneficiary_no != null && $min_beneficiary_no == null) {
            $result = $result->whereRaw(" $max_beneficiary_no >= (SELECT COUNT(*) FROM char_sponsorships_persons WHERE char_sponsorships_persons.sponsorship_id = char_sponsorships.id)");
        }elseif($max_beneficiary_no == null && $min_beneficiary_no != null) {
            $result = $result->whereRaw(" $min_beneficiary_no <= (SELECT COUNT(*) FROM char_sponsorships_persons WHERE char_sponsorships_persons.sponsorship_id = char_sponsorships.id)");
        }

        $result = $result->groupBy('char_sponsorships.id');
        $itemsCount = isset($filters['itemsCount'])? $filters['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$result->count());
         return $result->paginate($records_per_page);  

    }
    public static function getList($sponsor_id,$organization_id)
    {
        $result = \DB::table('char_sponsorships')
            ->leftJoin('char_sponsorships_persons', 'char_sponsorships.id', '=', 'char_sponsorships_persons.sponsorship_id')
            ->join('char_organizations as sponsor','sponsor.id','=','char_sponsorships.sponsor_id')
            ->join('char_categories','char_categories.id','=','char_sponsorships.category_id')
            ->selectRaw('char_sponsorships.*,char_categories.name as category_name,sponsor.name as sponsor_name');

        if($sponsor_id != -1){
          $result->where('char_sponsorships.organization_id','=',$organization_id);
          $result->where('char_sponsorships.sponsor_id','=',$sponsor_id);
        }else{
            $result->where('char_sponsorships.sponsor_id','=',$organization_id);
        }

        return  $result->groupBy('char_sponsorships.id')->get();
    }
}
