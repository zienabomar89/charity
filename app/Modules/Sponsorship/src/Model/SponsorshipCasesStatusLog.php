<?php
namespace Sponsorship\Model;

class SponsorshipCasesStatusLog  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_sponsorship_cases_status_log';
    protected $primaryKey = 'id';
    protected $fillable = ['sponsorship_cases_id', 'date','status','user_id'];
    protected $hidden = ['created_at','updated_at'];

    public static function getLogs($id)
    {
        $nominate =  trans('sponsorship::application.nominate');
        $sent =  trans('sponsorship::application.sent');
        $guaranteed =  trans('sponsorship::application.guaranteed');
        $stopped =  trans('sponsorship::application.stopped');
        $paused =  trans('sponsorship::application.paused');


        return  \DB::table('char_sponsorship_cases_status_log')
                ->where('char_sponsorship_cases_status_log.sponsorship_cases_id',$id)
                ->selectRaw("char_sponsorship_cases_status_log.*,
                             char_sponsorship_cases_status_log.created_at,
                                  CASE WHEN char_sponsorship_cases_status_log.status = 1 THEN '$nominate'
                                  WHEN char_sponsorship_cases_status_log.status = 2 THEN '$sent'
                                  WHEN char_sponsorship_cases_status_log.status = 3 THEN '$guaranteed'
                                  WHEN char_sponsorship_cases_status_log.status = 4 THEN '$stopped'
                                  WHEN char_sponsorship_cases_status_log.status = 5 THEN '$paused'
                             END
                             AS falg
                        ")
                ->paginate(config('constants.records_per_page'));
    }
}