<?php

namespace Sponsorship\Model;

class Person  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_persons';
    protected $primaryKey = 'id';
    protected $fillable = ['first_name','second_name','third_name', 'last_name','birthday','id_card_number','gender'
        ,'monthly_income','spouses','death_date', 'death_cause_id','nationality','prev_family_name','card_type'
        ,'birth_place','location_id','street_address','death_date','father_id','mother_id','marital_status_id','city','country','governarate',];
    public $timestamps = false;

    public function PersonsEducation()
    {
        return $this->hasOne('Common\Model\PersonModels\PersonEducation','person_id', 'id');

    }
    public function c_education()
    {
        return $this->morphMany('Common\Model\PersonModels\PersonEducation', 'EducationCases');
    }
    public function PersonsWork()
    {
        return $this->hasOne('Common\Model\PersonModels\PersonWork','person_id', 'id');
    }
    public function DeathCauses()
    {
        return $this->belongsTo('Setting\Model\DeathCauseI18n','death_cause_id','death_cause_id');

    }
    public function MaritalStatus()
    {

        return $this->belongsTo('Setting\Model\MaritalStatusI18n','marital_status_id','marital_status_id');
    }
    public function PersonsHealth()
    {
        return $this->hasOne('Common\Model\PersonModels\PersonHealth','person_id', 'id');
    }
    public function PersonResidence()
    {
        return $this->hasOne('Sponsorship\Model\PersonResidence','person_id', 'id');
    }
    public function Kinship()
    {
        return $this->belongsToMany('Setting\Model\KinshipI18n','char_persons_kinship','r_person_id','kinship_id');
    }
    public function Guardians()
    {
        return $this->hasMany('Sponsorship\Model\Guardians','individual_id','id');
    }
    public function PersonContact()
    {
        return $this->hasMany('Sponsorship\Model\PersonContact','person_id', 'id');
    }
    public function PersonIslamic()
    {
        return $this->hasOne('Sponsorship\Model\PersonIslamic','person_id', 'id');
    }
    public function Cases()
    {
        return $this->hasOne('Sponsorship\Model\Cases', 'person_id', 'id');
    }
    public function PersonsI18n()
    {
        return $this->hasMany('Common\Model\PersonModels\PersonI18n', 'person_id', 'id');
    }
    public function maritalStatusId()
    {
        return $this->belongsTo('Setting\Model\MaritalStatusI18n','marital_status_id','marital_status_id');
    }
    public function SponsorshipsPerson()
    {
        return $this->belongsTo('Sponsorship\Model\SponsorshipsPerson', 'id', 'person_id');
    }

/***********************************************************************************************/
    public static function getFamilyMember($action,$person,$l_person_id,$mother,$organization_id,$has_wive)
    {

        $language_id =  \App\Http\Helpers::getLocale();

        $husband_kinship_id = null;
        if($has_wive){
            $husband_kinship=\Setting\Model\Setting::where('id','husband_kinship')->first();
            if($husband_kinship){
                if(!is_null($husband_kinship->value) and $husband_kinship->value != "") {
                    $husband_kinship_id=$husband_kinship->value;
                }
            }
        }


        $return=['fm'=>[],'fm_cases'=>[]];

        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $refugee = trans('common::application.refugee');
        $citizen = trans('common::application.citizen');

        $not_exist = trans('common::application.not_exist');
        $exist = trans('common::application.exist');

        $not_work = trans('common::application.not_work');
        $work = trans('common::application.work');

        $is_alive = trans('common::application.is_alive');
        $deceased = trans('common::application.deceased');

        $vocational = trans('common::application.vocational');
        $academic = trans('common::application.academic');

        $perfect = trans('common::application.perfect');
        $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
        $special_needs = trans('common::application.special_needs');

        $excellent = trans('common::application.excellent');
        $acceptable = trans('common::application.acceptable');

        $good = trans('common::application.good');
        $very_good = trans('common::application.very_good');
        $weak = trans('common::application.weak');
        $very_bad_condition = trans('common::application.very_bad_condition');
        $bad_condition = trans('common::application.bad_condition');

        $the_first  = trans('common::application.the_first') ;
        $the_second = trans('common::application.the_second') ;
        $the_third  = trans('common::application.the_third') ;
        $the_fourth = trans('common::application.the_fourth') ;
        $the_Fifth  = trans('common::application.the_Fifth');
        $the_Sixth  = trans('common::application.the_Sixth');
        $the_Seventh = trans('common::application.the_Seventh');
        $the_Eighth= trans('common::application.the_Eighth');
        $the_Ninth = trans('common::application.the_Ninth');
        $the_tenth = trans('common::application.the_tenth');
        $the_eleventh = trans('common::application.the_eleventh');
        $the_twelveth= trans('common::application.the_twelveth');

        $special_studies= trans('common::application.special_studies');
        $vocational_training= trans('common::application.vocational_training');
        $first_year_of_university= trans('common::application.first_year_of_university');
        $second_year_of_university= trans('common::application.second_year_of_university');
        $third_year_of_university= trans('common::application.third_year_of_university');
        $fourth_year_of_university= trans('common::application.fourth_year_of_university');
        $fifth_year_of_university= trans('common::application.fifth_year_of_university');

        $regularly = trans('common::application.regularly');
        $does_not_pray = trans('common::application.does_not_pray');
        $sometimes = trans('common::application.sometimes');

        $is_alive = trans('common::application.is_alive');
        $deceased = trans('common::application.deceased');

        $is_can_work = trans('common::application.is_can_work');
        $not_can_work = trans('common::application.not_can_work');

        if($action == 'export'){
            if($person != null){
                if($l_person_id ==null && $mother== null) {
                    $return=\DB::table('char_persons')
                        ->join('char_persons_kinship', function($q) use($person){
                            $q->on('char_persons_kinship.r_person_id', '=', 'char_persons.id');
                            $q->where('char_persons_kinship.l_person_id', '=', $person);
                        })
                        ->leftjoin('char_kinship_i18n', function($q) use ($language_id){
                            $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                            $q->where('char_kinship_i18n.language_id',$language_id);
                        })
                        ->leftjoin('char_locations_i18n As L', function($q) use ($language_id) {
                            $q->on('L.location_id', '=', 'char_persons.birth_place');
                            $q->where('L.language_id',$language_id);
                        })
                        ->leftjoin('char_locations_i18n As L0', function($q)  use ($language_id){
                            $q->on('L0.location_id', '=', 'char_persons.nationality');
                            $q->where('L0.language_id',$language_id);
                        })
                        ->leftjoin('char_persons_i18n', function($q)  use ($language_id){
                            $q->on('char_persons_i18n.person_id', '=', 'char_persons.id');
                            $q->where('char_persons_i18n.language_id','2');
                        })
                        ->leftjoin('char_persons_health', function($q) {
                            $q->on('char_persons_health.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_diseases_i18n', function($q)  use ($language_id){
                            $q->on('char_diseases_i18n.disease_id', '=', 'char_persons_health.disease_id');
                            $q->where('char_diseases_i18n.language_id',$language_id);
                        })
                        ->leftjoin('char_persons_islamic_commitment', function($q) {
                            $q->on('char_persons_islamic_commitment.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_persons_education', function($q) {
                            $q->on('char_persons_education.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_edu_stages_i18n as char_edu_stages', function($q)  use ($language_id){
                            $q->on('char_edu_stages.edu_stage_id', '=', 'char_persons_education.stage');
                            $q->where('char_edu_stages.language_id',$language_id);
                        })
                        ->leftjoin('char_edu_authorities_i18n as char_edu_authorities', function($q)  use ($language_id){
                            $q->on('char_edu_authorities.edu_authority_id', '=', 'char_persons_education.authority');
                            $q->where('char_edu_authorities.language_id',$language_id);
                        })
                        ->leftjoin('char_edu_degrees_i18n as char_edu_degrees', function($q)  use ($language_id){
                            $q->on('char_edu_degrees.edu_degree_id', '=', 'char_persons_education.degree');
                            $q->where('char_edu_degrees.language_id',$language_id);
                        })
                        ->where(function ($q) use ($person,$husband_kinship_id) {
                            $q->where('char_persons.id', '!=', $person);

                            if(!is_null($husband_kinship_id)){
                                $q->where('char_persons_kinship.kinship_id', '!=', $husband_kinship_id);
                            }
                        })->selectRaw("
                                                    CASE WHEN char_persons_kinship.kinship_id is null THEN '-' Else char_kinship_i18n.name  END  AS kinship,
                                                    CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                                ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name,
                                                    CONCAT(ifnull(char_persons_i18n.first_name, ' '), ' ' ,ifnull(char_persons_i18n.second_name, ' '),' ',
                                                ifnull(char_persons_i18n.third_name, ' '),' ', ifnull(char_persons_i18n.last_name,' '))AS en_name,
                                                    char_persons.id_card_number,char_persons.old_id_card_number,
                                                    CASE WHEN char_persons.birthday is null THEN '-' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                                    CASE
                                                    WHEN char_persons.gender is null THEN '-'
                                                    WHEN char_persons.gender = 1 THEN '$male'
                                                    WHEN char_persons.gender = 2 THEN '$female'
                                                    END
                                                    AS the_gender,
                                                    CASE WHEN char_persons.birth_place is null THEN '-' Else L.name END AS birth_place,
                                                    CASE WHEN char_persons.nationality is null THEN '-' Else L0.name END AS nationality,
                                                    CASE
                                                            WHEN char_persons_health.condition is null THEN '-'
                                                            WHEN char_persons_health.condition = 1 THEN '$perfect'
                                                                  WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                                  WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                                    END
                                                    AS health_status,
                                                    CASE WHEN char_persons_health.details is null THEN '-' Else char_persons_health.details END   AS details,
                                                    CASE WHEN char_diseases_i18n.name is null THEN '-' Else char_diseases_i18n.name END   AS disease_id,
                                                    CASE
                                                    WHEN char_persons_education.study is null THEN '-'
                                                    WHEN char_persons_education.study = 1 THEN '$yes'
                                                    WHEN char_persons_education.study = 0 THEN '$no'
                                                    END
                                                    AS study,
                                                    CASE  WHEN char_persons_education.type is null THEN '-'
                                                          WHEN char_persons_education.type = 1 THEN '$vocational'
                                                          WHEN char_persons_education.type = 2 THEN '$academic'
                                                    END
                                                    AS type,
                                                    CASE
                                                       WHEN char_persons_education.level is null THEN ' '
                                                       WHEN char_persons_education.level = 1 THEN '$weak'
                                                       WHEN char_persons_education.level = 2 THEN '$acceptable'
                                                       WHEN char_persons_education.level = 3 THEN '$good'
                                                       WHEN char_persons_education.level = 4 THEN '$very_good'
                                                       WHEN char_persons_education.level = 5 THEN '$excellent'
                                                    END
                                                    AS level,
                                                    CASE
                                                               WHEN char_persons_education.grade is null THEN ' '
                                                               WHEN char_persons_education.grade = 1 THEN '$the_first '
                                                               WHEN char_persons_education.grade = 2 THEN '$the_second '
                                                               WHEN char_persons_education.grade = 3 THEN '$the_third '
                                                               WHEN char_persons_education.grade = 4 THEN '$the_fourth '
                                                               WHEN char_persons_education.grade = 5 THEN '$the_Fifth '
                                                               WHEN char_persons_education.grade = 6 THEN '$the_Sixth '
                                                               WHEN char_persons_education.grade = 7 THEN '$the_Seventh '
                                                               WHEN char_persons_education.grade = 8 THEN '$the_Eighth '
                                                               WHEN char_persons_education.grade = 9 THEN '$the_Ninth '
                                                               WHEN char_persons_education.grade = 10 THEN '$the_tenth'
                                                               WHEN char_persons_education.grade = 11 THEN '$the_eleventh'
                                                               WHEN char_persons_education.grade = 12 THEN '$the_twelveth'
                                                               WHEN char_persons_education.grade = 13 THEN '$special_studies'
                                                               WHEN char_persons_education.grade = 14 THEN '$vocational_training'
                                                               WHEN char_persons_education.grade = 15 THEN '$first_year_of_university'
                                                               WHEN char_persons_education.grade = 16 THEN '$second_year_of_university'
                                                               WHEN char_persons_education.grade = 17 THEN '$third_year_of_university'
                                                               WHEN char_persons_education.grade = 18 THEN '$fourth_year_of_university'
                                                               WHEN char_persons_education.grade = 19 THEN '$fifth_year_of_university'                                         
                                                     END
                                                     AS grade,
                                                    CASE WHEN char_persons_education.year is null THEN '-'   Else char_persons_education.year END  AS year,
                                                    CASE WHEN char_persons_education.points is null THEN '-' Else char_persons_education.points END  AS points,
                                                    CASE WHEN char_persons_education.school is null THEN '-' Else char_persons_education.school END  AS school,
                                                    CASE WHEN char_edu_authorities.name is null THEN '-' Else char_edu_authorities.name END as authority,
                                                    CASE WHEN char_edu_stages.name is null THEN '-' Else char_edu_stages.name END as stage,
                                                    CASE WHEN char_edu_degrees.name is null THEN '-' Else char_edu_degrees.name END as degree,
                                                    CASE WHEN char_persons_islamic_commitment.prayer is null THEN ' '
                                                         WHEN char_persons_islamic_commitment.prayer = 0 THEN '$regularly'
                                                         WHEN char_persons_islamic_commitment.prayer = 1 THEN '$does_not_pray'
                                                         WHEN char_persons_islamic_commitment.prayer = 2 THEN '$sometimes'
                                                    END AS prayer,
                                                    CASE
                                                        WHEN char_persons_islamic_commitment.quran_parts is null THEN '-'
                                                        WHEN char_persons_islamic_commitment.quran_parts = 0 THEN '$no'
                                                    Else '$yes'
                                                    END
                                                    AS save_quran, 
                                                    CASE
                                                        WHEN char_persons_islamic_commitment.quran_center is null THEN '-'
                                                        WHEN char_persons_islamic_commitment.quran_center = 0 THEN '$no'
                                                    Else '$yes'
                                                    END
                                                    AS quran_center,
                                                    CASE WHEN char_persons_islamic_commitment.prayer_reason is null THEN '-' Else  char_persons_islamic_commitment.prayer_reason END AS prayer_reason,
                                                    CASE WHEN char_persons_islamic_commitment.quran_reason is null  THEN '-' Else char_persons_islamic_commitment.quran_reason END AS quran_reason,
                                                    CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN '-' Else char_persons_islamic_commitment.quran_parts END  AS quran_parts,
                                                    CASE WHEN char_persons_islamic_commitment.quran_chapters is null  THEN '-' Else char_persons_islamic_commitment.quran_chapters END AS quran_chapters
                                        ")->get();

                }
                if($l_person_id !=null && $mother== null) {
                    $return=\DB::table('char_persons')
                        ->join('char_persons_kinship', function($q) use($l_person_id){
                            $q->on('char_persons_kinship.r_person_id', '=', 'char_persons.id');
                            $q->where('char_persons_kinship.l_person_id', '=', $l_person_id);
                        })
                        ->leftjoin('char_kinship_i18n', function($q) use ($language_id){
                            $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                            $q->where('char_kinship_i18n.language_id',$language_id);
                        })
                        ->leftjoin('char_locations_i18n As L', function($q)  use ($language_id){
                            $q->on('L.location_id', '=', 'char_persons.birth_place');
                            $q->where('L.language_id',$language_id);
                        })
                        ->leftjoin('char_locations_i18n As L0', function($q)  use ($language_id){
                            $q->on('L0.location_id', '=', 'char_persons.nationality');
                            $q->where('L0.language_id',$language_id);
                        })
                        ->leftjoin('char_persons_i18n', function($q) {
                            $q->on('char_persons_i18n.person_id', '=', 'char_persons.id');
                            $q->where('char_persons_i18n.language_id','2');
                        })
                        ->leftjoin('char_persons_health', function($q) {
                            $q->on('char_persons_health.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_diseases_i18n', function($q)  use ($language_id){
                            $q->on('char_diseases_i18n.disease_id', '=', 'char_persons_health.disease_id');
                            $q->where('char_diseases_i18n.language_id',$language_id);
                        })
                        ->leftjoin('char_persons_islamic_commitment', function($q) {
                            $q->on('char_persons_islamic_commitment.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_persons_education', function($q) {
                            $q->on('char_persons_education.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_edu_stages_i18n as char_edu_stages', function($q)  use ($language_id){
                            $q->on('char_edu_stages.edu_stage_id', '=', 'char_persons_education.stage');
                            $q->where('char_edu_stages.language_id',$language_id);
                        })
                        ->leftjoin('char_edu_authorities_i18n as char_edu_authorities', function($q) use ($language_id) {
                            $q->on('char_edu_authorities.edu_authority_id', '=', 'char_persons_education.authority');
                            $q->where('char_edu_authorities.language_id',$language_id);
                        })
                        ->leftjoin('char_edu_degrees_i18n as char_edu_degrees', function($q) use ($language_id){
                            $q->on('char_edu_degrees.edu_degree_id', '=', 'char_persons_education.degree');
                            $q->where('char_edu_degrees.language_id',$language_id);
                        })
                        ->where(function ($q) use ($person,$husband_kinship_id,$l_person_id) {
                            $q->whereNotIn('char_persons.id', [$person,$l_person_id]);

                            if(!is_null($husband_kinship_id)){
                                $q->where('char_persons_kinship.kinship_id', '!=', $husband_kinship_id);
                            }
                        })
                        ->selectRaw("
                                   CASE WHEN char_persons_kinship.kinship_id is null THEN '-' Else char_kinship_i18n.name  END  AS kinship,
                                    CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name,
                                    CONCAT(ifnull(char_persons_i18n.first_name, ' '), ' ' ,ifnull(char_persons_i18n.second_name, ' '),' ',
                                ifnull(char_persons_i18n.third_name, ' '),' ', ifnull(char_persons_i18n.last_name,' ')) AS en_name,
                                    char_persons.id_card_number,char_persons.old_id_card_number,
                                    CASE WHEN char_persons.birthday is null THEN '-' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                    CASE
                                    WHEN char_persons.gender is null THEN '-'
                                    WHEN char_persons.gender = 1 THEN '$male'
                                    WHEN char_persons.gender = 2 THEN '$female'
                                    END
                                    AS the_gender,
                                    CASE WHEN char_persons.birth_place is null THEN '-' Else L.name END AS birth_place,
                                    CASE WHEN char_persons.nationality is null THEN '-' Else L0.name END AS nationality,
                                    CASE WHEN char_persons_health.condition is null THEN '-'
                                         WHEN char_persons_health.condition = 1 THEN '$perfect'
                                         WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                         WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                    END AS health_status,
                                    CASE WHEN char_persons_health.details is null THEN '-' Else char_persons_health.details END   AS details,
                                    CASE WHEN char_diseases_i18n.name is null THEN '-' Else char_diseases_i18n.name END   AS disease_id,
                                    CASE
                                    WHEN char_persons_education.study is null THEN '-'
                                    WHEN char_persons_education.study = 1 THEN '$yes'
                                    WHEN char_persons_education.study = 0 THEN '$no'
                                    END
                                    AS study,
                                                                                         CASE
                                                               WHEN char_persons_education.type is null THEN '-'
                                                               WHEN char_persons_education.type = 1 THEN '$vocational'
                                                               WHEN char_persons_education.type = 2 THEN '$academic'
                                                    END
                                                     AS type,
                                                     CASE
                                                               WHEN char_persons_education.level is null THEN ' '
                                                               WHEN char_persons_education.level = 1 THEN '$weak'
                                                               WHEN char_persons_education.level = 2 THEN '$acceptable'
                                                               WHEN char_persons_education.level = 3 THEN '$good'
                                                               WHEN char_persons_education.level = 4 THEN '$very_good'
                                                               WHEN char_persons_education.level = 5 THEN '$excellent'
                                                     END
                                                     AS level,
                                    CASE
                                    WHEN char_persons_education.grade is null THEN '-'
                                                               WHEN char_persons_education.grade = 1 THEN '$the_first '
                                                               WHEN char_persons_education.grade = 2 THEN '$the_second '
                                                               WHEN char_persons_education.grade = 3 THEN '$the_third '
                                                               WHEN char_persons_education.grade = 4 THEN '$the_fourth '
                                                               WHEN char_persons_education.grade = 5 THEN '$the_Fifth '
                                                               WHEN char_persons_education.grade = 6 THEN '$the_Sixth '
                                                               WHEN char_persons_education.grade = 7 THEN '$the_Seventh '
                                                               WHEN char_persons_education.grade = 8 THEN '$the_Eighth '
                                                               WHEN char_persons_education.grade = 9 THEN '$the_Ninth '
                                                               WHEN char_persons_education.grade = 10 THEN '$the_tenth'
                                                               WHEN char_persons_education.grade = 11 THEN '$the_eleventh'
                                                               WHEN char_persons_education.grade = 12 THEN '$the_twelveth'
                                                               WHEN char_persons_education.grade = 13 THEN '$special_studies'
                                                               WHEN char_persons_education.grade = 14 THEN '$vocational_training'
                                                               WHEN char_persons_education.grade = 15 THEN '$first_year_of_university'
                                                               WHEN char_persons_education.grade = 16 THEN '$second_year_of_university'
                                                               WHEN char_persons_education.grade = 17 THEN '$third_year_of_university'
                                                               WHEN char_persons_education.grade = 18 THEN '$fourth_year_of_university'
                                                               WHEN char_persons_education.grade = 19 THEN '$fifth_year_of_university' 
                                                               END
                                    AS grade,
                                    CASE WHEN char_persons_education.year is null THEN '-'   Else char_persons_education.year END  AS year,
                                    CASE WHEN char_persons_education.points is null THEN '-' Else char_persons_education.points END  AS points,
                                    CASE WHEN char_persons_education.school is null THEN '-' Else char_persons_education.school END  AS school,
                                    CASE WHEN char_edu_authorities.name is null THEN '-' Else char_edu_authorities.name END as authority,
                                    CASE WHEN char_edu_stages.name is null THEN '-' Else char_edu_stages.name END as stage,
                                    CASE WHEN char_edu_degrees.name is null THEN '-' Else char_edu_degrees.name END as degree,
                                    CASE  WHEN char_persons_islamic_commitment.prayer is null THEN '-'
                                          WHEN char_persons_islamic_commitment.prayer = 0 THEN '$regularly'
                                          WHEN char_persons_islamic_commitment.prayer = 1 THEN '$does_not_pray'
                                          WHEN char_persons_islamic_commitment.prayer = 2 THEN '$sometimes'
                                    END  AS prayer,
                                    CASE
                                    WHEN char_persons_islamic_commitment.quran_parts is null THEN '-'
                                    WHEN char_persons_islamic_commitment.quran_parts = 0 THEN '$no'
                                    Else '$yes'
                                    END
                                    AS save_quran,
                                     CASE
                                    WHEN char_persons_islamic_commitment.quran_center is null THEN '-'
                                    WHEN char_persons_islamic_commitment.quran_center = 0 THEN '$no'
                                    Else '$yes'
                                    END
                                    AS quran_center,
                                    CASE WHEN char_persons_islamic_commitment.prayer_reason is null THEN '-' Else  char_persons_islamic_commitment.prayer_reason END AS prayer_reason,
                                    CASE WHEN char_persons_islamic_commitment.quran_reason is null  THEN '-' Else char_persons_islamic_commitment.quran_reason END AS quran_reason,
                                    CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN '-' Else char_persons_islamic_commitment.quran_parts END  AS quran_parts,
                                    CASE WHEN char_persons_islamic_commitment.quran_chapters is null  THEN '-' Else char_persons_islamic_commitment.quran_chapters END AS quran_chapters
                                        ")->get();
                }
                if($l_person_id !=null && $mother != null) {
                    $return=\DB::table('char_persons')
                        ->join('char_persons_kinship', function($q) use($l_person_id){
                            $q->on('char_persons_kinship.r_person_id', '=', 'char_persons.id');
                            $q->where('char_persons_kinship.l_person_id', '=', $l_person_id);
                        })
                        ->leftjoin('char_kinship_i18n', function($q) use ($language_id){
                            $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                            $q->where('char_kinship_i18n.language_id',$language_id);
                        })
                        ->leftjoin('char_locations_i18n As L', function($q) use ($language_id){
                            $q->on('L.location_id', '=', 'char_persons.birth_place');
                            $q->where('L.language_id',$language_id);
                        })
                        ->leftjoin('char_locations_i18n As L0', function($q) use ($language_id){
                            $q->on('L0.location_id', '=', 'char_persons.nationality');
                            $q->where('L0.language_id',$language_id);
                        })
                        ->leftjoin('char_persons_i18n', function($q) {
                            $q->on('char_persons_i18n.person_id', '=', 'char_persons.id');
                            $q->where('char_persons_i18n.language_id','2');
                        })
                        ->leftjoin('char_persons_health', function($q) {
                            $q->on('char_persons_health.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_diseases_i18n', function($q) use ($language_id){
                            $q->on('char_diseases_i18n.disease_id', '=', 'char_persons_health.disease_id');
                            $q->where('char_diseases_i18n.language_id',$language_id);
                        })
                        ->leftjoin('char_persons_islamic_commitment', function($q) {
                            $q->on('char_persons_islamic_commitment.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_persons_education', function($q) {
                            $q->on('char_persons_education.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_edu_stages_i18n as char_edu_stages', function($q) use ($language_id) {
                            $q->on('char_edu_stages.edu_stage_id', '=', 'char_persons_education.stage');
                            $q->where('char_edu_stages.language_id',$language_id);
                        })
                        ->leftjoin('char_edu_authorities_i18n as char_edu_authorities', function($q) use ($language_id){
                            $q->on('char_edu_authorities.edu_authority_id', '=', 'char_persons_education.authority');
                            $q->where('char_edu_authorities.language_id',$language_id);
                        })
                        ->leftjoin('char_edu_degrees_i18n as char_edu_degrees', function($q) use ($language_id){
                            $q->on('char_edu_degrees.edu_degree_id', '=', 'char_persons_education.degree');
                            $q->where('char_edu_degrees.language_id',$language_id);
                        })
                        ->where('char_persons.id', '!=', $person)
                        ->where('char_persons.id', '!=', $mother)
                        ->where('char_persons.id', '!=', $l_person_id)
                        ->selectRaw("
                                    CASE WHEN char_persons_kinship.kinship_id is null THEN '-' Else char_kinship_i18n.name  END  AS kinship,
                                    CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name,
                                    CONCAT(ifnull(char_persons_i18n.first_name, ' '), ' ' ,ifnull(char_persons_i18n.second_name, ' '),' ',
                                ifnull(char_persons_i18n.third_name, ' '),' ', ifnull(char_persons_i18n.last_name,' ')) AS en_name,
                                    char_persons.id_card_number,char_persons.old_id_card_number,
                                    CASE WHEN char_persons.birthday is null THEN '-' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                    CASE
                                    WHEN char_persons.gender is null THEN '-'
                                    WHEN char_persons.gender = 1 THEN '$male'
                                    WHEN char_persons.gender = 2 THEN '$female'
                                    END
                                    AS the_gender,
                                    CASE WHEN char_persons.birth_place is null THEN '-' Else L.name END AS birth_place,
                                    CASE WHEN char_persons.nationality is null THEN '-' Else L0.name END AS nationality,
                                    CASE WHEN char_persons_health.condition is null THEN '-'
                                         WHEN char_persons_health.condition = 1 THEN '$perfect'
                                         WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                         WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                    END AS health_status,
                                    CASE WHEN char_persons_health.details is null THEN '-' Else char_persons_health.details END   AS details,
                                    CASE WHEN char_diseases_i18n.name is null THEN '-' Else char_diseases_i18n.name END   AS disease_id,
                                    CASE
                                    WHEN char_persons_education.study is null THEN '-'
                                    WHEN char_persons_education.study = 1 THEN '$yes'
                                    WHEN char_persons_education.study = 0 THEN '$no'
                                    END
                                    AS study,
                                                                                        CASE
                                                               WHEN char_persons_education.type is null THEN '-'
                                                               WHEN char_persons_education.type = 1 THEN '$vocational'
                                                               WHEN char_persons_education.type = 2 THEN '$academic'
                                                    END
                                                     AS type,
                                                     CASE
                                                               WHEN char_persons_education.level is null THEN ' '
                                                               WHEN char_persons_education.level = 1 THEN '$weak'
                                                               WHEN char_persons_education.level = 2 THEN '$acceptable'
                                                               WHEN char_persons_education.level = 3 THEN '$good'
                                                               WHEN char_persons_education.level = 4 THEN '$very_good'
                                                               WHEN char_persons_education.level = 5 THEN '$excellent'
                                                     END
                                                     AS level,
                                                    CASE
                                                    WHEN char_persons_education.grade is null THEN '-'
                                                               WHEN char_persons_education.grade = 1 THEN '$the_first '
                                                               WHEN char_persons_education.grade = 2 THEN '$the_second '
                                                               WHEN char_persons_education.grade = 3 THEN '$the_third '
                                                               WHEN char_persons_education.grade = 4 THEN '$the_fourth '
                                                               WHEN char_persons_education.grade = 5 THEN '$the_Fifth '
                                                               WHEN char_persons_education.grade = 6 THEN '$the_Sixth '
                                                               WHEN char_persons_education.grade = 7 THEN '$the_Seventh '
                                                               WHEN char_persons_education.grade = 8 THEN '$the_Eighth '
                                                               WHEN char_persons_education.grade = 9 THEN '$the_Ninth '
                                                               WHEN char_persons_education.grade = 10 THEN '$the_tenth'
                                                               WHEN char_persons_education.grade = 11 THEN '$the_eleventh'
                                                               WHEN char_persons_education.grade = 12 THEN '$the_twelveth'
                                                               WHEN char_persons_education.grade = 13 THEN '$special_studies'
                                                               WHEN char_persons_education.grade = 14 THEN '$vocational_training'
                                                               WHEN char_persons_education.grade = 15 THEN '$first_year_of_university'
                                                               WHEN char_persons_education.grade = 16 THEN '$second_year_of_university'
                                                               WHEN char_persons_education.grade = 17 THEN '$third_year_of_university'
                                                               WHEN char_persons_education.grade = 18 THEN '$fourth_year_of_university'
                                                               WHEN char_persons_education.grade = 19 THEN '$fifth_year_of_university'  
                                    END
                                    AS grade,
                                    CASE WHEN char_persons_education.year is null THEN '-'   Else char_persons_education.year END  AS year,
                                    CASE WHEN char_persons_education.points is null THEN '-' Else char_persons_education.points END  AS points,
                                    CASE WHEN char_persons_education.school is null THEN '-' Else char_persons_education.school END  AS school,
                                    CASE WHEN char_edu_authorities.name is null THEN '-' Else char_edu_authorities.name END as authority,
                                    CASE WHEN char_edu_stages.name is null THEN '-' Else char_edu_stages.name END as stage,
                                    CASE WHEN char_edu_degrees.name is null THEN '-' Else char_edu_degrees.name END as degree,
                                    CASE  WHEN char_persons_islamic_commitment.prayer is null THEN '-'
                                          WHEN char_persons_islamic_commitment.prayer = 0 THEN '$regularly'
                                          WHEN char_persons_islamic_commitment.prayer = 1 THEN '$does_not_pray'
                                          WHEN char_persons_islamic_commitment.prayer = 2 THEN '$sometimes'
                                    END  AS prayer,
                                    CASE
                                    WHEN char_persons_islamic_commitment.quran_parts is null THEN '-'
                                    WHEN char_persons_islamic_commitment.quran_parts = 0 THEN '$no'
                                    Else '$yes'
                                    END
                                    AS save_quran,
                                     CASE
                                    WHEN char_persons_islamic_commitment.quran_center is null THEN '-'
                                    WHEN char_persons_islamic_commitment.quran_center = 0 THEN '$no'
                                    Else '$yes'
                                    END
                                    AS quran_center,
                                    CASE WHEN char_persons_islamic_commitment.prayer_reason is null THEN '-' Else  char_persons_islamic_commitment.prayer_reason END AS prayer_reason,
                                    CASE WHEN char_persons_islamic_commitment.quran_reason is null  THEN '-' Else char_persons_islamic_commitment.quran_reason END AS quran_reason,
                                    CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN '-' Else char_persons_islamic_commitment.quran_parts END  AS quran_parts,
                                    CASE WHEN char_persons_islamic_commitment.quran_chapters is null  THEN '-' Else char_persons_islamic_commitment.quran_chapters END AS quran_chapters
                                        ")->get();
                }
            }
            else{
                if($l_person_id !=null && $mother== null) {
                    $return=\DB::table('char_persons')
                        ->join('char_persons_kinship', function($q) use($l_person_id){
                            $q->on('char_persons_kinship.r_person_id', '=', 'char_persons.id');
                            $q->where('char_persons_kinship.l_person_id', '=', $l_person_id);
                        })
                        ->leftjoin('char_kinship_i18n', function($q) use ($language_id){
                            $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                            $q->where('char_kinship_i18n.language_id',$language_id);
                        })
                        ->leftjoin('char_locations_i18n As L', function($q) use ($language_id){
                            $q->on('L.location_id', '=', 'char_persons.birth_place');
                            $q->where('L.language_id',$language_id);
                        })
                        ->leftjoin('char_locations_i18n As L0', function($q) use ($language_id){
                            $q->on('L0.location_id', '=', 'char_persons.nationality');
                            $q->where('L0.language_id',$language_id);
                        })
                        ->leftjoin('char_persons_i18n', function($q) {
                            $q->on('char_persons_i18n.person_id', '=', 'char_persons.id');
                            $q->where('char_persons_i18n.language_id','2');
                        })
                        ->leftjoin('char_persons_health', function($q) {
                            $q->on('char_persons_health.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_diseases_i18n', function($q) use ($language_id){
                            $q->on('char_diseases_i18n.disease_id', '=', 'char_persons_health.disease_id');
                            $q->where('char_diseases_i18n.language_id',$language_id);
                        })
                        ->leftjoin('char_persons_islamic_commitment', function($q) {
                            $q->on('char_persons_islamic_commitment.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_persons_education', function($q) {
                            $q->on('char_persons_education.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_edu_stages_i18n as char_edu_stages', function($q)use ($language_id) {
                            $q->on('char_edu_stages.edu_stage_id', '=', 'char_persons_education.stage');
                            $q->where('char_edu_stages.language_id',$language_id);
                        })
                        ->leftjoin('char_edu_authorities_i18n as char_edu_authorities', function($q) use ($language_id){
                            $q->on('char_edu_authorities.edu_authority_id', '=', 'char_persons_education.authority');
                            $q->where('char_edu_authorities.language_id',$language_id);
                        })
                        ->leftjoin('char_edu_degrees_i18n as char_edu_degrees', function($q) use ($language_id) {
                            $q->on('char_edu_degrees.edu_degree_id', '=', 'char_persons_education.degree');
                            $q->where('char_edu_degrees.language_id',$language_id);
                        })
                        ->where(function ($q) use ($husband_kinship_id,$l_person_id) {
                            $q->where('char_persons.id','!=', $l_person_id);

                            if(!is_null($husband_kinship_id)){
                                $q->where('char_persons_kinship.kinship_id', '!=', $husband_kinship_id);
                            }
                        })

                        ->selectRaw("
                                    CASE WHEN char_persons_kinship.kinship_id is null THEN '-' Else char_kinship_i18n.name  END  AS kinship,
                                    CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name,
                                    CONCAT(ifnull(char_persons_i18n.first_name, ' '), ' ' ,ifnull(char_persons_i18n.second_name, ' '),' ',
                                ifnull(char_persons_i18n.third_name, ' '),' ', ifnull(char_persons_i18n.last_name,' ')) AS en_name,
                                    char_persons.id_card_number,char_persons.old_id_card_number,
                                    CASE WHEN char_persons.birthday is null THEN '-' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                    CASE
                                    WHEN char_persons.gender is null THEN '-'
                                    WHEN char_persons.gender = 1 THEN '$male'
                                    WHEN char_persons.gender = 2 THEN '$female'
                                    END
                                    AS the_gender,
                                    CASE WHEN char_persons.birth_place is null THEN '-' Else L.name END AS birth_place,
                                    CASE WHEN char_persons.nationality is null THEN '-' Else L0.name END AS nationality,
                                    CASE WHEN char_persons_health.condition is null THEN '-'
                                         WHEN char_persons_health.condition = 1 THEN '$perfect'
                                         WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                         WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                    END AS health_status,
                                    CASE WHEN char_persons_health.details is null THEN '-' Else char_persons_health.details END   AS details,
                                    CASE WHEN char_diseases_i18n.name is null THEN '-' Else char_diseases_i18n.name END   AS disease_id,
                                    CASE
                                    WHEN char_persons_education.study is null THEN '-'
                                    WHEN char_persons_education.study = 1 THEN '$yes'
                                    WHEN char_persons_education.study = 0 THEN '$no'
                                    END
                                    AS study,
                                                    CASE
                                                               WHEN char_persons_education.type is null THEN '-'
                                                               WHEN char_persons_education.type = 1 THEN '$vocational'
                                                               WHEN char_persons_education.type = 2 THEN '$academic'
                                                    END
                                                     AS type,
                                                     CASE
                                                               WHEN char_persons_education.level is null THEN ' '
                                                               WHEN char_persons_education.level = 1 THEN '$weak'
                                                               WHEN char_persons_education.level = 2 THEN '$acceptable'
                                                               WHEN char_persons_education.level = 3 THEN '$good'
                                                               WHEN char_persons_education.level = 4 THEN '$very_good'
                                                               WHEN char_persons_education.level = 5 THEN '$excellent'
                                                     END
                                                     AS level,
                                    CASE
                                    WHEN char_persons_education.grade is null THEN '-'
                                                               WHEN char_persons_education.grade = 1 THEN '$the_first '
                                                               WHEN char_persons_education.grade = 2 THEN '$the_second '
                                                               WHEN char_persons_education.grade = 3 THEN '$the_third '
                                                               WHEN char_persons_education.grade = 4 THEN '$the_fourth '
                                                               WHEN char_persons_education.grade = 5 THEN '$the_Fifth '
                                                               WHEN char_persons_education.grade = 6 THEN '$the_Sixth '
                                                               WHEN char_persons_education.grade = 7 THEN '$the_Seventh '
                                                               WHEN char_persons_education.grade = 8 THEN '$the_Eighth '
                                                               WHEN char_persons_education.grade = 9 THEN '$the_Ninth '
                                                               WHEN char_persons_education.grade = 10 THEN '$the_tenth'
                                                               WHEN char_persons_education.grade = 11 THEN '$the_eleventh'
                                                               WHEN char_persons_education.grade = 12 THEN '$the_twelveth'
                                                               WHEN char_persons_education.grade = 13 THEN '$special_studies'
                                                               WHEN char_persons_education.grade = 14 THEN '$vocational_training'
                                                               WHEN char_persons_education.grade = 15 THEN '$first_year_of_university'
                                                               WHEN char_persons_education.grade = 16 THEN '$second_year_of_university'
                                                               WHEN char_persons_education.grade = 17 THEN '$third_year_of_university'
                                                               WHEN char_persons_education.grade = 18 THEN '$fourth_year_of_university'
                                                               WHEN char_persons_education.grade = 19 THEN '$fifth_year_of_university' 
                                    END
                                    AS grade,
                                    CASE WHEN char_persons_education.year is null THEN '-'   Else char_persons_education.year END  AS year,
                                    CASE WHEN char_persons_education.points is null THEN '-' Else char_persons_education.points END  AS points,
                                    CASE WHEN char_persons_education.school is null THEN '-' Else char_persons_education.school END  AS school,
                                    CASE WHEN char_edu_authorities.name is null THEN '-' Else char_edu_authorities.name END as authority,
                                    CASE WHEN char_edu_stages.name is null THEN '-' Else char_edu_stages.name END as stage,
                                    CASE WHEN char_edu_degrees.name is null THEN '-' Else char_edu_degrees.name END as degree,
                                    CASE  WHEN char_persons_islamic_commitment.prayer is null THEN '-'
                                          WHEN char_persons_islamic_commitment.prayer = 0 THEN '$regularly'
                                          WHEN char_persons_islamic_commitment.prayer = 1 THEN '$does_not_pray'
                                          WHEN char_persons_islamic_commitment.prayer = 2 THEN '$sometimes'
                                    END  AS prayer,
                                    CASE
                                    WHEN char_persons_islamic_commitment.quran_parts is null THEN '-'
                                    WHEN char_persons_islamic_commitment.quran_parts = 0 THEN '$no'
                                    Else '$yes'
                                    END
                                    AS save_quran,
                                     CASE
                                    WHEN char_persons_islamic_commitment.quran_center is null THEN '-'
                                    WHEN char_persons_islamic_commitment.quran_center = 0 THEN '$no'
                                    Else '$yes'
                                    END
                                    AS quran_center,
                                    CASE WHEN char_persons_islamic_commitment.prayer_reason is null THEN '-' Else  char_persons_islamic_commitment.prayer_reason END AS prayer_reason,
                                    CASE WHEN char_persons_islamic_commitment.quran_reason is null  THEN '-' Else char_persons_islamic_commitment.quran_reason END AS quran_reason,
                                    CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN '-' Else char_persons_islamic_commitment.quran_parts END  AS quran_parts,
                                    CASE WHEN char_persons_islamic_commitment.quran_chapters is null  THEN '-' Else char_persons_islamic_commitment.quran_chapters END AS quran_chapters
                                        ")->get();
                }
                if($l_person_id !=null && $mother != null) {
                    $return=\DB::table('char_persons')
                        ->join('char_persons_kinship', function($q) use($l_person_id){
                            $q->on('char_persons_kinship.r_person_id', '=', 'char_persons.id');
                            $q->where('char_persons_kinship.l_person_id', '=', $l_person_id);
                        })
                        ->leftjoin('char_kinship_i18n', function($q) use ($language_id){
                            $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                            $q->where('char_kinship_i18n.language_id',$language_id);
                        })
                        ->leftjoin('char_locations_i18n As L', function($q) use ($language_id){
                            $q->on('L.location_id', '=', 'char_persons.birth_place');
                            $q->where('L.language_id',$language_id);
                        })
                        ->leftjoin('char_locations_i18n As L0', function($q) use ($language_id) {
                            $q->on('L0.location_id', '=', 'char_persons.nationality');
                            $q->where('L0.language_id',$language_id);
                        })
                        ->leftjoin('char_persons_i18n', function($q) {
                            $q->on('char_persons_i18n.person_id', '=', 'char_persons.id');
                            $q->where('char_persons_i18n.language_id','2');
                        })
                        ->leftjoin('char_persons_health', function($q) {
                            $q->on('char_persons_health.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_diseases_i18n', function($q) use ($language_id){
                            $q->on('char_diseases_i18n.disease_id', '=', 'char_persons_health.disease_id');
                            $q->where('char_diseases_i18n.language_id',$language_id);
                        })
                        ->leftjoin('char_persons_islamic_commitment', function($q) {
                            $q->on('char_persons_islamic_commitment.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_persons_education', function($q) {
                            $q->on('char_persons_education.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_edu_stages_i18n as char_edu_stages', function($q) use ($language_id) {
                            $q->on('char_edu_stages.edu_stage_id', '=', 'char_persons_education.stage');
                            $q->where('char_edu_stages.language_id',$language_id);
                        })
                        ->leftjoin('char_edu_authorities_i18n as char_edu_authorities', function($q) use ($language_id) {
                            $q->on('char_edu_authorities.edu_authority_id', '=', 'char_persons_education.authority');
                            $q->where('char_edu_authorities.language_id',$language_id);
                        })
                        ->leftjoin('char_edu_degrees_i18n as char_edu_degrees', function($q) use ($language_id){
                            $q->on('char_edu_degrees.edu_degree_id', '=', 'char_persons_education.degree');
                            $q->where('char_edu_degrees.language_id',$language_id);
                        })
                        ->where(function ($q) use ($mother,$husband_kinship_id,$l_person_id) {
                            $q->whereNotIn('char_persons.id', [$mother,$l_person_id]);

                            if(!is_null($husband_kinship_id)){
                                $q->where('char_persons_kinship.kinship_id', '!=', $husband_kinship_id);
                            }
                        })
                        ->selectRaw("
                                    CASE WHEN char_persons_kinship.kinship_id is null THEN '-' Else char_kinship_i18n.name  END  AS kinship,
                                                     CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                                ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' '))  AS name,
                                                    CONCAT(ifnull(char_persons_i18n.first_name, ' '), ' ' ,ifnull(char_persons_i18n.second_name, ' '),' ',
                                                ifnull(char_persons_i18n.third_name, ' '),' ', ifnull(char_persons_i18n.last_name,' '))  AS en_name,
                                                    char_persons.id_card_number,char_persons.old_id_card_number,
                                                    CASE WHEN char_persons.birthday is null THEN '-' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                                    CASE  WHEN char_persons.gender is null THEN '-'
                                                          WHEN char_persons.gender = 1 THEN '$male'
                                                          WHEN char_persons.gender = 2 THEN '$female'
                                                    END  AS the_gender,
                                                    CASE WHEN char_persons.birth_place is null THEN '-' Else L.name END AS birth_place,
                                                    CASE WHEN char_persons.nationality is null THEN '-' Else L0.name END AS nationality,
                                                    CASE
                                                          WHEN char_persons_health.condition is null THEN '-'
                                                          WHEN char_persons_health.condition = 1 THEN '$perfect'
                                                          WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                          WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                                    END
                                                    AS health_status,
                                                    CASE WHEN char_persons_health.details is null THEN '-' Else char_persons_health.details END   AS details,
                                                    CASE WHEN char_diseases_i18n.name is null THEN '-' Else char_diseases_i18n.name END   AS disease_id,
                                                    CASE
                                                        WHEN char_persons_education.study is null THEN '-'
                                                        WHEN char_persons_education.study = 1 THEN '$yes'
                                                        WHEN char_persons_education.study = 0 THEN '$no'
                                                        END
                                                    AS study,
                                                     CASE
                                                               WHEN char_persons_education.type is null THEN '-'
                                                               WHEN char_persons_education.type = 1 THEN '$vocational'
                                                               WHEN char_persons_education.type = 2 THEN '$academic'
                                                    END
                                                     AS type,
                                                     CASE
                                                               WHEN char_persons_education.level is null THEN ' '
                                                               WHEN char_persons_education.level = 1 THEN '$weak'
                                                               WHEN char_persons_education.level = 2 THEN '$acceptable'
                                                               WHEN char_persons_education.level = 3 THEN '$good'
                                                               WHEN char_persons_education.level = 4 THEN '$very_good'
                                                               WHEN char_persons_education.level = 5 THEN '$excellent'
                                                     END
                                                     AS level,
                                                    CASE
                                                               WHEN char_persons_education.grade is null THEN ' '
                                                               WHEN char_persons_education.grade = 1 THEN '$the_first '
                                                               WHEN char_persons_education.grade = 2 THEN '$the_second '
                                                               WHEN char_persons_education.grade = 3 THEN '$the_third '
                                                               WHEN char_persons_education.grade = 4 THEN '$the_fourth '
                                                               WHEN char_persons_education.grade = 5 THEN '$the_Fifth '
                                                               WHEN char_persons_education.grade = 6 THEN '$the_Sixth '
                                                               WHEN char_persons_education.grade = 7 THEN '$the_Seventh '
                                                               WHEN char_persons_education.grade = 8 THEN '$the_Eighth '
                                                               WHEN char_persons_education.grade = 9 THEN '$the_Ninth '
                                                               WHEN char_persons_education.grade = 10 THEN '$the_tenth'
                                                               WHEN char_persons_education.grade = 11 THEN '$the_eleventh'
                                                               WHEN char_persons_education.grade = 12 THEN '$the_twelveth'
                                                               WHEN char_persons_education.grade = 13 THEN '$special_studies'
                                                               WHEN char_persons_education.grade = 14 THEN '$vocational_training'
                                                               WHEN char_persons_education.grade = 15 THEN '$first_year_of_university'
                                                               WHEN char_persons_education.grade = 16 THEN '$second_year_of_university'
                                                               WHEN char_persons_education.grade = 17 THEN '$third_year_of_university'
                                                               WHEN char_persons_education.grade = 18 THEN '$fourth_year_of_university'
                                                               WHEN char_persons_education.grade = 19 THEN '$fifth_year_of_university'                                         
                                                     END
                                                     AS grade,
                                    CASE WHEN char_persons_education.year is null THEN '-'   Else char_persons_education.year END  AS year,
                                    CASE WHEN char_persons_education.points is null THEN '-' Else char_persons_education.points END  AS points,
                                    CASE WHEN char_persons_education.school is null THEN '-' Else char_persons_education.school END  AS school,
                                    CASE WHEN char_edu_authorities.name is null THEN '-' Else char_edu_authorities.name END as authority,
                                    CASE WHEN char_edu_stages.name is null THEN '-' Else char_edu_stages.name END as stage,
                                    CASE WHEN char_edu_degrees.name is null THEN '-' Else char_edu_degrees.name END as degree,
                                    CASE  WHEN char_persons_islamic_commitment.prayer is null THEN '-'
                                          WHEN char_persons_islamic_commitment.prayer = 0 THEN '$regularly'
                                          WHEN char_persons_islamic_commitment.prayer = 1 THEN '$does_not_pray'
                                          WHEN char_persons_islamic_commitment.prayer = 2 THEN '$sometimes'
                                    END  AS prayer,
                                    CASE
                                    WHEN char_persons_islamic_commitment.quran_parts is null THEN '-'
                                    WHEN char_persons_islamic_commitment.quran_parts = 0 THEN '$no'
                                    Else '$yes'
                                    END
                                    AS save_quran,
                                     CASE
                                    WHEN char_persons_islamic_commitment.quran_center is null THEN '-'
                                    WHEN char_persons_islamic_commitment.quran_center = 0 THEN '$no'
                                    Else '$yes'
                                    END
                                    AS quran_center,
                                    CASE WHEN char_persons_islamic_commitment.prayer_reason is null THEN '-' Else  char_persons_islamic_commitment.prayer_reason END AS prayer_reason,
                                    CASE WHEN char_persons_islamic_commitment.quran_reason is null  THEN '-' Else char_persons_islamic_commitment.quran_reason END AS quran_reason,
                                    CASE WHEN char_persons_islamic_commitment.quran_parts is null THEN '-' Else char_persons_islamic_commitment.quran_parts END  AS quran_parts,
                                    CASE WHEN char_persons_islamic_commitment.quran_chapters is null  THEN '-' Else char_persons_islamic_commitment.quran_chapters END AS quran_chapters
                                        ")->get();


                }
            }
        }
        else{
            if($person != null){
                if($l_person_id ==null && $mother== null) {
                    $fm=\DB::table('char_persons')
                        ->join('char_persons_kinship', function($q) use($person){
                            $q->on('char_persons_kinship.r_person_id', '=', 'char_persons.id');
                            $q->where('char_persons_kinship.l_person_id', '=', $person);
                        })
                        ->leftjoin('char_persons_education', function($q) {
                            $q->on('char_persons_education.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_persons_health', function($q) {
                            $q->on('char_persons_health.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_kinship_i18n', function($q) use ($language_id){
                            $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                            $q->where('char_kinship_i18n.language_id',$language_id);
                        });
                        if(!is_null($husband_kinship_id)){
                            $fm= $fm->where('char_persons_kinship.kinship_id', '!=', $husband_kinship_id);
                        }
                       $return['fm'] =$fm->selectRaw("CASE WHEN char_persons_kinship.kinship_id is null THEN '-' Else char_kinship_i18n.name  END  AS kinship,
                                    
                                          char_persons.id,
                                        char_persons.first_name,
                                        char_persons.second_name,
                                        char_persons.third_name,
                                        char_persons.last_name,
                                        char_persons.gender,
                                       CASE WHEN char_persons.id_card_number is null THEN '-' Else char_persons.id_card_number  END  AS id_card_number,
                                       CASE WHEN char_persons.old_id_card_number is null THEN '-' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                         CASE WHEN char_persons.birthday is null THEN '-' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                        char_persons_health.condition")->get();

                }
                if($l_person_id !=null && $mother== null) {
                    $return['fm']=\DB::table('char_persons')
                        ->join('char_persons_kinship', function($q) use($l_person_id){
                            $q->on('char_persons_kinship.r_person_id', '=', 'char_persons.id');
                            $q->where('char_persons_kinship.l_person_id', '=', $l_person_id);
                        })
                        ->leftjoin('char_persons_education', function($q) {
                            $q->on('char_persons_education.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_persons_health', function($q) {
                            $q->on('char_persons_health.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_kinship_i18n', function($q) use ($language_id){
                            $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                            $q->where('char_kinship_i18n.language_id',$language_id);
                        })
                        ->where(function ($q) use ($person,$husband_kinship_id,$l_person_id) {
                            $q->whereNotIn('char_persons.id', [$person,$l_person_id]);

                            if(!is_null($husband_kinship_id)){
                                $q->where('char_persons_kinship.kinship_id', '!=', $husband_kinship_id);
                            }
                        })
                        ->selectRaw("CASE WHEN char_persons_kinship.kinship_id is null THEN '-' Else char_kinship_i18n.name  END  AS kinship,
                                    
                                          char_persons.id,
                                        char_persons.first_name,
                                        char_persons.second_name,
                                        char_persons.third_name,
                                        char_persons.last_name,
                                        char_persons.gender,
                                       CASE WHEN char_persons.id_card_number is null THEN '-' Else char_persons.id_card_number  END  AS id_card_number,
                                       CASE WHEN char_persons.old_id_card_number is null THEN '-' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                         CASE WHEN char_persons.birthday is null THEN '-' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                        char_persons_health.condition")->get();
                }
                if($l_person_id !=null && $mother != null) {
                    $return['fm']=\DB::table('char_persons')
                        ->join('char_persons_kinship', function($q) use($l_person_id){
                            $q->on('char_persons_kinship.r_person_id', '=', 'char_persons.id');
                            $q->where('char_persons_kinship.l_person_id', '=', $l_person_id);
                        })
                        ->leftjoin('char_persons_education', function($q) {
                            $q->on('char_persons_education.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_persons_health', function($q) {
                            $q->on('char_persons_health.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_kinship_i18n', function($q) use ($language_id){
                            $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                            $q->where('char_kinship_i18n.language_id',$language_id);
                        })
                        ->where(function ($q) use ($person,$husband_kinship_id,$l_person_id,$mother,$organization_id) {
                                $q->whereNotIn('char_persons.id', function($query) use ($organization_id){
                                    $query->select('char_cases.person_id')
                                        ->where('char_cases.organization_id', '=', $organization_id)
                                        ->from('char_cases');
                                });
                                $q->where('char_persons.mother_id', '=', $mother);
                                $q->whereNotIn('char_persons.id',[$person,$l_person_id, $mother]);
                                if(!is_null($husband_kinship_id)){
                                    $q->where('char_persons_kinship.kinship_id', '!=', $husband_kinship_id);
                                }
                        })
                        ->selectRaw("
                                       char_persons_health.condition,
                                      CASE WHEN char_persons_kinship.kinship_id is null THEN '-' Else char_kinship_i18n.name  END  AS kinship,
                                            char_persons.id,
                                        char_persons.first_name,
                                        char_persons.second_name,
                                        char_persons.third_name,
                                        char_persons.last_name,
                                        char_persons.gender,
                                       CASE WHEN char_persons.id_card_number is null THEN '-' Else char_persons.id_card_number  END  AS id_card_number,
                                       CASE WHEN char_persons.old_id_card_number is null THEN '-' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                                       CASE WHEN char_persons.birthday is null THEN '-' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday 
                                                                              ")->get();

                    $return['fm_cases']=\DB::table('char_persons')
                        ->join('char_cases', function($q) use($organization_id){
                            $q->on('char_cases.person_id', '=', 'char_persons.id');
                            $q->where('char_cases.organization_id',$organization_id);
                        })
                        ->join('char_categories as ca', function($q) {
                            $q->on('ca.id', '=', 'char_cases.category_id');
                            $q->where('ca.type',1);
                        })->join('char_persons_kinship', function($q) use($l_person_id){
                            $q->on('char_persons_kinship.r_person_id', '=', 'char_persons.id');
                            $q->where('char_persons_kinship.l_person_id', '=', $l_person_id);
                        })
                        ->leftjoin('char_persons_education', function($q) {
                            $q->on('char_persons_education.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_persons_health', function($q) {
                            $q->on('char_persons_health.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_kinship_i18n', function($q) use ($language_id){
                            $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                            $q->where('char_kinship_i18n.language_id',$language_id);
                        })
                        ->where(function ($q) use ($person,$husband_kinship_id,$l_person_id,$mother,$organization_id) {
                               $q->whereIn('char_persons.id', function($query)  use ($organization_id){
                                    $query->select('char_cases.person_id')
                                        ->where('char_cases.organization_id', '=', $organization_id)
                                        ->from('char_cases');
                               });
                               $q ->where('char_persons.mother_id', '=', $mother);
                               $q ->whereNotIn('char_persons.id',[ $person, $mother]);

                            if(!is_null($husband_kinship_id)){
                                $q->where('char_persons_kinship.kinship_id', '!=', $husband_kinship_id);
                            }
                        })

                        ->selectRaw("
                                      ca.name as category_name,
                                      char_persons_health.condition,
                                       CASE WHEN char_persons_kinship.kinship_id is null THEN '-' Else char_kinship_i18n.name  END  AS kinship,
                                         char_cases.id as case_id,
                                        char_persons.id,
                                        char_persons.first_name,
                                        char_persons.second_name,
                                        char_persons.third_name,
                                        char_persons.last_name,
                                        char_persons.gender,
                                        CASE WHEN char_persons.id_card_number is null THEN '-' Else char_persons.id_card_number  END  AS id_card_number,
                                        CASE WHEN char_persons.old_id_card_number is null THEN '-' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                                        CASE WHEN char_persons.birthday is null THEN '-' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday

                                        ")->get();
                }
            }
            else{
                if($l_person_id !=null && $mother== null) {

                    $return['fm'] = \DB::table('char_persons_kinship')
                        ->join('char_persons', 'char_persons_kinship.r_person_id', '=', 'char_persons.id')
                        ->leftjoin('char_persons_education','char_persons_education.person_id', '=', 'char_persons.id')
                        ->leftjoin('char_persons_health','char_persons_health.person_id', '=', 'char_persons.id')
                        ->leftjoin('char_kinship_i18n', function($q) use ($language_id){
                            $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                            $q->where('char_kinship_i18n.language_id',$language_id);
                        })
                        ->where(function ($q) use ($person,$husband_kinship_id,$l_person_id,$mother) {
                            if(!is_null($husband_kinship_id)){
                                $q->where('char_persons_kinship.kinship_id', '!=', $husband_kinship_id);
                            }
                            $q->where('char_persons_kinship.l_person_id', '=', $l_person_id);
                            $q ->where('char_persons.id', '!=', $l_person_id);
                        })
                        ->selectRaw("CASE WHEN char_persons_kinship.kinship_id is null THEN '-' Else char_kinship_i18n.name  END  AS kinship,
                                          char_persons.id,
                                        char_persons.first_name,
                                        char_persons.second_name,
                                        char_persons.third_name,
                                        char_persons.last_name,
                                        char_persons.gender,
                                        char_persons.id_card_number,char_persons.old_id_card_number,
                                        CASE WHEN char_persons.id_card_number is null THEN '-' Else char_persons.id_card_number  END  AS id_card_number,
                                        CASE WHEN char_persons.old_id_card_number is null THEN '-' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                                        CASE WHEN char_persons.birthday is null THEN '-' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                        char_persons_health.condition")
                        ->get();
                    return $return;

                }
                if($l_person_id !=null && $mother != null) {
                    $return['fm']=\DB::table('char_persons')
                        ->join('char_persons_kinship', function($q) use($l_person_id){
                            $q->on('char_persons_kinship.r_person_id', '=', 'char_persons.id');
                            $q->where('char_persons_kinship.l_person_id', '=', $l_person_id);
                        })
                        ->leftjoin('char_persons_education', function($q) {
                            $q->on('char_persons_education.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_persons_health', function($q) {
                            $q->on('char_persons_health.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_kinship_i18n', function($q) use ($language_id){
                            $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                            $q->where('char_kinship_i18n.language_id',$language_id);
                        })
                        ->where(function ($q) use ($person,$husband_kinship_id,$l_person_id,$mother,$organization_id) {
                                $q->whereNotIn('char_persons.id', function($query)  use ($organization_id){
                                    $query->select('char_cases.person_id')
                                        ->where('char_cases.organization_id', '=', $organization_id)
                                        ->from('char_cases');
                                });

                                $q->where('char_persons.mother_id', '=', $mother);
                                $q->whereNotIn('char_persons.id',[$l_person_id,$mother]);

                                if(!is_null($husband_kinship_id)){
                                    $q->where('char_persons_kinship.kinship_id', '!=', $husband_kinship_id);
                                }
                        })
                        ->selectRaw(" CASE WHEN char_persons_kinship.kinship_id is null THEN '-' Else char_kinship_i18n.name  END  AS kinship,
                                          char_persons.id,
                                        char_persons.first_name,
                                        char_persons.second_name,
                                        char_persons.third_name,
                                        char_persons.last_name,
                                        char_persons.gender,
                                        char_persons.id_card_number,char_persons.old_id_card_number,
                                        CASE WHEN char_persons.id_card_number is null THEN '-' Else char_persons.id_card_number  END  AS id_card_number,
                                        CASE WHEN char_persons.old_id_card_number is null THEN '-' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                                        CASE WHEN char_persons.birthday is null THEN '-' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                        char_persons_health.condition")->get();

                    $return['fm_cases']=\DB::table('char_persons')
                        ->join('char_cases', function($q) use($organization_id){
                            $q->on('char_cases.person_id', '=', 'char_persons.id');
                            $q->where('char_cases.organization_id',$organization_id);
                        })
                        ->join('char_categories as ca', function($q) {
                            $q->on('ca.id', '=', 'char_cases.category_id');
                            $q->where('ca.type',1);
                        })
                        ->join('char_persons_kinship', function($q) use($l_person_id){
                            $q->on('char_persons_kinship.r_person_id', '=', 'char_persons.id');
                            $q->where('char_persons_kinship.l_person_id', '=', $l_person_id);
                        })
                        ->leftjoin('char_persons_education', function($q) {
                            $q->on('char_persons_education.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_persons_health', function($q) {
                            $q->on('char_persons_health.person_id', '=', 'char_persons.id');
                        })
                        ->leftjoin('char_kinship_i18n', function($q) {
                            $q->on('char_kinship_i18n.kinship_id', '=', 'char_persons_kinship.kinship_id');
                            $q->where('char_kinship_i18n.language_id',1);
                        })
                        ->where(function ($q) use ($person,$husband_kinship_id,$l_person_id,$mother ,$organization_id) {

                            $q->wherein('char_persons.id', function($query)  use ($organization_id){
                                $query->select('char_cases.person_id')
                                    ->where('char_cases.organization_id', '=', $organization_id)
                                    ->from('char_cases');
                            });

                            $q->where('char_persons.mother_id', '=', $mother);
                            $q->where('char_persons.id', '!=', $mother);
                            if(!is_null($husband_kinship_id)){
                                $q->where('char_persons_kinship.kinship_id', '!=', $husband_kinship_id);
                            }
                        })
                        ->selectRaw("CASE WHEN char_persons_kinship.kinship_id is null THEN '-' Else char_kinship_i18n.name  END  AS kinship,
                                        char_cases.id as case_id,
                                        char_persons.id,
                                        char_persons.first_name,
                                        char_persons.second_name,
                                        char_persons.third_name,
                                        char_persons.last_name,
                                        ca.name as category_name,
                                        char_persons.gender,
                                        char_persons.id_card_number,char_persons.old_id_card_number,
                                        CASE WHEN char_persons.id_card_number is null THEN '-' Else char_persons.id_card_number  END  AS id_card_number,
                                        CASE WHEN char_persons.old_id_card_number is null THEN '-' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                                        CASE WHEN char_persons.birthday is null THEN '-' Else DATE_FORMAT(char_persons.birthday,'%Y/%m/%d')  END  AS birthday,
                                        char_persons_health.condition")->get();
                }
            }
        }
        return $return;

    }
}
