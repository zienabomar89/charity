<?php

namespace Sponsorship\Model;
use App\Http\Helpers;

class ReportsCasesDocuments  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_sponsorship_cases_documents';
    protected $primaryKey = 'id';
    protected $fillable = ['sponsorship_cases_id','doc_category_id','document_type_id','document_id','date_from','date_to','status'];
//    public $timestamps = false;
    protected $hidden = ['created_at','updated_at'];

    public static function filter($options){

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $condition=[];
//        $filters = ['doc_category_id','document_type_id','date_from','date_to'];
        $date = ['date_from','date_to'];

        $filters = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number'];
        $cases = ['status'];

        $date_from = date('Y-m-d',strtotime($options['date_from']));
        $date_to = date('Y-m-d',strtotime($options['date_to']));
        $doc_category_id = $options['doc_category_id'];

        foreach ($options as $key => $value) {
            if(in_array($key, $filters)) {
                if ($value != "" && !is_null($value)) {
                    array_push($condition, ['char_persons.' . $key => $value]);
                }
            }elseif(in_array($key, $cases)) {
                if ($value != "" && !is_null($value)) {
                    array_push($condition, ['char_sponsorship_cases.' . $key =>(int) $value]);
                }
            }
        }
        $user = \Auth::user();
        $organization_id=$user->organization_id;
        $category_id= $options['category_id'];
        $sponsor_id= $options['sponsor_id'];

       $query= \DB::table('char_sponsorship_cases')
               ->join('char_cases', function($q) use ($category_id,$organization_id){
                    $q->on('char_cases.id', '=', 'char_sponsorship_cases.case_id');
                    $q->where('char_cases.category_id',$category_id);
//                    $q->where('char_cases.organization_id',$organization_id);
                })
                ->join('char_categories as ca','ca.id',  '=', 'char_cases.category_id')
                ->join('char_organizations', 'char_organizations.id', '=', 'char_sponsorship_cases.sponsor_id')
                ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
                ->leftjoin('char_persons_contact as mob1', function($q) {
                   $q->on('mob1.person_id', '=', 'char_cases.person_id');
                   $q->where('mob1.contact_type','primary_mobile');
                })
                ->leftjoin('char_guardians', function ($q){
                   $q->on('char_guardians.individual_id', '=', 'char_guardians.individual_id');
                   $q->on('char_guardians.organization_id', 'char_cases.organization_id');
                   $q->where('char_guardians.status','=',1);
                })
                ->leftjoin('char_persons AS guardian','guardian.id','=','char_guardians.guardian_id')
                ->leftjoin('char_persons_contact as mob2', function($q) {
                   $q->on('mob2.person_id', '=', 'char_guardians.guardian_id');
                   $q->where('mob2.contact_type','primary_mobile');
                })

           ->where('char_sponsorship_cases.sponsor_id',$sponsor_id);

                  
       if(isset($options['items'])){
               if(sizeof($options['items']) > 0 ){
                $query->whereIn('char_sponsorship_cases.id',$options['items']);  
              }      
            } 
            
        $all_organization=null;
        if(isset($options['all_organization'])){
            if($options['all_organization'] ===true){
                $all_organization=0;
            }elseif($options['all_organization'] ===false){
                $all_organization=1;

            }
        }

        if($all_organization ==0){

            $organizations=[];
            if(isset($options['organization_ids']) && $options['organization_ids'] !=null && $options['organization_ids'] !="" && $options['organization_ids'] !=[] && sizeof($options['organization_ids']) != 0) {
                if($options['organization_ids'][0]==""){
                    unset($options['organization_ids'][0]);
                }
                $organizations =$options['organization_ids'];
            }

            if(!empty($organizations)){
                $query->wherein('char_cases.organization_id',$organizations);
            }else{

                $user = \Auth::user();
                if($user->type == 2) {
                    $query->where(function ($anq) use ($organization_id,$user) {
                        $anq->where('char_cases.organization_id',$organization_id);
                        $anq->orwherein('char_cases.organization_id', function($decq) use($user) {
                            $decq->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });

                }else{

                    $query->where(function ($anq) use ($organization_id) {
                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                        $anq->wherein('char_cases.organization_id', function($decq) use($organization_id) {
                            $decq->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $organization_id);
                        });
                    });
                }
            }
        }
        elseif($all_organization ==1){
            $query->where('char_cases.organization_id',$organization_id);

        }

        if (count($condition) != 0) {
            $query->where(function ($q) use ($condition) {
                $names = [
                    'char_persons.first_name', 'char_persons.second_name',
                    'char_persons.third_name', 'char_persons.last_name'];

                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $names)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });

        }


        if( $options['action'] !='export'){
            $query=$query->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as full_name,
                                      char_persons.id_card_number,char_persons.old_id_card_number,                        
                                      ca.name as category_name,                         
                                      char_organizations.name as sponsor_name,
                                      char_sponsorship_cases.sponsor_number,
                                      CONCAT(ifnull(guardian.first_name, ' '), ' ' ,ifnull(guardian.second_name, ' '),' ',ifnull(guardian.third_name,' '),' ', ifnull(guardian.last_name,' '))as guardian_name,
                                      CASE WHEN mob1.contact_value is null THEN '-'  Else mob1.contact_value END AS mobile_no,
                                      CASE WHEN mob2.contact_value is null THEN '-'  Else mob2.contact_value END AS guardian_mobile,
                                      char_sponsorship_cases.id,
                                      char_sponsorship_cases.case_id,
                                      char_cases.person_id,

                                      char_get_sponsorship_cases_documents_count(char_sponsorship_cases.id,1,'$doc_category_id','$date_from','$date_to') as per_photo,
                                      char_get_sponsorship_cases_documents_id(char_sponsorship_cases.id,1,'$doc_category_id','$date_from','$date_to') as per_photo_id,
                                    
                                      char_get_sponsorship_cases_documents_count(char_sponsorship_cases.id,2,'$doc_category_id','$date_from','$date_to') as long_photo,
                                      char_get_sponsorship_cases_documents_id(char_sponsorship_cases.id,2,'$doc_category_id','$date_from','$date_to') as long_photo_id,
                                    
                                      char_get_sponsorship_cases_documents_count(char_sponsorship_cases.id,3,'$doc_category_id','$date_from','$date_to') as single_document,
                                      char_get_sponsorship_cases_documents_id(char_sponsorship_cases.id,3,'$doc_category_id','$date_from','$date_to') as single_document_id,
                                    
                                      char_get_sponsorship_cases_documents_count(char_sponsorship_cases.id,4,'$doc_category_id','$date_from','$date_to') as thanks_message,
                                      char_get_sponsorship_cases_documents_id(char_sponsorship_cases.id,4,'$doc_category_id','$date_from','$date_to') as thanks_message_id,
                                    
                                      char_get_sponsorship_cases_documents_count(char_sponsorship_cases.id,5,'$doc_category_id','$date_from','$date_to') as medical_report,
                                      char_get_sponsorship_cases_documents_id(char_sponsorship_cases.id,5,'$doc_category_id','$date_from','$date_to') as medical_report_id,
                                    
                                      char_get_sponsorship_cases_documents_count(char_sponsorship_cases.id,6,'$doc_category_id','$date_from','$date_to') as school_certificate,
                                      char_get_sponsorship_cases_documents_id(char_sponsorship_cases.id,6,'$doc_category_id','$date_from','$date_to') as school_certificate_id,
                                    
                                      char_get_sponsorship_cases_documents_count(char_sponsorship_cases.id,7,'$doc_category_id','$date_from','$date_to') as photo_album ,
                                      char_get_sponsorship_cases_documents_id(char_sponsorship_cases.id,7,'$doc_category_id','$date_from','$date_to') as photo_album_id
                ");
        }else{
            $query=$query->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as full_name,
                            char_persons.id_card_number,char_persons.old_id_card_number,                        
                            char_sponsorship_cases.sponsor_number,
                            char_get_sponsorship_cases_documents_file_path(char_sponsorship_cases.id,1,'$doc_category_id','$date_from','$date_to') as per_photo,
                            char_get_sponsorship_cases_documents_file_path(char_sponsorship_cases.id,2,'$doc_category_id','$date_from','$date_to') as long_photo,
                            char_get_sponsorship_cases_documents_file_path(char_sponsorship_cases.id,3,'$doc_category_id','$date_from','$date_to') as single_document,
                            char_get_sponsorship_cases_documents_file_path(char_sponsorship_cases.id,4,'$doc_category_id','$date_from','$date_to') as thanks_message,
                            char_get_sponsorship_cases_documents_file_path(char_sponsorship_cases.id,5,'$doc_category_id','$date_from','$date_to') as medical_report,
                            char_get_sponsorship_cases_documents_file_path(char_sponsorship_cases.id,6,'$doc_category_id','$date_from','$date_to') as school_certificate,
                            char_get_sponsorship_cases_documents_file_path(char_sponsorship_cases.id,7,'$doc_category_id','$date_from','$date_to') as photo_album
                ");
        }


        $query=$query->groupBy('char_sponsorship_cases.id');

        if($options['action'] =='exportToExcel') {
            return $query->get();

        }elseif( $options['action'] =='export'){
            $query = $query->get();
            $files=['thanks_message','school_certificate','long_photo','per_photo','single_document','medical_report','photo_album'];
            $multiple=['medical_report','photo_album'];

            $i = 0;
            foreach ($query as $key => $value) {
                $value->hasFiles = false ;
                $value->files=[];
                foreach ($files as $k){
                    if(!is_null($value->$k)){
                        if(!$value->hasFiles) {$value->hasFiles=true;}

                        if(in_array($k,$multiple)){
                            $value->files[$k]=explode(",",$value->$k);
                        }else{
                            $value->files[$k]=$value->$k;

                        }
                    }
                }
                if(sizeof($value->files) > 0){
                    $i++;
                }
            }
            return ['count'=> $i , 'items' =>$query];
        }
        $itemsCount = isset($options['itemsCount'])? $options['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
        return $query=$query->paginate($records_per_page);

    }

    public static function caseAttachments($options){

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $condition=[];
        $filters = ['doc_category_id','sponsorship_cases_id','date_from','date_to'];
        $dates = ['date_from','date_to'];


        $sponsorship_cases_id=$options['sponsorship_cases_id'];
        $doc_category_id=$options['doc_category_id'];
        $date_from=$options['date_from'];
        $date_to=$options['date_to'];

        foreach ($options as $key => $value) {
            if(in_array($key,$filters)) {
                if ($value != "" ) {
                    if(in_array($key,$dates)){
                        $data = ['char_sponsorship_cases_documents.' . $key => date('Y-m-d',strtotime($value))];
                    }else{
                        $data = ['char_sponsorship_cases_documents.' . $key => (int) $value];
                    }
                    array_push($condition,$data);
                }
            }
        }

        $query= \DB::table('char_report_document_types');
        if($options['action'] =='export'){
            $query->join('char_sponsorship_cases_documents', function($q) use($condition)  {
                $q->on('char_report_document_types.id','=','char_sponsorship_cases_documents.document_type_id');
                if (count($condition) != 0) {
                    $q =  $q->where(function ($Sq) use ($condition) {
                        for ($i = 0; $i < count($condition); $i++) {
                            foreach ($condition[$i] as $key => $value) {
                                $Sq->where($key,'=',$value);
                            }
                        }
                    });
                }
            })
                ->join('doc_files', 'char_sponsorship_cases_documents.document_id', '=', 'doc_files.id');
        }else{
            $query->leftjoin('char_sponsorship_cases_documents', function($q) use($condition)  {
                $q->on('char_report_document_types.id','=','char_sponsorship_cases_documents.document_type_id');
                if (count($condition) != 0) {
                    $q =  $q->where(function ($Sq) use ($condition) {
                        for ($i = 0; $i < count($condition); $i++) {
                            foreach ($condition[$i] as $key => $value) {
                                $Sq->where($key,'=',$value);
                            }
                        }
                    });
                }
            })
                ->leftjoin('doc_files', 'char_sponsorship_cases_documents.document_id', '=', 'doc_files.id');
        }

        $query = $query->selectRaw("char_report_document_types.name as document_type_name,
                                    char_report_document_types.id as document_type_id,
                                    char_report_document_types.multiple as multiple,
                                    char_sponsorship_cases_documents.id,
                                    '$sponsorship_cases_id' as sponsorship_cases_id,
                                    '$doc_category_id' as doc_category_id,
                                    '$date_from' as date_from,
                                    '$date_to' as date_to,
                                    char_sponsorship_cases_documents.document_id,
                                    char_sponsorship_cases_documents.status,
                                    doc_files.filepath");
        return $query->get();

    }

    public static function prepareImport($options){

        $user = \Auth::user();
        $organization_id = $user->organization_id;
        $condition=[];
        $filters = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number'];
        $cases = ['status'];

        foreach ($options as $key => $value) {
            if(in_array($key, $filters)) {
                if ($value != "" && !is_null($value)) {
                    array_push($condition, ['char_persons.' . $key =>$value]);
                }
            } elseif(in_array($key, $cases)) {
                if ($value != "" && !is_null($value)) {
                    array_push($condition, ['char_sponsorship_cases.' . $key =>$value]);
                }
            }
        }

        $query=\DB::table('char_sponsorship_cases')
                ->join('char_cases', function($q) use ($options,$user){
                    $q->on('char_cases.id', '=', 'char_sponsorship_cases.case_id');
                    $q->where('char_cases.category_id',$options['category_id']);
//                    $q->where('char_cases.organization_id',$user->organization_id);
                })
                ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
                ->where('char_sponsorship_cases.sponsor_id',$options['sponsor_id']);

        if(isset($options['items'])){
           if(sizeof($options['items']) > 0 ){
              $query->whereIn('char_sponsorship_cases.id',$options['items']);  
           }      
        } 
            
        $all_organization=null;
        if(isset($options['all_organization'])){
            if($options['all_organization'] ===true){
                $all_organization=0;
            }elseif($options['all_organization'] ===false){
                $all_organization=1;

            }
        }

        if($all_organization ==0){

            $organizations=[];
            if(isset($options['organization_ids']) && $options['organization_ids'] !=null && $options['organization_ids'] !="" && $options['organization_ids'] !=[] && sizeof($options['organization_ids']) != 0) {
                if($options['organization_ids'][0]==""){
                    unset($options['organization_ids'][0]);
                }
                $organizations =$options['organization_ids'];
            }

            if(!empty($organizations)){
                $query->wherein('char_cases.organization_id',$organizations);
            }else{

                $user = \Auth::user();
                if($user->type == 2) {
                    $query->where(function ($anq) use ($organization_id,$user) {
                        $anq->where('char_cases.organization_id',$organization_id);
                        $anq->orwherein('char_cases.organization_id', function($decq) use($user) {
                            $decq->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });

                }else{
                    $query->where(function ($anq) use ($organization_id) {
                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                        $anq->wherein('char_cases.organization_id', function($decq) use($organization_id) {
                            $decq->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $organization_id);
                        });
                    });
                }

            }
        }
        elseif($all_organization ==1){
            $query->where('char_cases.organization_id',$organization_id);

        }

        if (count($condition) != 0) {
            $query->where(function ($q) use ($condition) {
                $names = [
                    'char_persons.first_name', 'char_persons.second_name', 'char_persons.third_name', 'char_persons.last_name'];
                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $names)) {
                           $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });

        }

        $query=$query ->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as full_name,
                                   char_persons.id_card_number,char_persons.old_id_card_number,                        
                                   char_sponsorship_cases.sponsor_number,
                                   char_sponsorship_cases.sponsor_id,
                                   char_sponsorship_cases.id,
                                   char_sponsorship_cases.case_id,
                                   char_cases.person_id
                    ")->groupBy('char_sponsorship_cases.id')
                ->get();

        return $query;
    }

    public static function createNew($attributes)
    {
        return \Illuminate\Support\Facades\DB::transaction(function() use ($attributes) {
            $file = \Document\Model\File::forceCreate([
                'name' => $attributes['name'],
                'filepath' => $attributes['filepath'],
                'size' => $attributes['size'],
                'mimetype' => $attributes['mimetype'],
                'file_status_id' => 1,
                'created_by' => $attributes['user_id'],
            ]);
            if ($file->id) {
                $caseFile = self::forceCreate([
                    'sponsorship_cases_id' => $attributes['sponsorship_cases_id'],
                    'document_type_id' => $attributes['document_type_id'],
                    'doc_category_id' => $attributes['doc_category_id'],
                    'date_from' => $attributes['date_from'],
                    'date_to' => $attributes['date_to'],
                    'document_id' => $file->id,
                    'status' => 1,
                ]);

                return $caseFile;
            }
        });
    }


}
