<?php
namespace Sponsorship\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Sponsorship\Model\Sponsorships;
use Sponsorship\Model\SponsorshipsPerson;
use Sponsorship\Model\SponsorshipCases;

class nominationController extends Controller
{
    public function __construct()
        {
            $this->middleware('auth:api');
        }

    // paginate sponsorships  of logged user organization
    public function filterSponsorships(Request $request)
    {
        $this->authorize('manage',Sponsorships::class);
        $user = \Auth::user();
        return response()->json(Sponsorships::filterSponsorships($user->organization_id,$request->all(),$request->page));
    }

    // export person of sponsorship using sponsorship_id
    public function exportPerson($id)
    {
        $this->authorize('export',SponsorshipsPerson::class);

        $result = SponsorshipsPerson::fetch($id);
        $data=[];
        if(sizeof($result) !=0){
            foreach($result as $key =>$value){
                $nomination[$key][trans('setting::application.#')]=$key+1;
                foreach($value as $k =>$v){
                    if(substr( $k, 0, 7 ) === "mother_"){
                        $data[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.mother')  ." ) "]= $v;
                    }elseif(substr( $k, 0, 7 ) === "father_"){
                        $data[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.father')  ." ) "]= $v;
                    }elseif(substr( $k, 0, 9 ) === "guardian_"){
                        $data[$key][trans('sponsorship::application.'.substr($k,9))." ( ".trans('sponsorship::application.guardian')  ." ) " ]= $v;
                    }else{
                        $data[$key][trans('sponsorship::application.' . $k)]= $v;
                    }
                }
            }

            $token = md5(uniqid());
            \Excel::create('export_' . $token,  function($excel) use($data) {
                $excel->setTitle(trans('lang.excel_description_b'));
                $excel->sheet('Sheetname', function($sheet) use($data) {


                    $sheet->setStyle([
                        'font' => [
                            'name' => 'Calibri',
                            'size' => 11,
                            'bold' => true
                        ]
                    ]);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setRightToLeft(true);
                    $style = [
                         'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font' =>[
                            'name'      =>  'Simplified Arabic',
                            'size'      =>  12,
                            'bold'      =>  true
                        ]
                    ];

                    $sheet->getStyle("A1:BE1")->applyFromArray($style);
                    $sheet->setHeight(1,30);

                    $style = [
                         'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font' =>[
                            'name'      =>  'Simplified Arabic',
                            'size'      =>  12,
                            'bold' => false
                        ]
                    ];

                    $sheet->getDefaultStyle()->applyFromArray($style);
                    $sheet->fromArray($data);
                });
            })
                ->store('xlsx', storage_path('tmp/'));
            $target=Sponsorships::findorfail($id);
            \Log\Model\Log::saveNewLog('SPONSORSHIPS_PERSON_EXPORTED',trans('sponsorship::application.has exported a list of candidates') . ' "'.$target->sponsorship_number. '" ');

            return response()->json(['download_token' => $token]);
        }else{
            return 0;
        }

    }

    // get sponsorship using sponsorship_id
    public function show($id)
    {
        $this->authorize('view',Sponsorships::class);
        $user = \Auth::user();
        $Sponsorship =Sponsorships::fetch($id) ;
        if($Sponsorship){
            $Sponsorship->is_mine = false;
            if($Sponsorship->organization_id == $user->organization_id){
                $Sponsorship->is_mine = true;
            }
        }

        return response()->json($Sponsorship);
    }

    // get sponsorship of sponsor_id and logged organization_id
    public function getList($id){
        $user = \Auth::user();
        return response()->json(['list'=>Sponsorships::getList($id,$user->organization_id)]);
    }

    // update status of sponsorship using sponsorship_id and person_id
    public function update(Request $request,$id)
    {
        $response = array();
        $this->authorize('update',SponsorshipsPerson::class);
        $updated=SponsorshipsPerson::where(['person_id'=>$request->person_id,'sponsorship_id'=> $id])
            ->update(['status'=>$request->get('status')]);

        if($updated){
            $person = \Common\Model\Person::fetch(array('id' => $request->get('person_id'),'full_name'=>true));
            $name=$person->full_name;

            \Log\Model\Log::saveNewLog('SPONSORSHIPS_PERSONS_UPDATED',trans('sponsorship::application.Modified candidate status:') . ' "'.$name. '" ');
            $response["status"]= 'success';
            $response["msg"]= trans('sponsorship::application.The row is updated');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sponsorship::application.There is no update');
        }

        return response()->json($response);
    }

    // update status of sponsorship using sponsorship_id
    public function updateStatus(Request $request,$id)
    {
        $this->authorize('update',Sponsorships::class);

        $status=$request->get('status');
        $count=0;
        $size=sizeof($request->get('sponsorship'));

        foreach ($request->get('sponsorship') as $key =>$value) {
            $updated=Sponsorships::where(['id' =>$value ])->update(['status' => $status ]);
            if(!$updated){
                $count++;
            }
        }
        if($count==0){
            $response["status"]= 'success';
            $response["msg"]= trans('sponsorship::application.The row is updated');
        }elseif($count!=0 && $count < $size ){
            \Log\Model\Log::saveNewLog('SPONSORSHIP_UPDATED',trans('sponsorship::application.edited the case for a number of nominations:')  .'" ' .$size - $count.' "');
            $response["status"]= 'failed_some';
            $response["msg"]= trans('sponsorship::application.some row is not updated');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sponsorship::application.There is no update');
        }

        return response()->json($response);
    }

    // paginate sponsorships persons  of sponsorship using id
    public function persons(Request $request,$id)
    {
        $this->authorize('manage',SponsorshipsPerson::class);
        return response()->json(SponsorshipCases::paginatePerson($id,$request->page,$request->itemsCount));
    }

    // delete case from sponsorship using sponsorship_id and case_id
    public function destroy( $id,Request $request)
    {
        $this->authorize('delete',SponsorshipsPerson::class);

        $target = SponsorshipCases::where('id', $id)->first();
        $case = \Common\Model\CaseModel::fetch(array('full_name'=>true),$target->case_id);

        $person_id = $target->person_id;
        $sponsorship_id = $target->sponsorship_id;
        $response=array();

        if(SponsorshipCases::where('id', $id)->delete()){
            SponsorshipsPerson::where(['person_id' =>$person_id ,
                'status' =>1,
                'sponsorship_id'=>$sponsorship_id])->delete();
            $response["status"]= 'success';
            \Log\Model\Log::saveNewLog('SPONSORSHIPS_PERSON_DELETED',trans('sponsorship::application.Deleted candidate for sponsor:') . ' "'.$case->full_name. '" ');
            $response["msg"]= trans('setting::application.The row is deleted from db');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('setting::application.The row is not deleted from db');
        }
        return response()->json($response);

    }

}
