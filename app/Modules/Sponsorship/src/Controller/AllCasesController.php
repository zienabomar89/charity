<?php
namespace Sponsorship\Controller;

use App\Http\Controllers\Controller;
use App\Http\Helpers;
use Illuminate\Http\Request;
use Mockery\Exception;
use Sponsorship\Model\Sponsorships;
use Sponsorship\Model\SponsorshipsPerson;
use Setting\Model\Setting;
use Common\Model\BlockIDCard;
use Sponsorship\Model\SponsorshipCases;
use Sponsorship\Model\SponsorshipCasesStatusLog;
use Organization\Model\Organization;

class AllCasesController extends Controller
{
        public function __construct()
        {
            $this->middleware('auth:api');
        }

        // get families of Sponsorships Cases according data of (father or mother or guardian )
         public function getFamilies(Request $request)
        {
            
            ini_set('max_execution_time', 0);
            set_time_limit(0);
            Ini_set('memory_limit', '2048M');
            
            $user = \Auth::user();
            
            $Org=Organization::findorfail($user->organization_id);
            $master=true;
            if($Org->parent_id){
                $master=false;
            }

            $result= \Common\Model\SponsorshipsCases::getfamilies($request->page, $user->organization_id,$request->all());
            if($request->get('action')=="filters"){
                $this->authorize('manage',\Common\Model\SponsorshipsCases::class);
                return response()->json(['master'=>$master,'families'  => $result] );
            }else{
                $this->authorize('export',\Common\Model\SponsorshipsCases::class);
                if(sizeof($result) !=0){
                    $data=[];
                    foreach($result as $key =>$value){
                        $data[$key][trans('sponsorship::application.#')]=$key+1;
                        foreach($value as $k =>$v){
                            if($k !='id' && $k !='p_id'&& $k !='father_id'){
                                $data[$key][trans('sponsorship::application.' . $k)]= $v;
                            }
                        }
                    }
                    $token = md5(uniqid());
                    \Excel::create('export_' . $token, function($excel) use($data) {
                        $excel->sheet(trans('sponsorship::application.families'), function($sheet) use($data) {

                            $sheet->setStyle([
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 11,
                                    'bold' => true
                                ]
                            ]);
                            $sheet->setAllBorders('thin');
                            $sheet->setfitToWidth(true);
                            $sheet->setRightToLeft(true);
                            $style = [
                               'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' =>[
                                    'name'      =>  'Simplified Arabic',
                                    'size'      =>  12,
                                    'bold' => false
                                ]
                            ];

                            $sheet->getDefaultStyle()->applyFromArray($style);
                            $sheet->fromArray($data);
                        });
                    })
                        ->store('xlsx', storage_path('tmp/'));
                    return response()->json(['download_token' => $token]);
                }else{
                    return response()->json(['status' => false]);
                }
            }
        }

        // list cases of Sponsor according (Sponsor_id & category_id )
        public function getSponsorCasesList($id,Request $request){
            return response()->json( SponsorshipCases::getSponsorCasesList($id,$request->category_id));
        }

       // get cases of Sponsor according filters ( ExportToWord , paginate , xlsx )
        public function getSponsorCases(Request $request)
        {
           $this->authorize('manageSponsor',\Common\Model\SponsorshipsCases::class);
            $user = \Auth::user();
            $items= SponsorshipCases::getPerson($request->page,$user->organization_id,$request->all());

            if($request->get('action') =='export'){
                     if(sizeof($items) !=0){
                        foreach($items as $key =>$value){
                            $dat[$key][trans('setting::application.#')]=$key+1;
                            foreach($value as $k =>$v){
                                if($k != 'id' && $k != 'sponsor_id'&& $k != 'flag'&& $k != 'date_flag'){
                                    if(substr( $k, 0, 7 ) === "mother_"){
                                        $dat[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.mother')  ." ) "]= $v;
                                    }elseif(substr( $k, 0, 7 ) === "father_"){
                                        $dat[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.father')  ." ) "]= $v;
                                    }elseif(substr( $k, 0, 9 ) === "guardian_"){
                                        $dat[$key][trans('sponsorship::application.'.substr($k,9))." ( ".trans('sponsorship::application.guardian')  ." ) " ]= $v;
                                    }else{
                                        $dat[$key][trans('sponsorship::application.' . $k)]= $v;
                                    }
                                }
                            }
                        }
                        $data=$dat;
                         $token = md5(uniqid());
                         \Excel::create('export_' . $token, function($excel) use($data) {
                            $excel->sheet('Sheetname', function($sheet) use($data) {


                                $sheet->setStyle([
                                    'font' => [
                                        'name' => 'Calibri',
                                        'size' => 11,
                                        'bold' => true
                                    ]
                                ]);
                                $sheet->setAllBorders('thin');
                                $sheet->setfitToWidth(true);
                                $sheet->setRightToLeft(true);
                                $style = [
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font' =>[
                                        'name'      =>  'Simplified Arabic',
                                        'size'      =>  12,
                                        'bold'      =>  true
                                    ]
                                ];

                                $sheet->getStyle("A1:CE1")->applyFromArray($style);
                                $sheet->setHeight(1,30);

                                $style = [
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font' =>[
                                        'name'      =>  'Simplified Arabic',
                                        'size'      =>  12,
                                        'bold' => false
                                    ]
                                ];

                                $sheet->getDefaultStyle()->applyFromArray($style);
                                $sheet->fromArray($data);
                            });
                        })
                             ->store('xlsx', storage_path('tmp/'));
                         return response()->json([
                             'download_token' => $token,
                         ]);
                    }else{
                        return 0;
                     }
            }
            else if($request->get('action') =='ExportToWord'){


                $lang = 'ar';
                if($request->lang){
                    $lang = $request->lang;
                }

                $dirName = base_path('storage/app/templates/statistics');
                if (!is_dir($dirName)) {
                    @mkdir($dirName, 0777, true);
                }

                foreach($items as $item){
                    $Template=\Common\Model\Template::where(['category_id'=>$item->category_id,'organization_id'=>$item->sponsor_id])->first();
                    if($Template){
                        if($lang== 'en'){
                            $path = base_path('storage/app/'.$Template->en_filename);
                        }else{
                            $path = base_path('storage/app/'.$Template->filename);
                        }
                    }else{
                        $default_template=Setting::where(['id'=>'Sponsorship-default-template','organization_id'=>$item->organization_id])->first();
                        if($default_template){
                            $path = base_path('storage/app/'.$default_template->value);
                        }else{
                            $path = base_path('storage/app/templates/Sponsorship-default-template.docx');
                        }
                    }

                    $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);

                    $templateProcessor->setValue('category_name', $item->category_name);
                    $templateProcessor->setValue('first_name', $item->first_name);
                    $templateProcessor->setValue('second_name', $item->second_name);
                    $templateProcessor->setValue('third_name', $item->third_name );
                    $templateProcessor->setValue('last_name', $item->last_name );
                    $templateProcessor->setValue('name', $item->name);
                    $templateProcessor->setValue('id_card_number', $item->id_card_number);
                    $templateProcessor->setValue('brothers', $item->brothers);
                    $templateProcessor->setValue('birthday', $item->birthday);
                    $templateProcessor->setValue('gender', $item->gender);
                    $templateProcessor->setValue('birth_place', $item->birth_place);
                    $templateProcessor->setValue('nationality', $item->nationality);
                    $templateProcessor->setValue('country', $item->country);
                    $templateProcessor->setValue('governarate', $item->governarate);
                    $templateProcessor->setValue('city', $item->city);
                    $templateProcessor->setValue('location_id', $item->location_id);
                    $templateProcessor->setValue('street_address', $item->street_address);
                    $templateProcessor->setValue('address', $item->address);
                    $templateProcessor->setValue('phone', $item->phone);
                    $templateProcessor->setValue('primary_mobile', $item->primary_mobile);
                    $templateProcessor->setValue('wataniya', $item->wataniya);
                    $templateProcessor->setValue('secondary_mobile', $item->secondary_mobile );
                    $templateProcessor->setValue('guardian_kinship', $item->guardian_kinship);
                    $templateProcessor->setValue('en_first_name', $item->en_first_name);
                    $templateProcessor->setValue('en_second_name', $item->en_second_name);
                    $templateProcessor->setValue('en_third_name', $item->en_third_name);
                    $templateProcessor->setValue('en_last_name', $item->en_last_name);
                    $templateProcessor->setValue('health_status', $item->health_status);
                    $templateProcessor->setValue('details', $item->details);
                    $templateProcessor->setValue('disease_id', $item->disease_id);
                    $templateProcessor->setValue('disease_id', $item->disease_id);
                    $templateProcessor->setValue('property_types_id', $item->property_types_id);
                    $templateProcessor->setValue('roof_materials_id', $item->roof_materials_id);
                    $templateProcessor->setValue('area', $item->area);
                    $templateProcessor->setValue('rooms', $item->rooms);
                    $templateProcessor->setValue('residence_condition', $item->residence_condition);
                    $templateProcessor->setValue('indoor_condition', $item->indoor_condition);
                    $templateProcessor->setValue('study', $item->study);
                    $templateProcessor->setValue('type', $item->type);
                    $templateProcessor->setValue('level', $item->level);
                    $templateProcessor->setValue('year', $item->year);
                    $templateProcessor->setValue('points', $item->points);
                    $templateProcessor->setValue('school', $item->school);
                    $templateProcessor->setValue('grade', $item->grade);
                    $templateProcessor->setValue('authority', $item->authority);
                    $templateProcessor->setValue('stage', $item->stage);
                    $templateProcessor->setValue('degree', $item->degree);
                    $templateProcessor->setValue('prayer', $item->prayer);
                    $templateProcessor->setValue('prayer_reason', $item->prayer_reason);
                    $templateProcessor->setValue('quran_reason', $item->quran_reason);
                    $templateProcessor->setValue('quran_parts', $item->quran_parts);
                    $templateProcessor->setValue('quran_chapters', $item->quran_chapters);
                    $templateProcessor->setValue('save_quran', $item->save_quran);
                    $templateProcessor->setValue('bank_name', $item->bank_name);
                    $templateProcessor->setValue('branch_name', $item->branch_name);
                    $templateProcessor->setValue('account_number', $item->account_number);
                    $templateProcessor->setValue('account_owner', $item->account_owner);
                    $templateProcessor->setValue('visitor_note', $item->visitor_note);
                    $templateProcessor->setValue('guardian_kinship', $item->guardian_kinship);
                    $templateProcessor->setValue('guardian_is', $item->guardian_is);
                    $templateProcessor->setValue('mother_is_guardian', $item->mother_is_guardian);

                    if($item->father != null){
                        $templateProcessor->setValue('father_first_name', $item->father->first_name);
                        $templateProcessor->setValue('father_second_name', $item->father->second_name);
                        $templateProcessor->setValue('father_third_name', $item->father->third_name );
                        $templateProcessor->setValue('father_last_name', $item->father->last_name );
                        $templateProcessor->setValue('father_name', $item->father->name);
                        $templateProcessor->setValue('father_spouses', $item->father->spouses);
                        $templateProcessor->setValue('father_id_card_number', $item->father->id_card_number);
                        $templateProcessor->setValue('father_birthday', $item->father->birthday);
                        $templateProcessor->setValue('father_en_first_name', $item->father->en_first_name);
                        $templateProcessor->setValue('father_en_second_name', $item->father->en_second_name);
                        $templateProcessor->setValue('father_en_third_name', $item->father->en_third_name);
                        $templateProcessor->setValue('father_en_last_name', $item->father->en_last_name);
                        $templateProcessor->setValue('father_health_status', $item->father->health_status);
                        $templateProcessor->setValue('father_details', $item->father->details);
                        $templateProcessor->setValue('father_disease_id', $item->father->disease_id);
                        $templateProcessor->setValue('father_disease_id', $item->father->disease_id);
                        $templateProcessor->setValue('father_specialization', $item->father->specialization);
                        $templateProcessor->setValue('father_degree', $item->father->degree);
                        $templateProcessor->setValue('father_alive', $item->father->alive);
                        $templateProcessor->setValue('father_is_alive', $item->father->is_alive);
                        $templateProcessor->setValue('father_death_date', $item->father->death_date);
                        $templateProcessor->setValue('father_death_causes_id', $item->father->death_causes_id);
                        $templateProcessor->setValue('father_working', $item->father->working);
                        $templateProcessor->setValue('father_work_job_id', $item->father->work_job_id);
                        $templateProcessor->setValue('father_work_location', $item->father->work_location);
                        $templateProcessor->setValue('father_monthly_income', $item->father->monthly_income);
                    }else{
                        $templateProcessor->setValue('father_first_name', '-');
                        $templateProcessor->setValue('father_second_name', '-');
                        $templateProcessor->setValue('father_third_name', '-');
                        $templateProcessor->setValue('father_last_name', '-');
                        $templateProcessor->setValue('father_name', '-');
                        $templateProcessor->setValue('father_spouses', '-');
                        $templateProcessor->setValue('father_id_card_number','-');
                        $templateProcessor->setValue('father_birthday', '-');
                        $templateProcessor->setValue('father_en_first_name','-');
                        $templateProcessor->setValue('father_en_second_name', '-');
                        $templateProcessor->setValue('father_en_third_name', '-');
                        $templateProcessor->setValue('father_en_last_name', '-');
                        $templateProcessor->setValue('father_health_status', '-');
                        $templateProcessor->setValue('father_health_details','-');
                        $templateProcessor->setValue('father_diseases_name', '-');
                        $templateProcessor->setValue('father_specialization', '-');
                        $templateProcessor->setValue('father_degree', '-');
                        $templateProcessor->setValue('father_alive', '-');
                        $templateProcessor->setValue('father_is_alive', '-');
                        $templateProcessor->setValue('father_death_date', '-');
                        $templateProcessor->setValue('father_death_cause_name', '-');
                        $templateProcessor->setValue('father_working', '-');
                        $templateProcessor->setValue('father_work_job', '-');
                        $templateProcessor->setValue('father_work_location', '-');
                        $templateProcessor->setValue('father_monthly_income', '-');
                    }
                    if($item->mother != null){
                        $templateProcessor->setValue('mother_prev_family_name', $item->mother->prev_family_name);
                        $templateProcessor->setValue('mother_name', $item->mother->name);
                        $templateProcessor->setValue('mother_first_name', $item->mother->first_name);$templateProcessor->setValue('mother_second_name', $item->mother->second_name);
                        $templateProcessor->setValue('mother_third_name', $item->mother->third_name);
                        $templateProcessor->setValue('mother_last_name', $item->mother->last_name);
                        $templateProcessor->setValue('mother_name', $item->mother->name);
                        $templateProcessor->setValue('mother_id_card_number', $item->mother->id_card_number);
                        $templateProcessor->setValue('mother_birthday', $item->mother->birthday);
                        $templateProcessor->setValue('mother_marital_status_id', $item->mother->marital_status_id);
                        $templateProcessor->setValue('mother_nationality', $item->mother->nationality);
                        $templateProcessor->setValue('mother_en_first_name', $item->mother->en_first_name);
                        $templateProcessor->setValue('mother_en_second_name', $item->mother->en_second_name);
                        $templateProcessor->setValue('mother_en_third_name', $item->mother->en_third_name);
                        $templateProcessor->setValue('mother_en_last_name', $item->mother->en_last_name);
                        $templateProcessor->setValue('mother_health_status', $item->mother->health_status);
                        $templateProcessor->setValue('mother_details', $item->mother->details);
                        $templateProcessor->setValue('mother_disease_id', $item->mother->disease_id);
                        $templateProcessor->setValue('mother_specialization', $item->mother->specialization);
                        $templateProcessor->setValue('mother_stage', $item->mother->stage);
                        $templateProcessor->setValue('mother_alive', $item->mother->alive);
                        $templateProcessor->setValue('mother_is_alive', $item->mother->is_alive);
                        $templateProcessor->setValue('mother_death_date', $item->mother->death_date);
                        $templateProcessor->setValue('mother_death_causes_id', $item->mother->death_causes_id);
                        $templateProcessor->setValue('mother_working', $item->mother->working);
                        $templateProcessor->setValue('mother_work_job_id', $item->mother->work_job_id);
                        $templateProcessor->setValue('mother_work_location', $item->mother->work_location);
                        $templateProcessor->setValue('mother_monthly_income', $item->mother->monthly_income);
                    }else{
                        $templateProcessor->setValue('mother_prev_family_name','-');
                        $templateProcessor->setValue('mother_first_name', '-');
                        $templateProcessor->setValue('mother_second_name', '-');
                        $templateProcessor->setValue('mother_third_name', '-');
                        $templateProcessor->setValue('mother_last_name','-');
                        $templateProcessor->setValue('mother_name', '-');
                        $templateProcessor->setValue('mother_id_card_number', '-');
                        $templateProcessor->setValue('mother_birthday', '-');
                        $templateProcessor->setValue('mother_marital_status_name', '-');
                        $templateProcessor->setValue('mother_nationality', '-');
                        $templateProcessor->setValue('mother_en_first_name', '-');
                        $templateProcessor->setValue('mother_en_second_name', '-');
                        $templateProcessor->setValue('mother_en_third_name', '-');
                        $templateProcessor->setValue('mother_en_last_name', '-');
                        $templateProcessor->setValue('mother_health_status', '-');
                        $templateProcessor->setValue('mother_health_details','-');
                        $templateProcessor->setValue('mother_diseases_name', '-');
                        $templateProcessor->setValue('mother_specialization','-');
                        $templateProcessor->setValue('mother_stage', '-');
                        $templateProcessor->setValue('mother_alive', '-');
                        $templateProcessor->setValue('mother_is_alive', '-');
                        $templateProcessor->setValue('mother_death_date', '-');
                        $templateProcessor->setValue('mother_death_cause_name', '-');
                        $templateProcessor->setValue('mother_working', '-');
                        $templateProcessor->setValue('mother_work_job', '-');
                        $templateProcessor->setValue('mother_work_location', '-');
                        $templateProcessor->setValue('mother_monthly_income', '-');
                    }
                    if($item->guardian != null){
                        $templateProcessor->setValue('guardian_first_name', $item->guardian->first_name);
                        $templateProcessor->setValue('guardian_second_name', $item->guardian->second_name);
                        $templateProcessor->setValue('guardian_third_name', $item->guardian->third_name );
                        $templateProcessor->setValue('guardian_last_name', $item->guardian->last_name );
                        $templateProcessor->setValue('guardian_name', $item->guardian->name);
                        $templateProcessor->setValue('guardian_id_card_number', $item->guardian->id_card_number);
                        $templateProcessor->setValue('guardian_birthday', $item->guardian->birthday);
                        $templateProcessor->setValue('guardian_birth_place', $item->guardian->birth_place);
                        $templateProcessor->setValue('guardian_nationality', $item->guardian->nationality);
                        $templateProcessor->setValue('guardian_country', $item->guardian->country);
                        $templateProcessor->setValue('guardian_governarate', $item->guardian->governarate);
                        $templateProcessor->setValue('guardian_city', $item->guardian->city);
                        $templateProcessor->setValue('guardian_location_id', $item->guardian->location_id);
                        $templateProcessor->setValue('guardian_street_address', $item->guardian->street_address);
                        $templateProcessor->setValue('guardian_address', $item->guardian->address);
                        $templateProcessor->setValue('guardian_phone', $item->guardian->phone);
                        $templateProcessor->setValue('guardian_primary_mobile', $item->guardian->primary_mobile);
                        $templateProcessor->setValue('guardian_wataniya', $item->guardian->wataniya);
                        $templateProcessor->setValue('guardian_secondery_mobile', $item->guardian->secondery_mobile );
                        $templateProcessor->setValue('guardian_en_first_name', $item->guardian->en_first_name);
                        $templateProcessor->setValue('guardian_en_second_name', $item->guardian->en_second_name);
                        $templateProcessor->setValue('guardian_en_third_name', $item->guardian->en_third_name);
                        $templateProcessor->setValue('guardian_en_last_name', $item->guardian->en_last_name);
                        $templateProcessor->setValue('guardian_property_types_id', $item->guardian->property_types_id);
                        $templateProcessor->setValue('guardian_roof_materials_id', $item->guardian->roof_materials_id);
                        $templateProcessor->setValue('guardian_area', $item->guardian->area);
                        $templateProcessor->setValue('guardian_rooms', $item->guardian->rooms);
                        $templateProcessor->setValue('guardian_residence_condition', $item->guardian->residence_condition);
                        $templateProcessor->setValue('guardian_indoor_condition', $item->guardian->indoor_condition);
                        $templateProcessor->setValue('guardian_stage', $item->guardian->stage);
                        $templateProcessor->setValue('guardian_bank_name', $item->guardian->bank_name);
                        $templateProcessor->setValue('guardian_branch_name', $item->guardian->branch_name);
                        $templateProcessor->setValue('guardian_account_number', $item->guardian->account_number);
                        $templateProcessor->setValue('guardian_account_owner', $item->guardian->account_owner);
                        $templateProcessor->setValue('guardian_working', $item->guardian->working);
                        $templateProcessor->setValue('guardian_work_job_id', $item->guardian->work_job_id);
                        $templateProcessor->setValue('guardian_work_location', $item->guardian->work_location);
                        $templateProcessor->setValue('guardian_monthly_income', $item->guardian->monthly_income);
                    }else{

                        $templateProcessor->setValue('guardian_first_name','-');
                        $templateProcessor->setValue('guardian_second_name', '-');
                        $templateProcessor->setValue('guardian_third_name', '-');
                        $templateProcessor->setValue('guardian_last_name', '-');
                        $templateProcessor->setValue('guardian_name', '-');
                        $templateProcessor->setValue('guardian_id_card_number', '-');
                        $templateProcessor->setValue('guardian_birthday', '-');
                        $templateProcessor->setValue('guardian_birth_place', '-');
                        $templateProcessor->setValue('guardian_nationality', '-');
                        $templateProcessor->setValue('guardian_country', '-');
                        $templateProcessor->setValue('guardian_governarate', '-');
                        $templateProcessor->setValue('guardian_city', '-');
                        $templateProcessor->setValue('guardian_location_id', '-');
                        $templateProcessor->setValue('guardian_street_address', '-');
                        $templateProcessor->setValue('guardian_address', '-');
                        $templateProcessor->setValue('guardian_phone', '-');
                        $templateProcessor->setValue('guardian_primary_mobile', '-');
                        $templateProcessor->setValue('guardian_secondery_mobile', '-');
                        $templateProcessor->setValue('guardian_en_first_name', '-');
                        $templateProcessor->setValue('guardian_en_second_name', '-');
                        $templateProcessor->setValue('guardian_en_third_name', '-');
                        $templateProcessor->setValue('guardian_en_last_name', '-');
                        $templateProcessor->setValue('guardian_property_types', '-');
                        $templateProcessor->setValue('guardian_roof_materials', '-');
                        $templateProcessor->setValue('guardian_area', '-');
                        $templateProcessor->setValue('guardian_rooms', '-');
                        $templateProcessor->setValue('guardian_residence_condition', '-');
                        $templateProcessor->setValue('guardian_indoor_condition', '-');
                        $templateProcessor->setValue('guardian_stage','-');
                        $templateProcessor->setValue('guardian_working', '-');
                        $templateProcessor->setValue('guardian_work_job', '-');
                        $templateProcessor->setValue('guardian_work_location', '-');
                        $templateProcessor->setValue('guardian_monthly_income', '-');
                    }

                    try{
                        $templateProcessor->cloneRow('rowId', sizeof($item->family));

                        if (sizeof($item->family)!=0){
                            $z=1;
                            foreach($item->family as $k=>$v) {
                                $templateProcessor->setValue('rowId#' . $z, $z);
                                $templateProcessor->setValue('rowFmN#' . $z, $v->name);
                                $templateProcessor->setValue('rowFmBD#' . $z, $v->birthday);
                                $templateProcessor->setValue('rowFmGen#' . $z, $v->gender);
                                $templateProcessor->setValue('rowFmID#' . $z, $v->id_card_number);
                                $templateProcessor->setValue('rowFmStudy#' . $z, $v->study);
                                $templateProcessor->setValue('rowFmEStage#' . $z, $v->stage);
                                $templateProcessor->setValue('rowFmHStatus#' . $z, $v->health_status);
                                $z++;
                            }
                        }
                    }catch (\Exception $e){}
                    try{
                        $templateProcessor->cloneRow('rowDid', sizeof($item->documents));

                        if (sizeof($item->documents)!=0){
                            $r2=1;
                            foreach($item->documents as $k=>$v) {
                                $templateProcessor->setValue('rowDid#' . $r2, $r2);
                                $templateProcessor->setValue('rowDname#' . $r2, $v->name);
                                $templateProcessor->setImg('rowDimg#'. $r2, array('src'=> base_path('storage/app/').$v->filepath,'swh'=>'950'));
                                $r2++;
                            }
                        }
                    }catch (\Exception $e){}

                    $templateProcessor->saveAs($dirName.'/'.$item->organization_name.'-'.$item->category_name.'-'.$item->name.'.docx');
                }

                $token = md5(uniqid());
                $zipFileName = storage_path('tmp/export_' . $token . '.zip');

                $zip = new \ZipArchive;
                if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
                {
                    foreach ( glob( $dirName . '/*' ) as $fileName )
                    {
                        $file = basename( $fileName );
                        $zip->addFile( $fileName, $file );
                    }

                    $zip->close();
                    array_map('unlink', glob("$dirName/*.*"));
                    rmdir($dirName);
                }

                \Log\Model\Log::saveNewLog('CASE_EXPORTED',trans('sponsorship::application.Exported sponsorship forms'));

                return response()->json(['download_token' => $token,]);

            }

            return response()->json(['cases'  =>$items ] );

        }

       // get Sponsor case info according ( sponsorship_case_id , case_id ,sponsor_id)
        public function getSponsorCaseInfo($id,Request $request)
        {
            $response=array();
            $sponsorshipCases =SponsorshipCases::where('id',$id)->first();
            $case =\Common\Model\CaseModel::fetch(array(
                'action' => $request->get('action'),
                'category_type' => 1,
                'category'      => true,
                'full_name'     => true,
                'father_id'     => true,
                'mother_id'     => true,
                'guardian_id'   => true,
            ),$sponsorshipCases->case_id);

            if (!$case) {
                //throw (new \Illuminate\Database\Eloquent\ModelNotFoundException)->setModel(CaseModel::class);
                return response()->json(array(
                    'error' => 'Not found!'
                ), 404);
            }
            //$this->authorize('view', $case);
            $case->sponsor_id=$sponsorshipCases->sponsor_id;
            return response()->json($case);
        }

        // export word form of case according Sponsor template
        public function exportSponsorCase(Request $request){

            $this->authorize('exportSponsor',\Common\Model\SponsorshipsCases::class);

            $id= $request->id_;
            $target= $request->target;
            $case=SponsorshipCases::where('id',$id)->first();
            $lang = 'ar';
            if($request->lang){
                $lang = $request->lang;
            }
            $item =\Common\Model\CaseModel::fetch(array(
                'action' => 'show',
                'category'      => true, 'full_name'       => true, 'person'        => true,
                'persons_i18n'  => true, 'contacts'        => true, 'education'     => true,
                'islamic'       => true, 'health'          => true, 'work'          => true,
                'residence'     => true, 'case_needs'      => true, 'default_bank'  => true,
                'banks'         => true, 'reconstructions' => true, 'persons_documents'  => true,
                'parent_detail' => true, 'visitor_note'    => true ,'lang' => $lang
            ),$case->case_id);

            $sponsor=Organization::findorfail($case->sponsor_id);

            $Template=\Common\Model\Template::where(['category_id'=>$item->category_id,'organization_id'=>$case->sponsor_id,'template'=>$target])->first();

            if($Template){
                if($lang== 'en'){
                    $path = base_path('storage/app/'.$Template->en_filename);
                }else{
                    $path = base_path('storage/app/'.$Template->filename);
                }

            }else{
                    $default_template=Setting::where(['id'=>'Sponsorship-default-template','organization_id'=>$item->organization_id])->first();
                    if($default_template){
                        $path = base_path('storage/app/'.$default_template->value);
                    }else{
                        $path = base_path('storage/app/templates/Sponsorship-default-template.docx');
                    }
            }

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);
        $templateProcessor->setValue('Date', date('y-m-d'));
        $templateProcessor->setValue('sponsor_number', $case->sponsor_number);
        $templateProcessor->setValue('sponsor_number', $case->sponsor_number);
        $templateProcessor->setValue('date', date('y-m-d'));
        $templateProcessor->setValue('category_name', $item->category_name);
        $templateProcessor->setValue('first_name', $item->first_name);
        $templateProcessor->setValue('second_name', $item->second_name);
        $templateProcessor->setValue('third_name', $item->third_name );
        $templateProcessor->setValue('last_name', $item->last_name );
        $templateProcessor->setValue('name', $item->full_name);
        $templateProcessor->setValue('en_first_name', $item->en_first_name);
        $templateProcessor->setValue('en_second_name', $item->en_second_name);
        $templateProcessor->setValue('en_third_name', $item->en_third_name);
        $templateProcessor->setValue('en_last_name', $item->en_last_name);
        $templateProcessor->setValue('en_name', $item->en_name);
        $templateProcessor->setValue('id_card_number', $item->id_card_number);
        $templateProcessor->setValue('birthday', $item->birthday);
        $templateProcessor->setValue('gender', $item->gender);
        $templateProcessor->setValue('birth_place', $item->birth_place);
        $templateProcessor->setValue('nationality', $item->nationality);
        $templateProcessor->setValue('refugee', $item->refugee);
        $templateProcessor->setValue('unrwa_card_number', $item->unrwa_card_number);
        $templateProcessor->setValue('marital_status_name', $item->marital_status_name);
        $templateProcessor->setValue('monthly_income', $item->monthly_income);
        $templateProcessor->setValue('address', $item->address);
        $templateProcessor->setValue('country', $item->country);
        $templateProcessor->setValue('governarate', $item->governarate);
        $templateProcessor->setValue('city', $item->city);
        $templateProcessor->setValue('location_id', $item->nearLocation);
        $templateProcessor->setValue('street_address', $item->street_address);
        $templateProcessor->setValue('individual_count', $item->individual_count);
        $templateProcessor->setValue('male_count', $item->male_count);
        $templateProcessor->setValue('female_count', $item->female_count);
        $templateProcessor->setValue('phone', $item->phone);
        $templateProcessor->setValue('primary_mobile', $item->primary_mobile);
        $templateProcessor->setValue('wataniya', $item->wataniya);
        $templateProcessor->setValue('secondary_mobile', $item->secondary_mobile );
        $templateProcessor->setValue('property_types', $item->property_types);
        $templateProcessor->setValue('rent_value', $item->rent_value);
        $templateProcessor->setValue('roof_materials', $item->roof_materials);
        $templateProcessor->setValue('habitable', $item->habitable);
        $templateProcessor->setValue('house_condition', $item->house_condition);
        $templateProcessor->setValue('residence_condition', $item->residence_condition);
        $templateProcessor->setValue('indoor_condition', $item->indoor_condition);
        $templateProcessor->setValue('rooms', $item->rooms);
        $templateProcessor->setValue('area', $item->area);
        $templateProcessor->setValue('working', $item->working);
        $templateProcessor->setValue('works', $item->works);
        $templateProcessor->setValue('can_work', $item->can_work);
        $templateProcessor->setValue('can_works', $item->can_works);
        $templateProcessor->setValue('work_job', $item->work_job);
        $templateProcessor->setValue('work_reason', $item->work_reason);
        $templateProcessor->setValue('work_status', $item->work_status);
        $templateProcessor->setValue('work_wage', $item->work_wage);
        $templateProcessor->setValue('work_location', $item->work_location);
        $templateProcessor->setValue('needs', $item->needs);
        $templateProcessor->setValue('promised', $item->promised);
        $templateProcessor->setValue('reconstructions_organization_name', $item->reconstructions_organization_name);
        $templateProcessor->setValue('visitor', $item->visitor);
        $templateProcessor->setValue('notes', $item->notes);
        $templateProcessor->setValue('visited_at', $item->visited_at);
        $templateProcessor->setValue('health_status', $item->health_status);
        $templateProcessor->setValue('health_details', $item->health_details);
        $templateProcessor->setValue('diseases_name', $item->diseases_name);
        $templateProcessor->setValue('study', $item->study);
        $templateProcessor->setValue('type', $item->type);
        $templateProcessor->setValue('level', $item->level);
        $templateProcessor->setValue('year', $item->year);
        $templateProcessor->setValue('points', $item->points);
        $templateProcessor->setValue('school', $item->school);
        $templateProcessor->setValue('grade', $item->grade);
        $templateProcessor->setValue('authority', $item->authority);
        $templateProcessor->setValue('stage', $item->stage);
        $templateProcessor->setValue('degree', $item->degree);
        $templateProcessor->setValue('prayer', $item->prayer);
        $templateProcessor->setValue('prayer_reason', $item->prayer_reason);
        $templateProcessor->setValue('quran_reason', $item->quran_reason);
        $templateProcessor->setValue('quran_parts', $item->quran_parts);
        $templateProcessor->setValue('quran_chapters', $item->quran_chapters);
        $templateProcessor->setValue('save_quran', $item->save_quran);
        $templateProcessor->setValue('visitor_note', $item->visitor_note);
        $templateProcessor->setValue('guardian_kinship', $item->guardian_kinship);
        $templateProcessor->setValue('guardian_is', $item->guardian_is);
        $templateProcessor->setValue('mother_is_guardian', $item->mother_is_guardian);

            $setting_id = 'sponsorship_custom_form';
            $CaseCustomData =\Forms\Model\FormsCasesData::getCaseCustomData($item->case_id,$item->category_id,$item->organization_id,$setting_id);
            if (sizeof($CaseCustomData)!=0){
                try{
                    $templateProcessor->cloneRow('row_custom_name', sizeof($CaseCustomData));
                    $z=1;
                    foreach($CaseCustomData as $cValue) {
                        $templateProcessor->setValue('row_custom_name#' . $z, $cValue['label']);
                        $templateProcessor->setValue('row_custom_value#' . $z, $cValue['value']);
                        $z++;
                    }

                }catch (\Exception $e){}
            }
            else{
                try{
                    $templateProcessor->cloneRow('row_custom_name',1);
                    $templateProcessor->setValue('row_custom_name#' . 1, '-');
                    $templateProcessor->setValue('row_custom_value#' . 1, '-');
                }catch (\Exception $e){  }
            }

        if (sizeof($item->banks)!=0){
            try{
                $templateProcessor->cloneRow('r_bank_name', sizeof($item->banks));
                $z=1;
                foreach($item->banks as $rec) {
                    $templateProcessor->setValue('r_bank_name#' . $z, $rec->bank_name);
                    $templateProcessor->setValue('r_branch_name#' . $z, $rec->branch);
                    $templateProcessor->setValue('r_account_owner#' . $z, $rec->account_owner);
                    $templateProcessor->setValue('r_account_number#' . $z, $rec->account_number);
                    if($rec->check == true){
                        $templateProcessor->setValue('r_default#' . $z, trans('aid::application.yes') );
                    }else{
                        $templateProcessor->setValue('r_default#' . $z, trans('aid::application.no') );
                    }
                    $z++;
                }
            }catch (\Exception $e){}
        }else{
            try{
                $templateProcessor->cloneRow('r_bank_name', 1);
                $templateProcessor->setValue('r_bank_name#' . 1, '-');
                $templateProcessor->setValue('r_branch_name#' . 1, '-');
                $templateProcessor->setValue('r_account_owner#' . 1, '-');
                $templateProcessor->setValue('r_account_number#' . 1,'-');
                $templateProcessor->setValue('r_default#' . 1, '-');
            }catch (\Exception $e){  }
        }
        if (sizeof($item->home_indoor)!=0){
            try{
                $templateProcessor->cloneRow('row_es_name', sizeof($item->home_indoor));
                $z=1;
                foreach($item->home_indoor as $record) {
                    $templateProcessor->setValue('row_es_name#' . $z, $record->name);
                    $templateProcessor->setValue('row_es_if_exist#' . $z, $record->exist);
                    $templateProcessor->setValue('row_es_needs#' . $z, $record->needs);
                    $templateProcessor->setValue('row_es_condition#' . $z, $record->essentials_condition);
                    $z++;
                }
            }catch (\Exception $e){  }
        }else{
            try{
                $templateProcessor->cloneRow('row_es_name', 1);
                $templateProcessor->setValue('row_es_name#' . 1, '-');
                $templateProcessor->setValue('row_es_if_exist#' . 1, '-');
                $templateProcessor->setValue('row_es_needs#' . 1, '-');
                $templateProcessor->setValue('row_es_condition#' . 1, '-');

            }catch (\Exception $e){  }
        }
        if (sizeof($item->persons_properties)!=0){
            try{
                $templateProcessor->cloneRow('row_pro_name', sizeof($item->persons_properties));
                $z2=1;
                foreach($item->persons_properties as $sub_record) {
                    $templateProcessor->setValue('row_pro_name#' . $z2, $sub_record->name);
                    $templateProcessor->setValue('row_pro_if_exist#' . $z2, $sub_record->has_property);
                    $templateProcessor->setValue('row_pro_count#' . $z2, $sub_record->quantity);
                    $z2++;
                }
            }catch (\Exception $e){  }
        }else{
            try{
                $templateProcessor->cloneRow('row_pro_name', 1);
                $templateProcessor->setValue('row_pro_name#' . 1, '-');
                $templateProcessor->setValue('row_pro_if_exist#' . 1, '-');
                $templateProcessor->setValue('row_pro_count#' . 1, '-');
            }catch (\Exception $e){  }
        }
        if (sizeof($item->financial_aid_source)!=0){
            try{
                $templateProcessor->cloneRow('row_fin_name', sizeof($item->financial_aid_source));
                $z3=1;
                foreach($item->financial_aid_source as $record) {
                    $templateProcessor->setValue('row_fin_name#' . $z3, $record->name);
                    $templateProcessor->setValue('row_fin_if_exist#' . $z3, $record->aid_take);
                    $templateProcessor->setValue('row_fin_count#' . $z3, $record->aid_value);
                    $templateProcessor->setValue('row_fin_currency#' . $z3, $record->currency_name);
                    $z3++;
                }
            }catch (\Exception $e){  }
        }else{
            try{
                $templateProcessor->cloneRow('row_fin_name', 1);
                $templateProcessor->setValue('row_fin_name#' . 1, '-');
                $templateProcessor->setValue('row_fin_if_exist#' . 1, '-');
                $templateProcessor->setValue('row_fin_count#' . 1, '-');
                $templateProcessor->setValue('row_fin_currency#' . 1, '-');
            }catch (\Exception $e){  }
        }
        if (sizeof($item->non_financial_aid_source)!=0){
            try{
                $templateProcessor->cloneRow('row_nonfin_name', sizeof($item->non_financial_aid_source));
                $z3=1;
                foreach($item->non_financial_aid_source as $record) {
                    $templateProcessor->setValue('row_nonfin_name#' . $z3, $record->name);
                    $templateProcessor->setValue('row_nonfin_if_exist#' . $z3, $record->aid_take);
                    $templateProcessor->setValue('row_nonfin_count#' . $z3, $record->aid_value);
                    $z3++;
                }
            }catch (\Exception $e){  }
        }else{
            try{
                $templateProcessor->cloneRow('row_nonfin_name', 1);
                $templateProcessor->setValue('row_nonfin_name#' . 1, '-');
                $templateProcessor->setValue('row_nonfin_if_exist#' . 1, '-');
                $templateProcessor->setValue('row_nonfin_count#' . 1, '-');
            }catch (\Exception $e){  }
        }
        if (sizeof($item->persons_documents)!=0){
            try{
                $templateProcessor->cloneRow('row_doc_name', sizeof($item->persons_documents));
                $r2=1;
                foreach($item->persons_documents as $record) {
                    $templateProcessor->setValue('row_doc_name#' . $r2, $record->name);
                    $templateProcessor->setImg('row_doc_img#'. $r2, array('src'=> base_path('storage/app/').$record->filepath,'swh'=>'650'));
                    $r2++;
                }
            }catch (\Exception $e){  }
        }else{
            try{
                $templateProcessor->cloneRow('row_doc_name', 1);
                $templateProcessor->setValue('row_doc_name#' . 1, '-');
                $templateProcessor->setValue('row_doc_img#' . 1, '-');
            }catch (\Exception $e){  }
        }
            if (sizeof($item->family_member)!=0){
                try{
                    $templateProcessor->cloneRow('fm_nam', sizeof($item->family_member));
                    $z=1;
                    foreach($item->family_member as $record) {
                        $templateProcessor->setValue('fm_nam#' . $z, $record->name);
                        $templateProcessor->setValue('fm_bd#' . $z, $record->birthday);
                        $templateProcessor->setValue('fm_gen#' . $z, $record->gender);
                        $templateProcessor->setValue('fm_idc#' . $z, $record->id_card_number);
                        $templateProcessor->setValue('fm_ms#' . $z, $record->marital_status);
                        $templateProcessor->setValue('fm_kin#' . $z, $record->kinship_name);
                        $templateProcessor->setValue('fm_stg#' . $z, $record->stage);
                        $templateProcessor->setValue('fm_hs#' . $z, $record->health_status);
                        $z++;
                    }
                }catch (\Exception $e){  }
            }else{

                try{
                    $templateProcessor->cloneRow('fm_nam',1);
                    $templateProcessor->setValue('fm_nam#' . 1, '-');
                    $templateProcessor->setValue('fm_bd#' . 1, '-');
                    $templateProcessor->setValue('fm_gen#' . 1, '-');
                    $templateProcessor->setValue('fm_idc#' . 1,'-');
                    $templateProcessor->setValue('fm_ms#' . 1, '-');
                    $templateProcessor->setValue('fm_kin#' . 1, '-');
                    $templateProcessor->setValue('fm_stg#' . 1, '-');
                    $templateProcessor->setValue('fm_hs#' . 1, '-');
                }catch (\Exception $e){  }
            }

            $get=array('action' => 'show','id' => $item->father_id, 'person'  => true, 'persons_i18n'  =>  true, 'health'  =>  true,  'education'  =>  true, 'work'  =>  true);
            if($item->father_id != null){
                $get['id']=$item->father_id;
                $father =\Common\Model\Person::fetch($get);
                $templateProcessor->setValue('father_first_name', $father->first_name);
                $templateProcessor->setValue('father_second_name', $father->second_name);
                $templateProcessor->setValue('father_third_name', $father->third_name );
                $templateProcessor->setValue('father_last_name', $father->last_name );
                $templateProcessor->setValue('father_name', $father->full_name);
                $templateProcessor->setValue('father_spouses', $father->spouses);
                $templateProcessor->setValue('father_id_card_number', $father->id_card_number);
                $templateProcessor->setValue('father_birthday', $father->birthday);
                $templateProcessor->setValue('father_en_first_name', $father->en_first_name);
                $templateProcessor->setValue('father_en_second_name', $father->en_second_name);
                $templateProcessor->setValue('father_en_third_name', $father->en_third_name);
                $templateProcessor->setValue('father_en_last_name', $father->en_last_name);
                $templateProcessor->setValue('father_health_status', $father->health_status);
                $templateProcessor->setValue('father_health_details', $father->health_details);
                $templateProcessor->setValue('father_diseases_name', $father->diseases_name);
                $templateProcessor->setValue('father_specialization', $father->specialization);
                $templateProcessor->setValue('father_degree', $father->degree);
                $templateProcessor->setValue('father_alive', $father->alive);
                $templateProcessor->setValue('father_is_alive', $father->is_alive);
                $templateProcessor->setValue('father_death_date', $father->death_date);
                $templateProcessor->setValue('father_death_cause_name', $father->death_cause_name);
                $templateProcessor->setValue('father_working', $father->working);
                $templateProcessor->setValue('father_work_job', $father->work_job);
                $templateProcessor->setValue('father_work_location', $father->work_location);
                $templateProcessor->setValue('father_monthly_income', $father->monthly_income);
            }
            else{
                $templateProcessor->setValue('father_first_name', '-');
                $templateProcessor->setValue('father_second_name', '-');
                $templateProcessor->setValue('father_third_name', '-');
                $templateProcessor->setValue('father_last_name', '-');
                $templateProcessor->setValue('father_name', '-');
                $templateProcessor->setValue('father_spouses', '-');
                $templateProcessor->setValue('father_id_card_number','-');
                $templateProcessor->setValue('father_birthday', '-');
                $templateProcessor->setValue('father_en_first_name','-');
                $templateProcessor->setValue('father_en_second_name', '-');
                $templateProcessor->setValue('father_en_third_name', '-');
                $templateProcessor->setValue('father_en_last_name', '-');
                $templateProcessor->setValue('father_health_status', '-');
                $templateProcessor->setValue('father_health_details','-');
                $templateProcessor->setValue('father_diseases_name', '-');
                $templateProcessor->setValue('father_specialization', '-');
                $templateProcessor->setValue('father_degree', '-');
                $templateProcessor->setValue('father_alive', '-');
                $templateProcessor->setValue('father_is_alive', '-');
                $templateProcessor->setValue('father_death_date', '-');
                $templateProcessor->setValue('father_death_cause_name', '-');
                $templateProcessor->setValue('father_working', '-');
                $templateProcessor->setValue('father_work_job', '-');
                $templateProcessor->setValue('father_work_location', '-');
                $templateProcessor->setValue('father_monthly_income', '-');
            }
            if($item->mother_id != null){
                $get['id']=$item->mother_id;
                $mother =\Common\Model\Person::fetch($get);

                $templateProcessor->setValue('mother_prev_family_name', $mother->prev_family_name);
                $templateProcessor->setValue('mother_name', $mother->name);
                $templateProcessor->setValue('mother_first_name', $mother->first_name);
                $templateProcessor->setValue('mother_second_name', $mother->second_name);
                $templateProcessor->setValue('mother_third_name', $mother->third_name);
                $templateProcessor->setValue('mother_last_name', $mother->last_name);
                $templateProcessor->setValue('mother_name', $mother->full_name);
                $templateProcessor->setValue('mother_id_card_number', $mother->id_card_number);
                $templateProcessor->setValue('mother_birthday', $mother->birthday);
                $templateProcessor->setValue('mother_marital_status_name', $mother->marital_status_name);
                $templateProcessor->setValue('mother_nationality', $mother->nationality);
                $templateProcessor->setValue('mother_en_first_name', $mother->en_first_name);
                $templateProcessor->setValue('mother_en_second_name', $mother->en_second_name);
                $templateProcessor->setValue('mother_en_third_name', $mother->en_third_name);
                $templateProcessor->setValue('mother_en_last_name', $mother->en_last_name);
                $templateProcessor->setValue('mother_health_status', $mother->health_status);
                $templateProcessor->setValue('mother_health_details', $mother->health_details);
                $templateProcessor->setValue('mother_diseases_name', $mother->diseases_name);
                $templateProcessor->setValue('mother_specialization', $mother->specialization);
                $templateProcessor->setValue('mother_stage', $mother->stage);
                $templateProcessor->setValue('mother_alive', $mother->alive);
                $templateProcessor->setValue('mother_is_alive', $mother->is_alive);
                $templateProcessor->setValue('mother_death_date', $mother->death_date);
                $templateProcessor->setValue('mother_death_cause_name', $mother->death_cause_name);
                $templateProcessor->setValue('mother_working', $mother->working);
                $templateProcessor->setValue('mother_work_job', $mother->work_job);
                $templateProcessor->setValue('mother_work_location', $mother->work_location);
                $templateProcessor->setValue('mother_monthly_income', $mother->monthly_income);
            }
            else{
                $templateProcessor->setValue('mother_prev_family_name','-');
                $templateProcessor->setValue('mother_first_name', '-');
                $templateProcessor->setValue('mother_second_name', '-');
                $templateProcessor->setValue('mother_third_name', '-');
                $templateProcessor->setValue('mother_last_name','-');
                $templateProcessor->setValue('mother_name', '-');
                $templateProcessor->setValue('mother_id_card_number', '-');
                $templateProcessor->setValue('mother_birthday', '-');
                $templateProcessor->setValue('mother_marital_status_name', '-');
                $templateProcessor->setValue('mother_nationality', '-');
                $templateProcessor->setValue('mother_en_first_name', '-');
                $templateProcessor->setValue('mother_en_second_name', '-');
                $templateProcessor->setValue('mother_en_third_name', '-');
                $templateProcessor->setValue('mother_en_last_name', '-');
                $templateProcessor->setValue('mother_health_status', '-');
                $templateProcessor->setValue('mother_health_details','-');
                $templateProcessor->setValue('mother_diseases_name', '-');
                $templateProcessor->setValue('mother_specialization','-');
                $templateProcessor->setValue('mother_stage', '-');
                $templateProcessor->setValue('mother_alive', '-');
                $templateProcessor->setValue('mother_is_alive', '-');
                $templateProcessor->setValue('mother_death_date', '-');
                $templateProcessor->setValue('mother_death_cause_name', '-');
                $templateProcessor->setValue('mother_working', '-');
                $templateProcessor->setValue('mother_work_job', '-');
                $templateProcessor->setValue('mother_work_location', '-');
                $templateProcessor->setValue('mother_monthly_income', '-');
            }
            if($item->guardian_id != null){
                $guardian =\Common\Model\Person::fetch(array('action'=>'show', 'id' => $item->guardian_id,'person' =>true, 'persons_i18n' => true, 'contacts' => true,  'education' => true,
                    'work' => true, 'residence' => true, 'banks' => true));

                $templateProcessor->setValue('guardian_first_name', $guardian->first_name);
                $templateProcessor->setValue('guardian_second_name', $guardian->second_name);
                $templateProcessor->setValue('guardian_third_name', $guardian->third_name );
                $templateProcessor->setValue('guardian_last_name', $guardian->last_name );
                $templateProcessor->setValue('guardian_name', $guardian->full_name);
                $templateProcessor->setValue('guardian_id_card_number', $guardian->id_card_number);
                $templateProcessor->setValue('guardian_birthday', $guardian->birthday);
                $templateProcessor->setValue('guardian_birth_place', $guardian->birth_place);
                $templateProcessor->setValue('guardian_nationality', $guardian->nationality);
                $templateProcessor->setValue('guardian_country', $guardian->country);
                $templateProcessor->setValue('guardian_governarate', $guardian->governarate);
                $templateProcessor->setValue('guardian_city', $guardian->city);
                $templateProcessor->setValue('guardian_location_id', $guardian->location_id);
                $templateProcessor->setValue('guardian_street_address', $guardian->street_address);
                $templateProcessor->setValue('guardian_address', $guardian->address);
                $templateProcessor->setValue('guardian_phone', $guardian->phone);
                $templateProcessor->setValue('guardian_primary_mobile', $guardian->primary_mobile);
                $templateProcessor->setValue('guardian_wataniya', $guardian->wataniya);
                $templateProcessor->setValue('guardian_secondery_mobile', $guardian->secondery_mobile );
                $templateProcessor->setValue('guardian_en_first_name', $guardian->en_first_name);
                $templateProcessor->setValue('guardian_en_second_name', $guardian->en_second_name);
                $templateProcessor->setValue('guardian_en_third_name', $guardian->en_third_name);
                $templateProcessor->setValue('guardian_en_last_name', $guardian->en_last_name);
                $templateProcessor->setValue('guardian_property_types', $guardian->property_types);
                $templateProcessor->setValue('guardian_roof_materials', $guardian->roof_materials);
                $templateProcessor->setValue('guardian_area', $guardian->area);
                $templateProcessor->setValue('guardian_rooms', $guardian->rooms);
                $templateProcessor->setValue('guardian_residence_condition', $guardian->residence_condition);
                $templateProcessor->setValue('guardian_indoor_condition', $guardian->indoor_condition);
                $templateProcessor->setValue('guardian_stage', $guardian->stage);
//                $templateProcessor->setValue('guardian_bank_name', $guardian->bank_name);
//                $templateProcessor->setValue('guardian_branch_name', $guardian->branch_name);
//                $templateProcessor->setValue('guardian_account_number', $guardian->account_number);
//                $templateProcessor->setValue('guardian_account_owner', $guardian->account_owner);
                $templateProcessor->setValue('guardian_working', $guardian->working);
                $templateProcessor->setValue('guardian_work_job', $guardian->work_job);
                $templateProcessor->setValue('guardian_work_location', $guardian->work_location);
                $templateProcessor->setValue('guardian_monthly_income', $guardian->monthly_income);
            }
            else{

                $templateProcessor->setValue('guardian_first_name','-');
                $templateProcessor->setValue('guardian_second_name', '-');
                $templateProcessor->setValue('guardian_third_name', '-');
                $templateProcessor->setValue('guardian_last_name', '-');
                $templateProcessor->setValue('guardian_name', '-');
                $templateProcessor->setValue('guardian_id_card_number', '-');
                $templateProcessor->setValue('guardian_birthday', '-');
                $templateProcessor->setValue('guardian_birth_place', '-');
                $templateProcessor->setValue('guardian_nationality', '-');
                $templateProcessor->setValue('guardian_country', '-');
                $templateProcessor->setValue('guardian_governarate', '-');
                $templateProcessor->setValue('guardian_city', '-');
                $templateProcessor->setValue('guardian_location_id', '-');
                $templateProcessor->setValue('guardian_street_address', '-');
                $templateProcessor->setValue('guardian_address', '-');
                $templateProcessor->setValue('guardian_phone', '-');
                $templateProcessor->setValue('guardian_primary_mobile', '-');
                $templateProcessor->setValue('guardian_secondery_mobile', '-');
                $templateProcessor->setValue('guardian_en_first_name', '-');
                $templateProcessor->setValue('guardian_en_second_name', '-');
                $templateProcessor->setValue('guardian_en_third_name', '-');
                $templateProcessor->setValue('guardian_en_last_name', '-');
                $templateProcessor->setValue('guardian_property_types', '-');
                $templateProcessor->setValue('guardian_roof_materials', '-');
                $templateProcessor->setValue('guardian_area', '-');
                $templateProcessor->setValue('guardian_rooms', '-');
                $templateProcessor->setValue('guardian_residence_condition', '-');
                $templateProcessor->setValue('guardian_indoor_condition', '-');
                $templateProcessor->setValue('guardian_stage','-');
                $templateProcessor->setValue('guardian_working', '-');
                $templateProcessor->setValue('guardian_work_job', '-');
                $templateProcessor->setValue('guardian_work_location', '-');
                $templateProcessor->setValue('guardian_monthly_income', '-');
            }

        $dirName = base_path('storage/app/templates/SponsorTemp');

        if (!is_dir($dirName)) {
            @mkdir($dirName, 0777, true);
        }

        $templateProcessor->saveAs($dirName.'/'.$sponsor->name.'-'.$item->organization_name.'-'.$item->category_name.'-'.$item->full_name.'.docx');

        $token = md5(uniqid());
        $zipFileName = storage_path('tmp/export_' . $token . '.zip');

        $zip = new \ZipArchive;
        if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
        {
            foreach ( glob( $dirName . '/*' ) as $fileName )
            {
                $file = basename( $fileName );
                $zip->addFile( $fileName, $file );
            }

            $zip->close();
            array_map('unlink', glob("$dirName/*.*"));
            rmdir($dirName);
        }

        \Log\Model\Log::saveNewLog('CASE_EXPORTED',trans('sponsorship::application.Exported aid case forms'));

        return response()->json([
            'download_token' => $token,
        ]);
    }

        // get Sponsorship Cases according filters ( ExportToWord , paginate , xlsx ) to logged organization_id
        public function getCasesStatistics(Request $request)
        {
            $user = \Auth::user();

            ini_set('max_execution_time', 0);
            set_time_limit(0);
            Ini_set('memory_limit','2048M');//            $this->authorize('manage',SponsorshipCases::class);

            $Org=Organization::findorfail($user->organization_id);
            $master=true;
            if($Org->parent_id){
                $master=false;
            }

            $Cases=SponsorshipCases::getCasesStatistics($request->page,$user->organization_id,$request->get('sponsor_id'),$request->all());
            if($request->get('action') =='filters'){
                return response()->json(
                    ['master'=>$master,
                        'cases'  =>$Cases ] );

            }
            else if($request->get('action') =='export'){
    //                $this->authorize('export',SponsorshipCases::class);
                if(sizeof($Cases) !=0){
                    foreach($Cases as $key =>$value){
                        $dat[$key][trans('sponsorship::application.#')]=$key+1;
                        foreach($value as $k =>$v){
                            if($k != 'spon_date') {

                                if (substr($k, 0, 7) === "mother_") {
                                    $dat[$key][trans('sponsorship::application.' . substr($k, 7)) . " ( " . trans('sponsorship::application.mother') . " ) "] = $v;
                                } elseif (substr($k, 0, 7) === "father_") {
                                    $dat[$key][trans('sponsorship::application.' . substr($k, 7)) . " ( " . trans('sponsorship::application.father') . " ) "] = $v;
                                } elseif (substr($k, 0, 9) === "guardian_") {
                                    $dat[$key][trans('sponsorship::application.' . substr($k, 9)) . " ( " . trans('sponsorship::application.guardian') . " ) "] = $v;
                                } else {
                                    $dat[$key][trans('sponsorship::application.' . $k)] = $v;
                                }

                            }
                            //                        if($k != 'id' && $k != 'sponsor_id'&& $k != 'flag'&& $k != 'date_flag'){
                            //                           if($k =='sponsor_number'){
                            //                               $dat[$key][trans('sponsorship::application.sponsorship_number')]= $v;
                            //                           }else{
                            //                               $dat[$key][trans('sponsorship::application.' . $k)]= $v;
                            //                           }
                            //                        }
                        }
                    }

                    $data=$dat;
                    $token = md5(uniqid());
                    \Excel::create('export_' . $token,function($excel) use($data) {
                        $excel->sheet('كشف الحالات', function($sheet) use($data) {
                            $sheet->setRightToLeft(true);
                            $sheet->setAllBorders('thin');
                            $sheet->setfitToWidth(true);
                            $sheet->setHeight(1,40);
                            $sheet->getDefaultStyle()->applyFromArray([
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                            ]);
                            $sheet->getStyle("A1:BI1")->applyFromArray(['font' => ['bold' => true]]);
                            $sheet->fromArray($data);
                        });
                    })
                        ->store('xlsx', storage_path('tmp/'));
                    return response()->json([
                        'download_token' => $token,
                    ]);

                }else{
                    return 0;
                }
            }
            else if($request->get('action') =='ExportToWord'){

                $lang = 'ar';
                if($request->lang){
                    $lang = $request->lang;
                }


                $dirName = base_path('storage/app/templates/statistics');
                if (!is_dir($dirName)) {
                    @mkdir($dirName, 0777, true);
                }
                $template_target=$request->get('template_target');

                foreach($Cases as $item){

                    $Template=\Common\Model\Template::where(['template'=>$template_target,'category_id'=>$item->category_id,'organization_id'=>$item->sponsor_id])->first();
                    if($Template){
                        if($lang== 'en'){
                            $path = base_path('storage/app/'.$Template->en_filename);
                        }else{
                            $path = base_path('storage/app/'.$Template->filename);
                        }

                    }else{
                        $default_template=Setting::where(['id'=>'Sponsorship-default-template','organization_id'=>$item->organization_id])->first();
                        if($default_template){
                            $path = base_path('storage/app/'.$default_template->value);
                        }else{
                            $path = base_path('storage/app/templates/Sponsorship-default-template.docx');
                        }
                    }

                    $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);
                    $templateProcessor->setValue('date', date('y-m-d'));
                    $templateProcessor->setValue('category_name', $item->category_name);
                    $templateProcessor->setValue('first_name', $item->first_name);
                    $templateProcessor->setValue('second_name', $item->second_name);
                    $templateProcessor->setValue('third_name', $item->third_name );
                    $templateProcessor->setValue('last_name', $item->last_name );
                    $templateProcessor->setValue('name', $item->full_name);
                    $templateProcessor->setValue('en_first_name', $item->en_first_name);
                    $templateProcessor->setValue('en_second_name', $item->en_second_name);
                    $templateProcessor->setValue('en_third_name', $item->en_third_name);
                    $templateProcessor->setValue('en_last_name', $item->en_last_name);
                    $templateProcessor->setValue('en_name', $item->en_name);
                    $templateProcessor->setValue('id_card_number', $item->id_card_number);
                    $templateProcessor->setValue('birthday', $item->birthday);
                    $templateProcessor->setValue('gender', $item->gender);
                    $templateProcessor->setValue('birth_place', $item->birth_place);
                    $templateProcessor->setValue('nationality', $item->nationality);
                    $templateProcessor->setValue('refugee', $item->refugee);
                    $templateProcessor->setValue('unrwa_card_number', $item->unrwa_card_number);
                    $templateProcessor->setValue('marital_status_name', $item->marital_status_name);
                    $templateProcessor->setValue('monthly_income', $item->monthly_income);
                    $templateProcessor->setValue('address', $item->address);
                    $templateProcessor->setValue('country', $item->country);
                    $templateProcessor->setValue('governarate', $item->governarate);
                    $templateProcessor->setValue('city', $item->city);
                    $templateProcessor->setValue('location_id', $item->nearLocation);
                    $templateProcessor->setValue('street_address', $item->street_address);
                    $templateProcessor->setValue('individual_count', $item->individual_count);
                    $templateProcessor->setValue('family_count', $item->family_count);
                    $templateProcessor->setValue('male_count', $item->male_count);
                    $templateProcessor->setValue('female_count', $item->female_count);
                    $templateProcessor->setValue('phone', $item->phone);
                    $templateProcessor->setValue('primary_mobile', $item->primary_mobile);
                    $templateProcessor->setValue('wataniya', $item->wataniya);
                    $templateProcessor->setValue('secondary_mobile', $item->secondary_mobile );
                    $templateProcessor->setValue('property_types', $item->property_types);
                    $templateProcessor->setValue('rent_value', $item->rent_value);
                    $templateProcessor->setValue('roof_materials', $item->roof_materials);
                    $templateProcessor->setValue('habitable', $item->habitable);
                    $templateProcessor->setValue('house_condition', $item->house_condition);
                    $templateProcessor->setValue('residence_condition', $item->residence_condition);
                    $templateProcessor->setValue('indoor_condition', $item->indoor_condition);
                    $templateProcessor->setValue('rooms', $item->rooms);
                    $templateProcessor->setValue('area', $item->area);
                    $templateProcessor->setValue('working', $item->working);
                    $templateProcessor->setValue('works', $item->works);
                    $templateProcessor->setValue('can_work', $item->can_work);
                    $templateProcessor->setValue('can_works', $item->can_works);
                    $templateProcessor->setValue('work_job', $item->work_job);
                    $templateProcessor->setValue('work_reason', $item->work_reason);
                    $templateProcessor->setValue('work_status', $item->work_status);
                    $templateProcessor->setValue('work_wage', $item->work_wage);
                    $templateProcessor->setValue('work_location', $item->work_location);
                    $templateProcessor->setValue('needs', $item->needs);
                    $templateProcessor->setValue('promised', $item->promised);
                    $templateProcessor->setValue('reconstructions_organization_name', $item->reconstructions_organization_name);
                    $templateProcessor->setValue('visitor', $item->visitor);
                    $templateProcessor->setValue('notes', $item->notes);
                    $templateProcessor->setValue('visited_at', $item->visited_at);
                    $templateProcessor->setValue('health_status', $item->health_status);
                    $templateProcessor->setValue('health_details', $item->health_details);
                    $templateProcessor->setValue('diseases_name', $item->diseases_name);
                    $templateProcessor->setValue('study', $item->study);
                    $templateProcessor->setValue('type', $item->type);
                    $templateProcessor->setValue('level', $item->level);
                    $templateProcessor->setValue('year', $item->year);
                    $templateProcessor->setValue('points', $item->points);
                    $templateProcessor->setValue('school', $item->school);
                    $templateProcessor->setValue('grade', $item->grade);
                    $templateProcessor->setValue('authority', $item->authority);
                    $templateProcessor->setValue('stage', $item->stage);
                    $templateProcessor->setValue('degree', $item->degree);
                    $templateProcessor->setValue('prayer', $item->prayer);
                    $templateProcessor->setValue('prayer_reason', $item->prayer_reason);
                    $templateProcessor->setValue('quran_reason', $item->quran_reason);
                    $templateProcessor->setValue('quran_parts', $item->quran_parts);
                    $templateProcessor->setValue('quran_chapters', $item->quran_chapters);
                    $templateProcessor->setValue('save_quran', $item->save_quran);
                    $templateProcessor->setValue('visitor_note', $item->visitor_note);
                    $templateProcessor->setValue('sponsor_name', $item->sponsor_name);
    //      $templateProcessor->setValue('guardian_kinship', $item->guardian_kinship);

                    if (sizeof($item->banks)!=0){
                        try{
                            $templateProcessor->cloneRow('r_bank_name', sizeof($item->banks));
                            $z=1;
                            foreach($item->banks as $rec) {
                                $templateProcessor->setValue('r_bank_name#' . $z, $rec->bank_name);
                                $templateProcessor->setValue('r_branch_name#' . $z, $rec->branch);
                                $templateProcessor->setValue('r_account_owner#' . $z, $rec->account_owner);
                                $templateProcessor->setValue('r_account_number#' . $z, $rec->account_number);
                                if($rec->check == true){
                                    $templateProcessor->setValue('r_default#' . $z, trans('aid::application.yes') );
                                }else{
                                    $templateProcessor->setValue('r_default#' . $z, trans('aid::application.no') );
                                }
                                $z++;
                            }
                        }catch (\Exception $e){}
                    }else{
                        try{
                            $templateProcessor->cloneRow('r_bank_name', 1);
                            $templateProcessor->setValue('r_bank_name#' . 1, '-');
                            $templateProcessor->setValue('r_branch_name#' . 1, '-');
                            $templateProcessor->setValue('r_account_owner#' . 1, '-');
                            $templateProcessor->setValue('r_account_number#' . 1,'-');
                            $templateProcessor->setValue('r_default#' . 1, '-');
                        }catch (\Exception $e){  }
                    }
                    if (sizeof($item->home_indoor)!=0){
                        try{
                            $templateProcessor->cloneRow('row_es_name', sizeof($item->home_indoor));
                            $z=1;
                            foreach($item->home_indoor as $record) {
                                $templateProcessor->setValue('row_es_name#' . $z, $record->name);
                                $templateProcessor->setValue('row_es_if_exist#' . $z, $record->exist);
                                $templateProcessor->setValue('row_es_needs#' . $z, $record->needs);
                                $templateProcessor->setValue('row_es_condition#' . $z, $record->essentials_condition);
                                $z++;
                            }
                        }catch (\Exception $e){  }
                    }else{
                        try{
                            $templateProcessor->cloneRow('row_es_name', 1);
                            $templateProcessor->setValue('row_es_name#' . 1, '-');
                            $templateProcessor->setValue('row_es_if_exist#' . 1, '-');
                            $templateProcessor->setValue('row_es_needs#' . 1, '-');
                            $templateProcessor->setValue('row_es_condition#' . 1, '-');

                        }catch (\Exception $e){  }
                    }
                    if (sizeof($item->persons_properties)!=0){
                        try{
                            $templateProcessor->cloneRow('row_pro_name', sizeof($item->persons_properties));
                            $z2=1;
                            foreach($item->persons_properties as $sub_record) {
                                $templateProcessor->setValue('row_pro_name#' . $z2, $sub_record->name);
                                $templateProcessor->setValue('row_pro_if_exist#' . $z2, $sub_record->has_property);
                                $templateProcessor->setValue('row_pro_count#' . $z2, $sub_record->quantity);
                                $z2++;
                            }
                        }catch (\Exception $e){  }
                    }else{
                        try{
                            $templateProcessor->cloneRow('row_pro_name', 1);
                            $templateProcessor->setValue('row_pro_name#' . 1, '-');
                            $templateProcessor->setValue('row_pro_if_exist#' . 1, '-');
                            $templateProcessor->setValue('row_pro_count#' . 1, '-');
                        }catch (\Exception $e){  }
                    }
                    if (sizeof($item->financial_aid_source)!=0){
                        try{
                            $templateProcessor->cloneRow('row_fin_name', sizeof($item->financial_aid_source));
                            $z3=1;
                            foreach($item->financial_aid_source as $record) {
                                $templateProcessor->setValue('row_fin_name#' . $z3, $record->name);
                                $templateProcessor->setValue('row_fin_if_exist#' . $z3, $record->aid_take);
                                $templateProcessor->setValue('row_fin_count#' . $z3, $record->aid_value);
                                $templateProcessor->setValue('row_fin_currency#' . $z3, $record->currency_name);
                                $z3++;
                            }
                        }catch (\Exception $e){  }
                    }else{
                        try{
                            $templateProcessor->cloneRow('row_fin_name', 1);
                            $templateProcessor->setValue('row_fin_name#' . 1, '-');
                            $templateProcessor->setValue('row_fin_if_exist#' . 1, '-');
                            $templateProcessor->setValue('row_fin_count#' . 1, '-');
                            $templateProcessor->setValue('row_fin_currency#' . 1, '-');
                        }catch (\Exception $e){  }
                    }
                    if (sizeof($item->non_financial_aid_source)!=0){
                        try{
                            $templateProcessor->cloneRow('row_nonfin_name', sizeof($item->non_financial_aid_source));
                            $z3=1;
                            foreach($item->non_financial_aid_source as $record) {
                                $templateProcessor->setValue('row_nonfin_name#' . $z3, $record->name);
                                $templateProcessor->setValue('row_nonfin_if_exist#' . $z3, $record->aid_take);
                                $templateProcessor->setValue('row_nonfin_count#' . $z3, $record->aid_value);
                                $z3++;
                            }
                        }catch (\Exception $e){  }
                    } else{
                        try{
                            $templateProcessor->cloneRow('row_nonfin_name', 1);
                            $templateProcessor->setValue('row_nonfin_name#' . 1, '-');
                            $templateProcessor->setValue('row_nonfin_if_exist#' . 1, '-');
                            $templateProcessor->setValue('row_nonfin_count#' . 1, '-');
                        }catch (\Exception $e){  }
                    }
                    if (sizeof($item->persons_documents)!=0){
                        try{
                            $templateProcessor->cloneRow('row_doc_name', sizeof($item->persons_documents));
                            $r2=1;
                            foreach($item->persons_documents as $record) {
                                $templateProcessor->setValue('row_doc_name#' . $r2, $record->name);
                                $templateProcessor->setImg('row_doc_img#'. $r2, array('src'=> base_path('storage/app/').$record->filepath,'swh'=>'650'));
                                $r2++;
                            }
                        }catch (\Exception $e){  }
                    } else{
                        try{
                            $templateProcessor->cloneRow('row_doc_name', 1);
                            $templateProcessor->setValue('row_doc_name#' . 1, '-');
                            $templateProcessor->setValue('row_doc_img#' . 1, '-');
                        }catch (\Exception $e){  }
                    }
                    if (sizeof($item->family_member)!=0){
                        try{
                            $templateProcessor->cloneRow('fm_nam', sizeof($item->family_member));
                            $z=1;
                            foreach($item->family_member as $record) {
                                $templateProcessor->setValue('fm_nam#' . $z, $record->name);
                                $templateProcessor->setValue('fm_bd#' . $z, $record->birthday);
                                $templateProcessor->setValue('fm_gen#' . $z, $record->gender);
                                $templateProcessor->setValue('fm_idc#' . $z, $record->id_card_number);
                                $templateProcessor->setValue('fm_ms#' . $z, $record->marital_status);
                                $templateProcessor->setValue('fm_kin#' . $z, $record->kinship_name);
                                $templateProcessor->setValue('fm_stg#' . $z, $record->stage);
                                $templateProcessor->setValue('fm_hs#' . $z, $record->health_status);
                                $z++;
                            }
                        }catch (\Exception $e){  }
                    }else{

                        try{
                            $templateProcessor->cloneRow('fm_nam',1);
                            $templateProcessor->setValue('fm_nam#' . 1, '-');
                            $templateProcessor->setValue('fm_bd#' . 1, '-');
                            $templateProcessor->setValue('fm_gen#' . 1, '-');
                            $templateProcessor->setValue('fm_idc#' . 1,'-');
                            $templateProcessor->setValue('fm_ms#' . 1, '-');
                            $templateProcessor->setValue('fm_kin#' . 1, '-');
                            $templateProcessor->setValue('fm_stg#' . 1, '-');
                            $templateProcessor->setValue('fm_hs#' . 1, '-');
                        }catch (\Exception $e){  }
                    }
                    if($item->father_id != null){
                        $templateProcessor->setValue('father_first_name', $item->father->first_name);
                        $templateProcessor->setValue('father_second_name', $item->father->second_name);
                        $templateProcessor->setValue('father_third_name', $item->father->third_name );
                        $templateProcessor->setValue('father_last_name', $item->father->last_name );
                        $templateProcessor->setValue('father_name', $item->father->full_name);
                        $templateProcessor->setValue('father_spouses', $item->father->spouses);
                        $templateProcessor->setValue('father_id_card_number', $item->father->id_card_number);
                        $templateProcessor->setValue('father_birthday', $item->father->birthday);
                        $templateProcessor->setValue('father_en_first_name', $item->father->en_first_name);
                        $templateProcessor->setValue('father_en_second_name', $item->father->en_second_name);
                        $templateProcessor->setValue('father_en_third_name', $item->father->en_third_name);
                        $templateProcessor->setValue('father_en_last_name', $item->father->en_last_name);
                        $templateProcessor->setValue('father_health_status', $item->father->health_status);
                        $templateProcessor->setValue('father_health_details', $item->father->health_details);
                        $templateProcessor->setValue('father_diseases_name', $item->father->diseases_name);
                        $templateProcessor->setValue('father_specialization', $item->father->specialization);
                        $templateProcessor->setValue('father_degree', $item->father->degree);
                        $templateProcessor->setValue('father_alive', $item->father->alive);
                        $templateProcessor->setValue('father_is_alive', $item->father->is_alive);
                        $templateProcessor->setValue('father_death_date', $item->father->death_date);
                        $templateProcessor->setValue('father_death_cause_name', $item->father->death_cause_name);
                        $templateProcessor->setValue('father_working', $item->father->working);
                        $templateProcessor->setValue('father_work_job', $item->father->work_job);
                        $templateProcessor->setValue('father_work_location', $item->father->work_location);
                        $templateProcessor->setValue('father_monthly_income', $item->father->monthly_income);
                    }
                    else{
                        $templateProcessor->setValue('father_first_name', '-');
                        $templateProcessor->setValue('father_second_name', '-');
                        $templateProcessor->setValue('father_third_name', '-');
                        $templateProcessor->setValue('father_last_name', '-');
                        $templateProcessor->setValue('father_name', '-');
                        $templateProcessor->setValue('father_spouses', '-');
                        $templateProcessor->setValue('father_id_card_number','-');
                        $templateProcessor->setValue('father_birthday', '-');
                        $templateProcessor->setValue('father_en_first_name','-');
                        $templateProcessor->setValue('father_en_second_name', '-');
                        $templateProcessor->setValue('father_en_third_name', '-');
                        $templateProcessor->setValue('father_en_last_name', '-');
                        $templateProcessor->setValue('father_health_status', '-');
                        $templateProcessor->setValue('father_health_details','-');
                        $templateProcessor->setValue('father_diseases_name', '-');
                        $templateProcessor->setValue('father_specialization', '-');
                        $templateProcessor->setValue('father_degree', '-');
                        $templateProcessor->setValue('father_alive', '-');
                        $templateProcessor->setValue('father_is_alive', '-');
                        $templateProcessor->setValue('father_death_date', '-');
                        $templateProcessor->setValue('father_death_cause_name', '-');
                        $templateProcessor->setValue('father_working', '-');
                        $templateProcessor->setValue('father_work_job', '-');
                        $templateProcessor->setValue('father_work_location', '-');
                        $templateProcessor->setValue('father_monthly_income', '-');
                    }
                    if($item->mother_id != null){
                        $templateProcessor->setValue('mother_prev_family_name', $item->mother->prev_family_name);
                        $templateProcessor->setValue('mother_first_name', $item->mother->first_name);
                        $templateProcessor->setValue('mother_second_name', $item->mother->second_name);
                        $templateProcessor->setValue('mother_third_name', $item->mother->third_name);
                        $templateProcessor->setValue('mother_last_name', $item->mother->last_name);
                        $templateProcessor->setValue('mother_name', $item->mother->full_name);
                        $templateProcessor->setValue('mother_id_card_number', $item->mother->id_card_number);
                        $templateProcessor->setValue('mother_birthday', $item->mother->birthday);
                        $templateProcessor->setValue('mother_marital_status_name', $item->mother->marital_status_name);
                        $templateProcessor->setValue('mother_nationality', $item->mother->nationality);
                        $templateProcessor->setValue('mother_en_first_name', $item->mother->en_first_name);
                        $templateProcessor->setValue('mother_en_second_name', $item->mother->en_second_name);
                        $templateProcessor->setValue('mother_en_third_name', $item->mother->en_third_name);
                        $templateProcessor->setValue('mother_en_last_name', $item->mother->en_last_name);
                        $templateProcessor->setValue('mother_health_status', $item->mother->health_status);
                        $templateProcessor->setValue('mother_health_details', $item->mother->health_details);
                        $templateProcessor->setValue('mother_diseases_name', $item->mother->diseases_name);
                        $templateProcessor->setValue('mother_specialization', $item->mother->specialization);
                        $templateProcessor->setValue('mother_stage', $item->mother->stage);
                        $templateProcessor->setValue('mother_alive', $item->mother->alive);
                        $templateProcessor->setValue('mother_is_alive', $item->mother->is_alive);
                        $templateProcessor->setValue('mother_death_date', $item->mother->death_date);
                        $templateProcessor->setValue('mother_death_cause_name', $item->mother->death_cause_name);
                        $templateProcessor->setValue('mother_working', $item->mother->working);
                        $templateProcessor->setValue('mother_work_job', $item->mother->work_job);
                        $templateProcessor->setValue('mother_work_location', $item->mother->work_location);
                        $templateProcessor->setValue('mother_monthly_income', $item->mother->monthly_income);
                    }
                    else{
                        $templateProcessor->setValue('mother_prev_family_name','-');
                        $templateProcessor->setValue('mother_first_name', '-');
                        $templateProcessor->setValue('mother_second_name', '-');
                        $templateProcessor->setValue('mother_third_name', '-');
                        $templateProcessor->setValue('mother_last_name','-');
                        $templateProcessor->setValue('mother_name', '-');
                        $templateProcessor->setValue('mother_id_card_number', '-');
                        $templateProcessor->setValue('mother_birthday', '-');
                        $templateProcessor->setValue('mother_marital_status_name', '-');
                        $templateProcessor->setValue('mother_nationality', '-');
                        $templateProcessor->setValue('mother_en_first_name', '-');
                        $templateProcessor->setValue('mother_en_second_name', '-');
                        $templateProcessor->setValue('mother_en_third_name', '-');
                        $templateProcessor->setValue('mother_en_last_name', '-');
                        $templateProcessor->setValue('mother_health_status', '-');
                        $templateProcessor->setValue('mother_health_details','-');
                        $templateProcessor->setValue('mother_diseases_name', '-');
                        $templateProcessor->setValue('mother_specialization','-');
                        $templateProcessor->setValue('mother_stage', '-');
                        $templateProcessor->setValue('mother_alive', '-');
                        $templateProcessor->setValue('mother_is_alive', '-');
                        $templateProcessor->setValue('mother_death_date', '-');
                        $templateProcessor->setValue('mother_death_cause_name', '-');
                        $templateProcessor->setValue('mother_working', '-');
                        $templateProcessor->setValue('mother_work_job', '-');
                        $templateProcessor->setValue('mother_work_location', '-');
                        $templateProcessor->setValue('mother_monthly_income', '-');
                    }
                    if($item->guardian_id != null){
                        $templateProcessor->setValue('guardian_first_name', $item->guardian->first_name);
                        $templateProcessor->setValue('guardian_second_name', $item->guardian->second_name);
                        $templateProcessor->setValue('guardian_third_name', $item->guardian->third_name );
                        $templateProcessor->setValue('guardian_last_name', $item->guardian->last_name );
                        $templateProcessor->setValue('guardian_name', $item->guardian->full_name);
                        $templateProcessor->setValue('guardian_id_card_number', $item->guardian->id_card_number);
                        $templateProcessor->setValue('guardian_birthday', $item->guardian->birthday);
                        $templateProcessor->setValue('guardian_birth_place', $item->guardian->birth_place);
                        $templateProcessor->setValue('guardian_nationality', $item->guardian->nationality);
                        $templateProcessor->setValue('guardian_country', $item->guardian->country);
                        $templateProcessor->setValue('guardian_governarate', $item->guardian->governarate);
                        $templateProcessor->setValue('guardian_city', $item->guardian->city);
                        $templateProcessor->setValue('guardian_location_id', $item->guardian->location_id);
                        $templateProcessor->setValue('guardian_street_address', $item->guardian->street_address);
                        $templateProcessor->setValue('guardian_address', $item->guardian->address);
                        $templateProcessor->setValue('guardian_phone', $item->guardian->phone);
                        $templateProcessor->setValue('guardian_primary_mobile', $item->guardian->primary_mobile);
                        $templateProcessor->setValue('guardian_wataniya', $item->guardian->wataniya);
                        $templateProcessor->setValue('guardian_secondery_mobile', $item->guardian->secondary_mobile );
                        $templateProcessor->setValue('guardian_en_first_name', $item->guardian->en_first_name);
                        $templateProcessor->setValue('guardian_en_second_name', $item->guardian->en_second_name);
                        $templateProcessor->setValue('guardian_en_third_name', $item->guardian->en_third_name);
                        $templateProcessor->setValue('guardian_en_last_name', $item->guardian->en_last_name);
                        $templateProcessor->setValue('guardian_property_types', $item->guardian->property_types);
                        $templateProcessor->setValue('guardian_roof_materials', $item->guardian->roof_materials);
                        $templateProcessor->setValue('guardian_area', $item->guardian->area);
                        $templateProcessor->setValue('guardian_rooms', $item->guardian->rooms);
                        $templateProcessor->setValue('guardian_residence_condition', $item->guardian->residence_condition);
                        $templateProcessor->setValue('guardian_indoor_condition', $item->guardian->indoor_condition);
                        $templateProcessor->setValue('guardian_stage', $item->guardian->stage);
                        $templateProcessor->setValue('guardian_working', $item->guardian->working);
                        $templateProcessor->setValue('guardian_work_job', $item->guardian->work_job);
                        $templateProcessor->setValue('guardian_work_location', $item->guardian->work_location);
                        $templateProcessor->setValue('guardian_monthly_income', $item->guardian->monthly_income);
                    }
                    else{

                        $templateProcessor->setValue('guardian_first_name','-');
                        $templateProcessor->setValue('guardian_second_name', '-');
                        $templateProcessor->setValue('guardian_third_name', '-');
                        $templateProcessor->setValue('guardian_last_name', '-');
                        $templateProcessor->setValue('guardian_name', '-');
                        $templateProcessor->setValue('guardian_id_card_number', '-');
                        $templateProcessor->setValue('guardian_birthday', '-');
                        $templateProcessor->setValue('guardian_birth_place', '-');
                        $templateProcessor->setValue('guardian_nationality', '-');
                        $templateProcessor->setValue('guardian_country', '-');
                        $templateProcessor->setValue('guardian_governarate', '-');
                        $templateProcessor->setValue('guardian_city', '-');
                        $templateProcessor->setValue('guardian_location_id', '-');
                        $templateProcessor->setValue('guardian_street_address', '-');
                        $templateProcessor->setValue('guardian_address', '-');
                        $templateProcessor->setValue('guardian_phone', '-');
                        $templateProcessor->setValue('guardian_primary_mobile', '-');
                        $templateProcessor->setValue('guardian_secondery_mobile', '-');
                        $templateProcessor->setValue('guardian_en_first_name', '-');
                        $templateProcessor->setValue('guardian_en_second_name', '-');
                        $templateProcessor->setValue('guardian_en_third_name', '-');
                        $templateProcessor->setValue('guardian_en_last_name', '-');
                        $templateProcessor->setValue('guardian_property_types', '-');
                        $templateProcessor->setValue('guardian_roof_materials', '-');
                        $templateProcessor->setValue('guardian_area', '-');
                        $templateProcessor->setValue('guardian_rooms', '-');
                        $templateProcessor->setValue('guardian_residence_condition', '-');
                        $templateProcessor->setValue('guardian_indoor_condition', '-');
                        $templateProcessor->setValue('guardian_stage','-');
                        $templateProcessor->setValue('guardian_working', '-');
                        $templateProcessor->setValue('guardian_work_job', '-');
                        $templateProcessor->setValue('guardian_work_location', '-');
                        $templateProcessor->setValue('guardian_monthly_income', '-');
                    }


                    $templateProcessor->saveAs($dirName.'/'.$item->sponsor_name.'-'.$item->category_name.'-'.$item->full_name.'.docx');

                }

                $token = md5(uniqid());
                $zipFileName = storage_path('tmp/export_' . $token . '.zip');

                $zip = new \ZipArchive;
                if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
                {
                    foreach ( glob( $dirName . '/*' ) as $fileName )
                    {
                        $file = basename( $fileName );
                        $zip->addFile( $fileName, $file );
                    }

                    $zip->close();
                    array_map('unlink', glob("$dirName/*.*"));
                    rmdir($dirName);
                }

                \Log\Model\Log::saveNewLog('CASE_EXPORTED',trans('sponsorship::application.Exported aid case forms'));

                return response()->json([
                    'download_token' => $token,
                ]);


                //            $zipFileName = 'temp-statistics.zip';
                //            $dirName1 = base_path('storage/app/templates');
                //
                //            $zip = new \ZipArchive;
                //
                //            if ( $zip->open( $dirName1 . '/'.$zipFileName, \ZipArchive::CREATE ) === true )
                //            {
                //                foreach ( glob( $dirName . '/*' ) as $fileName )
                //                {
                //                    $file = basename( $fileName );
                //                    $zip->addFile( $fileName, $file );
                //                }
                //
                //                $zip->close();
                //                array_map('unlink', glob("$dirName/*.*"));
                //                rmdir($dirName);
                //                return response()->download( $dirName1 . '/' . $zipFileName, $zipFileName )->deleteFileAfterSend(true);
                //            }
            }

        }
        public function sponsorshipCasesFilter(Request $request){
//            $this->authorize('manage',SponsorshipCases::class);
            $user = \Auth::user();
            $Org=Organization::findorfail($user->organization_id);
            $master=true;
            if($Org->parent_id){
                $master=false;
            }

            $Cases=SponsorshipCases::sponsorshipCasesFilter($master,$request->page,$user->organization_id,$request->get('sponsor_id'),$request->all());
            if($request->get('action') =='export'){
    //                $this->authorize('export',SponsorshipCases::class);
                if(sizeof($Cases) !=0){
                    foreach($Cases as $key =>$value){
                        $dat[$key][trans('sponsorship::application.#')]=$key+1;
                        foreach($value as $k =>$v){
                            if($k != 'spon_date') {

                                if (substr($k, 0, 7) === "mother_") {
                                    $dat[$key][trans('sponsorship::application.' . substr($k, 7)) . " ( " . trans('sponsorship::application.mother') . " ) "] = $v;
                                } elseif (substr($k, 0, 7) === "father_") {
                                    $dat[$key][trans('sponsorship::application.' . substr($k, 7)) . " ( " . trans('sponsorship::application.father') . " ) "] = $v;
                                } elseif (substr($k, 0, 9) === "guardian_") {
                                    $dat[$key][trans('sponsorship::application.' . substr($k, 9)) . " ( " . trans('sponsorship::application.guardian') . " ) "] = $v;
                                } else {
                                    $dat[$key][trans('sponsorship::application.' . $k)] = $v;
                                }

                            }
                            //                        if($k != 'id' && $k != 'sponsor_id'&& $k != 'flag'&& $k != 'date_flag'){
                            //                           if($k =='sponsor_number'){
                            //                               $dat[$key][trans('sponsorship::application.sponsorship_number')]= $v;
                            //                           }else{
                            //                               $dat[$key][trans('sponsorship::application.' . $k)]= $v;
                            //                           }
                            //                        }
                        }
                    }

                    $data=$dat;
                    $token = md5(uniqid());
                    \Excel::create('export_' . $token,function($excel) use($data) {
                        $excel->sheet(trans('sponsorship::application.cases'), function($sheet) use($data) {
                            $sheet->setRightToLeft(true);
                            $sheet->setAllBorders('thin');
                            $sheet->setfitToWidth(true);
                            $sheet->setHeight(1,40);
                            $sheet->getDefaultStyle()->applyFromArray([
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                            ]);
                            $sheet->getStyle("A1:BI1")->applyFromArray(['font' => ['bold' => true]]);
                            $sheet->fromArray($data);
                        });
                    })
                        ->store('xlsx', storage_path('tmp/'));
                    return response()->json([
                        'download_token' => $token,
                    ]);

                }else{
                    return 0;
                }
            }
            return response()->json(['data' =>$Cases,'organization_id' =>$user->organization_id,'master'=>$master]);

        }

        // get Sponsorship Cases status log of sponsorship_case_id
        public function  getCasesStatusLog($id)
        {
            $this->authorize('view',SponsorshipCases::class);
            return response()->json(['items'  => SponsorshipCasesStatusLog::getLogs($id)] );
        }

        // save sponsorship_cases to sponsorship of some sponsor
        public function store(Request $request)
        {
            $this->authorize('create',SponsorshipsPerson::class);
            $this->authorize('create',Sponsorships::class);

            $response= array();
            $ids=[];
            $sponsor_blocked_count=0;
            $sponsor_blocked_names='';

            $user = \Auth::user();
            $user_id= $user->id;
            $organization_id= $user->organization_id;

            $sponsorships=$request->sponsorhips;

            if($sponsorships[0]== -100){
                $if_exist=false;
            }else{
                $if_exist=true;
            }

            if(isset($request->category_id)){
                $category_id=$request->category_id;
            }

            if($if_exist){
                foreach ($sponsorships as $item) {
                    $target=Sponsorships::findorFail($item);
                    $if_blocked=new BlockIDCard();
                    $blocked=$if_blocked->getSponsorStatus($target->sponsor_id,$target->category_id);
                    if($blocked['status']=='false'){
                        $sponsor_blocked_count ++;
                        $sponsorName = Organization::find($target->sponsor_id);

                        if($sponsor_blocked_count > 0){
                            $sponsor_blocked_names .= '،';
                        }
                        $sponsor_blocked_names .= $sponsorName->name ;
                    }else{
                        array_push($ids,['id'=>$item ,'category_id'=>$target->category_id,'sponsor_id'=>$target->sponsor_id]);
                    }
                }
                if($sponsor_blocked_count!=0 && $sponsor_blocked_count ==sizeof($sponsorships)){
                    $response["status"]= 'blocked';
                    $response["msg"]= trans('aid::application.The addition of the selected sponsorship to the target sponsor was blocked Please unblock to complete');
                    return response()->json($response);
                }
            }
            else{
                $r=$request->sponsor_id;

               for( $i= 0;$i< sizeof($r);$i++) {

                    $sponsor_blocked=null;

                    $if_blocked=new BlockIDCard();
                    $blocked=$if_blocked->getSponsorStatus($r[$i],$category_id);
                    if($blocked['status']=='false'){
                        $sponsor_blocked_count ++;
                        $sponsorName = Organization::find($r[$i]);

                        if($sponsor_blocked_count > 0){
                            $sponsor_blocked_names .= '،';
                        }
                        $sponsor_blocked_names .= $sponsorName->name;
                    }
                    else{
                        $new=  Sponsorships::create(['sponsor_id'=>$r[$i], 'category_id'=>$category_id, 'organization_id'=>$organization_id, 'user_id'=>$user_id,'status'=>1]);
                        array_push($ids, ['id'=>$new->id ,'category_id'=>$category_id,'sponsor_id'=>$r[$i]]);
                    }

                }
                if($sponsor_blocked_count!=0 && $sponsor_blocked_count ==sizeof($r)){
                    $response["status"]= 'blocked';
                    $response["msg"]= trans('aid::application.The addition of the selected sponsorship to the target sponsor was blocked Please unblock to complete');
                    return response()->json($response);
                }
            }


            $blocked = 0;
            $duplicated = 0;
            $success = 0;
            $policy_restricted = 0;
            $nominated = 0;
            $not_same = 0;
            $passed=[];
            $passed_case=[];
            $manipulate=[];
            $manipulate_case=[];
            $sponsorship_cases=[];
            $cases=[];
            $persons=[];
            $result=[];

            foreach ($ids as $k => $v) {
                $sponsorship= Sponsorships::where('id' ,$v['id'])->first();
                if(sizeof($request->sponsorship_cases) !=0 ){
                    foreach ($request->sponsorship_cases as $item) {
                        if ($item != null) {
                            if(!in_array($item['id'],$manipulate) || !in_array($item['id'],$manipulate_case)) {
                                if(!in_array($item['id'],$passed)|| !in_array($item['id'],$passed_case)){
                                    $manipulate[] = $item['id'];
                                    $manipulate_case[] = $item['case_id'];
                                    //                      $item['category_id']=$v['category_id'];

                                    if($item['category_id'] != $v['category_id']){
                                        $not_same++;
                                        $result[]=['name' => $item['name'],'id_card_number' => $item['id_card_number'],'reason'=>'not_same'];
                                    }else{
                                        $item['sponsor_id']=$sponsorship->sponsor_id;
                                        $isPass=SponsorshipsPerson::isPaassed($item['case_id'],$v['id'],$item['sponsor_id'],$user->organization_id);
                                        // $passed[]=$isPass;

                                        if ($isPass['status'] == true) {
                                            $passed[]=$item['id'];
                                            $passed_case[]=$item['case_id'];

                                            $s_data = [
                                                'person_id' => $item['id'],
                                                'sponsorship_id' => $v['id'],
                                            ];
                                            array_push($sponsorship_cases, $s_data);

                                            $case = [
                                                'case_id' => $item['case_id'],
                                                'sponsor_id' => $v['sponsor_id'],
                                                'sponsorship_id' => $v['id'],
                                                'status' => 1,
                                                'last_status_date' => date("Y-m-d")

                                            ];
                                            array_push($cases, $case);

                                            $person = [
                                                'person_id' => $item['id'],
                                                'sponsor_id' => $v['sponsor_id']
                                            ];
                                            array_push($persons, $person);

                                            $success++;
                                        }else{
                                            if ($isPass['reason'] =='blocked') {
                                                $blocked++;
                                                $result[]=['name' => $item['name'],'id_card_number' => $item['id_card_number'],'reason'=>'blocked'];
                                            }
                                            if ($isPass['reason'] =='nominated') {
                                                $nominated++;
                                                $result[]=['name' => $item['name'],'id_card_number' => $item['id_card_number'],'reason'=>'nominated'];
                                            }
                                            if ($isPass['reason'] =='policy_restricted') {
                                                $policy_restricted++;
                                                $result[]=['name' => $item['name'],'id_card_number' => $item['id_card_number'],'reason'=>$isPass['sub']];
                                            }
                                        } }
                                }else{
                                    $result[]=['name' => $item['name'],'id_card_number' => $item['id_card_number'],'reason'=>'duplicated'];
                                    $duplicated++;
                                }
                            }
                            else{
                                $result[]=['name' => $item['name'],'id_card_number' => $item['id_card_number'],'reason'=>'duplicated'];
                                $duplicated++;
                            }
                        }
                    }
                }
            }

            $total_restricted= $policy_restricted + $blocked + $nominated + $duplicated+$not_same;
            $response["result"]= $result;

            if($nominated == sizeof($request->sponsorship_cases)){
                $response["status"]= 'failed';
                $response["msg"]= trans('sponsorship::application.The selected cases are nominated');
                if($sponsor_blocked_count != 0 ) {
                    $response["msg"] .=' ، '.  trans('sponsorship::application.sponsor_blocked') .' : '. $sponsor_blocked_count  . '  (   '  . $sponsor_blocked_names.'   )';
                }
                return response()->json($response);
            }

            if(($policy_restricted+$blocked) ==sizeof($request->sponsorship_cases)){
                $response["status"]= 'inserted_error';
                $response["msg"]= trans('sponsorship::application.The selected cases are not pass may not category blocked or policy restricted');
                if($sponsor_blocked_count != 0 ) {
                    $response["msg"] .=' ، '.  trans('sponsorship::application.sponsor_blocked') .' : '. $sponsor_blocked_count  . '  (   '  . $sponsor_blocked_names.'   )';
                }
                return response()->json($response);
            }

            if($total_restricted ==sizeof($request->sponsorship_cases)){
                $response["status"]= 'inserted_error';
                $response["msg"]= trans('sponsorship::application.The selected cases are restricted');
                if($sponsor_blocked_count != 0 ) {
                    $response["msg"] .=' ، '.  trans('sponsorship::application.sponsor_blocked') .' : '. $sponsor_blocked_count  . '  (   '  . $sponsor_blocked_names.'   )';
                }
                return response()->json($response);
            }

            if(sizeof($sponsorship_cases) != 0 ){
                if(SponsorshipsPerson::insert($sponsorship_cases)){
                    $user = \Auth::user();
                    foreach ($cases as $item) {
                        $new=SponsorshipCases::create($item);
                        $log=['sponsorship_cases_id'=>$new->id, 'user_id'=>$user->id, 'status'=>1];
                        SponsorshipCasesStatusLog::create($log);
                    }


                    if($success !=0 ){
                        \Log\Model\Log::saveNewLog('SPONSORSHIP_CASE_CREATED',trans('sponsorship::application.Nominated a group of persons to one or several sponsors, the number of persons')  . ' ' .$success);

                    }
                    if($total_restricted ==0 ){
                        if($sponsor_blocked_count == 0 ){
                            $response["status"]= 'success';
                            $response["msg"]= trans('sponsorship::application.The cases has been successfully nomination');
                        }else{
                            $response["status"]= 'failed';
                            $response["msg"]= trans('sponsorship::application.The cases has been successfully nomination')
                                .' ، '.  trans('sponsorship::application.sponsor_blocked') .' : '. $sponsor_blocked_count  . '  (   '  . $sponsor_blocked_names.'   )';
                        }

                    }else{
                        $response["status"]= 'failed';
                        $response["msg"]= trans('sponsorship::application.The cases is not successfully nomination')
                            . ' ( '. trans('sponsorship::application.total') .': ' .sizeof($request->sponsorship_cases)
                            .' ، '.  trans('sponsorship::application.sponsor_blocked') .' : '. $sponsor_blocked_count .' , '
                            .' ، '. trans('sponsorship::application.new nominate') .' : '. $success.' , '
                            .' ، '. trans('sponsorship::application.not_same') .' : '. $not_same.' , '.
                            trans('sponsorship::application.blocked') .' : '.  $blocked .' , '.
                            trans('sponsorship::application.policy_restricted') .' : '. $policy_restricted .' , '.
                            trans('sponsorship::application.duplicated') .' : '. $duplicated .' , '.
                            trans('sponsorship::application.previously inserted') . ' : '.$nominated .' )';
                    }

                }else{
                    $response["status"]= 'failed';
                    $response["msg"]= trans('sponsorship::application.The cases is not successfully nomination')
                        . ' ( '. trans('sponsorship::application.total') .': ' .sizeof($request->sponsorship_cases)
                        .' ، '.  trans('sponsorship::application.sponsor_blocked') .' : '. $sponsor_blocked_count .' , '
                        .' ، '. trans('sponsorship::application.new nominate') .' : '. $success.' , '
                        .' ، '. trans('sponsorship::application.not_same') .' : '. $not_same.' , '.
                        trans('sponsorship::application.blocked') .' : '.  $blocked .' , '.
                        trans('sponsorship::application.policy_restricted') .' : '. $policy_restricted .' , '.
                        trans('sponsorship::application.duplicated') .' : '. $duplicated .' , '.
                        trans('sponsorship::application.previously inserted') . ' : '.$nominated .' )';       }
            }

            return response()->json($response);
        }

        // update status of sponsorship_cases
        public function UpdateCasesStatus(Request $request){

        $this->authorize('update', SponsorshipCases::class);
        $user = \Auth::user();
        $action=$request->get('action');
        $status=$request->get('status');
        $cases =$request->get('cases');

        $update=0;
        $theSame=0;
        $notAllowed=0;
        foreach ($cases as $item) {
            $data=$log=[];
            if($item['status'] ==$status){
                $theSame++;
            }
            else{

                $log['sponsorship_cases_id']=$item['id'];
                $log['user_id']=$user->id;
                $log['status']=$data['status']=$status;
                $log['date']=null;
                if($action=='single') {
                    if(isset($item['sponsorship_number'])){
                        $input['sponsorship_number']= $item['sponsorship_number'];
                        $rules['sponsorship_number']= 'required|unique:char_sponsorship_cases,sponsor_number,'.$item['id'];

                        $error = \App\Http\Helpers::isValid($input,$rules);
                        if($error)
                            return response()->json($error);

                    }
                }
                if($action =='group') {
                    if($request->get('sponsorship_date')){
                        $data['sponsorship_date'] =date('Y-m-d',strtotime($request->get('sponsorship_date')));
                    }
                } else {
                    if(isset($item['sponsorship_date'])){
                        $data['sponsorship_date'] =date('Y-m-d',strtotime($item['sponsorship_date']));
                    }
                }
                if($action =='group') {
                    if($request->get('sponsorship_number')){
                        $data['sponsor_number'] =$request->get('sponsorship_number');
                    }
                } else {
                    if(isset($item['sponsorship_number'])){
                        $data['sponsor_number']=$item['sponsorship_number'];
                    }
                }
                if($action =='group') {
                    if($request->get('reason')){
                        $log['status_reason']=$data['last_status_reason'] =$request->get('reason');
                    }
                } else {
                    if(isset($item['reason'])){
                        $log['status_reason']=$data['last_status_reason']=$item['reason'];
                    }

                }
                if($action =='group') {
                    if($request->get('sponsorship_date')){
                        $data['last_status_date'] =date('Y-m-d',strtotime($request->get('date')));
                        if($status == 3 ||$status == 4 ||$status == 4 ){
                            $log['date']=$data['last_status_date'];
                        }
                    }
                } else {
                    if(isset($item['date'])){
                        $data['last_status_date']=date('Y-m-d',strtotime($item['date']));
                        if($status == 3 ||$status == 4 ||$status == 4 ){
                            $log['date']=$data['last_status_date'];
                        }
                    }

                }

                $object=SponsorshipCases::findorfail($item['id']);
                if($object->status ==$status){
                    $theSame++;
                }else{
                    if($request->get('sub')){
                        $update_1=SponsorshipCases::where('id',$item['id'])->update($data);
                        if($update_1){
                            SponsorshipCasesStatusLog::create($log);
                            $update++;
                        }
                    }else{
                        if($status == $object->status+1 || $status < $object->status ){
                            $update_1=SponsorshipCases::where('id',$item['id'])->update($data);
                            if($update_1){
                                SponsorshipCasesStatusLog::create($log);
                                $update++;
                            }
                        }else{
                            $notAllowed++;
                        }
                    }

                }


            }
        }


        if($notAllowed == sizeof($cases)){
            $response["status"]= 'failed';
            $response["msg"]= trans('sponsorship::application.not allowed to change fro current status to selected status');
        }
        else if($theSame == sizeof($cases)){
            $response["status"]= 'failed';
            $response["msg"]= trans('sponsorship::application.all selected cases have the same status');
        }else{
            if($update == sizeof($cases)){
                \Log\Model\Log::saveNewLog('SPONSORSHIPS_CASES_UPDATED',trans('sponsorship::application.Edited a set of sponsored data'));
                $response["status"]= 'success';
                $response["msg"]= trans('sponsorship::application.The row is updated');
            }else if($update == 0 && $notAllowed ==0){
                $response["status"]= 'failed';
                $response["msg"]= trans('sponsorship::application.There is no update');
            }else{
                \Log\Model\Log::saveNewLog('SPONSORSHIPS_CASES_UPDATED',trans('sponsorship::application.Edited a set of sponsored data') );
                $response["status"]= 'success';
                $response["msg"]= trans('sponsorship::application.update status for some cases') .' , '.
                    trans('sponsorship::application.total cases count') .' : ' .sizeof($cases) .' , ' .
                    trans('sponsorship::application.updated') .' : ' .$update .' , ' .
                    trans('sponsorship::application.the same') .' : ' .$theSame .' . '.
                    trans('sponsorship::application.the not Allowed') .' : ' .$notAllowed ;
            }
        }
        return response()->json($response);
    }

        // update sponsorship_date of sponsorship_cases
        public function UpdateSponsorshipDate($id,Request $request){

        $obj_1 = SponsorshipCases::findOrFail($id);
        $this->authorize('update', $obj_1);

        $response = array();

        $input=['sponsorship_date' => strip_tags($request->get('sponsorship_date'))];
        $rules= ['sponsorship_date' => 'required|date'];


            $error = \App\Http\Helpers::isValid($input,$rules);
            if($error)
                return response()->json($error);


        if(SponsorshipCases::where('id',$id)->update(['sponsorship_date'=>date('Y-m-d',strtotime($input['sponsorship_date']))]))
        {
            $case = \Common\Model\CaseModel::fetch(array('full_name'=>true),$obj_1->case_id);
            $name=$case->full_name;
            \Log\Model\Log::saveNewLog('SPONSORSHIPS_CASES_UPDATED',trans('sponsorship::application.Edited sponsorship date for')  . ' "'.$name. '" ');
            $response["status"]= 'success';
            $response["msg"]= trans('setting::application.The row is updated');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('setting::application.There is no update');
        }
        return response()->json($response);

    }

        // update sponsor number of sponsorship_cases
        public function updateSponsorNumber($id,Request $request)
        {
            $response =array();
            $obj_1 = SponsorshipCases::findOrFail($id);
            $this->authorize('update', $obj_1);

            if($obj_1->sponsor_number == $request->get('sponsor_number')){
                $response["status"] = 'theSame';
                return response()->json($response);
            }
            $rules['sponsor_number']= 'required|unique:char_sponsorship_cases,sponsor_number,'.$id;
            $error = \App\Http\Helpers::isValid($request->all(),$rules);
            if($error)
                return response()->json($error);


            $obj_1->sponsor_number=$request->get('sponsor_number');
            if($obj_1->save()){

                $case = \Common\Model\CaseModel::fetch(array('full_name'=>true),$obj_1->case_id);
                $name=$case->full_name;
                \Log\Model\Log::saveNewLog('SPONSORSHIPS_CASES_UPDATED',trans('sponsorship::application.Edited sponsorship number for') . ' "'.$name. '" ');

                $response["status"]= 'success';
                $response["msg"]= trans('setting::application.The row is updated');

            }else{
                $response["status"]= 'failed';
                $response["msg"]= trans('setting::application.There is no update');
            }

            return response()->json($response );
        }

       // update status of sponsorship_cases (from xlsx)
        public function UpdateSponsorshipStatus(Request $request)
        {
    //        $this->authorize('import',SponsorshipCases::class);
            ini_set('max_execution_time',0);
            set_time_limit(0);
            Ini_set('memory_limit',-1);

            $user = \Auth::user();

            $using=$request->get('using');
            $sponsor_id=null;
            $category_id=null;

            if($using ==1){
                $sponsor_id=$request->get('sponsor_id');
                $category_id=$request->get('category_id');
            }else{
                $sponsorship_id=$request->get('sponsorship_id');
                $sponsorship=Sponsorships::where('id',$sponsorship_id)->first();
                if($sponsorship){
                    $category_id=$sponsorship->category_id;
                }
            }
            $status=$request->get('status');
            $old_status=$status-1;

            $importFile = $request->file;
            if ($importFile->isValid()) {
                $path = $importFile->getRealPath();
                $excel = \PHPExcel_IOFactory::load($path);
                $sheets = $excel->getSheetNames();
                if(in_array('data', $sheets)) {
                    $first = \Excel::selectSheets('data')->load($path)->first();
                    if (!is_null($first)) {
                        $keys = $first->keys()->toArray();
                        $validFile = true;
                        $constraint = ["alsbb","altarykh", "rkm_alhoy","rkm_altrshyh"];
                        foreach ($constraint as $item_) {
                          if ( !in_array($item_, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }
                        if ($validFile) {

                            $path = \Input::file('file')->getRealPath();

                            $data = \Excel::load($path, function ($reader) { $reader->formatDates(true, 'Y-m-d'); })->get();

                            $not_case=0;
                            $not_person=0;
                            $Done=0;
                            $Invalid_status=0;
                            $not_valid=0;
                            $not_nominated=0;
                            $theSame=0;
                            $restrictedInfo=[];
                            $size_ = 0;

                            foreach ($data as $key => $value) {
                                if($value->rkm_btak_alhoy || $value->rkm_alhoy){
                                    $card=null;
                                    if($value->rkm_btak_alhoy){
                                        $card=$value->rkm_btak_alhoy;
                                    }
                                    if($value->rkm_alhoy){
                                        $card=$value->rkm_alhoy;
                                    }

                                    $find=\Common\Model\Person::where(function ($q) use ($id_card_number){
                                        $q->where('id_card_number',$id_card_number);
                                    })->first();

                                    if($find){
                                        // Cases table
                                        $findCases= \Common\Model\AbstractCases::fatchCasesIds($find->id,$user->organization_id,$category_id);
                                        if(sizeof($findCases) != 0){
                                            // on statistic ( sponsorship_cases )
                                            $findRec=SponsorshipCases::where(['sponsor_id'=>$sponsor_id])->whereIn('case_id',$findCases)->first();
                                            if($findRec) {

                                                if($findRec->status == $status){
                                                    $theSame++;
                                                    $restrictedInfo[]=['name' =>$find->first_name .' '. $find->second_name .' '. $find->third_name.' '. $find->last_name ,
                                                        'id_card_number' => $card ,'reason' => 'the same status'];
                                                }else{
                                                    if(($status == ($findRec->status+1)) || ($status == ($findRec->status-1))){
                                                        $updated=['status' => $status, 'last_status_date' => date("Y-m-d")];
                                                        $log=['sponsorship_cases_id'=>$findRec->id, 'user_id'=>$user->id, 'status'=>$status];

                                                        if($value->alsbb){
                                                            $log['status_reason']=$updated['last_status_reason']=$value->alsbb;
                                                        }

                                                        if($value->altarykh){
                                                            if($status ==3){
                                                                $sponsorship_date = Helpers::getFormatedDate($value->altarykh);
                                                                if (!is_null($sponsorship_date)) {
                                                                    $updated['sponsorship_date']=$sponsorship_date;
                                                                }

                                                                if($value->rkm_altrshyh) {
                                                                    $valid=SponsorshipCases::checkISponsorNumber($findRec->case_id,$sponsor_id,$value->rkm_altrshyh);
                                                                    if($valid['status'] == true){
                                                                        $updated['sponsor_number']=$value->rkm_altrshyh;
                                                                    }
                                                                }

                                                                if($value->kod_altrshyh) {
                                                                    $valid=SponsorshipCases::checkISponsorNumber($findRec->case_id,$sponsor_id,$value->rkm_altrshyh);
                                                                    if($valid['status'] == true){
                                                                        $updated['sponsor_number']=$value->kod_altrshyh;
                                                                    }
                                                                }

                                                            }else if($status ==4 || $status ==5 ){
                                                                $log['date']= date('Y-m-d',strtotime($value->altarykh));
                                                            }
                                                        }


                                                        SponsorshipCases::where('id',$findRec->id)->update($updated);
                                                        SponsorshipCasesStatusLog::create($log);
                                                        $Done++;
                                                    }
                                                    else{
                                                        $Invalid_status++;
                                                        $restrictedInfo[]=['name' =>$find->first_name .' '. $find->second_name .' '. $find->third_name.' '. $find->last_name ,
                                                            'id_card_number' => $card ,'reason' => 'invalid status'];
                                                    }
                                                }


                                            }else{
                                                $not_nominated++;
                                                $restrictedInfo[]=['name' =>$find->first_name .' '. $find->second_name .' '. $find->third_name.' '. $find->last_name ,
                                                    'id_card_number' => $card ,'reason' => 'not_nominated'];
                                            }

                                        }
                                        else{
                                            $restrictedInfo[]=['name' =>$find->first_name .' '. $find->second_name .' '. $find->third_name.' '. $find->last_name ,
                                                'id_card_number' => $card ,'reason' => 'not_case'];
                                            $not_case++;
                                        }
                                    }else{
                                        $restrictedInfo[]=['name' =>'-' ,'id_card_number' => $card ,'reason' => 'invalid_id_card_number'];
                                        $not_person++;
                                    }

                                }else{
//                            $restrictedInfo[]=['name' =>'-' ,'id_card_number' =>]
                                    $not_valid++;
                                }
                                $size_++;
                            }

                            if(sizeof($data) == $not_valid){
                                $response["status"]= 'failed';
                                $response["msg"]= trans('sponsorship::application.the uploaded file empty or missing id car number check it');
                            }
                            else if($Invalid_status == sizeof($data)){
                                $response["status"]= 'failed';
                                $response["msg"]= trans('sponsorship::application.not allowed updating status oc cases from current status to selected');
                            }
                            else if($theSame == sizeof($data)){
                                $response["status"]= 'failed';
                                $response["msg"]= trans('sponsorship::application.all selected cases have the same status');
                            }
                            else if($not_nominated == sizeof($data)){
                                $response["status"]= 'failed';
                                $response["msg"]= trans('sponsorship::application.all selected cases have the same status');
                            }
                            else{
                                if($Done == 0){
                                    $response["status"]= 'failed';
                                    $response["msg"]= trans('sponsorship::application.There is no update') .' , '.
                                        trans('sponsorship::application.total cases count') .' : ' .$size_ .' , ' .
                                        trans('sponsorship::application.updated') .' : ' .$Done .' , ' .
                                        trans('sponsorship::application.invalid status') .' : ' .$Invalid_status .' , ' .
                                        trans('sponsorship::application.the same') .' : ' .$theSame .' , ' .
                                        trans('sponsorship::application.not person') .' : ' .$not_person .' , ' .
                                        trans('sponsorship::application.not case') .' : ' .$not_case .' . ' .
                                        trans('sponsorship::application.not nominated') .' : ' .$not_nominated .' . ' ;

                                }
                                else if($Done == sizeof($data)){
                                    $response["status"]= 'success';
                                    $response["msg"]= trans('sponsorship::application.The row is updated');
                                    \Log\Model\Log::saveNewLog('SPONSORSHIPS_CASES_UPDATED',trans('setting::application.Edited a set of sponsored data'));
                                }else if($Done == 0 && $Invalid_status == 0 && $theSame == 0 && $not_person == 0 && $not_case == 0 && $not_nominated == 0 ){
                                    $response["status"]= 'failed';
                                    $response["msg"]= trans('sponsorship::application.There is no update');
                                }else{
                                    \Log\Model\Log::saveNewLog('SPONSORSHIPS_CASES_UPDATED',trans('setting::application.Edited a set of sponsored data'));
                                    $response["status"]= 'success';
                                    $response["msg"]= trans('sponsorship::application.update status for some cases') .' , '.
                                        trans('sponsorship::application.total cases count') .' : ' .sizeof($data) .' , ' .
                                        trans('sponsorship::application.updated') .' : ' .$Done .' , ' .
                                        trans('sponsorship::application.invalid status') .' : ' .$Invalid_status .' , ' .
                                        trans('sponsorship::application.the same') .' : ' .$theSame .' , ' .
                                        trans('sponsorship::application.not person') .' : ' .$not_person .' , ' .
                                        trans('sponsorship::application.not case') .' : ' .$not_case .' . ' .
                                        trans('sponsorship::application.not nominated') .' : ' .$not_nominated .' . ' ;
                                }
                            }
                            $response['result']=$restrictedInfo;
                            return response()->json($response);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                    return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file is empty')]);
                }
                return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('aid::application.The import sheet does not exist in the Excel , please check the label in the instructions or in the template') ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.invalid file')]);
        }

}
