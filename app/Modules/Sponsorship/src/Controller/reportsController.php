<?php
namespace Sponsorship\Controller;

use App\Http\Controllers\Controller;
use App\Http\Helpers;
use Illuminate\Http\Request;
use Sponsorship\Model\SponsorshipCases;
use Sponsorship\Model\Reports;
use Sponsorship\Model\ReportsCasesDocuments;
use Illuminate\Support\Facades\Storage;

class reportsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // paginate of sponsor reports og logged organization
    public function filter(Request $request)
    {
        $this->authorize('manage',Reports::class);

        $items = Reports::filter($request->all());

        if($request->get('action') =='export'){
            if(sizeof($items) !=0){
                $data=[];
                foreach($items as $key =>$value){
                       $data[$key][trans('sponsorship::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        $data[$key][trans('sponsorship::application.'. $k)]= $v;
                    }
                }
                $token = md5(uniqid());
                \Excel::create('export_' . $token,function($excel) use($data) {
                    $excel->sheet(trans('sponsorship::application.List of archived reports'),function($sheet) use($data) {
                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Calibri',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $style = [
                             'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold'      =>  true
                            ]
                        ];

                        $sheet->getStyle("A1:J1")->applyFromArray($style);
                        $sheet->setHeight(1,30);

                        $style = [
                             'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold' => false
                            ]
                        ];

                        $sheet->getDefaultStyle()->applyFromArray($style);
                        $sheet->fromArray($data);
                    });
                })
                    ->store('xlsx',storage_path('tmp/'));
                \Log\Model\Log::saveNewLog('REPORT_EXPORTED',trans('sponsorship::application.Exported list of archived reports'));
                return response()->json(['status'=>true ,'download_token' => $token ]);
            }else{
                return response()->json(['status'=>false]);
            }
        }

        return response()->json($items);
    }

    // statistic the attachments of sponsor reports og logged organization
    public function attachments(Request $request)
    {
        $this->authorize('manage',ReportsCasesDocuments::class);

        $items = ReportsCasesDocuments::filter($request->all());

        if ($request->get('action') == 'export'){

            if($items['count'] > 0){
                if(sizeof($items['items'])!=0){
                    $token=  md5(uniqid());
                    $filename = storage_path('tmp/export_' . $token . '.zip');
                    $zip = new \ZipArchive();
                    $zip->open($filename,\ZipArchive::CREATE);
                    foreach ($items['items'] as $k =>$v){
                        if(sizeof($v->files) > 0){
                            $folder = $v->id_card_number . '_' . $v->full_name;
                            $zip->addEmptyDir($folder);

                            foreach ($v->files as $key => $value) {
                                if($key != 'photo_album' && $key != 'medical_report'){
                                    $path = storage_path('app/').$value;
                                    $ext = explode(".",$value);
                                    $zip->addFile($path,$folder .'/' .trans('sponsorship::application.'. $key).".".end($ext));
                                }else{
                                    $dir= $folder . '/'.trans('sponsorship::application.'. $key) ;
                                    $zip->addEmptyDir($dir);
                                    $i=1;
                                    foreach ($value as $photo){
                                        $path = storage_path('app/').$photo;
                                        $ext = explode(".",$photo);
                                        $zip->addFile($path,$folder .'/'.trans('sponsorship::application.'. $key).'/' .trans('sponsorship::application.'. $key)."_".$i.".".end($ext));
                                        $i++;
                                    }
                                }
                            }
                        }
                    }
                    $zip->close();
                    return response()->json(['download_token' => $token,'status' => true]);
                }
            }

            return response()->json(['status' => false]);

        }
        elseif($request->get('action') =='exportToExcel'){
            if(sizeof($items) !=0){
                $ignore=['id','case_id','person_id'];
                $files=['thanks_message','school_certificate','long_photo','per_photo','single_document','medical_report','photo_album'];
                $multiple=['medical_report','photo_album'];
                $data=[];

                foreach($items as $key =>$value){
                    $data[$key][trans('sponsorship::application.#')]=$key+1;
                    foreach($value as $k =>$v){

                        if (!in_array($k,$ignore)) {
                            if(in_array($k,$files)) {
                                if($k == 'photo_album' && $k == 'medical_report'){
                                    $data[$key][trans('sponsorship::application.'. $k)]= $v;
                                }else{
                                    $status = trans('sponsorship::application.exist');
                                    if($v == 0){
                                        $status = trans('sponsorship::application.not exist');
                                    }
                                    $data[$key][trans('sponsorship::application.'. $k)]= $status;
                                }
                            }
                            else{
                                $data[$key][trans('sponsorship::application.'. $k)]= $v;
                            }
                        }
                    }
                }
                $token = md5(uniqid());
                \Excel::create('export_' . $token,function($excel) use($data) {
                    $excel->sheet(trans('sponsorship::application.List of archived reports'),function($sheet) use($data) {
                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Calibri',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $style = [
                             'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold'      =>  true
                            ]
                        ];

                        $sheet->getStyle("A1:P1")->applyFromArray($style);
                        $sheet->setHeight(1,30);

                        $style = [
                             'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold' => false
                            ]
                        ];

                        $sheet->getDefaultStyle()->applyFromArray($style);
                        $sheet->fromArray($data);
                    });
                })
                    ->store('xlsx',storage_path('tmp/'));
                \Log\Model\Log::saveNewLog('REPORT_EXPORTED',trans('sponsorship::application.Exported list of archived reports'));
                return response()->json([
                    'download_token' => $token,
                ]);
            }else{
                return response()->json(['status'=>false]);
            }
        }

        return response()->json($items);
    }

    // return the attachments of case using ( sponsorship_cases_id , doc_category_id , date_from , date_to )
    public function caseAttachments(Request $request)
    {

        $items = ReportsCasesDocuments::caseAttachments($request->all());
        if ($request->get('action') == 'export'){
            if(sizeof($items)!=0){
                $token=  md5(uniqid());
                $filename = storage_path('tmp/export_' . $token . '.zip');
                $zip = new \ZipArchive();
                $zip->open($filename,\ZipArchive::CREATE);
                $folder = trans('sponsorship::application.Attachments');
                $zip->addEmptyDir($folder);
                $multiple = [7,5];
                $i=0;
                $j=0;

                foreach ($items as $key => $value) {

                    if(!in_array($value->document_type_id,$multiple)){
                        $path = storage_path('app/').$value->filepath;
                        $ext = explode(".",$value->filepath);
                        $zip->addFile($path,$folder .'/' .$value->document_type_name.".".end($ext));
                    }elseif(in_array($value->document_type_id,$multiple)){
                        if($value->document_type_id == 7){
                                $dir=trans('sponsorship::application.photo_album');
                        }else{
                                $dir=trans('sponsorship::application.medical_report');
                        }
                        if(!$zip->locateName($folder.'/'.$dir)){
                            $zip->addEmptyDir($folder.'/'.$dir);
                        }
                        $path = storage_path('app/').$value->filepath;
                        $ext = explode(".",$value->filepath);
                        if($value->document_type_id == 7){
                            $i++;
                            $zip->addFile($path,$folder.'/'.$dir .'/' .$value->document_type_name."_".$i.".".end($ext));
                        }else{
                            $j++;
                            $zip->addFile($path,$folder.'/'.$dir .'/' .$value->document_type_name."_".$j.".".end($ext));
                        }

                    }
                }
                $zip->close();
                return response()->json(['download_token' => $token,'status' => true]);
            }
            return response()->json(['status' => false]);

        }
        return response()->json($items);
    }

    // regenerate reports with options ( sponsorship_cases_id , doc_category_id , date_from , date_to) to replace updated documents
    public function generateWithOptions(Request $request)
    {

        $user = \Auth::user();
        $response = array();
        $case = SponsorshipCases::getCaseInfo($request->sponsorship_cases_id);
        $response=[];

        $reports= Reports::where(['organization_id'   =>$user->organization_id ,
            'sponsor_id'   => (int)$case->sponsor_id ,
            'category_id'  => (int)$case->category_id,
            'category'     => (int)$request->doc_category_id ,
            'date_from'    => date('Y-m-d',strtotime($request->date_from))  ,
            'date_to'      => date('Y-m-d',strtotime($request->date_to))])->get();
        if(sizeof($reports) > 0){
            foreach ( $reports as $key => $value){
                if(is_null($value->target)){
                    Reports::Regenerate($value);
                }else{
                    $cases=explode(",",$value->target);
                    if(in_array($request->sponsorship_cases_id,$cases)){
                        $value->finalCases=  $cases;
                        Reports::Regenerate($value);
                    }

                }
            }
            return response()->json(["status" => 'success' ,"msg" =>trans('sponsorship::application.the reports re generate successfully')]);
        }
        return response()->json(["status" => 'success' ,"msg" =>trans('sponsorship::application.no reports to regenerate')]);
    }

    // create new report and generate zip file on report storage folder
    public function store(Request $request)
    {
        try {

            $this->authorize('create',Reports::class);

            ini_set('max_execution_time',0);
            set_time_limit(0);
            Ini_set('memory_limit','2048M');

            $response = array();
            $input=[
                'sponsor_id'   => $request->sponsor_id ,'category_id'  => $request->category_id,
                'category'     => $request->category   ,'report_type'  => $request->report_type,
                'title'        => $request->title      ,'notes'        => $request->notes,
                'date_from'    => $request->date_from  ,'date_to'      => $request->date_to,
                'language'      => $request->language,
                'all'      => $request->all,
                'cases'      => $request->cases
            ];

            $user = \Auth::user();

            $error = Helpers::isValid($request->all(),Reports::$rules);
            if($error)
                return response()->json($error);

            $input['organization_id']=$user->organization_id;
            $input['date_from']=date('Y-m-d',strtotime($input['date_from']));
            $input['date_to']=date('Y-m-d',strtotime($input['date_to']));
            $input['date']=date('Y-m-d');
            $input['folder']=  md5(uniqid());

            $pass=true;

            $founded = Reports::where(['organization_id'   =>$user->organization_id ,
                                       'sponsor_id'   => (int)$request->sponsor_id ,
                                       'language'  => $request->language,
                                       'category_id'  => (int)$request->category_id,
                                       'category'     => (int)$request->category   ,'report_type'  => $request->report_type,
                                       'date_from'    => $input['date_from']  ,'date_to'      => $input['date_to']])->first();

            if($founded) {
                $pass=false;
                if($founded->category == 5){
                    $pass=true;
                }
            }

           if(!$pass){
               $response["msg"]= trans('sponsorship::application.the report is previously created');
                $response["status"]= 'failed';

            }else{
               $params = $input;
                $params['all']=$request->all;
                $params['cases']=$request->cases;
                $items = Reports::CasesData($input);

                if($items['status'] == false){
                    if(isset($items['items'])){
                        $response["msg"]= trans('sponsorship::application.cases un defined to sponsor category');
                    }else{
                        $response["msg"]= trans('sponsorship::application.the report can not create because attachments are incomplete');
                    }
                    $response["status"]= 'failed';
                }
                else{
                    if($input['all'] == false){
                        if(isset($input['cases'])){
                            if(is_array($input['cases']) ){
                                if(sizeof($input['cases'])>0){
                                    $casesList=[];
                                    foreach ($input['cases'] as $k=>$value){
                                        $casesList[]=$value['id'];
                                    }
                                    $input['target']=implode(",",$casesList);
                                }
                            }
                        }
                    }
                    unset($input['all']);
                    unset($input['cases']);
                    $report=Reports::create($input);
                    if($report) {
                        Reports::createZip($report,$items['items']);
                        $response['download_token']=  $report->folder;
                        $response["status"]= 'success';
                        $response["msg"]= trans('setting::application.The row is inserted to db');
                    }else{
                        $response["msg"]= trans('setting::application.The row is not insert to db');
                        $response["status"]= 'failed';
                    }
                }
            }

            return response()->json($response);

        } catch (Exception $e) {
            return response()->json(["status" => 'error',"error_code" =>  $e->getCode(),"msg" =>  $e->getMessage()]);
        }
    }

    // return zip file name to download it
    public function download($id)
    {
        try {
            $this->authorize('export',Reports::class);
            $report = Reports::findorfail($id);
            return response()->json(['download_token' => $report->folder]);
        } catch (Exception $e) {
            return response()->json(["status" => 'error',"error_code" =>  $e->getCode(),"msg" =>  $e->getMessage()]);
        }
    }

    // get info of  sponsorship case using (sponsorship_cases_id as id)
    public function getCaseInfo($id)
    {
        return response()->json(SponsorshipCases::getCaseInfo($id));
    }

    // set attachment if sponsorship case with options
    // ( sponsorship_cases_id, document_type_id , doc_category_id , date_from , date_to)
    // and regenerate the reports of this case with options replace updated documents
    public function setAttachment(Request $request)
    {
        $response = array();
        $id=$request->get('attach_id');

        if(!is_null($id)){
            $document_id=$request->get('document_id');
            $attachment = ReportsCasesDocuments::findorfail($id);
            $attachment->document_id=$document_id;
        }
        else{
            $attachment = new ReportsCasesDocuments();
            $attachment->sponsorship_cases_id = $request->sponsorship_cases_id;
            $attachment->doc_category_id = $request->doc_category_id;
            $attachment->document_type_id = $request->document_type_id;
            $attachment->document_id = $request->document_id;
            $attachment->date_from = date('Y-m-d',strtotime($request->date_from));
            $attachment->date_to = date('Y-m-d',strtotime($request->date_to));
            $attachment->status = 1;
        }

        if($attachment->save()){
            $user = \Auth::user();
            $case = SponsorshipCases::getCaseInfo($attachment->sponsorship_cases_id);
            $reports= Reports::where(['organization_id'   =>$user->organization_id ,
                'sponsor_id'   => (int)$case->sponsor_id ,
                'category_id'  => (int)$case->category_id,
                'category'     => (int)$attachment->doc_category_id ,
                'date_from'    => $attachment->date_from  ,'date_to'      => $attachment->date_to])->get();
            if(sizeof($reports) > 0){
                foreach ( $reports as $key => $value){
                    Reports::Regenerate($value);
                }
            }
            return response()->json(["status" => 'success' ,"msg" =>trans('aid::application.The Document is selected successfully')]);

        }else{
            return response()->json(["status" => 'failed' ,"msg" =>trans('aid::application.The Document selected failed')]);
        }
    }

    // prepare folders to import  case documents with options
    // ( sponsorship_cases_id, document_type_id , doc_category_id , date_from , date_to)
    public function prepare(Request $request)
    {
        try {
            $this->authorize('import',ReportsCasesDocuments::class);
            $items = ReportsCasesDocuments::prepareImport($request->all());

            $token = md5(uniqid());
            $filename = storage_path('tmp/import_documents_' . $token . '.zip');
            $zip = new \ZipArchive();
            $zip->open($filename, \ZipArchive::CREATE);

            $subFolders =  [ array('name'=> 'per_photo'  ,'id' => 1),
                             array('name'=> 'long_photo'  ,'id' => 2),
                             array('name'=> 'single_document'  ,'id' => 3),
                             array('name'=> 'thanks_message'   ,'id' => 4),
                             array('name'=> 'medical_report'   ,'id' => 5),
                             array('name'=> 'school_certificate','id' => 6),
                             array('name'=> 'photo_album'   ,'id' => 7)
                        ];

            foreach ($items as $key=>$value) {
                $mainFolder = $value->id . '__' . $value->id_card_number . '__' . $value->full_name;
                $zip->addEmptyDir($mainFolder);
                foreach ($subFolders as $folder) {
                    $subFolder = $folder['id'] . '__' . trans("sponsorship::application.".$folder['name']);
                    $zip->addEmptyDir($mainFolder . '/' . $subFolder);
                }
            }
            $zip->close();
            return response()->json(['download_token' => $token]);
        } catch (Exception $e) {
            return response()->json(["status" => 'error',"error_code" =>  $e->getCode(),"msg" =>  $e->getMessage()]);
        }
    }

    // do import of case documents with options
    // ( sponsorship_cases_id, document_type_id , doc_category_id , date_from , date_to)
    public function import(Request $request)
    {
        $this->authorize('import',ReportsCasesDocuments::class);
        $guesser = \Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser::getInstance();

        $date_from = date('Y-m-d',strtotime($request->date_from));
        $date_to = date('Y-m-d',strtotime($request->date_to));
        $doc_category_id = $request->doc_category_id;
        $user = \Auth::user();
        $uploadFile = $request->file('file');
        if ($uploadFile->isValid()) {
            $files = $this->readZipFile($uploadFile);
            $new = $updated = 0;
            if (sizeof($files) > 0) {
                $newFiles = [];
                if (isset($files) && is_array($files)) {
                    foreach ($files as $file) {
                        if (!isset($file['filepath']) || empty($file['filepath'])) {
                            continue;
                        }

                        $caseFile[] = ReportsCasesDocuments::createNew([
                            'sponsorship_cases_id' => $file['sponsorship_cases_id'],
                            'document_type_id' => $file['document_type_id'],
                            'doc_category_id' => $doc_category_id,
                            'date_from' => $date_from,
                            'date_to' => $date_to,
                            'name' => $file['document_type_name'] . ' - ' . $file['name'],
                            'filepath' => $file['filepath'],
                            'size' => Storage::size($file['filepath']),
                            'mimetype' => $guesser->guess(storage_path('app/' . $file['filepath'])),
                            'user_id' => $user->id,
                        ]);
                        $new++;
                    }
                }
            }

            return response()->json(['total'  => $new + $updated, 'insert' => $new, 'update' => $updated]);
        }
    }

    // read import compressed of case documents
    protected function readZipFile($uploadFile)
    {
        $files = [];
        $zip = new \ZipArchive();
        $res = $zip->open($uploadFile->getPathname());
        for ($i = 0; $i < $zip->numFiles; $i++) {
            $filename = $zip->getNameIndex($i);
            $parts = explode('/', trim($filename, '/'));
            if (count($parts) >= 3) {
                list($sponsorship_cases_id, $id_card_number, $name) = explode('__', $parts[0]);
                list($document_type_id,$document_type_name) = explode('__', $parts[1]);

                $content = $zip->getFromIndex($i);
                $fn = md5($content) . substr($parts[2], strrpos($parts[2], '.'));
                Storage::put('documents/' . $fn, $content);

                $files[] = [
                                                    'sponsorship_cases_id' => $sponsorship_cases_id,
                                                    'document_type_id' => $document_type_id,
                                                    'name' => $name,
                                                    'document_type_name' => $document_type_name,
                                                    'filename' => $parts[2],
                                                    'filepath' => 'documents/' . $fn,
                                                  ];
                unset($content);
            }

        }

        return $files;
    }

    // generate compressed file of report
    public function generate(Request $request,$id){
        $report = Reports::findorfail($id);$report = Reports::findorfail($id);
        if(!is_null($report)){
            Reports::Regenerate($report);
            return response()->json(["status" => 'success' ,
                'download_token' => $report->folder ,
                "msg" =>trans('sponsorship::application.the reports re generate successfully')]);
        }
        return response()->json(["status" => 'success' ,
            "msg" =>trans('sponsorship::application.no reports to regenerate')]);
    }


}
