<?php

namespace Sponsorship\Controller;
use App\Http\Controllers\Controller;
use App\Http\Helpers;
use Common\Model\CaseModel;
use Illuminate\Http\Request;
use Common\Model\PersonModels\Guardian;
use Setting\Model\BankChequeTemplate;
use Sponsorship\Model\SponsorshipCases;
use Sponsorship\Model\Sponsorships;
use Sponsorship\Model\Payments;
use Sponsorship\Model\PaymentsCases;
use Sponsorship\Model\PaymentsRecipient;
use Sponsorship\Model\Arabic_numbers;
use Sponsorship\Model\PaymentsDocuments;
use Setting\Model\Currency;
use Setting\Model\Setting;
use PDF;
use DB;
use Illuminate\Support\Facades\View;
use Sponsorship\Model\ArchivedCheques;

class PaymentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // paginate all payments of logged organization user
    public function all(Request $request){

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $this->authorize('manage',Payments::class);
        $user = \Auth::user();
        $return=Payments::filter($request->page, $user->organization_id, $request->all());

        if($request->action =='export'){
            if(sizeof($return) !=0){
                $data=[];
                $attachTypes = ['organization_sheet','organization_sheet','receipt_of_receipt'];
                foreach($return as $key =>$value){
                    $data[$key][trans('sponsorship::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        if(in_array($k, $attachTypes)) {
                            if($v == '0' || $v == 0){
                                $data[$key][trans('sponsorship::application.'. $k)] =trans('sponsorship::application.exist');
                            }else{
                                $data[$key][trans('sponsorship::application.'. $k)] =trans('sponsorship::application.not exist');
                            }
                        }else{
                            $data[$key][trans('sponsorship::application.'. $k)]= $v;
                        }
                    }
                }
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($data) {
                    $excel->sheet(trans('sponsorship::application.payments'), function($sheet) use($data) {
                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Calibri',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $style = [
                             'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold'      =>  true
                            ]
                        ];

                        $sheet->getStyle("A1:U1")->applyFromArray($style);
                        $sheet->setHeight(1,30);

                        $style = [
                             'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold' => false
                            ]
                        ];

                        $sheet->getDefaultStyle()->applyFromArray($style);
                        $sheet->fromArray($data);
                    });
                })
                    ->store('xlsx', storage_path('tmp/'));
                \Log\Model\Log::saveNewLog('PAYMENTS_EXPORTED',trans('sponsorship::application.Exported the payments report'));
                return response()->json([
                    'download_token' => $token,
                ]);
            }else{
                return response()->json(['status'=>false]);
            }
        }

        return response()->json(['Payments'  => $return['list'] ,'total'  => $return['total']]);
    }

    // paginate statistic payment's beneficiary of logged organization user
    public function persons(Request $request)
    {

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $this->authorize('manage',Payments::class);
        $user = \Auth::user();

        $result= PaymentsCases::filter($request->page, $user->organization_id, $request->all());
        if($request->action =='export'){
            if(sizeof($result) !=0){
                $data=[];
                foreach($result as $key =>$value){
                    $data[$key][trans('sponsorship::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        if($k != 'id' && $k != 'case_id'  && $k != 'pre_guardian_secondary_mobile'  && $k != 'pre_guardian_primary_mobile' && $k != 'pre_guardian_phone' ){
                            if(!$v){
                                $v='-';
                            }
                            if(substr( $k, 0, 10 ) === "recipient_"){
                                $data[$key][trans('sponsorship::application.'.substr($k,10))." ( ".trans('sponsorship::application.recipient')  ." ) "]= $v;
                            }elseif(substr( $k, 0, 7 ) === "mother_"){
                                $data[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.mother')  ." ) "]= $v;
                            }elseif(substr( $k, 0, 7 ) === "father_"){
                                $data[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.father')  ." ) "]= $v;
                            }elseif(substr( $k, 0, 9 ) === "guardian_"){
                                $data[$key][trans('sponsorship::application.'.substr($k,9))." ( ".trans('sponsorship::application.guardian')  ." ) " ]= $v;
                            }else{
                                $data[$key][trans('sponsorship::application.' . $k)]= $v;
                            }
                        }
                    }
                }
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($data) {
                    $excel->sheet(trans('sponsorship::application.payments'), function($sheet) use($data) {
                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Calibri',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $style = [
                             'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold'      =>  true
                            ]
                        ];

                        $sheet->getStyle("A1:Z1")->applyFromArray($style);
                        $sheet->setHeight(1,30);

                        $style = [
                             'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold' => false
                            ]
                        ];

                        $sheet->getDefaultStyle()->applyFromArray($style);
                        $sheet->fromArray($data);
                    });
                })
                    ->store('xlsx', storage_path('tmp/'));
                return response()->json([
                    'download_token' => $token,
                ]);

            }else{
                return response()->json(['status'=>false]);
            }
        }

        return response()->json(['cases'  => $result['list'] ,'total'=> $result['total']]);
    }

    // save payments details
    public function store(Request $request)
    {
        $this->authorize('create',Payments::class);

        $user = \Auth::user();

        if($request->organization_share != null && $request->organization_share != null ){
            $ancestor_id = \Organization\Model\Organization::getAncestor($user->organization_id);
            $row=\DB::table('char_settings')
                ->where('char_settings.id','=','max_organization_share')
                ->where('char_settings.organization_id',$ancestor_id)
                ->first();

            if($row){
                if($row->value < $request->organization_share){
                    return response()->json(array(
                        "status" => 'failed',
                        "msg" => trans('sponsorship::application.The entered organization share is exceeded the max of organization share')
                    ));
                }
            }
        }


        $error = Helpers::isValid($request->all(),Payments::getValidatorRules());
        if($error)
            return response()->json($error);

        $payment = new Payments();
        $payment->organization_id =$user->organization_id;
        $payment->bank_id =null;
        $payment->sponsorship_id =null;
        $fillable = Payments::getFields();
        $dates = ['payment_date' , 'exchange_date'];

        foreach ($fillable as $col){
            if(isset($request->$col)){
                if(in_array($col,$dates)){
                    if(!is_null($request->$col) && $request->$col != " " && $request->$col != ""){
                        $payment->$col =date('Y-m-d',strtotime($request->$col));
                    }
                }
                else{
                    $payment->$col =  $request->$col;
                }
            }
        }

        if($payment->save()) {
            \Log\Model\Log::saveNewLog('PAYMENTS_CREATED',trans('sponsorship::application.Added new payment'));
            return response()->json(array(
                "status" => 'success',
                "payment_id" => $payment->id,
                "msg" => trans('sponsorship::application.The row is inserted to db')
            ));
        }
        return response()->json(array(
            "status" => 'failed',
            "msg" => trans('sponsorship::application.The row is not insert to db')
        ));
    }

    // duplicate payment details to another payment
    public function duplicate(Request $request)
    {

        $this->authorize('create',Payments::class);

        $response = array();
        $user = \Auth::user();

        $id = $request->payments_id;
        $payment =Payments::findorfail($id);

        if($payment->organization_share != null && $payment->organization_share!= null ){
            $ancestor_id = \Organization\Model\Organization::getAncestor($user->organization_id);
            $row=\DB::table('char_settings')
                ->where('char_settings.id','=','max_organization_share')
                ->where('char_settings.organization_id',$ancestor_id)
                ->first();

            if($row){
                if($row->value < $payment->organization_share){
                    $response["status"] = 'failed';
                    $response["msg"]= trans('sponsorship::application.The entered organization share is exceeded the max of organization share');

                }
            }
        }

        $payment_in = $payment;

        $payment_new = new Payments();
        $payment_new->organization_id =$user->organization_id;
        $payment_new->bank_id =null;
        $payment_new->sponsorship_id =null;

        $fillable = Payments::getFields();
        $dates = ['payment_date' , 'exchange_date'];

        foreach ($fillable as $col){
            if(isset($payment->$col)){
                if(in_array($col,$dates)){
                    if(!is_null($payment->$col) && $payment->$col != " " && $payment->$col != ""){
                        $payment_new->$col =date('Y-m-d',strtotime($payment->$col));
                    }
                }
                else{
                    $payment_new->$col =  $payment->$col;
                }
            }
        }



        if($payment_new->save()) {

            if ($request->check){
                $resultCase = PaymentsCases::where('payment_id',$id)->get();
                if (!empty($resultCase)) {
                    foreach ($resultCase as $value) {
                        $value->payment_id = $payment_new->id ;
                        $value = $value->toArray();
                        PaymentsCases::create($value);
                    }
                }

                $resultRec = PaymentsRecipient::where('payment_id',$id)->get() ;
                if (!empty($resultRec)) {
                    foreach ($resultRec as $value) {
                        $value->payment_id = $payment_new->id ;
                        $value = $value->toArray();
                        PaymentsRecipient::create($value);

                    }
                }

            }

            \Log\Model\Log::saveNewLog('PAYMENTS_CREATED',trans('sponsorship::application.Added new payment'));
            return response()->json(array(
                "status" => 'success',
                "payment_id" => $payment_new,
                "msg" => trans('sponsorship::application.The row is inserted to db')
            ));
        }
        return response()->json(array(
            "status" => 'failed',
            "msg" => trans('sponsorship::application.The row is not insert to db')
        ));
    }

    // import cases to payment using xlsx sheet
    public function importCases(Request $request){


        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $validFile=true;

        $payment_id = $request->get('payment_id');
        $payment = Payments::where('id',$payment_id)->first();

        $sponsor_id=$payment->sponsor_id;
        $Sponsorships_id=[];

        $Sponsorships=Sponsorships::where('sponsor_id',$sponsor_id)->get();
        foreach( $Sponsorships as $k=>$v ) {
            $Sponsorships_id[]=$v->id;
        }

        $cases=[];
        $recipients=[];
        $manipulated_sponsor_number=[];
        $manipulated_id_card_number=[];
        $manipulated_recipients=[];
        $manipulated_cases=[];
        $manipulated=0;
        $duplicated=0;
        $invalid=0;
        $not_saved_sponsor_number=0;
        $not_saved_id_card_number=0;
        $old=[];
        $new=[];

        $importFile=$request->file ;
        if ($importFile->isValid()) {

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=\Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 )
                    {
                        $constraint =  ["rkm_alhoy", "rkm_alkfal", "bday_ftr_alsrf", "nhay_ftr_alsrf", "almblgh", "almblgh_alshykl"];

                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }
                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);
                            if ($total > 0)
                            {
                                $user = \Auth::user();
                                foreach ($records as $key =>$value) {

                                    $sponsor_number = $value->rkm_alkfal;
                                    if (!is_null($sponsor_number) && $sponsor_number !=  " " && $sponsor_number != '') {
                                        $nominatedCases=SponsorshipCases::where(['sponsor_number' => $sponsor_number ,
                                            'sponsor_id' => $payment->sponsor_id, 'status' => 3])->first();

                                        if(!in_array($sponsor_number,$manipulated_sponsor_number)){
                                            $manipulated_sponsor_number[] = $sponsor_number;
                                            if($nominatedCases){
                                                $case=SponsorshipCases::getRecord('sponsor_number',$sponsor_number, $payment_id,$payment->bank_id,$payment->payment_target,$payment->exchange_type,$sponsor_id);

                                                if(sizeof($case) > 0){
                                                    if(!in_array($value->case_id,$manipulated_cases)){
                                                        $manipulated_cases[] = $case[0]->case_id;
                                                        if( ($case[0]->payment_case_id == null && $case[0]->payments == 0) ||
                                                            ($case[0]->payment_case_id == null && $case[0]->payments != 0) ) {

                                                            $target_case=[ 'payment_id' => $payment_id,
                                                                'case_id' => $case[0]->case_id,
                                                                'guardian_id' => $case[0]->guardian_id,
                                                                'sponsor_number' => $sponsor_number, 'status' => 1];


                                                            if($case[0]->payments != 0){
                                                                $target_case['status']= 2;
                                                            }
                                                            if(is_null($case[0]->guardian_id)){
                                                                $target_case['recipient']=0;
                                                            }else{
                                                                if($case[0]->guardian_id == $case[0]->person_id) {
                                                                    $target_case['recipient']=0;
                                                                }else{
                                                                    $target_case['recipient']=1;
                                                                }
                                                            }

                                                            $amount=0;
                                                            if (!is_null($value->almblgh) && $value->almblgh !=  " " && $value->almblgh != '') {
                                                                $amount = $value->almblgh;
                                                            }

                                                            $target_case['amount']=$amount."";
//                                                            +$payment->administration_fees
                                                            $target_case['amount_after_discount']= $amount - (($amount * $payment->organization_share))."";
                                                            $target_case['shekel_amount_before_discount']= $amount * $payment->exchange_rate."";
//                                                            + $payment->administration_fees
                                                            $target_case['shekel_amount']= round(( $amount - (($amount * $payment->organization_share))) *
                                                                    $payment->exchange_rate)."";


                                                            //                                        if(in_array("shekel_amount", $map)) {
                                                            //                                            $shekel_amount=0;
                                                            //                                            $refShekelAmount=$header[$target['amount']];
                                                            //
                                                            //                                            if(!is_null($value->$refShekelAmount)){
                                                            //                                                $shekel_amount=$value->$refShekelAmount;
                                                            //                                            }
                                                            //                                            $target_case['shekel_amount']=$shekel_amount;
                                                            //                                        }

                                                            if (!is_null($value->bday_ftr_alsrf) && $value->bday_ftr_alsrf !=  " " && $value->bday_ftr_alsrf != '') {
                                                                $target_case['date_from']=date('Y-m-d',strtotime($value->bday_ftr_alsrf));
                                                            }

                                                            if (!is_null($value->nhay_ftr_alsrf) && $value->nhay_ftr_alsrf !=  " " && $value->nhay_ftr_alsrf != '') {
                                                                $target_case['date_to']=date('Y-m-d',strtotime($value->nhay_ftr_alsrf));
                                                            }

                                                            if($case[0]->payments == 0){
                                                                $new[]=$case[0]->case_id;
                                                            }else{
                                                                $target_case['status']= 2;
                                                                $old[]=$case[0]->case_id;
                                                            }

                                                            $cases[]=$target_case;
                                                        }
                                                        else if( ($case[0]->payment_case_id != null && $case[0]->payments == 0) || ($case[0]->payment_case_id != null && $case[0]->payments != 0) ){
                                                            if($case[0]->payments == 0){
                                                                $new[]=$case[0]->case_id;
                                                                $updated= ['status' => 1];
                                                            }else{
                                                                $updated= ['status' => 2];
                                                                $old[]=$case[0]->case_id;
                                                            }
                                                            PaymentsCases::where([ 'payment_id' => $payment_id, 'case_id' => $case[0]->case_id,'sponsor_number' => $case[0]->sponsor_number])->update($updated);
                                                        }

                                                    }else{
                                                        $duplicated++;
                                                    }
                                                }else{
                                                    $invalid ++;
                                                }

                                            }
                                            else{
                                                $not_saved_sponsor_number ++;
                                            }
                                        }else{
                                            $duplicated++;
                                        }
                                        $manipulated++;
                                    }else{

                                        $id_card_number = $value->rkm_alhoy;

                                        if (!is_null($id_card_number) && $id_card_number !=  " " && $id_card_number != '') {
                                            if(!in_array($id_card_number,$manipulated_id_card_number)){
                                                $manipulated_id_card_number[] = $id_card_number;
                                                $exist_person=\Common\Model\Person::where(function ($q) use ($id_card_number){
                                                                                        $q->where('id_card_number',$id_card_number);
                                                                                  })
                                                                                  ->first();
                                                if($exist_person != null){
                                                    $Records=SponsorshipCases::getRecord('id_card_number',$exist_person->id, $payment_id,$payment->bank_id,$payment->payment_target,$payment->exchange_type,$sponsor_id);
                                                    foreach ($Records as $kk => $vv) {
                                                        if(!in_array($vv->sponsor_number,$manipulated_sponsor_number)){
                                                            $manipulated_sponsor_number[] = $vv->sponsor_number;


//                                            if(!in_array($vv->case_id,$manipulated_cases)){
//                                                $manipulated_cases[] = $vv->case_id;
                                                            if( ($vv->payment_case_id == null && $vv->payments == 0) || ($vv->payment_case_id == null && $vv->payments != 0) ) {

                                                                $target_case=[ 'payment_id' => $payment_id, 'case_id' => $vv->case_id, 'guardian_id' => $vv->guardian_id,
                                                                    'sponsor_number' => $vv->sponsor_number, 'status' => 1 ];

                                                                if(is_null($vv->guardian_id)){
                                                                    $target_case['recipient']=0;
                                                                }else{
                                                                    if($vv->guardian_id == $vv->person_id) {
                                                                        $target_case['recipient']=0;
                                                                    }else{
                                                                        $target_case['recipient']=1;
                                                                    }
                                                                }


                                                                $amount=0;
                                                                if (!is_null($value->almblgh) && $value->almblgh !=  " " && $value->almblgh != '') {
                                                                    $amount = $value->almblgh;
                                                                }

                                                                $target_case['amount']=$amount."";
//                                                                +$payment->administration_fees
                                                                $target_case['amount_after_discount']= $amount - (($amount * $payment->organization_share))."";
                                                                $target_case['shekel_amount_before_discount']= $amount * $payment->exchange_rate."";
//                                                                +$payment->administration_fees
                                                                $target_case['shekel_amount']= round(( $amount - (($amount * $payment->organization_share))) *
                                                                        $payment->exchange_rate)."";


                                                                //                                        if(in_array("shekel_amount", $map)) {
                                                                //                                            $shekel_amount=0;
                                                                //                                            $refShekelAmount=$header[$target['amount']];
                                                                //
                                                                //                                            if(!is_null($value->$refShekelAmount)){
                                                                //                                                $shekel_amount=$value->$refShekelAmount;
                                                                //                                            }
                                                                //                                            $target_case['shekel_amount']=$shekel_amount;
                                                                //                                        }

                                                                if (!is_null($value->bday_ftr_alsrf) && $value->bday_ftr_alsrf !=  " " && $value->bday_ftr_alsrf != '') {
                                                                    $target_case['date_from']=date('Y-m-d',strtotime($value->bday_ftr_alsrf));
                                                                }

                                                                if (!is_null($value->nhay_ftr_alsrf) && $value->nhay_ftr_alsrf !=  " " && $value->nhay_ftr_alsrf != '') {
                                                                    $target_case['date_to']=date('Y-m-d',strtotime($value->nhay_ftr_alsrf));
                                                                }

                                                                if($vv->payments == 0){
                                                                    $new[]=$vv->case_id;
                                                                }else{
                                                                    $target_case['status']= 2;
                                                                    $old[]=$vv->case_id;
                                                                }

                                                                $cases[]=$target_case;

                                                            }else if( ($vv->payment_case_id != null && $vv->payments == 0) || ($vv->payment_case_id != null && $vv->payments != 0) ){
                                                                if($vv->payments == 0){
                                                                    $new[]=$vv->case_id;
                                                                    $updated= ['status' => 1];
                                                                }else{
                                                                    $updated= ['status' => 2];
                                                                    $old[]=$vv->case_id;
                                                                }
                                                                PaymentsCases::where([ 'payment_id' => $payment_id, 'case_id' => $vv->case_id,'sponsor_number' => $vv->sponsor_number])->update($updated);
                                                            }
//                                            }else{
//                                                $duplicated++;
//                                            }

                                                        }else{
                                                            $duplicated++;
                                                        }
                                                    }
                                                }else{
                                                    $not_saved_id_card_number++;
                                                }
                                            }else{
                                                $duplicated++;
                                            }
                                            $manipulated++;
                                        }
                                    }


                                }

                                $restricted= $not_saved_id_card_number+$not_saved_sponsor_number+$duplicated;

                                $inactive=$new;
                                foreach ($old as $key => $value) {
                                    $inactive[]= $value;
                                }

                                if($total == $restricted){
                                    if($restricted != 0 && $restricted == $not_saved_sponsor_number){
                                        return response()->json(['status' => false,
                                            'msg' => trans('sponsorship::application.All sponsorship numbers (nomination codes) entered are not registered or not guaranteed by the sponsor specified for exchange')]);
                                    }

                                    if($restricted != 0 && $restricted == $not_saved_id_card_number){
                                        return response()->json(['status' => false, 'msg' => trans('sponsorship::application.All ID numbers entered are not registered in the database')]);
                                    }

                                    if($restricted != 0 && $restricted == $manipulated){
                                        return response()->json(['status' => false, 'msg' => trans('sponsorship::application.All numbers entered are either not registered in the database or are not guaranteed by the sponsor') ]);
                                    }
                                }
                                else{


                                    if (!empty($cases)) {
                                        foreach ($cases as $key => $value) {
                                            PaymentsCases::create($value);
                                            \Log\Model\Log::saveNewLog('PAYMENTS_CASES_CREATED',trans('sponsorship::application.added new beneficiaries to the payment'));
                                        }
                                    }

                                    $inactive_payments_case_cnt=SponsorshipCases::getInactive($inactive,$payment_id,$payment->sponsor_id,$payment->organization_id);

                                    $rows =PaymentsRecipient::getRows($payment_id,0,$payment->bank_id,$payment->exchange_type);
                                    $rows->map(function ($subQuery) use($payment_id){

                                        $subQuery->amount =0;
                                        $subQuery->amount_after_discount =0;
                                        $subQuery->shekel_amount =0;
                                        $subQuery->shekel_amount_before_discount =0;

                                        $id = $subQuery->person_id;

                                        $total= \DB::select("select SUM(`amount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $payment_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))  ");

                                        if($total){ $subQuery->amount=$total[0]->amount.""; }

                                        $amount_after_discount= \DB::select("select SUM(`amount_after_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $payment_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))  ");

                                        if($amount_after_discount){$subQuery->amount_after_discount =$amount_after_discount[0]->amount."";}

                                        $total_shekel_amount= \DB::select("select SUM(`shekel_amount`) as amount  FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id` = $payment_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))   ");

                                        if($total_shekel_amount){$subQuery->shekel_amount=$total_shekel_amount[0]->amount."";}

                                        $shekel_amount_before_discount= \DB::select("select SUM(`shekel_amount_before_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $payment_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))  ");

                                        if($shekel_amount_before_discount){
                                            $subQuery->shekel_amount_before_discount =$shekel_amount_before_discount[0]->amount."";
                                        }

                                        return $subQuery;
                                    });

                                    foreach($rows as $k=>$v){
                                        if (PaymentsRecipient::where(['payment_id'=>$payment_id,'person_id'=>$v->person_id,'father_id'=>$v->father_id,'mother_id'=>$v->mother_id])->exists()) {
                                            PaymentsRecipient::where(['payment_id'=>$payment_id,'person_id'=>$v->person_id,'father_id'=>$v->father_id,'mother_id'=>$v->mother_id])
                                                ->update([
                                                    'amount'=>$v->amount."",
                                                    'amount_after_discount'=> ($v->amount - ($v->amount * $payment->organization_share))."",
                                                    'shekel_amount_before_discount'=>($v->amount * $payment->exchange_rate)."",
                                                    'shekel_amount'=>round( ( $v->amount - ($v->amount * $payment->organization_share)) * $payment->exchange_rate).""
                                                ]);
                                        }
                                        else{

                                            $v->amount_after_discount =($v->amount - ($v->amount * $payment->organization_share))."";
                                            $v->shekel_amount_before_discount  = ($v->amount * $payment->exchange_rate)."";
                                            $v->shekel_amount  =  round( ( $v->amount - ($v->amount * $payment->organization_share)) * $payment->exchange_rate)."";

                                            if($v->father_id ==null){
                                                unset($v->father_id);
                                            }

                                            if($v->mother_id ==null){
                                                unset($v->mother_id);
                                            }

                                            if($payment->exchange_type ==3){
                                                unset($v->bank_id);
                                                unset($v->cheque_account_number);
                                            }


                                            $item=(array)$v;
                                            $item['status']=0;
                                            $item['payment_id']=$payment_id;
                                            PaymentsRecipient::create($item);
                                        }
                                    }
                                    $rows =PaymentsRecipient::getRows($payment_id,1,$payment->bank_id,$payment->exchange_type);
                                    $rows->map(function ($subQuery) use($payment_id){

                                        $subQuery->amount =0;
                                        $subQuery->amount_after_discount =0;
                                        $subQuery->shekel_amount =0;
                                        $subQuery->shekel_amount_before_discount =0;

                                        $id = $subQuery->person_id;

                                        $total= \DB::select("select SUM(`amount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $payment_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))  ");

                                        if($total){ $subQuery->amount=$total[0]->amount.""; }

                                        $amount_after_discount= \DB::select("select SUM(`amount_after_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $payment_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))  ");

                                        if($amount_after_discount){$subQuery->amount_after_discount =$amount_after_discount[0]->amount."";}

                                        $total_shekel_amount= \DB::select("select SUM(`shekel_amount`) as amount  FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id` = $payment_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))   ");

                                        if($total_shekel_amount){$subQuery->shekel_amount=$total_shekel_amount[0]->amount."";}

                                        $shekel_amount_before_discount= \DB::select("select SUM(`shekel_amount_before_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $payment_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))  ");

                                        if($shekel_amount_before_discount){
                                            $subQuery->shekel_amount_before_discount =$shekel_amount_before_discount[0]->amount."";
                                        }

                                        return $subQuery;
                                    });

                                    foreach($rows as $k=>$v){
                                        if (PaymentsRecipient::where(['payment_id'=>$payment_id,'person_id'=>$v->person_id,'father_id'=>$v->father_id,'mother_id'=>$v->mother_id])->exists()) {
                                            PaymentsRecipient::where(['payment_id'=>$payment_id,'person_id'=>$v->person_id,'father_id'=>$v->father_id,'mother_id'=>$v->mother_id])
                                                ->update([
                                                    'amount'=>$v->amount."",
                                                    'amount_after_discount'=> ($v->amount - ($v->amount * $payment->organization_share))."",
                                                    'shekel_amount_before_discount'=>($v->amount * $payment->exchange_rate)."",
                                                    'shekel_amount'=>round( ( $v->amount - ($v->amount * $payment->organization_share)) * $payment->exchange_rate).""
                                                ]);

                                        }else{

                                            $v->amount_after_discount =($v->amount - ($v->amount * $payment->organization_share))."";
                                            $v->shekel_amount_before_discount  = ($v->amount * $payment->exchange_rate)."";
                                            $v->shekel_amount  =  round( ( $v->amount - ($v->amount * $payment->organization_share)) * $payment->exchange_rate)."";

                                            if($v->father_id ==null){
                                                unset($v->father_id);
                                            }

                                            if($v->mother_id ==null){
                                                unset($v->mother_id);
                                            }

                                            if($payment->exchange_type ==3){
                                                unset($v->bank_id);
                                                unset($v->cheque_account_number);
                                            }


                                            $item=(array)$v;
                                            $item['status']=0;
                                            $item['payment_id']=$payment_id;
                                            PaymentsRecipient::create($item);
                                        }
                                    }

                                    $max_allowed_amount = $payment->amount - ($payment->amount * $payment->organization_share) - $payment->administration_fees;
                                    $response['total']=PaymentsCases::where(["payment_id"=>$payment_id])->sum('amount');

                                    $response['status']=true;
                                    $response['exceed']=false;
                                    $response['msg']=
                                        trans('sponsorship::application.The row is inserted to db') .' , '.
                                        trans('sponsorship::application.total in file') .' : '.  $manipulated.' , '.
                                        trans('sponsorship::application.not_saved_id_card_number') .' : '. $not_saved_id_card_number.' , '.
                                        trans('sponsorship::application.not_saved_sponsor_number') .' : '. $not_saved_sponsor_number.' , '.
                                        trans('sponsorship::application.duplicated') .' : '. $duplicated.' , '.
                                        trans('sponsorship::application.new_cases') .' : '. PaymentsCases::where(["payment_id"=>$payment_id,'status'=>1])->count().' , '.
                                        trans('sponsorship::application.old_cases') .' : '. PaymentsCases::where(["payment_id"=>$payment_id,'status'=>2])->count().' , '.
                                        trans('sponsorship::application.inactive_cases') . ' : '.PaymentsCases::where(["payment_id"=>$payment_id,'status'=>3])->count() ;

                                    if($max_allowed_amount < $response['total']){
                                        $response['exceed']=true;
                                        $response['msg'] = $response['msg'] .' , '. trans('sponsorship::application.The amount exceed the allowed amount') ;
                                    }

                                }
                                return response()->json($response);

                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);

                    }
                }

                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);

            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }

    // save payments details and  import cases  external href to create from file
    public function importPaymentsCases(Request $request){


        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $validFile=true;

        $cases=[];
        $recipients=[];
        $manipulated_sponsor_number=[];
        $manipulated_id_card_number=[];
        $manipulated_recipients=[];
        $manipulated_cases=[];
        $manipulated=0;
        $duplicated=0;
        $not_saved_sponsor_number=0;
        $not_saved_id_card_number=0;
        $old=[];
        $new=[];

        $categories = [];
        $catPayment = [];
        $not_allowed_category_numbers = 0;
        $not_allowed_category = 0;
        $allowed_category = [];

        $importFile=$request->file ;
        if ($importFile->isValid()) {

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();

            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=\Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();

                    if(sizeof($keys) >0 )
                    {
                        $constraint =  ["rkm_alhoy", "rkm_alkfal", "bday_ftr_alsrf", "nhay_ftr_alsrf", "almblgh", "almblgh_alshykl" , "tsnyf_alsrfy"];

                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }
                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);
                            if ($total > 0) {

                                $attributes=$request->all();
                                $payment=[];
                                $inputs = ['sponsorship_id','category_id','sponsor_id','organization_id','payment_date','amount','currency_id','payment_exchange','bank_id','payment_target'
                                    ,'exchange_rate','exchange_date','organization_share','administration_fees','status','exchange_type','transfer','transfer_company_id'];

                                foreach ($attributes as $key => $value) {
                                    if ( $value != "" ) {
                                        if(in_array($key, $inputs)) {
                                            if ($key =='exchange_date' || $key =='payment_date') {
                                                $payment[$key] = date('Y-m-d',strtotime($value));
                                            }else{
                                                $payment[$key]=$value;
                                            }
                                        }
                                    }
                                }

                                if($payment['transfer'] != 1 || $payment['transfer'] != '1'){
                                    $payment['transfer_company_id']=null;

                                }
                                foreach ($records as $key => $value) {
                                    if($value->tsnyf_alsrfy){
                                        if(!is_null($value->tsnyf_alsrfy) && $value->tsnyf_alsrfy !=''){
                                            if(!in_array($value->tsnyf_alsrfy,$categories)){
                                                $categories[] = $value->tsnyf_alsrfy;
                                                $category_id=\Common\Model\CaseModel::constantId('payment_category',$value->tsnyf_alsrfy,null,null);
                                                if(!is_null($category_id)){
                                                    $allowed_category[]=$value->tsnyf_alsrfy;
                                                    $catPayment[$value->tsnyf_alsrfy] =['id'=>$category_id,'cases'=>[],'manipulated'=>[],'manipulated_sponsor_number'=>[]] ;
                                                }else{
                                                    $not_allowed_category++;
                                                }
                                            }
                                        }
                                    }
                                }

                                if(sizeof($allowed_category) > 0){
                                    $bank_id=null;
                                    $exchange_type=null;
                                    $sponsor_id=null;

                                    if(isset($payment['bank_id'])){
                                        $bank_id=$payment['bank_id'];
                                    }
                                    if(isset($payment['exchange_type'])){
                                        $exchange_type=$payment['exchange_type'];
                                    }
                                    if(isset($payment['sponsor_id'])){
                                        $sponsor_id=$payment['sponsor_id'];
                                    }

                                    foreach ($records as $key => $value) {
                                        $ref=null;

                                        if(in_array($value->tsnyf_alsrfy , $allowed_category)) {
                                            $sponsor_number = $value->rkm_alkfal;
                                            if (!is_null($sponsor_number) && $sponsor_number !=  " " && $sponsor_number != '') {
                                                $nominatedCases=SponsorshipCases::where(['sponsor_number' => $sponsor_number ,
                                                    'sponsor_id' => $sponsor_id, 'status' => 3])->first();
                                                if(!in_array($sponsor_number,$catPayment[$value->tsnyf_alsrfy]['manipulated_sponsor_number'])){
                                                    $manipulated_sponsor_number[] = $sponsor_number;
                                                    if($nominatedCases){
                                                        $case=SponsorshipCases::getNominated($sponsor_number,$bank_id,$exchange_type,$sponsor_id);
//                                        if(!in_array($value->case_id,$catPayment[$value->$catRef]['manipulated'])){
                                                        $catPayment[$value->tsnyf_alsrfy]['manipulated'][]= $case->case_id;

                                                        $amount=0;
                                                        if (!is_null($value->bday_ftr_alsrf) && $value->bday_ftr_alsrf !=  " " && $value->bday_ftr_alsrf != '') {
                                                            $target_case['date_from']=date('Y-m-d',strtotime($value->bday_ftr_alsrf));
                                                        }

                                                        if (!is_null($value->nhay_ftr_alsrf) && $value->nhay_ftr_alsrf !=  " " && $value->nhay_ftr_alsrf != '') {
                                                            $target_case['date_to']=date('Y-m-d',strtotime($value->nhay_ftr_alsrf));
                                                        }

                                                        if (!is_null($value->almblgh) && $value->almblgh !=  " " && $value->almblgh != '') {
                                                            $amount = $value->almblgh;
                                                        }

                                                        if($amount != 0 && $amount != '0' ){
                                                            $target_case=['case_id' => $case->case_id,'recipient' => $case->recipient, 'guardian_id' => $case->guardian_id,
                                                                'sponsor_number' => $case->sponsor_number, 'status' => 1];


                                                            if($case->payments_count != 0){
                                                                $target_case['status']= 2;
                                                            }

                                                            $target_case['amount']=$amount."";
                                                            $target_case['amount_after_discount']=($amount - ($amount * $payment['organization_share']))."";
                                                            $target_case['shekel_amount_before_discount']= ($amount * $payment['exchange_rate'])."";
                                                            $target_case['shekel_amount']=round(($amount - ($amount * $payment['organization_share'])) * $payment['exchange_rate'])."";

                                                            $catPayment[$value->tsnyf_alsrfy]['cases'][]=$target_case;

                                                        }


//                                        }
//                                        else{
//                                            $duplicated++;
//                                        }
                                                    }
                                                    else{
                                                        $not_saved_sponsor_number ++;
                                                    }
                                                }else{
                                                    $duplicated++;
                                                }
                                                $manipulated++;
                                            }
                                        }else{
                                            $not_allowed_category_numbers++;
                                        }
                                    }
                                }else{
                                    return response()->json(['status' => false, 'msg' => trans('sponsorship::application.All sponsorship type is not registered in the system')]);
                                }

                                $restricted = $not_allowed_category_numbers  +  $not_saved_sponsor_number + $not_saved_id_card_number;

                                if($total == $restricted){
                                    if($restricted != 0 && $restricted == $not_allowed_category_numbers){
                                        return response()->json(['status' => false,
                                            'msg' =>  trans('sponsorship::application.All sponsorship type is not registered in the system') ]);
                                    }

                                    if($restricted != 0 && $restricted == $not_saved_sponsor_number){
                                        return response()->json(['status' => false,
                                            'msg' => trans('sponsorship::application.All sponsorship numbers (nomination codes) entered into a banking class not registered in the system')]);
                                    }

                                    if($restricted != 0 && $restricted == $not_saved_id_card_number){
                                        return response()->json(['status' => false, 'msg' => trans('sponsorship::application.All ID numbers entered are not registered in the database')]);
                                    }

                                    if($restricted != 0 && $restricted == $manipulated){
                                        return response()->json(['status' => false, 'msg' =>trans('sponsorship::application.All numbers entered are either not registered in the database or are not guaranteed by the sponsor')
                                        ]);
                                    }
                                }
                                else{

//                return [$catPayment,$not_allowed_category_numbers,$manipulated,$duplicated,$not_saved_sponsor_number];
                                    foreach ($catPayment as $key => $value) {
                                        $payment['category_id']=$value['id'];
                                        $user = \Auth::user();
                                        $payment['organization_id']=$user->organization_id;
                                        $inserted= Payments::create($payment);
                                        $inserted_id=$inserted->id;
                                        $ids[]=$inserted->id;
                                        if (!empty($value['cases'])) {
                                            foreach ($value['cases'] as $k => $v) {
                                                $v['payment_id'] = $inserted->id;

                                                $exist = PaymentsCases::where(['payment_id'=> $inserted_id , 'case_id' => $v['case_id']])->first();
                                                if (is_null($exist)) {
                                                    PaymentsCases::create($v);
                                                }
                                            }
                                            Payments::where('id', $inserted_id)->update(['amount' => PaymentsCases::where('payment_id', $inserted_id)->sum('amount').""]);
                                            $inactive=[];
                                            $old = PaymentsCases::where('payment_id', $inserted_id)->get(['case_id']);
                                            foreach ($old as $k => $v) {
                                                $inactive[]=$v->case_id;
                                            }

                                            SponsorshipCases::getInactive($inactive,$inserted_id,$inserted->sponsor_id,$inserted->organization_id);

                                            $rows = PaymentsRecipient::getRows($inserted->id, 0, $inserted->bank_id, $inserted->exchange_type);
                                            $rows->map(function ($subQuery) use ($inserted_id) {

                                                $subQuery->amount = 0;
                                                $subQuery->amount_after_discount = 0;
                                                $subQuery->shekel_amount = 0;
                                                $subQuery->shekel_amount_before_discount = 0;

                                                $id = $subQuery->person_id;

                                                $total = \DB::select("select SUM(`amount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))  ");

                                                if ($total) {
                                                    $subQuery->amount = $total[0]->amount."";
                                                }

                                                $amount_after_discount = \DB::select("select SUM(`amount_after_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))  ");

                                                if ($amount_after_discount) {
                                                    $subQuery->amount_after_discount = $total[0]->amount."";
                                                }
                                                if ($amount_after_discount) {
                                                    $subQuery->amount_after_discount = $total[0]->amount."";
                                                }

                                                $total_shekel_amount = \DB::select("select SUM(`shekel_amount`) as amount  FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id` = $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))   ");

                                                if ($total_shekel_amount) {
                                                    $subQuery->shekel_amount = $total_shekel_amount[0]->amount."";
                                                }

                                                $shekel_amount_before_discount = \DB::select("select SUM(`shekel_amount_before_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))  ");

                                                if ($shekel_amount_before_discount) {
                                                    $subQuery->shekel_amount_before_discount_amount = $shekel_amount_before_discount[0]->amount."";
                                                }

                                                return $subQuery;
                                            });

                                            foreach ($rows as $k => $v) {
                                                if (PaymentsRecipient::where(['payment_id' => $inserted_id, 'person_id' => $v->person_id, 'father_id' => $v->father_id, 'mother_id' => $v->mother_id])->exists()) {
                                                    PaymentsRecipient::where(['payment_id' => $inserted_id, 'person_id' => $v->person_id, 'father_id' => $v->father_id, 'mother_id' => $v->mother_id])
                                                        ->update([
                                                            'amount' => $v->amount."",
                                                            'amount_after_discount' => ($v->amount - ($v->amount * $inserted->organization_share))."",
                                                            'shekel_amount_before_discount' => ($v->amount * $inserted->exchange_rate)."",
                                                            'shekel_amount' => round(($v->amount - ($v->amount * $inserted->organization_share)) * $inserted->exchange_rate).""
                                                        ]);
                                                } else {

                                                    $v->amount_after_discount = ($v->amount - ($v->amount * $inserted->organization_share)."");
                                                    $v->shekel_amount_before_discount = ($v->amount * $inserted->exchange_rate)."";
                                                    $v->shekel_amount = round(($v->amount - ($v->amount * $inserted->organization_share)) * $inserted->exchange_rate)."";

                                                    if ($v->father_id == null) {
                                                        unset($v->father_id);
                                                    }

                                                    if ($v->mother_id == null) {
                                                        unset($v->mother_id);
                                                    }

                                                    if ($inserted->exchange_type == 3) {
                                                        unset($v->bank_id);
                                                        unset($v->cheque_account_number);
                                                    }


                                                    $item = (array)$v;
                                                    $item['status'] = 0;
                                                    $item['payment_id'] = $inserted_id;
                                                    PaymentsRecipient::create($item);
                                                }
                                            }
                                            $rows = PaymentsRecipient::getRows($inserted->id, 1, $inserted->bank_id, $inserted->exchange_type);
                                            $rows->map(function ($subQuery) use ($inserted_id) {

                                                $subQuery->amount = 0;
                                                $subQuery->amount_after_discount = 0;
                                                $subQuery->shekel_amount = 0;
                                                $subQuery->shekel_amount_before_discount = 0;

                                                $id = $subQuery->person_id;

                                                $total = \DB::select("select SUM(`amount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))  ");

                                                if ($total) {
                                                    $subQuery->amount = $total[0]->amount."";
                                                }

                                                $amount_after_discount = \DB::select("select SUM(`amount_after_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))  ");

                                                if ($amount_after_discount) {
                                                    $subQuery->amount_after_discount = $amount_after_discount[0]->amount."";
                                                }

                                                $total_shekel_amount = \DB::select("select SUM(`shekel_amount`) as amount  FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id` = $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))   ");

                                                if ($total_shekel_amount) {
                                                    $subQuery->shekel_amount = $total_shekel_amount[0]->amount."";
                                                }

                                                $shekel_amount_before_discount = \DB::select("select SUM(`shekel_amount_before_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))  ");

                                                if ($shekel_amount_before_discount) {
                                                    $subQuery->shekel_amount_before_discount_amount = $shekel_amount_before_discount[0]->amount."";
                                                }

                                                return $subQuery;
                                            });

                                            foreach ($rows as $k => $v) {
                                                if (PaymentsRecipient::where(['payment_id' => $inserted_id, 'person_id' => $v->person_id, 'father_id' => $v->father_id, 'mother_id' => $v->mother_id])->exists()) {
                                                    PaymentsRecipient::where(['payment_id' => $inserted_id, 'person_id' => $v->person_id, 'father_id' => $v->father_id, 'mother_id' => $v->mother_id])
                                                        ->update([
                                                            'amount' => $v->amount,
                                                            'amount_after_discount' => ($v->amount - ($v->amount * $inserted->organization_share))."",
                                                            'shekel_amount_before_discount' => ($v->amount * $inserted->exchange_rate)."",
                                                            'shekel_amount' => round(($v->amount - ($v->amount * $inserted->organization_share)) * $inserted->exchange_rate).""
                                                        ]);

                                                } else {

                                                    $v->amount_after_discount = ($v->amount - ($v->amount * $inserted->organization_share))."";
                                                    $v->shekel_amount_before_discount = ($v->amount * $inserted->exchange_rate)."";
                                                    $v->shekel_amount = round(($v->amount - ($v->amount * $inserted->organization_share)) * $inserted->exchange_rate)."";

                                                    if ($v->father_id == null) {
                                                        unset($v->father_id);
                                                    }

                                                    if ($v->mother_id == null) {
                                                        unset($v->mother_id);
                                                    }

                                                    if ($inserted->exchange_type == 3) {
                                                        unset($v->bank_id);
                                                        unset($v->cheque_account_number);
                                                    }


                                                    $item = (array)$v;
                                                    $item['status'] = 0;
                                                    $item['payment_id'] = $inserted_id;
                                                    PaymentsRecipient::create($item);
                                                }
                                            }

                                            \Log\Model\Log::saveNewLog('PAYMENTS_CASES_CREATED',trans('sponsorship::application.added new beneficiaries to the payment'));
                                        }
                                    }


                                    $response['statistic']=Payments::statistic($ids);
                                    $response['status']=true;
                                    $response['msg']=
                                        trans('sponsorship::application.The row is inserted to db') .' , '.
                                        trans('sponsorship::application.total in file') .' : '.  $manipulated.' , '.
                                        trans('sponsorship::application.not_allowed_category_numbers') .' : '. $not_allowed_category_numbers.' , '.
                                        trans('sponsorship::application.not_saved_id_card_number') .' : '. $not_saved_id_card_number.' , '.
                                        trans('sponsorship::application.not_saved_sponsor_number') .' : '. $not_saved_sponsor_number.' , '.
                                        trans('sponsorship::application.duplicated') .' : '. $duplicated;

                                    return response()->json($response);
                                }
                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);

                    }
                }

                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);

            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }

    // import cases to payment using xlsx sheet after mapping (not use upgrade to use dedecated file to export)
    public function doImportPayment(Request $request)
    {

        $this->authorize('create',Payments::class);
        $response = array();
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $column = $request->get('row');
        $response = array();
        $map=[];
        $target=[];
        foreach ($column as $key => $value ) {
            if(isset($value['column']) ){
                if($value['column'] !="" ){
                    array_push($map,$value['column']);
                    $target[$value['column']]=$value['translate'];
                }}
        }

        $path = \Input::file('file')->getRealPath();
        $data = \Excel::selectSheets('data')->load($path, function ($reader) {})->get();

        $cases=[];
        $recipients=[];
        $manipulated_sponsor_number=[];
        $manipulated_id_card_number=[];
        $manipulated_recipients=[];
        $manipulated_cases=[];
        $manipulated=0;
        $duplicated=0;
        $not_saved_sponsor_number=0;
        $not_saved_id_card_number=0;
        $old=[];
        $new=[];
        $all=sizeof($data);
        $header =\Excel::selectSheets('data')->load($path)->first()->keys()->toArray();
        $catRef=null;

        if(isset($target['payment_category'])){
            if(isset($target['payment_category'])){
                $catRef = $header[$target['payment_category']];
            }
        }

        $categories = [];
        $catPayment = [];
        $not_allowed_category_numbers = 0;
        $not_allowed_category = 0;
        $allowed_category = [];

        if (\Input::hasFile('file')) {
            if (!empty($data) && $data->count()) {

                if(!is_null($catRef)){

                    $attributes=$request->all();
                    $payment=[];
                    $inputs = ['sponsorship_id','category_id','sponsor_id','organization_id','payment_date','amount','currency_id','payment_exchange','bank_id','payment_target'
                        ,'exchange_rate','exchange_date','organization_share','administration_fees','status','exchange_type','transfer','transfer_company_id'];

                    foreach ($attributes as $key => $value) {
                        if ( $value != "" ) {
                            if(in_array($key, $inputs)) {
                                if ($key =='exchange_date' || $key =='payment_date') {
                                    $payment[$key] = date('Y-m-d',strtotime($value));
                                }else{
                                    $payment[$key]=$value;
                                }
                            }
                        }
                    }

                    if($payment['transfer'] != 1 || $payment['transfer'] != '1'){
                        $payment['transfer_company_id']=null;

                    }
                    foreach ($data as $key => $value) {
                        if($value->$catRef){
                            if(!is_null($value->$catRef) && $value->$catRef !=''){
                                if(!in_array($value->$catRef,$categories)){
                                    $categories[] = $value->$catRef;
                                    $category_id=\Common\Model\CaseModel::constantId('payment_category',$value->$catRef,null,null);
                                    if(!is_null($category_id)){
                                        $allowed_category[]=$value->$catRef;
                                        $catPayment[$value->$catRef] =['id'=>$category_id,'cases'=>[],'manipulated'=>[],'manipulated_sponsor_number'=>[]] ;
                                    }else{
                                        $not_allowed_category++;
                                    }
                                }
                            }
                        }
                    }

                    if(sizeof($allowed_category) > 0){
                        $bank_id=null;
                        $exchange_type=null;
                        $sponsor_id=null;

                        if(isset($payment['bank_id'])){
                            $bank_id=$payment['bank_id'];
                        }
                        if(isset($payment['exchange_type'])){
                            $exchange_type=$payment['exchange_type'];
                        }
                        if(isset($payment['sponsor_id'])){
                            $sponsor_id=$payment['sponsor_id'];
                        }

                        foreach ($data as $key => $value) {
                            $ref=null;
                            if(isset($target['sponsor_number'])){
                                $ref=$header[$target['sponsor_number']];
                            }

                            if(in_array($value->$catRef , $allowed_category)) {
                                if(in_array("sponsor_number", $map) && $value->$ref !=null) {
                                    $nominatedCases=SponsorshipCases::where(['sponsor_number' => $value->$ref , 'sponsor_id' => $sponsor_id, 'status' => 3])->first();
                                    if(!in_array($value->$ref,$catPayment[$value->$catRef]['manipulated_sponsor_number'])){
                                        $manipulated_sponsor_number[] = $value->$ref;
                                        if($nominatedCases){
                                            $case=SponsorshipCases::getNominated($value->$ref,$bank_id,$exchange_type,$sponsor_id);
//                                        if(!in_array($value->case_id,$catPayment[$value->$catRef]['manipulated'])){
                                            $catPayment[$value->$catRef]['manipulated'][]= $case->case_id;

                                            if(in_array("amount", $map)) {
                                                $amount=0;
                                                $refAmount=$header[$target['amount']];

                                                if(!is_null($value->$refAmount)){
                                                    $amount=$value->$refAmount;
                                                    if($amount != 0 && $amount != '0' ){
                                                        $target_case=['case_id' => $case->case_id,'recipient' => $case->recipient, 'guardian_id' => $case->guardian_id,
                                                            'sponsor_number' => $case->sponsor_number, 'status' => 1];


                                                        if($case->payments_count != 0){
                                                            $target_case['status']= 2;
                                                        }

                                                        $target_case['amount']=$amount."";
                                                        $target_case['amount_after_discount']=($amount - ($amount * $payment['organization_share']))."";
                                                        $target_case['shekel_amount_before_discount']= ($amount * $payment['exchange_rate'])."";
                                                        $target_case['shekel_amount']=round(($amount - ($amount * $payment['organization_share'])) * $payment['exchange_rate'])."";
                                                        if(in_array("date_from", $map)) {
                                                            $refDateFrom=$header[$target['date_from']];
                                                            $target_case['date_from']=date('Y-m-d',strtotime($value->$refDateFrom));
                                                        }

                                                        if(in_array("date_to", $map)) {
                                                            $refDateTo=$header[$target['date_to']];
                                                            $target_case['date_to']=date('Y-m-d',strtotime($value->$refDateTo));
                                                        }

                                                        $catPayment[$value->$catRef]['cases'][]=$target_case;

                                                    }

                                                }
                                            }

//                                        }
//                                        else{
//                                            $duplicated++;
//                                        }
                                        }
                                        else{
                                            $not_saved_sponsor_number ++;
                                        }
                                    }else{
                                        $duplicated++;
                                    }
                                    $manipulated++;
                                }
                            }else{
                                $not_allowed_category_numbers++;
                            }
                        }
                    }else{
                        return response()->json(['status' => false, 'msg' =>trans('sponsorship::application.All sponsorship type is not registered in the system')]);
                    }
                }else{
                    return response()->json(['status' => false, 'msg' =>trans('sponsorship::application.You do not specify the corresponding column for the sponsorship classification in the file')]);

                }
            }
            $restricted = $not_allowed_category_numbers  +  $not_saved_sponsor_number + $not_saved_id_card_number;

            if($all == $restricted){
                if($restricted != 0 && $restricted == $not_allowed_category_numbers){
                    return response()->json(['status' => false,
                        'msg' =>trans('sponsorship::application.All sponsorship numbers (nomination codes) entered into a banking class not registered in the system')]);
                }

                if($restricted != 0 && $restricted == $not_saved_sponsor_number){
                    return response()->json(['status' => false,
                        'msg' =>trans('sponsorship::application.All numbers entered are either not registered in the database or are not guaranteed by the sponsor')]);
                }

                if($restricted != 0 && $restricted == $not_saved_id_card_number){
                    return response()->json(['status' => false, 'msg' =>trans('sponsorship::application.All ID numbers entered are not registered in the database')]);
                }

                if($restricted != 0 && $restricted == $manipulated){
                    return response()->json(['status' => false, 'msg' =>trans('sponsorship::application.All numbers entered are either not registered in the database or are not guaranteed by the sponsor')]);
                }
            }
            else{

//                return [$catPayment,$not_allowed_category_numbers,$manipulated,$duplicated,$not_saved_sponsor_number];
                foreach ($catPayment as $key => $value) {
                    $payment['category_id']=$value['id'];
                    $user = \Auth::user();
                    $payment['organization_id']=$user->organization_id;
                    $inserted= Payments::create($payment);
                    $inserted_id=$inserted->id;
                    $ids[]=$inserted->id;
                    if (!empty($value['cases'])) {
                        foreach ($value['cases'] as $k => $v) {
                            $v['payment_id'] = $inserted->id;
                            PaymentsCases::create($v);
                        }
                        Payments::where('id', $inserted_id)->update(['amount' => PaymentsCases::where('payment_id', $inserted_id)->sum('amount').""]);
                        $inactive=[];
                        $old = PaymentsCases::where('payment_id', $inserted_id)->get(['case_id']);
                        foreach ($old as $k => $v) {
                            $inactive[]=$v->case_id;
                        }

                        SponsorshipCases::getInactive($inactive,$inserted_id,$inserted->sponsor_id,$inserted->organization_id);

                        $rows = PaymentsRecipient::getRows($inserted->id, 0, $inserted->bank_id, $inserted->exchange_type);
                        $rows->map(function ($subQuery) use ($inserted_id) {

                            $subQuery->amount = 0;
                            $subQuery->amount_after_discount = 0;
                            $subQuery->shekel_amount = 0;
                            $subQuery->shekel_amount_before_discount = 0;

                            $id = $subQuery->person_id;

                            $total = \DB::select("select SUM(`amount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))  ");

                            if ($total) {
                                $subQuery->amount = $total[0]->amount."";
                            }

                            $amount_after_discount = \DB::select("select SUM(`amount_after_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))  ");

                            if ($amount_after_discount) {
                                $subQuery->amount_after_discount = $total[0]->amount."";
                            }
                            if ($amount_after_discount) {
                                $subQuery->amount_after_discount = $total[0]->amount."";
                            }

                            $total_shekel_amount = \DB::select("select SUM(`shekel_amount`) as amount  FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id` = $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))   ");

                            if ($total_shekel_amount) {
                                $subQuery->shekel_amount = $total_shekel_amount[0]->amount."";
                            }

                            $shekel_amount_before_discount = \DB::select("select SUM(`shekel_amount_before_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))  ");

                            if ($shekel_amount_before_discount) {
                                $subQuery->shekel_amount_before_discount_amount = $shekel_amount_before_discount[0]->amount."";
                            }

                            return $subQuery;
                        });

                        foreach ($rows as $k => $v) {
                            if (PaymentsRecipient::where(['payment_id' => $inserted_id, 'person_id' => $v->person_id, 'father_id' => $v->father_id, 'mother_id' => $v->mother_id])->exists()) {
                                PaymentsRecipient::where(['payment_id' => $inserted_id, 'person_id' => $v->person_id, 'father_id' => $v->father_id, 'mother_id' => $v->mother_id])
                                    ->update([
                                        'amount' => $v->amount."",
                                        'amount_after_discount' => ($v->amount - ($v->amount * $inserted->organization_share))."",
                                        'shekel_amount_before_discount' => ($v->amount * $inserted->exchange_rate)."",
                                        'shekel_amount' => round(($v->amount - ($v->amount * $inserted->organization_share)) * $inserted->exchange_rate).""
                                    ]);
                            } else {

                                $v->amount_after_discount = ($v->amount - ($v->amount * $inserted->organization_share)."");
                                $v->shekel_amount_before_discount = ($v->amount * $inserted->exchange_rate)."";
                                $v->shekel_amount = round(($v->amount - ($v->amount * $inserted->organization_share)) * $inserted->exchange_rate)."";

                                if ($v->father_id == null) {
                                    unset($v->father_id);
                                }

                                if ($v->mother_id == null) {
                                    unset($v->mother_id);
                                }

                                if ($inserted->exchange_type == 3) {
                                    unset($v->bank_id);
                                    unset($v->cheque_account_number);
                                }


                                $item = (array)$v;
                                $item['status'] = 0;
                                $item['payment_id'] = $inserted_id;
                                PaymentsRecipient::create($item);
                            }
                        }
                        $rows = PaymentsRecipient::getRows($inserted->id, 1, $inserted->bank_id, $inserted->exchange_type);
                        $rows->map(function ($subQuery) use ($inserted_id) {

                            $subQuery->amount = 0;
                            $subQuery->amount_after_discount = 0;
                            $subQuery->shekel_amount = 0;
                            $subQuery->shekel_amount_before_discount = 0;

                            $id = $subQuery->person_id;

                            $total = \DB::select("select SUM(`amount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))  ");

                            if ($total) {
                                $subQuery->amount = $total[0]->amount."";
                            }

                            $amount_after_discount = \DB::select("select SUM(`amount_after_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))  ");

                            if ($amount_after_discount) {
                                $subQuery->amount_after_discount = $amount_after_discount[0]->amount."";
                            }

                            $total_shekel_amount = \DB::select("select SUM(`shekel_amount`) as amount  FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id` = $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))   ");

                            if ($total_shekel_amount) {
                                $subQuery->shekel_amount = $total_shekel_amount[0]->amount."";
                            }

                            $shekel_amount_before_discount = \DB::select("select SUM(`shekel_amount_before_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))  ");

                            if ($shekel_amount_before_discount) {
                                $subQuery->shekel_amount_before_discount_amount = $shekel_amount_before_discount[0]->amount."";
                            }

                            return $subQuery;
                        });

                        foreach ($rows as $k => $v) {
                            if (PaymentsRecipient::where(['payment_id' => $inserted_id, 'person_id' => $v->person_id, 'father_id' => $v->father_id, 'mother_id' => $v->mother_id])->exists()) {
                                PaymentsRecipient::where(['payment_id' => $inserted_id, 'person_id' => $v->person_id, 'father_id' => $v->father_id, 'mother_id' => $v->mother_id])
                                    ->update([
                                        'amount' => $v->amount,
                                        'amount_after_discount' => ($v->amount - ($v->amount * $inserted->organization_share))."",
                                        'shekel_amount_before_discount' => ($v->amount * $inserted->exchange_rate)."",
                                        'shekel_amount' => round(($v->amount - ($v->amount * $inserted->organization_share)) * $inserted->exchange_rate).""
                                    ]);

                            } else {

                                $v->amount_after_discount = ($v->amount - ($v->amount * $inserted->organization_share))."";
                                $v->shekel_amount_before_discount = ($v->amount * $inserted->exchange_rate)."";
                                $v->shekel_amount = round(($v->amount - ($v->amount * $inserted->organization_share)) * $inserted->exchange_rate)."";

                                if ($v->father_id == null) {
                                    unset($v->father_id);
                                }

                                if ($v->mother_id == null) {
                                    unset($v->mother_id);
                                }

                                if ($inserted->exchange_type == 3) {
                                    unset($v->bank_id);
                                    unset($v->cheque_account_number);
                                }


                                $item = (array)$v;
                                $item['status'] = 0;
                                $item['payment_id'] = $inserted_id;
                                PaymentsRecipient::create($item);
                            }
                        }

                        \Log\Model\Log::saveNewLog('PAYMENTS_CASES_CREATED',trans('sponsorship::application.added new beneficiaries to the payment'));
                    }
                }


                $response['statistic']=Payments::statistic($ids);
                $response['status']=true;
                $response['msg']=
                    trans('sponsorship::application.The row is inserted to db') .' , '.
                    trans('sponsorship::application.total in file') .' : '.  $manipulated.' , '.
                    trans('sponsorship::application.not_allowed_category_numbers') .' : '. $not_allowed_category_numbers.' , '.
                    trans('sponsorship::application.not_saved_id_card_number') .' : '. $not_saved_id_card_number.' , '.
                    trans('sponsorship::application.not_saved_sponsor_number') .' : '. $not_saved_sponsor_number.' , '.
                    trans('sponsorship::application.duplicated') .' : '. $duplicated;
            }

        }
        else{
            $response['status']=false;
            $response['msg']=trans('common::application.no file selected to upload');

        }
        return response()->json($response);



    }

    // get payments details
    public function show($id)
    {
        $this->authorize('view',Payments::class);

        $user = \Auth::user();

        $entry = Payments::fetch($id,'edit');
        $entry->is_mine = false;

        if($entry->organization_id == $user->organization_id){
            $entry->is_mine = true;
        }

        return response()->json($entry);
    }

    // get cases statistic  no of payment using payment_id
    public function statistic($id,Request $request)
    {
        $payment = Payments::findOrFail($id);

        $entry = Payments::fetch($id,'edit');
        $user = \Auth::user();
        $is_mine = false;

        if($entry->organization_id == $user->organization_id){
            $is_mine = true;
        }

        return response()->json([
            'is_mine'=> $is_mine,
            'sponsorship_id'=> $payment->sponsorship_id,
            'count'=>PaymentsCases::where(["payment_id"=>$id])->count(),
            'new_'=>PaymentsCases::where(["payment_id"=>$id,'status'=>1])->count(),
            'old_'=>PaymentsCases::where(["payment_id"=>$id,'status'=>2])->count(),
            'no_payment'=>PaymentsCases::where(["payment_id"=>$id,'status'=>3])->count()
        ]);

    }

    // get payments list according options ( sponsor_id ,  currency_id)
    public function getOptions($id,Request $request)
    {
        $user = \Auth::user();
        if($request->currency_id){
            $list =Payments::with('category')
                ->where(['organization_id'=>$user->organization_id,'sponsor_id'=>$id,'currency_id'=>$request->currency_id])->get();
        }else{
            $list =Currency::whereIn('id', function($query) use ($id,$user){
                $query->select('currency_id')
                    ->from('char_payments')
                    ->where(function($query_) use ($id,$user){
                        $query_->where('sponsor_id', $id);
                        $query_->where('organization_id', $user->organization_id);
                    });

            })->get();
        }
        return response()->json(['list' => $list]);
    }

    // get ( beneficiary or recipient) of payment using $id
    public function getPaymentCases($id,Request $request)
    {
        $response = array();

        $response['payment']=Payments::fetch($id,'show');

        if($request->target == 'recipient'){
            $response['data'] = PaymentsRecipient::getRecord($id);
        }else{
            $response['data']=PaymentsCases::fetch($request->target,$id,$request->status,$request->paginate);
        }

        return response()->json($response);

    }

    // export recipient to excel sheet using payment_id
    public function exportCaseRecipient($id)
    {

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $data = PaymentsCases::exportCaseRecipient($id);

//        return $data;
//        organizations_name
        $keys=["case_name","id_card_number","category_name","sponsor_name","sponsor_number","guardian_name","guardian_id_card_number",
            "payment_date","currency_name", "amount","amount_after_discount","shekel_amount_before_discount","shekel_amount",
            "recipient_amount","recipient_amount_after_discount","recipient_shekel_amount_before_discount","recipient_shekel_amount",
            "date_from","date_to","recipients_name",'recipient_id_card_number',"phone","primary_mobile",
            "secondary_mobile","guardian_phone","guardian_primary_mobile","guardian_secondary_mobile","cheque_account_number","cheque_date","rec_status"];


        if(sizeof($data) !=0){
            $rows=[];
            foreach($data as $key =>$value){
                $rows[$key][trans('sponsorship::application.#')]=$key+1;
                foreach($keys as $ke){
                    $f_ke=null;
                    $vlu = is_null($value->$ke) ? '-' : $value->$ke;

                    if(substr( $ke, 0, 7 ) === "mother_"){
                        $f_ke = trans('sponsorship::application.'.substr($ke,7))." ( ".trans('sponsorship::application.mother')  ." ) ";
                    }elseif(substr( $ke, 0, 7 ) === "father_"){
                        $f_ke = trans('sponsorship::application.'.substr($ke,7))." ( ".trans('sponsorship::application.father')  ." ) ";
                    }elseif(substr( $ke, 0, 9 ) === "guardian_"){
                        $f_ke = trans('sponsorship::application.'.substr($ke,9))." ( ".trans('sponsorship::application.guardian')  ." ) ";
                    }elseif(substr( $ke, 0, 10 ) === "recipient_"){
                        $f_ke = trans('sponsorship::application.'.substr($ke,10))." ( ".trans('sponsorship::application.recipient')  ." ) ";
                    }else{
                        $f_ke = trans('sponsorship::application.' . $ke);
                    }
                    $rows[$key][$f_ke]= $vlu;
                }
            }
            $token = md5(uniqid());
            \Excel::create('export_' . $token, function($excel) use($rows) {
                $excel->sheet(trans('sponsorship::application.recipients report'), function($sheet) use($rows) {
                    $sheet->setStyle([
                        'font' => [
                            'name' => 'Calibri',
                            'size' => 11,
                            'bold' => true
                        ]
                    ]);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setRightToLeft(true);
//                    $sheet->setWidth(['A'=>7 ,'B'=> 15,'C'=> 30,'D'=> 15,'F'=> 30,'E'=> 23,'G'=> 15]);
                    $style = [
                         'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font' =>[
                            'name'      =>  'Simplified Arabic',
                            'size'      =>  12,
                            'bold'      =>  true
                        ]
                    ];

                    $sheet->getStyle("A1:AC1")->applyFromArray($style);
                    $sheet->setHeight(1,30);

                    $style = [
                         'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font' =>[
                            'name'      =>  'Simplified Arabic',
                            'size'      =>  12,
                            'bold' => false
                        ]
                    ];

                    $sheet->getDefaultStyle()->applyFromArray($style);
                    $sheet->fromArray($rows);

                });
            })
                ->store('xlsx', storage_path('tmp/'));
            return response()->json([
                'download_token' => $token,
            ]);
        }
        return response()->json(['status' => false]);
    }

    // export ( beneficiary or recipient - signature_recipient or )  to excel sheet using payment_id and other options (status)
    public function exportCaseList(Request $request)
    {

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        if($request->action =='signature_recipient' ||$request->action =='signature_sheet' || $request->action =='recipients'){
            $payment =Payments::fetch($request->payments_id,'show');

            if($request->action =='signature_sheet'){
                $data=PaymentsRecipient::export('excel',$request->payments_id,$payment->exchange_type,$payment->payment_exchange,true);
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function ($excel) use ($payment,$data) {
                    $excel->sheet(trans('aid::application.signature_sheet'), function($sheet) use ($payment,$data){

                        $sheet->setOrientation('landscape');
                        $sheet->setRightToLeft(true);

                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Calibri',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $sheet->setHeight(1,35);
                        $sheet->setWidth(['A'=>7 ,'B'=> 30,'C'=> 13,'D'=> 7,'F'=> 20,'E'=> 13,'G'=> 15, 'H'=>20,'I'=> 25,'J'=> 13]);

                        $sheet->getStyle("A1:AH1")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 12, 'bold'=>  true ]
                        ]);

                        if(!is_null($payment->organization_logo) && ( $payment->organization_logo != '')){
                            $drawing0 = new \PHPExcel_Worksheet_Drawing;
                            $drawing0->setPath( base_path('storage/app/' . $payment->organization_logo)); //your image path
                            $drawing0->setCoordinates('A1');
                            $drawing0->setWorksheet($sheet);
                            $drawing0->setResizeProportional();
                            $drawing0->setOffsetX($drawing0->getWidth() - $drawing0->getWidth() / 5);
                            $drawing0->setOffsetY(10);

                            // Set height and width
                            $drawing0->setWidth(150);
                            $drawing0->setHeight(150);
                        }
                        if(!is_null($payment->sponsor_logo) && ( $payment->sponsor_logo != '')){

                            $drawing = new \PHPExcel_Worksheet_Drawing;
                            $drawing->setPath( base_path('storage/app/' . $payment->sponsor_logo)); //your image path
                            $drawing->setCoordinates('H1');
                            $drawing->setWorksheet($sheet);
                            $drawing->setResizeProportional();
                            $drawing->setOffsetX($drawing->getWidth() - $drawing->getWidth() / 5);
                            $drawing->setOffsetY(10);
                            // Set height and width
                            $drawing->setWidth(150);
                            $drawing->setHeight(150);
                        }

                        $sheet->getStyle('A')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('B')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('C')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('D')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('E')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('F')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('G')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('H')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('I')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('J')->getAlignment()->setWrapText(true);

                        $sheet->setCellValue('C2',$payment->organization_name);
                        $sheet->mergeCells( 'C2:H2' );
                        $sheet->getStyle('C2:H2')->getAlignment()->setVertical('center');
                        $sheet->getStyle('C2:H2')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('C3',trans('sponsorship::application.payment') . ' : '. $payment->payment_category);
                        $sheet->mergeCells( 'C3:H3' );
                        $sheet->getStyle('C3:H3')->getAlignment()->setVertical('center');
                        $sheet->getStyle('C3:H3')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('C4',$payment->sponsor_name);
                        $sheet->mergeCells( 'C4:H4' );
                        $sheet->getStyle('C4:H4')->getAlignment()->setVertical('center');
                        $sheet->getStyle('C4:H4')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('A5',trans('aid::application.day') . ' : '. '-----------------');
                        $sheet->mergeCells( 'A5:E5' );
                        $sheet->getStyle('A5:E5')->getAlignment()->setVertical('center');
                        $sheet->getStyle('A5:E5')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('F5',trans('aid::application.date'). ' -----/------/---------- ');
                        $sheet->mergeCells( 'F5:J5' );
                        $sheet->getStyle('F5:O5')->getAlignment()->setVertical('center');
                        $sheet->getStyle('F5:O5')->getAlignment()->setHorizontal( 'center' );

                        $sheet->getStyle("A1:O5")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 12, 'bold'=>  true ]
                        ]);

                        $sheet->setCellValue('A6','#');
                        $sheet->setCellValue('B6',trans('sponsorship::application.name'));
                        $sheet->setCellValue('C6',trans('sponsorship::application.id_card_number'));
                        $sheet->setCellValue('D6',trans('aid::application.family_cnt'));
                        $sheet->setCellValue('E6',trans('sponsorship::application.mobile_no'));

                        if($payment->payment_exchange ==0){
                            $sheet->setCellValue('F6',trans('sponsorship::application.amount') .'  ( ' . $payment->name . ' )  ');
                        }else{
                            $sheet->setCellValue('F6','      ' .trans('sponsorship::application.amount') .'    ' .'    ( ' .trans('sponsorship::application.shekel_'). ' )   ');
                        }
                        if($payment->exchange_type ==1){
                            $sheet->setCellValue('G6',trans('sponsorship::application.account_number'));

                        }else if($payment->exchange_type == 2){
                            $sheet->setCellValue('G6',trans('sponsorship::application.cheque_number'));
                        }else{
                            $sheet->setCellValue('G6',trans('sponsorship::application.bond_number'));
                        }
                        $sheet->setCellValue('H6',trans('sponsorship::application.cheque_date'));
                        $sheet->setCellValue('I6',trans('sponsorship::application.country_name'));
                        $sheet->setCellValue('J6',trans('sponsorship::application.district'));
                        $sheet->setCellValue('K6',trans('sponsorship::application.city_name'));
                        $sheet->setCellValue('L6',trans('sponsorship::application.location'));
                        $sheet->setCellValue('M6',trans('sponsorship::application.city_name'));
                        $sheet->setCellValue('N6',trans('sponsorship::application.mosques'));
                        $sheet->setCellValue('O6',trans('sponsorship::application.street_address'));
                        $sheet->setCellValue('P6',trans('sponsorship::application.Signature'));
                        $sheet->getStyle('A6:P6')->getAlignment()->setVertical('center');
                        $sheet->getStyle('A6:P6')->getAlignment()->setHorizontal( 'center' );

                        $sheet->getStyle("A1:P6")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 12, 'bold'=>  true ]
                        ]);

                        $count = 1;
                        $z = 7;
//                        $sheet->fromArray($rows, null, 'A7', true);

                        foreach ($data as $key => $current) {
                            $sheet->setCellValue('A' . $z, $count);
                            $sheet->setCellValue('B' . $z, $current->name);
                            $sheet->setCellValue('C' . $z, $current->id_card_number );
                            $sheet->setCellValue('D' . $z, 0 );
                            $sheet->setCellValue('E' . $z, $current->mobile_no );
                            $sheet->setCellValue('F' . $z, $current->amount);
                            $sheet->setCellValue('G' . $z, $current->cheque_account_number );
                            $sheet->setCellValue('H' . $z, $current->cheque_date );
                            $sheet->setCellValue('I' . $z, $current->country );
                            $sheet->setCellValue('J' . $z, $current->district );
                            $sheet->setCellValue('K' . $z, $current->city );
                            $sheet->setCellValue('L' . $z, $current->location );
                            $sheet->setCellValue('M' . $z, $current->mosques );
                            $sheet->setCellValue('N' . $z, $current->street_address );
                            $sheet->setCellValue('O' . $z, ' ');
                            $sheet->getStyle('A'. $z .':O' . $z)->getAlignment()->setHorizontal('center');
                            $sheet->getStyle('A'. $z .':O' . $z)->getAlignment()->setVertical('center');
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $count++;
                            $z++;
                        }

                        $styleArray = array(
                            'font' => array(
                                'bold' => true,
                            ),
                             'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'borders' => array('top'     => array('style' => 'medium'),
                                'left'   => array('style' => 'medium'),
                                'right'  => array('style' => 'medium'),
                                'bottom' => array('style' => 'medium')

                            ),
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'cce6ff')

                            ),
                        );
                        $sheet->getStyle('A6:P6')->applyFromArray($styleArray);

                        $styleArray = array(
                            'borders' => array(
                                'allborders'    => array('style' => 'medium')
                            ),

                        );
                        $max = sizeof($data) + 6;
                        $sheet->getStyle('A6:P'.$max)->applyFromArray($styleArray);

                        $indicies = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O');
                        $indWidth =['A'=>7 ,'B'=> 25,'C'=> 13,'D'=> 7,'F'=> 20,'E'=> 13,'G'=> 10, 'H'=>15,'I'=> 15,'J'=> 15,
                            'K'=> 15,'L'=> 15,'M'=> 15,'N'=> 15,'O'=> 15];

                        foreach ($indicies as $index) {
                            $sheet->getColumnDimension($index)->setAutoSize(false);
                            $sheet->getColumnDimension($index)->setWidth($indWidth[$index]);

                        }
                        $sheet->getRowDimension(1)->setRowHeight(35);
                        $sheet->getRowDimension(2)->setRowHeight(45);
                        $sheet->getRowDimension(5)->setRowHeight(45);
                        $sheet->getRowDimension(6)->setRowHeight(55);
                        $sheet->getRowDimension(1)->setRowHeight(35);
                        $sheet->getRowDimension(2)->setRowHeight(45);
                        $sheet->getRowDimension(5)->setRowHeight(45);
                        $sheet->getRowDimension(6)->setRowHeight(55);

                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['download_token'=>$token]);
            }
            else{
                if($request->get('ExportTo') == 'excel'){
                    $data=PaymentsRecipient::export('excel',$request->payments_id,$payment->exchange_type,$payment->payment_exchange,false);
                    $rows=[];
                    if(sizeof($data) !=0){
                        foreach($data as $key =>$value){
                            $rows[$key][trans('sponsorship::application.#')]=$key+1;
                            foreach($value as $k =>$v){
                                if($k != 'case_id' && $k != 'id' && $k != 'sponsor_id'&& $k != 'flag'&& $k != 'date_flag'){
                                    if($k =='sponsor_number'){
                                        $rows[$key][trans('sponsorship::application.sponsorship_number')]= $v;
                                    }
                                    else if($k =='amount'){
                                        if($payment->payment_exchange ==0){
                                            $rows[$key][trans('sponsorship::application.' . $k) .' ( ' . $payment->name . ' ) ']= $value->amount;
                                        }else{
                                            $rows[$key][trans('sponsorship::application.' . $k) .' ( ' .trans('sponsorship::application.shekel'). ' ) ']= $value->amount;
                                        }
                                    }
                                    else if($k =='cheque_account_number'){
                                        if($payment->exchange_type ==1){
                                            $rows[$key][trans('sponsorship::application.account_number')]= $v;

                                        }else if($payment->exchange_type == 2){
                                            $rows[$key][trans('sponsorship::application.cheque_number')]= $v;
                                        }else{
                                            $rows[$key][trans('sponsorship::application.bond_number')]= $v;
                                        }
                                    }
                                    else{
                                        $rows[$key][trans('sponsorship::application.' . $k)]= $v;
                                    }
                                }
                            }
                        }
                        $token = md5(uniqid());
                        \Excel::create('export_' . $token, function($excel) use($rows) {
                            $excel->sheet(trans('sponsorship::application.case payment list'), function($sheet) use($rows) {
                                $sheet->setStyle([
                                    'font' => [
                                        'name' => 'Calibri',
                                        'size' => 11,
                                        'bold' => true
                                    ]
                                ]);
                                $sheet->setAllBorders('thin');
                                $sheet->setfitToWidth(true);
                                $sheet->setRightToLeft(true);
                                $style = [
                                     'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font' =>[
                                        'name'      =>  'Simplified Arabic',
                                        'size'      =>  12,
                                        'bold'      =>  true
                                    ]
                                ];

                                $sheet->getStyle("A1:O1")->applyFromArray($style);
                                $sheet->setHeight(1,30);

                                $style = [
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font' =>[
                                        'name'      =>  'Simplified Arabic',
                                        'size'      =>  12,
                                        'bold' => false
                                    ]
                                ];

                                $sheet->getDefaultStyle()->applyFromArray($style);
                                $sheet->fromArray($rows);

                            });
                        })
                            ->store('xlsx', storage_path('tmp/'));
                        return response()->json([
                            'download_token' => $token,
                        ]);
                    }
                }
                elseif($request->get('ExportTo') == 'Word'){

                    $payment_exchange=$payment->payment_exchange;
                    if($payment_exchange ==1){
                        $exchange=trans('sponsorship::application.Israeli shekel just only');
                    }else{
                        $currency=Currency::findorfail($payment->currency_id);
                        $exchange=$currency->name.' ' .trans('sponsorship::application.just only');
                    }

                    $dirName = base_path('storage/app/templates/cheTemp');
                    if (!is_dir($dirName)) {
                        @mkdir($dirName, 0777, true);
                    }

                    if($payment->exchange_type ==3){
                        $items=PaymentsRecipient::export('word',$request->payments_id,$payment->exchange_type,$payment->payment_exchange,false);
                        $user = \Auth::user();
                        $default_template=Setting::where(['id'=>'default-cheque-template','organization_id'=>$user->organization_id])->first();

                        if($default_template){
                            $path = base_path('storage/app/'.$default_template->value);
                        }else{
                            $path = base_path('storage/app/templates/cheque-default-template.docx');
                        }

                        $cnt =sizeof($items);
                        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);

                        try{
                            $templateProcessor->cloneRow('rId', $cnt);

                            $i=1;
                            foreach($items as $item) {
                                $Arabic_numbers=new Arabic_numbers();
                                $templateProcessor->setValue('rId#'.$i, $i);
                                $templateProcessor->setValue('name#'.$i, $item->name);
                                $templateProcessor->setValue('id_card_number#'.$i, $item->id_card_number);
                                $templateProcessor->setValue('value#'.$i, $item->amount);
                                $templateProcessor->setValue('character_value#'.$i, $Arabic_numbers->convert_number((float)$item->amount,$exchange));
                                $templateProcessor->setValue('cheque_number#'.$i, $item->cheque_account_number);
                                $templateProcessor->setValue('cheque_date#'.$i, $item->cheque_date);
                                if($i < $cnt){
                                    $i++;
                                }
                            }
                        }catch (\Exception $e){  }

                        $templateProcessor->saveAs($dirName.'/all-cheque.docx');
                    }
                    else{
                        $items= PaymentsRecipient::exportAccorddingBanks($request->payments_id,$payment->exchange_type,$payment->payment_exchange);
                        foreach($items as $item){
                            if(is_null($item['template'])){
                                $path = base_path('storage/app/templates/cheque-default-banks-template.docx');
                            }
                            else{
                                $path = base_path('storage/app/'.$item['template']);

                            }

                            $cnt =sizeof($item['items']);
                            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);
                            try{
                                $templateProcessor->cloneRow('rId', $cnt);

                                $i=1;
                                foreach($item['items'] as $row) {
                                    $Arabic_numbers=new Arabic_numbers();
                                    $templateProcessor->setValue('rId#'.$i, $i);
                                    $templateProcessor->setValue('name#'.$i, $row->name);
                                    $templateProcessor->setValue('id_card_number#'.$i, $row->id_card_number);
                                    $templateProcessor->setValue('value#'.$i, '#'.$row->amount.'#');
                                    $templateProcessor->setValue('character_value#'.$i, $Arabic_numbers->convert_number((float)$row->amount,$exchange));
                                    $templateProcessor->setValue('cheque_number#'.$i, $row->cheque_account_number);
                                    $templateProcessor->setValue('cheque_date#'.$i, $row->cheque_date);
                                    if($i < $cnt){
                                        $i++;
                                    }
                                }
                            }catch (\Exception $e){  }
                            $templateProcessor->saveAs($dirName.'/'.$item['name'].'.docx');
                        }
                    }

                    $token = md5(uniqid());
                    $zipFileName = storage_path('tmp/export_' . $token . '.zip');

                    $zip = new \ZipArchive;
                    if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
                    {
                        foreach ( glob( $dirName . '/*' ) as $fileName )
                        {
                            $file = basename( $fileName );
                            $zip->addFile( $fileName, $file );
                        }

                        $zip->close();
                        array_map('unlink', glob("$dirName/*.*"));
                        rmdir($dirName);
                    }

                    return response()->json(['download_token' => $token]);
                }
            }
        }
        else{
            $data=PaymentsCases::getSub($request->action,$request->get('target'),$request->payments_id);
            $rows=[];
            if(sizeof($data) !=0){
                foreach($data as $key =>$value){
                    $rows[$key][trans('sponsorship::application.#')]=$key+1;
                    foreach($value as $k =>$v){
//                        && $k != 'amount_in_shekel'
//                        if($k != 'case_id' && $k != 'id' && $k != 'sponsor_id'&& $k != 'flag'&& $k != 'date_flag'){
//                            if($k =='amount'){
//                                $rows[$key][trans('sponsorship::application.amount_in_payments_currency')]= $v;
//                            }else
                        if($k =='sponsor_number'){
                            $rows[$key][trans('sponsorship::application.sponsorship_number')]= $v;
                        }
//                            else{
                        $rows[$key][trans('sponsorship::application.' . $k)]= $v;
//                            }
//                        }
                    }
                }
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($rows) {
                    $excel->sheet(trans('sponsorship::application.payments report'), function($sheet) use($rows) {
                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Calibri',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $style = [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold'      =>  true
                            ]
                        ];

                        $sheet->getStyle("A1:O1")->applyFromArray($style);
                        $sheet->setHeight(1,30);

                        $style = [
                             'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold' => false
                            ]
                        ];

                        $sheet->getDefaultStyle()->applyFromArray($style);
                        $sheet->fromArray($rows);

                    });
                })
                    ->store('xlsx', storage_path('tmp/'));
                return response()->json(['download_token' => $token]);
            }
        }
        return response()->json(['status' => false]);
    }

    // export internal cheque of payment's beneficiary with options ( payment_id , date_to , date_from )
    public function exportCustom(Request $request)
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        ini_set('memory_limit', '1G');

        $payment = Payments::detail($request->payment_id);

        $range = PaymentsCases::where(function ($q) use ($request) {
            $q->where('payment_id', '=', $request->payment_id);
            $q->whereNotNull('date_from');
            $q->whereNotNull('date_to');
        })->first();

        $range_ = '-';
        if($range){
            $range_ = $range->date_from .'- '.$range->date_to ;
        }


        $districts = Payments::getDistrictPayment($request->payment_id);

        $dirName = base_path('storage/tmp');
        if (!is_dir($dirName)) {
            @mkdir($dirName, 0777, true);
        }

        $total_count = 0;
        foreach ($districts as $k=>$v){
            PDF::reset();
            PDF::SetTitle(trans('sponsorship::application.Maturity statements'));
            PDF::SetFont('aefurat', '', 18);
//            PDF::SetFont('aefurat', '', 18 , '', 'default', true );
//        PDF::SetFont('dejavusans', '', 18 , '', 'default', true );
            PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            PDF::SetAutoPageBreak(TRUE);
            PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);
            PDF::setCellHeightRatio(1.5);
            PDF::SetMargins(5, 15, 5);
            PDF::SetHeaderMargin(5);
            PDF::SetFooterMargin(5);
            PDF::SetAutoPageBreak(TRUE);

            $count = sizeof($v->beneficiary);
            $amount = 0;
            $total_count += $count;
            $ceil=ceil($count/17);

            for ($x = 0; $x < $ceil; $x++) {
                $curOffset = ($x) * 17;
                $html = '<!doctype html>
                 <html lang="ar">
                     <head>
                           <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;}
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .cell-style{text-align: center !important; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                    </head>
                    <body style="font-weight: normal !important;">';


                $html .= '<table width = "533" border="1" style="box-sizing:border-box;">';
                $imgOrg = base_path('storage/app/'.$payment->organization_logo);
                $imgSpo = base_path('storage/app/'.$payment->sponsor_logo);

                $html .= ' <tr style="line-height: 47px;font-size: 14px; height: 70px; border:0.5px solid black !important; ">
                              <td rowspan="3" align="center"     valign="middle" > 
                                    <br><br><img src="'.$imgOrg.'" style=" width: 80px;height: 70px;display: block;margin-left: auto;margin-right: auto;">
                                </td>
                                 <td align="center" colspan="2" style="font-size:12pt !important; width: 295px; "> '.trans('sponsorship::application.finance') .' /'.$payment->sponsor_name.'</td>
                                <td rowspan="3" align="center"     valign="middle" > 
                                   <br><br><img src="'.$imgSpo.'" style=" width: 80px;height: 70px;display: block;margin-left: auto;margin-right: auto;">
                                </td>
                   </tr>
                   <tr style=" line-height: 40px;  font-size: 14px; height: 57px; ">
                     <td align="center" colspan="2" style="font-size:12pt !important;  "> '.trans('sponsorship::application.Maturity for the period') .'( '.$range_.' )</td>
                    </tr>
                    <tr style=" line-height: 40px; font-size: 14px; height: 57px;   ">
                         <td align="center" colspan="2" style="font-size:12pt !important;">'.trans('sponsorship::application.Implementation of').' /'.$payment->organization_name.'</td>
                    </tr>';

                $html .= '</table>';
                $html .= '<span><br></span>';

                $html.= '<br><br>';

                $index=$curOffset;
                $html .=  '<table  cellpadding="pixels" style="   width:100%
                                            display: table;
                                            text-align: center;
                                            margin-bottom: 20px;
                                            border-collapse: collapse;
                                            border-spacing: 0;
                                          ">
                                <thead>
                                <tr style="height:40px; line-height: 25px; ">
                                    <th class="heading-style" style="width: 40px; "> '.trans('sponsorship::application.orphan_id').'</th>
                                    <th class="heading-style" style="width: 135px; "><br><br>'.trans('sponsorship::application.orphan_name') .'</th>
                                    <th class="heading-style" style="width: 50px; "> '.trans('sponsorship::application.guardian_card_').'</th>
                                    <th class="heading-style" style="width: 135px; "><br><br>'.trans('sponsorship::application.guardian_name_') .'</th>
                                    <th class="heading-style" style="width: 65px; "><br><br>'.trans('sponsorship::application.for') .'</th>
                                    <th class="heading-style" style="width: 40px; "> '.trans('sponsorship::application.amount_sk') .'</th>
                                    <th class="heading-style" style="width: 100px; "><br><br>'.trans('sponsorship::application.signature') .'</th>
                                </tr>
                                </thead> <tbody>';

                if($index < sizeof($v->beneficiary)) {
                    for ($z = 0; $z < 17; $z++) {
                        if($index < $count){
                            $v1 = $v->beneficiary[$index];
                            $sponsor_number = is_null($v1->sponsor_number) ? '-' : $v1->sponsor_number;
                            $case_name      = is_null($v1->case_name) ? '-' : $v1->case_name;
                            $guardian_card  = is_null($v1->guardian_card) ? '-' : $v1->guardian_card;
                            $guardian_name  = is_null($v1->guardian_name) ? '-' : $v1->guardian_name;
                            $shekel_amount  = is_null($v1->shekel_amount) ? '-' : $v1->shekel_amount;

                            $html .='<tr style="height:20px; line-height: 25px; ">
                                                    <td  class="cell-style" style="width:40px;  ">'.$sponsor_number .'</td>
                                                    <td  class="cell-style" style="width:135px; ">'.$case_name .'</td>
                                                    <td  class="cell-style" style="width:50px;  ">'.$guardian_card.'</td>
                                                    <td  class="cell-style" style="width:135px; ">'.$guardian_name .'</td>
                                                    <td  class="cell-style" style="width:65px;  ">'. $payment->payment_category .'</td>
                                                    <td  class="cell-style" style="width:40px;  ">'.$shekel_amount .'</td>
                                                    <td  class="cell-style" style="width:100px;  "> </td>
                                                </tr>';

                            $amount += $shekel_amount;
                            $index++;
                        }
                    }
                }
                $html .= '</tbody></table>';
//                $html .= ' page ' . ( $x+1 ) . 'From ' . $ceil;
                $html .= ' </body></html>';

                PDF::SetFooterMargin(0);
                PDF::AddPage('P','A4');
                PDF::setFooterCallback(function($pdf) use ($x , $ceil){
                    $footer = '<p style="text-align: left"> (  ' . ( $x+1 ) .' \ '. $ceil.' ) </p>';
                    PDF::writeHTMLCell(0, 0, '', 270, $footer, 0, 1, 0, true, '', true);
                });
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

            }

            $html = '<!doctype html>
                 <html lang="ar">
                     <head>
                           <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .cell-style{text-align: center !important; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                    </head>
                    <body style="font-weight: normal !important;">';
            $html.= '<h3 style="text-align:center;"> </h3>';
            $html.= '<h3 style="text-align:center;"> </h3>';
            $html.= '<h3 style="text-align:center;"> </h3>';

            $html  .=  ' <table border="0" align="center">';

            $html .='<tr style="line-height: 30px;font-size: 12px!important; height: 40px; ">
                        <th style="border: 1px solid black"> <br /> '.trans('sponsorship::application.Total number').' </th>
                        <th style="border: 1px solid black"> <br />   '.trans('sponsorship::application.Total amounts').' </th>
                        <th style="border: 1px solid black"> <br />    '.trans('sponsorship::application.category_name').' </th>
                 </tr>
                 <tr style="line-height: 30px;font-size: 12px!important; height: 40px;" >
                        <td style="border: 1px solid black"><br /> <br />' .$count . ' <br /></td>
                        <td style="border: 1px solid black"> <br /><br />' .$amount . ' <br /></td>
                        <td style="border: 1px solid black"> <br /><br />' .$payment->payment_category . ' <br /></td>
                </tr>';
            $html .= '</table>';
            $html.= '<br><br><br><br><br>';
            $html  .=  ' <table border="0" align="center">';
            $html .= ' <tr style="border: none !important; line-height: 30px;
                              font-size: 12px!important; height: 40px; ">
                        <td style="border: 1px solid black" colspan="3"> '.trans('sponsorship::application.cashier').':  </td>
                        <td colspan="2">  </td>
                        <td style="border: 1px solid black" colspan="3"> '.trans('sponsorship::application.President of the organization').' :  </td>
                   </tr>';

            $html .= ' <tr style="border: none !important; line-height: 30px;
                              font-size: 12px!important; height: 40px; ">
                        <td style="border: none" colspan="3"> <br><br><br><br> </td>
                        <td colspan="2">  </td>
                        <td style="border: none" colspan="3"> <br><br><br><br> </td>
                   </tr>';
            $html .= '</table>';
            $html .= '</body></html>';
            PDF::SetFooterMargin(0);
            PDF::AddPage('P','A4');
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

            PDF::Output($dirName.'/'.$v->name.'.pdf', 'F');

        }
        //***************************************//
        // total pdf page

        PDF::reset();
        PDF::SetTitle('كشوفات الاستحقاق');
        PDF::SetFont('aefurat', '', 18);
//            PDF::SetFont('aefurat', '', 18 , '', 'default', true );
//        PDF::SetFont('dejavusans', '', 18 , '', 'default', true );
        PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        PDF::SetAutoPageBreak(TRUE);
        PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);
        PDF::setCellHeightRatio(1.5);
        PDF::SetMargins(5, 15, 5);
        PDF::SetHeaderMargin(5);
        PDF::SetFooterMargin(5);
        PDF::SetAutoPageBreak(TRUE);
//        PDF::setHeaderCallback(function($pdf) {
//            $header = '<img src="'.storage_path('app/page/header.jpg').'" alt=""/>';
//            PDF::writeHTMLCell(0, 0, '', 5, $header, 0, 1, 0, true, '', true);
//        });
//        PDF::setFooterCallback(function($pdf) {
//            $footer = '<img src="'.storage_path('app/page/footer.jpg').'" alt=""/>';
//            PDF::writeHTMLCell(0, 0, '', 270, $footer, 0, 1, 0, true, '', true);
//        });

        $html = '<!doctype html>
                 <html lang="ar">
                     <head>
                           <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .cell-style{text-align: center !important; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                    </head>
                    <body style="font-weight: normal !important;">';
        $html.= '<h3 style="text-align:center;"> </h3>';
        $html.= '<h3 style="text-align:center;"> </h3>';
        $html.= '<h3 style="text-align:center;"> </h3>';

        $html  .=  ' <table border="0" align="center">';

        $html .='<tr style="line-height: 30px;font-size: 12px!important; height: 40px; ">
                        <th style="border: 1px solid black" rowspan="2"> <br /><br /> '.trans('sponsorship::application.totals').' <br /></th>
                        <th style="border: 1px solid black"> <br /> '.trans('sponsorship::application.total').'    </th>
                        <th style="border: 1px solid black"> <br />  '.trans('sponsorship::application.amount_sk').'</th>
                 </tr>
                 <tr style="line-height: 30px;font-size: 12px!important; height: 40px;" >
                        <td style="border: 1px solid black"> <br /><br />' .$total_count . '</td>
                        <td style="border: 1px solid black"><br /> <br />' .$payment->shekel_amount . '</td>
                </tr>';
        $html .= '</table>';
        $html.= '<br><br><br><br><br>';
        $html  .=  ' <table border="0" align="center">';
        $html .= ' <tr style="border: none !important; line-height: 30px;
                              font-size: 12px!important; height: 40px; ">
                        <td style="border: 1px solid black" colspan="3">  '.trans('sponsorship::application.cashier:').' </td>
                        <td colspan="2">  </td>
                        <td style="border: 1px solid black" colspan="3">'.trans('sponsorship::application.President of the organization').' </td>
                   </tr>';

        $html .= ' <tr style="border: none !important; line-height: 30px;
                              font-size: 12px!important; height: 40px; ">
                        <td style="border: none" colspan="3"> <br><br><br><br> </td>
                        <td colspan="2">  </td>
                        <td style="border: none" colspan="3"> <br><br><br><br> </td>
                   </tr>';
        $html .= '</table>';
        $html .= '</body></html>';
        PDF::SetFooterMargin(0);
        PDF::AddPage('P','A4');
        PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        PDF::Output($dirName.'/'.'الاجمالي'.'.pdf', 'F');


        //***************************************//

        $token = md5(uniqid());
        $zipFileName = storage_path('tmp/export_' . $token . '.zip');

        $zip = new \ZipArchive;
        if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
        {
            foreach ( glob( $dirName . '/*' ) as $fileName )
            {
                $file = basename( $fileName );
                $zip->addFile( $fileName, $file );
            }

            $zip->close();
//            array_map('unlink', glob("$dirName/*.*"));
//            rmdir($dirName);
        }

        return response()->json(['download_token' => $token , 'districts' =>$districts]);

    }

    // get ( beneficiary or recipient) of payment using payment_id , status
    public function getStatistic(Request $request)
    {
        $response = array();
        $id = $request->payment_id;

        $user = \Auth::user();
        $entry = Payments::fetch($id,'show');
        $entry->is_mine = false;

        if($entry->organization_id == $user->organization_id){
            $entry->is_mine = true;
        }
        $response['payment']=$entry;

        if($request->target == 'recipient'){
            $response['data']=PaymentsRecipient::getRecord($id,$request->status,$request->paginate);
        }else{
            $response['data']=PaymentsCases::fetch($request->target,$id,$request->status,$request->paginate);
        }
        return response()->json($response);

    }

    // update payments details and setRecipient if required
    public function update(Request $request, $id){
        $this->authorize('update',Payments::class);
        $user = \Auth::user();

        if($request->organization_share != null && $request->organization_share != null ){
            $ancestor_id = \Organization\Model\Organization::getAncestor($user->organization_id);
            $row=\DB::table('char_settings')
                ->where('char_settings.id','=','max_organization_share')
                ->where('char_settings.organization_id',$ancestor_id)
                ->first();

            if($row){
                if($row->value < $request->organization_share){
                    return response()->json(array(
                        "status" => 'failed',
                        "msg" => trans('sponsorship::application.The entered organization share is exceeded the max of organization share')
                    ));
                }
            }
        }

        $error = Helpers::isValid($request->all(),Payments::getValidatorRules());
        if($error)
            return response()->json($error);

        $payment = Payments::findOrFail($id);
        $exchange_type=$payment->exchange_type;

        $amount = $payment->amount;
        $organization_share=$payment->organization_share;
        $exchange_rate=$payment->exchange_rate;
        $administration_fees=$payment->administration_fees;

        $payment->bank_id =null;
        $payment->sponsorship_id =null;
        $fillable = Payments::getFields();
        $dates = ['payment_date' , 'exchange_date'];

        foreach ($fillable as $col){
            if(isset($request->$col)){
                if(in_array($col,$dates)){
                    if(!is_null($request->$col) && $request->$col != " " && $request->$col != ""){
                        $payment->$col =date('Y-m-d',strtotime($request->$col));
                    }
                }
                else{
                    $payment->$col =  $request->$col;
                }
            }
        }

        if($payment->save()) {
            \Log\Model\Log::saveNewLog('PAYMENTS_UPDATED',trans('sponsorship::application.Edited payment data') );
            if( $exchange_type != $payment->exchange_type || $amount != $payment->amount ||$organization_share != $payment->organization_share ||
                $exchange_rate != $payment->exchange_rate || $administration_fees != $payment->administration_fees ){
                PaymentsRecipient::setRecipient($id);
            }
            return response()->json(array(
                "status" => 'success',
                "payment_id" => $payment->id,
                "msg" => trans('sponsorship::application.The row is updated')
            ));
        }
        return response()->json(array(
            "status" => 'failed',
            "msg" => trans('sponsorship::application.There is no update')
        ));

    }

    // import cases to payment using (sponsorship or sponsor)
    public function setPaymentsPerson($id,Request $request)
    {
        $this->authorize('create',Payments::class);
        $this->authorize('update',Payments::class);

        $source=$request->source;
        $payment = Payments::where('id',$id)->first();

        $user = \Auth::user();
        $cases=[];
        $manipulated_sponsor_number=[];
        $manipulated_cases=[];
        $manipulated=0;
        $duplicated=0;
        $not_saved_sponsor_number=0;
        $old=[];
        $new=[];

        if($source == 'sponsorship') {

            if($payment->sponsorship_id == null){
                return response()->json([ 'status'=>false,'msg'=> trans('sponsorship::application.no sponsorship')]);
            }

            $sponsorship= Sponsorships::where('id' ,$payment->sponsorship_id)->first();
            $target=SponsorshipCases::getRecords([$payment->sponsorship_id],$sponsorship->sponsor_id,$id,$payment->bank_id,$payment->organization_id,$payment->payment_target,$payment->exchange_type);
            if(sizeof($target) == 0){
                return response()->json([ 'status'=>false,'msg'=> trans('sponsorship::application.no sponsorship persons accepted')]);
            }else{
                foreach($target as $key=>$value){

                    if(!in_array($value->sponsor_number,$manipulated_sponsor_number) || !in_array($value->case_id,$manipulated_cases)) {

                        if(!in_array($value->sponsor_number,$manipulated_sponsor_number)) {
                            $manipulated_sponsor_number[] = $value->sponsor_number;
                        }

                        if(!in_array($value->case_id,$manipulated_cases)) {
                            $manipulated_cases[] = $value->case_id;
                        }

                        if( ($value->payment_case_id == null && $value->payments == 0) || ($value->payment_case_id == null && $value->payments != 0) ) {

                            $target_case=[ 'payment_id' => $id, 'case_id' => $value->case_id, 'guardian_id' => $value->guardian_id,
                                'sponsor_number' => $value->sponsor_number, 'status' => 1 ,'recipient'=>1]; //,'recipient'=>0

                            if($value->payments == 0){
                                $new[]=$value->case_id;
                            }else{
                                $target_case['status']= 2;
                                $old[]=$value->case_id;
                            }

                            $cases[]=$target_case;

                        }else if( ($value->payment_case_id != null && $value->payments == 0) || ($value->payment_case_id != null && $value->payments != 0) ){
                            if($value->payments == 0){
                                $new[]=$value->case_id;
                                $updated= ['status' => 1];
                            }else{
                                $updated= ['status' => 2];
                                $old[]=$value->case_id;
                            }
                            PaymentsCases::where([ 'payment_id' => $id, 'case_id' => $value->case_id,'sponsor_number' => $value->sponsor_number])->update($updated);
                        }


                    }else{
                        $duplicated++;
                    }

                    $manipulated++;

                }

                if (!empty($cases)) {
                    foreach ($cases as $key => $value) {
                        PaymentsCases::create($value);
                        \Log\Model\Log::saveNewLog('PAYMENTS_CASES_CREATED',trans('sponsorship::application.added new beneficiaries to the payment'));
                    }
                }
                PaymentsRecipient::setRecipient($id);
            }
        }
        else if( $source == 'sponsor' ){
            $sponsorship= Sponsorships::where('sponsor_id' ,$payment->sponsor_id)->get();
            $sponsorship_id=array();
            foreach($sponsorship as $key=>$value) {
                $sponsorship_id[]=$value->id;
            }

            if(sizeof($sponsorship_id) == 0){
                return response()->json([ 'status'=>false,'msg'=> trans('sponsorship::application.no sponsorship to spnosor')]);
            }
            else{
                $target=SponsorshipCases::getRecords($sponsorship_id,$payment->sponsor_id,$id,$payment->bank_id,$payment->organization_id,$payment->payment_target,$payment->exchange_type);
                if(sizeof($target) == 0){
                    return response()->json([ 'status'=>false,'msg'=> trans('sponsorship::application.the sponsor not guaranteed persons')]);
                }else{

                    foreach($target as $key=>$value){
                        if(!in_array($value->sponsor_number,$manipulated_sponsor_number) || !in_array($value->case_id,$manipulated_cases)) {

                            if(!in_array($value->sponsor_number,$manipulated_sponsor_number)) {
                                $manipulated_sponsor_number[] = $value->sponsor_number;
                            }

                            if(!in_array($value->case_id,$manipulated_cases)) {
                                $manipulated_cases[] = $value->case_id;
                            }

                            if( ($value->payment_case_id == null && $value->payments == 0) || ($value->payment_case_id == null && $value->payments != 0) ) {

                                $target_case=[ 'payment_id' => $id, 'case_id' => $value->case_id, 'guardian_id' => $value->guardian_id,
                                    'sponsor_number' => $value->sponsor_number, 'status' => 1  ,'recipient'=>1]; //,'recipient'=>0

                                if($value->payments == 0){
                                    $new[]=$value->case_id;
                                }else{
                                    $target_case['status']= 2;
                                    $old[]=$value->case_id;
                                }

                                $cases[]=$target_case;

                            }else if( ($value->payment_case_id != null && $value->payments == 0) || ($value->payment_case_id != null && $value->payments != 0) ){
                                if($value->payments == 0){
                                    $new[]=$value->case_id;
                                    $updated= ['status' => 1];
                                }else{
                                    $updated= ['status' => 2];
                                    $old[]=$value->case_id;
                                }
                                PaymentsCases::where([ 'payment_id' => $id, 'case_id' => $value->case_id,'sponsor_number' => $value->sponsor_number])->update($updated);
                            }


                        }else{
                            $duplicated++;
                        }


                        $manipulated++;
                    }
                    if (!empty($cases)) {
                        foreach ($cases as $key => $value) {
                            PaymentsCases::create($value);
                            \Log\Model\Log::saveNewLog('PAYMENTS_CASES_CREATED',trans('sponsorship::application.added new beneficiaries to the payment'));
                        }
                    }
                    PaymentsRecipient::setRecipient($id);
                }
            }
        }

        return response()->json(['status'=>true,'msg'=>trans('common::application.action success')]);

    }

    // update status of beneficiary details and setRecipient if required
    public function updateStatus(Request $request, $id){

        //            $this->authorize('update',Payments::class);
        $action = $request->action;
        $newStatus = $request->value;
        $selectedStatus = $request->selectedStatus;
        $cases = $request->cases;
        $response = array();
        $update=true;
        $updated = ['status'=> $newStatus];

        if($newStatus == 3){
            $updated['amount']=0;
            $updated['amount_after_discount']=0;
            $updated['shekel_amount']=0;
            $updated['shekel_amount_before_discount']=0;

        }

        if($action == 'all'){
            if($newStatus == 2){
                $counts = PaymentsCases::where(["payment_id"=>$id,'status'=> 3])->count();
            }else{
                $counts = PaymentsCases::where(["payment_id"=>$id,'status'=> 2])->count();
            }

            if($counts == 0){
                return response()->json(["status" =>'failed',"msg" => trans('sponsorship::application.no cases has the prev status')]);
            }

            if($selectedStatus == 3 || $selectedStatus == 2){
                PaymentsCases::where(function ($q) use ($id,$selectedStatus) {
                    $q->where('status','=',$selectedStatus);
                    $q->where('payment_id','=',$id);

                })->update($updated);
            }else{
                PaymentsCases::where(["payment_id"=>$id])->update($updated);
            }

        }else{
            foreach ($cases as $key=>$value){
                PaymentsCases::where(["payment_id"=>$id, "case_id"=>$value['case_id'], "sponsor_number"=>$value['sponsor_number']])->update($updated);
            }
        }

        if($update){
            \Log\Model\Log::saveNewLog('PAYMENTS_CASES_UPDATED',trans('sponsorship::application.edited cases of beneficiaries of payment'));
            $response["status"]= 'success';
            $response["msg"]= trans('sponsorship::application.The row is updated');

            if($action == 'all'){
                PaymentsRecipient::setRecipient($id);
            }else{
                foreach ($cases as $key=>$value){
                    PaymentsCases::saveRecipient($id,$value['case_id'],$value['sponsor_number'],$newStatus);
                }
            }
        }
        else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sponsorship::application.There is no update');
        }
        return response()->json($response);
    }

    // update date range of guaranteed beneficiary details
    public function updateDateRange(Request $request, $id){

        $this->authorize('update',Payments::class);

        $rules= ['date_from' => 'required', 'date_to' => 'required'];
        $input= array(
            'date_from' =>  date('Y-m-d',strtotime($request->from)) ,
            'date_to' =>  date('Y-m-d',strtotime($request->to))
        );

        $error = Helpers::isValid($input,$rules);
        if($error)
            return response()->json($error);

        if(PaymentsCases::where(['case_id'=>$request->case_id,'sponsor_number'=>$request->sponsor_number,"payment_id"=>$id])->update($input))
        {
            $case = CaseModel::fetch(array('full_name'=>true), $request->get('case_id'));
            $name=$case->full_name;

            \Log\Model\Log::saveNewLog('PAYMENTS_CASES_UPDATED',trans('sponsorship::application.edited the payment period for the beneficiary of the payment') . ': "'.$name. '" ');
            return response()->json(array('status' => true,'from' => $input['from'], 'to' => $input['to'],
                'msg' => trans('setting::application.The row is updated')));
        }

        return response()->json(array('status' => false,'msg' => trans('setting::application.There is no update')));
    }

    // update guaranteed beneficiary details of payment and setRecipient if required
    public function updateGuaranteed(Request $request, $id){
        $items=$request->get('items');
        $recipient=[];
        $sponsor_number=[];

        $payment =Payments::findorfail($id);
        $max_allowed_amount = $payment->amount ;
        foreach($items as $k => $v){

            $saved = PaymentsCases::where(["payment_id"=>$id,"case_id"=>$v['case_id'],"sponsor_number"=>$v['sponsor_number']])->first();
            $totalAmount = PaymentsCases::where(["payment_id"=>$id])->sum('amount');
            $resetAmount  = (( $totalAmount - $saved->amount ) + $v['amount'])."" ;

            if($resetAmount > $max_allowed_amount ){
                return response()->json(["status" => false ,"msg" => trans('sponsorship::application.The amount exceed the allowed amount')]);
            }

            $updated=['amount'=>$v['amount']."",'shekel_amount'=>$v['shekel_amount']."",'recipient'=>$v['recipient']];
            $updated['amount_after_discount']=($updated["amount"] - ($updated["amount"] * $payment->organization_share))."";
            $updated['shekel_amount_before_discount']= ($updated["amount"] * $payment->exchange_rate)."";
            $updated['shekel_amount']=round( ( $updated["amount"] - ($updated["amount"] * $payment->organization_share)) * $payment->exchange_rate)."";


            PaymentsCases::where(["payment_id"=>$id,"case_id"=>$v['case_id'],"sponsor_number"=>$v['sponsor_number']])->update($updated);

            $recipient_id = null;
            $father_id = $v['father_id'];
            $mother_id = $v['mother_id'];

            if($v['recipient'] == 1){
                $recipient_id=$v['guardian_id'];
            }else{
                $recipient_id=$v['person_id'];
            }

            $query= \DB::select("select get_recipient_amount('$recipient_id','$id','$father_id','$mother_id',0) as amount,
                                     get_recipient_amount('$recipient_id','$id','$father_id','$mother_id',2) as amount_after_discount,
                                     get_recipient_amount('$recipient_id','$id','$father_id','$mother_id',3) as shekel_amount_before_discount,
                                     get_recipient_amount('$recipient_id','$id','$father_id','$mother_id',4) as shekel_amount");

            $r=  $query[0];

            PaymentsRecipient::where(['payment_id'=>$id,'person_id'=>$recipient_id,'father_id'=>$father_id,'mother_id'=>$mother_id])
                ->update([
                    'amount'=>$r->amount."",
                    'amount_after_discount'=>$r->amount_after_discount."",
                    'shekel_amount_before_discount'=>$r->shekel_amount_before_discount."",
                    'shekel_amount'=>$r->shekel_amount.""
                ]);

        }
        \Log\Model\Log::saveNewLog('PAYMENTS_CASES_UPDATED',trans('sponsorship::application.edited cases of beneficiaries of payment'));

        return response()->json(['status'=>true]);
    }

    // save date of guaranteed beneficiary (date , amount ) and setRecipient if required
    public function savePaymentCases(Request $request, $id){
        return response()->json(PaymentsCases::savePaymentCases($id,$request->all()));
    }

    // update recipient details of payment using payment_id
    public function saveCaseRecipient(Request $request, $id)
    {

//            $this->authorize('update',Payments::class);
        $response = array();
        $payment =Payments::findorfail($id);

        $case_id  = $request->get('case_id');
        $sponsor_number  = $request->get('sponsor_number');
        $recipient  = $request->get('recipient');

        $record = PaymentsCases::where(function($q) use($id,$case_id,$sponsor_number) {
            $q->where('status','!=',3);
            $q->where('payment_id','=',$id);
            $q->where('case_id','=',$case_id);
            $q->where('sponsor_number','=',$sponsor_number);
        })->first();

        $save = PaymentsCases::where(function($q) use($id,$case_id,$sponsor_number) {
            $q->where('status','!=',3);
            $q->where('payment_id','=',$id);
            $q->where('case_id','=',$case_id);
            $q->where('sponsor_number','=',$sponsor_number);
        })->update(['recipient'=>$recipient]);

        if($save) {

            $case = CaseModel::fetch(array('full_name'=>true), $case_id);
            $name=$case->full_name;
            \Log\Model\Log::saveNewLog('PAYMENTS_CASES_UPDATED',trans('sponsorship::application.edited the payment period for the beneficiary of the payment') . ': "'.$name. '" ');

            $person_id = CaseModel::PersonId($case_id);
            $old_recipient = $record->recipient ? $record->guardian_id : $person_id;
            $old_recipient_record = PaymentsRecipient::where(["payment_id"=>$id ,'person_id'=> $old_recipient])->first();

            if($old_recipient_record){

                $new_amount = $old_recipient_record->amount - $record->amount;
                if($new_amount > 0){
                    $old_recipient_record->amount = ($old_recipient_record->amount - $record->amount) ."";
                    $old_recipient_record->amount_after_discount = ($old_recipient_record->amount - ($old_recipient_record->amount * $payment->organization_share))."";
                    $old_recipient_record->shekel_amount_before_discount = ($old_recipient_record->amount * $payment->exchange_rate)."";
                    $old_recipient_record->shekel_amount=round( ( $old_recipient_record->amount - ($old_recipient_record->amount * $payment->organization_share)) * $payment->exchange_rate)."";
                    $old_recipient_record->save();

                }else{
                    PaymentsRecipient::where(["payment_id"=>$id ,'person_id'=> $old_recipient])->delete();
                }

            }

            $new_recipient = ($recipient == 0) ? $person_id : $record->guardian_id;

            $new_recipient_record = PaymentsRecipient::where(["payment_id"=>$id ,'person_id'=> $new_recipient])->first();

            if($new_recipient_record){
                $new_recipient_record->amount = ($new_recipient_record->amount + $record->amount) ."";
                $new_recipient_record->amount_after_discount = ($new_recipient_record->amount - ($new_recipient_record->amount * $payment->organization_share))."";
                $new_recipient_record->shekel_amount_before_discount = ($new_recipient_record->amount * $payment->exchange_rate);
                $new_recipient_record->shekel_amount=round( ( $new_recipient_record->amount - ($new_recipient_record->amount * $payment->organization_share)) * $payment->exchange_rate)."";
                $new_recipient_record->save();

            }
            else{
                PaymentsRecipient::setOneRecipient($id,$new_recipient,$case_id,$record->amount,$record->shekel_amount,$recipient);
            }

            $response["status"]= true;
            $response["msg"]= trans('setting::application.The row is updated');
        }else{
            $response["status"]= false;
            $response["msg"]= trans('setting::application.There is no update');
        }

        return response()->json($response);
    }

    // update the Cheque number of payment recipient using payment_id
    public function updateChequeNumber(Request $request, $id){
        return response()->json(PaymentsRecipient::updateChequeNumber($id,$request->offSet,$request->value));
    }

    // save date of recipient (cheque_date , cheque_number , bank )
    public function saveRecipient(Request $request, $id){
        return response()->json(PaymentsRecipient::saveRecipient($id,$request->all()));
    }

    // update date of recipient (cheque_date , cheque_number , bank )
    public function updatePaymentsRecipient(Request $request, $id){

        $items=$request->get('items');
        $payments=Payments::findorfail($id);
        $response=['status' => true];

        if($payments->exchange_type == 1) {
            foreach($items as $k => $v){

                if($v['bank_id'] == null || $v['bank_id'] == ""){
                    $response["msg"][$k]['bank_id'][0]= trans('sponsorship::application.required_field');
                }

                if($v['cheque_account_number'] == null || $v['cheque_account_number'] == ""){
                    $response["msg"][$k]['cheque_account_number'][0]= trans('sponsorship::application.required_field');
                }

                if($v['cheque_account_number'] != null && $v['cheque_account_number'] != "" &&
                    $v['bank_id'] != null && $v['bank_id'] != ""){

                    $details=[];
                    $details=['bank_id' =>$v['bank_id'] ,'cheque_account_number' => $v['cheque_account_number'],'status'=>$v['status']];

                    if (!empty($details)) {
                        PaymentsRecipient::where(["payment_id"=>$id ,'person_id'=>$v['person_id'],'father_id'=>$v['father_id'],'mother_id'=>$v['mother_id']])->update($details);
                    }
                }

            }
            if (!empty($response["msg"])) {
                $response['status']=false;
            }
        }
        if($payments->exchange_type == 2) {
            foreach($items as $k => $v){
                $updated=['cheque_account_number'=>null,'bank_id'=>$v['bank_id'],'status'=>$v['status']];
                if($v['cheque_account_number'] != null && $v['cheque_account_number'] != "" ){
                    $updated['cheque_account_number']=$v['cheque_account_number'];
                }
                if($v['cheque_date'] != null && $v['cheque_date'] != "" ){
                    $updated['cheque_date']=date('Y-m-d',strtotime($v['cheque_date']));
                }
                PaymentsRecipient::where(["payment_id"=>$id ,'person_id'=>$v['person_id']])->update($updated);
            }
        }
        if($payments->exchange_type == 3) {
            foreach($items as $k => $v){
                $updated=['cheque_account_number'=>null,'status'=>$v['status']];
                if($v['cheque_account_number'] != null && $v['cheque_account_number'] != "" ){
                    $updated['cheque_account_number']=$v['cheque_account_number'];
                }
                if($v['cheque_date'] != null && $v['cheque_date'] != "" ){
                    $updated['cheque_date']=date('Y-m-d',strtotime($v['cheque_date']));
                }
                PaymentsRecipient::where(["payment_id"=>$id ,'person_id'=>$v['person_id']])->update($updated);
            }
        }
        if (empty($response["msg"])) {
            \Log\Model\Log::saveNewLog('PAYMENTS_RECIPIENT_UPDATED',trans('sponsorship::application.update on payment recipients data')  . ' : ' . $id);
        }

        return response()->json($response);
    }

    // save bank account of recipient on person_bank table
    public function saveAccount(Request $request){

        $response=['status' => true];

        $Account=\Common\Model\PersonModels\PersonBank::where(['bank_id'=>$request->get('bank_id'),'account_number'=>$request->get('account_number')])->first();

        if($Account){
            if($Account->person_id != $request->person_id){
                $response['status']=false;
            }
        }
        else{
            \Common\Model\PersonModels\PersonBank::create(['person_id'=>$request->person_id,'bank_id' =>$request->get('bank_id') ,
                'account_number' => $request->get('account_number')]);

            $person = \Common\Model\Person::fetch(array('id' => $request->person_id));
            $name=$person->first_name.' '.$person->second_name.' '.$person->third_name.' '.$person->last_name;
            \Log\Model\Log::saveNewLog('CASES_UPDATED',trans('sponsorship::application.saved the bank account number') . ' "'.$request->get('account_number').
                '" '. ' لـ '. ' "'.$name. '" ');

        }

        return response()->json($response);

    }

    // update status of payment recipient
    public function updateRecipientStatus(Request $request, $id){

//            $this->authorize('update',Payments::class);
        $action = $request->action;
        $newStatus = $request->value;
        $response = array();
        $update=true;

        if($action == 'all'){
            if($newStatus == 1){
                $counts = PaymentsRecipient::where(["payment_id"=>$id,'status'=> 0])->count();
            }else{
                $counts = PaymentsRecipient::where(["payment_id"=>$id,'status'=> 1])->count();
            }

            if($counts == 0){
                return response()->json(["status" =>'failed',"msg" => trans('sponsorship::application.no recipient has the prev status')]);
            }

            PaymentsRecipient::where(["payment_id"=>$id])->update(['status'=> $newStatus]);

        }else{
            $recipients = $request->recipients;
            foreach ($recipients as $item){
                $recipient =  PaymentsRecipient::where(["id"=>$item])->first();
                $recipient->status = $newStatus;
                $recipient->save();
            }
        }

        if($update){
            \Log\Model\Log::saveNewLog('PAYMENTS_RECIPIENT_UPDATED',trans('sponsorship::application.Edited to payment recipient cases') . $id);
            $response["status"]= 'success';
            $response["msg"]= trans('sponsorship::application.The row is updated');
        }
        else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sponsorship::application.There is no update');
        }
        return response()->json($response);
    }

    // empty or delete payment
    public function destroy($id,Request $request)
    {
        if($request->action =='empty'){
            \Log\Model\Log::saveNewLog('PAYMENTS_DELETED',trans('sponsorship::application.deleted the payment'));
            if(PaymentsRecipient::where('payment_id',$id)->delete() && PaymentsCases::where('payment_id',$id)->delete() ) {
                \Log\Model\Log::saveNewLog('PAYMENTS_DELETED',trans('sponsorship::application.emptied the payment') );
                return response()->json(['status' => 'success' , 'msg' => trans('common::application.action success')]);
            }
        }else{
            PaymentsRecipient::where('payment_id',$id)->delete() ;
            PaymentsCases::where('payment_id',$id)->delete();
            if(Payments::destroy($id))
            {
                \Log\Model\Log::saveNewLog('PAYMENTS_DELETED',trans('sponsorship::application.deleted the payment'));
                return response()->json(['status' => 'success' , 'msg' => trans('common::application.action success')]);
            }
        }
        return response()->json(['status' =>'failed','msg'=>trans('common::application.An error occurred during during process') ]);
    }
    public function deleteGroup(Request $request)
    {
        DB::beginTransaction();
        $del_1 =PaymentsRecipient::whereIn('payment_id',$request->ids)->delete() ;
        $del_2 =PaymentsCases::whereIn('payment_id',$request->ids)->delete();

        if($request->action =='delete'){
            Payments::whereIn('id',$request->ids)->delete();
            \Log\Model\Log::saveNewLog('PAYMENTS_DELETED',trans('sponsorship::application.The user emptied and deleted a set of payments'));
            DB::commit();
            return response()->json(['status' => 'success' , 'msg' => trans('sponsorship::application.The row is deleted from db')]);
        }else{
            if($del_1 && $del_2) {
                \Log\Model\Log::saveNewLog('PAYMENTS_DELETED',trans('sponsorship::application.The user emptied a set of payments') );
                DB::commit();
                return response()->json(['status' => 'success' , 'msg' => trans('sponsorship::application.The row is emptied from db') ]);
            }
        }

        DB::rollBack();
        return response()->json(['status' =>'failed','msg'=>trans('common::application.An error occurred during during process')]);
    }

    // get documents of payment
    public function getPaymentDocuments($id,Request $request)
    {
        $this->authorize('view',Payments::class);
        $entry = Payments::findorfail($id);

        $user = \Auth::user();
        $is_mine = false;

        if($entry->organization_id == $user->organization_id){
            $is_mine = true;
        }
        return response()->json(['is_mine'=>$is_mine,'data' =>PaymentsDocuments::getDocument($id,$request->person_id)]);
    }

    // store documents of payment
    public function storePaymentDocument(Request $request)
    {
        $this->authorize('create',Payments::class);

        $document = new PaymentsDocuments();
        $document->created_at =  date("Y-m-d H:i:s");

        $fillable = ['payment_id' , 'attachment_type' ,'document_id','person_id'];
        foreach ($fillable as $col){
            $document->$col =  $request->$col;
        }

        if($document->person_id){
            $document->attachment_type = 4;
        }

        if( $document->save()) {
            \Log\Model\Log::saveNewLog('PAYMENTS_DOCUMENTS_CREATED',trans('sponsorship::application.implemented a new attachment to the payment') );
            return response()->json(array(
                'status' =>'success',
                'msg' => trans('sponsorship::application.The row is inserted to db')
            ));
        }

        return response()->json(array(
            'status' =>'failed',
            'msg' =>trans('sponsorship::application.The row is not deleted from db')
        ));

    }

    // delete documents of payment
    public function deleteDocument($id,Request $request)
    {

        $this->authorize('delete',Payments::class);

        $where=['payment_id'=>$request->payment_id, 'document_id' =>$id];

        if($request->person_id){
            $where['person_id']=$request->person_id;
        }

        if(PaymentsDocuments::where($where)->delete())
        {
            \Log\Model\Log::saveNewLog('PAYMENTS_DOCUMENTS_DELETED',trans('sponsorship::application.Deleted payment attachment'));
            return response()->json(array(
                'status' =>'success',
                'msg' => trans('sponsorship::application.The row is deleted from db')
            ));
        }

        return response()->json(array(
            'status' =>'failed',
            'msg' =>trans('sponsorship::application.The row is not deleted from db')
        ));

    }

    // get statistic of payments recipient according payment_ids
    public function result(Request $request)
    {

        $filter = $request->all();
        $ids=$filter['payment_ids'];
        $pIds = "".join(",",$ids)."";

        $query = PaymentsRecipient::result($request->all());
        $query->map(function ($subQuery) use($pIds){

            $amount=0;
            $total_sh_mot=0;
            $id = $subQuery->person_id;

            $total= \DB::select("select SUM(`amount`) as amount   FROM `char_payments_recipient` where `payment_id` IN($pIds) and `person_id` = $id  ");
            $total_shekel_amount= \DB::select("select SUM(`shekel_amount`) as amount   FROM `char_payments_recipient` where `payment_id` IN($pIds) and `person_id` = $id  ");

            if($total){
                $amount=$total[0]->amount;
            }

            if($total_shekel_amount){
                $total_sh_mot=$total_shekel_amount[0]->amount;
            }

            $subQuery->payment_value =$amount."";
            $subQuery->payment_value_shekel =$total_sh_mot."";
            return $subQuery;
        });

        return response()->json($query);
    }

    // get district of payments recipient according payment_ids
    public function districtRange(Request $request)
    {
        return response()->json(['size'=>sizeof(PaymentsRecipient::districtRange($request->all()))]);
    }

    // get the  payments recipient merged according payment_ids
    public function merge(Request $request)
    {
        $governarate = $districts=[];
        $ids= $request->ids;

        $items = \DB::table('char_payments_recipient')
            ->join('char_persons','char_persons.id',  '=', 'char_payments_recipient.person_id')
            ->whereIn('payment_id',$ids)
            ->orderBy('char_persons.country')->orderBy('char_persons.governarate')
            ->orderBy('char_persons.city')->orderBy('char_persons.location_id')
            ->orderBy('char_persons.first_name')->orderBy('char_persons.second_name')
            ->orderBy('char_persons.third_name')->orderBy('char_persons.last_name')
            ->groupBy('person_id')
            ->selectRaw('char_payments_recipient.person_id, char_persons.country,char_persons.governarate,char_persons.city,char_persons.location_id')->get();

        $cheque_number=$request->cheque_number;
        $cheque_date=date('Y-m-d',strtotime($request->cheque_date));

        PaymentsRecipient::whereIn('payment_id',$ids)->update(['cheque_date'=>$cheque_date]);

        foreach ($items as $key=>$value){
            PaymentsRecipient::whereRaw("person_id = $value->person_id AND payment_id IN (".join(",",$ids).")")->update(['cheque_account_number'=>$cheque_number]);
            $cheque_number++;

            if(!is_null($value->governarate)){
                if(!in_array($value->governarate,$governarate)){
                    $governarate[]=$value->governarate;
                }
            }
        }

        if(sizeof($governarate) >0 ){
            $districts=LocationI18n::whereIn('location_id',$governarate)->get();
        }
        return response()->json(['status' => true,'districts' =>$districts]);
    }

    // save date of recipient (cheque_date , cheque_number , bank ) according filters
    public function saveCheques(Request $request){
        return response()->json(PaymentsRecipient::saveCheques($request->all()));
    }

    // export pdf or excel sheet of payment recipient
    public function export(Request $request){

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $target= $request->target;

        if($target == 'pdf'){

            $items=PaymentsRecipient::OfSetExport($request->all());
//            return response()->json(['status'=>false,$items]);
            if(sizeof($items) == 0){
                return response()->json(['status'=>false]);
            }

            $dirName = storage_path('app/archiveCheques');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }
            $html='';
            PDF::SetTitle(trans('sponsorship::application.cheque'));
            PDF::SetFont('aealarabiya', '', 18);
            PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);
            PDF::SetMargins(5, 15, 5);
//            PDF::SetMargins(PDF_MARGIN_LEFT, 20, PDF_MARGIN_RIGHT);

            foreach ($items as $value) {
                $value=(array)$value;
                $address= '  ';

                if(!is_null($value['country'])){
                    $address = $value['country'];
                }

                if(!is_null($value['district'])){
                    $address .= ' - ' .$value['district'];
                }
                if(!is_null($value['city'])){
                    $address .= ' - ' .$value['city'];
                }
                if(!is_null($value['location'])){
                    $address .= ' - ' .$value['location'];
                }

                if(!is_null($value['street_address'])){
                    $address .= ' - ' .$value['street_address'];
                }


                $html = '<!doctype html>
                 <html lang="ar"> 
                     <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                        <style>
                          table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                          * , body {font-weight:normal !important; font-size: 10px;}
                          .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                          .bolder-content{text-align: right !important; vertical-align: middle!important;}
                          .vertical-middle{ vertical-align: middle !important;}
                          </style>
                    </head>
                    <body style="font-weight: normal !important;">'.
                    '<table cellpadding="pixels" style="display: table; width: 100%; border-collapse: collapse;border-spacing: 0; ">'.
                    '<tr>
                              <td colspan="2" style=" height: 20px ;  padding: 12px;  border:  none; "></td>
                              <td colspan="2" class="vertical-middle" style=" height: 20px ; padding: 15px 0 12px 0; text-align: right !important; font-weight: bold;">'.
                    trans('sponsorship::application.receipt_no') . '  :  '. $value['cheque_account_number'].
                    '</td></tr><tr><td  colspan="2" class="vertical-middle"
                                style=" height: 20px ; padding: 15px 0 12px 0; text-align: right !important; font-weight: bold;">'.
                    trans('sponsorship::application.name').'  :  '.
                    $value['name'].
                    '</td>'.
                    '<td style=" height: 20px ; padding: 15px 0 12px 0; font-weight: bold; text-align: right !important;">'.
                    trans('sponsorship::application.id_card_number') . '  :  '. $value['id_card_number'].
                    '</td>'.'</tr>'.
                    '<tr><td colspan="2" style=" height: 20px ; padding: 15px 0 15px 0; font-weight: bold; text-align: right !important;">'.
                    trans('sponsorship::application.address'). '  :  '. $address.'</td></tr>'.
                    '<tr><td colspan="2" style=" height: 20px ; padding: 15px 0 15px 0; font-weight: bold; text-align: right !important;">'.
                    trans('sponsorship::application.mobile'). '  :  '. $value['mobile'].'</td></tr>'.
                    '<tr><td colspan="4">
                            <table  cellpadding="pixels" style="   width:100%
                                            display: table;
                                            text-align: center;
                                            margin-bottom: 20px;
                                            border-collapse: collapse;
                                            border-spacing: 0;
                                          ">
                                <thead>
                                <tr style="height:15px; line-height: 23px; ">
                                    <th class="heading-style" style="width: 40px; ">'.trans('sponsorship::application.orphan_id') .'</th>
                                    <th class="heading-style" style="width: 120px; ">'.trans('sponsorship::application.name') .'</th>
                                    <th class="heading-style" style="width: 135px; ">'.trans('sponsorship::application.sponsor_name') .'</th>
                                    <th class="heading-style" style="width: 50px; ">'.trans('sponsorship::application.for') .'</th>
                                    <th class="heading-style" style="width: 40px; ">'.trans('sponsorship::application.amount') .'</th>
                                </tr>
                                </thead> <tbody>';

                $size =sizeof($value['individual']);

                if($size > 10){
                    foreach ($value['individual'] as $row) {

                        if($request->payment_exchange ==0 ){
                            $amount=$value['payment_value'];
                        }else{
                            $amount=$value['payment_value_shekel'];
                        }

                        $html .='<tr  class="vertical-middle" style=" line-height: 25px;">
                                  <td class="vertical-middle" style=" text-align: center !important;height: 8px ; border: 1px solid black;
                                              font-weight: normal;  ">'. $row['sponsor_number']. '</td>
                                  <td class="vertical-middle" style="overflow: hidden; word-break: break-all; text-align: center !important;height: 8px ;
                                      border: 1px solid black; font-weight: 100;">'.$row['full_name'].'</td>
                                    <td   class="vertical-middle"style="  height: 8px ; border: 1px solid black; ">'.
                            $value['sponsor_name'].'</td>
                                    <td   class="vertical-middle" style="  text-align: center !important;height: 8px ; border: 1px solid black; ">'.
                            $row['category_name'].'</td>
                                    <td  class="vertical-middle"style="  height: 8px ; border: 1px solid black;  ">'. $amount.'</td>
                                </tr>';
                    }
                }
                else{

                    foreach ($value['individual'] as $row) {
                        $row=(array)$row;
                        if($request->payment_exchange ==0 ){
                            $amount=$row['amount'];
                        }else{
                            $amount=$row['shekel_amount'];
                        }

                        $html .='<tr  class="vertical-middle"style=" line-height: 25px;">
                                    <td  class="vertical-middle"style=" text-align: center !important;height: 10px ; border: 1px solid black;
                                              font-weight: normal;  ">'. $row['sponsor_number']. '</td>
                                    <td   class="vertical-middle"style="overflow: hidden; word-break: break-all; 
                                             text-align: center !important;height: 10px ; border: 1px solid black; 
                                             font-weight: 100;">'.$row['full_name'].'</td>
                                    <td   class="vertical-middle"style="  height: 10px ; border: 1px solid black; ">'.
                            $value['sponsor_name'].'</td>
                                    <td   class="vertical-middle" style="  text-align: center !important;height: 10px ; border: 1px solid black; ">'.
                            $row['category_name'].'</td>
                                    <td   class="vertical-middle"style="  height: 10px ; border: 1px solid black;  ">'. number_format((float)$amount, 2, '.', '').'</td>
                                </tr>';
                    }
                    $length =  (10 - sizeof($value['individual'])) ;
                    for ($x = 0; $x <= $length ; $x++) {
                        $html .='<tr  class="vertical-middle"style=" line-height: 25px;">
                                    <td  style="height: 10px ;   border: 1px solid black;">  </td>
                                    <td  style="height: 10px ;   border: 1px solid black;">  </td>
                                    <td  style="height: 10px ;   border: 1px solid black;">  </td>
                                    <td  style="height: 10px ;   border: 1px solid black;">  </td>
                                    <td  style="height: 10px ;   border: 1px solid black;">  </td>
         
                                 </tr>';
                    }

                }

                if($request->payment_exchange ==0 ){
                    $total=$value['amount'];
                }else{
                    $total=$value['shekel_amount'];
                }

                $html .=         '<tr style="height:15px; line-height: 23px; ">
                                    <td  colspan="2" style="border: none"> </td>
                                    <td  colspan="2" class="heading-style">'.trans('sponsorship::application.sub total') .'</td>
                                    <td  class="heading-style">'.number_format((float)$total, 2, '.', '').'</td>
                                </tr>';

                $html.='</tbody> </table></td> </tr>';

                $html .= '<tr  class="vertical-middle"style=" line-height: 35px;"> 
                           <td colspan="5" style="text-align: right !important;  height: 25px ;padding: 10px 0 10px 0;  font-weight: bold;">'
                    .trans('sponsorship::application.notes') .'   : ......................................................................................................</td>
</tr>';
                $html .= '</table> <table>';
                $html .=
                    '<tr>
    <td class="bolder-content vertical-middle" style="text-align: right !important; height: 20px ;  padding: 10px 0 15px 0;   font-weight: bold;">'.
                    trans('sponsorship::application.recipients_name').' : .........................</td>
    <td   style="text-align: right !important; height: 20px ;  padding: 10px 0 15px 0;  font-weight: bold;">  '.
                    trans('sponsorship::application.creator').' : ........................</td>
    <td   style="text-align: right !important; height: 20px ;  padding: 10px 0 15px 0;  font-weight: bold;">  '.
                    trans('sponsorship::application.auditor').' : ........................</td>
</tr>
<tr>
    <td class="vertical-middle " style=" padding: 10px 0 15px 0; font-weight: bold;">'.
                    trans('sponsorship::application.Signature') .' : .........................</td>
    <td  class="vertical-middle " style=" padding: 10px 0 15px 0; font-weight: bold;">' .
                    trans('sponsorship::application.Signature') .' : ..........................</td>
    <td   class="vertical-middle " style=" padding: 10px 0 15px 0; font-weight: bold;">' .
                    trans('sponsorship::application.Signature') .' : .........................</td>
</tr>
';
                $html .= '</table></body></html>';
                PDF::SetFooterMargin(0);
                PDF::AddPage('P','A5');
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
            }

            $token = md5(uniqid());

            if($request->group == true && !is_null($request->title) && $request->title !='' && $request->title !=' ' ){
                ArchivedCheques::create(['title'=> $request->title , 'file_id' => $token ]);
            }
            PDF::Output($dirName.'/export_' . $token . '.pdf', 'F');
            return response()->json(['status'=>true,'download_token' => $token]);

        }
        else if($target == 'signature'){

            $data=PaymentsRecipient::exportRecipients($request->all());
            if(sizeof($data) == 0){
                return response()->json(['status'=>false]);
            }

            $rows=[];
            if(sizeof($data) !=0){
                $token = md5(uniqid());
                $payment_ids = $request->payment_ids;
                $payment_exchange = $request->payment_exchange;
                $payment =Payments::fetchGroup($payment_ids,$payment_exchange);
                \Excel::create('export_' . $token, function ($excel) use ($payment,$data,$payment_exchange) {
                    $excel->sheet(trans('aid::application.signature_sheet'), function($sheet) use ($payment,$data,$payment_exchange){

                        $sheet->setOrientation('landscape');
                        $sheet->setRightToLeft(true);

                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Calibri',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $sheet->setHeight(1,35);
                        $sheet->setWidth(['A'=>7 ,'B'=> 30,'C'=> 13,'D'=> 7,'F'=> 20,'E'=> 13,'G'=> 15, 'H'=>20,'I'=> 25,'J'=> 13]);

                        $sheet->getStyle("A1:AH1")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 12, 'bold'=>  true ]
                        ]);

                        if(!is_null($payment['organization_logo']) && ( $payment['organization_logo'] != '')){
                            $drawing0 = new \PHPExcel_Worksheet_Drawing;
                            $drawing0->setPath( base_path('storage/app/' . $payment['organization_logo'])); //your image path
                            $drawing0->setCoordinates('A1');
                            $drawing0->setWorksheet($sheet);
                            $drawing0->setResizeProportional();
                            $drawing0->setOffsetX($drawing0->getWidth() - $drawing0->getWidth() / 5);
                            $drawing0->setOffsetY(10);

                            // Set height and width
                            $drawing0->setWidth(150);
                            $drawing0->setHeight(150);
                        }
                        if(!is_null($payment['sponsor_logo']) && ( $payment['sponsor_logo'] != '')){

                            $drawing = new \PHPExcel_Worksheet_Drawing;
                            $drawing->setPath( base_path('storage/app/' . $payment['sponsor_logo'])); //your image path
                            $drawing->setCoordinates('H1');
                            $drawing->setWorksheet($sheet);
                            $drawing->setResizeProportional();
                            $drawing->setOffsetX($drawing->getWidth() - $drawing->getWidth() / 5);
                            $drawing->setOffsetY(10);
                            // Set height and width
                            $drawing->setWidth(150);
                            $drawing->setHeight(150);
                        }

                        $sheet->getStyle('A')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('B')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('C')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('D')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('E')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('F')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('G')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('H')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('I')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('J')->getAlignment()->setWrapText(true);

                        $sheet->setCellValue('C2',$payment['organization_name']);
                        $sheet->mergeCells( 'C2:H2' );
                        $sheet->getStyle('C2:H2')->getAlignment()->setVertical('center');
                        $sheet->getStyle('C2:H2')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('C3',trans('sponsorship::application.payment') . ' : '. $payment['payment_category']);
                        $sheet->mergeCells( 'C3:H3' );
                        $sheet->getStyle('C3:H3')->getAlignment()->setVertical('center');
                        $sheet->getStyle('C3:H3')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('C4',$payment['sponsor_name']);
                        $sheet->mergeCells( 'C4:H4' );
                        $sheet->getStyle('C4:H4')->getAlignment()->setVertical('center');
                        $sheet->getStyle('C4:H4')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('A5',trans('aid::application.day') . ' : '. '-----------------');
                        $sheet->mergeCells( 'A5:E5' );
                        $sheet->getStyle('A5:E5')->getAlignment()->setVertical('center');
                        $sheet->getStyle('A5:E5')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('F5',trans('aid::application.date'). ' -----/------/---------- ');
                        $sheet->mergeCells( 'F5:J5' );
                        $sheet->getStyle('F5:O5')->getAlignment()->setVertical('center');
                        $sheet->getStyle('F5:O5')->getAlignment()->setHorizontal( 'center' );

                        $sheet->getStyle("A1:O5")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 12, 'bold'=>  true ]
                        ]);

                        $sheet->setCellValue('A6','#');
                        $sheet->setCellValue('B6',trans('sponsorship::application.name'));
                        $sheet->setCellValue('C6',trans('sponsorship::application.id_card_number'));
                        $sheet->setCellValue('D6',trans('aid::application.family_cnt'));
                        $sheet->setCellValue('E6',trans('sponsorship::application.mobile_no'));

                        if($payment_exchange ==0){
                            $sheet->setCellValue('F6',trans('sponsorship::application.amount') .'  ( ' . $payment['name'] . ' )  ');
                        }else{
                            $sheet->setCellValue('F6','      ' .trans('sponsorship::application.amount') .'    ' .'    ( ' .trans('sponsorship::application.shekel_'). ' )   ');
                        }
                        if($payment['exchange_type'] ==1){
                            $sheet->setCellValue('G6',trans('sponsorship::application.account_number'));

                        }else if($payment['exchange_type'] == 2){
                            $sheet->setCellValue('G6',trans('sponsorship::application.cheque_number'));
                        }else{
                            $sheet->setCellValue('G6',trans('sponsorship::application.bond_number'));
                        }
                        $sheet->setCellValue('H6',trans('sponsorship::application.cheque_date'));
                        $sheet->setCellValue('I6',trans('sponsorship::application.country_name'));
                        $sheet->setCellValue('J6',trans('sponsorship::application.district'));
                        $sheet->setCellValue('K6',trans('sponsorship::application.city_name'));
                        $sheet->setCellValue('L6',trans('sponsorship::application.location'));
                        $sheet->setCellValue('M6',trans('sponsorship::application.city_name'));
                        $sheet->setCellValue('N6',trans('sponsorship::application.mosques'));
                        $sheet->setCellValue('O6',trans('sponsorship::application.street_address'));
                        $sheet->setCellValue('P6',trans('sponsorship::application.Signature'));
                        $sheet->getStyle('A6:P6')->getAlignment()->setVertical('center');
                        $sheet->getStyle('A6:P6')->getAlignment()->setHorizontal( 'center' );

                        $sheet->getStyle("A1:P6")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 12, 'bold'=>  true ]
                        ]);

                        $count = 1;
                        $z = 7;
//                        $sheet->fromArray($rows, null, 'A7', true);

                        foreach ($data as $key => $current) {
                            $sheet->setCellValue('A' . $z, $count);
                            $sheet->setCellValue('B' . $z, $current->name);
                            $sheet->setCellValue('C' . $z, $current->id_card_number );
                            $sheet->setCellValue('D' . $z, 0 );
                            $sheet->setCellValue('E' . $z, $current->mobile );
                            $sheet->setCellValue('F' . $z, $current->amount);
                            $sheet->setCellValue('G' . $z, $current->cheque_account_number );
                            $sheet->setCellValue('H' . $z, $current->cheque_date );
                            $sheet->setCellValue('I' . $z, $current->country );
                            $sheet->setCellValue('J' . $z, $current->district );
                            $sheet->setCellValue('K' . $z, $current->city );
                            $sheet->setCellValue('L' . $z, $current->location );
                            $sheet->setCellValue('M' . $z, $current->mosques );
                            $sheet->setCellValue('N' . $z, $current->street_address );
                            $sheet->setCellValue('O' . $z, ' ');
                            $sheet->getStyle('A'. $z .':O' . $z)->getAlignment()->setHorizontal('center');
                            $sheet->getStyle('A'. $z .':O' . $z)->getAlignment()->setVertical('center');
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $count++;
                            $z++;
                        }

                        $styleArray = array(
                            'font' => array(
                                'bold' => true,
                            ),
                             'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'borders' => array('top'     => array('style' => 'medium'),
                                'left'   => array('style' => 'medium'),
                                'right'  => array('style' => 'medium'),
                                'bottom' => array('style' => 'medium')

                            ),
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'cce6ff')

                            ),
                        );
                        $sheet->getStyle('A6:P6')->applyFromArray($styleArray);

                        $styleArray = array(
                            'borders' => array(
                                'allborders'    => array('style' => 'medium')
                            ),

                        );
                        $max = sizeof($data) + 6;
                        $sheet->getStyle('A6:P'.$max)->applyFromArray($styleArray);

                        $indicies = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O');
                        $indWidth =['A'=>7 ,'B'=> 25,'C'=> 13,'D'=> 7,'F'=> 20,'E'=> 13,'G'=> 10, 'H'=>15,'I'=> 15,'J'=> 15,
                            'K'=> 15,'L'=> 15,'M'=> 15,'N'=> 15,'O'=> 15];

                        foreach ($indicies as $index) {
                            $sheet->getColumnDimension($index)->setAutoSize(false);
                            $sheet->getColumnDimension($index)->setWidth($indWidth[$index]);

                        }
                        $sheet->getRowDimension(1)->setRowHeight(35);
                        $sheet->getRowDimension(2)->setRowHeight(45);
                        $sheet->getRowDimension(5)->setRowHeight(45);
                        $sheet->getRowDimension(6)->setRowHeight(55);
                        $sheet->getRowDimension(1)->setRowHeight(35);
                        $sheet->getRowDimension(2)->setRowHeight(45);
                        $sheet->getRowDimension(5)->setRowHeight(45);
                        $sheet->getRowDimension(6)->setRowHeight(55);

                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['status'=>true,'download_token' => $token]);
            }
            return response()->json(['status'=>false]);

        }
        else if($target == 'map'){

            ini_set('max_execution_time', 0);
            set_time_limit(0);
            ini_set('memory_limit', '1G');

            $payment = Payments::GroupDetail($request->payment_ids);
            $range = PaymentsCases::where(function ($q) use ($request) {
                $q->where('payment_id', '=', $request->payments_id);
                $q->whereNotNull('date_from');
                $q->whereNotNull('date_to');
            })->first();

            $currency =Currency::where('id',  $request->currency_id)->first();
            $currency_name = $currency->name;
            $range_ = '-';
            if($range){
                $range_ = $range->date_from .'- '.$range->date_to ;
            }
            $districts = Payments::getDistrictPaymentGroup($request->payment_ids);

            $dirName = base_path('storage/app/templates/CaseForm');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }

            $total_count = 0;
            foreach ($districts as $k=>$v){
                PDF::reset();
                PDF::SetTitle('كشوفات الاستحقاق');
                PDF::SetFont('aefurat', '', 18);
                PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                PDF::SetAutoPageBreak(TRUE);
                PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);
                PDF::setCellHeightRatio(1.5);
                PDF::SetMargins(5, 15, 5);
                PDF::SetHeaderMargin(5);
                PDF::SetFooterMargin(5);
                PDF::SetAutoPageBreak(TRUE);
//                PDF::setHeaderCallback(function($pdf) {
//                    $header = '<img src="'.storage_path('app/page/header.jpg').'" alt=""/>';
//                    PDF::writeHTMLCell(0, 0, '', 5, $header, 0, 1, 0, true, '', true);
//                });

                $count = sizeof($v->beneficiary);
                $amount = 0;
                $total_count += $count;
                $ceil=ceil($count/17);

                for ($x = 0; $x < $ceil; $x++) {
                    $curOffset = ($x) * 17;
                    $put_total = false;
                    $html = '<!doctype html>
                 <html lang="ar">
                     <head>
                           <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;}
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .cell-style{text-align: center !important; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                    </head>
                    <body style="font-weight: normal !important;">';
                    $html.= '<h2 style="text-align:center;"> </h2>';

                    $html  .=  ' <table border="0" align="center">';
                    $html .= ' <tr style="border: none !important; line-height: 30px;
                              font-size: 12px!important; height: 57px; fi ">
                        <td colspan="8" style="font-size: 14px;"> استحقاق عن الفترة   
                        ('. $range_ .') 
                        </td>
                   </tr>';
                    $html .= ' <tr style="border: none !important; line-height: 30px;
                              font-size: 12px!important; height: 40px; ">
                        <td style="border: 1px solid black"> الجهة الكافلة :  </td>
                        <td style="border: 1px solid black"colspan="2"> ' .$payment['sponsor_name'] . '</td>
                        <td colspan="2">  </td>
                        <td style="border: 1px solid black"> الجهة المشرفة :  </td>
                        <td style="border: 1px solid black"colspan="2">  جمعية دار الكتاب و السنة </td>

                   </tr>
                  <tr style="border: none !important; line-height: 30px;
                              font-size: 12px!important; height: 40px; ">
                        <td style="border: 1px solid black">  المحافظة </td>
                        <td style="border: 1px solid black"colspan="2"> ' .$v->name . '</td>
                        <td colspan="2">   </td>
                        <td style="border: 1px solid black"> التاريخ </td>
                        <td style="border: 1px solid black"colspan="2"> '.date('Y-m-d').' </td>
                   </tr>';
                    $html .= '</table>';
                    $html.= '<br><br>';

                    $index=$curOffset;
                    $html .=  '<table  cellpadding="pixels" style="   width:100%
                                            display: table;
                                            text-align: center;
                                            margin-bottom: 20px;
                                            border-collapse: collapse;
                                            border-spacing: 0;
                                          ">
                                <thead>
                                <tr style="height:40px; line-height: 25px; ">
                                    <th class="heading-style" style="width: 40px; "> رقم<br> اليتيم </th>
                                    <th class="heading-style" style="width: 135px; "><br><br>'.trans('sponsorship::application.orphan_name') .'</th>
                                    <th class="heading-style" style="width: 50px; "> هوية<br> الوصي </th>
                                    <th class="heading-style" style="width: 135px; "><br><br>'.trans('sponsorship::application.guardian_name_') .'</th>
                                    <th class="heading-style" style="width: 65px; "><br><br>'.trans('sponsorship::application.for') .'</th>
                                    <th class="heading-style" style="width: 40px; "> المبلغ<br> بالشيكل </th>
                                    <th class="heading-style" style="width: 100px; "><br><br>'.trans('sponsorship::application.signature') .'</th>
                                </tr>
                                </thead> <tbody>';

                    if($index < sizeof($v->beneficiary)) {
                        $idx = 0;
                        for ($z = 0; $z < 17; $z++) {
                            if($index < $count){
                                $v1 = $v->beneficiary[$index];
                                $sponsor_number = is_null($v1->sponsor_number) ? '-' : $v1->sponsor_number;
                                $case_name      = is_null($v1->case_name) ? '-' : $v1->case_name;
                                $guardian_card  = is_null($v1->guardian_card) ? '-' : $v1->guardian_card;
                                $guardian_name  = is_null($v1->guardian_name) ? '-' : $v1->guardian_name;
                                $shekel_amount  = is_null($v1->shekel_amount) ? '-' : $v1->shekel_amount;
                                $payment_category  = is_null($v1->payment_category) ? '-' : $v1->payment_category;

                                $html .='<tr style="height:20px; line-height: 25px; ">
                                                    <td  class="cell-style" style="width:40px;  ">'.$sponsor_number .'</td>
                                                    <td  class="cell-style" style="width:135px; ">'.$case_name .'</td>
                                                    <td  class="cell-style" style="width:50px;  ">'.$guardian_card.'</td>
                                                    <td  class="cell-style" style="width:135px; ">'.$guardian_name .'</td>
                                                    <td  class="cell-style" style="width:65px;  ">'.$payment_category .'</td>
                                                    <td  class="cell-style" style="width:40px;  ">'.$shekel_amount .'</td>
                                                    <td  class="cell-style" style="width:100px;  "> </td>
                                                </tr>';

                                $amount += $shekel_amount;
                                $index++;
                                $idx++;
                            }
                        }
                    }

                    if(($x == $ceil- 1) && ($idx < 12)){
                        $html .= '</tbody></table>';
                        $put_total = true;
                        $html  .=  ' <br><br><table border="0" align="center">';
                        $html .='<tr style="line-height: 30px;font-size: 12px!important; height: 40px; ">
                                    <th style="border: 1px solid black"> <br />   إجمالي العدد </th>
                                    <th style="border: 1px solid black"> <br />   إجمالي المبالغ </th>
                                   
                             </tr>
                             <tr style="line-height: 30px;font-size: 12px!important; height: 40px;" >
                                    <td style="border: 1px solid black">' .$count . '</td>
                                    <td style="border: 1px solid black"> ' .$amount . '</td>
                                
                            </tr>';
                        $html .= '</table>';
                        $html.= '<br><br><br>';
                        $html  .=  ' <table border="0" align="center">';
                        $html .= ' <tr style="border: none !important; line-height: 30px;
                                          font-size: 12px!important; height: 40px; ">
                                    <td style="border: none" colspan="3"> أمين الصندوق :  </td>
                                    <td colspan="2">  </td>
                                    <td style="border: none" colspan="3"> رئيس الجمعية :  </td>
                               </tr>';
                        $html .= '</table>';
                    }else{
                        $html .= '</tbody></table>';
                    }
                    $html .= '</body></html>';

                    PDF::SetFooterMargin(0);
                    PDF::setFooterCallback(function($pdf) use ($x,$ceil){
                        $html_ = '<p style="text-align:center;">('.'  '. ($x+1) .' / ' . $ceil .'  ) </h3>';
                        PDF::writeHTMLCell(0, 0, '', 260, $html_, 0, 1, 0, true, '', true);
                        $footer = '<img src="'.storage_path('app/page/footer.jpg').'" alt=""/>';
                        PDF::writeHTMLCell(0, 0, '', 270, $footer, 0, 1, 0, true, '', true);
                    });
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

                }

                if($put_total == false){
                    $html = '<!doctype html>
                 <html lang="ar">
                     <head>
                           <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .cell-style{text-align: center !important; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                    </head>
                    <body style="font-weight: normal !important;">';
                    $html.= '<h3 style="text-align:center;"> </h3>';
                    $html.= '<h3 style="text-align:center;"> </h3>';
                    $html.= '<h3 style="text-align:center;"> </h3>';
//                    <th style="border: 1px solid black"> <br />    نوع الكفالة </th>
//                    <td style="border: 1px solid black"> ' .$payment_category . ' <br /></td>
                    $html  .=  ' <table border="0" align="center">';
                    $html .='<tr style="line-height: 30px;font-size: 12px!important; height: 40px; ">
                        <th style="border: 1px solid black"> <br />   إجمالي العدد </th>
                        <th style="border: 1px solid black"> <br />   إجمالي المبالغ </th>
                      
                 </tr>
                 <tr style="line-height: 30px;font-size: 12px!important; height: 40px;" >
                        <td style="border: 1px solid black">' .$count . ' <br /></td>
                        <td style="border: 1px solid black"> ' .$amount . ' <br /></td>
                       
                </tr>';
                    $html .= '</table>';
                    $html.= '<br><br><br>';
                    $html  .=  ' <table border="0" align="center">';
                    $html .= ' <tr style="border: none !important; line-height: 30px;
                              font-size: 12px!important; height: 40px; ">
                        <td style="border: none" colspan="3"> أمين الصندوق :  </td>
                        <td colspan="2">  </td>
                        <td style="border: none" colspan="3"> رئيس الجمعية :  </td>
                   </tr>';
                    $html .= '</table>';

                    $html .= '</body></html>';
                    PDF::SetFooterMargin(0);
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

                    PDF::setFooterCallback(function($pdf) use ($x,$ceil){
                        $footer = '<img src="'.storage_path('app/page/footer.jpg').'" alt=""/>';
                        PDF::writeHTMLCell(0, 0, '', 270, $footer, 0, 1, 0, true, '', true);
                    });
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                }

                PDF::Output($dirName.'/'.$v->name.'.pdf', 'F');

            }
            //***************************************//
            // total pdf page

            PDF::reset();
            PDF::SetTitle('كشوفات الاستحقاق');
            PDF::SetFont('aefurat', '', 18);
//            PDF::SetFont('aefurat', '', 18 , '', 'default', true );
//        PDF::SetFont('dejavusans', '', 18 , '', 'default', true );
            PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            PDF::SetAutoPageBreak(TRUE);
            PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);
            PDF::setCellHeightRatio(1.5);
            PDF::SetMargins(5, 15, 5);
            PDF::SetHeaderMargin(5);
            PDF::SetFooterMargin(5);
            PDF::SetAutoPageBreak(TRUE);
//            PDF::setHeaderCallback(function($pdf) {
//                $header = '<img src="'.storage_path('app/page/header.jpg').'" alt=""/>';
//                PDF::writeHTMLCell(0, 0, '', 5, $header, 0, 1, 0, true, '', true);
//            });
//            PDF::setFooterCallback(function($pdf) {
//                $footer = '<img src="'.storage_path('app/page/footer.jpg').'" alt=""/>';
//                PDF::writeHTMLCell(0, 0, '', 270, $footer, 0, 1, 0, true, '', true);
//            });

            $html = '<!doctype html>
                 <html lang="ar">
                     <head>
                           <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .cell-style{text-align: center !important; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                    </head>
                    <body style="font-weight: normal !important;">';
            $html.= '<h3 style="text-align:center;"> </h3>';
            $html.= '<h3 style="text-align:center;"> </h3>';
            $html.= '<h3 style="text-align:center;"> </h3>';
            $index=$curOffset;
            $html .=  '<table  cellpadding="pixels" style="   width:100%
                                            display: table;
                                            text-align: center;
                                            margin-bottom: 20px;
                                            border-collapse: collapse;
                                            border-spacing: 0;
                                          ">
                                <thead>
                                <tr style="height:40px; line-height: 25px; ">
                                    <th class="heading-style" style="width: 50px; "> م </th>
                                    <th class="heading-style" style="width: 170px; "> البيان </th>
                                    <th class="heading-style" style="width: 80px; "> العدد </th>
                                    <th class="heading-style" style="width: 130px; "> المبلغ ( '.$currency_name .' )</th>
                                    <th class="heading-style" style="width: 130px; "> المبلغ بالشيكل</th>
                                </tr>
                                </thead> <tbody>';

            foreach ($payment['category_map'] as $key=>$value){
                $name = is_null($value->name) ? '-' : $value->name;
                $ben_cases      = is_null($value->ben_cases) ? '0' : $value->ben_cases;
                $amount      = is_null($value->amount) ? '0' : $value->amount;
                $shekel_amount      = is_null($value->shekel_amount) ? '0' : $value->shekel_amount;
                $html .='<tr style="height:20px; line-height: 25px; ">
                                                    <td  class="cell-style" style="width:50px;  ">'.( $key + 1 ) .'</td>
                                                    <td  class="cell-style" style="width:170px; ">'.$name .'</td>
                                                    <td  class="cell-style" style="width:80px; ">'.$ben_cases .'</td>
                                                    <td  class="cell-style" style="width:130px;  ">'.$amount .'</td>
                                                    <td  class="cell-style" style="width:130px;  ">'.$shekel_amount .'</td>
                                                </tr>';

                $amount += $shekel_amount;
                $index++;
                $idx++;
            }
            $html .= '</tbody></table>';
            $html.= '<br><br>';

            $html  .=  ' <table border="0" align="center">';
            $payment['shekel_amount'] = 0;
            $html .='<tr style="line-height: 30px;font-size: 12px!important; height: 40px; ">
                        <th style="border: 1px solid black" rowspan="2">  الإجماليات<br /></th>
                        <th style="border: 1px solid black" > <br />   العدد </th>
                        <th style="border: 1px solid black"> <br />  المبلغ بالشيكل </th>
                 </tr>
                 <tr style="line-height: 30px;font-size: 12px!important; height: 40px;" >
                        <td style="border: 1px solid black">' .$total_count . '</td>
                        <td style="border: 1px solid black">' .$payment['shekel_amount'] . '</td>
                </tr>';
            $html .= '</table>';
            $html.= '<br><br><br>';
            $html  .=  ' <table border="0" align="center">';
            $html .= ' <tr style="border: none !important; line-height: 30px;
                              font-size: 12px!important; height: 40px; ">
                        <td style="border: 1px solid black" colspan="3"> أمين الصندوق :  </td>
                        <td colspan="2">  </td>
                        <td style="border: 1px solid black" colspan="3"> رئيس الجمعية :  </td>
                   </tr>';

            $html .= '</table>';
            $html .= '</body></html>';
            PDF::SetFooterMargin(0);
            PDF::AddPage('P','A4');
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
            PDF::Output($dirName.'/'.'الاجمالي'.'.pdf', 'F');


            //***************************************//

            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');

            $zip = new \ZipArchive;
            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                foreach ( glob( $dirName . '/*' ) as $fileName )
                {
                    $file = basename( $fileName );
                    $zip->addFile( $fileName, $file );
                }

                $zip->close();
                array_map('unlink', glob("$dirName/*.*"));
                rmdir($dirName);
            }

            return response()->json(['status'=>true,'download_token' => $token]);

        }
        else{
            $data = PaymentsCases::exportCaseRecipientWithOptions($request->all());

//        return $data;
//        organizations_name
            $keys=["case_name","id_card_number","category_name","sponsor_name","sponsor_number","guardian_name","guardian_id_card_number",
                'country_name','governarate_name','city_name','location_name','mosque_name','street_address',
                "payment_date","currency_name", "amount","amount_after_discount","shekel_amount_before_discount","shekel_amount",
                "recipient_amount","recipient_amount_after_discount","recipient_shekel_amount_before_discount","recipient_shekel_amount",
                "date_from","date_to","recipients_name",'recipient_id_card_number',"phone","primary_mobile",
                "secondary_mobile","guardian_phone","guardian_primary_mobile","guardian_secondary_mobile","cheque_account_number","cheque_date","rec_status"];

            if(sizeof($data) !=0){
                $rows=[];
                foreach($data as $key =>$value){
                    $rows[$key][trans('sponsorship::application.#')]=$key+1;
                    foreach($keys as $ke){
                        $f_ke=null;
                        $vlu = is_null($value->$ke) ? '-' : $value->$ke;

                        if(substr( $ke, 0, 7 ) === "mother_"){
                            $f_ke = trans('sponsorship::application.'.substr($ke,7))." ( ".trans('sponsorship::application.mother')  ." ) ";
                        }elseif(substr( $ke, 0, 7 ) === "father_"){
                            $f_ke = trans('sponsorship::application.'.substr($ke,7))." ( ".trans('sponsorship::application.father')  ." ) ";
                        }elseif(substr( $ke, 0, 9 ) === "guardian_"){
                            $f_ke = trans('sponsorship::application.'.substr($ke,9))." ( ".trans('sponsorship::application.guardian')  ." ) ";
                        }elseif(substr( $ke, 0, 10 ) === "recipient_"){
                            $f_ke = trans('sponsorship::application.'.substr($ke,10))." ( ".trans('sponsorship::application.recipient')  ." ) ";
                        }else{
                            $f_ke = trans('sponsorship::application.' . $ke);
                        }
                        $rows[$key][$f_ke]= $vlu;

//                        if($k != 'case_id' && $k != 'id' && $k != 'sponsor_id'&& $k != 'flag'&& $k != 'date_flag'&& $k != 'person_id'){
//                            if($k =='sponsor_number'){
//                                $rows[$key][trans('sponsorship::application.sponsorship_number')]= $v;
//                            }
//                            else{
//                                $rows[$key][trans('sponsorship::application.' . $k)]= $v;
//                            }
//                        }
                    }
                }
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($rows) {
                    $excel->sheet(trans('sponsorship::application.recipients report') , function($sheet) use($rows) {
                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Calibri',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
//                    $sheet->setWidth(['A'=>7 ,'B'=> 15,'C'=> 30,'D'=> 15,'F'=> 30,'E'=> 23,'G'=> 15]);
                        $style = [
                             'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold'      =>  true
                            ]
                        ];

                        $sheet->getStyle("A1:AC1")->applyFromArray($style);
                        $sheet->setHeight(1,30);

                        $style = [
                             'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold' => false
                            ]
                        ];

                        $sheet->getDefaultStyle()->applyFromArray($style);
                        $sheet->fromArray($rows);

                    });
                })
                    ->store('xlsx', storage_path('tmp/'));
                return response()->json(['status'=>true,'download_token' => $token]);
            }
        }

        return response()->json(['status'=>false]);
    }

    public function map(Request $request)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $target= $request->target;

        if($target == 'pdf'){

            $items=PaymentsRecipient::OfSetExport($request->all());
//            return response()->json(['status'=>false,$items]);
            if(sizeof($items) == 0){
                return response()->json(['status'=>false]);
            }

            $dirName = storage_path('app/archiveCheques');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }
            $html='';
            PDF::SetTitle(trans('sponsorship::application.cheque'));
            PDF::SetFont('aealarabiya', '', 18);
            PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);
            PDF::SetMargins(5, 15, 5);
//            PDF::SetMargins(PDF_MARGIN_LEFT, 20, PDF_MARGIN_RIGHT);

            foreach ($items as $value) {
                $value=(array)$value;
                $address= '  ';

                if(!is_null($value['country'])){
                    $address = $value['country'];
                }

                if(!is_null($value['district'])){
                    $address .= ' - ' .$value['district'];
                }
                if(!is_null($value['city'])){
                    $address .= ' - ' .$value['city'];
                }
                if(!is_null($value['location'])){
                    $address .= ' - ' .$value['location'];
                }

                if(!is_null($value['street_address'])){
                    $address .= ' - ' .$value['street_address'];
                }


                $html = '<!doctype html>
                 <html lang="ar"> 
                     <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                        <style>
                          table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                          * , body {font-weight:normal !important; font-size: 10px;}
                          .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                          .bolder-content{text-align: right !important; vertical-align: middle!important;}
                          .vertical-middle{ vertical-align: middle !important;}
                          </style>
                    </head>
                    <body style="font-weight: normal !important;">'.
                    '<table cellpadding="pixels" style="display: table; width: 100%; border-collapse: collapse;border-spacing: 0; ">'.
                    '<tr>
                              <td colspan="2" style=" height: 20px ;  padding: 12px;  border:  none; "></td>
                              <td colspan="2" class="vertical-middle" style=" height: 20px ; padding: 15px 0 12px 0; text-align: right !important; font-weight: bold;">'.
                    trans('sponsorship::application.receipt_no') . '  :  '. $value['cheque_account_number'].
                    '</td></tr><tr><td  colspan="2" class="vertical-middle"
                                style=" height: 20px ; padding: 15px 0 12px 0; text-align: right !important; font-weight: bold;">'.
                    trans('sponsorship::application.name').'  :  '.
                    $value['name'].
                    '</td>'.
                    '<td style=" height: 20px ; padding: 15px 0 12px 0; font-weight: bold; text-align: right !important;">'.
                    trans('sponsorship::application.id_card_number') . '  :  '. $value['id_card_number'].
                    '</td>'.'</tr>'.
                    '<tr><td colspan="2" style=" height: 20px ; padding: 15px 0 15px 0; font-weight: bold; text-align: right !important;">'.
                    trans('sponsorship::application.address'). '  :  '. $address.'</td></tr>'.
                    '<tr><td colspan="2" style=" height: 20px ; padding: 15px 0 15px 0; font-weight: bold; text-align: right !important;">'.
                    trans('sponsorship::application.mobile'). '  :  '. $value['mobile'].'</td></tr>'.
                    '<tr><td colspan="4">
                            <table  cellpadding="pixels" style="   width:100%
                                            display: table;
                                            text-align: center;
                                            margin-bottom: 20px;
                                            border-collapse: collapse;
                                            border-spacing: 0;
                                          ">
                                <thead>
                                <tr style="height:15px; line-height: 23px; ">
                                    <th class="heading-style" style="width: 40px; ">'.trans('sponsorship::application.orphan_id') .'</th>
                                    <th class="heading-style" style="width: 120px; ">'.trans('sponsorship::application.name') .'</th>
                                    <th class="heading-style" style="width: 135px; ">'.trans('sponsorship::application.sponsor_name') .'</th>
                                    <th class="heading-style" style="width: 50px; ">'.trans('sponsorship::application.for') .'</th>
                                    <th class="heading-style" style="width: 40px; ">'.trans('sponsorship::application.amount') .'</th>
                                </tr>
                                </thead> <tbody>';

                $size =sizeof($value['individual']);

                if($size > 10){
                    foreach ($value['individual'] as $row) {

                        if($request->payment_exchange ==0 ){
                            $amount=$value['payment_value'];
                        }else{
                            $amount=$value['payment_value_shekel'];
                        }

                        $html .='<tr  class="vertical-middle" style=" line-height: 25px;">
                                  <td class="vertical-middle" style=" text-align: center !important;height: 8px ; border: 1px solid black;
                                              font-weight: normal;  ">'. $row['sponsor_number']. '</td>
                                  <td class="vertical-middle" style="overflow: hidden; word-break: break-all; text-align: center !important;height: 8px ;
                                      border: 1px solid black; font-weight: 100;">'.$row['full_name'].'</td>
                                    <td   class="vertical-middle"style="  height: 8px ; border: 1px solid black; ">'.
                            $value['sponsor_name'].'</td>
                                    <td   class="vertical-middle" style="  text-align: center !important;height: 8px ; border: 1px solid black; ">'.
                            $row['category_name'].'</td>
                                    <td  class="vertical-middle"style="  height: 8px ; border: 1px solid black;  ">'. $amount.'</td>
                                </tr>';
                    }
                }
                else{

                    foreach ($value['individual'] as $row) {
                        $row=(array)$row;
                        if($request->payment_exchange ==0 ){
                            $amount=$row['amount'];
                        }else{
                            $amount=$row['shekel_amount'];
                        }

                        $html .='<tr  class="vertical-middle"style=" line-height: 25px;">
                                    <td  class="vertical-middle"style=" text-align: center !important;height: 10px ; border: 1px solid black;
                                              font-weight: normal;  ">'. $row['sponsor_number']. '</td>
                                    <td   class="vertical-middle"style="overflow: hidden; word-break: break-all; 
                                             text-align: center !important;height: 10px ; border: 1px solid black; 
                                             font-weight: 100;">'.$row['full_name'].'</td>
                                    <td   class="vertical-middle"style="  height: 10px ; border: 1px solid black; ">'.
                            $value['sponsor_name'].'</td>
                                    <td   class="vertical-middle" style="  text-align: center !important;height: 10px ; border: 1px solid black; ">'.
                            $row['category_name'].'</td>
                                    <td   class="vertical-middle"style="  height: 10px ; border: 1px solid black;  ">'. number_format((float)$amount, 2, '.', '').'</td>
                                </tr>';
                    }
                    $length =  (10 - sizeof($value['individual'])) ;
                    for ($x = 0; $x <= $length ; $x++) {
                        $html .='<tr  class="vertical-middle"style=" line-height: 25px;">
                                    <td  style="height: 10px ;   border: 1px solid black;">  </td>
                                    <td  style="height: 10px ;   border: 1px solid black;">  </td>
                                    <td  style="height: 10px ;   border: 1px solid black;">  </td>
                                    <td  style="height: 10px ;   border: 1px solid black;">  </td>
                                    <td  style="height: 10px ;   border: 1px solid black;">  </td>
         
                                 </tr>';
                    }

                }

                if($request->payment_exchange ==0 ){
                    $total=$value['amount'];
                }else{
                    $total=$value['shekel_amount'];
                }

                $html .=         '<tr style="height:15px; line-height: 23px; ">
                                    <td  colspan="2" style="border: none"> </td>
                                    <td  colspan="2" class="heading-style">'.trans('sponsorship::application.sub total') .'</td>
                                    <td  class="heading-style">'.number_format((float)$total, 2, '.', '').'</td>
                                </tr>';

                $html.='</tbody> </table></td> </tr>';

                $html .= '<tr  class="vertical-middle"style=" line-height: 35px;"> 
                           <td colspan="5" style="text-align: right !important;  height: 25px ;padding: 10px 0 10px 0;  font-weight: bold;">'
                    .trans('sponsorship::application.notes') .'   : ......................................................................................................</td>
</tr>';
                $html .= '</table> <table>';
                $html .=
                    '<tr>
    <td class="bolder-content vertical-middle" style="text-align: right !important; height: 20px ;  padding: 10px 0 15px 0;   font-weight: bold;">'.
                    trans('sponsorship::application.recipients_name').' : .........................</td>
    <td   style="text-align: right !important; height: 20px ;  padding: 10px 0 15px 0;  font-weight: bold;">  '.
                    trans('sponsorship::application.creator').' : ........................</td>
    <td   style="text-align: right !important; height: 20px ;  padding: 10px 0 15px 0;  font-weight: bold;">  '.
                    trans('sponsorship::application.auditor').' : ........................</td>
</tr>
<tr>
    <td class="vertical-middle " style=" padding: 10px 0 15px 0; font-weight: bold;">'.
                    trans('sponsorship::application.Signature') .' : .........................</td>
    <td  class="vertical-middle " style=" padding: 10px 0 15px 0; font-weight: bold;">' .
                    trans('sponsorship::application.Signature') .' : ..........................</td>
    <td   class="vertical-middle " style=" padding: 10px 0 15px 0; font-weight: bold;">' .
                    trans('sponsorship::application.Signature') .' : .........................</td>
</tr>
';
                $html .= '</table></body></html>';
                PDF::SetFooterMargin(0);
                PDF::AddPage('P','A5');
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
            }

            $token = md5(uniqid());

            if($request->group == true && !is_null($request->title) && $request->title !='' && $request->title !=' ' ){
                ArchivedCheques::create(['title'=> $request->title , 'file_id' => $token ]);
            }
            PDF::Output($dirName.'/export_' . $token . '.pdf', 'F');
            return response()->json(['status'=>true,'download_token' => $token]);

        }
        else if($target == 'signature'){

            $data=PaymentsRecipient::exportRecipients($request->all());
            if(sizeof($data) == 0){
                return response()->json(['status'=>false]);
            }

            $rows=[];
            if(sizeof($data) !=0){
                $token = md5(uniqid());
                $payment_ids = $request->payment_ids;
                $payment_exchange = $request->payment_exchange;
                $payment =Payments::fetchGroup($payment_ids,$payment_exchange);
                \Excel::create('export_' . $token, function ($excel) use ($payment,$data,$payment_exchange) {
                    $excel->sheet(trans('aid::application.signature_sheet'), function($sheet) use ($payment,$data,$payment_exchange){

                        $sheet->setOrientation('landscape');
                        $sheet->setRightToLeft(true);

                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Calibri',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $sheet->setHeight(1,35);
                        $sheet->setWidth(['A'=>7 ,'B'=> 30,'C'=> 13,'D'=> 7,'F'=> 25,'E'=> 13,'G'=> 15, 'H'=>20,'I'=> 25,'J'=> 25,
                            'K'=>25,'L'=> 25,'M'=> 25,'N'=>25,'O'=> 25,'Q'=> 25]);

                        $sheet->getStyle("A1:AH1")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 12, 'bold'=>  true ]
                        ]);

                        if(!is_null($payment['organization_logo']) && ( $payment['organization_logo'] != '')){
                            $drawing0 = new \PHPExcel_Worksheet_Drawing;
                            $drawing0->setPath( base_path('storage/app/' . $payment['organization_logo'])); //your image path
                            $drawing0->setCoordinates('A1');
                            $drawing0->setWorksheet($sheet);
                            $drawing0->setResizeProportional();
                            $drawing0->setOffsetX($drawing0->getWidth() - $drawing0->getWidth() / 5);
                            $drawing0->setOffsetY(10);

                            // Set height and width
                            $drawing0->setWidth(150);
                            $drawing0->setHeight(150);
                        }
                        if(!is_null($payment['sponsor_logo']) && ( $payment['sponsor_logo'] != '')){

                            $drawing = new \PHPExcel_Worksheet_Drawing;
                            $drawing->setPath( base_path('storage/app/' . $payment['sponsor_logo'])); //your image path
                            $drawing->setCoordinates('H1');
                            $drawing->setWorksheet($sheet);
                            $drawing->setResizeProportional();
                            $drawing->setOffsetX($drawing->getWidth() - $drawing->getWidth() / 5);
                            $drawing->setOffsetY(10);
                            // Set height and width
                            $drawing->setWidth(150);
                            $drawing->setHeight(150);
                        }

                        $sheet->setCellValue('C2',$payment['organization_name']);
                        $sheet->mergeCells( 'C2:H2' );
                        $sheet->getStyle('C2:H2')->getAlignment()->setVertical('center');
                        $sheet->getStyle('C2:H2')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('C3',trans('sponsorship::application.payment') . ' : '. $payment['payment_category']);
                        $sheet->mergeCells( 'C3:H3' );
                        $sheet->getStyle('C3:H3')->getAlignment()->setVertical('center');
                        $sheet->getStyle('C3:H3')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('C4',$payment['sponsor_name']);
                        $sheet->mergeCells( 'C4:H4' );
                        $sheet->getStyle('C4:H4')->getAlignment()->setVertical('center');
                        $sheet->getStyle('C4:H4')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('A5',trans('aid::application.day') . ' : '. '-----------------');
                        $sheet->mergeCells( 'A5:E5' );
                        $sheet->getStyle('A5:E5')->getAlignment()->setVertical('center');
                        $sheet->getStyle('A5:E5')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('F5',trans('aid::application.date'). ' -----/------/---------- ');
                        $sheet->mergeCells( 'F5:J5' );
                        $sheet->getStyle('F5:O5')->getAlignment()->setVertical('center');
                        $sheet->getStyle('F5:O5')->getAlignment()->setHorizontal( 'center' );

                        $sheet->getStyle("A1:O5")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 12, 'bold'=>  true ]
                        ]);

                        $sheet->setCellValue('A6','#');
                        $sheet->setCellValue('B6',trans('sponsorship::application.name'));
                        $sheet->setCellValue('C6',trans('sponsorship::application.id_card_number'));
                        $sheet->setCellValue('D6',trans('aid::application.family_cnt'));
                        $sheet->setCellValue('E6',trans('sponsorship::application.mobile_no'));

                        if($payment_exchange ==0){
                            $sheet->setCellValue('F6',trans('sponsorship::application.amount') .'  ( ' . $payment['currency'] . ' )  ');
                        }else{
                            $sheet->setCellValue('F6','      ' .trans('sponsorship::application.amount') .'    ' .'    ( ' .trans('sponsorship::application.shekel_'). ' )   ');
                        }
                        if($payment['exchange_type'] ==1){
                            $sheet->setCellValue('G6',trans('sponsorship::application.account_number'));

                        }else if($payment['exchange_type'] == 2){
                            $sheet->setCellValue('G6',trans('sponsorship::application.cheque_number'));
                        }else{
                            $sheet->setCellValue('G6',trans('sponsorship::application.bond_number'));
                        }
                        $sheet->setCellValue('H6',trans('sponsorship::application.cheque_date'));
                        $sheet->setCellValue('I6',trans('sponsorship::application.country_name'));
                        $sheet->setCellValue('J6',trans('sponsorship::application.district'));
                        $sheet->setCellValue('K6',trans('sponsorship::application.city_name'));
                        $sheet->setCellValue('L6',trans('sponsorship::application.location'));
                        $sheet->setCellValue('M6',trans('sponsorship::application.city_name'));
                        $sheet->setCellValue('N6',trans('sponsorship::application.mosques'));
                        $sheet->setCellValue('O6',trans('sponsorship::application.street_address'));
                        $sheet->setCellValue('P6',trans('sponsorship::application.Signature'));
                        $sheet->getStyle('A6:P6')->getAlignment()->setVertical('center');
                        $sheet->getStyle('A6:P6')->getAlignment()->setHorizontal( 'center' );

                        $sheet->getStyle("A1:P6")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 12, 'bold'=>  true ]
                        ]);

                        $count = 1;
                        $z = 7;
//                        $sheet->fromArray($rows, null, 'A7', true);

                        foreach ($data as $key => $current) {
                            $sheet->setCellValue('A' . $z, $count);
                            $sheet->setCellValue('B' . $z, $current->name);
                            $sheet->setCellValue('C' . $z, $current->id_card_number );
                            $sheet->setCellValue('D' . $z, 0 );
                            $sheet->setCellValue('E' . $z, $current->mobile );
                            $sheet->setCellValue('F' . $z, $current->amount);
                            $sheet->setCellValue('G' . $z, $current->cheque_account_number );
                            $sheet->setCellValue('H' . $z, $current->cheque_date );
                            $sheet->setCellValue('I' . $z, $current->country );
                            $sheet->setCellValue('J' . $z, $current->district );
                            $sheet->setCellValue('K' . $z, $current->city );
                            $sheet->setCellValue('L' . $z, $current->location );
                            $sheet->setCellValue('M' . $z, $current->mosques );
                            $sheet->setCellValue('N' . $z, $current->street_address );
                            $sheet->setCellValue('O' . $z, ' ');
                            $sheet->getStyle('A'. $z .':O' . $z)->getAlignment()->setHorizontal('center');
                            $sheet->getStyle('A'. $z .':O' . $z)->getAlignment()->setVertical('center');
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $count++;
                            $z++;
                        }

                        $styleArray = array(
                            'font' => array(
                                'bold' => true,
                            ),
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'borders' => array('top'     => array('style' => 'medium'),
                                'left'   => array('style' => 'medium'),
                                'right'  => array('style' => 'medium'),
                                'bottom' => array('style' => 'medium')

                            ),
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'cce6ff')

                            ),
                        );
                        $sheet->getStyle('A6:P6')->applyFromArray($styleArray);

                        $styleArray = array(
                            'borders' => array(
                                'allborders'    => array('style' => 'medium')
                            ),

                        );
                        $max = sizeof($data) + 6;
                        $sheet->getStyle('A6:P'.$max)->applyFromArray($styleArray);

                        $indicies = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P');
                        $indWidth =['A'=>7 ,'B'=> 25,'C'=> 13,'D'=> 15,'F'=> 25,'E'=> 13,'G'=> 10, 'H'=>25,'I'=> 25,'J'=> 25,
                            'K'=> 25,'L'=> 25,'M'=> 25,'N'=> 30,'O'=> 20,'P'=> 20];

                        foreach ($indicies as $index) {
                            $sheet->getColumnDimension($index)->setAutoSize(false);
                            $sheet->getColumnDimension($index)->setWidth($indWidth[$index]);
                            $sheet->getStyle($index)->getAlignment()->setWrapText(true);


                        }
                        $sheet->getRowDimension(1)->setRowHeight(35);
                        $sheet->getRowDimension(2)->setRowHeight(45);
                        $sheet->getRowDimension(5)->setRowHeight(45);
                        $sheet->getRowDimension(6)->setRowHeight(55);
                        $sheet->getRowDimension(1)->setRowHeight(35);
                        $sheet->getRowDimension(2)->setRowHeight(45);
                        $sheet->getRowDimension(5)->setRowHeight(45);
                        $sheet->getRowDimension(6)->setRowHeight(55);

                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['status'=>true,'download_token' => $token]);
            }
            return response()->json(['status'=>false]);

        }
        else if($target == 'map'){

            ini_set('max_execution_time', 0);
            set_time_limit(0);
            ini_set('memory_limit', '1G');

            $payment = Payments::GroupDetail($request->payment_ids);
            $range = PaymentsCases::where(function ($q) use ($request) {
                $q->where('payment_id', '=', $request->payments_id);
                $q->whereNotNull('date_from');
                $q->whereNotNull('date_to');
            })->first();

            $currency =Currency::where('id',  $request->currency_id)->first();
            $currency_name = $currency->name;
            $range_ = '-';
            if($range){
                $range_ = $range->date_from .'- '.$range->date_to ;
            }
            $districts = Payments::getDistrictPaymentGroup($request->payment_ids);

            $dirName = base_path('storage/app/templates/CaseForm');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }

            $total_count = 0;
            foreach ($districts as $k=>$v){
                PDF::reset();
                PDF::SetTitle('كشوفات الاستحقاق');
                PDF::SetFont('aefurat', '', 18);
                PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                PDF::SetAutoPageBreak(TRUE);
                PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);
                PDF::setCellHeightRatio(1.5);
                PDF::SetMargins(5, 15, 5);
                PDF::SetHeaderMargin(5);
                PDF::SetFooterMargin(5);
                PDF::SetAutoPageBreak(TRUE);
//                PDF::setHeaderCallback(function($pdf) {
//                    $header = '<img src="'.storage_path('app/page/header.jpg').'" alt=""/>';
//                    PDF::writeHTMLCell(0, 0, '', 5, $header, 0, 1, 0, true, '', true);
//                });

                $count = sizeof($v->beneficiary);
                $amount = 0;
                $total_count += $count;
                $ceil=ceil($count/17);

                for ($x = 0; $x < $ceil; $x++) {
                    $curOffset = ($x) * 17;
                    $put_total = false;
                    $html = '<!doctype html>
                 <html lang="ar">
                     <head>
                           <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;}
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .cell-style{text-align: center !important; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                    </head>
                    <body style="font-weight: normal !important;">';
                    $html.= '<h2 style="text-align:center;"> </h2>';

                    $html  .=  ' <table border="0" align="center">';
                    $html .= ' <tr style="border: none !important; line-height: 30px;
                              font-size: 12px!important; height: 57px; fi ">
                        <td colspan="8" style="font-size: 14px;"> استحقاق عن الفترة   
                        ('. $range_ .') 
                        </td>
                   </tr>';
                    $html .= ' <tr style="border: none !important; line-height: 30px;
                              font-size: 12px!important; height: 40px; ">
                        <td style="border: 1px solid black"> الجهة الكافلة :  </td>
                        <td style="border: 1px solid black"colspan="2"> ' .$payment['sponsor_name'] . '</td>
                        <td colspan="2">  </td>
                        <td style="border: 1px solid black"> الجهة المشرفة :  </td>
                        <td style="border: 1px solid black"colspan="2">  جمعية دار الكتاب و السنة </td>

                   </tr>
                  <tr style="border: none !important; line-height: 30px;
                              font-size: 12px!important; height: 40px; ">
                        <td style="border: 1px solid black">  المحافظة </td>
                        <td style="border: 1px solid black"colspan="2"> ' .$v->name . '</td>
                        <td colspan="2">   </td>
                        <td style="border: 1px solid black"> التاريخ </td>
                        <td style="border: 1px solid black"colspan="2"> '.date('Y-m-d').' </td>
                   </tr>';
                    $html .= '</table>';
                    $html.= '<br><br>';

                    $index=$curOffset;
                    $html .=  '<table  cellpadding="pixels" style="   width:100%
                                            display: table;
                                            text-align: center;
                                            margin-bottom: 20px;
                                            border-collapse: collapse;
                                            border-spacing: 0;
                                          ">
                                <thead>
                                <tr style="height:40px; line-height: 25px; ">
                                    <th class="heading-style" style="width: 40px; "> رقم<br> اليتيم </th>
                                    <th class="heading-style" style="width: 135px; "><br><br>'.trans('sponsorship::application.orphan_name') .'</th>
                                    <th class="heading-style" style="width: 50px; "> هوية<br> الوصي </th>
                                    <th class="heading-style" style="width: 135px; "><br><br>'.trans('sponsorship::application.guardian_name_') .'</th>
                                    <th class="heading-style" style="width: 65px; "><br><br>'.trans('sponsorship::application.for') .'</th>
                                    <th class="heading-style" style="width: 40px; "> المبلغ<br> بالشيكل </th>
                                    <th class="heading-style" style="width: 100px; "><br><br>'.trans('sponsorship::application.signature') .'</th>
                                </tr>
                                </thead> <tbody>';

                    if($index < sizeof($v->beneficiary)) {
                        $idx = 0;
                        for ($z = 0; $z < 17; $z++) {
                            if($index < $count){
                                $v1 = $v->beneficiary[$index];
                                $sponsor_number = is_null($v1->sponsor_number) ? '-' : $v1->sponsor_number;
                                $case_name      = is_null($v1->case_name) ? '-' : $v1->case_name;
                                $guardian_card  = is_null($v1->guardian_card) ? '-' : $v1->guardian_card;
                                $guardian_name  = is_null($v1->guardian_name) ? '-' : $v1->guardian_name;
                                $shekel_amount  = is_null($v1->shekel_amount) ? '-' : $v1->shekel_amount;
                                $payment_category  = is_null($v1->payment_category) ? '-' : $v1->payment_category;

                                $html .='<tr style="height:20px; line-height: 25px; ">
                                                    <td  class="cell-style" style="width:40px;  ">'.$sponsor_number .'</td>
                                                    <td  class="cell-style" style="width:135px; ">'.$case_name .'</td>
                                                    <td  class="cell-style" style="width:50px;  ">'.$guardian_card.'</td>
                                                    <td  class="cell-style" style="width:135px; ">'.$guardian_name .'</td>
                                                    <td  class="cell-style" style="width:65px;  ">'.$payment_category .'</td>
                                                    <td  class="cell-style" style="width:40px;  ">'.$shekel_amount .'</td>
                                                    <td  class="cell-style" style="width:100px;  "> </td>
                                                </tr>';

                                $amount += $shekel_amount;
                                $index++;
                                $idx++;
                            }
                        }
                    }

                    if(($x == $ceil- 1) && ($idx < 12)){
                        $html .= '</tbody></table>';
                        $put_total = true;
                        $html  .=  ' <br><br><table border="0" align="center">';
                        $html .='<tr style="line-height: 30px;font-size: 12px!important; height: 40px; ">
                                    <th style="border: 1px solid black"> <br />   إجمالي العدد </th>
                                    <th style="border: 1px solid black"> <br />   إجمالي المبالغ </th>
                                   
                             </tr>
                             <tr style="line-height: 30px;font-size: 12px!important; height: 40px;" >
                                    <td style="border: 1px solid black">' .$count . '</td>
                                    <td style="border: 1px solid black"> ' .$amount . '</td>
                                
                            </tr>';
                        $html .= '</table>';
                        $html.= '<br><br><br>';
                        $html  .=  ' <table border="0" align="center">';
                        $html .= ' <tr style="border: none !important; line-height: 30px;
                                          font-size: 12px!important; height: 40px; ">
                                    <td style="border: none" colspan="3"> أمين الصندوق :  </td>
                                    <td colspan="2">  </td>
                                    <td style="border: none" colspan="3"> رئيس الجمعية :  </td>
                               </tr>';
                        $html .= '</table>';
                    }else{
                        $html .= '</tbody></table>';
                    }
                    $html .= '</body></html>';

                    PDF::SetFooterMargin(0);
                    PDF::setFooterCallback(function($pdf) use ($x,$ceil){
                        $html_ = '<p style="text-align:center;">('.'  '. ($x+1) .' / ' . $ceil .'  ) </h3>';
                        PDF::writeHTMLCell(0, 0, '', 260, $html_, 0, 1, 0, true, '', true);
                        $footer = '<img src="'.storage_path('app/page/footer.jpg').'" alt=""/>';
                        PDF::writeHTMLCell(0, 0, '', 270, $footer, 0, 1, 0, true, '', true);
                    });
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

                }

                if($put_total == false){
                    $html = '<!doctype html>
                 <html lang="ar">
                     <head>
                           <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .cell-style{text-align: center !important; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                    </head>
                    <body style="font-weight: normal !important;">';
                    $html.= '<h3 style="text-align:center;"> </h3>';
                    $html.= '<h3 style="text-align:center;"> </h3>';
                    $html.= '<h3 style="text-align:center;"> </h3>';
//                    <th style="border: 1px solid black"> <br />    نوع الكفالة </th>
//                    <td style="border: 1px solid black"> ' .$payment_category . ' <br /></td>
                    $html  .=  ' <table border="0" align="center">';
                    $html .='<tr style="line-height: 30px;font-size: 12px!important; height: 40px; ">
                        <th style="border: 1px solid black"> <br />   إجمالي العدد </th>
                        <th style="border: 1px solid black"> <br />   إجمالي المبالغ </th>
                      
                 </tr>
                 <tr style="line-height: 30px;font-size: 12px!important; height: 40px;" >
                        <td style="border: 1px solid black">' .$count . ' <br /></td>
                        <td style="border: 1px solid black"> ' .$amount . ' <br /></td>
                       
                </tr>';
                    $html .= '</table>';
                    $html.= '<br><br><br>';
                    $html  .=  ' <table border="0" align="center">';
                    $html .= ' <tr style="border: none !important; line-height: 30px;
                              font-size: 12px!important; height: 40px; ">
                        <td style="border: none" colspan="3"> أمين الصندوق :  </td>
                        <td colspan="2">  </td>
                        <td style="border: none" colspan="3"> رئيس الجمعية :  </td>
                   </tr>';
                    $html .= '</table>';

                    $html .= '</body></html>';
                    PDF::SetFooterMargin(0);
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

                    PDF::setFooterCallback(function($pdf) use ($x,$ceil){
                        $footer = '<img src="'.storage_path('app/page/footer.jpg').'" alt=""/>';
                        PDF::writeHTMLCell(0, 0, '', 270, $footer, 0, 1, 0, true, '', true);
                    });
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                }

                PDF::Output($dirName.'/'.$v->name.'.pdf', 'F');

            }
            //***************************************//
            // total pdf page

            PDF::reset();
            PDF::SetTitle('كشوفات الاستحقاق');
            PDF::SetFont('aefurat', '', 18);
//            PDF::SetFont('aefurat', '', 18 , '', 'default', true );
//        PDF::SetFont('dejavusans', '', 18 , '', 'default', true );
            PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            PDF::SetAutoPageBreak(TRUE);
            PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);
            PDF::setCellHeightRatio(1.5);
            PDF::SetMargins(5, 15, 5);
            PDF::SetHeaderMargin(5);
            PDF::SetFooterMargin(5);
            PDF::SetAutoPageBreak(TRUE);
//            PDF::setHeaderCallback(function($pdf) {
//                $header = '<img src="'.storage_path('app/page/header.jpg').'" alt=""/>';
//                PDF::writeHTMLCell(0, 0, '', 5, $header, 0, 1, 0, true, '', true);
//            });
//            PDF::setFooterCallback(function($pdf) {
//                $footer = '<img src="'.storage_path('app/page/footer.jpg').'" alt=""/>';
//                PDF::writeHTMLCell(0, 0, '', 270, $footer, 0, 1, 0, true, '', true);
//            });

            $html = '<!doctype html>
                 <html lang="ar">
                     <head>
                           <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .cell-style{text-align: center !important; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                    </head>
                    <body style="font-weight: normal !important;">';
            $html.= '<h3 style="text-align:center;"> </h3>';
            $html.= '<h3 style="text-align:center;"> </h3>';
            $html.= '<h3 style="text-align:center;"> </h3>';
            $index=$curOffset;
            $html .=  '<table  cellpadding="pixels" style="   width:100%
                                            display: table;
                                            text-align: center;
                                            margin-bottom: 20px;
                                            border-collapse: collapse;
                                            border-spacing: 0;
                                          ">
                                <thead>
                                <tr style="height:40px; line-height: 25px; ">
                                    <th class="heading-style" style="width: 50px; "> م </th>
                                    <th class="heading-style" style="width: 170px; "> البيان </th>
                                    <th class="heading-style" style="width: 80px; "> العدد </th>
                                    <th class="heading-style" style="width: 130px; "> المبلغ ( '.$currency_name .' )</th>
                                    <th class="heading-style" style="width: 130px; "> المبلغ بالشيكل</th>
                                </tr>
                                </thead> <tbody>';

            foreach ($payment['category_map'] as $key=>$value){
                $name = is_null($value->name) ? '-' : $value->name;
                $ben_cases      = is_null($value->ben_cases) ? '0' : $value->ben_cases;
                $amount      = is_null($value->amount) ? '0' : $value->amount;
                $shekel_amount      = is_null($value->shekel_amount) ? '0' : $value->shekel_amount;
                $html .='<tr style="height:20px; line-height: 25px; ">
                                                    <td  class="cell-style" style="width:50px;  ">'.( $key + 1 ) .'</td>
                                                    <td  class="cell-style" style="width:170px; ">'.$name .'</td>
                                                    <td  class="cell-style" style="width:80px; ">'.$ben_cases .'</td>
                                                    <td  class="cell-style" style="width:130px;  ">'.$amount .'</td>
                                                    <td  class="cell-style" style="width:130px;  ">'.$shekel_amount .'</td>
                                                </tr>';

                $amount += $shekel_amount;
                $index++;
                $idx++;
            }
            $html .= '</tbody></table>';
            $html.= '<br><br>';

            $html  .=  ' <table border="0" align="center">';
            $payment['shekel_amount'] = 0;
            $html .='<tr style="line-height: 30px;font-size: 12px!important; height: 40px; ">
                        <th style="border: 1px solid black" rowspan="2">  الإجماليات<br /></th>
                        <th style="border: 1px solid black" > <br />   العدد </th>
                        <th style="border: 1px solid black"> <br />  المبلغ بالشيكل </th>
                 </tr>
                 <tr style="line-height: 30px;font-size: 12px!important; height: 40px;" >
                        <td style="border: 1px solid black">' .$total_count . '</td>
                        <td style="border: 1px solid black">' .$payment['shekel_amount'] . '</td>
                </tr>';
            $html .= '</table>';
            $html.= '<br><br><br>';
            $html  .=  ' <table border="0" align="center">';
            $html .= ' <tr style="border: none !important; line-height: 30px;
                              font-size: 12px!important; height: 40px; ">
                        <td style="border: 1px solid black" colspan="3"> أمين الصندوق :  </td>
                        <td colspan="2">  </td>
                        <td style="border: 1px solid black" colspan="3"> رئيس الجمعية :  </td>
                   </tr>';

            $html .= '</table>';
            $html .= '</body></html>';
            PDF::SetFooterMargin(0);
            PDF::AddPage('P','A4');
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
            PDF::Output($dirName.'/'.'الاجمالي'.'.pdf', 'F');


            //***************************************//

            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');

            $zip = new \ZipArchive;
            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                foreach ( glob( $dirName . '/*' ) as $fileName )
                {
                    $file = basename( $fileName );
                    $zip->addFile( $fileName, $file );
                }

                $zip->close();
                array_map('unlink', glob("$dirName/*.*"));
                rmdir($dirName);
            }

            return response()->json(['status'=>true,'download_token' => $token]);

        }
        else{
            $data = PaymentsCases::exportCaseRecipientWithOptions($request->all());

//        return $data;
//        organizations_name
            $keys=["case_name","id_card_number","category_name","sponsor_name","sponsor_number","guardian_name","guardian_id_card_number",
                'country_name','governarate_name','city_name','location_name','mosque_name','street_address',
                "payment_date","currency_name", "amount","amount_after_discount","shekel_amount_before_discount","shekel_amount",
                "recipient_amount","recipient_amount_after_discount","recipient_shekel_amount_before_discount","recipient_shekel_amount",
                "date_from","date_to","recipients_name",'recipient_id_card_number',"phone","primary_mobile",
                "secondary_mobile","guardian_phone","guardian_primary_mobile","guardian_secondary_mobile","cheque_account_number","cheque_date","rec_status"];

            if(sizeof($data) !=0){
                $rows=[];
                foreach($data as $key =>$value){
                    $rows[$key][trans('sponsorship::application.#')]=$key+1;
                    foreach($keys as $ke){
                        $f_ke=null;
                        $vlu = is_null($value->$ke) ? '-' : $value->$ke;

                        if(substr( $ke, 0, 7 ) === "mother_"){
                            $f_ke = trans('sponsorship::application.'.substr($ke,7))." ( ".trans('sponsorship::application.mother')  ." ) ";
                        }elseif(substr( $ke, 0, 7 ) === "father_"){
                            $f_ke = trans('sponsorship::application.'.substr($ke,7))." ( ".trans('sponsorship::application.father')  ." ) ";
                        }elseif(substr( $ke, 0, 9 ) === "guardian_"){
                            $f_ke = trans('sponsorship::application.'.substr($ke,9))." ( ".trans('sponsorship::application.guardian')  ." ) ";
                        }elseif(substr( $ke, 0, 10 ) === "recipient_"){
                            $f_ke = trans('sponsorship::application.'.substr($ke,10))." ( ".trans('sponsorship::application.recipient')  ." ) ";
                        }else{
                            $f_ke = trans('sponsorship::application.' . $ke);
                        }
                        $rows[$key][$f_ke]= $vlu;

//                        if($k != 'case_id' && $k != 'id' && $k != 'sponsor_id'&& $k != 'flag'&& $k != 'date_flag'&& $k != 'person_id'){
//                            if($k =='sponsor_number'){
//                                $rows[$key][trans('sponsorship::application.sponsorship_number')]= $v;
//                            }
//                            else{
//                                $rows[$key][trans('sponsorship::application.' . $k)]= $v;
//                            }
//                        }
                    }
                }
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($rows) {
                    $excel->sheet(trans('sponsorship::application.recipients report') , function($sheet) use($rows) {
                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Calibri',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
//                    $sheet->setWidth(['A'=>7 ,'B'=> 15,'C'=> 30,'D'=> 15,'F'=> 30,'E'=> 23,'G'=> 15]);
                        $style = [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold'      =>  true
                            ]
                        ];

                        $sheet->getStyle("A1:AC1")->applyFromArray($style);
                        $sheet->setHeight(1,30);

                        $style = [
                             'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold' => false
                            ]
                        ];

                        $sheet->getDefaultStyle()->applyFromArray($style);
                        $sheet->fromArray($rows);

                    });
                })
                    ->store('xlsx', storage_path('tmp/'));
                return response()->json(['status'=>true,'download_token' => $token]);
            }
        }

        return response()->json(['status'=>false]);
    }

    // export pdf internal cheque of payment recipient
    public function pdf(Request $request)
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $payment =Payments::findorfail($request->payments_id);
        $payment_exchange=$payment->payment_exchange;

        if($payment_exchange ==1){
            $exchange=trans('sponsorship::application.Israeli shekel just only');
        }else{
            $currency=Currency::findorfail($payment->currency_id);
            $exchange=$currency->name.' ' .trans('sponsorship::application.just only');
        }

        $options = [ 'person_id' =>$request->person_id,
            'payment_exchange' =>$payment->payment_exchange,
            'payment_ids'=>[$request->payments_id]];

        $items=PaymentsRecipient::PaymentOfSetExport($options);
        if(sizeof($items) == 0){
            return response()->json(['status'=>false]);
        }

        $dirName = base_path('storage/tmp');
//            $dirName = storage_path('app/archiveCheques');
        if (!is_dir($dirName)) {
            @mkdir($dirName, 0777, true);
        }
        $html='';
        PDF::SetTitle(trans('sponsorship::application.cheque'));
        PDF::SetFont('aealarabiya', '', 18);
        PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);
        PDF::SetMargins(5, 15, 5);

//        ini_set('max_execution_time', 0);
        set_time_limit(0);
        ini_set('memory_limit', '1G');
        // ini_set('memory_limit', '-1');
        foreach ($items as $value) {
            $value=(array)$value;
            $address= '  ';

            if(!is_null($value['country'])){
                $address = $value['country'];
            }
            if(!is_null($value['district'])){
                $address .= ' - ' .$value['district'];
            }
            if(!is_null($value['city'])){
                $address .= ' - ' .$value['city'];
            }
            if(!is_null($value['location'])){
                $address .= ' - ' .$value['location'];
            }
            if(!is_null($value['street_address'])){
                $address .= ' - ' .$value['street_address'];
            }


            $html = '<!doctype html>
                 <html lang="ar"> 
                     <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                        <style>
                          table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                          * , body {font-weight:normal !important; font-size: 10px;}
                          .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                          .bolder-content{text-align: right !important; vertical-align: middle!important;}
                          .vertical-middle{ vertical-align: middle !important;}
                          </style>
                    </head>
                    <body style="font-weight: normal !important;">'.
                '<table cellpadding="pixels" style="display: table; width: 100%; border-collapse: collapse;border-spacing: 0; ">'.
                '<tr>
                              <td colspan="2" style=" height: 20px ;  padding: 12px;  border:  none; "></td>
                              <td colspan="2" class="vertical-middle" style=" height: 20px ; padding: 15px 0 12px 0; text-align: right !important; font-weight: bold;">'.
                trans('sponsorship::application.receipt_no') . '  :  '. $value['cheque_account_number'].
                '</td></tr><tr><td  colspan="2" class="vertical-middle"
                                style=" height: 20px ; padding: 15px 0 12px 0; text-align: right !important; font-weight: bold;">'.
                trans('sponsorship::application.name').'  :  '.
                $value['name'].
                '</td>'.
                '<td style=" height: 20px ; padding: 15px 0 12px 0; font-weight: bold; text-align: right !important;">'.
                trans('sponsorship::application.id_card_number') . '  :  '. $value['id_card_number'].
                '</td>'.'</tr>'.
                '<tr><td colspan="2" style=" height: 20px ; padding: 15px 0 15px 0; font-weight: bold; text-align: right !important;">'.
                trans('sponsorship::application.address'). '  :  '. $address.'</td></tr>'.
                '<tr><td colspan="2" style=" height: 20px ; padding: 15px 0 15px 0; font-weight: bold; text-align: right !important;">'.
                trans('sponsorship::application.mobile'). '  :  '. $value['mobile'].'</td></tr>'.
                '<tr><td colspan="4">
                            <table  cellpadding="pixels" style="   width:100%
                                            display: table;
                                            text-align: center;
                                            margin-bottom: 20px;
                                            border-collapse: collapse;
                                            border-spacing: 0;
                                          ">
                                <thead>
                                <tr style="height:15px; line-height: 25px; ">
                                    <th class="heading-style" style="width: 40px; ">'.trans('sponsorship::application.orphan_id') .'</th>
                                    <th class="heading-style" style="width: 120px; ">'.trans('sponsorship::application.name') .'</th>
                                    <th class="heading-style" style="width: 135px; ">'.trans('sponsorship::application.sponsor_name') .'</th>
                                     <th class="heading-style" style="width: 50px; ">'.trans('sponsorship::application.for') .'</th>
                                    <th class="heading-style" style="width: 40px; ">'.trans('sponsorship::application.amount') .'</th>
                                </tr>
                                </thead> <tbody>';

            $size =sizeof($value['individual']);

            if($size > 10){
                foreach ($value['individual'] as $row) {

                    if($payment_exchange ==0 ){
                        $amount=$row['amount'];
                    }else{
                        $amount=$row['shekel_amount'];
                    }

                    $html .='<tr  class="vertical-middle" style=" line-height: 30px;">
                                  <td class="vertical-middle" style=" text-align: center !important;height: 10px ; border: 1px solid black;
                                              font-weight: normal;  ">'. $row['sponsor_number']. '</td>
                                  <td class="vertical-middle" style="overflow: hidden; word-break: break-all; text-align: center !important;height: 10px ;
                                      border: 1px solid black; font-weight: 100;">'.$row['full_name'].'</td>
                                    <td   class="vertical-middle"style="  height: 10px ; border: 1px solid black; ">'.
                        $value['sponsor_name'].'</td>
                                    <td   class="vertical-middle" style="  text-align: center !important;height: 10px ; border: 1px solid black; ">'.
                        $row['category_name'].'</td>
                                    <td  class="vertical-middle"style="  height: 10px ; border: 1px solid black;  ">'. $amount.'</td>
                                </tr>';
                }
            }
            else{

                foreach ($value['individual'] as $row) {
                    $row=(array)$row;

                    if($payment_exchange == 0 ){
                        $amount=$row['amount'];
                    }else{
                        $amount=$row['shekel_amount'];
                    }

                    $html .='<tr  class="vertical-middle"style=" line-height: 25px;">
                                    <td  class="vertical-middle"style=" text-align: center !important;height: 10px ; border: 1px solid black;
                                              font-weight: normal;  ">'. $row['sponsor_number']. '</td>
                                    <td   class="vertical-middle"style="overflow: hidden; word-break: break-all; 
                                             text-align: center !important;height: 10px ; border: 1px solid black; 
                                             font-weight: 100;">'.$row['full_name'].'</td>
                                    <td   class="vertical-middle"style="  height: 10px ; border: 1px solid black; ">'.
                        $value['sponsor_name'].'</td>
                                    <td   class="vertical-middle" style="  text-align: center !important;height: 10px ; border: 1px solid black; ">'.
                        $row['category_name'].'</td>
                                    <td   class="vertical-middle"style="  height: 10px ; border: 1px solid black;  ">'. number_format((float)$amount, 2, '.', '').'</td>
                                </tr>';
                }
                $length =  (10 - sizeof($value['individual'])) ;
                for ($x = 0; $x <= $length ; $x++) {
                    $html .='<tr  class="vertical-middle"style=" line-height: 25px;">
                                    <td  style="height: 10px ;   border: 1px solid black;">  </td>
                                    <td  style="height: 10px ;   border: 1px solid black;">  </td>
                                    <td  style="height: 10px ;   border: 1px solid black;">  </td>
                                    <td  style="height: 10px ;   border: 1px solid black;">  </td>
                                    <td  style="height: 10px ;   border: 1px solid black;">  </td>
         
                                 </tr>';
                }

            }

            if($payment_exchange == 0 ){
                $total=$value['amount'];
            }else{
                $total=$value['shekel_amount'];
            }

            $html .=         '<tr style="height:15px; line-height: 25px; ">
                                    <td  colspan="2" style="border: none"> </td>
                                    <td  colspan="2" class="heading-style">'.trans('sponsorship::application.sub total') .'</td>
                                    <td  class="heading-style">'.number_format((float)$total, 2, '.', '').'</td>
                                </tr>';

            $html.='</tbody> </table></td> </tr>';

            $html .= '<tr  class="vertical-middle"style=" line-height: 35px;"> 
                           <td colspan="5" style="text-align: right !important;  height: 25px ;padding: 15px 0 15px 0;  font-weight: bold;">' .trans('sponsorship::application.notes') .'   :.....................................................................................................................................</td>
</tr>';
            $html .= '</table> <table>';
            $html .=
                '<tr>
    <td class="bolder-content vertical-middle" style="text-align: right !important; height: 20px ;  padding: 10px 0 15px 0;   font-weight: bold;">'.
                trans('sponsorship::application.recipients_name').' : .......................</td>
    <td   style="text-align: right !important; height: 20px ;  padding: 10px 0 15px 0;  font-weight: bold;">  '.
                trans('sponsorship::application.creator').' : .......................</td>
    <td   style="text-align: right !important; height: 20px ;  padding: 10px 0 15px 0;  font-weight: bold;">  '.
                trans('sponsorship::application.auditor').' : .......................</td>
</tr>
<tr>
    <td class="vertical-middle " style=" padding: 10px 0 15px 0; font-weight: bold;">' .
                trans('sponsorship::application.Signature') .' : ...............................</td>
    <td  class="vertical-middle " style=" padding: 10px 0 15px 0; font-weight: bold;">' .
                trans('sponsorship::application.Signature') .' : ..............................</td>
    <td   class="vertical-middle " style=" padding: 10px 0 15px 0; font-weight: bold;">' .
                trans('sponsorship::application.Signature') .' : .............................</td>
</tr>
';
            $html .= '</table></body></html>';
            PDF::SetFooterMargin(0);
            PDF::AddPage('P','A5');
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        }

        $token = md5(uniqid());
        $dirName = base_path('storage/tmp');
        PDF::Output($dirName.'/export_' . $token . '.pdf', 'F');

        return response()->json(['status'=>true,'download_token' => $token]);

    }

    // get payments_recipient details (bank)
    public function getChequeData($id)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $recipient = PaymentsRecipient::where('id',$id)->first();
        $payment   = Payments::findorfail($recipient->payment_id);
        $payment_exchange=$payment->payment_exchange;

        if($payment_exchange ==1){
            $exchange=trans('sponsorship::application.Israeli shekel just only');
        }else{
            $currency=Currency::findorfail($payment->currency_id);
            $exchange=$currency->name.' ' .trans('sponsorship::application.just only');
        }

        $item=PaymentsRecipient::exportRecord($recipient->person_id,$recipient->payment_id,$payment->exchange_type,$payment_exchange);
        $item->exchange=$exchange;
        $Arabic_numbers=new Arabic_numbers();
        $item->character_value= $Arabic_numbers->convert_number((float)$item->amount,$exchange);

        return response()->json(['row'=>$item]);
    }

    // get payments_recipient details (bank)
    public function getPaymentChequeData($id)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $payment   = Payments::findorfail($id);
        $payment_exchange=$payment->payment_exchange;

        if($payment_exchange ==1){
            $exchange=trans('sponsorship::application.Israeli shekel just only');
        }else{
            $currency=Currency::findorfail($payment->currency_id);
            $exchange=$currency->name.' ' .trans('sponsorship::application.just only');
        }

        $items= PaymentsRecipient::exportAccorddingBanks($id,$payment->exchange_type,$payment->payment_exchange);
        $data =$items[0];
        foreach($data['items'] as $key=>$item){
            $Arabic_numbers=new Arabic_numbers();
            $item->character_value = $Arabic_numbers->convert_number((float)$item->amount,$exchange);
        }
        return response()->json($data);
    }

    // paginate ArchivedCheques
    public function archivedCheques(Request $request)
    {

        $items = ArchivedCheques::query();
        if(isset($request->name)){
            if(!is_null($request->name) && $request->name!=''){
                $items->whereRaw("title like  ?", "%".$request->name."%");
            }
        }
        return response()->json(['items' =>$items->paginate(config('constants.records_per_page'))]);
    }

    // get ArchivedCheques file to download it
    public function downloadArchivedCheques(Request $request,$id)
    {
        $file = ArchivedCheques::findorfail($id);
        return response()->json(['download_token' =>$file->file_id]);
    }

    // get import payment template file to download it
    public function importPaymentTemplate(){
        return response()->json(['download_token' => 'import-payment-template']);
    }

    // ************************ Not Used (it's from pre-edition ) ************************//
    //
    public function updateBeneficiaryStatus(Request $request, $id){

//            $this->authorize('update',Payments::class);

        $sponsor_number= $request->get('sponsor_number');
        $Payments=Payments::where('id',$id)->first();
        $sponsor_id=$Payments->sponsor_id;
        $response = array();
        $updated=['status'=>$request->get('status')];

        if($request->get('status') == 3){
            $updated['amount']=0;
        }
        $update=PaymentsCases::where(["payment_id"=>$id,"case_id"=>$request->get('case_id'),"sponsor_number"=>$request->get('sponsor_number')])->update($updated);
        if($update){
            $case = \Common\Model\CaseModel::fetch(array('full_name'=>true), $request->get('case_id'));
            $name=$case->full_name;
            \Log\Model\Log::saveNewLog('PAYMENTS_CASES_UPDATED',trans('sponsorship::application.edited the case to a beneficiary of the payment')  . ' :"'.$name. '" ');

            $response["status"]= 'success';
            $response["msg"]= trans('sponsorship::application.The row is updated');
            $PaymentsCases=PaymentsCases::where(["payment_id"=>$id,"case_id"=>$request->get('case_id'),"sponsor_number"=>$request->get('sponsor_number')])->first();
            $recipient=$PaymentsCases->recipient;
            $case= \DB::table('char_cases')
                ->join('char_persons','char_persons.id',  '=', 'char_cases.person_id')
                ->where('char_cases.id','=',$request->get('case_id'))
                ->selectRaw("char_cases.organization_id,char_cases.person_id,char_persons.father_id,char_persons.mother_id")
                ->first();

            $recipient_id=null;
            $amount=0;

            if($request->get('status') == 3){
                if($recipient ==0) {
                    $recipient_id=$case->person_id;
                    $rows= \DB::table('char_cases')
                        ->join('char_persons', function($q) {
                            $q->on('char_persons.id','=','char_cases.person_id');
                        })
                        ->join('char_payments_cases', function($q) use($sponsor_id) {
                            $q->on('char_payments_cases.case_id','=','char_cases.id');
                            $q->where('char_payments_cases.status','!=',3);
                        })
                        ->where('char_payments_cases.payment_id','=',$id)
                        ->where('char_cases.person_id',$recipient_id)
                        ->where('char_payments_cases.sponsor_number','!=',$sponsor_number)
                        ->selectRaw("count(char_payments_cases.sponsor_number) as rows_count,char_persons.father_id,char_persons.mother_id")
                        ->first();

                    $count=$rows->rows_count;
                    $father_id=$rows->father_id;
                    $mother_id=$rows->mother_id;

                    if($count != 0){
                        $total= \DB::select("select get_payments_recipient_amount ( ?, 'person', ? ) AS amount", [$recipient_id,$id]);
                        if($total){
                            $amount=$total[0]->amount;
                        }
                    }

                }
                else{

                    $the_recipient=Guardian::where(['individual_id' =>$case->person_id,'organization_id'=>$case->organization_id,'status'=>1])->first();
                    if($the_recipient){
                        $recipient_id=$the_recipient->guardian_id;
                    }

                    $rows= \DB::table('char_payments_cases')
                        ->join('char_cases','char_cases.id',  '=', 'char_payments_cases.case_id')
                        ->join('char_persons', function($q) {
                            $q->on('char_persons.id','=','char_cases.person_id');
                        })
                        ->where('char_payments_cases.status','!=',3)
                        ->where('char_payments_cases.payment_id','=',$id)
                        ->where('char_payments_cases.guardian_id',$recipient_id)
                        ->where('char_payments_cases.sponsor_number','!=',$sponsor_number)
                        ->selectRaw("count(char_payments_cases.sponsor_number) as rows_count,char_persons.father_id,char_persons.mother_id")
                        ->first();

                    $count=$rows->rows_count;
                    $father_id=$rows->father_id;
                    $mother_id=$rows->mother_id;

                    if($count != 0){
                        $total= \DB::select("select get_payments_recipient_amount ( ?, 'guardian', ? ) AS amount", [$recipient_id,$id]);
                        if($total){
                            $amount=$total[0]->amount;
                        }
                    }

                }

                if($count == 0){
                    PaymentsRecipient::where(["payment_id"=>$id,"person_id"=>$recipient_id,'mother_id'=>$mother_id,'father_id'=>$father_id])->delete();
                }else{
                    PaymentsRecipient::where(["payment_id"=>$id,"person_id"=>$recipient_id,'mother_id'=>$mother_id,'father_id'=>$father_id])->update(['amount'=>$amount]);
                }
            }
            if($request->get('status') == 2){
                if($recipient ==0) {
                    $recipient_id=$case->person_id;
                    $total= \DB::select("select get_payments_recipient_amount ( ?, 'person', ? ) AS amount", [$recipient_id,$id]);
                    if($total){
                        $amount=$total[0]->amount;
                    }
                }
                else{
                    $the_recipient=Guardian::where(['individual_id' =>$case->person_id,'organization_id'=>$case->organization_id,'status'=>1])->first();
                    if($the_recipient){
                        $recipient_id=$the_recipient->guardian_id;
                        $total= \DB::select("select get_payments_recipient_amount ( ?, 'guardian', ? ) AS amount", [$recipient_id,$id]);
                        if($total){
                            $amount=$total[0]->amount;
                        }
                    }
                }

                $the_recipient= PaymentsRecipient::where(["payment_id"=>$id,"person_id"=>$recipient_id,'father_id'=>$case->father_id,'mother_id'=>$case->mother_id])->first();
                if(!$the_recipient){
                    $insert=['payment_id' => $id, 'person_id' => $recipient_id, 'status' => 0,'amount' =>$amount,'father_id'=>$case->father_id,'mother_id'=>$case->mother_id];
                    if ($Payments->exchange_type == 2) {
                        $insert['bank_id']= $Payments->bank_id;
                    }else{
                        if ($Payments->bank_id != null && $Payments->exchange_type == 1) {
                            $insert['bank_id'] = $Payments->bank_id;
                            $bank_account =\Common\Model\PersonModels\PersonBank::where(["person_id"=>$recipient_id,'bank_id'=>$Payments->bank_id ])->first();
                            if($bank_account){
                                $insert['cheque_account_number'] = $bank_account->account_number;
                            }
                        }
                    }
                    PaymentsRecipient::create($insert);
                }
            }
        }
        else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sponsorship::application.There is no update');
        }
        return response()->json($response);

    }
    public function exportCheque(Request $request)
    {


        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $payment =Payments::findorfail($request->payments_id);
        $payment_exchange=$payment->payment_exchange;

        if($payment_exchange ==1){
            $exchange=trans('sponsorship::application.Israeli shekel just only');
        }else{
            $currency=Currency::findorfail($payment->currency_id);
            $exchange=$currency->name.' ' .trans('sponsorship::application.just only');
        }

        $item=PaymentsRecipient::exportRecord($request->person_id,$request->payments_id,$payment->exchange_type,$payment_exchange);

        if($payment->exchange_type ==3){
            $user = \Auth::user();
            $dirName = base_path('storage/app/templates/cheTemp');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }

            $default_template=Setting::where(['id'=>'default-cheque-template','organization_id'=>$user->organization_id])->first();

            if($default_template){
                $path = base_path('storage/app/'.$default_template->value);
            }else{
                $path = base_path('storage/app/templates/cheque-default-template.docx');
            }

            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);
            try{
                $templateProcessor->cloneRow('rId', 1);
                $i=1;
                $Arabic_numbers=new Arabic_numbers();
                $templateProcessor->setValue('rId#'.$i, $i);
                $templateProcessor->setValue('name#'.$i, $item->name);
                $templateProcessor->setValue('id_card_number#'.$i, $item->id_card_number);
                $templateProcessor->setValue('value#'.$i, $item->amount);
                $templateProcessor->setValue('character_value#'.$i, $Arabic_numbers->convert_number((float)$item->amount,$exchange));
                $templateProcessor->setValue('cheque_number#'.$i, $item->cheque_account_number);
                $templateProcessor->setValue('cheque_date#'.$i, $item->cheque_date);
                $templateProcessor->saveAs($dirName.'/'.$item->name.'.docx');

            }catch (\Exception $e){  }

            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');

            $zip = new \ZipArchive;
            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                foreach ( glob( $dirName . '/*' ) as $fileName )
                {
                    $file = basename( $fileName );
                    $zip->addFile( $fileName, $file );
                }

                $zip->close();
                array_map('unlink', glob("$dirName/*.*"));
                rmdir($dirName);
            }
            return response()->json(['download_token' => $token]);
        }
        else{

            $dirName = base_path('storage/app/templates/cheTemp');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }

            if(is_null($item->template)){
                $path = base_path('storage/app/templates/cheque-default-banks-template.docx');
            }
            else{
                $path = base_path('storage/app/'.$item->template);

            }

            $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);
            try{
                $templateProcessor->cloneRow('rId', 1);

                $i=1;
                $Arabic_numbers=new Arabic_numbers();
                $templateProcessor->setValue('rId#'.$i, $i);
                $templateProcessor->setValue('name#'.$i, $item->name);
                $templateProcessor->setValue('id_card_number#'.$i, $item->id_card_number);
                $templateProcessor->setValue('value#'.$i, '#'.$item->amount.'#');
                $templateProcessor->setValue('character_value#'.$i, $Arabic_numbers->convert_number((float)$item->amount,$exchange));
                $templateProcessor->setValue('cheque_number#'.$i, $item->cheque_account_number);
                $templateProcessor->setValue('cheque_date#'.$i, $item->cheque_date);
                $templateProcessor->saveAs($dirName.'/'.$item->name.'.docx');
            }catch (\Exception $e){  }

            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');

            $zip = new \ZipArchive;
            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                foreach ( glob( $dirName . '/*' ) as $fileName )
                {
                    $file = basename( $fileName );
                    $zip->addFile( $fileName, $file );
                }

                $zip->close();
                array_map('unlink', glob("$dirName/*.*"));
                rmdir($dirName);
            }

            \Log\Model\Log::saveNewLog('CASE_EXPORTED',trans('sponsorship::application.Exported sponsorships cases') );

            return response()->json([
                'download_token' => $token,
            ]);
        }
    }
    // old import cases to payment
    public function import_(Request $request)
    {
        $this->authorize('import',Payments::class);

        $path = \Input::file('file')->getRealPath();
        if (\Input::hasFile('file')) {
            $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
            $objReader->setReadDataOnly(true);
            $excel = $objReader->load($path);
            $sheets = $excel->getSheetNames();

            if(\App\Http\Helpers::sheetFound("data",$sheets)) {
                $coordinate =[];

                $ActiveSheet = array_search('data',$sheets,true);
                $excel->setActiveSheetIndex($ActiveSheet);
                $sheetObj = $excel->getActiveSheet();
                foreach( $sheetObj->getRowIterator(0,1) as $row ){
                    foreach( $row->getCellIterator() as $cell ){
                        $value = $cell->getCalculatedValue();
                        if( $value != null) {
                            $coordinate[]=$value;
                        }
                    }
                }

                $map=[];
                $i=0;
                $First =\Excel::selectSheets('data')->load($path)->first()->keys()->toArray();
                foreach($First as $k =>$v){
                    if(isset($coordinate[$i])){
                        $map[]=['cell' => $coordinate[$i] , 'translate' => $k];
                    }
                    if($i <= sizeof($First)){
                        $i++;
                    }
                }

                return response()->json([ 'map'=>$map]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }
    public function doImport(Request $request)
    {

        $response = array();
        $this->authorize('import',Payments::class);


        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $this->authorize('import',Payments::class);
        $column = $request->get('row');
        $response = array();
        $map=[];
        $target=[];
        foreach ($column as $key => $value ) {
            if(isset($value['column']) ){
                if($value['column'] !="" ){
                    array_push($map,$value['column']);
                    $target[$value['column']]=$value['translate'];
                }}
        }

        $path = \Input::file('file')->getRealPath();
        $data = \Excel::selectSheets('data')->load($path, function ($reader) {})->get();

        $payment_id = $request->get('payment_id');
        $payment = Payments::where('id',$payment_id)->first();

        $sponsor_id=$payment->sponsor_id;
        $Sponsorships_id=[];

        $Sponsorships=Sponsorships::where('sponsor_id',$sponsor_id)->get();
        foreach( $Sponsorships as $k=>$v ) {
            $Sponsorships_id[]=$v->id;
        }

        $cases=[];
        $recipients=[];
        $manipulated_sponsor_number=[];
        $manipulated_id_card_number=[];
        $manipulated_recipients=[];
        $manipulated_cases=[];
        $manipulated=0;
        $duplicated=0;
        $not_saved_sponsor_number=0;
        $not_saved_id_card_number=0;
        $old=[];
        $new=[];
        $all=sizeof($data);
        $header =\Excel::selectSheets('data')->load($path)->first()->keys()->toArray();


        if (\Input::hasFile('file')) {

            if (!empty($data) && $data->count()) {

                foreach ($data as $key => $value) {
                    $ref=null;
                    if(isset($target['sponsor_number'])){
                        $ref=$header[$target['sponsor_number']];
                    }

                    $refCard=null;
                    if(isset($target['id_card_number'])){
                        $refCard=$header[$target['id_card_number']];
                    }

                    if(in_array("sponsor_number", $map) && $value->$ref !=null) {
                        $nominatedCases=SponsorshipCases::where(['sponsor_number' => $value->$ref , 'sponsor_id' => $payment->sponsor_id, 'status' => 3])->first();

                        if(!in_array($value->$ref,$manipulated_sponsor_number)){
                            $manipulated_sponsor_number[] = $value->$ref;
                            if($nominatedCases){
                                $case=SponsorshipCases::getRecord('sponsor_number',$value->$ref,$payment_id,$payment->bank_id,$payment->payment_target,$payment->exchange_type,$sponsor_id);

                                if(!in_array($value->case_id,$manipulated_cases)){
                                    $manipulated_cases[] = $case[0]->case_id;
                                    if( ($case[0]->payment_case_id == null && $case[0]->payments == 0) || ($case[0]->payment_case_id == null && $case[0]->payments != 0) ) {

                                        $target_case=[ 'payment_id' => $payment_id, 'case_id' => $case[0]->case_id, 'guardian_id' => $case[0]->guardian_id,
                                            'sponsor_number' => $case[0]->sponsor_number, 'status' => 1];


                                        if($case[0]->payments != 0){
                                            $target_case['status']= 2;
                                        }


//                                        if($value->$ref == 1321){
//                                            return [$case,$target_case];
//                                        }
                                        if(is_null($case[0]->guardian_id)){
                                            $target_case['recipient']=0;
                                        }else{
                                            if($case[0]->guardian_id == $case[0]->person_id) {
                                                $target_case['recipient']=0;
                                            }else{
                                                $target_case['recipient']=1;
                                            }
                                        }

                                        if(in_array("amount", $map)) {
                                            $amount=0;
                                            $refAmount=$header[$target['amount']];

                                            if(!is_null($value->$refAmount)){
                                                $amount=$value->$refAmount;
                                            }
                                            $target_case['amount']=$amount."";
//                                            +$payment->administration_fees
//                                            +$payment->administration_fees
                                            $target_case['amount_after_discount']=
                                                $amount - (($amount * $payment->organization_share))."";
                                            $target_case['shekel_amount_before_discount']= $amount * $payment->exchange_rate."";
                                            $target_case['shekel_amount']=
                                                round(($amount - (($amount * $payment->organization_share))) *
                                                    $payment->exchange_rate)."";
                                        }

//                                        if(in_array("shekel_amount", $map)) {
//                                            $shekel_amount=0;
//                                            $refShekelAmount=$header[$target['amount']];
//
//                                            if(!is_null($value->$refShekelAmount)){
//                                                $shekel_amount=$value->$refShekelAmount;
//                                            }
//                                            $target_case['shekel_amount']=$shekel_amount;
//                                        }

                                        if(in_array("date_from", $map)) {
                                            $refDateFrom=$header[$target['date_from']];
                                            $target_case['date_from']=date('Y-m-d',strtotime($value->$refDateFrom));
                                        }

                                        if(in_array("date_to", $map)) {
                                            $refDateTo=$header[$target['date_to']];
                                            $target_case['date_to']=date('Y-m-d',strtotime($value->$refDateTo));
                                        }

                                        if($case[0]->payments == 0){
                                            $new[]=$case[0]->case_id;
                                        }else{
                                            $target_case['status']= 2;
                                            $old[]=$case[0]->case_id;
                                        }

                                        $cases[]=$target_case;
                                    }
                                    else if( ($case[0]->payment_case_id != null && $case[0]->payments == 0) || ($case[0]->payment_case_id != null && $case[0]->payments != 0) ){
                                        if($case[0]->payments == 0){
                                            $new[]=$case[0]->case_id;
                                            $updated= ['status' => 1];
                                        }else{
                                            $updated= ['status' => 2];
                                            $old[]=$case[0]->case_id;
                                        }
                                        PaymentsCases::where([ 'payment_id' => $payment_id, 'case_id' => $case[0]->case_id,'sponsor_number' => $case[0]->sponsor_number])->update($updated);
                                    }

                                }else{
                                    $duplicated++;
                                }
                            }
                            else{
                                $not_saved_sponsor_number ++;
                            }
                        }else{
                            $duplicated++;
                        }
                        $manipulated++;
                    }
                    else if(in_array("id_card_number", $map) && $value->$refCard !=null) {
                        if(!in_array($value->$refCard,$manipulated_id_card_number)){
                            $manipulated_id_card_number[] = $value->$refCard;
                            $exist_person=\Common\Model\Person::where(function ($q) use ($value){
                                $q->where('id_card_number',$value->$refCard);
                            })->first();

                            if($exist_person != null){
                                $Records=SponsorshipCases::getRecord('id_card_number',$exist_person->id,$payment_id,$payment->bank_id,$payment->payment_target,$payment->exchange_type,$sponsor_id);
                                foreach ($Records as $kk => $vv) {
                                    if(!in_array($vv->sponsor_number,$manipulated_sponsor_number)){
                                        $manipulated_sponsor_number[] = $vv->sponsor_number;


//                                            if(!in_array($vv->case_id,$manipulated_cases)){
//                                                $manipulated_cases[] = $vv->case_id;
                                        if( ($vv->payment_case_id == null && $vv->payments == 0) || ($vv->payment_case_id == null && $vv->payments != 0) ) {

                                            $target_case=[ 'payment_id' => $payment_id, 'case_id' => $vv->case_id, 'guardian_id' => $vv->guardian_id,
                                                'sponsor_number' => $vv->sponsor_number, 'status' => 1 ];

                                            if(is_null($vv->guardian_id)){
                                                $target_case['recipient']=0;
                                            }else{
                                                if($vv->guardian_id == $vv->person_id) {
                                                    $target_case['recipient']=0;
                                                }else{
                                                    $target_case['recipient']=1;
                                                }
                                            }


                                            if(in_array("amount", $map)) {
                                                $amount=0;
                                                $refAmount=$header[$target['amount']];

                                                if(!is_null($value->$refAmount)){
                                                    $amount=$value->$refAmount;
                                                }
                                                $target_case['amount']=$amount."";
                                                $target_case['amount_after_discount']=$amount - ($amount * $payment->organization_share)."";
                                                $target_case['shekel_amount_before_discount']= $amount * $payment->exchange_rate."";
                                                $target_case['shekel_amount']=round(($amount - ($amount * $payment->organization_share)) * $payment->exchange_rate)."";
                                            }

//                                            if(in_array("shekel_amount", $map)) {
//                                                $shekel_amount=0;
//                                                $refShekelAmount=$header[$target['amount']];
//
//                                                if(!is_null($value->$refShekelAmount)){
//                                                    $shekel_amount=$value->$refShekelAmount;
//                                                }
//                                                $target_case['shekel_amount']=$shekel_amount;
//                                            }

                                            if(in_array("date_from", $map)) {
                                                $refDateFrom=$header[$target['date_from']];
                                                $target_case['date_from']=date('Y-m-d',strtotime($value->$refDateFrom));
                                            }

                                            if(in_array("date_to", $map)) {
                                                $refDateTo=$header[$target['date_to']];
                                                $target_case['date_to']=date('Y-m-d',strtotime($value->$refDateTo));
                                            }


                                            if($vv->payments == 0){
                                                $new[]=$vv->case_id;
                                            }else{
                                                $target_case['status']= 2;
                                                $old[]=$vv->case_id;
                                            }

                                            $cases[]=$target_case;

                                        }else if( ($vv->payment_case_id != null && $vv->payments == 0) || ($vv->payment_case_id != null && $vv->payments != 0) ){
                                            if($vv->payments == 0){
                                                $new[]=$vv->case_id;
                                                $updated= ['status' => 1];
                                            }else{
                                                $updated= ['status' => 2];
                                                $old[]=$vv->case_id;
                                            }
                                            PaymentsCases::where([ 'payment_id' => $payment_id, 'case_id' => $vv->case_id,'sponsor_number' => $vv->sponsor_number])->update($updated);
                                        }
//                                            }else{
//                                                $duplicated++;
//                                            }

                                    }else{
                                        $duplicated++;
                                    }
                                }
                            }else{
                                $not_saved_id_card_number++;
                            }
                        }else{
                            $duplicated++;
                        }
                        $manipulated++;
                    }
                }

                $restricted= $not_saved_id_card_number+$not_saved_sponsor_number+$duplicated;

                $inactive=$new;
                foreach ($old as $key => $value) {
                    $inactive[]= $value;
                }

                if($all == $restricted){
                    if($restricted != 0 && $restricted == $not_saved_sponsor_number){
                        return response()->json(['status' => false,
                            'msg' => trans('sponsorship::application.All sponsorship numbers (nomination codes) entered are not registered or not guaranteed by the sponsor specified for exchange') ]);
                    }

                    if($restricted != 0 && $restricted == $not_saved_id_card_number){
                        return response()->json(['status' => false, 'msg' =>trans('sponsorship::application.All ID numbers entered are not registered in the database')]);
                    }

                    if($restricted != 0 && $restricted == $manipulated){
                        return response()->json(['status' => false, 'msg' => trans('sponsorship::application.All numbers entered are either not registered in the database or are not guaranteed by the sponsor')]);
                    }
                }
                else{


                    if (!empty($cases)) {
                        foreach ($cases as $key => $value) {
                            PaymentsCases::create($value);
                            \Log\Model\Log::saveNewLog('PAYMENTS_CASES_CREATED',trans('sponsorship::application.added new beneficiaries to the payment'));
                        }
                    }

                    $inactive_payments_case_cnt=SponsorshipCases::getInactive($inactive,$payment_id,$payment->sponsor_id,$payment->organization_id);

                    $rows =PaymentsRecipient::getRows($payment_id,0,$payment->bank_id,$payment->exchange_type);
                    $rows->map(function ($subQuery) use($payment_id){

                        $subQuery->amount =0;
                        $subQuery->amount_after_discount =0;
                        $subQuery->shekel_amount =0;
                        $subQuery->shekel_amount_before_discount =0;

                        $id = $subQuery->person_id;

                        $total= \DB::select("select SUM(`amount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $payment_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))  ");

                        if($total){ $subQuery->amount=$total[0]->amount.""; }

                        $amount_after_discount= \DB::select("select SUM(`amount_after_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $payment_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))  ");

                        if($amount_after_discount){$subQuery->amount_after_discount =$amount_after_discount[0]->amount."";}

                        $total_shekel_amount= \DB::select("select SUM(`shekel_amount`) as amount  FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id` = $payment_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))   ");

                        if($total_shekel_amount){$subQuery->shekel_amount=$total_shekel_amount[0]->amount."";}

                        $shekel_amount_before_discount= \DB::select("select SUM(`shekel_amount_before_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $payment_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))  ");

                        if($shekel_amount_before_discount){
                            $subQuery->shekel_amount_before_discount =$shekel_amount_before_discount[0]->amount."";
                        }

                        return $subQuery;
                    });

                    foreach($rows as $k=>$v){
                        if (PaymentsRecipient::where(['payment_id'=>$payment_id,'person_id'=>$v->person_id,'father_id'=>$v->father_id,'mother_id'=>$v->mother_id])->exists()) {
                            PaymentsRecipient::where(['payment_id'=>$payment_id,'person_id'=>$v->person_id,'father_id'=>$v->father_id,'mother_id'=>$v->mother_id])
                                ->update([
                                    'amount'=>$v->amount."",
                                    'amount_after_discount'=> ($v->amount - ($v->amount * $payment->organization_share))."",
                                    'shekel_amount_before_discount'=>($v->amount * $payment->exchange_rate)."",
                                    'shekel_amount'=>round( ( $v->amount - ($v->amount * $payment->organization_share)) * $payment->exchange_rate).""
                                ]);
                        }
                        else{

                            $v->amount_after_discount =($v->amount - ($v->amount * $payment->organization_share))."";
                            $v->shekel_amount_before_discount  = ($v->amount * $payment->exchange_rate)."";
                            $v->shekel_amount  =  round( ( $v->amount - ($v->amount * $payment->organization_share)) * $payment->exchange_rate)."";

                            if($v->father_id ==null){
                                unset($v->father_id);
                            }

                            if($v->mother_id ==null){
                                unset($v->mother_id);
                            }

                            if($payment->exchange_type ==3){
                                unset($v->bank_id);
                                unset($v->cheque_account_number);
                            }


                            $item=(array)$v;
                            $item['status']=0;
                            $item['payment_id']=$payment_id;
                            PaymentsRecipient::create($item);
                        }
                    }
                    $rows =PaymentsRecipient::getRows($payment_id,1,$payment->bank_id,$payment->exchange_type);
                    $rows->map(function ($subQuery) use($payment_id){

                        $subQuery->amount =0;
                        $subQuery->amount_after_discount =0;
                        $subQuery->shekel_amount =0;
                        $subQuery->shekel_amount_before_discount =0;

                        $id = $subQuery->person_id;

                        $total= \DB::select("select SUM(`amount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $payment_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))  ");

                        if($total){ $subQuery->amount=$total[0]->amount.""; }

                        $amount_after_discount= \DB::select("select SUM(`amount_after_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $payment_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))  ");

                        if($amount_after_discount){$subQuery->amount_after_discount =$amount_after_discount[0]->amount."";}

                        $total_shekel_amount= \DB::select("select SUM(`shekel_amount`) as amount  FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id` = $payment_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))   ");

                        if($total_shekel_amount){$subQuery->shekel_amount=$total_shekel_amount[0]->amount."";}

                        $shekel_amount_before_discount= \DB::select("select SUM(`shekel_amount_before_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $payment_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))  ");

                        if($shekel_amount_before_discount){
                            $subQuery->shekel_amount_before_discount =$shekel_amount_before_discount[0]->amount."";
                        }

                        return $subQuery;
                    });

                    foreach($rows as $k=>$v){
                        if (PaymentsRecipient::where(['payment_id'=>$payment_id,'person_id'=>$v->person_id,'father_id'=>$v->father_id,'mother_id'=>$v->mother_id])->exists()) {
                            PaymentsRecipient::where(['payment_id'=>$payment_id,'person_id'=>$v->person_id,'father_id'=>$v->father_id,'mother_id'=>$v->mother_id])
                                ->update([
                                    'amount'=>$v->amount."",
                                    'amount_after_discount'=> ($v->amount - ($v->amount * $payment->organization_share))."",
                                    'shekel_amount_before_discount'=>($v->amount * $payment->exchange_rate)."",
                                    'shekel_amount'=>round( ( $v->amount - ($v->amount * $payment->organization_share)) * $payment->exchange_rate).""
                                ]);

                        }else{

                            $v->amount_after_discount =($v->amount - ($v->amount * $payment->organization_share))."";
                            $v->shekel_amount_before_discount  = ($v->amount * $payment->exchange_rate)."";
                            $v->shekel_amount  =  round( ( $v->amount - ($v->amount * $payment->organization_share)) * $payment->exchange_rate)."";

                            if($v->father_id ==null){
                                unset($v->father_id);
                            }

                            if($v->mother_id ==null){
                                unset($v->mother_id);
                            }

                            if($payment->exchange_type ==3){
                                unset($v->bank_id);
                                unset($v->cheque_account_number);
                            }


                            $item=(array)$v;
                            $item['status']=0;
                            $item['payment_id']=$payment_id;
                            PaymentsRecipient::create($item);
                        }
                    }

                    $max_allowed_amount = $payment->amount - ($payment->amount * $payment->organization_share) - $payment->administration_fees;
                    $response['total']=PaymentsCases::where(["payment_id"=>$payment_id])->sum('amount');

                    $response['status']=true;
                    $response['exceed']=false;
                    $response['msg']=
                        trans('sponsorship::application.The row is inserted to db') .' , '.
                        trans('sponsorship::application.total in file') .' : '.  $manipulated.' , '.
                        trans('sponsorship::application.not_saved_id_card_number') .' : '. $not_saved_id_card_number.' , '.
                        trans('sponsorship::application.not_saved_sponsor_number') .' : '. $not_saved_sponsor_number.' , '.
                        trans('sponsorship::application.duplicated') .' : '. $duplicated.' , '.
                        trans('sponsorship::application.new_cases') .' : '. PaymentsCases::where(["payment_id"=>$payment_id,'status'=>1])->count().' , '.
                        trans('sponsorship::application.old_cases') .' : '. PaymentsCases::where(["payment_id"=>$payment_id,'status'=>2])->count().' , '.
                        trans('sponsorship::application.inactive_cases') . ' : '.PaymentsCases::where(["payment_id"=>$payment_id,'status'=>3])->count() ;

                    if($max_allowed_amount < $response['total']){
                        $response['exceed']=true;
                        $response['msg'] = $response['msg'] .' , '. trans('sponsorship::application.The amount exceed the allowed amount') ;
                    }

                }
                return response()->json($response);

//            trans('sponsorship::application.new_cases') .' : '. PaymentsCases::where(["payment_id"=>$payment_id,'status'=>1])->count().' , '.
//            trans('sponsorship::application.old_cases') .' : '. PaymentsCases::where(["payment_id"=>$payment_id,'status'=>2])->count().' , '.
//            trans('sponsorship::application.inactive_cases') . ' : '.PaymentsCases::where(["payment_id"=>$payment_id,'status'=>3])->count() ;
//            trans('sponsorship::application.new_cases') .' : '. sizeof($new).' , '.
//            trans('sponsorship::application.old_cases') .' : '. sizeof($old).' , '.
//            trans('sponsorship::application.inactive_cases') . ' : '.$inactive_payments_case_cnt ;

//            'count'=>PaymentsCases::where(["payment_id"=>$id])->count(),
//                                      'new_'=>PaymentsCases::where(["payment_id"=>$payment_id,'status'=>1])->count(),
//                                      'old_'=>PaymentsCases::where(["payment_id"=>$payment_id,'status'=>2])->count(),
//                                      'no_payment'=>PaymentsCases::where(["payment_id"=>$payment_id,'status'=>3])->count()

            }
            return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

        }
        else{
            $response['status']=false;
            $response['msg']=trans('common::application.no file selected to upload');
        }
        return response()->json($response);



    }
    public function importPayment(Request $request)
    {
        $this->authorize('create',Payments::class);

        $path = \Input::file('file')->getRealPath();
        if (\Input::hasFile('file')) {
            $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
            $objReader->setReadDataOnly(true);
            $excel = $objReader->load($path);
            $sheets = $excel->getSheetNames();

            if(\App\Http\Helpers::sheetFound("data",$sheets)) {
                $coordinate =[];

                $ActiveSheet = array_search('data',$sheets,true);
                $excel->setActiveSheetIndex($ActiveSheet);
                $sheetObj = $excel->getActiveSheet();
                foreach( $sheetObj->getRowIterator(0,1) as $row ){
                    foreach( $row->getCellIterator() as $cell ){
                        $value = $cell->getCalculatedValue();
                        if( $value != null) {
                            $coordinate[]=$value;
                        }
                    }
                }

                $map=[];
                $i=0;
                $First =\Excel::selectSheets('data')->load($path)->first()->keys()->toArray();
                foreach($First as $k =>$v){
                    if(isset($coordinate[$i])){
                        $map[]=['cell' => $coordinate[$i] , 'translate' => $k];
                    }
                    if($i <= sizeof($First)){
                        $i++;
                    }
                }

                return response()->json([ 'map'=>$map]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }
    public function doImportPayment_old(Request $request)
    {

        $this->authorize('create',Payments::class);
        $response = array();
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $column = $request->get('row');
        $response = array();
        $map=[];
        $target=[];
        foreach ($column as $key => $value ) {
            if(isset($value['column']) ){
                if($value['column'] !="" ){
                    array_push($map,$value['column']);
                    $target[$value['column']]=$value['translate'];
                }}
        }

        $path = \Input::file('file')->getRealPath();
        $data = \Excel::selectSheets('data')->load($path, function ($reader) {})->get();

        $cases=[];
        $recipients=[];
        $manipulated_sponsor_number=[];
        $manipulated_id_card_number=[];
        $manipulated_recipients=[];
        $manipulated_cases=[];
        $manipulated=0;
        $duplicated=0;
        $not_saved_sponsor_number=0;
        $not_saved_id_card_number=0;
        $old=[];
        $new=[];
        $all=sizeof($data);
        $header =\Excel::selectSheets('data')->load($path)->first()->keys()->toArray();
        $catRef=null;

        if(isset($target['payment_category'])){
            if(isset($target['payment_category'])){
                $catRef = $header[$target['payment_category']];
            }
        }

        $categories = [];
        $catPayment = [];
        $not_allowed_category_numbers = 0;
        $not_allowed_category = 0;
        $allowed_category = [];

        if (\Input::hasFile('file')) {
            if (!empty($data) && $data->count()) {

                if(!is_null($catRef)){

                    $attributes=$request->all();
                    $payment=[];
                    $inputs = ['sponsorship_id','category_id','sponsor_id','organization_id','payment_date','amount','currency_id','payment_exchange','bank_id','payment_target'
                        ,'exchange_rate','exchange_date','organization_share','administration_fees','status','exchange_type','transfer','transfer_company_id'];

                    foreach ($attributes as $key => $value) {
                        if ( $value != "" ) {
                            if(in_array($key, $inputs)) {
                                if ($key =='exchange_date' || $key =='payment_date') {
                                    $payment[$key] = date('Y-m-d',strtotime($value));
                                }else{
                                    $payment[$key]=$value;
                                }
                            }
                        }
                    }

                    if($payment['transfer'] != 1 || $payment['transfer'] != '1'){
                        $payment['transfer_company_id']=null;

                    }
                    foreach ($data as $key => $value) {
                        if($value->$catRef){
                            if(!is_null($value->$catRef) && $value->$catRef !=''){
                                if(!in_array($value->$catRef,$categories)){
                                    $categories[] = $value->$catRef;
                                    $category_id=\Common\Model\CaseModel::constantId('payment_category',$value->$catRef,null,null);
                                    if(!is_null($category_id)){
                                        $allowed_category[]=$value->$catRef;
                                        $catPayment[$value->$catRef] =['id'=>$category_id,'cases'=>[],'manipulated'=>[],'manipulated_sponsor_number'=>[]] ;
                                    }else{
                                        $not_allowed_category++;
                                    }
                                }
                            }
                        }
                    }

                    if(sizeof($allowed_category) > 0){
                        $bank_id=null;
                        $exchange_type=null;
                        $sponsor_id=null;

                        if(isset($payment['bank_id'])){
                            $bank_id=$payment['bank_id'];
                        }
                        if(isset($payment['exchange_type'])){
                            $exchange_type=$payment['exchange_type'];
                        }
                        if(isset($payment['sponsor_id'])){
                            $sponsor_id=$payment['sponsor_id'];
                        }

                        foreach ($data as $key => $value) {
                            $ref=null;
                            if(isset($target['sponsor_number'])){
                                $ref=$header[$target['sponsor_number']];
                            }

                            if(in_array($value->$catRef , $allowed_category)) {
                                if(in_array("sponsor_number", $map) && $value->$ref !=null) {
                                    $nominatedCases=SponsorshipCases::where(['sponsor_number' => $value->$ref , 'sponsor_id' => $sponsor_id, 'status' => 3])->first();
                                    if(!in_array($value->$ref,$catPayment[$value->$catRef]['manipulated_sponsor_number'])){
                                        $manipulated_sponsor_number[] = $value->$ref;
                                        if($nominatedCases){
                                            $case=SponsorshipCases::getNominated($value->$ref,$bank_id,$exchange_type,$sponsor_id);
//                                        if(!in_array($value->case_id,$catPayment[$value->$catRef]['manipulated'])){
                                            $catPayment[$value->$catRef]['manipulated'][]= $case->case_id;

                                            if(in_array("amount", $map)) {
                                                $amount=0;
                                                $refAmount=$header[$target['amount']];

                                                if(!is_null($value->$refAmount)){
                                                    $amount=$value->$refAmount;
                                                    if($amount != 0 && $amount != '0' ){
                                                        $target_case=['case_id' => $case->case_id,'recipient' => $case->recipient, 'guardian_id' => $case->guardian_id,
                                                            'sponsor_number' => $case->sponsor_number, 'status' => 1];


                                                        if($case->payments_count != 0){
                                                            $target_case['status']= 2;
                                                        }

                                                        $target_case['amount']=$amount."";
                                                        $target_case['amount_after_discount']=($amount - ($amount * $payment['organization_share']))."";
                                                        $target_case['shekel_amount_before_discount']= ($amount * $payment['exchange_rate'])."";
                                                        $target_case['shekel_amount']=round(($amount - ($amount * $payment['organization_share'])) * $payment['exchange_rate'])."";
                                                        if(in_array("date_from", $map)) {
                                                            $refDateFrom=$header[$target['date_from']];
                                                            $target_case['date_from']=date('Y-m-d',strtotime($value->$refDateFrom));
                                                        }

                                                        if(in_array("date_to", $map)) {
                                                            $refDateTo=$header[$target['date_to']];
                                                            $target_case['date_to']=date('Y-m-d',strtotime($value->$refDateTo));
                                                        }

                                                        $catPayment[$value->$catRef]['cases'][]=$target_case;

                                                    }

                                                }
                                            }

//                                        }
//                                        else{
//                                            $duplicated++;
//                                        }
                                        }
                                        else{
                                            $not_saved_sponsor_number ++;
                                        }
                                    }else{
                                        $duplicated++;
                                    }
                                    $manipulated++;
                                }
                            }else{
                                $not_allowed_category_numbers++;
                            }
                        }
                    }else{
                        return response()->json(['status' => false, 'msg' =>trans('sponsorship::application.All sponsorship type is not registered in the system')]);
                    }
                }else{
                    return response()->json(['status' => false, 'msg' => trans('sponsorship::application.You do not specify the corresponding column for the sponsorship classification in the file')]);

                }
            }
            $restricted = $not_allowed_category_numbers  +  $not_saved_sponsor_number + $not_saved_id_card_number;

            if($all == $restricted){
                if($restricted != 0 && $restricted == $not_allowed_category_numbers){
                    return response()->json(['status' => false,
                        'msg' =>trans('sponsorship::application.All sponsorship numbers (nomination codes) entered into a banking class not registered in the system')]);
                }

                if($restricted != 0 && $restricted == $not_saved_sponsor_number){
                    return response()->json(['status' => false,
                        'msg' =>trans('sponsorship::application.All sponsorship numbers (nomination codes) entered are not registered or not guaranteed by the sponsor specified for exchange')]);
                }

                if($restricted != 0 && $restricted == $not_saved_id_card_number){
                    return response()->json(['status' => false, 'msg' =>trans('sponsorship::application.All ID numbers entered are not registered in the database')]);
                }

                if($restricted != 0 && $restricted == $manipulated){
                    return response()->json(['status' => false, 'msg' =>trans('sponsorship::application.All numbers entered are either not registered in the database or are not guaranteed by the sponsor')]);
                }
            }
            else{

//                return [$catPayment,$not_allowed_category_numbers,$manipulated,$duplicated,$not_saved_sponsor_number];
                foreach ($catPayment as $key => $value) {
                    $payment['category_id']=$value['id'];
                    $user = \Auth::user();
                    $payment['organization_id']=$user->organization_id;
                    $inserted= Payments::create($payment);
                    $inserted_id=$inserted->id;
                    $ids[]=$inserted->id;
                    if (!empty($value['cases'])) {
                        foreach ($value['cases'] as $k => $v) {
                            $v['payment_id'] = $inserted->id;
                            PaymentsCases::create($v);
                        }
                        Payments::where('id', $inserted_id)->update(['amount' => PaymentsCases::where('payment_id', $inserted_id)->sum('amount').""]);
                        $inactive=[];
                        $old = PaymentsCases::where('payment_id', $inserted_id)->get(['case_id']);
                        foreach ($old as $k => $v) {
                            $inactive[]=$v->case_id;
                        }

                        SponsorshipCases::getInactive($inactive,$inserted_id,$inserted->sponsor_id,$inserted->organization_id);

                        $rows = PaymentsRecipient::getRows($inserted->id, 0, $inserted->bank_id, $inserted->exchange_type);
                        $rows->map(function ($subQuery) use ($inserted_id) {

                            $subQuery->amount = 0;
                            $subQuery->amount_after_discount = 0;
                            $subQuery->shekel_amount = 0;
                            $subQuery->shekel_amount_before_discount = 0;

                            $id = $subQuery->person_id;

                            $total = \DB::select("select SUM(`amount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))  ");

                            if ($total) {
                                $subQuery->amount = $total[0]->amount."";
                            }

                            $amount_after_discount = \DB::select("select SUM(`amount_after_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))  ");

                            if ($amount_after_discount) {
                                $subQuery->amount_after_discount = $total[0]->amount."";
                            }
                            if ($amount_after_discount) {
                                $subQuery->amount_after_discount = $total[0]->amount."";
                            }

                            $total_shekel_amount = \DB::select("select SUM(`shekel_amount`) as amount  FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id` = $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))   ");

                            if ($total_shekel_amount) {
                                $subQuery->shekel_amount = $total_shekel_amount[0]->amount."";
                            }

                            $shekel_amount_before_discount = \DB::select("select SUM(`shekel_amount_before_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0))  ");

                            if ($shekel_amount_before_discount) {
                                $subQuery->shekel_amount_before_discount_amount = $shekel_amount_before_discount[0]->amount."";
                            }

                            return $subQuery;
                        });

                        foreach ($rows as $k => $v) {
                            if (PaymentsRecipient::where(['payment_id' => $inserted_id, 'person_id' => $v->person_id, 'father_id' => $v->father_id, 'mother_id' => $v->mother_id])->exists()) {
                                PaymentsRecipient::where(['payment_id' => $inserted_id, 'person_id' => $v->person_id, 'father_id' => $v->father_id, 'mother_id' => $v->mother_id])
                                    ->update([
                                        'amount' => $v->amount."",
                                        'amount_after_discount' => ($v->amount - ($v->amount * $inserted->organization_share))."",
                                        'shekel_amount_before_discount' => ($v->amount * $inserted->exchange_rate)."",
                                        'shekel_amount' => round(($v->amount - ($v->amount * $inserted->organization_share)) * $inserted->exchange_rate).""
                                    ]);
                            } else {

                                $v->amount_after_discount = ($v->amount - ($v->amount * $inserted->organization_share)."");
                                $v->shekel_amount_before_discount = ($v->amount * $inserted->exchange_rate)."";
                                $v->shekel_amount = round(($v->amount - ($v->amount * $inserted->organization_share)) * $inserted->exchange_rate)."";

                                if ($v->father_id == null) {
                                    unset($v->father_id);
                                }

                                if ($v->mother_id == null) {
                                    unset($v->mother_id);
                                }

                                if ($inserted->exchange_type == 3) {
                                    unset($v->bank_id);
                                    unset($v->cheque_account_number);
                                }


                                $item = (array)$v;
                                $item['status'] = 0;
                                $item['payment_id'] = $inserted_id;
                                PaymentsRecipient::create($item);
                            }
                        }
                        $rows = PaymentsRecipient::getRows($inserted->id, 1, $inserted->bank_id, $inserted->exchange_type);
                        $rows->map(function ($subQuery) use ($inserted_id) {

                            $subQuery->amount = 0;
                            $subQuery->amount_after_discount = 0;
                            $subQuery->shekel_amount = 0;
                            $subQuery->shekel_amount_before_discount = 0;

                            $id = $subQuery->person_id;

                            $total = \DB::select("select SUM(`amount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))  ");

                            if ($total) {
                                $subQuery->amount = $total[0]->amount."";
                            }

                            $amount_after_discount = \DB::select("select SUM(`amount_after_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))  ");

                            if ($amount_after_discount) {
                                $subQuery->amount_after_discount = $amount_after_discount[0]->amount."";
                            }

                            $total_shekel_amount = \DB::select("select SUM(`shekel_amount`) as amount  FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id` = $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))   ");

                            if ($total_shekel_amount) {
                                $subQuery->shekel_amount = $total_shekel_amount[0]->amount."";
                            }

                            $shekel_amount_before_discount = \DB::select("select SUM(`shekel_amount_before_discount`) as amount   FROM `char_payments_cases`
                                 join char_cases on char_cases .id = char_payments_cases.case_id
                                  where `payment_id`= $inserted_id and 
                                        ((char_cases.`person_id` = $id and recipient = 0) or( `guardian_id` = $id and recipient = 1 ))  ");

                            if ($shekel_amount_before_discount) {
                                $subQuery->shekel_amount_before_discount_amount = $shekel_amount_before_discount[0]->amount."";
                            }

                            return $subQuery;
                        });

                        foreach ($rows as $k => $v) {
                            if (PaymentsRecipient::where(['payment_id' => $inserted_id, 'person_id' => $v->person_id, 'father_id' => $v->father_id, 'mother_id' => $v->mother_id])->exists()) {
                                PaymentsRecipient::where(['payment_id' => $inserted_id, 'person_id' => $v->person_id, 'father_id' => $v->father_id, 'mother_id' => $v->mother_id])
                                    ->update([
                                        'amount' => $v->amount,
                                        'amount_after_discount' => ($v->amount - ($v->amount * $inserted->organization_share))."",
                                        'shekel_amount_before_discount' => ($v->amount * $inserted->exchange_rate)."",
                                        'shekel_amount' => round(($v->amount - ($v->amount * $inserted->organization_share)) * $inserted->exchange_rate).""
                                    ]);

                            } else {

                                $v->amount_after_discount = ($v->amount - ($v->amount * $inserted->organization_share))."";
                                $v->shekel_amount_before_discount = ($v->amount * $inserted->exchange_rate)."";
                                $v->shekel_amount = round(($v->amount - ($v->amount * $inserted->organization_share)) * $inserted->exchange_rate)."";

                                if ($v->father_id == null) {
                                    unset($v->father_id);
                                }

                                if ($v->mother_id == null) {
                                    unset($v->mother_id);
                                }

                                if ($inserted->exchange_type == 3) {
                                    unset($v->bank_id);
                                    unset($v->cheque_account_number);
                                }


                                $item = (array)$v;
                                $item['status'] = 0;
                                $item['payment_id'] = $inserted_id;
                                PaymentsRecipient::create($item);
                            }
                        }

                        \Log\Model\Log::saveNewLog('PAYMENTS_CASES_CREATED',trans('sponsorship::application.added new beneficiaries to the payment'));
                    }
                }


                $response['statistic']=Payments::statistic($ids);
                $response['status']=true;
                $response['msg']=
                    trans('sponsorship::application.The row is inserted to db') .' , '.
                    trans('sponsorship::application.total in file') .' : '.  $manipulated.' , '.
                    trans('sponsorship::application.not_allowed_category_numbers') .' : '. $not_allowed_category_numbers.' , '.
                    trans('sponsorship::application.not_saved_id_card_number') .' : '. $not_saved_id_card_number.' , '.
                    trans('sponsorship::application.not_saved_sponsor_number') .' : '. $not_saved_sponsor_number.' , '.
                    trans('sponsorship::application.duplicated') .' : '. $duplicated;
            }

        }
        else{
            $response['status']=false;
            $response['msg']=trans('common::application.no file selected to upload');
        }
        return response()->json($response);



    }

}