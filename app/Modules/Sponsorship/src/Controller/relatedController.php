<?php
namespace Sponsorship\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Sponsorship\Model\Person;
use Sponsorship\Model\Sponsorships;
use Setting\Model\Setting;

class relatedController extends Controller
{
    public function __construct()
        {
            $this->middleware('auth:api');
        }

    // get family of edges (left , mother) using person's id
    public function getFamily(Request $request)
        {
            $user = \Auth::user();
            $has_Wives= false;
            $family_structure= 2;

            if($request->category_id){
               $category=\Common\Model\Categories\SponsorshipCategories::where(['id'=>$request->category_id])->first();
               if($category){
                   $family_structure= $category->family_structure;
                   if($category->has_Wives !=0){
                       $has_Wives=true;
                   }
               }
            }

            $members =Person::getFamilyMember('filter',null, $request->get('l_person_id'), $request->get('mother_id'),
                $user->organization_id,$has_Wives);
            $members['family_structure'] = $family_structure;
            return response()->json($members);
        }

    // get template of update status
    public function updateStatusTemplate(Request $request){
        return response()->json(['download_token' => 'update-status-template']);
    }

    // get sponsorships list using sponsor_id and logged organization_id
    public function getSponsorshipList($id){
        $user = \Auth::user();
        return response()->json(['s'=>Sponsorships::getList($id,$user->organization_id)]);
    }

    // get cheque bank or org template
    public function templateInstruction()
    {
        return response()->json(['download_token' => 'instruction-cheque-template']);
    }

    // get default cheque bank
    public function SysDefaultTemplate()
    {
        return response()->json(['download_token' => 'cheque-default-template']);
    }

    // get default cheque bank of org template
    public function defaultTemplate($id)
    {
        $user = \Auth::user();
        $template=Setting::where(['id'=>'default-cheque-template','organization_id'=>$user->organization_id])->first();
        $this->authorize('view', $template);

        $sub = explode(".", $template->value);
        $token = explode("/", $sub[0]);

        return response()->json(['download_token' => $token[1]]);
    }

    // upload default cheque bank of org template
    public function defaultTemplateUpload(Request $request)
    {
        $user = \Auth::user();
        $templateFile = $request->file('file');
        $templatePath = $templateFile->store('templates');
        if ($templatePath) {

            $count= Setting::where(['id'=>'default-cheque-template','organization_id'=>$user->organization_id])->count();
            if($count==1){
                $template=Setting::where(['id'=>'default-cheque-template','organization_id'=>$user->organization_id])->first();
                if (\Storage::has($template->value)) {
                    \Storage::delete($template->value);
                }
                Setting::where('id','default-cheque-template')->update(['value' => $templatePath]);
            }else{
                Setting::insert(['id'=>'default-cheque-template','value'=>$templatePath,'organization_id'=>$user->organization_id]);
            }

            \Log\Model\Log::saveNewLog('PERSONS_VOUCHER_EXPORT',trans('sponsorship::application.Uploaded the default voucher export template'));

            return Setting::where(['id'=>'default-cheque-template','organization_id'=>$user->organization_id])->first();
//            return response()->json($templatePath);
        }
        return false;
    }
}
