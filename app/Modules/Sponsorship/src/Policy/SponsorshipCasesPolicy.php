<?php

namespace Sponsorship\Policy;
use Auth\Model\User;
use Sponsorship\Model\SponsorshipCases;

class SponsorshipCasesPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('sponsorship.sponsorshipCases.manage')) {
            return true;
        }

        return false;
    }

     public function search(User $user)
        {
        if ($user->hasPermission('sponsorship.sponsorshipCases.search')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('sponsorship.sponsorshipCases.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, SponsorshipCases $sponsorshipcases = null)
    {
        if ($user->hasPermission('sponsorship.sponsorshipCases.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, SponsorshipCases $sponsorshipcases = null)
    {
        if ($user->hasPermission('sponsorship.sponsorshipCases.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, SponsorshipCases $sponsorshipcases = null)
    {
        if ($user->hasPermission(['sponsorship.sponsorshipCases.view','sponsorship.sponsorshipCases.update'])) {
            return true;
        }

        return false;
    }

    public function export(User $user, SponsorshipCases $sponsorshipcases = null)
    {
        if ($user->hasPermission('sponsorship.sponsorshipCases.export')) {
            return true;
        }

        return false;
    }

    public function import(User $user, SponsorshipCases $sponsorshipcases = null)
    {
        if ($user->hasPermission('sponsorship.sponsorshipCases.create')&&$user->hasPermission('sponsorship.sponsorshipCases.update')) {
            return true;
        }

        return false;
    }

}

