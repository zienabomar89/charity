<?php

namespace Sponsorship\Policy;
use Auth\Model\User;
use Sponsorship\Model\Reports;

class ReportsPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('sponsorship.reports.manage')) {
            return true;
        }

        return false;
    }

      public function create(User $user)
    {
        if ($user->hasPermission('sponsorship.reports.create')) {
            return true;
        }

        return false;
    }
    public function update(User $user, Reports $report = null)
    {
        if ($user->hasPermission('sponsorship.reports.update')) {
            return true;
        }

        return false;
    }
    public function delete(User $user, Reports $report = null)
    {
        if ($user->hasPermission('sponsorship.reports.delete')) {
            return true;
        }

        return false;
    }
    public function view(User $user, Reports $report = null)
    {
        if ($user->hasPermission(['sponsorship.reports.view','sponsorship.reports.update'])) {
            return true;
        }

        return false;
    }
    public function export(User $user, Reports $report = null)
    {
        if ($user->hasPermission('sponsorship.reports.export')) {
            return true;
        }

        return false;
    }

}

