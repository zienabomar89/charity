<?php

namespace Sponsorship\Policy;
use Auth\Model\User;
use Sponsorship\Model\ReportsCasesDocuments;

class ReportsCasesDocumentsPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('sponsorship.reportsCasesDocuments.manage')) {
            return true;
        }

        return false;
    }

      public function create(User $user)
    {
        if ($user->hasPermission('sponsorship.reportsCasesDocuments.create')) {
            return true;
        }

        return false;
    }
    public function update(User $user, ReportsCasesDocuments $report = null)
    {
        if ($user->hasPermission('sponsorship.reportsCasesDocuments.update')) {
            return true;
        }

        return false;
    }
    public function delete(User $user, ReportsCasesDocuments $report = null)
    {
        if ($user->hasPermission('sponsorship.reportsCasesDocuments.delete')) {
            return true;
        }

        return false;
    }
    public function view(User $user, ReportsCasesDocuments $report = null)
    {
        if ($user->hasPermission(['sponsorship.reportsCasesDocuments.view','sponsorship.reportsCasesDocuments.update'])) {
            return true;
        }

        return false;
    }
    public function export(User $user, ReportsCasesDocuments $report = null)
    {
        if ($user->hasPermission('sponsorship.reportsCasesDocuments.export')) {
            return true;
        }

        return false;
    }
    public function import(User $user, ReportsCasesDocuments $report = null)
    {
        if ($user->hasPermission('sponsorship.reportsCasesDocuments.import')) {
            return true;
        }

        return false;
    }
}

