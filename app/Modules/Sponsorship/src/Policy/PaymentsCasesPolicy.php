<?php

namespace Sponsorship\Policy;
use Auth\Model\User;
use Sponsorship\Model\PaymentsCases;

class PaymentsCasesPolicy
{
  
    public function manage(User $user)
    {
        if ($user->hasPermission('sponsorship.paymentsCases.manage')) {
            return true;
        }

        return false;
    }

 
    public function create(User $user)
    {
        if ($user->hasPermission('sponsorship.paymentsCases.create')) {
            return true;
        }

        return false;
    }
    public function update(User $user,PaymentsCases $paymentsCases = null)
    {
        if ($user->hasPermission('sponsorship.paymentsCases.update')) {
            return true;
        }

        return false;
    }
    public function delete(User $user,PaymentsCases $paymentsCases = null)
    {
        if ($user->hasPermission('sponsorship.paymentsCases.delete')) {
            return true;
        }

        return false;
    }
    public function view(User $user,PaymentsCases $paymentsCases = null)
    {
        if ($user->hasPermission(['sponsorship.paymentsCases.view','sponsorship.paymentsCases.update'])) {
            return true;
        }

        return false;
    }
    public function export(User $user,PaymentsCases $paymentsCases = null)
    {
        if ($user->hasPermission('sponsorship.paymentsCases.export')) {
            return true;
        }

        return false;
    }

}

