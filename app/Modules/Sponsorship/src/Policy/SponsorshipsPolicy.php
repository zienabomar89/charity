<?php
namespace Sponsorship\Policy;
use Auth\Model\User;
use Sponsorship\Model\Sponsorships;

class SponsorshipsPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('sponsorship.sponsorships.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('sponsorship.sponsorships.create')) {
        return true;
    }

        return false;
    }

    public function update(User $user, Sponsorships $sponsorships = null)
    {
        if ($user->hasPermission('sponsorship.sponsorships.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Sponsorships $sponsorships = null)
    {
        if ($user->hasPermission('sponsorship.sponsorships.delete')) {
        return true;
    }

        return false;
    }

    public function view(User $user, Sponsorships $sponsorships = null)
    {
        if ($user->hasPermission('sponsorship.sponsorships.view')) {
            return true;
        }

        return false;
    }
}

