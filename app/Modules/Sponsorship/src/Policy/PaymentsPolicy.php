<?php

namespace Sponsorship\Policy;
use Auth\Model\User;
use Sponsorship\Model\Payments;

class PaymentsPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('sponsorship.payments.manage')) {
            return true;
        }

        return false;
    }

      public function create(User $user)
    {
        if ($user->hasPermission('sponsorship.payments.create')) {
            return true;
        }

        return false;
    }
    public function update(User $user, Payments $payments = null)
    {
        if ($user->hasPermission('sponsorship.payments.update')) {
            return true;
        }

        return false;
    }
    public function delete(User $user, Payments $payments = null)
    {
        if ($user->hasPermission('sponsorship.payments.delete')) {
            return true;
        }

        return false;
    }
    public function view(User $user, Payments $payments = null)
    {
        if ($user->hasPermission(['sponsorship.payments.view','sponsorship.payments.update'])) {
            return true;
        }

        return false;
    }
    public function export(User $user, Payments $payments = null)
    {
        if ($user->hasPermission('sponsorship.payments.export')) {
            return true;
        }

        return false;
    }

    public function upload(User $user, Payments $payments = null)
    {
        if ($user->hasPermission('sponsorship.payments.upload')) {
            return true;
        }

        return false;
    }
    public function import(User $user, Payments $payments = null)
    {
        if ($user->hasPermission('sponsorship.payments.import')) {
            return true;
        }

        return false;
    }
}

