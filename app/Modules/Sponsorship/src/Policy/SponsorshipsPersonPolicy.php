<?php
namespace Sponsorship\Policy;
use Auth\Model\User;
use Sponsorship\Model\SponsorshipsPerson;

class SponsorshipsPersonPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('sponsorship.sponsorshipsPerson.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('sponsorship.sponsorshipsPerson.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, SponsorshipsPerson $sponsorshipsperson = null)
    {
        if ($user->hasPermission('sponsorship.sponsorshipsPerson.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, SponsorshipsPerson $sponsorshipsperson = null)
    {
        if ($user->hasPermission('sponsorship.sponsorshipsPerson.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, SponsorshipsPerson $sponsorshipsperson = null)
    {
        if ($user->hasPermission(['sponsorship.sponsorshipsPerson.view','sponsorship.sponsorshipsPerson.update'])) {
            return true;
        }

        return false;
    }

    public function export(User $user, SponsorshipsPerson $sponsorshipsperson = null)
    {
        if ($user->hasPermission('sponsorship.sponsorshipsPerson.export')) {
            return true;
        }

        return false;
    }
}

