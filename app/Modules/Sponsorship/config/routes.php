<?php

Route::get('/cheque/print/{id}','PaymentController@getView');

Route::group(['prefix' => 'api/v1.0/sponsorship/paymentsService' ,'namespace' => 'Sponsorship\Controller'], function() {
    Route::post('importExcel', 'PaymentController@importCases');
    Route::post('ImportPaymentExcel', 'PaymentController@importPaymentsCases');
});

Route::group(['middleware' => ['api','lang','auth.check'], 'prefix' => 'api/v1.0/sponsorship' , 'namespace' => 'Sponsorship\Controller'], function() {

    //---------------------------------------- PAYMENTS -------------------------------------------------------------
         Route::group(['prefix' => 'paymentsService'], function() {

             Route::post('exportCustom','PaymentController@exportCustom');
             Route::post('SponsorPayments','SponsorController@all');
             Route::post('SponsorPersons','SponsorController@persons');

             Route::delete('deleteDocument/{id}','PaymentController@deleteDocument');

             Route::put('updateRecipientStatus/{id}','PaymentController@updateRecipientStatus');
             Route::put('updateChequeNumber/{id}','PaymentController@updateChequeNumber');
             Route::put('updatePaymentsRecipient/{id}','PaymentController@updatePaymentsRecipient');
             Route::put('updateDateRange/{id}','PaymentController@updateDateRange');
             Route::put('savePaymentCases/{id}','PaymentController@savePaymentCases');
             Route::put('saveCaseRecipient/{id}','PaymentController@saveCaseRecipient');
             Route::put('saveRecipient/{id}','PaymentController@saveRecipient');
             Route::put('updateGuaranteed/{id}','PaymentController@updateGuaranteed');
             Route::put('updateStatus/{id}','PaymentController@updateStatus');

             Route::get('importPaymentTemplate/','PaymentController@importPaymentTemplate');
             Route::get('getChequeData/{id}','PaymentController@getChequeData');
             Route::get('getPaymentChequeData/{id}','PaymentController@getPaymentChequeData');
             Route::get('exportCaseRecipient/{id}','PaymentController@exportCaseRecipient');
             Route::get('archivedCheques/download/{id}','PaymentController@downloadArchivedCheques');
             Route::get('list/{id}','PaymentController@getOptions');
             Route::get('getPaymentDocuments/{id}','PaymentController@getPaymentDocuments');
             Route::get('getPaymentCases/{id}','PaymentController@getPaymentCases');
             Route::get('setPaymentsPerson/{id}','PaymentController@setPaymentsPerson');
             Route::get('statistic/{id}','PaymentController@statistic');

             Route::post('deleteGroup','PaymentController@deleteGroup');
             Route::post('pdf','PaymentController@pdf');
             Route::post('districtRange','PaymentController@districtRange');
             Route::post('map','PaymentController@map');
             Route::post('export','PaymentController@export');
             Route::post('archivedCheques','PaymentController@archivedCheques');
             Route::post('saveCheques','PaymentController@saveCheques');
             Route::post ('result','PaymentController@result');
             Route::post ('saveAccount','PaymentController@saveAccount');
             Route::post('storePaymentDocument','PaymentController@storePaymentDocument');
             Route::post('exportCaseList','PaymentController@exportCaseList');
             Route::post('all','PaymentController@all');
             Route::post('duplicate','PaymentController@duplicate');
             Route::post('persons','PaymentController@persons');
             Route::post('getStatistic','PaymentController@getStatistic');

         });
         Route::resource('paymentsService', 'PaymentController');
         //--------------------------------------------------------------------------------------------------------------------------
         Route::group(['prefix' => 'reports'], function() {
             Route::get('getSponsorCasesList/{id}','AllCasesController@getSponsorCasesList');
             Route::post('generateWithOptions','reportsController@generateWithOptions');
             Route::post('attachments/import', 'reportsController@import');
             Route::post('attachments/prepare', 'reportsController@prepare');
             Route::get('getCaseInfo/{id}','reportsController@getCaseInfo');
             Route::get('generate/{id}','reportsController@generate');
             Route::get('download/{id}','reportsController@download');
             Route::post('setAttachment','reportsController@setAttachment');
             Route::post('caseAttachments','reportsController@caseAttachments');
             Route::post('attachments','reportsController@attachments');
             Route::post('filter','reportsController@filter');
         });
         Route::resource('reports', 'reportsController');
//--------------------------------------------------------------------------------------------------------------------------
          Route::group(['prefix' => 'sponsor_cases'], function() {
              Route::Post ('getSponsorCases/','AllCasesController@getSponsorCases');
              Route::get ('getSponsorCaseInfo/{id}/','AllCasesController@getSponsorCaseInfo');
              Route::Post ('exportSponsorCase','AllCasesController@exportSponsorCase');
         });

//--------------------------------------------------------------------------------------------------------------------------
         Route::group(['prefix' => 'cases-info'], function() {
             Route::post('sponsorshipCasesFilter','AllCasesController@sponsorshipCasesFilter');
             Route::post('getFamilies','AllCasesController@getFamilies');
             Route::post('getCasesStatistics/','AllCasesController@getCasesStatistics');
             Route::get('getCasesStatusLog/{id}','AllCasesController@getCasesStatusLog');
             Route::post('UpdateCasesStatus/','AllCasesController@UpdateCasesStatus');
             Route::put('UpdateSponsorshipDate/{id}','AllCasesController@UpdateSponsorshipDate');
             Route::put('updateSponsorNumber/{id}','AllCasesController@updateSponsorNumber');
             Route::post('UpdateSponsorshipStatus/','AllCasesController@UpdateSponsorshipStatus');
         });
         Route::resource('cases-info', 'AllCasesController');
//--------------------------------------------------------------------------------------------------------------------------
         Route::group(['prefix' => 'nominate'], function() {
             Route::put('updateStatus/{id}','nominationController@updateStatus');
             Route::get ('persons/{id}','nominationController@persons');
             Route::get ('export/{id}','nominationController@exportPerson');
             Route::post('filterSponsorships','nominationController@filterSponsorships');
             Route::get('list/{id}','nominationController@getList');
         });
         Route::resource('nominate', 'nominationController');
//--------------------------------------------------------------------------------------------------------------------------
         Route::group(['prefix' => 'related'], function() {
             Route::post('getFamily/','relatedController@getFamily');
             Route::get('updateStatusTemplate','relatedController@updateStatusTemplate');
             Route::get('sys-cheque-template/','relatedController@SysDefaultTemplate');
             Route::get('templateInstruction/','relatedController@templateInstruction');
             Route::get('cheque-template/{id}','relatedController@defaultTemplate');
             Route::post('cheque-template-upload','relatedController@defaultTemplateUpload');
         });

});

