<?php

$loader = new Composer\Autoload\ClassLoader();
$loader->addPsr4('Sponsorship\\', __DIR__ . '/../src');
$loader->register();