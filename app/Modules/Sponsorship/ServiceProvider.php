<?php

namespace App\Modules\Sponsorship;
use Sponsorship\Model\Reports;
use Sponsorship\Model\ReportsCasesDocuments;
use Sponsorship\Model\SponsorshipCases;
use Sponsorship\Policy\ReportsCasesDocumentsPolicy;
use Sponsorship\Policy\ReportsPolicy;
use Sponsorship\Policy\SponsorshipCasesPolicy;
use Sponsorship\Model\SponsorshipsPerson;
use Sponsorship\Policy\SponsorshipsPersonPolicy;
use Sponsorship\Model\Sponsorships;
use Sponsorship\Policy\SponsorshipsPolicy;
use Sponsorship\Model\Payments;
use Sponsorship\Policy\PaymentsPolicy;
use Sponsorship\Model\PaymentsCases;
use Sponsorship\Policy\PaymentsCasesPolicy;
    
class ServiceProvider extends \App\Providers\AuthServiceProvider
{
    protected $policies = [
        ReportsCasesDocuments::class => ReportsCasesDocumentsPolicy::class,
        Reports::class => ReportsPolicy::class,
        SponsorshipCases::class => SponsorshipCasesPolicy::class,
        SponsorshipsPerson::class => SponsorshipsPersonPolicy::class,
        Sponsorships::class => SponsorshipsPolicy::class,
        Payments::class => PaymentsPolicy::class,
        PaymentsCases::class => PaymentsCasesPolicy::class,
    ];

    public function boot()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/config/routes.php';
        }
        $this->loadViewsFrom(__DIR__ . '/views', 'sponsorship');
        $this->loadTranslationsFrom(__DIR__.'/languages', 'sponsorship');
        
        require __DIR__ . '/config/autoload.php';
        $this->registerPolicies();
    }

    public function register()
    {
        /*$this->mergeConfigFrom(
            __DIR__ . '/config/config.php', 'user'
        );*/
    }

}
