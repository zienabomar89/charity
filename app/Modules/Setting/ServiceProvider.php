<?php

namespace App\Modules\Setting;

class ServiceProvider extends \App\Providers\AuthServiceProvider
{

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [

        \Setting\Model\OrganizationsCategory::class => \Setting\Policy\OrganizationsCategoryPolicy::class,
        \Setting\Model\ProjectType::class => \Setting\Policy\ProjectTypePolicy::class,
        \Setting\Model\ProjectCategory::class => \Setting\Policy\ProjectCategoryPolicy::class,
        \Setting\Model\ProjectRegion::class => \Setting\Policy\ProjectRegionPolicy::class,
        \Setting\Model\ProjectBeneficiaryCategory::class => \Setting\Policy\ProjectBeneficiaryCategoryPolicy::class,
        \Setting\Model\ProjectActivities::class => \Setting\Policy\ProjectActivitiesPolicy::class,

        \Setting\Model\TransferCompany::class => \Setting\Policy\TransferCompanyPolicy::class,
        \Setting\Model\BackupVersions::class => \Setting\Policy\BackupVersionsPolicy::class,
        \Setting\Model\PaymentCategory::class => \Setting\Policy\PaymentCategoryPolicy::class,
        \Setting\Model\AidSource::class => \Setting\Policy\AidSourcePolicy::class,
        \Setting\Model\Bank::class => \Setting\Policy\BankPolicy::class,
        \Setting\Model\Branch::class => \Setting\Policy\BankBranchPolicy::class,
        \Setting\Model\Categories::class => \Setting\Policy\CategoriesPolicy::class,
        \Setting\Model\Currency::class => \Setting\Policy\CurrencyPolicy::class,
        \Setting\Model\DeathCause::class => \Setting\Policy\DeathCausePolicy::class,
        \Setting\Model\Disease::class => \Setting\Policy\DiseasePolicy::class,
        \Setting\Model\DocumentType::class => \Setting\Policy\DocumentTypePolicy::class,
        \Setting\Model\Education\Authority::class => \Setting\Policy\EduAuthorityPolicy::class,
        \Setting\Model\Education\Stage::class => \Setting\Policy\EduStagePolicy::class,
        \Setting\Model\Education\Degree::class => \Setting\Policy\EduDegreePolicy::class,
        \Setting\Model\Essential::class => \Setting\Policy\EssentialPolicy::class,
        \Setting\Model\Kinship::class => \Setting\Policy\KinshipPolicy::class,
        \Setting\Model\Language::class => \Setting\Policy\LanguagePolicy::class,
        \Setting\Model\aidsLocation::class => \Setting\Policy\aidsLocationPolicy::class,
        \Setting\Model\Location::class => \Setting\Policy\LocationPolicy::class,
        \Setting\Model\MaritalStatus::class => \Setting\Policy\MaritalStatusPolicy::class,
        \Setting\Model\PropertyType::class => \Setting\Policy\PropertyTypePolicy::class,
        \Setting\Model\Property::class => \Setting\Policy\PropertyPolicy::class,
        \Setting\Model\RoofMaterial::class => \Setting\Policy\RoofMaterialPolicy::class,
        \Setting\Model\HouseStatus::class => \Setting\Policy\HouseStatusPolicy::class,
        \Setting\Model\HabitableStatus::class => \Setting\Policy\HabitableStatusPolicy::class,
        \Setting\Model\FurnitureStatus::class => \Setting\Policy\FurnitureStatusPolicy::class,
        \Setting\Model\BuildingStatus::class => \Setting\Policy\BuildingStatusPolicy::class,
        \Setting\Model\Setting::class => \Setting\Policy\SettingPolicy::class,
        \Setting\Model\Work\Wage::class => \Setting\Policy\WorkWagePolicy::class,
        \Setting\Model\Work\Status::class => \Setting\Policy\WorkStatusPolicy::class,
        \Setting\Model\Work\Reason::class => \Setting\Policy\WorkReasonPolicy::class,
        \Setting\Model\Work\Job::class => \Setting\Policy\WorkJobPolicy::class,
    ];

    public function boot()
    {
        if (!$this->app->routesAreCached()) {
            require __DIR__ . '/config/routes.php';
        }
        $this->loadViewsFrom(__DIR__ . '/views', 'constants');
        $this->loadTranslationsFrom(__DIR__ . '/languages', 'setting');

        require __DIR__ . '/config/autoload.php';
        $this->registerPolicies();
        $this->registeObservers();
    }

    public function register()
    {
        /* $this->mergeConfigFrom(
          __DIR__ . '/config/config.php', 'user'
          ); */
    }
    
    protected function registeObservers()
    {
        \Setting\Model\AidSource::observe(\Setting\Observer\AidSourceObserver::class);
        \Setting\Model\Bank::observe(\Setting\Observer\BankObserver::class);
        \Setting\Model\Currency::observe(\Setting\Observer\CurrencyObserver::class);
        \Setting\Model\DeathCause::observe(\Setting\Observer\DeathCauseObserver::class);
        \Setting\Model\Disease::observe(\Setting\Observer\DiseaseObserver::class);
        \Setting\Model\DocumentType::observe(\Setting\Observer\DocumentTypeObserver::class);
        \Setting\Model\Education\Authority::observe(\Setting\Observer\EducationAuthorityObserver::class);
        \Setting\Model\Education\Stage::observe(\Setting\Observer\EducationStageObserver::class);
        \Setting\Model\Education\Degree::observe(\Setting\Observer\EducationDegreeObserver::class);
        \Setting\Model\Essential::observe(\Setting\Observer\EssentialObserver::class);
        \Setting\Model\Kinship::observe(\Setting\Observer\KinshipObserver::class);
        \Setting\Model\Language::observe(\Setting\Observer\LanguageObserver::class);
        \Setting\Model\Location::observe(\Setting\Observer\LocationObserver::class);
        \Setting\Model\MaritalStatus::observe(\Setting\Observer\MaritalStatusObserver::class);
        \Setting\Model\PropertyType::observe(\Setting\Observer\PropertyTypeObserver::class);
        \Setting\Model\Property::observe(\Setting\Observer\PropertyObserver::class);
        \Setting\Model\RoofMaterial::observe(\Setting\Observer\RoofMaterialObserver::class);
        \Setting\Model\Work\Wage::observe(\Setting\Observer\WorkWageObserver::class);
        \Setting\Model\Work\Status::observe(\Setting\Observer\WorkStatusObserver::class);
        \Setting\Model\Work\Reason::observe(\Setting\Observer\WorkReasonObserver::class);
        \Setting\Model\Work\Job::observe(\Setting\Observer\WorkJobPolicy::class);
    }

}
