<?php

namespace Setting\Observer;

class EducationAuthorityObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل جهة مشرفة تعليمية جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل الجهة المشرفة التعليمية "%s"',
        'DELETED' => 'قام بحذف سجل الجهة المشرفة التعليمية "%s"',
    ];
}