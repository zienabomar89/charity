<?php

namespace Setting\Observer;

class EducationDegreeObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل درجة علمية جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل الدرجة العلمية "%s"',
        'DELETED' => 'قام بحذف سجل الدرجة العلمية "%s"',
    ];
}