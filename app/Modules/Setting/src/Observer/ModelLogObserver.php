<?php

namespace Setting\Observer;

use Log\Model\Log;

abstract class ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات السجل "%s"',
        'DELETED' => 'قام بحذف السجل "%s"',
    ];
    
    /**
     * Listen to the Model created event.
     *
     * @param  $object
     * @return void
     */
    public function created($object)
    {
        $this->log('CREATED', $object);
    }
    
    /**
     * Listen to the Model deleted event.
     *
     * @param  $object
     * @return void
     */
    public function updated($object)
    {
        $this->log('UPDATED', $object);
    }
    
    /**
     * Listen to the Model deleted event.
     *
     * @param  $object
     * @return void
     */
    public function deleted($object)
    {   
        $this->log('DELETED', $object);
    }
    
    protected function log($action, $object)
    {
        $log = $this->newLogModel();
        $log->action = $this->formatAction($object, $action);
        $log->message = sprintf($this->messages[$action], $this->getName($object));
        $log->save();
    }
    
    protected function newLogModel()
    {
        $user = \Illuminate\Support\Facades\Auth::user();
        $id = $user? $user->id : 0;
        $log = new Log();
        $log->user_id = $id;
        $log->ip = request()->ip();
//        $log->created_at = date('Y-m-d H:i:s');
        $log->created_at = date('Y-m-d H:i:s');

        return $log;
    }
    
    protected function formatAction($object, $action)
    {
        $reflection = new \ReflectionClass($object);
        $name = $reflection->getShortName();
        return strtoupper($this->camelCaseToUnderscore($name) . '_' . $action);
    }
    
    protected function camelCaseToUnderscore($string)
    {
        $separator   = '_';
        $pattern     = ['#(?<=(?:[A-Z]))([A-Z]+)([A-Z][a-z])#', '#(?<=(?:[a-z0-9]))([A-Z])#'];
        $replacement = ['\1' . $separator . '\2', $separator . '\1'];
        return preg_replace($pattern, $replacement, $string);
    }
    
    protected function getName($object)
    {
        if (isset($object->name)) {
            return $object->name;
        }
        
        if ($object instanceof \Application\Model\I18nableInterface) {
            $attributes = $object->getI18nAttributes();
            if (isset($attributes['name'])) {
                return $attributes['name'];
            }
            
            $i18n = $object->findI18nModel(1);
            if ($i18n && isset($i18n->name)) {
                return $i18n->name;
            }
        }
        
        return '';
    }
}