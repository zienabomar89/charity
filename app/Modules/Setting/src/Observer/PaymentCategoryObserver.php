<?php

namespace Setting\Observer;

class PaymentCategoryObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل تصنيف صرفية جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل تصنيف صرفية "%s"',
        'DELETED' => 'قام بحذف سجل تصنيف صرفية "%s"',
    ];
}