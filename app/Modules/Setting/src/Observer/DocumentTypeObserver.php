<?php

namespace Setting\Observer;

class DocumentTypeObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل نوع مستند/مرفق جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل نوع المستند/المرفق "%s"',
        'DELETED' => 'قام بحذف سجل نوع المستند/المرفق "%s"',
    ];
}