<?php

namespace Setting\Observer;

class FurnitureStatusObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل حالة الأثاث جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل حالة الأثاث "%s"',
        'DELETED' => 'قام بحذف سجل حالة الأثاث "%s"',
    ];
}