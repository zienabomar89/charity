<?php

namespace Setting\Observer;

class WorkWageObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل فئة قيمة الدخل/الأجرة جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل فئة قيمة الدخل/الأجرة "%s"',
        'DELETED' => 'قام بحذف سجل فئة قيمة الدخل/الأجرة "%s"',
    ];
}