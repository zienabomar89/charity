<?php

namespace Setting\Observer;

class LocationObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل موقع/عنوان جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل موقع/عنوان "%s"',
        'DELETED' => 'قام بحذف سجل موقع/عنوان "%s"',
    ];
}