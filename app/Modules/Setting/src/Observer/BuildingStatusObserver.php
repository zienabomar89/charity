<?php

namespace Setting\Observer;

class BuildingStatusObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل حالة بناء المنزل جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل حالة بناء المنزل "%s"',
        'DELETED' => 'قام بحذف سجل حالة بناء المنزل "%s"',
    ];
}