<?php

namespace Setting\Observer;

class PropertyTypeObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل نوع الممتلكات جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل نوع الممتلكات "%s"',
        'DELETED' => 'قام بحذف سجل نوع الممتلكات "%s"',
    ];
}