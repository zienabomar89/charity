<?php

namespace Setting\Observer;

class EssentialObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل ضروريات وأثاث المنزل جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل ضروريات وأثاث المنزل "%s"',
        'DELETED' => 'قام بحذف سجل ضروريات وأثاث المنزل "%s"',
    ];
}