<?php

namespace Setting\Observer;

class DiseaseObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل مرض جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل المرض "%s"',
        'DELETED' => 'قام بحذف سجل المرض "%s"',
    ];
}