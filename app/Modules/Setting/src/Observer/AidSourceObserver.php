<?php

namespace Setting\Observer;

class AidSourceObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل بنك جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل البنك "%s"',
        'DELETED' => 'قام بحذف سجل البنك "%s"',
    ];
}