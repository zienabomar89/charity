<?php

namespace Setting\Observer;

class WorkReasonObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل سبب عدم المقدرة على العمل جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل سبب عدم المقدرة على العمل "%s"',
        'DELETED' => 'قام بحذف سجل سبب عدم المقدرة على العمل "%s"',
    ];
}