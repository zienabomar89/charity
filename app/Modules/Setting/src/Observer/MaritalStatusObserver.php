<?php

namespace Setting\Observer;

class MaritalStatusObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل حالة إجتماعية جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل حالة إجتماعية "%s"',
        'DELETED' => 'قام بحذف سجل حالة إجتماعية "%s"',
    ];
}