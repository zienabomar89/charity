<?php

namespace Setting\Observer;

class DeathCauseObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل سبب وفاة جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل سبب الوفاة "%s"',
        'DELETED' => 'قام بحذف سجل سبب الوفاة "%s"',
    ];
}