<?php

namespace Setting\Observer;

class KinshipObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل صلة قرابة جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل صلة قرابة "%s"',
        'DELETED' => 'قام بحذف سجل صلة قرابة "%s"',
    ];
}