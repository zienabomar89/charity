<?php

namespace Setting\Observer;

class EducationStageObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل مرحلة تعليمية جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل المرحلة التعليمية "%s"',
        'DELETED' => 'قام بحذف سجل المرحلة التعليمية "%s"',
    ];
}