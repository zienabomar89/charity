<?php

namespace Setting\Observer;

class RoofMaterialObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل نوع أسقف المنازل جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل نوع أسقف المنازل "%s"',
        'DELETED' => 'قام بحذف سجل نوع أسقف المنازل "%s"',
    ];
}