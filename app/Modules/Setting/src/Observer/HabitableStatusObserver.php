<?php

namespace Setting\Observer;

class HabitableStatusObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل صلاحية المنزل للسكن جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل صلاحية المنزل للسكن "%s"',
        'DELETED' => 'قام بحذف سجل صلاحية المنزل للسكن "%s"',
    ];
}