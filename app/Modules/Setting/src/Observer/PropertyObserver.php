<?php

namespace Setting\Observer;

class PropertyObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل ممتلكات جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل ممتلكات "%s"',
        'DELETED' => 'قام بحذف سجل ممتلكات "%s"',
    ];
}