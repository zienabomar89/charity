<?php

namespace Setting\Observer;

class CurrencyObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل عملة جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل العملة "%s"',
        'DELETED' => 'قام بحذف سجل العملة "%s"',
    ];
}