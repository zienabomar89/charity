<?php

namespace Setting\Observer;

class WorkStatusObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل حالة ونظام العمل جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل حالة ونظام العمل "%s"',
        'DELETED' => 'قام بحذف سجل حالة ونظام العمل "%s"',
    ];
}