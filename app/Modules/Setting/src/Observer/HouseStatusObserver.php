<?php

namespace Setting\Observer;

class HouseStatusObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل وضع المنزل جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل وضع المنزل "%s"',
        'DELETED' => 'قام بحذف سجل وضع المنزل "%s"',
    ];
}