<?php

namespace Setting\Observer;

class WorkJobObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل مسمى وظيفي جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل مسمى وظيفي "%s"',
        'DELETED' => 'قام بحذف سجل مسمى وظيفي "%s"',
    ];
}