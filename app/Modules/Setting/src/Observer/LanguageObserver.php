<?php

namespace Setting\Observer;

class LanguageObserver extends ModelLogObserver
{
    protected $messages = [
        'CREATED' => 'قام بإضافة سجل لغة جديد "%s"',
        'UPDATED' => 'قام بتحرير معلومات سجل لغة "%s"',
        'DELETED' => 'قام بحذف سجل لغة "%s"',
    ];
}