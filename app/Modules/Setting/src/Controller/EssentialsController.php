<?php

namespace Setting\Controller;

use Setting\Model\Essential;

class EssentialsController extends AbstractController
{
    // initialize Essential model object to use in process
    protected function newModel()
    {
        return new Essential();
    }

    // get Essential model using id
    protected function findOrFail($id)
    {
        $entry = Essential::findOrFail($id);
        return $entry;
    }

    // get Essential model searching by name
    public function index()
    {
        $this->authorize('manage', Essential::class);
        $entries = Essential::query();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all Essential model
    public function getList()
    {
        $entries = Essential::all();
        return response()->json($entries);
    }

    // create new Essential model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', Essential::class);
        $this->validate($request, Essential::getValidatorRules());
        return parent::store($request);
    }

}
