<?php

namespace Setting\Controller;

use App\Http\Controllers\Controller;
use Setting\Model\Property;
use Setting\Model\PropertyWeight;

class PropertyWeightsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    public function index($id)
    {
        $this->authorize('manage', Property::class);
        $entries = PropertyWeight::where('property_id', $id)->get();
        return response()->json($entries);
    }

    // create new PropertyWeight model
    public function store(\Illuminate\Http\Request $request, $id)
    {
        $this->authorize('create', Property::class);
        $this->validate($request, PropertyWeight::getValidatorRules());
        
        $min = $request->input('value_min');
        $max = $request->input('value_max');
        if (!$min && !$max) {
            $validator = $this->getValidationFactory()->make($request->all(), PropertyWeight::getValidatorRules());
            $this->throwValidationException($request, $validator);
        }
        
        $entity = PropertyWeight::forceCreate(array(
            'property_id' => $id,
            'value_min' => $request->input('value_min'),
            'value_max' => $request->input('value_max'),
            'weight' => $request->input('weight'),
        ));
        
        return response()->json($entity);
    }

    // update exist PropertyWeight model by id
    public function update(\Illuminate\Http\Request $request, $id, $id2)
    {
        
        $this->authorize('update', Property::class);
        $entity = PropertyWeight::findOrFail($id2);
        $original = clone $entity;
        
        $entity->value_min = $request->input('value_min');
        $entity->value_max = $request->input('value_max');
        $entity->weight = $request->input('weight');
        $entity->save();
        
        $entity->updateLinkedRank($original);
        
        return response()->json($entity);
    }

    // delete exist PropertyWeight model by id
    public function destroy(\Illuminate\Http\Request $request, $id, $id2)
    {
        $entity = PropertyWeight::findOrFail($id2);
        $this->authorize('delete', Property::class);
        $entity->delete();
        return response()->json(array('deleted' => 1));
    }
}
