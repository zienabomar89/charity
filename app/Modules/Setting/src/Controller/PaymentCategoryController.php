<?php

namespace Setting\Controller;

use Setting\Model\PaymentCategory;

class PaymentCategoryController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        PaymentCategory::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize PaymentCategory model object to use in process
    protected function newModel()
    {
        return new PaymentCategory();
    }


    // get PaymentCategory model using id
    protected function findOrFail($id)
    {
        $entry = PaymentCategory::findOrFail($id);
        return $entry;
    }

    // get PaymentCategory model searching by name
    public function index()
    {
        $this->authorize('manage', PaymentCategory::class);
        $entries = PaymentCategory::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }


    // get all PaymentCategory model
    public function getList()
    {
        $entries = PaymentCategory::i18n()->get();
        return response()->json($entries);
    }

    // create new PaymentCategory model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', PaymentCategory::class);
        $this->validate($request, PaymentCategory::getValidatorRules());
        return parent::store($request);
    }
}
