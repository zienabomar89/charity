<?php

namespace Setting\Controller;

use Setting\Model\Kinship;
use Setting\Model\KinshipI18n;

class KinshipController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        Kinship::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize Kinship model object to use in process
    protected function newModel()
    {
        return new Kinship();
    }

    // get Kinship model using id
    protected function findOrFail($id)
    {
        $entry = Kinship::findOrFail($id);
        return $entry;
    }

    // get Kinship model searching by name
    public function index()
    {
        $this->authorize('manage', Kinship::class);
        $entries = Kinship::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all Kinship model
    public function getList()
    {
        $entries = Kinship::i18n()->get();
        return response()->json($entries);
    }

    // create new Kinship model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', Kinship::class);
        $this->validate($request, Kinship::getValidatorRules());
        return parent::store($request);
    }

    // export Kinship name and key on excel file
    public function export()
    {
        $entries = KinshipI18n::export();

        if(sizeof($entries) !=0){
            $data=array();
            foreach($entries as $key =>$value){
                foreach($value as $k =>$v){
                    if($k != 'language_id')
                        $data[$key][trans('setting::application.' . $k)]= $v;
                }
            }
            $token = md5(uniqid());
            \Excel::create('export_' . $token, function($excel) use($data) {
                $excel->sheet(trans('application::application.kinship map'), function($sheet) use($data) {
                    $sheet->setRightToLeft(true);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);
                    $sheet->getStyle("A1:B1")->applyFromArray(['font' => ['bold' => true]]);
                    $sheet->fromArray($data);
                });

            })->store('xlsx', storage_path('tmp/'));
            return response()->json(['download_token' => $token]);
        }
        return response()->json(['status' => false]);
    }



}
