<?php

namespace Setting\Controller;

use Setting\Model\Property;

class PropertiesController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        Property::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize Properties model object to use in process
    protected function newModel()
    {
        return new Property();
    }

    // get Properties model using id
    protected function findOrFail($id)
    {
        $entry = Property::findOrFail($id);
        return $entry;
    }

    // get Properties model searching by name
    public function index()
    {
        $this->authorize('manage', Property::class);
        $entries = Property::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all Properties model
    public function getList()
    {
        $entries = Property::i18n()->get();
        return response()->json($entries);
    }

    // create new Properties model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', Property::class);
        $this->validate($request, Property::getValidatorRules());
        return parent::store($request);
    }
    
    
}
