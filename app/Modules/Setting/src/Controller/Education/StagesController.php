<?php

namespace Setting\Controller\Education;

use Setting\Controller\AbstractController;
use Setting\Model\Education\Stage;

class StagesController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        Stage::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize EducationStage model object to use in process
    protected function newModel()
    {
        return new Stage();
    }

    // get EducationStage model using id
    protected function findOrFail($id)
    {
        $entry = Stage::findOrFail($id);
        return $entry;
    }

    // get EducationStage model searching by name
    public function index()
    {
        $this->authorize('manage', Stage::class);
        $entries = Stage::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all EducationStage model  
    public function getList()
    {
        $entries = Stage::i18n()->get();
        return response()->json($entries);
    }

    // create new EducationStage model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', Stage::class);
        $this->validate($request, Stage::getValidatorRules());
        return parent::store($request);
    }
}
