<?php

namespace Setting\Controller\Education;

use Setting\Controller\AbstractController;
use Setting\Model\Education\Degree;

class DegreesController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        Degree::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize EducationDegree model object to use in process
    protected function newModel()
    {
        return new Degree();
    }

    // get EducationDegree model using id
    protected function findOrFail($id)
    {
        $entry = Degree::findOrFail($id);
        return $entry;
    }

    // get EducationDegree model searching by name
    public function index()
    {
        $this->authorize('manage', Degree::class);
        $entries = Degree::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all EducationDegree model  
    public function getList()
    {
        $entries = Degree::i18n()->get();
        return response()->json($entries);
    }

    // create new EducationDegree model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', Degree::class);
        $this->validate($request, Degree::getValidatorRules());
        return parent::store($request);
    }
}
