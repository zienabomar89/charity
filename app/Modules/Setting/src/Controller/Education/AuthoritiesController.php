<?php

namespace Setting\Controller\Education;

use Setting\Controller\AbstractController;
use Setting\Model\Education\Authority;

class AuthoritiesController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        Authority::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize EducationAuthority model object to use in process
    protected function newModel()
    {
        return new Authority();
    }

    // get EducationAuthority model using id
    protected function findOrFail($id)
    {
        $entry = Authority::findOrFail($id);
        return $entry;
    }

    // get EducationAuthority model searching by name
    public function index()
    {
        $this->authorize('manage', Authority::class);
        $entries = Authority::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all EducationAuthority model
    public function getList()
    {
        $entries = Authority::i18n()->get();
        return response()->json($entries);
    }

    // create new EducationAuthority model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', Authority::class);
        $this->validate($request, Authority::getValidatorRules());
        return parent::store($request);
    }
}
