<?php

namespace Setting\Controller;

use Setting\Model\RoofMaterial;

class RoofMaterialsController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        RoofMaterial::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize RoofMaterial model object to use in process
    protected function newModel()
    {
        return new RoofMaterial();
    }

    // get RoofMaterial model using id
    protected function findOrFail($id)
    {
        $entry = RoofMaterial::findOrFail($id);
        return $entry;
    }

    // get RoofMaterial model searching by name
    public function index()
    {
        $this->authorize('manage', RoofMaterial::class);
        $entries = RoofMaterial::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all RoofMaterial model
    public function getList()
    {
        $entries = RoofMaterial::i18n()->get();
        return response()->json($entries);
    }

    // create new RoofMaterial model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', RoofMaterial::class);
        $this->validate($request, RoofMaterial::getValidatorRules());
        return parent::store($request);
    }
    
}
