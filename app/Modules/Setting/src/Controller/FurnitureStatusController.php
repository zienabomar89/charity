<?php

namespace Setting\Controller;

use Setting\Model\FurnitureStatus;

class FurnitureStatusController extends AbstractController
{

    public function __construct()
    {
        $language_id = 1;
        FurnitureStatus::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize FurnitureStatus model object to use in process
    protected function newModel()
    {
        return new FurnitureStatus();
    }

    // get FurnitureStatus model using id
    protected function findOrFail($id)
    {
        $entry = FurnitureStatus::findOrFail($id);
        return $entry;
    }

    // get FurnitureStatus model searching by name
    public function index()
    {
        $this->authorize('manage', FurnitureStatus::class);
        $entries = FurnitureStatus::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all FurnitureStatus model
    public function getList()
    {
        $entries = FurnitureStatus::i18n()->get();
        return response()->json($entries);
    }

    // create new FurnitureStatus model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', FurnitureStatus::class);
        $this->validate($request, FurnitureStatus::getValidatorRules());
        return parent::store($request);
    }

}
