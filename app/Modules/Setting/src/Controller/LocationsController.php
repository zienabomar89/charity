<?php

namespace Setting\Controller;

use Setting\Model\Location;

class LocationsController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        Location::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize Location model object to use in process
    protected function newModel()
    {
        \App\Http\Helpers::getLocale();
        $location = new Location();
        $routeName = request()->route()->getName();
        if ($routeName == 'countries.store') {
            $location->location_type = Location::TYPE_COUNTRY;
        }
        else if ($routeName == 'districts.store') {
            $location->location_type = Location::TYPE_DISTRICT;
        }
        else if ($routeName == 'cities.store') {
            $location->location_type = Location::TYPE_CITY;
        }
        else if ($routeName == 'neighborhoods.store') {
            $location->location_type = Location::TYPE_NEIGHBORHOOD;
        }
        else if ($routeName == 'mosques.store') {
            $location->location_type = Location::TYPE_MOSQUES;
        }
        return $location;
    }

    // get Location row using id
    protected function findOrFail($id)
    {
        \App\Http\Helpers::getLocale();
        $entry = Location::i18n()->findOrFail($id);
        return $entry;
    }

    // get Location rows searching by name (according location type)
    public function index($id = null)
    {
        $this->authorize('manage', Location::class);

        \App\Http\Helpers::getLocale();
        $routeName = request()->route()->getName();
        if ($routeName == 'countries.index') {
            return $this->countries();
        }
        if ($routeName == 'districts.index') {
            return $this->districts($id);
        }
        if ($routeName == 'cities.index') {
            return $this->cities($id);
        }
        if ($routeName == 'neighborhoods.index') {
            return $this->neighborhoods($id);
        }

        if ($routeName == 'mosques.index') {
            return $this->mosques($id);
        }


        $entries = Location::i18n()->descendants($id)->paginate();
        return response()->json($entries);
    }

    // get all Location rows (according location type)
    public function getList($id = null)
    {
        \App\Http\Helpers::getLocale();
        $routeName = request()->route()->getName();
        if ($routeName == 'countries.list') {
            return $this->countriesList();
        }
        if ($routeName == 'districts.list') {
            return $this->districtsList($id);
        }
        if ($routeName == 'cities.list') {
            return $this->citiesList($id);
        }
        if ($routeName == 'neighborhoods.list') {
            return $this->neighborhoodsList($id);
        }

        if ($routeName == 'mosques.list') {
            return $this->mosquesList($id);
        }
        $entries = Location::i18n()->descendants($id)->get();
        return response()->json($entries);
    }

    // get all Location rows (according location type)
    public function typeList($type)
    {
        \App\Http\Helpers::getLocale();
        $entries = Location::i18n()->where('location_type', '=', $type)->get();
        return response()->json($entries);
    }

    // create new Location model (according location type)
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', Location::class);
        \App\Http\Helpers::getLocale();
        $rules = Location::getValidatorRules();
        if ('countries.store' == $request->route()->getName()) {
            unset($rules['parent_id']);
        }
        $this->validate($request, $rules);

        return parent::store($request);
    }

    // get countries from Location rows searching by name
    public function countries()
    {
        \App\Http\Helpers::getLocale();
        $entries = Location::i18n()->countries();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get districts from Location models searching by name , id (parent)
    public function districts($id = null)
    {
        \App\Http\Helpers::getLocale();
        $entries = Location::i18n()->districts($id);
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get cities from Location models searching by name , id (parent)
    public function cities($id = null)
    {
        \App\Http\Helpers::getLocale();
        $entries = Location::i18n()->cities($id);
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get mosques from Location models searching by name , id (parent)
    public function neighborhoods($id = null)
    {
        \App\Http\Helpers::getLocale();
        $entries = Location::i18n()->neighborhoods($id);
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get mosques from Location models searching by name , id (parent)
    public function mosques($id = null)
    {
        \App\Http\Helpers::getLocale();
        $entries = Location::i18n()->mosques($id);
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get countries list from Location models searching id (parent)
    public function countrieslist()
    {
        \App\Http\Helpers::getLocale();
        $entries = Location::i18n()->countries()->get();
        return response()->json($entries);
    }

    // get districts list from Location models searching id (parent)
    public function districtsList($id = null)
    {
        \App\Http\Helpers::getLocale();
        $entries = Location::i18n()->districts($id)->get();
        return response()->json($entries);
    }

    // get cities list from Location models searching id (parent)
    public function citiesList($id = null)
    {
        \App\Http\Helpers::getLocale();
        $entries = Location::i18n()->cities($id)->get();
        return response()->json($entries);
    }

    // get neighborhoods list from Location models searching id (parent)
    public function neighborhoodsList($id = null)
    {
        \App\Http\Helpers::getLocale();
        $entries = Location::i18n()->neighborhoods($id)->get();
        return response()->json($entries);
    }

    // get mosques list from Location models searching id (parent)
    public function mosquesList($id = null)
    {
        \App\Http\Helpers::getLocale();
        $entries = Location::i18n()->mosques($id)->get();
        return response()->json($entries);
    }

    // export Location name and key on excel file
    public function export()
    {
        \App\Http\Helpers::getLocale();
        $entries = Location::export();
        if(sizeof($entries) !=0){
            $data=array();
            foreach($entries as $key =>$value){
                foreach($value as $k =>$v){
                    $data[$key][trans('setting::application.' . $k)]= $v;
                }
            }
            $token = md5(uniqid());
            \Excel::create('export_' . $token, function($excel) use($data) {
                $excel->sheet('location_map', function($sheet) use($data) {
                    $sheet->setRightToLeft(true);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);
                    $sheet->getStyle("A1:H1")->applyFromArray(['font' => ['bold' => true]]);
                    $sheet->fromArray($data);
                });

            })->store('xlsx', storage_path('tmp/'));
            return response()->json(['download_token' => $token]);
        }
        return response()->json(['status' => false]);
    }


}
