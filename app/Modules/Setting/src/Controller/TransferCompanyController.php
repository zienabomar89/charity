<?php

namespace Setting\Controller;

use Setting\Model\TransferCompany;

class TransferCompanyController extends AbstractController
{

    // initialize TransferCompany model object to use in process
    protected function newModel()
    {
        return new TransferCompany();
    }

    // get TransferCompany model using id
    protected function findOrFail($id)
    {
        $entry = TransferCompany::findOrFail($id);
        return $entry;
    }

    // get TransferCompany model searching by name
    public function index()
    {
        $this->authorize('manage', TransferCompany::class);
        $entries = TransferCompany::query();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all TransferCompany model
    public function getList()
    {
        $entries = TransferCompany::all();
        return response()->json($entries);
    }

    // create new TransferCompany model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', TransferCompany::class);
        $this->validate($request, TransferCompany::getValidatorRules());
        return parent::store($request);
    }


}
