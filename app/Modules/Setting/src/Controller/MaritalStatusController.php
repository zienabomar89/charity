<?php

namespace Setting\Controller;

use Setting\Model\MaritalStatus;

class MaritalStatusController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        MaritalStatus::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize MaritalStatus model object to use in process
    protected function newModel()
    {
        return new MaritalStatus();
    }


    // get MaritalStatus model using id
    protected function findOrFail($id)
    {
        $entry = MaritalStatus::findOrFail($id);
        return $entry;
    }

    // get MaritalStatus model searching by name
    public function index()
    {
        $this->authorize('manage', MaritalStatus::class);
        $entries = MaritalStatus::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all MaritalStatus model
    public function getList()
    {
        $entries = MaritalStatus::i18n()->get();
        return response()->json($entries);
    }

    // create new MaritalStatus model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', MaritalStatus::class);
        $this->validate($request, MaritalStatus::getValidatorRules());
        return parent::store($request);
    }
    
    
}
