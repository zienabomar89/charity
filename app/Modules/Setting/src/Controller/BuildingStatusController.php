<?php

namespace Setting\Controller;

use Setting\Model\BuildingStatus;

class BuildingStatusController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        BuildingStatus::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize BuildingStatus model object to use in process
    protected function newModel()
    {
        return new BuildingStatus();
    }


    // get BuildingStatus model using id
    protected function findOrFail($id)
    {
        $entry = BuildingStatus::findOrFail($id);
        return $entry;
    }

    // get BuildingStatus model searching by name
    public function index()
    {
        $this->authorize('manage', BuildingStatus::class);
        $entries = BuildingStatus::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }


    // get all BuildingStatus model
    public function getList()
    {
        $entries = BuildingStatus::i18n()->get();
        return response()->json($entries);
    }

    // create new BuildingStatus model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', BuildingStatus::class);
        $this->validate($request, BuildingStatus::getValidatorRules());
        return parent::store($request);
    }
    
    
}
