<?php

namespace Setting\Controller;

use App\Http\Controllers\Controller;
use Setting\Model\AidSource;
use Setting\Model\AidSourceWeight;

class AidSourceWeightsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get AidSourceWeight model
    public function index($id)
    {
        $this->authorize('manage', AidSource::class);
        $entries = AidSourceWeight::where('aid_source_id', $id)->get();
        return response()->json($entries);
    }

    // create new AidSourceWeight model
    public function store(\Illuminate\Http\Request $request, $id)
    {
        $this->authorize('create', AidSource::class);
        $this->validate($request, AidSourceWeight::getValidatorRules());
        
        $min = $request->input('value_min');
        $max = $request->input('value_max');
        if (!$min && !$max) {
            $validator = $this->getValidationFactory()->make($request->all(), AidSourceWeight::getValidatorRules());
            $this->throwValidationException($request, $validator);
        }
        
        $entity = AidSourceWeight::forceCreate(array(
            'aid_source_id' => $id,
            'value_min' => $request->input('value_min'),
            'value_max' => $request->input('value_max'),
            'weight' => $request->input('weight'),
        ));
        
        return response()->json($entity);
    }

    // update exist AidSourceWeight model by id
    public function update(\Illuminate\Http\Request $request, $id, $id2)
    {
        
        $this->authorize('update', AidSource::class);
        $entity = AidSourceWeight::findOrFail($id2);
        $original = clone $entity;
        
        $entity->value_min = $request->input('value_min');
        $entity->value_max = $request->input('value_max');
        $entity->weight = $request->input('weight');
        $entity->save();
        
        $entity->updateLinkedRank($original);
        
        return response()->json($entity);
    }

    // delete exist AidSourceWeight model by id
    public function destroy(\Illuminate\Http\Request $request, $id, $id2)
    {
        $entity = AidSourceWeight::findOrFail($id2);
        $this->authorize('delete', AidSource::class);
        $entity->delete();
        return response()->json(array('deleted' => 1));
    }
}
