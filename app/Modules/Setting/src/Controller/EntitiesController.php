<?php

namespace Setting\Controller;

use App\Http\Controllers\Controller;
use Auth\Model\User;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Setting\Model\Setting;

class EntitiesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
//        \Application\Model\I18nableEntity::getGlobalLanguageId();

    }

    // get constant entities
    public function index(Request $request)
    {
        \App\Http\Helpers::getLocale();
        $result = [];
        $entities = explode(',', request()->query('c'));
        foreach ($entities as $entity) {
            if (($data = $this->get($entity)) !== false) {
                    $result[$entity] = $data;
            }
        }

//        if (!array_key_exists($name, $entities)) {
//            return false;
//        }


        return response()->json($result);
    }

    // get constant model entities by name
    protected function get($name)
    {

        $user = \Auth::user();

        $entities = [
            'aidSources' => function() { return \Setting\Model\AidSource::all(); },
            'banks' => function() { return \Setting\Model\Bank::all(); },
            'currencies' => function() { return \Setting\Model\Currency::listCurrency(); },
            'deathCauses' => function() { return \Setting\Model\DeathCause::i18n()->get(); },
            'diseases' => function() { return \Setting\Model\Disease::i18n()->get(); },
            'documentTypes' => function() { return \Setting\Model\DocumentType::i18n()->get(); },
            'educationAuthorities' => function() { return \Setting\Model\Education\Authority::i18n()->get(); },
            'educationDegrees' => function() { return \Setting\Model\Education\Degree::i18n()->get(); },
            'educationStages' => function() { return \Setting\Model\Education\Stage::i18n()->get(); },
            'essentials' => function() { return \Setting\Model\Essential::all(); },
            'maritalStatus' => function() { return \Setting\Model\MaritalStatus::i18n()->get(); },
            'kinship' => function() { return \Setting\Model\Kinship::i18n()->get(); },
            'languages' => function() { return \Setting\Model\Language::all(); },
            'countries' => function() { return \Setting\Model\Location::i18n()->countries()->get(); },
            'aidCountries' => function() { return \Setting\Model\aidsLocation::i18n()->countries()->get(); },
            'aidCountriesTree' => function()  use($user) {
                            return \Setting\Model\aidsLocation::i18n()->countries()
//                                ->whereIn('id', function($query) use($user) {
//                                    $query->select('location_id')
//                                        ->from('char_organization_locations')
//                                        ->where('organization_id',$user->organization_id);
//                                })
                                ->get();

                            },
            'properties' => function() { return \Setting\Model\Property::i18n()->get(); },
            'propertyTypes' => function() { return \Setting\Model\PropertyType::i18n()->get(); },
            'roofMaterials' => function() { return \Setting\Model\RoofMaterial::i18n()->get(); },
            'furnitureStatus' => function() { return \Setting\Model\FurnitureStatus::i18n()->get(); },
            'houseStatus' => function() { return \Setting\Model\HouseStatus::i18n()->get(); },
            'habitableStatus' => function() { return \Setting\Model\HabitableStatus::i18n()->get(); },
            'buildingStatus' => function() { return \Setting\Model\BuildingStatus::i18n()->get(); },
            'workJobs' => function() { return \Setting\Model\Work\Job::i18n()->get(); },
            'workReasons' => function() { return \Setting\Model\Work\Reason::i18n()->get(); },
            'workStatus' => function() { return \Setting\Model\Work\Status::i18n()->get(); },
            'transferCompany' => function() { return \Setting\Model\TransferCompany::all(); },
            'paymentCategory' => function() { return \Setting\Model\PaymentCategory::i18n()->get(); },
            'workWages' => function() { return \Setting\Model\Work\Wage::all(); },
            'ProjectActivities' => function() { return \Setting\Model\ProjectActivities::i18n()->get(); },
            'ProjectBeneficiaryCategory' => function() { return \Setting\Model\ProjectBeneficiaryCategory::i18n()->get(); },
            'ProjectCategory' => function() { return \Setting\Model\ProjectCategory::i18n()->whereNULL('parent_id')->get(); },
            'ProjectType' => function() { return \Setting\Model\ProjectType::i18n()->get(); },
            'ProjectRegion' => function() { return \Setting\Model\ProjectRegion::i18n()->get(); },
            'organizationsCat' => function() { return \Setting\Model\OrganizationsCategory::i18n()->get(); },

        ];


        if (!array_key_exists($name, $entities)) {
            return false;
        }

        return call_user_func($entities[$name]);
    }
}