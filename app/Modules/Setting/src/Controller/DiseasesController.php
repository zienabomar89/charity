<?php

namespace Setting\Controller;

use Setting\Model\Disease;

class DiseasesController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        Disease::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize Disease model object to use in process
    protected function newModel()
    {
        return new Disease();
    }


    // get Disease model using id
    protected function findOrFail($id)
    {
        $entry = Disease::findOrFail($id);
        return $entry;
    }

    // get Disease model searching by name
    public function index()
    {
        $this->authorize('manage', Disease::class);
        $entries = Disease::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }


    // get all Disease model
    public function getList()
    {
        $entries = Disease::i18n()->get();
        return response()->json($entries);
    }
        // create new Disease model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', Disease::class);
        $this->validate($request, Disease::getValidatorRules());
        return parent::store($request);
    }
}
