<?php

namespace Setting\Controller;

use Setting\Model\Language;

class LanguagesController extends AbstractController
{
    // initialize Language model object to use in process
    protected function newModel()
    {
        return new Language();
    }

    // get Language model using id
    protected function findOrFail($id)
    {
        $entry = Language::findOrFail($id);
        return $entry;
    }

    // get Language model searching by name
    public function index()
    {
        $this->authorize('manage', Language::class);
        $entries = Language::query();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all Language model
    public function getList()
    {
        $entries = Language::all();
        return response()->json($entries);
    }

    // create new Language model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', Language::class);
        $this->validate($request, Language::getValidatorRules());
        return parent::store($request);
    }  
}
