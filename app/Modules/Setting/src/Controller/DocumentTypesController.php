<?php

namespace Setting\Controller;

use Setting\Model\DocumentType;

class DocumentTypesController extends AbstractController
{
    
    public function __construct()
    {
        $language_id = 1;
        DocumentType::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize DocumentType model object to use in process
    protected function newModel()
    {
        return new DocumentType();
    }

    // get DocumentType model using id
    protected function findOrFail($id)
    {
        $entry = DocumentType::findOrFail($id);
        return $entry;
    }

    // get DocumentType model searching by name
    public function index()
    {
        $this->authorize('manage', DocumentType::class);
        $entries = DocumentType::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all DocumentType model
    public function getList()
    {
        \App\Http\Helpers::getLocale();
        $entries = DocumentType::i18n()->orderBy('name')->get();
        return response()->json($entries);
    }

    // create new DocumentType model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', DocumentType::class);
        $this->validate($request, DocumentType::getValidatorRules());
        return parent::store($request);
    }  
}
