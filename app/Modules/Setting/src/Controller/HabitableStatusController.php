<?php

namespace Setting\Controller;

use Setting\Model\HabitableStatus;

class HabitableStatusController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
         HabitableStatus::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize HabitableStatus model object to use in process
    protected function newModel()
    {
        return new HabitableStatus();
    }

    // get HabitableStatus model using id
    protected function findOrFail($id)
    {
        $entry = HabitableStatus::findOrFail($id);
        return $entry;
    }

    // get HabitableStatus model searching by name
    public function index()
    {
        $this->authorize('manage', HabitableStatus::class);
        $entries = HabitableStatus::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all HabitableStatus model
    public function getList()
    {
        $entries = HabitableStatus::i18n()->get();
        return response()->json($entries);
    }

    // create new HabitableStatus model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', HabitableStatus::class);
        $this->validate($request, HabitableStatus::getValidatorRules());
        return parent::store($request);
    }
    
}
