<?php

namespace Setting\Controller;

use Setting\Model\Bank;
use Illuminate\Http\Request;

class BanksController extends AbstractController
{

    // initialize Bank model object to use in process
    protected function newModel()
    {
        return new Bank();
    }


    // get Bank model using id
    protected function findOrFail($id)
    {
        $entry = Bank::findOrFail($id);
        return $entry;
    }

    // get Bank model searching by name
    public function index()
    {
        $this->authorize('manage', Bank::class);
        $entries = Bank::query();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }


    // get all Bank model
    public function getList()
    {
        $entries = Bank::all();
        return response()->json($entries);
    }

    // create new Bank model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', Bank::class);
        $this->validate($request, Bank::getValidatorRules());
        return parent::store($request);
    }

    // download banks template instruction
    public function templateInstruction()
    {
        return response()->json(['download_token' => 'instruction-cheque-banks-template']);
    }

    // get bank template
    public function template($id)
    {

        $entity = $this->findOrFail($id);

        $sub = explode(".", $entity->template);
        $token = explode("/", $sub[0]);
        return response()->json(['download_token' => $token[1]]);
    }

    // upload bank template
    public function templateUpload($id,Request $request)
    {
        $templateFile = $request->file('file');
        $templatePath = $templateFile->store('templates');
        if ($templatePath) {
            $entity = $this->findOrFail($id);
            if($entity->template){
                if (\Storage::has($entity->template)) {
                    \Storage::delete($entity->template);
                }
            }
            $entity->template = $templatePath;
            $entity->saveOrFail();
            return response()->json($templatePath);
        }

        return false;
    }


}
