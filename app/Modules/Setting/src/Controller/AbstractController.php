<?php

namespace Setting\Controller;

use App\Http\Controllers\Controller;
use Auth\Model\User;
use Illuminate\Http\Request;

abstract class AbstractController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // create new object model
    public function store(Request $request)
    {
        try {
            $entity = $this->newModel();
            $language_id =  \App\Http\Helpers::getLocale();
            if ($entity instanceof \Application\Model\I18nableInterface) {
                $entity->setLanguageId($language_id);
                $entity->setI18nAttributes(array(
                    'name' => $request->input('name'),
                ));
            } else {
                $entity->name = $request->input('name');
            }
            
            if ($entity instanceof \Setting\Model\RankEntity) {
                if(!$request->input('weight') or is_null(!$request->input('weight'))){
                    $entity->weight = 0;
                }else{
                    $entity->weight = $request->input('weight');
                }
            }

            if($request->input('ratio')){
                $entity->ratio = $request->input('ratio');
            }

            if (method_exists($entity, 'setAttributesFromRequest')) {
                $entity->setAttributesFromRequest($request);
            }

            $entity->saveOrFail();
            if ($entity instanceof \Application\Model\I18nableInterface) {
                $entity->name = $request->input('name');
            }
            return response()->json($entity);
        } catch (\Exception $e) {
            return response()->json(array('error' => $e->getMessage()), 422);
        }
    }

    // get existed object model by id
    public function show($id)
    {
        $entity = $this->findOrFail($id);
        $this->authorize('view', $entity);
        return response()->json($entity);
    }

    // update existed object model by id
    public function update(Request $request, $id, $id2 = null)
    {
        if (null !== $id2) {
            $id = $id2;
        }
        
        $entity = $this->findOrFail($id);
        $this->authorize('update', $entity);
        
        $old = clone $entity;
        
        if ($entity instanceof \Application\Model\I18nableInterface) {
            $entity->setI18nAttributes(array(
                'name' => $request->input('name'),
            ));
        } else {
            $entity->name = $request->input('name');
        }
        
        if ($entity instanceof \Setting\Model\RankEntity) {
            $entity->weight = $request->input('weight');
        }
        
        if (method_exists($entity, 'setAttributesFromRequest')) {
            $entity->setAttributesFromRequest($request);
        }
        if($request->input('ratio')){
            $entity->ratio = $request->input('ratio');
        }

        $entity->saveOrFail();
        if ($entity instanceof \Setting\Model\RankEntity) {
            $entity->updateLinkedRank($old);
        }
        return response()->json($entity);
    }

    // delete existed object model by id
    public function destroy(Request $request, $id, $id2 = null)
    {
        if (null !== $id2) {
            $id = $id2;
        }
        
        $entity = $this->findOrFail($id);
        $this->authorize('delete', $entity);
        $entity->delete();
        return response()->json(array(
            'deleted' => 1,
        ));
    }

    // get object model translations by id
    public function translations($id, $id2 = null)
    {
        if (null !== $id2) {
            $id = $id2;
        }
        
        $entity = $this->findOrFail($id);
        return response()->json($entity->translations(array('name')));
    }

    // update or set object model translations by id
    public function saveTranslations(Request $request, $id, $id2 = null)
    {
        if (null !== $id2) {
            $id = $id2;
        }
        
        $entity = $this->findOrFail($id);
        $i18nModel = $entity->getI18nModel();
        $entries = $request->input('translation');
        foreach ($entries as $entry) {
            $i18nModel->updateOrCreate(array(
                $i18nModel->getEntityIdColumn() => $i18nModel->getAttribute($i18nModel->getEntityIdColumn()),
                $i18nModel->getLanguageIdColumn() => $entry['language_id'],
            ), array(
                'name' => $entry['name']
            ));
        }
        
        return response()->json($entity->translations(array('name')));
    }
}

