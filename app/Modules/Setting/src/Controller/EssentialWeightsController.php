<?php

namespace Setting\Controller;

use App\Http\Controllers\Controller;
use Setting\Model\Essential;
use Setting\Model\EssentialWeight;

class EssentialWeightsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get EssentialWeight model
    public function index($id)
    {
        $this->authorize('manage', Essential::class);
        $entries = EssentialWeight::where('essential_id', $id)->get();
        return response()->json($entries);
    }

    // create new EssentialWeight model
    public function store(\Illuminate\Http\Request $request, $id)
    {
        $this->authorize('create', Essential::class);

        $weights = $request->input('weights');
        $entries = [];
        foreach ($weights as $entry) {
            $entity = EssentialWeight::updateOrCreate(array(
                'essential_id' => $id,
                'value' => $entry['id'],
            ), array(
                'weight' => $entry['weight']
            ));
            $entries[] = $entity;
        }
        
        return response()->json($entries);
    }

}
