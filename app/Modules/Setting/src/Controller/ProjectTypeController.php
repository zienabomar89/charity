<?php

namespace Setting\Controller;

use Setting\Model\ProjectType;

class ProjectTypeController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        ProjectType::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize ProjectType model object to use in process
    protected function newModel()
    {
        return new ProjectType();
    }


    // get ProjectType model using id
    protected function findOrFail($id)
    {
        $entry = ProjectType::findOrFail($id);
        return $entry;
    }

    // get ProjectType model searching by name
    public function index()
    {
        $this->authorize('manage', ProjectType::class);
        $entries = ProjectType::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }


    // get all ProjectType model
    public function getList()
    {
        $entries = ProjectType::i18n()->get();
        return response()->json($entries);
    }

    // create new ProjectType model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', ProjectType::class);
        $this->validate($request, ProjectType::getValidatorRules());
        return parent::store($request);
    }
}
