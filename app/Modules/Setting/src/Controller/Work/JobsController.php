<?php

namespace Setting\Controller\Work;

use Setting\Controller\AbstractController;
use Setting\Model\Work\Job;

class JobsController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        Job::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize WorkJob model object to use in process
    protected function newModel()
    {
        return new Job();
    }

    // get WorkJob model using id
    protected function findOrFail($id)
    {
        $entry = Job::findOrFail($id);
        return $entry;
    }

    // get WorkJob model searching by name
    public function index()
    {
        $this->authorize('manage', Job::class);
        $entries = Job::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all WorkJob model
    public function getList()
    {
        $entries = Job::i18n()->get();
        return response()->json($entries);
    }

    // create new WorkJob model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', Job::class);
        $this->validate($request, Job::getValidatorRules());
        return parent::store($request);
    }
}
