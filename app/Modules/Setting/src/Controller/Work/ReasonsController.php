<?php

namespace Setting\Controller\Work;

use Setting\Controller\AbstractController;
use Setting\Model\Work\Reason;

class ReasonsController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        Reason::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize WorkReason model object to use in process
    protected function newModel()
    {
        return new Reason();
    }

    // get WorkReason model using id
    protected function findOrFail($id)
    {
        $entry = Reason::findOrFail($id);
        return $entry;
    }

    // get WorkReason model searching by name
    public function index()
    {
        $this->authorize('manage', Reason::class);
        $entries = Reason::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all WorkReason model
    public function getList()
    {
        $entries = Reason::i18n()->get();
        return response()->json($entries);
    }

    // create new WorkReason model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', Reason::class);
        $this->validate($request, Reason::getValidatorRules());
        return parent::store($request);
    }
}
