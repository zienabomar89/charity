<?php

namespace Setting\Controller\Work;

use Setting\Controller\AbstractController;
use Setting\Model\Work\Wage;

class WagesController extends AbstractController
{

    // initialize WorkWage model object to use in process
    protected function newModel()
    {
        return new Wage();
    }

    // get WorkWage model using id
    protected function findOrFail($id)
    {
        $entry = Wage::findOrFail($id);
        return $entry;
    }

    // get WorkWage model searching by name
    public function index()
    {
        $this->authorize('manage', Wage::class);
        $entries = Wage::query();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all WorkWage model
    public function getList()
    {
        $entries = Wage::all();
        return response()->json($entries);
    }

    // create new WorkWage model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', Wage::class);
        $this->validate($request, Wage::getValidatorRules());
        return parent::store($request);
    }
}
