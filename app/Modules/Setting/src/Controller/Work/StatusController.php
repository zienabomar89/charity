<?php

namespace Setting\Controller\Work;

use Setting\Controller\AbstractController;
use Setting\Model\Work\Status;

class StatusController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        Status::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize WorkStatus model object to use in process
    protected function newModel()
    {
        return new Status();
    }

    // get WorkStatus model using id
    protected function findOrFail($id)
    {
        $entry = Status::findOrFail($id);
        return $entry;
    }

    // get WorkStatus model searching by name
    public function index()
    {
        $this->authorize('manage', Status::class);
        $entries = Status::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all WorkStatus model
    public function getList()
    {
        $entries = Status::i18n()->get();
        return response()->json($entries);
    }

    // create new WorkStatus model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', Status::class);
        $this->validate($request, Status::getValidatorRules());
        return parent::store($request);
    }
}
