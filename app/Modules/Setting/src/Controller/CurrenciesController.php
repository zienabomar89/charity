<?php

namespace Setting\Controller;

use Setting\Model\Currency;

class CurrenciesController extends AbstractController
{

    // initialize Currency model object to use in process
    protected function newModel()
    {
        return new Currency();
    }


    // get Currency model using id
    protected function findOrFail($id)
    {
        $entry = Currency::findOrFail($id);
        return $entry;
    }

    // get Currency model searching by name
    public function index()
    {
        $this->authorize('manage', Currency::class);
        $entries = Currency::query();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }


    // get all Currency model
    public function getList()
    {
        $entries = Currency::all();
        return response()->json($entries);
    }

    // create new Currency model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', Currency::class);
        $this->validate($request, Currency::getValidatorRules());
        return parent::store($request);
    }
}
