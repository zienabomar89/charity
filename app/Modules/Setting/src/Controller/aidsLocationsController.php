<?php

namespace Setting\Controller;

use Common\Model\Citizen;
use Common\Model\Transfers\Transfers;
use function GuzzleHttp\Promise\all;
use Organization\Model\OrgLocations;
use Setting\Model\aidsLocation;
use Setting\Model\aidsLocationI18n;
use Illuminate\Http\Request;
use Organization\Model\Organization;
use Common\Model\Person;

class aidsLocationsController extends AbstractController
{

    public function __construct()
    {
//        $language_id = 1;
//        aidsLocation::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize aidsLocation model object to use in process
    protected function newModel()
    {
        \App\Http\Helpers::getLocale();
        $aidsLocation = new aidsLocation();
        $routeName = request()->route()->getName();
        if ($routeName == 'scountries.store') {
            $aidsLocation->Location_type = aidsLocation::TYPE_COUNTRY;
        }
        else if ($routeName == 'sdistricts.store') {
            $aidsLocation->Location_type = aidsLocation::TYPE_DISTRICT;
        }
        else if ($routeName == 'sregions.store') {
            $aidsLocation->Location_type = aidsLocation::TYPE_REGION;
        }
        else if ($routeName == 'sneighborhoods.store') {
            $aidsLocation->Location_type = aidsLocation::TYPE_NEIGHBORHOOD;
        }
        else if ($routeName == 'ssquares.store') {
            $aidsLocation->Location_type = aidsLocation::TYPE_SQUARE;
        }
        else if ($routeName == 'smosques.store') {
            $aidsLocation->Location_type = aidsLocation::TYPE_MOSQUES;
        }
        return $aidsLocation;
    }

    // get aidsLocation row using id
    protected function findOrFail($id)
    {
        \App\Http\Helpers::getLocale();
        $entry = aidsLocation::i18n()->findOrFail($id);
        return $entry;
    }

    // get aidsLocation rows searching by name (according location type)
    public function index($id = null)
    {
        \App\Http\Helpers::getLocale();
        $this->authorize('manage', aidsLocation::class);

        $routeName = request()->route()->getName();
        if ($routeName == 'scountries.index') {
            return $this->countries();
        }
        else if ($routeName == 'sdistricts.index') {
            return $this->districts($id);
        }
        else if ($routeName == 'sregions.index') {
            return $this->regions($id);
        }
        else if ($routeName == 'sneighborhoods.index') {
            return $this->neighborhoods($id);
        }
        else if ($routeName == 'ssquares.store') {
            return $this->squares($id);
        }
        else if ($routeName == 'smosques.index') {
            return $this->mosques($id);
        }
        $entries = aidsLocation::i18n()->descendants($id)->paginate();
        return response()->json($entries);
    }

    // get all aidsLocation rows (according location type)
    public function getList($id = null)
    {
        \App\Http\Helpers::getLocale();
        $routeName = request()->route()->getName();

        if ($routeName == 'scountries.list') {
            return $this->countries();
        }
        else if ($routeName == 'sdistricts.list') {
            return $this->districts($id);
        }
        else if ($routeName == 'sregions.list') {
            return $this->regions($id);
        }
        else if ($routeName == 'sneighborhoods.list') {
            return $this->neighborhoods($id);
        }
        else if ($routeName == 'ssquares.list') {
            return $this->squares($id);
        }
        else if ($routeName == 'smosques.list') {
            return $this->neighborhoods($id);
        }

        $entries = aidsLocation::i18n()->descendants($id)->get();
        return response()->json($entries);
    }

    // get all aidsLocation rows (according location type)
    public function typeList($type)
    {
        \App\Http\Helpers::getLocale();
        $entries = aidsLocation::i18n()->where('Location_type', '=', $type)->get();
        return response()->json($entries);
    }

    // create new aidsLocation model (according location type)
    public function store(Request $request)
    {
        \App\Http\Helpers::getLocale();
        $this->authorize('create', aidsLocation::class);
        $rules = aidsLocation::getValidatorRules();
        $routeName = request()->route()->getName();
        if ($routeName == 'scountries.store') {
            unset($rules['parent_id']);
        }

        $this->validate($request, $rules);

        $where = array('parent_id'=> $request->parent_id);

        if ($routeName == 'scountries.store') {
            $where['parent_id'] = Null;
            $where['location_type'] = aidsLocation::TYPE_COUNTRY;
        }else if ($routeName == 'sdistricts.store') {
            $where['location_type'] = aidsLocation::TYPE_DISTRICT;
        }
        else if ($routeName == 'sregions.store') {
            $where['location_type'] = aidsLocation::TYPE_REGION;
        }
        else if ($routeName == 'sneighborhoods.store') {
            $where['location_type'] = aidsLocation::TYPE_NEIGHBORHOOD;
        }
        else if ($routeName == 'ssquares.store') {
            $where['location_type'] = aidsLocation::TYPE_SQUARE;
        }
        else if ($routeName == 'smosques.store') {
            $where['location_type'] = aidsLocation::TYPE_MOSQUES;
        }

        $sum =aidsLocation::where($where)->sum('ratio');
        $ratio = $request->ratio;
        if(($ratio + $sum) > 100){
            return response()->json(array('error' => trans('common::application.Total input ratios must be equal 100% for the current tree level')), 422);
        }


        $entity = $this->newModel();
        if ($entity instanceof \Application\Model\I18nableInterface) {
            $entity->setLanguageId(1);
            $entity->setI18nAttributes(array(
                'name' => $request->input('name'),
            ));
        } else {
            $entity->name = $request->input('name');
        }

        if ($entity instanceof \Setting\Model\RankEntity) {
            if(!$request->input('weight') or is_null(!$request->input('weight'))){
                $entity->weight = 0;
            }else{
                $entity->weight = $request->input('weight');
            }
        }

        if($request->input('ratio')){
            $entity->ratio = $request->input('ratio');
        }

        if (method_exists($entity, 'setAttributesFromRequest')) {
            $entity->setAttributesFromRequest($request);
        }

        $entity->saveOrFail();
        if ($entity instanceof \Application\Model\I18nableInterface) {
            $entity->name = $request->input('name');
        }

        OrgLocations::create([ 'organization_id' => 1 ,'location_id' => $entity->id , 'created_at' => date('Y-m-d H:i:s')]);
        return response()->json($entity);

//        return parent::store($request);
    }

    // get countries from aidsLocation rows searching by name
    public function countries()
    {
        \App\Http\Helpers::getLocale();
        $entries = aidsLocation::i18n()->countries();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get districts from aidsLocation rows searching by name , id (parent)
    function districts($id = null)
    {
        \App\Http\Helpers::getLocale();
        $entries = aidsLocation::i18n()->districts($id);
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get regions from aidsLocation rows searching by name , id (parent)
    public function regions($id = null)
    {
        \App\Http\Helpers::getLocale();
        $entries = aidsLocation::i18n()->regions($id);
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get neighborhoods from aidsLocation rows searching by name , id (parent)
    public function neighborhoods($id = null)
    {
        \App\Http\Helpers::getLocale();
        $entries = aidsLocation::i18n()->neighborhoods($id);
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get mosques from aidsLocation models searching by name , id (parent)
    public function mosques($id = null)
    {
        \App\Http\Helpers::getLocale();
        $entries = aidsLocation::i18n()->mosques($id);
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get squares from aidsLocation models searching by name , id (parent)
    public function squares($id = null)
    {
        \App\Http\Helpers::getLocale();
        $entries = aidsLocation::i18n()->squares($id);
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get countries list from aidsLocation models searching id (parent)
    public function countrieslist()
    {
        \App\Http\Helpers::getLocale();
        $entries = aidsLocation::i18n()->countries()->get();
        return response()->json($entries);
    }

    // get districts list from aidsLocation models searching id (parent)
    public function districtsList(Request $request,$id = null)
    {

        $user = \Auth::user();
        \App\Http\Helpers::getLocale();
        $query = aidsLocation::i18n()->districts($id);

        if (isset($request->tree)) {
            $query->whereIn('id', function($query) use($user) {
                $query->select('location_id')
                    ->from('char_organization_locations')
                    ->where('organization_id',$user->organization_id);
            });
        }

        $entries = $query->get();
        return response()->json($entries);
    }

    // get regions list from aidsLocation models searching id (parent)
    public function regionsList(Request $request,$id = null)
    {
        $user = \Auth::user();
        \App\Http\Helpers::getLocale();
        $query = aidsLocation::i18n()->regions($id);

        if (isset($request->tree)) {
            $query->whereIn('id', function($query) use($user) {
                $query->select('location_id')
                    ->from('char_organization_locations')
                    ->where('organization_id',$user->organization_id);
            });
        }

        $entries = $query->get();
        return response()->json($entries);
    }

    // get neighborhoods list from aidsLocation models searching id (parent)
    public function neighborhoodsList(Request $request,$id = null)
    {
        $user = \Auth::user();
        \App\Http\Helpers::getLocale();
        $query = aidsLocation::i18n()->neighborhoods($id);

        if (isset($request->tree)) {
            $query->whereIn('id', function($query) use($user) {
                $query->select('location_id')
                    ->from('char_organization_locations')
                    ->where('organization_id',$user->organization_id);
            });
        }

        $entries = $query->get();
        return response()->json($entries);
    }

    // get squares list from aidsLocation models searching id (parent)
    public function squaresList(Request $request,$id = null)
    {
        $user = \Auth::user();
        \App\Http\Helpers::getLocale();
        $query = aidsLocation::i18n()->squares($id);

        if (isset($request->tree)) {
            $query->whereIn('id', function($query) use($user) {
                $query->select('location_id')
                    ->from('char_organization_locations')
                    ->where('organization_id',$user->organization_id);
            });
        }

        $entries = $query->get();
        return response()->json($entries);
    }

    // get mosques list from aidsLocation models searching id (parent)
    public function mosquesList(Request $request,$id = null)
    {
        $user = \Auth::user();
        \App\Http\Helpers::getLocale();
        $query = aidsLocation::i18n()->mosques($id);

        if (isset($request->tree)) {
            $query->whereIn('id', function($query) use($user) {
                $query->select('location_id')
                    ->from('char_organization_locations')
                    ->where('organization_id',$user->organization_id);
            });
        }

        $entries = $query->get();
        return response()->json($entries);
    }

    // transfer tree and save
    // transfer tree and save
    public function tree($id = null)
    {
        \App\Http\Helpers::getLocale();

        $entry = aidsLocation::findOrFail($id);
        $location_type = $entry->location_type;
        $response = ['countries' => [] , 'country_id' => null ,'re_country_id' => true ,
            'districts' => [] ,  'district_id' => null ,'re_district_id' => true ,
            'regions' => [] ,  'region_id' => null , 're_region_id' => true ,
            'neighborhoods' => [] ,  'neighborhood_id' => null ,'re_neighborhood_id' => true ,
            'squares' => [] ,  'square_id' => null , 're_square_id' => true ];

        $response['countries'] = aidsLocation::i18n()->where(['location_type' => aidsLocation::TYPE_COUNTRY ])->get();

        if ($location_type == aidsLocation::TYPE_DISTRICT) {
            $response['country_id'] = $entry->parent_id;
            $response['re_district_id'] = false;
            $response['re_region_id'] = false;
            $response['re_neighborhood_id'] = false;
            $response['re_square_id'] = false;
        }
        else if ($location_type == aidsLocation::TYPE_REGION) {
            $response['re_region_id'] = false;
            $response['re_neighborhood_id'] = false;
            $response['re_square_id'] = false;

            $response['districts'] = aidsLocation::i18n()->where(['location_type' => aidsLocation::TYPE_DISTRICT ])->get();
            $response['district_id'] = $entry->parent_id;

            $district = aidsLocation::i18n()->where(['id' => $entry->parent_id ])->first();
            if ($district) {
                $response['country_id'] = $district->parent_id;
            }

        }
        else if ($location_type == aidsLocation::TYPE_NEIGHBORHOOD) {
            $response['re_neighborhood_id'] = false;
            $response['re_square_id'] = false;

            $response['districts'] = aidsLocation::i18n()->where(['location_type' => aidsLocation::TYPE_DISTRICT ])->get();

            $response['region_id'] = $entry->parent_id;
            $region = aidsLocation::i18n()->where(['id' => $entry->parent_id ])->first();

            if ($region) {
                $response['district_id'] = $region->parent_id;
                $district = aidsLocation::i18n()->where(['id' => $region->parent_id ])->first();
                if ($district) {
                    $response['country_id'] = $district->parent_id;
                    $response['regions'] = aidsLocation::i18n()->where(['location_type' => aidsLocation::TYPE_REGION ,
                        'parent_id' => $district->id ])->get();

                }
            }

        }
        else if ($location_type == aidsLocation::TYPE_SQUARE) {
            $response['re_square_id'] = false;

            $response['districts'] = aidsLocation::i18n()->where(['location_type' => aidsLocation::TYPE_DISTRICT ])->get();
            $response['neighborhood_id'] = $entry->parent_id;
            $neighborhoods = aidsLocation::i18n()->where(['id' => $entry->parent_id ])->first();
            if ($neighborhoods) {
                $response['region_id'] = $neighborhoods->parent_id;
                $region = aidsLocation::i18n()->where(['id' => $neighborhoods->parent_id ])->first();
                if ($region) {
                    $response['district_id'] = $region->parent_id;
                    $district = aidsLocation::i18n()->where(['id' => $region->parent_id ])->first();
                    if ($district) {
                        $response['country_id'] = $district->parent_id;
                        $response['regions'] = aidsLocation::i18n()->where(['location_type' => aidsLocation::TYPE_REGION ,
                            'parent_id' => $district->id ])->get();

                    }
                    $response['neighborhoods'] = aidsLocation::i18n()->where(['location_type' => aidsLocation::TYPE_NEIGHBORHOOD ,
                        'parent_id' => $region->id ])->get();
                }
            }


        }
        else if ($location_type == aidsLocation::TYPE_MOSQUES) {

            $response['districts'] = aidsLocation::i18n()->where(['location_type' => aidsLocation::TYPE_DISTRICT ])->get();

            $response['square_id'] = $entry->parent_id;
            $square = aidsLocation::i18n()->where(['id' => $entry->parent_id ])->first();
            if ($square) {
                $response['neighborhood_id'] = $square->parent_id;
                $neighborhoods = aidsLocation::i18n()->where(['id' => $square->parent_id ])->first();
                if ($neighborhoods) {
                    $response['region_id'] = $neighborhoods->parent_id;
                    $region = aidsLocation::i18n()->where(['id' => $neighborhoods->parent_id ])->first();
                    if ($region) {
                        $response['district_id'] = $region->parent_id;
                        $district = aidsLocation::i18n()->where(['id' => $region->parent_id ])->first();
                        if ($district) {
                            $response['country_id'] = $district->parent_id;
                            $response['regions'] = aidsLocation::i18n()->where(['location_type' => aidsLocation::TYPE_REGION ,
                                'parent_id' => $district->id ])->get();

                        }
                        $response['neighborhoods'] = aidsLocation::i18n()->where(['location_type' => aidsLocation::TYPE_NEIGHBORHOOD ,
                            'parent_id' => $region->id ])->get();
                    }
                }
                $response['squares'] = aidsLocation::i18n()->where(['location_type' => aidsLocation::TYPE_SQUARE ,
                    'parent_id' => $square->parent_id ])->get();
            }
        }

        return response()->json($response);
    }

    public function transfer(Request $request,$id)
    {
        \App\Http\Helpers::getLocale();

        $entry = aidsLocation::findOrFail($id);
        $location_type = $entry->location_type;
        $prev = $request->pre;
        $data = $request->all();
        unset($data['pre']);
        unset($data['entity']);
        unset($data['id']);

        if ( ($data['country_id'] == $prev['country_id']) &&
            ($data['district_id'] == $prev['district_id']) &&
            ($data['region_id'] == $prev['region_id']) &&
            ($data['neighborhood_id'] == $prev['neighborhood_id'])&&
            ($data['square_id'] == $prev['square_id'])) {
            return response()->json(["status" => 'failed' ,
                "msg" => trans('common::application.there is no update on location tree')]);

        }

        $updates=[];
        $where=[];
        $tree=[];

        if ($location_type == aidsLocation::TYPE_DISTRICT) {
            $where['adsdistrict_id'] = $entry->id;
            $entry->parent_id = $data['country_id'];
            $entry->save();

            $updates['adscountry_id'] = $entry->parent_id;
            $tree[] = $updates['adscountry_id'];

        }
        else if ($location_type == aidsLocation::TYPE_REGION) {
            $where['adsregion_id'] = $entry->id;
            $entry->parent_id = $data['district_id'];
            $entry->save();

            $updates['adsdistrict_id'] = $entry->parent_id ;
            $tree[] = $updates['adsdistrict_id'];

            if ($data['country_id'] != $prev['country_id']) {
                $updates['adscountry_id'] = $data['country_id'];
                $tree[] = $updates['adscountry_id'];
            }

        }
        else if ($location_type == aidsLocation::TYPE_NEIGHBORHOOD) {
            $where['adsneighborhood_id'] = $entry->id;
            $entry->parent_id = $data['region_id'];
            $entry->save();

            $updates['adsregion_id'] = $entry->parent_id;
            $updates['adsdistrict_id'] = $data['district_id'];
            $updates['adscountry_id'] = $data['country_id'];

            $tree[] = $updates['adsregion_id'];
            $tree[] = $updates['adsdistrict_id'];
            $tree[] = $updates['adscountry_id'];
        }
        else if ($location_type == aidsLocation::TYPE_SQUARE) {
            $where['adssquare_id'] = $entry->id;
            $entry->parent_id = $data['neighborhood_id'];
            $entry->save();

            $updates['adsneighborhood_id'] = $entry->parent_id;
            $tree[] = $updates['adsneighborhood_id'];

            if ($data['region_id'] != $prev['region_id']) {
                $updates['adsregion_id'] = $data['region_id'];
                $tree[] = $updates['adsregion_id'];
            }

            if ($data['district_id'] != $prev['district_id']) {
                $updates['adsdistrict_id'] = $data['district_id'];
                $tree[] = $updates['adsdistrict_id'];
            }

            if ($data['country_id'] != $prev['country_id']) {
                $updates['adscountry_id'] = $data['country_id'];
                $tree[] = $updates['adscountry_id'];
            }

        }
        else if ($location_type == aidsLocation::TYPE_MOSQUES) {
            $where['adsmosques_id'] = $entry->id;
            $entry->parent_id = $data['square_id'];
            $entry->save();

            $updates['adssquare_id'] = $entry->parent_id;
            $tree[] = $updates['adssquare_id'];

            if ($data['neighborhood_id'] != $prev['neighborhood_id']) {
                $updates['adsneighborhood_id'] = $data['neighborhood_id'];
                $tree[] = $updates['adsneighborhood_id'];
            }

            if ($data['region_id'] != $prev['region_id']) {
                $updates['adsregion_id'] = $data['region_id'];
                $tree[] = $updates['adsregion_id'];
            }

            if ($data['district_id'] != $prev['district_id']) {
                $updates['adsdistrict_id'] = $data['district_id'];
                $tree[] = $updates['adsdistrict_id'];
            }

            if ($data['country_id'] != $prev['country_id']) {
                $updates['adscountry_id'] = $data['country_id'];
                $tree[] = $updates['adscountry_id'];
            }
        }

        foreach ($tree as $location_id) {
            $organizations = \DB::table('char_organizations')
                ->where(function ($ws) use ($id,$location_id){
                    $ws->whereIn('id',function ($w) use ($id){
                        $w->select('organization_id')
                            ->from('char_organization_locations')
                            ->where('location_id' , $id);
                    });
                    $ws->whereNotIn('id',function ($w) use ($location_id){
                        $w->select('organization_id')
                            ->from('char_organization_locations')
                            ->where('location_id' , $location_id);
                    });
                })
                ->selectRaw("char_organizations.id as organization_id")
                ->groupBy('char_organizations.id')
                ->get();

            foreach ($organizations as $key => $value) {
                OrgLocations::create(['organization_id'=>$value->organization_id,'location_id'=>$location_id,'created_at'=>date('Y-m-d H:i:s')]);
            }
        }

        if (sizeof($updates) > 0 ){
            Citizen::where($where)->update($updates);
            Person::where($where)->update($updates);
            Transfers::where($where)->update($updates);

        }

        return response()->json(["status" => 'success']);
    }

    // delete existed object model by id
    public function destroy(Request $request, $id, $id2 = null)
    {
        if (null !== $id2) {
            $id = $id2;
        }

        $entity = $this->findOrFail($id);

        $related = OrgLocations::where(function ($q) use ($id){
                            $q->where('location_id',$id);
                            $q->where('organization_id','!=',1);
                        })->count();

        if($related > 0 ){
            $response = [
                'status' => 'error',
                'msg' => 'يجب عليك رفع العنوان المراد حذفه من نطاق عمل الجمعيات في النظم حتى يتسنى لك اتمام عملية الحذف',
            ];
            return response()->json($response);
        }
        OrgLocations::where(['location_id' => $id ,'organization_id'=>1])->delete();
        $this->authorize('delete', $entity);
        $entity->delete();
        return response()->json(array(
            'deleted' => 1,
        ));
    }

    // get existed object model by id
    public function show($id)
    {
        $entity = $this->findOrFail($id);

        $fullTree = [];

        $parent = $entity->parent_id;
        while(!is_null($parent)) {
            $parentEntity = $this->findOrFail($parent);
            $fullTree[] = ['id' =>$parentEntity->id ,'name' =>$parentEntity->name];
            $parent = $parentEntity->parent_id;
        }

        $entity->tree = array_reverse($fullTree);
        $this->authorize('view', $entity);
        return response()->json($entity);
    }
}
