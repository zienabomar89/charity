<?php

namespace Setting\Controller;

use Setting\Model\ProjectRegion;

class ProjectRegionController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        ProjectRegion::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize ProjectRegion model object to use in process
    protected function newModel()
    {
        return new ProjectRegion();
    }


    // get ProjectRegion model using id
    protected function findOrFail($id)
    {
        $entry = ProjectRegion::findOrFail($id);
        return $entry;
    }

    // get ProjectRegion model searching by name
    public function index()
    {
        $this->authorize('manage', ProjectRegion::class);
        $entries = ProjectRegion::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }


    // get all ProjectRegion model
    public function getList()
    {
        $entries = ProjectRegion::i18n()->get();
        return response()->json($entries);
    }

    // create new ProjectRegion model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', ProjectRegion::class);
        $this->validate($request, ProjectRegion::getValidatorRules());
        return parent::store($request);
    }
}
