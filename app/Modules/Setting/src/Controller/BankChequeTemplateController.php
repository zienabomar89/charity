<?php

namespace Setting\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Setting\Model\BankChequeTemplate;

class BankChequeTemplateController extends Controller {


    public function __construct()
    {
        $this->middleware('auth:api');
    }
    public function index(Request $request){
        return response()->json(BankChequeTemplate::filter() );
    }

    // create new BankChequeTemplate model
    public function store(Request $request){
        $response = array();

        $error = \App\Http\Helpers::isValid($request->all(),BankChequeTemplate::getValidatorRules());
        if($error)
            return response()->json($error);

        if(BankChequeTemplate::Create($request->all())){
            $response["status"]= 'success';
            $response["msg"]= trans('sms::application.The row is inserted to db');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sms::application.The row is not insert to db');

        }
        return response()->json($response);

    }

    // update data of BankChequeTemplate object model
    public function update(Request $request, $id)
    {
        $error = \App\Http\Helpers::isValid($request->all(),BankChequeTemplate::getValidatorRules());
        if($error)
            return response()->json($error);

        $inputs=$request->all();
        unset($inputs['id']);
        $update= BankChequeTemplate::where(['bank_id'=>$request->bank_id])->update($inputs);
        if($update) {
            $response["status"]= 'success';
            $response["msg"]= trans('sms::application.The row is updated');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sms::application.There is no update');
        }
        return response()->json($response);

    }

}
