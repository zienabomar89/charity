<?php

namespace Setting\Controller;

use Setting\Model\HouseStatus;

class HouseStatusController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        HouseStatus::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize HouseStatus model object to use in process
    protected function newModel()
    {
        return new HouseStatus();
    }

    // get HouseStatus model using id
    protected function findOrFail($id)
    {
        $entry = HouseStatus::findOrFail($id);
        return $entry;
    }

    // get HouseStatus model searching by name
    public function index()
    {
        $this->authorize('manage', HouseStatus::class);
        $entries = HouseStatus::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all HouseStatus model
    public function getList()
    {
        $entries = HouseStatus::i18n()->get();
        return response()->json($entries);
    }

    // create new HouseStatus model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', HouseStatus::class);
        $this->validate($request, HouseStatus::getValidatorRules());
        return parent::store($request);
    }
    
    
}
