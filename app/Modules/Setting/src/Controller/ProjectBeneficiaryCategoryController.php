<?php

namespace Setting\Controller;

use Setting\Model\ProjectBeneficiaryCategory;

class ProjectBeneficiaryCategoryController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        ProjectBeneficiaryCategory::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize ProjectBeneficiaryCategory model object to use in process
    protected function newModel()
    {
        return new ProjectBeneficiaryCategory();
    }


    // get ProjectBeneficiaryCategory model using id
    protected function findOrFail($id)
    {
        $entry = ProjectBeneficiaryCategory::findOrFail($id);
        return $entry;
    }

    // get ProjectBeneficiaryCategory model searching by name
    public function index()
    {
        $this->authorize('manage', ProjectBeneficiaryCategory::class);
        $entries = ProjectBeneficiaryCategory::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }


    // get all ProjectBeneficiaryCategory model
    public function getList()
    {
        $entries = ProjectBeneficiaryCategory::i18n()->get();
        return response()->json($entries);
    }

    // create new ProjectBeneficiaryCategory model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', ProjectBeneficiaryCategory::class);
        $this->validate($request, ProjectBeneficiaryCategory::getValidatorRules());
        return parent::store($request);
    }
}
