<?php

namespace Setting\Controller;

use Organization\Model\OrgLocations;
use Setting\Model\aidsLocation;
use Setting\Model\ProjectCategory;

class ProjectCategoryController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        ProjectCategory::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize ProjectCategory model object to use in process
    protected function newModel()
    {
        return new ProjectCategory();
    }


    // get ProjectCategory model using id
    protected function findOrFail($id)
    {
        $entry = ProjectCategory::findOrFail($id);
        return $entry;
    }

    // get ProjectCategory model searching by name
    public function index($id = null)
    {
        $this->authorize('manage', ProjectCategory::class);
        $entries = ProjectCategory::i18n();

        if(is_null($id)){
            $entries->whereNull('parent_id');
        }else{
            $entries->where('parent_id',$id);

        }
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all ProjectCategory model
    public function getList()
    {
        $entries = ProjectCategory::i18n()->get();
        return response()->json($entries);
    }

    public function subList($id)
    {
        $entries = ProjectCategory::i18n()->where('parent_id',$id)->get();
        return response()->json($entries);
    }

    // create new ProjectCategory model
    public function store(\Illuminate\Http\Request $request)
    {
        \App\Http\Helpers::getLocale();
        $this->authorize('create', ProjectCategory::class);
        $rules = ProjectCategory::getValidatorRules();
        $routeName = request()->route()->getName();

        if ($routeName != 'sub.store') {
            unset($rules['parent_id']);
        }

        $this->validate($request, $rules);

        $entity = $this->newModel();
        if ($entity instanceof \Application\Model\I18nableInterface) {
            $entity->setLanguageId(1);
            $entity->setI18nAttributes(array(
                'name' => $request->input('name'),
            ));
        } else {
            $entity->name = $request->input('name');
        }

        if($request->input('parent_id')){
            $entity->parent_id = $request->input('parent_id');
        }

        if ($entity instanceof \Setting\Model\RankEntity) {
            if(!$request->input('weight') or is_null(!$request->input('weight'))){
                $entity->weight = 0;
            }else{
                $entity->weight = $request->input('weight');
            }
        }

        if($request->input('ratio')){
            $entity->ratio = $request->input('ratio');
        }

        if (method_exists($entity, 'setAttributesFromRequest')) {
            $entity->setAttributesFromRequest($request);
        }

        $entity->saveOrFail();
        if ($entity instanceof \Application\Model\I18nableInterface) {
            $entity->name = $request->input('name');
        }

        return response()->json($entity);

//        return parent::store($request);
    }




}
