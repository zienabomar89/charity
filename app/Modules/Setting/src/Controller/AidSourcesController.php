<?php

namespace Setting\Controller;

use Setting\Model\AidSource;

class AidSourcesController extends AbstractController
{

    // initialize AidSource model object to use in process
    protected function newModel()
    {
        return new AidSource();
    }

    // get AidSource model using id
    protected function findOrFail($id)
    {
        $entry = AidSource::findOrFail($id);
        return $entry;
    }

    // get AidSource model searching by name
    public function index()
    {
        $this->authorize('manage', AidSource::class);
        $entries = AidSource::query();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all AidSource model
    public function getList()
    {
        $entries = AidSource::all();
        return response()->json($entries);
    }

    // create new AidSource model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', AidSource::class);
        $this->validate($request, AidSource::getValidatorRules());
        return parent::store($request);
    }

}
