<?php

namespace Setting\Controller;

use Setting\Model\Branch;

class BankBranchesController extends AbstractController
{

    // initialize Branch model object to use in process
    protected function newModel()
    {
        return new Branch();
    }

    // get Branch model using id
    protected function findOrFail($id)
    {
        $entry = Branch::findOrFail($id);
        return $entry;
    }

    // get all Branch model for bank using id
    public function index($id)
    {
        $this->authorize('manage', Branch::class);
        $entries = Branch::where('bank_id', $id);
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get all Branch model
    public function getList($id)
    {
        $entries = Branch::where('bank_id', $id)->get();
        return response()->json($entries);
    }

    // create new Branch model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', Branch::class);
        $this->validate($request, Branch::getValidatorRules());

        $entity = $this->newModel();
        $entity->bank_id = $request->input('parent_id');
        $entity->name = $request->input('name');
        $entity->saveOrFail();
        return response()->json($entity);
    }
}
