<?php

namespace Setting\Controller;

use Setting\Model\ProjectActivities;

class ProjectActivitiesController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        ProjectActivities::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize ProjectActivities model object to use in process
    protected function newModel()
    {
        return new ProjectActivities();
    }


    // get ProjectActivities model using id
    protected function findOrFail($id)
    {
        $entry = ProjectActivities::findOrFail($id);
        return $entry;
    }

    // get ProjectActivities model searching by name
    public function index()
    {
        $this->authorize('manage', ProjectActivities::class);
        $entries = ProjectActivities::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }


    // get all ProjectActivities model
    public function getList()
    {
        $entries = ProjectActivities::i18n()->get();
        return response()->json($entries);
    }

    // create new ProjectActivities model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', ProjectActivities::class);
        $this->validate($request, ProjectActivities::getValidatorRules());
        return parent::store($request);
    }
}
