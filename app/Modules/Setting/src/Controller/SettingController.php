<?php

namespace Setting\Controller;

use App\Http\Controllers\Controller;
use Forms\Model\CustomForms;
use Illuminate\Http\Request;
use Setting\Model\Setting;

class SettingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {

    }
    // get Setting model by id (setting name)
    public function show($id)
    {
        $obj = Setting::findOrFail($id);
        $this->authorize('view', $obj);
        return response()->json($obj);
    }

    // update Setting model by id (setting name)
    public function update(Request $request, $id)
    {
        $this->authorize('update', Setting::class);

        $response = array();
        $user = \Auth::user();
        $organization_id=$user->organization_id;

        $settings =$request->toArray();
        $updated=0;
        foreach($settings as $k => $v) {
            if( Setting::where(['id' =>$k , 'organization_id'=> $organization_id])->first()){
                if(Setting::where(['id' =>$k , 'organization_id'=> $organization_id])->update(['value' => $v])){
                    $updated++;
                }
            }else{
                if(Setting::insert(['id' =>$k , 'organization_id'=> $organization_id,'value' => $v])){
                    $updated++;
                }
            }
        }

        if($updated != 0) {
            $response["status"]= 'success';
            $response["msg"]= trans('setting::application.The row is updated');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('setting::application.There is no update');
        }
        return response()->json($response);
    }

    // get Setting model by id (setting name) and login user organization
    public function getSettingById(Request $request,$id)
    {
        $response = array('status' =>false);
        $user = \Auth::user();

        $entry = Setting::where(['id' => $id ,'organization_id' => $user->organization_id])->first();
        if (!is_null($entry)) {
            $response["status"]= true;
            $response["Setting"]= $entry;
        }

        return response()->json($response);
    }

    // get setting of login user organization ancestor
    public function getAncestorSetting($id)
    {
        $response = array('status' =>false);
        $user = \Auth::user();
        $ancestor_id = \Organization\Model\Organization::getAncestor($user->organization_id);

        $entry = Setting::where(['id' => $id ,'organization_id' => $ancestor_id])->first();
        if (!is_null($entry)) {
            $response["status"]= true;
            $response["Setting"]= $entry;
        }

        return response()->json($response);
    }

    // get setting of CustomForm login user organization by id
    public function getCustomForm(Request $request,$id)
    {
        $response = array();
        $user = \Auth::user();

        $form= \Setting\Model\Setting::where(['id'=>$id,'organization_id'=>$user->organization_id])->first();
        $response["status"]= false;

        if(!is_null($form)){
            $ActiveForm=CustomForms::where(function ($q) use($form){
                $q->where('id','=',$form->value);
                $q->where('status','=',1);
            })->first();

            if($ActiveForm){
                $response["status"]= true;
                $response["Setting"]= Setting::where(['id' => $id ,'organization_id' => $user->organization_id])->first();
            }
        }

        return response()->json($response);

    }

    // set CustomForm of login user organization by id
    public function setCustomForm(Request $request)
    {
        $response = array();

        $error = \App\Http\Helpers::isValid(['value' => strip_tags($request->form_id)],['value'=>'required']);
        if($error)
            return response()->json($error);

        $user = \Auth::user();
        $setting = Setting::where(['id' =>$request->setting_id , 'organization_id'=> $user->organization_id])->first();

        if(!$setting){
            $setting = new Setting();
            $setting->id=strip_tags($request->setting_id);
            $setting->organization_id=$user->organization_id;
        }

        $setting->value=$request->form_id;
        if($setting->save())
        {
            $form =\Forms\Model\CustomForms::findOrFail($request->form_id);
            \Log\Model\Log::saveNewLog('CUSTOM_FORM_UPDATED', trans('setting::application.Assigned') . ' ' . $form->name  . ' ' .  trans('setting::application.As a custom form'));
            $response["status"]= 'success';
            $response["msg"]= trans('setting::application.The custom form is updated');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('setting::application.The custom form is not updated');
        }

        return response()->json($response);
    }

    // get setting value of login user organization ancestor
    protected function get($id)
    {
        $value=null;
        $user = \Auth::user();
        $ancestor_id = \Organization\Model\Organization::getAncestor($user->organization_id);

        if (Setting::where(['id' => $id ,'organization_id' => $ancestor_id])->exists()) {
            $Setting= Setting::where(['id' => $id ,'organization_id' => $ancestor_id])->first();
            $value=$Setting->value;
        }
        return $value;

    }

    // get setting value of request entities
    public function getSysSetting(Request $request)
    {
        $result = [];
        $entities = explode(',', $request->entities);
        foreach ($entities as $entity) {
            $result[$entity] = $this->get($entity);
        }
        return response()->json($result);
    }
}
