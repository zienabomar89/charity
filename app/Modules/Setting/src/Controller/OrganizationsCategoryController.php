<?php

namespace Setting\Controller;

use Setting\Model\OrganizationsCategory;

class OrganizationsCategoryController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        OrganizationsCategory::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize OrganizationsCategory model object to use in process
    protected function newModel()
    {
        return new OrganizationsCategory();
    }


    // get OrganizationsCategory model using id
    protected function findOrFail($id)
    {
        $entry = OrganizationsCategory::findOrFail($id);
        return $entry;
    }

    // get OrganizationsCategory model searching by name
    public function index()
    {

        $this->authorize('manage', OrganizationsCategory::class);
        $entries = OrganizationsCategory::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }


    // get all OrganizationsCategory model
    public function getList()
    {
        $entries = OrganizationsCategory::i18n()->get();
        return response()->json($entries);
    }

    // create new OrganizationsCategory model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', OrganizationsCategory::class);
        $this->validate($request, OrganizationsCategory::getValidatorRules());
        return parent::store($request);
    }
}