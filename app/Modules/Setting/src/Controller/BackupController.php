<?php

namespace Setting\Controller;

use App\Http\Controllers\Controller;
use Backup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Setting\Model\BackupVersions;

class BackupController extends Controller {


    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get BackupVersions rows searching filter
    public function filter(Request $request){
        return response()->json(BackupVersions::filter($request->all()) );
    }

    // create new BackupVersions model
    public function store() {

        $backupPath = base_path('storage/backup');
        if (!is_dir($backupPath)) {
            @mkdir($backupPath, 0777, true);
        }

        Backup::setPath('../storage/backup/');
        if(Backup::getPath()){
            $user = \Auth::user();
            $name = 'backup-' . date('Ymd-His');
            Backup::setFilename($name);
            Backup::export();
            BackupVersions::create(['name' => $name ,'user_id' => $user->id,'date'=>date('y-m-d')]);
//        dd(Backup::getProcessOutput());
            $response["status"] = true;
        }else{
            $response["status"] = false;
        }
        return response()->json($response);
    }

    // delete exist download file by id
    public function download($id){

        $entity = BackupVersions::findOrFail($id);
        if ($entity) {
            return response()->json(['download_token' => $entity->name]);
        }
        return response()->json(['status' => false, 'error' => 'Invalid file token']);
    }

    // delete exist BackupVersions model by id
    public function destroy($id,Request $request)
    {
        $entity = BackupVersions::findOrFail($id);
        $entityName = $entity->name;

        if (BackupVersions::destroy($id)) {
            unlink(storage_path('backup').'/'.$entityName .'.sql');
            \Log\Model\Log::saveNewLog('BACKUP_VERSION_DELETED',trans('common::application.delete buackup') );
            return response()->json(['status' => 'success' , 'msg' => trans('common::application.action success')]);
        }

        return response()->json(['status' =>'failed','msg'=>trans('common::application.An error occurred during during process') ]);
    }

}
