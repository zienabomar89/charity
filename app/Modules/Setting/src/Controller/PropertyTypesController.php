<?php

namespace Setting\Controller;

use Setting\Model\PropertyType;

class PropertyTypesController extends AbstractController
{
    public function __construct()
    {
        $language_id = 1;
        PropertyType::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize PropertyType model object to use in process
    protected function newModel()
    {
        return new PropertyType();
    }


    // get PropertyTypes model using id
    protected function findOrFail($id)
    {
        $entry = PropertyType::findOrFail($id);
        return $entry;
    }

    // get PropertyTypes model searching by name
    public function index()
    {
        $this->authorize('manage', PropertyType::class);
        $entries = PropertyType::i18n();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }


    // get all PropertyTypes model
    public function getList()
    {
        $entries = PropertyType::i18n()->get();
        return response()->json($entries);
    }

    // create new PropertyTypes model
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', PropertyType::class);
        $this->validate($request, PropertyType::getValidatorRules());
        return parent::store($request);
    }
    
    
}
