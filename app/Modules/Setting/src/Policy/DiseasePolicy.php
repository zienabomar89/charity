<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\Disease;

class DiseasePolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.diseases.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.diseases.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Disease $disease  = null)
    {
        if ($user->hasPermission('setting.diseases.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Disease $disease  = null)
    {
        if ($user->hasPermission('setting.diseases.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Disease $disease  = null)
    {
        if ($user->hasPermission(['setting.diseases.view','setting.diseases.update'])) {
            return true;
        }

        return false;
    }

}

