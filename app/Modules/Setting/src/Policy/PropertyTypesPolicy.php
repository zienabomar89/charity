<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\Propertytypes;

class PropertyTypesPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.PropertyTypes.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.PropertyTypes.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Propertytypes $propertytypes = null)
    {
        if ($user->hasPermission('setting.PropertyTypes.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Propertytypes $propertytypes = null)
    {
        if ($user->hasPermission('setting.PropertyTypes.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Propertytypes $propertytypes = null)
    {
        if ($user->hasPermission(['setting.PropertyTypes.view','setting.PropertyTypes.update'])) {
            return true;
        }

        return false;
    }

}

