<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\BuildingStatus;

class BuildingStatusPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.buildingStatus.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.buildingStatus.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, BuildingStatus $status = null)
    {
        if ($user->hasPermission('setting.buildingStatus.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, BuildingStatus $status = null)
    {
        if ($user->hasPermission('setting.buildingStatus.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, BuildingStatus $status = null)
    {
        if ($user->hasPermission(['setting.buildingStatus.view','setting.buildingStatus.update'])) {
            return true;
        }

        return false;
    }

}

