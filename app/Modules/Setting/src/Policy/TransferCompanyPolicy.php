<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\TransferCompany;

class TransferCompanyPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.transferCompany.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.transferCompany.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, TransferCompany $transferCompany = null)
    {
        if ($user->hasPermission('setting.transferCompany.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, TransferCompany $transferCompany = null)
    {
        if ($user->hasPermission('setting.transferCompany.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, TransferCompany $transferCompany = null)
    {
        if ($user->hasPermission(['setting.transferCompany.view','setting.transferCompany.update'])) {
            return true;
        }

        return false;
    }

}

