<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\EduDegrees;

class EduDegreesPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.eduDegrees.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.eduDegrees.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, EduDegrees $edudegrees = null)
    {
        if ($user->hasPermission('setting.eduDegrees.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, EduDegrees $edudegrees = null)
    {
        if ($user->hasPermission('setting.eduDegrees.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, EduDegrees $edudegrees = null)
    {
        if ($user->hasPermission(['setting.eduDegrees.view','setting.eduDegrees.update'])) {
            return true;
        }

        return false;
    }

}

