<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\HabitableStatus;

class HabitableStatusPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.habitableStatus.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.habitableStatus.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, HabitableStatus $status = null)
    {
        if ($user->hasPermission('setting.habitableStatus.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, HabitableStatus $status = null)
    {
        if ($user->hasPermission('setting.habitableStatus.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, HabitableStatus $status = null)
    {
        if ($user->hasPermission(['setting.habitableStatus.view','setting.habitableStatus.update'])) {
            return true;
        }

        return false;
    }

}

