<?php
namespace Setting\Policy;


use Auth\Model\User;
use Setting\Model\DeathCause;

class DeathCausePolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.deathCauses.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.deathCauses.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, DeathCause $deathCauses  = null)
    {
        if ($user->hasPermission('setting.deathCauses.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, DeathCause $deathCauses  = null)
    {
        if ($user->hasPermission('setting.deathCauses.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, DeathCause $deathCauses  = null)
    {
        if ($user->hasPermission(['setting.deathCauses.view','setting.deathCauses.update'])) {
            return true;
        }

        return false;
    }

}

