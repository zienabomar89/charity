<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\Kinship;

class KinshipPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.kinship.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.kinship.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Kinship $kinship = null)
    {
        if ($user->hasPermission('setting.kinship.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Kinship $kinship = null)
    {
        if ($user->hasPermission('setting.kinship.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Kinship $kinship = null)
    {
        if ($user->hasPermission(['setting.kinship.view','setting.kinship.update'])) {
            return true;
        }

        return false;
    }

}

