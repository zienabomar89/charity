<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\Education\Authority;

class EduAuthorityPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.eduAuthorities.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.eduAuthorities.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Authority $eduauthorities = null)
    {
        if ($user->hasPermission('setting.eduAuthorities.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Authority $eduauthorities = null)
    {
        if ($user->hasPermission('setting.eduAuthorities.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Authority $eduauthorities = null)
    {
        if ($user->hasPermission(['setting.eduAuthorities.view','setting.eduAuthorities.update'])) {
            return true;
        }

        return false;
    }

}

