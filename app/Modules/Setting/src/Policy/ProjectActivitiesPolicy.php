<?php
namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\ProjectActivities;

class ProjectActivitiesPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.ProjectActivities.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.ProjectActivities.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, ProjectActivities $ProjectActivities = null)
    {
        if ($user->hasPermission('setting.ProjectActivities.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, ProjectActivities $ProjectActivities = null)
    {
        if ($user->hasPermission('setting.ProjectActivities.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, ProjectActivities $ProjectActivities = null)
    {
        if ($user->hasPermission(['setting.ProjectActivities.view','setting.ProjectActivities.update'])) {
            return true;
        }

        return false;
    }

}
