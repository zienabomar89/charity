<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\Currency;


class CurrencyPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.currency.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.currency.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Currency $currency = null)
    {
        if ($user->hasPermission('setting.currency.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Currency $currency = null)
    {
        if ($user->hasPermission('setting.currency.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Currency $currency = null)
    {
        if ($user->hasPermission(['setting.currency.view','setting.currency.update'])) {
            return true;
        }

        return false;
    }

}

