<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\DocumentType;

class DocumentTypePolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.documentTypes.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.documentTypes.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, DocumentType $documenttypes = null)
    {
        if ($user->hasPermission('setting.documentTypes.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, DocumentType $documenttypes = null)
    {
        if ($user->hasPermission('setting.documentTypes.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, DocumentType $documenttypes = null)
    {
        if ($user->hasPermission(['setting.documentTypes.view','setting.documentTypes.update'])) {
            return true;
        }

        return false;
    }

}

