<?php
namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\Language;

class LanguagePolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.language.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.language.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Language $language = null)
    {
        if ($user->hasPermission('setting.language.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Language $language = null)
    {
        if ($user->hasPermission('setting.language.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Language $language = null)
    {
        if ($user->hasPermission(['setting.language.view','setting.language.update'])) {
            return true;
        }

        return false;
    }

}
