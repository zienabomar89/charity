<?php
namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\ProjectCategory;

class ProjectCategoryPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.ProjectCategory.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.ProjectCategory.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, ProjectCategory $projectCategory  = null)
    {
        if ($user->hasPermission('setting.ProjectCategory.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, ProjectCategory $projectCategory = null)
    {
        if ($user->hasPermission('setting.ProjectCategory.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, ProjectCategory $projectCategory = null)
    {
        if ($user->hasPermission(['setting.ProjectCategory.view','setting.ProjectCategory.update'])) {
            return true;
        }

        return false;
    }

}
