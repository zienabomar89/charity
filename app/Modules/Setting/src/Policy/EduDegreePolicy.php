<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\Education\Degree;

class EduDegreePolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.eduDegrees.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.eduDegrees.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Degree $edudegrees = null)
    {
        if ($user->hasPermission('setting.eduDegrees.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Degree $edudegrees = null)
    {
        if ($user->hasPermission('setting.eduDegrees.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Degree $edudegrees = null)
    {
        if ($user->hasPermission(['setting.eduDegrees.view','setting.eduDegrees.update'])) {
            return true;
        }

        return false;
    }

}

