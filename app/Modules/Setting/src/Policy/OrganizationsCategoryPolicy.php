<?php
namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\OrganizationsCategory;

class OrganizationsCategoryPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.OrganizationsCategory.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.OrganizationsCategory.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, OrganizationsCategory $OrganizationsCategory = null)
    {
        if ($user->hasPermission('setting.OrganizationsCategory.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, OrganizationsCategory $OrganizationsCategory = null)
    {
        if ($user->hasPermission('setting.OrganizationsCategory.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, OrganizationsCategory $OrganizationsCategory = null)
    {
        if ($user->hasPermission(['setting.OrganizationsCategory.view','setting.OrganizationsCategory.update'])) {
            return true;
        }

        return false;
    }

}
