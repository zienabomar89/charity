<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\Work\Job;

class WorkJobPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.workJob.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.workJob.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Job $workjob = null)
    {
        if ($user->hasPermission('setting.workJob.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Job $workjob = null)
    {
        if ($user->hasPermission('setting.workJob.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Job $workjob = null)
    {
        if ($user->hasPermission(['setting.workJob.view','setting.workJob.update'])) {
            return true;
        }

        return false;
    }

}

