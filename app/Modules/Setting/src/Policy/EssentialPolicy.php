<?php
namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\Essential;

class EssentialPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.essentials.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.essentials.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Essential $essentials = null)
    {
        if ($user->hasPermission('setting.essentials.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Essential $essentials = null)
    {
        if ($user->hasPermission('setting.essentials.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Essential $essentials = null)
    {
        if ($user->hasPermission(['setting.essentials.view','setting.essentials.update'])) {
            return true;
        }

        return false;
    }

}

