<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\RoofMaterial;

class RoofMaterialPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.roofMaterials.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.roofMaterials.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, RoofMaterial $roofmaterials= null)
    {
        if ($user->hasPermission('setting.roofMaterials.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, RoofMaterial $roofmaterials= null)
    {
        if ($user->hasPermission('setting.roofMaterials.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, RoofMaterial $roofmaterials= null)
    {
        if ($user->hasPermission(['setting.roofMaterials.view','setting.roofMaterials.update'])) {
            return true;
        }

        return false;
    }

}

