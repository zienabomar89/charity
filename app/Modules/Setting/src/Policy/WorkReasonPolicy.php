<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\Work\Reason;

class WorkReasonPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.workReason.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.workReason.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Reason $workreason = null)
    {
        if ($user->hasPermission('setting.workReason.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Reason $workreason = null)
    {
        if ($user->hasPermission('setting.workReason.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Reason $workreason = null)
    {
        if ($user->hasPermission(['setting.workReason.view','setting.workReason.update'])) {
            return true;
        }

        return false;
    }

}

