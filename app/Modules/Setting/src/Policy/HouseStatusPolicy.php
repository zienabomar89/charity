<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\HouseStatus;

class HouseStatusPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.houseStatus.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.houseStatus.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, HouseStatus $status = null)
    {
        if ($user->hasPermission('setting.houseStatus.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, HouseStatus $status = null)
    {
        if ($user->hasPermission('setting.houseStatus.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, HouseStatus $status = null)
    {
        if ($user->hasPermission(['setting.houseStatus.view','setting.houseStatus.update'])) {
            return true;
        }

        return false;
    }

}

