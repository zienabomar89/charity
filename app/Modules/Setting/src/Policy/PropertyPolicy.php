<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\Property;

class PropertyPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.properties.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.properties.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Property $properties = null)
    {
        if ($user->hasPermission('setting.properties.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Property $properties = null)
    {
        if ($user->hasPermission('setting.properties.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Property $properties = null)
    {
        if ($user->hasPermission(['setting.properties.view','setting.properties.update'])) {
            return true;
        }

        return false;
    }

}
