<?php
namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\ProjectType;

class ProjectTypePolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.ProjectType.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.ProjectType.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, ProjectType $projectType = null)
    {
        if ($user->hasPermission('setting.ProjectType.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, ProjectType $projectType = null)
    {
        if ($user->hasPermission('setting.ProjectType.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, ProjectType $projectType = null)
    {
        if ($user->hasPermission(['setting.ProjectType.view','setting.ProjectType.update'])) {
            return true;
        }

        return false;
    }

}
