<?php
namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\PaymentCategory;

class PaymentCategoryPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.paymentCategory.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.paymentCategory.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, PaymentCategory $paymentCategory = null)
    {
        if ($user->hasPermission('setting.paymentCategory.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, PaymentCategory $paymentCategory = null)
    {
        if ($user->hasPermission('setting.paymentCategory.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, PaymentCategory $paymentCategory = null)
    {
        if ($user->hasPermission(['setting.paymentCategory.view','setting.paymentCategory.update'])) {
            return true;
        }

        return false;
    }

}
