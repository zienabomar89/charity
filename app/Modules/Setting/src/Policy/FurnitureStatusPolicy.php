<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\FurnitureStatus;

class FurnitureStatusPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.furnitureStatus.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.furnitureStatus.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, FurnitureStatus $status = null)
    {
        if ($user->hasPermission('setting.furnitureStatus.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, FurnitureStatus $status = null)
    {
        if ($user->hasPermission('setting.furnitureStatus.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, FurnitureStatus $status = null)
    {
        if ($user->hasPermission(['setting.furnitureStatus.view','setting.furnitureStatus.update'])) {
            return true;
        }

        return false;
    }

}

