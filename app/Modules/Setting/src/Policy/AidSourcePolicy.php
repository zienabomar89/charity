<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\AidSource;

class AidSourcePolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.aidSources.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.aidSources.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, AidSource $aidSources = null)
    {
        if ($user->hasPermission('setting.aidSources.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, AidSource $aidSources = null)
    {
        if ($user->hasPermission('setting.aidSources.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, AidSource $aidSources = null)
    {
        if ($user->hasPermission(['setting.aidSources.view','setting.aidSources.update'])) {
            return true;
        }

        return false;
    }

}

