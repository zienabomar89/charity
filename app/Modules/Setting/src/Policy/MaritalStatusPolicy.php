<?php
namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\MaritalStatus;

class MaritalStatusPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.maritalStatus.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.maritalStatus.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, MaritalStatus $maritalStatus = null)
    {
        if ($user->hasPermission('setting.maritalStatus.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, MaritalStatus $maritalStatus = null)
    {
        if ($user->hasPermission('setting.maritalStatus.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, MaritalStatus $maritalStatus = null)
    {
        if ($user->hasPermission(['setting.maritalStatus.view','setting.maritalStatus.update'])) {
            return true;
        }

        return false;
    }

}
