<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\Work\Status;

class WorkStatusPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.workStatus.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.workStatus.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Status $workstatus = null)
    {
        if ($user->hasPermission('setting.workStatus.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Status $workstatus = null)
    {
        if ($user->hasPermission('setting.workStatus.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Status $workstatus = null)
    {
        if ($user->hasPermission(['setting.workStatus.view','setting.workStatus.update'])) {
            return true;
        }

        return false;
    }

}

