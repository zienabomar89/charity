<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\Bank;


class BankPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.banks.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.banks.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Bank $banks = null)
    {
        if ($user->hasPermission('setting.banks.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Bank $banks = null)
    {
        if ($user->hasPermission('setting.banks.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Bank $banks = null)
    {
        if ($user->hasPermission(['setting.banks.view','setting.banks.update'])) {
            return true;
        }

        return false;
    }

}
