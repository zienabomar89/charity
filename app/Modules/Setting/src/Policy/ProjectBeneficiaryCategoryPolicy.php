<?php
namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\ProjectBeneficiaryCategory;

class ProjectBeneficiaryCategoryPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.ProjectBeneficiaryCategory.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.ProjectBeneficiaryCategory.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, ProjectBeneficiaryCategory $ProjectBeneficiaryCategory = null)
    {
        if ($user->hasPermission('setting.ProjectBeneficiaryCategory.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, ProjectBeneficiaryCategory $ProjectBeneficiaryCategory = null)
    {
        if ($user->hasPermission('setting.ProjectBeneficiaryCategory.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, ProjectBeneficiaryCategory $ProjectBeneficiaryCategory = null)
    {
        if ($user->hasPermission(['setting.ProjectBeneficiaryCategory.view','setting.ProjectBeneficiaryCategory.update'])) {
            return true;
        }

        return false;
    }

}
