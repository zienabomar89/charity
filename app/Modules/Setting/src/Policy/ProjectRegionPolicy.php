<?php
namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\ProjectRegion;

class ProjectRegionPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.ProjectRegion.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.ProjectRegion.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, ProjectRegion $ProjectRegion = null)
    {
        if ($user->hasPermission('setting.ProjectRegion.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, ProjectRegion $ProjectRegion = null)
    {
        if ($user->hasPermission('setting.ProjectRegion.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, ProjectRegion $ProjectRegion = null)
    {
        if ($user->hasPermission(['setting.ProjectRegion.view','setting.ProjectRegion.update'])) {
            return true;
        }

        return false;
    }

}
