<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\Education\Stage;

class EduStagePolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.eduStages.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.eduStages.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Stage $edustages = null)
    {
        if ($user->hasPermission('setting.eduStages.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Stage $edustages = null)
    {
        if ($user->hasPermission('setting.eduStages.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Stage $edustages = null)
    {
        if ($user->hasPermission(['setting.eduStages.view','setting.eduStages.update'])) {
            return true;
        }

        return false;
    }

}

