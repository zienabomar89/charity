<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\Categories;

class CategoriesPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.categories.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.categories.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Categories $categories = null)
    {
        if ($user->hasPermission('setting.categories.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Categories $categories = null)
    {
        if ($user->hasPermission('setting.categories.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Categories $categories = null)
    {
        if ($user->hasPermission(['setting.categories.view','setting.categories.update'])) {
            return true;
        }

        return false;
    }

}

