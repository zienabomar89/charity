<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\Setting;

class SettingPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.setting.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.setting.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Setting $setting = null)
    {
        if ($user->hasPermission('setting.setting.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Setting $setting = null)
    {
        if ($user->hasPermission('setting.setting.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Setting $setting = null)
    {
        if ($user->hasPermission(['setting.setting.view','setting.setting.update'])) {
            return true;
        }

        return false;
    }

}

