<?php
namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\Location;

class LocationPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.locations.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.locations.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Location $location = null)
    {
        if ($user->hasPermission('setting.locations.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Location $location = null)
    {
        if ($user->hasPermission('setting.locations.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Location $location = null)
    {
        if ($user->hasPermission(['setting.locations.view', 'setting.locations.update'])) {
            return true;
        }

        return false;
    }

}
