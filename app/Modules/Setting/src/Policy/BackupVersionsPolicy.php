<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\BackupVersions;

class BackupVersionsPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('auth.BackupVersions.manage')) {
            return true;
        }

        return false;
    }
    public function manageCitizenRequest(User $user)
    {
        if ($user->hasPermission('reports.CitizenRequest.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('auth.BackupVersions.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, BackupVersions $BackupVersions = null)
    {
        if ($user->hasPermission('auth.BackupVersions.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, BackupVersions $BackupVersions = null)
    {
        if ($user->hasPermission('auth.BackupVersions.delete')) {
            return true;
        }

        return false;
    }

    public function download(User $user, BackupVersions $BackupVersions = null)
    {
        if ($user->hasPermission('auth.BackupVersions.download')) {
            return true;
        }

        return false;
    }

    public function view(User $user, BackupVersions $BackupVersions = null)
    {
        if ($user->hasPermission(['auth.BackupVersions.view','setting.BackupVersions.update'])) {
            return true;
        }

        return false;
    }

}

