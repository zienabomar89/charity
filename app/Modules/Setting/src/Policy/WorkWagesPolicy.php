<?php

namespace Setting\Policy;

use Auth\Model\User;
use Setting\Model\WorkWages;

class WorkWagesPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('setting.workWages.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('setting.workWages.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, WorkWages $workwages = null)
    {
        if ($user->hasPermission('setting.workWages.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, WorkWages $workwages = null)
    {
        if ($user->hasPermission('setting.workWages.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, WorkWages $workwages = null)
    {
        if ($user->hasPermission(['setting.workWages.view','setting.workWages.update'])) {
            return true;
        }

        return false;
    }

}

