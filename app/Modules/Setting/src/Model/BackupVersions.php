<?php
namespace Setting\Model;
use App\Http\Helpers;


class BackupVersions  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_backup_versions';
    protected $fillable = ['name','date','user_id'];
    protected $hidden = ['created_at','updated_at'];
//    public $timestamps = false;



    public function user()
    {
        return $this->belongsTo('Auth\Model\User', 'user_id', 'id');
    }
    public static function filter($options)
    {
        $query = self::with('user');
        $query->orderBy('char_backup_versions.created_at','desc');
        $itemsCount = isset($options['itemsCount'])?$options['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
        return $query->paginate($records_per_page);
    }
}
