<?php

namespace Setting\Model;

use Illuminate\Database\Eloquent\Model;

class AidSource extends Model
{
    protected $table = 'char_aid_sources';
    
    public $timestamps = false;
    
    protected static $rules;
    
    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'name' => 'required|max:255',
            );
        }
        
        return self::$rules;
    }
}

