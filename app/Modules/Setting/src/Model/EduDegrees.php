<?php

namespace Setting\Model;
class EduDegrees extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_edu_degrees';
    protected $fillable = ['id','language_id', 'name'];
    public $timestamps = false;

    public function languages()
    {
        return $this->belongsTo('Setting\Model\Language','language_id','id');
    }

    public function personsEducation()
    {
        return $this->hasMany('Sponsorship\Model\PersonsEducation','degree_id','id');
    }

    Public function get_translate_list($id){
        return \DB::table('char_edu_degrees')
            ->join('char_languages AS L', 'L.id', '=', 'char_edu_degrees.language_id')
            ->where('char_edu_degrees.id', '=', $id)
            ->select(
                'char_edu_degrees.id as id',
                'char_edu_degrees.name as name',
                'L.name as language_name',
                'L.id as language_id'

            )
            ->get();
    }

}
