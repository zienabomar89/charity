<?php

namespace Setting\Model;

use Illuminate\Database\Eloquent\Model;

class BankChequeTemplate extends Model
{
    protected $table = 'char_banks_cheque_template';
    protected $fillable = ['bank_id' , 'height' , 'width' , 'nameMT' , 'nameMR' , 'dateMT' , 'dateMR' , 'vMT' , 'vMR' , 'vaMT' , 'vaMR' , 'noMT' , 'noMR' ];
    public $timestamps = false;
    public static $rules;

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'bank_id' =>'required',
                'height' =>'required',
                'width' =>'required',
                'nameMT' =>'required',
                'nameMR' =>'required',
                'dateMT' =>'required',
                'dateMR' =>'required',
                'vMT' =>'required',
                'vMR' =>'required',
                'vaMT' =>'required',
                'vaMR' =>'required',
                'noMT' =>'required',
                'noMR' =>'required'
            );
        }

        return self::$rules;
    }

    public static function filter(){
        return
            \DB::table('char_banks')
                ->leftjoin('char_banks_cheque_template', 'char_banks_cheque_template.bank_id', '=', 'char_banks.id')
                ->orderBy('char_banks.name')
                ->selectRaw("char_banks.*,char_banks_cheque_template.*")
                ->whereNotIn('char_banks.id',[1,2,3])
                ->get();

    }
}

