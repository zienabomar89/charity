<?php

namespace Setting\Model;
class Locations extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_locations';
    protected $fillable = ['location_type', 'id' , 'parent_id'];
    protected $hidden = ['created_at','updated_at'];

    public function locations_i18n()
    {
        return $this->hasOne('Setting\Model\LocationsI18n','location_id', 'id');
    }

    public function person()
    {
        return $this->hasMany('Common\Model\Person','location_id','id');
    }

    public function getCountry()
    {
        $rows = \DB::table('char_locations')
            ->join('char_locations_i18n', 'char_locations_i18n.location_id', '=', 'char_locations.id')
            ->where('char_locations.location_type','=',0)
            ->where('char_locations_i18n.language_id','=',1)
            ->orderBy('char_locations_i18n.name')
            ->select('char_locations.id as id', 'char_locations_i18n.name as name')
            ->get();
        return $rows;
    }

    public function getLocations($parant,$type)
    {

        $rows = \DB::table('char_locations')
                ->join('char_locations_i18n', 'char_locations_i18n.location_id', '=', 'char_locations.id')
                ->where('char_locations.location_type','=',$type)
                ->where('char_locations_i18n.language_id','=',1)
                ->orderBy('char_locations_i18n.name');

        if($parant != -1){
            $rows =$rows->where('char_locations.parent_id','=',$parant);
        }

        $rows= $rows->select('char_locations.id as id', 'char_locations_i18n.name as name')->get();

        return $rows;
    }

    public function getTablelist($page)
    {
        $rows = \DB::table('char_locations')
            ->join('char_locations_i18n', 'char_locations_i18n.location_id', '=', 'char_locations.id')
            ->where('char_locations_i18n.language_id','=',1)
            ->orderBy('char_locations_i18n.name')
            ->select(
                'char_locations.id as id',
                'char_locations_i18n.name as name',
                'char_locations_i18n.language_id as language_id',
                \DB::raw('(CASE WHEN char_locations.location_type = 0 THEN "دولة"
                               WHEN char_locations.location_type = 1 THEN "محافظة"
                               WHEN char_locations.location_type = 2 THEN "مدينة"
                               WHEN char_locations.location_type = 3 THEN "منطقة"
                          END) AS location_type')
            )
            ->paginate(config('constants.records_per_page'));
        return $rows;
    }

    Public function get_translate_list($id){
        return \DB::table('char_locations_i18n')
            ->join('char_languages AS L', 'L.id', '=', 'char_locations_i18n.language_id')
            ->where('char_locations_i18n.location_id', '=', $id)
            ->orderBy('char_locations_i18n.name')
            ->select(
                'char_locations_i18n.location_id as id',
                'char_locations_i18n.name as name',
                'L.name as language_name',
                'L.id as language_id'

            )
            ->get();
    }

}
