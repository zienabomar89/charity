<?php

namespace Setting\Model;

use Application\Model\I18nEntity;

class ProjectTypeI18n extends I18nEntity
{
    protected $table = 'char_project_type_i18n';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type_id', 'language_id', 'name'];

    public function getEntityIdColumn()
    {
        return 'type_id';
    }
}

