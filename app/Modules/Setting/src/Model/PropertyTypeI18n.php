<?php

namespace Setting\Model;

use Application\Model\I18nEntity;

class PropertyTypeI18n extends I18nEntity
{
    protected $table = 'char_property_types_i18n';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['property_type_id', 'language_id', 'name'];
    
    public function getEntityIdColumn()
    {
        return 'property_type_id';
    }
}
