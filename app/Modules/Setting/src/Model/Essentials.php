<?php
namespace Setting\Model;
class Essentials extends  \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_essentials';
    protected $fillable = ['name'];
    protected $hidden = ['created_at','updated_at'];
    public $timestamps = false;
}
