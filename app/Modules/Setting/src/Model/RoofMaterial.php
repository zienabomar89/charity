<?php

namespace Setting\Model;

use Application\Model\I18nableEntity;
use Setting\Model\RankEntity;

class RoofMaterial extends I18nableEntity implements RankEntity
{
    protected $table = 'char_roof_materials';
    
    public $timestamps = false;
    
    protected static $rules;
    
    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'name' => 'required|max:255',
            );
        }
        
        return self::$rules;
    }
    
    /**
     * 
     * @return I18nInterface
     */
    public function getI18nModel()
    {
        if ($this->i18nModel === null) {
            $this->i18nModel = new RoofMaterialI18n();
            $this->i18nModel->forceFill(array(
                $this->i18nModel->getEntityIdColumn() => $this->id,
                $this->i18nModel->getLanguageIdColumn() => $this->getLanguageId(),
            ));
        }
        
        return $this->i18nModel;
    }
    
    public function updateLinkedRank(RankEntity $old)
    {
        if ($old->weight == $this->weight) {
            return true;
        }
        
        $query = 'UPDATE `char_cases` SET
            `rank` = `rank` - :old + :new
            WHERE `person_id` in (
                SELECT `person_id` FROM `char_residence` WHERE `roof_material_id` = :id)';
        
        return $this->getConnection()->update($query, array(
            'old' => $old->weight,
            'new' => $this->weight,
            'id' => $this->id,
        ));
    }

}

