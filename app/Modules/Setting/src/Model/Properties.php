<?php

namespace Setting\Model;

class Properties extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_properties';
    protected $fillable = ['language_id', 'name','id'];
    public $timestamps = false;

    public function languages()
    {
        return $this->belongsTo('Setting\Model\Language','language_id','id');
    }

    Public function get_translate_list($id){
        return \DB::table('char_properties')
            ->join('char_languages AS L', 'L.id', '=', 'char_properties.language_id')
            ->where('char_properties.id', '=', $id)
            ->select(
                'char_properties.id as id',
                'char_properties.name as name',
                'L.name as language_name',
                'L.id as language_id'

            )
            ->get();
    }
}
