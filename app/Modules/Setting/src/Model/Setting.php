<?php

namespace Setting\Model;

class Setting  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_settings';
    protected $fillable = ['id','organization_id','value','user_id'];
    public $timestamps = false;
    protected $keyType = 'string';
    public $incrementing = false;

}
