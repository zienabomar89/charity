<?php

namespace Setting\Model\Work;

use Application\Model\I18nEntity;

class StatusI18n extends I18nEntity
{
    protected $table = 'char_work_status_i18n';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['work_status_id', 'language_id', 'name'];
    
    public function getEntityIdColumn()
    {
        return 'work_status_id';
    }
}

