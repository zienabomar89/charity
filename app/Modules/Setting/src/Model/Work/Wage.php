<?php

namespace Setting\Model\Work;

use Illuminate\Database\Eloquent\Model;
use Setting\Model\RankEntity;

class Wage extends Model implements RankEntity
{
    protected $table = 'char_work_wages';
    
    public $timestamps = false;
    
    protected static $rules;
    
    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'name' => 'required|max:255',
            );
        }
        
        return self::$rules;
    }
    
    public function updateLinkedRank(RankEntity $old)
    {
        if ($old->weight == $this->weight) {
            return true;
        }
        
        $query = 'UPDATE `char_cases` SET
            `rank` = `rank` - :old + :new
            WHERE `person_id` in (
                SELECT `person_id` FROM `char_persons_work` WHERE `work_wage_id` = :id)';
        
        return $this->getConnection()->update($query, array(
            'old' => $old->weight,
            'new' => $this->weight,
            'id' => $this->id,
        ));
    }
}

