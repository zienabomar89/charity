<?php

namespace Setting\Model\Work;

use Application\Model\I18nEntity;

class JobI18n extends I18nEntity
{
    protected $table = 'char_work_jobs_i18n';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['work_job_id', 'language_id', 'name'];
    
    public function getEntityIdColumn()
    {
        return 'work_job_id';
    }
}

