<?php

namespace Setting\Model\Work;

use Application\Model\I18nEntity;

class ReasonI18n extends I18nEntity
{
    protected $table = 'char_work_reasons_i18n';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['work_reason_id', 'language_id', 'name'];
    
    public function getEntityIdColumn()
    {
        return 'work_reason_id';
    }
}

