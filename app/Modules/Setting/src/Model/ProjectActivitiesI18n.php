<?php

namespace Setting\Model;

use Application\Model\I18nEntity;

class ProjectActivitiesI18n extends I18nEntity
{
    protected $table = 'char_project_activities_category_i18n';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['activity_category_id', 'language_id', 'name'];

    public function getEntityIdColumn()
    {
        return 'activity_category_id';
    }
}

