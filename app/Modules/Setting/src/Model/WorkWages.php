<?php

namespace Setting\Model;

class WorkWages  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_work_wages';
    protected $fillable = ['weight','name'];
    public $timestamps = false;



    public function Person()
    {
        return $this->belongsToMany('Common\Model\Person', 'char_persons_work', 'work_wage_id', 'person_id');
    }

}
