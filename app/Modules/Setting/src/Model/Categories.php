<?php

namespace Setting\Model;

class Categories extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_categories';
    protected $fillable = ['language_id', 'name','type'];
    public $timestamps = false;

    public function languages()
    {
        return $this->belongsTo('Setting\Model\Language','language_id','id');
    }

}
