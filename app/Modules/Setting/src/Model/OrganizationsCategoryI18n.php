<?php

namespace Setting\Model;

use Application\Model\I18nEntity;

class OrganizationsCategoryI18n extends I18nEntity
{
    protected $table = 'char_organizations_category_i18n';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['category_id', 'language_id', 'name'];

    public function getEntityIdColumn()
    {
        return 'category_id';
    }
}

