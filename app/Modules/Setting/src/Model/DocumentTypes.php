<?php

namespace Setting\Model;

class DocumentTypes extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_document_types';
    protected $fillable = ['expiry', 'name'];
    protected $hidden = ['created_at','updated_at'];
    public $timestamps = false;

    public function char_document_types_i18n()
    {
        return $this->hasOne('Setting\Model\DocumentI18n','document_type_id', 'id');
    }

    public function getTablelist($page)
    {
        $rows = \DB::table('char_document_types')
            ->join('char_document_types_i18n', 'char_document_types_i18n.document_type_id', '=', 'char_document_types.id')
            ->where('char_document_types_i18n.language_id','=',1)
            ->orderBy('char_document_types_i18n.name')
            ->select(
                'char_document_types.id as id',
                'char_document_types.expiry as expiry',
                'char_document_types_i18n.name as name',
                'char_document_types_i18n.language_id as language_id'
            )
            ->paginate(config('constants.records_per_page'));
        return $rows;
    }

    Public function get_translate_list($id){
        return \DB::table('char_document_types_i18n')
            ->join('char_languages AS L', 'L.id', '=', 'char_document_types_i18n.language_id')
            ->where('char_document_types_i18n.document_type_id', '=', $id)
            ->select(
                'char_document_types_i18n.document_type_id as id',
                'char_document_types_i18n.name as name',
                'L.name as language_name',
                'L.id as language_id'

            )
            ->get();
    }



}
