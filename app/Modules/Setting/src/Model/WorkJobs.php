<?php

namespace Setting\Model;

class WorkJobs  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_work_jobs';
    protected $fillable = ['id','language_id', 'name'];
    public $timestamps = false;

    public function languages()
    {
        return $this->belongsTo('Setting\Model\Language','language_id','id');
    }

    public function Person()
    {
        return $this->belongsToMany('Common\Model\Person', 'char_persons_work', 'work_job_id', 'person_id');

    }

    Public function get_translate_list($id){
        return \DB::table('char_work_jobs')
            ->join('char_languages AS L', 'L.id', '=', 'char_work_jobs.language_id')
            ->where('char_work_jobs.id', '=', $id)
            ->select(
                'char_work_jobs.id as id',
                'char_work_jobs.name as name',
                'L.name as language_name',
                'L.id as language_id'
            )
            ->get();
    }

}
