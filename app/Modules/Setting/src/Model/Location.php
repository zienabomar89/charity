<?php

namespace Setting\Model;

class Location extends \Application\Model\I18nableEntity
{
    const TYPE_COUNTRY      = 0;
    const TYPE_DISTRICT     = 1;
    const TYPE_CITY         = 2;
    const TYPE_NEIGHBORHOOD = 3;
    const TYPE_MOSQUES      = 4;

    protected $table = 'char_locations';
    
    protected static $rules;
    
    protected $appends = ['type_name', 'type_options'];

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'parent_id' => 'required|int',
                //'location_type' => 'required|int',
                'name' => 'required|max:255',
            );
        }
        
        return self::$rules;
    }
    
    /**
     * Scope a query to only include a location descendants.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDescendants($query, $id = null)
    {
        if ($id === null) {
            return $query;
        }
        return $query->where('parent_id', '=', $id);
    }
    
    /**
     * Scope a query to only include countries.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCountries($query)
    {
        return $query->where('location_type', '=', self::TYPE_COUNTRY);
    }
    
    /**
     * Scope a query to only include districts.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDistricts($query, $parent_id = null)
    {
        $query->where('location_type', '=', self::TYPE_DISTRICT);
        if (null !== $parent_id) {
            $query->where('parent_id', '=', $parent_id);
        }
        return $query;
    }
    
    /**
     * Scope a query to only include cities.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCities($query, $parent_id = null)
    {
        $query->where('location_type', '=', self::TYPE_CITY);
        if (null !== $parent_id) {
            $query->where('parent_id', '=', $parent_id);
        }
        return $query;
    }
    
    /**
     * Scope a query to only include neighborhoods.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNeighborhoods($query, $parent_id = null)
    {
        $query->where('location_type', '=', self::TYPE_NEIGHBORHOOD);
        if (null !== $parent_id) {
            $query->where('parent_id', '=', $parent_id);
        }
        return $query;
    }

/**
     * Scope a query to only include mosques.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMosques($query, $parent_id = null)
    {
        $query->where('location_type', '=', self::TYPE_MOSQUES);
        if (null !== $parent_id) {
            $query->where('parent_id', '=', $parent_id);
        }
        return $query;
    }

    /**
     * 
     * @return I18nInterface
     */
    public function getI18nModel()
    {
        if ($this->i18nModel === null) {
            $this->i18nModel = new LocationI18n();
            $this->i18nModel->forceFill(array(
                $this->i18nModel->getEntityIdColumn() => $this->id,
                $this->i18nModel->getLanguageIdColumn() => $this->getLanguageId(),
            ));
        }
        
        return $this->i18nModel;
    }
    
    public function setAttributesFromRequest(\Illuminate\Http\Request $request)
    {
        $this->parent_id = $request->input('parent_id');
        return $this;
    }
    
    public static function type($value = null)
    {
        $options = array(
            self::TYPE_COUNTRY => trans('setting::application.state'),
            self::TYPE_DISTRICT => trans('setting::application.governorate'),
            self::TYPE_CITY => trans('setting::application.city'),
            self::TYPE_NEIGHBORHOOD => trans('setting::application.region'),
            self::TYPE_MOSQUES => trans('setting::application.mosques'),
        );
        
        if (null === $value) {
            return $options;
        }
        
        if(array_key_exists($value, $options)) {
            return $options[$value];
        }
    }
    
    public function getType()
    {
        return self::type($this->location_type);
    }
    
    public function getTypeNameAttribute()
    {
        return $this->getType();
    }
    
    public function getTypeOptionsAttribute()
    {
        return self::type();
    }

    public function LocationI18n()
    {
        return $this->hasOne('Setting\Model\LocationI18n','location_id', 'id');
    }

    public static function export()
    {
        return  \DB::table('location_map_view')->get();
    }
}
