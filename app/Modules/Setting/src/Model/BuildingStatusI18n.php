<?php

namespace Setting\Model;

use Application\Model\I18nEntity;

class BuildingStatusI18n extends I18nEntity
{
    protected $table = 'char_building_status_i18n';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['building_status_id', 'language_id', 'name'];
    
    public function getEntityIdColumn()
    {
        return 'building_status_id';
    }
}
