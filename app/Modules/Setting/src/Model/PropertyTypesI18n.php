<?php

namespace Setting\Model;

class PropertyTypesI18n  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_property_types_i18n';
    protected $primaryKey = 'property_type_id';
    protected $fillable = ['property_type_id','language_id', 'name'];
    public $timestamps = false;


    public function PropertyTypes()
    {
        return $this->belongsTo('Setting\Model\PropertyTypes', 'property_type_id', 'id');
    }

    public function languages()
    {
        return $this->belongsTo('Setting\Model\Language','language_id','id');
    }

    public function getPropertyTypesList($id)
    {
        return \DB::table('char_property_types_i18n')
            ->join('char_languages', 'char_languages.id', '=', 'char_property_types_i18n.language_id')
            ->where('char_property_types_i18n.property_type_id','=',$id)
            ->select(
                'char_property_types_i18n.property_type_id as id',
                'char_property_types_i18n.name as name',
                'char_property_types_i18n.language_id as language_id',
                'char_languages.name as language_name'
            )
            ->get();
    }



}
