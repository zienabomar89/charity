<?php

namespace Setting\Model;

class aidsLocationI18n extends \Application\Model\I18nEntity
{
    protected $table = 'char_aids_locations_i18n';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['location_id', 'language_id', 'name'];

    public function getEntityIdColumn()
    {
        return 'location_id';
    }


    public static function getLocationId($parent,$name){

        if(!is_null($parent) && $name != " "){

            $child
                = \DB::table('char_aids_locations')
                ->join('char_aids_locations_i18n', function ($q) use($name){
                    $q->on('char_aids_locations_i18n.location_id', '=', 'char_aids_locations.id');
                    $q->where('char_aids_locations_i18n.language_id','=',1);
                    if($name != " "){
                        $q->whereRaw("normalize_text_ar(char_aids_locations_i18n.name) like  normalize_text_ar('$name')");
//                        $q->whereRaw("normalize_text_ar(char_aids_locations_i18n.name) like  normalize_text_ar(?)", '%' . $name . '%');
                    }
                });


            if($parent){
                $child =$child->where('char_aids_locations.parent_id','=',$parent);
            }


            $child= $child->selectRaw('char_aids_locations.id as id,char_aids_locations_i18n.*')->first();

            return is_null($child)? null : (int) $child->id;
        }
        return null;

    }

    public static function getTypeList($type){

        $language_id =  \App\Http\Helpers::getLocale();

        return
            \DB::table('char_aids_locations')
                ->join('char_aids_locations_i18n', function ($q) use ($language_id){
                    $q->on('char_aids_locations_i18n.location_id', '=', 'char_aids_locations.id');
                    $q->where('char_aids_locations_i18n.language_id','=',$language_id);
                })
                ->where('char_aids_locations.location_type','=',$type)
               ->selectRaw('char_aids_locations.*,char_aids_locations_i18n.*')
                ->get();


    }


    public static function getRelatedList($type,$whereIn,$map){

        $language_id =  \App\Http\Helpers::getLocale();

        if(sizeof($whereIn) >0 ){
            if(!$map){
                $connected=[];
                foreach ($whereIn as $key=>$value){
                    $connected[]= $value->parent_id;
                }
            }else{
                $connected =  $whereIn;
            }

            return
                \DB::table('char_aids_locations')
                    ->join('char_aids_locations_i18n', function ($q) use ($language_id){
                        $q->on('char_aids_locations_i18n.location_id', '=', 'char_aids_locations.id');
                        $q->where('char_aids_locations_i18n.language_id','=',$language_id);
                    })
                    ->where(function ($q) use ($type,$connected) {
                        $q->whereIn('char_aids_locations.id',$connected);
                        $q ->where('char_aids_locations.location_type','=',$type);
                    })

                    ->selectRaw('char_aids_locations.*,char_aids_locations_i18n.*')
                    ->get();
        }

        return  [];

    }


    public static function getConnectedIds($type,$whereIn,$map){

        if(sizeof($whereIn) >0 ){
            if(!$map){
                $connected=[];
                foreach ($whereIn as $key=>$value){
                    $connected[]= $value->parent_id;
                }
            }else{
                $connected =  $whereIn;
            }

            $related = \DB::table('char_aids_locations')
                       ->where(function ($q) use ($type,$connected) {
                        $q->whereIn('char_aids_locations.id',$connected);
                        $q ->where('char_aids_locations.location_type','=',$type);
                    })

                    ->selectRaw('char_aids_locations.*')
                    ->get();

                if(sizeof($related) >0 ) {
                    $return=[];
                    foreach ($related as $key=>$value){
                        $return[]= $value->id;
                    }

                    return $return;
                }
        }

        return  [];

    }



}
