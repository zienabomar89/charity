<?php

namespace Setting\Model;

use Application\Model\I18nEntity;

class PropertyI18n extends I18nEntity
{
    protected $table = 'char_properties_i18n';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['property_id', 'language_id', 'name'];
    
    public function getEntityIdColumn()
    {
        return 'property_id';
    }
}
