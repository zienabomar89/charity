<?php

namespace Setting\Model;

use Illuminate\Database\Eloquent\Model;

class EssentialWeight extends Model implements RankEntity
{
    protected $table = 'char_essentials_weights';
    
    public $timestamps = false;
    public $incrementing = false;
    
    protected $fillable = ['essential_id', 'value', 'weight'];

    protected static $rules;
    
    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                //'essential_id' => 'required',
                'value' => 'required',
                'weight' => 'required'
            );
        }
        
        return self::$rules;
    }
    
    public function updateLinkedRank(RankEntity $old)
    {
        if ($old->weight == $this->weight) {
            return true;
        }
        
        $bindings = array(
            'old' => $old->weight,
            'new' => $this->weight,
            'id' => $this->id,
        );
        
        $query = 'UPDATE `char_cases` SET
            `rank` = `rank` - :old + :new
            WHERE `id` in (
                SELECT `case_id` FROM `char_cases_essentials` WHERE `essential_id` = :id
                AND `condition` = :value
            )';
        
        return $this->getConnection()->update($query, $bindings);
    }
    
    /**
     * Set the keys for a save update query.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function setKeysForSaveQuery(\Illuminate\Database\Eloquent\Builder $query)
    {
        $query->where('essential_id', '=', $this->essential_id)
              ->where('value', '=', $this->value);

        return $query;
    }
}

