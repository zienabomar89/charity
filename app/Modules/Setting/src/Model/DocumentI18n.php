<?php

namespace Setting\Model;

class DocumentI18n extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_document_types_i18n';
    protected $primaryKey='document_type_id';
    protected $fillable = ['document_type_id','language_id', 'name'];
    public $timestamps = false;


     public function char_document_types()
    {
        return $this->belongsTo('Setting\Model\DocumentTypes', 'document_type_id', 'id');
    }

    public function languages()
    {
        return $this->belongsTo('Setting\Model\Language','language_id','id');
    }


    public static function getDocumentName($id)
    {
        $documents = self::query()->where(['document_type_id'=>$id ,'language_id'=>1])->first();
        return (is_null($documents))? ' ' :$documents->name;
    }



}
