<?php

namespace Setting\Model;

use Application\Model\I18nEntity;

class MaritalStatusI18n extends I18nEntity
{
    protected $table = 'char_marital_status_i18n';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['marital_status_id', 'language_id', 'name'];
    
    public function getEntityIdColumn()
    {
        return 'marital_status_id';
    }
}

