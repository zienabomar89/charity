<?php

namespace Setting\Model;

use Application\Model\I18nEntity;

class KinshipI18n extends I18nEntity
{
    protected $table = 'char_kinship_i18n';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['kinship_id', 'language_id', 'name'];
    
    public function getEntityIdColumn()
    {
        return 'kinship_id';
    }

    public static function export()
    {
        return  \DB::table('char_kinship_i18n')->get();
    }
}
