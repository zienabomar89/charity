<?php

namespace Setting\Model;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = 'char_branches';
        
    protected static $rules;
    
    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'name' => 'required|max:255',
            );
        }
        
        return self::$rules;
    }

    public static function getBranchId($parent,$name){


        if(!is_int($parent)){
            $parentRow= \Setting\Model\Bank::where(['name'=>$parent , 'language_id'=>1])->first();
            $parentId = is_null($parentRow)? null : (int) $parentRow->id;
        }else{
            $parentId=$parent;
        }

        if(!is_null($parentId) && !is_null($parentId && $name == " ")){

            $child = \DB::table('char_branches');
            if($parentId){
                $child =$child->where('char_branches.bank_id','=',$parentId);
            }

            $child= $child->select('char_branches.id')->first();

            return is_null($child)? null : (int) $child->id;
        }
        return null;

    }

}

