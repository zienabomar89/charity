<?php

namespace Setting\Model;

use Illuminate\Database\Eloquent\Model;

class AidSourceWeight extends Model implements RankEntity
{
    protected $table = 'char_aid_sources_weights';
    
    public $timestamps = false;

    protected static $rules;
    
    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                //'aid_source_id' => 'required',
                'value_max' => 'sometimes|required',
                'value_min' => 'sometimes|required',
                'weight' => 'required'
            );
        }
        
        return self::$rules;
    }
    
    public function updateLinkedRank(RankEntity $old)
    {
        if ($old->weight == $this->weight) {
            return true;
        }
        
        $bindings = array(
            'old' => $old->weight,
            'new' => $this->weight,
            'id' => $this->id,
        );
        if ($this->value_min !== null) {
            $where[] = 'ifnull(`aid_value`, 0) >= :min';
            $bindings['min'] = $this->value_min;
        }
        if ($this->value_max !== null) {
            $where[] = 'ifnull(`aid_value`, 0) <= :max';
            $bindings['max'] = $this->value_max;
        }
        
        $query = 'UPDATE `char_cases` SET
            `rank` = `rank` - :old + :new
            WHERE `person_id` in (
                SELECT `person_id` FROM `char_persons_aids` WHERE `aid_source_id` = :id
                AND ' . implode(' AND ', $where) . '
            )';
        
        return $this->getConnection()->update($query, $bindings);
    }
}

