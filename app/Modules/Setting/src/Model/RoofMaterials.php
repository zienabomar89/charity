<?php

namespace Setting\Model;

class RoofMaterials  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_roof_materials';
    protected $fillable = ['weight'];
    public $timestamps = false;

    public function RoofMaterialsI18n()
    {
        return $this->hasOne('Setting\Model\RoofMaterialsI18n','roof_material_id', 'id');
    }

    public function getTablelist($page)
    {
        $rows = \DB::table('char_roof_materials')
            ->join('char_roof_materials_i18n', 'char_roof_materials_i18n.roof_material_id', '=', 'char_roof_materials.id')
            ->where('char_roof_materials_i18n.language_id','=',1)
            ->orderBy('char_roof_materials_i18n.name')
            ->select(
                'char_roof_materials.id as id',
                'char_roof_materials.weight as weight',
                'char_roof_materials_i18n.name as name',
                'char_roof_materials_i18n.language_id as language_id'
            )
            ->paginate(config('constants.records_per_page'));
        return $rows;
    }

    Public function get_translate_list($id){
        return \DB::table('char_roof_materials_i18n')
            ->join('char_languages AS L', 'L.id', '=', 'char_roof_materials_i18n.language_id')
            ->where('char_roof_materials_i18n.roof_material_id', '=', $id)
            ->select(
                'char_roof_materials_i18n.roof_material_id as id',
                'char_roof_materials_i18n.name as name',
                'L.name as language_name',
                'L.id as language_id'

            )
            ->get();
    }


}
