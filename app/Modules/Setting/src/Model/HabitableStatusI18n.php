<?php

namespace Setting\Model;

use Application\Model\I18nEntity;

class HabitableStatusI18n extends I18nEntity
{
    protected $table = 'char_habitable_status_i18n';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['habitable_status_id', 'language_id', 'name'];
    
    public function getEntityIdColumn()
    {
        return 'habitable_status_id';
    }
}
