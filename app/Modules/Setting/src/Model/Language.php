<?php

namespace Setting\Model;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    protected $table = 'char_languages';
    
    public $timestamps = false;
    
    protected static $rules;
    
    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'name' => 'required|max:255',
                'code' => 'required',
                'native_name' => 'required',
            );
        }
        
        return self::$rules;
    }
    
    public function setAttributesFromRequest(\Illuminate\Http\Request $request)
    {
        $this->code = $request->input('code');
        $this->native_name = $request->input('native_name');
        //$this->default = $request->input('default', 0);
        return $this;
    }
}