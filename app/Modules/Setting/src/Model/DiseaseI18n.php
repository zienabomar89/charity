<?php

namespace Setting\Model;

use Application\Model\I18nEntity;

class DiseaseI18n extends I18nEntity
{
    protected $table = 'char_diseases_i18n';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['disease_id', 'language_id', 'name'];
    
    public function getEntityIdColumn()
    {
        return 'disease_id';
    }
}

