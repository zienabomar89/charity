<?php

namespace Setting\Model;

use Application\Model\I18nEntity;

class HouseStatusI18n extends I18nEntity
{
    protected $table = 'char_house_status_i18n';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['house_status_id', 'language_id', 'name'];
    
    public function getEntityIdColumn()
    {
        return 'house_status_id';
    }
}
