<?php

namespace Setting\Model;

use Application\Model\I18nEntity;

class ProjectBeneficiaryCategoryI18n extends I18nEntity
{
    protected $table = 'char_project_beneficiary_category_i18n';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['beneficiary_category_id', 'language_id', 'name'];

    public function getEntityIdColumn()
    {
        return 'beneficiary_category_id';
    }
}

