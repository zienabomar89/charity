<?php

namespace Setting\Model;

class RoofMaterialsI18n  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_roof_materials_i18n';
    protected $primaryKey = 'roof_material_id';
    protected $fillable = ['roof_material_id','language_id', 'name'];
    public $timestamps = false;

    public function languages()
    {
        return $this->belongsTo('Setting\Model\Language','language_id','id');
    }

    public function RoofMaterials()
    {
        return $this->belongsTo('Setting\Model\RoofMaterials', 'roof_material_id', 'id');
    }

    public function getRoofMaterialsList($id)
    {
        return \DB::table('char_roof_materials_i18n')
            ->join('char_languages', 'char_languages.id', '=', 'char_roof_materials_i18n.language_id')
            ->where('char_roof_materials_i18n.roof_material_id','=',$id)
            ->select(
                'char_roof_materials_i18n.roof_material_id as id',
                'char_roof_materials_i18n.name as name',
                'char_roof_materials_i18n.language_id as language_id',
                'char_languages.name as language_name'
            )
            ->get();
    }


}
