<?php

namespace Setting\Model;

use Application\Model\I18nEntity;

class DeathCauseI18n extends I18nEntity
{
    protected $table = 'char_death_causes_i18n';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['death_cause_id', 'language_id', 'name'];
    
    public function getEntityIdColumn()
    {
        return 'death_cause_id';
    }
}

