<?php

namespace Setting\Model;

use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'char_cases';
    
    protected static $rules;
    
    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'first_name' => 'required',
                'second_name' => 'required',
                'third_name' => 'required',
                'last_name' => 'required',
                'id_card_number' => 'required',
                'gender' => 'required',
                'marital_status_id' => 'required',
                'birthday' => 'required',
                'birth_place' => 'required',
                'nationality' => 'required',
                'death_date' => 'required',
                'death_cause_id' => 'required',
                'father_id' => 'required',
                'mother_id' => 'required',
                'location_id' => 'required',
                'street_address' => 'required',
                'refugee' => 'required',
                'unrwa_card_number' => 'required',
                'spouses' => 'required',
            );
        }
        
        return self::$rules;
    }
    
    public static function fetch($options = array())
    {
        $language_id = 1;
        
        $defaults = array(
            'marital_status_name' => false,
            'birth_place_name' => false,
            'nationality_name' => false,
            'death_cause_name' => false,
            'father_death_cause' => false,
            'father_name' => false,
            'mother_name' => false,
            'mother_death_cause' => false,
            'location_name' => false,
            
            'id' => null,
            'marital_status_id' => null,
            'death_cause_id' => null,
            'father_id' => null,
            'father_death_cause_id' => null,
            'mother_id' => null,
            'mother_death_cause_id' => null,
            
            'paginate' => false,
        );
        
        $options = array_merge($defaults, $options);
        
        $query = self::query()->addSelect(array(
            'char_persons.first_name',
            'char_persons.second_name',
            'char_persons.third_name',
            'char_persons.last_name',
        ))
        ->orderBy('char_persons.first_name')
        ->orderBy('char_persons.second_name')
        ->orderBy('char_persons.third_name')
        ->orderBy('char_persons.last_name');
        
        if ($options['marital_status_name']) {
            $query->join('char_marital_status_i18n', function($join) use ($language_id) {
                $join->on('char_persons.marital_status_id', '=', 'char_marital_status_i18n.marital_status_id')
                     ->where('char_marital_status_i18n.language_id', '=', $language_id);
            })
            ->addSelect(array(
                'char_marital_status_i18n.name AS marital_status_name',
            ));
        }
        if ($options['death_cause_name']) {
            $query->join('char_death_causes_i18n', function($join) use ($language_id) {
                $join->on('char_persons.death_cause_id', '=', 'char_death_causes_i18n.death_cause_id')
                     ->where('char_death_causes_i18n.language_id', '=', $language_id);
            })
            ->addSelect(array(
                'char_marital_status_i18n.name AS death_cause_name',
            ));
        }
        if ($options['father_name'] || $options['father_death_cause']) {
            $query->join('char_persons AS father', 'char_persons.father_id', '=', 'father.id');
            if ($options['father_name']) {
                $query->addSelect(array(
                    'father.first_name AS father_first_name',
                    'father.second_name AS father_second_name',
                    'father.third_name AS father_third_name',
                    'father.last_name AS father_last_name',
                ));
            }
            if ($options['father_death_cause']) {
                $query->join('char_death_causes_i18n AS father_death', function($join) use ($language_id) {
                    $join->on('father.death_cause_id', '=', 'father_death.death_cause_id')
                         ->where('father_death.language_id', '=', $language_id);
                })
                ->addSelect(array(
                    'father_death.name AS father_death_cause',
                ));
            }
        }
        if ($options['mother_name'] || $options['mother_death_cause']) {
            $query->join('char_persons AS mother', 'char_persons.mother_id', '=', 'mother.id');
            if ($options['mother_name']) {
                $query->addSelect(array(
                    'mother.first_name AS mother_first_name',
                    'mother.second_name AS mother_second_name',
                    'mother.third_name AS mother_third_name',
                    'mother.last_name AS mother_last_name',
                ));
            }
            if ($options['mother_death_cause']) {
                $query->join('char_death_causes_i18n AS mother_death', function($join) use ($language_id) {
                    $join->on('mother.death_cause_id', '=', 'mother_death.death_cause_id')
                         ->where('mother_death.language_id', '=', $language_id);
                })
                ->addSelect(array(
                    'mother_death.name AS mother_death_cause',
                ));
            }
        }
        
        if ($options['id']) {
            return $query->where('char_persons.id', '=', $options['id'])->first();
        }
        if ($options['paginate']) {
            return $query->paginate($options['paginate']);
        }
        
        return $query->get();
    }
    
    public static function descendants($id){
        return self::query()->where('father_id', '=', $id)
                            ->orWhere('mother_id', '=', $id)
                            ->get();
    }
    public function getDescendants()
    {
        return self::descendants($this->id);
    }
    
    public static function father($id){
        return self::query()->join('char_persons AS d', 'char_persons.id', '=', 'd.father_id')
                            ->where('d.id', '=', $id)
                            ->first();
    }
    public function getFather()
    {
        return self::father($this->father_id);
    }
    
    public static function mother($id){
        return self::query()->join('char_persons AS d', 'char_persons.id', '=', 'd.mother_id')
                            ->where('d.id', '=', $id)
                            ->first();
    }
    public function getMother()
    {
        return self::mother($this->mother_id);
    }

    public static function siblings($id){
        return self::query()->join('char_persons AS d', 'char_persons.father_id', '=', 'd.father_id')
                            ->where('d.id', '=', $id)
                            ->get();
    }
    public function getSiblings()
    {
        return self::siblings($this->id);
    }
    
    public static function cases($id, $organization_id = null)
    {
        $query = self::query()->join('char_cases', 'char_persons.id', '=', 'char_cases.person_id')
                              ->join('char_categories', 'char_cases.category_id', '=', 'char_categories.id')
                              ->where('char_persons.id', '=', $id)
                              ->select(array(
                                  'char_cases.*',
                                  'char_categories.name AS category_name'
                              ));
        if (null !== $organization_id) {
            $query->where('char_cases.organization_id', '=', $organization_id);
        } else {
            $query->join('char_organizations', 'char_cases.organization_id', '=', 'char_organizations.id')
                  ->addSelect('char_organizations.name AS organization_name');
        }
        
        return $query->get();
    }
    public function getCases()
    {
        return self::cases($this->id);
    }
}

