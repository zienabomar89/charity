<?php

namespace Setting\Model;
class PropertyTypes  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_property_types';
    protected $fillable = ['weight'];
    public $timestamps = false;

    public function property_types_i18n()
    {
        return $this->hasOne('Setting\Model\PropertyTypesI18n','property_type_id', 'id');
    }

    public function getTablelist($page)
    {
        $rows = \DB::table('char_property_types')
            ->join('char_property_types_i18n', 'char_property_types_i18n.property_type_id', '=', 'char_property_types.id')
            ->where('char_property_types_i18n.language_id','=',1)
            ->orderBy('char_property_types_i18n.name')
            ->select(
                'char_property_types.id as id',
                'char_property_types.weight as weight',
                'char_property_types_i18n.name as name',
                'char_property_types_i18n.language_id as language_id'
            )
            ->paginate(config('constants.records_per_page'));
        return $rows;
    }
    Public function get_translate_list($id){
        return \DB::table('char_property_types_i18n')
            ->join('char_languages AS L', 'L.id', '=', 'char_property_types_i18n.language_id')
            ->where('char_property_types_i18n.property_type_id', '=', $id)
            ->select(
                'char_property_types_i18n.property_type_id as id',
                'char_property_types_i18n.name as name',
                'L.name as language_name',
                'L.id as language_id'

            )
            ->get();
    }



}
