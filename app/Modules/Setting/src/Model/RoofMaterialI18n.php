<?php

namespace Setting\Model;

use Application\Model\I18nEntity;

class RoofMaterialI18n extends I18nEntity
{
    protected $table = 'char_roof_materials_i18n';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['roof_material_id', 'language_id', 'name'];
    
    public function getEntityIdColumn()
    {
        return 'roof_material_id';
    }
}
