<?php

namespace Setting\Model;

class LocationI18n extends \Application\Model\I18nEntity
{
    protected $table = 'char_locations_i18n';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['location_id', 'language_id', 'name'];
    
    public function getEntityIdColumn()
    {
        return 'location_id';
    }


    public static function getLocationId($parent,$name){


        if(!is_int($parent)){
            $parentRow= \Setting\Model\LocationI18n::where(['name'=>$parent , 'language_id'=>1])->first();
            $parentId = is_null($parentRow)? null : (int) $parentRow->location_id;
        }else{
            $parentId=$parent;
        }

        if(!is_null($parentId) && !is_null($parentId && $name == " ")){

           $child
                = \DB::table('char_locations')
                ->join('char_locations_i18n', function ($q) use($name){
                    $q->on('char_locations_i18n.location_id', '=', 'char_locations.id');
                    $q->where('char_locations_i18n.language_id','=',1);
                    if($name != " "){
                        $q->whereRaw("normalize_text_ar(char_locations_i18n.name) like  normalize_text_ar(?)", '%' . $name . '%');
                    }
                });


            if($parentId){
                $child =$child->where('char_locations.parent_id','=',$parentId);
            }

            $child= $child->selectRaw('char_locations.id as id,char_locations_i18n.*')->first();

            return is_null($child)? null : (int) $child->id;
        }
        return null;

    }

}
