<?php

namespace Setting\Model;

class EduAuthorities extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_edu_authorities';
    protected $fillable = ['id','language_id', 'name'];
    public $timestamps = false;


    public function languages()
    {
        return $this->belongsTo('Setting\Model\Language','language_id','id');
    }

    public function personsEducation()
    {
        return $this->hasMany('Sponsorship\Model\PersonsEducation','authority','id');
    }

    Public function get_translate_list($id){
        return \DB::table('char_edu_authorities')
            ->join('char_languages AS L', 'L.id', '=', 'char_edu_authorities.language_id')
            ->where('char_edu_authorities.id', '=', $id)
            ->select(
                'char_edu_authorities.id as id',
                'char_edu_authorities.name as name',
                'L.name as language_name',
                'L.id as language_id'

            )
            ->get();
    }

}
