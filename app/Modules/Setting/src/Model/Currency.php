<?php

namespace Setting\Model;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'char_currencies';
    
    protected $fillable = ['name', 'en_name', 'code', 'symbol'];
    
    public $timestamps = false;
    
    protected static $rules;
    
    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'name' => 'required|max:255',
                'en_name' => 'required|max:255',
                'code' => 'required|max:255',
                'symbol' => 'required|max:255',
            );
        }
        
        return self::$rules;
    }
    
    public function setAttributesFromRequest(\Illuminate\Http\Request $request)
    {
        $this->code = $request->input('code');
        $this->symbol = $request->input('symbol');
        return $this;
    }

    public static function listCurrency()
    {
        $language_id =  \App\Http\Helpers::getLocale();

        return Self::query()
              ->selectRaw("char_currencies.* ,
                         CASE  WHEN $language_id = 1 THEN char_currencies.name Else char_currencies.en_name END  AS name ")
                ->get();

    }
}
