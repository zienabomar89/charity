<?php

namespace Setting\Model;

use Application\Model\I18nEntity;

class ProjectRegionI18n extends I18nEntity
{
    protected $table = 'char_project_regions_i18n';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['region_id', 'language_id', 'name'];

    public function getEntityIdColumn()
    {
        return 'region_id';
    }
}

