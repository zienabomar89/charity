<?php

namespace Setting\Model;
class LocationsI18n extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_locations_i18n';
    protected $fillable = ['language_id', 'name','location_id'];
    public $timestamps = false;

    public function languages()
    {
        return $this->belongsTo('Setting\Model\Language','language_id','id');
    }
}
