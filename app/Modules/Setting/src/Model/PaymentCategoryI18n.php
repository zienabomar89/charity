<?php

namespace Setting\Model;

use Application\Model\I18nEntity;

class PaymentCategoryI18n extends I18nEntity
{
    protected $table = 'char_payment_category_i18n';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['payment_category_id', 'language_id', 'name'];

    public function getEntityIdColumn()
    {
        return 'payment_category_id';
    }
}

