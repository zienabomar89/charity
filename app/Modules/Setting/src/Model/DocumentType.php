<?php

namespace Setting\Model;

use Application\Model\I18nableEntity;

class DocumentType extends I18nableEntity
{
    protected $table = 'char_document_types';
        
    protected static $rules;
    
    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'name' => 'required|max:255',
                'expiry' => 'required'
            );
        }
        
        return self::$rules;
    }
    
    /**
     * 
     * @return I18nInterface
     */
    public function getI18nModel()
    {
        if ($this->i18nModel === null) {
            $this->i18nModel = new DocumentTypeI18n();
            $this->i18nModel->forceFill(array(
                $this->i18nModel->getEntityIdColumn() => $this->id,
                $this->i18nModel->getLanguageIdColumn() => $this->getLanguageId(),
            ));
        }
        
        return $this->i18nModel;
    }
    
    public function setAttributesFromRequest(\Illuminate\Http\Request $request)
    {
        $this->expiry = $request->input('expiry');
        return $this;
    }
}

