<?php

namespace Setting\Model;

use Application\Model\I18nEntity;

class DocumentTypeI18n extends I18nEntity
{
    protected $table = 'char_document_types_i18n';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['document_type_id', 'language_id', 'name'];
    
    public function getEntityIdColumn()
    {
        return 'document_type_id';
    }
}
