<?php

namespace Setting\Model;

use Application\Model\I18nableEntity;

class ProjectBeneficiaryCategory extends I18nableEntity
{
    protected $table = 'char_project_beneficiary_category';

    public $timestamps = false;

    protected static $rules;

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'name' => 'required|max:255',
            );
        }

        return self::$rules;
    }

    /**
     *
     * @return I18nInterface
     */
    public function getI18nModel()
    {
        if ($this->i18nModel === null) {
            $this->i18nModel = new ProjectBeneficiaryCategoryI18n();
            $this->i18nModel->forceFill(array(
                $this->i18nModel->getEntityIdColumn() => $this->id,
                $this->i18nModel->getLanguageIdColumn() => $this->getLanguageId(),
            ));
        }

        return $this->i18nModel;
    }


}

