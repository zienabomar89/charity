<?php

namespace Setting\Model;

class WorkStatus  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_work_status';
    protected $fillable = ['language_id', 'name'];
    public $timestamps = false;

    public function languages()
    {
        return $this->belongsTo('Setting\Model\Language','language_id','id');
    }

    public function Person()
    {
        return $this->belongsToMany('Common\Model\Person', 'char_persons_work', 'work_status_id', 'person_id');

    }

    Public function get_translate_list($id){
        return \DB::table('char_work_status')
            ->join('char_languages AS L', 'L.id', '=', 'char_work_status.language_id')
            ->where('char_work_status.id', '=', $id)
            ->select(
                'char_work_status.id as id',
                'char_work_status.name as name',
                'L.name as language_name',
                'L.id as language_id'
            )
            ->get();
    }


}
