<?php

namespace Setting\Model;

use Organization\Model\Organization;

class aidsLocation extends \Application\Model\I18nableEntity
{
    const TYPE_COUNTRY        = 0;
    const TYPE_DISTRICT       = 1;
    const TYPE_REGION         = 2;
    const TYPE_NEIGHBORHOOD   = 3;
    const TYPE_SQUARE         = 4;
    const TYPE_MOSQUES        = 5;
    protected $table = 'char_aids_locations';
    protected static $rules;

    protected $appends = ['type_name', 'type_options'];

    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'parent_id' => 'required|int',
                'ratio' => 'required',
                 //'location_type' => 'required|int',
                'name' => 'required|max:255',
            );
        }

        return self::$rules;
    }

    /**
     * Scope a query to only include a location descendants.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDescendants($query, $id = null)
    {
        if ($id === null) {
            return $query;
        }
        return $query->where('parent_id', '=', $id);
    }

    /**
     * Scope a query to only include countries.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCountries($query)
    {
        return $query->where('location_type', '=', self::TYPE_COUNTRY);
    }

    /**
     * Scope a query to only include districts.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDistricts($query, $parent_id = null)
    {
        $query->where('location_type', '=', self::TYPE_DISTRICT);
        if (null !== $parent_id) {
            $query->where('parent_id', '=', $parent_id);
        }
        return $query;
    }

    /**
     * Scope a query to only include regions
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeRegions($query, $parent_id = null)
    {
        $query->where('location_type', '=', self::TYPE_REGION);
        if (null !== $parent_id) {
            $query->where('parent_id', '=', $parent_id);
        }
        return $query;
    }

    /**
     * Scope a query to only include neighborhoods.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNeighborhoods($query, $parent_id = null)
    {
        $query->where('location_type', '=', self::TYPE_NEIGHBORHOOD);
        if (null !== $parent_id) {
            $query->where('parent_id', '=', $parent_id);
        }
        return $query;
    }

    /**
     * Scope a query to only include squares.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeSquares($query, $parent_id = null)
    {
        $query->where('location_type', '=', self::TYPE_SQUARE);
        if (null !== $parent_id) {
            $query->where('parent_id', '=', $parent_id);
        }
        return $query;
    }

    /**
     * Scope a query to only include mosques.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMosques($query, $parent_id = null)
    {
        $query->where('location_type', '=', self::TYPE_MOSQUES);
        if (null !== $parent_id) {
            $query->where('parent_id', '=', $parent_id);
        }
        return $query;
    }

    /**
     *
     * @return I18nInterface
     */
    public function getI18nModel()
    {
        if ($this->i18nModel === null) {
            $this->i18nModel = new aidsLocationI18n();
            $this->i18nModel->forceFill(array(
                $this->i18nModel->getEntityIdColumn() => $this->id,
                $this->i18nModel->getLanguageIdColumn() => $this->getLanguageId(),
            ));
        }

        return $this->i18nModel;
    }

    public function setAttributesFromRequest(\Illuminate\Http\Request $request)
    {
        $this->parent_id = $request->input('parent_id');
        return $this;
    }

    public static function type($value = null)
    {
        $options = array(
            self::TYPE_COUNTRY => trans('setting::application.state'),
            self::TYPE_DISTRICT => trans('setting::application.governorate'),
            self::TYPE_REGION => trans('setting::application.region'),
            self::TYPE_NEIGHBORHOOD => trans('setting::application.sub_region'),
            self::TYPE_SQUARE => trans('setting::application.squares'),
            self::TYPE_MOSQUES => trans('setting::application.mosques'),
        );

        if (null === $value) {
            return $options;
        }

        if(array_key_exists($value, $options)) {
            return $options[$value];
        }
    }

    public function getType()
    {
        return self::type($this->location_type);
    }

    public function getTypeNameAttribute()
    {
        return $this->getType();
    }

    public function getTypeOptionsAttribute()
    {
        return self::type();
    }

    public function LocationI18n()
    {
        return $this->hasOne('Setting\Model\aidsLocation','location_id', 'id');
    }


    public static function  getRelatedRationByType($organization_id,$type)
    {
        $ratios = [];
        $locations = \DB::table('char_aids_locations')
                    ->where(function($subQuery) use($organization_id,$type) {
//                        $subQuery->whereIn('id', function($query) use($organization_id) {
//                            $query->select('location_id')
//                                ->from('char_organization_locations')
//                                ->where('organization_id',$organization_id);
//                        });
                        $subQuery->where('location_type',$type);
                    })
                    ->selectRaw('char_aids_locations.id as location_id,char_aids_locations.ratio')
                    ->get();

        return $locations;
    }

    public static function getFullTree ($locations){
        if(sizeof($locations) > 0){
            $TYPE_MOSQUES = aidsLocation::TYPE_MOSQUES;
            $Squares = aidsLocation::where(function($subQuery) use($locations,$TYPE_MOSQUES) {
                $subQuery->where('location_type',$TYPE_MOSQUES);
                $subQuery->wherein('id',$locations);
            })
                ->groupBy('char_aids_locations.parent_id')
                ->selectRaw('char_aids_locations.parent_id')
                ->get();

            $child = [];
            if(sizeof($Squares) > 0){
                foreach ($Squares as $key=>$value){
                    $child[]=$value->parent_id;
                    if(!in_array($value->parent_id,$locations)){
                        $child[]=$value->parent_id;
                        $locations[]=$value->parent_id;
                    }
                }

                $TYPE_SQUARE = aidsLocation::TYPE_SQUARE;
                $neighbours = aidsLocation::where(function($subQuery) use($child,$TYPE_SQUARE) {
                    $subQuery->where('location_type',$TYPE_SQUARE);
                    $subQuery->wherein('id',$child);
                })
                    ->groupBy('char_aids_locations.parent_id')
                    ->selectRaw('char_aids_locations.parent_id')
                    ->get();

                $child = [];
                if(sizeof($neighbours) > 0) {
                    foreach ($neighbours as $key => $value) {
                        if (!in_array($value->parent_id, $locations)) {
                            $child[] = $value->parent_id;
                            $locations[] = $value->parent_id;
                        }


                    }

                    $TYPE_NEIGHBORHOOD= aidsLocation::TYPE_NEIGHBORHOOD;
                    $region = aidsLocation::where(function($subQuery) use($child,$TYPE_NEIGHBORHOOD) {
                        $subQuery->where('location_type',$TYPE_NEIGHBORHOOD);
                        $subQuery->wherein('id',$child);
                    })
                        ->groupBy('char_aids_locations.parent_id')
                        ->selectRaw('char_aids_locations.parent_id')
                        ->get();

                    $child = [];
                    if(sizeof($region) > 0){
                        foreach ($region as $key=>$value){
                            if(!in_array($value->parent_id,$locations)){
                                $child[]=$value->parent_id;
                                $locations[]=$value->parent_id;
                            }
                        }

                        $TYPE_REGION= aidsLocation::TYPE_REGION;
                        $district = aidsLocation::where(function($subQuery) use($child,$TYPE_REGION) {
                            $subQuery->where('location_type',$TYPE_REGION);
                            $subQuery->wherein('id',$child);
                        })
                            ->groupBy('char_aids_locations.parent_id')
                            ->selectRaw('char_aids_locations.parent_id')
                            ->get();


                        $child = [];
                        if(sizeof($district) > 0){
                            foreach ($district as $key=>$value){
                                if(!in_array($value->parent_id,$locations)){
                                    $child[]=$value->parent_id;
                                    $locations[]=$value->parent_id;
                                }
                            }
                        }
                    }
                }
            }
        }

        return $locations;
    }

    public static function getTotalCountTree ($total,$voucher_id){

        $user = \Auth::user();
        $organization_id=$user->organization_id;
        $UserOrg_=$user->organization;

        $mosques = [];

        $district = aidsLocation::where(function($subQuery) use ($organization_id,$UserOrg_) {
                    $subQuery->where('location_type',aidsLocation::TYPE_DISTRICT);
                    if($UserOrg_->level != Organization::LEVEL_MASTER_CENTER){
                        $subQuery->whereIn('id', function($query) use($organization_id) {
                                    $query->select('location_id')
                                        ->from('char_organization_locations')
                                        ->where('organization_id',$organization_id);
                                });
                            }
                    })
                    ->selectRaw('id,ratio')
                    ->get();

        foreach ($district as $key=>$value){
            if(sizeof($district) == 1){
                $value->ratio = 100;
            }

            $value->total = round(($total * $value->ratio) / 100, 0) ;
            $value->region =aidsLocation::where(function($subQuery) use ($organization_id,$UserOrg_) {
                                    $subQuery->where('location_type',aidsLocation::TYPE_REGION);
                                    if($UserOrg_->level != Organization::LEVEL_MASTER_CENTER){
                                        $subQuery->whereIn('id', function($query) use($organization_id) {
                                            $query->select('location_id')
                                                ->from('char_organization_locations')
                                                ->where('organization_id',$organization_id);
                                        });
                                    }

                           })
                        ->selectRaw('id,ratio')
                        ->get();


            foreach ($value->region as $k=>$v){
                if(sizeof($value->region) == 1){
                    $v->ratio = 100;
                }

                $v->total = round(($value->total * $v->ratio) / 100 , 0) ;
                $v->neighborhoods =aidsLocation::where(function($subQuery) use ($organization_id,$UserOrg_,$v) {
                                        $subQuery->where('location_type',aidsLocation::TYPE_NEIGHBORHOOD);
                                        $subQuery->where('parent_id',$v->id);
                                        if($UserOrg_->level != Organization::LEVEL_MASTER_CENTER){
                                            $subQuery->whereIn('id', function($query) use($organization_id) {
                                                $query->select('location_id')
                                                    ->from('char_organization_locations')
                                                    ->where('organization_id',$organization_id);
                                            });
                                        }
                                    })
                                    ->selectRaw('id,ratio')
                                    ->get();

                foreach ($v->neighborhoods as $k_=>$v_){
                    if(sizeof($v->neighborhoods) == 1){
                        $v_->ratio = 100;
                    }

                    $v_->total = round(($v->total * $v_->ratio) / 100 , 0) ;
                    $v_->squares =aidsLocation::where(function($subQuery) use ($organization_id,$UserOrg_,$v_) {
                                    $subQuery->where('location_type',aidsLocation::TYPE_SQUARE);
                                    $subQuery->where('parent_id',$v_->id);
                                    if($UserOrg_->level != Organization::LEVEL_MASTER_CENTER){
                                        $subQuery->whereIn('id', function($query) use($organization_id) {
                                            $query->select('location_id')
                                                ->from('char_organization_locations')
                                                ->where('organization_id',$organization_id);
                                        });
                                    }

                                })
                                ->selectRaw('id,ratio')
                                ->get();

                    foreach ($v_->squares as $k2=>$v2){
                        if(sizeof($v_->squares) == 1){
                            $v2->ratio = 100;
                        }

                        $v2->total = round(($v_->total * $v2->ratio) / 100 , 0) ;
                        $v2->mosques =aidsLocation::where(function($subQuery) use ($organization_id,$UserOrg_,$v2) {
                                        $subQuery->where('location_type',aidsLocation::TYPE_MOSQUES);
                                        $subQuery->where('parent_id',$v2->id);
                                        if($UserOrg_->level != Organization::LEVEL_MASTER_CENTER){
                                            $subQuery->whereIn('id', function($query) use($organization_id) {
                                                $query->select('location_id')
                                                    ->from('char_organization_locations')
                                                    ->where('organization_id',$organization_id);
                                            });
                                        }
                                    })
                                    ->selectRaw("id,ratio ,get_mosque_person_count($voucher_id,id) as count")
                                    ->get();

                        foreach ($v2->mosques as $k3=>$v3){
                            if(sizeof($v2->mosques) == 1){
                                $v3->ratio = 100;
                            }

                            $v3->total = round(($v2->total * $v3->ratio) / 100 , 0) ;

                            if($v3->total > 0)
                              $mosques[] = $v3;

                        }

                    }
                }

            }
        }

        return $mosques;
    }

    public static function getTotalCountProjectTree ($total,$amount,$project_id,$organization_id,$level){

        $language_id =  \App\Http\Helpers::getLocale();

        $neighborhoods = [];

        $district = aidsLocation::where(function($subQuery) use ($organization_id,$level) {
            $subQuery->where('location_type',aidsLocation::TYPE_DISTRICT);
            $subQuery->whereIn('id', function($query) use($organization_id) {
                $query->select('location_id')
                    ->from('char_organization_locations')
                    ->where('organization_id',$organization_id);
            });
        })
            ->selectRaw('id,ratio')
            ->get();

        foreach ($district as $key=>$value){
            if(sizeof($district) == 1){
                $value->ratio = 100;
            }

            $value->total = round(($total * $value->ratio) / 100, 0) ;
            $value->amount = round(($amount * $value->ratio) / 100, 0) ;
            $value->region =aidsLocation::where(function($subQuery) use ($organization_id,$level) {
                $subQuery->where('location_type',aidsLocation::TYPE_REGION);
                if($level != Organization::LEVEL_MASTER_CENTER){
                    $subQuery->whereIn('id', function($query) use($organization_id) {
                        $query->select('location_id')
                            ->from('char_organization_locations')
                            ->where('organization_id',$organization_id);
                    });
                }

            })
                ->selectRaw('id,ratio')
                ->get();


            foreach ($value->region as $k=>$v){
                if(sizeof($value->region) == 1){
                    $v->ratio = 100;
                }

                $v->total  = round(($value->total  * $v->ratio) / 100 , 0) ;
                $v->amount = round(($value->amount * $v->ratio) / 100, 0) ;
                $v->neighborhoods = \DB::table('char_aids_locations')
                    ->join('char_aids_locations_i18n', function ($q) use($language_id){
                        $q->on('char_aids_locations_i18n.location_id', '=', 'char_aids_locations.id');
                        $q->where('char_aids_locations_i18n.language_id','=',$language_id);
                    })
                    ->where(function($subQuery) use ($organization_id,$level,$v) {
                        $subQuery->where('char_aids_locations.location_type',aidsLocation::TYPE_NEIGHBORHOOD);
                        $subQuery->where('char_aids_locations.parent_id',$v->id);
                        $subQuery->whereIn('char_aids_locations.id', function($query) use($organization_id) {
                            $query->select('char_organization_locations.location_id')
                                ->from('char_organization_locations')
                                ->where('organization_id',$organization_id);
                        });
                    })
                    ->selectRaw("id,char_aids_locations_i18n.name as location_name,ratio,
                                                get_project_location_persons_count($project_id,1,1,id,$organization_id) as nominated")
                    ->get();


                foreach ($v->neighborhoods as $k_=>$v_){
                    $v_->mosque_cnt = 0;
                    $v_->square_cnt = 0;
                    if(sizeof($v->neighborhoods) == 1){
                        $v_->ratio = 100;
                    }

                    $v_->total  = round(($v->total  * $v_->ratio) / 100, 0) ;
                    $v_->amount = round(($v->amount * $v_->ratio) / 100, 0) ;

                    $v_->squares =\DB::table('char_aids_locations')
                        ->join('char_aids_locations_i18n', function ($q) use($language_id){
                            $q->on('char_aids_locations_i18n.location_id', '=', 'char_aids_locations.id');
                            $q->where('char_aids_locations_i18n.language_id','=',$language_id);
                        })
                        ->where(function($subQuery) use ($organization_id,$level,$v_) {
                            $subQuery->where('char_aids_locations.location_type',aidsLocation::TYPE_SQUARE);
                            $subQuery->where('char_aids_locations.parent_id',$v_->id);
                            if($level != Organization::LEVEL_MASTER_CENTER){
                                $subQuery->whereIn('char_aids_locations.id', function($query) use($organization_id) {
                                    $query->select('char_organization_locations.location_id')
                                        ->from('char_organization_locations')
                                        ->where('organization_id',$organization_id);
                                });
                            }
                        })
                        ->selectRaw("id,char_aids_locations_i18n.name as location_name,ratio,
                                    get_project_location_persons_count($project_id,1,2,id,$organization_id) as nominated")
                        ->get();

                    $v_->square_cnt = sizeof($v_->squares);

                    foreach ($v_->squares as $k2=>$v2){
                        if(sizeof($v_->squares) == 1){
                            $v2->ratio = 100;
                        }

                        $v2->total  = round(($v_->total  * $v2->ratio) / 100, 0) ;
                        $v2->amount = round(($v_->amount * $v2->ratio) / 100, 0) ;
                        $v2->mosques =
                            \DB::table('char_aids_locations')
                                ->join('char_aids_locations_i18n', function ($q) use($language_id){
                                    $q->on('char_aids_locations_i18n.location_id', '=', 'char_aids_locations.id');
                                    $q->where('char_aids_locations_i18n.language_id','=',$language_id);
                                })
                                ->where(function($subQuery) use ($organization_id,$level,$v2) {
                                    $subQuery->where('location_type',aidsLocation::TYPE_MOSQUES);
                                    $subQuery->where('parent_id',$v2->id);
                                    if($level != Organization::LEVEL_MASTER_CENTER){
                                        $subQuery->whereIn('id', function($query) use($organization_id) {
                                            $query->select('location_id')
                                                ->from('char_organization_locations')
                                                ->where('organization_id',$organization_id);
                                        });
                                    }
                                })
                                ->selectRaw("id,char_aids_locations_i18n.name as location_name,ratio,
                                                   get_project_location_persons_count($project_id,1,3,id,$organization_id) as nominated")
                                ->get();
                        $v2->mosque_cnt  = sizeof($v2->mosques) ;
                        $v_->mosque_cnt += sizeof($v2->mosques);

                        foreach ($v2->mosques as $k3=>$v3){
                            if(sizeof($v2->mosques) == 1){
                                $v3->ratio = 100;
                            }

                            $v3->total  = round(($v2->total  * $v3->ratio) / 100, 0) ;
                            $v3->amount = round(($v2->amount * $v3->ratio) / 100, 0) ;
                        }

                    }

                    unset($v_->type_name);
                    unset($v_->type_options);
                    $neighborhoods[]=$v_;
                }

            }
        }

        return $neighborhoods;
    }

    public static function getBranchesCountProject ($total,$amount,$project_id,$organization_id,$level){

        $language_id =  \App\Http\Helpers::getLocale();

        $return = [];

        $district =  self::query()
            ->join('char_aids_locations_i18n', function ($q) use($language_id){
                $q->on('char_aids_locations_i18n.location_id', '=', 'char_aids_locations.id');
                $q->where('char_aids_locations_i18n.language_id','=',$language_id);
            })
            ->where(function($subQuery) use ($organization_id,$level) {
                $subQuery->where('location_type',aidsLocation::TYPE_DISTRICT);
                $subQuery->whereIn('id', function($query) use($organization_id) {
                    $query->select('location_id')
                        ->from('char_organization_locations')
                        ->where('organization_id',$organization_id);
                });
            })
            ->selectRaw("id,char_aids_locations_i18n.name as location_name,ratio,
                                   get_project_location_persons_count($project_id,1,1,id,$organization_id) as nominated,
                                   get_project_location_persons_count($project_id,3,1,id,$organization_id) as accepted")
            ->get();

        foreach ($district as $key=>$value){
            if(sizeof($district) == 1){
                $value->ratio = 100;
            }

            $value->nominated = (int) $value->nominated ;
            $value->accepted = (int) $value->accepted ;
            $value->total = round(($total * $value->ratio) / 100, 0) ;
            $value->amount = round(($amount * $value->ratio) / 100, 0) ;
            $value->max = $value->total - $value->accepted;
            if($level == 3) {
                unset($value->type_name);
                unset($value->type_options);
                $return[] = $value;
            }
            else{

                $value->region = self::query()
                                 ->join('char_aids_locations_i18n', function ($q) use($language_id){
                                        $q->on('char_aids_locations_i18n.location_id', '=', 'char_aids_locations.id');
                                        $q->where('char_aids_locations_i18n.language_id','=',$language_id);
                                 })
                                 ->where(function($subQuery) use ($organization_id,$level) {
                                        $subQuery->where('location_type',aidsLocation::TYPE_REGION);
                                        $subQuery->whereIn('id', function($query) use($organization_id) {
                                            $query->select('location_id')
                                                ->from('char_organization_locations')
                                                ->where('organization_id',$organization_id);
                                        });
                                 })
                                 ->selectRaw("id,char_aids_locations_i18n.name as location_name,ratio,
                                                           get_project_location_persons_count($project_id,1,2,id,$organization_id) as nominated,
                                                           get_project_location_persons_count($project_id,3,2,id,$organization_id) as accepted")
                                 ->get();


                foreach ($value->region as $k=>$v){
                    if(sizeof($value->region) == 1){
                        $v->ratio = 100;
                    }

                    $v->nominated = (int) $v->nominated ;
                    $v->accepted = (int) $v->accepted ;
                    $v->total  = round(($value->total  * $v->ratio) / 100 , 0) ;
                    $v->amount = round(($value->amount * $v->ratio) / 100, 0) ;
                    $v->max = $v->total - $v->accepted;

                    if($level != 4) {
                        $v->neighborhoods = self::query()
                                            ->join('char_aids_locations_i18n', function ($q) use($language_id){
                                                $q->on('char_aids_locations_i18n.location_id', '=', 'char_aids_locations.id');
                                                $q->where('char_aids_locations_i18n.language_id','=',$language_id);
                                            })
                                            ->where(function($subQuery) use ($organization_id,$level,$v) {
                                                $subQuery->where('char_aids_locations.location_type',aidsLocation::TYPE_NEIGHBORHOOD);
                                                $subQuery->where('char_aids_locations.parent_id',$v->id);
                                                $subQuery->whereIn('char_aids_locations.id', function($query) use($organization_id) {
                                                    $query->select('char_organization_locations.location_id')
                                                        ->from('char_organization_locations')
                                                        ->where('organization_id',$organization_id);
                                                });
                                            })
                                            ->selectRaw("id,char_aids_locations_i18n.name as location_name,ratio,
                                                         get_project_location_persons_count($project_id,1,3,id,$organization_id) as nominated,
                                                         get_project_location_persons_count($project_id,3,3,id,$organization_id) as accepted")
                                            ->get();


                        foreach ($v->neighborhoods as $k_=>$v_){
                            if(sizeof($v->neighborhoods) == 1){
                                $v_->ratio = 100;
                            }

                            $v_->nominated = (int) $v_->nominated ;
                            $v_->accepted = (int) $v_->accepted ;
                            $v_->total  = round(($v->total  * $v_->ratio) / 100, 0) ;
                            $v_->amount = round(($v->amount * $v_->ratio) / 100, 0) ;
                            $v_->max = $v_->total - $v_->accepted;

                            if($level != 5) {
                                $v_->squares =  self::query()
                                                ->join('char_aids_locations_i18n', function ($q) use($language_id){
                                                    $q->on('char_aids_locations_i18n.location_id', '=', 'char_aids_locations.id');
                                                    $q->where('char_aids_locations_i18n.language_id','=',$language_id);
                                                })
                                                ->where(function($subQuery) use ($organization_id,$level,$v_) {
                                                    $subQuery->where('char_aids_locations.location_type',aidsLocation::TYPE_SQUARE);
                                                    $subQuery->where('char_aids_locations.parent_id',$v_->id);
                                                    $subQuery->whereIn('char_aids_locations.id', function($query) use($organization_id) {
                                                        $query->select('char_organization_locations.location_id')
                                                            ->from('char_organization_locations')
                                                            ->where('organization_id',$organization_id);
                                                    });
                                                })
                                                ->selectRaw("id,char_aids_locations_i18n.name as location_name,ratio,
                                                             get_project_location_persons_count($project_id,1,4,id,$organization_id) as nominated,
                                                             get_project_location_persons_count($project_id,3,4,id,$organization_id) as accepted")
                                                ->get();

                                foreach ($v_->squares as $k2=>$v2){
                                    if(sizeof($v_->squares) == 1){
                                        $v2->ratio = 100;
                                    }

                                    $v2->nominated = (int) $v2->nominated ;
                                    $v2->accepted = (int) $v2->accepted ;
                                    $v2->total  = round(($v_->total  * $v2->ratio) / 100, 0) ;
                                    $v2->amount = round(($v_->amount * $v2->ratio) / 100, 0) ;
                                    $v2->max = $v2->total - $v2->accepted;

                                    if($level != 6) {
                                         $v2->mosques = self::query()
                                                        ->join('char_aids_locations_i18n', function ($q) use($language_id){
                                                            $q->on('char_aids_locations_i18n.location_id', '=', 'char_aids_locations.id');
                                                            $q->where('char_aids_locations_i18n.language_id','=',$language_id);
                                                        })
                                                        ->where(function($subQuery) use ($organization_id,$v2) {
                                                            $subQuery->where('location_type',aidsLocation::TYPE_MOSQUES);
                                                            $subQuery->where('parent_id',$v2->id);
                                                            $subQuery->whereIn('id', function($query) use($organization_id) {
                                                                $query->select('location_id')
                                                                    ->from('char_organization_locations')
                                                                    ->where('organization_id',$organization_id);
                                                            });
                                                        })
                                                        ->selectRaw("id,
                                                                     char_aids_locations_i18n.name as location_name,ratio,
                                                                     get_project_location_persons_count($project_id,1,5,id,$organization_id) as nominated,
                                                                     get_project_location_persons_count($project_id,3,5,id,$organization_id) as accepted")
                                                        ->get();

                                        foreach ($v2->mosques as $k3=>$v3){
                                            if(sizeof($v2->mosques) == 1){
                                                $v3->ratio = 100;
                                            }

                                            $v3->nominated = (int) $v3->nominated ;
                                            $v3->accepted = (int) $v3->accepted ;
                                            $v3->total  = round(($v2->total  * $v3->ratio) / 100, 0) ;
                                            $v3->amount = round(($v2->amount * $v3->ratio) / 100, 0) ;
                                            $v3->max = $v3->total - $v3->accepted;

                                            unset($v3->type_name);
                                            unset($v3->type_options);
                                            $return[]=$v3;
                                        }
                                    }else{
                                        unset($v2->type_name);
                                        unset($v2->type_options);
                                        $return[]=$v2;
                                    }
                                }

                            }else{
                                unset($v_->type_name);
                                unset($v_->type_options);
                                $return[]=$v_;
                            }
                        }
                    }else{
                        unset($v->type_name);
                        unset($v->type_options);
                        $return[]=$v;
                    }
                }
            }
        }

        return $return;
    }

}
