<?php

namespace Setting\Model;

use Application\Model\I18nEntity;

class FurnitureStatusI18n extends I18nEntity
{
    protected $table = 'char_furniture_status_i18n';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['furniture_status_id', 'language_id', 'name'];
    
    public function getEntityIdColumn()
    {
        return 'furniture_status_id';
    }
}
