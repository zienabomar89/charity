<?php

namespace Setting\Model\Education;

use Application\Model\I18nEntity;

class AuthorityI18n extends I18nEntity
{
    protected $table = 'char_edu_authorities_i18n';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['edu_authority_id', 'language_id', 'name'];
    
    public function getEntityIdColumn()
    {
        return 'edu_authority_id';
    }
}

