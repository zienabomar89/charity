<?php

namespace Setting\Model\Education;

use Application\Model\I18nEntity;

class StageI18n extends I18nEntity
{
    protected $table = 'char_edu_stages_i18n';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['edu_stage_id', 'language_id', 'name'];
    
    public function getEntityIdColumn()
    {
        return 'edu_stage_id';
    }
}

