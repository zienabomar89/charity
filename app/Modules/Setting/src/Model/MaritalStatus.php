<?php

namespace Setting\Model;

use Application\Model\I18nableEntity;

class MaritalStatus extends I18nableEntity implements RankEntity
{
    protected $table = 'char_marital_status';
        
    public $timestamps = false;
    
    protected static $rules;
    
    public static function getValidatorRules()
    {
        if (!self::$rules) {
            self::$rules = array(
                'name' => 'required|min:3|max:255',
                'weight' => 'required',
            );
        }
        
        return self::$rules;
    }
    
    /**
     * 
     * @return I18nInterface
     */
    public function getI18nModel()
    {
        if ($this->i18nModel === null) {
            $this->i18nModel = new MaritalStatusI18n();
            $this->i18nModel->forceFill(array(
                $this->i18nModel->getEntityIdColumn() => $this->id,
                $this->i18nModel->getLanguageIdColumn() => $this->getLanguageId(),
            ));
        }
        
        return $this->i18nModel;
    }
    
    public function updateLinkedRank(RankEntity $old)
    {
        if ($old->weight == $this->weight) {
            return true;
        }
        
        $query = 'UPDATE `char_cases` SET
            `rank` = `rank` - :old + :new
            WHERE `person_id` in (
                SELECT `id` FROM `char_persons` WHERE `marital_status_id` = :id)';
        
        return $this->getConnection()->update($query, array(
            'old' => $old->weight,
            'new' => $this->weight,
            'id' => $this->id,
        ));
    }

}
