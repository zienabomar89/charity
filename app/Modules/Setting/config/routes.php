<?php

Route::group(['middleware' => ['api','lang','auth.check'], 'prefix' => 'api/v1.0/common' ,'namespace' => 'Setting\Controller'], function() {

    Route::get('project-category/{id}/translations', 'ProjectCategoryController@translations');
    Route::post('project-category/{id}/translations', 'ProjectCategoryController@saveTranslations');
    Route::resource('project-category', 'ProjectCategoryController');
    Route::get('project-category/{id}/sub/list', 'ProjectCategoryController@subList');

    Route::get('sub/{id}/translations', 'ProjectCategoryController@translations');
    Route::post('sub/{id}/translations', 'ProjectCategoryController@saveTranslations');
    Route::resource('project-category/{id}/sub', 'ProjectCategoryController');

    Route::get('organizations-category/{id}/translations', 'OrganizationsCategoryController@translations');
    Route::post('organizations-category/{id}/translations', 'OrganizationsCategoryController@saveTranslations');
    Route::get('organizations-category/list', 'OrganizationsCategoryController@getList');
    Route::resource('organizations-category', 'OrganizationsCategoryController');

    Route::get('project-type/{id}/translations', 'ProjectTypeController@translations');
    Route::post('project-type/{id}/translations', 'ProjectTypeController@saveTranslations');
    Route::get('project-type/list', 'ProjectTypeController@getList');
    Route::resource('project-type', 'ProjectTypeController');

    Route::get('project-region/{id}/translations', 'ProjectRegionController@translations');
    Route::post('project-region/{id}/translations', 'ProjectRegionController@saveTranslations');
    Route::get('project-region/list', 'ProjectRegionController@getList');
    Route::resource('project-region', 'ProjectRegionController');

    Route::get('project-beneficiary-category/{id}/translations', 'ProjectBeneficiaryCategoryController@translations');
    Route::post('project-beneficiary-category/{id}/translations', 'ProjectBeneficiaryCategoryController@saveTranslations');
    Route::get('project-beneficiary-category/list', 'ProjectBeneficiaryCategoryController@getList');
    Route::resource('project-beneficiary-category', 'ProjectBeneficiaryCategoryController');

    Route::get('project-activities/{id}/translations', 'ProjectActivitiesController@translations');
    Route::post('project-activities/{id}/translations', 'ProjectActivitiesController@saveTranslations');
    Route::get('project-activities/list', 'ProjectActivitiesController@getList');
    Route::resource('project-activities', 'ProjectActivitiesController');

    Route::get('kinship/export', 'KinshipController@export')->name('Kinship.export');
    Route::get('locations/export', 'LocationsController@export')->name('Locations.export');
    Route::get('locations/get_type_location/{id}', 'LocationsController@typeList');

    Route::get('transfer-company/list', 'TransferCompanyController@getList');
    Route::resource('transfer-company', 'TransferCompanyController');

    Route::get('BankChequeTemplate/pdf/{id}', 'BankChequeTemplateController@pdf');
    Route::resource('BankChequeTemplate', 'BankChequeTemplateController');

    Route::get('payment-category/{id}/translations', 'PaymentCategoryController@translations');
    Route::post('payment-category/{id}/translations', 'PaymentCategoryController@saveTranslations');
    Route::get('payment-category/list', 'PaymentCategoryController@getList');
    Route::resource('payment-category', 'PaymentCategoryController');

    Route::post('setting/getSysSetting/', 'SettingController@getSysSetting');
    Route::get('setting/getAncestorSetting/{id}', 'SettingController@getAncestorSetting');
    Route::post('setting/setCustomForm/', 'SettingController@setCustomForm');
    Route::get('setting/getCustomForm/{id}', 'SettingController@getCustomForm');
    Route::get('setting/getDefaultbyId/{id}', 'SettingController@getDefaultbyId');
    Route::get('setting/getSettingById/{id}', 'SettingController@getSettingById');
    Route::resource('setting', 'SettingController');

    Route::get('building-status/list', 'BuildingStatusController@getList');
    Route::get('building-status/{id}/translations', 'BuildingStatusController@translations');
    Route::post('building-status/{id}/translations', 'BuildingStatusController@saveTranslations');
    Route::resource('building-status', 'BuildingStatusController');

    Route::get('furniture-status/list', 'FurnitureStatusController@getList');
    Route::get('furniture-status/{id}/translations', 'FurnitureStatusController@translations');
    Route::post('furniture-status/{id}/translations', 'FurnitureStatusController@saveTranslations');
    Route::resource('furniture-status', 'FurnitureStatusController');

    Route::get('habitable-status/list', 'HabitableStatusController@getList');
    Route::get('habitable-status/{id}/translations', 'HabitableStatusController@translations');
    Route::post('habitable-status/{id}/translations', 'HabitableStatusController@saveTranslations');
    Route::resource('habitable-status', 'HabitableStatusController');

    Route::get('house-status/list', 'HouseStatusController@getList');
    Route::get('house-status/{id}/translations', 'HouseStatusController@translations');
    Route::post('house-status/{id}/translations', 'HouseStatusController@saveTranslations');
    Route::resource('house-status', 'HouseStatusController');

    Route::get('entities', 'EntitiesController@index');

    Route::get('aid-sources/list', 'AidSourcesController@getList');
    Route::resource('aid-sources', 'AidSourcesController');
    Route::resource('aid-sources/{id}/weights', 'AidSourceWeightsController');

    Route::get('banks/templateInstruction/','BanksController@templateInstruction');
    Route::post('banks/template-upload/{id}','BanksController@templateUpload');
    Route::get('banks/template/{id}','BanksController@template');
    Route::get('banks/list', 'BanksController@getList');
    Route::resource('banks', 'BanksController');

    Route::get('banks/{id}/branches/list', 'BankBranchesController@getList');
    Route::resource('banks/{id}/branches', 'BankBranchesController');

    Route::get('currencies/list', 'CurrenciesController@getList');
    Route::resource('currencies', 'CurrenciesController');

    Route::get('death-causes/list', 'DeathCausesController@getList');
    Route::get('death-causes/{id}/translations', 'DeathCausesController@translations');
    Route::post('death-causes/{id}/translations', 'DeathCausesController@saveTranslations');
    Route::resource('death-causes', 'DeathCausesController');

    Route::get('diseases/list', 'DiseasesController@getList');
    Route::get('diseases/{id}/translations', 'DiseasesController@translations');
    Route::post('diseases/{id}/translations', 'DiseasesController@saveTranslations');
    Route::resource('diseases', 'DiseasesController');

    Route::get('document-types/list', 'DocumentTypesController@getList');
    Route::get('document-types/{id}/translations', 'DocumentTypesController@translations');
    Route::post('document-types/{id}/translations', 'DocumentTypesController@saveTranslations');
    Route::resource('document-types', 'DocumentTypesController');

    Route::get('education-authorities/list', 'Education\AuthoritiesController@getList');
    Route::get('education-authorities/{id}/translations', 'Education\AuthoritiesController@translations');
    Route::post('education-authorities/{id}/translations', 'Education\AuthoritiesController@saveTranslations');
    Route::resource('education-authorities', 'Education\AuthoritiesController');

    Route::get('education-degrees/list', 'Education\DegreesController@getList');
    Route::get('education-degrees/{id}/translations', 'Education\DegreesController@translations');
    Route::post('education-degrees/{id}/translations', 'Education\DegreesController@saveTranslations');
    Route::resource('education-degrees', 'Education\DegreesController');

    Route::get('education-stages/list', 'Education\StagesController@getList');
    Route::get('education-stages/{id}/translations', 'Education\StagesController@translations');
    Route::post('education-stages/{id}/translations', 'Education\StagesController@saveTranslations');
    Route::resource('education-stages', 'Education\StagesController');

    Route::get('essentials/list', 'EssentialsController@getList');
    Route::resource('essentials', 'EssentialsController');
    Route::resource('essentials/{id}/weights', 'EssentialWeightsController', [
        'only' => ['index', 'store']
    ]);

    Route::get('kinship/list', 'KinshipController@getList');
    Route::get('kinship/{id}/translations', 'KinshipController@translations');
    Route::post('kinship/{id}/translations', 'KinshipController@saveTranslations');
    Route::resource('kinship', 'KinshipController');

    Route::get('languages/list', 'LanguagesController@getList');
    Route::resource('languages', 'LanguagesController');


    Route::get('countries/list', 'LocationsController@countriesList')->name('Locations.countriesList');
    Route::get('countries/{id}/translations', 'LocationsController@translations');
    Route::post('countries/{id}/translations', 'LocationsController@saveTranslations');
    Route::resource('countries', 'LocationsController');
    Route::get('countries/{id}/districts/list', 'LocationsController@districtsList')->name('Locations.districtsList');

    Route::get('districts/{id}/translations', 'LocationsController@translations');
    Route::post('districts/{id}/translations', 'LocationsController@saveTranslations');
    Route::resource('countries/{id}/districts', 'LocationsController');

    Route::get('districts/{id}/cities/list', 'LocationsController@citiesList')->name('Locations.citiesList');
    Route::get('cities/{id}/translations', 'LocationsController@translations');
    Route::post('cities/{id}/translations', 'LocationsController@saveTranslations');
    Route::resource('districts/{id}/cities', 'LocationsController');

    Route::get('cities/{id}/neighborhoods/list', 'LocationsController@neighborhoodsList')->name('Locations.neighborhoodsList');
    Route::get('neighborhoods/{id}/translations', 'LocationsController@translations');
    Route::post('neighborhoods/{id}/translations', 'LocationsController@saveTranslations');
    Route::resource('cities/{id}/neighborhoods', 'LocationsController');

    Route::get('neighborhoods/{id}/mosques/list', 'LocationsController@mosquesList')->name('Locations.mosquesList');
    Route::get('mosques/{id}/translations', 'LocationsController@translations');
    Route::post('mosques/{id}/translations', 'LocationsController@saveTranslations');
    Route::resource('neighborhoods/{id}/mosques', 'LocationsController');

    Route::get('marital-status/list', 'MaritalStatusController@getList');
    Route::get('marital-status/{id}/translations', 'MaritalStatusController@translations');
    Route::post('marital-status/{id}/translations', 'MaritalStatusController@saveTranslations');
    Route::resource('marital-status', 'MaritalStatusController');

    Route::get('properties/list', 'PropertiesController@getList');
    Route::get('properties/{id}/translations', 'PropertiesController@translations');
    Route::post('properties/{id}/translations', 'PropertiesController@saveTranslations');
    Route::resource('properties', 'PropertiesController');
    Route::resource('properties/{id}/weights', 'PropertyWeightsController');

    Route::get('property-types/list', 'PropertyTypesController@getList');
    Route::get('property-types/{id}/translations', 'PropertyTypesController@translations');
    Route::post('property-types/{id}/translations', 'PropertyTypesController@saveTranslations');
    Route::resource('property-types', 'PropertyTypesController');

    Route::get('roof-materials/list', 'RoofMaterialsController@getList');
    Route::get('roof-materials/{id}/translations', 'RoofMaterialsController@translations');
    Route::post('roof-materials/{id}/translations', 'RoofMaterialsController@saveTranslations');
    Route::resource('roof-materials', 'RoofMaterialsController');

    Route::get('work-jobs/list', 'Work\JobsController@getList');
    Route::get('work-jobs/{id}/translations', 'Work\JobsController@translations');
    Route::post('work-jobs/{id}/translations', 'Work\JobsController@saveTranslations');
    Route::resource('work-jobs', 'Work\JobsController');

    Route::get('work-reasons/list', 'Work\ReasonsController@getList');
    Route::get('work-reasons/{id}/translations', 'Work\ReasonsController@translations');
    Route::post('work-reasons/{id}/translations', 'Work\ReasonsController@saveTranslations');
    Route::resource('work-reasons', 'Work\ReasonsController');

    Route::get('work-status/list', 'Work\StatusController@getList');
    Route::get('work-status/{id}/translations', 'Work\StatusController@translations');
    Route::post('work-status/{id}/translations', 'Work\StatusController@saveTranslations');
    Route::resource('work-status', 'Work\StatusController');

    Route::get('work-wages/list', 'Work\WagesController@getList');
    Route::resource('work-wages', 'Work\WagesController');

    Route::get('Backup/download/{id}', 'BackupController@download');
    Route::post('Backup/filter', 'BackupController@filter')->name('Backup.filter');
    Route::resource('Backup', 'BackupController');

   // *********************************  aid addressing ********************************** //

    Route::put('aidsLocations/transfer/{id}', 'aidsLocationsController@transfer')->name('aidsLocations.transfer');
    Route::get('aidsLocations/tree/{id}', 'aidsLocationsController@tree')->name('aidsLocations.tree');
    Route::get('scountries/list', 'aidsLocationsController@countriesList')->name('aidsLocations.countriesList');
    Route::get('scountries/{id}/translations', 'aidsLocationsController@translations');
    Route::post('scountries/{id}/translations', 'aidsLocationsController@saveTranslations');
    Route::resource('scountries', 'aidsLocationsController');

    Route::get('scountries/{id}/sdistricts/list', 'aidsLocationsController@districtsList')->name('aidsCountries.districtsList');

    Route::get('sdistricts/{id}/translations', 'aidsLocationsController@translations');
    Route::post('sdistricts/{id}/translations', 'aidsLocationsController@saveTranslations');
    Route::resource('scountries/{id}/sdistricts', 'aidsLocationsController');

    Route::get('sdistricts/{id}/sregions/list', 'aidsLocationsController@regionsList')->name('aidsCountries.regionsList');
    Route::get('sregions/{id}/translations', 'aidsLocationsController@translations');
    Route::post('sregions/{id}/translations', 'aidsLocationsController@saveTranslations');
    Route::resource('sdistricts/{id}/sregions', 'aidsLocationsController');

    Route::get('sregions/{id}/sneighborhoods/list', 'aidsLocationsController@neighborhoodsList')->name('aidsCountries.neighborhoodsList');
    Route::get('sneighborhoods/{id}/translations', 'aidsLocationsController@translations');
    Route::post('sneighborhoods/{id}/translations', 'aidsLocationsController@saveTranslations');
    Route::resource('sregions/{id}/sneighborhoods', 'aidsLocationsController');

    Route::get('sneighborhoods/{id}/ssquares/list', 'aidsLocationsController@squaresList')->name('aidsCountries.squaresList');
    Route::get('ssquares/{id}/translations', 'aidsLocationsController@translations');
    Route::post('ssquares/{id}/translations', 'aidsLocationsController@saveTranslations');
    Route::resource('sneighborhoods/{id}/ssquares', 'aidsLocationsController');

    Route::get('ssquares/{id}/smosques/list', 'aidsLocationsController@mosquesList')->name('aidsCountries.mosquesList');
    Route::get('smosques/{id}/translations', 'aidsLocationsController@translations');
    Route::post('smosques/{id}/translations', 'aidsLocationsController@saveTranslations');
    Route::resource('ssquares/{id}/smosques', 'aidsLocationsController');

});
