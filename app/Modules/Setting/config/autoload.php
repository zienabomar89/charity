<?php

$loader = new Composer\Autoload\ClassLoader();
$loader->addPsr4('Setting\\', __DIR__ . '/../src');
$loader->register();