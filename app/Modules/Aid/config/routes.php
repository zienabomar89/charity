<?php

Route::group(['middleware' => ['api','lang','auth.check'], 'prefix' => 'api/v1.0/aids' ,'namespace' => 'Aid\Controller'], function() {

    Route::post ('testJob','testJob@test');
    Route::resource('neighborhoods_ratios', 'NeighborhoodsRatiosController');
    //---------------------------------------------------------------------------------------------------//

    Route::group(['prefix' => 'social_search'], function() {

        Route::post('export-group','SocialSearchCasesController@exportGroup');
        Route::post('import-banks','SocialSearchCasesController@importBanks');
        Route::post('import-essentials','SocialSearchCasesController@importEssentials');
        Route::post('import-properties','SocialSearchCasesController@importProperties');
        Route::post('import-fin-aids','SocialSearchCasesController@importFinAids');
        Route::post('import-non-fin-aids','SocialSearchCasesController@importNonFinAids');

        Route::post('import-cases','SocialSearchCasesController@importCases');
        Route::post('import-family','SocialSearchCasesController@importFamily');

        Route::get('pdf/{id}','SocialSearchCasesController@pdf');
        Route::get('word/{id}','SocialSearchCasesController@word');
        Route::get('form/{id}','SocialSearchCasesController@form');
        Route::post('cases','SocialSearchCasesController@cases');

        Route::get('template/{id}','SocialSearchController@template');
        Route::post('template-upload','SocialSearchController@templateUpload');
        Route::get('templates','SocialSearchController@templates');

        Route::post('status','SocialSearchController@status');
        Route::put('refresh/{id}','SocialSearchController@refresh');
        Route::put('restore/{id}','SocialSearchController@restore');
        Route::post('filter','SocialSearchController@filter');
        Route::get('list','SocialSearchController@getList');

    });
    Route::resource('social_search', 'SocialSearchController');
    //---------------------------------------------------------------------------------------------------//
    Route::group(['prefix' => 'vouchers_categories'], function() {
        Route::put('updateSub/{id}','VoucherCategoriesController@updateSub');
        Route::get('getSiblingsCategory/{id}','VoucherCategoriesController@getSiblingsCategory');
        Route::get('getParentsCategory/','VoucherCategoriesController@getParentsCategory');
        Route::get('list/','VoucherCategoriesController@getList');
    });
    Route::resource('vouchers_categories', 'VoucherCategoriesController');
    //---------------------------------------------------------------------------------------------------//
    Route::post('vouchers/beneficiary/excel/cases','ExportController@cases');
    Route::post('vouchers/beneficiary/excel/individuals','ExportController@individuals');

    Route::group(['prefix' => 'cs_persons_vouchers'], function() {

        Route::post('trashed/','VouchersController@trashed');
        Route::put('restore/{id}','VouchersController@restore');

        Route::get('vouchersTitleList','VouchersController@vouchersTitleList')->name('api.v1.0.aids.Vouchers.vouchersTitleList');
        Route::get('vouchersCodeList','VouchersController@vouchersCodeList')->name('api.v1.0.aids.Vouchers.vouchersCodeList');
        Route::get('codesList','VouchersController@codesList')->name('api.v1.0.aids.Vouchers.codesList');
        Route::post('deleteSerial','VouchersController@deleteSerial')->name('api.v1.0.aids.Vouchers.deleteSerial');

        Route::post('import','VouchersController@import');
        Route::get('getVouchers/{id}','VouchersController@getVouchers')->name('api.v1.0.aids.Vouchers.getVouchers');
        Route::post('updateSerial','VouchersController@updateSerial')->name('api.v1.0.aids.Vouchers.updateSerial');
        Route::post('importBeneficiary','ExportController@importBeneficiary')->name('api.v1.0.aids.Export.importBeneficiary');
        Route::post('setBeneficiary','ExportController@setBeneficiary');

        Route::get('getAttachments/{id}','VouchersController@getAttachments');
        Route::get('getBeneficiaryAttachments/{id}','VouchersController@getBeneficiaryAttachments');
        Route::post('setAttachment','VouchersController@setAttachment');
        Route::post('statistics','VouchersController@statistics');
        Route::post('persons','VouchersController@persons');
        Route::post('cases','VouchersController@cases');
        Route::post('createToken','VouchersController@createToken');
        Route::post('activateDeactivate','VouchersController@activateDeactivate');

        Route::post('upload','VouchersController@attachmentUpload');
        Route::get ('attachments/{id}','VouchersController@attachments');
        Route::put('reset/{id}','VouchersController@reset');
        Route::put('setStatus/{id}','VouchersController@setStatus');        
        Route::get('default-import-template/','VouchersController@importTemplate');
        Route::get('sys-default-template/','VouchersController@SysDefaultTemplate');
        Route::get('templateInstruction/','VouchersController@templateInstruction');
        Route::get('default-template/{id}','VouchersController@defaultTemplate');
        Route::get('template/{id}','VouchersController@template');
        Route::get ('export/{id}','VouchersController@export');
        Route::get('exportToWord/{person_id}/{voucher_id}','VouchersController@exportToWord');
        Route::get('exportToPDF/{person_id}/{voucher_id}','VouchersController@exportToPDF');
        Route::get('list/{id}','VouchersController@getList');
        Route::get('getPersonsList/{id}','VouchersController@getPersonsList');
        Route::post('default-template-upload','VouchersController@defaultTemplateUpload');
        Route::post('individualVoucher','VouchersController@individualVoucher');
        Route::post('storePersonsVoucher/','VouchersController@storePersonsVoucher');
        Route::post('storePersonsVoucherByExcel','VouchersController@storePersonsVoucherByExcel');
        Route::post('template-upload','VouchersController@templateUpload');
        Route::post('all/','VouchersController@all');
        Route::put('updatePersonsVoucherDetails/{id}','VouchersController@updatePersonsVoucherDetails');
        Route::put('updateStatus/{id}','VouchersController@updateStatus');
        Route::post('setDetails','VouchersController@setDetails');
        Route::delete('deletePersonsVoucher/{id}','VouchersController@deletePersonsVoucher');
    });
    Route::resource('cs_persons_vouchers', 'VouchersController');

});

?>
