<?php

$loader = new Composer\Autoload\ClassLoader();
$loader->addPsr4('Aid\\', __DIR__ . '/../src');
$loader->register();