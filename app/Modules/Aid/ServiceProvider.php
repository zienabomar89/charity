<?php

namespace App\Modules\Aid;

class ServiceProvider extends \App\Providers\AuthServiceProvider
{

    public function boot()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/config/routes.php';
        }
        $this->loadViewsFrom(__DIR__ . '/views', 'aid');
        $this->loadTranslationsFrom(__DIR__.'/languages', 'aid');

        require __DIR__ . '/config/autoload.php';
        $this->registerPolicies();
    }

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \Aid\Model\Vouchers::class => \Aid\Policy\VouchersPolicy::class,
        \Aid\Model\VoucherCategories::class => \Aid\Policy\VoucherCategoriesPolicy::class,
        \Aid\Model\NeighborhoodsRatios::class => \Aid\Policy\NeighborhoodsRatiosPolicy::class,
        \Aid\Model\SocialSearch\SocialSearch::class => \Aid\Policy\SocialSearchPolicy::class,
    ];

    public function register()
    {
        /*$this->mergeConfigFrom(
            __DIR__ . '/config/config.php', 'user'
        );*/
    }

}
