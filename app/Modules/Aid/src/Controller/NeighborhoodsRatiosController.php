<?php

namespace aid\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Aid\Model\NeighborhoodsRatios;

class NeighborhoodsRatiosController  extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get logged user organization neighborhoods ratios
    public function index()
    {
        $this->authorize('manage', NeighborhoodsRatios::class);
        $user = \Auth::user();
        $returns= NeighborhoodsRatios::getRatios($user->organization_id);
        if($returns['status'] == false){
            return response()->json(['status' =>false]);
        }

        $cases= \Common\Model\AidsCases::where(['organization_id' => $user->organization_id ,'status'=>0])->count();
        return response()->json(['status' =>true,'Ratios' =>$returns['rows'],'sum'=>$returns['sum'],'count'=>$returns['count'],'cases'=>$cases]);
    }

    // save ratio of logged user organization neighborhoods
    public function update(Request $request, $id)
    {
        $response = array();
        $this->authorize('update', NeighborhoodsRatios::class);
        $user = \Auth::user();
        $rows =$request->get('rows');
        $done=0;
        NeighborhoodsRatios::where('organization_id',$user->organization_id)->delete();

        foreach($rows as $k=>$v){
            if($v['ratio'] != null){
                if($v['ratio'] != 0 ||$v['ratio'] != null ){
                    NeighborhoodsRatios::create(['ratio' =>$v['ratio'],'location_id' =>$v['location_id'],'organization_id' =>$user->organization_id]);
                    $done++;
                }
            }
        }

        if($done != 0) {
            \Log\Model\Log::saveNewLog('NEIGHBORHOODS_RTIOS_UPDATED',
                                        trans('aid::application.Edited the eligibility ratios for the areas of the organization') );
            $response["status"]= 'success';
            $response["msg"]= trans('setting::application.The row is updated');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('setting::application.There is no update');
        }
        return response()->json($response);

    }

}
