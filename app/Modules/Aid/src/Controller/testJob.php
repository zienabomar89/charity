<?php

namespace aid\Controller;

use App\Http\Controllers\Controller;
use App\Jobs\getData;
use App\Jobs\ImportPersonsVouchers;
use Common\Model\Person;
use Illuminate\Http\Request;
use Excel;
use Aid\Model\VoucherPersons;
use App\Http\Helpers;
class testJob  extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function test(Request $request)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','-1');

        $importFile=$request->file ;
        if ($importFile->isValid()) {
            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){
                        $validFile = false;
                        foreach ($keys as $item) {
                            if ( $item == 'rkm_alhoy' ) { $validFile = true; break; }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);
                            if ($total > 0) {
                                $permission = \Common\Model\CaseModel::hasPermission('reports.case.RelayCitizenRepository') ;
                                getData::dispatch($records,$request->type,$permission);
                                return response()->json(['status' => 'success','msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.All numbers successfully checked')]);
                            }
                           return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }

    public function persons(Request $request)
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','-1');

        $voucher_id = 1951;
        $status = 1;

        $importFile = $request->file;

        if ($importFile->isValid()) {
            $path = $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load($path);
            $sheets = $excel->getSheetNames();
            if(in_array('data', $sheets)) {
                $first = Excel::selectSheets('data')->load($path)->first();
                if (!is_null($first)) {
                    $validFile = true;
                    if ($validFile) {
                        $records = \Excel::selectSheets('data')->load($path)->get();
                        if(sizeof($records) > 0 ){
                            ImportPersonsVouchers::dispatch($voucher_id, $status, $records );
                            // dispatch(new ImportPersonsVouchers($list));
                            return response()->json(['status' => 'success','msg' => trans('aid::application.The voucher row is inserted to db')]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file is empty')]);
                    }
                    return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                }
                return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file is empty')]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('aid::application.The import sheet does not exist in the Excel , please check the label in the instructions or in the template') ]);
        }
        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.invalid file')]);


    }

    public function import(Request $request)
    {
        try {
            ini_set('max_execution_time',0);
            set_time_limit(0);
            Ini_set('memory_limit','2048M');
            $this->authorize('createPersonsVoucher', Vouchers::class);

            $user = \Auth::user();

            if($request->get('source') == 'file'){

                if ($user->hasPermission('aid.voucher.updateSerial')) {
                    if($request->un_serial){
                        $un_count = Vouchers::where(['un_serial'=>$request->un_serial])->count();
                        if($un_count > 0){
                            return response()->json(['error' => trans('aid::application.must_unique')], 422);
                        }
                    }
                }

                if($request->center == '1' || $request->center == '1'){
                    return $this->import($request);
                }
                else{
                    $path = \Input::file('file')->getRealPath();
                    if (\Input::hasFile('file')) {
                        $category_id = $request->case_category_id;
                        $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$category_id)->first();

                        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
                        $objReader->setReadDataOnly(true);
                        $excel = $objReader->load($path);
                        $sheets = $excel->getSheetNames();

                        if(\App\Http\Helpers::sheetFound("data",$sheets)) {
                            $first=Excel::selectSheets('data')->load($path)->first();

                            if(!is_null($first)){

                                $keys = $first->keys()->toArray();
                                if(sizeof($keys) >0 ){
                                    $validFile=true;
                                    $constraint =  ["rkm_alhoy", "kym_alksym", "mhto_alksym", "mkan_alastlam", "tarykh_alastlam","okt_alastlam"];

                                    foreach ($constraint as $item) {
                                        if ( !in_array($item, $keys) ) {
                                            $validFile = false;
                                            break;
                                        }
                                    }

                                    if($validFile){
                                        $user = \Auth::user();
                                        $add_cases = $request->add_cases;
                                        $response = array();
                                        $row=0;
                                        $case_added = 0;
                                        $not_card_number=[];
                                        $policy_restricted=[];
                                        $success=0;
                                        $not_case=[];
                                        $out_of_regions=[];
                                        $not_person=[];
                                        $duplicated=[];
                                        $values_keys=[];
                                        $persons=[];
                                        $vouchers=[];
                                        $result=[];
                                        $beneficiaries=[];
                                        $path = \Input::file('file')->getRealPath();
                                        $data = \Excel::selectSheets('data')
                                            ->load($path, function ($reader) {$reader->formatDates(true, 'Y-m-d');})->get();
                                        $total=sizeof($data);
                                        if ($total > 0) {
                                            foreach ($data as $key => $value) {
                                                $index=null;
                                                $voucher_content=null;
                                                $voucher_value=null;

                                                if(isset($value->kym_alksym)){
                                                    if ($value->kym_alksym) {
                                                        $voucher_value=$value->kym_alksym;
                                                        $index = $value->kym_alksym;

                                                        if (isset($value->mhto_alksym)) {
                                                            if ($value->mhto_alksym) {
                                                                $index = $index . '-' . $value->mhto_alksym;
                                                                $voucher_content=$value->mhto_alksym;
                                                            }
                                                        }
                                                    }
                                                }


                                                if($index){

                                                    if (!in_array($index, $values_keys)) {
                                                        $values_keys[] = $index;
                                                    }
                                                    if (!isset($persons[$index])) {
                                                        $persons[$index] = [];
                                                    }
                                                    if (!isset($vouchers[$index])) {
                                                        $vouchers[$index]['value']=$voucher_value;
                                                        if($voucher_content){
                                                            $vouchers[$index]['content']=$voucher_content;
                                                        }
                                                    }

                                                    if ($value->rkm_alhoy){
                                                        $value->rkm_alhoy = (int)$value->rkm_alhoy;
                                                        if ($value->rkm_alhoy != "" && strlen($value->rkm_alhoy) == 9){
                                                            if(!in_array($value->rkm_alhoy,$persons[$index])) {
                                                                $isPass=VoucherPersons::checkPersons('id_card_number',$value->rkm_alhoy,null,$user->organization_id,$voucher_value,$user->id);

                                                                if ($isPass['status'] == true) {

                                                                    if(!in_array($isPass['id'],$persons[$index])) {
                                                                        $persons[$index][]=$isPass['id'];
                                                                        $details['person_id']=$isPass['id'];
                                                                        if ($value->mkan_alastlam){
                                                                            if ($value->mkan_alastlam != "" &&$value->mkan_alastlam != "-") {
                                                                                $details['receipt_location']=$value->mkan_alastlam;
                                                                            }
                                                                        }
                                                                        if ($value->okt_alastlam){
                                                                            if (!is_null($value->okt_alastlam ) && $value->okt_alastlam != "" && $value->okt_alastlam != "-") {
                                                                                if (is_object($value['okt_alastlam'])) {
                                                                                    $details['receipt_time'] = date('H:i:s',strtotime($value['okt_alastlam']));
                                                                                }
                                                                            }
                                                                        }

                                                                        if ($value->tarykh_alastlam){
                                                                            if ($value->tarykh_alastlam != "" &&$value->tarykh_alastlam != "-") {
//                                                                            $details['receipt_date']=date('Y-m-d',strtotime($value->tarykh_alastlam));
                                                                                $receipt_date = Helpers::getFormatedDate($value->tarykh_alastlam);

                                                                                if (!is_null($receipt_date)) {
                                                                                    $details['receipt_date']=$receipt_date;
                                                                                }
                                                                            }
                                                                        }

                                                                        if (!isset($vouchers[$index]['persons'])) {
                                                                            $vouchers[$index]['persons']=[];
                                                                        }

                                                                        array_push($vouchers[$index]['persons'],$details);
                                                                        $beneficiaries[]=['name' => $isPass['name'],'id_card_number' => $value->rkm_alhoy,'status'=>trans('aid::application.old beneficiary')];
                                                                        $success++;
                                                                    }else{
                                                                        $duplicated[]=$value->rkm_alhoy;
                                                                    }


                                                                }
                                                                else{
                                                                    if ($isPass['reason'] =='policy_restricted') {
                                                                        if(!in_array($value->rkm_alhoy,$policy_restricted)){
                                                                            $policy_restricted[]=$value->rkm_alhoy;
                                                                        }else{
                                                                            $duplicated[]=$value->rkm_alhoy;
                                                                        }

                                                                        $result[]=['id_card_number' =>$value->rkm_alhoy,'reason'=>'policy_restricted'];
                                                                    }
                                                                    if ($isPass['reason'] =='out_of_regions') {
                                                                        if(!in_array($value->rkm_alhoy,$out_of_regions)){
                                                                            $out_of_regions[]=$value->rkm_alhoy;
                                                                        }else{
                                                                            $duplicated[]=$value->rkm_alhoy;
                                                                        }
                                                                        $result[]=['name' => $isPass['name'],'id_card_number' => $value->rkm_alhoy,'reason'=>'out_of_regions'];
                                                                    }

                                                                    if ($isPass['reason'] =='not_case') {
                                                                        if(!in_array($value->rkm_alhoy,$not_case)){

                                                                            if($add_cases == true || $add_cases == 'true' ){
                                                                                $id_card_number = $value->rkm_alhoy;
                                                                                $temp = ['name' => $isPass['name'], 'id' => $isPass['id'], 'id_card_number' => $id_card_number , 'receipt_time'=>null];

                                                                                $case=  \Common\Model\AidsCases::create(['person_id' =>  $isPass['id'],
                                                                                    'status' => 1, 'category_id' => $category_id,
                                                                                    'created_at' => date('Y-m-d H:i:s'),
                                                                                    'user_id' => $user->id,
                                                                                    'rank' => 0,
                                                                                    'organization_id' => $user->organization_id]);

                                                                                \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id, 'reason'=>trans('common::application.case was created'), 'status'=>0, 'date'=>date("Y-m-d")]);
                                                                                \Log\Model\Log::saveNewLog('CASE_CREATED',trans('common::application.Has added new case data for')  . '  : '.' "'. ' ' .  $isPass['name'] . ' " ' . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);

                                                                                $persons[$index][]=$isPass['id'];
                                                                                $details['person_id']=$isPass['id'];
                                                                                if ($value->mkan_alastlam){
                                                                                    if ($value->mkan_alastlam != "" &&$value->mkan_alastlam != "-") {
                                                                                        $details['receipt_location']=$value->mkan_alastlam;
                                                                                    }
                                                                                }
                                                                                if ($value->okt_alastlam){
                                                                                    if (!is_null($value->okt_alastlam ) && $value->okt_alastlam != "" && $value->okt_alastlam != "-") {
                                                                                        if (is_object($value['okt_alastlam'])) {
                                                                                            $details['receipt_time'] = date('H:i:s',strtotime($value['okt_alastlam']));
                                                                                        }
                                                                                    }
                                                                                }

                                                                                if ($value->tarykh_alastlam){
                                                                                    if ($value->tarykh_alastlam != "" &&$value->tarykh_alastlam != "-") {
//                                                                                  $details['receipt_date']=date('Y-m-d',strtotime($value->tarykh_alastlam));
                                                                                        $receipt_date = Helpers::getFormatedDate($value->tarykh_alastlam);

                                                                                        if (!is_null($receipt_date)) {
                                                                                            $details['receipt_date']=$receipt_date;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                if (!isset($vouchers[$index]['persons'])) {
                                                                                    $vouchers[$index]['persons']=[];
                                                                                }

                                                                                array_push($vouchers[$index]['persons'],$details);
                                                                                $temp['status'] =trans('aid::application.new beneficiary');
                                                                                $beneficiaries[]=$temp;
                                                                                $success++;
                                                                            }else{
                                                                                $not_case[]=$value->rkm_alhoy;
                                                                            }
                                                                        }else{
                                                                            $duplicated[]=$value->rkm_alhoy;
                                                                        }

                                                                        $result[]=['id_card_number' =>$value->rkm_alhoy,'reason'=>'not_case'];
                                                                    }
                                                                    if ($isPass['reason'] =='not_person') {
                                                                        if(!in_array($value->rkm_alhoy,$not_person)){
                                                                            $not_person[]=$value->rkm_alhoy;
                                                                        }else{
                                                                            $duplicated[]=$value->rkm_alhoy;
                                                                        }

                                                                        $result[]=['id_card_number' =>$value->rkm_alhoy,'reason'=>'not_person'];
                                                                    }
                                                                }
                                                            }else{
                                                                $duplicated[]=$value->rkm_alhoy;
                                                                $result[]=['id_card_number' =>$value->rkm_alhoy,'reason'=>'duplicated'];
                                                            }
                                                        }else{

                                                            if(!in_array($value->rkm_alhoy,$not_card_number)){
                                                                $not_card_number[]=$value->rkm_alhoy;
                                                                $result[]=['id_card_number' =>$value->rkm_alhoy,'reason'=>'not_card_number'];
                                                            }else{
                                                                $duplicated[]=$value->rkm_alhoy;
                                                                $result[]=['id_card_number' =>$value->rkm_alhoy,'reason'=>'not_card_number'];
                                                            }
                                                        }
                                                    }
                                                    $row++;
                                                }
                                            }

                                            $vouchers_number=0;
                                            if(!empty($vouchers)) {
                                                foreach($vouchers as $k=>$v){
                                                    if(!empty($v['persons'])) {
                                                        $input=['content' => $v['content'],'value' => $v['value']];
                                                        $input['title'] =strip_tags($request->get('title'));
                                                        $input['notes'] =strip_tags($request->get('notes'));
                                                        $input['center'] =strip_tags($request->get('center'));
                                                        $input['beneficiary'] =strip_tags($request->get('beneficiary'));
                                                        $input['exchange_rate'] = strip_tags($request->get('exchange_rate'));
                                                        $input['currency_id'] = strip_tags($request->get('currency_id'));
                                                        $input['category_id'] = strip_tags($request->get('category_id'));
                                                        $input['case_category_id'] = strip_tags($request->get('case_category_id'));
                                                        $input['type'] = strip_tags($request->get('type'));
                                                        $input['sponsor_id'] = strip_tags($request->get('sponsor_id'));
                                                        $input['transfer'] = strip_tags($request->get('transfer'));
                                                        $input['transfer'] = strip_tags($request->get('transfer'));
                                                        $input['collected'] = strip_tags($request->get('collected'));
                                                        $input['un_serial'] = strip_tags($request->get('un_serial'));
                                                        $input['transfer_company_id'] = null;
                                                        if($input['transfer'] == 1 || $input['transfer'] == '1'){
                                                            $input['transfer_company_id'] = $request->get('transfer_company_id');
                                                        }
                                                        $input['voucher_date'] =date('Y-m-d',strtotime(strip_tags($request->get('voucher_date'))));
                                                        $input['count'] =sizeof($v['persons']);
                                                        $input['organization_id']=$user->organization_id;

                                                        $user = \Auth::user();
                                                        $district_id = $user->organization->district_id  ;

                                                        $district_code = Helpers::getDistrictCode($user->organization_id,$district_id);
                                                        $mSerial= $district_code.date('Y').date('m').'-';

                                                        $voucher_cnt = Vouchers::query()
                                                            ->where(function ($q) use ($mSerial) {
                                                                $q->whereRaw("serial like ?",$mSerial. "%");
                                                            })
                                                            ->count();

                                                        $voucher_cnt ++;
                                                        $input['serial']= $mSerial.$voucher_cnt;
                                                        $voucher=Vouchers::create($input);
                                                        if($voucher){
                                                            foreach($v['persons'] as $kg=>$vg){
                                                                $vg['voucher_id']=$voucher->id;
                                                                $vg['status']=2;
                                                                VoucherPersons::create($vg);
                                                            }
                                                        }
                                                        $vouchers_number++;
                                                    }
                                                }
                                            }
                                            $response["result"]= $result;
                                            if($success != 0){
                                                \Log\Model\Log::saveNewLog('PERSON_VOUCHER_CREATED',trans('aid::application.Added vouchers and beneficiaries with an Excel file') );

                                            }
                                            if($row == 0){
                                                $response["status"]= false;
                                                $response["msg"] = trans('aid::application.The voucher row is not insert to db');
                                            }
                                            else{
                                                if($row != 0 && $success == 0 && $row == $success){
                                                    $response["status"]= true;
                                                    $response["msg"] = trans('aid::application.The voucher row is inserted to db');
                                                }else{

                                                    $response["status"]= 'inserted_error';
                                                    $response["msg"]=
                                                        trans('aid::application.Some cases has been successfully nomination to voucher but another is restricted'). ' ( '
                                                        . trans('aid::application.total no') .' : '.  $row .' , '.
                                                        trans('aid::application.success') .' : '. $success.' , '.
                                                        trans('aid::application.not_person') .' : '. sizeof($not_person) .' , '.
                                                        trans('aid::application.not_case') .' : '. sizeof($not_case) .' , '.
                                                        trans('aid::application.not_card_number') .' : '. sizeof($not_card_number) .' , '.
                                                        trans('aid::application.out_of_regions') .' : '. sizeof($out_of_regions) .' , '.
                                                        trans('aid::application.policy_restricted') .' : '. sizeof($policy_restricted) .' , '.
                                                        trans('aid::application.duplicated') .' : '. sizeof($duplicated) .' )';
                                                }

                                            }

                                            if(sizeof($result) > 0 || sizeof($beneficiaries) > 0){
                                                $rows=[];
                                                foreach($result as $key =>$value){
                                                    $rows[$key][trans('aid::application.#')]=$key+1;
                                                    foreach($value as $k =>$v){
                                                        if($k != 'case_id' && $k != 'id' && $k != 'sponsor_id'&& $k != 'flag'&& $k != 'date_flag'){
                                                            if($k =='sponsor_number'){
                                                                $rows[$key][trans('aid::application.sponsorship_number')]= $v;
                                                            }else{
                                                                $rows[$key][trans('aid::application.' . $k)]= $v;
                                                            }
                                                            $translate='-';
                                                            if($k =='reason'){
                                                                if($v =='not_same'){ $translate= trans('aid::application.not_same');}
                                                                if($v =='blocked'){ $translate= trans('aid::application.blocked');}
                                                                if($v =='nominated'){ $translate= trans('aid::application.nominated');}
                                                                if($v =='duplicated'){ $translate= trans('aid::application.duplicated');}
                                                                if($v =='out_of_regions'){ $translate= trans('aid::application.out_of_regions');}
                                                                if($v =='out_of_ratio'){ $translate= trans('aid::application.out_of_ratio');}
                                                                if($v =='policy_restricted'){ $translate= trans('aid::application.policy_restricted');}
                                                                if($v =='restricted_total_payment_persons'){ $translate= trans('aid::application.restricted_total_payment_persons');}
                                                                if($v =='restricted_count_of_family'){ $translate= trans('aid::application.restricted_count_of_family');}
                                                                if($v =='restricted_amount'){ $translate= trans('aid::application.restricted_amounts');}
                                                                if($v =='the same status'){ $translate= trans('sponsorship::application.the same status');}
                                                                if($v =='invalid status'){ $translate= trans('sponsorship::application.invalid status');}
                                                                if($v =='not_nominated'){ $translate= trans('sponsorship::application.not_nominated');}
                                                                if($v =='not_case'){ $translate= trans('sponsorship::application.not_case');}
                                                                if($v =='limited'){ $translate= trans('aid::application.max_limited_');}
                                                                if($v =='invalid_id_card_number'){ $translate= trans('sponsorship::application.invalid_id_card_number');}
                                                                $rows[$key][trans('aid::application.reason')]= $translate;
                                                            }
                                                        }
                                                    }
                                                }

                                                $token = md5(uniqid());
                                                \Excel::create('export_' . $token, function($excel) use($rows,$beneficiaries) {
                                                    if(sizeof($rows) > 0){
                                                        $excel->sheet(trans('aid::application.rejected_sheet'), function ($sheet) use ($rows) {
                                                            $sheet->setStyle([
                                                                'font' => [
                                                                    'name' => 'Calibri',
                                                                    'size' => 11,
                                                                    'bold' => true
                                                                ]
                                                            ]);
                                                            $sheet->setAllBorders('thin');
                                                            $sheet->setfitToWidth(true);
                                                            $sheet->setRightToLeft(true);
                                                            $style = [
                                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                                'font' =>[
                                                                    'name'      =>  'Simplified Arabic',
                                                                    'size'      =>  12,
                                                                    'bold'      =>  true
                                                                ]
                                                            ];

                                                            $sheet->getStyle("A1:D1")->applyFromArray($style);
                                                            $sheet->setHeight(1,30);

                                                            $style = [
                                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                                'font' =>[
                                                                    'name'      =>  'Simplified Arabic',
                                                                    'size'      =>  12,
                                                                    'bold' => false
                                                                ]
                                                            ];

                                                            $sheet->getDefaultStyle()->applyFromArray($style);
                                                            $sheet->fromArray($rows);

                                                        });
                                                    }

                                                    if(sizeof($beneficiaries) > 0){
                                                        $excel->sheet(trans('aid::application.Beneficiaries'), function ($sheet) use ($beneficiaries) {
                                                            $sheet->setStyle([
                                                                'font' => [
                                                                    'name' => 'Calibri',
                                                                    'size' => 11,
                                                                    'bold' => true
                                                                ]
                                                            ]);
                                                            $sheet->setAllBorders('thin');
                                                            $sheet->setfitToWidth(true);
                                                            $sheet->setRightToLeft(true);
                                                            $style = [
                                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                                'font' => [
                                                                    'name' => 'Simplified Arabic',
                                                                    'size' => 12,
                                                                    'bold' => true
                                                                ]
                                                            ];

                                                            $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                            $sheet->setHeight(1, 30);

                                                            $style = [
                                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                                'font' => [
                                                                    'name' => 'Simplified Arabic',
                                                                    'size' => 12,
                                                                    'bold' => false
                                                                ]
                                                            ];

                                                            $sheet->getDefaultStyle()->applyFromArray($style);

                                                            $sheet->getStyle("A1:B1")->applyFromArray(['font' => ['bold' => true]]);
                                                            $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                            $sheet ->setCellValue('B1',trans('common::application.name'));
                                                            $z= 2;
                                                            foreach($beneficiaries as $v){
                                                                $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                                $sheet ->setCellValue('B'.$z,$v['name']);
                                                                $z++;
                                                            }
                                                        });
                                                    }
                                                })->store('xlsx', storage_path('tmp/'));
                                                $response["download_token"]= $token;
                                                $response["msg"] = trans('aid::application.Downloading the file containing the banned candidates').' ، '. $response["msg"];
                                            }
                                            return response()->json($response);

                                        }
                                        return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                                    }
                                    return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);

                                }

                            }

                            return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
                    }
                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
                }

            }
            else{
                $response = array();
                $voucher=Vouchers::findorfail($request->voucher_id);
                $to_date = date('Y-m-d', strtotime($voucher->voucher_date. ' + '.$voucher->allow_day.' days'));

                if(strtotime($to_date) > strtotime('now')){
                    $response["status"]= 'inserted_error';
                    $response["msg"]= trans('aid::application.exceeded the number of days allowed to add beneficiaries to voucher');
                    return response()->json($response);

                }

                $rules = [
                    'voucher_id' => 'required|integer', 'persons' => 'required',
                    'receipt_date' => 'required|date', 'receipt_location' => 'required|max:255',
                ];

                $input = [
                    'voucher_id' => $request->voucher_id,
                    'persons' => $request->get('persons'),
                    'receipt_date' => strip_tags($request->receipt_date),
                    'receipt_location' => strip_tags($request->receipt_location)
                ];

                $error = \App\Http\Helpers::isValid($input,$rules);
                if($error)
                    return response()->json($error);

                $old_v = VoucherPersons::where(['voucher_id' => $input['voucher_id']])->count();
                $max = $voucher->count - $old_v;

                if ($max == 0) {
                    $response["status"] = 'inserted_error';
                    $response["msg"] = trans('aid::application.The voucher is max exceed persons');
                    return response()->json($response);
                }


                $records = $request->persons;
                $total = sizeof($records);
                if ($total > 0) {

                    $user = \Auth::user();
                    $response = array();

                    $invalid_card = 0;
                    $not_person = 0;
                    $not_case = 0;
                    $blocked = 0;
                    $out_of_regions = 0;
                    $policy_restricted = 0;
                    $duplicated = 0;
                    $nominated = 0;
                    $limited = 0;
                    $success = 0;
                    $processed = [];
                    $passed = [];
                    $result = [];
                    $out_of_ratio = 0;
                    foreach ($records as $key => $value) {
                        $id_card_number = $value['id_card_number'];
                        $isPass=VoucherPersons::checkPersons('id_card_number',$id_card_number,$request->voucher_id,$user->organization_id,$voucher->value,$user->id);
                        if (!in_array($isPass['id'], $processed)) {
                            if ($isPass['status'] == true) {
                                $temp = ['name' => $isPass['name'], 'id' => $isPass['id'], 'id_card_number' =>$id_card_number , 'receipt_time'=>null];
                                $passed[] = $temp;
                                $success++;
                            } else {
                                if ($isPass['reason'] == 'policy_restricted') {
                                    $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'policy_restricted'];
                                    $policy_restricted++;
                                }

                                if ($isPass['reason'] == 'out_of_regions') {
                                    $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'out_of_regions'];
                                    $out_of_regions++;
                                }
                                if ($isPass['reason'] == 'blocked') {
                                    $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'blocked'];
                                    $blocked++;
                                }

                                if ($isPass['reason'] == 'not_case') {
                                    $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'not_case'];
                                    $not_case++;
                                }
                                if ($isPass['reason'] == 'not_person') {
                                    $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'not_person'];
                                    $not_person++;
                                }
                                if ($isPass['reason'] == 'nominated') {
                                    $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'nominated'];
                                    $nominated++;
                                }
                            }

                            $processed[] = $id_card_number;
                        } else {
                            $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'duplicated'];
                            $duplicated++;
                        }
                    }

                    $response["result"] = $result;
                    $response["pass"] = $passed;
                    $response["status"] = 'inserted_error';
                    $total_restricted = sizeof($result);
                    if (($policy_restricted + $blocked) == sizeof($records)) {
                        $response["status"] = 'inserted_error';
                        $response["msg"] = trans('aid::application.The selected cases are not pass may not category blocked or policy restricted');
                    }
                    else if (($policy_restricted + $blocked) == sizeof($records)) {
                        $response["status"] = 'inserted_error';
                        $response["msg"] = trans('aid::application.The selected cases are not pass may not category blocked or policy restricted');
                    }
                    else if ($nominated == sizeof($records)) {
                        $response["status"] = 'inserted_error';
                        $response["msg"] = trans('aid::application.The selected cases are nominated');
                    }
                    else if ($out_of_regions == sizeof($records)) {
                        $response["status"] = 'inserted_error';
                        $response["msg"] = trans('aid::application.The selected cases are not pass may not in connector locations');
                    }
                    else if ($limited == sizeof($records)) {
                        $response["status"] = 'inserted_error';
                        $response["msg"] = trans('aid::application.The selected cases are limited');
                    }
                    else if ($total_restricted == sizeof($records)) {
                        $response["status"] = 'inserted_error';
                        $response["msg"] = trans('aid::application.The selected cases are restricted');
                    }

                    else{

                        if (sizeof($passed) > 0) {
                            $receipt_location = $request->receipt_location;
                            $receipt_date = date('Y-m-d', strtotime($request->receipt_date));

                            $user = \Auth::user();
                            $organization_id = $user->organization_id;

                            $r = 0;
                            $inserted_ = [];
                            $limited_ = [];
                            $pIds = [];
                            foreach ($passed as $person) {
                                $pIds[] = $person['id'];
                            }

                            $response["reg"] = $pIds;
                            $done = [];

                            if($request->withRatio == true){
                                $mosques_ratio = aidsLocation::getTotalCountTree($voucher->count,$voucher->id);
                                foreach ($mosques_ratio as $k => $v) {
                                    $mosques_limit = $v->total - $v->count;
                                    $mosques_person = \DB::table('char_persons')
                                        ->where('char_persons.mosques_id', $v->location_id)
                                        ->wherein('char_persons.id', $pIds)
                                        ->selectRaw("char_persons.id ,
                                                             CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ',
                                                                 ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                                 char_persons.id_card_number")
                                        ->groupBy('char_persons.id')
                                        ->get();

                                    if (sizeof($mosques_person) > 0) {
                                        if ($mosques_limit > 0) {
                                            $onMosque = [];

                                            foreach ($mosques_person as $key => $value) {
                                                $onMosque[] = $value->id;
                                            }

                                            $person = \DB::table('char_persons')
                                                ->where('char_persons.mosques_id', $v->location_id)
                                                ->wherein('char_persons.id', $pIds)
                                                ->selectRaw("char_persons.id ,
                                                             CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ',
                                                                 ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                                 char_persons.id_card_number")
                                                ->groupBy('char_persons.id')
                                                ->limit($mosques_limit)
                                                ->get();

                                            if (sizeof($person) > 0) {
                                                $person_ =[];
                                                foreach ($person as $kk => $vv) {
                                                    $done[]=$vv->id;
                                                    $person_[]=$vv->id;
                                                    if ($old_v < $voucher->count) {
//                                                    if ($old_v < $max) {
                                                        $founded = VoucherPersons::where(['voucher_id' => $voucher->id, 'person_id' => $vv->id])->first();
                                                        if ($founded) {
                                                            $result[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'nominated'];
                                                            $nominated++;
                                                        } else {

                                                            VoucherPersons::create([ 'person_id' => $vv->id, 'status' => 2,
                                                                'voucher_id' => $voucher->id, 'receipt_location' => $receipt_location,
                                                                'receipt_date' => $receipt_date]);
                                                            $inserted_[] = $vv->id;
                                                            $success++;
                                                            $old_v++;
                                                            $r++;
                                                        }
                                                    } else {
                                                        if (!in_array($value->id, $limited_)) {
                                                            $result[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'limited'];
                                                            $limited_[] = $vv->id;
                                                            $limited++;
                                                        }
                                                    }
                                                }
                                            }

                                            $person_diff = array_diff($onMosque, $person_);

                                            if (sizeof($person_diff) > 0) {
                                                $person_ = \DB::table('char_persons')
                                                    ->where('char_persons.mosques_id', $v->location_id)
                                                    ->wherein('char_persons.id', $person_diff)
                                                    ->selectRaw("char_persons.id,CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,
                                                                 ifnull(char_persons.second_name, ' '),' ',
                                                                 ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as full_name,
                                                                 char_persons.id_card_number")
                                                    ->groupBy('char_persons.id')
                                                    ->get();

                                                foreach ($person_ as $key => $value) {
                                                    if(in_array($value->id, $limited_)) {
                                                        $result[] = ['name' => $value->full_name, 'id_card_number' => $value->id_card_number, 'reason' => 'out_of_ratio'];
                                                        $done[] = $value->id;
                                                        $out_of_ratio++;
                                                    }
                                                }
                                            }

                                        }
                                        else{
                                            foreach ($mosques_person as $kk => $vv) {
                                                $result[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'out_of_ratio'];
                                                $done[]=$vv->id;
                                                $out_of_ratio++;
                                            }
                                        }
                                    }
                                }

                                $pe = array_diff($pIds, $done);

                                $out_per = \DB::table('char_persons')
                                    ->wherein('char_persons.id', $pe)
                                    ->selectRaw("char_persons.id ,
                                                             CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ',
                                                                 ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                                 char_persons.mosques_id,char_persons.id_card_number")
                                    ->groupBy('char_persons.id')
                                    ->get();

                                foreach ($out_per as $kk => $vv) {
                                    $result[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'out_of_ratio'];
                                    $out_of_ratio++;
                                }
                                $response["count"] =$old_v;
                            }
                            else{

                                $person = \DB::table('char_persons')
                                    ->wherein('char_persons.id', $pIds)
                                    ->selectRaw("char_persons.id ,
                                                             CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ',
                                                                 ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                                 char_persons.id_card_number")
                                    ->groupBy('char_persons.id')
                                    ->get();

//                                return response()->json(['status' => 'failed', 'error_type' => ' ',$person, 'msg' => trans('aid::application.No filter cases were specified')]);

                                $person_ =[];
                                foreach ($person as $kk => $vv) {
                                    $done[]=$vv->id;
                                    $person_[]=$vv->id;
                                    if ($old_v < $voucher->count) {
                                        $founded =VoucherPersons::where(['voucher_id' => $voucher->id, 'person_id' => $vv->id])->first();
                                        if ($founded) {
                                            $result[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'nominated'];
                                            $nominated++;
                                        } else {

                                           VoucherPersons::create([ 'person_id' => $vv->id, 'status' => 2,
                                                'voucher_id' => $voucher->id, 'receipt_location' => $receipt_location,
                                                'receipt_date' => $receipt_date]);
                                            $inserted_[] = $vv->id;
                                            $success++;
                                            $old_v++;
                                            $r++;
                                        }
                                    } else {
                                        if (!in_array($vv->id, $limited_)) {
                                            $result[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'limited'];
                                            $limited_[] = $vv->id;
                                            $limited++;
                                        }
                                    }
                                }

                            }
                        }
                        $response["r_"] = $r;
                        $response["pass_"] = $result;

                        if ($r > 0) {
                            if (sizeof($result) > 0) {
                                $response["status"] = 'inserted_error';
                                $response["msg"] =
                                    trans('aid::application.Some cases has been successfully nomination to voucher but another is restricted') . ' ( '
                                    . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                    trans('aid::application.success') . ' : ' . $r . ' , ' .
                                    trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_card . ' , ' .
                                    trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                                    trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                                    trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                                    trans('aid::application.out_of_regions') . ' : ' . $out_of_regions . ' , ' .
                                    trans('aid::application.policy_restricted') . ' : ' . $policy_restricted . ' , ' .
                                    trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                                    trans('aid::application.out_of_ratio') . ' : ' . $out_of_ratio . ' , ' .
                                    trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                    trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                                \Log\Model\Log::saveNewLog('PERSON_VOUCHER_CREATED', trans('aid::application.Downloading the file containing the banned candidates') . ' "' . $voucher->title . '"');

                            } else {
                                $response["status"] = 'success';
                                $response["msg"] = trans('aid::application.The voucher row is inserted to db');
                            }

                        } else {
                            if (sizeof($result) > 0) {
                                $response["status"] = 'inserted_error';
                                $response["msg"] =
                                    trans('aid::application.Some cases has been successfully nomination to voucher but another is restricted') . ' ( '
                                    . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                    trans('aid::application.success') . ' : ' . $r . ' , ' .
                                    trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_card . ' , ' .
                                    trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                                    trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                                    trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                                    trans('aid::application.out_of_regions') . ' : ' . $out_of_regions . ' , ' .
                                    trans('aid::application.policy_restricted') . ' : ' . $policy_restricted . ' , ' .
                                    trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                                    trans('aid::application.out_of_ratio') . ' : ' . $out_of_ratio . ' , ' .
                                    trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                    trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                                \Log\Model\Log::saveNewLog('PERSON_VOUCHER_CREATED', trans('aid::application.Downloading the file containing the banned candidates') . ' "' . $voucher->title . '"');

                            } else {
                                $response["status"] = 'success';
                                $response["msg"] = trans('aid::application.The voucher row is inserted to db');
                            }

                        }

                    }

                    if (sizeof($result) > 0) {
                        $rows = [];
                        foreach ($result as $key => $value) {
                            $rows[$key][trans('aid::application.#')] = $key + 1;
                            foreach ($value as $k => $v) {
                                if ($k != 'case_id' && $k != 'id' && $k != 'sponsor_id' && $k != 'flag' && $k != 'date_flag') {
                                    if ($k == 'sponsor_number') {
                                        $rows[$key][trans('aid::application.sponsorship_number')] = $v;
                                    } else {
                                        $rows[$key][trans('aid::application.' . $k)] = $v;
                                    }
                                    $translate = '-';
                                    if ($k == 'reason') {
                                        if ($v == 'not_person') {
                                            $translate = trans('aid::application.not_person');
                                        }
                                        if ($v == 'not_same') {
                                            $translate = trans('aid::application.not_same');
                                        }
                                        if ($v == 'blocked') {
                                            $translate = trans('aid::application.blocked');
                                        }
                                        if ($v == 'nominated') {
                                            $translate = trans('aid::application.nominated');
                                        }
                                        if ($v == 'duplicated') {
                                            $translate = trans('aid::application.duplicated');
                                        }
                                        if ($v == 'out_of_regions') {
                                            $translate = trans('aid::application.out_of_regions');
                                        }
                                        if ($v == 'out_of_ratio') {
                                            $translate = trans('aid::application.out_of_ratio');
                                        }
                                        if ($v == 'policy_restricted') {
                                            $translate = trans('aid::application.policy_restricted');
                                        }
                                        if ($v == 'restricted_total_payment_persons') {
                                            $translate = trans('aid::application.restricted_total_payment_persons');
                                        }
                                        if ($v == 'restricted_count_of_family') {
                                            $translate = trans('aid::application.restricted_count_of_family');
                                        }
                                        if ($v == 'restricted_amount') {
                                            $translate = trans('aid::application.restricted_amounts');
                                        }
                                        if ($v == 'the same status') {
                                            $translate = trans('sponsorship::application.the same status');
                                        }
                                        if ($v == 'invalid status') {
                                            $translate = trans('sponsorship::application.invalid status');
                                        }
                                        if ($v == 'not_nominated') {
                                            $translate = trans('sponsorship::application.not_nominated');
                                        }
                                        if ($v == 'not_case') {
                                            $translate = trans('sponsorship::application.not_case');
                                        }
                                        if ($v == 'limited') {
                                            $translate = trans('aid::application.max_limited_');
                                        }
                                        if ($v == 'invalid_id_card_number') {
                                            $translate = trans('sponsorship::application.invalid_id_card_number');
                                        }
                                        $rows[$key][trans('aid::application.reason')] = $translate;
                                    }
                                }
                            }
                        }

                        $token = md5(uniqid());
                        \Excel::create('export_' . $token, function ($excel) use ($rows) {
                            $excel->sheet(trans('aid::application.rejected_sheet'), function ($sheet) use ($rows) {
                                $sheet->setStyle([
                                    'font' => [
                                        'name' => 'Calibri',
                                        'size' => 11,
                                        'bold' => true
                                    ]
                                ]);
                                $sheet->setAllBorders('thin');
                                $sheet->setfitToWidth(true);
                                $sheet->setRightToLeft(true);
                                $style = [
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font' => [
                                        'name' => 'Simplified Arabic',
                                        'size' => 12,
                                        'bold' => true
                                    ]
                                ];

                                $sheet->getStyle("A1:D1")->applyFromArray($style);
                                $sheet->setHeight(1, 30);

                                $style = [
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font' => [
                                        'name' => 'Simplified Arabic',
                                        'size' => 12,
                                        'bold' => false
                                    ]
                                ];

                                $sheet->getDefaultStyle()->applyFromArray($style);
                                $sheet->fromArray($rows);

                            });
                        })->store('xlsx', storage_path('tmp/'));
                        $response["download_token"] = $token;
                        $response["msg"] = $response["msg"] . '  , ' . trans('aid::application.all restricted_');
                    }

                    return response()->json($response);
                }
                return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('aid::application.No filter cases were specified')]);
            }

        } catch (Exception $e) {
            return response()->json(['status' =>'error',"error_code" => $e->getCode() ,'msg'=>$e->getMessage() ]);
        }
    }

}