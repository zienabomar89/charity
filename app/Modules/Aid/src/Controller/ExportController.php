<?php

namespace aid\Controller;

use App\Http\Controllers\Controller;
use App\Http\Helpers;
use Common\Model\Person;
use Illuminate\Http\Request;
use League\Flysystem\Exception;
use Organization\Model\Organization;
use Organization\Model\OrgLocations;
use Setting\Model\aidsLocation;
use Setting\Model\Setting;
use Aid\Model\Vouchers;
use Aid\Model\VoucherPersons;
use Aid\Model\VoucherDocuments;
use Excel;
use Elibyy\TCPDF\TCPDF;
use PDF;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Common\Model\GovServices;

class ExportController  extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // *************************************************************************************************************//
    // *************************************************************************************************************//
    // set cases as beneficiary of voucher  by voucher_id using person_id ( to center project )
    public function importBeneficiary(Request $request)
    {

        //  can('aid.voucher.center')
        $this->authorize('center', Vouchers::class);

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit', '2048M');
        $voucher_id = (int)$request->voucher_id;
        $voucher = Vouchers::findorfail($request->voucher_id);

        $to_date = date('Y-m-d', strtotime($voucher->voucher_date . ' + ' . $voucher->allow_day . ' days'));

        if (strtotime($to_date) > strtotime('now')) {
            $response["status"] = 'inserted_error';
            $response["msg"] = trans('aid::application.exceeded the number of days allowed to add beneficiaries to voucher');
            return response()->json($response);
        }

        $old_v =VoucherPersons::where(['voucher_id' => $voucher_id])->count();
        $max = $voucher->count - $old_v;

        if ($max == 0) {
            $response["status"] = 'inserted_error';
            $response["msg"] = trans('aid::application.The voucher is max exceed persons');
            return response()->json($response);
        }

        $importFile = $request->file;

        if ($importFile->isValid()) {

            $path = $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load($path);
            $sheets = $excel->getSheetNames();

            if(in_array('data', $sheets)) {

                $first = Excel::selectSheets('data')->load($path)->first();
                if (!is_null($first)) {
                    $keys = $first->keys()->toArray();

                    $validFile = true;
                    $constraint = ["rkm_alhoy","mkan_alastlam", "tarykh_alastlam", "okt_alastlam"]; /*,"mkan_alastlam"*/
                    foreach ($constraint as $item_) {
                        if ( !in_array($item_, $keys) ) {
                            $validFile = false;
                            break;
                        }
                    }

                    if ($validFile) {
                        $records = \Excel::selectSheets('data')->load($path)->get();
                        $total = sizeof($records);

                        if($total > 0 ){

                            $user = \Auth::user();
                            $organization_id = $user->organization_id;
                            $response = array();

                            $list = [];
                            $cards_list = [];
                            $old_cards = [];
                            $restricted = [];
                            $check_card = [];
                            $details =[];
                            $case_added = 0;
                            $not_person = 0;
                            $invalid_id_card_number = 0;
                            $not_case = 0;
                            $blocked = 0;
                            $out_of_regions = 0;
                            $policy_restricted = 0;
                            $duplicated = 0;
                            $nominated = 0;
                            $limited = 0;
                            $out_of_ratio = 0;

                            foreach ($records as $key => $value) {
                                $id_card_number = (int)$value['rkm_alhoy'];
                                if (!in_array($id_card_number, $list)) {
                                    if(GovServices::checkCard($id_card_number)) {
                                        $temp = ['receipt_time'=>null,'receipt_location'=>null,'receipt_date'=>null,'voucher_id'=>$voucher_id];

                                        if (isset($value['okt_alastlam'])) {
                                            if (!is_null($value['okt_alastlam']) && $value['okt_alastlam'] != ' ' && $value['okt_alastlam'] != '') {
                                                if (is_object($value['okt_alastlam'])) {
                                                    $temp['receipt_time'] = date('H:i:s',strtotime($value['okt_alastlam']));
                                                }
                                            }
                                        }

                                        if (isset($value['mkan_alastlam'])) {
                                            if (!is_null($value['mkan_alastlam']) && $value['mkan_alastlam'] != ' ' && $value['mkan_alastlam'] != '') {
                                                $temp['receipt_location'] = $value['mkan_alastlam'];
                                            }
                                        }
                                        if (isset($value['tarykh_alastlam'])) {
                                            if (!is_null($value['tarykh_alastlam']) && $value['tarykh_alastlam'] != ' ' && $value['tarykh_alastlam'] != '') {
                                                $receipt_date = Helpers::getFormatedDate($value['tarykh_alastlam']);
                                                if (!is_null($receipt_date)) {
                                                    $temp['receipt_date']=$receipt_date;
                                                }
                                            }
                                        }

                                        $details[$id_card_number] = $temp;
                                        $list[] = $id_card_number;

                                        if(strstr($id_card_number, '700')){
                                            if(strpos($id_card_number, '700') == 0 ){
                                                $cards_list[]= $id_card_number;
                                            }
                                        }
                                    }
                                    else{
                                        $restricted[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'invalid_id_card_number'];
                                    }

                                    $check_card[] = $id_card_number;
                                } else {
                                    $restricted[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                                }
                            }
                            $final = [];
                            $person_vouchers = [];
                            if(sizeof($list) > 0 ){

                                if (sizeof($cards_list) > 0){
                                    $old_card_check = Person::oldCardCheck($cards_list);
                                    if (sizeof($old_card_check) > 0){
                                        foreach ($old_card_check as $key =>$value){
                                            $old_cards[] = $value->old_id_card_number;
                                            $restricted[] = ['name' => ' ', 'id_card_number' => $value->old_id_card_number, 'reason' => 'duplicated'];
                                        }
                                    }
                                }

                                $response['check'] = $this->checkCasesToCenter($details,$list,$voucher->case_category_id,$voucher_id,$organization_id,$voucher->value,$user->id,$request->ratio,$restricted,$voucher->count,$old_v);
                                $final = $response['check']['final'];
                                $restricted = $response['check']['restricted'];
                                $person_vouchers = $response['check']['person_vouchers'];

                                if(sizeof($person_vouchers) > 0) {
                                   VoucherPersons::insert($person_vouchers);
                                }
                            }

                            if (sizeof($final) > 0 || sizeof($restricted) > 0) {

                                if(sizeof($final) > 0) {
                                    foreach($final as $k=>$v){
                                        if($v->status == 'new_case'){
                                            $v->status_name = trans('aid::application.new beneficiary');
                                            $case_added++;
                                        }else{
                                            $v->status_name = trans('aid::application.old beneficiary');
                                        }
                                    }

                                }

                                if(sizeof($restricted) > 0) {
                                    foreach($restricted as $k=>$v){

                                        if ($v['reason'] == 'not_person') {
                                            $not_person++;
                                        }
                                        if ($v['reason'] == 'blocked') {
                                            $blocked++;
                                        }
                                        if ($v['reason'] == 'nominated') {
                                            $nominated++;
                                        }
                                        if ($v['reason'] == 'duplicated') {
                                            $duplicated++;
                                        }
                                        if ($v['reason'] == 'out_of_regions') {
                                            $out_of_regions++;
                                        }
                                        if ($v['reason'] == 'out_of_ratio') {
                                            $out_of_ratio++;
                                        }
                                        if ($v['reason'] == 'policy_restricted') {
                                            $policy_restricted++;
                                        }

                                        if ($v['reason'] == 'not_case') {
                                            $not_case++;
                                        }
                                        if ($v['reason'] == 'limited') {
                                            $limited++;
                                        }
                                        if ($v['reason'] == 'invalid_id_card_number') {
                                            $invalid_id_card_number++;
                                        }

                                        if(isset($v['sub'])){
                                            $v['reason'] = trans('aid::application.policy_restricted') .' '. trans('aid::application.'.$v['sub']);
                                        }else{
                                            $v['reason'] = trans('aid::application.'.$v['reason']);
                                        }
                                    }
                                }

                                $token = md5(uniqid());
                                \Excel::create('export_' . $token, function ($excel) use ($final,$restricted,$old_cards) {

                                    if(sizeof($final) > 0){
                                        $excel->sheet(trans('aid::application.Beneficiaries'), function ($sheet) use ($final) {
                                            $sheet->setStyle([
                                                'font' => [
                                                    'name' => 'Calibri',
                                                    'size' => 11,
                                                    'bold' => true
                                                ]
                                            ]);
                                            $sheet->setAllBorders('thin');
                                            $sheet->setfitToWidth(true);
                                            $sheet->setRightToLeft(true);
                                            $style = [
                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                'font' => [
                                                    'name' => 'Simplified Arabic',
                                                    'size' => 12,
                                                    'bold' => true
                                                ]
                                            ];

                                            $sheet->getStyle("A1:C1")->applyFromArray($style);
                                            $sheet->setHeight(1, 30);

                                            $style = [
                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                'font' => [
                                                    'name' => 'Simplified Arabic',
                                                    'size' => 12,
                                                    'bold' => false
                                                ]
                                            ];

                                            $sheet->getDefaultStyle()->applyFromArray($style);
                                            $sheet->getDefaultStyle()->applyFromArray($style);

                                            $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                            $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                            $sheet ->setCellValue('B1',trans('common::application.name'));
                                            $sheet ->setCellValue('C1',trans('common::application.status'));
                                            $z= 2;

                                            foreach($final as $k=>$v){
                                                $sheet ->setCellValue('A'.$z,$v->id_card_number);
                                                $sheet ->setCellValue('B'.$z,$v->name);
                                                $sheet ->setCellValue('C'.$z,$v->status_name);
                                                $z++;
                                            }
                                        });
                                    }

                                    if(sizeof($restricted) > 0){
                                        $excel->sheet(trans('aid::application.rejected_sheet'), function ($sheet) use ($restricted) {
                                            $sheet->setStyle([
                                                'font' => [
                                                    'name' => 'Calibri',
                                                    'size' => 11,
                                                    'bold' => true
                                                ]
                                            ]);
                                            $sheet->setAllBorders('thin');
                                            $sheet->setfitToWidth(true);
                                            $sheet->setRightToLeft(true);
                                            $style = [
                                               'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                'font' => [
                                                    'name' => 'Simplified Arabic',
                                                    'size' => 12,
                                                    'bold' => true
                                                ]
                                            ];

                                            $sheet->getStyle("A1:B1")->applyFromArray($style);
                                            $sheet->setHeight(1, 30);

                                            $style = [
                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                'font' => [
                                                    'name' => 'Simplified Arabic',
                                                    'size' => 12,
                                                    'bold' => false
                                                ]
                                            ];

                                            $sheet->getDefaultStyle()->applyFromArray($style);

                                            $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                            $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                            $sheet ->setCellValue('B1',trans('common::application.reason'));
                                            $z= 2;
                                            foreach($restricted as $k=>$v){
                                                $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                $sheet ->setCellValue('B'.$z,trans('aid::application.'.$v['reason']));
                                                $z++;
                                            }
                                        });
                                    }

                                    if(sizeof($old_cards) > 0){
                                        $excel->sheet(trans('common::application.old card number'), function ($sheet) use ($old_cards) {
                                            $sheet->setStyle([
                                                'font' => [
                                                    'name' => 'Calibri',
                                                    'size' => 11,
                                                    'bold' => true
                                                ]
                                            ]);
                                            $sheet->setAllBorders('thin');
                                            $sheet->setfitToWidth(true);
                                            $sheet->setRightToLeft(true);
                                            $style = [
                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                'font' => [
                                                    'name' => 'Simplified Arabic',
                                                    'size' => 12,
                                                    'bold' => true
                                                ]
                                            ];

                                            $sheet->getStyle("A1")->applyFromArray($style);
                                            $sheet->setHeight(1, 30);

                                            $style = [
                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                'font' => [
                                                    'name' => 'Simplified Arabic',
                                                    'size' => 12,
                                                    'bold' => false
                                                ]
                                            ];

                                            $sheet->getDefaultStyle()->applyFromArray($style);
                                            $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                            $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                            $z= 2;
                                            foreach($old_cards as $v){
                                                $sheet ->setCellValue('A'.$z,$v);
                                                $z++;
                                            }
                                        });

                                    }

                                })->store('xlsx', storage_path('tmp/'));

                                $response["download_token"] = $token;
                            }

                            if(sizeof($restricted)  == sizeof($records)){
                                $response["status"] = 'inserted_error';
                                $response["msg"] = trans('aid::application.The voucher row is not insert to db'). ' , ' .
                                    trans('aid::application.Some cases has been successfully nomination to voucher but another is restricted') . ' ( '
                                    . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                    trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_id_card_number . ' , ' .
                                    trans('common::application.old card number') .' :  ' . sizeof($old_cards) . ' ,  ' .
                                    trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                                    trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                                    trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                                    trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                    trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                            }
                            else{
                                if(sizeof($restricted) == 0){
                                    $response["status"] = 'success';
                                    $response["msg"] = trans('aid::application.The voucher row is inserted to db');
                                }
                                else{
                                    $response["status"] = 'success';
                                    $response["msg"] =
                                        trans('aid::application.Some cases has been successfully nomination to voucher but another is restricted') . ' ( '
                                        . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                        trans('aid::application.success') . ' : ' . sizeof($final) . ' , ' .
                                        trans('aid::application.register_as_new_case') . ' : ' . $case_added . ' , ' .
                                        trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_id_card_number . ' , ' .
                                        trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                                        trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                                        trans('common::application.old card number') .' :  ' . sizeof($old_cards) . ' ,  ' .
                                        trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                                        trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                        trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                                }
                            }
                            return response()->json($response);

                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file is empty')]);
                    }

                    return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                }

                return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file is empty')]);
            }

            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('aid::application.The import sheet does not exist in the Excel , please check the label in the instructions or in the template') ]);
        }

        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.invalid file')]);

    }

    function checkCasesToCenter($details,$list,$category_id,$voucher_id,$organization_id,$amount,$user_id,$ratio,$restricted,$count,$ben_count){

        $not_case = array();
        $not_person = array();
        $new_cases = array();
        $persons_card_list = array();
        $passed_item = array();
        $final = array();
        $person_vouchers = array();

        $first =date('Y-01-01');
        $last =date('Y-12-t');

        $user = \Auth::user();
        $UserType=$user->type;
        $ancestor_id = \Organization\Model\Organization::getAncestor($organization_id);

        $user_id = $user->id;
        $persons =  \DB::table('char_persons')
                        ->leftjoin('char_cases', function($q) use ($category_id,$user,$UserType,$ancestor_id,$list,$user_id,$organization_id) {
                            $q->on('char_cases.person_id','=','char_persons.id');
                            if($UserType == 2) {
                                $q->whereRaw("(char_cases.category_id = $category_id And char_cases.status = 0) And
                                               ( char_cases.organization_id = $organization_id or char_cases.organization_id IN (
                                                              select char_user_organizations.organization_id 
                                                              from   char_user_organizations 
                                                              where  char_user_organizations.user_id = $user_id))");

                            }
                            else{

                                $q->whereRaw("(char_cases.category_id = $category_id And char_cases.status = 0) And
                                                 ( char_cases.organization_id = $organization_id or  char_cases.organization_id IN (
                                                              select char_organizations_closure.descendant_id 
                                                              from   char_organizations_closure 
                                                              where  char_organizations_closure.ancestor_id = $organization_id))");
                            }
                        })
                        ->leftjoin('char_vouchers_persons', function($q)  use ($voucher_id){
                            $q->on('char_vouchers_persons.person_id','=','char_persons.id');
                            $q->where('char_vouchers_persons.voucher_id','=',$voucher_id);
                        })
                        ->where(function($q)  use ($list){
                            $q->whereIn('char_persons.id_card_number',$list);
                        })
                        ->selectRaw("char_persons.id as person_id , char_persons.id_card_number ,char_persons.old_id_card_number ,
                                     char_cases.id as case_id , char_cases.organization_id , char_cases.category_id ,
                                     char_vouchers_persons.voucher_id,
                                     CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                            ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name 
                                                         ")
                        ->orderby('char_cases.status')
                        ->groupBy('char_persons.id')
                        ->get();

        $processed=[];
        $persons_details =[];
        if(sizeof($persons) > 0 ){
            foreach($persons as $k=>$v){
                $persons_details[$v->person_id]=$v;
                if($count > $ben_count){
                    if(!is_null($v->voucher_id)){
                        $restricted[]= ['id_card_number' => $v->id_card_number,'name' => $v->name , 'reason' => 'nominated'];
                        $processed[]=$v->id_card_number;
                    }else{
                        if(!is_null($v->case_id)){
                            $v->status = 'old_case';
                            $passed_item[$v->person_id]=$v;
                            $persons_list[]=$v->person_id;
                            $persons_card_list[]=$v->id_card_number;
                            if(!isset($details[$v->id_card_number]))
                                return $v->id_card_number;
                            $v->details = $details[$v->id_card_number];
                            $v->details['person_id']=$v->person_id;
                            $person_vouchers[]=$v->details ;
                            $final[]=$v ;
                            $ben_count++;
                        }
                        else{
                            $v->status = 'new_case';
                            $new_cases[]=$v;
                            $passed_item[$v->person_id]=$v;
                            $v->details = $details[$v->id_card_number];
                            $v->details['person_id']=$v->person_id;
                            $person_vouchers[]=$v->details ;
                            $final[]=$v ;
                            $persons_card_list[]=$v->id_card_number;
                            $ben_count++;
                        }
                    }
                }else{
                    $restricted[]= ['id_card_number' => $v->id_card_number,'name' => $v->name , 'reason' => 'limited'];
                    $processed[]=$v->id_card_number;
                }
            }
        }

        foreach($list as $card){
            if (!in_array($card, $persons_card_list) && !in_array($card, $processed)) {
                $restricted[]= ['id_card_number' => $card,'name' => ' ' , 'reason' => 'not_person'];
            }
        }

        if(sizeof($new_cases) > 0 ){
            $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$category_id)->first();

            foreach($new_cases as $k =>$v ){

                $exist =  \Common\Model\CaseModel::where(['person_id' => $v->person_id ,'organization_id' => $user->organization_id ])
                    ->first();

                if(is_null($exist)){
                    $case=  \Common\Model\AidsCases::create(['person_id' =>  $v->person_id,
                        'status' => 1, 'category_id' => $category_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'user_id' => $user->id,
                        'rank' => 0,
                        'organization_id' => $user->organization_id]);

                    \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id,
                        'reason'=>trans('common::application.case was created'), 'status'=>0, 'date'=>date("Y-m-d")]);
                    \Log\Model\Log::saveNewLog('CASE_CREATED',trans('common::application.Has added new case data for')  . '  : '.' "'. ' ' .  $v->name . ' " ' . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);

                }
            }
        }

        return array('final'=>$final,'restricted'=>$restricted,'person_vouchers'=>$person_vouchers,'new_cases'=>$new_cases);
    }

    // *************************************************************************************************************//
    // *************************************************************************************************************//
    public function setBeneficiary(Request $request)
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit', '2048M');

        $voucher = Vouchers::findorfail($request->voucher_id);

        $to_date = date('Y-m-d', strtotime($voucher->voucher_date . ' + ' . $voucher->allow_day . ' days'));

        if (strtotime($to_date) > strtotime('now')) {
            $response["status"] = 'inserted_error';
            $response["msg"] = trans('aid::application.exceeded the number of days allowed to add beneficiaries to voucher');
            return response()->json($response);
        }

        $voucher_id = $request->voucher_id;
        $records = $request->persons;
        $total = sizeof($records);

        if ($total > 0) {

            $rules = [
                'voucher_id' => 'required|integer', 'persons' => 'required',
                'receipt_date' => 'required|date', 'receipt_location' => 'required|max:255',
            ];

            $error = \App\Http\Helpers::isValid($request->all(),$rules);
            if($error)
                return response()->json($error);

            $old_v =VoucherPersons::where(['voucher_id' => $voucher_id])->count();
            $max = $voucher->count - $old_v;

            if ($max == 0) {
                $response["status"] = 'inserted_error';
                $response["msg"] = trans('aid::application.The voucher is max exceed persons');
                return response()->json($response);
            }

            $user = \Auth::user();
            $organization_id = $user->organization_id;
            $response = array();
            $add_cases = $request->add_cases;

            $restricted = [];
            $list = [];
            $details =[];
            $case_added = 0;
            $not_person = 0;
            $invalid_id_card_number = 0;
            $not_case = 0;
            $blocked = 0;
            $out_of_regions = 0;
            $policy_restricted = 0;
            $duplicated = 0;
            $nominated = 0;
            $limited = 0;
            $out_of_ratio = 0;

            $receipt_date = date('Y-m-d', strtotime($request->receipt_date));
            $receipt_location = $request->receipt_location;
            foreach ($records as $key => $value) {
                $id_card_number = $value['id_card_number'];
                if (!in_array($id_card_number, $list)) {
                    $temp = ['receipt_time'=>null,'receipt_location'=>$receipt_location,
                             'receipt_date'=>$receipt_date,'voucher_id'=>$request->voucher_id];

                    $details[$id_card_number] = $temp;
                    $list[] = $id_card_number;
                } else {
                    $restricted[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                }
            }

            if(sizeof($list) > 0 ){

                $voucher_id = (int)$request->voucher_id;
                $response['check'] = $this->checkCases($add_cases,$details,$list,$voucher->case_category_id,$voucher_id,$organization_id,$voucher->value,$user->id,$request->ratio,$restricted,$voucher->count,$old_v);

                $final = $response['check']['final'];
                $restricted = $response['check']['restricted'];
                $person_vouchers = $response['check']['person_vouchers'];

                if (sizeof($final) > 0 || sizeof($restricted) > 0 || sizeof($person_vouchers) > 0) {

                    if(sizeof($person_vouchers) > 0) {
                       VoucherPersons::insert($person_vouchers);
                    }

                    if (sizeof($final) > 0 || sizeof($restricted) > 0) {

                        foreach($final as $k=>$v){
                            if($v->status == 'new_case'){
                                $v->status_name = trans('aid::application.new beneficiary');
                                $case_added++;
                            }else{
                                $v->status_name = trans('aid::application.old beneficiary');
                            }
                        }

                        foreach($restricted as $k=>$v){

                            if ($v['reason'] == 'not_person') {
                                $not_person++;
                            }
                            if ($v['reason'] == 'blocked') {
                                $blocked++;
                            }
                            if ($v['reason'] == 'nominated') {
                                $nominated++;
                            }
                            if ($v['reason'] == 'duplicated') {
                                $duplicated++;
                            }
                            if ($v['reason'] == 'out_of_regions') {
                                $out_of_regions++;
                            }
                            if ($v['reason'] == 'out_of_ratio') {
                                $out_of_ratio++;
                            }
                            if ($v['reason'] == 'policy_restricted') {
                                $policy_restricted++;
                            }

                            if ($v['reason'] == 'not_case') {
                                $not_case++;
                            }
                            if ($v['reason'] == 'limited') {
                                $limited++;
                            }
                            if ($v['reason'] == 'invalid_id_card_number') {
                                $invalid_id_card_number++;
                            }

                            if(isset($v['sub'])){
                                $v['reason'] = trans('aid::application.policy_restricted') .' '. trans('aid::application.'.$v['sub']);
                            }else{
                                $v['reason'] = trans('aid::application.'.$v['reason']);
                            }
                        }
                    }

                    $token = md5(uniqid());
                    \Excel::create('export_' . $token, function ($excel) use ($final,$restricted) {

                        if(sizeof($final) > 0){
                            $excel->sheet(trans('aid::application.Beneficiaries'), function ($sheet) use ($final) {
                                $sheet->setStyle([
                                    'font' => [
                                        'name' => 'Calibri',
                                        'size' => 11,
                                        'bold' => true
                                    ]
                                ]);
                                $sheet->setAllBorders('thin');
                                $sheet->setfitToWidth(true);
                                $sheet->setRightToLeft(true);
                                $style = [
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font' => [
                                        'name' => 'Simplified Arabic',
                                        'size' => 12,
                                        'bold' => true
                                    ]
                                ];

                                $sheet->getStyle("A1:C1")->applyFromArray($style);
                                $sheet->setHeight(1, 30);

                                $style = [
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font' => [
                                        'name' => 'Simplified Arabic',
                                        'size' => 12,
                                        'bold' => false
                                    ]
                                ];

                                $sheet->getDefaultStyle()->applyFromArray($style);
                                $sheet->getDefaultStyle()->applyFromArray($style);

                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                $sheet ->setCellValue('B1',trans('common::application.name'));
                                $sheet ->setCellValue('C1',trans('common::application.status'));
                                $z= 2;

                                foreach($final as $k=>$v){
                                    $sheet ->setCellValue('A'.$z,$v->id_card_number);
                                    $sheet ->setCellValue('B'.$z,$v->name);
                                    $sheet ->setCellValue('C'.$z,$v->status_name);
                                    $z++;
                                }
                            });
                        }

                        if(sizeof($restricted) > 0){
                            $excel->sheet(trans('aid::application.rejected_sheet'), function ($sheet) use ($restricted) {
                                $sheet->setStyle([
                                    'font' => [
                                        'name' => 'Calibri',
                                        'size' => 11,
                                        'bold' => true
                                    ]
                                ]);
                                $sheet->setAllBorders('thin');
                                $sheet->setfitToWidth(true);
                                $sheet->setRightToLeft(true);
                                $style = [
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font' => [
                                        'name' => 'Simplified Arabic',
                                        'size' => 12,
                                        'bold' => true
                                    ]
                                ];

                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                $sheet->setHeight(1, 30);

                                $style = [
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font' => [
                                        'name' => 'Simplified Arabic',
                                        'size' => 12,
                                        'bold' => false
                                    ]
                                ];

                                $sheet->getDefaultStyle()->applyFromArray($style);

                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                $sheet ->setCellValue('B1',trans('common::application.reason'));
                                $z= 2;
                                foreach($restricted as $k=>$v){
                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                    $sheet ->setCellValue('B'.$z,trans('aid::application.'.$v['reason']));
                                    $z++;
                                }
                            });
                        }

                    })->store('xlsx', storage_path('tmp/'));

                    $response["download_token"] = $token;
                }

                if(sizeof($restricted)  == sizeof($records)){
                    $response["status"] = 'inserted_error';
                    $response["msg"] = trans('aid::application.The voucher row is not insert to db'). ' , ' .
                        trans('aid::application.Some cases has been successfully nomination to voucher but another is restricted') . ' ( '
                        . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                        trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_id_card_number . ' , ' .
                        trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                        trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                        trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                        trans('aid::application.out_of_regions') . ' : ' . $out_of_regions . ' , ' .
                        trans('aid::application.policy_restricted') . ' : ' . $policy_restricted . ' , ' .
                        trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                        trans('aid::application.out_of_ratio') . ' : ' . $out_of_ratio . ' , ' .
                        trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                        trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                }else{
                    if(sizeof($restricted) == 0){
                        $response["status"] = 'success';
                        $response["msg"] = trans('aid::application.The voucher row is inserted to db');
                    }
                    else{
                        $response["status"] = 'success';
                        $response["msg"] =
                            trans('aid::application.Some cases has been successfully nomination to voucher but another is restricted') . ' ( '
                            . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                            trans('aid::application.success') . ' : ' . sizeof($final) . ' , ' .
                            trans('aid::application.register_as_new_case') . ' : ' . $case_added . ' , ' .
                            trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_id_card_number . ' , ' .
                            trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                            trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                            trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                            trans('aid::application.out_of_regions') . ' : ' . $out_of_regions . ' , ' .
                            trans('aid::application.policy_restricted') . ' : ' . $policy_restricted . ' , ' .
                            trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                            trans('aid::application.out_of_ratio') . ' : ' . $out_of_ratio . ' , ' .
                            trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                            trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                    }
                }
                return response()->json($response);
            }

        }

        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('aid::application.No filter cases were specified')]);

    }

    // import cases as beneficiary of voucher  by voucher_id , person_id  using excel sheet
    public function cases(Request $request)
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit', '2048M');

        $voucher = Vouchers::findorfail($request->voucher_id);

        $to_date = date('Y-m-d', strtotime($voucher->voucher_date . ' + ' . $voucher->allow_day . ' days'));

        if (strtotime($to_date) > strtotime('now')) {
            $response["status"] = 'inserted_error';
            $response["msg"] = trans('aid::application.exceeded the number of days allowed to add beneficiaries to voucher');
            return response()->json($response);
        }

        $importFile = $request->file;

        if ($importFile->isValid()) {

            $path = $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load($path);
            $sheets = $excel->getSheetNames();

            if(in_array('data', $sheets)) {

                $first = Excel::selectSheets('data')->load($path)->first();

                if (!is_null($first)) {

                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ) {

                        $validFile = true;
                        $constraint = ["rkm_alhoy", "okt_alastlam" ,"mkan_alastlam" , "tarykh_alastlam"];
                        foreach ($constraint as $item_) {
                            if ( !in_array($item_, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if ($validFile) {
                            $records = \Excel::selectSheets('data')->load($path)->get();
                            $total = sizeof($records);
                            if ($total > 0) {

                                $rules = [
                                    'voucher_id' => 'required|integer',
//                                    'receipt_date' => 'required|date'
//                                    , 'receipt_location' => 'required|max:255',
                                ];
                                $input = [
                                    'voucher_id' => $request->voucher_id,
//                                    'receipt_date' => strip_tags($request->receipt_date)
//                                    , 'receipt_location' => strip_tags($request->receipt_location)
                                ];

                                $error = \App\Http\Helpers::isValid($input, $rules);
                                if ($error)
                                    return response()->json($error);

                                $old_v =VoucherPersons::where(['voucher_id' => $input['voucher_id']])->count();
                                $max = $voucher->count - $old_v;

                                if ($max == 0) {
                                    $response["status"] = 'inserted_error';
                                    $response["msg"] = trans('aid::application.The voucher is max exceed persons');
                                    return response()->json($response);
                                }

                                $user = \Auth::user();
                                $organization_id = $user->organization_id;
                                $response = array();
                                $add_cases = $request->add_cases;

                                $restricted = [];
                                $list = [];
                                $details =[];
                                $individuals =[];

                                $beneficiaries = [];
                                $processed = [];
                                $beneficiaries = [];
                                $passed = [];

                                $case_added = 0;
                                $not_person = 0;
                                $invalid_id_card_number = 0;
                                $not_case = 0;
                                $blocked = 0;
                                $out_of_regions = 0;
                                $policy_restricted = 0;
                                $duplicated = 0;
                                $nominated = 0;
                                $limited = 0;
                                $out_of_ratio = 0;

                                foreach ($records as $key => $value) {
                                    $id_card_number = (int)$value['rkm_alhoy'];
                                    if (strlen($id_card_number) <= 9 ) {

                                        if(GovServices::checkCard($id_card_number)){

                                            if (!in_array($id_card_number, $list)) {
                                                $temp = ['receipt_time'=>null,'voucher_id'=>$request->voucher_id];

                                                    if (isset($value['okt_alastlam'])) {
                                                        if (!is_null($value['okt_alastlam']) && $value['okt_alastlam'] != ' ' && $value['okt_alastlam'] != '') {
                                                            if (is_object($value['okt_alastlam'])) {
                                                                $temp['receipt_time'] = date('H:i:s',strtotime($value['okt_alastlam']));
                                                            }
                                                        }
                                                    }

                                                    if (isset($value['mkan_alastlam'])) {
                                                        if (!is_null($value['mkan_alastlam']) && $value['mkan_alastlam'] != ' ' && $value['mkan_alastlam'] != '') {
                                                            $temp['receipt_location'] = $value['mkan_alastlam'];
                                                        }
                                                    }
                                                    if (isset($value['tarykh_alastlam'])) {
                                                        if (!is_null($value['tarykh_alastlam']) && $value['tarykh_alastlam'] != ' ' && $value['tarykh_alastlam'] != '') {
                                                            $receipt_date = Helpers::getFormatedDate($value['tarykh_alastlam']);
                                                            if (!is_null($receipt_date)) {
                                                                $temp['receipt_date']=$receipt_date;
                                                            }
                                                        }
                                                    }

                                                $details[$id_card_number] = $temp;
                                                $list[] = $id_card_number;
                                            } else {
                                                $restricted[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                                            }
                                        }else{
                                            $restricted[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'invalid_id_card_number'];
                                            $processed[] = $id_card_number;
                                        }
                                    } else {
                                        $restricted[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'invalid_id_card_number'];
                                        $processed[] = $id_card_number;
                                    }
                                }

                                if(sizeof($list) > 0 ){

                                    $voucher_id = (int)$request->voucher_id;
                                    $response['check'] = $this->checkCases($add_cases,$details,$list,$voucher->case_category_id,$voucher_id,$organization_id,$voucher->value,$user->id,$request->ratio,$restricted,$voucher->count,$old_v);
                                    $final = $response['check']['final'];
                                    $restricted = $response['check']['restricted'];
                                    $person_vouchers = $response['check']['person_vouchers'];

                                    if (sizeof($final) > 0 || sizeof($restricted) > 0 || sizeof($person_vouchers) > 0) {

                                        if(sizeof($person_vouchers) > 0) {
                                           VoucherPersons::insert($person_vouchers);
                                        }

                                        if(sizeof($final) > 0) {

                                            foreach($final as $k=>$v){
                                                if($v->status == 'new_case'){
                                                    $v->status_name = trans('aid::application.new beneficiary');
                                                    $case_added++;
                                                }else{
                                                    $v->status_name = trans('aid::application.old beneficiary');
                                                }
                                            }
                                        }
                                        if(sizeof($restricted) > 0) {
                                            foreach($restricted as $k=>$v){

                                                if ($v['reason'] == 'not_person') {
                                                    $not_person++;
                                                }
                                                if ($v['reason'] == 'blocked') {
                                                    $blocked++;
                                                }
                                                if ($v['reason'] == 'nominated') {
                                                    $nominated++;
                                                }
                                                if ($v['reason'] == 'duplicated') {
                                                    $duplicated++;
                                                }
                                                if ($v['reason'] == 'out_of_regions') {
                                                    $out_of_regions++;
                                                }
                                                if ($v['reason'] == 'out_of_ratio') {
                                                    $out_of_ratio++;
                                                }
                                                if ($v['reason'] == 'policy_restricted') {
                                                    $policy_restricted++;
                                                }

                                                if ($v['reason'] == 'not_case') {
                                                    $not_case++;
                                                }
                                                if ($v['reason'] == 'limited') {
                                                    $limited++;
                                                }
                                                if ($v['reason'] == 'invalid_id_card_number') {
                                                    $invalid_id_card_number++;
                                                }

                                                if(isset($v['sub'])){
                                                    $v['reason'] = trans('aid::application.policy_restricted') .' '. trans('aid::application.'.$v['sub']);
                                                }else{
                                                    $v['reason'] = trans('aid::application.'.$v['reason']);
                                                }
                                            }
                                        }

                                        $token = md5(uniqid());
                                        \Excel::create('export_' . $token, function ($excel) use ($final,$restricted) {

                                            if(sizeof($final) > 0){
                                                $excel->sheet(trans('aid::application.Beneficiaries'), function ($sheet) use ($final) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                       'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:C1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                       'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);
                                                    $sheet->getDefaultStyle()->applyFromArray($style);

                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('B1',trans('common::application.name'));
                                                    $sheet ->setCellValue('C1',trans('common::application.status'));
                                                    $z= 2;

                                                    foreach($final as $k=>$v){
                                                        $sheet ->setCellValue('A'.$z,$v->id_card_number);
                                                        $sheet ->setCellValue('B'.$z,$v->name);
                                                        $sheet ->setCellValue('C'.$z,$v->status_name);
                                                        $z++;
                                                    }
                                                });
                                            }

                                            if(sizeof($restricted) > 0){
                                                $excel->sheet(trans('aid::application.rejected_sheet'), function ($sheet) use ($restricted) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);

                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('B1',trans('common::application.reason'));
                                                    $z= 2;
                                                    foreach($restricted as $k=>$v){
                                                        $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                        $sheet ->setCellValue('B'.$z,trans('aid::application.'.$v['reason']));
                                                        $z++;
                                                    }
                                                });
                                            }

                                        })->store('xlsx', storage_path('tmp/'));

                                        $response["download_token"] = $token;
                                    }

                                    if(sizeof($restricted)  == sizeof($records)){
                                        $response["status"] = 'inserted_error';
                                        $response["msg"] = trans('aid::application.The voucher row is not insert to db'). ' , ' .
                                            trans('aid::application.Some cases has been successfully nomination to voucher but another is restricted') . ' ( '
                                            . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                            trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_id_card_number . ' , ' .
                                            trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                                            trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                                            trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                                            trans('aid::application.out_of_regions') . ' : ' . $out_of_regions . ' , ' .
                                            trans('aid::application.policy_restricted') . ' : ' . $policy_restricted . ' , ' .
                                            trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                                            trans('aid::application.out_of_ratio') . ' : ' . $out_of_ratio . ' , ' .
                                            trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                            trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                                    }else{
                                        if(sizeof($restricted) == 0){
                                            $response["status"] = 'success';
                                            $response["msg"] = trans('aid::application.The voucher row is inserted to db');
                                        }
                                        else{
                                            $response["status"] = 'success';
                                            $response["msg"] =
                                                trans('aid::application.Some cases has been successfully nomination to voucher but another is restricted') . ' ( '
                                                . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                                trans('aid::application.success') . ' : ' . sizeof($final) . ' , ' .
                                                trans('aid::application.register_as_new_case') . ' : ' . $case_added . ' , ' .
                                                trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_id_card_number . ' , ' .
                                                trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                                                trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                                                trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                                                trans('aid::application.out_of_regions') . ' : ' . $out_of_regions . ' , ' .
                                                trans('aid::application.policy_restricted') . ' : ' . $policy_restricted . ' , ' .
                                                trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                                                trans('aid::application.out_of_ratio') . ' : ' . $out_of_ratio . ' , ' .
                                                trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                                trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                                        }
                                    }
                                    return response()->json($response);
                                }
                            }
                            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file is empty')]);
                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);

                    }
                }

                return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file is empty')]);
            }

            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('aid::application.The import sheet does not exist in the Excel , please check the label in the instructions or in the template') ]);
        }

        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.invalid file')]);
    }

    function checkCases($add_cases,$details,$list,$category_id,$voucher_id,$organization_id,$amount,$user_id,$ratio,$restricted,$count,$ben_count){

        $organizations_map = array();
        $not_case = array();
        $not_person = array();
        $new_cases = array();
        $persons_card_list = array();
        $passed_item = array();
        $final = array();
        $person_vouchers = array();

        $first =date('Y-01-01');
        $last =date('Y-12-t');

        $user = \Auth::user();
        $UserType=$user->type;
        $ancestor_id = \Organization\Model\Organization::getAncestor($organization_id);

        $user_id = $user->id;
        $max_total=0;
        $max_total_amount_of_family=\DB::table('char_settings')
            ->where('char_settings.id','=','max_total_amount_of_voucher')
            ->where('char_settings.organization_id',$ancestor_id)
            ->first();

        if($max_total_amount_of_family){
            if($max_total_amount_of_family->value && $max_total_amount_of_family->value != 0){
                $max_total=$max_total_amount_of_family->value;
            }
        }

        $person_policy_restricted= \DB::table('char_categories_policy')
            ->where('char_categories_policy.category_id','=',$category_id)
            ->where('char_categories_policy.level','=',1)
            ->selectRaw("char_categories_policy.*")
            ->first();

        $organizations = \DB::table('char_organizations')
            ->whereIn('id',function ($w) use ($list,$ancestor_id,$UserType,$user,$category_id){
                $w->select('organization_id')
                    ->from('char_cases')
                    ->where(function ($q) use ($list,$ancestor_id,$UserType,$user,$category_id){
                        $q->whereIn('person_id' , function ($q_) use ($list){
                            $q_->select('id')
                                ->from('char_persons')
                                ->where(function($q)  use ($list){
                                    $q->whereIn('char_persons.id_card_number',$list);
                                });
                        });
                        $q->where('category_id',$category_id);
//                        $q->where('status',0);
                    });
            })
            ->selectRaw('char_organizations.*')
            ->groupBy('char_organizations.id')
            ->get();

        foreach($organizations as $k=>$v) {
            $organizations_map[$v->id]=OrgLocations::getConnected($v->id);
        }

        $mosques_map = [];
        $mosques = [];
        $mosques_ratio = [];

        if($ratio == 1 || $ratio == '1'){
            $mosques = \DB::table('char_aids_locations')
                ->whereIn('id',function ($w) use ($list,$ancestor_id,$UserType,$user,$category_id){
                    $w->select('adsmosques_id')
                        ->from('char_persons')
                        ->whereIn('char_persons.id_card_number',$list);
                })
                ->selectRaw('char_aids_locations.*')
                ->get();
            foreach($mosques as $k=>$v) {
                $mosques_map[$v->id]=$v;
            }

            $mosques_ratio = aidsLocation::getTotalCountTree($count,$voucher_id);
        }

        $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$category_id)->first();

        $persons =  \DB::table('char_persons')
                    ->leftjoin('char_cases', function($q) use ($category_id,$user,$UserType,$ancestor_id,$list,$user_id,$organization_id) {
                        $q->on('char_cases.person_id','=','char_persons.id');
                        if($UserType == 2) {
                            $q->whereRaw("(char_cases.category_id = $category_id And char_cases.status = 0) And
                                            ( char_cases.organization_id = $organization_id or  char_cases.organization_id IN (
                                                                      select char_user_organizations.organization_id 
                                                                      from   char_user_organizations 
                                                                      where  char_user_organizations.user_id = $user_id))");

                        }
                        else{

                            $q->whereRaw("(char_cases.category_id = $category_id And char_cases.status = 0) And
                                            ( char_cases.organization_id = $organization_id or   char_cases.organization_id IN (
                                                                      select char_organizations_closure.descendant_id 
                                                                      from   char_organizations_closure 
                                                                      where  char_organizations_closure.ancestor_id = $organization_id))");
                        }
                    })
                    ->leftjoin('char_block_id_card_number', function($q)  {
                            $q->on('char_block_id_card_number.id_card_number','=','char_persons.id_card_number');
                            $q->where('char_block_id_card_number.type','=',2);
                        })
                        ->leftjoin('char_vouchers_persons', function($q)  use ($voucher_id){
                            $q->on('char_vouchers_persons.person_id','=','char_persons.id');
                            $q->where('char_vouchers_persons.voucher_id','=',$voucher_id);
                        })
                        ->leftjoin('char_block_categories', function($q)  {
                            $q->on('char_block_categories.block_id', '=', 'char_block_id_card_number.id');
                            $q->on('char_block_categories.category_id', '=', 'char_cases.category_id');
                        })
                        ->whereIn('char_persons.id_card_number',$list)
                        ->selectRaw("char_persons.id as person_id , 
                                              char_persons.id_card_number ,
                                              char_persons.family_cnt as family_count ,
                                              char_persons.adscountry_id  ,
                                              char_persons.adsdistrict_id ,
                                              char_persons.adsregion_id ,
                                              char_persons.adsneighborhood_id ,
                                              char_persons.adssquare_id ,
                                              char_persons.adsmosques_id ,
                                              char_cases.id as case_id ,
                                              char_cases.organization_id ,
                                              char_cases.category_id ,
                                              char_vouchers_persons.voucher_id,
                                              CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                                     ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name ,
                                              char_block_categories.block_id
                                             ")
                        ->groupBy('char_persons.id')
                        ->get();

        $processed=[];
        $persons_details =[];
        if(sizeof($persons) > 0 ){

            foreach($persons as $k=>$v){
                $persons_details[$v->person_id]=$v;
                if($count > $ben_count){

                    if($ratio == 1 || $ratio == '1') {

                        if(is_null($v->adsmosques_id)){
                            $restricted[]= ['id_card_number' => $v->id_card_number,'name' => $v->name , 'reason' => 'out_of_ratio'];
                            $processed[]=$v->id_card_number;
                        }else{
                            $mosques_ratio_data = $mosques_map[$v->adsmosques_id];
                            $mosques_limit = $mosques_ratio_data->total - $mosques_ratio_data->count;

                            if($mosques_limit > 0 ){

                                if(!is_null($v->case_id) && ( is_null($v->voucher_id) && is_null($v->block_id))){
                                    $passed = true;
                                    $mainConnector= $organizations_map[$v->organization_id];
                                    if(!is_null($v->adsdistrict_id)){
                                        if(!in_array($v->adsdistrict_id,$mainConnector)){
                                            $passed = false;
                                        }else{
                                            if(!is_null($v->adsregion_id)){
                                                if(!in_array($v->adsregion_id,$mainConnector)){
                                                    $passed = false;
                                                }else{

                                                    if(!is_null($v->adsneighborhood_id)){
                                                        if(!in_array($v->adsneighborhood_id,$mainConnector)){
                                                            $passed = false;
                                                        }else{

                                                            if(!is_null($v->adssquare_id)){
                                                                if(!in_array($v->adssquare_id,$mainConnector)){
                                                                    $passed = false;
                                                                }else{
                                                                    if(!is_null($v->adsmosques_id)){
                                                                        if(!in_array($v->adsmosques_id,$mainConnector)){
                                                                            $passed = false;
                                                                        }
                                                                    }else{
                                                                        $passed = false;
                                                                    }
                                                                }
                                                            }else{
                                                                $passed = false;
                                                            }
                                                        }
                                                    }else{
                                                        $passed = false;
                                                    }
                                                }
                                            }else{
                                                $passed = false;
                                            }
                                        }
                                    }else{
                                        $passed = false;
                                    }

                                    if($passed == false){
                                        $restricted[]= ['id_card_number' => $v->id_card_number,'name' => $v->name , 'reason' => 'out_of_regions'];
                                        $processed[]=$v->id_card_number;
                                    }else{
                                        $restrict = false ;
                                        $reason = '';
                                        $sub = '';
                                        if($person_policy_restricted){
                                            $voucher_persons_cnt=
                                                \DB::table('char_persons')
                                                    ->where('char_persons.id','=',$v->person_id)
                                                    ->selectRaw("char_get_voucher_persons_count ( char_persons.id  ,'$person_policy_restricted->started_at','$person_policy_restricted->ends_at') AS amount")
                                                    ->first();


                                            $person_current_year_amount=
                                                \DB::table('char_persons')
                                                    ->where('char_persons.id','=',$v->person_id)
                                                    ->selectRaw("char_get_voucher_persons_count ( char_persons.id  ,'$first','$last') AS amount")
                                                    ->first();

                                            if($voucher_persons_cnt){
                                                if($person_policy_restricted->count_of_family != 0){
                                                    if($person_policy_restricted->count_of_family <= $v->family_count){
                                                        $restrict = true ;
                                                        $reason = 'policy_restricted';
                                                        $sub = 'restricted_count_of_family';
                                                    }
                                                }

                                                if($person_policy_restricted->amount != null && $person_policy_restricted->amount != 0){
                                                    if($person_policy_restricted->amount <= ($voucher_persons_cnt->amount + $amount)){
                                                        $restrict = true ;
                                                        $reason = 'policy_restricted';
                                                        $sub = 'restricted_amount';
                                                    }
                                                }

                                                if($max_total_amount_of_family){
                                                    if($max_total <= ($person_current_year_amount->amount + $amount)){
                                                        $restrict = true ;
                                                        $reason = 'policy_restricted';
                                                        $sub = 'restricted_total_payment_persons';
                                                    }
                                                }
                                            }
                                        }

                                        if($restrict == true){
                                            $restricted[]= ['id_card_number' => $v->id_card_number,'name' => $v->name , 'reason' => $reason, 'sub' => $sub];
                                            $processed[]=$v->id_card_number;
                                        }
                                        else{
                                            $v->status = 'old_case';
                                            $passed_item[$v->person_id]=$v;
                                            $persons_list[]=$v->person_id;
                                            $persons_card_list[]=$v->id_card_number;
                                            $v->details = $details[$v->id_card_number];
                                            $v->details['person_id']=$v->person_id;
                                            $person_vouchers[]=$v->details ;
                                            $final[]=$v ;
                                            $mosques_map[$v->adsmosques_id]->count++;
                                            $ben_count++;
                                        }
                                    }
                                }
                                else{
                                    if(!is_null($v->block_id)){
                                        $restricted[]= ['id_card_number' => $v->id_card_number,'name' => $v->name , 'reason' => 'blocked'];
                                        $processed[]=$v->id_card_number;
                                    }
                                    elseif(!is_null($v->voucher_id)){
                                        $restricted[]= ['id_card_number' => $v->id_card_number,'name' => $v->name , 'reason' => 'nominated'];
                                        $processed[]=$v->id_card_number;
                                    }
                                    elseif(is_null($v->case_id)){
                                        if($add_cases == true){
                                            $v->status = 'new_case';
                                            $new_cases[]=$v;
                                            $passed_item[$v->person_id]=$v;
                                            $v->details = $details[$v->id_card_number];
                                            $v->details['person_id']=$v->person_id;
                                            $person_vouchers[]=$v->details ;
                                            $final[]=$v ;
                                            $persons_card_list[]=$v->id_card_number;
                                            $ben_count++;
                                            $mosques_map[$v->adsmosques_id]->count++;
                                        }else{
                                            $not_case[]= $v->person_id;
                                            $processed[]=$v->id_card_number;
                                        }
                                    }
                                }

                            }else{
                                $restricted[]= ['id_card_number' => $v->id_card_number,'name' => $v->name , 'reason' => 'out_of_ratio'];
                                $processed[]=$v->id_card_number;
                            }
                        }
                    }
                    else{
                        if(!is_null($v->case_id) && ( is_null($v->voucher_id) && is_null($v->block_id))){
                            $passed = true;
                            $mainConnector= $organizations_map[$v->organization_id];
                            if(!is_null($v->adsdistrict_id)){
                                if(!in_array($v->adsdistrict_id,$mainConnector)){
                                    $passed = false;
                                }else{
                                    if(!is_null($v->adsregion_id)){
                                        if(!in_array($v->adsregion_id,$mainConnector)){
                                            $passed = false;
                                        }else{

                                            if(!is_null($v->adsneighborhood_id)){
                                                if(!in_array($v->adsneighborhood_id,$mainConnector)){
                                                    $passed = false;
                                                }else{

                                                    if(!is_null($v->adssquare_id)){
                                                        if(!in_array($v->adssquare_id,$mainConnector)){
                                                            $passed = false;
                                                        }else{
                                                            if(!is_null($v->adsmosques_id)){
                                                                if(!in_array($v->adsmosques_id,$mainConnector)){
                                                                    $passed = false;
                                                                }
                                                            }else{
                                                                $passed = false;
                                                            }
                                                        }
                                                    }else{
                                                        $passed = false;
                                                    }
                                                }
                                            }else{
                                                $passed = false;
                                            }
                                        }
                                    }else{
                                        $passed = false;
                                    }
                                }
                            }else{
                                $passed = false;
                            }

                            if($passed == false){
                                $restricted[]= ['id_card_number' => $v->id_card_number,'name' => $v->name , 'reason' => 'out_of_regions'];
                                $processed[]=$v->id_card_number;
                            }else{
                                $restrict = false ;
                                $reason = '';
                                $sub = '';
                                if($person_policy_restricted){
                                    $voucher_persons_cnt=
                                        \DB::table('char_persons')
                                            ->where('char_persons.id','=',$v->person_id)
                                            ->selectRaw("char_get_voucher_persons_count ( char_persons.id  ,'$person_policy_restricted->started_at','$person_policy_restricted->ends_at') AS amount")
                                            ->first();


                                    $person_current_year_amount=
                                        \DB::table('char_persons')
                                            ->where('char_persons.id','=',$v->person_id)
                                            ->selectRaw("char_get_voucher_persons_count ( char_persons.id  ,'$first','$last') AS amount")
                                            ->first();

                                    if($voucher_persons_cnt){
                                        if($person_policy_restricted->count_of_family != 0){
                                            if($person_policy_restricted->count_of_family <= $v->family_count){
                                                $restrict = true ;
                                                $reason = 'policy_restricted';
                                                $sub = 'restricted_count_of_family';
                                            }
                                        }

                                        if($person_policy_restricted->amount != null && $person_policy_restricted->amount != 0){
                                            if($person_policy_restricted->amount <= ($voucher_persons_cnt->amount + $amount)){
                                                $restrict = true ;
                                                $reason = 'policy_restricted';
                                                $sub = 'restricted_amount';
                                            }
                                        }

                                        if($max_total_amount_of_family){
                                            if($max_total <= ($person_current_year_amount->amount + $amount)){
                                                $restrict = true ;
                                                $reason = 'policy_restricted';
                                                $sub = 'restricted_total_payment_persons';
                                            }
                                        }
                                    }
                                }

                                if($restrict == true){
                                    $restricted[]= ['id_card_number' => $v->id_card_number,'name' => $v->name , 'reason' => $reason, 'sub' => $sub];
                                    $processed[]=$v->id_card_number;
                                }else{
                                    $v->status = 'old_case';
                                    $passed_item[$v->person_id]=$v;
                                    $persons_list[]=$v->person_id;
                                    $persons_card_list[]=$v->id_card_number;
                                    $v->details = $details[$v->id_card_number];
                                    $v->details['person_id']=$v->person_id;
                                    $person_vouchers[]=$v->details ;
                                    $final[]=$v ;
                                    $ben_count++;
                                }
                            }

                        }
                        else{
                            if(!is_null($v->block_id)){
                                $restricted[]= ['id_card_number' => $v->id_card_number,'name' => $v->name , 'reason' => 'blocked'];
                                $processed[]=$v->id_card_number;
                            }elseif(!is_null($v->voucher_id)){
                                $restricted[]= ['id_card_number' => $v->id_card_number,'name' => $v->name , 'reason' => 'nominated'];
                                $processed[]=$v->id_card_number;
                            }elseif(is_null($v->case_id)){
                                if($add_cases == true){
                                    $v->status = 'new_case';
                                    $new_cases[]=$v;
                                    $passed_item[$v->person_id]=$v;
                                    $v->details = $details[$v->id_card_number];
                                    $v->details['person_id']=$v->person_id;
                                    $person_vouchers[]=$v->details ;
                                    $final[]=$v ;
                                    $persons_card_list[]=$v->id_card_number;
                                    $ben_count++;
                                }else{
                                    $not_case[]= $v->person_id;
                                    $processed[]=$v->id_card_number;
                                }
                            }
                        }
                    }

                }else{
                    $restricted[]= ['id_card_number' => $v->id_card_number,'name' => $v->name , 'reason' => 'limited'];
                    $processed[]=$v->id_card_number;
                }
            }

        }

        foreach($list as $card){
            if (!in_array($card, $persons_card_list) && !in_array($card, $processed)) {
                $restricted[]= ['id_card_number' => $card,'name' => ' ' , 'reason' => 'not_person'];
            }
        }

        if(sizeof($new_cases) > 0 ){
            foreach($new_cases as $k =>$v ){

                $exist =  \Common\Model\CaseModel::where(['person_id' => $v->person_id ,'organization_id' => $user->organization_id ])
                    ->first();

                if(is_null($exist)){
                    $case=  \Common\Model\AidsCases::create(['person_id' =>  $v->person_id,
                        'status' => 1, 'category_id' => $category_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'user_id' => $user->id,
                        'rank' => 0,
                        'organization_id' => $user->organization_id]);

                    \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id,
                        'reason'=>trans('common::application.case was created'), 'status'=>0, 'date'=>date("Y-m-d")]);
                    \Log\Model\Log::saveNewLog('CASE_CREATED',trans('common::application.Has added new case data for')  . '  : '.' "'. ' ' .  $v->name . ' " ' . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);

                }
            }
        }

        $this->DisablePersonInNoConnected($list,$organizations_map,$persons_details);

        return array('final'=>$final,'restricted'=>$restricted,'person_vouchers'=>$person_vouchers,'new_cases'=>$new_cases);

    }

    function DisablePersonInNoConnected($cards,$organization_connector_map,$persons_map){

        $query = \DB::table('char_cases')
            ->join('char_categories', function ($join){
                $join->on('char_categories.id', '=', 'char_cases.category_id');
                $join->where('char_categories.type', 2);
            })
            ->where(function($q_) use($cards) {
                $q_->where('status',0);
                $q_->wherein('char_cases.person_id', function($query_) use($cards) {
                        $query_->select('id')
                            ->from('char_persons')
                            ->whereIn('char_persons.id_card_number',$cards);
                   });
             })
            ->selectRaw('char_cases.*')->get();

        $on_range = [];
        foreach ($query as $k=>$v) {
            $passed = true;
            $person = $persons_map[$v->person_id];
            $mainConnector = $organization_connector_map[$v->organization_id];
            if(!is_null($person->adsdistrict_id)){
                if(!in_array($person->adsdistrict_id,$mainConnector)){
                    $passed = false;
                }else{
                    if(!is_null($person->adsregion_id)){
                        if(!in_array($person->adsregion_id,$mainConnector)){
                            $passed = false;
                        }else{

                            if(!is_null($person->adsneighborhood_id)){
                                if(!in_array($person->adsneighborhood_id,$mainConnector)){
                                    $passed = false;
                                }else{

                                    if(!is_null($person->adssquare_id)){
                                        if(!in_array($person->adssquare_id,$mainConnector)){
                                            $passed = false;
                                        }else{
                                            if(!is_null($person->adsmosques_id)){
                                                if(!in_array($person->adsmosques_id,$mainConnector)){
                                                    $passed = false;
                                                }
                                            }else{
                                                $passed = false;
                                            }
                                        }
                                    }else{
                                        $passed = false;
                                    }
                                }
                            }else{
                                $passed = false;
                            }
                        }
                    }else{
                        $passed = false;
                    }
                }
            }else{
                $passed = false;
            }
            if(!$passed){
                \Common\Model\CaseModel::where(['id' => $v->id ])->update(['status' => 1]);
                $action='CASE_UPDATED';
                $message=trans('common::application.edited data') . '  : '.' "'.$person->name. ' " ';
                \Log\Model\Log::saveNewLog($action,$message);
                \Common\Model\CasesStatusLog::create(['case_id'=>$v->id, 'user_id'=>$v->user_id,
                    'reason'=>trans('common::application.Disabled your recorded status for') . ' "'.$person->name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization'), 'status'=>1, 'date'=>date("Y-m-d")]);
                \Log\Model\Log::saveNewLog('CASE_UPDATED',trans('common::application.Disabled your recorded status for') . ' "'.$person->name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization'));
            }
        }
        return true;
    }

    // *************************************************************************************************************//
    // *************************************************************************************************************//
    // import cases as beneficiary of voucher  by voucher_id , person_id , individual_id ( null ) using excel sheet
    public function individuals(Request $request)
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit', '2048M');

        $voucher = Vouchers::findorfail($request->voucher_id);

        $to_date = date('Y-m-d', strtotime($voucher->voucher_date . ' + ' . $voucher->allow_day . ' days'));

        $category_id = $voucher->case_category_id;
        $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$category_id)->first();

        if (strtotime($to_date) > strtotime('now')) {
            $response["status"] = 'inserted_error';
            $response["msg"] = trans('aid::application.exceeded the number of days allowed to add beneficiaries to voucher');
            return response()->json($response);
        }

        $importFile = $request->file;
        if ($importFile->isValid()) {

            $path = $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load($path);
            $sheets = $excel->getSheetNames();

            if(in_array('data', $sheets)) {

                $first = Excel::selectSheets('data')->load($path)->first();

                if (!is_null($first)) {

                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ) {

                        $validFile = true;
                        $constraint = ["rkm_alhoy_rb_alasr","rkm_hoy_almaaal", "okt_alastlam" ,"mkan_alastlam" , "tarykh_alastlam"];
                        foreach ($constraint as $item_) {
                            if ( !in_array($item_, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if ($validFile) {
                            $records = \Excel::selectSheets('data')->load($path)->get();
                            $total = sizeof($records);

                            if ($total > 0) {

                                $rules = ['voucher_id' => 'required|integer'];
                                $input = ['voucher_id' => $request->voucher_id];

                                $error = \App\Http\Helpers::isValid($input, $rules);
                                if ($error)
                                    return response()->json($error);

                                $old_v =VoucherPersons::where(['voucher_id' => $input['voucher_id']])->count();
                                $max = $voucher->count - $old_v;

                                if ($max == 0) {
                                    return response()->json(['status' => 'inserted_error' ,
                                        'msg' => trans('aid::application.The voucher is max exceed persons')]);
                                }

                                $add_cases = $request->add_cases;
                                $user = \Auth::user();
                                $organization_id = $user->organization_id;

                                $response = array();

                                $restricted = [];
                                $individuals =[];
                                $individuals_map =[];


                                $list = [];
                                $individuals_list = [];
                                $details =[];
                                $individuals_rev =[];
                                $processed = [];

                                $case_added = 0;
                                $not_person = 0;
                                $invalid_id_card_number = 0;
                                $not_case = 0;
                                $blocked = 0;
                                $out_of_regions = 0;
                                $invalid_individual_id_card_number = 0;
                                $policy_restricted = 0;
                                $duplicated = 0;
                                $nominated = 0;
                                $limited = 0;
                                $out_of_ratio = 0;

                                foreach ($records as $key => $value) {
                                    if (isset($value['rkm_hoy_almaaal']) && isset($value['rkm_alhoy_rb_alasr'])) {
                                        $id_card_number = $value['rkm_alhoy_rb_alasr'];
                                        $individual_id_card_number = $value['rkm_hoy_almaaal'];

                                        if (!is_null($id_card_number) && $id_card_number != ' ' && $id_card_number != '' &&
                                            !is_null($individual_id_card_number) && $individual_id_card_number != ' ' &&
                                            $individual_id_card_number != '')
                                        {

                                            if(in_array($value['rkm_hoy_almaaal'] , $individuals)){
                                                $restricted[] = ['name' => ' ', 'id_card_number' => $individual_id_card_number, 'reason' => 'duplicate'];
                                            }else{
                                                $individual_id_card_number = (int) $individual_id_card_number;
                                                $id_card_number = (int) $id_card_number;

                                                if(strlen($id_card_number) <= 9 && strlen($individual_id_card_number) <= 9){

                                                    $caseCheckCard =  GovServices::checkCard($id_card_number) ;
                                                    $individualCheckCard =  GovServices::checkCard($individual_id_card_number) ;

                                                    if($caseCheckCard && $individualCheckCard ){
                                                        $temp = ['receipt_time'=>null,'voucher_id'=>$request->voucher_id];

                                                        if (isset($value['okt_alastlam'])) {
                                                            if (!is_null($value['okt_alastlam']) && $value['okt_alastlam'] != ' ' && $value['okt_alastlam'] != '') {
                                                                if (is_object($value['okt_alastlam'])) {
                                                                    $temp['receipt_time'] = date('H:i:s',strtotime($value['okt_alastlam']));
                                                                }
                                                            }
                                                        }

                                                        if (isset($value['mkan_alastlam'])) {
                                                            if (!is_null($value['mkan_alastlam']) && $value['mkan_alastlam'] != ' ' && $value['mkan_alastlam'] != '') {
                                                                $temp['receipt_location'] = $value['mkan_alastlam'];
                                                            }
                                                        }

                                                        if (isset($value['tarykh_alastlam'])) {
                                                            if (!is_null($value['tarykh_alastlam']) && $value['tarykh_alastlam'] != ' ' && $value['tarykh_alastlam'] != '') {
                                                                $receipt_date = Helpers::getFormatedDate($value['tarykh_alastlam']);
                                                                if (!is_null($receipt_date)) {
                                                                    $temp['receipt_date']=$receipt_date;
                                                                }
                                                            }
                                                        }
                                                        $temp['id_card_number']=$id_card_number;

                                                        $details[$individual_id_card_number] = $temp;
                                                        $individuals_rev[$individual_id_card_number]=$id_card_number;

                                                    }else{
                                                        if(!$caseCheckCard) {
                                                            $restricted[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'invalid_id_card_number'];
                                                        }

                                                        if(!$individualCheckCard) {
                                                            $restricted[] = ['name' => ' ', 'id_card_number' => $individual_id_card_number, 'reason' => 'invalid_individual_id_card_number'];
                                                        }
                                                    }
                                                }
                                            }

                                            $individuals[] = $individual_id_card_number;

                                        }else{
                                            if(!strlen($id_card_number) <= 9) {
                                                $restricted[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'invalid_id_card_number'];
                                            }

                                            if(!strlen($individual_id_card_number) <= 9) {
                                                $restricted[] = ['name' => ' ', 'id_card_number' => $individual_id_card_number, 'reason' => 'invalid_individual_id_card_number'];
                                            }
                                        }
                                    }
                                }

                                if(sizeof($individuals) > 0 ){

                                    $voucher_id = (int)$request->voucher_id;
                                    $response['check'] = $this->checkIndividuals($add_cases,$details,$individuals,$individuals_rev,$category_id,$voucher_id,$organization_id,$voucher->value,$user->id,$restricted,$voucher->count,$old_v);

                                    $final = $response['check']['final'];
                                    $restricted = $response['check']['restricted'];
                                    $person_vouchers = $response['check']['person_vouchers'];

                                    if (sizeof($final) > 0 || sizeof($restricted) > 0) {

                                        if(sizeof($person_vouchers) > 0) {
                                           VoucherPersons::insert($person_vouchers);
                                        }

                                        if(sizeof($restricted) > 0) {

                                            foreach($final as $k=>$v){
                                                if($v->status == 'new_case'){
                                                    $v->status_name = trans('aid::application.new beneficiary');
                                                    $case_added++;
                                                }else{
                                                    $v->status_name = trans('aid::application.old beneficiary');
                                                }
                                            }

                                            foreach($restricted as $k=>$v){

                                                if ($v['reason'] == 'not_person') {
                                                    $not_person++;
                                                }
                                                if ($v['reason'] == 'blocked') {
                                                    $blocked++;
                                                }
                                                if ($v['reason'] == 'nominated') {
                                                    $nominated++;
                                                }
                                                if ($v['reason'] == 'duplicated') {
                                                    $duplicated++;
                                                }
                                                if ($v['reason'] == 'out_of_regions') {
                                                    $out_of_regions++;
                                                }
                                                if ($v['reason'] == 'invalid_individual_id_card_number') {
                                                    $invalid_individual_id_card_number++;
                                                }
                                                if ($v['reason'] == 'out_of_ratio') {
                                                    $out_of_ratio++;
                                                }
                                                if ($v['reason'] == 'policy_restricted') {
                                                    $policy_restricted++;
                                                }

                                                if ($v['reason'] == 'not_case') {
                                                    $not_case++;
                                                }
                                                if ($v['reason'] == 'limited') {
                                                    $limited++;
                                                }
                                                if ($v['reason'] == 'invalid_id_card_number') {
                                                    $invalid_id_card_number++;
                                                }

                                                if(isset($v['sub'])){
                                                    $v['reason'] = trans('aid::application.policy_restricted') .' '. trans('aid::application.'.$v['sub']);
                                                }else{
                                                    $v['reason'] = trans('aid::application.'.$v['reason']);
                                                }
                                            }
                                        }

                                        $token = md5(uniqid());
                                        \Excel::create('export_' . $token, function ($excel) use ($final,$restricted) {

                                            if(sizeof($final) > 0){
                                                $excel->sheet(trans('aid::application.Beneficiaries'), function ($sheet) use ($final) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:C1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);
                                                    $sheet->getDefaultStyle()->applyFromArray($style);

                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('B1',trans('common::application.name'));
                                                    $sheet ->setCellValue('C1',trans('common::application.status'));
                                                    $z= 2;

                                                    foreach($final as $k=>$v){
                                                        $sheet ->setCellValue('A'.$z,$v->id_card_number);
                                                        $sheet ->setCellValue('B'.$z,$v->name);
                                                        $sheet ->setCellValue('C'.$z,$v->status_name);
                                                        $z++;
                                                    }
                                                });
                                            }

                                            if(sizeof($restricted) > 0){
                                                $excel->sheet(trans('aid::application.rejected_sheet'), function ($sheet) use ($restricted) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                       'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);

                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('B1',trans('common::application.reason'));
                                                    $z= 2;
                                                    foreach($restricted as $k=>$v){
                                                        $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                        $sheet ->setCellValue('B'.$z,trans('aid::application.'.$v['reason']));
                                                        $z++;
                                                    }
                                                });
                                            }

                                        })->store('xlsx', storage_path('tmp/'));

                                        $response["download_token"] = $token;
                                    }

                                    if(sizeof($restricted)  == sizeof($records)){
                                        $response["status"] = 'inserted_error';
                                        $response["msg"] = trans('aid::application.The voucher row is not insert to db'). ' , ' .
                                            trans('aid::application.Some cases has been successfully nomination to voucher but another is restricted') . ' ( '
                                            . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                            trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_id_card_number . ' , ' .
                                            trans('aid::application.invalid_individual_id_card_number') . ' : ' . $invalid_individual_id_card_number . ' , ' .
                                            trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                                            trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                                            trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                                            trans('aid::application.out_of_regions') . ' : ' . $out_of_regions . ' , ' .
                                            trans('aid::application.policy_restricted') . ' : ' . $policy_restricted . ' , ' .
                                            trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                                            trans('aid::application.out_of_ratio') . ' : ' . $out_of_ratio . ' , ' .
                                            trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                            trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                                    }
                                    else{
                                        if(sizeof($restricted) == 0){
                                            $response["status"] = 'success';
                                            $response["msg"] = trans('aid::application.The voucher row is inserted to db');
                                        }else{
                                            $response["status"] = 'success';
                                            $response["msg"] =
                                                trans('aid::application.Some cases has been successfully nomination to voucher but another is restricted') . ' ( '
                                                . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                                trans('aid::application.success') . ' : ' . sizeof($final) . ' , ' .
                                                trans('aid::application.register_as_new_case') . ' : ' . $case_added . ' , ' .
                                                trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_id_card_number . ' , ' .
                                                trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                                                trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                                                trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                                                trans('aid::application.out_of_regions') . ' : ' . $out_of_regions . ' , ' .
                                                trans('aid::application.policy_restricted') . ' : ' . $policy_restricted . ' , ' .
                                                trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                                                trans('aid::application.out_of_ratio') . ' : ' . $out_of_ratio . ' , ' .
                                                trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                                trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                                        }
                                    }
                                }

                                $response["individuals_map"] = $individuals_map;
                                return response()->json($response);

                            }

                            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file is empty')]);
                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file is empty')]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('aid::application.The import sheet does not exist in the Excel , please check the label in the instructions or in the template') ]);
        }
        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.invalid file')]);
    }

    function checkIndividuals($add_cases,$details,$list,$fathers,$category_id,$voucher_id,$organization_id,$amount,$user_id,$restricted,$count,$ben_count){

        $organizations_map = array();
        $not_case = array();
        $not_person = array();
        $new_cases = array();
        $persons_card_list = array();
        $passed_item = array();
        $final = array();
        $person_vouchers = array();

        $first =date('Y-01-01');
        $last =date('Y-12-t');

        $user = \Auth::user();
        $UserType=$user->type;
        $ancestor_id = \Organization\Model\Organization::getAncestor($organization_id);

        $user_id = $user->id;
        $max_total=0;
        $max_total_amount_of_family=\DB::table('char_settings')
            ->where('char_settings.id','=','max_total_amount_of_voucher')
            ->where('char_settings.organization_id',$ancestor_id)
            ->first();

        if($max_total_amount_of_family){
            if($max_total_amount_of_family->value && $max_total_amount_of_family->value != 0){
                $max_total=$max_total_amount_of_family->value;
            }
        }

        $person_policy_restricted= \DB::table('char_categories_policy')
            ->where('char_categories_policy.category_id','=',$category_id)
            ->where('char_categories_policy.level','=',1)
            ->selectRaw("char_categories_policy.*")
            ->first();

        $organizations = \DB::table('char_organizations')
            ->whereIn('id',function ($w) use ($list,$ancestor_id,$UserType,$user,$category_id,$organization_id){
                $w->select('organization_id')
                    ->from('char_cases')
                    ->where(function ($q) use ($list,$ancestor_id,$UserType,$user,$category_id,$organization_id){
                        if($UserType == 2) {
                            $q->where(function ($anq) use ($organization_id,$user) {
                                $anq->where('char_cases.organization_id',$organization_id);
                                $anq->orwherein('char_cases.organization_id', function ($q_0) use ($user) {
                                    $q_0->select('organization_id')
                                        ->from('char_user_organizations')
                                        ->where('user_id', '=', $user->id);
                                });
                            });

                        }else{
                            $q->where(function ($anq) use ($organization_id) {
                                /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                                $anq->wherein('char_cases.organization_id', function($query) use($organization_id) {
                                    $query->select('descendant_id')
                                        ->from('char_organizations_closure')
                                        ->where('ancestor_id', '=', $organization_id);
                                });
                            });
                        }
                        $q->whereIn('person_id' , function ($q_) use ($list){
                            $q_->select('id')
                                ->from('char_persons')
                                ->whereIn('char_persons.id_card_number',$list);
                        });
                        $q->where('category_id',$category_id);
//                                            $q->where('status',0);
                    });
            })
            ->selectRaw('char_organizations.*')
            ->groupBy('char_organizations.id')
            ->get();

        foreach($organizations as $k=>$v) {
            $organizations_map[$v->id]=OrgLocations::getConnected($v->id);
        }

        $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$category_id)->first();

        $persons =  \DB::table('char_persons')
                        ->leftjoin('char_cases', function($q) use ($category_id,$user,$UserType,$ancestor_id,$list,$user_id,$organization_id) {
                            $q->on('char_cases.person_id','=','char_persons.id');
                            if($UserType == 2) {
                                $q->whereRaw("(char_cases.category_id = $category_id ) And
                                                ( char_cases.organization_id =   $organization_id or char_cases.organization_id IN (
                                                                          select char_user_organizations.organization_id 
                                                                          from   char_user_organizations 
                                                                          where  char_user_organizations.user_id = $user_id))");

                            }
                            else{

                                $q->whereRaw("(char_cases.category_id != $category_id) And
                                                  ( char_cases.organization_id =   $organization_id or char_cases.organization_id IN (
                                                                          select char_organizations_closure.descendant_id 
                                                                          from   char_organizations_closure 
                                                                          where  char_organizations_closure.ancestor_id = $organization_id))");
                            }
                        })
                        ->leftjoin('char_block_id_card_number', function($q)  {
                            $q->on('char_block_id_card_number.id_card_number','=','char_persons.id_card_number');
                            $q->where('char_block_id_card_number.type','=',2);
                        })
                        ->leftjoin('char_vouchers_persons', function($q)  use ($voucher_id){
                            $q->on('char_vouchers_persons.person_id','=','char_persons.id');
                            $q->where('char_vouchers_persons.voucher_id','=',$voucher_id);
                        })
                        ->leftjoin('char_block_categories', function($q)  {
                            $q->on('char_block_categories.block_id', '=', 'char_block_id_card_number.id');
                            $q->on('char_block_categories.category_id', '=', 'char_cases.category_id');
                        })
                        ->whereIn('char_persons.id_card_number',$list)
                        ->selectRaw("char_persons.id as person_id , 
                                              char_persons.id_card_number ,
                                              char_persons.old_id_card_number ,
                                              char_persons.family_cnt as family_count ,
                                              char_persons.adscountry_id  ,
                                              char_persons.adsdistrict_id ,
                                              char_persons.adsregion_id ,
                                              char_persons.adsneighborhood_id ,
                                              char_persons.adssquare_id ,
                                              char_persons.adsmosques_id ,
                                              char_cases.id as case_id ,
                                              char_cases.organization_id ,
                                              char_cases.category_id ,
                                              char_vouchers_persons.voucher_id,
                                              CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',
                                                     ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name ,
                                              char_block_categories.block_id
                                             ")
                        ->groupBy('char_persons.id')
                        ->get();

        $processed=[];
        if(sizeof($persons) > 0 ){

            foreach($persons as $k=>$v){
                if($count > $ben_count){

                    if(!is_null($v->case_id) && ( is_null($v->voucher_id) && is_null($v->block_id))){
                        $passed = true;
                        $mainConnector= $organizations_map[$v->organization_id];
                        if(!is_null($v->adsdistrict_id)){
                            if(!in_array($v->adsdistrict_id,$mainConnector)){
                                $passed = false;
                            }else{
                                if(!is_null($v->adsregion_id)){
                                    if(!in_array($v->adsregion_id,$mainConnector)){
                                        $passed = false;
                                    }else{

                                        if(!is_null($v->adsneighborhood_id)){
                                            if(!in_array($v->adsneighborhood_id,$mainConnector)){
                                                $passed = false;
                                            }else{

                                                if(!is_null($v->adssquare_id)){
                                                    if(!in_array($v->adssquare_id,$mainConnector)){
                                                        $passed = false;
                                                    }else{
                                                        if(!is_null($v->adsmosques_id)){
                                                            if(!in_array($v->adsmosques_id,$mainConnector)){
                                                                $passed = false;
                                                            }
                                                        }else{
                                                            $passed = false;
                                                        }
                                                    }
                                                }else{
                                                    $passed = false;
                                                }
                                            }
                                        }else{
                                            $passed = false;
                                        }
                                    }
                                }else{
                                    $passed = false;
                                }
                            }
                        }else{
                            $passed = false;
                        }

                        if($passed == false){
                            $restricted[]= ['id_card_number' => $v->id_card_number,'name' => $v->name , 'reason' => 'out_of_regions'];
                            $processed[]=$v->id_card_number;
                        }else{
                            $restrict = false ;
                            $reason = '';
                            $sub = '';
                            if($person_policy_restricted){
                                $voucher_persons_cnt=
                                    \DB::table('char_persons')
                                        ->where('char_persons.id','=',$v->person_id)
                                        ->selectRaw("char_get_voucher_persons_count ( char_persons.id  ,'$person_policy_restricted->started_at','$person_policy_restricted->ends_at') AS amount")
                                        ->first();


                                $person_current_year_amount=
                                    \DB::table('char_persons')
                                        ->where('char_persons.id','=',$v->person_id)
                                        ->selectRaw("char_get_voucher_persons_count ( char_persons.id  ,'$first','$last') AS amount")
                                        ->first();

                                if($voucher_persons_cnt){
                                    if($person_policy_restricted->count_of_family != 0){
                                        if($person_policy_restricted->count_of_family <= $v->family_count){
                                            $restrict = true ;
                                            $reason = 'policy_restricted';
                                            $sub = 'restricted_count_of_family';
                                        }
                                    }

                                    if($person_policy_restricted->amount != null && $person_policy_restricted->amount != 0){
                                        if($person_policy_restricted->amount <= ($voucher_persons_cnt->amount + $amount)){
                                            $restrict = true ;
                                            $reason = 'policy_restricted';
                                            $sub = 'restricted_amount';
                                        }
                                    }

                                    if($max_total_amount_of_family){
                                        if($max_total <= ($person_current_year_amount->amount + $amount)){
                                            $restrict = true ;
                                            $reason = 'policy_restricted';
                                            $sub = 'restricted_total_payment_persons';
                                        }
                                    }
                                }
                            }

                            if($restrict == true){
                                $restricted[]= ['id_card_number' => $v->id_card_number,'name' => $v->name , 'reason' => $reason, 'sub' => $sub];
                                $processed[]=$v->id_card_number;
                            }else{
                                $v->status = 'old_case';
                                $passed_item[$v->person_id]=$v;
                                $persons_list[]=$v->person_id;
                                $persons_card_list[]=$v->id_card_number;
                                $v->details = $details[$v->id_card_number];
                                $v->details['person_id']=$v->person_id;
                                $person_vouchers[]=$v->details ;
                                $final[]=$v ;
                                $ben_count++;
                            }
                        }

                    }
                    else{
                        if(!is_null($v->block_id)){
                            $restricted[]= ['id_card_number' => $v->id_card_number,'name' => $v->name , 'reason' => 'blocked'];
                            $processed[]=$v->id_card_number;
                        }elseif(!is_null($v->voucher_id)){
                            $restricted[]= ['id_card_number' => $v->id_card_number,'name' => $v->name , 'reason' => 'nominated'];
                            $processed[]=$v->id_card_number;
                        }elseif(!is_null($v->case_id)){
                            if($add_cases == true){
                                $v->status = 'new_case';
                                $new_cases[]=$v;
                                $passed_item[$v->person_id]=$v;
                                $v->details = $details[$v->id_card_number];
                                $v->details['person_id']=$v->person_id;
                                $person_vouchers[]=$v->details ;
                                $final[]=$v ;
                                $persons_card_list[]=$v->id_card_number;
                                $ben_count++;
                            }else{
                                $not_case[]= $v->person_id;
                                $processed[]=$v->id_card_number;
                            }
                        }
                    }

                }else{
                    $restricted[]= ['id_card_number' => $v->id_card_number,'name' => $v->name , 'reason' => 'limited'];
                    $processed[]=$v->id_card_number;
                }
            }

            foreach($list as $card){
                if (!in_array($card, $persons_card_list) && !in_array($card, $processed)) {
                    $restricted[]= ['id_card_number' => $card,'name' => ' ' , 'reason' => 'not_person'];
                }
            }

            if(sizeof($new_cases) > 0 ){
                foreach($new_cases as $k =>$v ){
                    $case=  \Common\Model\AidsCases::create(['person_id' =>  $v->person_id,
                        'status' => 1, 'category_id' => $category_id,
                        'created_at' => date('Y-m-d H:i:s'),
                        'user_id' => $user->id,
                        'rank' => 0,
                        'organization_id' => $user->organization_id]);

                    \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id,
                        'reason'=>trans('common::application.case was created'), 'status'=>0, 'date'=>date("Y-m-d")]);
                    \Log\Model\Log::saveNewLog('CASE_CREATED',trans('common::application.Has added new case data for')  . '  : '.' "'. ' ' .  $v->name . ' " ' . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);

                }
            }
        }

        return array('final'=>$final,'restricted'=>$restricted,'person_vouchers'=>$person_vouchers,'new_cases'=>$new_cases);

    }

}
