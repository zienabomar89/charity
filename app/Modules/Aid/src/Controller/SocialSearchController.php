<?php

namespace aid\Controller;

use Aid\Model\SocialSearch\Cases;
use Aid\Model\SocialSearch\CasesAids;
use Aid\Model\SocialSearch\CasesBanks;
use Aid\Model\SocialSearch\CasesEssentials;
use Aid\Model\SocialSearch\CasesFamily;
use Aid\Model\SocialSearch\CasesProperties;
use Aid\Model\SocialSearch\SocialSearch;
use Aid\Model\SocialSearch\Templates;
use AidRepository\Model\Repository;
use App\Http\Controllers\Controller;
use Citizens\Model\CitizenRequest;
use Common\Model\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Setting\Model\Setting;

class SocialSearchController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // filter user report
    public function filter(Request $request){


        $this->authorize('manage', SocialSearch::class);

        $items = SocialSearch::filter($request->all());
        
        if($request->action == 'excel'){
            if(sizeof($items) !=0){
                $token = md5(uniqid());

                $trashed = false ;
                if(isset($request->trashed)){
                    if($request->trashed == true || $request->trashed){
                        $trashed = true ;
                    }
                }

                \Excel::create('export_' . $token, function($excel) use($items,$trashed) {
                    $excel->setTitle(trans('aid::application.Social Search List'));
                    $excel->setDescription(trans('aid::application.Social Search List'));
                    $excel->sheet(trans('aid::application.Social Search List'), function($sheet) use($items,$trashed) {

                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Calibri',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $style = [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' => [
                                'name' => 'Simplified Arabic',
                                'size' => 12,
                                'bold' => true
                            ]
                        ];

                        $sheet->getStyle("A1:J1")->applyFromArray($style);
                        $sheet->setHeight(1, 30);

                        $style = [
                           'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' => [
                                'name' => 'Simplified Arabic',
                                'size' => 12,
                                'bold' => false
                            ]
                        ];

                        $sheet->getDefaultStyle()->applyFromArray($style);

                        $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet ->setCellValue('A1',trans('aid::application.search_title'));
                        $sheet ->setCellValue('B1',trans('aid::application.organization_name'));
                        $sheet ->setCellValue('C1',trans('aid::application.category_name'));
                        $sheet ->setCellValue('D1',trans('aid::application.notes'));
                        $sheet ->setCellValue('E1',trans('aid::application.date_from'));
                        $sheet ->setCellValue('F1',trans('aid::application.date_to'));
                        $sheet ->setCellValue('G1',trans('aid::application.status_name'));
                        $sheet ->setCellValue('H1',trans('aid::application.username'));
                        $sheet ->setCellValue('I1',trans('aid::application.created_at'));

                        if($trashed == true){
                            $sheet ->setCellValue('J1',trans('aid::application.deleted_at'));
                        }
                        $z= 2;

                        foreach($items as $k=>$v){
                            $sheet ->setCellValue('A'.$z,$v->title);
                            $sheet ->setCellValue('B'.$z,$v->organization_name);
                            $sheet ->setCellValue('C'.$z,$v->category_name);
                            $sheet ->setCellValue('D'.$z,$v->notes);
                            $sheet ->setCellValue('E'.$z,$v->date_from);
                            $sheet ->setCellValue('F'.$z,$v->date_to);
                            $sheet ->setCellValue('G'.$z,$v->status_name);
                            $sheet ->setCellValue('H'.$z,$v->user_name);
                            $sheet ->setCellValue('I'.$z,$v->created_at_date);

                            if($trashed == true){
                                $sheet ->setCellValue('J'.$z,$v->deleted_at);
                            }
                            $z++;
                        }

                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['download_token' => $token]);
            }
            return response()->json(['status' => 'failed','msg'=>trans('organization::application.no report data')]);
        }

        return response()->json(['items' => $items]);

    }

    public function getList(Request $request){
        $user = \Auth::user();

        $items = SocialSearch::where(function ($q) use ($user) {
                                    $q->whereNull('.deleted_at');
                               })->get();

        return response()->json($items);

    }

    // create new social search with option
    public function store(Request $request)
    {
        $this->authorize('create', SocialSearch::class);

        $rules = ['title'=> 'required','date_from'=> 'required','category_id'=> 'required',
                  'date_to'=> 'required','status'=> 'required','close'=> 'required'];

        $error = \App\Http\Helpers::isValid($request->all(),$rules);
        if($error)
            return response()->json($error);

        $user = \Auth::user();

        $entry = new SocialSearch();
        $entry->user_id = $user->id;
        $entry->organization_id = $user->organization_id;
        $entry->date = date('Y-m-d');

        $inputs = ['title','notes','status','close','date_to','date_from','category_id'];
        $dates = ['date_from','date_to'];

        foreach ($inputs as $key){
            if(isset($request->$key)){
                if(in_array($key,$dates)){
                    $entry->$key =date('Y-m-d',strtotime($request->$key));
                }
                else{
                    $entry->$key = $request->$key;
                }
            }
        }

        if($entry->save()) {
             \Log\Model\Log::saveNewLog('SOCIAL_SEARCH_CREATED', trans('organization::application.He added a new social search') . ' "' . $entry->title . '" ');
            return response()->json(['status' => 'success','report_id'=>$entry->id,'msg'=>trans('organization::application.The row is inserted to db')]);
        }

        return response()->json(['status' => 'failed','msg'=>trans('organization::application.The row is not insert to db')]);
    }

    // get existed object model by id
    public function form($id)
    {
        $search = SocialSearch::findOrFail($id);
        $this->authorize('view', $search);
        return response()->json($search);

    }

    // update exist social search with option by id
    public function update(Request $request,$id)
    {

        $entry = SocialSearch::findOrFail($id);
        $this->authorize('update', $entry);

        $user = \Auth::user();
        $rules = ['title'=> 'required','date_from'=> 'required','category_id'=> 'required',
                  'date_to'=> 'required','close'=> 'required','status'=> 'required'];

        $error = \App\Http\Helpers::isValid($request->all(),$rules);
        if($error)
            return response()->json($error);

        $inputs = ['title','notes','status','date_to','close','date_from','category_id'];
        $dates = ['date_from','date_to'];

        foreach ($inputs as $key){
            if(isset($request->$key)){
                if(in_array($key,$dates)){
                    $entry->$key =date('Y-m-d',strtotime($request->$key));
                }
                else{
                    $entry->$key = $request->$key;
                }
            }
        }

        if($entry->save()) {
            \Log\Model\Log::saveNewLog('SOCIAL_SEARCH_UPDATED', trans('organization::application.He edited the properties of the social search') . ' "' . $entry->title . '" ');
            return response()->json(['status' => 'success','msg'=>trans('organization::application.The row is updated')]);
        }
        return response()->json(['status' => 'failed','msg'=>trans('organization::application.There is no update')]);

    }

    // update existed object model by id
    public function destroy($id,Request $request)
    {
        $entry = SocialSearch::findOrFail($id);
        $this->authorize('delete', $entry);
        $title = $entry->title;
        if($entry->destroy($id)) {
            \Log\Model\Log::saveNewLog('SOCIAL_SEARCH_DELETED',trans('organization::application.He deleted the social search')  . ' "' . $title . '" ');
            return response()->json(['status' => 'success','msg'=>trans('organization::application.The row is deleted from db')]);
        }
        return response()->json(['status' => 'failed','msg'=>trans('organization::application.The row is not deleted from db')]);
    }

    // restore trashed social search by id
    public function restore(Request $request, $id)
    {
        $entity = SocialSearch::onlyTrashed()->findOrFail($id);
//        $this->authorize('restore', $entity);
        if ($entity->trashed()) {
            $entity->restore();
            $title = $entity->title;
            \Log\Model\Log::saveNewLog('SOCIAL_SEARCH_RESTORE',trans('organization::application.He restore the social search')  . ' "' . $title . '" ');
            return response()->json(['status' => 'success','msg'=>trans('organization::application.action success')]);
        }
        return response()->json(['status' => 'failed','msg'=>trans('organization::application.action failed')]);
    }

    // search form templates

    // get existed object model by id
    public function templates(Request $request)
    {
        $this->authorize('templates', SocialSearch::class);
        $user = \Auth::user();
        $response= ['has_default_template'=>false,'default_template'=>null,
                    'templates'=>Templates::getTemplates($user->organization_id)];

        $entry = Setting::where(['id' => 'social-search-form' ,'organization_id' => $user->organization_id])->first();
        if (!is_null($entry)) {
            $response["has_default_template"]= true;
            $response["default_template"]= $entry->value;
        }
        return response()->json($response);

    }

    // upload social search form template
    public function templateUpload(Request $request){

        $this->authorize('templates', SocialSearch::class);
        $category_id = $request->category_id;

        $user = \Auth::user();
        $organization_id=$user->organization_id;

        if($category_id != -1){
            $templateFile = $request->file('file');
            $templatePath = $templateFile->store('templates');
            if ($templatePath) {
                $template = Templates::where(['organization_id'=> $organization_id,
                                              'category_id' =>$category_id])->first();
                if (!$template) {
                    $template = new Templates();
                    $template->organization_id =$organization_id;
                    $template->category_id = $category_id;
                    $template->filename = $templatePath;
                    $template->save();
                }
                else {
                    $updated['filename'] = $templatePath;
                    if (\Storage::has($template->filename)) {
                        \Storage::delete($template->filename);
                    }

                    Templates::where(['organization_id'=> $organization_id, 'category_id' =>$category_id,])->update($updated);
                }

                $category = $this->findOrFail($category_id);
                $message=trans('common::application.uploaded aid form export template'). ' "' . $category->name . '" ';
                \Log\Model\Log::saveNewLog('CATEGORIES_TEMPLATE_UPLOADED',$message);
                return response()->json($templatePath);
            }
            return false;
        }
        else{
            $templateFile = $request->file('file');
            $templatePath = $templateFile->store('templates');
            if ($templatePath) {
                $count= Setting::where(['id'=>'social-search-form','organization_id'=>$user->organization_id])->count();
                if($count==1){
                    $template=Setting::where(['id'=>'social-search-form','organization_id'=>$user->organization_id])->first();
                    if (\Storage::has($template->value)) {
                        \Storage::delete($template->value);
                    }
                    Setting::where('id','social-search-form')->update(['value' => $templatePath]);
                }else{
                    Setting::insert(['id'=>'social-search-form','value'=>$templatePath,'organization_id'=>$user->organization_id]);
                }

                $message=trans('common::application.Upload the default export template for search form');
                \Log\Model\Log::saveNewLog('CATEGORIES_TEMPLATE_UPLOADED',$message );

                return response()->json($templatePath);
            }
            return false;

        }
    }

    // get social search form template
    public function template($id,Request $request){

        $this->authorize('templates', SocialSearch::class);
        if($id != -1){
            $template = Templates::findOrFail($id);
            $filename= $template->filename;
        }else{
            $user = \Auth::user();
            $template= Setting::where(['id'=>'social-search-form','organization_id'=>$user->organization_id])->first();
            $filename= $template->value;
        }

        $sub = explode(".", $filename);
        $token = explode("/", $sub[0]);

        return response()->json(['download_token' => $token[1]]);
    }

    // refresh case data from social search data
    public function refresh($id,Request $request)
    {
        return response()->json(Cases::refreshGroup($id));
    }

    // update request status  using request id
    public function status(Request $request)
    {
        $action = $request->action ;

        if($action =='single'){
            $id = $request->target ;
            $entity = SocialSearch::findOrFail($id);
            $entity->status = $request->status;

            if($entity->saveOrFail()) {
//            \Log\Model\Log::saveNewLog('CITIZEN_REQUEST_STATUS_UPDATE', trans('sms::application.Edit the messages provider data')  . ' " '.$entry['Label']  . ' " ');
                $response["status"]= 'success';
                $response["msg"]= trans('sms::application.The row is updated');

            }else{
                $response["status"]= 'failed';
                $response["msg"]= trans('sms::application.There is no update');
            }
            return response()->json($response);
        }else{

            $requests = $request->requests;
            $entries = SocialSearch::whereIn('id',$requests)->get();
            if(SocialSearch::whereIn('id',$requests)->update(['status' => $request->status])) {
//            \Log\Model\Log::saveNewLog('CITIZEN_REQUEST_STATUS_UPDATE', trans('sms::application.Edit the messages provider data')  . ' " '.$entry['Label']  . ' " ');
                $response["status"]= 'success';
                $response["msg"]= trans('sms::application.The row is updated');

            }else{
                $response["status"]= 'failed';
                $response["msg"]= trans('sms::application.There is no update');
            }
            return response()->json($response);

        }
    }

}
