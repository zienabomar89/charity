<?php

namespace aid\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Aid\Model\VoucherCategories;
use App\Http\Helpers;

class VoucherCategoriesController  extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

   // paginate all voucher categories
    public function index(Request $request)
    {
        $this->authorize('manage', VoucherCategories::class);
        $return = VoucherCategories::whereNull('parent')->orderBy('name');
        $itemsCount = ($request->itemsCount !== null)?$request->itemsCount:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$return->count());

        return response()->json($return->paginate($records_per_page));
    }

    // list all siblings categories of main category by id
    public function getSiblingsCategory($id)
    {
        return response()->json(['data'=>VoucherCategories::where('parent',$id)->get()]);
    }

    // list all main categories
    public function getParentsCategory()
    {
        return response()->json(VoucherCategories::whereNull('parent')->get());
    }

    // list all main categories
    public function getList()
    {
        return response()->json(VoucherCategories::whereNotNull('parent')->get());
    }

    // save new main voucher category
    public function store(Request $request)
    {
        $this->authorize('create', VoucherCategories::class);
        $response = array();

        $input=['type' => strip_tags($request->get('type')),'name' => strip_tags($request->get('name'))];
        $rules=['type' => 'required', 'name' => 'required|max:255'];

        if($input['type'] != null && $input['type'] != '' && $input['type'] == 1){
            $rules['parent']='required';
            $input['parent']=strip_tags($request->get('parent'));
        }

        $error = \App\Http\Helpers::isValid($input,$rules);
        if($error)
            return response()->json($error);


        if(VoucherCategories::create($input))
        {
            \Log\Model\Log::saveNewLog('VOUCHER_CATEGORIES_CREATED',trans('aid::application.Added an aid voucher type') . ' "' . $input['name'] . '" ');
            $response["status"]= 'success';
            $response["msg"]= trans('aid::application.The row is inserted to db');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('aid::application.The row is not insert to db');

        }
        return response()->json($response);

    }

    // get voucher category by id
    public function show($id)
    {
        $obj = VoucherCategories::findOrFail($id);
        $this->authorize('view', $obj);
        return response()->json($obj);

    }

    // update row of voucher category using id
    public function update(Request $request, $id)
    {
        $category = VoucherCategories::findOrFail($id);
        $this->authorize('update', $category);

        $response = array();

        $input=['name' => strip_tags($request->get('name'))];
        $rules= [
            'name' => 'required|max:255|unique:char_vouchers_categories,name,'.$id,
        ];

        $error = \App\Http\Helpers::isValid($input,$rules);
        if($error)
            return response()->json($error);

        if($category->update($input))
        {
            \Log\Model\Log::saveNewLog('VOUCHER_CATEGORIES_UPDATED',trans('aid::application.Edited the aid coupon type data') . ' "' . $category->name . '" ');
            $response["status"]= 'success';
            $response["data"]= VoucherCategories::orderBy('name')->paginate(config('constants.records_per_page'));
            $response["msg"]= trans('aid::application.The row is updated');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('aid::application.There is no update');
        }
        return response()->json($response);

    }

    // update row of sub voucher category using id
    public function updateSub(Request $request, $id)
    {
        $category = VoucherCategories::findOrFail($id);
        $this->authorize('update', $category);
        $update=0;
        $response = array();
        foreach($request->get('Sub') as $k =>$v){
            $sub = VoucherCategories::findOrFail($v['id']);
            $sub->name=$v['name'];
            if($sub->save()){
                $update++;
            }
        }

        if($update!=0)
        {
            \Log\Model\Log::saveNewLog('VOUCHER_CATEGORIES_UPDATED',trans('aid::application.Edited the aid coupon type data') . ' "' . $category->name . '" ');
            $response["status"]= 'success';
            $response["msg"]= trans('aid::application.The row is updated');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('aid::application.There is no update');
        }
        return response()->json($response);
    }

    // delete voucher category using id
    public function destroy($id,Request $request)
    {
        $response = array();

        $category = VoucherCategories::findOrFail($id);
        $this->authorize('delete', $category);

        if($category->delete())
        {
            \Log\Model\Log::saveNewLog('VOUCHER_CATEGORIES_DELETED',trans('aid::application.Deleted the aid coupon type') . ' "' . $category->name . '" ');
            $response["status"]= 'success';
            $response["msg"]= trans('aid::application.The row is deleted from db');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('aid::application.The row is not deleted from db');

        }
        return response()->json($response);

    }

  }
