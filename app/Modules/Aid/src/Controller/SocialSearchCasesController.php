<?php

namespace aid\Controller;

use Aid\Model\SocialSearch\Cases;
use Aid\Model\SocialSearch\CasesAids;
use Aid\Model\SocialSearch\CasesBanks;
use Aid\Model\SocialSearch\CasesEssentials;
use Aid\Model\SocialSearch\CasesFamily;
use Aid\Model\SocialSearch\CasesProperties;
use Aid\Model\SocialSearch\SocialSearch;
use Aid\Model\SocialSearch\Templates;
use App\Http\Controllers\Controller;
use Auth\Model\User;
use Common\Model\GovServices;
use Common\Model\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use Setting\Model\AidSource;
use Setting\Model\Essential;
use Setting\Model\PropertyI18n;

class SocialSearchCasesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /************ filter cases report ************/
    public function cases(Request $request){
        $this->authorize('manageCases', SocialSearch::class);

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $user = \Auth::user();
        $organization_id = $user->organization_id;

        $items = Cases::filter($request->all());

        if($request->get('action') =='filter') {
            $search_title = null;
            if($request->search_id){
                $search = SocialSearch::findOrFail($request->search_id);
                $search_title = $search->title;
            }
            return response()->json(['items' => $items,'search_title'=>$search_title,'organization_id'=>$organization_id]);
        }
        else if($request->get('action') =='ExportToExcel') {
            if($request->get('deleted') == true){
                $statistic=$items;
            }else{
                $statistic=$items['statistic'];
                $aid_source=$items['aid_source'];
                $properties=$items['properties'];
                $essentials=$items['essentials'];
            }

            $data=[];
            if(sizeof($statistic) !=0){
                foreach($statistic as $key =>$value){
                    $n=0;
                    $data[$key][trans('aid::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        if(!in_array($k,['id','case_id','person_id','data','elements','financial_aid_source_elements','financial_aid_source',
                            'non_financial_aid_source_elements','non_financial_aid_source',
                            'persons_properties_elements','persons_properties','persons_essentials_elements','persons_essentials'
                        ])){

                            if($v == null){
                                $v=' ';
                            }
                            $data[$key][trans('aid::application.' . $k)]= str_replace("-"," ",$v) ;
                            $n++;
                        }
                    }
                    foreach($aid_source as $k1_ =>$v2_){
                        $cuVal='0';
                        if(in_array($v2_->id,$value->financial_aid_source_elements)){
                            $cuVal=$value->financial_aid_source[$v2_->id];
                        }
                        $data[$key][$v2_->name . '(' . trans('common::application.financial'). ')']= $cuVal; ;
                    }
                    foreach($aid_source as $k1_ =>$v2_){
                        $cuVal='0';
                        if(in_array($v2_->id,$value->non_financial_aid_source_elements)){
                            $cuVal=$value->non_financial_aid_source[$v2_->id];
                        }
                        $data[$key][$v2_->name . ' (' . trans('common::application.non_financial'). ') ']= $cuVal; ;
                    }
                    foreach($properties as $k1_0 =>$v2_0){
                        $cuVal='0';
                        if(in_array($v2_0->id,$value->persons_properties_elements)){
                            $cuVal=$value->persons_properties[$v2_0->id];
                        }
                        $data[$key][$v2_0->name]= $cuVal; ;
                    }
                    foreach($essentials as $k1_0 =>$v2_0){
                        $cuVal='0';
                        if(in_array($v2_0->id,$value->persons_essentials_elements)){
                            $cuVal=$value->persons_essentials[$v2_0->id];
                        }
                        $data[$key][$v2_0->name]= $cuVal; ;
                    }
                }

                $n +=sizeof($aid_source);
                $n +=sizeof($aid_source);
                $n +=sizeof($properties);
                $n +=sizeof($essentials);
                for($r = ""; $n >= 0; $n = intval($n / 26) - 1)
                    $r = chr($n%26 + 0x41) . $r;

                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($data,$r) {
                    $excel->sheet('sheet', function($sheet) use($data,$r) {

                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $sheet->setHeight(1,30);

                        $sheet->getStyle("A1:".$r."1")->applyFromArray( [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold'      =>  true
                            ]
                        ]);
                        $sheet->getDefaultStyle()->applyFromArray( [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold' => false
                            ]
                        ]);
                        $sheet->fromArray($data);
                    });
                })->store('xlsx', storage_path('tmp/'));

                \Log\Model\Log::saveNewLog('SOCIAL_SEARCH_CASE_EXPORTED',trans('common::application.Exported a list of aid cases'));
                return response()->json(['download_token' => $token]);
            }

        }
        else if($request->get('action') =='FamilyMember') {

            $keys=["father_id_card_number", "father_name", "id_card_number", "name", "gender", "birthday",
                "marital_status","grade", "school" , "currently_study", "health_status" ,
                "diseases_name" , "diseases_details" , "kinship_name" ];


            $data=[];
            if(sizeof($items) !=0){
                foreach($items as $key =>$value){
                    $data[$key][trans('sponsorship::application.#')]=$key+1;
                    foreach($keys as $ke){
                        $f_ke=null;
                        $vlu = is_null($value->$ke) ? '__' : $value->$ke;
                        if($ke === "kinship_name"){
                            $f_ke = trans('sponsorship::application.kinship_name')." ( ".trans('sponsorship::application.family_res')  ." ) ";
                        }else{
                            $f_ke = trans('sponsorship::application.' . $ke);
                        }
                        $data[$key][$f_ke]= $vlu;
                    }
                }

                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($data) {
                    $excel->sheet(trans('common::application.Detection of family members - brothers') , function($sheet) use($data) {

                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $sheet->setHeight(1,30);
                        $r='W';

                        $sheet->getStyle("A1:".$r."1")->applyFromArray( [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold'      =>  true
                            ]
                        ]);
                        $sheet->getDefaultStyle()->applyFromArray( [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold'      =>  false
                            ]
                        ]);
                        $sheet->fromArray($data);
                    });
                })->store('xlsx', storage_path('tmp/'));

                \Log\Model\Log::saveNewLog('SOCIAL_SEARCH_CASE_EXPORTED',trans('common::application.Exported a list of aid cases'));
                return response()->json(['download_token' => $token,'organization_id'=>$organization_id]);
            }

        }
        else if($request->get('action') =='pdf') {

            $dirName = base_path('storage/app/templates/CaseForm');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }
            foreach($items as $k=>$item){

                $html='';
//        PDF::SetTitle($item->full_name .' '.$item->search_name);
                PDF::SetFont('aealarabiya', '', 18);
                PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);

                PDF::SetMargins(PDF_MARGIN_LEFT, 20, PDF_MARGIN_RIGHT);
//        PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
//        PDF::SetFooterMargin(PDF_MARGIN_FOOTER);
                PDF::SetAutoPageBreak(TRUE);
                $lg = Array();
                $lg['a_meta_charset'] = 'UTF-8';
                $lg['a_meta_dir'] = 'rtl';
                $lg['a_meta_language'] = 'fa';
                $lg['w_page'] = 'page';
                PDF::setLanguageArray($lg);
                PDF::setCellHeightRatio(1.5);

                $date = date('Y-m-d');

                $organization_name = $item->organization_name;
                $search_date = $item->date;
                $category_name = $item->category_name;
                $first_name = $item->first_name;
                $second_name = $item->second_name;
                $third_name = $item->third_name ;
                $last_name = $item->last_name ;
                $name = $item->full_name;

                $id_card_number = $item->id_card_number;
                $birthday = $item->birthday;
                $age = $item->age;
                $gender = $item->gender;
                $gender_male = $item->gender_male;
                $gender_female = $item->gender_female;
                $deserted = $item->gender;
                $is_deserted = $item->is_deserted;
                $not_deserted = $item->not_deserted;
                $birth_place = $item->birth_place;
                $nationality = $item->nationality;
                $refugee = $item->refugee;

                $is_refugee = $item->is_refugee;
                $per_refugee = $item->per_refugee;
                $per_citizen = $item->per_citizen;
                $unrwa_card_number = $item->unrwa_card_number;

                $marital_status_name = $item->marital_status_name;

                $monthly_income = $item->monthly_income;

                $address = $item->address;

                $phone = $item->phone;
                $primary_mobile = $item->primary_mobile;
                $wataniya = $item->watanya;
                $secondary_mobile = $item->secondary_mobile ;

                $property_types = $item->property_types;
                $rent_value = $item->rent_value;
                $roof_materials = $item->roof_materials;
                $habitable = $item->habitable;
                $house_condition = $item->house_condition;
                $residence_condition = $item->residence_condition;
                $indoor_condition = $item->indoor_condition;
                $rooms = $item->rooms;
                $area = $item->area;

                $working = $item->working;
                $works = $item->works;
                $is_working = $item->is_working;
                $not_working = $item->not_working;
                $can_work = $item->can_work;
                $can_works = $item->can_works;
                $can_working = $item->can_working;
                $can_not_working = $item->can_not_working;
                $work_job = $item->work_job;
                $work_reason = $item->work_reason;
                $work_status = $item->work_status;
                $work_wage = $item->work_wage;
                $work_location = $item->work_location;

                $needs = $item->needs;
                $promised = $item->promised;
                $if_promised = $item->if_promised;
                $was_promised = $item->was_promised;
                $not_promised = $item->not_promised;
                $reconstructions_organization_name = $item->reconstructions_organization_name;

                $visitor = $item->visitor;
                $notes = $item->notes;
                $visited_at = $item->visited_at;

                $health_status = $item->health_status;
                $good_health_status = $item->good_health_status;
                $chronic_disease = $item->chronic_disease;
                $disabled = $item->disabled;
                $health_details = $item->health_details;
                $diseases_name = $item->diseases_name;

                $visitor_note = $item->visitor_note;
                $deserted = $item->deserted;
                $family_cnt = $item->family_cnt;
                $spouses = $item->spouses;
                $male_live = $item->male_live;
                $female_live = $item->female_live;


                $is_qualified = $item->is_qualified;
                $qualified_card_number = $item->qualified_card_number;


                $district = $item->governarate;
                $region_name = $item->region_name;
                $nearLocation = $item->nearLocation;
                $square_name = $item->square_name;
                $mosque_name = $item->mosque_name;

                $actual_monthly_income = $item->actual_monthly_income;
                $receivables = $item->receivables;
                $receivables_sk_amount = $item->receivables_sk_amount;
                $has_other_work_resources = $item->has_other_work_resources;


                $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                $html.= '<h2 style="text-align:center;"> '.$category_name.'('.$search_date.')'.'</h2>';
                $html .= '<table border="1" style="border: 1px solid black; box-sizing:border-box;">';

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.full_name').'  </b></td>
                       <td width="158" align="center">'.$name.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.id_card').'  </b></td>
                       <td width="152" align="center">'.$id_card_number.'</td>
                      
                   </tr>';

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.BIRTH_DT').' </b></td>
                       <td width="158" align="center" >'.$birthday.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.birth_place').'</b></td>
                       <td width="152" align="center">'.$birth_place.'</td>
                   </tr>';

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.SOCIAL_STATUS').'  </b></td>
                       <td width="158" align="center">'.$marital_status_name.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.nationality').'  </b></td>
                       <td width="152" align="center">'.$nationality.'</td>
                      
                   </tr>';

                $html .= ' <tr>
                    <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.gender').' </b></td>
                   <td width="158" align="center">'.$gender.'</td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('aid::application.deserted').'   </b></td>
                   <td width="152" align="center">'.$deserted.'</td>
               </tr>';

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.citizen').'  / '.trans('common::application.refugee').'</b></td>
                       <td width="158" align="center">'.$refugee.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.unrwa_card_number').'</b></td>
                       <td width="152" align="center">'.$unrwa_card_number.'</td>
                   </tr>';

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.is_qualified').'</b></td>
                       <td width="158" align="center">'.$is_qualified.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.qualified_card_number').'</b></td>
                       <td width="152" align="center">'.$qualified_card_number.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.family_member_count').'</b></td>
                       <td width="158" align="center">'.$family_cnt.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.spouses').'</b></td>
                       <td width="152" align="center">'.$spouses.'</td>
                   </tr>';

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.male_live').'</b></td>
                       <td width="158" align="center">'.$male_live.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.female_live').'</b></td>
                       <td width="152" align="center">'.$female_live.'</td>
                   </tr>';

                $html .='</table>';
                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="95" align="center"><b>  '.trans('common::application.district').' </b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="105" align="center"><b>  '.trans('common::application.region_').' </b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="105" align="center"><b>   '.trans('common::application.nearlocation_').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="105" align="center"><b>  '.trans('common::application.square').' </b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="103" align="center"><b>  '.trans('common::application.mosques').' </b></td>
                   </tr>';

                $html .= ' <tr>
                       <td width="95" align="center"> <br>'.$district.' <br></td>
                       <td width="105" align="center"> <br>'.$region_name.' <br></td>
                       <td width="105" align="center"> <br>'.$nearLocation.' <br></td>
                       <td width="105" align="center"> <br>'.$square_name.' <br></td>
                       <td width="103" align="center"> <br>'.$mosque_name.' <br></td>
                   </tr>';


                $html .='</table>';
                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                        '.trans('common::application.contact information').'
                        </b></td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.mobile').'</b></td>
                       <td width="156" align="center">'.$primary_mobile.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.secondery_mobile').'</b></td>
                       <td width="157" align="center">'.$secondary_mobile.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.phone').'</b></td>
                       <td width="156" align="center">'.$phone.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.wataniya').'</b></td>
                       <td width="157" align="center">'.$wataniya.'</td>
                   </tr>';
                $html .='</table>';

                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                       '.trans('common::application.works info').' 
                        </b></td>
                   </tr>';

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.is_she_working?').'</b></td>
                       <td width="156" align="center">'.$working.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.work_job_').' </b></td>
                       <td width="137" align="center">'.$work_job.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.is_can_work').'</b></td>
                       <td width="156" align="center">'.$can_work.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.work_reason').' </b></td>
                       <td width="137" align="center">'.$work_reason.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_location').' </b></td>
                       <td width="156" align="center">'.$work_location.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b>'.trans('common::application.Work_Status').' </b></td>
                       <td width="137" align="center">'.$work_status.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.Wage category').':  </b></td>
                       <td width="156" align="center">'.$work_wage.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b>'.trans('common::application.monthly_income').'</b></td>
                       <td width="137" align="center">'. $monthly_income .'</td>
                   </tr>';


                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.actual_monthly_income').'</b></td>
                       <td width="156" align="center">'.$actual_monthly_income.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.has_other_work_resources').' </b></td>
                       <td width="137" align="center">'.$has_other_work_resources.'</td>
                       
                   </tr>';
                $html .= ' <tr>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.receivables').' </b></td>
                           <td width="156" align="center">'.$receivables.'</td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b>   '.trans('common::application.receivables_sk_amount').'</b></td>
                           <td width="137" align="center">'.$receivables_sk_amount.'</td>
                           
                       </tr>';

                $html .='</table>';


                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1">';
                $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                       '.trans('common::application.home info').'
                        </b></td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.property_type').'</b></td>
                       <td width="156" align="center">'.$property_types.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.property_type').'('.trans('common::application.for renter').') </b></td>
                       <td width="157" align="center">'.$rent_value.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.roof_material').'</b></td>
                       <td width="156" align="center">'.$roof_materials.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.residence_condition').'</b></td>
                       <td width="157" align="center">'.$house_condition.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.indoor_condition').'</b></td>
                       <td width="156" align="center">'.$residence_condition.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.habitable').'</b></td>
                       <td width="157" align="center">'.$habitable.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.home_area').'</b></td>
                       <td width="156" align="center">'.$area.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.rooms').'</b></td>
                       <td width="157" align="center">'.$rooms.'</td>
                   </tr>';
                $need_repair = $item->need_repair;
                $repair_notes = $item->repair_notes;
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="200" align="center"><b>'.
                    trans('common::application.need_repair').'</b></td>
                       <td width="312" align="center">'.$need_repair.'</td>
                   </tr>';
                $html .='</table>';

                $html .='</body></html>';
                PDF::AddPage('P','A4');
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


                $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';

                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                      '.trans('common::application.health info').'
                        </b></td>
                   </tr>';

                $has_health_insurance = $item->has_health_insurance;
                $health_insurance_number = $item->health_insurance_number;
                $health_insurance_type = $item->health_insurance_type;

                $has_device = $item->has_device;
                $used_device_name = $item->used_device_name;

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.has_health_insurance').' </b></td>
                       <td width="156" align="center">'.$has_health_insurance.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.health_insurance_number').'</b></td>
                       <td width="157" align="center">'.$health_insurance_number.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.health_insurance_type').' </b></td>
                       <td width="156" align="center">'.$health_insurance_type.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.has_device').'</b></td>
                       <td width="157" align="center">'.$has_device.'</td>
                   </tr>';

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.used_device_name').'</b></td>
                       <td width="413" align="center">'.$used_device_name.'</td>
                   </tr>';

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.disease_/_disability_details').'</b></td>
                       <td width="413" align="center">'.$health_details.'</td>
                   </tr>';

                $html .='</table>';

                $html .= '<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="300" align="center"><b> '.trans('common::application.reconstructions_promised').'? ('.trans('common::application.yes').' /'.trans('common::application.no').') </b></td>
                       <td width="213" align="center">' . $promised . '</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="300" align="center"><b> '.trans('common::application.ReconstructionOrg').'('.trans('common::application.if the answer to the previous question is yes').'):  </b></td>
                       <td width="213" align="center">' . $reconstructions_organization_name . '</td>
                   </tr>';
                $html .='</table>';

                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="height: 50px ; padding: 55px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b> 
                       '.trans('common::application.case_needs').'
                       </b></td>
                       <td width="415" align="center">' . $needs . '</td>
                   </tr>';

                $html .= ' <tr>
                       <td style="height: 50px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b>  '.trans("common::application.repair_notes").'</b></td>
                       <td width="415" align="center">' . $repair_notes . '</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 50px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b>  '.trans("common::application.social researcher's recommendations").'</b></td>
                       <td width="415" align="center">' . $notes . '</td>
                   </tr>';

                $visitor_opinion = $item->visitor_opinion;
                $html .= ' <tr>
                       <td style="height: 50px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b>  '.trans("common::application.visitor_opinion").'</b></td>
                       <td width="415" align="center">' . $visitor_opinion . '</td>
                   </tr>';
                $html .='</table>';

                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';

                $visitor_card = $item->visitor_card;
                $visitor_evaluation = $item->visitor_evaluation_;
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.visitor_card').'</b></td>
                       <td width="156" align="center">' . $visitor_card . '</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.visitor').' </b></td>
                       <td width="157" align="center">' . $visitor . '</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.visited_at').'</b></td>
                       <td width="156" align="center">' . $visited_at . '</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.visitor_evaluation').' </b></td>
                       <td width="157" align="center">' . $visitor_evaluation . '</td>
                   </tr>';



                $html .='</table>';

                $html .='</body></html>';
                PDF::AddPage('P','A4');
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

                if (sizeof($item->family_members)!=0){

                    $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                    $html.= '<h2 style="text-align:center;"> '.trans('common::application.family-data').'

    ('. sizeof($item->family_members) .')</h2>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.name').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.id_card').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.BIRTH_DT').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="40" align="center"><b> '.trans('common::application.gender').'</b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.SOCIAL_STATUS').'</b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.currently_study').'</b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.health_condition').' </b></td>
                   </tr>';
                    $z=1;
                    foreach($item->family_members as $record) {
                        $fm_nam = $record->name;
                        $fm_idc = $record->id_card_number;
                        $fm_bd = $record->birthday;
                        $fm_gen = $record->gender;
                        $fm_age = $record->age;
                        $fm_ms = $record->marital_status;
                        $fm_kin = $record->kinship_name;
                        $fm_grd = $record->grade;
                        $fm_scl = $record->school;
                        $fm_currently_study = $record->currently_study;
                        $fm_hs = $record->health_status;
                        $fm_disnm = $record->diseases_name;
                        $html .= ' <tr>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="120" align="center"><b> '.$fm_nam.'  </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="70" align="center"><b> '.$fm_idc.' </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="70" align="center"><b> '.$fm_bd.'   </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="40" align="center"><b>'.$fm_gen.'</b></td> 
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="70" align="center"><b> '.$fm_ms.' </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="60" align="center"><b> '.$fm_currently_study.' </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$fm_hs.'   </b></td>
                   </tr>';
                        $z++;
//                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="50" align="center"><b>'.$fm_kin.'</b></td>
//                                    <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_working.'   </b></td>
//                <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$fm_job.'   </b></td>
                    }
                    $html .='</table>';
                    $html .='</body></html>';

                    PDF::SetMargins(PDF_MARGIN_LEFT, 2, PDF_MARGIN_RIGHT);
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                }
                if (sizeof($item->home_indoor)!=0){
                    $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                    $html.= '<h2 style="text-align:center;"> '.trans('common::application.furniture-info').'</h2>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="200" align="center"><b> '.trans('common::application.content').'  </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.exist').'/'.trans('common::application.not exist').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_status').'   </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.needs').'</b></td>
                   </tr>';

                    foreach($item->home_indoor as $record) {

                        $row_es_name = $record->name;
                        $row_es_if_exist= $record->exist;
                        $row_es_needs=is_null($record->needs) ? ' ' : $record->needs;
                        $row_es_condition=is_null($record->essentials_condition) ? ' ' : $record->essentials_condition;

                        $html .= ' <tr>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="200" align="center"><b> '.$row_es_name.'  </b></td>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$row_es_if_exist.' </b></td>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$row_es_condition.'   </b></td>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b>'.$row_es_needs .'</b></td>
                   </tr>';
                    }
                    $html .='</table>';
                    $html .='</body></html>';

                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                }
                if (sizeof($item->financial_aid_source)!=0){

                    $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                    $html.= '<h2 style="text-align:center;"> '.trans('common::application.concrete foreign aid data').'</h2>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="200" align="center"><b>  '.trans('common::application.Name of the organization source of assistance').'  </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="130" align="center"><b> '.trans('common::application.Does the family receive help from org?').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.quantity').'   </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.currency_name').' </b></td>
                   </tr>';

                    foreach($item->financial_aid_source as $record) {
                        $row_fin_name = $record->name;
                        $row_fin_if_exist= $record->aid_take;
                        $row_fin_count= $record->aid_value;
                        $row_fin_currency= $record->currency_name;
                        $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="200" align="center"><b> '.$row_fin_name.'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="130" align="center"><b> '.$row_fin_if_exist.' </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$row_fin_count.'   </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="80" align="center"><b>'.$row_fin_currency.'</b></td>
                   </tr>';
                    }
                    $html .='</table>';
                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


                }
                if (sizeof($item->non_financial_aid_source)!=0){
                    $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                    $html.= '<h2 style="text-align:center;"> '.trans('common::application.concrete foreign aid data').'</h2>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="250" align="center"><b>  '.trans('common::application.Name of the organization source of assistance').'  </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="150" align="center"><b> '.trans('common::application.Does the family receive help from org?').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.quantity').'   </b></td>
                   </tr>';

                    foreach($item->non_financial_aid_source as $record) {
                        $row_non_fin_name = $record->name;
                        $row_non_fin_if_exist= $record->aid_take;
                        $row_non_fin_count= $record->aid_value;
                        $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="250" align="center"><b> '.$row_non_fin_name.'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="150" align="center"><b> '.$row_non_fin_if_exist.' </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$row_non_fin_count.'   </b></td>
                   </tr>';

                    }

                    $html .='</table>';

                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                }
                if (sizeof($item->properties)!=0){
                    $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                    $html.= '<h2 style="text-align:center;"> '.trans('common::application.Family property data').'</h2>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="250" align="center"><b>  '.trans('common::application.Naming property').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="140" align="center"><b> '.trans('common::application.exist').'/'.trans('common::application.not exist').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.quantity').'   </b></td>
                   </tr>';
                    foreach($item->properties as $record) {
                        $pro_name = $record->name;
                        $has_property= $record->has_property;
                        $quantity= $record->quantity;
                        $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="250" align="center"><b> '.$pro_name.'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="140" align="center"><b> '.$has_property.' </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="120" align="center"><b> '.$quantity.'   </b></td>
                   </tr>';

                    }
                    $html .='</table>';

                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);  }

                $dirName = base_path('storage/app/templates/CaseForm');
                if (!is_dir($dirName)) {
                    @mkdir($dirName, 0777, true);
                }

                PDF::Output($dirName.'/'.$item->category_name.' '.$item->id_card_number.' '.$item->full_name . '.pdf', 'F');

            }

            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');

            $zip = new \ZipArchive;
            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                foreach ( glob( $dirName . '/*' ) as $fileName )
                {
                    $file = basename( $fileName );
                    $zip->addFile( $fileName, $file );
                }

                $zip->close();
                array_map('unlink', glob("$dirName/*.*"));
                rmdir($dirName);
            }

            \Log\Model\Log::saveNewLog('SOCIAL_SEARCH_CASE_EXPORTED',trans('common::application.exported cases forms'));
            return response()->json(['download_token' => $token]);

        }
        else if($request->get('action') =='ExportToWord') {
            $dirName = base_path('storage/app/templates/temp');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }

            foreach($items as $k=>$item){
                $dirName = base_path('storage/app/templates/CaseForm');
                if (!is_dir($dirName)) {
                    @mkdir($dirName, 0777, true);
                }

                $Template=Templates::where(['category_id'=>$item->category_id,'organization_id'=>$user->organization_id])->first();
                if($Template){
                    $path = base_path('storage/app/'.$Template->filename);
                }
                else{
                    $default_template=\Setting\Model\Setting::where(['id'=>'social-search-form','organization_id'=>$user->organization_id])->first();

                    if($default_template){
                        $path = base_path('storage/app/'.$default_template->value);
                    }else{
                        $path = base_path('storage/app/templates/default-search-form-template.docx');
                    }
                }

                $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);

                $templateProcessor->setValue('date', $item->date);
                $templateProcessor->setValue('organization_name', $item->organization_name);
                $templateProcessor->setValue('category_name', $item->category_name);

                $templateProcessor->setValue('first_name', $item->first_name);
                $templateProcessor->setValue('second_name', $item->second_name);
                $templateProcessor->setValue('third_name', $item->third_name );
                $templateProcessor->setValue('last_name', $item->last_name );
                $templateProcessor->setValue('name', $item->full_name);

                $templateProcessor->setValue('id_card_number', $item->id_card_number);
                $templateProcessor->setValue('card_type', $item->card_type);
                $templateProcessor->setValue('birthday', $item->birthday);
                $templateProcessor->setValue('age', $item->age);
                $templateProcessor->setValue('gender', $item->gender);
                $templateProcessor->setValue('gender_male', $item->gender_male);
                $templateProcessor->setValue('gender_female', $item->gender_female);
                $templateProcessor->setValue('birth_place', $item->birth_place);
                $templateProcessor->setValue('nationality', $item->nationality);
                $templateProcessor->setValue('refugee', $item->refugee);
                $templateProcessor->setValue('is_refugee', $item->is_refugee);
                $templateProcessor->setValue('per_refugee', $item->per_refugee);
                $templateProcessor->setValue('is_citizen', $item->is_citizen);
                $templateProcessor->setValue('per_citizen', $item->per_citizen);
                $templateProcessor->setValue('unrwa_card_number', $item->unrwa_card_number);
                $templateProcessor->setValue('marital_status_name', $item->marital_status_name);

                $templateProcessor->setValue('deserted', $item->deserted);
                $templateProcessor->setValue('is_deserted', $item->is_deserted);
                $templateProcessor->setValue('not_deserted', $item->not_deserted);

                $templateProcessor->setValue('receivables', $item->receivables);
                $templateProcessor->setValue('is_receivables', $item->is_receivables);
                $templateProcessor->setValue('is_ receivables', $item->not_receivables);
                $templateProcessor->setValue('not_receivables', $item->not_receivables);
                $templateProcessor->setValue('receivables_sk_amount', $item->receivables_sk_amount);

                $templateProcessor->setValue('is_qualified', $item->is_qualified);
                $templateProcessor->setValue('qualified', $item->qualified);
                $templateProcessor->setValue('not_qualified', $item->not_qualified);
                $templateProcessor->setValue('qualified_card_number', $item->qualified_card_number);

                $templateProcessor->setValue('monthly_income', $item->monthly_income);
                $templateProcessor->setValue('actual_monthly_income', $item->actual_monthly_income);

                $templateProcessor->setValue('address', $item->address);
                $templateProcessor->setValue('country', $item->country);
                $templateProcessor->setValue('governarate', $item->governarate);
                $templateProcessor->setValue('nearLocation', $item->nearLocation);
                $templateProcessor->setValue('mosque_name', $item->mosque_name);
                $templateProcessor->setValue('region', $item->region_name);
                $templateProcessor->setValue('square', $item->square_name);
                $templateProcessor->setValue('street_address', $item->street_address);

                $templateProcessor->setValue('family_cnt', $item->family_cnt);
                $templateProcessor->setValue('male_live', $item->male_live);
                $templateProcessor->setValue('female_live', $item->female_live);
                $templateProcessor->setValue('spouses', $item->spouses);
                $templateProcessor->setValue('family_count', $item->family_count);

                $templateProcessor->setValue('phone', $item->phone);
                $templateProcessor->setValue('primary_mobile', $item->primary_mobile);
                $templateProcessor->setValue('wataniya', $item->wataniya);
                $templateProcessor->setValue('secondary_mobile', $item->secondary_mobile );

                $templateProcessor->setValue('working', $item->working);
                $templateProcessor->setValue('works', $item->works);
                $templateProcessor->setValue('is_working', $item->is_working);
                $templateProcessor->setValue('not_working', $item->not_working);
                $templateProcessor->setValue('can_work', $item->can_work);
                $templateProcessor->setValue('can_works', $item->can_works);
                $templateProcessor->setValue('can_working', $item->can_working);
                $templateProcessor->setValue('can_not_working', $item->can_not_working);
                $templateProcessor->setValue('work_job', $item->work_job);
                $templateProcessor->setValue('work_reason', $item->work_reason);
                $templateProcessor->setValue('work_status', $item->work_status);
                $templateProcessor->setValue('work_wage', $item->work_wage);
                $templateProcessor->setValue('work_location', $item->work_location);
                $templateProcessor->setValue('has_other_work_resources', $item->has_other_work_resources);
                $templateProcessor->setValue('has_other_resources', $item->has_other_resources);
                $templateProcessor->setValue('not_has_other_resources', $item->not_has_other_resources);

                $templateProcessor->setValue('health_status', $item->health_status);
                $templateProcessor->setValue('good_health_status', $item->good_health_status);
                $templateProcessor->setValue('chronic_disease', $item->chronic_disease);
                $templateProcessor->setValue('disabled', $item->disabled);
                $templateProcessor->setValue('diseases_name', $item->diseases_name);
                $templateProcessor->setValue('has_disease', $item->has_disease);
                $templateProcessor->setValue('has_health_insurance', $item->has_health_insurance);
                $templateProcessor->setValue('have_health_insurance', $item->have_health_insurance);
                $templateProcessor->setValue('not_have_health_insurance', $item->not_have_health_insurance);
                $templateProcessor->setValue('health_insurance_number', $item->health_insurance_number);
                $templateProcessor->setValue('health_insurance_type', $item->health_insurance_type);
                $templateProcessor->setValue('has_device', $item->has_device);
                $templateProcessor->setValue('use_device', $item->use_device);
                $templateProcessor->setValue('not_use_device', $item->not_use_device);
                $templateProcessor->setValue('used_device_name', $item->used_device_name);
                $templateProcessor->setValue('health_details', $item->health_details);
                $templateProcessor->setValue('property_types', $item->property_types);
                $templateProcessor->setValue('rent_value', $item->rent_value);
                $templateProcessor->setValue('roof_materials', $item->roof_materials);
                $templateProcessor->setValue('habitable', $item->habitable);
                $templateProcessor->setValue('house_condition', $item->house_condition);
                $templateProcessor->setValue('residence_condition', $item->residence_condition);
                $templateProcessor->setValue('indoor_condition', $item->indoor_condition);
                $templateProcessor->setValue('rooms', $item->rooms);
                $templateProcessor->setValue('area', $item->area);
                $templateProcessor->setValue('repair_notes', $item->repair_notes);
                $templateProcessor->setValue('need_repair', $item->need_repair);
                $templateProcessor->setValue('repair', $item->repair);
                $templateProcessor->setValue('not_repair', $item->not_repair);
                $templateProcessor->setValue('deserted', $item->deserted);

                $templateProcessor->setValue('needs', $item->needs);
                $templateProcessor->setValue('promised', $item->promised);
                $templateProcessor->setValue('if_promised', $item->if_promised);
                $templateProcessor->setValue('was_promised', $item->was_promised);
                $templateProcessor->setValue('not_promised', $item->not_promised);
                $templateProcessor->setValue('reconstructions_organization_name', $item->reconstructions_organization_name);

                $templateProcessor->setValue('visitor', $item->visitor);
                $templateProcessor->setValue('notes', $item->notes);
                $templateProcessor->setValue('visited_at', $item->visited_at);
                $templateProcessor->setValue('visitor_opinion', $item->visitor_opinion);
                $templateProcessor->setValue('visitor_card', $item->visitor_card);
                $templateProcessor->setValue('visitor_evaluation', $item->visitor_evaluation);
                $templateProcessor->setValue('very_bad_condition_evaluation', $item->very_bad_condition_evaluation);
                $templateProcessor->setValue('bad_condition_evaluation', $item->bad_condition_evaluation);
                $templateProcessor->setValue('good_evaluation', $item->good_evaluation);
                $templateProcessor->setValue('very_good_evaluation', $item->very_good_evaluation);
                $templateProcessor->setValue('excellent_evaluation', $item->excellent_evaluation);


                if (sizeof($item->banks)!=0){
                    try{
                        $templateProcessor->cloneRow('r_bank_name', sizeof($item->banks));
                        $zxs=1;
                        foreach($item->banks as $rec) {
                            $templateProcessor->setValue('r_bank_name#' . $zxs, $rec->bank_name);
                            $templateProcessor->setValue('r_branch_name#' . $zxs, $rec->branch);
                            $templateProcessor->setValue('r_account_owner#' . $zxs, $rec->account_owner);
                            $templateProcessor->setValue('r_account_number#' . $zxs, $rec->account_number);
                            if($rec->check == true){
                                $templateProcessor->setValue('r_default#' . $zxs, trans('aid::application.yes') );
                            }else{
                                $templateProcessor->setValue('r_default#' . $zxs, trans('aid::application.no') );
                            }
                            $zxs++;
                        }
                    }catch (\Exception $e){}
                }else{
                    try{
                        $templateProcessor->cloneRow('r_bank_name', 1);
                        $templateProcessor->setValue('r_bank_name#' . 1, ' ');
                        $templateProcessor->setValue('r_branch_name#' . 1, ' ');
                        $templateProcessor->setValue('r_account_owner#' . 1, ' ');
                        $templateProcessor->setValue('r_account_number#' . 1,' ');
                        $templateProcessor->setValue('r_default#' . 1, ' ');
                    }catch (\Exception $e){  }
                }

                if (sizeof($item->home_indoor)!=0){
                    try{
                        $templateProcessor->cloneRow('row_es_name', sizeof($item->home_indoor));
                        $z=1;
                        foreach($item->home_indoor as $record) {
                            $templateProcessor->setValue('row_es_name#' . $z, $record->name);
                            $templateProcessor->setValue('row_es_if_exist#' . $z, $record->exist);
                            $templateProcessor->setValue('row_es_needs#' . $z, $record->needs);
                            $templateProcessor->setValue('row_es_condition#' . $z, $record->essentials_condition);
                            $templateProcessor->setValue('row_es_not_exist#' . $z, $record->not_exist);
                            $templateProcessor->setValue('row_es_very_bad_condition#' . $z, $record->very_bad_condition);
                            $templateProcessor->setValue('row_es_bad_condition#' . $z, $record->bad_condition);
                            $templateProcessor->setValue('row_es_good#' . $z, $record->good);
                            $templateProcessor->setValue('row_es_very_good#' . $z, $record->very_good);
                            $templateProcessor->setValue('row_es_excellent#' . $z, $record->excellent);
                            $z++;
                        }
                    }catch (\Exception $e){  }
                }
                else{
                    try{
                        $templateProcessor->cloneRow('row_es_name', 1);
                        $templateProcessor->setValue('row_es_name#' . 1, ' ');
                        $templateProcessor->setValue('row_es_if_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_es_needs#' . 1, ' ');
                        $templateProcessor->setValue('row_es_condition#' . 1, ' ');
                        $templateProcessor->setValue('row_es_not_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_es_very_bad_condition#' . 1, ' ');
                        $templateProcessor->setValue('row_es_bad_condition#' . 1, ' ');
                        $templateProcessor->setValue('row_es_good#' . 1, ' ');
                        $templateProcessor->setValue('row_es_very_good#' . 1, ' ');
                        $templateProcessor->setValue('row_es_excellent#' . 1, ' ');

                    }catch (\Exception $e){  }
                }
                if (sizeof($item->properties)!=0){
                    try{
                        $templateProcessor->cloneRow('row_pro_name', sizeof($item->properties));
                        $z2=1;
                        foreach($item->properties as $sub_record) {
                            $templateProcessor->setValue('row_pro_name#' . $z2, $sub_record->name);
                            $templateProcessor->setValue('row_pro_if_exist#' . $z2, $sub_record->has_property);
                            $templateProcessor->setValue('row_pro_exist#' . $z2, $sub_record->exist);
                            $templateProcessor->setValue('row_pro_not_exist#' . $z2, $sub_record->not_exist);
                            $templateProcessor->setValue('row_pro_count#' . $z2, $sub_record->quantity);
                            $z2++;
                        }
                    }catch (\Exception $e){  }
                }else{
                    try{
                        $templateProcessor->cloneRow('row_pro_name#', 1);
                        $templateProcessor->setValue('row_pro_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_pro_not_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_pro_name#' . 1, ' ');
                        $templateProcessor->setValue('row_pro_if_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_pro_count#' . 1, ' ');
                    }catch (\Exception $e){  }
                }
                if (sizeof($item->financial_aid_source)!=0){
                    try{
                        $templateProcessor->cloneRow('row_fin_name', sizeof($item->financial_aid_source));
                        $z3=1;
                        foreach($item->financial_aid_source as $record) {
                            $templateProcessor->setValue('row_fin_name#' . $z3, $record->name);
                            $templateProcessor->setValue('row_fin_if_exist#' . $z3, $record->aid_take);
                            $templateProcessor->setValue('row_fin_take#' . $z3, $record->take);
                            $templateProcessor->setValue('row_fin_not_take#' . $z3, $record->not_take);
                            $templateProcessor->setValue('row_fin_count#' . $z3, $record->aid_value);
                            $templateProcessor->setValue('row_fin_currency#' . $z3, $record->currency_name);
                            $z3++;
                        }
                    }catch (\Exception $e){  }
                }else{
                    try{
                        $templateProcessor->cloneRow('row_fin_name', 1);
                        $templateProcessor->setValue('row_fin_name#' . 1, ' ');
                        $templateProcessor->setValue('row_fin_if_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_fin_count#' . 1, ' ');
                        $templateProcessor->setValue('row_fin_take#' . 1, ' ');
                        $templateProcessor->setValue('row_fin_not_take#' . 1, ' ');
                        $templateProcessor->setValue('row_fin_currency#' . 1, ' ');
                    }catch (\Exception $e){  }
                }
                if (sizeof($item->non_financial_aid_source)!=0){
                    try{
                        $templateProcessor->cloneRow('row_nonfin_name', sizeof($item->non_financial_aid_source));
                        $z3=1;
                        foreach($item->non_financial_aid_source as $record) {
                            $templateProcessor->setValue('row_nonfin_name#' . $z3, $record->name);
                            $templateProcessor->setValue('row_nonfin_if_exist#' . $z3, $record->aid_take);
                            $templateProcessor->setValue('row_nonfin_not_take#' . $z3, $record->not_take);
                            $templateProcessor->setValue('row_nonfin_take#' . $z3, $record->take);
                            $templateProcessor->setValue('row_nonfin_count#' . $z3, $record->aid_value);
                            $z3++;
                        }
                    }catch (\Exception $e){  }
                } else{
                    try{
                        $templateProcessor->cloneRow('row_nonfin_name', 1);
                        $templateProcessor->setValue('row_nonfin_name#' . 1, ' ');
                        $templateProcessor->setValue('row_nonfin_if_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_nonfin_take#' . 1, ' ');
                        $templateProcessor->setValue('row_nonfin_not_take#' . 1, ' ');
                        $templateProcessor->setValue('row_nonfin_count#' . 1, ' ');
                    }catch (\Exception $e){  }
                }

                if (sizeof($item->family_members)!=0){
                    try{
                        $templateProcessor->cloneRow('fm_nam', sizeof($item->family_members));
                        $z=1;
                        foreach($item->family_members as $record) {
                            $templateProcessor->setValue('fm_inc#' . $z,$z);
                            $templateProcessor->setValue('fm_nam#' . $z, $record->name);
                            $templateProcessor->setValue('fm_bd#' . $z, $record->birthday);
                            $templateProcessor->setValue('fm_gen#' . $z, $record->gender);
                            $templateProcessor->setValue('fm_age#' . $z, $record->age);
                            $templateProcessor->setValue('fm_idc#' . $z, $record->id_card_number);
                            $templateProcessor->setValue('fm_ms#' . $z, $record->marital_status);
                            $templateProcessor->setValue('fm_kin#' . $z, $record->kinship_name);
                            $templateProcessor->setValue('fm_grd#' . $z, $record->grade);
                            $templateProcessor->setValue('fm_scl#' . $z, $record->school);
                            $templateProcessor->setValue('fm_hs#' . $z, $record->health_status);
                            $templateProcessor->setValue('fm_disnm#' . $z, $record->diseases_name);
                            $z++;
                        }
                    }catch (\Exception $e){  }
                }
                else{
                    try{
                        $templateProcessor->cloneRow('fm_nam',1);
                        $templateProcessor->setValue('fm_inc#' . 1,1);
                        $templateProcessor->setValue('fm_scl#' . 1, ' ');
                        $templateProcessor->setValue('fm_nam#' . 1, ' ');
                        $templateProcessor->setValue('fm_bd#' . 1, ' ');
                        $templateProcessor->setValue('fm_gen#' . 1, ' ');
                        $templateProcessor->setValue('fm_age#' . 1, ' ');
                        $templateProcessor->setValue('fm_idc#' . 1,' ');
                        $templateProcessor->setValue('fm_ms#' . 1, ' ');
                        $templateProcessor->setValue('fm_kin#' . 1, ' ');
                        $templateProcessor->setValue('fm_grd#' . 1, ' ');
                        $templateProcessor->setValue('fm_hs#' . 1, ' ');
                        $templateProcessor->setValue('fm_disnm#' . 1, ' ');
                    }catch (\Exception $e){  }
                }

                $templateProcessor->saveAs($dirName.'/'.$item->category_name.' '.$item->id_card_number.' '.$item->full_name.'.docx');

            }

            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');

            $zip = new \ZipArchive;
            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                foreach ( glob( $dirName . '/*' ) as $fileName )
                {
                    $file = basename( $fileName );
                    $zip->addFile( $fileName, $file );
                }

                $zip->close();
                array_map('unlink', glob("$dirName/*.*"));
                rmdir($dirName);
            }

            $message=trans('common::application.exported cases forms').trans('common::application.aids') ;

            \Log\Model\Log::saveNewLog('SOCIAL_SEARCH_CASE_EXPORTED',$message);

            return response()->json([
                'download_token' => $token]);




        }

        return response()->json(['status'=>false]);
    }

    /************ get existed object model by id ************/
    public function form($id)
    {
        $this->authorize('manageCases', SocialSearch::class);
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $item =Cases::fetch($id);
        return response()->json($item);

    }

    /************ EXPORT PDF Form ************/
    public function pdf($id){

        $this->authorize('manageCases', SocialSearch::class);
        $item =Cases::fetch($id);
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $html='';
//        PDF::SetTitle($item->full_name .' '.$item->search_name);
        PDF::SetFont('aealarabiya', '', 18);
        PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);

        PDF::SetMargins(PDF_MARGIN_LEFT, 20, PDF_MARGIN_RIGHT);
//        PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
//        PDF::SetFooterMargin(PDF_MARGIN_FOOTER);
        PDF::SetAutoPageBreak(TRUE);
        $lg = Array();
        $lg['a_meta_charset'] = 'UTF-8';
        $lg['a_meta_dir'] = 'rtl';
        $lg['a_meta_language'] = 'fa';
        $lg['w_page'] = 'page';
        PDF::setLanguageArray($lg);
        PDF::setCellHeightRatio(1.5);

        $date = date('Y-m-d');

        $organization_name = $item->organization_name;
        $search_date = $item->date;
        $category_name = $item->category_name;
        $first_name = $item->first_name;
        $second_name = $item->second_name;
        $third_name = $item->third_name ;
        $last_name = $item->last_name ;
        $name = $item->full_name;

        $id_card_number = $item->id_card_number;
        $birthday = $item->birthday;
        $age = $item->age;
        $gender = $item->gender;
        $gender_male = $item->gender_male;
        $gender_female = $item->gender_female;
        $deserted = $item->gender;
        $is_deserted = $item->is_deserted;
        $not_deserted = $item->not_deserted;
        $birth_place = $item->birth_place;
        $nationality = $item->nationality;
        $refugee = $item->refugee;

        $is_refugee = $item->is_refugee;
        $per_refugee = $item->per_refugee;
        $per_citizen = $item->per_citizen;
        $unrwa_card_number = $item->unrwa_card_number;

        $marital_status_name = $item->marital_status_name;

        $monthly_income = $item->monthly_income;

        $address = $item->address;

        $phone = $item->phone;
        $primary_mobile = $item->primary_mobile;
        $wataniya = $item->watanya;
        $secondary_mobile = $item->secondary_mobile ;

        $property_types = $item->property_types;
        $rent_value = $item->rent_value;
        $roof_materials = $item->roof_materials;
        $habitable = $item->habitable;
        $house_condition = $item->house_condition;
        $residence_condition = $item->residence_condition;
        $indoor_condition = $item->indoor_condition;
        $rooms = $item->rooms;
        $area = $item->area;

        $working = $item->working;
        $works = $item->works;
        $is_working = $item->is_working;
        $not_working = $item->not_working;
        $can_work = $item->can_work;
        $can_works = $item->can_works;
        $can_working = $item->can_working;
        $can_not_working = $item->can_not_working;
        $work_job = $item->work_job;
        $work_reason = $item->work_reason;
        $work_status = $item->work_status;
        $work_wage = $item->work_wage;
        $work_location = $item->work_location;

        $needs = $item->needs;
        $promised = $item->promised;
        $if_promised = $item->if_promised;
        $was_promised = $item->was_promised;
        $not_promised = $item->not_promised;
        $reconstructions_organization_name = $item->reconstructions_organization_name;

        $visitor = $item->visitor;
        $notes = $item->notes;
        $visited_at = $item->visited_at;

        $health_status = $item->health_status;
        $good_health_status = $item->good_health_status;
        $chronic_disease = $item->chronic_disease;
        $disabled = $item->disabled;
        $health_details = $item->health_details;
        $diseases_name = $item->diseases_name;

        $visitor_note = $item->visitor_note;
        $deserted = $item->deserted;
        $family_cnt = $item->family_cnt;
        $spouses = $item->spouses;
        $male_live = $item->male_live;
        $female_live = $item->female_live;


        $is_qualified = $item->is_qualified;
        $qualified_card_number = $item->qualified_card_number;


        $district = $item->governarate;
        $region_name = $item->region_name;
        $nearLocation = $item->nearLocation;
        $square_name = $item->square_name;
        $mosque_name = $item->mosque_name;

        $actual_monthly_income = $item->actual_monthly_income;
        $receivables = $item->receivables;
        $receivables_sk_amount = $item->receivables_sk_amount;
        $has_other_work_resources = $item->has_other_work_resources;


        $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
        $html.= '<h2 style="text-align:center;"> '.$category_name.'('.$search_date.')'.'</h2>';
        $html .= '<table border="1" style="border: 1px solid black; box-sizing:border-box;">';

        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.full_name').'  </b></td>
                       <td width="158" align="center">'.$name.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.id_card').'  </b></td>
                       <td width="152" align="center">'.$id_card_number.'</td>
                      
                   </tr>';

        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.BIRTH_DT').' </b></td>
                       <td width="158" align="center" >'.$birthday.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.birth_place').'</b></td>
                       <td width="152" align="center">'.$birth_place.'</td>
                   </tr>';

        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.SOCIAL_STATUS').'  </b></td>
                       <td width="158" align="center">'.$marital_status_name.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.nationality').'  </b></td>
                       <td width="152" align="center">'.$nationality.'</td>
                      
                   </tr>';

        $html .= ' <tr>
                    <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.gender').' </b></td>
                   <td width="158" align="center">'.$gender.'</td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('aid::application.deserted').'   </b></td>
                   <td width="152" align="center">'.$deserted.'</td>
               </tr>';

        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.citizen').'  / '.trans('common::application.refugee').'</b></td>
                       <td width="158" align="center">'.$refugee.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.unrwa_card_number').'</b></td>
                       <td width="152" align="center">'.$unrwa_card_number.'</td>
                   </tr>';

        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.is_qualified').'</b></td>
                       <td width="158" align="center">'.$is_qualified.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.qualified_card_number').'</b></td>
                       <td width="152" align="center">'.$qualified_card_number.'</td>
                   </tr>';
        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.family_member_count').'</b></td>
                       <td width="158" align="center">'.$family_cnt.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.spouses').'</b></td>
                       <td width="152" align="center">'.$spouses.'</td>
                   </tr>';

        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.male_live').'</b></td>
                       <td width="158" align="center">'.$male_live.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.female_live').'</b></td>
                       <td width="152" align="center">'.$female_live.'</td>
                   </tr>';

        $html .='</table>';
        $html .='<div style="height: 5px; color: white"> - </div>';
        $html .= '<table border="1" style="">';
        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="95" align="center"><b>  '.trans('common::application.district').' </b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="105" align="center"><b>  '.trans('common::application.region_').' </b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="105" align="center"><b>   '.trans('common::application.nearlocation_').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="105" align="center"><b>  '.trans('common::application.square').' </b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="103" align="center"><b>  '.trans('common::application.mosques').' </b></td>
                   </tr>';

        $html .= ' <tr>
                       <td width="95" align="center"> <br>'.$district.' <br></td>
                       <td width="105" align="center"> <br>'.$region_name.' <br></td>
                       <td width="105" align="center"> <br>'.$nearLocation.' <br></td>
                       <td width="105" align="center"> <br>'.$square_name.' <br></td>
                       <td width="103" align="center"> <br>'.$mosque_name.' <br></td>
                   </tr>';


        $html .='</table>';
        $html .='<div style="height: 5px; color: white"> - </div>';
        $html .= '<table border="1" style="">';
        $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                        '.trans('common::application.contact information').'
                        </b></td>
                   </tr>';
        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.mobile').'</b></td>
                       <td width="156" align="center">'.$primary_mobile.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.secondery_mobile').'</b></td>
                       <td width="157" align="center">'.$secondary_mobile.'</td>
                   </tr>';
        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.phone').'</b></td>
                       <td width="156" align="center">'.$phone.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.wataniya').'</b></td>
                       <td width="157" align="center">'.$wataniya.'</td>
                   </tr>';
        $html .='</table>';

        $html .='<div style="height: 5px; color: white"> - </div>';
        $html .= '<table border="1" style="">';
        $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                       '.trans('common::application.works info').' 
                        </b></td>
                   </tr>';

        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.is_she_working?').'</b></td>
                       <td width="156" align="center">'.$working.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.work_job_').' </b></td>
                       <td width="137" align="center">'.$work_job.'</td>
                   </tr>';
        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.is_can_work').'</b></td>
                       <td width="156" align="center">'.$can_work.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.work_reason').' </b></td>
                       <td width="137" align="center">'.$work_reason.'</td>
                   </tr>';
        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_location').' </b></td>
                       <td width="156" align="center">'.$work_location.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b>'.trans('common::application.Work_Status').' </b></td>
                       <td width="137" align="center">'.$work_status.'</td>
                   </tr>';
        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.Wage category').':  </b></td>
                       <td width="156" align="center">'.$work_wage.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b>'.trans('common::application.monthly_income').'</b></td>
                       <td width="137" align="center">'. $monthly_income .'</td>
                   </tr>';


        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.actual_monthly_income').'</b></td>
                       <td width="156" align="center">'.$actual_monthly_income.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.has_other_work_resources').' </b></td>
                       <td width="137" align="center">'.$has_other_work_resources.'</td>
                       
                   </tr>';
        $html .= ' <tr>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.receivables').' </b></td>
                           <td width="156" align="center">'.$receivables.'</td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b>   '.trans('common::application.receivables_sk_amount').'</b></td>
                           <td width="137" align="center">'.$receivables_sk_amount.'</td>
                           
                       </tr>';

        $html .='</table>';


        $html .='<div style="height: 5px; color: white"> - </div>';
        $html .= '<table border="1">';
        $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                       '.trans('common::application.home info').'
                        </b></td>
                   </tr>';
        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.property_type').'</b></td>
                       <td width="156" align="center">'.$property_types.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.property_type').'('.trans('common::application.for renter').') </b></td>
                       <td width="157" align="center">'.$rent_value.'</td>
                   </tr>';
        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.roof_material').'</b></td>
                       <td width="156" align="center">'.$roof_materials.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.residence_condition').'</b></td>
                       <td width="157" align="center">'.$house_condition.'</td>
                   </tr>';
        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.indoor_condition').'</b></td>
                       <td width="156" align="center">'.$residence_condition.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.habitable').'</b></td>
                       <td width="157" align="center">'.$habitable.'</td>
                   </tr>';
        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.home_area').'</b></td>
                       <td width="156" align="center">'.$area.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.rooms').'</b></td>
                       <td width="157" align="center">'.$rooms.'</td>
                   </tr>';
        $need_repair = $item->need_repair;
        $repair_notes = $item->repair_notes;
        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="200" align="center"><b>'.
            trans('common::application.need_repair').'</b></td>
                       <td width="312" align="center">'.$need_repair.'</td>
                   </tr>';
        $html .='</table>';

        $html .='</body></html>';
        PDF::AddPage('P','A4');
        PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


        $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';

        $html .='<div style="height: 5px; color: white"> - </div>';
        $html .= '<table border="1" style="">';
        $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                      '.trans('common::application.health info').'
                        </b></td>
                   </tr>';

        $has_health_insurance = $item->has_health_insurance;
        $health_insurance_number = $item->health_insurance_number;
        $health_insurance_type = $item->health_insurance_type;

        $has_device = $item->has_device;
        $used_device_name = $item->used_device_name;

        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.has_health_insurance').' </b></td>
                       <td width="156" align="center">'.$has_health_insurance.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.health_insurance_number').'</b></td>
                       <td width="157" align="center">'.$health_insurance_number.'</td>
                   </tr>';
        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.health_insurance_type').' </b></td>
                       <td width="156" align="center">'.$health_insurance_type.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.has_device').'</b></td>
                       <td width="157" align="center">'.$has_device.'</td>
                   </tr>';

        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.used_device_name').'</b></td>
                       <td width="413" align="center">'.$used_device_name.'</td>
                   </tr>';

        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.disease_/_disability_details').'</b></td>
                       <td width="413" align="center">'.$health_details.'</td>
                   </tr>';

        $html .='</table>';

        $html .= '<div style="height: 5px; color: white"> - </div>';
        $html .= '<table border="1" style="">';
        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="300" align="center"><b> '.trans('common::application.reconstructions_promised').'? ('.trans('common::application.yes').' /'.trans('common::application.no').') </b></td>
                       <td width="213" align="center">' . $promised . '</td>
                   </tr>';
        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="300" align="center"><b> '.trans('common::application.ReconstructionOrg').'('.trans('common::application.if the answer to the previous question is yes').'):  </b></td>
                       <td width="213" align="center">' . $reconstructions_organization_name . '</td>
                   </tr>';
        $html .='</table>';

        $html .='<div style="height: 5px; color: white"> - </div>';
        $html .= '<table border="1" style="">';
        $html .= ' <tr>
                       <td style="height: 50px ; padding: 55px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b> 
                       '.trans('common::application.case_needs').'
                       </b></td>
                       <td width="415" align="center">' . $needs . '</td>
                   </tr>';

        $html .= ' <tr>
                       <td style="height: 50px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b>  '.trans("common::application.repair_notes").'</b></td>
                       <td width="415" align="center">' . $repair_notes . '</td>
                   </tr>';
        $html .= ' <tr>
                       <td style="height: 50px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b>  '.trans("common::application.social researcher's recommendations").'</b></td>
                       <td width="415" align="center">' . $notes . '</td>
                   </tr>';

        $visitor_opinion = $item->visitor_opinion;
        $html .= ' <tr>
                       <td style="height: 50px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b>  '.trans("common::application.visitor_opinion").'</b></td>
                       <td width="415" align="center">' . $visitor_opinion . '</td>
                   </tr>';
        $html .='</table>';

        $html .='<div style="height: 5px; color: white"> - </div>';
        $html .= '<table border="1" style="">';

        $visitor_card = $item->visitor_card;
        $visitor_evaluation = $item->visitor_evaluation_;
        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.visitor_card').'</b></td>
                       <td width="156" align="center">' . $visitor_card . '</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.visitor').' </b></td>
                       <td width="157" align="center">' . $visitor . '</td>
                   </tr>';
        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.visited_at').'</b></td>
                       <td width="156" align="center">' . $visited_at . '</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.visitor_evaluation').' </b></td>
                       <td width="157" align="center">' . $visitor_evaluation . '</td>
                   </tr>';



        $html .='</table>';

        $html .='</body></html>';
        PDF::AddPage('P','A4');
        PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

        if (sizeof($item->family_members)!=0){

            $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
            $html.= '<h2 style="text-align:center;"> '.trans('common::application.family-data').'

    ('. sizeof($item->family_members) .')</h2>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.name').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.id_card').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.BIRTH_DT').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="40" align="center"><b> '.trans('common::application.gender').'</b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.SOCIAL_STATUS').'</b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.currently_study').'</b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.health_condition').' </b></td>
                   </tr>';
            $z=1;
            foreach($item->family_members as $record) {
                $fm_nam = $record->name;
                $fm_idc = $record->id_card_number;
                $fm_bd = $record->birthday;
                $fm_gen = $record->gender;
                $fm_age = $record->age;
                $fm_ms = $record->marital_status;
                $fm_kin = $record->kinship_name;
                $fm_grd = $record->grade;
                $fm_scl = $record->school;
                $fm_currently_study = $record->currently_study;
                $fm_hs = $record->health_status;
                $fm_disnm = $record->diseases_name;
                $html .= ' <tr>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="120" align="center"><b> '.$fm_nam.'  </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="70" align="center"><b> '.$fm_idc.' </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="70" align="center"><b> '.$fm_bd.'   </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="40" align="center"><b>'.$fm_gen.'</b></td> 
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="70" align="center"><b> '.$fm_ms.' </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="60" align="center"><b> '.$fm_currently_study.' </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$fm_hs.'   </b></td>
                   </tr>';
                $z++;
//                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="50" align="center"><b>'.$fm_kin.'</b></td>
//                                    <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_working.'   </b></td>
//                <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$fm_job.'   </b></td>
            }
            $html .='</table>';
            $html .='</body></html>';

            PDF::SetMargins(PDF_MARGIN_LEFT, 2, PDF_MARGIN_RIGHT);
            PDF::AddPage('P','A4');
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        }
        if (sizeof($item->home_indoor)!=0){
            $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
            $html.= '<h2 style="text-align:center;"> '.trans('common::application.furniture-info').'</h2>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="200" align="center"><b> '.trans('common::application.content').'  </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.exist').'/'.trans('common::application.not exist').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_status').'   </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.needs').'</b></td>
                   </tr>';

            foreach($item->home_indoor as $record) {

                $row_es_name = $record->name;
                $row_es_if_exist= $record->exist;
                $row_es_needs=is_null($record->needs) ? ' ' : $record->needs;
                $row_es_condition=is_null($record->essentials_condition) ? ' ' : $record->essentials_condition;

                $html .= ' <tr>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="200" align="center"><b> '.$row_es_name.'  </b></td>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$row_es_if_exist.' </b></td>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$row_es_condition.'   </b></td>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b>'.$row_es_needs .'</b></td>
                   </tr>';
            }
            $html .='</table>';
            $html .='</body></html>';

            PDF::AddPage('P','A4');
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        }
        if (sizeof($item->financial_aid_source)!=0){

            $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
            $html.= '<h2 style="text-align:center;"> '.trans('common::application.concrete foreign aid data').'</h2>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="200" align="center"><b>  '.trans('common::application.Name of the organization source of assistance').'  </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="130" align="center"><b> '.trans('common::application.Does the family receive help from org?').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.quantity').'   </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.currency_name').' </b></td>
                   </tr>';

            foreach($item->financial_aid_source as $record) {
                $row_fin_name = $record->name;
                $row_fin_if_exist= $record->aid_take;
                $row_fin_count= $record->aid_value;
                $row_fin_currency= $record->currency_name;
                $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="200" align="center"><b> '.$row_fin_name.'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="130" align="center"><b> '.$row_fin_if_exist.' </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$row_fin_count.'   </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="80" align="center"><b>'.$row_fin_currency.'</b></td>
                   </tr>';
            }
            $html .='</table>';
            $html .='</body></html>';
            PDF::AddPage('P','A4');
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


        }
        if (sizeof($item->non_financial_aid_source)!=0){
            $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
            $html.= '<h2 style="text-align:center;"> '.trans('common::application.concrete foreign aid data').'</h2>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="250" align="center"><b>  '.trans('common::application.Name of the organization source of assistance').'  </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="150" align="center"><b> '.trans('common::application.Does the family receive help from org?').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.quantity').'   </b></td>
                   </tr>';

            foreach($item->non_financial_aid_source as $record) {
                $row_non_fin_name = $record->name;
                $row_non_fin_if_exist= $record->aid_take;
                $row_non_fin_count= $record->aid_value;
                $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="250" align="center"><b> '.$row_non_fin_name.'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="150" align="center"><b> '.$row_non_fin_if_exist.' </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$row_non_fin_count.'   </b></td>
                   </tr>';

            }

            $html .='</table>';

            $html .='</body></html>';
            PDF::AddPage('P','A4');
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        }
        if (sizeof($item->properties)!=0){
            $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
            $html.= '<h2 style="text-align:center;"> '.trans('common::application.Family property data').'</h2>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="250" align="center"><b>  '.trans('common::application.Naming property').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="140" align="center"><b> '.trans('common::application.exist').'/'.trans('common::application.not exist').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.quantity').'   </b></td>
                   </tr>';
            foreach($item->properties as $record) {
                $pro_name = $record->name;
                $has_property= $record->has_property;
                $quantity= $record->quantity;
                $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="250" align="center"><b> '.$pro_name.'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="140" align="center"><b> '.$has_property.' </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="120" align="center"><b> '.$quantity.'   </b></td>
                   </tr>';

            }
            $html .='</table>';

            $html .='</body></html>';
            PDF::AddPage('P','A4');
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);  }

        $dirName = base_path('storage/app/templates/CaseForm');
        if (!is_dir($dirName)) {
            @mkdir($dirName, 0777, true);
        }

        PDF::Output($dirName.'/'.$item->category_name.' '.$item->full_name . '.pdf', 'F');
        $token = md5(uniqid());
        $zipFileName = storage_path('tmp/export_' . $token . '.zip');

        $zip = new \ZipArchive;
        if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
        {
            foreach ( glob( $dirName . '/*' ) as $fileName )
            {
                $file = basename( $fileName );
                $zip->addFile( $fileName, $file );
            }

            $zip->close();
            array_map('unlink', glob("$dirName/*.*"));
            rmdir($dirName);
        }

        return response()->json(['download_token' => $token]);

    }

    /************ EXPORT WORD Form ************/
    public function word($id){

        $this->authorize('manageCases', SocialSearch::class);
        $user = \Auth::user();
        $item =Cases::fetch($id);

        $organization_id = $item->organization_id;
        $dirName = base_path('storage/app/templates/CaseForm');
        if (!is_dir($dirName)) {
            @mkdir($dirName, 0777, true);
        }
        $Template=Templates::where(['category_id'=>$item->category_id,'organization_id'=>$user->organization_id])->first();
        if($Template){
            $path = base_path('storage/app/'.$Template->filename);
        }
        else{
            $default_template=\Setting\Model\Setting::where(['id'=>'social-search-form','organization_id'=>$user->organization_id])->first();

            if($default_template){
                $path = base_path('storage/app/'.$default_template->value);
            }else{
                $path = base_path('storage/app/templates/default-search-form-template.docx');
            }
        }

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);

        $templateProcessor->setValue('date', $item->date);
        $templateProcessor->setValue('organization_name', $item->organization_name);
        $templateProcessor->setValue('category_name', $item->category_name);

        $templateProcessor->setValue('first_name', $item->first_name);
        $templateProcessor->setValue('second_name', $item->second_name);
        $templateProcessor->setValue('third_name', $item->third_name );
        $templateProcessor->setValue('last_name', $item->last_name );
        $templateProcessor->setValue('name', $item->full_name);

        $templateProcessor->setValue('id_card_number', $item->id_card_number);
        $templateProcessor->setValue('card_type', $item->card_type);
        $templateProcessor->setValue('birthday', $item->birthday);
        $templateProcessor->setValue('age', $item->age);
        $templateProcessor->setValue('gender', $item->gender);
        $templateProcessor->setValue('gender_male', $item->gender_male);
        $templateProcessor->setValue('gender_female', $item->gender_female);
        $templateProcessor->setValue('birth_place', $item->birth_place);
        $templateProcessor->setValue('nationality', $item->nationality);
        $templateProcessor->setValue('refugee', $item->refugee);
        $templateProcessor->setValue('is_refugee', $item->is_refugee);
        $templateProcessor->setValue('per_refugee', $item->per_refugee);
        $templateProcessor->setValue('is_citizen', $item->is_citizen);
        $templateProcessor->setValue('per_citizen', $item->per_citizen);
        $templateProcessor->setValue('unrwa_card_number', $item->unrwa_card_number);
        $templateProcessor->setValue('marital_status_name', $item->marital_status_name);

        $templateProcessor->setValue('deserted', $item->deserted);
        $templateProcessor->setValue('is_deserted', $item->is_deserted);
        $templateProcessor->setValue('not_deserted', $item->not_deserted);

        $templateProcessor->setValue('receivables', $item->receivables);
        $templateProcessor->setValue('is_receivables', $item->is_receivables);
        $templateProcessor->setValue('is_ receivables', $item->not_receivables);
        $templateProcessor->setValue('not_receivables', $item->not_receivables);
        $templateProcessor->setValue('receivables_sk_amount', $item->receivables_sk_amount);

        $templateProcessor->setValue('is_qualified', $item->is_qualified);
        $templateProcessor->setValue('qualified', $item->qualified);
        $templateProcessor->setValue('not_qualified', $item->not_qualified);
        $templateProcessor->setValue('qualified_card_number', $item->qualified_card_number);

        $templateProcessor->setValue('monthly_income', $item->monthly_income);
        $templateProcessor->setValue('actual_monthly_income', $item->actual_monthly_income);

        $templateProcessor->setValue('address', $item->address);
        $templateProcessor->setValue('country', $item->country);
        $templateProcessor->setValue('governarate', $item->governarate);
        $templateProcessor->setValue('nearLocation', $item->nearLocation);
        $templateProcessor->setValue('mosque_name', $item->mosque_name);
        $templateProcessor->setValue('region', $item->region_name);
        $templateProcessor->setValue('square', $item->square_name);
        $templateProcessor->setValue('street_address', $item->street_address);

        $templateProcessor->setValue('family_cnt', $item->family_cnt);
        $templateProcessor->setValue('male_live', $item->male_live);
        $templateProcessor->setValue('female_live', $item->female_live);
        $templateProcessor->setValue('spouses', $item->spouses);
        $templateProcessor->setValue('family_count', $item->family_count);

        $templateProcessor->setValue('phone', $item->phone);
        $templateProcessor->setValue('primary_mobile', $item->primary_mobile);
        $templateProcessor->setValue('wataniya', $item->wataniya);
        $templateProcessor->setValue('secondary_mobile', $item->secondary_mobile );

        $templateProcessor->setValue('working', $item->working);
        $templateProcessor->setValue('works', $item->works);
        $templateProcessor->setValue('is_working', $item->is_working);
        $templateProcessor->setValue('not_working', $item->not_working);
        $templateProcessor->setValue('can_work', $item->can_work);
        $templateProcessor->setValue('can_works', $item->can_works);
        $templateProcessor->setValue('can_working', $item->can_working);
        $templateProcessor->setValue('can_not_working', $item->can_not_working);
        $templateProcessor->setValue('work_job', $item->work_job);
        $templateProcessor->setValue('work_reason', $item->work_reason);
        $templateProcessor->setValue('work_status', $item->work_status);
        $templateProcessor->setValue('work_wage', $item->work_wage);
        $templateProcessor->setValue('work_location', $item->work_location);
        $templateProcessor->setValue('has_other_work_resources', $item->has_other_work_resources);
        $templateProcessor->setValue('has_other_resources', $item->has_other_resources);
        $templateProcessor->setValue('not_has_other_resources', $item->not_has_other_resources);

        $templateProcessor->setValue('health_status', $item->health_status);
        $templateProcessor->setValue('good_health_status', $item->good_health_status);
        $templateProcessor->setValue('chronic_disease', $item->chronic_disease);
        $templateProcessor->setValue('disabled', $item->disabled);
        $templateProcessor->setValue('diseases_name', $item->diseases_name);
        $templateProcessor->setValue('has_disease', $item->has_disease);
        $templateProcessor->setValue('has_health_insurance', $item->has_health_insurance);
        $templateProcessor->setValue('have_health_insurance', $item->have_health_insurance);
        $templateProcessor->setValue('not_have_health_insurance', $item->not_have_health_insurance);
        $templateProcessor->setValue('health_insurance_number', $item->health_insurance_number);
        $templateProcessor->setValue('health_insurance_type', $item->health_insurance_type);
        $templateProcessor->setValue('has_device', $item->has_device);
        $templateProcessor->setValue('use_device', $item->use_device);
        $templateProcessor->setValue('not_use_device', $item->not_use_device);
        $templateProcessor->setValue('used_device_name', $item->used_device_name);
        $templateProcessor->setValue('health_details', $item->health_details);
        $templateProcessor->setValue('property_types', $item->property_types);
        $templateProcessor->setValue('rent_value', $item->rent_value);
        $templateProcessor->setValue('roof_materials', $item->roof_materials);
        $templateProcessor->setValue('habitable', $item->habitable);
        $templateProcessor->setValue('house_condition', $item->house_condition);
        $templateProcessor->setValue('residence_condition', $item->residence_condition);
        $templateProcessor->setValue('indoor_condition', $item->indoor_condition);
        $templateProcessor->setValue('rooms', $item->rooms);
        $templateProcessor->setValue('area', $item->area);
        $templateProcessor->setValue('repair_notes', $item->repair_notes);
        $templateProcessor->setValue('need_repair', $item->need_repair);
        $templateProcessor->setValue('repair', $item->repair);
        $templateProcessor->setValue('not_repair', $item->not_repair);
        $templateProcessor->setValue('deserted', $item->deserted);

        $templateProcessor->setValue('needs', $item->needs);
        $templateProcessor->setValue('promised', $item->promised);
        $templateProcessor->setValue('if_promised', $item->if_promised);
        $templateProcessor->setValue('was_promised', $item->was_promised);
        $templateProcessor->setValue('not_promised', $item->not_promised);
        $templateProcessor->setValue('reconstructions_organization_name', $item->reconstructions_organization_name);

        $templateProcessor->setValue('visitor', $item->visitor);
        $templateProcessor->setValue('notes', $item->notes);
        $templateProcessor->setValue('visited_at', $item->visited_at);
        $templateProcessor->setValue('visitor_opinion', $item->visitor_opinion);
        $templateProcessor->setValue('visitor_card', $item->visitor_card);
        $templateProcessor->setValue('visitor_evaluation', $item->visitor_evaluation);
        $templateProcessor->setValue('very_bad_condition_evaluation', $item->very_bad_condition_evaluation);
        $templateProcessor->setValue('bad_condition_evaluation', $item->bad_condition_evaluation);
        $templateProcessor->setValue('good_evaluation', $item->good_evaluation);
        $templateProcessor->setValue('very_good_evaluation', $item->very_good_evaluation);
        $templateProcessor->setValue('excellent_evaluation', $item->excellent_evaluation);


        if (sizeof($item->banks)!=0){
            try{
                $templateProcessor->cloneRow('r_bank_name', sizeof($item->banks));
                $zxs=1;
                foreach($item->banks as $rec) {
                    $templateProcessor->setValue('r_bank_name#' . $zxs, $rec->bank_name);
                    $templateProcessor->setValue('r_branch_name#' . $zxs, $rec->branch);
                    $templateProcessor->setValue('r_account_owner#' . $zxs, $rec->account_owner);
                    $templateProcessor->setValue('r_account_number#' . $zxs, $rec->account_number);
                    if($rec->check == true){
                        $templateProcessor->setValue('r_default#' . $zxs, trans('aid::application.yes') );
                    }else{
                        $templateProcessor->setValue('r_default#' . $zxs, trans('aid::application.no') );
                    }
                    $zxs++;
                }
            }catch (\Exception $e){}
        }else{
            try{
                $templateProcessor->cloneRow('r_bank_name', 1);
                $templateProcessor->setValue('r_bank_name#' . 1, ' ');
                $templateProcessor->setValue('r_branch_name#' . 1, ' ');
                $templateProcessor->setValue('r_account_owner#' . 1, ' ');
                $templateProcessor->setValue('r_account_number#' . 1,' ');
                $templateProcessor->setValue('r_default#' . 1, ' ');
            }catch (\Exception $e){  }
        }

        if (sizeof($item->home_indoor)!=0){
            try{
                $templateProcessor->cloneRow('row_es_name', sizeof($item->home_indoor));
                $z=1;
                foreach($item->home_indoor as $record) {
                    $templateProcessor->setValue('row_es_name#' . $z, $record->name);
                    $templateProcessor->setValue('row_es_if_exist#' . $z, $record->exist);
                    $templateProcessor->setValue('row_es_needs#' . $z, $record->needs);
                    $templateProcessor->setValue('row_es_condition#' . $z, $record->essentials_condition);
                    $templateProcessor->setValue('row_es_not_exist#' . $z, $record->not_exist);
                    $templateProcessor->setValue('row_es_very_bad_condition#' . $z, $record->very_bad_condition);
                    $templateProcessor->setValue('row_es_bad_condition#' . $z, $record->bad_condition);
                    $templateProcessor->setValue('row_es_good#' . $z, $record->good);
                    $templateProcessor->setValue('row_es_very_good#' . $z, $record->very_good);
                    $templateProcessor->setValue('row_es_excellent#' . $z, $record->excellent);
                    $z++;
                }
            }catch (\Exception $e){  }
        }
        else{
            try{
                $templateProcessor->cloneRow('row_es_name', 1);
                $templateProcessor->setValue('row_es_name#' . 1, ' ');
                $templateProcessor->setValue('row_es_if_exist#' . 1, ' ');
                $templateProcessor->setValue('row_es_needs#' . 1, ' ');
                $templateProcessor->setValue('row_es_condition#' . 1, ' ');
                $templateProcessor->setValue('row_es_not_exist#' . 1, ' ');
                $templateProcessor->setValue('row_es_very_bad_condition#' . 1, ' ');
                $templateProcessor->setValue('row_es_bad_condition#' . 1, ' ');
                $templateProcessor->setValue('row_es_good#' . 1, ' ');
                $templateProcessor->setValue('row_es_very_good#' . 1, ' ');
                $templateProcessor->setValue('row_es_excellent#' . 1, ' ');

            }catch (\Exception $e){  }
        }
        if (sizeof($item->properties)!=0){
            try{
                $templateProcessor->cloneRow('row_pro_name', sizeof($item->properties));
                $z2=1;
                foreach($item->properties as $sub_record) {
                    $templateProcessor->setValue('row_pro_name#' . $z2, $sub_record->name);
                    $templateProcessor->setValue('row_pro_if_exist#' . $z2, $sub_record->has_property);
                    $templateProcessor->setValue('row_pro_exist#' . $z2, $sub_record->exist);
                    $templateProcessor->setValue('row_pro_not_exist#' . $z2, $sub_record->not_exist);
                    $templateProcessor->setValue('row_pro_count#' . $z2, $sub_record->quantity);
                    $z2++;
                }
            }catch (\Exception $e){  }
        }else{
            try{
                $templateProcessor->cloneRow('row_pro_name#', 1);
                $templateProcessor->setValue('row_pro_exist#' . 1, ' ');
                $templateProcessor->setValue('row_pro_not_exist#' . 1, ' ');
                $templateProcessor->setValue('row_pro_name#' . 1, ' ');
                $templateProcessor->setValue('row_pro_if_exist#' . 1, ' ');
                $templateProcessor->setValue('row_pro_count#' . 1, ' ');
            }catch (\Exception $e){  }
        }
        if (sizeof($item->financial_aid_source)!=0){
            try{
                $templateProcessor->cloneRow('row_fin_name', sizeof($item->financial_aid_source));
                $z3=1;
                foreach($item->financial_aid_source as $record) {
                    $templateProcessor->setValue('row_fin_name#' . $z3, $record->name);
                    $templateProcessor->setValue('row_fin_if_exist#' . $z3, $record->aid_take);
                    $templateProcessor->setValue('row_fin_take#' . $z3, $record->take);
                    $templateProcessor->setValue('row_fin_not_take#' . $z3, $record->not_take);
                    $templateProcessor->setValue('row_fin_count#' . $z3, $record->aid_value);
                    $templateProcessor->setValue('row_fin_currency#' . $z3, $record->currency_name);
                    $z3++;
                }
            }catch (\Exception $e){  }
        }else{
            try{
                $templateProcessor->cloneRow('row_fin_name', 1);
                $templateProcessor->setValue('row_fin_name#' . 1, ' ');
                $templateProcessor->setValue('row_fin_if_exist#' . 1, ' ');
                $templateProcessor->setValue('row_fin_count#' . 1, ' ');
                $templateProcessor->setValue('row_fin_take#' . 1, ' ');
                $templateProcessor->setValue('row_fin_not_take#' . 1, ' ');
                $templateProcessor->setValue('row_fin_currency#' . 1, ' ');
            }catch (\Exception $e){  }
        }
        if (sizeof($item->non_financial_aid_source)!=0){
            try{
                $templateProcessor->cloneRow('row_nonfin_name', sizeof($item->non_financial_aid_source));
                $z3=1;
                foreach($item->non_financial_aid_source as $record) {
                    $templateProcessor->setValue('row_nonfin_name#' . $z3, $record->name);
                    $templateProcessor->setValue('row_nonfin_if_exist#' . $z3, $record->aid_take);
                    $templateProcessor->setValue('row_nonfin_not_take#' . $z3, $record->not_take);
                    $templateProcessor->setValue('row_nonfin_take#' . $z3, $record->take);
                    $templateProcessor->setValue('row_nonfin_count#' . $z3, $record->aid_value);
                    $z3++;
                }
            }catch (\Exception $e){  }
        } else{
            try{
                $templateProcessor->cloneRow('row_nonfin_name', 1);
                $templateProcessor->setValue('row_nonfin_name#' . 1, ' ');
                $templateProcessor->setValue('row_nonfin_if_exist#' . 1, ' ');
                $templateProcessor->setValue('row_nonfin_take#' . 1, ' ');
                $templateProcessor->setValue('row_nonfin_not_take#' . 1, ' ');
                $templateProcessor->setValue('row_nonfin_count#' . 1, ' ');
            }catch (\Exception $e){  }
        }

        if (sizeof($item->family_members)!=0){
            try{
                $templateProcessor->cloneRow('fm_nam', sizeof($item->family_members));
                $z=1;
                foreach($item->family_members as $record) {
                    $templateProcessor->setValue('fm_inc#' . $z,$z);
                    $templateProcessor->setValue('fm_nam#' . $z, $record->name);
                    $templateProcessor->setValue('fm_bd#' . $z, $record->birthday);
                    $templateProcessor->setValue('fm_gen#' . $z, $record->gender);
                    $templateProcessor->setValue('fm_age#' . $z, $record->age);
                    $templateProcessor->setValue('fm_idc#' . $z, $record->id_card_number);
                    $templateProcessor->setValue('fm_ms#' . $z, $record->marital_status);
                    $templateProcessor->setValue('fm_kin#' . $z, $record->kinship_name);
                    $templateProcessor->setValue('fm_grd#' . $z, $record->grade);
                    $templateProcessor->setValue('fm_scl#' . $z, $record->school);
                    $templateProcessor->setValue('fm_hs#' . $z, $record->health_status);
                    $templateProcessor->setValue('fm_disnm#' . $z, $record->diseases_name);
                    $z++;
                }
            }catch (\Exception $e){  }
        }
        else{
            try{
                $templateProcessor->cloneRow('fm_nam',1);
                $templateProcessor->setValue('fm_inc#' . 1,1);
                $templateProcessor->setValue('fm_scl#' . 1, ' ');
                $templateProcessor->setValue('fm_nam#' . 1, ' ');
                $templateProcessor->setValue('fm_bd#' . 1, ' ');
                $templateProcessor->setValue('fm_gen#' . 1, ' ');
                $templateProcessor->setValue('fm_age#' . 1, ' ');
                $templateProcessor->setValue('fm_idc#' . 1,' ');
                $templateProcessor->setValue('fm_ms#' . 1, ' ');
                $templateProcessor->setValue('fm_kin#' . 1, ' ');
                $templateProcessor->setValue('fm_grd#' . 1, ' ');
                $templateProcessor->setValue('fm_hs#' . 1, ' ');
                $templateProcessor->setValue('fm_disnm#' . 1, ' ');
            }catch (\Exception $e){  }
        }

        $filename = $dirName.'/'.$item->category_name.' '.$item->id_card_number.' '.$item->full_name;
        $templateProcessor->saveAs($filename .".docx");
        $token = md5(uniqid());
        $zipFileName = storage_path('tmp/export_' . $token . '.zip');

        $zip = new \ZipArchive;
        if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
        {
            foreach ( glob( $dirName . '/*' ) as $fileName )
            {
                $file = basename( $fileName );
                $zip->addFile( $fileName, $file );
            }

            $zip->close();
            array_map('unlink', glob("$dirName/*.*"));
            rmdir($dirName);
        }

        return response()->json(['download_token' => $token]);

    }

    /************ IMPORT CASE FORM DATA ************/
    public function importCases(Request $request){


        $this->authorize('import', SocialSearch::class);
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $importFile=$request->file ;
        if ($importFile->isValid()) {

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();

            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();

                if(!is_null($first)){
                    $keys = $first->keys()->toArray();

                    if(sizeof($keys) >0 ){

                        $constraint =  ["rkm_alhoy", "noaa_albtak"
                            , "alasm_alaol_aarby", "alasm_althany_aarby", "alasm_althalth_aarby", "alaaael_aarby"
                            , "alasm_alaol_anjlyzy", "alasm_althany_anjlyzy", "alasm_althalth_anjlyzy"
                            , "alaaael_anjlyzy", "tarykh_almylad", "mkan_almylad", "aljns", "aljnsy"
                            , "hal_almoatn", "rkm_krt_altmoyn", "alhal_alajtmaaay", "mhjor", "aadd_alzojat","aadd_afrad_alasr"
                            , "aadd_alabna_ghyr_almtzojyn", "aadd_albnat_alghyr_mtzojat", "ahly_rb_alasr"
                            , "rkm_almaayyl", "aldol", "almhafth", "almntk", "alhy", "almrbaa", "almsjd"
                            , "tfasyl_alaanoan", "rkm_joal_asasy", "rkm_joal_ahtyaty", "rkm_alotny", "rkm_hatf", "kadr_aal_alaaml"
                            , "sbb_aadm_alkdr", "hl_yaaml", "hal_alaaml", "alothyf", "mkan_alaaml", "fe_alajr"
                            , "hl_ldyh_msadr_dkhl_akhr", "aldkhl_alshhry_shykl", "aldkhl_alshhry_alfaaly_shykl"
                            , "hl_ldyh_thmm_maly", "kym_althmm_balshykl", "mlky_almnzl", "skf_almnzl"
                            , "kably_almnzl_llskn", "odaa_almnzl", "aadd_alghrf", "hal_alathath"
                            , "hal_almskn", "almsah_balmtr", "kym_alayjar", "hl_bhaj_ltrmym_llmoaem"
                            , "osf_odaa_almnzl", "hl_ldyh_tamyn_shy", "noaa_altamyn_alshy", "rkm_altamyn_alshy"
                            , "hl_ystkhdm_jhaz", "asm_aljhaz_almstkhdm", "alhal_alshy", "almrd_ao_alaaaak"
                            , "tfasyl_alhal_alshy", "hl_oaadt_balbna_mn_kbl_okal_alghoth_ao_jh_akhr"
                            , "asm_almoess_alty_oaadt_balbna", "ahtyajat_alaaael_bshd", "asm_albahth_alajtmaaay", "rkm_hoy_albahth"
                            , "tosyat_albahth_alajtmaaay", "ray_albahth_alajtmaaay", "tkyym_albahth_lhal_almnzl", "tarykh_alzyar"];

                        $validFile=true;

                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if($validFile){
                            $user = \Auth::user();
                            $errors=[];
                            $processed=[];
                            $result=[];
                            $processed_=[];

                            $duplicated =0;
                            $old_case=0;
                            $success =0;
                            $father_id =Null;
                            $mother_id =Null;
                            $guardian_id =Null;
                            $invalid_cards=0;

                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if($total == 0){
                                return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                            }
                            if ($total < 2000) {
                                $search_id = $request->search_id;

                                foreach ($records as $key =>$value) {

                                    $id_card_number =(int)$value['rkm_alhoy'];
                                    if(in_array($id_card_number,$processed)){
                                        $result[] = ['id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                                        $duplicated++;
                                    } else{
                                        if(GovServices::checkCard($id_card_number)){
                                            $isUnique = Cases::isUnique($id_card_number,$search_id);
                                            if($isUnique['status'] == false) {

                                                $remap = Cases::filterInputs($value);
                                                if(sizeof($remap['errors']) >0 ){
                                                    foreach ($remap['errors'] as $ek =>$ev){
                                                        $errors[] =['id_card_number'=>$id_card_number , 'error' => trans('common::application.'.$ek) , 'reason' => $ev];
                                                    }
                                                }

                                                $remap['search_id'] = $search_id ;

                                                Cases::saveCase($remap);
                                                $processed_[]=$id_card_number;
                                                $success++;
                                            }
                                            else {
                                                $result[] = ['id_card_number' => $id_card_number, 'reason' => 'old_case'];
                                                $old_case++;
                                            }
                                        }else{
                                            $result[] = ['id_card_number' =>  $id_card_number, 'reason' => 'invalid_id_card_number'];
                                            $invalid_cards++;
                                        }
                                        $processed[]=$id_card_number;
                                    }
                                }

                                if(sizeof($processed_) > 0 ){
                                    $persons = Person::whereIn('id_card_number',$processed_)->get(
                                        ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number',
                                            'gender', 'birthday','marital_status_id','death_date']);

                                    foreach ($persons as $key=> $person){

                                        $updates = array();
                                        $fields =  ['first_name', 'second_name', 'third_name', 'last_name',
                                            'gender', 'birthday','marital_status_id','death_date'];

                                        foreach ($fields as $field) {
                                            $updates[$field]=$person->$field;
                                        }
                                        Cases::where(['search_id'=>$search_id, 'id_card_number'=>$person->id_card_number])->update($updates);
                                    }
                                }

                                if( ($success == $total)){
                                    return response()->json(['status' => 'success','msg'=> trans('common::application.Successfully imported all cases')]);
                                }
                                else{
                                    $response = [];
                                    if($old_case == $total){
                                        $response['status'] = 'failed';
                                        $response['msg'] =  trans('common::application.All names entered are previously registered');
                                    }

                                    if($success != 0){
                                        $response['status'] = 'success';
                                        $response['msg'] = trans('common::application.Some cases were imported successfully, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.The number of incorrect cards') .' :  ' .$invalid_cards . ' ,  ' .
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of previously registered case names') .' :  ' .$old_case . ' ,  ' .
                                            trans('common::application.Number of repeated cases') .' :  '.$duplicated ;
                                    }
                                    else{
                                        $response['status'] = 'failed';
                                        $response['msg'] = trans('common::application.no cases were imported , as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.The number of incorrect cards') .' :  ' .$invalid_cards . ' ,  ' .
                                            trans('common::application.Number of previously registered case names') .' :  ' .$old_case . ' ,  ' .
                                            trans('common::application.Number of repeated cases') .' :  '.$duplicated;

                                    }

                                    if (sizeof($result) > 0 || sizeof($errors) > 0 || sizeof($processed_) > 0) {
                                        $token = md5(uniqid());
                                        \Excel::create('export_' . $token, function ($excel) use ($result,$processed_,$errors) {
                                            if(sizeof($processed_) > 0){
                                                $excel->sheet(trans('common::application.saved'), function($sheet) use($processed_) {

                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setHeight(1,40);
                                                    $sheet->getDefaultStyle()->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                    ]);

                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));

                                                    $z= 2;
                                                    foreach($processed_ as $v){
                                                        $sheet ->setCellValue('A'.$z,$v);
                                                        $z++;
                                                    }

                                                });
                                            }
                                            if(sizeof($result) > 0){
                                                $excel->sheet(trans('common::application.excluded'), function ($sheet) use ($result) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('B1',trans('common::application.reason'));
                                                    $z= 2;
                                                    foreach($result as $v){
                                                        $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                        $sheet ->setCellValue('B'.$z,trans('common::application.'.$v['reason']));
                                                        $z++;
                                                    }
                                                });

                                            }
                                            if(sizeof($errors) > 0){
                                                $excel->sheet(trans('common::application.data_errors'), function ($sheet) use ($errors) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                       'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('B1',trans('common::application.field_name'));
                                                    $sheet ->setCellValue('C1',trans('common::application.reason'));
                                                    $z= 2;
                                                    foreach($errors as $v){
                                                        $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                        $sheet ->setCellValue('B'.$z,$v['error']);
                                                        $sheet ->setCellValue('C'.$z,$v['reason']);
                                                        $z++;
                                                    }
                                                });

                                            }
                                        })->store('xlsx', storage_path('tmp/'));
                                        $response["download_token"] = $token;
                                        $response["msg"] = $response["msg"] . '  , ' . trans('aid::application.all restricted_cases');
                                    }

                                    return response()->json($response);
                                }

                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.The number of records in the file must not exceed 2000 records') ]);
                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }

                }

                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }

            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }

    /************ IMPORT FAMILY ************/
    public function importFamily(Request $request){

        $this->authorize('import', SocialSearch::class);

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $importFile=$request->file ;

        if ($importFile->isValid()) {

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();

            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){

                        $validFile=true;

                        $constraint =  ["rkm_alhoy_rb_alasr", "rkm_alhoy", "sl_alkrab_rb_alasr", "alasm_alaol_aarby"
                            , "alasm_althany_aarby", "alasm_althalth_aarby", "alaaael_aarby", "asm_alaaael_alsabk", "aljns"
                            , "alhal_alajtmaaay", "tarykh_almylad", "rkm_aljoal"
                            , "rkm_alotny", "alhal_alshy", "almrd_ao_aleaaak"
                            , "tfasyl_alhal_alshy"
                            , "ydrs_ao_mtaalm", "alsf", "asm_almdrs"];

                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {

                                $processed_ =[];
                                $search_cases_map =[];
                                $errors =[];
                                $processed=[];

                                $old_person=0;
                                $duplicated =0;
                                $not_person=0;
                                $invalid_card=0;
                                $invalid_father_card=0;
                                $father_not_enter_on_search = 0;
                                $success =0;
                                $search_id=$request->search_id ;

                                foreach ($records as $key =>$value) {
                                    $id_card_number =(int)$value['rkm_alhoy'];
                                    if(in_array($id_card_number,$processed)){
                                        $result[] = ['id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                                        $duplicated++;
                                    }
                                    else{
                                        $search_cases_card =(int)$value['rkm_alhoy_rb_alasr'];

                                        if(GovServices::checkCard($id_card_number)) {

                                            if(!GovServices::checkCard($search_cases_card)){
                                                $result[] = ['id_card_number' => $id_card_number, 'reason' => 'invalid_father_card'];
                                                $invalid_father_card++;
                                            }else{
                                                $isUnique = CasesFamily::isUnique($id_card_number,$search_id,$search_cases_card);

                                                if($isUnique['status'] == false) {
                                                    $remap = CasesFamily::filterInputs($value);

                                                    if(sizeof($remap['errors']) >0 ){
                                                        foreach ($remap['errors'] as $ek =>$ev){
                                                            $errors[] =['id_card_number'=>$id_card_number , 'error' => trans('common::application.'.$ek) , 'reason' => $ev];
                                                        }
                                                    }

                                                    $remap['search_cases_id'] = $isUnique['search_cases_id'] ;
                                                    CasesFamily::saveMember($remap);
                                                    $processed_[]=$id_card_number;
                                                    $search_cases_map[$id_card_number]=$isUnique['search_cases_id'];
                                                    $success++;
                                                }
                                                else {

                                                    $result[] = ['id_card_number' => $id_card_number, 'reason' => $isUnique['reason']];
                                                    if($isUnique['reason'] == 'father_not_enter_on_search'){
                                                        $father_not_enter_on_search++;
                                                    }else{
                                                        $old_person++;

                                                    }
                                                }

                                            }
                                        }else{
                                            $result[] = ['id_card_number' => $id_card_number, 'reason' => 'invalid_card'];
                                            $invalid_card++;
                                        }
                                        $processed[]=$id_card_number;
                                    }
                                }

//                                CasesFamily::saveMember($remap);
//                                $processed_[]=$id_card_number;
//                                $search_cases_map[$id_card_number]=$isUnique['search_cases_id'];

                                if(sizeof($processed_) > 0 ){
                                    $persons = Person::whereIn('id_card_number',$processed_)->get(
                                        ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number',
                                            'gender', 'birthday','marital_status_id','death_date','prev_family_name']);

                                    foreach ($persons as $key=> $person){

                                        $updates = array();
                                        $fields =  ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number',
                                                    'gender', 'birthday','marital_status_id','prev_family_name'];

                                        $search_cases_id = $search_cases_map[$person->id_card_number];

                                        if(!is_null($person->death_date)){
                                            CasesFamily::where(['search_cases_id'=>$search_cases_id, 'id_card_number'=>$person->id_card_number])->delete();
                                        }else{
                                            foreach ($fields as $field) {
                                                $updates[$field]=$person->$field;
                                            }
                                            CasesFamily::where(['search_cases_id'=>$search_cases_id, 'id_card_number'=>$person->id_card_number])->update($updates);
                                        }
                                    }
                                }

                                if( $success == $total && ($old_person ==0 && $not_person == 0 )){
                                    return response()->json(['status' => 'success','msg'=> trans('common::application.Successfully imported all individuals')]);
                                }
                                else{

                                    if($success != 0){
                                        $response["status"] = 'success';
                                        $response["msg"] =
                                            trans('common::application.Some individuals were imported successfully, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.invalid_father_card') .' :  ' .$invalid_father_card . ' ,  ' .
                                            trans('common::application.Number of father_not_enter_on_search') .' :  ' .$father_not_enter_on_search . ' ,  ' .
                                            trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.Number of previously registered individuals') .' :  ' .$old_person . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }else{
                                        $response["status"] = 'failed';
                                        $response["msg"] =
                                            trans('common::application.No cases were imported, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.invalid_father_card') .' :  ' .$invalid_father_card . ' ,  ' .
                                            trans('common::application.Number of father_not_enter_on_search') .' :  ' .$father_not_enter_on_search . ' ,  ' .
                                            trans('common::application.invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.Number of previously registered individuals') .' :  ' .$old_person . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }

                                    if (sizeof($result) > 0 || sizeof($errors) > 0 || sizeof($processed_) > 0) {
                                        $token = md5(uniqid());
                                        \Excel::create('export_' . $token, function ($excel) use ($result,$processed_,$errors) {

                                            if(sizeof($processed_) > 0){
                                                $excel->sheet(trans('common::application.saved'), function($sheet) use($processed_) {

                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setHeight(1,40);
                                                    $sheet->getDefaultStyle()->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                    ]);

                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));

                                                    $z= 2;
                                                    foreach($processed_ as $v){
                                                        $sheet ->setCellValue('A'.$z,$v);
                                                        $z++;
                                                    }

                                                });
                                            }

                                            if(sizeof($result) > 0){
                                                $excel->sheet(trans('common::application.excluded'), function ($sheet) use ($result) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);
                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('B1',trans('common::application.reason'));
                                                    $z= 2;
                                                    foreach($result as $v){
                                                        $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                        $sheet ->setCellValue('B'.$z,trans('common::application.'.$v['reason']));
                                                        $z++;
                                                    }
                                                });

                                            }

                                            if(sizeof($errors) > 0){
                                                $excel->sheet(trans('common::application.data_errors'), function ($sheet) use ($errors) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                       'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('B1',trans('common::application.field_name'));
                                                    $sheet ->setCellValue('C1',trans('common::application.reason'));
                                                    $z= 2;
                                                    foreach($errors as $v){
                                                        $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                        $sheet ->setCellValue('B'.$z,$v['error']);
                                                        $sheet ->setCellValue('C'.$z,$v['reason']);
                                                        $z++;
                                                    }
                                                });

                                            }

                                        })->store('xlsx', storage_path('tmp/'));

                                        $response["download_token"] = $token;
                                        $response["msg"] = $response["msg"] . '  , ' . trans('aid::application.all restricted_');
                                    }
                                    return response()->json($response);
                                }
                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }

        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }

    /************ IMPORT CASE Bank Accounts ************/
    public function importBanks(Request $request){

        $this->authorize('import', SocialSearch::class);

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $importFile=$request->file ;

        if ($importFile->isValid()) {

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();

            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){

                        $validFile=true;
                        $constraint =  ["rkm_alhoy", "asm_albnk", "alfraa", "rkm_alhsab", "malk_alhsab"];

                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {

                                $processed_ =[];
                                $errors =[];
                                $processed=[];
                                $result =[];

                                $no_data=0;
                                $invalid_card=0;
                                $invalid_data=0;

                                $duplicated =0;

                                $old_person=0;
                                $not_person=0;
                                $success =0;
                                $case_not_enter_on_search =0;
                                $account_number_previously_add_to_you =0;
                                $account_number_owned_by_other =0;
                                $search_id=$request->search_id ;

                                foreach ($records as $key =>$value) {

                                    $id_card_number =(int)$value['rkm_alhoy'];
                                    if(in_array($id_card_number,$processed)){
                                        $result[] = ['id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                                        $duplicated++;
                                    }
                                    else{
                                        if(GovServices::checkCard($id_card_number)) {
                                            $details = ['search_id' => $search_id , 'id_card_number' => $id_card_number];

                                            $bank_id = null;
                                            $account_number = null;
                                            $account_owner = null;
                                            $branch_name = null;


                                            if(isset($value['rkm_alhsab'])) {
                                                if(!(is_null($value['rkm_alhsab']) || $value['rkm_alhsab'] =='' || $value['rkm_alhsab'] ==' ')){
                                                    $account_number = $value['rkm_alhsab'];
                                                }
                                            }

                                            if(!is_null($account_number)){
                                                $details['account_number']=$account_number;

                                                if(isset($value['asm_albnk'])) {
                                                    if(!(is_null($value['asm_albnk']) || $value['asm_albnk'] =='' || $value['asm_albnk'] ==' ')){
                                                        $object= \Setting\Model\Bank::where(['name'=>$value['asm_albnk'] ])->first();
                                                        $bank_id = is_null($object)? null : (int) $object->id;
                                                        if(is_null($bank_id) ){
                                                            $errors[] =['id_card_number'=>$id_card_number , 'error' => trans('common::application.bank_name') , 'reason' => trans('common::application.invalid_constant')];
                                                        }else{
                                                            $details['bank_id']=$bank_id;
                                                            if(isset($value['alfraa'])) {
                                                                if(!(is_null($value['alfraa']) || $value['alfraa'] =='' || $value['alfraa'] ==' ')){
                                                                    $branch_name = \Setting\Model\Branch::getBranchId($bank_id,$value['alfraa']);
                                                                    if( is_null($branch_name) ){
                                                                        $errors[] =['id_card_number'=>$id_card_number , 'error' => trans('common::application.branch_name') , 'reason' => trans('common::application.invalid_constant')];
                                                                    }else{
                                                                        $details['branch_name']=$branch_name;
                                                                    }
                                                                }
                                                            }

                                                            if(isset($value['malk_alhsab'])) {
                                                                if(!(is_null($value['malk_alhsab']) || $value['malk_alhsab'] =='' || $value['malk_alhsab'] ==' ')){
                                                                    $details['account_owner']=$value['malk_alhsab'];
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                if(is_null($bank_id) ) {
                                                    $result[] = ['id_card_number' => $id_card_number, 'reason' => 'invalid_data'];
                                                    $invalid_data++;
                                                }else{
                                                    $saveAccount = CasesBanks::saveAccount($details);
                                                    if($saveAccount['status'] == true ){
                                                        $processed_[]=['id_card_number' => $id_card_number,
                                                            'bank_name' => $value['asm_albnk'],
                                                            'account_number' => $account_number];
                                                        $processed[]=$details;
                                                        $success++;
                                                    }else{
                                                        $result[] = ['id_card_number' => $id_card_number, 'reason' =>$saveAccount['reason']];

                                                        if($saveAccount['reason'] == 'case_not_enter_on_search'){
                                                            $case_not_enter_on_search++;
                                                        }elseif($saveAccount['reason'] == 'account_number_previously_add_to_you'){
                                                            $account_number_previously_add_to_you++;
                                                        }else{
                                                            $account_number_owned_by_other++;
                                                        }
                                                    }

                                                }


                                            }else{
                                                $result[] = ['id_card_number' => $id_card_number, 'reason' => 'no_data'];
                                                $no_data++;
                                            }

                                        }else{
                                            $result[] = ['id_card_number' => $id_card_number, 'reason' => 'invalid_card'];
                                            $invalid_card++;
                                        }
                                        $processed[]=$id_card_number;
                                    }
                                }


                                if( $success == $total && ($old_person ==0 && $not_person == 0 )){
                                    $response["status"] = 'success';
                                    $response["msg"] = trans('common::application.Successfully import');
                                }
                                else{

                                    $response["result"] =$result;
                                    $response["processed_"] =$processed_;
                                    $response["errors"] =$errors;
                                    if($success != 0){
                                        $response["status"] = 'success';
                                        $response["msg"] =
                                            trans('common::application.Some individuals were imported successfully, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.Number of case_not_enter_on_search') .' :  ' .$case_not_enter_on_search . ' ,  ' .
                                            trans('common::application.Number of account_number_previously_add_to_you') .' :  ' .$account_number_previously_add_to_you . ' ,  ' .
                                            trans('common::application.Number of account_number_owned_by_other') .' :  ' .$account_number_owned_by_other . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }else{
                                        $response["status"] = 'failed';
                                        $response["msg"] =
                                            trans('common::application.No cases were imported, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.Number of case_not_enter_on_search') .' :  ' .$case_not_enter_on_search . ' ,  ' .
                                            trans('common::application.Number of account_number_previously_add_to_you') .' :  ' .$account_number_previously_add_to_you . ' ,  ' .
                                            trans('common::application.Number of account_number_owned_by_other') .' :  ' .$account_number_owned_by_other . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }
                                }

                                if (sizeof($result) > 0 || sizeof($errors) > 0 || sizeof($processed_) > 0) {
                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function ($excel) use ($result,$processed_,$errors) {

                                        if(sizeof($processed_) > 0){
                                            $excel->sheet(trans('common::application.saved'), function($sheet) use($processed_) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);


                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.bank_name'));
                                                $sheet ->setCellValue('C1',trans('common::application.account_number'));

                                                $z= 2;

                                                foreach($processed_ as $key=>$v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,$v['bank_name']);
                                                    $sheet ->setCellValue('C'.$z,$v['account_number']);
                                                    $z++;
                                                }

                                            });
                                        }

                                        if(sizeof($result) > 0){
                                            $excel->sheet(trans('common::application.excluded'), function ($sheet) use ($result) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.reason'));
                                                $z= 2;
                                                foreach($result as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,trans('common::application.'.$v['reason']));
                                                    $z++;
                                                }
                                            });

                                        }

                                        if(sizeof($errors) > 0){
                                            $excel->sheet(trans('common::application.data_errors'), function ($sheet) use ($errors) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.field_name'));
                                                $sheet ->setCellValue('C1',trans('common::application.reason'));
                                                $z= 2;
                                                foreach($errors as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,$v['error']);
                                                    $sheet ->setCellValue('C'.$z,$v['reason']);
                                                    $z++;
                                                }
                                            });

                                        }

                                    })->store('xlsx', storage_path('tmp/'));

                                    $response["download_token"] = $token;
                                    $response["msg"] = $response["msg"] . '  , ' . trans('common::application.account restricted');
                                }
                                return response()->json($response);
                            }

                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);

                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }

    /************ IMPORT CASE Essentials OF cases  ************/
    public function importEssentials(Request $request){

        $this->authorize('import', SocialSearch::class);

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $importFile=$request->file ;


        if ($importFile->isValid()) {

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
//            $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
//            $objReader->setReadDataOnly(true);
//            $excel = $objReader->load($path);
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){

                        $validFile=true;
                        $constraint =  ["rkm_alhoy"];

                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if($validFile){

                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {

                                $result =[];
                                $processed_ =[];
                                $errors =[];
                                $processed=[];
                                $invalid_card=0;
                                $duplicated =0;
                                $old_person=0;
                                $not_person=0;
                                $success =0;
                                $no_data = 0;
                                $case_not_enter_on_search =0;
                                $search_id=$request->search_id ;

                                $coordinate =[];

                                $ActiveSheet = array_search('data',$sheets,true);
                                $excel->setActiveSheetIndex($ActiveSheet);
                                $sheetObj = $excel->getActiveSheet();
                                foreach( $sheetObj->getRowIterator(0,1) as $row ){
                                    $cellIterator = $row->getCellIterator();
                                    $cellIterator->setIterateOnlyExistingCells(false);
                                    foreach ($cellIterator as $cell) {
                                        $val = $cell->getValue();
                                        $col = $cell->getColumn();
                                        if($col != 'A'){
                                            if(!(is_null($val) || $val ==' '|| $val =='')){
                                                $entry = Essential::where('name',$val)->first();
                                                if(is_null($entry)){
                                                    $errors[] =['id_card_number'=>' ' , 'error' => $val , 'reason' => trans('common::application.invalid_constant')];
                                                }else{
                                                    $colIndex = \PHPExcel_Cell::columnIndexFromString($col);
                                                    $adjustedColumnIndex = $colIndex;
                                                    $adjustedColumn = \PHPExcel_Cell::stringFromColumnIndex($adjustedColumnIndex);
                                                    $coordinate[$col]=['name'=>$cell->getValue(),'next'=> $adjustedColumn ,'id' => $entry->id];
                                                }
                                            }
                                        }
                                    }
                                }
                                $inc = 3;
                                foreach ($records as $key =>$value) {
                                    $id_card_number =(int)$value['rkm_alhoy'];
                                    if(in_array($id_card_number,$processed)){
                                        $result[] = ['id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                                        $duplicated++;
                                    }
                                    else{
                                        if(GovServices::checkCard($id_card_number)) {
                                            $case = Cases::where(['search_id' => $search_id , 'id_card_number' => $id_card_number])->first();

                                            if(!is_null($case)){
                                                $essentials = [];

                                                foreach ($coordinate as $ix => $det ) {
                                                    $CEL = $ix.$inc;
                                                    $condition = Null;
                                                    $condition_ = $sheetObj->getCell($CEL)->getValue();
                                                    if(!(is_null($condition_) || $condition_ ==' '|| $condition_ =='')) {
                                                        $condition = 0;
                                                        if($condition_ == 'ممتاز'){
                                                            $condition = 5;
                                                        }elseif ($condition_ == 'جيد جدا'){
                                                            $condition = 4;
                                                        }elseif ($condition_ == 'جيد'){
                                                            $condition = 3;
                                                        }elseif ($condition_ == 'سيء'){
                                                            $condition = 2;
                                                        }elseif ($condition_ == 'سيء جدا'){
                                                            $condition = 1;
                                                        }

                                                    }

                                                    $CEL_1 = $det['next'].$inc;
                                                    $needs = 0;
                                                    $needs_ = $sheetObj->getCell($CEL_1)->getValue();
                                                    if(!(is_null($needs_) || $needs_ ==' '|| $needs_ =='')) {
                                                        $needs = (float)$needs_;
                                                    }

                                                    $essentials[]=[
                                                        'search_cases_id'=>$case->id ,
                                                        'essential_id'=>$det['id'] ,
                                                        'condition'=> $condition ,
                                                        'needs'=> $needs
                                                    ];
                                                }

                                                if(sizeof($essentials)> 0 ){
                                                    CasesEssentials::where(['search_cases_id' => $case->id ])->delete();
                                                    CasesEssentials::insert($essentials);
                                                    $processed_[]=['id_card_number' => $id_card_number];
                                                    $success++;
                                                }else{
                                                    $result[] = ['id_card_number' => $id_card_number, 'reason' =>'invalid_data'];
                                                    $invalid_data++;
                                                }

                                            }else{
                                                $result[] = ['id_card_number' => $id_card_number, 'reason' =>'case_not_enter_on_search'];
                                                $case_not_enter_on_search++;
                                            }

                                        }else{
                                            $result[] = ['id_card_number' => $id_card_number, 'reason' => 'invalid_card'];
                                            $invalid_card++;
                                        }
                                        $processed[]=$id_card_number;
                                    }
                                }


                                if( $success == $total && ($old_person ==0 && $not_person == 0 )){
                                    $response["status"] = 'success';
                                    $response["msg"] = trans('common::application.Successfully import');
                                }
                                else{
                                    if($success != 0){
                                        $response["status"] = 'success';
                                        $response["msg"] =
                                            trans('common::application.Some individuals were imported successfully, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.number of case that not has data') .' :  ' .$no_data . ' ,  ' .
                                            trans('common::application.Number of case_not_enter_on_search') .' :  ' .$case_not_enter_on_search . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }
                                    else{
                                        $response["status"] = 'failed';
                                        $response["msg"] =
                                            trans('common::application.No cases were imported, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.number of case that not has data') .' :  ' .$no_data . ' ,  ' .
                                            trans('common::application.Number of case_not_enter_on_search') .' :  ' .$case_not_enter_on_search . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }
                                }

                                if (sizeof($result) > 0 || sizeof($errors) > 0 || sizeof($processed_) > 0) {
                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function ($excel) use ($result,$processed_,$errors) {

                                        if(sizeof($processed_) > 0){
                                            $excel->sheet(trans('common::application.saved'), function($sheet) use($processed_) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);


                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;

                                                foreach($processed_ as $key=>$v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $z++;
                                                }

                                            });
                                        }

                                        if(sizeof($result) > 0){
                                            $excel->sheet(trans('common::application.excluded'), function ($sheet) use ($result) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                   'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.reason'));
                                                $z= 2;
                                                foreach($result as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,trans('common::application.'.$v['reason']));
                                                    $z++;
                                                }
                                            });

                                        }

                                        if(sizeof($errors) > 0){
                                            $excel->sheet(trans('common::application.data_errors'), function ($sheet) use ($errors) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                   'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.field_name'));
                                                $sheet ->setCellValue('C1',trans('common::application.reason'));
                                                $z= 2;
                                                foreach($errors as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,$v['error']);
                                                    $sheet ->setCellValue('C'.$z,$v['reason']);
                                                    $z++;
                                                }
                                            });

                                        }

                                    })->store('xlsx', storage_path('tmp/'));

                                    $response["download_token"] = $token;
                                    $response["msg"] = $response["msg"] . '  , ' . trans('common::application.data restricted');
                                }
                                return response()->json($response);
                            }

                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);

                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }

    /************ IMPORT CASE Aids Source OF cases ************/
    public function importFinAids(Request $request){

        $this->authorize('import', SocialSearch::class);

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $importFile=$request->file ;


        if ($importFile->isValid()) {

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
//            $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
//            $objReader->setReadDataOnly(true);
//            $excel = $objReader->load($path);
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){

                        $validFile=true;
                        $constraint =  ["rkm_alhoy"];

                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if($validFile){

                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {

                                $processed_ =[];
                                $result =[];
                                $errors =[];
                                $processed=[];
                                $invalid_card=0;
                                $duplicated =0;
                                $old_person=0;
                                $invalid_data=0;
                                $not_person=0;
                                $success =0;
                                $no_data = 0;
                                $case_not_enter_on_search =0;
                                $search_id=$request->search_id ;


                                $coordinate =[];

                                $ActiveSheet = array_search('data',$sheets,true);
                                $excel->setActiveSheetIndex($ActiveSheet);
                                $sheetObj = $excel->getActiveSheet();
                                foreach( $sheetObj->getRowIterator(0,1) as $row ){
                                    $cellIterator = $row->getCellIterator();
                                    $cellIterator->setIterateOnlyExistingCells(false);
                                    foreach ($cellIterator as $cell) {
                                        $val = $cell->getValue();
                                        $col = $cell->getColumn();
                                        if($col != 'A'){
                                            if(!(is_null($val) || $val ==' '|| $val =='')){
                                                $entry = AidSource::where('name',$val)->first();

                                                if(is_null($entry)){
                                                    $errors[] =['id_card_number'=>' ' , 'error' => $val , 'reason' => trans('common::application.invalid_constant')];
                                                }else{
                                                    $coordinate[$col]=['name'=>$cell->getValue(),'id' => $entry->id];
                                                }
                                            }
                                        }
                                    }
                                }

                                $inc = 2;
                                foreach ($records as $key =>$value) {
                                    $id_card_number =(int)$value['rkm_alhoy'];
                                    if(in_array($id_card_number,$processed)){
                                        $result[] = ['id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                                        $duplicated++;
                                    }
                                    else{
                                        if(GovServices::checkCard($id_card_number)) {

                                            $case = Cases::where(['search_id' => $search_id , 'id_card_number' => $id_card_number])->first();

                                            if(!is_null($case)){
                                                $aid_type = 1;
                                                $aids = [];

                                                foreach ($coordinate as $ix => $det ) {
                                                    $CEL = $ix.$inc;
                                                    $val_ = $sheetObj->getCell($CEL)->getValue();
                                                    $needs = 0;
                                                    $aid_take = 0;
                                                    if(!(is_null($val_) || $val_ ==' '|| $val_ =='')) {
                                                        $needs = (float)$val_;
                                                        if($needs > 0){
                                                            $aid_take = 1;
                                                        }
                                                    }
                                                    
                                                    
                                                    $aids[]=[
                                                        'search_cases_id'=>$case->id ,
                                                        'aid_source_id'=>$det['id'] ,
                                                        'aid_take'=> $aid_take,
                                                        'aid_value'=> $needs,
                                                        'aid_type'=> $aid_type,
                                                        'currency_id'=>4
                                                    ];
                                                }

                                                if(sizeof($aids)> 0 ){
                                                    CasesAids::where(['search_cases_id' => $case->id ,'aid_type'=>$aid_type])->delete();
                                                    CasesAids::insert($aids);
                                                    $processed_[]=['id_card_number' => $id_card_number];
                                                    $success++;
                                                }else{
                                                    $result[] = ['id_card_number' => $id_card_number, 'reason' =>'invalid_data'];
                                                    $invalid_data++;
                                                }

                                            }else{
                                                $result[] = ['id_card_number' => $id_card_number, 'reason' =>'case_not_enter_on_search'];
                                                $case_not_enter_on_search++;
                                            }

                                        }else{
                                            $result[] = ['id_card_number' => $id_card_number, 'reason' => 'invalid_card'];
                                            $invalid_card++;
                                        }
                                        $processed[]=$id_card_number;
                                    }
                                }


                                if( $success == $total && ($old_person ==0 && $not_person == 0 )){
                                    $response["status"] = 'success';
                                    $response["msg"] = trans('common::application.Successfully import');
                                }
                                else{
                                    if($success != 0){
                                        $response["status"] = 'success';
                                        $response["msg"] =
                                            trans('common::application.Some individuals were imported successfully, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.number of case that not has data') .' :  ' .$no_data . ' ,  ' .
                                            trans('common::application.Number of case_not_enter_on_search') .' :  ' .$case_not_enter_on_search . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }
                                    else{
                                        $response["status"] = 'failed';
                                        $response["msg"] =
                                            trans('common::application.No cases were imported, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.number of case that not has data') .' :  ' .$no_data . ' ,  ' .
                                            trans('common::application.Number of case_not_enter_on_search') .' :  ' .$case_not_enter_on_search . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }
                                }

                                if (sizeof($result) > 0 || sizeof($errors) > 0 || sizeof($processed_) > 0) {
                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function ($excel) use ($result,$processed_,$errors) {

                                        if(sizeof($processed_) > 0){
                                            $excel->sheet(trans('common::application.saved'), function($sheet) use($processed_) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);


                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;

                                                foreach($processed_ as $key=>$v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $z++;
                                                }

                                            });
                                        }

                                        if(sizeof($result) > 0){
                                            $excel->sheet(trans('common::application.excluded'), function ($sheet) use ($result) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.reason'));
                                                $z= 2;
                                                foreach($result as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,trans('common::application.'.$v['reason']));
                                                    $z++;
                                                }
                                            });

                                        }

                                        if(sizeof($errors) > 0){
                                            $excel->sheet(trans('common::application.data_errors'), function ($sheet) use ($errors) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.field_name'));
                                                $sheet ->setCellValue('C1',trans('common::application.reason'));
                                                $z= 2;
                                                foreach($errors as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,$v['error']);
                                                    $sheet ->setCellValue('C'.$z,$v['reason']);
                                                    $z++;
                                                }
                                            });

                                        }

                                    })->store('xlsx', storage_path('tmp/'));

                                    $response["download_token"] = $token;
                                    $response["msg"] = $response["msg"] . '  , ' . trans('common::application.data restricted');
                                }
                                return response()->json($response);
                            }

                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);

                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }
    public function importNonFinAids(Request $request){

        $this->authorize('import', SocialSearch::class);

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $importFile=$request->file ;


        if ($importFile->isValid()) {

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
//            $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
//            $objReader->setReadDataOnly(true);
//            $excel = $objReader->load($path);
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){

                        $validFile=true;
                        $constraint =  ["rkm_alhoy"];

                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if($validFile){

                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {

                                $processed_ =[];
                                $errors =[];
                                $processed=[];
                                $invalid_card=0;
                                $invalid_data=0;
                                $result =[];
                                $duplicated =0;
                                $old_person=0;
                                $not_person=0;
                                $success =0;
                                $no_data = 0;
                                $case_not_enter_on_search =0;
                                $search_id=$request->search_id ;


                                $coordinate =[];

                                $ActiveSheet = array_search('data',$sheets,true);
                                $excel->setActiveSheetIndex($ActiveSheet);
                                $sheetObj = $excel->getActiveSheet();
                                foreach( $sheetObj->getRowIterator(0,1) as $row ){
                                    $cellIterator = $row->getCellIterator();
                                    $cellIterator->setIterateOnlyExistingCells(false);
                                    foreach ($cellIterator as $cell) {
                                        $val = $cell->getValue();
                                        $col = $cell->getColumn();
                                        if($col != 'A'){
                                            if(!(is_null($val) || $val ==' '|| $val =='')){
                                                $entry = AidSource::where('name',$val)->first();

                                                if(is_null($entry)){
                                                    $errors[] =['id_card_number'=>' ' , 'error' => $val , 'reason' => trans('common::application.invalid_constant')];
                                                }else{
                                                    $coordinate[$col]=['name'=>$cell->getValue(),'id' => $entry->id];
                                                }
                                            }
                                        }
                                    }
                                }

                                $inc = 2;
                                foreach ($records as $key =>$value) {
                                    $id_card_number =(int)$value['rkm_alhoy'];
                                    if(in_array($id_card_number,$processed)){
                                        $result[] = ['id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                                        $duplicated++;
                                    }
                                    else{
                                        if(GovServices::checkCard($id_card_number)) {

                                            $case = Cases::where(['search_id' => $search_id , 'id_card_number' => $id_card_number])->first();

                                            if(!is_null($case)){
                                                $aid_type = 2;
                                                $aids = [];

                                                foreach ($coordinate as $ix => $det ) {
                                                    $CEL = $ix.$inc;
                                                    $val_ = $sheetObj->getCell($CEL)->getValue();
                                                    $needs = 0;
                                                    $aid_take = 0;
                                                    if(!(is_null($val_) || $val_ ==' '|| $val_ =='')) {
                                                        $needs = (float)$val_;
                                                        if($needs > 0){
                                                            $aid_take = 1;
                                                        }
                                                    }
                                                    $aids[]=[
                                                        'search_cases_id'=>$case->id ,
                                                        'aid_source_id'=>$det['id'] ,
                                                        'aid_take'=> $aid_take,
                                                        'aid_value'=> $needs,
                                                        'aid_type'=> $aid_type,
                                                        'currency_id'=>4
                                                    ];
                                                }

                                                if(sizeof($aids)> 0 ){
                                                    CasesAids::where(['search_cases_id' => $case->id ,'aid_type'=>$aid_type])->delete();
                                                    CasesAids::insert($aids);
                                                    $processed_[]=['id_card_number' => $id_card_number];
                                                    $success++;
                                                }else{
                                                    $result[] = ['id_card_number' => $id_card_number, 'reason' =>'invalid_data'];
                                                    $invalid_data++;
                                                }

                                            }else{
                                                $result[] = ['id_card_number' => $id_card_number, 'reason' =>'case_not_enter_on_search'];
                                                $case_not_enter_on_search++;
                                            }

                                        }else{
                                            $result[] = ['id_card_number' => $id_card_number, 'reason' => 'invalid_card'];
                                            $invalid_card++;
                                        }
                                        $processed[]=$id_card_number;
                                    }
                                }


                                if( $success == $total && ($old_person ==0 && $not_person == 0 )){
                                    $response["status"] = 'success';
                                    $response["msg"] = trans('common::application.Successfully import');
                                }
                                else{
                                    if($success != 0){
                                        $response["status"] = 'success';
                                        $response["msg"] =
                                            trans('common::application.Some individuals were imported successfully, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.number of case that not has data') .' :  ' .$no_data . ' ,  ' .
                                            trans('common::application.Number of case_not_enter_on_search') .' :  ' .$case_not_enter_on_search . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }
                                    else{
                                        $response["status"] = 'failed';
                                        $response["msg"] =
                                            trans('common::application.No cases were imported, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.number of case that not has data') .' :  ' .$no_data . ' ,  ' .
                                            trans('common::application.Number of case_not_enter_on_search') .' :  ' .$case_not_enter_on_search . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }
                                }

                                if (sizeof($result) > 0 || sizeof($errors) > 0 || sizeof($processed_) > 0) {
                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function ($excel) use ($result,$processed_,$errors) {

                                        if(sizeof($processed_) > 0){
                                            $excel->sheet(trans('common::application.saved'), function($sheet) use($processed_) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);


                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;

                                                foreach($processed_ as $key=>$v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $z++;
                                                }

                                            });
                                        }

                                        if(sizeof($result) > 0){
                                            $excel->sheet(trans('common::application.excluded'), function ($sheet) use ($result) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.reason'));
                                                $z= 2;
                                                foreach($result as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,trans('common::application.'.$v['reason']));
                                                    $z++;
                                                }
                                            });

                                        }

                                        if(sizeof($errors) > 0){
                                            $excel->sheet(trans('common::application.data_errors'), function ($sheet) use ($errors) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.field_name'));
                                                $sheet ->setCellValue('C1',trans('common::application.reason'));
                                                $z= 2;
                                                foreach($errors as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,$v['error']);
                                                    $sheet ->setCellValue('C'.$z,$v['reason']);
                                                    $z++;
                                                }
                                            });

                                        }

                                    })->store('xlsx', storage_path('tmp/'));

                                    $response["download_token"] = $token;
                                    $response["msg"] = $response["msg"] . '  , ' . trans('common::application.data restricted');
                                }
                                return response()->json($response);
                            }

                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);

                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }

    /************ IMPORT CASE Properties OF cases ************/
    public function importProperties(Request $request){

        $this->authorize('import', SocialSearch::class);

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $importFile=$request->file ;


        if ($importFile->isValid()) {

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
//            $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
//            $objReader->setReadDataOnly(true);
//            $excel = $objReader->load($path);
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){

                        $validFile=true;
                        $constraint =  ["rkm_alhoy"];

                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if($validFile){

                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {

                                $processed_ =[];
                                $errors =[];
                                $processed=[];
                                $invalid_card=0;
                                $duplicated =0;
                                $old_person=0;
                                $not_person=0;
                                $success =0;
                                $no_data = 0;
                                $case_not_enter_on_search =0;
                                $result =[];
                                $account_number_previously_add_to_you =0;
                                $account_number_owned_by_other =0;
                                $search_id=$request->search_id ;


                                $coordinate =[];

                                $ActiveSheet = array_search('data',$sheets,true);
                                $excel->setActiveSheetIndex($ActiveSheet);
                                $sheetObj = $excel->getActiveSheet();
                                foreach( $sheetObj->getRowIterator(0,1) as $row ){
                                    $cellIterator = $row->getCellIterator();
                                    $cellIterator->setIterateOnlyExistingCells(false);
                                    foreach ($cellIterator as $cell) {
                                        $val = $cell->getValue();
                                        $col = $cell->getColumn();
                                        if($col != 'A'){
                                            if(!(is_null($val) || $val ==' '|| $val =='')){
                                                $entry = PropertyI18n::where(['name'=>$val,'language_id'=>1])->first();

                                                if(is_null($entry)){
                                                    $errors[] =['id_card_number'=>' ' , 'error' => $val , 'reason' => trans('common::application.invalid_constant')];
                                                }else{
                                                    $coordinate[$col]=['name'=>$cell->getValue(),'id' => $entry->property_id];
                                                }
                                            }
                                        }
                                    }
                                }

                                $inc = 2;
                                foreach ($records as $key =>$value) {
                                    $id_card_number =(int)$value['rkm_alhoy'];
                                    if(in_array($id_card_number,$processed)){
                                        $result[] = ['id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                                        $duplicated++;
                                    }
                                    else{
                                        if(GovServices::checkCard($id_card_number)) {

                                            $case = Cases::where(['search_id' => $search_id , 'id_card_number' => $id_card_number])->first();

                                            if(!is_null($case)){
                                                $properties = [];

                                                foreach ($coordinate as $ix => $det ) {
                                                    $CEL = $ix.$inc;
                                                    $val_ = $sheetObj->getCell($CEL)->getValue();
                                                    $has_property = 0;
                                                    $quantity = 0;
                                                    if(!(is_null($val_) || $val_ ==' '|| $val_ =='')) {
                                                        $quantity = (float)$val_;
                                                    }
                                                    if($quantity >0 ){
                                                        $has_property = 1;
                                                    }
                                                    $properties[]=[
                                                        'search_cases_id'=>$case->id ,
                                                        'property_id'=>$det['id'] ,
                                                        'has_property'=> $has_property,
                                                        'quantity'=> $quantity
                                                    ];
                                                }

                                                if(sizeof($properties)> 0 ){
                                                    CasesProperties::where(['search_cases_id' => $case->id ])->delete();
                                                    CasesProperties::insert($properties);
                                                    $processed_[]=['id_card_number' => $id_card_number];
                                                    $success++;
                                                }else{
                                                    $result[] = ['id_card_number' => $id_card_number, 'reason' =>'invalid_data'];
                                                    $invalid_data++;
                                                }

                                            }else{
                                                $result[] = ['id_card_number' => $id_card_number, 'reason' =>'case_not_enter_on_search'];
                                                $case_not_enter_on_search++;
                                            }

                                        }else{
                                            $result[] = ['id_card_number' => $id_card_number, 'reason' => 'invalid_card'];
                                            $invalid_card++;
                                        }
                                        $processed[]=$id_card_number;
                                    }
                                    $inc++;
                                }


                                if( $success == $total && ($old_person ==0 && $not_person == 0 )){
                                    $response["status"] = 'success';
                                    $response["msg"] = trans('common::application.Successfully import');
                                }
                                else{
                                    if($success != 0){
                                        $response["status"] = 'success';
                                        $response["msg"] =
                                            trans('common::application.Some individuals were imported successfully, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.number of case that not has data') .' :  ' .$no_data . ' ,  ' .
                                            trans('common::application.Number of case_not_enter_on_search') .' :  ' .$case_not_enter_on_search . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }
                                    else{
                                        $response["status"] = 'failed';
                                        $response["msg"] =
                                            trans('common::application.No cases were imported, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.number of case that not has data') .' :  ' .$no_data . ' ,  ' .
                                            trans('common::application.Number of case_not_enter_on_search') .' :  ' .$case_not_enter_on_search . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }
                                }

                                if (sizeof($result) > 0 || sizeof($errors) > 0 || sizeof($processed_) > 0) {
                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function ($excel) use ($result,$processed_,$errors) {

                                        if(sizeof($processed_) > 0){
                                            $excel->sheet(trans('common::application.saved'), function($sheet) use($processed_) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);


                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;

                                                foreach($processed_ as $key=>$v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $z++;
                                                }

                                            });
                                        }

                                        if(sizeof($result) > 0){
                                            $excel->sheet(trans('common::application.excluded'), function ($sheet) use ($result) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.reason'));
                                                $z= 2;
                                                foreach($result as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,trans('common::application.'.$v['reason']));
                                                    $z++;
                                                }
                                            });

                                        }

                                        if(sizeof($errors) > 0){
                                            $excel->sheet(trans('common::application.data_errors'), function ($sheet) use ($errors) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                   'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.field_name'));
                                                $sheet ->setCellValue('C1',trans('common::application.reason'));
                                                $z= 2;
                                                foreach($errors as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,$v['error']);
                                                    $sheet ->setCellValue('C'.$z,$v['reason']);
                                                    $z++;
                                                }
                                            });

                                        }

                                    })->store('xlsx', storage_path('tmp/'));

                                    $response["download_token"] = $token;
                                    $response["msg"] = $response["msg"] . '  , ' . trans('common::application.data restricted');
                                }
                                return response()->json($response);
                            }

                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);

                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }


    /************ export cases report ************/
    public function exportGroup(Request $request){
        $this->authorize('manageCases', SocialSearch::class);

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $user = \Auth::user();
        $organization_id = $user->organization_id;

        $items = Cases::filter($request->all());
        $items = Cases::exportGroup($request->reports,$request->action);

        if($request->get('action') =='ExportToExcel') {
            if($request->get('deleted') == true){
                $statistic=$items;
            }else{
                $statistic=$items['statistic'];
                $aid_source=$items['aid_source'];
                $properties=$items['properties'];
                $essentials=$items['essentials'];
            }

            $data=[];
            if(sizeof($statistic) !=0){
                foreach($statistic as $key =>$value){
                    $n=0;
                    $data[$key][trans('aid::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        if(!in_array($k,['id','case_id','person_id','data','elements','financial_aid_source_elements','financial_aid_source',
                            'non_financial_aid_source_elements','non_financial_aid_source',
                            'persons_properties_elements','persons_properties','persons_essentials_elements','persons_essentials'
                        ])){

                            if($v == null){
                                $v=' ';
                            }
                            $data[$key][trans('aid::application.' . $k)]= str_replace("-"," ",$v) ;
                            $n++;
                        }
                    }
                    foreach($aid_source as $k1_ =>$v2_){
                        $cuVal='0';
                        if(in_array($v2_->id,$value->financial_aid_source_elements)){
                            $cuVal=$value->financial_aid_source[$v2_->id];
                        }
                        $data[$key][$v2_->name . '(' . trans('common::application.financial'). ')']= $cuVal; ;
                    }
                    foreach($aid_source as $k1_ =>$v2_){
                        $cuVal='0';
                        if(in_array($v2_->id,$value->non_financial_aid_source_elements)){
                            $cuVal=$value->non_financial_aid_source[$v2_->id];
                        }
                        $data[$key][$v2_->name . ' (' . trans('common::application.non_financial'). ') ']= $cuVal; ;
                    }
                    foreach($properties as $k1_0 =>$v2_0){
                        $cuVal='0';
                        if(in_array($v2_0->id,$value->persons_properties_elements)){
                            $cuVal=$value->persons_properties[$v2_0->id];
                        }
                        $data[$key][$v2_0->name]= $cuVal; ;
                    }
                    foreach($essentials as $k1_0 =>$v2_0){
                        $cuVal='0';
                        if(in_array($v2_0->id,$value->persons_essentials_elements)){
                            $cuVal=$value->persons_essentials[$v2_0->id];
                        }
                        $data[$key][$v2_0->name]= $cuVal; ;
                    }
                }

                $n +=sizeof($aid_source);
                $n +=sizeof($aid_source);
                $n +=sizeof($properties);
                $n +=sizeof($essentials);
                for($r = ""; $n >= 0; $n = intval($n / 26) - 1)
                    $r = chr($n%26 + 0x41) . $r;

                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($data,$r) {
                    $excel->sheet('sheet', function($sheet) use($data,$r) {

                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $sheet->setHeight(1,30);

                        $sheet->getStyle("A1:".$r."1")->applyFromArray( [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold'      =>  true
                            ]
                        ]);
                        $sheet->getDefaultStyle()->applyFromArray( [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold' => false
                            ]
                        ]);
                        $sheet->fromArray($data);
                    });
                })->store('xlsx', storage_path('tmp/'));

                \Log\Model\Log::saveNewLog('SOCIAL_SEARCH_CASE_EXPORTED',trans('common::application.Exported a list of aid cases'));
                return response()->json(['download_token' => $token]);
            }

        }
        else if($request->get('action') =='FamilyMember') {

            $keys=["father_id_card_number", "father_name", "id_card_number", "name", "gender", "birthday",
                "marital_status","grade", "school" , "currently_study", "health_status" ,
                "diseases_name" , "diseases_details" , "kinship_name" ];


            $data=[];
            if(sizeof($items) !=0){
                foreach($items as $key =>$value){
                    $data[$key][trans('sponsorship::application.#')]=$key+1;
                    foreach($keys as $ke){
                        $f_ke=null;
                        $vlu = is_null($value->$ke) ? '__' : $value->$ke;
                        if($ke === "kinship_name"){
                            $f_ke = trans('sponsorship::application.kinship_name')." ( ".trans('sponsorship::application.family_res')  ." ) ";
                        }else{
                            $f_ke = trans('sponsorship::application.' . $ke);
                        }
                        $data[$key][$f_ke]= $vlu;
                    }
                }

                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($data) {
                    $excel->sheet(trans('common::application.Detection of family members - brothers') , function($sheet) use($data) {

                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $sheet->setHeight(1,30);
                        $r='W';

                        $sheet->getStyle("A1:".$r."1")->applyFromArray( [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold'      =>  true
                            ]
                        ]);
                        $sheet->getDefaultStyle()->applyFromArray( [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold'      =>  false
                            ]
                        ]);
                        $sheet->fromArray($data);
                    });
                })->store('xlsx', storage_path('tmp/'));

                \Log\Model\Log::saveNewLog('SOCIAL_SEARCH_CASE_EXPORTED',trans('common::application.Exported a list of aid cases'));
                return response()->json(['download_token' => $token,'organization_id'=>$organization_id]);
            }

        }
        else if($request->get('action') =='pdf') {

            $dirName = base_path('storage/app/templates/CaseForm');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }
            foreach($items as $k=>$item){

                $html='';
//        PDF::SetTitle($item->full_name .' '.$item->search_name);
                PDF::SetFont('aealarabiya', '', 18);
                PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);

                PDF::SetMargins(PDF_MARGIN_LEFT, 20, PDF_MARGIN_RIGHT);
//        PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
//        PDF::SetFooterMargin(PDF_MARGIN_FOOTER);
                PDF::SetAutoPageBreak(TRUE);
                $lg = Array();
                $lg['a_meta_charset'] = 'UTF-8';
                $lg['a_meta_dir'] = 'rtl';
                $lg['a_meta_language'] = 'fa';
                $lg['w_page'] = 'page';
                PDF::setLanguageArray($lg);
                PDF::setCellHeightRatio(1.5);

                $date = date('Y-m-d');

                $organization_name = $item->organization_name;
                $search_date = $item->date;
                $category_name = $item->category_name;
                $first_name = $item->first_name;
                $second_name = $item->second_name;
                $third_name = $item->third_name ;
                $last_name = $item->last_name ;
                $name = $item->full_name;

                $id_card_number = $item->id_card_number;
                $birthday = $item->birthday;
                $age = $item->age;
                $gender = $item->gender;
                $gender_male = $item->gender_male;
                $gender_female = $item->gender_female;
                $deserted = $item->gender;
                $is_deserted = $item->is_deserted;
                $not_deserted = $item->not_deserted;
                $birth_place = $item->birth_place;
                $nationality = $item->nationality;
                $refugee = $item->refugee;

                $is_refugee = $item->is_refugee;
                $per_refugee = $item->per_refugee;
                $per_citizen = $item->per_citizen;
                $unrwa_card_number = $item->unrwa_card_number;

                $marital_status_name = $item->marital_status_name;

                $monthly_income = $item->monthly_income;

                $address = $item->address;

                $phone = $item->phone;
                $primary_mobile = $item->primary_mobile;
                $wataniya = $item->watanya;
                $secondary_mobile = $item->secondary_mobile ;

                $property_types = $item->property_types;
                $rent_value = $item->rent_value;
                $roof_materials = $item->roof_materials;
                $habitable = $item->habitable;
                $house_condition = $item->house_condition;
                $residence_condition = $item->residence_condition;
                $indoor_condition = $item->indoor_condition;
                $rooms = $item->rooms;
                $area = $item->area;

                $working = $item->working;
                $works = $item->works;
                $is_working = $item->is_working;
                $not_working = $item->not_working;
                $can_work = $item->can_work;
                $can_works = $item->can_works;
                $can_working = $item->can_working;
                $can_not_working = $item->can_not_working;
                $work_job = $item->work_job;
                $work_reason = $item->work_reason;
                $work_status = $item->work_status;
                $work_wage = $item->work_wage;
                $work_location = $item->work_location;

                $needs = $item->needs;
                $promised = $item->promised;
                $if_promised = $item->if_promised;
                $was_promised = $item->was_promised;
                $not_promised = $item->not_promised;
                $reconstructions_organization_name = $item->reconstructions_organization_name;

                $visitor = $item->visitor;
                $notes = $item->notes;
                $visited_at = $item->visited_at;

                $health_status = $item->health_status;
                $good_health_status = $item->good_health_status;
                $chronic_disease = $item->chronic_disease;
                $disabled = $item->disabled;
                $health_details = $item->health_details;
                $diseases_name = $item->diseases_name;

                $visitor_note = $item->visitor_note;
                $deserted = $item->deserted;
                $family_cnt = $item->family_cnt;
                $spouses = $item->spouses;
                $male_live = $item->male_live;
                $female_live = $item->female_live;


                $is_qualified = $item->is_qualified;
                $qualified_card_number = $item->qualified_card_number;


                $district = $item->governarate;
                $region_name = $item->region_name;
                $nearLocation = $item->nearLocation;
                $square_name = $item->square_name;
                $mosque_name = $item->mosque_name;

                $actual_monthly_income = $item->actual_monthly_income;
                $receivables = $item->receivables;
                $receivables_sk_amount = $item->receivables_sk_amount;
                $has_other_work_resources = $item->has_other_work_resources;


                $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                $html.= '<h2 style="text-align:center;"> '.$category_name.'('.$search_date.')'.'</h2>';
                $html .= '<table border="1" style="border: 1px solid black; box-sizing:border-box;">';

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.full_name').'  </b></td>
                       <td width="158" align="center">'.$name.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.id_card').'  </b></td>
                       <td width="152" align="center">'.$id_card_number.'</td>
                      
                   </tr>';

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.BIRTH_DT').' </b></td>
                       <td width="158" align="center" >'.$birthday.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.birth_place').'</b></td>
                       <td width="152" align="center">'.$birth_place.'</td>
                   </tr>';

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.SOCIAL_STATUS').'  </b></td>
                       <td width="158" align="center">'.$marital_status_name.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.nationality').'  </b></td>
                       <td width="152" align="center">'.$nationality.'</td>
                      
                   </tr>';

                $html .= ' <tr>
                    <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.gender').' </b></td>
                   <td width="158" align="center">'.$gender.'</td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('aid::application.deserted').'   </b></td>
                   <td width="152" align="center">'.$deserted.'</td>
               </tr>';

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.citizen').'  / '.trans('common::application.refugee').'</b></td>
                       <td width="158" align="center">'.$refugee.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.unrwa_card_number').'</b></td>
                       <td width="152" align="center">'.$unrwa_card_number.'</td>
                   </tr>';

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.is_qualified').'</b></td>
                       <td width="158" align="center">'.$is_qualified.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.qualified_card_number').'</b></td>
                       <td width="152" align="center">'.$qualified_card_number.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.family_member_count').'</b></td>
                       <td width="158" align="center">'.$family_cnt.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.spouses').'</b></td>
                       <td width="152" align="center">'.$spouses.'</td>
                   </tr>';

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.male_live').'</b></td>
                       <td width="158" align="center">'.$male_live.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.female_live').'</b></td>
                       <td width="152" align="center">'.$female_live.'</td>
                   </tr>';

                $html .='</table>';
                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="95" align="center"><b>  '.trans('common::application.district').' </b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="105" align="center"><b>  '.trans('common::application.region_').' </b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="105" align="center"><b>   '.trans('common::application.nearlocation_').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="105" align="center"><b>  '.trans('common::application.square').' </b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="103" align="center"><b>  '.trans('common::application.mosques').' </b></td>
                   </tr>';

                $html .= ' <tr>
                       <td width="95" align="center"> <br>'.$district.' <br></td>
                       <td width="105" align="center"> <br>'.$region_name.' <br></td>
                       <td width="105" align="center"> <br>'.$nearLocation.' <br></td>
                       <td width="105" align="center"> <br>'.$square_name.' <br></td>
                       <td width="103" align="center"> <br>'.$mosque_name.' <br></td>
                   </tr>';


                $html .='</table>';
                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                        '.trans('common::application.contact information').'
                        </b></td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.mobile').'</b></td>
                       <td width="156" align="center">'.$primary_mobile.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.secondery_mobile').'</b></td>
                       <td width="157" align="center">'.$secondary_mobile.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.phone').'</b></td>
                       <td width="156" align="center">'.$phone.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.wataniya').'</b></td>
                       <td width="157" align="center">'.$wataniya.'</td>
                   </tr>';
                $html .='</table>';

                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                       '.trans('common::application.works info').' 
                        </b></td>
                   </tr>';

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.is_she_working?').'</b></td>
                       <td width="156" align="center">'.$working.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.work_job_').' </b></td>
                       <td width="137" align="center">'.$work_job.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.is_can_work').'</b></td>
                       <td width="156" align="center">'.$can_work.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.work_reason').' </b></td>
                       <td width="137" align="center">'.$work_reason.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_location').' </b></td>
                       <td width="156" align="center">'.$work_location.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b>'.trans('common::application.Work_Status').' </b></td>
                       <td width="137" align="center">'.$work_status.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.Wage category').':  </b></td>
                       <td width="156" align="center">'.$work_wage.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b>'.trans('common::application.monthly_income').'</b></td>
                       <td width="137" align="center">'. $monthly_income .'</td>
                   </tr>';


                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.actual_monthly_income').'</b></td>
                       <td width="156" align="center">'.$actual_monthly_income.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.has_other_work_resources').' </b></td>
                       <td width="137" align="center">'.$has_other_work_resources.'</td>
                       
                   </tr>';
                $html .= ' <tr>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.receivables').' </b></td>
                           <td width="156" align="center">'.$receivables.'</td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b>   '.trans('common::application.receivables_sk_amount').'</b></td>
                           <td width="137" align="center">'.$receivables_sk_amount.'</td>
                           
                       </tr>';

                $html .='</table>';


                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1">';
                $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                       '.trans('common::application.home info').'
                        </b></td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.property_type').'</b></td>
                       <td width="156" align="center">'.$property_types.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.property_type').'('.trans('common::application.for renter').') </b></td>
                       <td width="157" align="center">'.$rent_value.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.roof_material').'</b></td>
                       <td width="156" align="center">'.$roof_materials.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.residence_condition').'</b></td>
                       <td width="157" align="center">'.$house_condition.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.indoor_condition').'</b></td>
                       <td width="156" align="center">'.$residence_condition.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.habitable').'</b></td>
                       <td width="157" align="center">'.$habitable.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.home_area').'</b></td>
                       <td width="156" align="center">'.$area.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.rooms').'</b></td>
                       <td width="157" align="center">'.$rooms.'</td>
                   </tr>';
                $need_repair = $item->need_repair;
                $repair_notes = $item->repair_notes;
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="200" align="center"><b>'.
                    trans('common::application.need_repair').'</b></td>
                       <td width="312" align="center">'.$need_repair.'</td>
                   </tr>';
                $html .='</table>';

                $html .='</body></html>';
                PDF::AddPage('P','A4');
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


                $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';

                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                      '.trans('common::application.health info').'
                        </b></td>
                   </tr>';

                $has_health_insurance = $item->has_health_insurance;
                $health_insurance_number = $item->health_insurance_number;
                $health_insurance_type = $item->health_insurance_type;

                $has_device = $item->has_device;
                $used_device_name = $item->used_device_name;

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.has_health_insurance').' </b></td>
                       <td width="156" align="center">'.$has_health_insurance.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.health_insurance_number').'</b></td>
                       <td width="157" align="center">'.$health_insurance_number.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.health_insurance_type').' </b></td>
                       <td width="156" align="center">'.$health_insurance_type.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.has_device').'</b></td>
                       <td width="157" align="center">'.$has_device.'</td>
                   </tr>';

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.used_device_name').'</b></td>
                       <td width="413" align="center">'.$used_device_name.'</td>
                   </tr>';

                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.disease_/_disability_details').'</b></td>
                       <td width="413" align="center">'.$health_details.'</td>
                   </tr>';

                $html .='</table>';

                $html .= '<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="300" align="center"><b> '.trans('common::application.reconstructions_promised').'? ('.trans('common::application.yes').' /'.trans('common::application.no').') </b></td>
                       <td width="213" align="center">' . $promised . '</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="300" align="center"><b> '.trans('common::application.ReconstructionOrg').'('.trans('common::application.if the answer to the previous question is yes').'):  </b></td>
                       <td width="213" align="center">' . $reconstructions_organization_name . '</td>
                   </tr>';
                $html .='</table>';

                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="height: 50px ; padding: 55px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b> 
                       '.trans('common::application.case_needs').'
                       </b></td>
                       <td width="415" align="center">' . $needs . '</td>
                   </tr>';

                $html .= ' <tr>
                       <td style="height: 50px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b>  '.trans("common::application.repair_notes").'</b></td>
                       <td width="415" align="center">' . $repair_notes . '</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 50px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b>  '.trans("common::application.social researcher's recommendations").'</b></td>
                       <td width="415" align="center">' . $notes . '</td>
                   </tr>';

                $visitor_opinion = $item->visitor_opinion;
                $html .= ' <tr>
                       <td style="height: 50px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b>  '.trans("common::application.visitor_opinion").'</b></td>
                       <td width="415" align="center">' . $visitor_opinion . '</td>
                   </tr>';
                $html .='</table>';

                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';

                $visitor_card = $item->visitor_card;
                $visitor_evaluation = $item->visitor_evaluation_;
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.visitor_card').'</b></td>
                       <td width="156" align="center">' . $visitor_card . '</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.visitor').' </b></td>
                       <td width="157" align="center">' . $visitor . '</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.visited_at').'</b></td>
                       <td width="156" align="center">' . $visited_at . '</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.visitor_evaluation').' </b></td>
                       <td width="157" align="center">' . $visitor_evaluation . '</td>
                   </tr>';



                $html .='</table>';

                $html .='</body></html>';
                PDF::AddPage('P','A4');
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

                if (sizeof($item->family_members)!=0){

                    $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                    $html.= '<h2 style="text-align:center;"> '.trans('common::application.family-data').'

    ('. sizeof($item->family_members) .')</h2>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.name').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.id_card').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.BIRTH_DT').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="40" align="center"><b> '.trans('common::application.gender').'</b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.SOCIAL_STATUS').'</b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.currently_study').'</b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.health_condition').' </b></td>
                   </tr>';
                    $z=1;
                    foreach($item->family_members as $record) {
                        $fm_nam = $record->name;
                        $fm_idc = $record->id_card_number;
                        $fm_bd = $record->birthday;
                        $fm_gen = $record->gender;
                        $fm_age = $record->age;
                        $fm_ms = $record->marital_status;
                        $fm_kin = $record->kinship_name;
                        $fm_grd = $record->grade;
                        $fm_scl = $record->school;
                        $fm_currently_study = $record->currently_study;
                        $fm_hs = $record->health_status;
                        $fm_disnm = $record->diseases_name;
                        $html .= ' <tr>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="120" align="center"><b> '.$fm_nam.'  </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="70" align="center"><b> '.$fm_idc.' </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="70" align="center"><b> '.$fm_bd.'   </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="40" align="center"><b>'.$fm_gen.'</b></td> 
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="70" align="center"><b> '.$fm_ms.' </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="60" align="center"><b> '.$fm_currently_study.' </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$fm_hs.'   </b></td>
                   </tr>';
                        $z++;
//                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="50" align="center"><b>'.$fm_kin.'</b></td>
//                                    <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_working.'   </b></td>
//                <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$fm_job.'   </b></td>
                    }
                    $html .='</table>';
                    $html .='</body></html>';

                    PDF::SetMargins(PDF_MARGIN_LEFT, 2, PDF_MARGIN_RIGHT);
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                }
                if (sizeof($item->home_indoor)!=0){
                    $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                    $html.= '<h2 style="text-align:center;"> '.trans('common::application.furniture-info').'</h2>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="200" align="center"><b> '.trans('common::application.content').'  </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.exist').'/'.trans('common::application.not exist').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_status').'   </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.needs').'</b></td>
                   </tr>';

                    foreach($item->home_indoor as $record) {

                        $row_es_name = $record->name;
                        $row_es_if_exist= $record->exist;
                        $row_es_needs=is_null($record->needs) ? ' ' : $record->needs;
                        $row_es_condition=is_null($record->essentials_condition) ? ' ' : $record->essentials_condition;

                        $html .= ' <tr>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="200" align="center"><b> '.$row_es_name.'  </b></td>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$row_es_if_exist.' </b></td>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$row_es_condition.'   </b></td>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b>'.$row_es_needs .'</b></td>
                   </tr>';
                    }
                    $html .='</table>';
                    $html .='</body></html>';

                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                }
                if (sizeof($item->financial_aid_source)!=0){

                    $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                    $html.= '<h2 style="text-align:center;"> '.trans('common::application.concrete foreign aid data').'</h2>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="200" align="center"><b>  '.trans('common::application.Name of the organization source of assistance').'  </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="130" align="center"><b> '.trans('common::application.Does the family receive help from org?').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.quantity').'   </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.currency_name').' </b></td>
                   </tr>';

                    foreach($item->financial_aid_source as $record) {
                        $row_fin_name = $record->name;
                        $row_fin_if_exist= $record->aid_take;
                        $row_fin_count= $record->aid_value;
                        $row_fin_currency= $record->currency_name;
                        $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="200" align="center"><b> '.$row_fin_name.'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="130" align="center"><b> '.$row_fin_if_exist.' </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$row_fin_count.'   </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="80" align="center"><b>'.$row_fin_currency.'</b></td>
                   </tr>';
                    }
                    $html .='</table>';
                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


                }
                if (sizeof($item->non_financial_aid_source)!=0){
                    $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                    $html.= '<h2 style="text-align:center;"> '.trans('common::application.concrete foreign aid data').'</h2>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="250" align="center"><b>  '.trans('common::application.Name of the organization source of assistance').'  </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="150" align="center"><b> '.trans('common::application.Does the family receive help from org?').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.quantity').'   </b></td>
                   </tr>';

                    foreach($item->non_financial_aid_source as $record) {
                        $row_non_fin_name = $record->name;
                        $row_non_fin_if_exist= $record->aid_take;
                        $row_non_fin_count= $record->aid_value;
                        $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="250" align="center"><b> '.$row_non_fin_name.'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="150" align="center"><b> '.$row_non_fin_if_exist.' </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$row_non_fin_count.'   </b></td>
                   </tr>';

                    }

                    $html .='</table>';

                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                }
                if (sizeof($item->properties)!=0){
                    $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                    $html.= '<h2 style="text-align:center;"> '.trans('common::application.Family property data').'</h2>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="250" align="center"><b>  '.trans('common::application.Naming property').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="140" align="center"><b> '.trans('common::application.exist').'/'.trans('common::application.not exist').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.quantity').'   </b></td>
                   </tr>';
                    foreach($item->properties as $record) {
                        $pro_name = $record->name;
                        $has_property= $record->has_property;
                        $quantity= $record->quantity;
                        $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="250" align="center"><b> '.$pro_name.'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="140" align="center"><b> '.$has_property.' </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="120" align="center"><b> '.$quantity.'   </b></td>
                   </tr>';

                    }
                    $html .='</table>';

                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);  }

                $dirName = base_path('storage/app/templates/CaseForm');
                if (!is_dir($dirName)) {
                    @mkdir($dirName, 0777, true);
                }

                PDF::Output($dirName.'/'.$item->category_name.' '.$item->id_card_number.' '.$item->full_name . '.pdf', 'F');

            }

            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');

            $zip = new \ZipArchive;
            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                foreach ( glob( $dirName . '/*' ) as $fileName )
                {
                    $file = basename( $fileName );
                    $zip->addFile( $fileName, $file );
                }

                $zip->close();
                array_map('unlink', glob("$dirName/*.*"));
                rmdir($dirName);
            }

            \Log\Model\Log::saveNewLog('SOCIAL_SEARCH_CASE_EXPORTED',trans('common::application.exported cases forms'));
            return response()->json(['download_token' => $token]);

        }
        else if($request->get('action') =='ExportToWord') {
            $dirName = base_path('storage/app/templates/temp');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }

            foreach($items as $k=>$item){
                $dirName = base_path('storage/app/templates/CaseForm');
                if (!is_dir($dirName)) {
                    @mkdir($dirName, 0777, true);
                }

                $Template=Templates::where(['category_id'=>$item->category_id,'organization_id'=>$user->organization_id])->first();
                if($Template){
                    $path = base_path('storage/app/'.$Template->filename);
                }
                else{
                    $default_template=\Setting\Model\Setting::where(['id'=>'social-search-form','organization_id'=>$user->organization_id])->first();

                    if($default_template){
                        $path = base_path('storage/app/'.$default_template->value);
                    }else{
                        $path = base_path('storage/app/templates/default-search-form-template.docx');
                    }
                }

                $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);

                $templateProcessor->setValue('date', $item->date);
                $templateProcessor->setValue('organization_name', $item->organization_name);
                $templateProcessor->setValue('category_name', $item->category_name);

                $templateProcessor->setValue('first_name', $item->first_name);
                $templateProcessor->setValue('second_name', $item->second_name);
                $templateProcessor->setValue('third_name', $item->third_name );
                $templateProcessor->setValue('last_name', $item->last_name );
                $templateProcessor->setValue('name', $item->full_name);

                $templateProcessor->setValue('id_card_number', $item->id_card_number);
                $templateProcessor->setValue('card_type', $item->card_type);
                $templateProcessor->setValue('birthday', $item->birthday);
                $templateProcessor->setValue('age', $item->age);
                $templateProcessor->setValue('gender', $item->gender);
                $templateProcessor->setValue('gender_male', $item->gender_male);
                $templateProcessor->setValue('gender_female', $item->gender_female);
                $templateProcessor->setValue('birth_place', $item->birth_place);
                $templateProcessor->setValue('nationality', $item->nationality);
                $templateProcessor->setValue('refugee', $item->refugee);
                $templateProcessor->setValue('is_refugee', $item->is_refugee);
                $templateProcessor->setValue('per_refugee', $item->per_refugee);
                $templateProcessor->setValue('is_citizen', $item->is_citizen);
                $templateProcessor->setValue('per_citizen', $item->per_citizen);
                $templateProcessor->setValue('unrwa_card_number', $item->unrwa_card_number);
                $templateProcessor->setValue('marital_status_name', $item->marital_status_name);

                $templateProcessor->setValue('deserted', $item->deserted);
                $templateProcessor->setValue('is_deserted', $item->is_deserted);
                $templateProcessor->setValue('not_deserted', $item->not_deserted);

                $templateProcessor->setValue('receivables', $item->receivables);
                $templateProcessor->setValue('is_receivables', $item->is_receivables);
                $templateProcessor->setValue('is_ receivables', $item->not_receivables);
                $templateProcessor->setValue('not_receivables', $item->not_receivables);
                $templateProcessor->setValue('receivables_sk_amount', $item->receivables_sk_amount);

                $templateProcessor->setValue('is_qualified', $item->is_qualified);
                $templateProcessor->setValue('qualified', $item->qualified);
                $templateProcessor->setValue('not_qualified', $item->not_qualified);
                $templateProcessor->setValue('qualified_card_number', $item->qualified_card_number);

                $templateProcessor->setValue('monthly_income', $item->monthly_income);
                $templateProcessor->setValue('actual_monthly_income', $item->actual_monthly_income);

                $templateProcessor->setValue('address', $item->address);
                $templateProcessor->setValue('country', $item->country);
                $templateProcessor->setValue('governarate', $item->governarate);
                $templateProcessor->setValue('nearLocation', $item->nearLocation);
                $templateProcessor->setValue('mosque_name', $item->mosque_name);
                $templateProcessor->setValue('region', $item->region_name);
                $templateProcessor->setValue('square', $item->square_name);
                $templateProcessor->setValue('street_address', $item->street_address);

                $templateProcessor->setValue('family_cnt', $item->family_cnt);
                $templateProcessor->setValue('male_live', $item->male_live);
                $templateProcessor->setValue('female_live', $item->female_live);
                $templateProcessor->setValue('spouses', $item->spouses);
                $templateProcessor->setValue('family_count', $item->family_count);

                $templateProcessor->setValue('phone', $item->phone);
                $templateProcessor->setValue('primary_mobile', $item->primary_mobile);
                $templateProcessor->setValue('wataniya', $item->wataniya);
                $templateProcessor->setValue('secondary_mobile', $item->secondary_mobile );

                $templateProcessor->setValue('working', $item->working);
                $templateProcessor->setValue('works', $item->works);
                $templateProcessor->setValue('is_working', $item->is_working);
                $templateProcessor->setValue('not_working', $item->not_working);
                $templateProcessor->setValue('can_work', $item->can_work);
                $templateProcessor->setValue('can_works', $item->can_works);
                $templateProcessor->setValue('can_working', $item->can_working);
                $templateProcessor->setValue('can_not_working', $item->can_not_working);
                $templateProcessor->setValue('work_job', $item->work_job);
                $templateProcessor->setValue('work_reason', $item->work_reason);
                $templateProcessor->setValue('work_status', $item->work_status);
                $templateProcessor->setValue('work_wage', $item->work_wage);
                $templateProcessor->setValue('work_location', $item->work_location);
                $templateProcessor->setValue('has_other_work_resources', $item->has_other_work_resources);
                $templateProcessor->setValue('has_other_resources', $item->has_other_resources);
                $templateProcessor->setValue('not_has_other_resources', $item->not_has_other_resources);

                $templateProcessor->setValue('health_status', $item->health_status);
                $templateProcessor->setValue('good_health_status', $item->good_health_status);
                $templateProcessor->setValue('chronic_disease', $item->chronic_disease);
                $templateProcessor->setValue('disabled', $item->disabled);
                $templateProcessor->setValue('diseases_name', $item->diseases_name);
                $templateProcessor->setValue('has_disease', $item->has_disease);
                $templateProcessor->setValue('has_health_insurance', $item->has_health_insurance);
                $templateProcessor->setValue('have_health_insurance', $item->have_health_insurance);
                $templateProcessor->setValue('not_have_health_insurance', $item->not_have_health_insurance);
                $templateProcessor->setValue('health_insurance_number', $item->health_insurance_number);
                $templateProcessor->setValue('health_insurance_type', $item->health_insurance_type);
                $templateProcessor->setValue('has_device', $item->has_device);
                $templateProcessor->setValue('use_device', $item->use_device);
                $templateProcessor->setValue('not_use_device', $item->not_use_device);
                $templateProcessor->setValue('used_device_name', $item->used_device_name);
                $templateProcessor->setValue('health_details', $item->health_details);
                $templateProcessor->setValue('property_types', $item->property_types);
                $templateProcessor->setValue('rent_value', $item->rent_value);
                $templateProcessor->setValue('roof_materials', $item->roof_materials);
                $templateProcessor->setValue('habitable', $item->habitable);
                $templateProcessor->setValue('house_condition', $item->house_condition);
                $templateProcessor->setValue('residence_condition', $item->residence_condition);
                $templateProcessor->setValue('indoor_condition', $item->indoor_condition);
                $templateProcessor->setValue('rooms', $item->rooms);
                $templateProcessor->setValue('area', $item->area);
                $templateProcessor->setValue('repair_notes', $item->repair_notes);
                $templateProcessor->setValue('need_repair', $item->need_repair);
                $templateProcessor->setValue('repair', $item->repair);
                $templateProcessor->setValue('not_repair', $item->not_repair);
                $templateProcessor->setValue('deserted', $item->deserted);

                $templateProcessor->setValue('needs', $item->needs);
                $templateProcessor->setValue('promised', $item->promised);
                $templateProcessor->setValue('if_promised', $item->if_promised);
                $templateProcessor->setValue('was_promised', $item->was_promised);
                $templateProcessor->setValue('not_promised', $item->not_promised);
                $templateProcessor->setValue('reconstructions_organization_name', $item->reconstructions_organization_name);

                $templateProcessor->setValue('visitor', $item->visitor);
                $templateProcessor->setValue('notes', $item->notes);
                $templateProcessor->setValue('visited_at', $item->visited_at);
                $templateProcessor->setValue('visitor_opinion', $item->visitor_opinion);
                $templateProcessor->setValue('visitor_card', $item->visitor_card);
                $templateProcessor->setValue('visitor_evaluation', $item->visitor_evaluation);
                $templateProcessor->setValue('very_bad_condition_evaluation', $item->very_bad_condition_evaluation);
                $templateProcessor->setValue('bad_condition_evaluation', $item->bad_condition_evaluation);
                $templateProcessor->setValue('good_evaluation', $item->good_evaluation);
                $templateProcessor->setValue('very_good_evaluation', $item->very_good_evaluation);
                $templateProcessor->setValue('excellent_evaluation', $item->excellent_evaluation);


                if (sizeof($item->banks)!=0){
                    try{
                        $templateProcessor->cloneRow('r_bank_name', sizeof($item->banks));
                        $zxs=1;
                        foreach($item->banks as $rec) {
                            $templateProcessor->setValue('r_bank_name#' . $zxs, $rec->bank_name);
                            $templateProcessor->setValue('r_branch_name#' . $zxs, $rec->branch);
                            $templateProcessor->setValue('r_account_owner#' . $zxs, $rec->account_owner);
                            $templateProcessor->setValue('r_account_number#' . $zxs, $rec->account_number);
                            if($rec->check == true){
                                $templateProcessor->setValue('r_default#' . $zxs, trans('aid::application.yes') );
                            }else{
                                $templateProcessor->setValue('r_default#' . $zxs, trans('aid::application.no') );
                            }
                            $zxs++;
                        }
                    }catch (\Exception $e){}
                }else{
                    try{
                        $templateProcessor->cloneRow('r_bank_name', 1);
                        $templateProcessor->setValue('r_bank_name#' . 1, ' ');
                        $templateProcessor->setValue('r_branch_name#' . 1, ' ');
                        $templateProcessor->setValue('r_account_owner#' . 1, ' ');
                        $templateProcessor->setValue('r_account_number#' . 1,' ');
                        $templateProcessor->setValue('r_default#' . 1, ' ');
                    }catch (\Exception $e){  }
                }

                if (sizeof($item->home_indoor)!=0){
                    try{
                        $templateProcessor->cloneRow('row_es_name', sizeof($item->home_indoor));
                        $z=1;
                        foreach($item->home_indoor as $record) {
                            $templateProcessor->setValue('row_es_name#' . $z, $record->name);
                            $templateProcessor->setValue('row_es_if_exist#' . $z, $record->exist);
                            $templateProcessor->setValue('row_es_needs#' . $z, $record->needs);
                            $templateProcessor->setValue('row_es_condition#' . $z, $record->essentials_condition);
                            $templateProcessor->setValue('row_es_not_exist#' . $z, $record->not_exist);
                            $templateProcessor->setValue('row_es_very_bad_condition#' . $z, $record->very_bad_condition);
                            $templateProcessor->setValue('row_es_bad_condition#' . $z, $record->bad_condition);
                            $templateProcessor->setValue('row_es_good#' . $z, $record->good);
                            $templateProcessor->setValue('row_es_very_good#' . $z, $record->very_good);
                            $templateProcessor->setValue('row_es_excellent#' . $z, $record->excellent);
                            $z++;
                        }
                    }catch (\Exception $e){  }
                }
                else{
                    try{
                        $templateProcessor->cloneRow('row_es_name', 1);
                        $templateProcessor->setValue('row_es_name#' . 1, ' ');
                        $templateProcessor->setValue('row_es_if_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_es_needs#' . 1, ' ');
                        $templateProcessor->setValue('row_es_condition#' . 1, ' ');
                        $templateProcessor->setValue('row_es_not_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_es_very_bad_condition#' . 1, ' ');
                        $templateProcessor->setValue('row_es_bad_condition#' . 1, ' ');
                        $templateProcessor->setValue('row_es_good#' . 1, ' ');
                        $templateProcessor->setValue('row_es_very_good#' . 1, ' ');
                        $templateProcessor->setValue('row_es_excellent#' . 1, ' ');

                    }catch (\Exception $e){  }
                }
                if (sizeof($item->properties)!=0){
                    try{
                        $templateProcessor->cloneRow('row_pro_name', sizeof($item->properties));
                        $z2=1;
                        foreach($item->properties as $sub_record) {
                            $templateProcessor->setValue('row_pro_name#' . $z2, $sub_record->name);
                            $templateProcessor->setValue('row_pro_if_exist#' . $z2, $sub_record->has_property);
                            $templateProcessor->setValue('row_pro_exist#' . $z2, $sub_record->exist);
                            $templateProcessor->setValue('row_pro_not_exist#' . $z2, $sub_record->not_exist);
                            $templateProcessor->setValue('row_pro_count#' . $z2, $sub_record->quantity);
                            $z2++;
                        }
                    }catch (\Exception $e){  }
                }else{
                    try{
                        $templateProcessor->cloneRow('row_pro_name#', 1);
                        $templateProcessor->setValue('row_pro_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_pro_not_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_pro_name#' . 1, ' ');
                        $templateProcessor->setValue('row_pro_if_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_pro_count#' . 1, ' ');
                    }catch (\Exception $e){  }
                }
                if (sizeof($item->financial_aid_source)!=0){
                    try{
                        $templateProcessor->cloneRow('row_fin_name', sizeof($item->financial_aid_source));
                        $z3=1;
                        foreach($item->financial_aid_source as $record) {
                            $templateProcessor->setValue('row_fin_name#' . $z3, $record->name);
                            $templateProcessor->setValue('row_fin_if_exist#' . $z3, $record->aid_take);
                            $templateProcessor->setValue('row_fin_take#' . $z3, $record->take);
                            $templateProcessor->setValue('row_fin_not_take#' . $z3, $record->not_take);
                            $templateProcessor->setValue('row_fin_count#' . $z3, $record->aid_value);
                            $templateProcessor->setValue('row_fin_currency#' . $z3, $record->currency_name);
                            $z3++;
                        }
                    }catch (\Exception $e){  }
                }else{
                    try{
                        $templateProcessor->cloneRow('row_fin_name', 1);
                        $templateProcessor->setValue('row_fin_name#' . 1, ' ');
                        $templateProcessor->setValue('row_fin_if_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_fin_count#' . 1, ' ');
                        $templateProcessor->setValue('row_fin_take#' . 1, ' ');
                        $templateProcessor->setValue('row_fin_not_take#' . 1, ' ');
                        $templateProcessor->setValue('row_fin_currency#' . 1, ' ');
                    }catch (\Exception $e){  }
                }
                if (sizeof($item->non_financial_aid_source)!=0){
                    try{
                        $templateProcessor->cloneRow('row_nonfin_name', sizeof($item->non_financial_aid_source));
                        $z3=1;
                        foreach($item->non_financial_aid_source as $record) {
                            $templateProcessor->setValue('row_nonfin_name#' . $z3, $record->name);
                            $templateProcessor->setValue('row_nonfin_if_exist#' . $z3, $record->aid_take);
                            $templateProcessor->setValue('row_nonfin_not_take#' . $z3, $record->not_take);
                            $templateProcessor->setValue('row_nonfin_take#' . $z3, $record->take);
                            $templateProcessor->setValue('row_nonfin_count#' . $z3, $record->aid_value);
                            $z3++;
                        }
                    }catch (\Exception $e){  }
                } else{
                    try{
                        $templateProcessor->cloneRow('row_nonfin_name', 1);
                        $templateProcessor->setValue('row_nonfin_name#' . 1, ' ');
                        $templateProcessor->setValue('row_nonfin_if_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_nonfin_take#' . 1, ' ');
                        $templateProcessor->setValue('row_nonfin_not_take#' . 1, ' ');
                        $templateProcessor->setValue('row_nonfin_count#' . 1, ' ');
                    }catch (\Exception $e){  }
                }

                if (sizeof($item->family_members)!=0){
                    try{
                        $templateProcessor->cloneRow('fm_nam', sizeof($item->family_members));
                        $z=1;
                        foreach($item->family_members as $record) {
                            $templateProcessor->setValue('fm_inc#' . $z,$z);
                            $templateProcessor->setValue('fm_nam#' . $z, $record->name);
                            $templateProcessor->setValue('fm_bd#' . $z, $record->birthday);
                            $templateProcessor->setValue('fm_gen#' . $z, $record->gender);
                            $templateProcessor->setValue('fm_age#' . $z, $record->age);
                            $templateProcessor->setValue('fm_idc#' . $z, $record->id_card_number);
                            $templateProcessor->setValue('fm_ms#' . $z, $record->marital_status);
                            $templateProcessor->setValue('fm_kin#' . $z, $record->kinship_name);
                            $templateProcessor->setValue('fm_grd#' . $z, $record->grade);
                            $templateProcessor->setValue('fm_scl#' . $z, $record->school);
                            $templateProcessor->setValue('fm_hs#' . $z, $record->health_status);
                            $templateProcessor->setValue('fm_disnm#' . $z, $record->diseases_name);
                            $z++;
                        }
                    }catch (\Exception $e){  }
                }
                else{
                    try{
                        $templateProcessor->cloneRow('fm_nam',1);
                        $templateProcessor->setValue('fm_inc#' . 1,1);
                        $templateProcessor->setValue('fm_scl#' . 1, ' ');
                        $templateProcessor->setValue('fm_nam#' . 1, ' ');
                        $templateProcessor->setValue('fm_bd#' . 1, ' ');
                        $templateProcessor->setValue('fm_gen#' . 1, ' ');
                        $templateProcessor->setValue('fm_age#' . 1, ' ');
                        $templateProcessor->setValue('fm_idc#' . 1,' ');
                        $templateProcessor->setValue('fm_ms#' . 1, ' ');
                        $templateProcessor->setValue('fm_kin#' . 1, ' ');
                        $templateProcessor->setValue('fm_grd#' . 1, ' ');
                        $templateProcessor->setValue('fm_hs#' . 1, ' ');
                        $templateProcessor->setValue('fm_disnm#' . 1, ' ');
                    }catch (\Exception $e){  }
                }

                $templateProcessor->saveAs($dirName.'/'.$item->category_name.' '.$item->id_card_number.' '.$item->full_name.'.docx');

            }

            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');

            $zip = new \ZipArchive;
            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                foreach ( glob( $dirName . '/*' ) as $fileName )
                {
                    $file = basename( $fileName );
                    $zip->addFile( $fileName, $file );
                }

                $zip->close();
                array_map('unlink', glob("$dirName/*.*"));
                rmdir($dirName);
            }

            $message=trans('common::application.exported cases forms').trans('common::application.aids') ;

            \Log\Model\Log::saveNewLog('SOCIAL_SEARCH_CASE_EXPORTED',$message);

            return response()->json([
                'download_token' => $token]);




        }

        return response()->json(['status'=>false]);
    }

}
