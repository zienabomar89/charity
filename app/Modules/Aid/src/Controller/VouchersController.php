<?php

namespace aid\Controller;
use App\Http\Controllers\Controller;
use App\Http\Helpers;
use Auth\Model\User;
use Common\Model\CaseModel;
use Illuminate\Http\Request;
use League\Flysystem\Exception;
use Organization\Model\Organization;
use Organization\Model\OrganizationProject;
use Setting\Model\aidsLocation;
use Setting\Model\Setting;
use Aid\Model\Vouchers;
use Aid\Model\VoucherPersons;
use Aid\Model\VoucherDocuments;
use Excel;
use Elibyy\TCPDF\TCPDF;
use PDF;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Common\Model\GovServices;

use Document\Model\File;

class VouchersController  extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    //----------------------------------------------------------------------------------------------------

   // get titles voucher
    public function vouchersTitleList(Request $request){

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        return response()->json([ 'list'  => Vouchers::getTitlesList()]);
    }

   // get general code voucher
    public function codesList(Request $request){

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        return response()->json([ 'list'  => Vouchers::getCodesList()]);
    }


    // get voucher code
    public function vouchersCodeList(Request $request){

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        return response()->json([ 'list'  => Vouchers::getVouchersCodeList()]);
    }
    // delete using un_serial
    public function deleteSerial(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $user = \Auth::user();
        $UserType=$user->type;

        $codes = $request->codes;

        if(sizeof($codes) == 0){
            return response()->json(["status" => 'failed', "msg"=>trans('aid::application.no vouchers to update')]);
        }

        $count = Vouchers::where(function ($q) use ($user,$UserType,$codes) {
            $q->whereIn('un_serial' ,$codes);

            $q->whereNull('deleted_at');
            $q->whereNotNull('un_serial');

            if(!$user->super_admin){
                $q->where(function ($anq) use ($user) {
                    $anq->whereNull('project_id');
                    $anq->wherein('organization_id', function($decq) use($user) {
                        $decq->select('descendant_id')
                            ->from('char_organizations_closure')
                            ->where('ancestor_id', '=', $user->organization_id);
                    });
                });
            }
            else{
                if($UserType == 2) {
                    $q->where(function ($anq) use ($user) {
                        $anq->where('status','=',0);
                        $anq->whereNull('project_id');
                        $anq->where('organization_id',$user->organization_id);
                        $anq->orwherein('organization_id', function($quer) use($user) {
                            $quer->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });
                }
                else{
                    $q->where(function ($anq) use ($user) {
                        $anq->where('status','=',0);
                        $anq->whereNull('project_id');
                        $anq->wherein('organization_id', function($decq) use($user) {
                            $decq->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $user->organization_id);
                        });
                    });
                }
            }

        })->count();
        if($count == 0){
            return response()->json(["status" => 'failed', "msg"=>trans('aid::application.no vouchers to update')]);
        }

        $update = 0;
        foreach ($codes as $code){
//            VoucherDocuments::whereIn('voucher_id',function($q) use($code) {
//                $q->select('id')
//                    ->from('char_vouchers')
//                    ->where(function ($q) use ($code) {
//                        $q->whereNull('deleted_at');
//                        $q->where('un_serial', $code);
//                    });
//            })->delete();
//
//           VoucherPersons::whereIn('voucher_id',function($q) use($code) {
//                $q->select('id')
//                    ->from('char_vouchers')
//                    ->where(function ($q) use ($code) {
//                        $q->whereNull('deleted_at');
//                        $q->where('un_serial', $code);
//                    });
//            })->delete();

            Vouchers::where('un_serial' ,$code)->delete();
            $update++;
            \Log\Model\Log::saveNewLog('VOUCHER_DELETE',trans('aid::application.delete aid voucher that has code')  . ' "' . $code . '" ');
        }

        if($update > 0 ){
            return response()->json(["status" => 'success', "msg"=>trans('setting::application.The row is deleted from db')]);
        }

        return response()->json(["status" => 'failed', "msg"=>trans('setting::application.The row is not deleted from db')]);

    }

    // --------------------------------------------------------------------
    public function cases(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $status=$request->action;

        $this->authorize('manage', Vouchers::class);

        $user = \Auth::user();
        $Org=Organization::findorfail($user->organization_id);
        $master=true;
        if($Org->parent_id){
            $master=false;
        }
        $request->request->add(['organization_id'=> $user->organization_id]);
        $VouchersList=Vouchers::personsList($request->all());
        if($status =='ExportToExcel'){
            if(sizeof($VouchersList) !=0){
                $data=array();
                foreach($VouchersList as $key =>$value){
                    $data[$key][trans('aid::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        if($master){
                            $data[$key][trans('aid::application.' . $k)]= str_replace("-","_",$v) ;
//                            $data[$key][trans('aid::application.' . $k)]= $v;
                        }else{
                            if($k != 'organization_name'){
                                $data[$key][trans('aid::application.' . $k)]= str_replace("-","_",$v) ;
                            }
                        }
                    }
                }
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($data) {
                    $excel->setTitle(trans('aid::application.voucher_name'));
                    $excel->setDescription(trans('aid::application.voucher_name'));
                    $excel->sheet(trans('aid::application.vouchers_sheet'), function($sheet) use($data) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);
                        $sheet->getStyle("A1:AQ1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->fromArray($data);
                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['download_token' => $token]);
            }
            return response()->json(['status' => false]);
        }
        return response()->json(['master'=>$master,
            'data' => $VouchersList['list'],
            'total' => $VouchersList['total']]);

    }

    // filter voucher statistic
    public function all(Request $request)
    {

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $status=$request->action;

        if($status =='filter'){
            $this->authorize('manage', Vouchers::class);
        }else{
            //            $this->authorize('export', Vouchers::class);
        }
        $user = \Auth::user();
        $Org=Organization::findorfail($user->organization_id);
        $master=true;
        if($Org->parent_id){
            $master=false;
        }

        $request->request->add(['organization_id'=> $user->organization_id]);
        $request->request->add(['trashed'=> false ]);
        $VouchersList=Vouchers::filter($request->all());

        if($status =='ExportToExcel'){
            if(sizeof($VouchersList) !=0){
                $data=array();
                foreach($VouchersList as $key =>$value){
                    $data[$key][trans('aid::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        if($master){
                            $data[$key][trans('aid::application.' . $k)]= $v;
                        }else{
                            if($k != 'organization_name'){
                                $data[$key][trans('aid::application.' . $k)]= $v;
                            }
                        }
                    }
                }
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($data) {
                    $excel->setTitle(trans('aid::application.voucher_name'));
                    $excel->setDescription(trans('aid::application.voucher_name'));
                    $excel->sheet(trans('aid::application.vouchers_sheet'), function($sheet) use($data) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);
                        $sheet->getStyle("A1:U1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->fromArray($data);
                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['download_token' => $token]);
            }
            return response()->json(['status' => false]);
        }
        else if($status =='ExportToWord'){
            $dirName = base_path('storage/app/templates/Vouchers');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }
            $n=0;
            foreach($VouchersList as $voucher) {

                if($voucher->voucher_beneficiary !=0){
                    $n++;
                    $Beneficiary=VoucherPersons::VoucherBeneficiary($voucher->id,-1);

                    if(!$voucher->template){
                        $default_template=Setting::where(['id'=>'default-voucher-template','organization_id'=>$user->organization_id])->first();
                        if($default_template){
                            $path = base_path('storage/app/'.$default_template->value);
                        }else{
                            $path = base_path('storage/app/templates/voucher-default-template.docx');
                        }

                    }else{
                        $path = base_path('storage/app/'.$voucher->template);
                    }

                    $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);

                    try{
                        $templateProcessor->cloneRow('rId', sizeof($Beneficiary));
                        $i =1;
                        foreach($Beneficiary as $item){
                            $templateProcessor->setValue('rId#'.$i, $i);
                            $templateProcessor->setValue('organization_name#'.$i, $voucher->organization_name);
                            $templateProcessor->setValue('sponsor_name#'.$i, $voucher->sponsor_name);
                            $templateProcessor->setValue('num#'.$i, $i);

                            if(!is_null($voucher->sponsor_logo)){
                                $templateProcessor->setImg('image#'.$i, array('src'=> base_path('storage/app/' . $voucher->sponsor_logo),'swh'=>'150'));
                            }else{
                                $templateProcessor->setImg('image#'.$i, array('src'=> base_path('storage/app/logos/emptyLogo.png'),'swh'=>'150'));
                            }
                            if(!is_null($voucher->organization_logo)){
                                $templateProcessor->setImg('org_image#'.$i, array('src'=> base_path('storage/app/' . $voucher->organization_logo),'swh'=>'150'));
                            }else{
                                $templateProcessor->setImg('org_image#'.$i, array('src'=> base_path('storage/app/logos/emptyLogo.png'),'swh'=>'150'));
                            }
                            $templateProcessor->setValue('name#'.$i, (is_null($item->name) || $item->name == ' ' ) ? ' ' : $item->name );
                            $templateProcessor->setValue('id_card_number#'.$i, (is_null($item->id_card_number) || $item->id_card_number == ' ' ) ? ' ' : $item->id_card_number );
                            $templateProcessor->setValue('address#'.$i, (is_null($item->address) || $item->address == ' ' ) ? ' ' : $item->address );
                            $templateProcessor->setValue('phone#'.$i, (is_null($item->phone) || $item->phone == ' ' ) ? ' ' : $item->phone );
                            $templateProcessor->setValue('secondary_mobile#'.$i, (is_null($item->secondery_mobile) || $item->secondery_mobile == ' ' ) ? ' ' : $item->secondery_mobile );
                            $templateProcessor->setValue('primary_mobile#'.$i, (is_null($item->primary_mobile) || $item->primary_mobile == ' ' ) ? ' ' : $item->primary_mobile );
                            $templateProcessor->setValue('receipt_date#'.$i, (is_null($item->receipt_date) || $item->receipt_date == ' ' ) ? ' ' : $item->receipt_date );
                            $templateProcessor->setValue('receipt_time#'.$i, (is_null($item->receipt_time) || $item->receipt_time == ' ' ) ? ' ' : $item->receipt_time );
                            $templateProcessor->setValue('receipt_location#'.$i, (is_null($item->receipt_location) || $item->receipt_location == ' ' ) ? ' ' : $item->receipt_location );
                            $templateProcessor->setValue('voucher_title#'.$i, (is_null($voucher->title) || $voucher->title == ' ' ) ? ' ' : $voucher->title );
                            $templateProcessor->setValue('voucher_type#'.$i, (is_null($voucher->type) || $voucher->type == ' ' ) ? ' ' : $voucher->type );
                            $templateProcessor->setValue('voucher_currency#'.$i, (is_null($voucher->currency) || $voucher->currency == ' ' ) ? ' ' : $voucher->currency );
                            $templateProcessor->setValue('voucher_value#'.$i, (is_null($voucher->value) || $voucher->value == ' ' ) ? ' ' : $voucher->value );
                            $templateProcessor->setValue('voucher_sh_value#'.$i, (is_null($voucher->shekel_value) || $voucher->shekel_value == ' ' ) ? ' ' : ($voucher->shekel_value ) );
                            $templateProcessor->setValue('voucher_contents#'.$i, (is_null($voucher->content) || $voucher->content == ' ' ) ? ' ' : $voucher->content );
                            $templateProcessor->setValue('voucher_notes#'.$i, (is_null($voucher->notes) || $voucher->notes == ' ' ) ? ' ' : $voucher->notes );
                            $templateProcessor->setValue('voucher_category#'.$i, (is_null($voucher->category_name) || $voucher->category_name == ' ' ) ? ' ' : $voucher->category_name );
                            $i=$i+1;
                        }
                    } catch (Exception $e){
                        return response()->json(['status' => false]);
                    }
                    $token = md5(uniqid());
                    $title = str_replace(" ","_",$voucher->title) ;
                    $templateProcessor->saveAs($dirName.'/'.$title.'.docx');

                }
            }
            if($n!=0){
                $token = md5(uniqid());
                $zipFileName = storage_path('tmp/export_' . $token . '.zip');
                $zip = new \ZipArchive;

                if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
                {
                    foreach ( glob( $dirName . '/*' ) as $fileName )
                    {
                        $file = basename( $fileName );
                        $zip->addFile( $fileName, $file );
                    }

                    $zip->close();
                    array_map('unlink', glob("$dirName/*.*"));
                    rmdir($dirName);
                    return response()->json(['status' => true,
                        'download_token' => $token,
                    ]);
                }
            }
            return response()->json(['status' => false]);
        }
        else if($status =='ExportToPDF'){

            $dirName = base_path('storage/app/templates/Vouchers');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }
            $n=0;
            foreach($VouchersList as $voucher) {
                if($voucher->voucher_beneficiary !=0){
                    $html='';
                    PDF::reset();
                    PDF::SetTitle($voucher->title);
                    PDF::SetFont('aealarabiya', '', 18);
                    PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                    PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);

                    PDF::SetMargins(10, 20, 10);
                    PDF::SetFooterMargin(0);
                    PDF::SetAutoPageBreak(TRUE);
                    $lg = Array();
                    $lg['a_meta_charset'] = 'UTF-8';
                    $lg['a_meta_dir'] = 'rtl';
                    $lg['a_meta_language'] = 'fa';
                    $lg['w_page'] = 'page';
                    PDF::setLanguageArray($lg);
                    PDF::setCellHeightRatio(1.5);

                    $n++;
                    $Beneficiary=VoucherPersons::VoucherBeneficiary($voucher->id,-1);

                    $i = 0;
                    $z = 1;
                    $table='';
                    $key = 1;
                    $count = sizeof($Beneficiary);
                    foreach ($Beneficiary as $item){

                        $table .= '<table width = "533" border="1" style="box-sizing:border-box;">';
                        $imgOrg = base_path('storage/app/'.$voucher->organization_logo);
                        $imgSpo = base_path('storage/app/'.$voucher->sponsor_logo);

                        $table .= ' <tr style="line-height: 47px;font-size: 14px; height: 70px; border:0.5px solid black !important; ">
                                  <td rowspan="3" align="center"     valign="middle" > 
                                        <br><br><img src="'.$imgOrg.'" style=" width: 80px;height: 70px;display: block;margin-left: auto;margin-right: auto;">
                                    </td>
                                    <td colspan="2" align="center" style="font-size:12pt !important;  ">  '.$voucher->title.' </td>
                                    <td rowspan="3" align="center"     valign="middle" > 
                                       <br><br><img src="'.$imgSpo.'" style=" width: 80px;height: 70px;display: block;margin-left: auto;margin-right: auto;">
                                    </td>
                       </tr>
                       <tr style=" line-height: 40px;  font-size: 14px; height: 57px; ">
                             <td align="center" colspan="2" style="font-size:12pt !important;  ">' . trans('aid::application.finance').' /'.$voucher->sponsor_name.'</td>
                        </tr>
                        <tr style=" line-height: 40px; font-size: 14px; height: 57px;   ">
                             <td align="center" colspan="2" style="font-size:12pt !important;">'. trans('aid::application.Implementation of')  .' /'.$voucher->organization_name.'</td>
                        </tr>';

                        $table .= ' <tr style=" line-height: 40px; font-size: 14px; height: 65px; ">
                                      <td colspan="4" align="center" style="font-size:12pt !important;  "> '.trans('aid::application.card number').' ('.$key.') </td>
                                   </tr>';

                        $table .= '<tr style=" line-height: 30px;   font-size: 11px; height: 35px; ">
                                     <td  style=" vertical-align: middle; text-align:center;font-size:12pt !important;  "> '.trans('aid::application.Beneficiary Name').'</td>   
                                     <td align="center" colspan="3" style="font-size:12pt !important; ">    '. $item->name.' </td>   
                                 </tr>';

                        $table .= '<tr style=" line-height: 30px;   font-size: 11px; height: 35px; ">
                                      <td  style=" vertical-align: middle; text-align:center;font-size:10pt !important; ">'.trans('aid::application.id_card').' </td>   
                                     <td   align="center" style="font-size:10pt !important; ">    '. $item->id_card_number.' </td>  
                                     <td   style=" vertical-align: middle; text-align:center;font-size:10pt !important;">'.trans('aid::application.primary_mobile').' </td>   
                                     <td   align="center"style="  font-size:10pt !important;">    '. $item->phone.' </td>   
                                 </tr>';

                        $receipt_time_ =   str_replace('PM', trans('common::application.PM'), $item->receipt_time_);
                        $receipt_time_ =   str_replace('AM', trans('common::application.AM'), $item->receipt_time_);

                        $table .= '<tr style=" line-height: 30px;   font-size: 11px; height: 35px; ">
                                     <td  style="  vertical-align: middle; text-align:center;font-size:10pt !important;"> '.trans('aid::application.receipt_date').'  </td>   
                                     <td align="center"style="font-size:10pt !important; ">    '. $item->receipt_date.' </td>  
                                     <td  style=" vertical-align: middle; text-align:center;font-size:10pt !important;">'.trans('aid::application.receipt_time').' </td>   
                                     <td align="center"style="  font-size:10pt !important;">    '. $receipt_time_.' </td>    
                                 </tr>';
                        $table .= '<tr style=" line-height: 30px;   font-size: 11px; height: 35px; ">
                                     <td  style="  vertical-align: middle; text-align:center;font-size:10pt !important;"> '.trans('aid::application.case_receipt_location').'  </td>   
                                     <td colspan="3" style="  font-size:10pt !important;">    '. $item->receipt_location.' </td>   
                                 </tr>';
                        $table .= '<tr style=" line-height: 30px;   font-size: 11px; height: 35px; ">
                                     <td  style="  vertical-align: middle; text-align:center;font-size:10pt !important;">  '.trans('aid::application.that contain').' </td>   
                                     <td   colspan="3" style="  font-size:10pt !important;">    '. $voucher->content.' </td>   
                                 </tr>';
                        $table .= '<tr style=" line-height: 20px;   font-size: 11px; height: 25px; ">
                          <td colspan="4" style="font-size:9pt !important; text-align: left; top: 0px">  '.trans('aid::application.confirm').'/ </td>
                       </tr>';

                        $table .= '<tr style=" line-height: 20px;   font-size: 11px; height: 25px; ">
                          <td colspan="4" align="center" style="font-size:9pt !important;"> ............................................................................................................................................................................................................... </td>
                       </tr>';

                        $table .= '</table>';
                        $table .= '<span><br></span>';


                        if($i == 1 || ($count %2 == 1 && $key == $count)){
                            $template ='<!DOCTYPE html >
                            <html>
                            <head>
                            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                            <style>
                            body {  background: rgb(204,204,204);  }
                            page[size="chk"] {  background: white;  width: 11.5cm;  height: 3.7cm;  display: block;  margin: 0 auto;  margin-bottom: 0.5cm; ;  }
                            @media print {  body, page[size="chk"] {  margin: 0;  box-shadow: 0;   } }
                            td{
                                font-size : 11px;
                                border: 1px solid black;
                            }
                            </style>
                            </head>
                            <body style=" !important;" dir="rtl">';
                            $template .= $table;
                            $template .='</body></html>';

                            PDF::setFooterCallback(function($pdf) use($z) {
                                $footer = '<br><p style=" text-align: center;">'.($z-1).'<p/>';
                                PDF::writeHTMLCell(0, 0, '', 270, $footer, 0, 1, 0, true, '', true);
                            });

                            PDF::AddPage('P','A4');
                            PDF::writeHTMLCell(0, 0, '', '', $template, 0, 1, 0, true, '', true);
                            $table='';
                            $z++;
                            $i=0;
                        }else{
                            $i++;
                        }
                        $key++ ;
                    }
                    $token = md5(uniqid());
                    $title = str_replace(" ","_",$voucher->title) ;
                    PDF::Output($dirName.'/'. $title. '.pdf', 'F');
                    $key = 1;
                }
            }
            if($n!=0){
                $token = md5(uniqid());
                $zipFileName = storage_path('tmp/export_' . $token . '.zip');
                $zip = new \ZipArchive;

                if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
                {
                    foreach ( glob( $dirName . '/*' ) as $fileName )
                    {
                        $file = basename( $fileName );
                        $zip->addFile( $fileName, $file );
                    }
                    $zip->close();
                    array_map('unlink', glob("$dirName/*.*"));
                    rmdir($dirName);
                    return response()->json([
                        'download_token' => $token,
                    ]);
                }
            }
            return response()->json(['status' => false]);
        }
        return response()->json(['master'=>$master, 'Vouchers' => $VouchersList['list'],
                                 'total' => $VouchersList['total'],
                                  'beneficiary' => $VouchersList['beneficiary']]);

    }

    // export voucher beneficiary bonds on pdf
    public function exportToPDF($person_id, $voucher_id)
    {

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $dirName = base_path('storage/tmp');
        if (!is_dir($dirName)) {
            @mkdir($dirName, 0777, true);
        }

        $user = \Auth::user();
        $voucher= Vouchers::fetch($voucher_id);
        $Beneficiary=VoucherPersons::VoucherBeneficiary($voucher_id,$person_id);

        $count = sizeof($Beneficiary);
        $ceil=ceil($count/2);

        for ($x = 0; $x < $ceil; $x++) {
            $curOffset = ($x) * 2;
            $index=$curOffset;

            PDF::SetTitle('cheque bank');
            PDF::SetFont('aealarabiya', '', 18);
            PDF::SetMargins(10, 20, 10);
            PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);
            $html = '<!doctype html><html lang="ar">
            <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
            <style>
            table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
            </style>
            </head>
            <body style="font-weight: normal !important;">';

            if($index < $count) {
                for ($z = 0; $z < 2; $z++) {
                    if($index < $count){
                        $item = $Beneficiary[$index];
                        $html .= '<table width = "533" border="1" style="box-sizing:border-box;">';
                        $imgOrg = base_path('storage/app/'.$voucher->organization_logo);
                        $imgSpo = base_path('storage/app/'.$voucher->sponsor_logo);

                        $html .= ' <tr style="line-height: 47px;font-size: 14px; height: 70px; border:0.5px solid black !important; ">
                                  <td rowspan="3" align="center"     valign="middle" > 
                                        <br><br><img src="'.$imgOrg.'" style=" width: 80px;height: 70px;display: block;margin-left: auto;margin-right: auto;">
                                    </td>
                                    <td colspan="2" align="center" style="font-size:12pt !important;  ">  '.$voucher->title.' </td>
                                    <td rowspan="3" align="center"     valign="middle" > 
                                       <br><br><img src="'.$imgSpo.'" style=" width: 80px;height: 70px;display: block;margin-left: auto;margin-right: auto;">
                                    </td>
                       </tr>
                       <tr style=" line-height: 40px;  font-size: 14px; height: 57px; ">
                             <td align="center" colspan="2" style="font-size:12pt !important;  ">' . trans('aid::application.finance').'/'.$voucher->sponsor_name.'</td>
                        </tr>
                        <tr style=" line-height: 40px; font-size: 14px; height: 57px;   ">
                             <td align="center" colspan="2" style="font-size:12pt !important;">'. trans('aid::application.Implementation of')  .'/'.$voucher->organization_name.'</td>
                        </tr>';

                        $html .= ' <tr style=" line-height: 40px; font-size: 14px; height: 65px; ">
                                      <td colspan="4" align="center" style="font-size:12pt !important;  ">'.trans('aid::application.card number').'  ('.$index.') </td>
                                   </tr>';

                        $html .= '<tr style=" line-height: 30px;   font-size: 11px; height: 35px; ">
                                     <td  style=" vertical-align: middle; text-align:center;font-size:12pt !important;  "> '.trans('aid::application.Beneficiary Name').'</td>   
                                     <td align="center" colspan="3" style="font-size:12pt !important; ">    '. $item->name.' </td>   
                                 </tr>';

                        $html .= '<tr style=" line-height: 30px;   font-size: 11px; height: 35px; ">
                                      <td  style=" vertical-align: middle; text-align:center;font-size:10pt !important; ">'.trans('aid::application.id_card').' </td>   
                                     <td   align="center" style="font-size:10pt !important; ">    '. $item->id_card_number.' </td>  
                                     <td   style=" vertical-align: middle; text-align:center;font-size:10pt !important;">'.trans('aid::application.primary_mobile').' </td>   
                                     <td   align="center"style="  font-size:10pt !important;">    '. $item->phone.' </td>   
                                 </tr>';

                        $receipt_time_ =   str_replace('PM', trans('aid::application.evening') , $item->receipt_time_);
                        $receipt_time_ =   str_replace('AM', trans('aid::application.morning') , $item->receipt_time_);


                        $html .= '<tr style=" line-height: 30px;   font-size: 11px; height: 35px; ">
                                     <td  style="  vertical-align: middle; text-align:center;font-size:10pt !important;">'.trans('aid::application.receipt_date').'</td>   
                                     <td align="center"style="font-size:10pt !important; ">    '. $item->receipt_date.' </td>  
                                     <td  style=" vertical-align: middle; text-align:center;font-size:10pt !important;">'.trans('aid::application.receipt_time').'</td>   
                                     <td align="center"style="  font-size:10pt !important;">    '. $receipt_time_.' </td>    
                                 </tr>';
                        $html .= '<tr style=" line-height: 30px;   font-size: 11px; height: 35px; ">
                                     <td  style="  vertical-align: middle; text-align:center;font-size:10pt !important;"> '.trans('aid::application.case_receipt_location').'</td>   
                                     <td colspan="3" style="  font-size:10pt !important;">    '. $item->receipt_location.' </td>   
                                 </tr>';
                        $html .= '<tr style=" line-height: 30px;   font-size: 11px; height: 35px; ">
                                     <td  style="  vertical-align: middle; text-align:center;font-size:10pt !important;"> '.trans('aid::application.that contain').'</td>   
                                     <td   colspan="3" style="  font-size:10pt !important;">    '. $voucher->content.' </td>   
                                 </tr>';
                        $html .= '<tr style=" line-height: 20px;   font-size: 11px; height: 25px; ">
                          <td colspan="4" style="font-size:9pt !important; text-align: left; top: 0px">'.trans('aid::application.confirm').'/ </td>
                       </tr>';

                        $html .= '<tr style=" line-height: 20px;   font-size: 11px; height: 25px; ">
                          <td colspan="4" align="center" style="font-size:9pt !important;"> ............................................................................................................................................................................................................... </td>
                       </tr>';

                        $html .= '</table>';
                        $html .= '<span><br></span>';

                        $index++;
                    }
                }
            }
            $html .='</body></html>';
            PDF::AddPage('P','A4');
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
        }

        $token = md5(uniqid());
        PDF::Output($dirName.'/export_' . $token . '.pdf', 'F');
        return response()->json(['download_token' => $token]);
    }

    // list voucher as beneficiary type
    public function getList($id)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $user = \Auth::user();
        return response()->json([ 'Vouchers'  => Vouchers::getList($user,$id,1) ,
            'Owns'  => Vouchers::getList($user,$id,0)]);
    }

    // store new voucher
    public function store(Request $request)
    {

        try {
            ini_set('max_execution_time',0);
            set_time_limit(0);
            Ini_set('memory_limit','2048M');
            $this->authorize('create', Vouchers::class);
            $exist=false;
            $inserted=false;
            $user = \Auth::user();


            $response = array();
            $input=[
                'beneficiary' => strip_tags($request->get('beneficiary')),
                'title' => strip_tags($request->get('title')),
                'notes' => strip_tags($request->get('notes')),
                'content' => strip_tags($request->get('content')),
                'currency_id' => strip_tags($request->get('currency_id')),
                'exchange_rate' => strip_tags($request->get('exchange_rate')),
                'category_id' => strip_tags($request->get('category_id')),
                'type' => strip_tags($request->get('type')),
                'value' => strip_tags($request->get('value')),
                'sponsor_id' => strip_tags($request->get('sponsor_id')),
                'voucher_date' => strip_tags($request->get('voucher_date')),
                'count' => strip_tags($request->get('count')),
                'case_category_id' => strip_tags($request->get('case_category_id')),
                'transfer_company_id' => strip_tags($request->get('transfer_company_id')),
                'center' => strip_tags($request->get('center')),
                'transfer' => strip_tags($request->get('transfer')),
                'collected' => strip_tags($request->get('collected')),
            ];

            if(!($request->un_serial == null || $request->un_serial== 'null')){
                $input['un_serial'] = strip_tags($request->get('un_serial'));
            }

            $rules= [
                'beneficiary' => 'required',
                'notes' => 'required',
                'title' => 'required|max:255',
                'voucher_date' => 'required',
                'sponsor_id' => 'required',
                'exchange_rate' => 'required',
                'currency_id' => 'required',
                'count' => 'required|integer',
                'content' => 'required',
                'category_id' => 'required|integer',
                'type' => 'required|integer',
                'value' => 'required|numeric|min:0',
                'transfer' => 'required',
                'transfer_company_id' => 'required_if:transfer,1'
            ];

            if ($user->hasPermission('aid.voucher.updateSerial')) {
                $rules['un_serial']='required';
                $rules['collected']='required';

                if(!($request->un_serial == null || $request->un_serial== 'null')){
                    if($request->un_serial){
                        $un_count = Vouchers::where(['un_serial'=>$request->un_serial])->count();
                        if($un_count > 0){
                            return response()->json(['error' => trans('aid::application.must_unique')], 422);
                        }
                    }
                }

            }

            if($request->get('project_id')){
                $input['project_id']=$request->get('project_id');

                $project = \AidRepository\Model\Project::with('Repository')->findOrFail($input['project_id']);

                $input['currency_id']=$project->currency_id;
                $input['exchange_rate']=$project->exchange_rate .'';
                $input['case_category_id']=$project->case_category_id;



                $repository = \AidRepository\Model\Repository::findOrFail($project->repository_id);
                if ($repository->status == \AidRepository\Model\Repository::STATUS_CLOSED) {
                    return response()->json(['error' => trans('aid::application.can not added voucher in closed repository')], 422);
                }

                if ($project->status == \AidRepository\Model\Project::STATUS_CLOSED) {
                    return response()->json(['error' => trans('aid::application.can not added voucher in closed project')], 422);
                }

                $organization_quantity = 0;
                $extra_quantity = 0;

                $ProjectOrganization = \AidRepository\Model\ProjectOrganization::where(['project_id' =>$input['project_id'] ,
                    'organization_id' => $user->organization_id])->first() ;
                if($ProjectOrganization){
                    $organization_quantity = $ProjectOrganization->quantity;
                }
                if ($user->organization_id == $project->organization_id) {
                    $extra_quantity = ($project->quantity * $project->organization_ratio)/100;
                }

                if($project->custom_organization == $user->custom_organization){
                    $custom_org = Organization::where('id', $project->custom_organization)->first();
                    if($project->custom_ratio){
                        if(!is_null($project->custom_ratio) && $project->custom_ratio != 0 && $project->custom_ratio != '0'){
                            $project_custom_ratio = $project->custom_ratio;
                            if($project_custom_ratio > 0){
                                $original_ration = $custom_org->container_share;
                                if($project_custom_ratio > $organization_quantity){
                                    $organization_quantity = (($project->quantity * $project->custom_ratio)/100);
                                }
                            }
                        }
                    }
                }

                $total = $organization_quantity + $extra_quantity ;

                if($total == 0){
                    return response()->json(['error' => trans('aid::application.dont have quantity on project to create voucher')], 422);
                }

                $ben_count = \AidRepository\Model\ProjectPerson::where([
                    'status'=> 3,
                    'project_id'=> $input['project_id'],
                    'organization_id'=>$user->organization_id])->count();

                if($ben_count != $total){
                    return response()->json(['error' => trans('aid::application.You cannot create a voucher before the required number of beneficiaries is approved')], 422);
                }
                $founded = Vouchers::where(['project_id' =>$input['project_id'] ,'organization_id'=> $user->organization_id])->count();
                if($founded > 0 ){
                    return response()->json(['error' => trans('aid::application.previously add voucher of project')], 422);
                }
                $input['organization_id']=$user->organization_id;
                $response['ben_count']=$ben_count;
                $response['total']=$total;
            }else{
                $rules['case_category_id'] = 'required';
                if($request->get('organization_id')){
                    $input['organization_id']=$request->get('organization_id');
                }else{
                    $input['organization_id']=$user->organization_id;
                }


            }

            if($request->get('urgent')){

                $input['count']=sizeof($request->get('persons'));

                if($request->get('case_id')) {
                    $isPass=VoucherPersons::checkPersons('id',$request->get('case_id'),null,$user->organization_id,$input['value'],$user->id);
                    if ($isPass['status'] != true) {

                        if ($isPass['reason'] =='not_same') {
                            $response["msg"] = trans('sponsorship::application.The Person was') . ' ' .trans('sponsorship::application.not_same');
                        }
                        elseif ($isPass['reason'] =='nominated') {
                            $response["msg"]=  trans('sponsorship::application.The Person was') . ' ' .trans('sponsorship::application.nominated');
                        }
                        elseif ($isPass['reason'] =='blocked') {
                            $response["msg"]=  trans('sponsorship::application.The Person was') . ' ' .trans('sponsorship::application.blocked');
                        }
                        elseif ($isPass['reason'] =='duplicated') {
                            $response["msg"]=  trans('sponsorship::application.The Person was') . ' ' .trans('sponsorship::application.duplicated');
                        }
                        elseif ($isPass['reason'] =='policy_restricted') {
                            $response["msg"]=  trans('sponsorship::application.The Person was') . ' ' .trans('sponsorship::application.policy_restricted');
                        }
                        elseif ($isPass['reason'] =='restricted_total_payment_persons') {
                            $response["msg"]=  trans('sponsorship::application.The Person was') . ' ' .trans('sponsorship::application.restricted_total_payment_persons');
                        }
                        elseif ($isPass['reason'] =='restricted_count_of_family') {
                            $response["msg"]= trans('sponsorship::application.The Person was') . ' ' . trans('sponsorship::application.restricted_count_of_family');
                        }
                        elseif ($isPass['reason'] =='restricted_amount') {
                            $response["msg"]= trans('sponsorship::application.The Person was') . ' ' . trans('sponsorship::application.restricted_amount');
                        }
                        elseif ($isPass['reason'] =='out_of_regions') {
                            $response["msg"]= trans('sponsorship::application.The Person was') . ' ' . trans('sponsorship::application.out_of_regions');
                        }
                        elseif ($isPass['reason'] =='out_of_ratio') {
                            $response["msg"]= trans('sponsorship::application.The Person was') . ' ' . trans('sponsorship::application.out_of_ratio');
                        }else{

                        }
                        $response["status"]= 'failed';
                        return response()->json($response);
                    }
                }

                $input['receipt_date']=strip_tags($request->get('receipt_date'));
                $input['receipt_time']=strip_tags($request->get('receipt_time'));
                $input['receipt_location']=strip_tags($request->get('receipt_location'));
                $input['urgent']=$request->get('urgent');
                $rules['receipt_date']='required';
                $rules['receipt_time']='required';
                $rules['receipt_location']='required';
            }


            $error = \App\Http\Helpers::isValid($input,$rules);
            if($error)
                return response()->json($error);

            if($request->get('urgent')) {
                unset($input['receipt_date']);
                unset($input['receipt_time']);
                unset($input['receipt_location']);
            }

            $input['voucher_date']=date('Y-m-d',strtotime($input['voucher_date']));

            if($request->get('project_id')) {
                if(Vouchers::where(['project_id' =>$input['project_id'] ,'organization_id'=> $user->organization_id])->first()){
                    $exist=true;
                }
            }

            if($request->transfer == '0' || $request->transfer == "0" || $request->transfer == 0) {
                $input['transfer_company_id'] = null;
            }


            if(!$exist){

                $user = \Auth::user();
                $district_id = $user->organization->district_id  ;

                $district_code = Helpers::getDistrictCode($user->organization_id,$district_id);
                $mSerial= $district_code.date('Y').date('m').'-';

                $voucher_cnt = Vouchers::query()
                    ->where(function ($q) use ($mSerial) {
                        $q->whereRaw("serial like ?",$mSerial. "%");
                    })
                    ->count();

                $voucher_cnt ++;
                $input['serial']= $mSerial.$voucher_cnt;

                $inserted=Vouchers::create($input);
            }

            if($inserted) {
                if($request->get('persons')){
                    if(!empty($request->get('persons'))){
                        foreach($request->get('persons') as $item){
                           VoucherPersons::create(['person_id'=>$item , 'status' => 2,
                                'voucher_id' => $inserted->id,
                                'receipt_date' =>date('Y-m-d',strtotime(strip_tags($request->get('receipt_date')))),
                                'receipt_time' =>$request->get('receipt_time'),
                                'receipt_location' => strip_tags($request->get('receipt_location'))
                            ]);

                        }
                    }
                }

                if($request->get('project_id')) {
                    $persons = \AidRepository\Model\ProjectPerson::where([
                        'status'=> 3,
                        'project_id'=> $input['project_id'],
                        'organization_id'=>$user->organization_id])->get();
                    if(!empty($persons)){
                        foreach($persons as $k=>$v){
                           VoucherPersons::create(['person_id'=>$v->person_id , 'status' => 2,
                                'voucher_id' => $inserted->id,
                                'receipt_date' =>date('Y-m-d',strtotime(strip_tags($request->get('receipt_date')))),
                                'receipt_time' =>$request->get('receipt_time'),
                                'receipt_location' => strip_tags($request->get('receipt_location'))
                            ]);

                        }
                    }
                }

                \Log\Model\Log::saveNewLog('VOUCHER_CREATED', trans('aid::application.added a new aid voucher') . ' "' . $input['title'] . '" ');
                $response["status"]= 'success';
                $response["msg"]= trans('setting::application.The row is inserted to db');

            }else{

                if($exist){
                    $response["msg"]= trans('aid::application.voucher for this project was created at last');
                }else{
                    $response["msg"]= trans('setting::application.The row is not insert to db');

                }

                $response["status"]= 'failed';

            }
            return response()->json($response);


        } catch (Exception $e) {
            return response()->json(['status' =>'error',"error_code" => $e->getCode() ,'msg'=>$e->getMessage() ]);
        }

    }

    // update voucher details
    public function update(Request $request, $id)
    {
        try {
            $response = array();

            $voucher = Vouchers::findOrFail($id);

            $this->authorize('update', $voucher);
            $response = array();
            $input=[
                'beneficiary' => strip_tags($request->get('beneficiary')),
                'title' => strip_tags($request->get('title')),
                'notes' => strip_tags($request->get('notes')),
                'content' => strip_tags($request->get('content')),
                'currency_id' => strip_tags($request->get('currency_id')),
                'exchange_rate' => strip_tags($request->get('exchange_rate')),
                'category_id' => strip_tags($request->get('category_id')),
                'type' => strip_tags($request->get('type')),
                'value' => strip_tags($request->get('value')),
                'sponsor_id' => strip_tags($request->get('sponsor_id')),
                'voucher_date' => strip_tags($request->get('voucher_date')),
                'allow_day' => strip_tags($request->get('allow_day')),
                'count' => strip_tags($request->get('count')),
                'case_category_id' => strip_tags($request->get('case_category_id')),
                'transfer_company_id' => strip_tags($request->get('transfer_company_id')),
                'center' => strip_tags($request->get('center')),
                'transfer' => strip_tags($request->get('transfer'))
            ];
            $rules= [
                'beneficiary' => 'required',
                'notes' => 'required',
                'title' => 'required|max:255',
                'voucher_date' => 'required',
                'currency_id' => 'required',
                'exchange_rate' => 'required',
                'sponsor_id' => 'required',
                'count' => 'required|integer',
                'content' => 'required',
                'category_id' => 'required|integer',
                'type' => 'required|integer',
                'value' => 'required|numeric|min:0',
                'transfer' => 'required',
                'transfer_company_id' => 'required_if:transfer,1'
            ];
            $error = \App\Http\Helpers::isValid($input,$rules);
            if($error)
                return response()->json($error);

            $input['voucher_date']=date('Y-m-d',strtotime($input['voucher_date']));

            if($request->transfer == '0' || $request->transfer == "0" || $request->transfer == 0) {
                $input['transfer_company_id'] = null;
            }

//                if( $voucher->count != $input['count']){
            $person_count =VoucherPersons::where(['voucher_id'=> $id])->count() ;
            if( $person_count > $input['count']){
                $response["status"]= 'failed';
                $response["msg"]= trans('aid::application.The voucher count must be equal or greater the beneficiary count');
                return response()->json($response);
            }
//                }



            if($voucher->update($input))
            {
                \Log\Model\Log::saveNewLog('VOUCHER_UPDATED',trans('aid::application.edited aid voucher')  . ' "' . $input['title'] . '" ');

                $response["status"]= 'success';
                $response["msg"]= trans('setting::application.The row is updated');
            }else{
                $response["status"]= 'failed';
                $response["msg"]= trans('setting::application.There is no update');
            }
            return response()->json($response);

        } catch (Exception $e) {
            return response()->json(['status' =>'error',"error_code" => $e->getCode() ,'msg'=>$e->getMessage() ]);
        }

    }

    // create voucher token (check benefit of voucher)
    public function createToken(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $voucher = Vouchers::findOrFail($request->voucher_id);
        $input = [];
        $input['token']= md5(uniqid());
        if($voucher->update($input)){
            $response['token_value'] = $input['token'];
            $response["status"]= 'success';
            $response["msg"]= trans('aid::application.The link is add');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('aid::application.There is no link add');
        }
        return response()->json($response);
    }

    // active or deactive voucher token
    public function activateDeactivate(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $voucher = Vouchers::findOrFail($request->voucher_id);
        $input = [];
        $input['valid_token']=$request->activate;

        if($request->activate){
            $success_msg = trans('aid::application.the link is activate');
        }else{
            $success_msg = trans('aid::application.the link is deactivate');
        }

        if($voucher->update($input)){
            $response["status"]= 'success';
            $response["valid_token"]= $input['valid_token'];
            $response["msg"]= $success_msg;
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('setting::application.There is no update');
        }
        return response()->json($response);
    }

    // get voucher by id
    public function show($id)
    {
        $entry = Vouchers::findOrFail($id);

        if($entry->exchange_rate){
            $entry->exchange_rate = (floatval($entry->exchange_rate));
        }

        $this->authorize('view', $entry);

        return response()->json($entry);
    }

    // reset voucher by id ( empty  beneficiary , attachments )
    public function setStatus(Request $request,$id)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $response = array();
        $voucher = Vouchers::findOrFail($id);
        $this->authorize('update', $voucher);

        $voucher->status = $request->status;

        $response =["status" => 'failed'];

        $count =VoucherPersons::where(['voucher_id' => $id ,'status' => 2 ])->count();
        if($count > 0){
            return response()->json(["status" => 'failed',
                "msg"=>trans('aid::application.There are a number of beneficiaries who did not receive the voucher') .' :(' .$count.')']);
        }
        if($voucher->save()){

            \Log\Model\Log::saveNewLog('VOUCHER_CLOSED',trans('aid::application.The User Close Voucher') . ' "' . $voucher->title. '" ');
            return response()->json(["status" => 'success',
                "msg"=>trans('aid::application.successfully closed voucher')]);
        }

        return response()->json(["status" => 'failed',
            "msg"=>trans('aid::application.can not close voucher')]);

    }

    // get vouchers bu un_serial
    public function getVouchers($id)
    {

        $user = \Auth::user();
        $entries = Vouchers::where(['un_serial' => $id ,
            'organization_id' => $user->organization_id ])->get();
        return response()->json(['list'=>$entries]);
    }

    // reset un_serial
    public function updateSerial(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $response = array();

        $user = \Auth::user();
        $old = $request->old;
        $new_un = $request->new_un;
        $vouchers = $request->vouchers;

        if($new_un == $old || sizeof($vouchers) == 0){
            return response()->json(["status" => 'failed', "msg"=>trans('aid::application.no vouchers to update')]);
        }

        $count = Vouchers::where(function($q) use ($user , $old ,$vouchers){
            $q->where('organization_id' ,$user->organization_id);
            $q->where('un_serial' ,$old);
            $q->whereIn('id' ,$vouchers);
        })->count();
        if($count == 0){
            return response()->json(["status" => 'failed', "msg"=>trans('aid::application.no vouchers to update')]);
        }

        $update = 0;
        foreach ($vouchers as $id){
            $voucher = Vouchers::findOrFail($id);
            $voucher->un_serial = $request->new_un ;
            if($voucher->save()){
                $update++;
                \Log\Model\Log::saveNewLog('VOUCHER_UPDATED',trans('aid::application.edited aid voucher')  . ' "' . $voucher->title . '" ');
            }
        }

        if($update > 0 ){
            return response()->json(["status" => 'success', "msg"=>trans('setting::application.The row is updated')]);
        }

        return response()->json(["status" => 'failed', "msg"=>trans('setting::application.There is no update')]);

    }

    // reset voucher by id ( empty  beneficiary , attachments )
    public function reset(Request $request,$id)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $response = array();
        $voucher = Vouchers::findOrFail($id);
        $this->authorize('update', $voucher);

        if($voucher->status == 1){
            $response = ['status' => 'success', 'msg'=> trans('aid::application.cant not empty a closed voucher') ];
        }else{
            $person_empty =true ;
            $documents_empty =true ;

            $person_count =VoucherPersons::where(['voucher_id'=> $id])->count() ;
            $documents_count =VoucherDocuments::where(['voucher_id'=> $id])->count() ;

            if($person_count > 0 || $documents_count > 0 ){
                if(VoucherPersons::where(['voucher_id'=> $id])->count() >0 ) {
                    if(VoucherPersons::where(['voucher_id'=> $id])->delete()) {
                        $person_empty =true ;
                    }else{
                        $person_empty =false ;
                    }
                }
                if( VoucherDocuments::where(['voucher_id'=> $id])->count() >0 ) {
                    if( VoucherDocuments::where(['voucher_id'=> $id])->delete()) {
                        $documents_empty =true ;
                    }else{
                        $documents_empty =false ;
                    }

                }

                if( $person_empty && $documents_empty ) {

                    $response["status"]= 'success';
                    $response["msg"]= trans('aid::application.successfully empty closed voucher');
                    \Log\Model\Log::saveNewLog('VOUCHER_UPDATED',trans('aid::application.Empty aid voucher from beneficiaries and attachments') . ' "' . $voucher->title. '" ');
                }else{
                    $response["status"]= 'failed';
                    $response["msg"]= trans('aid::application.An error occurred during during empty closed voucher');
                }

            }
            else{
                $response = ['status' => 'success', 'msg'=> trans('aid::application.The voucher is empty')];
            }

        }


        return response()->json($response);
    }

    // destroy voucher by id ( destroy  beneficiary , attachments )
    public function destroy($id,Request $request)
    {

        $empty = $request->empty;
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $this->authorize('delete',Vouchers::class);

        if($request->parant){
            if(VoucherDocuments::where(['document_id'=>$id,'voucher_id'=>$request->parant])->delete()) {
                $voucher = Vouchers::findOrFail($request->parant);
                $title = $voucher->title;
                Helpers::deleteFile($id);
                \Log\Model\Log::saveNewLog('VOUCHER_DOCUMENT_DELETE',trans('aid::application.Deleted voucher document') .' : ' . $title);
                return response()->json(['status' => 'success' ,'msg' =>trans('aid::application.The row is deleted from db')]);
            }
        }else{

            if ($empty){
//                VoucherDocuments::where('voucher_id',$id)->delete();
//               VoucherPersons::where('voucher_id',$id)->delete();

                $voucher = Vouchers::findOrFail($id);
                $title = $voucher->title;
                if(Vouchers::destroy($id))
                {
                    OrganizationProject::where('voucher_id',$id)->update(['voucher_id' => null]);
                    \Log\Model\Log::saveNewLog('VOUCHER_DELETED',trans('aid::application.Deleted voucher titled') .' : ' . $title);
                    return response()->json(['status' => 'success' , 'msg' => trans('common::application.action success')]);
                }
            }else{
//                $attachment = VoucherDocuments::where('voucher_id',$id)->count();
//                $persons =VoucherPersons::where('voucher_id',$id)->count();
//                if($attachment != 0 || $persons != 0){
//                    return response()->json(['status' => 'error' , 'msg' => trans('aid::application.you must empty the voucher from beneficiaries and attachments to delete it') ]);
//                }else{
                    $voucher = Vouchers::findOrFail($id);
                    $title = $voucher->title;
                    if(Vouchers::destroy($id))
                    {
                        OrganizationProject::where('voucher_id',$id)->update(['voucher_id' => null]);
                        \Log\Model\Log::saveNewLog('VOUCHER_DELETED',trans('aid::application.Deleted voucher titled') .' : ' . $title);
                        return response()->json(['status' => 'success' , 'msg' => trans('common::application.action success')]);
                    }
//                }
            }

        }
        return response()->json(['status' =>'failed','msg'=>trans('common::application.An error occurred during during process') ]);
    }

    //----------------------------------------------------------------------------------------------------

    // list voucher beneficiary_id by voucher_id
    public function getPersonsList($id)
    {
        return response()->json(['status' => true , 'persons'=>VoucherPersons::listPerson($id)]);
    }

    // filter voucher beneficiary by voucher_id
    public function persons(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        //        $this->authorize('managePersonsVoucher', Vouchers::class);
        $voucher = Vouchers::fetch($request->voucher_id);
        $voucher->is_mine = false;
        $user = \Auth::user();

        if($voucher->organization_id == $user->organization_id){
            $voucher->is_mine = true;
        }
        $options = ['organization_id'=> $user->organization_id,'voucher_id'=> $request->voucher_id];

        if($request->target != 0){

            if($request->action == 'xls'){

                $options['action'] = $request->action;
                $options['target'] = $request->target;
                $options['beneficiary'] = $voucher->beneficiary_;
                $return =VoucherPersons::filter($options);
                $data= $return['receipt'];
                $beneficiary= $return['beneficiary'];
                $rows=[];
                foreach($beneficiary as $key =>$value){
                    $value->shekel_value = round($value->shekel_value,0);
                    $rows[$key][trans('aid::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        $rows[$key][trans('aid::application.'. $k)]= $v;
                    }
                }

                $token = md5(uniqid());
                \Excel::create('export_' . $token, function ($excel) use ($rows,$data,$voucher) {
                    $excel->sheet(trans('aid::application.beneficiary_sheet'), function ($sheet) use ($rows,$data,$voucher) {
                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Simplified Arabic',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $style = [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' => [
                                'name' => 'Simplified Arabic',
                                'size' => 12,
                                'bold' => true
                            ]
                        ];

                        $sheet->getStyle("A1:N1")->applyFromArray($style);
                        $sheet->setHeight(1, 30);
                        $style = [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' => [
                                'name' => 'Simplified Arabic',
                                'size' => 11,
//                                'bold' => true
                            ]
                        ];
                        $sheet->getDefaultStyle()->applyFromArray($style);
                        $sheet->fromArray($rows);

                    });
                    $excel->sheet(trans('aid::application.receipt_sheet'), function($sheet) use ($data , $voucher){

                        $sheet->setOrientation('landscape');
                        $sheet->setRightToLeft(true);

                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Calibri',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $sheet->setHeight(1,35);

                        $sheet->getStyle("A1:K1")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 12, 'bold'=>  true ]
                        ]);

                        $sheet->getStyle('A')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('B')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('C')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('D')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('E')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('F')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('G')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('H')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('I')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('J')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('K')->getAlignment()->setWrapText(true);
                        $sheet->setCellValue('A1','#');
                        $sheet->setCellValue('B1',trans('aid::application.name of head of family'));
                        $sheet->setCellValue('C1',trans('aid::application.id_card_number'));
                        $sheet->setCellValue('D1',trans('aid::application.family_cnt'));
                        $sheet->setCellValue('E1',trans('aid::application.voucher_value'));
                        $sheet->setCellValue('F1',trans('aid::application.shekel_v'));
                        $sheet->setCellValue('G1',trans('aid::application.primary_mobile'));
                        $sheet->setCellValue('H1',trans('aid::application.date'));
                        $sheet->setCellValue('I1',trans('aid::application.place'));
                        $sheet->setCellValue('J1',trans('aid::application.voucher_serial'));
                        $sheet->setCellValue('K1',trans('aid::application.receipt_status'));
                        $sheet->getStyle('A1:K1')->getAlignment()->setVertical('center');
                        $sheet->getStyle('A1:K1')->getAlignment()->setHorizontal( 'center' );

                        $sheet->getStyle("A1:K1")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 12, 'bold'=>  true ]
                        ]);

                        $count = 1;
                        $z = 2;
                        foreach ($data as $key => $current) {
                            $sheet->setCellValue('A' . $z, $count);
                            $sheet->setCellValue('B' . $z, $current->name);
                            $sheet->setCellValue('C' . $z, $current->id_card_number );
                            $sheet->setCellValue('D' . $z, $current->family_count );
                            $sheet->setCellValue('E' . $z, $current->voucher_value * $current->receipt_count );
                            $sheet->setCellValue('F' . $z, round(($current->shekel_value * $current->receipt_count),0));
                            $sheet->setCellValue('G' . $z, $current->primary_mobile );
                            $sheet->setCellValue('H' . $z, $current->receipt_date );
                            $sheet->setCellValue('I' . $z, $current->receipt_location );
                            $sheet->setCellValue('J' . $z, $current->voucher_serial);
                            $sheet->setCellValue('K' . $z, $current->receipt_status_name );
                            $sheet->getStyle('A'. $z .':K' . $z)->getAlignment()->setHorizontal('center');
                            $sheet->getStyle('A'. $z .':K' . $z)->getAlignment()->setVertical('center');
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $count++;
                            $z++;
                        }

                        $styleArray = array(
                            'font' => array(
                                'bold' => true,
                            ),
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'borders' => array('top'     => array('style' => 'medium'),
                                'left'   => array('style' => 'medium'),
                                'right'  => array('style' => 'medium'),
                                'bottom' => array('style' => 'medium')

                            ),
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'cce6ff')

                            ),
                        );
                        $sheet->getStyle('A1:K1')->applyFromArray($styleArray);

                        $styleArray = array(
                            'borders' => array(
                                'allborders'    => array('style' => 'medium')
                            ),

                        );
                        $max = sizeof($data) + 1;
                        $sheet->getStyle('A1:K'.$max)->applyFromArray($styleArray);

                        $indicies = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K');
                        $indWidth =array('A'=>7 ,'B'=> 27,'C'=> 13,'D'=> 7,'E'=> 7,'F'=> 7,'G'=> 13, 'H'=>13,'I'=> 13,'J'=> 15,'K'=> 13);
                        foreach ($indicies as $index) {
                            $sheet->getColumnDimension($index)->setAutoSize(false);
                            $sheet->getColumnDimension($index)->setWidth($indWidth[$index]);

                        }
                    });
                    $excel->sheet(trans('aid::application.signature_sheet'), function($sheet) use ($data , $voucher){

                        $sheet->setOrientation('landscape');
                        $sheet->setRightToLeft(true);

                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Calibri',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $sheet->setHeight(1,35);
                        $sheet->setWidth([
                            'A'=>7 ,'B'=> 25,'C'=> 15,'D'=> 15,'F'=> 15,'E'=> 15,'G'=> 15,
                            'H'=>15,'I'=> 15,'J'=> 25,'K'=> 15,'L'=> 15,'M'=> 15,'N'=> 15,
                            'O'=>15,'P'=> 15,'Q'=> 15,'R'=> 15,'S'=> 15,'T'=> 15,'U'=> 15,
                            'V'=>15,'W'=> 15,'X'=> 15,'Y'=> 15,'Z'=> 15,
                            'AA'=>15 ,'AB'=> 25,'AC'=> 15,'AD'=> 15,'AF'=> 25,'AE'=> 11,'AG'=> 19,'AH'=> 30
                        ]);

                        $sheet->getStyle("A1:AH1")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 12, 'bold'=>  true ]
                        ]);

                        if(!is_null($voucher->organization_logo) && ( $voucher->organization_logo != '')){
                            $drawing0 = new \PHPExcel_Worksheet_Drawing;
                            $drawing0->setPath( base_path('storage/app/' . $voucher->organization_logo)); //your image path
                            $drawing0->setCoordinates('A1');
                            $drawing0->setWorksheet($sheet);
                            $drawing0->setResizeProportional();
                            $drawing0->setOffsetX($drawing0->getWidth() - $drawing0->getWidth() / 5);
                            $drawing0->setOffsetY(10);

                            // Set height and width
                            $drawing0->setWidth(150);
                            $drawing0->setHeight(150);
                        }

                        if(!is_null($voucher->sponsor_logo) && ( $voucher->sponsor_logo != '')){

                            $drawing = new \PHPExcel_Worksheet_Drawing;
                            $drawing->setPath( base_path('storage/app/' . $voucher->sponsor_logo)); //your image path
                            $drawing->setCoordinates('H1');
                            $drawing->setWorksheet($sheet);
                            $drawing->setResizeProportional();
                            $drawing->setOffsetX($drawing->getWidth() - $drawing->getWidth() / 5);
                            $drawing->setOffsetY(10);

                            // Set height and width
                            $drawing->setWidth(150);
                            $drawing->setHeight(150);
                        }


                        $sheet->getStyle('A')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('B')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('C')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('D')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('E')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('F')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('G')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('H')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('I')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('J')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('K')->getAlignment()->setWrapText(true);

                        $sheet->setCellValue('C2',$voucher->organization_name);
                        $sheet->mergeCells( 'C2:G2' );
                        $sheet->getStyle('C2:G2')->getAlignment()->setVertical('center');
                        $sheet->getStyle('C2:G2')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('C3',$voucher->title);
                        $sheet->mergeCells( 'C3:G3' );
                        $sheet->getStyle('C3:G3')->getAlignment()->setVertical('center');
                        $sheet->getStyle('C3:G3')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('C4',$voucher->sponsor_name);
                        $sheet->mergeCells( 'C4:G4' );
                        $sheet->getStyle('C4:G4')->getAlignment()->setVertical('center');
                        $sheet->getStyle('C4:G4')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('A5',trans('aid::application.day') . ' : '. '-----------------');
                        $sheet->mergeCells( 'A5:E5' );
                        $sheet->getStyle('A5:E5')->getAlignment()->setVertical('center');
                        $sheet->getStyle('A5:E5')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('F5',trans('aid::application.date'). ' -----/------/---------- ');
                        $sheet->mergeCells( 'F5:K5' );
                        $sheet->getStyle('F5:K5')->getAlignment()->setVertical('center');
                        $sheet->getStyle('F5:K5')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('A6','#');
                        $sheet->setCellValue('B6',trans('aid::application.name of head of family'));
                        $sheet->setCellValue('C6',trans('aid::application.id_card_number'));
                        $sheet->setCellValue('D6',trans('aid::application.family_cnt'));
                        $sheet->setCellValue('E6',trans('aid::application.voucher_value'));
                        $sheet->setCellValue('F6',trans('aid::application.shekel_v'));
                        $sheet->setCellValue('G6',trans('aid::application.primary_mobile'));
                        $sheet->setCellValue('H6',trans('aid::application.date'));
                        $sheet->setCellValue('I6',trans('aid::application.place'));
                        $sheet->setCellValue('J6',trans('aid::application.voucher_serial'));
                        $sheet->setCellValue('K6',trans('aid::application.signature'));
                        $sheet->getStyle('A6:K6')->getAlignment()->setVertical('center');
                        $sheet->getStyle('A6:K6')->getAlignment()->setHorizontal( 'center' );

                        $sheet->getStyle("A1:K6")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 12, 'bold'=>  true ]
                        ]);

                        $count = 1;
                        $z = 7;
                        foreach ($data as $key => $current) {
                            $sheet->setCellValue('A' . $z, $count);
                            $sheet->setCellValue('B' . $z, $current->name);
                            $sheet->setCellValue('C' . $z, $current->id_card_number );
                            $sheet->setCellValue('D' . $z, $current->family_count );
                            $sheet->setCellValue('E' . $z, $current->voucher_value * $current->receipt_count );
                            $sheet->setCellValue('F' . $z, $current->shekel_value * $current->receipt_count );
                            $sheet->setCellValue('G' . $z, $current->primary_mobile );
                            $sheet->setCellValue('H' . $z, $current->receipt_date );
                            $sheet->setCellValue('I' . $z, $current->receipt_location );
                            $sheet->setCellValue('J' . $z, $current->voucher_serial);
                            $sheet->setCellValue('K' . $z, ' ');
                            $sheet->getStyle('A'. $z .':K' . $z)->getAlignment()->setHorizontal('center');
                            $sheet->getStyle('A'. $z .':K' . $z)->getAlignment()->setVertical('center');
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $count++;
                            $z++;
                        }

                        $styleArray = array(
                            'font' => array(
                                'bold' => true,
                            ),
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'borders' => array('top'     => array('style' => 'medium'),
                                'left'   => array('style' => 'medium'),
                                'right'  => array('style' => 'medium'),
                                'bottom' => array('style' => 'medium')

                            ),
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'cce6ff')

                            ),
                        );
                        $sheet->getStyle('A6:K6')->applyFromArray($styleArray);

                        $styleArray = array(
                            'borders' => array(
                                'allborders'    => array('style' => 'medium')
                            ),

                        );
                        $max = sizeof($data) + 6;
                        $sheet->getStyle('A6:K'.$max)->applyFromArray($styleArray);

                        $indicies = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K');
                        $indWidth =array('A'=>7 ,'B'=> 27,'C'=> 13,'D'=> 7,'E'=> 7,'F'=> 7,'G'=> 13, 'H'=>13,'I'=> 13,'J'=> 15,'K'=> 13);
                        foreach ($indicies as $index) {
                            $sheet->getColumnDimension($index)->setAutoSize(false);
                            $sheet->getColumnDimension($index)->setWidth($indWidth[$index]);

                        }
                        $sheet->getRowDimension(1)->setRowHeight(35);
                        $sheet->getRowDimension(2)->setRowHeight(45);
                        $sheet->getRowDimension(5)->setRowHeight(45);
                        $sheet->getRowDimension(6)->setRowHeight(55);

                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['download_token'=>$token]);

            }
            else{

                PDF::reset();
                PDF::SetTitle(trans('aid::application.Maturity statements'));
                PDF::SetFont('aefurat', '', 18);
                PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                PDF::SetAutoPageBreak(TRUE);
                PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);
                PDF::setCellHeightRatio(1.5);
                PDF::SetMargins(5, 15, 5);
                PDF::SetHeaderMargin(5);
                PDF::SetFooterMargin(5);
                PDF::SetAutoPageBreak(TRUE);

                $options['action'] = $request->action;
                $options['target'] = $request->target;
                $options['beneficiary'] = $voucher->beneficiary_;
                $options['all'] = true;
                $options['offset'] = null;

                $beneficiary =VoucherPersons::filter($options);
                $count = sizeof($beneficiary);
                $ceil=ceil($count/17);

                $indx = 1;
                for ($x = 0; $x < $ceil; $x++) {
                    $curOffset = ($x) * 17;
                    $html = '<!doctype html>
                     <html lang="ar">
                         <head>
                               <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                                 <style>
                                    .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                     table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;}
                                     * , body {font-weight:normal !important; font-size: 10px;}
                                     .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                     .cell-style{text-align: center !important; border: 1px solid black;}
                                     .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                     .vertical-middle{ vertical-align: middle !important;}
                                </style>
                        </head>
                        <body style="font-weight: normal !important;">';


                    $html .= '<table width = "533" border="1" style="box-sizing:border-box;">';
                    $imgOrg = base_path('storage/app/'.$voucher->organization_logo);
                    $imgSpo = base_path('storage/app/'.$voucher->sponsor_logo);

                    $html .= ' <tr style="line-height: 47px;font-size: 14px; height: 70px; border:0.5px solid black !important; ">
                                  <td rowspan="3" align="center"     valign="middle" > 
                                        <br><br><img src="'.$imgOrg.'" style=" width: 80px;height: 70px;display: block;margin-left: auto;margin-right: auto;">
                                    </td>
                                     <td align="center" colspan="2" style="font-size:12pt !important; width: 295px; ">' . trans('aid::application.finance').' /'.$voucher->sponsor_name.'</td>
                                    <td rowspan="3" align="center"     valign="middle" > 
                                       <br><br><img src="'.$imgSpo.'" style=" width: 80px;height: 70px;display: block;margin-left: auto;margin-right: auto;">
                                    </td>
                       </tr>
                       <tr style=" line-height: 40px;  font-size: 14px; height: 57px; ">
                         <td align="center" colspan="2" style="font-size:12pt !important;  "> '.$voucher->title.'</td>
                        </tr>
                        <tr style=" line-height: 40px; font-size: 14px; height: 57px;   ">
                             <td align="center" colspan="2" style="font-size:12pt !important;">' . trans('aid::application.Implementation of').' /'.$voucher->organization_name.'</td>
                        </tr>';

                    $html .= '</table>';
                    $html .= '<span><br></span>';
                    $html .= '<table width = "533" border="1" style="box-sizing:border-box;">';
                    $html .= ' <tr style="line-height: 40px;font-size: 14px; height: 60px; border:0.5px solid black !important; ">
                                    <td align="center" style="font-size:12pt !important; width: 140px; ">'.trans('aid::application.day').'</td>
                                    <td align="center" style="font-size:12pt !important; width: 140px; "> ------------------ </td>
                                    <td align="center" style="font-size:12pt !important; width: 140px; ">'.trans('aid::application.date').'</td>
                                    <td align="center" style="font-size:12pt !important; width: 140px; "> '.date('d/m/Y').'</td>
                                </tr>';

                    $html .= '</table>';
                    $html.= '<br><br>';
                    $index=$curOffset;
                    $html .=  '<table  cellpadding="pixels" style="   width:100%
                                                display: table;
                                                text-align: center;
                                                margin-bottom: 20px;
                                                border-collapse: collapse;
                                                border-spacing: 0;
                                              ">
                                    <thead>
                                    <tr style="height:40px; line-height: 25px; background-color: #e4e4e4">
                                        <th class="heading-style" style="width: 30px; "> # </th>
                                        <th class="heading-style" style="width: 130px; ">' .trans('aid::application.name of head of family') .'</th>
                                        <th class="heading-style" style="width: 55px; "> '.trans('aid::application.id_card_number') .'</th>
                                        <th class="heading-style" style="width: 55px; "> '.trans('aid::application.primary_mobile') .'</th>
                                        <th class="heading-style" style="width: 40px; "> '.trans('aid::application.value') .'</th>
                                        <th class="heading-style" style="width: 55px; "> '.trans('aid::application.date') .'</th>
                                        <th class="heading-style" style="width: 100px; "> '.trans('aid::application.place') .'</th>
                                        <th class="heading-style" style="width: 100px; ">'.trans('sponsorship::application.signature') .'</th>
                                    </tr>
                                    </thead> <tbody>';

                    if($index < sizeof($beneficiary)) {
                        for ($z = 0; $z < 17; $z++) {
                            if($index < $count){
                                $v1 = $beneficiary[$index];
                                $name      = is_null($v1->name) ? '-' : $v1->name;
                                $id_card_number  = is_null($v1->id_card_number) ? '-' : $v1->id_card_number;
                                $primary_mobile  = is_null($v1->primary_mobile) ? '-' : $v1->primary_mobile;
                                $shekel_amount  = is_null($v1->shekel_value) ? '-' : ($v1->shekel_value * $v1->receipt_count);
                                $receipt_date  = is_null($v1->receipt_date) ? '-' : $v1->receipt_date;
                                $receipt_location  = is_null($v1->receipt_location) ? '-' : $v1->receipt_location;


                                $html .='<tr style="height:20px; line-height: 25px; ">
                                                        <td  class="cell-style" style="width:30px;  ">'.$indx .'</td>
                                                        <td  class="cell-style" style="width:130px;  ">'.$name .'</td>
                                                        <td  class="cell-style" style="width:55px; ">'.$id_card_number .'</td>
                                                        <td  class="cell-style" style="width:55px;  ">'.$primary_mobile.'</td>
                                                        <td  class="cell-style" style="width:40px;  "> '.$shekel_amount.' </td>
                                                        <td  class="cell-style" style="width:55px;  ">'.$receipt_date .'</td>
                                                        <td  class="cell-style" style="width:100px;  ">'.$receipt_location .'</td>
                                                        <td  class="cell-style" style="width:100px;  "> </td>
                                                    </tr>';

                                $index++;
                                $indx++;
                            }
                        }
                    }
                    $html .= '</tbody></table>';
                    $html .= ' </body></html>';

                    PDF::SetFooterMargin(0);
                    PDF::AddPage('P','A4');
                    PDF::setFooterCallback(function($pdf) use ($x , $ceil){
                        $footer = '<p style="text-align: left"> (  ' . ( $x+1 ) .' \ '. $ceil.' ) </p>';
                        PDF::writeHTMLCell(0, 0, '', 270, $footer, 0, 1, 0, true, '', true);
                    });

                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

                }

                $token = md5(uniqid());

                $dirName = storage_path('tmp');
                if (!is_dir($dirName)) {
                    @mkdir($dirName, 0777, true);
                }

                PDF::Output($dirName.'/export_' . $token . '.pdf', 'F');
                return response()->json(['download_token'=>$token]);

            }
        }

        if(isset($request->itemsCount)){
            $options['itemsCount'] =  $request->itemsCount;
        }
        $options['page'] =  $request->page;
        $options['action'] =  'filter';
        return response()->json(['beneficiary'=> $voucher->beneficiary_,
            'voucher'=> $voucher->title,
            'is_mine'=> $voucher->is_mine,
            'status'=> $voucher->status, 'voucher_id'=>$request->voucher_id,
            'Beneficiary'=>VoucherPersons::filter($options)]);

    }

    // filter voucher beneficiary by un_serial
    public function statistics(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $user = \Auth::user();

        $options = ['organization_id'=> $user->organization_id,'un_serial'=> $request->un_serial];
        $un_serial = $request->un_serial ;
        $is_mine = true ;

        $count = Vouchers::where(function($q) use ($user ,$un_serial){
            $q->where('un_serial' ,$un_serial);
            $q->where('organization_id' ,'!=',$user->organization_id);
        })->count();

        if($count != 0){
            $is_mine = false ;
        }

        if($request->target != 0){

            if($request->action == 'xls'){

                $options['action'] = $request->action;
                $options['target'] = $request->target;
                $return =VoucherPersons::filter($options);
                $data= $return['receipt'];
                $beneficiary= $return['beneficiary'];
                $rows=[];
                foreach($beneficiary as $key =>$value){
                    $value->shekel_value = round($value->shekel_value,0);
                    $rows[$key][trans('aid::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        $rows[$key][trans('aid::application.'. $k)]= $v;
                    }
                }

                $token = md5(uniqid());
                \Excel::create('export_' . $token, function ($excel) use ($rows,$data,$un_serial) {
                    $excel->sheet(trans('aid::application.beneficiary_sheet'), function ($sheet) use ($rows,$data) {
                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Simplified Arabic',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $style = [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' => [
                                'name' => 'Simplified Arabic',
                                'size' => 12,
                                'bold' => true
                            ]
                        ];

                        $sheet->getStyle("A1:N1")->applyFromArray($style);
                        $sheet->setHeight(1, 30);
                        $style = [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' => [
                                'name' => 'Simplified Arabic',
                                'size' => 11,
//                                'bold' => true
                            ]
                        ];
                        $sheet->getDefaultStyle()->applyFromArray($style);
                        $sheet->fromArray($rows);

                    });
                    $excel->sheet(trans('aid::application.receipt_sheet'), function($sheet) use ($data){

                        $sheet->setOrientation('landscape');
                        $sheet->setRightToLeft(true);

                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Calibri',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $sheet->setHeight(1,35);

                        $sheet->getStyle("A1:K1")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 12, 'bold'=>  true ]
                        ]);

                        $sheet->getStyle('A')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('B')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('C')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('D')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('E')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('F')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('G')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('H')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('I')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('J')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('K')->getAlignment()->setWrapText(true);
                        $sheet->setCellValue('A1','#');
                        $sheet->setCellValue('B1',trans('aid::application.name of head of family'));
                        $sheet->setCellValue('C1',trans('aid::application.id_card_number'));
                        $sheet->setCellValue('D1',trans('aid::application.family_cnt'));
                        $sheet->setCellValue('E1',trans('aid::application.voucher_value'));
                        $sheet->setCellValue('F1',trans('aid::application.shekel_v'));
                        $sheet->setCellValue('G1',trans('aid::application.primary_mobile'));
                        $sheet->setCellValue('H1',trans('aid::application.date'));
                        $sheet->setCellValue('I1',trans('aid::application.place'));
                        $sheet->setCellValue('J1',trans('aid::application.voucher_serial'));
                        $sheet->setCellValue('K1',trans('aid::application.receipt_status'));
                        $sheet->getStyle('A1:K1')->getAlignment()->setVertical('center');
                        $sheet->getStyle('A1:K1')->getAlignment()->setHorizontal( 'center' );

                        $sheet->getStyle("A1:K1")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 12, 'bold'=>  true ]
                        ]);

                        $count = 1;
                        $z = 2;
                        foreach ($data as $key => $current) {
                            $sheet->setCellValue('A' . $z, $count);
                            $sheet->setCellValue('B' . $z, $current->name);
                            $sheet->setCellValue('C' . $z, $current->id_card_number );
                            $sheet->setCellValue('D' . $z, $current->family_count );
                            $sheet->setCellValue('E' . $z, $current->voucher_value * $current->receipt_count );
                            $sheet->setCellValue('F' . $z, round(($current->shekel_value * $current->receipt_count),0));
                            $sheet->setCellValue('G' . $z, $current->primary_mobile );
                            $sheet->setCellValue('H' . $z, $current->receipt_date );
                            $sheet->setCellValue('I' . $z, $current->receipt_location );
                            $sheet->setCellValue('J' . $z, $current->voucher_serial);
                            $sheet->setCellValue('K' . $z, $current->receipt_status_name );
                            $sheet->getStyle('A'. $z .':K' . $z)->getAlignment()->setHorizontal('center');
                            $sheet->getStyle('A'. $z .':K' . $z)->getAlignment()->setVertical('center');
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $count++;
                            $z++;
                        }

                        $styleArray = array(
                            'font' => array(
                                'bold' => true,
                            ),
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'borders' => array('top'     => array('style' => 'medium'),
                                'left'   => array('style' => 'medium'),
                                'right'  => array('style' => 'medium'),
                                'bottom' => array('style' => 'medium')

                            ),
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'cce6ff')

                            ),
                        );
                        $sheet->getStyle('A1:K1')->applyFromArray($styleArray);

                        $styleArray = array(
                            'borders' => array(
                                'allborders'    => array('style' => 'medium')
                            ),

                        );
                        $max = sizeof($data) + 1;
                        $sheet->getStyle('A1:K'.$max)->applyFromArray($styleArray);

                        $indicies = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K');
                        $indWidth =array('A'=>7 ,'B'=> 27,'C'=> 13,'D'=> 7,'E'=> 7,'F'=> 7,'G'=> 13, 'H'=>13,'I'=> 13,'J'=> 15,'K'=> 13);
                        foreach ($indicies as $index) {
                            $sheet->getColumnDimension($index)->setAutoSize(false);
                            $sheet->getColumnDimension($index)->setWidth($indWidth[$index]);

                        }
                    });
                    $excel->sheet(trans('aid::application.signature_sheet'), function($sheet) use ($data,$un_serial){

                        $sheet->setOrientation('landscape');
                        $sheet->setRightToLeft(true);

                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Calibri',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $sheet->setHeight(1,35);
                        $sheet->setWidth([
                            'A'=>7 ,'B'=> 25,'C'=> 15,'D'=> 15,'F'=> 15,'E'=> 15,'G'=> 15,
                            'H'=>15,'I'=> 15,'J'=> 15,'K'=> 15,'L'=> 15,'M'=> 15,'N'=> 15,
                            'O'=>15,'P'=> 15,'Q'=> 15,'R'=> 15,'S'=> 15,'T'=> 15,'U'=> 15,
                            'V'=>15,'W'=> 15,'X'=> 15,'Y'=> 15,'Z'=> 15,
                            'AA'=>15 ,'AB'=> 25,'AC'=> 15,'AD'=> 15,'AF'=> 25,'AE'=> 11,'AG'=> 19,'AH'=> 30
                        ]);

                        $sheet->getStyle("A1:AH1")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 12, 'bold'=>  true ]
                        ]);

//                        if(!is_null($voucher->organization_logo) && ( $voucher->organization_logo != '')){
//                            $drawing0 = new \PHPExcel_Worksheet_Drawing;
//                            $drawing0->setPath( base_path('storage/app/' . $voucher->organization_logo)); //your image path
//                            $drawing0->setCoordinates('A1');
//                            $drawing0->setWorksheet($sheet);
//                            $drawing0->setResizeProportional();
//                            $drawing0->setOffsetX($drawing0->getWidth() - $drawing0->getWidth() / 5);
//                            $drawing0->setOffsetY(10);
//
//                            // Set height and width
//                            $drawing0->setWidth(150);
//                            $drawing0->setHeight(150);
//                        }

//                        if(!is_null($voucher->sponsor_logo) && ( $voucher->sponsor_logo != '')){
//
//                            $drawing = new \PHPExcel_Worksheet_Drawing;
//                            $drawing->setPath( base_path('storage/app/' . $voucher->sponsor_logo)); //your image path
//                            $drawing->setCoordinates('H1');
//                            $drawing->setWorksheet($sheet);
//                            $drawing->setResizeProportional();
//                            $drawing->setOffsetX($drawing->getWidth() - $drawing->getWidth() / 5);
//                            $drawing->setOffsetY(10);
//
//                            // Set height and width
//                            $drawing->setWidth(150);
//                            $drawing->setHeight(150);
//                        }


                        $sheet->getStyle('A')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('B')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('C')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('D')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('E')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('F')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('G')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('H')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('I')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('J')->getAlignment()->setWrapText(true);
                        $sheet->getStyle('K')->getAlignment()->setWrapText(true);

                        $sheet->setCellValue('C2',' ');
                        $sheet->mergeCells( 'C2:G2' );
                        $sheet->getStyle('C2:G2')->getAlignment()->setVertical('center');
                        $sheet->getStyle('C2:G2')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('C3',$un_serial);
                        $sheet->mergeCells( 'C3:G3' );
                        $sheet->getStyle('C3:G3')->getAlignment()->setVertical('center');
                        $sheet->getStyle('C3:G3')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('C4', ' ');
                        $sheet->mergeCells( 'C4:G4' );
                        $sheet->getStyle('C4:G4')->getAlignment()->setVertical('center');
                        $sheet->getStyle('C4:G4')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('A5',trans('aid::application.day') . ' : '. '-----------------');
                        $sheet->mergeCells( 'A5:E5' );
                        $sheet->getStyle('A5:E5')->getAlignment()->setVertical('center');
                        $sheet->getStyle('A5:E5')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('F5',trans('aid::application.date'). ' -----/------/---------- ');
                        $sheet->mergeCells( 'F5:K5' );
                        $sheet->getStyle('F5:K5')->getAlignment()->setVertical('center');
                        $sheet->getStyle('F5:K5')->getAlignment()->setHorizontal( 'center' );

                        $sheet->setCellValue('A6','#');
                        $sheet->setCellValue('B6',trans('aid::application.name of head of family'));
                        $sheet->setCellValue('C6',trans('aid::application.id_card_number'));
                        $sheet->setCellValue('D6',trans('aid::application.family_cnt'));
                        $sheet->setCellValue('E6',trans('aid::application.voucher_value'));
                        $sheet->setCellValue('F6',trans('aid::application.shekel_v'));
                        $sheet->setCellValue('G6',trans('aid::application.primary_mobile'));
                        $sheet->setCellValue('H6',trans('aid::application.date'));
                        $sheet->setCellValue('I6',trans('aid::application.place'));
                        $sheet->setCellValue('J6',trans('aid::application.voucher_serial'));
                        $sheet->setCellValue('K6',trans('aid::application.signature'));
                        $sheet->getStyle('A6:K6')->getAlignment()->setVertical('center');
                        $sheet->getStyle('A6:K6')->getAlignment()->setHorizontal( 'center' );

                        $sheet->getStyle("A1:K6")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 12, 'bold'=>  true ]
                        ]);

                        $count = 1;
                        $z = 7;
                        foreach ($data as $key => $current) {
                            $sheet->setCellValue('A' . $z, $count);
                            $sheet->setCellValue('B' . $z, $current->name);
                            $sheet->setCellValue('C' . $z, $current->id_card_number );
                            $sheet->setCellValue('D' . $z, $current->family_count );
                            $sheet->setCellValue('E' . $z, $current->voucher_value * $current->receipt_count );
                            $sheet->setCellValue('F' . $z, $current->shekel_value * $current->receipt_count );
                            $sheet->setCellValue('G' . $z, $current->primary_mobile );
                            $sheet->setCellValue('H' . $z, $current->receipt_date );
                            $sheet->setCellValue('I' . $z, $current->receipt_location );
                            $sheet->setCellValue('J' . $z, $current->voucher_serial);
                            $sheet->setCellValue('K' . $z, ' ');
                            $sheet->getStyle('A'. $z .':K' . $z)->getAlignment()->setHorizontal('center');
                            $sheet->getStyle('A'. $z .':K' . $z)->getAlignment()->setVertical('center');
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $count++;
                            $z++;
                        }

                        $styleArray = array(
                            'font' => array(
                                'bold' => true,
                            ),
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'borders' => array('top'     => array('style' => 'medium'),
                                'left'   => array('style' => 'medium'),
                                'right'  => array('style' => 'medium'),
                                'bottom' => array('style' => 'medium')

                            ),
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'cce6ff')

                            ),
                        );
                        $sheet->getStyle('A6:K6')->applyFromArray($styleArray);

                        $styleArray = array(
                            'borders' => array(
                                'allborders'    => array('style' => 'medium')
                            ),

                        );
                        $max = sizeof($data) + 6;
                        $sheet->getStyle('A6:K'.$max)->applyFromArray($styleArray);

                        $indicies = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K');
                        $indWidth =array('A'=>7 ,'B'=> 27,'C'=> 13,'D'=> 7,'E'=> 7,'F'=> 7,'G'=> 13, 'H'=>13,'I'=> 13,'J'=> 15,'K'=> 13);
                        foreach ($indicies as $index) {
                            $sheet->getColumnDimension($index)->setAutoSize(false);
                            $sheet->getColumnDimension($index)->setWidth($indWidth[$index]);

                        }
                        $sheet->getRowDimension(1)->setRowHeight(35);
                        $sheet->getRowDimension(2)->setRowHeight(45);
                        $sheet->getRowDimension(5)->setRowHeight(45);
                        $sheet->getRowDimension(6)->setRowHeight(55);

                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['download_token'=>$token]);

            }

        }

        if(isset($request->itemsCount)){
            $options['itemsCount'] =  $request->itemsCount;
        }
        $options['page'] =  $request->page;
        $options['action'] =  'filter';
        return response()->json([ 'is_mine'=> $is_mine,'un_serial'=> $request->un_serial,'Beneficiary'=>VoucherPersons::filter($options)]);
    }

    // update voucher status
    public function updateStatus(Request $request,$id)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $response = array();

        $this->authorize('updatePersonsVoucher', Vouchers::class);
        $voucher = Vouchers::findOrFail($id);
        $status=$request->status;

        $target =VoucherPersons::wherein('id',$request->target)->get();
        $count=0;

        foreach ($target as $key =>$value) {
            $entry =VoucherPersons::findOrFail($value->id);
            $entry->status = $status;
            if($entry->save()){
                $person = \Common\Model\Person::fetch(array('id' => $value->person_id));
                $name=$person->first_name.' '.$person->second_name.' '.$person->third_name.' '.$person->last_name;
                \Log\Model\Log::saveNewLog('PERSON_VOUCHER_UPDATED',trans('aid.application.edited the case to a beneficiary of an aid voucher') . ' "'.$voucher->title .'" - '. ' "'.$name. '" ');
                $count++;
            }
        }

        if($count > 0) {
            $response["status"]= 'success';
            $response["msg"]= trans('setting::application.The row is updated');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('setting::application.There is no update');
        }

        return response()->json($response);
    }

    // store individual as beneficiary of voucher  by voucher_id , person_id , individual_id
    public function individualVoucher(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $this->authorize('createPersonsVoucher', Vouchers::class);
        if($request->source == 'file'){

            $out_of_ratio = 0;

            ini_set('max_execution_time', 0);
            set_time_limit(0);
            Ini_set('memory_limit', '2048M');

            $voucher = Vouchers::findorfail($request->voucher_id);

            $to_date = date('Y-m-d', strtotime($voucher->voucher_date . ' + ' . $voucher->allow_day . ' days'));

            if (strtotime($to_date) > strtotime('now')) {
                $response["status"] = 'inserted_error';
                $response["msg"] = trans('aid::application.exceeded the number of days allowed to add beneficiaries to voucher');
                return response()->json($response);
            }

            $category_id = $voucher->case_category_id;
            $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$category_id)->first();

            $importFile = $request->file;
            if ($importFile->isValid()) {

                $path = $importFile->getRealPath();
                $excel = \PHPExcel_IOFactory::load($path);
                $sheets = $excel->getSheetNames();
                if(in_array('data', $sheets)) {
                    $first = Excel::selectSheets('data')->load($path)->first();
                    if (!is_null($first)) {
                        $keys = $first->keys()->toArray();

                        if(sizeof($keys) >0 ) {

                            $validFile = true;
                            $constraint = ["rkm_alhoy_rb_alasr","rkm_hoy_almaaal", "okt_alastlam"]; /*,"mkan_alastlam"*/
                            foreach ($constraint as $item_) {
                                if ( !in_array($item_, $keys) ) {
                                    $validFile = false;
                                    break;
                                }
                            }

                            if ($validFile) {
                                $records = \Excel::selectSheets('data')->load($path)->get();
                                $total = sizeof($records);
                                if ($total > 0) {
                                    $rules = [
                                        'voucher_id' => 'required|integer',
                                        'receipt_date' => 'required|date'
                                        , 'receipt_location' => 'required|max:255',
                                    ];

                                    $input = [
                                        'voucher_id' => $request->voucher_id,
                                        'receipt_date' => strip_tags($request->receipt_date),
                                        'receipt_location' => strip_tags($request->receipt_location)
                                    ];

                                    $error = \App\Http\Helpers::isValid($input, $rules);
                                    if ($error)
                                        return response()->json($error);

                                    $old_v =VoucherPersons::where(['voucher_id' => $input['voucher_id']])->count();
                                    $max = $voucher->count - $old_v;

                                    if ($max == 0) {
                                        $response["status"] = 'inserted_error';
                                        $response["msg"] = trans('aid::application.The voucher is max exceed persons');
                                        return response()->json($response);
                                    }

                                    $user = \Auth::user();
                                    $response = array();

                                    $invalid_card = 0;
                                    $out_of_ratio = 0;
                                    $not_person = 0;
                                    $not_case = 0;
                                    $blocked = 0;
                                    $out_of_ratio = 0;
                                    $out_of_regions = 0;
                                    $policy_restricted = 0;
                                    $duplicated = 0;
                                    $nominated = 0;
                                    $limited = 0;
                                    $success = 0;
                                    $processed = [];
                                    $passed = [];
                                    $result = [];
                                    $out_of_ratio=0;
                                    $out_of_ratio = 0;
                                    $add_cases = $request->add_cases;
                                    $beneficiaries = [];

                                    foreach ($records as $key => $value) {
                                        $id_card_number = (int) $value['rkm_alhoy_rb_alasr'];
                                        if (strlen($id_card_number) <= 9) {
                                            if (GovServices::checkCard($id_card_number)) {
                                                $isPass =VoucherPersons::checkPersons('id_card_number', $id_card_number, $voucher->id, $user->organization_id, $voucher->value,$user->id);
                                                if (!in_array($isPass['id'], $value['rkm_hoy_almaaal'])) {
                                                    if ($isPass['status'] == true) {
                                                        $temp = ['name' => $isPass['name'], 'id' => $isPass['id'], 'id_card_number' => $id_card_number , 'receipt_time'=>null];
                                                        $beneficiaries[] = ['name' => $isPass['name'], 'id_card_number' => $id_card_number, 'status' => trans('aid::application.old beneficiary')];
                                                        if (isset($value['okt_alastlam'])) {
                                                            if (!is_null($value['okt_alastlam']) && $value['okt_alastlam'] != ' ' && $value['okt_alastlam'] != '') {
                                                                if (is_object($value['okt_alastlam'])) {
                                                                    $temp['receipt_time'] = date('H:i:s',strtotime($value['okt_alastlam']));
                                                                }
                                                            }
                                                        }
                                                        if (isset($value['rkm_hoy_almaaal'])) {
                                                            if (!is_null($value['rkm_hoy_almaaal']) && $value['rkm_hoy_almaaal'] != ' ' && $value['rkm_hoy_almaaal'] != '') {
                                                                $individual = \DB::table('char_persons')
                                                                    ->where('char_persons.id_card_number',$value['rkm_hoy_almaaal'])->first();
                                                                $temp['individual_card_number'] = $value['rkm_hoy_almaaal'];
                                                                $temp['individual_name'] = $individual->fullname;
                                                                $temp['individual_id'] = $individual->id;
                                                            }
                                                        }

//                                                        if (isset($value['mkan_alastlam'])) {
//                                                            if (!is_null($value['mkan_alastlam']) && $value['mkan_alastlam'] != ' ' && $value['mkan_alastlam'] != '') {
//                                                                if (is_object($value['okt_alastlam'])) {
//                                                                    $temp['receipt_location'] = $value['mkan_alastlam'];
//                                                                }
//                                                            }
//                                                        }

                                                        $passed[] = $temp;
                                                        $success++;
                                                    } else {

                                                        if ($isPass['reason'] == 'not_case') {
//                                                            $result[] = ['individual_' => '', 'individual_card_number' => $value['rkm_hoy_almaaal'],'guardian' => $isPass['name'], 'id_card_number' => $id_card_number, 'reason' => 'not_case'];
//                                                            $not_case++;

                                                            $case=  \Common\Model\AidsCases::create(['person_id' =>  $isPass['id'],
                                                                'status' => 1, 'category_id' => $category_id,
                                                                'created_at' => date('Y-m-d H:i:s'),
                                                                'user_id' => $user->id,
                                                                'rank' => 0,
                                                                'organization_id' => $user->organization_id]);

                                                            \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id, 'reason'=>trans('common::application.case was created'), 'status'=>0, 'date'=>date("Y-m-d")]);
                                                            \Log\Model\Log::saveNewLog('CASE_CREATED',trans('common::application.Has added new case data for')  . '  : '.' "'. ' ' .  $isPass['name'] . ' " ' . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);

                                                            $temp = ['name' => $isPass['name'], 'id' => $isPass['id'], 'id_card_number' => $id_card_number , 'receipt_time'=>null];
                                                            if (isset($value['okt_alastlam'])) {
                                                                if (!is_null($value['okt_alastlam']) && $value['okt_alastlam'] != ' ' && $value['okt_alastlam'] != '') {
                                                                    if (is_object($value['okt_alastlam'])) {
                                                                        $temp['receipt_time'] = date('H:i:s',strtotime($value['okt_alastlam']));
                                                                    }
                                                                }
                                                            }
                                                            if (isset($value['rkm_hoy_almaaal'])) {
                                                                if (!is_null($value['rkm_hoy_almaaal']) && $value['rkm_hoy_almaaal'] != ' ' && $value['rkm_hoy_almaaal'] != '') {
                                                                    $individual = \DB::table('char_persons')
                                                                        ->where('char_persons.id_card_number',$value['rkm_hoy_almaaal'])
                                                                        ->first();
                                                                    $temp['individual_card_number'] = $value['rkm_hoy_almaaal'];
                                                                    $temp['individual_name'] = $individual->fullname;
                                                                    $temp['individual_id'] = $individual->id;
                                                                }
                                                            }


                                                            if (isset($value['okt_alastlam'])) {
                                                                if (!is_null($value['okt_alastlam']) && $value['okt_alastlam'] != ' ' && $value['okt_alastlam'] != '') {
                                                                    if (is_object($value['okt_alastlam'])) {
                                                                        $temp['receipt_time'] = date('H:i:s',strtotime($value['okt_alastlam']));
                                                                    }
                                                                }
                                                            }

//                                                            if (isset($value['mkan_alastlam'])) {
//                                                                if (!is_null($value['mkan_alastlam']) && $value['mkan_alastlam'] != ' ' && $value['mkan_alastlam'] != '') {
//                                                                    if (is_object($value['okt_alastlam'])) {
//                                                                        $temp['receipt_location'] = $value['mkan_alastlam'];
//                                                                    }
//                                                                }
//                                                            }

                                                            $beneficiaries[] = ['name' => $isPass['name'], 'id_card_number' => $id_card_number, 'status' => trans('aid::application.old beneficiary')];
                                                            $passed[] = $temp;
                                                            $success++;
                                                        }

                                                        elseif ($isPass['reason'] == 'policy_restricted') {
                                                            $result[] = ['individual_' => '', 'individual_card_number' => $value['rkm_hoy_almaaal'],'guardian' => $isPass['name'], 'id_card_number' => $id_card_number, 'reason' => 'policy_restricted'];
                                                            $policy_restricted++;
                                                        }

                                                        elseif ($isPass['reason'] == 'out_of_regions') {
                                                            $result[] = ['individual_' => '', 'individual_card_number' => $value['rkm_hoy_almaaal'],'guardian' => $isPass['name'], 'id_card_number' => $id_card_number, 'reason' => 'out_of_regions'];
                                                            $out_of_regions++;
                                                        }

                                                        elseif ($isPass['reason'] == 'blocked') {
                                                            $result[] = ['individual_' => '', 'individual_card_number' => $value['rkm_hoy_almaaal'],'guardian' => $isPass['name'], 'id_card_number' => $id_card_number, 'reason' => 'blocked'];
                                                            $blocked++;
                                                        }

                                                        elseif ($isPass['reason'] == 'not_person') {
                                                            $result[] = ['individual_' => '', 'individual_card_number' => $value['rkm_hoy_almaaal'],'guardian' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'not_person'];
                                                            $not_person++;
                                                        }

                                                        elseif ($isPass['reason'] == 'nominated') {
                                                            $result[] = ['individual_' => '', 'individual_card_number' => $value['rkm_hoy_almaaal'],'guardian' => $isPass['name'], 'id_card_number' => $id_card_number, 'reason' => 'nominated'];
                                                            $nominated++;
                                                        }
                                                    }

                                                    $processed[] = $value['rkm_hoy_almaaal'];
                                                } else {
                                                    $result[] = ['individual_' => '', 'individual_card_number' => $value['rkm_hoy_almaaal'],'guardian' => $isPass['name'], 'id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                                                    $duplicated++;
                                                }
                                            } else {
                                                $result[] = ['individual_' => '', 'individual_card_number' => $value['rkm_hoy_almaaal'],'guardian' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'invalid_id_card_number'];
                                                $processed[] = $id_card_number;
                                                $invalid_card++;
                                            }
                                        } else {
                                            $result[] = ['individual_' => '', 'individual_card_number' => $value['rkm_hoy_almaaal'],'guardian' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'invalid_id_card_number'];
                                            $processed[] = $id_card_number;
                                            $invalid_card++;
                                        }
                                    }
                                    $total_restricted = sizeof($result);
//                                        $total_restricted = $invalid_card + $policy_restricted + $out_of_regions + $blocked + $not_case + $not_person + $nominated;

                                    if (($policy_restricted + $blocked) == sizeof($records)) {
                                        $response["status"] = 'inserted_error';
                                        $response["msg"] = trans('aid::application.The selected cases are not pass may not category blocked or policy restricted');
                                    }
                                    else if ($nominated == sizeof($records)) {
                                        $response["status"] = 'inserted_error';
                                        $response["msg"] = trans('aid::application.The selected cases are nominated');
                                    }
                                    else if ($out_of_regions == sizeof($records)) {
                                        $response["status"] = 'inserted_error';
                                        $response["msg"] = trans('aid::application.The selected cases are not pass may not in connector locations');
                                    }
                                    else if ($limited == sizeof($records)) {
                                        $response["status"] = 'inserted_error';
                                        $response["msg"] = trans('aid::application.The selected cases are limited');
                                    }

                                    else if ($total_restricted == sizeof($records)) {
                                        $response["status"] = 'inserted_error';
                                        $response["msg"] = trans('aid::application.The selected cases are restricted') . ' ( '
                                            . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                            trans('aid::application.success') . ' : ' . 0 . ' , ' .
                                            trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_card . ' , ' .
                                            trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                                            trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                                            trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                                            trans('aid::application.out_of_regions') . ' : ' . $out_of_regions . ' , ' .
                                            trans('aid::application.policy_restricted') . ' : ' . $policy_restricted . ' , ' .
                                            trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                                            trans('aid::application.out_of_ratio') . ' : ' . $out_of_ratio . ' , ' .
                                            trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                            trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                                    }else{

                                        if (sizeof($passed) != 0) {
                                            $receipt_location = $request->receipt_location;
                                            $receipt_date = date('Y-m-d', strtotime($request->receipt_date));

                                            $user = \Auth::user();
                                            $organization_id = $user->organization_id;

                                            $r = 0;
                                            $inserted_ = [];
                                            $limited_ = [];
                                            $pIds = [];
                                            $receipt_times = [];
                                            foreach ($passed as $person) {
                                                $pIds[] = $person['individual_id'];
                                                $receipt_times[$person['id']] = $person['receipt_time'];
                                            }
                                            $done = [];
                                            $mosques_limit = 0;
                                            $mosques_ratio = aidsLocation::getTotalCountTree($voucher->count,$voucher->id);
                                            foreach ($mosques_ratio as $k => $v) {
                                                $mosques_limit = $v->total - $v->count;
                                                $mosques_person = \DB::table('char_persons_kinship')
                                                    ->join('char_persons AS individual','individual.id','=','char_persons_kinship.r_person_id')
                                                    ->join('char_persons', 'char_persons.id', '=', 'char_persons_kinship.l_person_id')
                                                    ->where('char_persons.mosques_id', $v->location_id)
                                                    ->wherein('char_persons_kinship.r_person_id', $pIds)
                                                    ->groupBy('char_persons_kinship.r_person_id')
                                                    ->selectRaw("char_persons.id ,CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ', ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                        char_persons.mosques_id,char_persons.id_card_number,char_persons.old_id_card_number,
                                                        CONCAT(ifnull(individual.first_name, ' '), ' ' , ifnull(individual.second_name, ' '),' ',ifnull(individual.third_name,' '),' ', ifnull(individual.last_name,' '))as individual_name ,individual.id_card_number as individual_card_number,
                                                            char_persons_kinship.r_person_id")
                                                    ->get();

                                                if (sizeof($mosques_person) > 0) {
                                                    if ($mosques_limit > 0) {
                                                        $onMosque = [];
                                                        foreach ($mosques_person as $key => $value) {
                                                            $onMosque[] = $value->id;
                                                        }

                                                        $person = \DB::table('char_persons_kinship')
                                                            ->join('char_persons AS individual','individual.id','=','char_persons_kinship.r_person_id')
                                                            ->join('char_persons', 'char_persons.id', '=', 'char_persons_kinship.l_person_id')
                                                            ->where('char_persons.mosques_id', $v->location_id)
                                                            ->wherein('char_persons_kinship.l_person_id', $pIds)
                                                            ->groupBy('char_persons_kinship.r_person_id')
                                                            ->selectRaw("char_persons.id ,CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ', ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                            char_persons.mosques_id,char_persons.id_card_number,char_persons.old_id_card_number,
                                                            CONCAT(ifnull(individual.first_name, ' '), ' ' , ifnull(individual.second_name, ' '),' ',ifnull(individual.third_name,' '),' ', ifnull(individual.last_name,' '))as individual_name ,individual.id_card_number as individual_card_number,
                                                            char_persons_kinship.r_person_id")
                                                            ->limit($mosques_limit)
                                                            ->get();

                                                        if (sizeof($person) > 0) {
                                                            $person_ =[];
                                                            foreach ($person as $kk => $vv) {
                                                                $done[]=$vv->r_person_id;
                                                                $person_[]=$vv->r_person_id;
                                                                if ($old_v < $voucher->count) {
//                                                                    if ($old_v < $max) {
                                                                    $founded =VoucherPersons::where(['voucher_id' => $voucher->id,'individual_id' => $vv->r_person_id, 'person_id' => $vv->id])->first();
                                                                    if ($founded) {
                                                                        $result[] = ['individual_' =>  $vv->individual_name, 'individual_card_number' =>  $vv->individual_card_number,'guardian' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'nominated'];
                                                                        $nominated++;
                                                                    } else {

                                                                       VoucherPersons::create([ 'person_id' => $vv->id, 'status' => 2,
                                                                            'voucher_id' => $voucher->id,
                                                                            'individual_id' => $vv->r_person_id,
                                                                            'receipt_location' => $receipt_location,
                                                                            'receipt_date' => $receipt_date]);
                                                                        $inserted_[] = $vv->r_person_id;
                                                                        $beneficiaries[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'status' => trans('aid::application.old beneficiary')];
                                                                        $success++;
                                                                        $old_v++;
                                                                        $r++;
                                                                    }
                                                                } else {
                                                                    if(in_array($value->id, $limited_)) {
                                                                        $result[] = ['individual_' =>  $vv->individual_name, 'individual_card_number' =>  $vv->individual_card_number,'guardian' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'limited'];
                                                                        $limited_[] = $vv->r_person_id;
                                                                        $limited++;
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        $person_diff = array_diff($onMosque, $person_);

                                                        if (sizeof($person_diff) > 0) {
                                                            $person_ = \DB::table('char_persons_kinship')
                                                                ->join('char_persons AS individual','individual.id','=','char_persons_kinship.r_person_id')
                                                                ->join('char_persons', 'char_persons.id', '=', 'char_persons_kinship.l_person_id')
                                                                ->where('char_persons.mosques_id', $v->location_id)
                                                                ->wherein('char_persons_kinship.l_person_id', $person_diff)
                                                                ->groupBy('char_persons_kinship.r_person_id')
                                                                ->selectRaw("char_persons.id ,CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ', ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                                char_persons.mosques_id,char_persons.id_card_number,char_persons.old_id_card_number,
                                                                CONCAT(ifnull(individual.first_name, ' '), ' ' , ifnull(individual.second_name, ' '),' ',ifnull(individual.third_name,' '),' ', ifnull(individual.last_name,' '))as individual_name ,individual.id_card_number as individual_card_number,
                                                            char_persons_kinship.r_person_id")
                                                                ->get();


                                                            foreach ($person_ as $key => $value) {
                                                                if(in_array($value->id, $limited_)) {
                                                                    $result[] = ['individual_' =>  $value->individual_name, 'individual_card_number' =>  $value->individual_card_number,'guardian' => $value->full_name, 'id_card_number' => $value->id_card_number, 'reason' => 'out_of_ratio'];
                                                                    $done[] = $value->id;
                                                                    $out_of_ratio++;
                                                                }
                                                            }
                                                        }

                                                    }
                                                    else{
                                                        foreach ($mosques_person as $kk => $vv) {
                                                            $result[] = ['individual_' =>  $vv->individual_name, 'individual_card_number' =>  $vv->individual_card_number,'guardian' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'out_of_ratio'];
                                                            $done[]=$vv->r_person_id;
                                                            $out_of_ratio++;
                                                        }
                                                    }
                                                }
                                            }
                                            $pe = array_diff($pIds, $done);



                                            $out_per = \DB::table('char_persons_kinship')
                                                ->join('char_persons AS individual','individual.id','=','char_persons_kinship.r_person_id')
                                                ->join('char_persons', 'char_persons.id', '=', 'char_persons_kinship.l_person_id')
                                                ->wherein('char_persons_kinship.r_person_id', $pe)
                                                ->groupBy('char_persons_kinship.r_person_id')
                                                ->selectRaw("char_persons.id ,CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ', ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                    char_persons.mosques_id,char_persons.id_card_number,char_persons.old_id_card_number,
                                                    CONCAT(ifnull(individual.first_name, ' '), ' ' , ifnull(individual.second_name, ' '),' ',ifnull(individual.third_name,' '),' ', ifnull(individual.last_name,' '))as individual_name ,individual.id_card_number as individual_card_number,'char_persons_kinship.r_person_id'")
                                                ->get();

                                            foreach ($out_per as $kk => $vv) {
                                                $result[] = ['individual_' =>  $vv->individual_name, 'individual_card_number' =>  $vv->individual_card_number,'guardian' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'out_of_ratio'];
                                                $out_of_ratio++;
                                            }

                                            if ($r > 0) {
                                                if ($total_restricted != 0 && $limited != 0) {
                                                    $response["status"] = 'inserted_error';
                                                    $response["msg"] =
                                                        trans('aid::application.Some cases has been successfully nomination to voucher but another is restricted') . ' ( '
                                                        . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                                        trans('aid::application.success') . ' : ' . $r . ' , ' .
                                                        trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_card . ' , ' .
                                                        trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                                                        trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                                                        trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                                                        trans('aid::application.out_of_regions') . ' : ' . $out_of_regions . ' , ' .
                                                        trans('aid::application.policy_restricted') . ' : ' . $policy_restricted . ' , ' .
                                                        trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                                                        trans('aid::application.out_of_ratio') . ' : ' . $out_of_ratio . ' , ' .
                                                        trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                                        trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                                                    \Log\Model\Log::saveNewLog('PERSON_VOUCHER_CREATED', trans('aid::application.Added beneficiaries to the aid voucher:') . ' "' . $voucher->title . '"');

                                                } else {
                                                    $response["status"] = 'success';
                                                    $response["msg"] = trans('aid::application.The voucher row is inserted to db');
                                                }
                                            } else {

                                                if(sizeof($beneficiaries) > 0){
                                                    $response["status"] = 'inserted_error';
                                                    $response["msg"] = trans('aid::application.The selected cases are restricted') . ' ( '
                                                        . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                                        trans('aid::application.success') . ' : ' . sizeof($beneficiaries) . ' , ' .
                                                        trans('aid::application.register_as_new_case') . ' : ' . $case_added . ' , ' .
                                                        trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_card . ' , ' .
                                                        trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                                                        trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                                                        trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                                                        trans('aid::application.out_of_regions') . ' : ' . $out_of_regions . ' , ' .
                                                        trans('aid::application.policy_restricted') . ' : ' . $policy_restricted . ' , ' .
                                                        trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                                                        trans('aid::application.out_of_ratio') . ' : ' . $out_of_ratio . ' , ' .
                                                        trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                                        trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                                                }else{
                                                    $response["status"] = 'failed';
                                                    $response["msg"] = trans('aid::application.The voucher row is not insert to db');
                                                }
                                            }
                                        }
                                    }


                                    if (sizeof($result) > 0 || sizeof($beneficiaries) > 0) {
                                        $rows = [];
                                        foreach ($result as $key => $value) {
                                            $rows[$key][trans('aid::application.#')] = $key + 1;
                                            foreach ($value as $k => $v) {
                                                if ($k != 'case_id' && $k != 'id' && $k != 'sponsor_id' && $k != 'flag' && $k != 'date_flag') {
                                                    if ($k == 'sponsor_number') {
                                                        $rows[$key][trans('aid::application.sponsorship_number')] = $v;
                                                    } else {
                                                        $rows[$key][trans('aid::application.' . $k)] = $v;
                                                    }
                                                    $translate = '-';
                                                    if ($k == 'reason') {
                                                        if ($v == 'not_person') {
                                                            $translate = trans('aid::application.not_person');
                                                        }
                                                        if ($v == 'not_same') {
                                                            $translate = trans('aid::application.not_same');
                                                        }
                                                        if ($v == 'blocked') {
                                                            $translate = trans('aid::application.blocked');
                                                        }
                                                        if ($v == 'nominated') {
                                                            $translate = trans('aid::application.nominated');
                                                        }
                                                        if ($v == 'duplicated') {
                                                            $translate = trans('aid::application.duplicated');
                                                        }
                                                        if ($v == 'out_of_regions') {
                                                            $translate = trans('aid::application.out_of_regions');
                                                        }
                                                        if ($v == 'out_of_ratio') {
                                                            $translate = trans('aid::application.out_of_ratio');
                                                        }
                                                        if ($v == 'policy_restricted') {
                                                            $translate = trans('aid::application.policy_restricted');
                                                        }
                                                        if ($v == 'restricted_total_payment_persons') {
                                                            $translate = trans('aid::application.restricted_total_payment_persons');
                                                        }
                                                        if ($v == 'restricted_count_of_family') {
                                                            $translate = trans('aid::application.restricted_count_of_family');
                                                        }
                                                        if ($v == 'restricted_amount') {
                                                            $translate = trans('aid::application.restricted_amounts');
                                                        }
                                                        if ($v == 'the same status') {
                                                            $translate = trans('sponsorship::application.the same status');
                                                        }
                                                        if ($v == 'invalid status') {
                                                            $translate = trans('sponsorship::application.invalid status');
                                                        }
                                                        if ($v == 'not_nominated') {
                                                            $translate = trans('sponsorship::application.not_nominated');
                                                        }
                                                        if ($v == 'not_case') {
                                                            $translate = trans('sponsorship::application.not_case');
                                                        }
                                                        if ($v == 'limited') {
                                                            $translate = trans('aid::application.max_limited_');
                                                        }
                                                        if ($v == 'invalid_id_card_number') {
                                                            $translate = trans('sponsorship::application.invalid_id_card_number');
                                                        }
                                                        $rows[$key][trans('aid::application.reason')] = $translate;
                                                    }
                                                }
                                            }
                                        }

                                        $token = md5(uniqid());
                                        \Excel::create('export_' . $token, function ($excel) use ($rows,$beneficiaries) {
                                            if(sizeof($rows)){
                                                $excel->sheet(trans('aid::application.rejected_sheet'), function ($sheet) use ($rows) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:F1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);
                                                    $sheet->fromArray($rows);

                                                });
                                            }

                                            if(sizeof($beneficiaries)){
                                                $excel->sheet(trans('aid::application.Beneficiaries'), function ($sheet) use ($beneficiaries) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:C1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);

                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('B1',trans('common::application.name'));
                                                    $sheet ->setCellValue('C1',trans('common::application.status'));
                                                    $z= 2;
                                                    foreach($beneficiaries as $v){
                                                        $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                        $sheet ->setCellValue('B'.$z,$v['name']);
                                                        $sheet ->setCellValue('C'.$z,$v['status']);
                                                        $z++;
                                                    }
                                                });
                                            }

                                        })->store('xlsx', storage_path('tmp/'));
                                        $response["download_token"] = $token;
                                        $response["msg"] = $response["msg"] . '  , ' . trans('aid::application.all restricted_');
                                    }

                                    return response()->json($response);
                                }
                                return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file is empty')]);
                            }

                            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                        }

                    }
                    return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file is empty')]);
                }

                return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('aid::application.The import sheet does not exist in the Excel , please check the label in the instructions or in the template') ]);
            }

            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.invalid file')]);

        }
        else{
            $response = array();

            $voucher=Vouchers::findorfail($request->voucher_id);

            $to_date = date('Y-m-d', strtotime($voucher->voucher_date. ' + '.$voucher->allow_day.' days'));

            if(strtotime($to_date) > strtotime('now')){
                $response["status"]= 'inserted_error';
                $response["msg"]= trans('aid::application.exceeded the number of days allowed to add beneficiaries to voucher');
                return response()->json($response);

            }

            $rules = [
                'voucher_id' => 'required|integer', 'persons' => 'required',
                'receipt_date' => 'required|date', 'receipt_location' => 'required|max:255',
            ];

            $input = [
                'voucher_id' => $request->voucher_id,
                'persons' => $request->get('persons'),
                'receipt_date' => strip_tags($request->receipt_date),
                'receipt_location' => strip_tags($request->receipt_location)
            ];

            $error = \App\Http\Helpers::isValid($input,$rules);
            if($error)
                return response()->json($error);

            $old_v =VoucherPersons::where(['voucher_id' => $input['voucher_id']])->count();
            $max = $voucher->count - $old_v;

            if ($max == 0) {
                $response["status"] = 'inserted_error';
                $response["msg"] = trans('aid::application.The voucher is max exceed persons');
                return response()->json($response);
            }


            $records = $request->persons;
            $total = sizeof($records);
            if ($total > 0) {

                $user = \Auth::user();
                $response = array();

                $invalid_card = 0;
                $not_person = 0;
                $not_case = 0;
                $blocked = 0;
                $out_of_regions = 0;
                $policy_restricted = 0;
                $duplicated = 0;
                $nominated = 0;
                $limited = 0;
                $success = 0;
                $processed = [];
                $passed = [];
                $result = [];
                $out_of_ratio = 0;
                foreach ($records as $key => $value) {

                    $id_card_number =(int) $value['id_card_number'];
                    $isPass=VoucherPersons::checkPersons('id_card_number',$id_card_number,$request->voucher_id,$user->organization_id,$voucher->value,$user->id,$value['individual_id']);
                    if (strlen($id_card_number) <= 9) {
                        if (GovServices::checkCard($id_card_number)) {
                            if($isPass['status']){
                                $temp = ['name' => $isPass['name'],'individual_name' => $value['r_full_name'],'indiv_card_number'=>$value['individual_id_card_number'], 'id' => $isPass['id'], 'id_card_number' =>$id_card_number ,'individual_id'=>$value['individual_id'], 'receipt_time'=>null];
                                $passed[] = $temp;
                                $success++;
                            } else {
                                if (!in_array($value['individual_id_card_number'], $processed)) {
                                    if ($isPass['status'] == false) {
                                        if ($isPass['reason'] == 'policy_restricted') {
                                            $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'policy_restricted','individual_name' => $value['r_full_name'],'indiv_card_number'=>$value['individual_id_card_number']];
                                            $policy_restricted++;
                                        }

                                        if ($isPass['reason'] == 'out_of_regions') {
                                            $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'out_of_regions','individual_name' => $value['r_full_name'],'indiv_card_number'=>$value['individual_id_card_number']];
                                            $out_of_regions++;
                                        }
                                        if ($isPass['reason'] == 'blocked') {
                                            $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'blocked','individual_name' => $value['r_full_name'],'indiv_card_number'=>$value['individual_id_card_number']];
                                            $blocked++;
                                        }

                                        if ($isPass['reason'] == 'not_case') {
                                            $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'not_case','individual_name' => $value['r_full_name'],'indiv_card_number'=>$value['individual_id_card_number']];
                                            $not_case++;
                                        }
                                        if ($isPass['reason'] == 'not_person') {
                                            $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'not_person','individual_name' => $value['r_full_name'],'indiv_card_number'=>$value['individual_id_card_number']];
                                            $not_person++;
                                        }
                                        if ($isPass['reason'] == 'nominated') {
                                            $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'nominated','individual_name' => $value['r_full_name'],'indiv_card_number'=>$value['individual_id_card_number']];
                                            $nominated++;
                                        }

                                    }else {
                                        $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'duplicated','individual_name' => $value['r_full_name'],'indiv_card_number'=>$value['individual_id_card_number']];
                                        $duplicated++;
                                    }

                                    $processed[] = $value['individual_id_card_number'];
                                }
                            }
                        }else{
                            $result[] = ['individual_' => '', 'individual_card_number' =>'','name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'invalid_id_card_number'];
                            $processed[] = $id_card_number;
                            $invalid_card++;
                        }
                    }else {
                        $result[] = ['individual_' => '', 'individual_card_number' =>'','name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'invalid_id_card_number'];
                        $processed[] = $id_card_number;
                        $invalid_card++;
                    }

                }

                $total_restricted = sizeof($result);

                if (($policy_restricted + $blocked) == sizeof($records)) {
                    $response["status"] = 'inserted_error';
                    $response["msg"] = trans('aid::application.The selected cases are not pass may not category blocked or policy restricted');
                }
                else if ($nominated == sizeof($records)) {
                    $response["status"] = 'inserted_error';
                    $response["msg"] = trans('aid::application.The selected cases are nominated');
                }
                else if ($out_of_regions == sizeof($records)) {
                    $response["status"] = 'inserted_error';
                    $response["msg"] = trans('aid::application.The selected cases are not pass may not in connector locations');
                }
                else if ($limited == sizeof($records)) {
                    $response["status"] = 'inserted_error';
                    $response["msg"] = trans('aid::application.The selected cases are limited');
                }
                else if ($total_restricted == sizeof($records)) {
                    $response["status"] = 'inserted_error';
                    $response["msg"] = trans('aid::application.The selected cases are restricted');
                }
                else if (sizeof($passed) != 0) {
                    $receipt_location = $request->receipt_location;
                    $receipt_date = date('Y-m-d', strtotime($request->receipt_date));

                    $user = \Auth::user();
                    $organization_id = $user->organization_id;

                    $r = 0;
                    $inserted_ = [];
                    $limited_ = [];
                    $pIds = [];
                    foreach ($passed as $person) {
                        $pIds[] = $person['individual_id'];
                    }

                    $done = [];
                    $mosques_limit =0;
                    $mosques_ratio = aidsLocation::getTotalCountTree($voucher->count,$voucher->id);

                    foreach ($mosques_ratio as $k => $v) {
                        $mosques_limit = $v->total - $v->count;

                        $mosques_person = \DB::table('char_persons_kinship')
                            ->join('char_persons AS individual','individual.id','=','char_persons_kinship.r_person_id')
                            ->join('char_persons', 'char_persons.id', '=', 'char_persons_kinship.l_person_id')
                            ->where('char_persons.mosques_id', $v->location_id)
                            ->wherein('char_persons_kinship.r_person_id', $pIds)
                            ->groupBy('char_persons_kinship.r_person_id')
                            ->selectRaw("char_persons.id ,CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ', ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                            char_persons.mosques_id,char_persons.id_card_number,char_persons.old_id_card_number,
                                            CONCAT(ifnull(individual.first_name, ' '), ' ' , ifnull(individual.second_name, ' '),' ',ifnull(individual.third_name,' '),' ', ifnull(individual.last_name,' '))as individual_name ,individual.id_card_number as individual_card_number,
                                            char_persons_kinship.r_person_id")
                            ->get();



                        if (sizeof($mosques_person) > 0) {
                            if ($mosques_limit > 0) {
                                $onMosque = [];

                                foreach ($mosques_person as $key => $value) {
                                    $onMosque[] = $value->id;
                                }

                                $person = \DB::table('char_persons_kinship')
                                    ->join('char_persons AS individual','individual.id','=','char_persons_kinship.r_person_id')
                                    ->join('char_persons', 'char_persons.id', '=', 'char_persons_kinship.l_person_id')
                                    ->where('char_persons.mosques_id', $v->location_id)
                                    ->wherein('char_persons_kinship.r_person_id', $pIds)
                                    ->groupBy('char_persons_kinship.r_person_id')
                                    ->selectRaw("char_persons.id ,CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ', ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                            char_persons.mosques_id,char_persons.id_card_number,char_persons.old_id_card_number,
                                            CONCAT(ifnull(individual.first_name, ' '), ' ' , ifnull(individual.second_name, ' '),' ',ifnull(individual.third_name,' '),' ', ifnull(individual.last_name,' '))as individual_name ,individual.id_card_number as individual_card_number,
                                            char_persons_kinship.r_person_id")
                                    ->limit($mosques_limit)
                                    ->get();

                                if (sizeof($person) > 0) {
                                    $person_ =[];
                                    foreach ($person as $kk => $vv) {
                                        $done[]=$vv->r_person_id;
                                        $person_[]=$vv->id;
                                        if ($old_v < $voucher->count) {
//                                                if ($old_v < $max) {
                                            $founded =VoucherPersons::where(['voucher_id' => $voucher->id,'individual_id' => $vv->r_person_id , 'person_id' => $vv->id])->first();
                                            if ($founded) {
                                                $result[] = ['individual_' =>  $vv->individual_name, 'individual_card_number' =>  $vv->individual_card_number,'guardian' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'nominated'];
                                                $nominated++;
                                            } else {

                                               VoucherPersons::create([ 'person_id' => $vv->id, 'status' => 2,
                                                    'voucher_id' => $voucher->id,
                                                    'individual_id' => $vv->r_person_id,
                                                    'receipt_location' => $receipt_location,
                                                    'receipt_date' => $receipt_date]);
                                                $inserted_[] = $vv->r_person_id;
                                                $success++;
                                                $old_v++;
                                                $r++;
                                            }
                                        } else {
                                            if(in_array($value->id, $limited_)) {
                                                $result[] = ['individual_' =>  $vv->individual_name, 'individual_card_number' =>  $vv->individual_card_number,'guardian' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'limited'];
                                                $limited_[] = $vv->r_person_id;
                                                $limited++;
                                            }
                                        }
                                    }
                                }

                                $person_diff = array_diff($onMosque, $person_);

                                if (sizeof($person_diff) > 0) {

                                    $person_ = \DB::table('char_persons_kinship')
                                        ->join('char_persons AS individual','individual.id','=','char_persons_kinship.r_person_id')
                                        ->join('char_persons', 'char_persons.id', '=', 'char_persons_kinship.l_person_id')
                                        ->where('char_persons.mosques_id', $v->location_id)
                                        ->wherein('char_persons_kinship.l_person_id', $person_diff)
                                        ->groupBy('char_persons_kinship.r_person_id')
                                        ->selectRaw("char_persons.id ,CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ', ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                char_persons.mosques_id,char_persons.id_card_number,char_persons.old_id_card_number,
                                                CONCAT(ifnull(individual.first_name, ' '), ' ' , ifnull(individual.second_name, ' '),' ',ifnull(individual.third_name,' '),' ', ifnull(individual.last_name,' '))as individual_name ,individual.id_card_number as individual_card_number,
                                                char_persons_kinship.r_person_id")
                                        ->get();

                                    foreach ($person_ as $key => $value) {
                                        if(in_array($value->id, $limited_)) {
                                            $result[] = ['individual_' =>  $value->individual_name, 'individual_card_number' =>  $value->individual_card_number,'guardian' => $value->full_name, 'id_card_number' => $value->id_card_number, 'reason' => 'out_of_ratio'];
                                            $done[] = $value->r_person_id;
                                            $out_of_ratio++;
                                        }
                                    }
                                }

                            }
                            else{
                                foreach ($mosques_person as $kk => $vv) {
                                    $result[] = ['individual_' =>  $value->individual_name, 'individual_card_number' =>  $value->individual_card_number,'guardian' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'out_of_ratio'];
                                    $done[]=$vv->r_person_id;
                                    $out_of_ratio++;
                                }
                            }
                        }
                    }

                    $pe = array_diff($pIds, $done);

                    $out_per = \DB::table('char_persons_kinship')
                        ->join('char_persons AS individual','individual.id','=','char_persons_kinship.r_person_id')
                        ->join('char_persons', 'char_persons.id', '=', 'char_persons_kinship.l_person_id')
                        ->wherein('char_persons_kinship.r_person_id', $pe)
                        ->groupBy('char_persons_kinship.r_person_id')
                        ->selectRaw("char_persons.id ,CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ', ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                        char_persons.mosques_id,char_persons.id_card_number,char_persons.old_id_card_number,
                                        CONCAT(ifnull(individual.first_name, ' '), ' ' , ifnull(individual.second_name, ' '),' ',ifnull(individual.third_name,' '),' ', ifnull(individual.last_name,' '))as individual_name ,individual.id_card_number as individual_card_number,
                                        char_persons_kinship.r_person_id")
                        ->get();

                    foreach ($out_per as $kk => $vv) {
                        $result[] = ['individual_' =>  $vv->individual_name, 'individual_card_number' =>  $vv->individual_card_number ,'guardian' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'out_of_ratio'];
                        $out_of_ratio++;
                    }

                    if ($r > 0) {
                        if ($total_restricted != 0 && $limited != 0) {
                            $response["status"] = 'inserted_error';
                            $response["msg"] =
                                trans('aid::application.Some cases has been successfully nomination to voucher but another is restricted') . ' ( '
                                . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                trans('aid::application.success') . ' : ' . $r . ' , ' .
                                trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_card . ' , ' .
                                trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                                trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                                trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                                trans('aid::application.out_of_regions') . ' : ' . $out_of_regions . ' , ' .
                                trans('aid::application.policy_restricted') . ' : ' . $policy_restricted . ' , ' .
                                trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                                trans('aid::application.out_of_ratio') . ' : ' . $out_of_ratio . ' , ' .
                                trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                            \Log\Model\Log::saveNewLog('PERSON_VOUCHER_CREATED', trans('aid::application.Added beneficiaries to the aid voucher:') . ' "' . $voucher->title . '"');

                        } else {
                            $response["status"] = 'success';
                            $response["msg"] = trans('aid::application.The voucher row is inserted to db');
                        }

                    } else {
                        $response["status"] = 'failed';
                        $response["msg"] = trans('aid::application.The voucher row is not insert to db');
                    }
                }

                if (sizeof($result) > 0) {
                    $rows = [];
                    foreach ($result as $key => $value) {
                        $rows[$key][trans('aid::application.#')] = $key + 1;
                        foreach ($value as $k => $v) {
                            if ($k != 'case_id' && $k != 'id' && $k != 'sponsor_id' && $k != 'flag' && $k != 'date_flag') {
                                if ($k == 'sponsor_number') {
                                    $rows[$key][trans('aid::application.sponsorship_number')] = $v;
                                } else {
                                    $rows[$key][trans('aid::application.' . $k)] = $v;
                                }
                                $translate = '-';
                                if ($k == 'reason') {
                                    if ($v == 'not_person') {
                                        $translate = trans('aid::application.not_person');
                                    }
                                    if ($v == 'not_same') {
                                        $translate = trans('aid::application.not_same');
                                    }
                                    if ($v == 'blocked') {
                                        $translate = trans('aid::application.blocked');
                                    }
                                    if ($v == 'nominated') {
                                        $translate = trans('aid::application.nominated');
                                    }
                                    if ($v == 'duplicated') {
                                        $translate = trans('aid::application.duplicated');
                                    }
                                    if ($v == 'out_of_regions') {
                                        $translate = trans('aid::application.out_of_regions');
                                    }
                                    if ($v == 'out_of_ratio') {
                                        $translate = trans('aid::application.out_of_ratio');
                                    }
                                    if ($v == 'policy_restricted') {
                                        $translate = trans('aid::application.policy_restricted');
                                    }
                                    if ($v == 'restricted_total_payment_persons') {
                                        $translate = trans('aid::application.restricted_total_payment_persons');
                                    }
                                    if ($v == 'restricted_count_of_family') {
                                        $translate = trans('aid::application.restricted_count_of_family');
                                    }
                                    if ($v == 'restricted_amount') {
                                        $translate = trans('aid::application.restricted_amounts');
                                    }
                                    if ($v == 'the same status') {
                                        $translate = trans('sponsorship::application.the same status');
                                    }
                                    if ($v == 'invalid status') {
                                        $translate = trans('sponsorship::application.invalid status');
                                    }
                                    if ($v == 'not_nominated') {
                                        $translate = trans('sponsorship::application.not_nominated');
                                    }
                                    if ($v == 'not_case') {
                                        $translate = trans('sponsorship::application.not_case');
                                    }
                                    if ($v == 'limited') {
                                        $translate = trans('aid::application.max_limited_');
                                    }
                                    if ($v == 'invalid_id_card_number') {
                                        $translate = trans('sponsorship::application.invalid_id_card_number');
                                    }
                                    $rows[$key][trans('aid::application.reason')] = $translate;
                                }
                            }
                        }
                    }

                    $token = md5(uniqid());
                    \Excel::create('export_' . $token, function ($excel) use ($rows) {
                        $excel->sheet(date('y-m-d'), function ($sheet) use ($rows) {
                            $sheet->setStyle([
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 11,
                                    'bold' => true
                                ]
                            ]);
                            $sheet->setAllBorders('thin');
                            $sheet->setfitToWidth(true);
                            $sheet->setRightToLeft(true);
                            $style = [
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Simplified Arabic',
                                    'size' => 12,
                                    'bold' => true
                                ]
                            ];

                            $sheet->getStyle("A1:F1")->applyFromArray($style);
                            $sheet->setHeight(1, 30);

                            $style = [
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Simplified Arabic',
                                    'size' => 12,
                                    'bold' => false
                                ]
                            ];

                            $sheet->getDefaultStyle()->applyFromArray($style);
                            $sheet->fromArray($rows);

                        });
                    })->store('xlsx', storage_path('tmp/'));
                    $response["download_token"] = $token;
                    $response["msg"] = $response["msg"] . '  , ' . trans('aid::application.all restricted_');
                }

                return response()->json($response);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('aid::application.No filter cases were specified')]);
        }

    }

    // import cases as beneficiary of voucher  by voucher_id , person_id , individual_id ( null ) using excel sheet
    public function storePersonsVoucherByExcel(Request $request)
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit', '2048M');

        $voucher = Vouchers::findorfail($request->voucher_id);

        $to_date = date('Y-m-d', strtotime($voucher->voucher_date . ' + ' . $voucher->allow_day . ' days'));

        $category_id = $voucher->case_category_id;
        $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$category_id)->first();

        if (strtotime($to_date) > strtotime('now')) {
            $response["status"] = 'inserted_error';
            $response["msg"] = trans('aid::application.exceeded the number of days allowed to add beneficiaries to voucher');
            return response()->json($response);
        }

        $out_of_ratio=0;
        $importFile = $request->file;
        if ($importFile->isValid()) {

            $path = $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load($path);
            $sheets = $excel->getSheetNames();
            if(in_array('data', $sheets)) {

                $first = Excel::selectSheets('data')->load($path)->first();

                if (!is_null($first)) {
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ) {

                        $validFile = true;
                        $constraint = ["rkm_alhoy", "okt_alastlam"]; /*,"mkan_alastlam"*/
                        foreach ($constraint as $item_) {
                            if ( !in_array($item_, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if ($validFile) {
                            $records = \Excel::selectSheets('data')->load($path)->get();
                            $total = sizeof($records);
                            if ($total > 0) {
                                $rules = [
                                    'voucher_id' => 'required|integer',
                                    'receipt_date' => 'required|date'
                                    , 'receipt_location' => 'required|max:255',
                                ];

                                $input = [
                                    'voucher_id' => $request->voucher_id,
                                    'receipt_date' => strip_tags($request->receipt_date)
                                    , 'receipt_location' => strip_tags($request->receipt_location)
                                ];

                                $error = \App\Http\Helpers::isValid($input, $rules);
                                if ($error)
                                    return response()->json($error);

                                $old_v =VoucherPersons::where(['voucher_id' => $input['voucher_id']])->count();
                                $max = $voucher->count - $old_v;

                                if ($max == 0) {
                                    $response["status"] = 'inserted_error';
                                    $response["msg"] = trans('aid::application.The voucher is max exceed persons');
                                    return response()->json($response);
                                }

                                $user = \Auth::user();
                                $response = array();

                                $add_cases = $request->add_cases;

                                $invalid_card = 0;
                                $case_added = 0;
                                $not_person = 0;
                                $not_case = 0;
                                $blocked = 0;
                                $out_of_regions = 0;
                                $policy_restricted = 0;
                                $duplicated = 0;
                                $nominated = 0;
                                $limited = 0;
                                $success = 0;
                                $processed = [];
                                $beneficiaries = [];
                                $passed = [];
                                $result = [];
                                $out_of_ratio = 0;
                                foreach ($records as $key => $value) {
                                    $id_card_number = (int)$value['rkm_alhoy'];
                                    if (strlen($id_card_number) <= 9) {
                                        if (GovServices::checkCard($id_card_number)) {
                                            $isPass =VoucherPersons::checkPersons('id_card_number', $id_card_number, $voucher->id, $user->organization_id, $voucher->value,$user->id);
                                            if (!in_array($isPass['id'], $processed)) {
                                                if ($isPass['status'] == true) {
                                                    $temp = ['name' => $isPass['name'], 'id' => $isPass['id'], 'id_card_number' => $id_card_number , 'receipt_time'=>null];
                                                    if (isset($value['okt_alastlam'])) {
                                                        if (!is_null($value['okt_alastlam']) && $value['okt_alastlam'] != ' ' && $value['okt_alastlam'] != '') {
                                                            if (is_object($value['okt_alastlam'])) {
                                                                $temp['receipt_time'] = date('H:i:s',strtotime($value['okt_alastlam']));
                                                            }
                                                        }
                                                    }

//                                                    if (isset($value['mkan_alastlam'])) {
//                                                        if (!is_null($value['mkan_alastlam']) && $value['mkan_alastlam'] != ' ' && $value['mkan_alastlam'] != '') {
//                                                            if (is_object($value['okt_alastlam'])) {
//                                                                $temp['receipt_location'] = $value['mkan_alastlam'];
//                                                            }
//                                                        }
//                                                    }

                                                    $beneficiaries[] = ['name' => $isPass['name'], 'id_card_number' => $id_card_number, 'status' => trans('aid::application.old beneficiary')];
                                                    $passed[] = $temp;
//                                                        $success++;
                                                } else {
                                                    if ($isPass['reason'] == 'policy_restricted') {
                                                        $result[] = ['name' => $isPass['name'], 'id_card_number' => $id_card_number, 'reason' => 'policy_restricted'];
                                                        $policy_restricted++;
                                                    }

                                                    if ($isPass['reason'] == 'out_of_regions') {
                                                        $result[] = ['name' => $isPass['name'], 'id_card_number' => $id_card_number, 'reason' => 'out_of_regions'];
                                                        $out_of_regions++;
                                                    }
                                                    if ($isPass['reason'] == 'blocked') {
                                                        $result[] = ['name' => $isPass['name'], 'id_card_number' => $id_card_number, 'reason' => 'blocked'];
                                                        $blocked++;
                                                    }

                                                    if ($isPass['reason'] == 'not_case') {

                                                        if($add_cases == true || $add_cases == 'true' ){

                                                            $temp = ['name' => $isPass['name'], 'id' => $isPass['id'], 'id_card_number' => $id_card_number , 'receipt_time'=>null];

                                                            $case=  \Common\Model\AidsCases::create(['person_id' =>  $isPass['id'],
                                                                'status' => 1, 'category_id' => $category_id,
                                                                'created_at' => date('Y-m-d H:i:s'),
                                                                'user_id' => $user->id,
                                                                'rank' => 0,
                                                                'organization_id' => $user->organization_id]);

                                                            \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id, 'reason'=>trans('common::application.case was created'), 'status'=>0, 'date'=>date("Y-m-d")]);
                                                            \Log\Model\Log::saveNewLog('CASE_CREATED',trans('common::application.Has added new case data for')  . '  : '.' "'. ' ' .  $isPass['name'] . ' " ' . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);

                                                            if (isset($value['okt_alastlam'])) {
                                                                if (!is_null($value['okt_alastlam']) && $value['okt_alastlam'] != ' ' && $value['okt_alastlam'] != '') {
                                                                    if (is_object($value['okt_alastlam'])) {
                                                                        $temp['receipt_time'] = date('H:i:s',strtotime($value['okt_alastlam']));
                                                                    }
                                                                }
                                                            }
//
//                                                            if (isset($value['mkan_alastlam'])) {
//                                                                if (!is_null($value['mkan_alastlam']) && $value['mkan_alastlam'] != ' ' && $value['mkan_alastlam'] != '') {
//                                                                    if (is_object($value['okt_alastlam'])) {
//                                                                        $temp['receipt_location'] = $value['mkan_alastlam'];
//                                                                    }
//                                                                }
//                                                            }

                                                            $temp['status']=trans('aid::application.new beneficiary');
                                                            $beneficiaries[] = $temp;
                                                            $passed[] = $temp;

                                                        }else{

                                                            $result[] = ['name' => $isPass['name'], 'id_card_number' => $id_card_number, 'reason' => 'not_case'];
                                                            $not_case++;

                                                        }
                                                    }
                                                    if ($isPass['reason'] == 'not_person') {
                                                        $result[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'not_person'];
                                                        $not_person++;
                                                    }
                                                    if ($isPass['reason'] == 'nominated') {
                                                        $result[] = ['name' => $isPass['name'], 'id_card_number' => $id_card_number, 'reason' => 'nominated'];
                                                        $nominated++;
                                                    }
                                                }

                                                $processed[] = $id_card_number;
                                            } else {
                                                $result[] = ['name' => $isPass['name'], 'id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                                                $duplicated++;
                                            }
                                        } else {
                                            $result[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'invalid_id_card_number'];
                                            $processed[] = $id_card_number;
                                            $invalid_card++;
                                        }
                                    } else {
                                        $result[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'invalid_id_card_number'];
                                        $processed[] = $id_card_number;
                                        $invalid_card++;
                                    }
                                }

//                                    $total_restricted = $invalid_card + $policy_restricted + $out_of_regions + $blocked + $not_case + $not_person + $nominated;
                                $total_restricted = sizeof($result);

                                if (($policy_restricted + $blocked) == sizeof($records)) {
                                    $response["status"] = 'inserted_error';
                                    $response["msg"] = trans('aid::application.The selected cases are not pass may not category blocked or policy restricted');
                                } else if ($nominated == sizeof($records)) {
                                    $response["status"] = 'inserted_error';
                                    $response["msg"] = trans('aid::application.The selected cases are nominated');
                                }
                                else if ($out_of_regions == sizeof($records)) {
                                    $response["status"] = 'inserted_error';
                                    $response["msg"] = trans('aid::application.The selected cases are not pass may not in connector locations');
                                }
                                else if ($limited == sizeof($records)) {
                                    $response["status"] = 'inserted_error';
                                    $response["msg"] = trans('aid::application.The selected cases are limited');
                                }

                                else if ($total_restricted == sizeof($records)) {
                                    $r = 0;
                                    $response["status"] = 'inserted_error';
                                    $response["msg"] = trans('aid::application.The selected cases are restricted') . ' ( '
                                        . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                        trans('aid::application.success') . ' : ' . $r . ' , ' .
                                        trans('aid::application.register_as_new_case') . ' : ' . $case_added . ' , ' .
                                        trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_card . ' , ' .
                                        trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                                        trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                                        trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                                        trans('aid::application.out_of_regions') . ' : ' . $out_of_regions . ' , ' .
                                        trans('aid::application.policy_restricted') . ' : ' . $policy_restricted . ' , ' .
                                        trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                                        trans('aid::application.out_of_ratio') . ' : ' . $out_of_ratio . ' , ' .
                                        trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                        trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                                    \Log\Model\Log::saveNewLog('PERSON_VOUCHER_CREATED', trans('aid::application.Added beneficiaries to the aid voucher') . ' "' . $voucher->title . '"');
                                }
                                else{

                                    if (sizeof($passed) != 0) {

                                        $receipt_location = $request->receipt_location;
                                        $receipt_date = date('Y-m-d', strtotime($request->receipt_date));

                                        $user = \Auth::user();
                                        $organization_id = $user->organization_id;

                                        $r = 0;
                                        $inserted_ = [];
                                        $limited_ = [];
                                        $pIds = [];
                                        $receipt_times = [];
                                        foreach ($passed as $person) {
                                            $pIds[] = $person['id'];
                                            $receipt_times[$person['id']] = $person['receipt_time'];
                                        }

                                        $done = [];

                                        if($request->ratio == 1 || $request->ratio == '1') {
                                            $mosques_ratio = aidsLocation::getTotalCountTree($voucher->count,$voucher->id);
                                            foreach ($mosques_ratio as $k => $v) {
                                                $mosques_limit = $v->total - $v->count;

                                                $mosques_person = \DB::table('char_persons')
                                                    ->where('char_persons.mosques_id', $v->location_id)
                                                    ->wherein('char_persons.id', $pIds)
                                                    ->selectRaw("char_persons.id ,
                                                                 CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ',
                                                                     ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                                     char_persons.id_card_number")
                                                    ->groupBy('char_persons.id')
                                                    ->get();

                                                if (sizeof($mosques_person) > 0) {
                                                    if ($mosques_limit > 0) {
                                                        $onMosque = [];

                                                        foreach ($mosques_person as $key => $value) {
                                                            $onMosque[] = $value->id;
                                                        }

                                                        $person = \DB::table('char_persons')
                                                            ->where('char_persons.mosques_id', $v->location_id)
                                                            ->wherein('char_persons.id', $pIds)
                                                            ->selectRaw("char_persons.id ,
                                                                 CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ',
                                                                     ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                                     char_persons.id_card_number")
                                                            ->groupBy('char_persons.id')
                                                            ->limit($mosques_limit)
                                                            ->get();

                                                        if (sizeof($person) > 0) {
                                                            $person_ =[];
                                                            foreach ($person as $kk => $vv) {
                                                                $done[]=$vv->id;
                                                                $person_[]=$vv->id;
                                                                if ($old_v < $voucher->count) {
//                                                                if ($old_v < $max) {
                                                                    $founded =VoucherPersons::where(['voucher_id' => $voucher->id, 'person_id' => $vv->id])->first();
                                                                    if ($founded) {
                                                                        $result[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'nominated'];
                                                                        $nominated++;
                                                                    } else {

                                                                       VoucherPersons::create([ 'person_id' => $vv->id, 'status' => 2,
                                                                            'voucher_id' => $voucher->id, 'receipt_location' => $receipt_location,
                                                                            'receipt_date' => $receipt_date]);
                                                                        $beneficiaries[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'status' => trans('aid::application.old beneficiary')];
                                                                        $inserted_[] = $vv->id;
                                                                        $success++;
                                                                        $old_v++;
                                                                        $r++;
                                                                    }
                                                                } else {
                                                                    if (!in_array($value->id, $limited_)) {
                                                                        $result[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'limited'];
                                                                        $limited_[] = $vv->id;
                                                                        $limited++;
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        $person_diff = array_diff($onMosque, $person_);

                                                        if (sizeof($person_diff) > 0) {
                                                            $person_ = \DB::table('char_persons')
                                                                ->where('char_persons.mosques_id', $v->location_id)
                                                                ->wherein('char_persons.id', $person_diff)
                                                                ->selectRaw("char_persons.id,CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,
                                                                     ifnull(char_persons.second_name, ' '),' ',
                                                                     ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as full_name,
                                                                     char_persons.id_card_number")
                                                                ->groupBy('char_persons.id')
                                                                ->get();

                                                            foreach ($person_ as $key => $value) {
                                                                if (!in_array($value->id, $limited_)) {
                                                                    $result[] = ['name' => $value->full_name, 'id_card_number' => $value->id_card_number, 'reason' => 'out_of_ratio'];
                                                                    $done[] = $value->id;
                                                                    $out_of_ratio++;
                                                                }
                                                            }
                                                        }

                                                    }
                                                    else{
                                                        foreach ($mosques_person as $kk => $vv) {
                                                            $result[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'out_of_ratio'];
                                                            $done[]=$vv->id;
                                                            $out_of_ratio++;
                                                        }
                                                    }
                                                }
                                            }

                                            $pe = array_diff($pIds, $done);

                                            $out_per = \DB::table('char_persons')
                                                ->wherein('char_persons.id', $pe)
                                                ->selectRaw("char_persons.id ,
                                                                 CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ',
                                                                     ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                                     char_persons.mosques_id,char_persons.id_card_number")
                                                ->groupBy('char_persons.id')
                                                ->get();
                                            ;

                                            foreach ($out_per as $kk => $vv) {
                                                $result[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'out_of_ratio'];
                                                $out_of_ratio++;
                                            }

                                        }
                                        else{
                                            $person = \DB::table('char_persons')
                                                ->wherein('char_persons.id', $pIds)
                                                ->selectRaw("char_persons.id ,
                                                                 CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ',
                                                                     ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                                     char_persons.id_card_number")
                                                ->groupBy('char_persons.id')
                                                ->get();

                                            if (sizeof($person) > 0) {
                                                $onMosque = [];
                                                if (sizeof($person) > 0) {
                                                    $person_ =[];
                                                    foreach ($person as $kk => $vv) {
                                                        $done[]=$vv->id;
                                                        $person_[]=$vv->id;
                                                        if ($old_v < $voucher->count) {
//                                                                if ($old_v < $max) {
                                                            $founded =VoucherPersons::where(['voucher_id' => $voucher->id, 'person_id' => $vv->id])->first();
                                                            if ($founded) {
                                                                $result[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'nominated'];
                                                                $nominated++;
                                                            } else {

                                                                $row_ = [ 'person_id' => $vv->id, 'status' => 2,
                                                                    'voucher_id' => $voucher->id,
                                                                    'receipt_location' => $receipt_location,
                                                                    'receipt_date' => $receipt_date];

                                                                if(isset($receipt_times[$vv->id])){
                                                                    if(!(is_null($receipt_times[$vv->id]) || $receipt_times[$vv->id] == '' || $receipt_times[$vv->id] == '')){
                                                                        $row_['receipt_time'] = $receipt_times[$vv->id];
                                                                    }
                                                                }
                                                                $beneficiaries[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'status' => trans('aid::application.old beneficiary')];
                                                               VoucherPersons::create($row_);
                                                                $inserted_[] = $vv->id;
                                                                $success++;
                                                                $old_v++;
                                                                $r++;
                                                            }
                                                        } else {
                                                            if (!in_array($value->id, $limited_)) {
                                                                $result[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'limited'];
                                                                $limited_[] = $vv->id;
                                                                $limited++;
                                                            }
                                                        }
                                                    }
                                                }

                                            }
                                        }

                                        if ($r > 0) {
                                            if ($total_restricted != 0 && $limited != 0) {
                                                $response["status"] = 'inserted_error';
                                                $response["msg"] =
                                                    trans('aid::application.Some cases has been successfully nomination to voucher but another is restricted') . ' ( '
                                                    . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                                    trans('aid::application.success') . ' : ' . $r . ' , ' .
                                                    trans('aid::application.register_as_new_case') . ' : ' . $case_added . ' , ' .
                                                    trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_card . ' , ' .
                                                    trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                                                    trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                                                    trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                                                    trans('aid::application.out_of_regions') . ' : ' . $out_of_regions . ' , ' .
                                                    trans('aid::application.policy_restricted') . ' : ' . $policy_restricted . ' , ' .
                                                    trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                                                    trans('aid::application.out_of_ratio') . ' : ' . $out_of_ratio . ' , ' .
                                                    trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                                    trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                                                \Log\Model\Log::saveNewLog('PERSON_VOUCHER_CREATED', trans('aid::application.Added beneficiaries to the aid voucher') . ' "' . $voucher->title . '"');

                                            } else {
                                                $response["status"] = 'success';
                                                $response["msg"] = trans('aid::application.The voucher row is inserted to db');
                                            }

                                        }
                                        else {

                                            if(sizeof($beneficiaries) > 0){
                                                $response["status"] = 'inserted_error';
                                                $response["msg"] = trans('aid::application.The selected cases are restricted') . ' ( '
                                                    . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                                    trans('aid::application.success') . ' : ' . sizeof($beneficiaries) . ' , ' .
                                                    trans('aid::application.register_as_new_case') . ' : ' . $case_added . ' , ' .
                                                    trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_card . ' , ' .
                                                    trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                                                    trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                                                    trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                                                    trans('aid::application.out_of_regions') . ' : ' . $out_of_regions . ' , ' .
                                                    trans('aid::application.policy_restricted') . ' : ' . $policy_restricted . ' , ' .
                                                    trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                                                    trans('aid::application.out_of_ratio') . ' : ' . $out_of_ratio . ' , ' .
                                                    trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                                    trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                                            }else{
                                                $response["status"] = 'failed';
                                                $response["msg"] = trans('aid::application.The voucher row is not insert to db');
                                            }
                                        }
                                    }
                                }

                                $response["stat_us"] =$result;

                                if (sizeof($result) > 0 || sizeof($beneficiaries) > 0) {
                                    $rows = [];
                                    foreach ($result as $key => $value) {
                                        $rows[$key][trans('aid::application.#')] = $key + 1;
                                        foreach ($value as $k => $v) {
                                            if ($k != 'case_id' && $k != 'id' && $k != 'sponsor_id' && $k != 'flag' && $k != 'date_flag') {
                                                if ($k == 'sponsor_number') {
                                                    $rows[$key][trans('aid::application.sponsorship_number')] = $v;
                                                } else {
                                                    $rows[$key][trans('aid::application.' . $k)] = $v;
                                                }
                                                $translate = '-';
                                                if ($k == 'reason') {
                                                    if ($v == 'not_person') {
                                                        $translate = trans('aid::application.not_person');
                                                    }
                                                    if ($v == 'not_same') {
                                                        $translate = trans('aid::application.not_same');
                                                    }
                                                    if ($v == 'blocked') {
                                                        $translate = trans('aid::application.blocked');
                                                    }
                                                    if ($v == 'nominated') {
                                                        $translate = trans('aid::application.nominated');
                                                    }
                                                    if ($v == 'duplicated') {
                                                        $translate = trans('aid::application.duplicated');
                                                    }
                                                    if ($v == 'out_of_regions') {
                                                        $translate = trans('aid::application.out_of_regions');
                                                    }
                                                    if ($v == 'out_of_ratio') {
                                                        $translate = trans('aid::application.out_of_ratio');
                                                    }
                                                    if ($v == 'policy_restricted') {
                                                        $translate = trans('aid::application.policy_restricted');
                                                    }
                                                    if ($v == 'restricted_total_payment_persons') {
                                                        $translate = trans('aid::application.restricted_total_payment_persons');
                                                    }
                                                    if ($v == 'restricted_count_of_family') {
                                                        $translate = trans('aid::application.restricted_count_of_family');
                                                    }
                                                    if ($v == 'restricted_amount') {
                                                        $translate = trans('aid::application.restricted_amounts');
                                                    }
                                                    if ($v == 'the same status') {
                                                        $translate = trans('sponsorship::application.the same status');
                                                    }
                                                    if ($v == 'invalid status') {
                                                        $translate = trans('sponsorship::application.invalid status');
                                                    }
                                                    if ($v == 'not_nominated') {
                                                        $translate = trans('sponsorship::application.not_nominated');
                                                    }
                                                    if ($v == 'not_case') {
                                                        $translate = trans('sponsorship::application.not_case');
                                                    }
                                                    if ($v == 'limited') {
                                                        $translate = trans('aid::application.max_limited_');
                                                    }
                                                    if ($v == 'invalid_id_card_number') {
                                                        $translate = trans('sponsorship::application.invalid_id_card_number');
                                                    }
                                                    $rows[$key][trans('aid::application.reason')] = $translate;
                                                }
                                            }
                                        }
                                    }

                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function ($excel) use ($rows,$beneficiaries) {

                                        if(sizeof($rows) > 0){
                                            $excel->sheet(trans('aid::application.rejected_sheet'), function ($sheet) use ($rows) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:D1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
                                                $sheet->fromArray($rows);

                                            });
                                        }

                                        if(sizeof($beneficiaries) > 0){
                                            $excel->sheet(trans('aid::application.Beneficiaries'), function ($sheet) use ($beneficiaries) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:C1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);

                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.name'));
                                                $sheet ->setCellValue('C1',trans('common::application.status'));
                                                $z= 2;
                                                foreach($beneficiaries as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,$v['name']);
                                                    $sheet ->setCellValue('C'.$z,$v['status']);
                                                    $z++;
                                                }
                                            });
                                        }
                                    })->store('xlsx', storage_path('tmp/'));
                                    $response["download_token"] = $token;
                                    $response["msg"] = $response["msg"] . '  , ' . trans('aid::application.all restricted_');
                                }

                                return response()->json($response);
                            }
                            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file is empty')]);
                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);

                    }
                }

                return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file is empty')]);

            }

            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('aid::application.The import sheet does not exist in the Excel , please check the label in the instructions or in the template') ]);
        }

        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.invalid file')]);
    }

    // store cases as beneficiary of voucher  by voucher_id , person_id , individual_id ( null )
    public function storePersonsVoucher(Request $request)
    {
        try {
            ini_set('max_execution_time',0);
            set_time_limit(0);
            Ini_set('memory_limit','2048M');
            $this->authorize('createPersonsVoucher', Vouchers::class);

            $user = \Auth::user();

            if($request->get('source') == 'file'){

                if ($user->hasPermission('aid.voucher.updateSerial')) {
                    if(!($request->un_serial == null || $request->un_serial== 'null')){
                        if($request->un_serial){
                            $un_count = Vouchers::where(['un_serial'=>$request->un_serial])->count();
                            if($un_count > 0){
                                return response()->json(['error' => trans('aid::application.must_unique')], 422);
                            }
                        }
                    }
                }

                if($request->center == '1' || $request->center == '1'){
                    return $this->import($request);
                }
                else{
                    $path = \Input::file('file')->getRealPath();
                    if (\Input::hasFile('file')) {
                        $category_id = $request->case_category_id;
                        $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$category_id)->first();

                        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
                        $objReader->setReadDataOnly(true);
                        $excel = $objReader->load($path);
                        $sheets = $excel->getSheetNames();

                        if(\App\Http\Helpers::sheetFound("data",$sheets)) {
                            $first=Excel::selectSheets('data')->load($path)->first();

                            if(!is_null($first)){

                                $keys = $first->keys()->toArray();
                                if(sizeof($keys) >0 ){
                                    $validFile=true;
                                    $constraint =  ["rkm_alhoy", "kym_alksym", "mhto_alksym", "mkan_alastlam", "tarykh_alastlam","okt_alastlam"];

                                    foreach ($constraint as $item) {
                                        if ( !in_array($item, $keys) ) {
                                            $validFile = false;
                                            break;
                                        }
                                    }

                                    if($validFile){
                                        $user = \Auth::user();
                                        $add_cases = $request->add_cases;
                                        $response = array();
                                        $row=0;
                                        $case_added = 0;
                                        $not_card_number=[];
                                        $policy_restricted=[];
                                        $success=0;
                                        $not_case=[];
                                        $out_of_regions=[];
                                        $not_person=[];
                                        $duplicated=[];
                                        $values_keys=[];
                                        $persons=[];
                                        $vouchers=[];
                                        $result=[];
                                        $beneficiaries=[];
                                        $path = \Input::file('file')->getRealPath();
                                        $data = \Excel::selectSheets('data')
                                            ->load($path, function ($reader) {$reader->formatDates(true, 'Y-m-d');})->get();
                                        $total=sizeof($data);
                                        if ($total > 0) {
                                            foreach ($data as $key => $value) {
                                                $index=null;
                                                $voucher_content=null;
                                                $voucher_value=null;

                                                if(isset($value->kym_alksym)){
                                                    if ($value->kym_alksym) {
                                                        $voucher_value=$value->kym_alksym;
                                                        $index = $value->kym_alksym;

                                                        if(isset($value->mhto_alksym)){
                                                            if ($value->mhto_alksym) {
                                                                $index = $index . '-' . $value->mhto_alksym;
                                                                $voucher_content=$value->mhto_alksym;
                                                            }
                                                        }
                                                    }
                                                }


                                                if($index){

                                                    if (!in_array($index, $values_keys)) {
                                                        $values_keys[] = $index;
                                                    }
                                                    if (!isset($persons[$index])) {
                                                        $persons[$index] = [];
                                                    }
                                                    if (!isset($vouchers[$index])) {
                                                        $vouchers[$index]['value']=$voucher_value;
                                                        if($voucher_content){
                                                            $vouchers[$index]['content']=$voucher_content;
                                                        }
                                                    }

                                                    if ($value->rkm_alhoy){
                                                        $value->rkm_alhoy = (int)$value->rkm_alhoy;
                                                        if ($value->rkm_alhoy != "" && strlen($value->rkm_alhoy) == 9){
                                                            if(!in_array($value->rkm_alhoy,$persons[$index])) {
                                                                $isPass=VoucherPersons::checkPersons('id_card_number',$value->rkm_alhoy,null,$user->organization_id,$voucher_value,$user->id);

                                                                if ($isPass['status'] == true) {

                                                                    if(!in_array($isPass['id'],$persons[$index])) {
                                                                        $persons[$index][]=$isPass['id'];
                                                                        $details['person_id']=$isPass['id'];
                                                                        if ($value->mkan_alastlam){
                                                                            if ($value->mkan_alastlam != "" &&$value->mkan_alastlam != "-") {
                                                                                $details['receipt_location']=$value->mkan_alastlam;
                                                                            }
                                                                        }
                                                                        if ($value->okt_alastlam){
                                                                            if (!is_null($value->okt_alastlam ) && $value->okt_alastlam != "" && $value->okt_alastlam != "-") {
                                                                                if (is_object($value['okt_alastlam'])) {
                                                                                    $details['receipt_time'] = date('H:i:s',strtotime($value['okt_alastlam']));
                                                                                }
                                                                            }
                                                                        }

                                                                        if ($value->tarykh_alastlam){
                                                                            if ($value->tarykh_alastlam != "" &&$value->tarykh_alastlam != "-") {
//                                                                            $details['receipt_date']=date('Y-m-d',strtotime($value->tarykh_alastlam));
                                                                                $receipt_date = Helpers::getFormatedDate($value->tarykh_alastlam);

                                                                                if (!is_null($receipt_date)) {
                                                                                    $details['receipt_date']=$receipt_date;
                                                                                }
                                                                            }
                                                                        }

                                                                        if (!isset($vouchers[$index]['persons'])) {
                                                                            $vouchers[$index]['persons']=[];
                                                                        }

                                                                        array_push($vouchers[$index]['persons'],$details);
                                                                        $beneficiaries[]=['name' => $isPass['name'],'id_card_number' => $value->rkm_alhoy,'status'=>trans('aid::application.old beneficiary')];
                                                                        $success++;
                                                                    }else{
                                                                        $duplicated[]=$value->rkm_alhoy;
                                                                    }


                                                                }
                                                                else{
                                                                    if ($isPass['reason'] =='policy_restricted') {
                                                                        if(!in_array($value->rkm_alhoy,$policy_restricted)){
                                                                            $policy_restricted[]=$value->rkm_alhoy;
                                                                        }else{
                                                                            $duplicated[]=$value->rkm_alhoy;
                                                                        }

                                                                        $result[]=['id_card_number' =>$value->rkm_alhoy,'reason'=>'policy_restricted'];
                                                                    }
                                                                    if ($isPass['reason'] =='out_of_regions') {
                                                                        if(!in_array($value->rkm_alhoy,$out_of_regions)){
                                                                            $out_of_regions[]=$value->rkm_alhoy;
                                                                        }else{
                                                                            $duplicated[]=$value->rkm_alhoy;
                                                                        }
                                                                        $result[]=['name' => $isPass['name'],'id_card_number' => $value->rkm_alhoy,'reason'=>'out_of_regions'];
                                                                    }

                                                                    if ($isPass['reason'] =='not_case') {
                                                                        if(!in_array($value->rkm_alhoy,$not_case)){

                                                                            if($add_cases == true || $add_cases == 'true' ){
                                                                                $id_card_number = $value->rkm_alhoy;
                                                                                $temp = ['name' => $isPass['name'], 'id' => $isPass['id'], 'id_card_number' => $id_card_number , 'receipt_time'=>null];

                                                                                $case=  \Common\Model\AidsCases::create(['person_id' =>  $isPass['id'],
                                                                                    'status' => 1, 'category_id' => $category_id,
                                                                                    'created_at' => date('Y-m-d H:i:s'),
                                                                                    'user_id' => $user->id,
                                                                                    'rank' => 0,
                                                                                    'organization_id' => $user->organization_id]);

                                                                                \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id, 'reason'=>trans('common::application.case was created'), 'status'=>0, 'date'=>date("Y-m-d")]);
                                                                                \Log\Model\Log::saveNewLog('CASE_CREATED',trans('common::application.Has added new case data for')  . '  : '.' "'. ' ' .  $isPass['name'] . ' " ' . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);

                                                                                $persons[$index][]=$isPass['id'];
                                                                                $details['person_id']=$isPass['id'];
                                                                                if ($value->mkan_alastlam){
                                                                                    if ($value->mkan_alastlam != "" &&$value->mkan_alastlam != "-") {
                                                                                        $details['receipt_location']=$value->mkan_alastlam;
                                                                                    }
                                                                                }
                                                                                if ($value->okt_alastlam){
                                                                                    if (!is_null($value->okt_alastlam ) && $value->okt_alastlam != "" && $value->okt_alastlam != "-") {
                                                                                        if (is_object($value['okt_alastlam'])) {
                                                                                            $details['receipt_time'] = date('H:i:s',strtotime($value['okt_alastlam']));
                                                                                        }
                                                                                    }
                                                                                }

                                                                                if ($value->tarykh_alastlam){
                                                                                    if ($value->tarykh_alastlam != "" &&$value->tarykh_alastlam != "-") {
//                                                                                  $details['receipt_date']=date('Y-m-d',strtotime($value->tarykh_alastlam));
                                                                                        $receipt_date = Helpers::getFormatedDate($value->tarykh_alastlam);

                                                                                        if (!is_null($receipt_date)) {
                                                                                            $details['receipt_date']=$receipt_date;
                                                                                        }
                                                                                    }
                                                                                }

                                                                                if (!isset($vouchers[$index]['persons'])) {
                                                                                    $vouchers[$index]['persons']=[];
                                                                                }

                                                                                array_push($vouchers[$index]['persons'],$details);
                                                                                $temp['status'] =trans('aid::application.new beneficiary');
                                                                                $beneficiaries[]=$temp;
                                                                                $success++;
                                                                            }else{
                                                                                $not_case[]=$value->rkm_alhoy;
                                                                            }
                                                                        }else{
                                                                            $duplicated[]=$value->rkm_alhoy;
                                                                        }

                                                                        $result[]=['id_card_number' =>$value->rkm_alhoy,'reason'=>'not_case'];
                                                                    }
                                                                    if ($isPass['reason'] =='not_person') {
                                                                        if(!in_array($value->rkm_alhoy,$not_person)){
                                                                            $not_person[]=$value->rkm_alhoy;
                                                                        }else{
                                                                            $duplicated[]=$value->rkm_alhoy;
                                                                        }

                                                                        $result[]=['id_card_number' =>$value->rkm_alhoy,'reason'=>'not_person'];
                                                                    }
                                                                }
                                                            }
                                                        }else{

                                                            if(!in_array($value->rkm_alhoy,$not_card_number)){
                                                                $not_card_number[]=$value->rkm_alhoy;
                                                            }else{
                                                                $duplicated[]=$value->rkm_alhoy;
                                                            }
                                                            $result[]=['id_card_number' =>$value->rkm_alhoy,'reason'=>'not_card_number'];
                                                        }
                                                    }
                                                    $row++;
                                                }
                                            }

                                            $vouchers_number=0;
                                            if(!empty($vouchers)) {
                                                foreach($vouchers as $k=>$v){
                                                    if(!empty($v['persons'])) {
                                                        if(!isset($v['content'])) {$v['content'] = null;}
                                                        $input=['content' => $v['content'],'value' => $v['value']];
                                                        $input['title'] =strip_tags($request->get('title'));
                                                        $input['notes'] =strip_tags($request->get('notes'));
                                                        $input['center'] =strip_tags($request->get('center'));
                                                        $input['beneficiary'] =strip_tags($request->get('beneficiary'));
                                                        $input['exchange_rate'] = strip_tags($request->get('exchange_rate'));
                                                        $input['currency_id'] = strip_tags($request->get('currency_id'));
                                                        $input['category_id'] = strip_tags($request->get('category_id'));
                                                        $input['case_category_id'] = strip_tags($request->get('case_category_id'));
                                                        $input['type'] = strip_tags($request->get('type'));
                                                        $input['sponsor_id'] = strip_tags($request->get('sponsor_id'));
                                                        $input['transfer'] = strip_tags($request->get('transfer'));
                                                        $input['transfer'] = strip_tags($request->get('transfer'));
                                                        $input['collected'] = strip_tags($request->get('collected'));
                                                        if(!($request->un_serial == null || $request->un_serial== 'null')){
                                                            $input['un_serial'] = strip_tags($request->un_serial);
                                                        }
                                                        $input['transfer_company_id'] = null;
                                                        if($input['transfer'] == 1 || $input['transfer'] == '1'){
                                                            $input['transfer_company_id'] = $request->get('transfer_company_id');
                                                        }
                                                        $input['voucher_date'] =date('Y-m-d',strtotime(strip_tags($request->get('voucher_date'))));
                                                        $input['count'] =sizeof($v['persons']);
                                                        $input['organization_id']=$user->organization_id;

                                                        $user = \Auth::user();
                                                        $district_id = $user->organization->district_id  ;

                                                        $district_code = Helpers::getDistrictCode($user->organization_id,$district_id);
                                                        $mSerial= $district_code.date('Y').date('m').'-';

                                                        $voucher_cnt = Vouchers::query()
                                                            ->where(function ($q) use ($mSerial) {
                                                                $q->whereRaw("serial like ?",$mSerial. "%");
                                                            })
                                                            ->count();

                                                        $voucher_cnt ++;
                                                        $input['serial']= $mSerial.$voucher_cnt;
                                                        $voucher=Vouchers::create($input);
                                                        if($voucher){
                                                            foreach($v['persons'] as $kg=>$vg){
                                                                $vg['voucher_id']=$voucher->id;
                                                                $vg['status']=2;
                                                               VoucherPersons::create($vg);
                                                            }
                                                        }
                                                        $vouchers_number++;
                                                    }
                                                }
                                            }
                                            $response["result"]= $result;
                                            if($success != 0){
                                                \Log\Model\Log::saveNewLog('PERSON_VOUCHER_CREATED',trans('aid::application.Added vouchers and beneficiaries with an Excel file') );

                                            }
                                            if($row == 0){
                                                $response["status"]= false;
                                                $response["msg"] = trans('aid::application.The voucher row is not insert to db');
                                            }
                                            else{


                                                if($row != 0 && $success == 0 && $row == $success){
                                                    $response["status"]= true;
                                                    $response["msg"] = trans('aid::application.The voucher row is inserted to db');
                                                }else{

                                                    if(sizeof($result) > 0){
                                                        $response["status"]= 'inserted_error';
                                                    }else{
                                                        $response["status"]= true;
                                                    }

                                                    $response["msg"]=
                                                        trans('aid::application.Some cases has been successfully nomination to voucher but another is restricted'). ' ( '
                                                        . trans('aid::application.total no') .' : '.  $row .' , '.
                                                        trans('aid::application.success') .' : '. $success.' , '.
                                                        trans('aid::application.not_person') .' : '. sizeof($not_person) .' , '.
                                                        trans('aid::application.not_case') .' : '. sizeof($not_case) .' , '.
                                                        trans('aid::application.not_card_number') .' : '. sizeof($not_card_number) .' , '.
                                                        trans('aid::application.out_of_regions') .' : '. sizeof($out_of_regions) .' , '.
                                                        trans('aid::application.policy_restricted') .' : '. sizeof($policy_restricted) .' , '.
                                                        trans('aid::application.duplicated') .' : '. sizeof($duplicated) .' )';
                                                }

                                            }

                                            if(sizeof($result) > 0 || sizeof($beneficiaries) > 0){
                                                $rows=[];
                                                foreach($result as $key =>$value){
                                                    $rows[$key][trans('aid::application.#')]=$key+1;
                                                    foreach($value as $k =>$v){
                                                        if($k != 'case_id' && $k != 'id' && $k != 'sponsor_id'&& $k != 'flag'&& $k != 'date_flag'){
                                                            if($k =='sponsor_number'){
                                                                $rows[$key][trans('aid::application.sponsorship_number')]= $v;
                                                            }else{
                                                                $rows[$key][trans('aid::application.' . $k)]= $v;
                                                            }
                                                            $translate='-';
                                                            if($k =='reason'){
                                                                if($v =='not_same'){ $translate= trans('aid::application.not_same');}
                                                                if($v =='blocked'){ $translate= trans('aid::application.blocked');}
                                                                if($v =='nominated'){ $translate= trans('aid::application.nominated');}
                                                                if($v =='duplicated'){ $translate= trans('aid::application.duplicated');}
                                                                if($v =='out_of_regions'){ $translate= trans('aid::application.out_of_regions');}
                                                                if($v =='out_of_ratio'){ $translate= trans('aid::application.out_of_ratio');}
                                                                if($v =='policy_restricted'){ $translate= trans('aid::application.policy_restricted');}
                                                                if($v =='restricted_total_payment_persons'){ $translate= trans('aid::application.restricted_total_payment_persons');}
                                                                if($v =='restricted_count_of_family'){ $translate= trans('aid::application.restricted_count_of_family');}
                                                                if($v =='restricted_amount'){ $translate= trans('aid::application.restricted_amounts');}
                                                                if($v =='the same status'){ $translate= trans('sponsorship::application.the same status');}
                                                                if($v =='invalid status'){ $translate= trans('sponsorship::application.invalid status');}
                                                                if($v =='not_nominated'){ $translate= trans('sponsorship::application.not_nominated');}
                                                                if($v =='not_case'){ $translate= trans('sponsorship::application.not_case');}
                                                                if($v =='limited'){ $translate= trans('aid::application.max_limited_');}
                                                                if($v =='invalid_id_card_number'){ $translate= trans('sponsorship::application.invalid_id_card_number');}
                                                                $rows[$key][trans('aid::application.reason')]= $translate;
                                                            }
                                                        }
                                                    }
                                                }

                                                $token = md5(uniqid());
                                                \Excel::create('export_' . $token, function($excel) use($rows,$beneficiaries) {
                                                    if(sizeof($rows) > 0){
                                                        $excel->sheet(trans('aid::application.rejected_sheet'), function ($sheet) use ($rows) {
                                                            $sheet->setStyle([
                                                                'font' => [
                                                                    'name' => 'Calibri',
                                                                    'size' => 11,
                                                                    'bold' => true
                                                                ]
                                                            ]);
                                                            $sheet->setAllBorders('thin');
                                                            $sheet->setfitToWidth(true);
                                                            $sheet->setRightToLeft(true);
                                                            $style = [
                                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                                'font' =>[
                                                                    'name'      =>  'Simplified Arabic',
                                                                    'size'      =>  12,
                                                                    'bold'      =>  true
                                                                ]
                                                            ];

                                                            $sheet->getStyle("A1:D1")->applyFromArray($style);
                                                            $sheet->setHeight(1,30);

                                                            $style = [
                                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                                'font' =>[
                                                                    'name'      =>  'Simplified Arabic',
                                                                    'size'      =>  12,
                                                                    'bold' => false
                                                                ]
                                                            ];

                                                            $sheet->getDefaultStyle()->applyFromArray($style);
                                                            $sheet->fromArray($rows);

                                                        });
                                                    }

                                                    if(sizeof($beneficiaries) > 0){
                                                        $excel->sheet(trans('aid::application.Beneficiaries'), function ($sheet) use ($beneficiaries) {
                                                            $sheet->setStyle([
                                                                'font' => [
                                                                    'name' => 'Calibri',
                                                                    'size' => 11,
                                                                    'bold' => true
                                                                ]
                                                            ]);
                                                            $sheet->setAllBorders('thin');
                                                            $sheet->setfitToWidth(true);
                                                            $sheet->setRightToLeft(true);
                                                            $style = [
                                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                                'font' => [
                                                                    'name' => 'Simplified Arabic',
                                                                    'size' => 12,
                                                                    'bold' => true
                                                                ]
                                                            ];

                                                            $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                            $sheet->setHeight(1, 30);

                                                            $style = [
                                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                                'font' => [
                                                                    'name' => 'Simplified Arabic',
                                                                    'size' => 12,
                                                                    'bold' => false
                                                                ]
                                                            ];

                                                            $sheet->getDefaultStyle()->applyFromArray($style);

                                                            $sheet->getStyle("A1:B1")->applyFromArray(['font' => ['bold' => true]]);
                                                            $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                            $sheet ->setCellValue('B1',trans('common::application.name'));
                                                            $z= 2;
                                                            foreach($beneficiaries as $v){
                                                                $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                                $sheet ->setCellValue('B'.$z,$v['name']);
                                                                $z++;
                                                            }
                                                        });
                                                    }
                                                })->store('xlsx', storage_path('tmp/'));
                                                $response["download_token"]= $token;
                                                $response["msg"] = trans('aid::application.Downloading the file containing the banned candidates').' ، '. $response["msg"];
                                            }
                                            return response()->json($response);

                                        }
                                        return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                                    }
                                    return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);

                                }

                            }

                            return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
                    }
                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
                }

            }
            else{
                $response = array();
                $voucher=Vouchers::findorfail($request->voucher_id);
                $to_date = date('Y-m-d', strtotime($voucher->voucher_date. ' + '.$voucher->allow_day.' days'));

                if(strtotime($to_date) > strtotime('now')){
                    $response["status"]= 'inserted_error';
                    $response["msg"]= trans('aid::application.exceeded the number of days allowed to add beneficiaries to voucher');
                    return response()->json($response);

                }

                $rules = [
                    'voucher_id' => 'required|integer', 'persons' => 'required',
                    'receipt_date' => 'required|date', 'receipt_location' => 'required|max:255',
                ];

                $input = [
                    'voucher_id' => $request->voucher_id,
                    'persons' => $request->get('persons'),
                    'receipt_date' => strip_tags($request->receipt_date),
                    'receipt_location' => strip_tags($request->receipt_location)
                ];

                $error = \App\Http\Helpers::isValid($input,$rules);
                if($error)
                    return response()->json($error);

                $old_v =VoucherPersons::where(['voucher_id' => $input['voucher_id']])->count();
                $max = $voucher->count - $old_v;

                if ($max == 0) {
                    $response["status"] = 'inserted_error';
                    $response["msg"] = trans('aid::application.The voucher is max exceed persons');
                    return response()->json($response);
                }


                $records = $request->persons;
                $total = sizeof($records);
                if ($total > 0) {

                    $user = \Auth::user();
                    $response = array();

                    $invalid_card = 0;
                    $not_person = 0;
                    $not_case = 0;
                    $blocked = 0;
                    $out_of_regions = 0;
                    $policy_restricted = 0;
                    $duplicated = 0;
                    $nominated = 0;
                    $limited = 0;
                    $success = 0;
                    $processed = [];
                    $passed = [];
                    $result = [];
                    $out_of_ratio = 0;
                    foreach ($records as $key => $value) {
                        $id_card_number = $value['id_card_number'];
                        $isPass=VoucherPersons::checkPersons('id_card_number',$id_card_number,$request->voucher_id,$user->organization_id,$voucher->value,$user->id);
                        if (!in_array($isPass['id'], $processed)) {
                            if ($isPass['status'] == true) {
                                $temp = ['name' => $isPass['name'], 'id' => $isPass['id'], 'id_card_number' =>$id_card_number , 'receipt_time'=>null];
                                $passed[] = $temp;
                                $success++;
                            } else {
                                if ($isPass['reason'] == 'policy_restricted') {
                                    $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'policy_restricted'];
                                    $policy_restricted++;
                                }

                                if ($isPass['reason'] == 'out_of_regions') {
                                    $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'out_of_regions'];
                                    $out_of_regions++;
                                }
                                if ($isPass['reason'] == 'blocked') {
                                    $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'blocked'];
                                    $blocked++;
                                }

                                if ($isPass['reason'] == 'not_case') {
                                    $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'not_case'];
                                    $not_case++;
                                }
                                if ($isPass['reason'] == 'not_person') {
                                    $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'not_person'];
                                    $not_person++;
                                }
                                if ($isPass['reason'] == 'nominated') {
                                    $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'nominated'];
                                    $nominated++;
                                }
                            }

                            $processed[] = $id_card_number;
                        } else {
                            $result[] = ['name' => $isPass['name'], 'id_card_number' =>  $id_card_number, 'reason' => 'duplicated'];
                            $duplicated++;
                        }
                    }

                    $response["result"] = $result;
                    $response["pass"] = $passed;
                    $response["status"] = 'inserted_error';
                    $total_restricted = sizeof($result);
                    if (($policy_restricted + $blocked) == sizeof($records)) {
                        $response["status"] = 'inserted_error';
                        $response["msg"] = trans('aid::application.The selected cases are not pass may not category blocked or policy restricted');
                    }
                    else if (($policy_restricted + $blocked) == sizeof($records)) {
                        $response["status"] = 'inserted_error';
                        $response["msg"] = trans('aid::application.The selected cases are not pass may not category blocked or policy restricted');
                    }
                    else if ($nominated == sizeof($records)) {
                        $response["status"] = 'inserted_error';
                        $response["msg"] = trans('aid::application.The selected cases are nominated');
                    }
                    else if ($out_of_regions == sizeof($records)) {
                        $response["status"] = 'inserted_error';
                        $response["msg"] = trans('aid::application.The selected cases are not pass may not in connector locations');
                    }
                    else if ($limited == sizeof($records)) {
                        $response["status"] = 'inserted_error';
                        $response["msg"] = trans('aid::application.The selected cases are limited');
                    }
                    else if ($total_restricted == sizeof($records)) {
                        $response["status"] = 'inserted_error';
                        $response["msg"] = trans('aid::application.The selected cases are restricted');
                    }

                    else{

                        if (sizeof($passed) > 0) {
                            $receipt_location = $request->receipt_location;
                            $receipt_date = date('Y-m-d', strtotime($request->receipt_date));

                            $user = \Auth::user();
                            $organization_id = $user->organization_id;

                            $r = 0;
                            $inserted_ = [];
                            $limited_ = [];
                            $pIds = [];
                            foreach ($passed as $person) {
                                $pIds[] = $person['id'];
                            }

                            $response["reg"] = $pIds;
                            $done = [];

                            if($request->withRatio == true){
                                $mosques_ratio = aidsLocation::getTotalCountTree($voucher->count,$voucher->id);
                                foreach ($mosques_ratio as $k => $v) {
                                    $mosques_limit = $v->total - $v->count;
                                    $mosques_person = \DB::table('char_persons')
                                        ->where('char_persons.mosques_id', $v->location_id)
                                        ->wherein('char_persons.id', $pIds)
                                        ->selectRaw("char_persons.id ,
                                                             CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ',
                                                                 ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                                 char_persons.id_card_number")
                                        ->groupBy('char_persons.id')
                                        ->get();

                                    if (sizeof($mosques_person) > 0) {
                                        if ($mosques_limit > 0) {
                                            $onMosque = [];

                                            foreach ($mosques_person as $key => $value) {
                                                $onMosque[] = $value->id;
                                            }

                                            $person = \DB::table('char_persons')
                                                ->where('char_persons.mosques_id', $v->location_id)
                                                ->wherein('char_persons.id', $pIds)
                                                ->selectRaw("char_persons.id ,
                                                             CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ',
                                                                 ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                                 char_persons.id_card_number")
                                                ->groupBy('char_persons.id')
                                                ->limit($mosques_limit)
                                                ->get();

                                            if (sizeof($person) > 0) {
                                                $person_ =[];
                                                foreach ($person as $kk => $vv) {
                                                    $done[]=$vv->id;
                                                    $person_[]=$vv->id;
                                                    if ($old_v < $voucher->count) {
//                                                    if ($old_v < $max) {
                                                        $founded =VoucherPersons::where(['voucher_id' => $voucher->id, 'person_id' => $vv->id])->first();
                                                        if ($founded) {
                                                            $result[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'nominated'];
                                                            $nominated++;
                                                        } else {

                                                           VoucherPersons::create([ 'person_id' => $vv->id, 'status' => 2,
                                                                'voucher_id' => $voucher->id, 'receipt_location' => $receipt_location,
                                                                'receipt_date' => $receipt_date]);
                                                            $inserted_[] = $vv->id;
                                                            $success++;
                                                            $old_v++;
                                                            $r++;
                                                        }
                                                    } else {
                                                        if (!in_array($value->id, $limited_)) {
                                                            $result[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'limited'];
                                                            $limited_[] = $vv->id;
                                                            $limited++;
                                                        }
                                                    }
                                                }
                                            }

                                            $person_diff = array_diff($onMosque, $person_);

                                            if (sizeof($person_diff) > 0) {
                                                $person_ = \DB::table('char_persons')
                                                    ->where('char_persons.mosques_id', $v->location_id)
                                                    ->wherein('char_persons.id', $person_diff)
                                                    ->selectRaw("char_persons.id,CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,
                                                                 ifnull(char_persons.second_name, ' '),' ',
                                                                 ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as full_name,
                                                                 char_persons.id_card_number")
                                                    ->groupBy('char_persons.id')
                                                    ->get();

                                                foreach ($person_ as $key => $value) {
                                                    if(in_array($value->id, $limited_)) {
                                                        $result[] = ['name' => $value->full_name, 'id_card_number' => $value->id_card_number, 'reason' => 'out_of_ratio'];
                                                        $done[] = $value->id;
                                                        $out_of_ratio++;
                                                    }
                                                }
                                            }

                                        }
                                        else{
                                            foreach ($mosques_person as $kk => $vv) {
                                                $result[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'out_of_ratio'];
                                                $done[]=$vv->id;
                                                $out_of_ratio++;
                                            }
                                        }
                                    }
                                }

                                $pe = array_diff($pIds, $done);

                                $out_per = \DB::table('char_persons')
                                    ->wherein('char_persons.id', $pe)
                                    ->selectRaw("char_persons.id ,
                                                             CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ',
                                                                 ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                                 char_persons.mosques_id,char_persons.id_card_number")
                                    ->groupBy('char_persons.id')
                                    ->get();

                                foreach ($out_per as $kk => $vv) {
                                    $result[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'out_of_ratio'];
                                    $out_of_ratio++;
                                }
                                $response["count"] =$old_v;
                            }
                            else{

                                $person = \DB::table('char_persons')
                                    ->wherein('char_persons.id', $pIds)
                                    ->selectRaw("char_persons.id ,
                                                             CONCAT(ifnull(char_persons.first_name, ' '), ' ' , ifnull(char_persons.second_name, ' '),' ',
                                                                 ifnull(char_persons.third_name,' '),' ', ifnull(char_persons.last_name,' '))as name,
                                                                 char_persons.id_card_number")
                                    ->groupBy('char_persons.id')
                                    ->get();

//                                return response()->json(['status' => 'failed', 'error_type' => ' ',$person, 'msg' => trans('aid::application.No filter cases were specified')]);

                                $person_ =[];
                                foreach ($person as $kk => $vv) {
                                    $done[]=$vv->id;
                                    $person_[]=$vv->id;
                                    if ($old_v < $voucher->count) {
                                        $founded =VoucherPersons::where(['voucher_id' => $voucher->id, 'person_id' => $vv->id])->first();
                                        if ($founded) {
                                            $result[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'nominated'];
                                            $nominated++;
                                        } else {

                                           VoucherPersons::create([ 'person_id' => $vv->id, 'status' => 2,
                                                'voucher_id' => $voucher->id, 'receipt_location' => $receipt_location,
                                                'receipt_date' => $receipt_date]);
                                            $inserted_[] = $vv->id;
                                            $success++;
                                            $old_v++;
                                            $r++;
                                        }
                                    } else {
                                        if (!in_array($vv->id, $limited_)) {
                                            $result[] = ['name' => $vv->name, 'id_card_number' => $vv->id_card_number, 'reason' => 'limited'];
                                            $limited_[] = $vv->id;
                                            $limited++;
                                        }
                                    }
                                }

                            }
                        }
                        $response["r_"] = $r;
                        $response["pass_"] = $result;

                        if ($r > 0) {
                            if (sizeof($result) > 0) {
                                $response["status"] = 'inserted_error';
                                $response["msg"] =
                                    trans('aid::application.Some cases has been successfully nomination to voucher but another is restricted') . ' ( '
                                    . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                    trans('aid::application.success') . ' : ' . $r . ' , ' .
                                    trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_card . ' , ' .
                                    trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                                    trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                                    trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                                    trans('aid::application.out_of_regions') . ' : ' . $out_of_regions . ' , ' .
                                    trans('aid::application.policy_restricted') . ' : ' . $policy_restricted . ' , ' .
                                    trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                                    trans('aid::application.out_of_ratio') . ' : ' . $out_of_ratio . ' , ' .
                                    trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                    trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                                \Log\Model\Log::saveNewLog('PERSON_VOUCHER_CREATED', trans('aid::application.Downloading the file containing the banned candidates') . ' "' . $voucher->title . '"');

                            } else {
                                $response["status"] = 'success';
                                $response["msg"] = trans('aid::application.The voucher row is inserted to db');
                            }

                        } else {
                            if (sizeof($result) > 0) {
                                $response["status"] = 'inserted_error';
                                $response["msg"] =
                                    trans('aid::application.Some cases has been successfully nomination to voucher but another is restricted') . ' ( '
                                    . trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                    trans('aid::application.success') . ' : ' . $r . ' , ' .
                                    trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_card . ' , ' .
                                    trans('aid::application.not_case') . ' : ' . $not_case . ' , ' .
                                    trans('aid::application.not_person') . ' : ' . $not_person . ' , ' .
                                    trans('aid::application.blocked') . ' : ' . $blocked . ' , ' .
                                    trans('aid::application.out_of_regions') . ' : ' . $out_of_regions . ' , ' .
                                    trans('aid::application.policy_restricted') . ' : ' . $policy_restricted . ' , ' .
                                    trans('aid::application.limited') . ' : ' . $limited . ' , ' .
                                    trans('aid::application.out_of_ratio') . ' : ' . $out_of_ratio . ' , ' .
                                    trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                    trans('aid::application.previously inserted') . ' : ' . $nominated . ' )';
                                \Log\Model\Log::saveNewLog('PERSON_VOUCHER_CREATED', trans('aid::application.Downloading the file containing the banned candidates') . ' "' . $voucher->title . '"');

                            } else {
                                $response["status"] = 'success';
                                $response["msg"] = trans('aid::application.The voucher row is inserted to db');
                            }

                        }

                    }

                    if (sizeof($result) > 0) {
                        $rows = [];
                        foreach ($result as $key => $value) {
                            $rows[$key][trans('aid::application.#')] = $key + 1;
                            foreach ($value as $k => $v) {
                                if ($k != 'case_id' && $k != 'id' && $k != 'sponsor_id' && $k != 'flag' && $k != 'date_flag') {
                                    if ($k == 'sponsor_number') {
                                        $rows[$key][trans('aid::application.sponsorship_number')] = $v;
                                    } else {
                                        $rows[$key][trans('aid::application.' . $k)] = $v;
                                    }
                                    $translate = '-';
                                    if ($k == 'reason') {
                                        if ($v == 'not_person') {
                                            $translate = trans('aid::application.not_person');
                                        }
                                        if ($v == 'not_same') {
                                            $translate = trans('aid::application.not_same');
                                        }
                                        if ($v == 'blocked') {
                                            $translate = trans('aid::application.blocked');
                                        }
                                        if ($v == 'nominated') {
                                            $translate = trans('aid::application.nominated');
                                        }
                                        if ($v == 'duplicated') {
                                            $translate = trans('aid::application.duplicated');
                                        }
                                        if ($v == 'out_of_regions') {
                                            $translate = trans('aid::application.out_of_regions');
                                        }
                                        if ($v == 'out_of_ratio') {
                                            $translate = trans('aid::application.out_of_ratio');
                                        }
                                        if ($v == 'policy_restricted') {
                                            $translate = trans('aid::application.policy_restricted');
                                        }
                                        if ($v == 'restricted_total_payment_persons') {
                                            $translate = trans('aid::application.restricted_total_payment_persons');
                                        }
                                        if ($v == 'restricted_count_of_family') {
                                            $translate = trans('aid::application.restricted_count_of_family');
                                        }
                                        if ($v == 'restricted_amount') {
                                            $translate = trans('aid::application.restricted_amounts');
                                        }
                                        if ($v == 'the same status') {
                                            $translate = trans('sponsorship::application.the same status');
                                        }
                                        if ($v == 'invalid status') {
                                            $translate = trans('sponsorship::application.invalid status');
                                        }
                                        if ($v == 'not_nominated') {
                                            $translate = trans('sponsorship::application.not_nominated');
                                        }
                                        if ($v == 'not_case') {
                                            $translate = trans('sponsorship::application.not_case');
                                        }
                                        if ($v == 'limited') {
                                            $translate = trans('aid::application.max_limited_');
                                        }
                                        if ($v == 'invalid_id_card_number') {
                                            $translate = trans('sponsorship::application.invalid_id_card_number');
                                        }
                                        $rows[$key][trans('aid::application.reason')] = $translate;
                                    }
                                }
                            }
                        }

                        $token = md5(uniqid());
                        \Excel::create('export_' . $token, function ($excel) use ($rows) {
                            $excel->sheet(trans('aid::application.rejected_sheet'), function ($sheet) use ($rows) {
                                $sheet->setStyle([
                                    'font' => [
                                        'name' => 'Calibri',
                                        'size' => 11,
                                        'bold' => true
                                    ]
                                ]);
                                $sheet->setAllBorders('thin');
                                $sheet->setfitToWidth(true);
                                $sheet->setRightToLeft(true);
                                $style = [
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font' => [
                                        'name' => 'Simplified Arabic',
                                        'size' => 12,
                                        'bold' => true
                                    ]
                                ];

                                $sheet->getStyle("A1:D1")->applyFromArray($style);
                                $sheet->setHeight(1, 30);

                                $style = [
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font' => [
                                        'name' => 'Simplified Arabic',
                                        'size' => 12,
                                        'bold' => false
                                    ]
                                ];

                                $sheet->getDefaultStyle()->applyFromArray($style);
                                $sheet->fromArray($rows);

                            });
                        })->store('xlsx', storage_path('tmp/'));
                        $response["download_token"] = $token;
                        $response["msg"] = $response["msg"] . '  , ' . trans('aid::application.all restricted_');
                    }

                    return response()->json($response);
                }
                return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('aid::application.No filter cases were specified')]);
            }

        } catch (Exception $e) {
            return response()->json(['status' =>'error',"error_code" => $e->getCode() ,'msg'=>$e->getMessage() ]);
        }
    }

    public function import(Request $request)
    {
        try {

            ini_set('max_execution_time',0);
            set_time_limit(0);
            Ini_set('memory_limit','2048M');
            $this->authorize('createPersonsVoucher', Vouchers::class);

            $path = \Input::file('file')->getRealPath();
            $user = \Auth::user();

            if (\Input::hasFile('file')) {
                $category_id = $request->case_category_id;
                $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
                $objReader->setReadDataOnly(true);

                $excel = $objReader->load($path);
                $sheets = $excel->getSheetNames();

                if(\App\Http\Helpers::sheetFound("data",$sheets)) {
                    $first=Excel::selectSheets('data')->load($path)->first();

                    if(!is_null($first)){

                        $keys = $first->keys()->toArray();
                        if(sizeof($keys) >0 ){
                            $validFile=true;
                            $constraint =  ["rkm_alhoy", "kym_alksym", "mhto_alksym", "mkan_alastlam", "tarykh_alastlam","okt_alastlam"];

                            foreach ($constraint as $item) {
                                if ( !in_array($item, $keys) ) {
                                    $validFile = false;
                                    break;
                                }
                            }

                            if($validFile){
                                $response = array();
                                $data = \Excel::selectSheets('data')->load($path, function ($reader) {$reader->formatDates(true, 'Y-m-d');})->get();
                                $total=sizeof($data);
                                if ($total > 0) {

                                    $row=0;
                                    $success=0;
                                    $case_added = [];

                                    $not_card_number=[];
                                    $not_person=[];
                                    $duplicated=[];
                                    $values_keys=[];

                                    $persons=[];
                                    $new_cases=[];
                                    $vouchers=[];

                                    $result=[];
                                    $beneficiaries=[];

                                    foreach ($data as $key => $value) {
                                        $index=null;
                                        $voucher_content=null;
                                        $voucher_value=null;

                                        if(isset($value->kym_alksym)){
                                            if ($value->kym_alksym) {
                                                $voucher_value=$value->kym_alksym;
                                                $index = $value->kym_alksym;

                                                if(isset($value->mhto_alksym)){
                                                    if ($value->mhto_alksym) {
                                                        $index = $index . '-' . $value->mhto_alksym;
                                                        $voucher_content=$value->mhto_alksym;
                                                    }
                                                }
                                            }
                                        }


                                        if($index){

                                            if (!in_array($index, $values_keys)) {
                                                $values_keys[] = $index;
                                            }

                                            if (!isset($persons[$index])) {
                                                $persons[$index] = [];
                                            }

                                            if (!isset($vouchers[$index])) {
                                                $vouchers[$index]['value']=$voucher_value;
                                                if($voucher_content){
                                                    $vouchers[$index]['content']=$voucher_content;
                                                }
                                            }

                                            if ($value->rkm_alhoy){
                                                $value->rkm_alhoy = (int)$value->rkm_alhoy;

                                                if ($value->rkm_alhoy != "" && strlen($value->rkm_alhoy) == 9){

                                                    if(!in_array($value->rkm_alhoy,$persons[$index])) {
                                                        $isPass =VoucherPersons::checkMainPersons($value->rkm_alhoy, $user->organization_id,$category_id);
                                                        if ($isPass['status'] == true){

                                                            $ben_status = trans('aid::application.old beneficiary');
                                                            if($isPass['add_case'] == true){
                                                                $ben_status = trans('aid::application.new beneficiary');
                                                                if(!in_array($isPass['id'],$case_added)){
                                                                    $case_added[] = $isPass['id'];
                                                                    $new_cases[]=['person_id' => $isPass['id'] ,'name'=> $isPass['name']];
                                                                }
                                                            }


                                                            if(!in_array($isPass['id'],$persons[$index])) {
                                                                $persons[$index][]=$isPass['id'];
                                                                $details['person_id']=$isPass['id'];
                                                                if ($value->mkan_alastlam){
                                                                    if ($value->mkan_alastlam != "" &&$value->mkan_alastlam != "-") {
                                                                        $details['receipt_location']=$value->mkan_alastlam;
                                                                    }
                                                                }
                                                                if ($value->okt_alastlam){
                                                                    if (!is_null($value->okt_alastlam ) && $value->okt_alastlam != "" && $value->okt_alastlam != "-") {
                                                                        if (is_object($value['okt_alastlam'])) {
                                                                            $details['receipt_time'] = date('H:i:s',strtotime($value['okt_alastlam']));
                                                                        }
                                                                    }
                                                                }

                                                                if ($value->tarykh_alastlam){
                                                                    if ($value->tarykh_alastlam != "" &&$value->tarykh_alastlam != "-") {
//                                                                            $details['receipt_date']=date('Y-m-d',strtotime($value->tarykh_alastlam));
                                                                        $receipt_date = Helpers::getFormatedDate($value->tarykh_alastlam);

                                                                        if (!is_null($receipt_date)) {
                                                                            $details['receipt_date']=$receipt_date;
                                                                        }
                                                                    }
                                                                }

                                                                if (!isset($vouchers[$index]['persons'])) {
                                                                    $vouchers[$index]['persons']=[];
                                                                }

                                                                array_push($vouchers[$index]['persons'],$details);
                                                                $beneficiaries[]=['name' => $isPass['name'],'id_card_number' => $value->rkm_alhoy,'status'=>$ben_status];
                                                                $success++;
                                                            }else{
                                                                $duplicated[]=$value->rkm_alhoy;
                                                            }
                                                        }else{

                                                            if ($isPass['reason'] =='not_person') {
                                                                if(!in_array($value->rkm_alhoy,$not_person)){
                                                                    $result[]=['id_card_number' =>$value->rkm_alhoy,'name'=>'_','reason'=>'not_person'];
                                                                    $not_person[]=$value->rkm_alhoy;
                                                                }else{
                                                                    $result[]=['id_card_number' =>$value->rkm_alhoy,'name'=>'_','reason'=>'duplicated'];
                                                                    $duplicated[]=$value->rkm_alhoy;
                                                                }
                                                            }
                                                        }

                                                    }else{
                                                        $duplicated[]=$value->rkm_alhoy;
                                                        $result[]=['id_card_number' =>$value->rkm_alhoy,'name'=>'_','reason'=>'duplicated'];
                                                    }

                                                }else{
                                                    if(!in_array($value->rkm_alhoy,$not_card_number)){
                                                        $not_card_number[]=$value->rkm_alhoy;
                                                        $result[]=['id_card_number' =>$value->rkm_alhoy,'name'=>'_','reason'=>'invalid_id_card_number'];
                                                    }
                                                }
                                            }
                                            $row++;
                                        }

                                    }

                                    $vouchers_number=0;
                                    if(!empty($vouchers)) {
                                        foreach($vouchers as $k=>$v){
                                            if(!empty($v['persons'])) {

                                                if(!isset($v['content'])) {$v['content'] = null;}

                                                $input=['content' => $v['content'],'value' => $v['value']];
                                                $input['title'] =strip_tags($request->get('title'));
                                                $input['notes'] =strip_tags($request->get('notes'));
                                                $input['center'] =strip_tags($request->get('center'));
                                                $input['beneficiary'] =strip_tags($request->get('beneficiary'));
                                                $input['exchange_rate'] = strip_tags($request->get('exchange_rate'));
                                                $input['currency_id'] = strip_tags($request->get('currency_id'));
                                                $input['category_id'] = strip_tags($request->get('category_id'));
                                                $input['case_category_id'] = strip_tags($request->get('case_category_id'));
                                                $input['type'] = strip_tags($request->get('type'));
                                                $input['sponsor_id'] = strip_tags($request->get('sponsor_id'));
                                                $input['transfer'] = strip_tags($request->get('transfer'));
                                                $input['collected'] = strip_tags($request->get('collected'));
                                                if(!($request->un_serial == null || $request->un_serial== 'null')){
                                                    $input['un_serial'] = strip_tags($request->get('un_serial'));
                                                }
                                                $input['transfer_company_id'] = null;
                                                if($input['transfer'] == 1 || $input['transfer'] == '1'){
                                                    $input['transfer_company_id'] = $request->get('transfer_company_id');
                                                }
                                                $input['voucher_date'] =date('Y-m-d',strtotime(strip_tags($request->get('voucher_date'))));
                                                $input['count'] =sizeof($v['persons']);
                                                $input['organization_id']=$user->organization_id;

                                                $district_id = $user->organization->district_id  ;
                                                $district_code = Helpers::getDistrictCode($user->organization_id,$district_id);
                                                $mSerial= $district_code.date('Y').date('m').'-';

                                                $voucher_cnt = Vouchers::query()
                                                    ->where(function ($q) use ($mSerial) {
                                                        $q->whereRaw("serial like ?",$mSerial. "%");
                                                    })
                                                    ->count();

                                                $voucher_cnt ++;
                                                $input['serial']= $mSerial.$voucher_cnt;
                                                $voucher=Vouchers::create($input);
                                                if($voucher){
                                                    foreach($v['persons'] as $kg=>$vg){
                                                        $vg['voucher_id']=$voucher->id;
                                                        $vg['status']=2;
                                                       VoucherPersons::create($vg);
                                                    }
                                                }
                                                $vouchers_number++;
                                            }
                                        }
                                    }
                                    $response["result"]= $result;

                                    if(sizeof($new_cases) > 0 ){
                                        $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$category_id)->first();

                                        foreach($new_cases as $k =>$v ){

                                            $exist =  CaseModel::where(['person_id' => $v['person_id'] ,
                                                'organization_id' => $user->organization_id ])->first();

                                            if(is_null($exist)){
                                                $case=  \Common\Model\AidsCases::create(['person_id' =>  $v['person_id'],
                                                    'status' => 1, 'category_id' => $category_id,
                                                    'created_at' => date('Y-m-d H:i:s'),
                                                    'user_id' => $user->id,
                                                    'rank' => 0,
                                                    'organization_id' => $user->organization_id]);

                                                \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id,
                                                    'reason'=>trans('common::application.case was created'), 'status'=>0, 'date'=>date("Y-m-d")]);
                                                \Log\Model\Log::saveNewLog('CASE_CREATED',trans('common::application.Has added new case data for')  . '  : '.' "'. ' ' .  $v['name'] . ' " ' . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);

                                            }
                                        }
                                    }

                                    if($success != 0){
                                        \Log\Model\Log::saveNewLog('PERSON_VOUCHER_CREATED',trans('aid::application.Added vouchers and beneficiaries with an Excel file') );

                                    }
                                    if($row == 0){
                                        $response["status"]= false;
                                        $response["msg"] = trans('aid::application.The voucher row is not insert to db');
                                    }
                                    else
                                    {
                                        if($row != 0 && $success == 0 && $row == $success){
                                            $response["status"]= true;
                                            $response["msg"] = trans('aid::application.The voucher row is inserted to db');
                                        }
                                        else{

                                            if(sizeof($result) > 0){
                                                $response["status"]= 'inserted_error';
                                            }else{
                                                $response["status"]= true;
                                            }
                                            $response["msg"]=
                                                trans('aid::application.Some cases has been successfully nomination to voucher but another is restricted'). ' ( '
                                                . trans('aid::application.total no') .' : '.  $row .' , '.
                                                trans('aid::application.success') .' : '. $success.' , '.
                                                trans('aid::application.not_card_number') .' : '. sizeof($not_card_number) .' , '.
                                                trans('aid::application.not_person') .' : '. sizeof($not_person) .' , '.
                                                trans('aid::application.new_case') .' : '. sizeof($new_cases) .' , '.
                                                trans('aid::application.duplicated') .' : '. sizeof($duplicated) .' )';
                                        }

                                    }

                                    if(sizeof($result) > 0 || sizeof($beneficiaries) > 0){
                                        $rows=[];
                                        foreach($result as $key =>$value){
                                            $rows[$key][trans('aid::application.#')]=$key+1;
                                            foreach($value as $k =>$v){
                                                if($k != 'case_id' && $k != 'id' && $k != 'sponsor_id'&& $k != 'flag'&& $k != 'date_flag'){
                                                    if($k =='sponsor_number'){
                                                        $rows[$key][trans('aid::application.sponsorship_number')]= $v;
                                                    }else{
                                                        $rows[$key][trans('aid::application.' . $k)]= $v;
                                                    }
                                                    $translate='-';
                                                    if($k =='reason'){
                                                        if($v =='not_person'){ $translate= trans('aid::application.not_person');}
                                                        if($v =='not_same'){ $translate= trans('aid::application.not_same');}
                                                        if($v =='duplicated'){ $translate= trans('aid::application.duplicated');}
                                                        if($v =='not_case'){ $translate= trans('sponsorship::application.not_case');}
                                                        if($v =='invalid_id_card_number'){ $translate= trans('sponsorship::application.invalid_id_card_number');}
                                                        $rows[$key][trans('aid::application.reason')]= $translate;
                                                    }
                                                }
                                            }
                                        }

                                        $token = md5(uniqid());
                                        \Excel::create('export_' . $token, function($excel) use($rows,$beneficiaries) {
                                            if(sizeof($rows) > 0){
                                                $excel->sheet(trans('aid::application.rejected_sheet'), function ($sheet) use ($rows) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' =>[
                                                            'name'      =>  'Simplified Arabic',
                                                            'size'      =>  12,
                                                            'bold'      =>  true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:D1")->applyFromArray($style);
                                                    $sheet->setHeight(1,30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' =>[
                                                            'name'      =>  'Simplified Arabic',
                                                            'size'      =>  12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);
                                                    $sheet->fromArray($rows);

                                                });
                                            }

                                            if(sizeof($beneficiaries) > 0){
                                                $excel->sheet(trans('aid::application.Beneficiaries'), function ($sheet) use ($beneficiaries) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:C1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);

                                                    $sheet->getStyle("A1:B1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('B1',trans('common::application.name'));
//                                                    $sheet ->setCellValue('C1',trans('common::application.status'));
                                                    $z= 2;
                                                    foreach($beneficiaries as $v){
                                                        $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                        $sheet ->setCellValue('B'.$z,$v['name']);
//                                                        $sheet ->setCellValue('C'.$z,$v['status_name']);

                                                        $z++;
                                                    }
                                                });
                                            }
                                        })->store('xlsx', storage_path('tmp/'));
                                        $response["download_token"]= $token;
                                        $response["msg"] = trans('aid::application.Downloading the file containing the banned candidates').' ، '. $response["msg"];
                                    }
                                    return response()->json($response);
                                }
                                return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                            }
                            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                        }
                    }
                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
                }

                return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);

            }

            return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

        } catch (Exception $e) {
            return response()->json(['status' =>'error',"error_code" => $e->getCode() ,'msg'=>$e->getMessage() ]);
        }
    }

    //----------------------------------------------------------------------------------------------------

    // export details of voucher beneficiary to word (recipt bonds) using (voucher_id + person_id)
    public function exportToWord($person_id, $voucher_id){

        $this->authorize('exportPersonsVoucher', Vouchers::class);
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $user = \Auth::user();
        $voucher= Vouchers::fetch($voucher_id);
        $Beneficiary=VoucherPersons::VoucherBeneficiary($voucher_id,$person_id);

        if(!$voucher->template){
            $default_template=Setting::where(['id'=>'default-voucher-template','organization_id'=>$user->organization_id])->first();
            if($default_template){
                $path = base_path('storage/app/'.$default_template->value);
            }else{
                $path = base_path('storage/app/templates/voucher-default-template.docx');
            }
        }else{
            $path = base_path('storage/app/'.$voucher->template);
        }
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);

        try{
            $templateProcessor->cloneRow('rId', sizeof($Beneficiary));
            $i =1;


            foreach($Beneficiary as $item){
                $templateProcessor->setValue('rId#'.$i, $i);
                $templateProcessor->setValue('num#'.$i, $i);
                if(!is_null($voucher->sponsor_logo)){
                    $templateProcessor->setImg('image#'.$i, array('src'=> base_path('storage/app/' . $voucher->sponsor_logo),'swh'=>'150'));
                }else{
                    $templateProcessor->setImg('image#'.$i, array('src'=> base_path('storage/app/logos/emptyLogo.png'),'swh'=>'150'));
                }
                if(!is_null($voucher->organization_logo)){
                    $templateProcessor->setImg('org_image#'.$i, array('src'=> base_path('storage/app/' . $voucher->organization_logo),'swh'=>'150'));
                }else{
                    $templateProcessor->setImg('org_image#'.$i, array('src'=> base_path('storage/app/logos/emptyLogo.png'),'swh'=>'150'));
                }
                $templateProcessor->setValue('organization_name#'.$i, $voucher->organization_name);
                $templateProcessor->setValue('sponsor_name#'.$i, $voucher->sponsor_name);
                $templateProcessor->setValue('name#'.$i, (is_null($item->name) || $item->name == ' ' ) ? ' ' : $item->name );


                $templateProcessor->setValue('id_card_number#'.$i, (is_null($item->id_card_number) || $item->id_card_number == ' ' ) ? ' ' : $item->id_card_number );
                $templateProcessor->setValue('address#'.$i, (is_null($item->address) || $item->address == ' ' ) ? ' ' : $item->address );
                $templateProcessor->setValue('phone#'.$i, (is_null($item->phone) || $item->phone == ' ' ) ? ' ' : $item->phone );
                $templateProcessor->setValue('secondary_mobile#'.$i, (is_null($item->secondery_mobile) || $item->secondery_mobile == ' ' ) ? ' ' : $item->secondery_mobile );
                $templateProcessor->setValue('primary_mobile#'.$i, (is_null($item->primary_mobile) || $item->primary_mobile == ' ' ) ? ' ' : $item->primary_mobile );
                $templateProcessor->setValue('receipt_date#'.$i, (is_null($item->receipt_date) || $item->receipt_date == ' ' ) ? ' ' : $item->receipt_date );
                $templateProcessor->setValue('receipt_time#'.$i, (is_null($item->receipt_time) || $item->receipt_time == ' ' ) ? ' ' : $item->receipt_time );
                $templateProcessor->setValue('receipt_location#'.$i, (is_null($item->receipt_location) || $item->receipt_location == ' ' ) ? ' ' : $item->receipt_location );
                $templateProcessor->setValue('voucher_title#'.$i, (is_null($voucher->title) || $voucher->title == ' ' ) ? ' ' : $voucher->title );
                $templateProcessor->setValue('voucher_type#'.$i, (is_null($voucher->type) || $voucher->type == ' ' ) ? ' ' : $voucher->type );
                $templateProcessor->setValue('voucher_currency#'.$i, (is_null($voucher->currency) || $voucher->currency == ' ' ) ? ' ' : $voucher->currency );
                $templateProcessor->setValue('voucher_value#'.$i, (is_null($voucher->value) || $voucher->value == ' ' ) ? ' ' : ($voucher->value ) );
                $templateProcessor->setValue('voucher_sh_value#'.$i, (is_null($voucher->shekel_value) || $voucher->shekel_value == ' ' ) ? ' ' : ($voucher->shekel_value ) );
                $templateProcessor->setValue('voucher_contents#'.$i, (is_null($voucher->content) || $voucher->content == ' ' ) ? ' ' : $voucher->content );
                $templateProcessor->setValue('voucher_notes#'.$i, (is_null($voucher->notes) || $voucher->notes == ' ' ) ? ' ' : $voucher->notes );
                $templateProcessor->setValue('voucher_category#'.$i, (is_null($voucher->category_name) || $voucher->category_name == ' ' ) ? ' ' : $voucher->category_name );
                if($i < sizeof($Beneficiary)){
                    $i=$i+1;
                }
            }

        }catch (Exception $e){
            return response()->json(['status' => false]);

        }

        $token = md5(uniqid());
        $templateProcessor->saveAs(storage_path('tmp/export_' . $token . '.docx'));
        $Voucher = Vouchers::findOrFail($voucher_id);
        \Log\Model\Log::saveNewLog('PERSONS_VOUCHER_EXPORT',trans('aid::application.Exported vouchers file to beneficiaries of'). ' "' . $Voucher->title . '" ');
        return response()->json(['download_token' => $token]);
    }

    // export details of voucher beneficiary to excel  using (voucher_id + person_id)
    public function export($id,Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $this->authorize('exportPersonsVoucher', Vouchers::class);
        $target = $request->target;
        $Beneficiary=VoucherPersons::allBeneficiary($id,$target);
        if(sizeof($Beneficiary) !=0){
            foreach($Beneficiary as $key =>$value){
                $dat[$key][trans('aid::application.#')]=$key+1;
                foreach($value as $k =>$v){
                    if($k != 'receipt_time'){
                        $dat[$key][trans('aid::application.' . $k)]= $v;
                    }
                }
                if($target ==1){
                    $dat[$key][trans('aid::application.signature')]= '';
                }
            }
            $data=$dat;
            if($target ==1){
                $Sheetname=trans('aid::application.signature_sheet');
            }else{
                $Sheetname=trans('aid::application.receipt_sheet');
            }
            $Voucher = Vouchers::findOrFail($id);

            $token = md5(uniqid());
            \Excel::create('export_' . $token, function($excel) use($data,$Sheetname) {
                $excel->setTitle(trans('aid::application.voucher_name'));
                $excel->setDescription(trans('aid::application.voucher_name'));
                $excel->sheet($Sheetname, function($sheet) use($data) {

                    $sheet->setStyle([
                        'font' => [
                            'name' => 'Calibri',
                            'size' => 11,
                            'bold' => true
                        ]
                    ]);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setRightToLeft(true);
                    $style = [
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font' =>[
                            'name'      =>  'Simplified Arabic',
                            'size'      =>  12,
                            'bold'      =>  true
                        ]
                    ];

                    $sheet->getStyle("A1:S1")->applyFromArray($style);
                    $sheet->setHeight(1,30);

                    $style = [
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font' =>[
                            'name'      =>  'Simplified Arabic',
                            'size'      =>  12,
                            'bold' => false
                        ]
                    ];

                    $sheet->getDefaultStyle()->applyFromArray($style);

                    $sheet->fromArray($data);
                });
            })->store('xlsx', storage_path('tmp/'));

            \Log\Model\Log::saveNewLog('PERSONS_VOUCHER_EXPORT',trans('aid::application.Exported a list of beneficiaries'). ' "' . $Voucher->title . '" ');
            return response()->json([
                'download_token' => $token,
            ]);


        }else{
            return 0;
        }
    }

    // update details of person from voucher beneficiary using (voucher_id + person_id)
    public function updatePersonsVoucherDetails(Request $request,$id)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $persons = [];
        if($id == 0)
            $persons = $request->get('persons');
        else
            $persons[] = ['person_id'=> $id,'voucher_id'=> $request->get('voucher_id')];

        try {
            $this->authorize('updatePersonsVoucher', Vouchers::class);
            $response = array();
            $rules= [
                'receipt_date'          => 'required|date',
                'receipt_time'          => 'required|date',
                'receipt_location'    => 'required|max:255',
            ];

            $input=[
                'receipt_date'     => strip_tags($request->get('receipt_date')),
                'receipt_time'     => strip_tags($request->get('receipt_time')),
                'receipt_location' => strip_tags($request->get('receipt_location'))
            ];


            $error = \App\Http\Helpers::isValid($input,$rules);
            if($error)
                return response()->json($error);


            $voucher_id= $request->get('voucher_id');

            foreach ($persons as $key => $value) {
                if(VoucherPersons::where(['person_id' =>$value['person_id'] ,'voucher_id'=> $value['voucher_id']])->update($input))
                {
                    $voucher = Vouchers::findOrFail($value['voucher_id']);
                    $person = \Common\Model\Person::fetch(array('id' => $value['person_id']));
                    $name=$person->first_name.' '.$person->second_name.' '.$person->third_name.' '.$person->last_name;
                    \Log\Model\Log::saveNewLog('PERSON_VOUCHER_UPDATED',trans('aid::application.edited the receipt data for the beneficiary of an aid voucher') . ' "'.$voucher->title .'" - '. ' "'.$name. '" ');

                    $response["status"]= 'success';
                    $response["msg"]= trans('setting::application.The row is updated');
                }else{
                    $response["status"]= 'failed';
                    $response["msg"]= trans('setting::application.There is no update');
                }
            }

            return response()->json($response);
        }
        catch (Exception $e) {
            return response()->json(['status' =>'error',"error_code" => $e->getCode() ,'msg'=>$e->getMessage() ]);
        }
    }

    // delete person from voucher beneficiary using (voucher_id + person_id)
    public function deletePersonsVoucher($id,Request $request)
    {
        try {
            ini_set('max_execution_time',0);
            set_time_limit(0);
            Ini_set('memory_limit','2048M');
            $response = array();

            $this->authorize('deletePersonsVoucher', Vouchers::class);

            if(VoucherPersons::where(['person_id'=>$id,'voucher_id'=>$request->get('voucher_id')])->delete())
            {
                $voucher = Vouchers::findOrFail($request->get('voucher_id'));
                $person = \Common\Model\Person::fetch(array('id' => $id));
                $name=$person->first_name.' '.$person->second_name.' '.$person->third_name.' '.$person->last_name;
                \Log\Model\Log::saveNewLog('PERSON_VOUCHER_UPDATED',trans('aid::application.deleted a beneficiary from an aid voucher')  . ' "'.$voucher->title .'" - '. ' "'.$name. '" ');
                $response["status"]= 'success';
                $response["msg"]= trans('aid::application.The row is deleted from db');
            }else{
                $response["status"]= 'failed';
                $response["msg"]= trans('aid::application.The row is not deleted from db');

            }
            return response()->json($response);

        } catch (Exception $e) {
            return response()->json(['status' =>'error',"error_code" => $e->getCode() ,'msg'=>$e->getMessage() ]);
        }

    }

    //----------------------------------------------------------------------------------------------------

    // upload voucher template by voucher id
    public function templateUpload(Request $request)
    {
        $id = $request->input('Voucher_id');
        $Voucher = Vouchers::findOrFail($id);
        $this->authorize('upload', $Voucher);

        $templateFile = $request->file('file');
        $templatePath = $templateFile->store('templates');
        if ($templatePath) {
            if (!$Voucher->template) {
                $Voucher->template = $templatePath;
                $Voucher->save();
            } else {
                if (\Storage::has($Voucher->template)) {
                    \Storage::delete($Voucher->template);
                }
                Vouchers::where(['id'=> $id])->update(['template' => $templatePath]);
            }
            \Log\Model\Log::saveNewLog('VOUCHER_UPDATED',trans('aid::application.Uploaded voucher data export template for') . ' "' . $Voucher->title . '" ');
            return response()->json($templatePath);
        }

        return false;
    }

    // upload logged user organization default voucher template
    public function defaultTemplateUpload(Request $request)
    {
        $this->authorize('upload', Vouchers::class);

        $user = \Auth::user();
        $templateFile = $request->file('file');
        $templatePath = $templateFile->store('templates');
        if ($templatePath) {

            $count= Setting::where(['id'=>'default-voucher-template','organization_id'=>$user->organization_id])->count();
            if($count==1){
                $template=Setting::where(['id'=>'default-voucher-template','organization_id'=>$user->organization_id])->first();
                if (\Storage::has($template->value)) {
                    \Storage::delete($template->value);
                }
                Setting::where('id','default-voucher-template')->update(['value' => $templatePath]);
            }else{
                Setting::insert(['id'=>'default-voucher-template','value'=>$templatePath,'organization_id'=>$user->organization_id]);
            }
            \Log\Model\Log::saveNewLog('PERSONS_VOUCHER_EXPORT',trans('aid::application.Uploaded the default coupon export template'));
            return Setting::where(['id'=>'default-voucher-template','organization_id'=>$user->organization_id])->first();
        }

        return false;
    }

    // get logged user organization default voucher template
    public function defaultTemplate($id){
        $user = \Auth::user();
        $template=Setting::where(['id'=>'default-voucher-template','organization_id'=>$user->organization_id])->first();
        $sub = explode(".", $template->value);
        $token = explode("/", $sub[0]);

        return response()->json(['download_token' => $token[1]]);
    }

    // get voucher template by voucher id
    public function template($id){
        $entry = Vouchers::findOrFail($id);
        $this->authorize('view', $entry);

        $sub = explode(".", $entry->template);
        $token = explode("/", $sub[0]);

        return response()->json(['download_token' => $token[1]]);
    }

    // get download_token to instruction voucher template
    public function templateInstruction(){
        return response()->json(['download_token' => 'voucher-aid-template']);
    }

    // get download_token to import voucher template
    public function importTemplate(){
        return response()->json(['download_token' => 'import-vouchers-template']);
    }

    // get download_token to default voucher template
    public function SysDefaultTemplate(){
        return response()->json(['download_token' => 'voucher-default-template']);
    }

    //----------------------------------------------------------------------------------------------------

    // get voucher attachment by voucher id
    public function attachments($id){
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $entry = Vouchers::with(['attachments','attachments.file'])->findOrFail($id);
        $this->authorize('view', $entry);
        return response()->json(['row'=>$entry]);
    }

    public function getAttachments($id)
    {

        $files = VoucherDocuments::with(['file'])->where('voucher_id',$id)->get();

        if(sizeof($files) != 0){

            $dirName = base_path('storage/app/voucherAttachments');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }

            foreach($files as $k=>$file){
                $extention = explode(".", $file->file->filepath);
                copy(base_path('storage/app/') . $file->file->filepath, $dirName . '/' . $file->file->name . '.' . end($extention));
            }

            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');
            $zip = new \ZipArchive;

            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dirName), \RecursiveIteratorIterator::LEAVES_ONLY);
                foreach ($files as $name => $file)
                {
                    if (!$file->isDir())
                    {
                        $filePath = $file->getRealPath();
                        $relativePath = substr($filePath, strlen($dirName) + 1);
                        $zip->addFile($filePath, $relativePath);
                    }
                }
                $zip->close();

                $dir=$dirName;
                if (is_dir($dir)) {
                    $objects = scandir($dir);
                    foreach ($objects as $object) {
                        if ($object != "." && $object != "..") {
                            if (filetype($dir."/".$object) == "dir"){

                                $objects_1 = scandir($dir."/".$object);
                                foreach ($objects_1 as $object1) {
                                    if ($object1 != "." && $object1 != "..") {
                                        unlink   ($dir."/".$object."/".$object1);
                                    }
                                }
                                rmdir($dir."/".$object);
                            }
                            else {
                                unlink   ($dir."/".$object);
                            }
                        }
                    }
                    rmdir($dir);
                }
                return response()->json(['download_token' => $token]);
            }

            return response()->json(['status' => false]);
        }else{
            return response()->json(['status' => false]);
        }


    }

    // upload attachment and linked to voucher
    public function attachmentUpload(Request $request){

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
//        $this->authorize('upload', $Voucher);
        $file = new VoucherDocuments();
        $file->voucher_id = $request->voucher_id;
        $file->document_id = $request->document_id;
        $file->created_at =date('Y-m-d H:i:s');

        if($file->save()) {
            $voucher = Vouchers::findOrFail($request->voucher_id);
            \Log\Model\Log::saveNewLog('VOUCHER_UPDATED',trans('aid::application.Uploaded an attachment to the aid voucher')  . ' "' . $voucher->title . '" ');
            return response()->json(['status' => 'success' ,'msg' =>trans('aid::application.The row is inserted to db'),'file'=>$file::with(['file'])->first()]);
        }
        return response()->json(['status' => 'failed' ,'msg' =>trans('aid::application.The row is not insert to db')]);
    }

    // set Details for benificery of voucher
    public function setDetails(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $response = array();

        $this->authorize('updatePersonsVoucher', Vouchers::class);

        $source = $request->source;
        $user = \Auth::user();
        $organization_id = $user->organization_id;

        if($request->un_serial){

            $id = $request->un_serial;

            if($source =='id'){
                $inputsToUpdate = [];
                $keys = ['status','receipt_date','receipt_time','receipt_location'];
                foreach ($keys as $key){
                    if(isset($request->$key)){
                        if(!(is_null($request->$key) || $request->$key == ' ' || $request->$key == '')){
                            $val = strip_tags($request->$key) ;
                            if($key == 'receipt_date'){
                                $inputsToUpdate[$key]= date('Y-m-d',strtotime($val)) ;
                            }else{
                                $inputsToUpdate[$key]= $val ;
                            }
                        }
                    }
                }

                $target =VoucherPersons::wherein('id',$request->target)->get();
                $count=0;

                foreach ($target as $key =>$value) {
                    $entry =VoucherPersons::with(['voucher'])->findOrFail($value->id);
                    $voucher = $entry->voucher;
                    foreach ($inputsToUpdate as $k => $vl){
                        $entry->$k = $vl;
                    }
                    if($entry->save()){
                        $person = \Common\Model\Person::fetch(array('id' => $value->person_id));
                        $name=$person->first_name.' '.$person->second_name.' '.$person->third_name.' '.$person->last_name;
                        \Log\Model\Log::saveNewLog('PERSON_VOUCHER_UPDATED',trans('aid.application.edited the case to a beneficiary of an aid voucher') . ' "'.$voucher->title .'" - '. ' "'.$name. '" ');
                        $count++;
                    }
                }

                if($count > 0) {
                    $response["status"]= 'success';
                    $response["msg"]= trans('setting::application.The row is updated');
                }else{
                    $response["status"]= 'failed';
                    $response["msg"]= trans('setting::application.There is no update');
                }

                return response()->json($response);
            }
            elseif($source =='voucher'){
                $status = $request->status;
                $target =VoucherPersons::where(function ($anq) use ($status,$id) {
                                                $anq->whereIn('voucher_id', function ($sq) use ($id) {
                                                $sq->select('id')
                                                    ->from('char_vouchers')
                                                    ->where(function ($q) use ($id) {
                                                         $q->whereNull('deleted_at');
                                                         $q->where('un_serial', $id);
                                                     });

                                            });
                                            $anq->where('status','!=',$status);
                                        })->get();

                $count=0;
                if(sizeof($target) > 0){
                    foreach ($target as $key =>$value) {
                        $entry =VoucherPersons::with(['voucher'])->findOrFail($value->id);
                        $voucher = $entry->voucher;
                        $entry->status = $status;
                        if($entry->save()){
                            $person = \Common\Model\Person::fetch(array('id' => $value->person_id));
                            $name=$person->first_name.' '.$person->second_name.' '.$person->third_name.' '.$person->last_name;
                            \Log\Model\Log::saveNewLog('PERSON_VOUCHER_UPDATED',trans('aid.application.edited the case to a beneficiary of an aid voucher') . ' "'.$voucher->title .'" - '. ' "'.$name. '" ');
                            $count++;
                        }
                    }

                    $response["status"]= 'success';
                    $response["msg"]= trans('setting::application.The row is updated');
                    return response()->json($response);
                }

                return response()->json(["status" => 'failed' , 'msg' => 'جميع الحالات تملك نفس حالة الاستلام الذي تحاول تعميمها']);
            }
            else {

                $importFile = $request->file;
                if ($importFile->isValid()) {

                    $path = $importFile->getRealPath();
                    $excel = \PHPExcel_IOFactory::load($path);
                    $sheets = $excel->getSheetNames();

                    if(in_array('data', $sheets)) {

                        $first = Excel::selectSheets('data')->load($path)->first();

                        if (!is_null($first)) {

                            $keys = $first->keys()->toArray();

                            if (sizeof($keys) > 0) {

                                $validFile = true;

                                $constraint = ["rkm_alhoy", "okt_alastlam", "mkan_alastlam", "tarykh_alastlam", "tm_alastlam"];


                                foreach ($constraint as $item_) {
                                    if ( !in_array($item_, $keys) ) {
                                        $validFile = false;
                                        break;
                                    }
                                }
                                if ($validFile) {
                                    $records = \Excel::selectSheets('data')->load($path)->get();
                                    $total = sizeof($records);

                                    if ($total > 0) {


                                        $response = array();
                                        $processed = [];
                                        $restricted = [];

                                        $invalid_id_card_number = 0;
                                        $success = 0;
                                        $not_nominated = 0;
                                        $duplicated = 0;
                                        $beneficiary = 1;

                                        foreach ($records as $key => $value) {
                                            if ($beneficiary == 1) {
                                                $id_card_number = (int)$value['rkm_alhoy'];
                                                $card_to_check = (int)$value['rkm_alhoy'];
                                                if (!in_array($card_to_check, $processed)) {
                                                    if (GovServices::checkCard($id_card_number)) {
                                                        $exist =VoucherPersons::where(function ($q) use ($id_card_number, $id) {
                                                            $q->whereIn('voucher_id', function ($sq) use ($id_card_number, $id) {
                                                                $sq->select('id')
                                                                    ->from('char_vouchers')
                                                                    ->where(function ($q) use ($id) {
                                                                        $q->whereNull('deleted_at');
                                                                        $q->where('un_serial', $id);
                                                                    });
                                                            });
                                                            $q->whereIn('person_id', function ($sq) use ($id_card_number, $id) {
                                                                $sq->select('id')
                                                                    ->from('char_persons')
                                                                    ->where('id_card_number', $id_card_number);
                                                            });
                                                        })->first();

                                                        if (!is_null($exist)) {

                                                            if (isset($value['tm_alastlam'])) {
                                                                if (!is_null($value['tm_alastlam']) && $value['tm_alastlam'] != ' ' && $value['tm_alastlam'] != '') {
                                                                    $status = ($value['tm_alastlam'] == 'نعم') ? 1 : 2;
                                                                    $exist->status = $status;
                                                                    if($status == 1){
                                                                        if (isset($value['okt_alastlam'])) {
                                                                            if (!is_null($value['okt_alastlam']) && $value['okt_alastlam'] != ' ' && $value['okt_alastlam'] != '') {
                                                                                if (is_object($value['okt_alastlam'])) {
                                                                                    $exist->receipt_time = date('H:i:s', strtotime($value['okt_alastlam']));
                                                                                }
                                                                            }
                                                                        }
                                                                        if (isset($value['mkan_alastlam'])) {
                                                                            if (!is_null($value['mkan_alastlam']) && $value['mkan_alastlam'] != ' ' && $value['mkan_alastlam'] != '') {
                                                                                $exist->receipt_location = $value['mkan_alastlam'];
                                                                            }
                                                                        }
                                                                        if (isset($value['tarykh_alastlam'])) {
                                                                            if (!is_null($value['tarykh_alastlam']) && $value['tarykh_alastlam'] != ' ' && $value['tarykh_alastlam'] != '') {
                                                                                $receipt_date = Helpers::getFormatedDate($value['tarykh_alastlam']);

                                                                                if (!is_null($receipt_date)) {
                                                                                    $exist->receipt_date = $receipt_date;
                                                                                }
                                                                            }
                                                                        }
                                                                    }else{
                                                                        $exist->receipt_time = null;
                                                                        $exist->receipt_date = null;
                                                                        $exist->receipt_location = null;
                                                                    }

                                                                }
                                                            }
                                                            $exist->save();
                                                            $success++;
                                                        } else {
                                                            $restricted[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'not_nominated'];
                                                            $not_nominated++;
                                                        }

                                                    } else {
                                                        $restricted[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'invalid_id_card_number'];
                                                        $invalid_id_card_number++;
                                                    }
                                                    $processed[] = $id_card_number;
                                                } else {
                                                    $restricted[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                                                    $duplicated++;
                                                }

                                            } else {
                                                $id_card_number = (int)$value['rkm_alhoy_rb_alasr'];
                                                $card_to_check = (int)$value['rkm_hoy_almaaal'];
                                                if (!in_array($card_to_check, $processed)) {
                                                    if (GovServices::checkCard($card_to_check)) {
                                                        $pass = true ;

                                                        if (!GovServices::checkCard($id_card_number)) {
                                                            $restricted[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'invalid_id_card_number'];
                                                            $pass = false ;
                                                            $invalid_id_card_number++;
                                                        }

                                                        if($pass == true){
                                                            $exist =VoucherPersons::where(function ($q) use ($id_card_number,$card_to_check, $id) {
                                                                $q->whereIn('voucher_id', function ($sq) use ($id_card_number, $id) {
                                                                    $sq->select('id')
                                                                        ->from('char_vouchers')
                                                                        ->where(function ($q) use ($id) {
                                                                            $q->whereNull('deleted_at');
                                                                            $q->where('un_serial', $id);
                                                                        });
                                                                });
                                                                $q->whereIn('person_id', function ($sq) use ($id_card_number, $id) {
                                                                    $sq->select('id')
                                                                        ->from('char_persons')
                                                                        ->where('id_card_number', $id_card_number);
                                                                });
                                                                $q->whereIn('individual_id', function ($sq) use ($card_to_check, $id) {
                                                                    $sq->select('id')
                                                                        ->from('char_persons')
                                                                        ->where('id_card_number', $card_to_check);
                                                                });
                                                            })->first();

                                                            if (!is_null($exist)) {
                                                                if (isset($value['okt_alastlam'])) {
                                                                    if (!is_null($value['okt_alastlam']) && $value['okt_alastlam'] != ' ' && $value['okt_alastlam'] != '') {
                                                                        if (is_object($value['okt_alastlam'])) {
                                                                            $exist->receipt_time = date('H:i:s', strtotime($value['okt_alastlam']));
                                                                        }
                                                                    }
                                                                }
                                                                if (isset($value['mkan_alastlam'])) {
                                                                    if (!is_null($value['mkan_alastlam']) && $value['mkan_alastlam'] != ' ' && $value['mkan_alastlam'] != '') {
                                                                        $exist->receipt_location = $value['mkan_alastlam'];
                                                                    }
                                                                }
                                                                if (isset($value['tarykh_alastlam'])) {
                                                                    if (!is_null($value['tarykh_alastlam']) && $value['tarykh_alastlam'] != ' ' && $value['tarykh_alastlam'] != '') {
                                                                        $receipt_date = Helpers::getFormatedDate($value['tarykh_alastlam']);

                                                                        if (!is_null($receipt_date)) {
                                                                            $exist->receipt_date = $receipt_date;
                                                                        }
                                                                    }
                                                                }

                                                                $exist->save();
                                                                $success++;
                                                            } else {
                                                                $restricted[] = ['name' => ' ', 'id_card_number' => $card_to_check, 'reason' => 'not_nominated'];
                                                                $not_nominated++;
                                                            }
                                                        }


                                                    } else {
                                                        $restricted[] = ['name' => ' ', 'id_card_number' => $card_to_check, 'reason' => 'invalid_id_card_number'];
                                                        $invalid_id_card_number++;
                                                    }
                                                    $processed[] = $card_to_check;
                                                } else {
                                                    $restricted[] = ['name' => ' ', 'id_card_number' => $card_to_check, 'reason' => 'duplicated'];
                                                    $duplicated++;
                                                }
                                            }
                                        }

                                        if (sizeof($restricted) > 0) {

                                            foreach ($restricted as $k => $v) {
                                                $v['reason'] = trans('aid::application.' . $v['reason']);
                                            }

                                            $token = md5(uniqid());
                                            \Excel::create('export_' . $token, function ($excel) use ($restricted) {
                                                $excel->sheet(trans('aid::application.rejected_sheet'), function ($sheet) use ($restricted) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);

                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet->setCellValue('A1', trans('common::application.id_card_number'));
                                                    $sheet->setCellValue('B1', trans('common::application.reason'));
                                                    $z = 2;
                                                    foreach ($restricted as $k => $v) {
                                                        $sheet->setCellValue('A' . $z, $v['id_card_number']);
                                                        $sheet->setCellValue('B' . $z, trans('aid::application.' . $v['reason']));
                                                        $z++;
                                                    }
                                                });
                                            })->store('xlsx', storage_path('tmp/'));

                                            $response["download_token"] = $token;
                                        }

                                        $response["status"] = 'success';
                                        $response["msg"] = trans('setting::application.The row is updated');
                                        if (sizeof($restricted) == 0) {
                                            $response["msg"] = trans('setting::application.The row is updated');
                                        } else {
                                            if($success == 0 ){
                                                $response["status"] = 'update_error';
                                                $response["msg"]= trans('setting::application.There is no update');
                                            }
                                            $response["msg"] .=  trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                                trans('aid::application.success') . ' : ' . $success . ' , ' .
                                                trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_id_card_number . ' , ' .
                                                trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                                trans('aid::application.not_nominated') . ' : ' . $not_nominated . ' )';
                                        }
                                        return response()->json($response);
                                    }
                                    return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file is empty')]);
                                }
                                return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                            }
                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file is empty')]);
                    }
                    return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('aid::application.The import sheet does not exist in the Excel , please check the label in the instructions or in the template')]);
                }
                return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.invalid file')]);
            }

        }
        else{
            $id = $request->voucher_id;
            $voucher = Vouchers::findOrFail($id);
            $beneficiary = $voucher->beneficiary;

            if($source =='id'){
                $inputsToUpdate = [];
                $keys = ['status','receipt_date','receipt_time','receipt_location'];
                foreach ($keys as $key){
                    if(isset($request->$key)){
                        if(!(is_null($request->$key) || $request->$key == ' ' || $request->$key == '')){
                            $val = strip_tags($request->$key) ;
                            if($key == 'receipt_date'){
                                $inputsToUpdate[$key]= date('Y-m-d',strtotime($val)) ;
                            }else{
                                $inputsToUpdate[$key]= $val ;
                            }
                        }
                    }
                }

                $target =VoucherPersons::wherein('id',$request->target)->get();
                $count=0;

                foreach ($target as $key =>$value) {
                    $entry =VoucherPersons::findOrFail($value->id);
                    foreach ($inputsToUpdate as $k => $vl){
                        $entry->$k = $vl;
                    }
                    if($entry->save()){
                        $person = \Common\Model\Person::fetch(array('id' => $value->person_id));
                        $name=$person->first_name.' '.$person->second_name.' '.$person->third_name.' '.$person->last_name;
                        \Log\Model\Log::saveNewLog('PERSON_VOUCHER_UPDATED',trans('aid.application.edited the case to a beneficiary of an aid voucher') . ' "'.$voucher->title .'" - '. ' "'.$name. '" ');
                        $count++;
                    }
                }

                if($count > 0) {
                    $response["status"]= 'success';
                    $response["msg"]= trans('setting::application.The row is updated');
                }else{
                    $response["status"]= 'failed';
                    $response["msg"]= trans('setting::application.There is no update');
                }

                return response()->json($response);
            }
            elseif($source =='voucher'){

                $status = $request->status;

                $target =VoucherPersons::where(function ($anq) use ($status,$id) {
                    $anq->where('voucher_id',$id);
                    $anq->where('status','!=',$status);
                })->get();
                $count=0;


                if(sizeof($target) > 0){
                    foreach ($target as $key =>$value) {
                        $entry =VoucherPersons::findOrFail($value->id);
                        $entry->status = $status;
                        if($entry->save()){
                            $person = \Common\Model\Person::fetch(array('id' => $value->person_id));
                            $name=$person->first_name.' '.$person->second_name.' '.$person->third_name.' '.$person->last_name;
                            \Log\Model\Log::saveNewLog('PERSON_VOUCHER_UPDATED',trans('aid.application.edited the case to a beneficiary of an aid voucher') . ' "'.$voucher->title .'" - '. ' "'.$name. '" ');
                            $count++;
                        }
                    }

                    $response["status"]= 'success';
                    $response["msg"]= trans('setting::application.The row is updated');
                    return response()->json($response);
                }

                $response["status"]= 'failed';
                $response["msg"]= 'جميع الحالات تملك نفس حالة الاستلام الذي تحاول تعميمها';

                return response()->json($response);
            }
            else {

                $importFile = $request->file;
                if ($importFile->isValid()) {

                    $path = $importFile->getRealPath();
                    $excel = \PHPExcel_IOFactory::load($path);
                    $sheets = $excel->getSheetNames();

                    if(in_array('data', $sheets)) {

                        $first = Excel::selectSheets('data')->load($path)->first();

                        if (!is_null($first)) {

                            $keys = $first->keys()->toArray();

                            if (sizeof($keys) > 0) {

                                $validFile = true;

                                if ($beneficiary == 1) {
                                    $constraint = ["rkm_alhoy", "okt_alastlam", "mkan_alastlam", "tarykh_alastlam", "tm_alastlam"];
                                } else {
                                    $constraint = ["rkm_alhoy_rb_alasr",  "rkm_hoy_almaaal", "okt_alastlam", "mkan_alastlam", "tarykh_alastlam","tm_alastlam"];
                                }

                                foreach ($constraint as $item_) {
                                    if ( !in_array($item_, $keys) ) {
                                        $validFile = false;
                                        break;
                                    }
                                }
                                if ($validFile) {
                                    $records = \Excel::selectSheets('data')->load($path)->get();
                                    $total = sizeof($records);

                                    if ($total > 0) {
                                        $user = \Auth::user();
                                        $beneficiary = $voucher->beneficiary;
                                        $organization_id = $user->organization_id;

                                        $response = array();
                                        $processed = [];
                                        $restricted = [];

                                        $invalid_id_card_number = 0;
                                        $success = 0;
                                        $not_nominated = 0;
                                        $duplicated = 0;

                                        foreach ($records as $key => $value) {
                                            if ($beneficiary == 1) {
                                                $id_card_number = (int)$value['rkm_alhoy'];
                                                $card_to_check = (int)$value['rkm_alhoy'];
                                                if (!in_array($card_to_check, $processed)) {
                                                    if (GovServices::checkCard($id_card_number)) {
                                                        $exist =VoucherPersons::where(function ($q) use ($id_card_number, $id) {
                                                            $q->where('voucher_id', $id);
                                                            $q->whereIn('person_id', function ($sq) use ($id_card_number, $id) {
                                                                $sq->select('id')
                                                                    ->from('char_persons')
                                                                    ->where('id_card_number', $id_card_number);
                                                            });
                                                        })->first();

                                                        if (!is_null($exist)) {

                                                            if (isset($value['tm_alastlam'])) {
                                                                if (!is_null($value['tm_alastlam']) && $value['tm_alastlam'] != ' ' && $value['tm_alastlam'] != '') {
                                                                    $status = ($value['tm_alastlam'] == 'نعم') ? 1 : 2;
                                                                    $exist->status = $status;
                                                                    if($status == 1){
                                                                        if (isset($value['okt_alastlam'])) {
                                                                            if (!is_null($value['okt_alastlam']) && $value['okt_alastlam'] != ' ' && $value['okt_alastlam'] != '') {
                                                                                if (is_object($value['okt_alastlam'])) {
                                                                                    $exist->receipt_time = date('H:i:s', strtotime($value['okt_alastlam']));
                                                                                }
                                                                            }
                                                                        }
                                                                        if (isset($value['mkan_alastlam'])) {
                                                                            if (!is_null($value['mkan_alastlam']) && $value['mkan_alastlam'] != ' ' && $value['mkan_alastlam'] != '') {
                                                                                $exist->receipt_location = $value['mkan_alastlam'];
                                                                            }
                                                                        }
                                                                        if (isset($value['tarykh_alastlam'])) {
                                                                            if (!is_null($value['tarykh_alastlam']) && $value['tarykh_alastlam'] != ' ' && $value['tarykh_alastlam'] != '') {
                                                                                $receipt_date = Helpers::getFormatedDate($value['tarykh_alastlam']);

                                                                                if (!is_null($receipt_date)) {
                                                                                    $exist->receipt_date = $receipt_date;
                                                                                }
                                                                            }
                                                                        }
                                                                    }else{
                                                                        $exist->receipt_time = null;
                                                                        $exist->receipt_date = null;
                                                                        $exist->receipt_location = null;
                                                                    }

                                                                }
                                                            }
                                                            $exist->save();
                                                            $success++;
                                                        } else {
                                                            $restricted[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'not_nominated'];
                                                            $not_nominated++;
                                                        }

                                                    } else {
                                                        $restricted[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'invalid_id_card_number'];
                                                        $invalid_id_card_number++;
                                                    }
                                                    $processed[] = $id_card_number;
                                                } else {
                                                    $restricted[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                                                    $duplicated++;
                                                }

                                            } else {
                                                $id_card_number = (int)$value['rkm_alhoy_rb_alasr'];
                                                $card_to_check = (int)$value['rkm_hoy_almaaal'];
                                                if (!in_array($card_to_check, $processed)) {
                                                    if (GovServices::checkCard($card_to_check)) {
                                                        $pass = true ;

                                                        if (!GovServices::checkCard($id_card_number)) {
                                                            $restricted[] = ['name' => ' ', 'id_card_number' => $id_card_number, 'reason' => 'invalid_id_card_number'];
                                                            $pass = false ;
                                                            $invalid_id_card_number++;
                                                        }

                                                        if($pass == true){
                                                            $exist =VoucherPersons::where(function ($q) use ($id_card_number,$card_to_check, $id) {
                                                                $q->where('voucher_id', $id);
                                                                $q->whereIn('person_id', function ($sq) use ($id_card_number, $id) {
                                                                    $sq->select('id')
                                                                        ->from('char_persons')
                                                                        ->where('id_card_number', $id_card_number);
                                                                });
                                                                $q->whereIn('individual_id', function ($sq) use ($card_to_check, $id) {
                                                                    $sq->select('id')
                                                                        ->from('char_persons')
                                                                        ->where('id_card_number', $card_to_check);
                                                                });
                                                            })->first();

                                                            if (!is_null($exist)) {
                                                                if (isset($value['okt_alastlam'])) {
                                                                    if (!is_null($value['okt_alastlam']) && $value['okt_alastlam'] != ' ' && $value['okt_alastlam'] != '') {
                                                                        if (is_object($value['okt_alastlam'])) {
                                                                            $exist->receipt_time = date('H:i:s', strtotime($value['okt_alastlam']));
                                                                        }
                                                                    }
                                                                }
                                                                if (isset($value['mkan_alastlam'])) {
                                                                    if (!is_null($value['mkan_alastlam']) && $value['mkan_alastlam'] != ' ' && $value['mkan_alastlam'] != '') {
                                                                        $exist->receipt_location = $value['mkan_alastlam'];
                                                                    }
                                                                }
                                                                if (isset($value['tarykh_alastlam'])) {
                                                                    if (!is_null($value['tarykh_alastlam']) && $value['tarykh_alastlam'] != ' ' && $value['tarykh_alastlam'] != '') {
                                                                        $receipt_date = Helpers::getFormatedDate($value['tarykh_alastlam']);

                                                                        if (!is_null($receipt_date)) {
                                                                            $exist->receipt_date = $receipt_date;
                                                                        }
                                                                    }
                                                                }

                                                                $exist->save();
                                                                $success++;
                                                            } else {
                                                                $restricted[] = ['name' => ' ', 'id_card_number' => $card_to_check, 'reason' => 'not_nominated'];
                                                                $not_nominated++;
                                                            }
                                                        }


                                                    } else {
                                                        $restricted[] = ['name' => ' ', 'id_card_number' => $card_to_check, 'reason' => 'invalid_id_card_number'];
                                                        $invalid_id_card_number++;
                                                    }
                                                    $processed[] = $card_to_check;
                                                } else {
                                                    $restricted[] = ['name' => ' ', 'id_card_number' => $card_to_check, 'reason' => 'duplicated'];
                                                    $duplicated++;
                                                }
                                            }
                                        }

                                        if (sizeof($restricted) > 0) {

                                            foreach ($restricted as $k => $v) {
                                                $v['reason'] = trans('aid::application.' . $v['reason']);
                                            }

                                            $token = md5(uniqid());
                                            \Excel::create('export_' . $token, function ($excel) use ($restricted) {
                                                $excel->sheet(trans('aid::application.rejected_sheet'), function ($sheet) use ($restricted) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);

                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet->setCellValue('A1', trans('common::application.id_card_number'));
                                                    $sheet->setCellValue('B1', trans('common::application.reason'));
                                                    $z = 2;
                                                    foreach ($restricted as $k => $v) {
                                                        $sheet->setCellValue('A' . $z, $v['id_card_number']);
                                                        $sheet->setCellValue('B' . $z, trans('aid::application.' . $v['reason']));
                                                        $z++;
                                                    }
                                                });
                                            })->store('xlsx', storage_path('tmp/'));

                                            $response["download_token"] = $token;
                                        }

                                        $response["status"] = 'success';
                                        $response["msg"] = trans('setting::application.The row is updated');
                                        if (sizeof($restricted) == 0) {
                                            $response["msg"] = trans('setting::application.The row is updated');
                                        } else {
                                            if($success == 0 ){
                                                $response["status"] = 'update_error';
                                                $response["msg"]= trans('setting::application.There is no update');
                                            }
                                            $response["msg"] .=  trans('aid::application.total no') . ' : ' . sizeof($records) . ' , ' .
                                                trans('aid::application.success') . ' : ' . $success . ' , ' .
                                                trans('sponsorship::application.invalid_id_card_number') . ' : ' . $invalid_id_card_number . ' , ' .
                                                trans('aid::application.duplicated') . ' : ' . $duplicated . ' , ' .
                                                trans('aid::application.not_nominated') . ' : ' . $not_nominated . ' )';
                                        }
                                        return response()->json($response);
                                    }
                                    return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file is empty')]);
                                }
                                return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                            }
                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file is empty')]);
                    }
                    return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('aid::application.The import sheet does not exist in the Excel , please check the label in the instructions or in the template')]);
                }
                return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.invalid file')]);
            }
        }
    }

    public function setAttachment(Request $request)
    {

        $entry =VoucherPersons::findOrFail($request->cur_id);
        $old_document_id = $entry->document_id;
        $entry->document_id = $request->document_id;
        if($entry->save()){
//            \Log\Model\Log::saveNewLog('PERSON_VOUCHER_UPDATED',trans('aid.application.edited the case to a beneficiary of an aid voucher') . ' "'.$voucher->title .'" - '. ' "'.$name. '" ');
            if(!is_null($old_document_id)){
                $file = File::findOrFail($old_document_id);
                \Illuminate\Support\Facades\Storage::delete($file->filepath);
                foreach (\Document\Model\FileRevision::allforFile($file->id) as $revision) {
                    \Illuminate\Support\Facades\Storage::delete($revision->filepath);
                }
                $file->delete();
            }

            return response()->json(["status" => 'success' ,"msg" =>trans('aid::application.The Document is selected successfully')]);
        }

        return response()->json(["status" => 'failed' ,"msg" =>trans('aid::application.The Document selected failed')]);

    }

    // get persons voucher attachment by voucher id
    public function getBeneficiaryAttachments($id)
    {

        $files =VoucherPersons::with(['file','person','individual'])->where(function ($q) use ($id) {
            $q->whereNotNull('document_id');
            $q->where('voucher_id',$id);
        })->get();

        if(sizeof($files) != 0){

            $dirName = base_path('storage/app/voucherFiles');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }

            foreach($files as $k=>$file){
                $person =$file->person;
                if (!is_null($file->individual)) {
                    $person = $file->individual;
                }
                $SubDirName = $person->first_name .' ' .$person->second_name.' ' .$person->third_name.' ' .$person->last_name .' ';
//                $SubDir = base_path('storage/app/voucherFiles/' .$SubDirName);
//                if (!is_dir($SubDir)) {
//                    @mkdir($SubDir, 0777, true);
//                }

                $extention = explode(".", $file->file->filepath);
                copy(base_path('storage/app/') . $file->file->filepath, $dirName . '/' . $SubDirName . '.' . end($extention));

            }

            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');
            $zip = new \ZipArchive;

            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dirName), \RecursiveIteratorIterator::LEAVES_ONLY);
                foreach ($files as $name => $file)
                {
                    if (!$file->isDir())
                    {
                        $filePath = $file->getRealPath();
                        $relativePath = substr($filePath, strlen($dirName) + 1);
                        $zip->addFile($filePath, $relativePath);
                    }
                }
                $zip->close();

                $dir=$dirName;
                if (is_dir($dir)) {
                    $objects = scandir($dir);
                    foreach ($objects as $object) {
                        if ($object != "." && $object != "..") {
                            if (filetype($dir."/".$object) == "dir"){

                                $objects_1 = scandir($dir."/".$object);
                                foreach ($objects_1 as $object1) {
                                    if ($object1 != "." && $object1 != "..") {
                                        unlink   ($dir."/".$object."/".$object1);
                                    }
                                }
                                rmdir($dir."/".$object);
                            }
                            else {
                                unlink   ($dir."/".$object);
                            }
                        }
                    }
                    rmdir($dir);
                }
                return response()->json(['download_token' => $token]);
            }

            return response()->json(['status' => false]);
        }else{
            return response()->json(['status' => false]);
        }


    }


    // filter trashed voucher
    public function trashed(Request $request)
    {

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $status=$request->action;

        if($status =='filter'){
            $this->authorize('manage', Vouchers::class);
        }else{
            //            $this->authorize('export', Vouchers::class);
        }
        $user = \Auth::user();
        $Org=Organization::findorfail($user->organization_id);
        $master=true;
        if($Org->parent_id){
            $master=false;
        }

        $request->request->add(['organization_id'=> $user->organization_id]);
        $request->request->add(['trashed'=> true ]);
        $VouchersList=Vouchers::filter($request->all());

        if($status =='ExportToExcel'){
            if(sizeof($VouchersList) !=0){
                $data=array();
                foreach($VouchersList as $key =>$value){
                    $data[$key][trans('aid::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        if($master){
                            $data[$key][trans('aid::application.' . $k)]= $v;
                        }else{
                            if($k != 'organization_name'){
                                $data[$key][trans('aid::application.' . $k)]= $v;
                            }
                        }
                    }
                }
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($data) {
                    $excel->setTitle(trans('aid::application.voucher_name'));
                    $excel->setDescription(trans('aid::application.voucher_name'));
                    $excel->sheet(trans('aid::application.vouchers_sheet'), function($sheet) use($data) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);
                        $sheet->getStyle("A1:U1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->fromArray($data);
                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['download_token' => $token]);
            }
            return response()->json(['status' => false]);
        }
        return response()->json(['master'=>$master, 'Vouchers' => $VouchersList['list'], 'total' => $VouchersList['total'], 'beneficiary' => $VouchersList['beneficiary']]);

    }

    // restore trashed aid_repository by id
    public function restore(Request $request, $id)
    {
        $entity = Vouchers::onlyTrashed()->findOrFail($id);
//        $this->authorize('restore', $entity);
        if ($entity->trashed()) {
            $entity->restore();
            \Log\Model\Log::saveNewLog('VOUCHER_RESTORE',trans('aid::application.Restored Voucher')  . ' "'.$entity->title. '" ');
        }
        return response()->json($entity);
    }

}
