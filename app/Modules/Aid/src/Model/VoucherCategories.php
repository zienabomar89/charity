<?php
namespace Aid\Model;

class VoucherCategories  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_vouchers_categories';
    protected $fillable = ['name','parent'];
    protected $hidden = ['created_at','updated_at'];
}