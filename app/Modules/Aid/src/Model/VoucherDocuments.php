<?php
namespace Aid\Model;

class VoucherDocuments  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_vouchers_documents';
    protected $fillable = ['document_id','voucher_id','person_id','created_at'];
    public $timestamps = false;

    public function voucher()
    {
        return $this->belongsTo('Aid\Model\Vouchers','voucher_id','id');
    }
    public function file()
    {
        return $this->belongsTo('Document\Model\File','document_id','id');
    }
    
}