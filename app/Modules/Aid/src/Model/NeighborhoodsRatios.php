<?php
namespace Aid\Model;

class NeighborhoodsRatios  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_organization_neighborhoods_ratios';
    public $timestamps = false;
    protected $fillable = ['organization_id', 'location_id', 'ratio'];

    public static function getRatios($organization_id){
        $language_id =  \App\Http\Helpers::getLocale();
        $entries = \DB::table('char_organization_locations')
                    ->join('char_locations_i18n', function($join) use ($language_id){
                        $join->on('char_organization_locations.location_id', '=','char_locations_i18n.location_id' )
                              ->where('char_locations_i18n.language_id',$language_id);
                    })
                   ->leftjoin('char_organization_neighborhoods_ratios', function($q) use ($organization_id){
                        $q->on('char_organization_neighborhoods_ratios.organization_id', '=', 'char_organization_locations.organization_id');
                        $q->on('char_organization_neighborhoods_ratios.location_id', '=', 'char_organization_locations.location_id');
                    })
                   ->where('char_organization_locations.organization_id',$organization_id)
                   ->selectRaw("char_organization_locations.*,
                                char_locations_i18n.name,
                                get_cases_count(char_organization_locations.organization_id,char_organization_locations.location_id) as count,
                                CASE WHEN char_organization_neighborhoods_ratios.ratio is null THEN 0
                                      Else char_organization_neighborhoods_ratios.ratio
                                      END  as ratio")
                   ->get();

        if(sizeof($entries) >0){
            $total_cnt=0;
            $total_sum=0;
            foreach($entries as $k=>$v){
                $total_cnt += $v->count;
                $total_sum += $v->ratio;
            }
            return ['rows' => $entries,'count' => $total_cnt, 'status' => true, 'sum' => $total_sum];
        }

       return ['status'=>false];
    }
}
