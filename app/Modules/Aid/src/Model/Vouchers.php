<?php
namespace Aid\Model;
use App\Http\Helpers;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vouchers  extends \Illuminate\Database\Eloquent\Model
{

    use SoftDeletes;

    protected $table = 'char_vouchers';
    protected $hidden = ['created_at','updated_at'];
    protected $dates = ['deleted_at'];
    protected $fillable = ['organization_id','organization_project_id','title','content','urgent','header','footer','notes',
                           'case_category_id','exchange_rate','category_id','type','value','center',
                           'currency_id','sponsor_id','beneficiary','voucher_date' ,'count','project_id' ,
                            'transfer_company_id' , 'transfer','allow_day','token','valid_token','serial','collected','un_serial'];

    public function Person()
    {
        return $this->belongsToMany('Common\Model\Person', 'char_vouchers_persons', 'voucher_id', 'person_id');
    }

    public function case_category()
    {
        return $this->belongsTo('Aid\Model\AidsCases','case_category_id','id');
    }

    public function category()
    {
        return $this->belongsTo('Aid\Model\VoucherCategories','category_id','id');
    }

    public function attachments()
    {
        return $this->hasMany('Aid\Model\VoucherDocuments','voucher_id','id');
    }

    public  static function getList($user,$id,$all){

        $language_id =  \App\Http\Helpers::getLocale();
        $UserType=$user->type;

        return \DB::table('char_vouchers')
                ->leftJoin('char_categories as ca', 'ca.id', '=', 'char_vouchers.case_category_id')
                ->where(function ($q) use ($user,$UserType,$id,$all) {
                    $q->whereNull('char_vouchers.deleted_at');

                    if($all == 1){
                        if($UserType == 2) {
                            $q->where(function ($anq) use ($user,$id) {
                                if($id != -1){
                                    $anq->where('beneficiary','=',$id);
                                }

                                if($id == 1){
                                    $anq->where('center','=',0);
                                }
                                $anq->where('status','=',0);
                                $anq->whereNull('project_id');
                                $anq->where('organization_id',$user->organization_id);
                                $anq->orwherein('organization_id', function($quer) use($user) {
                                    $quer->select('organization_id')
                                        ->from('char_user_organizations')
                                        ->where('user_id', '=', $user->id);
                                });
                            });

                        }
                        else{

                            $q->where(function ($anq) use ($user,$id) {
                                if($id != -1){
                                    $anq->where('beneficiary','=',$id);
                                }

                                if($id == 1){
                                    $anq->where('center','=',0);
                                }
                                $anq->where('status','=',0);
                                $anq->whereNull('project_id');
                                /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                                $anq->wherein('organization_id', function($decq) use($user) {
                                    $decq->select('descendant_id')
                                        ->from('char_organizations_closure')
                                        ->where('ancestor_id', '=', $user->organization_id);
                                });
                            });
                        }

                    }else{
                          $q->where(function ($anq) use ($user) {
                            $anq->where('status','=',0);
                            $anq->whereNull('project_id');
                            $anq->where('organization_id',$user->organization_id);
                        });
                    }


                })
                ->selectRaw("char_vouchers.id ,char_vouchers.case_category_id ,char_vouchers.notes,                        
                     CASE  WHEN $language_id = 1 THEN  CONCAT(char_vouchers.title,' - ',ifnull(ca.name,' '))
                           Else CONCAT(char_vouchers.title,' - ',ifnull(ca.en_name,' ')) END  AS title
                    ")->get();
    }

    public  static function getCodesList(){

        $user = \Auth::user();
        $UserType=$user->type;

        return \DB::table('char_vouchers')
            ->leftJoin('char_categories as ca', 'ca.id', '=', 'char_vouchers.case_category_id')
            ->where(function ($q) use ($user,$UserType) {
                $q->whereNull('char_vouchers.deleted_at');
                $q->whereNotNull('char_vouchers.un_serial');
                if($UserType == 2) {
                    $q->where(function ($anq) use ($user) {
//                        $anq->where('status','=',0);
                        $anq->whereNull('project_id');
                        $anq->where('organization_id',$user->organization_id);
                        $anq->orwherein('organization_id', function($quer) use($user) {
                            $quer->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });
                }
                else{
                    $q->where(function ($anq) use ($user) {
//                        $anq->where('status','=',0);
                        $anq->whereNull('project_id');
                        $anq->wherein('organization_id', function($decq) use($user) {
                            $decq->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $user->organization_id);
                        });
                    });
                }



            })
            ->groupBy('un_serial')
            ->selectRaw("char_vouchers.un_serial as id , char_vouchers.un_serial as name")->get();
    }

    public  static function getTitlesList(){

        $user = \Auth::user();
        $UserType=$user->type;

        return \DB::table('char_vouchers')
            ->leftJoin('char_categories as ca', 'ca.id', '=', 'char_vouchers.case_category_id')
            ->where(function ($q) use ($user,$UserType) {
                $q->whereNull('char_vouchers.deleted_at');
                $q->whereNotNull('char_vouchers.un_serial');
                if($UserType == 2) {
                    $q->where(function ($anq) use ($user) {
//                        $anq->where('status','=',0);
                        $anq->whereNull('project_id');
                        $anq->where('organization_id',$user->organization_id);
                        $anq->orwherein('organization_id', function($quer) use($user) {
                            $quer->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });
                }
                else{
                    $q->where(function ($anq) use ($user) {
//                        $anq->where('status','=',0);
                        $anq->whereNull('project_id');
                        $anq->wherein('organization_id', function($decq) use($user) {
                            $decq->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $user->organization_id);
                        });
                    });
                }



            })
            ->groupBy('title')
            ->selectRaw("char_vouchers.title as id , char_vouchers.title as name")->get();
    }

    public  static function getVouchersCodeList(){

        $user = \Auth::user();
        $language_id =  \App\Http\Helpers::getLocale();

        $UserType=$user->type;

        return \DB::table('char_vouchers')
            ->leftJoin('char_categories as ca', 'ca.id', '=', 'char_vouchers.case_category_id')
            ->where(function ($q) use ($user,$UserType) {
                $q->whereNull('deleted_at');
                $q->whereNotNull('serial');
                if($UserType == 2) {
                    $q->where(function ($anq) use ($user) {
//                        $anq->where('status','=',0);
                        $anq->whereNull('project_id');
                        $anq->where('organization_id',$user->organization_id);
                        $anq->orwherein('organization_id', function($quer) use($user) {
                            $quer->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });
                }
                else{
                    $q->where(function ($anq) use ($user) {
//                        $anq->where('status','=',0);
                        $anq->whereNull('project_id');
                        $anq->wherein('organization_id', function($decq) use($user) {
                            $decq->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $user->organization_id);
                        });
                    });
                }



            })
            ->groupBy('serial')
            ->selectRaw("char_vouchers.serial as id , char_vouchers.serial as name")->get();
    }

    public  static function filter($options)
    {

        $language_id =  \App\Http\Helpers::getLocale();
        $user = \Auth::user();
        $UserType=$user->type;
        $condition = [];
        $organization_id = $options['organization_id'];
        $char_vouchers =['title','content','header','footer','notes','category_id','center',
                         'case_category_id','type','sponsor_id','urgent','currency_id','status', 'transfer_company_id' ,
                         'serial','un_serial','collected', 'transfer','allow_day'];
        $all_organization=null;

        foreach ($options as $key => $value) {
            if(in_array($key, $char_vouchers)) {
                if ( $value != "" ) {
                    $data = ['char_vouchers.'. $key => $value];
                    array_push($condition, $data);
                }
            }
        }

        $vouchers= \DB::table('char_vouchers')

            ->leftJoin('char_categories as ca', 'ca.id', '=', 'char_vouchers.case_category_id')
            ->leftJoin('char_currencies', 'char_currencies.id', '=', 'char_vouchers.currency_id')
            ->leftJoin('char_transfer_company', 'char_transfer_company.id', '=', 'char_vouchers.transfer_company_id')
            ->leftJoin('char_vouchers_categories', 'char_vouchers_categories.id', '=', 'char_vouchers.category_id')
            ->join('char_organizations as org','org.id',  '=', 'char_vouchers.organization_id')
            ->leftjoin('char_organization_projects', 'char_organization_projects.id', '=', 'char_vouchers.organization_project_id')
            ->leftJoin('char_organizations as sponsor','sponsor.id',  '=', 'char_vouchers.sponsor_id');


        if(isset($options['all_organization'])){
            if($options['all_organization'] ===true){
                $all_organization=0;
            }elseif($options['all_organization'] ===false){
                $all_organization=1;

            }
        }

        $organizations=[];

        if(isset($options['organization_ids']) && $options['organization_ids'] !=null && $options['organization_ids'] !="" && $options['organization_ids'] !=[] && sizeof($options['organization_ids']) != 0) {
            if($options['organization_ids'][0]==""){
                unset($options['organization_ids'][0]);
            }
            $organizations =$options['organization_ids'];
        }
        $organization_category=[];
        if(isset($options['organization_category']) && $options['organization_category'] !=null &&
            $options['organization_category'] !="" && $options['organization_category'] !=[] && sizeof($options['organization_category']) != 0) {
            if($options['organization_category'][0]==""){
                unset($options['organization_category'][0]);
            }
            $organization_category = $options['organization_category'];
        }

        $trashed = false;
        if(isset($options['trashed']) && $options['trashed'] !=null && $options['trashed'] !="" ) {
            $trashed = $options['trashed'];
        }

        $vouchers =  $vouchers
            ->where(function ($qu) use ($condition,$trashed,$options,$all_organization,$organization_id,$user,$organization_category,$UserType,$organizations) {

                if ($trashed){
                    $qu->whereNotNull('char_vouchers.deleted_at');

                }else{
                    $qu->whereNull('char_vouchers.deleted_at');

                }

//

                    if (count($condition) != 0) {
                        $qu->where(function ($q) use ($condition) {
                                $str = ['char_vouchers.title','char_vouchers.serial','char_vouchers.un_serial',
                                    'char_vouchers.content','char_vouchers.header', 'char_vouchers.footer','char_vouchers.notes'];
                                for ($i = 0; $i < count($condition); $i++) {
                                    foreach ($condition[$i] as $key => $value) {
                                        if(in_array($key, $str)) {
                                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                                        } else {
                                            $q->where($key, '=', $value);
                                        }
                                    }
                                }
                            });
                    }

                    if($all_organization ==0){
                        if(!empty($organizations)){
                            $qu->where(function ($anq) use ($organizations,$organization_category) {
                                $anq->wherein('char_vouchers.organization_id',$organizations);
                                if(!empty($organization_category)){
                                    $anq->wherein('org.category_id',$organization_category);
                                }
                            });
                        }
                        else{
                            if($UserType == 2) {
                                $qu->where(function ($anq) use ($organization_id,$user,$organization_category) {
                                    $anq->where('char_vouchers.organization_id',$organization_id);
                                    $anq->orwherein('char_vouchers.organization_id', function($quer) use($user) {
                                        $quer->select('organization_id')
                                            ->from('char_user_organizations')
                                            ->where('user_id', '=', $user->id);
                                    });

                                    if(!empty($organization_category)){
                                        $anq->wherein('org.category_id',$organization_category);
                                    }
                                });

                            }else{
                                $qu->where(function ($anq) use ($organization_id,$organization_category) {
                                    /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                                    $anq->wherein('char_vouchers.organization_id', function($quer) use($organization_id) {
                                        $quer->select('descendant_id')
                                            ->from('char_organizations_closure')
                                            ->where('ancestor_id', '=', $organization_id);
                                    });
                                    if(!empty($organization_category)){
                                        $anq->wherein('org.category_id',$organization_category);
                                    }
                                });
                            }
                        }
                    }
                    elseif($all_organization ==1){
                        $qu->where('char_vouchers.organization_id',$organization_id);
                    }

            });


        $min_value=null;
         $max_value=null;

         if(isset($options['max_value']) && $options['max_value'] !=null && $options['max_value'] !=""){
             $max_value=$options['max_value'];
         }

         if(isset($options['min_value']) && $options['min_value'] !=null && $options['min_value'] !=""){
             $min_value=$options['min_value'];
         }

         if($min_value != null && $max_value != null) {
             $vouchers = $vouchers->whereRaw(" $max_value >= char_vouchers.value");
             $vouchers = $vouchers->whereRaw(" $min_value <= char_vouchers.value");
         }elseif($max_value != null && $min_value == null) {
             $vouchers = $vouchers->whereRaw(" $max_value >= char_vouchers.value");
         }elseif($max_value == null && $min_value != null) {
             $vouchers = $vouchers->whereRaw(" $min_value <= char_vouchers.value");
         }

         $voucher_source=null;

         if(isset($options['voucher_source']) && $options['voucher_source'] !=null && $options['voucher_source'] !=""){
             $voucher_source=$options['voucher_source'];
         }

         if($voucher_source != null){
             if($voucher_source == 0){
                 $vouchers->whereNotNull('char_vouchers.project_id');
             }
             elseif($voucher_source == 1){
                 $vouchers->whereNull('char_vouchers.project_id');
             }
         }

         $is_organization_project=null;
         if(isset($options['organization_project']) && $options['organization_project'] !=null && $options['organization_project'] !=""){
             $is_organization_project=$options['organization_project'];
         }

         if($is_organization_project != null){
             if($is_organization_project == 0){
                 $vouchers->whereNotNull('char_vouchers.organization_project_id');
             }
             elseif($is_organization_project == 1){
                 $vouchers->whereNull('char_vouchers.organization_project_id');
             }
         }


        $min_count=null;
         $max_count=null;

         if(isset($options['max_count']) && $options['max_count'] !=null && $options['max_count'] !=""){
             $max_count=$options['max_count'];
         }

         if(isset($options['min_count']) && $options['min_count'] !=null && $options['min_count'] !=""){
             $min_count=$options['min_count'];
         }

         if($min_count != null && $max_count != null) {
             $vouchers = $vouchers->whereRaw(" $max_count >= char_vouchers.count");
             $vouchers = $vouchers->whereRaw(" $min_count <= char_vouchers.count");
         }elseif($max_count != null && $min_count == null) {
             $vouchers = $vouchers->whereRaw(" $max_count >= char_vouchers.count");
         }elseif($max_count == null && $min_count != null) {
             $vouchers = $vouchers->whereRaw(" $min_count <= char_vouchers.count");
         }

         $date_to=null;
         $date_from=null;

         if(isset($options['date_to']) && $options['date_to'] !=null){
             $date_to=date('Y-m-d',strtotime($options['date_to']));
         }
         if(isset($options['date_from']) && $options['date_from'] !=null){
             $date_from=date('Y-m-d',strtotime($options['date_from']));
         }
         if($date_from != null && $date_to != null) {
             $vouchers = $vouchers->whereDate('char_vouchers.created_at', '>=', $date_from);
             $vouchers = $vouchers->whereDate('char_vouchers.created_at', '<=', $date_to);
         }elseif($date_from != null && $date_to == null) {
             $vouchers = $vouchers->whereDate('char_vouchers.created_at', '>=', $date_from);
         }elseif($date_from == null && $date_to != null) {
             $vouchers = $vouchers->whereDate('char_vouchers.created_at', '<=', $date_to);
         }

         $voucher_date_to=null;
         $voucher_date_from=null;

         if(isset($options['voucher_date_to']) && $options['voucher_date_to'] !=null){
             $voucher_date_to=date('Y-m-d',strtotime($options['voucher_date_to']));
         }
         if(isset($options['voucher_date_from']) && $options['voucher_date_from'] !=null){
             $voucher_date_from=date('Y-m-d',strtotime($options['voucher_date_from']));
         }

         if($voucher_date_from != null && $voucher_date_to != null) {
             $vouchers = $vouchers->whereDate('char_vouchers.voucher_date', '>=', $voucher_date_from);
             $vouchers = $vouchers->whereDate('char_vouchers.voucher_date', '<=', $voucher_date_to);
         }elseif($voucher_date_from != null && $voucher_date_to == null) {
             $vouchers = $vouchers->whereDate('char_vouchers.voucher_date', '>=', $voucher_date_from);
         }elseif($voucher_date_from == null && $voucher_date_to != null) {
             $vouchers = $vouchers->whereDate('char_vouchers.voucher_date', '<=', $voucher_date_to);
         }

        $financial = trans('common::application.financial');
        $non_financial = trans('common::application.non_financial');

        $from_project = trans('aid::application.from_project');
        $from_organization_project = trans('aid::application.organization_project');
        $manually_ = trans('aid::application.manually_');
        $urgent = trans('aid::application.urgent');
        $not_urgent = trans('aid::application.not_urgent');
        $transfer_company = trans('aid::application.transfer_company');
        $direct = trans('aid::application.direct');

        $yes = trans('aid::application.yes');
        $no = trans('aid::application.no');

        $map = [
            "title" => 'char_vouchers.title',
            "organization_name" => 'org.name',
            "voucher_sponsor_name" => 'sponsor.name',
            "category_name" => 'char_vouchers_categories.name',
            "value" => 'char_vouchers.value',
            "voucher_total" => 'voucher_total',
            "currency" => 'char_currencies.name',
            "count" => 'char_vouchers.count',
            "center" => 'char_vouchers.center',
            "un_serial" => 'char_vouchers.un_serial',
            "serial" => 'char_vouchers.serial',
            "project_id" => 'char_vouchers.project_id',
            "organization_project_id" => 'char_vouchers.organization_project_id',
            "voucher_value" => 'char_vouchers.voucher_value',
            "voucher_date" => 'char_vouchers.voucher_date',
            "case_category_name" => 'ca.name',
            "status" => 'char_vouchers.status'
        ];


        $order = false;

        if (isset($options['sortKeyArr_rev']) && $options['sortKeyArr_rev'] != null && $options['sortKeyArr_rev'] != "" &&
            $options['sortKeyArr_rev'] != [] && sizeof($options['sortKeyArr_rev']) != 0
        ) {

            $order = true;

            foreach ($options['sortKeyArr_rev'] as $key_) {
                $vouchers->orderBy($map[$key_],'desc');
            }
        }


        if (isset($options['sortKeyArr_un_rev']) && $options['sortKeyArr_un_rev'] != null && $options['sortKeyArr_un_rev'] != "" &&
            $options['sortKeyArr_un_rev'] != [] && sizeof($options['sortKeyArr_un_rev']) != 0
        ) {

            $order = true;
            foreach ($options['sortKeyArr_un_rev'] as $key_) {
                $vouchers->orderBy($map[$key_],'asc');
            }
        }


        if(!$order){
            $vouchers->orderBy('char_vouchers.created_at','desc');
        }
        $language_id =  \App\Http\Helpers::getLocale();


        if($options['action'] =='ExportToWord' || $options['action'] =='ExportToPDF' || $options['action'] =='pdf') {
                         
           if(isset($options['items'])){
              if(sizeof($options['items']) > 0 ){
                $vouchers->whereIn('char_vouchers.id',$options['items']);  
             }      
           } 
            return $vouchers->selectRaw("
                              char_vouchers.id,
                              char_organization_projects.title as organization_project,
                              char_vouchers.title,
                              char_vouchers.header,
                              char_vouchers.footer,
                              char_vouchers.content,
                              char_vouchers.notes,
                              char_vouchers.deleted_at,
                              char_vouchers.voucher_date,
                              char_transfer_company.name,
                              char_vouchers.count,
                              char_vouchers.collected,
                              CASE WHEN char_vouchers.un_serial is null THEN '-'
                                              Else char_vouchers.un_serial
                                         END  AS un_serial,
                              CASE WHEN char_vouchers.serial is null THEN '-'
                                              Else char_vouchers.serial
                                         END  AS voucher_serial,
                              ( char_vouchers.count * char_vouchers.value) total,
                              char_vouchers.value,
                              char_vouchers.exchange_rate,
                              char_vouchers.allow_day,  
                              (char_vouchers.exchange_rate * char_vouchers.value) shekel_value,
                              char_vouchers.template,
                              char_vouchers_categories.name as category_name,
                              CASE  WHEN $language_id = 1 THEN ca.name Else ca.en_name END  AS case_category_name,
                              CASE
                                 WHEN char_vouchers.type = 1 THEN '$non_financial'
                                 WHEN char_vouchers.type = 2 THEN '$financial'
                              END
                             AS type,
                             CASE
                                 WHEN char_vouchers.center = 1 THEN '$yes'
                                 WHEN char_vouchers.center != 1 THEN '$no'
                              END
                             AS center,
                              CASE
                                 WHEN char_vouchers.urgent = 0 THEN '$not_urgent'
                                 WHEN char_vouchers.urgent = 1 THEN '$urgent'
                              END
                             AS urgent,
                             CASE
                                 WHEN char_vouchers.transfer = 0 THEN '$direct'
                                 WHEN char_vouchers.transfer = 1 THEN '$transfer_company'
                              END
                             AS transfer,
                             char_vouchers.transfer_company_id,
                             CASE
                                  WHEN char_vouchers.transfer_company_id is null THEN '-'
                                  Else char_transfer_company.name
                             END  AS transfer_company_name,
                             CASE
                                  WHEN char_vouchers.project_id is not null THEN '$from_project'
                                  Else '$manually_'
                             END  AS voucher_source,
                             CASE
                                  WHEN char_vouchers.organization_project_id is not null THEN '$from_organization_project'
                                  Else '$manually_'
                             END  AS organization_project,
                             CASE
                                  WHEN char_vouchers.currency_id is null THEN '-'
                                  Else char_currencies.name
                             END  AS currency,
                              CASE  WHEN $language_id = 1 THEN sponsor.name Else sponsor.en_name END  AS sponsor_name,
                              CASE  WHEN $language_id = 1 THEN org.name Else org.en_name END  AS organization_name,
                              sponsor.logo as sponsor_logo,
                              org.logo as organization_logo,
                             char_vouchers.count as voucher_beneficiary ,
                              char_voucher_total_amount(char_vouchers.id,'o') as voucher_total,
                              char_voucher_total_amount(char_vouchers.id,'sh') as voucher_total_sk_value
                                                        ")->get();
        }
        else if($options['action'] =='ExportToExcel') {
            return $vouchers->selectRaw("CASE WHEN char_vouchers.un_serial is null THEN '-'
                                              Else char_vouchers.un_serial
                                         END  AS un_serial,
                                         CASE WHEN char_vouchers.serial is null THEN '-'
                                              Else char_vouchers.serial
                                         END  AS voucher_serial,
                            char_vouchers.title as voucher_title,
                             char_organization_projects.title as organization_project,
                               char_vouchers.value as voucher_value,                           
                            CASE
                                  WHEN char_vouchers.currency_id is null THEN '-'
                                  Else char_currencies.name
                             END  AS currency,
                             char_vouchers.exchange_rate,
                            (char_vouchers.exchange_rate * char_vouchers.value) shekel_value,
                            CASE  WHEN $language_id = 1 THEN org.name Else org.en_name END  AS organization_name,
                            CASE  WHEN $language_id = 1 THEN sponsor.name Else sponsor.en_name END  AS sponsor_name,
                            char_voucher_total_amount(char_vouchers.id,'o') as voucher_total,
                            char_voucher_total_amount(char_vouchers.id,'sh') as voucher_total_sk_value,
                            char_vouchers.count as voucher_beneficiary ,
                            char_vouchers_categories.name as category_type,
                            CASE  WHEN $language_id = 1 THEN ca.name Else ca.en_name END  AS case_category_name,
                            CASE
                                  WHEN char_vouchers.project_id is not null THEN '$from_project'
                                  Else '$manually_'
                            END  AS voucher_source,
                            CASE
                                  WHEN char_vouchers.organization_project_id is not null THEN '$from_organization_project'
                                  Else '$manually_'
                             END  AS organization_project,
                            CASE
                                   WHEN char_vouchers.type = 1 THEN '$non_financial'
                                   WHEN char_vouchers.type = 2 THEN '$financial'
                            END
                            AS voucher_type,
                            CASE
                                 WHEN char_vouchers.center = 1 THEN '$yes'
                                 WHEN char_vouchers.center != 1 THEN '$no'
                            END
                            AS center,
                            CASE
                                 WHEN char_vouchers.collected = 1 THEN '$yes'
                                 WHEN char_vouchers.collected != 1 THEN '$no'
                            END
                            AS collected,
                             CASE
                                 WHEN char_vouchers.urgent = 0 THEN '$not_urgent'
                                 WHEN char_vouchers.urgent = 1 THEN '$urgent'
                             END
                             AS voucher_urgent,
                              CASE
                                 WHEN char_vouchers.transfer = 0 THEN '$direct'
                                 WHEN char_vouchers.transfer = 1 THEN '$transfer_company'
                              END
                             AS transfer,
                             CASE
                                  WHEN char_vouchers.transfer_company_id is null THEN '-'
                                  Else char_transfer_company.name
                             END  AS transfer_company_name,
                             char_vouchers.content as voucher_content,
                             ifnull(char_vouchers.notes,'-') as voucher_note ,
                             char_vouchers.deleted_at

                                     ")
                ->get();

        }
        else{

         
            $paginate = $vouchers->selectRaw("
                                           char_organization_projects.title as organization_project,
                                           CASE WHEN char_vouchers.organization_id = '$organization_id' THEN 1 Else 0  END  AS is_mine,
                                           CASE  WHEN $language_id = 1 THEN org.name Else org.en_name END  AS organization_name,
                                           CASE  WHEN $language_id = 1 THEN sponsor.name Else sponsor.en_name END  AS sponsor_name,
                                           char_vouchers.*,
                                           CASE  WHEN $language_id = 1 THEN ca.name Else ca.en_name END  AS case_category_name,
                                           CASE
                                              WHEN char_vouchers.type = 1 THEN '$non_financial'
                                              WHEN char_vouchers.type = 2 THEN '$financial'
                                           END  AS voucher_type,
                                           CASE
                                              WHEN char_vouchers.transfer = 0 THEN '$direct'
                                              WHEN char_vouchers.transfer = 1 THEN '$transfer_company'
                                           END
                                          AS transfer,
                                          char_vouchers.deleted_at,

                                         char_vouchers.transfer_company_id,
                                         CASE
                                              WHEN char_vouchers.transfer_company_id is null THEN '-'
                                              Else char_transfer_company.name
                                         END  AS transfer_company_name,
                                         CASE
                                 WHEN char_vouchers.center = 1 THEN '$yes'
                                 WHEN char_vouchers.center != 1 THEN '$no'
                              END
                             AS center,
                             char_vouchers.center as center_,

                                 CASE
                                      WHEN char_vouchers.currency_id is null THEN '-'
                                      Else char_currencies.name
                                 END  AS currency,
                             char_voucher_total_amount(char_vouchers.id,'o') as voucher_total,
                            char_voucher_total_amount(char_vouchers.id,'sh') as voucher_total_sk_value,
                                 char_vouchers.value as voucher_value,
                                   char_vouchers.exchange_rate,
                                   char_vouchers.collected as collected,
                                   CASE WHEN char_vouchers.un_serial is null THEN '-'
                                              Else char_vouchers.un_serial
                                         END  AS un_serial,
                                   char_vouchers.beneficiary as beneficiary_,
                                  (char_vouchers.exchange_rate * char_vouchers.value) shekel_value,
                                 char_count_voucher_persons(char_vouchers.id) as beneficiary,
                                 char_total_voucher_amount(char_vouchers.id) as voucher_total_value,
                               char_vouchers_categories.name as category_name")
                    ;
            $itemsCount = isset($options['itemsCount'])?$options['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$paginate->count());
            $paginate = $paginate->paginate($records_per_page);

            $amount=0;
            $beneficiary=0;
            if (!$trashed){
                if($options['page'] == 1){
                    $total_amount    = $vouchers->selectRaw("sum(char_voucher_total_amount(char_vouchers.id,'sh')) as total")->first();
                    if($total_amount){
                        $amount = $total_amount->total;
                    }

                    $total_beneficiary    = $vouchers->selectRaw("sum(char_count_voucher_persons(char_vouchers.id)) as total")->first();
                    if($total_beneficiary){
                        $beneficiary = $total_beneficiary->total;
                    }
                }
            }

            return ['list' => $paginate, 'total' =>round($amount,0), 'beneficiary' =>round($beneficiary,0)];

        }
        }

    public  static function fetch($id)
    {

        $user = \Auth::user();
        $organization_id = $user->organization_id;

        $language_id =  \App\Http\Helpers::getLocale();
        $financial = trans('common::application.financial');
        $non_financial = trans('common::application.non_financial');
        $financial = trans('common::application.financial');
        $non_financial = trans('common::application.non_financial');

        $from_project = trans('aid::application.from_project');
        $from_organization_project = trans('aid::application.organization_project');
        $manually_ = trans('aid::application.manually_');
        $urgent = trans('aid::application.urgent');
        $not_urgent = trans('aid::application.not_urgent');
        $transfer_company = trans('aid::application.transfer_company');
        $direct = trans('aid::application.direct');

        $yes = trans('aid::application.yes');
        $no = trans('aid::application.no');

        return \DB::table('char_vouchers')
                ->leftJoin('char_categories as ca', 'ca.id', '=', 'char_vouchers.case_category_id')
                ->leftJoin('char_vouchers_categories', 'char_vouchers_categories.id', '=', 'char_vouchers.category_id')
                ->leftJoin('char_currencies', 'char_currencies.id', '=', 'char_vouchers.currency_id')
                ->leftJoin('char_transfer_company', 'char_transfer_company.id', '=', 'char_vouchers.transfer_company_id')
                ->join('char_organizations as org','org.id',  '=', 'char_vouchers.organization_id')
                ->leftJoin('char_organizations as sponsor','sponsor.id',  '=', 'char_vouchers.sponsor_id')
                ->where('char_vouchers.id','=',$id)
                ->selectRaw("char_vouchers.title,
                             char_vouchers.header,
                             char_vouchers.footer,
                             char_vouchers.content,
                             char_vouchers.beneficiary,
                             char_vouchers.organization_id,
                             char_vouchers.sponsor_id,
                             char_vouchers.notes,
                             char_vouchers.status,
                             char_vouchers.value,
                             char_vouchers.allow_day,
                             CASE  WHEN $language_id = 1 THEN ca.name Else ca.en_name END  AS case_category_name,
                             CASE  WHEN $language_id = 1 THEN sponsor.name Else sponsor.en_name END  AS sponsor_name,
                             CASE  WHEN $language_id = 1 THEN org.name Else org.en_name END  AS organization_name,
                             org.logo as organization_logo,
                             sponsor.logo as sponsor_logo,
                             char_vouchers.transfer,
                             char_vouchers.transfer_company_id,
                             CASE
                                  WHEN char_vouchers.transfer_company_id is null THEN '-'
                                  Else char_transfer_company.name
                             END  AS transfer_company_name,
                             char_vouchers.exchange_rate,
                             (char_vouchers.exchange_rate * char_vouchers.value) shekel_value,
                             char_vouchers.template as template,
                             char_vouchers_categories.name as category_name,
                               CASE
                                  WHEN char_vouchers.currency_id is null THEN '-'
                                  Else char_currencies.name
                             END  AS currency,
                           CASE
                                 WHEN char_vouchers.type = 1 THEN '$non_financial'
                                 WHEN char_vouchers.type = 2 THEN '$financial'
                             END
                             AS type ,
                             
                             CASE WHEN char_vouchers.organization_id = '$organization_id' THEN 1 Else 0  END  AS is_mine,
                                           CASE  WHEN $language_id = 1 THEN org.name Else org.en_name END  AS organization_name,
                                           CASE  WHEN $language_id = 1 THEN sponsor.name Else sponsor.en_name END  AS sponsor_name,
                                           char_vouchers.*,
                                           CASE  WHEN $language_id = 1 THEN ca.name Else ca.en_name END  AS case_category_name,
                                           CASE
                                              WHEN char_vouchers.type = 1 THEN '$non_financial'
                                              WHEN char_vouchers.type = 2 THEN '$financial'
                                           END  AS voucher_type,
                                           CASE
                                              WHEN char_vouchers.transfer = 0 THEN '$direct'
                                              WHEN char_vouchers.transfer = 1 THEN '$transfer_company'
                                           END
                                          AS transfer,
                                         char_vouchers.transfer_company_id,
                                         CASE
                                              WHEN char_vouchers.transfer_company_id is null THEN '-'
                                              Else char_transfer_company.name
                                         END  AS transfer_company_name,
                                         CASE
                                 WHEN char_vouchers.center = 1 THEN '$yes'
                                 WHEN char_vouchers.center != 1 THEN '$no'
                              END
                             AS center,
                             char_vouchers.center as center_,

                                 CASE
                                      WHEN char_vouchers.currency_id is null THEN '-'
                                      Else char_currencies.name
                                 END  AS currency,
                             char_voucher_total_amount(char_vouchers.id,'o') as voucher_total,
                            char_voucher_total_amount(char_vouchers.id,'sh') as voucher_total_sk_value,
                                 char_vouchers.value as voucher_value,
                                   char_vouchers.exchange_rate,
                                   char_vouchers.collected as collected,
                                   CASE WHEN char_vouchers.un_serial is null THEN '-'
                                              Else char_vouchers.un_serial
                                         END  AS un_serial,
                                   char_vouchers.beneficiary as beneficiary_,
                                  (char_vouchers.exchange_rate * char_vouchers.value) shekel_value,
                                 char_count_voucher_persons(char_vouchers.id) as beneficiary,
                                 char_total_voucher_amount(char_vouchers.id) as voucher_total_value,
                               char_vouchers_categories.name as category_name
                         ")
                ->first();
    }

    public  static function filterForSponsor($options,$sponsor_id)
    {

        $language_id =  \App\Http\Helpers::getLocale();

        $condition = [];
        $char_vouchers =['title','content','header','footer','notes','category_id','center',
            'case_category_id','type','urgent','currency_id','status', 'transfer_company_id' , 'transfer','allow_day'];
        $all_organization=null;

        foreach ($options as $key => $value) {
            if(in_array($key, $char_vouchers)) {
                if ( $value != "" ) {
                    $data = ['char_vouchers.'. $key => $value];
                    array_push($condition, $data);
                }
            }
        }

        $vouchers= \DB::table('char_vouchers')
                      ->leftJoin('char_categories as ca', 'ca.id', '=', 'char_vouchers.case_category_id')
                      ->leftJoin('char_currencies', 'char_currencies.id', '=', 'char_vouchers.currency_id')
                      ->leftJoin('char_vouchers_categories', 'char_vouchers_categories.id', '=', 'char_vouchers.category_id')
                      ->leftJoin('char_organizations as org','org.id',  '=', 'char_vouchers.organization_id')
                      ->where(function ($q) use ($sponsor_id) {
                        $q->where('char_vouchers.sponsor_id' , $sponsor_id);
                        $q->whereNull('char_vouchers.deleted_at');
                      });


        if (sizeof($condition) > 0) {
                 $vouchers =  $vouchers
                             ->where(function ($q) use ($condition) {
                                 $str = ['char_vouchers.title','char_vouchers.content','char_vouchers.header', 'char_vouchers.footer','char_vouchers.notes'];
                                 for ($i = 0; $i < count($condition); $i++) {
                                     foreach ($condition[$i] as $key => $value) {
                                         if(in_array($key, $str)) {
                                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                                         } else {
                                             $q->where($key, '=', $value);
                                         }
                                     }
                                 }
                              });
             }
        
            if(isset($options['vouchers'])){
                if($options['vouchers'] > 0 ){
                   $vouchers->whereIn('char_vouchers.id',$options['vouchers']);
                } 
            }  

            $organizations=[];
            if(isset($options['organization_ids']) && 
                    $options['organization_ids'] !=null && 
                    $options['organization_ids'] !="" && $options['organization_ids'] !=[] && 
                    sizeof($options['organization_ids']) != 0) {
                if($options['organization_ids'][0]==""){
                    unset($options['organization_ids'][0]);
                }
                if(!empty($options['organization_ids'])){
                   $vouchers->wherein('char_vouchers.organization_id',$options['organization_ids']);
                 }
        
            }

            

        $min_value=null;
         $max_value=null;

         if(isset($options['max_value']) && $options['max_value'] !=null && $options['max_value'] !=""){
             $max_value=$options['max_value'];
         }

         if(isset($options['min_value']) && $options['min_value'] !=null && $options['min_value'] !=""){
             $min_value=$options['min_value'];
         }

         if($min_value != null && $max_value != null) {
             $vouchers = $vouchers->whereRaw(" $max_value >= char_vouchers.value");
             $vouchers = $vouchers->whereRaw(" $min_value <= char_vouchers.value");
         }elseif($max_value != null && $min_value == null) {
             $vouchers = $vouchers->whereRaw(" $max_value >= char_vouchers.value");
         }elseif($max_value == null && $min_value != null) {
             $vouchers = $vouchers->whereRaw(" $min_value <= char_vouchers.value");
         }

         $voucher_source=null;

         if(isset($options['voucher_source']) && $options['voucher_source'] !=null && $options['voucher_source'] !=""){
             $voucher_source=$options['voucher_source'];
         }

         if($voucher_source != null){
             if($voucher_source == 0){
                 $vouchers->whereNotNull('char_vouchers.project_id');
             }
             elseif($voucher_source == 1){
                 $vouchers->whereNull('char_vouchers.project_id');
             }
         }

        $is_organization_project=null;
        if(isset($options['organization_project']) && $options['organization_project'] !=null && $options['organization_project'] !=""){
            $is_organization_project=$options['organization_project'];
        }

        if($is_organization_project != null){
            if($is_organization_project == 0){
                $vouchers->whereNotNull('char_vouchers.organization_project_id');
            }
            elseif($is_organization_project == 1){
                $vouchers->whereNull('char_vouchers.organization_project_id');
            }
        }

        $min_count=null;
         $max_count=null;

         if(isset($options['max_count']) && $options['max_count'] !=null && $options['max_count'] !=""){
             $max_count=$options['max_count'];
         }

         if(isset($options['min_count']) && $options['min_count'] !=null && $options['min_count'] !=""){
             $min_count=$options['min_count'];
         }

         if($min_count != null && $max_count != null) {
             $vouchers = $vouchers->whereRaw(" $max_count >= char_vouchers.count");
             $vouchers = $vouchers->whereRaw(" $min_count <= char_vouchers.count");
         }elseif($max_count != null && $min_count == null) {
             $vouchers = $vouchers->whereRaw(" $max_count >= char_vouchers.count");
         }elseif($max_count == null && $min_count != null) {
             $vouchers = $vouchers->whereRaw(" $min_count <= char_vouchers.count");
         }

         $date_to=null;
         $date_from=null;

         if(isset($options['date_to']) && $options['date_to'] !=null){
             $date_to=date('Y-m-d',strtotime($options['date_to']));
         }
         if(isset($options['date_from']) && $options['date_from'] !=null){
             $date_from=date('Y-m-d',strtotime($options['date_from']));
         }
         if($date_from != null && $date_to != null) {
             $vouchers = $vouchers->whereDate('char_vouchers.created_at', '>=', $date_from);
             $vouchers = $vouchers->whereDate('char_vouchers.created_at', '<=', $date_to);
         }elseif($date_from != null && $date_to == null) {
             $vouchers = $vouchers->whereDate('char_vouchers.created_at', '>=', $date_from);
         }elseif($date_from == null && $date_to != null) {
             $vouchers = $vouchers->whereDate('char_vouchers.created_at', '<=', $date_to);
         }

         $voucher_date_to=null;
         $voucher_date_from=null;

         if(isset($options['voucher_date_to']) && $options['voucher_date_to'] !=null){
             $voucher_date_to=date('Y-m-d',strtotime($options['voucher_date_to']));
         }
         if(isset($options['voucher_date_from']) && $options['voucher_date_from'] !=null){
             $voucher_date_from=date('Y-m-d',strtotime($options['voucher_date_from']));
         }

         if($voucher_date_from != null && $voucher_date_to != null) {
             $vouchers = $vouchers->whereDate('char_vouchers.voucher_date', '>=', $voucher_date_from);
             $vouchers = $vouchers->whereDate('char_vouchers.voucher_date', '<=', $voucher_date_to);
         }elseif($voucher_date_from != null && $voucher_date_to == null) {
             $vouchers = $vouchers->whereDate('char_vouchers.voucher_date', '>=', $voucher_date_from);
         }elseif($voucher_date_from == null && $voucher_date_to != null) {
             $vouchers = $vouchers->whereDate('char_vouchers.voucher_date', '<=', $voucher_date_to);
         }

        $financial = trans('common::application.financial');
        $non_financial = trans('common::application.non_financial');

        $from_project = trans('aid::application.from_project');
        $from_organization_project = trans('aid::application.organization_project');
        $manually_ = trans('aid::application.manually_');
        $urgent = trans('aid::application.urgent');
        $not_urgent = trans('aid::application.not_urgent');
        $transfer_company = trans('aid::application.transfer_company');
        $direct = trans('aid::application.direct');

        $yes = trans('aid::application.yes');
        $no = trans('aid::application.no');

        $map = [
            "title" => 'char_vouchers.title',
            "organization_name" => 'org.name',
            "category_name" => 'char_vouchers_categories.name',
            "value" => 'char_vouchers.value',
            "voucher_total" => 'voucher_total',
            "currency" => 'char_currencies.name',
            "count" => 'char_vouchers.count',
            "center" => 'char_vouchers.center',
            "project_id" => 'char_vouchers.project_id',
            "organization_project_id" => 'char_vouchers.organization_project_id',
            "voucher_date" => 'char_vouchers.voucher_date',
            "case_category_name" => 'ca.name',
            "status" => 'char_vouchers.status'
        ];


        $order = false;

        if (isset($options['sortKeyArr_rev']) && $options['sortKeyArr_rev'] != null && $options['sortKeyArr_rev'] != "" &&
            $options['sortKeyArr_rev'] != [] && sizeof($options['sortKeyArr_rev']) != 0
        ) {

            $order = true;

            foreach ($options['sortKeyArr_rev'] as $key_) {
                $vouchers->orderBy($map[$key_],'desc');
            }
        }


        if (isset($options['sortKeyArr_un_rev']) && $options['sortKeyArr_un_rev'] != null && $options['sortKeyArr_un_rev'] != "" &&
            $options['sortKeyArr_un_rev'] != [] && sizeof($options['sortKeyArr_un_rev']) != 0
        ) {

            $order = true;
            foreach ($options['sortKeyArr_un_rev'] as $key_) {
                $vouchers->orderBy($map[$key_],'asc');
            }
        }


        if(!$order){
            $vouchers->orderBy('char_vouchers.created_at','desc');
        }
        $language_id =  \App\Http\Helpers::getLocale();


        if($options['action'] =='ExportToExcel') {
            return $vouchers->selectRaw("
                            char_vouchers.title as voucher_title,
                            CASE  WHEN $language_id = 1 THEN org.name Else org.en_name END  AS organization_name,
                            char_voucher_total_amount(char_vouchers.id,'o') as voucher_total,
                            CASE
                                  WHEN char_vouchers.currency_id is null THEN '-'
                                  Else char_currencies.name
                             END  AS currency,
                             char_vouchers.exchange_rate,
                            char_voucher_total_amount(char_vouchers.id,'sh') as voucher_total_sk_value,
                            char_vouchers.value as voucher_value,                           
                            char_vouchers.count as voucher_beneficiary ,
                            char_vouchers_categories.name as category_type,
                            CASE  WHEN $language_id = 1 THEN ca.name Else ca.en_name END  AS case_category_name,
                            CASE
                                   WHEN char_vouchers.type = 1 THEN '$non_financial'
                                   WHEN char_vouchers.type = 2 THEN '$financial'
                            END
                            AS voucher_type,
                             CASE
                                 WHEN char_vouchers.urgent = 0 THEN '$not_urgent'
                                 WHEN char_vouchers.urgent = 1 THEN '$urgent'
                             END
                             AS voucher_urgent,
                             char_vouchers.content as voucher_content,
                             ifnull(char_vouchers.notes,'-') as voucher_note
                                     ")
                ->get();

        }
        else{

         
            $paginate = $vouchers->selectRaw("
                                           CASE  WHEN $language_id = 1 THEN org.name Else org.en_name END  AS organization_name,
                                           char_vouchers.*,
                                           CASE  WHEN $language_id = 1 THEN ca.name Else ca.en_name END  AS case_category_name,
                                           CASE
                                              WHEN char_vouchers.type = 1 THEN '$non_financial'
                                              WHEN char_vouchers.type = 2 THEN '$financial'
                                           END  AS voucher_type,
                                 CASE
                                      WHEN char_vouchers.currency_id is null THEN '-'
                                      Else char_currencies.name
                                 END  AS currency,
                             char_voucher_total_amount(char_vouchers.id,'o') as voucher_total,
                            char_voucher_total_amount(char_vouchers.id,'sh') as voucher_total_sk_value,
                                 char_vouchers.value as voucher_value,
                                    char_vouchers.id,
                                    char_vouchers.exchange_rate,
                                   char_vouchers.beneficiary as beneficiary_,
                                  (char_vouchers.exchange_rate * char_vouchers.value) shekel_value,
                                 char_count_voucher_persons(char_vouchers.id) as beneficiary,
                                 char_total_voucher_amount(char_vouchers.id) as voucher_total_value,
                               char_vouchers_categories.name as category_name")
                    ;
            $itemsCount = isset($options['itemsCount'])?$options['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$paginate->count());
            $paginate = $paginate->paginate($records_per_page);
            $amount=0;
            if($options['page'] == 1){
                $total    = $vouchers->selectRaw("sum(char_total_voucher_amount(char_vouchers.id)) as total")->first();
                if($total){
                    $amount = $total->total;}
            }
            return ['list' => $paginate, 'total' =>round($amount,0)];

        }
        }

    public  static function personsList($options = array())
    {

        $language_id =  \App\Http\Helpers::getLocale();
        $defaults = array('all_organization' => false, 'action' => "filter", 'page' => "1");
        $ignore = array('items','all_organization','organization_id','action','page','target' , /*'id_card_number',*/
            'offset' , 'all' , 'organization_ids','itemsCount','sortKeyArr_rev','sortKeyArr_un_rev');

        $options = array_merge($defaults, $options);
        $condition = [];
        $organization_id = $options['organization_id'];

        foreach ($options as $key => $value) {

            if (!in_array($key, $ignore)) {
                if ( $value != "" ) {
                    $data = [$key => $value];
                    array_push($condition, $data);
                }
            }
        }

        if($language_id == 1){
            $vouchers= \DB::table('char_vouchers_persons_view');
        }else{
            $vouchers= \DB::table('char_vouchers_persons_view_en');
        }

        $vouchers->whereNull('deleted_at');

        $all_organization=1;
        if(isset($options['all_organization'])){
            if($options['all_organization'] ===true){
                $all_organization=0;
            }elseif($options['all_organization'] ===false){
                $all_organization=1;
            }
        }

        if($all_organization ==0){
            $organizations=[];
            if(isset($options['organization_ids']) && $options['organization_ids'] !=null && $options['organization_ids'] !="" && $options['organization_ids'] !=[] && sizeof($options['organization_ids']) != 0) {
                if($options['organization_ids'][0]==""){
                    unset($options['organization_ids'][0]);
                }
                $organizations =$options['organization_ids'];
            }

            $organization_category=[];
            if(isset($options['organization_category']) && $options['organization_category'] !=null &&
                $options['organization_category'] !="" && $options['organization_category'] !=[] && sizeof($options['organization_category']) != 0) {
                if($options['organization_category'][0]==""){
                    unset($options['organization_category'][0]);
                }
                $organization_category = $options['organization_category'];
            }

            if(!empty($organizations)){
                $vouchers->where(function ($anq) use ($organizations,$organization_category) {
                    $anq->wherein('organization_id',$organizations);
                    if(!empty($organization_category)){
                        $anq->wherein('org.category_id',$organization_category);
                    }
                });
            }else{

                $user = \Auth::user();
                $UserType=$user->type;

                if($UserType == 2) {
                    $vouchers->where(function ($anq) use ($organization_id,$user,$organization_category) {
                        $anq->where('organization_id',$organization_id);
                        $anq->orwherein('organization_id', function($quer) use($user) {
                            $quer->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                        if(!empty($organization_category)){
                            $anq->wherein('org.category_id',$organization_category);
                        }
                    });

                }
                else{

                    $vouchers->where(function ($anq) use ($organization_id,$organization_category) {
                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                        $anq->wherein('organization_id', function($quer) use($organization_id) {
                            $quer->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $organization_id);
                        });
                        if(!empty($organization_category)){
                            $anq->wherein('org.category_id',$organization_category);
                        }
                    });
                }


            }

        }
        else{
            $vouchers->where('organization_id',$organization_id);
        }

        if (count($condition) != 0) {
            $vouchers->where(function ($q) use ($condition) {
                $str = ['title','content','header','first_name', 'second_name', 'third_name', 'last_name',
                        'individual_first_name', 'individual_second_name','individual_third_name',
                        'individual_last_name', 'footer','notes'];

                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $str)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });
        }


        $min_value=null;
        $max_value=null;

        if(isset($options['max_value']) && $options['max_value'] !=null && $options['max_value'] !=""){
            $max_value=$options['max_value'];
        }

        if(isset($options['min_value']) && $options['min_value'] !=null && $options['min_value'] !=""){
            $min_value=$options['min_value'];
        }

        if($min_value != null && $max_value != null) {
            $vouchers->whereRaw(" ( $max_value >= value and $min_value <= value ) ");
        }elseif($max_value != null && $min_value == null) {
            $vouchers->whereRaw(" ( $max_value >= value ) ");
        }elseif($max_value == null && $min_value != null) {
            $vouchers->whereRaw(" ( $min_value <= value ) ");
        }

        $voucher_source=null;
        if(isset($options['voucher_source']) && $options['voucher_source'] !=null && $options['voucher_source'] !=""){
            $voucher_source=$options['voucher_source'];
        }

        if($voucher_source != null){
            if($voucher_source == 0){
                $vouchers->whereNotNull('project_id');
            }
            elseif($voucher_source == 1){
                $vouchers->whereNull('project_id');
            }
        }

        $is_organization_project=null;
        if(isset($options['organization_project']) && $options['organization_project'] !=null && $options['organization_project'] !=""){
            $is_organization_project=$options['organization_project'];
        }

        if($is_organization_project != null){
            if($is_organization_project == 0){
                $vouchers->whereNotNull('organization_project_id');
            }
            elseif($is_organization_project == 1){
                $vouchers->whereNull('organization_project_id');
            }
        }
        $min_count=null;
        $max_count=null;

        if(isset($options['max_count']) && $options['max_count'] !=null && $options['max_count'] !=""){
            $max_count=$options['max_count'];
        }

        if(isset($options['min_count']) && $options['min_count'] !=null && $options['min_count'] !=""){
            $min_count=$options['min_count'];
        }

        if($min_count != null && $max_count != null) {
            $vouchers->whereRaw(" ( $max_count >= count and $min_count <= count )");
        }elseif($max_count != null && $min_count == null) {
            $vouchers->whereRaw(" ( $max_count >= count )");
        }elseif($max_count == null && $min_count != null) {
            $vouchers->whereRaw(" ( $min_count <= count )");
        }

        $date_to=null;
        $date_from=null;

        if(isset($options['date_to']) && $options['date_to'] !=null){
            $date_to=date('Y-m-d',strtotime($options['date_to']));
        }
        if(isset($options['date_from']) && $options['date_from'] !=null){
            $date_from=date('Y-m-d',strtotime($options['date_from']));
        }
        if($date_from != null && $date_to != null) {
            $vouchers = $vouchers->whereDate('created_at', '>=', $date_from);
            $vouchers = $vouchers->whereDate('created_at', '<=', $date_to);
        }elseif($date_from != null && $date_to == null) {
            $vouchers = $vouchers->whereDate('created_at', '>=', $date_from);
        }elseif($date_from == null && $date_to != null) {
            $vouchers = $vouchers->whereDate('created_at', '<=', $date_to);
        }

        $voucher_date_to=null;
        $voucher_date_from=null;

        if(isset($options['voucher_date_to']) && $options['voucher_date_to'] !=null){
            $voucher_date_to=date('Y-m-d',strtotime($options['voucher_date_to']));
        }
        if(isset($options['voucher_date_from']) && $options['voucher_date_from'] !=null){
            $voucher_date_from=date('Y-m-d',strtotime($options['voucher_date_from']));
        }

        if($voucher_date_from != null && $voucher_date_to != null) {
            $vouchers = $vouchers->whereDate('voucher_date', '>=', $voucher_date_from);
            $vouchers = $vouchers->whereDate('voucher_date', '<=', $voucher_date_to);
        }elseif($voucher_date_from != null && $voucher_date_to == null) {
            $vouchers = $vouchers->whereDate('voucher_date', '>=', $voucher_date_from);
        }elseif($voucher_date_from == null && $voucher_date_to != null) {
            $vouchers = $vouchers->whereDate('voucher_date', '<=', $voucher_date_to);
        }

        $map = [
            "name" => 'name',
            "primary_mobile" => 'primary_mobile',
            "id_card_number" => 'id_card_number',
            "title" => 'title',
            "receipt_status_name" => 'receipt_status_name',
            "organization_name" => 'organization_name',
            "currency_name" => 'currency_name',
            "voucher_sponsor_name" => 'sponsor_name',
            "category_type" => 'category_type',
            "value" => 'value',
            "shekel_value" => 'shekel_value',
            "count" => 'count',
            "exchange_rate" => 'exchange_rate',
            "individual_name" => 'individual_name',
            "individual_id_card_number" => 'individual_id_card_number',
            "beneficiary" => 'beneficiary_name',
            "project_id" => 'project_id',
            "organization_project_id" => 'organization_project_id',
            "voucher_date" => 'voucher_date',
            "status" => 'status',
            "receipt_date" => 'receipt_date',
            "receipt_time" => 'receipt_time',
            "case_category_name" => 'case_category_name',
            "un_serial" => 'un_serial',
            "serial" => 'serial',
            "receipt_location" => 'receipt_location',
            "receipt_status" => 'status'
        ];

        $order = false;

        if (isset($options['sortKeyArr_rev']) && $options['sortKeyArr_rev'] != null && $options['sortKeyArr_rev'] != "" &&
            $options['sortKeyArr_rev'] != [] && sizeof($options['sortKeyArr_rev']) != 0
        ) {

            $order = true;

            foreach ($options['sortKeyArr_rev'] as $key_) {
                $vouchers->orderBy($map[$key_],'desc');
            }
        }


        if (isset($options['sortKeyArr_un_rev']) && $options['sortKeyArr_un_rev'] != null && $options['sortKeyArr_un_rev'] != "" &&
            $options['sortKeyArr_un_rev'] != [] && sizeof($options['sortKeyArr_un_rev']) != 0
        ) {

            $order = true;
            foreach ($options['sortKeyArr_un_rev'] as $key_) {
                $vouchers->orderBy($map[$key_],'asc');
            }
        }


        if(!$order){
            $vouchers->orderBy('created_at','desc');
        }


        $male = trans('common::application.male');
        $female = trans('common::application.female');
        $receipt = trans('common::application.receipt');
        $not_receipt = trans('common::application.not_receipt');
        $financial = trans('common::application.financial');
        $non_financial = trans('common::application.non_financial');

        $from_project = trans('aid::application.from_project');
        $from_organization_project = trans('aid::application.organization_project');
        $manually_ = trans('aid::application.manually_');
        $urgent = trans('aid::application.urgent');
        $not_urgent = trans('aid::application.not_urgent');
        $transfer_company = trans('aid::application.transfer_company');
        $direct = trans('aid::application.direct');

        $head = trans('aid::application.head of family');
        $other = trans('aid::application.other person');

        if($options['action'] =='ExportToExcel') {
            
             if(isset($options['items'])){
                if(sizeof($options['items']) > 0 ){
                  $vouchers->whereIn('id',$options['items']);  
               }      
             }  

            $vouchers->selectRaw("
                                  un_serial as un_serial, 
                                  serial as voucher_serial, 
                                  title as voucher_title, 
                                  CASE  WHEN $language_id = 1 THEN sponsor_name      Else en_sponsor_name END  AS sponsor_name,
                                  CASE  WHEN $language_id = 1 THEN organization_name Else en_organization_name END  AS organization_name,
                                  CASE  WHEN $language_id = 1 THEN case_category_name Else en_case_category_name END  AS case_category_name,
                                  CASE  WHEN beneficiary = 1 THEN '$head' Else '$other' END  AS beneficiary_name, 
                                  name, marital_status,
                                  CASE WHEN p_gender is null THEN '-'  WHEN p_gender = 1 THEN '$male' WHEN p_gender = 2 THEN '$female' END  AS gender,
                                  id_card_number,
                                  family_count,
                                  CASE  WHEN beneficiary = 1 THEN ' ' Else individual_name END  AS individual_name, 
                                  CASE  WHEN beneficiary = 1 THEN ' ' Else individual_id_card_number END  AS individual_id_card_number, 
                                  CASE  WHEN beneficiary = 1 THEN '-' 
                                        WHEN beneficiary != 1 and ind_gender is null THEN '-'  
                                        WHEN beneficiary != 1 and ind_gender = 1 THEN '$male' 
                                        WHEN beneficiary != 1 and ind_gender = 2 THEN '$female' END  AS individual_gender,
                                  CASE  WHEN beneficiary = 1 THEN ' ' Else individual_marital_status END  AS individual_marital_status, 
                                  primary_mobile, wataniya, secondery_mobile, phone, 
                                        country, district, region, nearlocation, square, mosque, street_address, full_address,
                                        voucher_date,
                                        CASE
                                                WHEN receipt_status = 1 THEN '$receipt'
                                                WHEN receipt_status = 2 THEN ' $not_receipt'
                                                END
                                     as receipt_status_name,
                                        receipt_date,     receipt_time_,receipt_location,category_type, 
                                        value as voucher_value,
                                        exchange_rate,
                                         CASE  WHEN $language_id = 1 THEN currency_name Else en_currency_name END  AS currency_name
                                         ,shekel_value,
                                        CASE  WHEN project_id is null THEN '$from_project'
                                          Else '$manually_'
                                         END  AS voucher_source,
                                                                      CASE
                                  WHEN organization_project_id is not null THEN '$from_organization_project'
                                  Else '$manually_'
                             END  AS organization_project,
                                     CASE
                                           WHEN type = 1 THEN '$non_financial'
                                           WHEN type = 2 THEN '$financial'
                                      END
                                     AS voucher_type,
                                     CASE
                                         WHEN urgent = 0 THEN '$not_urgent'
                                         WHEN urgent = 1 THEN '$urgent'
                                     END
                                     AS voucher_urgent,
                                      
                                     CASE
                                         WHEN transfer = 0 THEN '$direct'
                                         WHEN transfer = 1 THEN '$transfer_company'
                                      END
                                     AS transfer,
                 transfer_company_name");

            return  $vouchers->get();

        }
        else{
             $vouchers->selectRaw("
             un_serial as un_serial, 
                                  serial as voucher_serial, 
                                  title as voucher_title, 
                                                           id,
          CASE  WHEN $language_id = 1 THEN sponsor_name      Else en_sponsor_name END  AS sponsor_name,
                                   CASE  WHEN $language_id = 1 THEN organization_name Else en_organization_name END  AS organization_name,
                                   CASE  WHEN $language_id = 1 THEN case_category_name Else en_case_category_name END  AS case_category_name,
                                   CASE  WHEN beneficiary = 1 THEN '$head' Else '$other' END  AS beneficiary_name, 
                                   name,
                                   marital_status,
                                   CASE WHEN p_gender is null THEN '-'  WHEN p_gender = 1 THEN '$male' WHEN p_gender = 2 THEN '$female' END  AS gender,
                                   id_card_number,family_count,
                                   CASE  WHEN beneficiary = 1 THEN '-' Else individual_name END  AS individual_name, 
                                   CASE  WHEN beneficiary = 1 THEN '-' Else individual_id_card_number END  AS individual_id_card_number, 
                                   CASE  WHEN beneficiary = 1 THEN '-' 
                                         WHEN beneficiary != 1 and ind_gender is null THEN '-'  
                                         WHEN beneficiary != 1 and ind_gender = 1 THEN '$male' 
                                         WHEN beneficiary != 1 and ind_gender = 2 THEN '$female' END  AS individual_gender,
                                   CASE  WHEN beneficiary = 1 THEN '-' Else individual_marital_status END  AS individual_marital_status, 
                                   primary_mobile, wataniya, secondery_mobile, phone, 
                                   country, district, region, nearlocation, square, mosque, street_address, full_address,
                                        voucher_date,
                                        exchange_rate,
                                         CASE
                                                WHEN receipt_status = 1 THEN '$receipt'
                                                WHEN receipt_status = 2 THEN ' $not_receipt'
                                                END
                                     as receipt_status_name,
                                        receipt_date,     receipt_time_,receipt_location,category_type, value as voucher_value,
                                        CASE  WHEN $language_id = 1 THEN currency_name Else en_currency_name END  AS currency_name,
                                        shekel_value,
                                        CASE  WHEN project_id is null THEN '$from_project'
                                          Else '$manually_'
                                         END  AS voucher_source,
                                                                      CASE
                                  WHEN organization_project_id is not null THEN '$from_organization_project'
                                  Else '$manually_'
                             END  AS organization_project,
                                     CASE
                                           WHEN type = 1 THEN '$non_financial'
                                           WHEN type = 2 THEN '$financial'
                                      END
                                     AS voucher_type,
                                    
                                     CASE
                                         WHEN urgent = 0 THEN '$not_urgent'
                                         WHEN urgent = 1 THEN '$urgent'
                                     END
                                     AS voucher_urgent,
                                      
                                     CASE
                                         WHEN transfer = 0 THEN '$direct'
                                         WHEN transfer = 1 THEN '$transfer_company'
                                      END
                                     AS transfer,
                                     char_persons_receipt_count(voucher_id,person_id) as receipt_count,
                 transfer_company_name");
            $paginate = $vouchers;
            $itemsCount = isset($options['itemsCount'])?$options['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$paginate->count());
            $paginate = $paginate->paginate($records_per_page);
            $amount=0;
            if($options['page'] == 1){
                $total    = $vouchers->selectRaw("sum(shekel_value) as total")->first();
                if($total){
                    $amount = $total->total;
                }
            }
            return ['list' => $paginate, 'total' =>round($amount,0)];
        }
    }
    
    public  static function personsFilterForSponsor($options = array(),$sponsor_id)
    {

        $language_id =  \App\Http\Helpers::getLocale();

        $defaults = array('all_organization' => false, 'action' => "filter");
        $ignore = array('persons','all_organization','ctype','organization_id','action',
            'page','target' ,'offset' , 'all' , 'organization_ids','itemsCount','sortKeyArr_rev','sortKeyArr_un_rev');

        $options = array_merge($defaults, $options);
        $condition = [];

        foreach ($options as $key => $value) {

            if (!in_array($key, $ignore)) {
                if ( $value != "" ) {
                    $data = [$key => $value];
                    array_push($condition, $data);
                }
            }
        }

        if($language_id == 1){
            $vouchers= \DB::table('char_vouchers_persons_view');
        }else{
            $vouchers= \DB::table('char_vouchers_persons_view_en');
        }

       $vouchers->where(function ($q) use ($sponsor_id) {
           $q->whereNull('deleted_at');
           $q->where('sponsor_id',$sponsor_id);
       });


        if (count($condition) != 0) {
            $vouchers =  $vouchers
                ->where(function ($q) use ($condition) {
                    $str = ['title','content','header',
                        'first_name', 'second_name', 'third_name', 'last_name',
                        'individual_first_name', 'individual_second_name',
                        'individual_third_name', 'individual_last_name',
                        'footer','notes'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $str)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });
        }

 

            if(isset($options['organization_ids']) && 
                     $options['organization_ids'] !=null && 
                     $options['organization_ids'] !="" && 
                     $options['organization_ids'] !=[] && 
                     sizeof($options['organization_ids']) != 0) {
                if($options['organization_ids'][0]==""){
                    unset($options['organization_ids'][0]);
                }
                $vouchers->wherein('organization_id',$options['organization_ids']);
                
            }

        $min_value=null;
        $max_value=null;

        if(isset($options['max_value']) && $options['max_value'] !=null && $options['max_value'] !=""){
            $max_value=$options['max_value'];
        }

        if(isset($options['min_value']) && $options['min_value'] !=null && $options['min_value'] !=""){
            $min_value=$options['min_value'];
        }

        if($min_value != null && $max_value != null) {
            $vouchers = $vouchers->whereRaw(" $max_value >= value");
            $vouchers = $vouchers->whereRaw(" $min_value <= value");
        }elseif($max_value != null && $min_value == null) {
            $vouchers = $vouchers->whereRaw(" $max_value >= value");
        }elseif($max_value == null && $min_value != null) {
            $vouchers = $vouchers->whereRaw(" $min_value <= value");
        }

        $voucher_source=null;

        if(isset($options['voucher_source']) && $options['voucher_source'] !=null && $options['voucher_source'] !=""){
            $voucher_source=$options['voucher_source'];
        }

        if($voucher_source != null){
            if($voucher_source == 0){
                $vouchers->whereNotNull('project_id');
            }
            elseif($voucher_source == 1){
                $vouchers->whereNull('project_id');
            }
        }

        $is_organization_project=null;
        if(isset($options['organization_project']) && $options['organization_project'] !=null && $options['organization_project'] !=""){
            $is_organization_project=$options['organization_project'];
        }

        if($is_organization_project != null){
            if($is_organization_project == 0){
                $vouchers->whereNotNull('organization_project_id');
            }
            elseif($is_organization_project == 1){
                $vouchers->whereNull('organization_project_id');
            }
        }

        $min_count=null;
        $max_count=null;

        if(isset($options['max_count']) && $options['max_count'] !=null && $options['max_count'] !=""){
            $max_count=$options['max_count'];
        }

        if(isset($options['min_count']) && $options['min_count'] !=null && $options['min_count'] !=""){
            $min_count=$options['min_count'];
        }

        if($min_count != null && $max_count != null) {
            $vouchers = $vouchers->whereRaw(" $max_count >= count");
            $vouchers = $vouchers->whereRaw(" $min_count <= count");
        }elseif($max_count != null && $min_count == null) {
            $vouchers = $vouchers->whereRaw(" $max_count >= count");
        }elseif($max_count == null && $min_count != null) {
            $vouchers = $vouchers->whereRaw(" $min_count <= count");
        }

        $date_to=null;
        $date_from=null;

        if(isset($options['date_to']) && $options['date_to'] !=null){
            $date_to=date('Y-m-d',strtotime($options['date_to']));
        }
        if(isset($options['date_from']) && $options['date_from'] !=null){
            $date_from=date('Y-m-d',strtotime($options['date_from']));
        }
        if($date_from != null && $date_to != null) {
            $vouchers = $vouchers->whereDate('created_at', '>=', $date_from);
            $vouchers = $vouchers->whereDate('created_at', '<=', $date_to);
        }elseif($date_from != null && $date_to == null) {
            $vouchers = $vouchers->whereDate('created_at', '>=', $date_from);
        }elseif($date_from == null && $date_to != null) {
            $vouchers = $vouchers->whereDate('created_at', '<=', $date_to);
        }

        $voucher_date_to=null;
        $voucher_date_from=null;

        if(isset($options['voucher_date_to']) && $options['voucher_date_to'] !=null){
            $voucher_date_to=date('Y-m-d',strtotime($options['voucher_date_to']));
        }
        if(isset($options['voucher_date_from']) && $options['voucher_date_from'] !=null){
            $voucher_date_from=date('Y-m-d',strtotime($options['voucher_date_from']));
        }

        if($voucher_date_from != null && $voucher_date_to != null) {
            $vouchers = $vouchers->whereDate('voucher_date', '>=', $voucher_date_from);
            $vouchers = $vouchers->whereDate('voucher_date', '<=', $voucher_date_to);
        }elseif($voucher_date_from != null && $voucher_date_to == null) {
            $vouchers = $vouchers->whereDate('voucher_date', '>=', $voucher_date_from);
        }elseif($voucher_date_from == null && $voucher_date_to != null) {
            $vouchers = $vouchers->whereDate('voucher_date', '<=', $voucher_date_to);
        }

        $map = [
            "name" => 'name',
            "primary_mobile" => 'primary_mobile',
            "id_card_number" => 'id_card_number',
            "title" => 'title',
            "receipt_status_name" => 'receipt_status_name',
            "organization_name" => 'organization_name',
            "currency_name" => 'currency_name',
            "voucher_sponsor_name" => 'sponsor_name',
            "category_type" => 'category_type',
            "value" => 'value',
            "shekel_value" => 'shekel_value',
            "count" => 'count',
            "exchange_rate" => 'exchange_rate',
            "individual_name" => 'individual_name',
            "individual_id_card_number" => 'individual_id_card_number',
            "beneficiary" => 'beneficiary_name',
            "project_id" => 'project_id',
            "organization_project_id" => 'organization_project_id',
            "voucher_date" => 'voucher_date',
            "status" => 'status',
            "receipt_date" => 'receipt_date',
            "receipt_time" => 'receipt_time',
            "case_category_name" => 'case_category_name',
            "receipt_location" => 'receipt_location',
            "receipt_status" => 'status'
        ];

        $order = false;

        if (isset($options['sortKeyArr_rev']) && $options['sortKeyArr_rev'] != null && $options['sortKeyArr_rev'] != "" &&
            $options['sortKeyArr_rev'] != [] && sizeof($options['sortKeyArr_rev']) != 0
        ) {

            $order = true;

            foreach ($options['sortKeyArr_rev'] as $key_) {
                $vouchers->orderBy($map[$key_],'desc');
            }
        }


        if (isset($options['sortKeyArr_un_rev']) && $options['sortKeyArr_un_rev'] != null && $options['sortKeyArr_un_rev'] != "" &&
            $options['sortKeyArr_un_rev'] != [] && sizeof($options['sortKeyArr_un_rev']) != 0
        ) {

            $order = true;
            foreach ($options['sortKeyArr_un_rev'] as $key_) {
                $vouchers->orderBy($map[$key_],'asc');
            }
        }


        if(!$order){
            $vouchers->orderBy('created_at','desc');
        }


        $male = trans('common::application.male');
        $female = trans('common::application.female');
        $receipt = trans('common::application.receipt');
        $not_receipt = trans('common::application.not_receipt');
        $financial = trans('common::application.financial');
        $non_financial = trans('common::application.non_financial');

        $from_project = trans('aid::application.from_project');
        $from_organization_project = trans('aid::application.organization_project');
        $manually_ = trans('aid::application.manually_');
        $urgent = trans('aid::application.urgent');
        $not_urgent = trans('aid::application.not_urgent');
        $transfer_company = trans('aid::application.transfer_company');
        $direct = trans('aid::application.direct');

        $head = trans('aid::application.head of family');
        $other = trans('aid::application.other person');

            
        if($options['action'] =='ExportToExcel') {
            
        if(isset($options['persons'])){
            if($options['persons'] > 0 ){
               $vouchers->whereIn('id',$options['persons']);
            } 
        }  
        
        
// primary_mobile, wataniya, secondery_mobile, phone, 
//                                        country, district, region, nearlocation, square, mosque, street_address, full_address,
            $vouchers->selectRaw("title as voucher_title, 
                                  CASE  WHEN $language_id = 1 THEN organization_name Else en_organization_name END  AS organization_name,
                                  CASE  WHEN $language_id = 1 THEN case_category_name Else en_case_category_name END  AS case_category_name,
                                  CASE  WHEN beneficiary = 1 THEN '$head' Else '$other' END  AS beneficiary_name, 
                                  name, marital_status,
                                  CASE WHEN p_gender is null THEN '-'  WHEN p_gender = 1 THEN '$male' WHEN p_gender = 2 THEN '$female' END  AS gender,
                                  id_card_number,
                                  family_count,
                                  CASE  WHEN beneficiary = 1 THEN ' ' Else individual_name END  AS individual_name, 
                                  CASE  WHEN beneficiary = 1 THEN ' ' Else individual_id_card_number END  AS individual_id_card_number, 
                                  CASE  WHEN beneficiary = 1 THEN '-' 
                                        WHEN beneficiary != 1 and ind_gender is null THEN '-'  
                                        WHEN beneficiary != 1 and ind_gender = 1 THEN '$male' 
                                        WHEN beneficiary != 1 and ind_gender = 2 THEN '$female' END  AS individual_gender,
                                  CASE  WHEN beneficiary = 1 THEN ' ' Else individual_marital_status END  AS individual_marital_status, 
                                 
                                        voucher_date,
                                        CASE
                                                WHEN receipt_status = 1 THEN '$receipt'
                                                WHEN receipt_status = 2 THEN ' $not_receipt'
                                                END
                                     as receipt_status_name,
                                        receipt_date,     receipt_time_,receipt_location,category_type, 
                                        value as voucher_value,
                                        exchange_rate,
                                         CASE  WHEN $language_id = 1 THEN currency_name Else en_currency_name END  AS currency_name
                                         ,shekel_value,
                                     CASE
                                           WHEN type = 1 THEN '$non_financial'
                                           WHEN type = 2 THEN '$financial'
                                      END
                                     AS voucher_type,
                                     CASE
                                         WHEN urgent = 0 THEN '$not_urgent'
                                         WHEN urgent = 1 THEN '$urgent'
                                     END
                                     AS voucher_urgent");

            return  $vouchers->get();

        }
        else{
             $vouchers->selectRaw("title as voucher_title,
                                   CASE  WHEN $language_id = 1 THEN sponsor_name      Else en_sponsor_name END  AS sponsor_name,
                                   CASE  WHEN $language_id = 1 THEN organization_name Else en_organization_name END  AS organization_name,
                                   CASE  WHEN $language_id = 1 THEN case_category_name Else en_case_category_name END  AS case_category_name,
                                   CASE  WHEN beneficiary = 1 THEN '$head' Else '$other' END  AS beneficiary_name, 
                                   name,
                                   marital_status,
                                   CASE WHEN p_gender is null THEN '-'  WHEN p_gender = 1 THEN '$male' WHEN p_gender = 2 THEN '$female' END  AS gender,
                                   id_card_number,family_count,
                                   CASE  WHEN beneficiary = 1 THEN '-' Else individual_name END  AS individual_name, 
                                   CASE  WHEN beneficiary = 1 THEN '-' Else individual_id_card_number END  AS individual_id_card_number, 
                                   CASE  WHEN beneficiary = 1 THEN '-' 
                                         WHEN beneficiary != 1 and ind_gender is null THEN '-'  
                                         WHEN beneficiary != 1 and ind_gender = 1 THEN '$male' 
                                         WHEN beneficiary != 1 and ind_gender = 2 THEN '$female' END  AS individual_gender,
                                   CASE  WHEN beneficiary = 1 THEN '-' Else individual_marital_status END  AS individual_marital_status, 
                                   primary_mobile, wataniya, secondery_mobile, phone, 
                                   country, district, region, nearlocation, square, mosque, street_address, full_address,
                                        voucher_date,
                                        exchange_rate,
                                         CASE
                                                WHEN receipt_status = 1 THEN '$receipt'
                                                WHEN receipt_status = 2 THEN ' $not_receipt'
                                                END
                                     as receipt_status_name,
                                        receipt_date,     receipt_time_,receipt_location,category_type, value as voucher_value,
                                        CASE  WHEN $language_id = 1 THEN currency_name Else en_currency_name END  AS currency_name,
                                        shekel_value,
                                        CASE  WHEN project_id is null THEN '$from_project'
                                          Else '$manually_'
                                         END  AS voucher_source,
                                                                      CASE
                                  WHEN organization_project_id is not null THEN '$from_organization_project'
                                  Else '$manually_'
                             END  AS organization_project,
                                     CASE
                                           WHEN type = 1 THEN '$non_financial'
                                           WHEN type = 2 THEN '$financial'
                                      END
                                     AS voucher_type,
                                    
                                     CASE
                                         WHEN urgent = 0 THEN '$not_urgent'
                                         WHEN urgent = 1 THEN '$urgent'
                                     END
                                     AS voucher_urgent,
                                      
                                     CASE
                                         WHEN transfer = 0 THEN '$direct'
                                         WHEN transfer = 1 THEN '$transfer_company'
                                      END
                                     AS transfer,
                                     char_persons_receipt_count(voucher_id,person_id) as receipt_count,
                 transfer_company_name");
            $paginate = $vouchers;
            
            $itemsCount = isset($options['itemsCount'])?$options['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$paginate->count());
            $paginate = $paginate->paginate($records_per_page);
            $amount=0;
            if($options['page'] == 1){
                $total    = $vouchers->selectRaw("sum(shekel_value) as total")->first();
                if($total){
                    $amount = $total->total;
                }
            }
            return ['list' => $paginate, 'total' =>round($amount,0)];
        }
    }
        
}