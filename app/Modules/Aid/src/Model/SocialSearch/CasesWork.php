<?php
namespace Aid\Model\SocialSearch;

class CasesWork  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_social_search_cases_work';
    protected $primaryKey='search_cases_id ';
    protected $fillable = ['search_cases_id','working','work_status_id','work_job_id','work_wage_id','work_location','can_work','work_reason_id','has_other_work_resources'];
    public $timestamps = false;

}
