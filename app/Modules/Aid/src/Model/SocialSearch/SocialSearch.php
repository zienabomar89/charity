<?php

namespace Aid\Model\SocialSearch;
use App\Http\Helpers;
use Illuminate\Database\Eloquent\SoftDeletes;

class SocialSearch  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_social_search';
    protected $hidden = ['created_at','updated_at'];
    protected $fillable = ['organization_id','category_id','user_id','title','notes','status', 'date','date_to','date_from'];
    protected $dates = ['deleted_at'];
    use SoftDeletes;

    public static function filter($options)
    {

        $condition = [];
        $user = \Auth::user();
        $organization_id = $user->organization_id;
        $UserType=$user->type;

//        'organization_id',
        $filters = ['category_id','user_id','title','notes','status'];

        foreach ($options as $key => $value) {
            if(in_array($key,$filters)) {
                if ($value != "" ) {
                    $data = [$key => (int) $value];
                    array_push($condition,$data);
                }
            }
        }

        $query = self::query()
                     ->join('char_categories','char_categories.id',  '=', 'char_social_search.category_id')
                     ->join('char_organizations','char_organizations.id',  '=', 'char_social_search.organization_id')
                     ->join('char_users','char_users.id',  '=', 'char_social_search.user_id');

  
        $begin_date_from=null;
        $end_date_from=null;


        if(isset($options['begin_date_from']) && $options['begin_date_from'] !=null){
            $begin_date_from=date('Y-m-d',strtotime($options['begin_date_from']));
        }

        if(isset($options['end_date_from']) && $options['end_date_from'] !=null){
            $end_date_from=date('Y-m-d',strtotime($options['end_date_from']));
        }


        if($begin_date_from != null && $end_date_from != null) {
            $query->whereBetween( 'char_social_search.date_from', [ $begin_date_from, $end_date_from]);
        }elseif($begin_date_from != null && $end_date_from == null) {
            $query->whereDate('char_social_search.date_from', '>=', $begin_date_from);
        }elseif($begin_date_from == null && $end_date_from != null) {
            $query->whereDate('char_social_search.date_from', '<=', $end_date_from);
        }

        $begin_date_to=null;
        $end_date_to=null;

        if(isset($options['end_date_to']) && $options['end_date_to'] !=null){
            $end_date_to=date('Y-m-d',strtotime($options['end_date_to']));
        }

        if(isset($options['begin_date_to']) && $options['begin_date_to'] !=null){
            $begin_date_to=date('Y-m-d',strtotime($options['begin_date_to']));
        }

        if($begin_date_to != null && $end_date_to != null) {
            $query->whereBetween( 'char_social_search.date_from', [ $begin_date_to, $end_date_to]);
        }elseif($begin_date_to != null && $end_date_to == null) {
            $query->whereDate('char_social_search.date_from', '>=', $begin_date_to);
        }elseif($begin_date_to == null && $end_date_to != null) {
            $query->whereDate('char_social_search.date_from', '<=', $end_date_to);
        }

        $created_at_to=null;
        $created_at_from=null;
        if(isset($options['created_at_from']) && $options['created_at_from'] !=null){
            $created_at_from=date('Y-m-d',strtotime($options['created_at_from']));
        }
        if(isset($options['created_at_to']) && $options['created_at_to'] !=null){
            $created_at_to=date('Y-m-d',strtotime($options['created_at_to']));
        }

        if($created_at_from != null && $created_at_to != null) {
            $query->whereBetween('char_social_search.created_at', [ $created_at_from, $created_at_to]);
        }elseif($created_at_from != null && $created_at_to == null) {
            $query->whereDate('char_social_search.created_at', '>=', $created_at_from);
        }elseif($created_at_from == null && $created_at_to != null) {
            $query->whereDate('char_social_search.created_at', '<=', $created_at_to);
        }

        if (count($condition) != 0) {
            $query->where(function ($q) use ($condition) {
                $names = ['title', 'notes'];
                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $names)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });
        }

        if(isset($options['trashed'])){
            if($options['trashed'] == true || $options['trashed'] == 'true'){
                $query->onlyTrashed();
            }else{
                $query->whereNull('char_social_search.deleted_at');
            }

        }else{
            $query->whereNull('char_social_search.deleted_at');
        }


        if(isset($options['all_organization'])){
            if($options['all_organization'] ===true){
                $all_organization=0;
            }elseif($options['all_organization'] ===false){
                $all_organization=1;

            }
        }

        if($all_organization ==0){

            $organizations=[];
            if(isset($options['organization_ids']) && $options['organization_ids'] !=null && $options['organization_ids'] !="" && $options['organization_ids'] !=[] && sizeof($options['organization_ids']) != 0) {
                if($options['organization_ids'][0]==""){
                    unset($options['organization_ids'][0]);
                }
                $organizations =$options['organization_ids'];
            }

            if(!empty($organizations)){
                $query->wherein('char_social_search.organization_id',$organizations);
            }else{
                if($UserType == 2) {
                    $query->where(function ($anq) use ($organization_id,$user) {
                        $anq->where('char_social_search.organization_id',$organization_id);
                        $anq->orwherein('char_social_search.organization_id', function($quer) use($user) {
                            $quer->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });

                }else{
                    $query->where(function ($anq) use ($organization_id) {
                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                        $anq->wherein('char_social_search.organization_id', function($quer) use($organization_id) {
                            $quer->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $organization_id);
                        });
                    });
                }

            }
        }
        elseif($all_organization ==1){
            $query->where('char_social_search.organization_id',$organization_id);

        }




        $finished = trans('common::application.finished');
        $draft = trans('common::application.draft');
        $inProgress = trans('common::application.inProgress');

        $language_id =  \App\Http\Helpers::getLocale();

        $query->selectRaw("char_social_search.id ,
                                    char_social_search.category_id ,
                                    char_social_search.user_id ,
                                    char_social_search.organization_id ,
                                    char_social_search.status ,
                                    char_social_search.close ,
                                    char_social_search.title ,
                                    CASE WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name ,
                                    CONCAT(char_users.firstname,' ',char_users.lastname)as user_name,
                                    CASE WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS category_name,
                                    char_social_search.notes ,
                                    char_social_search.date_from ,
                                    char_social_search.date_to ,
                                    char_social_search.created_at as created_at_date ,
                                    CASE
                                        WHEN char_social_search.status = 1 THEN '$draft'
                                        WHEN char_social_search.status = 2 THEN ' $inProgress'
                                        WHEN char_social_search.status = 3 THEN ' $finished'
                                   END as status_name,char_social_search.deleted_at");

        if($options['action'] =='paginate') {
            $itemsCount = isset($options['itemsCount'])? $options['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
            return $query=$query->paginate($records_per_page);
        }else{
            if(isset($options['items'])){
              if(sizeof($options['items']) > 0 ){
                $query->whereIn('char_social_search.id',$options['items']);  
             }      
           }  
        }

        return $query->get();
    }

}