<?php

namespace Aid\Model\SocialSearch;

class CasesHealth  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_social_search_cases_health';
    protected $primaryKey='search_cases_id ';
    protected $fillable = [ 'search_cases_id','condition','details','disease_id', 'has_health_insurance',
                            'health_insurance_number', 'health_insurance_type',
                            'has_device', 'used_device_name'];
    public $timestamps = false;

}
