<?php
namespace Aid\Model\SocialSearch;

use App\Http\Helpers;
use Common\Model\CaseModel;

class CasesFamily  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_social_search_cases_family';
    protected $primaryKey='id';
    protected $fillable = ['search_cases_id', 'id_card_number','card_type', 'first_name','second_name', 'third_name',
                            'last_name', 'full_name','prev_family_name', 'gender','marital_status_id',
                            'birthday','kinship_id', 'primary_mobile','watanya', 'condition','disease_id',
                            'details','grade', 'currently_study','school',
                          ];
    public $timestamps = false;

    public static $_translator = [
        "noaa_albtak"=>'card_type',
        "rkm_alhoy" =>'id_card_number',
        "alasm_alaol_aarby"=>'first_name',
        "alasm_althany_aarby"=>'second_name',
        "alasm_althalth_aarby"=>'third_name',
        "alaaael_aarby"=>'last_name',
        "asm_alaaael_alsabk"=>'prev_family_name',
        "tarykh_almylad"=>'birthday',
        "mkan_almylad"=>'birth_place',
        "aljns"=>'gender',
        "aljnsy"=>'nationality',
        "aadd_afrad_alasr"=>'family_cnt',
        "aadd_alabna_ghyr_almtzojyn"=>'male_live',
        "aadd_albnat_alghyr_mtzojat"=>'female_live',
        "aadd_alzojat"=>'spouses',
        "alhal_alajtmaaay"=>'marital_status_id',
        "hal_almoatn"=>'refugee',
        "rkm_krt_altmoyn"=>'unrwa_card_number',
        "aldol"=>'country',
        "almhafth"=>'governarate',
        "almdyn"=>'city',
        "almntk"=>'location_id',
        "almsjd"=>'mosque_id',
        "tfasyl_alaanoan"=>'street_address',
        "rkm_aljoal"=>'primary_mobile',
        "rkm_alotny"=>'watanya',
        "rkm_joal_ahtyaty"=>'secondary_mobile',
        "rkm_hatf"=>'phone',
        "asm_albnk"=>'bank_id',
        "alfraa"=>'branch_name',
        "asm_sahb_alhsab"=>'account_owner',
        "rkm_alhsab"=>'account_number',
        "hl_yaaml"=>'working',
        "hl_taaml"=>'working',
        "kadr_aal_alaaml"=>'can_work',
        "sbb_aadm_alkdr"=>'work_reason_id',
        "hal_alaaml"=>'work_status_id',
        "fe_alajr"=>'work_wage_id',
        "alothyf"=>'work_job_id',
        "mkan_alaaml"=>'work_location',
        "aldkhl_alshhry_shykl"=>'monthly_income',

        "aadd_alghrf"=>'rooms',
        "almsah_balmtr"=>'area',
        "kym_alayjar"=>'rent_value',
        "noaa_almlky"=>'property_type_id',
        "mlky_almnzl"=>'property_type_id',
        "noaa_skf_almnzl"=>'roof_material_id',
        "skf_almnzl"=>'roof_material_id',

        "hal_almskn"=>'residence_condition',
        "hal_alathath"=>'indoor_condition',
        "kably_almnzl_llskn"=>'habitable',

        "odaa_almnzl"=>'house_condition',

        "hl_oaadt_balbna_mn_kbl_okal_alghoth_ao_jh_akhr"=>'promised',
        "asm_almoess_alty_oaadt_balbna"=>'organization_name',
        "ahtyajat_alaaael_bshd"=>'needs',
        "tosyat_albahth_alajtmaaay"=>'notes',
        "ydrs_ao_mtaalm"=>"currently_study" ,
        "noaa_aldras"=>"type" ,
        "jh_aldras"=>"authority" ,
        "almoehl_alaalmy"=>"stage" ,
        "almrhl_aldrasy"=>"degree" ,
        "altkhss"=>"specialization" ,
        "alsf"=>"grade" ,
        "alsn_aldarsy"=>"year" ,
        "asm_almdrs"=>"school" ,
        "akhr_ntyj" =>"points" ,
        "almsto_althsyl"=>"level",
        "alhal_alshy"=>"condition",
        "almrd_ao_aleaaak"=>"disease_id",
        "tfasyl_alhal_alshy"=>"details",
        "tarykh_alzyar" => "visited_at",
        "asm_albahth_alajtmaaay" => "visitor",
        "hl_ysly"=>"prayer",
        "sbb_trk_alsla"=>"prayer_reason",
        "hl_yhfth_alkran"=>"save_quran",
        "aadd_alajza"=>"quran_parts",
        "aadd_alsor"=>"quran_chapters",
        "sbb_trk_althfyth"=>"quran_reason",
        "mlthk_balmsjd"=>"quran_center",
        "tarykh_ofa_alab"=>"quran_center",
        "tarykh_alofa"=>"death_date",
        "sbb_alofa"=>"death_cause_id",
        "sl_alkrab"=>"kinship_id",
        "hl_alam_maayl"=>"mother_is_guardian",
        "hl_ldyh_msadr_dkhl_akhr" => "has_other_work_resources",
        "aldkhl_alshhry_alfaaly_shykl" => "actual_monthly_income",
        "hl_bhaj_ltrmym_llmoaem" => "need_repair",
        "osf_odaa_almnzl" => "repair_notes",
        "hl_ldyh_tamyn_shy" => "has_health_insurance",
        "noaa_altamyn_alshy" => "health_insurance_type",
        "rkm_altamyn_alshy" => "health_insurance_number",
        "hl_ystkhdm_jhaz" => "has_device",
        "asm_aljhaz_almstkhdm" => "used_device_name",
        "almrd_ao_alaaaak" => "disease_id",
        "rkm_hoy_albahth" => "visitor_card",
        "ray_albahth_alajtmaaay" => "visitor_opinion",
        "tkyym_albahth_lhal_almnzl"=> "visitor_evaluation",
        "hl_ldyh_thmm_maly"=> "receivables",
        "kym_althmm_balshykl"=> "receivables_sk_amount",
    ];

    public static function getList($id)
    {

        $language_id = 1;
        $en_language_id= 2;

        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');

        $not_selected = trans('common::application.not_selected');
        $refugee = trans('common::application.refugee');
        $citizen = trans('common::application.citizen');

        $not_exist = trans('common::application.not_exist');
        $exist = trans('common::application.exist');

        $not_work = trans('common::application.not_work');
        $work = trans('common::application.work');


        $perfect = trans('common::application.perfect');
        $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
        $special_needs = trans('common::application.special_needs');

        $excellent = trans('common::application.excellent');
        $acceptable = trans('common::application.acceptable');
        $good = trans('common::application.good');
        $very_good = trans('common::application.very_good');
        $weak = trans('common::application.weak');
        $very_bad_condition = trans('common::application.very_bad_condition');
        $bad_condition = trans('common::application.bad_condition');
        $bad = trans('common::application.bad_condition');
        $very_bad = trans('common::application.very_bad_condition');

        $qualified = trans('common::application.qualified');
        $not_qualified = trans('common::application.not qualified');

        $vocational = trans('common::application.vocational');
        $academic = trans('common::application.academic');

        $the_first  = trans('common::application.the_first') ;
        $the_second = trans('common::application.the_second') ;
        $the_third  = trans('common::application.the_third') ;
        $the_fourth = trans('common::application.the_fourth') ;
        $the_Fifth  = trans('common::application.the_Fifth');
        $the_Sixth  = trans('common::application.the_Sixth');
        $the_Seventh = trans('common::application.the_Seventh');
        $the_Eighth= trans('common::application.the_Eighth');
        $the_Ninth = trans('common::application.the_Ninth');
        $the_tenth = trans('common::application.the_tenth');
        $the_eleventh = trans('common::application.the_eleventh');
        $the_twelveth= trans('common::application.the_twelveth');

        $special_studies= trans('common::application.special_studies');
        $vocational_training= trans('common::application.vocational_training');
        $first_year_of_university= trans('common::application.first_year_of_university');
        $second_year_of_university= trans('common::application.second_year_of_university');
        $third_year_of_university= trans('common::application.third_year_of_university');
        $fourth_year_of_university= trans('common::application.fourth_year_of_university');
        $fifth_year_of_university= trans('common::application.fifth_year_of_university');

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $not_promise = trans('common::application.not_promise');
        $promise = trans('common::application.promise');

        $kinship_ids = [2,22,21,29];


         return  \DB::table('char_social_search_cases_family')
                          ->leftjoin('char_marital_status_i18n', function($q)use ($language_id){
                            $q->on('char_marital_status_i18n.marital_status_id', '=', 'char_social_search_cases_family.marital_status_id');
                            $q->where('char_marital_status_i18n.language_id', $language_id);
                          })
                          ->leftjoin('char_kinship_i18n', function($q)use ($language_id){
                                $q->on('char_kinship_i18n.kinship_id', '=', 'char_social_search_cases_family.kinship_id');
                                $q->where('char_kinship_i18n.language_id', $language_id);
                          })
                          ->leftjoin('char_diseases_i18n', function ($q)  use ($language_id){
                            $q->on('char_diseases_i18n.disease_id', '=', 'char_social_search_cases_family.disease_id');
                            $q->where('char_diseases_i18n.language_id', $language_id);
                          })
                          ->where('char_social_search_cases_family.search_cases_id', '=', $id)
                          ->selectRaw("CONCAT(ifnull(char_social_search_cases_family.first_name, ' '), ' ' ,ifnull(char_social_search_cases_family.second_name, ' '),' ',ifnull(char_social_search_cases_family.third_name, ' '),' ', ifnull(char_social_search_cases_family.last_name,' ')) AS name,
                                       CASE WHEN char_social_search_cases_family.id_card_number is null THEN ' ' Else char_social_search_cases_family.id_card_number  END  AS id_card_number,
                                       CASE WHEN char_social_search_cases_family.birthday is null THEN ' ' Else DATE_FORMAT(char_social_search_cases_family.birthday,'%Y/%m/%d')  END  AS birthday,
                                       CASE WHEN  char_social_search_cases_family.birthday is null THEN ' ' Else  EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(), char_social_search_cases_family.birthday)))) END AS age,
                                       CASE WHEN char_social_search_cases_family.marital_status_id is null THEN ' '  Else char_marital_status_i18n.name END AS marital_status,
                                       CASE WHEN char_social_search_cases_family.gender is null THEN ' '
                                       WHEN char_social_search_cases_family.gender = 1 THEN '$male'
                                             WHEN char_social_search_cases_family.gender = 2 THEN '$female'
                                      END AS gender,
                                      CASE  WHEN char_social_search_cases_family.currently_study is null THEN '-'
                                            WHEN char_social_search_cases_family.currently_study = 1 THEN '$yes'
                                            WHEN char_social_search_cases_family.currently_study = 0 THEN '$no'
                                         END
                                      AS currently_study,
                                       CASE
                                                               WHEN char_social_search_cases_family.grade is null THEN ' '
                                                               WHEN char_social_search_cases_family.grade = 1 THEN '$the_first '
                                                               WHEN char_social_search_cases_family.grade = 2 THEN '$the_second '
                                                               WHEN char_social_search_cases_family.grade = 3 THEN '$the_third '
                                                               WHEN char_social_search_cases_family.grade = 4 THEN '$the_fourth '
                                                               WHEN char_social_search_cases_family.grade = 5 THEN '$the_Fifth '
                                                               WHEN char_social_search_cases_family.grade = 6 THEN '$the_Sixth '
                                                               WHEN char_social_search_cases_family.grade = 7 THEN '$the_Seventh '
                                                               WHEN char_social_search_cases_family.grade = 8 THEN '$the_Eighth '
                                                               WHEN char_social_search_cases_family.grade = 9 THEN '$the_Ninth '
                                                               WHEN char_social_search_cases_family.grade = 10 THEN '$the_tenth'
                                                               WHEN char_social_search_cases_family.grade = 11 THEN '$the_eleventh'
                                                               WHEN char_social_search_cases_family.grade = 12 THEN '$the_twelveth'
                                                               WHEN char_social_search_cases_family.grade = 13 THEN '$special_studies'
                                                               WHEN char_social_search_cases_family.grade = 14 THEN '$vocational_training'
                                                               WHEN char_social_search_cases_family.grade = 15 THEN '$first_year_of_university'
                                                               WHEN char_social_search_cases_family.grade = 16 THEN '$second_year_of_university'
                                                               WHEN char_social_search_cases_family.grade = 17 THEN '$third_year_of_university'
                                                               WHEN char_social_search_cases_family.grade = 18 THEN '$fourth_year_of_university'
                                                               WHEN char_social_search_cases_family.grade = 19 THEN '$fifth_year_of_university' 
                                                    END
                                                     AS grade,
                                                     CASE WHEN char_social_search_cases_family.school is null THEN ' '   Else char_social_search_cases_family.school END  AS school,
                                                     CASE
                                                          WHEN char_social_search_cases_family.condition is null THEN ' '
                                                          WHEN char_social_search_cases_family.condition = 1 THEN '$perfect'
                                                          WHEN char_social_search_cases_family.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                          WHEN char_social_search_cases_family.condition = 3 THEN '$special_needs'
                                                     END
                                                     AS health_status,
                                                      CASE WHEN char_social_search_cases_family.details is null THEN ' '   Else char_social_search_cases_family.details END  AS diseases_details,
                                                     CASE WHEN char_diseases_i18n.name is null THEN ' ' Else char_diseases_i18n.name END   AS diseases_name,
                                                     char_kinship_i18n.name as kinship_name
                                                   ")
                ->orderBy('char_social_search_cases_family.birthday')
                ->get();


    }

    public static function isUnique($id_card_number,$search_id,$search_cases_card)
    {

        $father = Cases::where(function ($q) use ($search_cases_card,$search_id) {
            $q->where('id_card_number', '=', $search_cases_card);
            $q->where('search_id', '=', $search_id);
            })
            ->first();

        if(is_null($father)){
            return ['status'=>true , 'reason' => 'father_not_enter_on_search', 'row' => null];
        }

        $entry = self::query()
                ->where(function ($q) use ($id_card_number,$father) {
                    $q->where('id_card_number', '=', $id_card_number);
                    $q->where('search_cases_id', '=', $father->id);
                })
                ->first();

        return (is_null($entry))? ['status' => false , 'search_cases_id' => $father->id ] : array('status' => true , 'reason' => 'old_person', 'row' => $entry);
    }

    public static function filterInputs($inputs){

        $data = [];
        foreach ($inputs as $k=>$v) {
            if(!is_null($inputs[$k]) && $inputs[$k] != " "){
                $data[$k] = $v;
            }
        }

        $dates = ['birthday','visited_at','death_date'];
        $constant =  [
            'birth_place', 'nationality', 'marital_status_id','death_cause_id',
            'country', 'governarate', 'city', 'location_id',
            'kinship_id',
            'mosque_id',
            'bank_id', 'branch_name','disease_id','authority', 'stage','degree',
            'work_reason_id', 'work_status_id', 'work_wage_id', 'work_job_id',
            'property_type_id', 'roof_material_id',
            'residence_condition', 'indoor_condition', 'habitable', 'house_condition',"country_id",
            "district_id", "mosque_id", "neighborhood_id", "region_id", "square_id"];
        $hasParent =  ['governarate', 'city', 'location_id', 'branch_name','mosque_id',"district_id", "neighborhood_id", "region_id", "square_id"];
        $Parents = ['governarate'=>'country',
            'city' => 'governarate',
            'location_id' => 'city',
            'branch_name' => 'bank_id' ,
            "district_id" =>'country_id',
            "region_id" =>'district_id',
            "neighborhood_id" =>'region_id',
            "square_id" =>'neighborhood_id',
            "mosque_id" =>'square_id'];

        $ParentsNames = [
            'governarate'=>'aldol',
            'city' => 'almhafth',
            'location_id' => 'almdyn',
            'branch_name' => 'asm_albnk',
            "district_id" =>'aldol',
            "region_id" =>'almhafth',
            "neighborhood_id" =>'almntk',
            "square_id" =>'alhy',
            "mosque_id" =>'almrbaa'];

        $numeric =  ['id_card_number', 'spouses', 'unrwa_card_number',
            'primary_mobile', 'watanya', 'secondary_mobile', 'phone',
            'monthly_income', 'rooms','area','rent_value',"points" ,"quran_parts","quran_chapters",
        ];

        $row=[];
        $translator =self::$_translator;
        $row['errors']=[];

        foreach ($data as $k=>$v) {
            $key=null;
            if(isset($translator[$k])){
                $key=$translator[$k];
            }
            if(!is_null($key)){

                if(in_array($key,$numeric)){
                    if(!is_null($v) && $v != " "){
                        if(!is_numeric($v)){
                            $row['errors'][$key]=trans('common::application.invalid_number');
                        }
                    }
                }

                if(in_array($key,$dates)){
                    if(!is_null($v) && $v != " "){
                        $date_ = Helpers::getFormatedDate($v);
                        if (!is_null($date_)) {
                            $row[$key]=$date_;
                        }else{
                            $row['errors'][$key]=trans('common::application.invalid_date');
                        }
                    }
                }
                elseif($key =='gender'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'ذكر') ? 1 : 2;
                    }
                }elseif($key =='deserted'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 2;
                    }
                }elseif($key =='has_device'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 2;
                    }
                }elseif($key =='has_health_insurance'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 2;
                    }
                }elseif($key =='need_repair'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? '1' : '2';
                    }
                }elseif($key =='has_other_work_resources'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 2;
                    }
                }elseif($key =='card_type'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        if ($vlu == 'بطاقة تعريف'){
                            $row[$key] = 2;
                        }elseif ($vlu == 'جواز سفر'){
                            $row[$key] = 3;
                        }else{
                            $row[$key] = 1;
                        }
                    }
                }
                elseif($key =='type'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'تقني') ? 2 : 1;
                    }
                }
                elseif($key =='level'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){

                        if($vlu == 'ممتاز'){
                            $row[$key] = 1;
                        }elseif ($vlu == 'جيد جدا'){
                            $row[$key] = 2;
                        }elseif ($vlu == 'جيد'){
                            $row[$key] = 3;
                        }elseif ($vlu == 'مقبول'){
                            $row[$key] = 4;
                        }elseif ($vlu == 'ضعيف'){
                            $row[$key] = 5;
                        }
                    }
                }elseif($key =='visitor_evaluation'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){

                        if($vlu == 'ممتاز'){
                            $row[$key] = 4;
                        }elseif ($vlu == 'جيد جدا'){
                            $row[$key] = 3;
                        }elseif ($vlu == 'جيد'){
                            $row[$key] = 2;
                        }elseif ($vlu == 'سيء'){
                            $row[$key] = 1;
                        }elseif ($vlu == 'سيء جدا'){
                            $row[$key] = 0;
                        }
                    }
                }
                elseif($key =='grade'){
                    $vlu = trim($v);
                    if(!is_null($vlu) && $vlu != " "){
                        if($vlu == 'اول ابتدائي'){ $row[$key] = 1;}
                        elseif($vlu == 'ثاني ابتدائي'){$row[$key] = 2;}
                        elseif ($vlu == 'ثالث ابتدئي'){$row[$key] = 3;}
                        elseif ($vlu == 'رابع ابتدئي'){$row[$key] = 4;}
                        elseif ($vlu == 'خامس ابتدئي'){$row[$key] = 5;}
                        elseif ($vlu == 'سادس ابتدئي'){$row[$key] = 6;}
                        elseif ($vlu == 'السابع'){$row[$key] = 7;}
                        elseif ($vlu == 'الثامن'){$row[$key] = 8;}
                        elseif ($vlu == 'التاسع'){$row[$key] = 9;}
                        elseif ($vlu == 'العاشر'){$row[$key] = 10;}
                        elseif ($vlu == 'الحادي عشر'){$row[$key] = 11;}
                        elseif ($vlu == 'الثاني عشر'){$row[$key] = 12;}
                        elseif ($vlu == 'دراسات خاصة'){$row[$key] = 13;}
                        elseif ($vlu == 'تدريب مهنى'){$row[$key] = 14;}
                        elseif ($vlu == 'سنة أولى جامعة'){$row[$key] = 15;}
                        elseif ($vlu == 'سنة ثانية جامعة'){$row[$key] = 16;}
                        elseif ($vlu == 'سنة ثالثة جامعة'){$row[$key] = 17;}
                        elseif ($vlu == 'سنة رابعة جامعة'){$row[$key] = 18;}
                        elseif ($vlu == 'سنة خامسة جامعة'){$row[$key] = 19;}
                    }
                }
                elseif($key =='condition'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){

                        if ($vlu == 'مرض مزمن'){
                            $row[$key] = 2;
                        }elseif ($vlu == 'ذوي احتياجات خاصة'){
                            $row[$key] = 3;
                        }else{
                            $row[$key] = 1;
                        }
                    }
                }
                elseif($key =='refugee'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'مواطن') ? 1 : 2;
                    }
                }
                elseif($key =='working'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 2;
                    }
                }
                elseif($key =='mother_is_guardian'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 2;
                    }
                }
                elseif($key =='study'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 0;
                    }
                }elseif($key =='currently_study'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 0;
                    }
                }elseif($key =='receivables'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 0;
                    }
                }elseif($key =='is_qualified'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 0;
                    }
                }
                elseif($key =='can_work'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 2 : 1;
                    }
                }
                elseif($key =='promised'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 2 : 1;
                    }
                }
                elseif(in_array($key,$constant)){
                    $parent =null;
                    $isChild =false;
                    if(in_array($key,$hasParent)){
                        $isChild =true;
                        $parentKey=$Parents[$key];
                        $parentName=$ParentsNames[$key];
                        if(isset($row[$parentKey])){
                            $parent=$row[$parentKey];
                        }
                    }
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != '') {
                        $cons_val = CaseModel::constantId($key,$vlu,$isChild,$parent);
                        if(!is_null($cons_val)) {
                            $row[$key] = $cons_val;
                        }else{
                            $row['errors'][$key]=trans('common::application.invalid_constant');
                        }
                    }
                }
                elseif(in_array($key,$numeric)){
                    if(!is_null($v) && $v != " "){
                        if(is_numeric($v)){
                            $row[$key]=(int)$v;
                        }
                    }
                }else{
                    if(!is_null($v) && $v != " "){
                        $row[$key]=$v;
                    }
                }
            }

        }

        return $row;
    }

    public static function saveMember($input = array())
    {

        $case_Input = ['search_cases_id', 'id_card_number','card_type', 'first_name','second_name', 'third_name',
                        'last_name', 'full_name','prev_family_name', 'gender','marital_status_id',
                        'birthday','kinship_id', 'primary_mobile','watanya', 'condition','disease_id',
                        'details','grade', 'currently_study','school'];

        $IntegerInput =  ['search_cases_id', 'id_card_number','card_type', 'gender',
                          'marital_status_id','kinship_id', 'primary_mobile','watanya',
                          'condition','disease_id', 'grade', 'currently_study'
                         ];

        $insert=[];
        foreach ($case_Input as $var) {
            if(isset($input[$var])){
                if($input[$var] != null && $input[$var] != "") {
                    if(in_array($var, $IntegerInput)){
                        $insert[$var] =(int )$input[$var];
                    }else{
                        if ($var == 'birthday' || $var == 'death_date') {
                            $insert[$var] = date('Y-m-d', strtotime($input[$var]));
                        }else{
                            $insert[$var] =$input[$var];
                        }
                    }
                }else{
                    $insert[$var] = null;
                }
            }
        }

        $case=null;

        \DB::beginTransaction();
        if(sizeof($insert) > 0){
            $case = new CasesFamily();
            foreach ($insert as $key => $value) {
                $case->$key = $value;
            }
            $case->save();
        }
        \DB::commit();

        return $case;

    }


}
