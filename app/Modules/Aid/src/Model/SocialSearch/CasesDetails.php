<?php
namespace Aid\Model\SocialSearch;

class CasesDetails  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_social_search_cases_details';
    protected $primaryKey='search_cases_id ';
    protected $fillable = ["search_cases_id","refugee","unrwa_card_number",
                            "is_qualified", "receivables","receivables_sk_amount",
                            "qualified_card_number","monthly_income","actual_monthly_income",
                            "needs"  ,"promised", "reconstructions_organization_name","visitor","visitor_card", "visited_at",
                            "visitor_evaluation", "notes","visitor_opinion"];

    public $timestamps = false;

    public static function saveDetails ($search_cases_id,$inputs){

        $inputs['receivables'] = is_null($inputs['receivables'])? 0 : $inputs['receivables'];
        $inputs['promised'] = is_null($inputs['promised'])? 1 : $inputs['promised'];
        if(CasesDetails::where(["search_cases_id"=>$search_cases_id])->first()){
            CasesDetails::where("search_cases_id",$search_cases_id)->update($inputs);
        }else{
            $inputs['search_cases_id']=$search_cases_id;
            CasesDetails::create($inputs);
        }
        return true;
    }
}
