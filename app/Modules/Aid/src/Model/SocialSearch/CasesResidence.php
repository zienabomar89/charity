<?php
namespace Aid\Model\SocialSearch;

class CasesResidence  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_social_search_cases_residence';
    protected $primaryKey='search_cases_id ';
    protected $fillable = ['search_cases_id','property_type_id','rent_value','roof_material_id','residence_condition',
                           'indoor_condition','habitable','house_condition','rooms','area','need_repair','repair_notes'];
    public $timestamps = false;

    public static function saveResidence($search_cases_id,$inputs){

        if(CasesResidence::where(["search_cases_id"=>$search_cases_id])->first()){
            CasesResidence::where("search_cases_id",$search_cases_id)->update($inputs);
        }else{
            $inputs['search_cases_id']=$search_cases_id;
            CasesResidence::create($inputs);
        }
        return true;
    }
}
