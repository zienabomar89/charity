<?php

namespace Aid\Model\SocialSearch;

use App\Http\Helpers;
class CasesProperties  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_social_search_cases_properties';
    protected $fillable = ['search_cases_id','property_id','has_property','quantity'];
    public $timestamps = false;

    public static function getList($id)
    {

        $language_id = 1;

        $not_exist = trans('common::application.not_exist');
        $exist = trans('common::application.exist');

        return \DB::table('char_properties_i18n')
            ->leftjoin('char_social_search_cases_properties', function($join) use($id){
                $join->on('char_properties_i18n.property_id', '=', 'char_social_search_cases_properties.property_id')
                    ->where('char_social_search_cases_properties.search_cases_id', '=', $id);
            })
            ->where('char_properties_i18n.language_id', $language_id)
            ->orderBy('char_properties_i18n.name')
            ->selectRaw("char_properties_i18n.* ,
                         CASE WHEN char_social_search_cases_properties.has_property is null THEN '$not_exist'
                              WHEN char_social_search_cases_properties.has_property = 1 THEN '$exist'
                              Else '$not_exist' END
                         AS  has_property,
                                                CASE WHEN char_social_search_cases_properties.has_property is null THEN '0'
                                                     WHEN char_social_search_cases_properties.has_property = 0 THEN 0
                                                     WHEN char_social_search_cases_properties.quantity is null THEN 0
                                                     Else char_social_search_cases_properties.quantity END
                                                AS  quantity,
                                                CASE WHEN char_social_search_cases_properties.has_property is null THEN '*'
                                                     WHEN char_social_search_cases_properties.has_property = 1 THEN ' '
                                                     Else '*' END
                                                AS  not_exist ,
                                                CASE WHEN char_social_search_cases_properties.has_property is not null THEN '*'
                                                     WHEN char_social_search_cases_properties.has_property = 1 THEN '*'
                                                     Else ' ' END
                                                AS  exist")
            ->get();

    }

}