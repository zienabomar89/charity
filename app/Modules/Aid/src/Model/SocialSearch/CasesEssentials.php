<?php
namespace Aid\Model\SocialSearch;
use App\Http\Helpers;


class CasesEssentials  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_social_search_cases_essentials';
    protected $primaryKey='search_cases_id ';
    protected $fillable = [ 'search_cases_id','essential_id','condition','needs'];
    public $timestamps = false;


    public static function getList($id)
    {

        $not_exist = trans('common::application.not_exist');
        $exist = trans('common::application.exist');

        $excellent = trans('common::application.excellent');
        $good = trans('common::application.good');
        $very_good = trans('common::application.very_good');
        $very_bad_condition = trans('common::application.very_bad_condition');
        $bad_condition = trans('common::application.bad_condition');
        
        return \DB::table('char_essentials')
            ->leftjoin('char_social_search_cases_essentials', function($q) use($id){
                $q->on('char_social_search_cases_essentials.essential_id', '=', 'char_essentials.id');
                $q->where('char_social_search_cases_essentials.search_cases_id', '=', $id);
            })
            ->selectRaw("char_essentials.id as essential_id,char_essentials.name,char_social_search_cases_essentials.needs")
            ->selectRaw("CASE WHEN char_social_search_cases_essentials.condition = 0 THEN '$not_exist'  Else '$exist' END AS  exist,
                                             CASE WHEN char_social_search_cases_essentials.condition = 0 THEN '$not_exist'
                                                  WHEN char_social_search_cases_essentials.condition = 1 THEN '$very_bad_condition'
                                                  WHEN char_social_search_cases_essentials.condition = 2 THEN '$bad_condition'
                                                  WHEN char_social_search_cases_essentials.condition = 3 THEN '$good'
                                                  WHEN char_social_search_cases_essentials.condition = 4 THEN '$very_good'
                                                  WHEN char_social_search_cases_essentials.condition = 5 THEN '$excellent'
                                             END AS essentials_condition,
                                             CASE WHEN char_social_search_cases_essentials.condition = 0 THEN '*' Else ' ' END AS not_exist,
                                             CASE WHEN char_social_search_cases_essentials.condition = 1 THEN '*' Else ' ' END AS very_bad_condition,
                                             CASE WHEN char_social_search_cases_essentials.condition = 2 THEN '*' Else ' ' END AS bad_condition,
                                             CASE WHEN char_social_search_cases_essentials.condition = 3 THEN '*' Else ' ' END AS good,
                                             CASE WHEN char_social_search_cases_essentials.condition = 4 THEN '*' Else ' ' END AS very_good,
                                             CASE WHEN char_social_search_cases_essentials.condition = 5 THEN '*' Else ' ' END AS excellent


                                             ")
            ->orderBy('char_essentials.name')
            ->get();
    }
    
}