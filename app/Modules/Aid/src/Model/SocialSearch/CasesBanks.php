<?php

namespace Aid\Model\SocialSearch;

class CasesBanks  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_social_search_cases_banks';
    protected $fillable = [ 'search_cases_id','bank_id','account_number','account_owner','branch_name'];
    public $timestamps = false;

    public static function getList($id){

         return self::query()
                ->join('char_banks','char_social_search_cases_banks.bank_id',  '=', 'char_banks.id')
                ->leftjoin('char_branches','char_social_search_cases_banks.branch_name',  '=', 'char_branches.id')
                ->where('char_social_search_cases_banks.search_cases_id','=',$id)
                ->selectRaw("char_social_search_cases_banks.id,
                                      char_social_search_cases_banks.bank_id,
                                      char_social_search_cases_banks.branch_name,
                                      CASE WHEN char_banks.name is null THEN '-' Else char_banks.name END AS bank_name,
                                      CASE WHEN char_branches.name is null THEN '-' Else char_branches.name END AS branch,
                                      CASE WHEN char_social_search_cases_banks.account_number is null THEN '-' Else char_social_search_cases_banks.account_number END AS account_number,
                                      CASE WHEN char_social_search_cases_banks.account_owner is null THEN '-' Else char_social_search_cases_banks.account_owner END AS account_owner
                           ")
                ->get();

    }

    public static function saveAccount($data){


        $case = Cases::where(function ($q) use ($data) {
                        $q->where('id_card_number', '=', $data['id_card_number']);
                        $q->where('search_id', '=', $data['search_id']);
                 })
                 ->first();

        if(is_null($case)){
            return ['status'=>false , 'reason' => 'case_not_enter_on_search'];
        }

        $entry = self::query()
            ->where(function ($q) use ($data,$case) {
                $q->where('bank_id', '=', $data['bank_id']);
                $q->where('account_number', '=', $data['account_number']);
            })
            ->first();

        if(!is_null($entry)){
            if($entry->search_cases_id == $case->id){
                return ['status'=>false , 'reason' => 'account_number_previously_add_to_you'];
            } else if($entry->search_cases_id != $case->id){
                return ['status'=>false , 'reason' => 'account_number_owned_by_other'];
            }
        }

        $insert = ['bank_id','account_number','account_owner','branch_name'];

        $account = new CasesBanks();
        $account->search_cases_id = $case->id;
        foreach ($insert as $key) {
            if(isset($data[$key])){
                $account->$key = $data[$key];
            }
        }
        $account->save();

        return ['status'=>true ];
    }

}
