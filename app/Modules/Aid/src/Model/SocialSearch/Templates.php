<?php
namespace Aid\Model\SocialSearch;

class Templates  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_social_search_templates';
    protected $fillable = ['organization_id','filename','category_id'];
    protected $hidden = ['created_at','updated_at'];

    public static function getTemplates($organization_id){
        $language_id =  \App\Http\Helpers::getLocale();
        return
            \DB::table('char_categories')
                ->where('char_categories.type','=',2)
                ->orderBy('char_categories.name', 'DESC')
                ->leftjoin('char_social_search_templates', function($q) use($organization_id){
                    $q->on('char_social_search_templates.category_id','=','char_categories.id');
                    $q->where('char_social_search_templates.organization_id','=',$organization_id);
                })
                ->selectRaw("CASE WHEN $language_id = 1 THEN char_categories.name 
                                  Else char_categories.en_name END  
                                  AS name,
                             char_social_search_templates.id as id , char_categories.id as category_id ,
                              char_social_search_templates.filename as template")



                ->get();
    }

}
