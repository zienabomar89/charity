<?php
namespace Aid\Model\SocialSearch;

use App\Http\Helpers;

class CasesAids  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_social_search_cases_aids';
    protected $fillable = [ 'search_cases_id','aid_source_id','aid_type','aid_value','aid_take','currency_id'];
    public $timestamps = false;


    public static function getList($id,$aid_type)
    {

        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        return \DB::table('char_aid_sources')
            ->leftjoin('char_social_search_cases_aids', function ($join) use ($id, $aid_type) {
                $join->on('char_social_search_cases_aids.aid_source_id', '=', 'char_aid_sources.id')
                    ->where('char_social_search_cases_aids.search_cases_id', '=', $id)
                    ->where('char_social_search_cases_aids.aid_type', '=', $aid_type);
            })
            ->leftjoin('char_currencies', 'char_social_search_cases_aids.currency_id', '=', 'char_currencies.id')
            ->selectRaw("char_social_search_cases_aids.*, char_aid_sources.name, char_aid_sources.id as aid_source_id,
                         CASE WHEN char_social_search_cases_aids.aid_value is null THEN '$no'
                              WHEN char_social_search_cases_aids.aid_take = 0 THEN '$no'  Else '$yes' END AS  aid_take,
                         CASE WHEN char_social_search_cases_aids.aid_value is null THEN ' '
                              WHEN char_social_search_cases_aids.aid_take = 0 THEN ' '
                              WHEN char_currencies.name is null THEN ' '
                              Else char_currencies.name END
                         AS  currency_name,
                         CASE WHEN char_social_search_cases_aids.aid_value is null THEN '0'
                              WHEN char_social_search_cases_aids.aid_take = 0 THEN '0'
                              WHEN char_social_search_cases_aids.aid_value is null THEN '0'
                              Else char_social_search_cases_aids.aid_value END
                         AS  aid_value,
                         CASE WHEN char_social_search_cases_aids.aid_value is null THEN '*'
                              WHEN char_social_search_cases_aids.aid_take = 0 THEN '*'  Else ' ' END
                         AS  not_take ,
                         CASE WHEN char_social_search_cases_aids.aid_value is not null THEN '*'
                              WHEN char_social_search_cases_aids.aid_take = 1 THEN '*'  Else ' ' END
                         AS  take")
            ->orderBy('char_aid_sources.name')
            ->get();
    }
}