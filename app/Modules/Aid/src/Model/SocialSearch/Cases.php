<?php
namespace Aid\Model\SocialSearch;

use Common\Model\CaseModel;
use Common\Model\Person;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Setting\Model\Setting;
use Illuminate\Database\Eloquent\Model;
use App\Http\Helpers;

class Cases  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'char_social_search_cases';
    protected $hidden = ['created_at','updated_at'];
    protected $fillable = ["search_id","id_card_number","card_type", "first_name" ,
        "second_name","third_name","last_name","prev_family_name","gender",
        "marital_status_id", "deserted", "birthday","birth_place","death_date"   ,
        "death_cause_id","nationality" ,"family_cnt","spouses","male_live" ,
        "female_live","phone","primary_mobile" , "secondary_mobile","watanya",
        "country_id","district_id","region_id","neighborhood_id","square_id","mosque_id" ,
        "street_address", "status","reason"];

        public static $_translator = [
        "almntk"=>'region_id',
        "alhy"=>'neighborhood_id',
        "almrbaa"=>'square_id',
        "almsjd"=>'mosque_id',
        "noaa_albtak"=>'card_type',
        "rkm_alhoy" =>'id_card_number',
        "alasm_alaol_aarby"=>'first_name',
        "alasm_althany_aarby"=>'second_name',
        "alasm_althalth_aarby"=>'third_name',
        "alaaael_aarby"=>'last_name',
        "asm_alaaael_alsabk"=>'prev_family_name',
        "tarykh_almylad"=>'birthday',
        "mkan_almylad"=>'birth_place',
        "aljns"=>'gender',
        "aljnsy"=>'nationality',
        "aadd_afrad_alasr"=>'family_cnt',
        "aadd_alabna_ghyr_almtzojyn"=>'male_live',
        "aadd_albnat_alghyr_mtzojat"=>'female_live',
        "aadd_alzojat"=>'spouses',
        "alhal_alajtmaaay"=>'marital_status_id',
        "hal_almoatn"=>'refugee',
        "rkm_krt_altmoyn"=>'unrwa_card_number',
        "aldol"=>'country_id',
        "almhafth"=>'district_id',
        "tfasyl_alaanoan"=>'street_address',
        "rkm_joal_asasy"=>'primary_mobile',
        "rkm_alotny"=>'watanya',
        "rkm_joal_ahtyaty"=>'secondary_mobile',
        "rkm_hatf"=>'phone',
        "asm_albnk"=>'bank_id',
        "alfraa"=>'branch_name',
        "asm_sahb_alhsab"=>'account_owner',
        "rkm_alhsab"=>'account_number',
        "hl_yaaml"=>'working',
        "hl_taaml"=>'working',
        "kadr_aal_alaaml"=>'can_work',
        "sbb_aadm_alkdr"=>'work_reason_id',
        "hal_alaaml"=>'work_status_id',
        "fe_alajr"=>'work_wage_id',
        "alothyf"=>'work_job_id',
        "mkan_alaaml"=>'work_location',
        "aldkhl_alshhry_shykl"=>'monthly_income',

        "aadd_alghrf"=>'rooms',
        "almsah_balmtr"=>'area',
        "kym_alayjar"=>'rent_value',
        "noaa_almlky"=>'property_type_id',
        "mlky_almnzl"=>'property_type_id',
        "noaa_skf_almnzl"=>'roof_material_id',
        "skf_almnzl"=>'roof_material_id',

        "hal_almskn"=>'residence_condition',
        "hal_alathath"=>'indoor_condition',
        "kably_almnzl_llskn"=>'habitable',

        "odaa_almnzl"=>'house_condition',

        "hl_oaadt_balbna_mn_kbl_okal_alghoth_ao_jh_akhr"=>'promised',
        "asm_almoess_alty_oaadt_balbna"=>'organization_name',
        "ahtyajat_alaaael_bshd"=>'needs',
        "tosyat_albahth_alajtmaaay"=>'notes',
        "ydrs_ao_mtaalm"=>"study" ,
        "noaa_aldras"=>"type" ,
        "jh_aldras"=>"authority" ,
        "almoehl_alaalmy"=>"stage" ,
        "almrhl_aldrasy"=>"degree" ,
        "altkhss"=>"specialization" ,
        "alsf"=>"grade" ,
        "alsn_aldarsy"=>"year" ,
        "asm_almdrs"=>"school" ,
        "akhr_ntyj" =>"points" ,
        "almsto_althsyl"=>"level",
        "alhal_alshy"=>"condition",
        "almrd_ao_aleaaak"=>"disease_id",
        "tfasyl_alhal_alshy"=>"details",
        "tarykh_alzyar" => "visited_at",
        "asm_albahth_alajtmaaay" => "visitor",
        "hl_ysly"=>"prayer",
        "sbb_trk_alsla"=>"prayer_reason",
        "hl_yhfth_alkran"=>"save_quran",
        "aadd_alajza"=>"quran_parts",
        "aadd_alsor"=>"quran_chapters",
        "sbb_trk_althfyth"=>"quran_reason",
        "mlthk_balmsjd"=>"quran_center",
        "tarykh_ofa_alab"=>"quran_center",
        "tarykh_alofa"=>"death_date",
        "sbb_alofa"=>"death_cause_id",
        "sl_alkrab"=>"kinship_id",
        "hl_alam_maayl"=>"mother_is_guardian",
        "hl_ldyh_msadr_dkhl_akhr" => "has_other_work_resources",
        "aldkhl_alshhry_alfaaly_shykl" => "actual_monthly_income",
        "hl_bhaj_ltrmym_llmoaem" => "need_repair",
        "osf_odaa_almnzl" => "repair_notes",
        "hl_ldyh_tamyn_shy" => "has_health_insurance",
        "noaa_altamyn_alshy" => "health_insurance_type",
        "rkm_altamyn_alshy" => "health_insurance_number",
        "hl_ystkhdm_jhaz" => "has_device",
        "asm_aljhaz_almstkhdm" => "used_device_name",
        "almrd_ao_alaaaak" => "disease_id",
        "rkm_hoy_albahth" => "visitor_card",
        "ray_albahth_alajtmaaay" => "visitor_opinion",
        "tkyym_albahth_lhal_almnzl"=> "visitor_evaluation",
        "hl_ldyh_thmm_maly"=> "receivables",
        "kym_althmm_balshykl"=> "receivables_sk_amount",
    ];

    public static function filter($filters)
    {

        $user = \Auth::user();
        $organization_id = $user->organization_id;

        $language_id =  \App\Http\Helpers::getLocale();

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $condition =array();
        $output=array();
        $all_organization=null;

        $char_social_search = ['category_id',"organization_id" ,"user_id"];
        $char_social_search_cases = ["search_id","id_card_number","card_type", "first_name" ,
            "second_name","third_name","last_name","prev_family_name","gender",
            "marital_status_id", "deserted", "birthday","birth_place","death_date"   ,
            "death_cause_id","nationality" ,"family_cnt","spouses","male_live" ,
            "female_live","phone","primary_mobile" , "secondary_mobile","watanya",
            "country_id","district_id","region_id","neighborhood_id","square_id","mosque_id" ,
            "street_address"];

        $char_social_search_cases_details = ["refugee","unrwa_card_number",
            "is_qualified", "receivables","receivables_sk_amount",
            "qualified_card_number","monthly_income","actual_monthly_income",
            "needs"  ,"promised", "reconstructions_organization_name","visitor","visitor_card", "visited_at",
            "visitor_evaluation", "notes","visitor_opinion", "status","reason"];

        $char_social_search_cases_residence = ['property_type_id', 'roof_material_id','residence_condition','indoor_condition',
            'house_condition','habitable','rent_value','need_repair','repair_notes'];

        $char_social_search_cases_work = ['working','can_work','work_status_id','work_reason_id','work_job_id','work_wage_id','work_location'];

        $char_social_search_cases_health = ['condition','disease_id','has_health_insurance', 'health_insurance_number', 'health_insurance_type',
            'has_device', 'used_device_name'];

        $char_social_search_cases_banks = ['bank_id','branch_name','account_number','account_owner'];

        $char_social_search_cases_residence_cnt =
        $char_social_search_cases_work_cnt  =
        $char_social_search_cases_health_cnt =
        $char_social_search_cases_banks_cnt = 0;

        foreach ($filters as $key => $value) {

            if(in_array($key, $char_social_search)) {
                if ( $value != "" ) {
                    $data = ['char_social_search.' . $key => $value];
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $char_social_search_cases_details)) {
                if ( $value != "" ) {
                    $data = ['char_social_search_cases_details.' . $key => $value];
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $char_social_search_cases)) {
                if ( $value != "" ) {
                    $data = ['char_social_search_cases.' . $key => $value];
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $char_social_search_cases_residence)) {
                if ( $value != "" ) {
                    $data = ['char_social_search_cases_residence.' . $key =>  $value];
                    $char_social_search_cases_residence_cnt++;
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $char_social_search_cases_work)) {

                if ( $value != "" ) {
                    $data = ['char_social_search_cases_work.' . $key => $value];
                    $char_social_search_cases_work_cnt ++;
                    array_push($condition, $data);
                }

            }

            if(in_array($key, $char_social_search_cases_health)) {
                if ( $value != "" ) {
                    $data = ['char_social_search_cases_health.' . $key =>  $value];
                    $char_social_search_cases_health_cnt++;
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $char_social_search_cases_banks)) {
                if ( $value != "" ) {
                    $data = ['char_social_search_cases_banks.'. $key => $value];
                    $char_social_search_cases_banks_cnt++;
                    array_push($condition, $data);
                }
            }

        }

        $result = \DB::table('char_social_search_cases')
            ->join('char_social_search', function($q) use ($language_id){
                $q->on('char_social_search.id', '=', 'char_social_search_cases.search_id');
                $q->whereNull('char_social_search.deleted_at');
            })
            ->join('char_organizations as org','org.id',  '=', 'char_social_search.organization_id')
            ->join('char_categories','char_categories.id',  '=', 'char_social_search.category_id')
            ->leftjoin('char_marital_status_i18n as char_marital_status', function($q) use ($language_id) {
                $q->on('char_marital_status.marital_status_id', '=', 'char_social_search_cases.marital_status_id');
                $q->where('char_marital_status.language_id',$language_id);
            })
            ->leftjoin('char_aids_locations_i18n As district_name', function($join) use ($language_id)  {
                $join->on('char_social_search_cases.district_id', '=','district_name.location_id' )
                    ->where('district_name.language_id',$language_id);
            })
            ->leftjoin('char_aids_locations_i18n As region_name', function($join)  use ($language_id) {
                $join->on('char_social_search_cases.region_id', '=','region_name.location_id' )
                    ->where('region_name.language_id',$language_id);
            });

        if($filters['action'] =='ExportToExcel' || $filters['action'] =='csv') {

            $result->leftjoin('char_social_search_cases_details', 'char_social_search_cases_details.search_cases_id', '=', 'char_social_search_cases.id');

            $result ->leftjoin('char_aids_locations_i18n As country_name', function($join)  use ($language_id) {
                $join->on('char_social_search_cases.country_id', '=','country_name.location_id' )
                    ->where('country_name.language_id',$language_id);
            })
                ->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)   {
                    $join->on('char_social_search_cases.neighborhood_id', '=','location_name.location_id' )
                        ->where('location_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As square_name', function($join)  use ($language_id) {
                    $join->on('char_social_search_cases.square_id', '=','square_name.location_id' )
                        ->where('square_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
                    $join->on('char_social_search_cases.mosque_id', '=','mosques_name.location_id' )
                        ->where('mosques_name.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L', function($q) use ($language_id){
                    $q->on('L.location_id', '=', 'char_social_search_cases.birth_place');
                    $q->where('L.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L0', function($q) use ($language_id){
                    $q->on('L0.location_id', '=', 'char_social_search_cases.nationality');
                    $q->where('L0.language_id',$language_id);
                })
                ->leftjoin('char_social_search_cases_health','char_social_search_cases_health.search_cases_id', '=', 'char_social_search_cases.id')
                ->leftjoin('char_diseases_i18n', function($q) use ($language_id){
                    $q->on('char_diseases_i18n.disease_id', '=', 'char_social_search_cases_health.disease_id');
                    $q->where('char_diseases_i18n.language_id',$language_id);
                })
                ->leftjoin('char_social_search_cases_residence', 'char_social_search_cases_residence.search_cases_id', '=', 'char_social_search_cases.id')
                ->leftjoin('char_property_types_i18n', function($q) use ($language_id){
                    $q->on('char_property_types_i18n.property_type_id', '=', 'char_social_search_cases_residence.property_type_id');
                    $q->where('char_property_types_i18n.language_id',$language_id);
                })
                ->leftjoin('char_roof_materials_i18n', function($q) use ($language_id){
                    $q->on('char_roof_materials_i18n.roof_material_id', '=', 'char_social_search_cases_residence.roof_material_id');
                    $q->where('char_roof_materials_i18n.language_id',$language_id);
                })
                ->leftjoin('char_building_status_i18n', function($q) use ($language_id){
                    $q->on('char_building_status_i18n.building_status_id', '=', 'char_social_search_cases_residence.house_condition');
                    $q->where('char_building_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_furniture_status_i18n', function($q) use ($language_id){
                    $q->on('char_furniture_status_i18n.furniture_status_id', '=', 'char_social_search_cases_residence.indoor_condition');
                    $q->where('char_furniture_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_habitable_status_i18n', function($q) use ($language_id){
                    $q->on('char_habitable_status_i18n.habitable_status_id', '=', 'char_social_search_cases_residence.habitable');
                    $q->where('char_habitable_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_house_status_i18n', function($q) use ($language_id){
                    $q->on('char_house_status_i18n.house_status_id', '=', 'char_social_search_cases_residence.residence_condition');
                    $q->where('char_house_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_social_search_cases_work', 'char_social_search_cases_work.search_cases_id', '=', 'char_social_search_cases.id')
                ->leftjoin('char_work_jobs_i18n as work_job', function($q) use ($language_id){
                    $q->on('work_job.work_job_id', '=', 'char_social_search_cases_work.work_job_id');
                    $q->where('work_job.language_id',$language_id);
                })
                ->leftjoin('char_work_wages as work_wage', function($q) use ($language_id){
                    $q->on('work_wage.id', '=', 'char_social_search_cases_work.work_wage_id');
                })
                ->leftjoin('char_work_status_i18n as work_status', function($q) use ($language_id){
                    $q->on('work_status.work_status_id', '=', 'char_social_search_cases_work.work_status_id');
                    $q->where('work_status.language_id',$language_id);
                })
                ->leftjoin('char_work_reasons_i18n as work_reason', function($q) use ($language_id){
                    $q->on('work_reason.work_reason_id', '=', 'char_social_search_cases_work.work_reason_id');
                    $q->where('work_reason.language_id',$language_id);
                });


        }
        else{

            if($char_social_search_cases_details != 0) {
                $result->leftjoin('char_social_search_cases_details', 'char_social_search_cases_details.search_cases_id', '=', 'char_social_search_cases.id');
            }

            if($char_social_search_cases_residence_cnt != 0) {
                $result->leftjoin('char_social_search_cases_residence', 'char_social_search_cases_residence.search_cases_id', '=', 'char_social_search_cases.id');
            }
            if($char_social_search_cases_work_cnt != 0){
                $result ->leftjoin('char_social_search_cases_work','char_social_search_cases_work.search_cases_id', '=', 'char_social_search_cases.id');
            }
            if($char_social_search_cases_health_cnt!= 0 ){
                $result->leftjoin('char_social_search_cases_health','char_social_search_cases_health.search_cases_id','=','char_social_search_cases.id');
            }

        }

        if (count($condition) != 0) {
            $result =  $result
                ->where(function ($q) use ($condition) {
                    $names = ['char_social_search_cases.street_address','char_social_search_cases.first_name', 'char_social_search_cases.second_name',
                        'char_social_search_cases.third_name', 'char_social_search_cases.last_name',
                        'char_social_search_cases_i18n.first_name', 'char_social_search_cases_i18n.second_name', 'char_social_search_cases_i18n.third_name',
                        'char_social_search_cases_i18n.last_name','char_social_search_cases_residence.repair_notes',
                        'char_social_search_cases_details.visitor','char_social_search_cases_details.notes','char_social_search_cases_details.visitor_card',
                        'char_social_search_cases_details.visitor_opinion',
                        'char_social_search_cases_work.work_location','char_social_search_cases_banks.account_owner',
                        'char_social_search_cases_health.health_insurance_number','char_social_search_cases_health.used_device_name',
                        'char_social_search_cases_residence.reconstructions_organization_name',
                        'char_social_search_cases_residence.repair_notes',
                        'char_social_search_cases_work.work_location','char_social_search_cases_health.used_device_name'];


                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $names)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });

        }

        $active = trans('common::application.active');
        $inactive = trans('common::application.inactive');

        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $refugee = trans('common::application.refugee');
        $citizen = trans('common::application.citizen');

        $qualified = trans('common::application.qualified');
        $not_qualified = trans('common::application.not qualified');

        $perfect = trans('common::application.perfect');
        $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
        $special_needs = trans('common::application.special_needs');

        if(isset($filters['all_organization'])){
            if($filters['all_organization'] ===true || $filters['all_organization'] ==='true'){
                $all_organization=0;
            }elseif($filters['all_organization'] ===false || $filters['all_organization'] ==='false'){
                $all_organization=1;
            }
        }

        if($all_organization ==0){
            $organizations=[];
            if(isset($filters['organization_ids']) && $filters['organization_ids'] !=null &&
                $filters['organization_ids'] !="" && $filters['organization_ids'] !=[]) {

                if(is_array($filters['organization_ids'])){
                    if(sizeof($filters['organization_ids']) != 0){
                        if($filters['organization_ids'][0]==""){
                            unset($filters['organization_ids'][0]);
                        }
                        $organizations =$filters['organization_ids'];
                    }
                }
            }

            if(!empty($organizations)){
                $result->wherein('char_social_search.organization_id',$organizations);
            }else{

                $user = \Auth::user();
                $UserType=$user->type;

                if($UserType == 2) {
                    $result->where(function ($anq) use ($organization_id,$user) {
                        $anq->where('char_social_search.organization_id',$organization_id);
                        $anq->orwherein('char_social_search.organization_id', function($quer) use($user) {
                            $quer->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });
                }else{

                    $result->where(function ($anq) use ($organization_id) {
                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                        $anq->wherein('char_social_search.organization_id', function($quer) use($organization_id) {
                            $quer->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $organization_id);
                        });
                    });

                }
            }

        }
        elseif($all_organization ==1){
            if(isset($filters['organizationsCases']) && !empty($filters['organizationsCases']) && $filters['organizationsCases'])
                $result= $result->where('char_social_search.organization_id','!=',$organization_id);
            else
                $result= $result->where('char_social_search.organization_id',$organization_id);
        }

        if((isset($filters['birthday_to']) && $filters['birthday_to'] !=null) || (isset($filters['birthday_from']) && $filters['birthday_from'] !=null)){

            $birthday_to=null;
            $birthday_from=null;

            if(isset($filters['birthday_to']) && $filters['birthday_to'] !=null){
                $birthday_to=date('Y-m-d',strtotime($filters['birthday_to']));
            }
            if(isset($filters['birthday_from']) && $filters['birthday_from'] !=null){
                $birthday_from=date('Y-m-d',strtotime($filters['birthday_from']));
            }
            if($birthday_from != null && $birthday_to != null) {
                $result->whereBetween( 'char_social_search_cases.birthday', [ $birthday_from, $birthday_to]);
            }elseif($birthday_from != null && $birthday_to == null) {
                $result->whereDate('char_social_search_cases.birthday', '>=', $birthday_from);
            }elseif($birthday_from == null && $birthday_to != null) {
                $result->whereDate('char_social_search_cases.birthday', '<=', $birthday_to);
            }

        }

        if((isset($filters['date_to']) && $filters['date_to'] !=null) || (isset($filters['date_from']) && $filters['date_from'] !=null)){

            $date_to=null;
            $date_from=null;

            if(isset($filters['date_to']) && $filters['date_to'] !=null){
                $date_to=date('Y-m-d',strtotime($filters['date_to']));
            }
            if(isset($filters['date_from']) && $filters['date_from'] !=null){
                $date_from=date('Y-m-d',strtotime($filters['date_from']));
            }
            if($date_from != null && $date_to != null) {
                $result->whereBetween( 'char_social_search_cases.created_at', [ $date_from, $date_to]);
            }elseif($date_from != null && $date_to == null) {
                $result->whereDate('char_social_search_cases.created_at', '>=', $date_from);
            }elseif($date_from == null && $date_to != null) {
                $result->whereDate('char_social_search_cases.created_at', '<=', $date_to);
            }


        }

        if((isset($filters['visited_at_to']) && $filters['visited_at_to'] !=null) || (isset($filters['visited_at_from']) && $filters['visited_at_from'] !=null)){
            $visited_at_to=null;
            $visited_at_from=null;

            if(isset($filters['visited_at_to']) && $filters['visited_at_to'] !=null){
                $visited_at_to=date('Y-m-d',strtotime($filters['visited_at_to']));
            }
            if(isset($filters['visited_at_from']) && $filters['visited_at_from'] !=null){
                $visited_at_from=date('Y-m-d',strtotime($filters['visited_at_from']));
            }

            if($visited_at_from != null && $visited_at_to != null) {
                $result->whereBetween( 'char_social_search_cases_details.visited_at', [ $visited_at_from, $visited_at_to]);
            }elseif($visited_at_from != null && $visited_at_to == null) {
                $result->whereDate('char_social_search_cases_details.visited_at', '>=', $visited_at_from);
            }elseif($visited_at_from == null && $visited_at_to != null) {
                $result->whereDate('char_social_search_cases_details.visited_at', '<=', $visited_at_to);
            }

        }

        if((isset($filters['monthly_income_to']) && $filters['monthly_income_to'] !=null && $filters['monthly_income_to'] !="")||
            (isset($filters['monthly_income_from']) && $filters['monthly_income_from'] !=null && $filters['monthly_income_from'] !="")){

            $monthly_income_from=null;
            $monthly_income_to=null;
            if(isset($filters['monthly_income_to']) && $filters['monthly_income_to'] !=null && $filters['monthly_income_to'] !=""){
                $monthly_income_to=$filters['monthly_income_to'];
            }
            if(isset($filters['monthly_income_from']) && $filters['monthly_income_from'] !=null && $filters['monthly_income_from'] !=""){
                $monthly_income_from=$filters['monthly_income_from'];
            }

            if($monthly_income_from != null && $monthly_income_to != null) {
                $result->whereRaw(" $monthly_income_to >= char_social_search_cases_details.monthly_income");
                $result->whereRaw(" $monthly_income_from <= char_social_search_cases_details.monthly_income");
            }
            elseif($monthly_income_to != null && $monthly_income_from == null) {
                $result->whereRaw(" $monthly_income_to >= char_social_search_cases_details.monthly_income");
            }
            elseif($monthly_income_to == null && $monthly_income_from != null) {
                $result->whereRaw(" $monthly_income_from <= char_social_search_cases_details.monthly_income");
            }
        }

        if((isset($filters['max_family_count']) && $filters['max_family_count'] !=null && $filters['max_family_count'] !="")||
            (isset($filters['min_family_count']) && $filters['min_family_count'] !=null && $filters['min_family_count'] !="")){

            $max_family_count=null;
            $min_family_count=null;

            if(isset($filters['max_family_count']) && $filters['max_family_count'] !=null && $filters['max_family_count'] !=""){
                $max_family_count=$filters['max_family_count'];
            }
            if(isset($filters['min_family_count']) && $filters['min_family_count'] !=null && $filters['min_family_count'] !=""){
                $min_family_count=$filters['min_family_count'];
            }
            if($min_family_count != null && $max_family_count != null) {
                $result = $result->whereRaw(" ( char_social_search_cases.family_cnt between ? and  ?)", array($min_family_count, $max_family_count));
            }elseif($max_family_count != null && $min_family_count == null) {
                $result = $result->whereRaw(" $max_family_count >= char_social_search_cases.family_cnt");
            }elseif($max_family_count == null && $min_family_count != null) {
                $result = $result->whereRaw(" $min_family_count <= char_social_search_cases.family_cnt");
            }
        }

        if(isset($filters['Properties'])) {
            for ($x = 0; $x < sizeof($filters['Properties']); $x++) {
                $item=$filters['Properties'][$x];
                if(isset($item['exist'])){
                    if($item['exist'] != "") {
                        $has_property=(int)$item['exist'];
                        $id=(int)$item['id'];
                        $table='char_social_search_cases_properties as prop'.$x;
                        $result=$result ->join($table, function($q) use($x,$id,$has_property){
                            $q->on('prop'.$x.'.search_cases_id', '=', 'char_social_search_cases.id');
                            $q->where('prop'.$x.'.property_id', '=', $id);
                            $q->where('prop'.$x.'.has_property', '=', $has_property);
                        });
                    }
                }

            }
        }
        if(isset($filters['AidSources'])) {
            for ($x = 0; $x < sizeof($filters['AidSources']); $x++) {
                $item=$filters['AidSources'][$x];
                if(isset($item['aid_take'])){
                    if($item['aid_take'] != "") {
                        $aid_take=(int)$item['aid_take'];
                        $id=(int)$item['id'];
                        $table='char_social_search_cases_aids as aid'.$x;
                        $result=$result ->join($table, function($q) use($x,$id,$aid_take){
                            $q->on('aid'.$x.'.search_cases_id', '=', 'char_social_search_cases.id');
                            $q->where('aid'.$x.'.aid_source_id', '=', $id);
                            $q->where('aid'.$x.'.aid_take', '=', $aid_take);
                        });
                    }
                }

            }
        }
        if(isset($filters['Essential'])) {
            $custom=[];
            foreach($filters['Essential'] as $item){
                if((isset($item['max']) && $item['max'] !=null && $item['max'] !="" ) ||
                    isset($item['min']) && $item['min'] !=null && $item['min'] !="" ){
                    array_push($custom,$item);
                }
            }

            foreach($custom as $k => $value) {
                $max=null;
                $min=null;
                $id=(int)$value['id'];
                $table='char_social_search_cases_essentials as essent'.$k;
                $need='essent'.$k.'.needs';

                if(isset($value['max']) && $value['max'] !=null && $value['max'] !=""){
                    $max=(int)$value['max'];
                }
                if(isset($value['min']) && $value['min'] !=null && $value['min'] !=""){
                    $min=(int)$value['min'];
                }

                $result=$result ->join($table, function($q) use($k,$id,$min,$max) {
                    $q->on('essent' . $k . '.search_cases_id', '=', 'char_social_search_cases.id');
                    $q->where('essent' . $k . '.essential_id', '=', $id);

                    if($min != null && $max != null) {
                        $q->where('essent'.$k.'.needs', '>=', $min);
                        $q->where('essent'.$k.'.needs', '<=', $max);
                    }elseif($max == null && $min != null) {
                        $q->where('essent'.$k.'.needs', '>=', $min);
                    }elseif($max != null && $min == null) {
                        $q->where('essent'.$k.'.needs', '<=', $max);
                    }
                });
            }
        }

        $map = [
            "title" => 'char_social_search.title',
            "search_date_from" => 'char_social_search.search_date_from',
            "search_date_to" => 'char_social_search.search_date_to',
            "date" => 'char_social_search.date',
            "organization_name" => 'org.name',
            "name" => 'char_social_search_cases.first_name',
            "id_card_number" => 'char_social_search_cases.id_card_number',
            "gender" => 'char_social_search_cases.gender',
            "case_gender" => 'char_social_search_cases.gender',
            "marital_status" => 'char_marital_status.name',
            "category_name" => 'char_categories.name',
            "status" => 'char_social_search_cases.status',
            "cases_status" => 'char_social_search_cases.status',
            "visitor" => 'char_social_search_cases_details.visitor',
            "delete_reason" => 'char_social_search_cases.reason',
            "deleted_at" => 'char_social_search_cases.deleted_at',
            "primary_mobile" => 'char_social_search_cases.primary_mobile',
            "governarate" => 'district_name.name',
            "region_name" => 'region_name.name',

        ];


        $order = false;

        if (isset($filters['sortKeyArr_rev']) && $filters['sortKeyArr_rev'] != null && $filters['sortKeyArr_rev'] != "" &&
            $filters['sortKeyArr_rev'] != [] && sizeof($filters['sortKeyArr_rev']) != 0
        ) {

            $order = true;

            foreach ($filters['sortKeyArr_rev'] as $key_) {
                if($key_ == 'name' || $key_ == 'full_name'){
                    $result->orderBy('char_social_search_cases.first_name','desc');
                    $result->orderBy('char_social_search_cases.second_name','desc');
                    $result->orderBy('char_social_search_cases.third_name','desc');
                    $result->orderBy('char_social_search_cases.last_name','desc');
                }else{
                    $result->orderBy($map[$key_],'desc');
                }
            }
        }

        if (isset($filters['sortKeyArr_un_rev']) && $filters['sortKeyArr_un_rev'] != null && $filters['sortKeyArr_un_rev'] != "" &&
            $filters['sortKeyArr_un_rev'] != [] && sizeof($filters['sortKeyArr_un_rev']) != 0
        ) {

            $order = true;
            foreach ($filters['sortKeyArr_un_rev'] as $key_) {
                if($key_ == 'name' || $key_ == 'full_name'){
                    $result->orderBy('char_social_search_cases.first_name','asc');
                    $result->orderBy('char_social_search_cases.second_name','asc');
                    $result->orderBy('char_social_search_cases.third_name','asc');
                    $result->orderBy('char_social_search_cases.last_name','asc');
                }else{
                    $result->orderBy($map[$key_],'asc');
                }
            }
        }


        if(!$order){
            $result->orderBy('char_social_search_cases.first_name',
                'char_social_search_cases.second_name',
                'char_social_search_cases.third_name',
                'char_social_search_cases.last_name');
        }

        if($filters['action'] =='ExportToWord' || $filters['action'] =='pdf'){
            $result= $result->selectRaw("char_social_search_cases.id")->get();
            foreach($result as $item) {
                $output[] = self::fetch($item->id);
            }
        }
        else if($filters['action'] =='FamilyMember'){

            $result= $result->selectRaw("char_social_search_cases.id,
                              CONCAT(ifnull(char_social_search_cases.first_name, ' '), ' ' ,ifnull(char_social_search_cases.second_name, ' '),' ',ifnull(char_social_search_cases.third_name, ' '),' ', ifnull(char_social_search_cases.last_name,' ')) AS full_name,
                              CASE WHEN char_social_search_cases.id_card_number is null THEN ' ' Else char_social_search_cases.id_card_number  END  AS id_card_number,
                              CASE
                                           WHEN char_social_search_cases.card_type is null THEN ' '
                                           WHEN char_social_search_cases.card_type = 1 THEN '$id_card'
                                           WHEN char_social_search_cases.card_type = 2 THEN '$id_number'
                                           WHEN char_social_search_cases.card_type = 3 THEN '$passport'
                                     END
                                     AS card_type                            
                              ")->get();


            foreach($result as $item) {
                $members = CasesFamily::getList($item->id);
                if (sizeof($members) > 0) {
                    foreach ($members as $key => $member) {
                        $member->father_name = $item->full_name;
                        $member->father_id_card_number = $item->id_card_number;
                        array_push($output, $member);
                    }
                }
            }

            return $output;
        }
        else if($filters['action'] =='ExportToExcel') {

            $result->selectRaw("char_social_search_cases.id,
                                         char_social_search_cases.id as person_id,
                                         char_social_search.title as search_title,char_social_search.date_from,
                                         char_social_search.date_to,
                                         CONCAT(ifnull(char_social_search_cases.first_name, ' '), ' ' ,ifnull(char_social_search_cases.second_name, ' '),' ',ifnull(char_social_search_cases.third_name, ' '),' ', ifnull(char_social_search_cases.last_name,' ')) AS name,
                                         CASE WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS category_name,
                                         org.name as organization_name,
                                         CASE WHEN char_social_search_cases.id_card_number is null THEN ' ' Else char_social_search_cases.id_card_number  END  AS id_card_number,
                                         CASE
                                                   WHEN char_social_search_cases.card_type is null THEN ' '
                                                   WHEN char_social_search_cases.card_type = 1 THEN '$id_card'
                                                   WHEN char_social_search_cases.card_type = 2 THEN '$id_number'
                                                   WHEN char_social_search_cases.card_type = 3 THEN '$passport'
                                             END
                                             AS card_type ,
                                         CASE WHEN char_social_search_cases.birthday is null THEN ' ' Else DATE_FORMAT(char_social_search_cases.birthday,'%Y/%m/%d')  END  AS birthday,
                                         CASE
                                                   WHEN char_social_search_cases.gender is null THEN ' '
                                                   WHEN char_social_search_cases.gender = 1 THEN '$male'
                                                   WHEN char_social_search_cases.gender = 2 THEN '$female'
                                         END  AS gender,
                                         CASE WHEN char_social_search_cases.marital_status_id is null THEN ' '
                                                  Else char_marital_status.name  END  
                                         AS marital_status_id,
                                         CASE WHEN char_social_search_cases.deserted is null THEN ' '
                                                 WHEN char_social_search_cases.deserted = 1 THEN '$yes'
                                                 WHEN char_social_search_cases.deserted = 0 THEN '$no'
                                             END
                                             AS deserted,
                                         CASE
                                               WHEN char_social_search_cases_details.refugee is null THEN ' '
                                               WHEN char_social_search_cases_details.refugee = 0 THEN ' '
                                               WHEN char_social_search_cases_details.refugee = 1 THEN ' $citizen '
                                               WHEN char_social_search_cases_details.refugee = 2 THEN '$refugee'
                                         END
                                         AS refugee,
                                         CASE
                                               WHEN char_social_search_cases_details.refugee is null THEN ' '
                                               WHEN char_social_search_cases_details.refugee = 0 THEN ' '
                                               WHEN char_social_search_cases_details.refugee = 1 THEN ' '
                                               WHEN char_social_search_cases_details.refugee = 2 THEN char_social_search_cases_details.unrwa_card_number
                                         END
                                         AS unrwa_card_number,
                                         CASE WHEN char_social_search_cases.birth_place is null THEN ' ' Else L.name END AS birth_place,
                                         CASE WHEN char_social_search_cases.nationality is null THEN ' ' Else L0.name END AS nationality,
                                               char_social_search_cases.family_cnt AS family_count,
                                               char_social_search_cases.spouses AS spouses,
                                               char_social_search_cases.male_live AS male_count,
                                               char_social_search_cases.female_live AS female_count ,
                                              
                                        CASE WHEN char_social_search_cases.country_id  is null THEN ' ' Else country_name.name END   AS country,
                                        CASE WHEN char_social_search_cases.district_id  is null THEN ' ' Else district_name.name END   AS governarate ,
                                        CASE WHEN char_social_search_cases.region_id  is null THEN ' ' Else region_name.name END   AS region_name,
                                        CASE WHEN char_social_search_cases.neighborhood_id is null THEN ' ' Else location_name.name END   AS nearLocation,
                                        CASE WHEN char_social_search_cases.square_id is null THEN ' ' Else square_name.name END   AS square_name,
                                        CASE WHEN char_social_search_cases.mosque_id is null THEN ' ' Else mosques_name.name END   AS mosque_name,
                                        CASE WHEN char_social_search_cases.street_address is null THEN ' ' Else char_social_search_cases.street_address END   AS street_address,
                                        CONCAT(ifnull(country_name.name,' '),' _ ',
                                                            ifnull(district_name.name,' '),'  ',
                                                            ifnull(region_name.name,' '),'  ',
                                                            ifnull(location_name.name,' '),'  ',
                                                            ifnull(square_name.name,' '),'  ',
                                                            ifnull(mosques_name.name,' '),' ',
                                                            ifnull(char_social_search_cases.street_address,' '))
                                            AS address,
                                        CASE WHEN char_social_search_cases.phone is null THEN ' ' Else char_social_search_cases.phone  END  AS phone,
                                        CASE WHEN char_social_search_cases.primary_mobile is null THEN ' ' Else char_social_search_cases.primary_mobile  END  AS primary_mobile,
                                        CASE WHEN char_social_search_cases.secondary_mobile is null THEN ' ' Else char_social_search_cases.secondary_mobile  END  AS secondary_mobile,
                                        CASE WHEN char_social_search_cases.watanya is null THEN ' ' Else char_social_search_cases.watanya  END  AS watanya ,
                                        CASE WHEN char_social_search_cases_details.receivables = 1 THEN '$yes' Else '$no' END AS receivables,
                                        CASE
                                              WHEN char_social_search_cases_details.receivables is null THEN '0'
                                              WHEN char_social_search_cases_details.receivables = 0 THEN '0'
                                              WHEN char_social_search_cases_details.receivables = 1 THEN char_social_search_cases_details.receivables_sk_amount
                                        END AS receivables_sk_amount ,
                                        CASE
                                                   WHEN char_social_search_cases_details.is_qualified is null THEN '-'
                                                   WHEN char_social_search_cases_details.is_qualified = 0 THEN '$not_qualified '
                                                   WHEN char_social_search_cases_details.is_qualified = 1 THEN ' $qualified '
                                             END
                                        AS is_qualified,
                                        CASE WHEN char_social_search_cases_details.qualified_card_number is null THEN '-' Else char_social_search_cases_details.qualified_card_number  END  AS qualified_card_number,
                                        CASE WHEN char_social_search_cases_details.monthly_income is null THEN ' ' Else char_social_search_cases_details.monthly_income  END  AS monthly_income,
                                        CASE WHEN char_social_search_cases_details.actual_monthly_income is null THEN ' ' Else char_social_search_cases_details.actual_monthly_income  END  AS actual_monthly_income,
                                        CASE
                                           WHEN char_social_search_cases_work.working is null THEN ' '
                                           WHEN char_social_search_cases_work.working = 1 THEN '$yes'
                                           WHEN char_social_search_cases_work.working = 2 THEN '$no'
                                         END
                                         AS working,
                                         CASE
                                               WHEN char_social_search_cases_work.working is null THEN ' '
                                               WHEN char_social_search_cases_work.working = 1 THEN work_status.name
                                               WHEN char_social_search_cases_work.working = 2 THEN ' '
                                         END
                                         AS work_status,
                                         CASE
                                               WHEN char_social_search_cases_work.working is null THEN ' '
                                               WHEN char_social_search_cases_work.working = 1 THEN work_job.name
                                               WHEN char_social_search_cases_work.working = 2 THEN ' '
                                         END
                                         AS work_job,
                                         CASE
                                               WHEN char_social_search_cases_work.working is null THEN ' '
                                               WHEN char_social_search_cases_work.working = 1 THEN work_wage.name
                                               WHEN char_social_search_cases_work.working = 2 THEN ' '
                                         END
                                         AS work_wage,
                                         CASE
                                               WHEN char_social_search_cases_work.working is null THEN ' '
                                               WHEN char_social_search_cases_work.working = 1 THEN char_social_search_cases_work.work_location
                                               WHEN char_social_search_cases_work.working = 2 THEN ' '
                                         END
                                         AS work_location,
                                         CASE
                                               WHEN char_social_search_cases_work.can_work is null THEN ' '
                                               WHEN char_social_search_cases_work.can_work = 1 THEN '$yes'
                                               WHEN char_social_search_cases_work.can_work = 2 THEN '$no'
                                         END
                                         AS can_work,
                                         CASE
                                               WHEN char_social_search_cases_work.can_work is null THEN ' '
                                               WHEN char_social_search_cases_work.can_work = 1 THEN ' '
                                               WHEN char_social_search_cases_work.can_work = 2 THEN work_reason.name
                                         END
                                         AS work_reason,
                                         CASE WHEN char_social_search_cases_work.has_other_work_resources is null THEN ' '
                                              WHEN char_social_search_cases_work.has_other_work_resources = 1 THEN '$yes'
                                              WHEN char_social_search_cases_work.has_other_work_resources = 0 THEN '$no'
                                         END AS has_other_work_resources,
                                         CASE WHEN char_building_status_i18n.name is null THEN ' ' Else char_building_status_i18n.name  END  AS house_condition,
                                         CASE WHEN char_furniture_status_i18n.name is null THEN ' ' Else char_furniture_status_i18n.name  END  AS indoor_condition,
                                         CASE WHEN char_habitable_status_i18n.name is null THEN ' ' Else char_habitable_status_i18n.name  END  AS habitable,
                                         CASE WHEN char_house_status_i18n.name is null     THEN ' ' Else char_house_status_i18n.name  END  AS residence_condition,
                                         CASE WHEN char_property_types_i18n.name is null   THEN ' ' Else char_property_types_i18n.name  END  AS property_types,
                                         CASE WHEN char_roof_materials_i18n.name is null   THEN ' ' Else char_roof_materials_i18n.name  END  AS roof_materials,
                                         CASE WHEN char_social_search_cases_residence.area is null             THEN ' ' Else char_social_search_cases_residence.area  END  AS area,
                                         CASE WHEN char_social_search_cases_residence.rooms is null            THEN ' ' Else char_social_search_cases_residence.rooms  END  AS rooms,
                                         CASE WHEN char_social_search_cases_residence.rent_value is null       THEN ' ' Else char_social_search_cases_residence.rent_value  END  AS rent_value,
                                         CASE WHEN char_social_search_cases_residence.need_repair is null THEN ' '
                                              WHEN char_social_search_cases_residence.need_repair = 1 THEN '$yes'
                                              WHEN char_social_search_cases_residence.need_repair = 2 THEN '$no'
                                            END AS need_repair ,
                                         CASE WHEN char_social_search_cases_residence.repair_notes is null   THEN ' ' Else char_social_search_cases_residence.repair_notes  END  AS repair_notes,
                                         CASE
                                                          WHEN char_social_search_cases_health.condition is null THEN ' '
                                                          WHEN char_social_search_cases_health.condition = 1 THEN '$perfect'
                                                          WHEN char_social_search_cases_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                          WHEN char_social_search_cases_health.condition = 3 THEN '$special_needs'
                                                     END
                                                     AS health_status,
                                                     CASE WHEN char_social_search_cases_health.details is null THEN ' ' Else char_social_search_cases_health.details END   AS details,
                                                     CASE WHEN char_diseases_i18n.name is null THEN ' ' Else char_diseases_i18n.name END   AS disease_id,
                                                     CASE
                                                          WHEN char_social_search_cases_health.has_health_insurance is null THEN ' '
                                                          WHEN char_social_search_cases_health.has_health_insurance = 1 THEN '$yes'
                                                          WHEN char_social_search_cases_health.has_health_insurance = 0 THEN '$no'
                                                     END
                                                     AS has_health_insurance,
                                                     CASE WHEN char_social_search_cases_health.health_insurance_number is null THEN ' ' Else char_social_search_cases_health.health_insurance_number END   AS health_insurance_number,
                                                     CASE WHEN char_social_search_cases_health.health_insurance_type is null THEN ' ' Else char_social_search_cases_health.health_insurance_type END   AS health_insurance_type,
                                                     CASE
                                                          WHEN char_social_search_cases_health.has_device is null THEN ' '
                                                          WHEN char_social_search_cases_health.has_device = 1 THEN '$yes'
                                                          WHEN char_social_search_cases_health.has_device = 0 THEN '$no'
                                                     END
                                                     AS has_device,
                                                     CASE WHEN char_social_search_cases_health.used_device_name is null THEN ' ' Else char_social_search_cases_health.used_device_name END   AS used_device_name,
                                         CASE
                                                WHEN char_social_search_cases_details.promised is null THEN ' '
                                                WHEN char_social_search_cases_details.promised = 1 THEN '$no'
                                                WHEN char_social_search_cases_details.promised = 2 THEN '$yes'
                                            END
                                          AS promised,
                                          CASE
                                              WHEN char_social_search_cases_details.promised = 1 THEN ' '
                                              WHEN char_social_search_cases_details.promised is null THEN ' '
                                              WHEN char_social_search_cases_details.promised = 2 THEN char_social_search_cases_details.reconstructions_organization_name
                                          END
                                          AS ReconstructionOrg,
                                          CASE WHEN char_social_search_cases_details.visited_at is null THEN ' ' Else char_social_search_cases_details.visited_at END AS visited_at,
                                          CASE WHEN char_social_search_cases_details.visitor is null THEN ' ' Else char_social_search_cases_details.visitor END AS visitor,
                                          CASE WHEN char_social_search_cases_details.notes is null THEN ' ' Else char_social_search_cases_details.notes END AS notes,
                                          CASE WHEN char_social_search_cases_details.visitor_evaluation is null THEN ' ' Else char_social_search_cases_details.visitor_evaluation END AS visitor_evaluation,
                                          CASE WHEN char_social_search_cases_details.visitor_opinion is null THEN ' ' Else char_social_search_cases_details.visitor_opinion END AS visitor_opinion,
                                          CASE WHEN char_social_search_cases_details.visitor_card is null THEN ' ' Else char_social_search_cases_details.visitor_card END AS visitor_card
                                   ");

            $result=$result->get();

            $aid_source= \DB::table('char_aid_sources')
                ->selectRaw(" char_aid_sources.name, char_aid_sources.id")
                ->orderBy('char_aid_sources.name')->get();

            $properties = \DB::table('char_properties_i18n')
                ->selectRaw(" char_properties_i18n.name, char_properties_i18n.property_id as id")
                ->where('char_properties_i18n.language_id', $language_id)
                ->orderBy('char_properties_i18n.name')->get();

            $essentials = \DB::table('char_essentials')
                ->selectRaw(" char_essentials.id as id,char_essentials.name")
                ->orderBy('char_essentials.name')->get();

            foreach($result as $item) {

                $financial_aid_source_= \DB::table('char_social_search_cases_aids')
                    ->where('char_social_search_cases_aids.aid_type', '=', 1)
                    ->where('char_social_search_cases_aids.search_cases_id', '=', $item->id)
                    ->selectRaw("aid_source_id ,CASE WHEN char_social_search_cases_aids.aid_value is null THEN '0' 
                                                                  WHEN char_social_search_cases_aids.aid_take = 0 THEN '0'
                                                                                   WHEN char_social_search_cases_aids.aid_value is null THEN '0'  
                                                                                     Else char_social_search_cases_aids.aid_value END 
                                                                                AS  aid_value
                                                         ")->get();

                $data_ = [];
                $item->financial_aid_source_elements = [];
                foreach($financial_aid_source_ as $kk=>$i) {
                    if(!isset($data_[$i->aid_source_id])){
                        $item->financial_aid_source_elements[]=$i->aid_source_id;
                        $data_[$i->aid_source_id]=$i->aid_value;
                    }
                }
                $item->financial_aid_source = $data_;

                $non_financial_aid_source_= \DB::table('char_social_search_cases_aids')
                    ->where('char_social_search_cases_aids.aid_type', '=', 2)
                    ->where('char_social_search_cases_aids.search_cases_id', '=', $item->id)
                    ->selectRaw("aid_source_id ,CASE WHEN char_social_search_cases_aids.aid_value is null THEN '0' 
                                                                  WHEN char_social_search_cases_aids.aid_take = 0 THEN '0'
                                                                                   WHEN char_social_search_cases_aids.aid_value is null THEN '0'  
                                                                                     Else char_social_search_cases_aids.aid_value END 
                                                                                AS  aid_value
                                                         ")->get();

                $data_ = [];
                $item->non_financial_aid_source_elements = [];
                foreach($non_financial_aid_source_ as $kk=>$i) {
                    if(!isset($data_[$i->aid_source_id])){
                        $item->non_financial_aid_source_elements[]=$i->aid_source_id;
                        $data_[$i->aid_source_id]=$i->aid_value;
                    }
                }
                $item->non_financial_aid_source = $data_;

                $persons_properties= \DB::table('char_social_search_cases_properties')
                    ->where('char_social_search_cases_properties.search_cases_id', '=', $item->id)
                    ->selectRaw("char_social_search_cases_properties.property_id as property_id ,
                                                             CASE WHEN char_social_search_cases_properties.has_property is null THEN '0' 
                                                                  WHEN char_social_search_cases_properties.has_property = 0 THEN 0  
                                                                  WHEN char_social_search_cases_properties.quantity is null THEN 0  
                                                                  Else char_social_search_cases_properties.quantity END
                                                             AS  quantity")
                    ->get();

                $data_=[];
                $item->persons_properties_elements = [];
                foreach($persons_properties as $kk=>$i) {
                    if(!isset($data_[$i->property_id])){
                        $item->persons_properties_elements[]=$i->property_id;
                        $data_[$i->property_id]=$i->quantity;
                    }
                }

                $item->persons_properties = $data_;

                $persons_essentials= \DB::table('char_social_search_cases_essentials')
                    ->where('char_social_search_cases_essentials.search_cases_id', '=', $item->id)
                    ->selectRaw("char_social_search_cases_essentials.essential_id ,
                                          char_social_search_cases_essentials.needs as needs")
                    ->get();

                $data_=[];
                $item->persons_essentials_elements = [];
                foreach($persons_essentials as $kk=>$i) {
                    if(!isset($data_[$i->essential_id])){
                        $item->persons_essentials_elements[]=$i->essential_id;
                        $data_[$i->essential_id]=$i->needs;
                    }
                }

                $item->persons_essentials = $data_;

            }


            $output = array('statistic'=>$result,
                'essentials'=>$essentials ,'properties'=>$properties , 'aid_source'=>$aid_source);
        }
        else {
            $result->selectRaw("
                               char_social_search.title as search_title,
                                         char_social_search.date_from,
                                         char_social_search.date_to,
                                             CASE WHEN char_social_search.organization_id = '$organization_id' THEN 1 Else 0  END  AS is_mine,
                                    CONCAT(ifnull(char_social_search_cases.first_name, ' '), ' ' ,ifnull(char_social_search_cases.second_name, ' '),' ',ifnull(char_social_search_cases.third_name, ' '),' ', ifnull(char_social_search_cases.last_name,' ')) AS full_name,
                                    CASE WHEN char_social_search_cases.id_card_number is null THEN ' ' Else char_social_search_cases.id_card_number  END  AS id_card_number,
                                    CASE WHEN char_social_search_cases.gender is null THEN ' '
                                         WHEN char_social_search_cases.gender = 1 THEN '$male'
                                         WHEN char_social_search_cases.gender = 2 THEN '$female'
                                    END AS case_gender,char_social_search_cases.gender,
                                    char_social_search_cases.id,
                                    char_social_search_cases.id as case_id,
                                    char_social_search.category_id ,
                                    CASE WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS category_name,
                                    char_social_search.organization_id,org.name as organization_name,
                                    CASE WHEN  char_social_search_cases.birthday is null THEN ' ' Else 
                                    EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(), char_social_search_cases.birthday)))) END AS age,    
                                    char_social_search_cases_details.visitor,
                                    CASE WHEN char_social_search_cases.status = 1 THEN '$inactive'WHEN char_social_search_cases.status = 0 THEN '$active' END AS cases_status,
                                    char_social_search_cases.status,
                                    CASE WHEN char_social_search_cases.marital_status_id is null THEN ' ' Else char_marital_status.name  END  AS marital_status,
                                    char_social_search_cases.family_cnt AS family_count,
                                    char_social_search_cases.spouses AS spouses,
                                    char_social_search_cases.male_live AS male_count,
                                    char_social_search_cases.female_live AS female_count,
                                    CASE WHEN char_social_search_cases.primary_mobile is null THEN ' ' Else char_social_search_cases.primary_mobile  END  AS primary_mobile,
                                    CASE WHEN char_social_search_cases.district_id  is null THEN ' ' Else district_name.name END   AS governarate ,
                                    CASE WHEN char_social_search_cases.region_id  is null THEN ' ' Else region_name.name END   AS region_name
                                   ");

            $itemsCount = isset($filters['itemsCount'])? $filters['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$result->count());
            $output=$result->paginate($records_per_page);
        }

        return $output;
    }

    public static function fetch($id)
    {


        $language_id = 1;
        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');

        $not_selected = trans('common::application.not_selected');
        $refugee = trans('common::application.refugee');
        $citizen = trans('common::application.citizen');

        $not_work = trans('common::application.not_work');
        $work = trans('common::application.work');

        $perfect = trans('common::application.perfect');
        $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
        $special_needs = trans('common::application.special_needs');

        $excellent = trans('common::application.excellent');
        $good = trans('common::application.good');
        $very_good = trans('common::application.very_good');
        $bad = trans('common::application.bad_condition');
        $very_bad = trans('common::application.very_bad_condition');

        $qualified = trans('common::application.qualified');
        $not_qualified = trans('common::application.not qualified');


        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $not_promise = trans('common::application.not_promise');
        $promise = trans('common::application.promise');

        $query = self::query()
            ->join('char_social_search','char_social_search_cases.search_id', 'char_social_search.id')
            ->join('char_categories','char_social_search.category_id', 'char_categories.id')
            ->join('char_organizations','char_social_search.organization_id', 'char_organizations.id')
            ->leftjoin('char_social_search_cases_details','char_social_search_cases.id', '=','char_social_search_cases_details.search_cases_id')
            ->selectRaw("char_social_search_cases.* , char_social_search.* , char_categories.name AS category_name ,
            char_categories.type as category_type,
                                   CASE WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS category_name,
                                   CASE WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name,
                                   CASE WHEN char_social_search_cases_details.needs is null  
                                        THEN ' ' Else char_social_search_cases_details.needs  END  AS needs,
                                   CASE WHEN char_social_search_cases_details.promised = 2 THEN '$yes'  Else '$no'  END AS promised,
                                   CASE WHEN char_social_search_cases_details.promised = 2 THEN '$promise' Else '$not_promise' END AS if_promised,
                                   CASE WHEN char_social_search_cases_details.promised = 2 THEN '*' Else ' ' END AS was_promised,
                                   CASE WHEN char_social_search_cases_details.promised = 2 THEN ' ' Else '*' END AS not_promised,
                                   CASE WHEN char_social_search_cases_details.promised = 2 THEN char_social_search_cases_details.reconstructions_organization_name Else ' ' END 
                                   AS reconstructions_organization_name");

        $query->selectRaw("CASE WHEN  char_social_search_cases.birthday is null THEN ' ' Else 
                               EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(), char_social_search_cases.birthday)))) END AS age");

        $query->selectRaw("CASE WHEN char_social_search_cases_details.visited_at is null THEN ' ' Else char_social_search_cases_details.visited_at END AS visited_at,
                                    CASE WHEN char_social_search_cases_details.visitor is null THEN ' ' Else char_social_search_cases_details.visitor END AS visitor,
                                    CASE WHEN char_social_search_cases_details.visitor_evaluation is null THEN ' ' Else char_social_search_cases_details.visitor_evaluation END AS visitor_evaluation,
                                    CASE
                                        WHEN char_social_search_cases_details.visitor_evaluation is null THEN ' '
                                        WHEN char_social_search_cases_details.visitor_evaluation = 0 THEN '$very_bad'
                                        WHEN char_social_search_cases_details.visitor_evaluation = 1 THEN '$bad'
                                        WHEN char_social_search_cases_details.visitor_evaluation = 2 THEN '$good'
                                        WHEN char_social_search_cases_details.visitor_evaluation = 3 THEN '$very_good'
                                        WHEN char_social_search_cases_details.visitor_evaluation = 4 THEN '$excellent'
                                        Else ' '
                                    END AS visitor_evaluation_,
                                    CASE WHEN char_social_search_cases_details.visitor_opinion is null THEN ' ' Else char_social_search_cases_details.visitor_opinion END AS visitor_opinion,
                                    CASE WHEN char_social_search_cases_details.visitor_card is null THEN ' ' Else char_social_search_cases_details.visitor_card END AS visitor_card,
                                    char_social_search_cases.id_card_number,
                                    CASE WHEN char_social_search_cases_details.visitor_evaluation is null THEN ' ' Else char_social_search_cases_details.visitor_evaluation END AS visitor_evaluation,
                                    CASE WHEN char_social_search_cases_details.visitor_opinion is null THEN ' ' Else char_social_search_cases_details.visitor_opinion END AS visitor_opinion,
                                    CASE WHEN char_social_search_cases_details.visitor_card is null THEN ' ' Else char_social_search_cases_details.visitor_card END AS visitor_card,
                                    char_social_search.organization_id,
                                    CASE WHEN char_social_search_cases_details.visitor_evaluation = 0 THEN '*' Else ' ' END AS very_bad_condition_evaluation,
                                    CASE WHEN char_social_search_cases_details.visitor_evaluation = 1 THEN '*' Else ' ' END AS bad_condition_evaluation,
                                    CASE WHEN char_social_search_cases_details.visitor_evaluation = 2 THEN '*' Else ' ' END AS good_evaluation,
                                    CASE WHEN char_social_search_cases_details.visitor_evaluation = 3 THEN '*' Else ' ' END AS very_good_evaluation,
                                    CASE WHEN char_social_search_cases_details.visitor_evaluation = 4 THEN '*' Else ' ' END AS excellent_evaluation ,  
                                    CASE WHEN char_social_search_cases_details.visitor_evaluation is null THEN ' ' Else char_social_search_cases_details.visitor_evaluation END AS visitor_evaluation,
                                    CASE WHEN char_social_search_cases_details.visitor_opinion is null THEN ' ' Else char_social_search_cases_details.visitor_opinion END AS visitor_opinion,
                                    CASE WHEN char_social_search_cases_details.visitor_card is null THEN ' ' Else char_social_search_cases_details.visitor_card END AS visitor_card,
                                    CASE WHEN char_social_search_cases_details.visited_at is null THEN ' ' Else char_social_search_cases_details.visited_at END AS visited_at,
                                    CASE WHEN char_social_search_cases_details.visitor is null THEN ' ' Else char_social_search_cases_details.visitor END AS visitor,
                                    CASE WHEN char_social_search_cases_details.notes is null THEN ' ' Else char_social_search_cases_details.notes END AS notes");

        $query->selectRaw("CASE WHEN char_social_search_cases_details.actual_monthly_income  is null
                                         THEN ' ' Else char_social_search_cases_details.actual_monthly_income END AS actual_monthly_income");


        $query->leftjoin('char_marital_status_i18n', function($join) use ($language_id) {
            $join->on('char_social_search_cases.marital_status_id', '=', 'char_marital_status_i18n.marital_status_id')
                ->where('char_marital_status_i18n.language_id', $language_id);
        })
            ->selectRaw("CASE WHEN char_social_search_cases.marital_status_id is null THEN ' ' Else char_marital_status_i18n.name  END  AS marital_status_name");


        $query->leftjoin('char_locations_i18n As birth_place_name', function($join) use ($language_id) {
            $join->on('char_social_search_cases.birth_place', '=','birth_place_name.location_id' )
                ->where('birth_place_name.language_id', $language_id);
        })
            ->selectRaw("CASE WHEN char_social_search_cases.birth_place is null THEN ' ' Else birth_place_name.name  END  AS birth_place");

        $query->leftjoin('char_locations_i18n As nationality_name', function($join) use ($language_id)  {
            $join->on('char_social_search_cases.nationality', '=','nationality_name.location_id' )
                ->where('nationality_name.language_id', $language_id);

        })
            ->selectRaw("CASE WHEN char_social_search_cases.nationality is null THEN ' ' Else nationality_name.name  END  AS nationality");


        $query->leftjoin('char_aids_locations_i18n As country_name', function($join)  use ($language_id) {
            $join->on('char_social_search_cases.country_id', '=','country_name.location_id' )
                ->where('country_name.language_id', $language_id);
        })
            ->selectRaw("CASE WHEN char_social_search_cases.country_id  is null THEN ' ' Else country_name.name END   AS country");


        $query->leftjoin('char_aids_locations_i18n As district_name', function($join)  use ($language_id) {
            $join->on('char_social_search_cases.district_id', '=','district_name.location_id' )
                ->where('district_name.language_id', $language_id);
        })
            ->selectRaw("CASE WHEN char_social_search_cases.district_id  is null THEN ' ' Else district_name.name END   AS governarate");

        $query->leftjoin('char_aids_locations_i18n As region_name', function($join) use ($language_id)  {
            $join->on('char_social_search_cases.region_id', '=','region_name.location_id' )
                ->where('region_name.language_id', $language_id);
        })
            ->selectRaw("CASE WHEN char_social_search_cases.region_id is null THEN ' ' Else region_name.name END   AS region_name");

        $query->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)  {
            $join->on('char_social_search_cases.neighborhood_id', '=','location_name.location_id' )
                ->where('location_name.language_id', $language_id);
        })
            ->selectRaw("CASE WHEN char_social_search_cases.neighborhood_id is null THEN ' ' Else location_name.name END   AS nearLocation");

        $query->leftjoin('char_aids_locations_i18n As square_name', function($join) use ($language_id)  {
            $join->on('char_social_search_cases.square_id', '=','square_name.location_id' )
                ->where('square_name.language_id', $language_id);
        })
            ->selectRaw("CASE WHEN char_social_search_cases.square_id is null THEN ' ' Else square_name.name END   AS square_name");

        $query->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
            $join->on('char_social_search_cases.mosque_id', '=','mosques_name.location_id' )
                ->where('mosques_name.language_id', $language_id);
        })
            ->selectRaw("CASE WHEN char_social_search_cases.mosque_id is null THEN ' ' Else mosques_name.name END   AS mosque_name");

        $query->selectRaw("CONCAT(ifnull(country_name.name,' '),' _ ',
                                                            ifnull(district_name.name,' '),' _ ',
                                                            ifnull(region_name.name,' '),' _ ',
                                                            ifnull(location_name.name,' '),' _ ',
                                                            ifnull(square_name.name,' '),' _ ',
                                                            ifnull(mosques_name.name,' '),' _ ',
                                                            ifnull(char_social_search_cases.street_address,' '))
                                            AS address");


        $query->selectRaw("CASE WHEN char_social_search_cases.street_address  is null THEN ' ' Else char_social_search_cases.street_address END AS street_address");
        $query->selectRaw("
                                   CASE WHEN char_social_search_cases.id_card_number is null THEN ' ' Else char_social_search_cases.id_card_number  END  AS id_card_number,
                                   CASE
                                           WHEN char_social_search_cases.card_type is null THEN ' '
                                           WHEN char_social_search_cases.card_type = 1 THEN '$id_card'
                                           WHEN char_social_search_cases.card_type = 2 THEN '$id_number'
                                           WHEN char_social_search_cases.card_type = 3 THEN '$passport '
                                     END
                                     AS card_type,
                                   CASE WHEN char_social_search_cases.birthday is null THEN ' ' Else DATE_FORMAT(char_social_search_cases.birthday,'%Y/%m/%d')  END  AS birthday,
                                   CASE WHEN char_social_search_cases_details.monthly_income is null THEN ' ' Else char_social_search_cases_details.monthly_income  END  AS monthly_income,
                                   CASE
                                       WHEN char_social_search_cases_details.refugee is null THEN '$not_selected'
                                       WHEN char_social_search_cases_details.refugee = 0 THEN '$not_selected'
                                       WHEN char_social_search_cases_details.refugee = 1 THEN ' $refugee '
                                       WHEN char_social_search_cases_details.refugee = 2 THEN '$citizen'
                                   END
                                   AS refugee,
                                   CASE WHEN char_social_search_cases_details.refugee = 2 THEN '$yes' Else '$no' END AS is_refugee,
                                   CASE WHEN char_social_search_cases_details.refugee = 2 THEN '*' Else ' ' END AS per_refugee,
                                   CASE WHEN char_social_search_cases_details.refugee = 1 THEN '$yes' Else '$no' END AS is_citizen,
                                   CASE WHEN char_social_search_cases_details.refugee = 1 THEN '*' Else ' ' END AS per_citizen,
                                   CASE
                                       WHEN char_social_search_cases_details.unrwa_card_number is null THEN ' '
                                       WHEN char_social_search_cases_details.refugee is null THEN ' '
                                       WHEN char_social_search_cases_details.refugee = 0 THEN ' '
                                       WHEN char_social_search_cases_details.refugee = 1 THEN ' '
                                       WHEN char_social_search_cases_details.refugee = 2 THEN char_social_search_cases_details.unrwa_card_number
                                 END
                                 AS unrwa_card_number,
                                 char_social_search_cases.gender as gender_id,
                                 CASE
                                       WHEN char_social_search_cases.gender is null THEN ' '
                                       WHEN char_social_search_cases.gender = 1 THEN '$male'
                                       WHEN char_social_search_cases.gender = 2 THEN '$female'
                                 END
                                 AS gender,
                                 CASE WHEN char_social_search_cases.gender = 1 THEN '*' Else ' ' END AS gender_male,         
                                 CASE WHEN char_social_search_cases.gender = 2 THEN '*' Else ' ' END AS gender_female,
                                  CASE
                                       WHEN char_social_search_cases.deserted is null THEN '$no'
                                       WHEN char_social_search_cases.deserted = 1 THEN '$yes'
                                       WHEN char_social_search_cases.deserted = 0 THEN '$no'
                                 END
                                 AS deserted,
                                 CASE WHEN (char_social_search_cases.deserted is null or char_social_search_cases.deserted = 1 ) THEN '*'  
                                      Else ' ' END AS is_deserted,         
                                 CASE WHEN char_social_search_cases.deserted = 0 THEN '*' Else ' ' END AS not_deserted,
                                 CONCAT(ifnull(char_social_search_cases.first_name, ' '), ' ' ,ifnull(char_social_search_cases.second_name, ' '),' ',ifnull(char_social_search_cases.third_name, ' '),' ', ifnull(char_social_search_cases.last_name,' ')) AS full_name,
                                 CASE WHEN char_social_search_cases.first_name is null THEN ' ' Else char_social_search_cases.first_name  END  AS first_name,
                                 CASE WHEN char_social_search_cases.first_name is null THEN ' ' Else char_social_search_cases.first_name  END  AS first_name,
                                 CASE WHEN char_social_search_cases.second_name is null THEN ' ' Else char_social_search_cases.second_name  END  AS second_name,
                                 CASE WHEN char_social_search_cases.third_name is null THEN ' ' Else char_social_search_cases.third_name  END  AS third_name,
                                 CASE WHEN char_social_search_cases.last_name is null THEN ' ' Else char_social_search_cases.last_name  END  AS last_name,
                                 CASE WHEN char_social_search_cases_details.receivables = 1 THEN '$yes' Else '$no' END AS receivables,
                                 CASE WHEN char_social_search_cases_details.receivables = 1 THEN '*' Else ' ' END AS is_receivables,         
                                 CASE WHEN char_social_search_cases_details.receivables = 0 THEN '*' Else ' ' END AS not_receivables,
                                 CASE
                                      WHEN char_social_search_cases_details.receivables is null THEN '0'
                                      WHEN char_social_search_cases_details.receivables = 0 THEN '0'
                                      WHEN char_social_search_cases_details.receivables = 1 THEN char_social_search_cases_details.receivables_sk_amount
                                 END AS receivables_sk_amount ,
                                 CASE WHEN char_social_search_cases_details.is_qualified = 1 THEN '$qualified' Else '$not_qualified' END AS is_qualified,
                                 CASE WHEN char_social_search_cases_details.is_qualified = 1 THEN '*' Else ' ' END AS qualified,         
                                 CASE WHEN char_social_search_cases_details.is_qualified != 1 THEN '*' Else ' ' END AS not_qualified,
                                 CASE WHEN char_social_search_cases_details.qualified_card_number is null THEN '-' Else char_social_search_cases_details.qualified_card_number  END  AS qualified_card_number    
                               ");

        $query->selectRaw("CASE WHEN char_social_search_cases.marital_status_id is null THEN ' ' Else char_marital_status_i18n.name  END  AS marital_status_name");
        $query->selectRaw("CONCAT(ifnull(char_social_search_cases.first_name, ' '), ' ' ,ifnull(char_social_search_cases.second_name, ' '),' ',ifnull(char_social_search_cases.third_name, ' '),' ', ifnull(char_social_search_cases.last_name,' ')) AS full_name");
        $query->selectRaw("char_social_search_cases.primary_mobile  AS primary_mobile");
        $query->selectRaw("char_social_search_cases.secondary_mobile  AS secondary_mobile");
        $query->selectRaw("char_social_search_cases.watanya  AS watanya");
        $query->selectRaw("char_social_search_cases.phone  AS phone");

        $query->leftjoin('char_social_search_cases_work','char_social_search_cases.id', '=','char_social_search_cases_work.search_cases_id');

        $query->selectRaw("
                                 CASE WHEN char_social_search_cases_work.working is null THEN ' '
                                        WHEN char_social_search_cases_work.working = 1 THEN '$work'
                                        WHEN char_social_search_cases_work.working = 2 THEN '$not_work'
                                        END
                                  AS working,
                                  CASE WHEN char_social_search_cases_work.working is null THEN ' '
                                        WHEN char_social_search_cases_work.working = 1 THEN '$yes'
                                        WHEN char_social_search_cases_work.working = 2 THEN '$no'
                                        END
                                  AS works, 
                                  CASE WHEN char_social_search_cases_work.has_other_work_resources is null THEN ' '
                                        WHEN char_social_search_cases_work.has_other_work_resources = 1 THEN '$yes'
                                        WHEN char_social_search_cases_work.has_other_work_resources = 0 THEN '$no'
                                        END
                                  AS has_other_work_resources,
                                  CASE WHEN char_social_search_cases_work.has_other_work_resources = 1 THEN '*' Else ' ' END AS has_other_resources,         
                                  CASE WHEN char_social_search_cases_work.has_other_work_resources is null THEN '*'
                                       WHEN char_social_search_cases_work.has_other_work_resources != 1 THEN '*' Else ' ' END AS not_has_other_resources,                                       
                                  CASE WHEN char_social_search_cases_work.working = 1 THEN '*' Else ' ' END AS is_working,         
                                  CASE WHEN char_social_search_cases_work.working is null THEN '*'
                                       WHEN char_social_search_cases_work.working != 1 THEN '*' Else ' ' END AS not_working         
                                  ");

        $is_can_work = trans('common::application.is_can_work');
        $not_can_work = trans('common::application.not_can_work');

        $query->selectRaw("CASE WHEN char_social_search_cases_work.can_work is null THEN ' '
                                       WHEN char_social_search_cases_work.can_work = 1 THEN '$is_can_work'
                                       WHEN char_social_search_cases_work.can_work = 2 THEN '$not_can_work'
                                       END 
                                  AS can_work,
                                  CASE WHEN char_social_search_cases_work.can_work is null THEN ' '
                                       WHEN char_social_search_cases_work.can_work = 1 THEN '$yes'
                                       WHEN char_social_search_cases_work.can_work = 2 THEN '$no'
                                       END 
                                  AS can_works,
                                   CASE WHEN char_social_search_cases_work.can_work = 1 THEN '*' Else ' ' END AS can_working,         
                                  CASE WHEN char_social_search_cases_work.can_work is null THEN '*'
                                       WHEN char_social_search_cases_work.can_work = 2 THEN '*' Else ' ' END AS can_not_working         
                                  ");

        $query->selectRaw("CASE WHEN char_social_search_cases_work.working is null THEN ' '
                                       WHEN char_social_search_cases_work.working = 1 THEN char_social_search_cases_work.work_location
                                       WHEN char_social_search_cases_work.working = 2 THEN ' '
                                       END
                                  AS work_location");

        $query->leftjoin('char_work_jobs_i18n', function($join) use ($language_id){
            $join->on('char_social_search_cases_work.work_job_id', '=',  'char_work_jobs_i18n.work_job_id')
                ->where('char_work_jobs_i18n.language_id', $language_id);
        })
            ->selectRaw("CASE  WHEN char_social_search_cases_work.working is null THEN ' '
                                                    WHEN char_social_search_cases_work.working = 1 THEN char_work_jobs_i18n.name
                                                    WHEN char_social_search_cases_work.working = 2 THEN ' '
                                                    END
                                              AS work_job");


        $query->leftjoin('char_work_status_i18n', function($join) use ($language_id) {
            $join->on('char_social_search_cases_work.work_status_id', '=',  'char_work_status_i18n.work_status_id')
                ->where('char_work_status_i18n.language_id', $language_id);
        })
            ->selectRaw("CASE  WHEN char_social_search_cases_work.working is null THEN ' '
                                                   WHEN char_social_search_cases_work.working = 1 THEN char_work_status_i18n.name
                                                   WHEN char_social_search_cases_work.working = 2 THEN ' '
                                                   END
                                             AS work_status");

        $query->leftjoin('char_work_wages',function($join) use ($language_id) {
            $join->on('char_social_search_cases_work.work_wage_id', '=', 'char_work_wages.id');
        })->selectRaw("CASE
                                                       WHEN char_social_search_cases_work.working is null THEN ' '
                                                       WHEN char_social_search_cases_work.working = 1 THEN char_work_wages.name
                                                       WHEN char_social_search_cases_work.working = 2 THEN ' '
                                                 END
                                                 AS work_wage");

        $query ->leftjoin('char_work_reasons_i18n', function($join) use ($language_id)  {
            $join->on('char_social_search_cases_work.work_reason_id', '=', 'char_work_reasons_i18n.work_reason_id')
                ->where('char_work_reasons_i18n.language_id', $language_id);
        })->selectRaw("CASE
                                           WHEN char_social_search_cases_work.can_work is null THEN ' '
                                           WHEN char_social_search_cases_work.can_work = 1 THEN ' '
                                           WHEN char_social_search_cases_work.can_work = 2 THEN char_work_reasons_i18n.name
                                       END AS work_reason");


        $query->leftjoin('char_social_search_cases_residence', 'char_social_search_cases.id', '=','char_social_search_cases_residence.search_cases_id');

        $query ->leftjoin('char_roof_materials_i18n', function($join) use ($language_id){
            $join->on('char_roof_materials_i18n.roof_material_id', '=', 'char_social_search_cases_residence.roof_material_id')
                ->where('char_roof_materials_i18n.language_id', $language_id);
        })->selectRaw("CASE WHEN char_roof_materials_i18n.name is null THEN ' ' Else char_roof_materials_i18n.name END   AS roof_materials");

        $query->leftjoin('char_property_types_i18n', function ($join) use ($language_id){
            $join->on('char_property_types_i18n.property_type_id', '=', 'char_social_search_cases_residence.property_type_id')
                ->where('char_property_types_i18n.language_id', $language_id);
        })->selectRaw("CASE WHEN char_property_types_i18n.name is null THEN ' ' Else char_property_types_i18n.name END   AS property_types");

        $query->leftjoin('char_building_status_i18n', function($join) use ($language_id){
            $join->on('char_building_status_i18n.building_status_id', '=', 'char_social_search_cases_residence.house_condition')
                ->where('char_building_status_i18n.language_id', $language_id);
        })->selectRaw("CASE WHEN char_building_status_i18n.name is null THEN ' ' Else char_building_status_i18n.name  END  AS house_condition");

        $query->leftjoin('char_house_status_i18n', function($join)  use ($language_id){
            $join->on('char_house_status_i18n.house_status_id', '=', 'char_social_search_cases_residence.residence_condition')
                ->where('char_house_status_i18n.language_id', $language_id);
        })->selectRaw("CASE WHEN char_house_status_i18n.name is null     THEN ' ' Else char_house_status_i18n.name  END  AS residence_condition");

        $query->leftjoin('char_furniture_status_i18n', function($join)  use ($language_id){
            $join->on('char_furniture_status_i18n.furniture_status_id', '=', 'char_social_search_cases_residence.indoor_condition')
                ->where('char_furniture_status_i18n.language_id', $language_id);
        })->selectRaw("CASE WHEN char_furniture_status_i18n.name is null THEN ' ' Else char_furniture_status_i18n.name  END  AS indoor_condition");

        $query ->leftjoin('char_habitable_status_i18n', function($join)  use ($language_id){
            $join->on('char_habitable_status_i18n.habitable_status_id', '=', 'char_social_search_cases_residence.habitable')
                ->where('char_habitable_status_i18n.language_id', $language_id);
        })->selectRaw("CASE WHEN char_habitable_status_i18n.name is null THEN ' ' Else char_habitable_status_i18n.name  END  AS habitable");

        $query ->selectRaw("CASE WHEN char_social_search_cases_residence.rooms is null THEN ' ' Else char_social_search_cases_residence.rooms  END  AS rooms");
        $query->selectRaw("CASE WHEN char_social_search_cases_residence.area is null THEN ' ' Else char_social_search_cases_residence.area  END  AS area");
        $query->selectRaw("CASE WHEN char_social_search_cases_residence.rent_value is null       THEN ' ' 
                                                Else char_social_search_cases_residence.rent_value  END  AS rent_value ,
                                             CASE WHEN char_social_search_cases_residence.repair_notes is null       THEN ' ' 
                                                Else char_social_search_cases_residence.repair_notes  END  AS repair_notes ,
                                            CASE WHEN char_social_search_cases_residence.need_repair is null THEN ' '
                                                WHEN char_social_search_cases_residence.need_repair = 1 THEN '$yes'
                                                WHEN char_social_search_cases_residence.need_repair = 2 THEN '$no'
                                                END
                                            AS need_repair,
                                          CASE WHEN char_social_search_cases_residence.need_repair = 1 THEN '*' Else ' ' END AS repair,         
                                          CASE WHEN char_social_search_cases_residence.need_repair = 2 THEN '*' Else ' ' END AS not_repair
                                   ");

        $query->leftjoin('char_social_search_cases_health', 'char_social_search_cases.id', '=','char_social_search_cases_health.search_cases_id');

        $query->leftjoin('char_diseases_i18n', function ($q)  use ($language_id){
            $q->on('char_diseases_i18n.disease_id', '=', 'char_social_search_cases_health.disease_id');
            $q->where('char_diseases_i18n.language_id', $language_id);
        })->selectRaw("CASE WHEN char_diseases_i18n.name is null THEN ' ' Else char_diseases_i18n.name END   AS diseases_name");

        $query->selectRaw("CASE WHEN char_social_search_cases_health.condition is null THEN ' '
                                                  WHEN char_social_search_cases_health.condition = 1 THEN '$perfect'
                                                  WHEN char_social_search_cases_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                  WHEN char_social_search_cases_health.condition = 3 THEN '$special_needs'
                                           END AS health_status,
                                          CASE WHEN char_social_search_cases_health.condition = 1 THEN '*' Else ' ' END AS good_health_status,
                                          CASE WHEN char_social_search_cases_health.condition = 2 THEN '*' Else ' ' END AS chronic_disease,
                                          CASE WHEN char_social_search_cases_health.condition = 3 THEN '*' Else ' ' END AS disabled,
                                          CASE WHEN char_social_search_cases_health.condition = 1 THEN 'X' Else ' ' END AS has_good_health,
                                          CASE WHEN char_social_search_cases_health.condition is null THEN ' '
                                                  WHEN char_social_search_cases_health.condition = 1 THEN ' '
                                                  WHEN char_social_search_cases_health.condition = 2 THEN 'X'
                                                  WHEN char_social_search_cases_health.condition = 3 THEN 'X'
                                           END AS has_disease,
                                          CASE WHEN char_social_search_cases_health.details is null THEN ' ' Else char_social_search_cases_health.details END   AS health_details,
                                          CASE
                                                  WHEN char_social_search_cases_health.has_health_insurance is null THEN ' '
                                                  WHEN char_social_search_cases_health.has_health_insurance = 1 THEN '$yes'
                                                  WHEN char_social_search_cases_health.has_health_insurance = 0 THEN '$no'
                                          END  AS has_health_insurance,
                                          CASE WHEN char_social_search_cases_health.has_health_insurance = 1 THEN '*' Else ' ' END AS have_health_insurance,         
                                          CASE WHEN char_social_search_cases_health.has_health_insurance = 0 THEN '*' Else ' ' END AS not_have_health_insurance,
                                 
                                          CASE WHEN char_social_search_cases_health.health_insurance_number is null THEN ' ' Else char_social_search_cases_health.health_insurance_number END   AS health_insurance_number,
                                          CASE WHEN char_social_search_cases_health.health_insurance_type is null THEN ' ' Else char_social_search_cases_health.health_insurance_type END   AS health_insurance_type,
                                                     CASE
                                                          WHEN char_social_search_cases_health.has_device is null THEN ' '
                                                          WHEN char_social_search_cases_health.has_device = 1 THEN '$yes'
                                                          WHEN char_social_search_cases_health.has_device = 0 THEN '$no'
                                                     END
                                                     AS has_device,
                                          CASE WHEN char_social_search_cases_health.has_device = 1 THEN '*' Else ' ' END AS use_device,         
                                          CASE WHEN char_social_search_cases_health.has_device = 0 THEN '*' Else ' ' END AS not_use_device,
                                          CASE WHEN char_social_search_cases_health.used_device_name is null THEN ' ' Else char_social_search_cases_health.used_device_name END   AS used_device_name
                                                    ");

        $return =$query->where('char_social_search_cases.id', '=', $id)->first();

        if($return){
            $return->family_members= CasesFamily::getList($id);
            $return->banks= CasesBanks::getList($id);
            $return->home_indoor= CasesEssentials::getList($id);
            $return->properties= CasesProperties::getList($id);
            $return->financial_aid_source= CasesAids::getList($id,1);
            $return->non_financial_aid_source= CasesAids::getList($id,2);

        }

        return $return;
    }

    public static function isUnique($id_card_number,$search_id)
    {

        $entry = self::query()
            ->where(function ($q) use ($id_card_number,$search_id) {
                $q->where('id_card_number', '=', $id_card_number);
                $q->where('search_id', '=', $search_id);
            })
            ->first();

        return (is_null($entry))? ['status' => false] : array('status' => true , 'row' => $entry);
    }

    public static function constantId($name,$value,$isChild,$parent){

        if(!is_null($value) && $value != ''){
            switch ($name) {
                case 'payment_category'  : {
                    $object= \Setting\Model\PaymentCategoryI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->payment_category_id;
                }
                    break;
                case 'birth_place'  : {
                    $object= \Setting\Model\LocationI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->location_id;
                }
                    break;
                case 'nationality'  :{
                    $object= \Setting\Model\LocationI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->location_id;
                }
                    break;
                case 'marital_status_id'  : {
                    $object= \Setting\Model\MaritalStatusI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->marital_status_id;
                }
                    break;

                case 'bank_id'  : {
                    $object= \Setting\Model\Bank::where(['name'=>$value ])->first();
                    return is_null($object)? null : (int) $object->id;
                }
                    break;
                case 'branch_name'  : {
                    if(!is_null($parent) && $parent !=' '){
                        return \Setting\Model\Branch::getBranchId($parent,$name);
                    }
                    return null;
                }
                    break;
                case 'work_reason_id'  : {
                    $object= \Setting\Model\Work\ReasonI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->work_reason_id;
                }
                    break;
                case 'work_status_id'  :{
                    $object= \Setting\Model\Work\StatusI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->work_status_id;
                }
                    break;
                case 'work_wage_id'  : {
                    $object= \Setting\Model\Work\Wage::where(['name'=>$value])->first();
                    return is_null($object)? null : (int) $object->work_wage_id;
                }
                    break;
                case 'work_job_id'  : {
                    $object= \Setting\Model\Work\JobI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->work_job_id;
                }
                    break;
                case 'death_cause_id'  : {
                    $object= \Setting\Model\DeathCauseI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->death_cause_id;
                }
                    break;
                case 'property_type_id'  : {
                    $object= \Setting\Model\PropertyTypeI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->property_type_id;
                }
                    break;
                case 'roof_material_id'  : {
                    $object= \Setting\Model\RoofMaterialI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->roof_material_id;
                }
                    break;
                case 'residence_condition'  : {
                    $object= \Setting\Model\HouseStatusI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->house_status_id;
                }
                    break;
                case 'indoor_condition'  : {
                    $object= \Setting\Model\FurnitureStatusI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->furniture_status_id;
                } break;
                case 'habitable'  : {
                    $object= \Setting\Model\HabitableStatusI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->habitable_status_id;
                } break;
                case 'house_condition'  : {
                    $object= \Setting\Model\BuildingStatusI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->building_status_id;
                } break;
                case 'disease_id'  : {
                    $object= \Setting\Model\DiseaseI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->disease_id;
                }break;
                case 'authority'  : {
                    $object= \Setting\Model\Education\AuthorityI18n::where(['name'=>$value])->first();
                    return is_null($object)? null : (int) $object->edu_authority_id;
                }break;
                case 'stage'  : {
                    $object= \Setting\Model\Education\StageI18n::where(['name'=>$value])->first();
                    return is_null($object)? null : (int) $object->edu_stage_id;
                }break;
                case 'degree'  : {
                    $object= \Setting\Model\Education\DegreeI18n::where(['name'=>$value])->first();
                    return is_null($object)? null : (int) $object->edu_degree_id;
                }break;
                case 'kinship_id'  : {
                    $object= \Setting\Model\KinshipI18n::where(['name'=>$value])->first();
                    return is_null($object)? null : (int) $object->kinship_id;
                }break;
                case 'country_id'  : {
                    $object= \Setting\Model\aidsLocationI18n::where(['name'=>$value , 'language_id'=>1])->first();
                    return is_null($object)? null : (int) $object->location_id;
                }
                    break;
                case 'district_id'  : {
                    if(!is_null($parent) && $parent !=' '){
                        return \Setting\Model\aidsLocationI18n::getLocationId($parent,$value);
                    }
                    return null;
                }
                    break;
                case 'region_id'  : {
                    if(!is_null($parent) && $parent !=' '){
                        return \Setting\Model\aidsLocationI18n::getLocationId($parent,$value);
                    }
                    return null;
                }
                    break;
                case 'neighborhood_id'  : {
                    if(!is_null($parent) && $parent !=' '){
                        return \Setting\Model\aidsLocationI18n::getLocationId($parent,$value);
                    }
                    return null;
                }
                    break;
                case 'square_id'  : {
                    if(!is_null($parent) && $parent !=' '){
                        return \Setting\Model\aidsLocationI18n::getLocationId($parent,$value);
                    }
                    return null;
                }
                    break;
                case 'mosque_id'  : {
                    if(!is_null($parent) && $parent !=' '){
                        return \Setting\Model\aidsLocationI18n::getLocationId($parent,$value);
                    }
                    return null;
                }
                    break;
            }
        }
        return null;

    }

    public static function filterInputs($inputs){

        $data = [];
        foreach ($inputs as $k=>$v) {
            if(!is_null($inputs[$k]) && $inputs[$k] != " "){
                $data[$k] = $v;
            }
        }

        $dates = ['birthday','visited_at','death_date'];
        $constant =  ['birth_place', 'nationality', 'marital_status_id','death_cause_id',
            'country_id', 'district_id', 'city', 'location_id','kinship_id','mosque_id',
            'bank_id', 'branch_name','disease_id','authority', 'stage','degree',
            'work_reason_id', 'work_status_id', 'work_wage_id', 'work_job_id',
            'property_type_id', 'roof_material_id',
            'residence_condition', 'indoor_condition', 'habitable', 'house_condition',"country_id",
            "district_id", "neighborhood_id", "region_id", "square_id", "mosque_id"];
        $hasParent =  ['district_id', 'city', 'location_id', 'branch_name',
                     'mosque_id',"district_id", "neighborhood_id", "region_id", "square_id"];
        $Parents = [
            'branch_name' => 'bank_id' ,
            "district_id" =>'country_id',
            "region_id" =>'district_id',
            "neighborhood_id" =>'region_id',
            "square_id" =>'neighborhood_id',
            "mosque_id" =>'square_id'];

        $ParentsNames = [
            'location_id' => 'almdyn',
            'branch_name' => 'asm_albnk',
            "district_id" =>'aldol',
            "region_id" =>'almhafth',
            "neighborhood_id" =>'almntk',
            "square_id" =>'alhy',
            "mosque_id" =>'almrbaa'];

        $numeric =  ['id_card_number', 'spouses', 'unrwa_card_number',
            'primary_mobile', 'watanya', 'secondary_mobile', 'phone',
            'monthly_income', 'rooms','area','rent_value',"points" ,"quran_parts","quran_chapters",
        ];

        $row=[];
        $translator =self::$_translator;
        $row['errors']=[];

        foreach ($data as $k=>$v) {
            $key=null;
            if(isset($translator[$k])){
                $key=$translator[$k];
            }

            if($k == 'square_id'){
                return $v ;
            }
            if(!is_null($key)){

                if(in_array($key,$numeric)){
                    if(!is_null($v) && $v != " "){
                        if(!is_numeric($v)){
                            $row['errors'][$key]=trans('common::application.invalid_number');
                        }
                    }
                }

                if(in_array($key,$dates)){
                    if(!is_null($v) && $v != " "){
                        $date_ = Helpers::getFormatedDate($v);
                        if (!is_null($date_)) {
                            $row[$key]=$date_;
                        }else{
                            $row['errors'][$key]=trans('common::application.invalid_date');
                        }
                    }
                }
                elseif($key =='gender'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'ذكر') ? 1 : 2;
                    }
                }elseif($key =='deserted'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 2;
                    }
                }elseif($key =='has_device'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 2;
                    }
                }elseif($key =='has_health_insurance'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 2;
                    }
                }elseif($key =='need_repair'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? '1' : '2';
                    }
                }elseif($key =='receivables'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 0;
                    }
                }elseif($key =='has_other_work_resources'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 0;
                    }
                }elseif($key =='card_type'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        if ($vlu == 'بطاقة تعريف'){
                            $row[$key] = 2;
                        }elseif ($vlu == 'جواز سفر'){
                            $row[$key] = 3;
                        }else{
                            $row[$key] = 1;
                        }
                    }
                }
                elseif($key =='type'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'تقني') ? 2 : 1;
                    }
                }
                elseif($key =='level'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){

                        if($vlu == 'ممتاز'){
                            $row[$key] = 1;
                        }elseif ($vlu == 'جيد جدا'){
                            $row[$key] = 2;
                        }elseif ($vlu == 'جيد'){
                            $row[$key] = 3;
                        }elseif ($vlu == 'مقبول'){
                            $row[$key] = 4;
                        }elseif ($vlu == 'ضعيف'){
                            $row[$key] = 5;
                        }
                    }
                }elseif($key =='visitor_evaluation'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){

                        if($vlu == 'ممتاز'){
                            $row[$key] = 4;
                        }elseif ($vlu == 'جيد جدا'){
                            $row[$key] = 3;
                        }elseif ($vlu == 'جيد'){
                            $row[$key] = 2;
                        }elseif ($vlu == 'سيء'){
                            $row[$key] = 1;
                        }elseif ($vlu == 'سيء جدا'){
                            $row[$key] = 0;
                        }
                    }
                }
                elseif($key =='grade'){
                    $vlu = trim($v);
                    if(!is_null($vlu) && $vlu != " "){
                        if($vlu == 'اول ابتدائي'){ $row[$key] = 1;}
                        elseif($vlu == 'ثاني ابتدائي'){$row[$key] = 2;}
                        elseif ($vlu == 'ثالث ابتدئي'){$row[$key] = 3;}
                        elseif ($vlu == 'رابع ابتدئي'){$row[$key] = 4;}
                        elseif ($vlu == 'خامس ابتدئي'){$row[$key] = 5;}
                        elseif ($vlu == 'سادس ابتدئي'){$row[$key] = 6;}
                        elseif ($vlu == 'السابع'){$row[$key] = 7;}
                        elseif ($vlu == 'الثامن'){$row[$key] = 8;}
                        elseif ($vlu == 'التاسع'){$row[$key] = 9;}
                        elseif ($vlu == 'العاشر'){$row[$key] = 10;}
                        elseif ($vlu == 'الحادي عشر'){$row[$key] = 11;}
                        elseif ($vlu == 'الثاني عشر'){$row[$key] = 12;}
                        elseif ($vlu == 'دراسات خاصة'){$row[$key] = 13;}
                        elseif ($vlu == 'تدريب مهنى'){$row[$key] = 14;}
                        elseif ($vlu == 'سنة أولى جامعة'){$row[$key] = 15;}
                        elseif ($vlu == 'سنة ثانية جامعة'){$row[$key] = 16;}
                        elseif ($vlu == 'سنة ثالثة جامعة'){$row[$key] = 17;}
                        elseif ($vlu == 'سنة رابعة جامعة'){$row[$key] = 18;}
                        elseif ($vlu == 'سنة خامسة جامعة'){$row[$key] = 19;}
                    }
                }
                elseif($key =='condition'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){

                        if ($vlu == 'مرض مزمن'){
                            $row[$key] = 2;
                        }elseif ($vlu == 'ذوي احتياجات خاصة'){
                            $row[$key] = 3;
                        }else{
                            $row[$key] = 1;
                        }
                    }
                }
                elseif($key =='refugee'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'مواطن') ? 1 : 2;
                    }
                }
                elseif($key =='working'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 2;
                    }
                }
                elseif($key =='mother_is_guardian'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 2;
                    }
                }
                elseif($key =='study'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 0;
                    }
                }elseif($key =='receivables'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 0;
                    }
                }elseif($key =='is_qualified'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 1 : 0;
                    }
                }
                elseif($key =='can_work'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 2 : 1;
                    }
                }
                elseif($key =='promised'){
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != " "){
                        $row[$key] = ($vlu == 'نعم') ? 2 : 1;
                    }
                }
                elseif(in_array($key,$constant)){
                    $parent =null;
                    $isChild =false;
                    if(in_array($key,$hasParent)){
                        $isChild =true;
                        $parentKey=$Parents[$key];
                        $parentName=$ParentsNames[$key];
                        if(isset($row[$parentKey])){
                            $parent=$row[$parentKey];
                        }
                    }
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != '') {
                        $cons_val = self::constantId($key,$vlu,$isChild,$parent);
                        if(!is_null($cons_val)) {
                            $row[$key] = $cons_val;
                        }else{
                            $row['errors'][$key]=trans('common::application.invalid_constant');
                        }
                    }
                }
                elseif(in_array($key,$numeric)){
                    if(!is_null($v) && $v != " "){
                        if(is_numeric($v)){
                            $row[$key]=(int)$v;
                        }
                    }
                }else{
                    if(!is_null($v) && $v != " "){
                        $row[$key]=$v;
                    }
                }
            }

        }

        $row['outSource']=true;
        if(isset($row['bank_id']) && isset($row['account_number'])&& isset($row['branch_name'])&& isset($row['branch_name'])){
            $row['banks'][]=[
                'bank_id' => $row['bank_id'],
                'account_owner' => $row['account_owner'],
                'account_number' => $row['account_number'],
                'branch_name' => $row['branch_name'],
                'check' => true
            ];

        }

        return $row;
    }

    public static function saveCase($input = array())
    {

        $case_Input = ["search_id","id_card_number","card_type", "first_name" ,
            "second_name","third_name","last_name","prev_family_name","gender",
            "marital_status_id", "deserted", "birthday","birth_place","death_date"   ,
            "death_cause_id","nationality" ,"family_cnt","spouses","male_live" ,
            "female_live",
//                       "refugee","unrwa_card_number","is_qualified", "receivables","receivables_sk_amount",
//                        "qualified_card_number","monthly_income","actual_monthly_income",
            "phone","primary_mobile" ,
            "secondary_mobile","watanya", "country_id","district_id","region_id","neighborhood_id","square_id","mosque_id" ,
            "street_address",
//            "needs"  ,"promised", "reconstructions_organization_name","visitor","visitor_card", "visited_at",
//                        "visitor_evaluation", "notes","visitor_opinion",
            "status","reason"
        ];

        $IntegerInput =  ['habitable','house_condition','rooms','area','rent_value' ,'receivables',
            'card_type','is_qualified','qualified_card_number',
            'indoor_condition','residence_condition','roof_material_id','property_type_id',
            'kinship_id','stage','condition','health_condition','marital_status_id',
            'WhoIsGuardian','can_work','working','nationality',
            'gender',
            'birth_place', 'marital_status_id' , 'refugee','alive','spouses','health_disease_id',
            'deserted','family_cnt','visitor_card', 'visitor_evaluation'
        ];

        $insert=[];
        foreach ($case_Input as $var) {
            if(isset($input[$var])){
                if($input[$var] != null && $input[$var] != "") {
                    if(in_array($var, $IntegerInput)){
                        $insert[$var] =(int )$input[$var];
                    }else{
                        if ($var == 'birthday' || $var == 'death_date') {
                            $insert[$var] = date('Y-m-d', strtotime($input[$var]));
                        }else{
                            $insert[$var] =$input[$var];
                        }
                    }
                }else{
                    $insert[$var] = null;
                }
            }
        }

        $return=array();
        $case=null;

        \DB::beginTransaction();

        if(sizeof($insert) > 0){
            $case = new Cases();
            foreach ($insert as $key => $value) {
                $case->$key = $value;
            }
            $case->save();
        }

        if ($case) {
            $return['id']= $case->id;
            $return['id_card_number']= $case->id_card_number;
            $return['full_name']= $case->first_name . ' ' .$case->second_name . ' ' .$case->third_name . ' ' .$case->last_name . ' ' ;

            $search_cases_id = $case->id;

            if(isset($input['working']) || isset($input['can_work']) ||isset($input['work_job_id']) || isset($input['work_status_id']) ||
                isset($input['work_wage_id']) || isset($input['work_location']) || isset($input['work_reason_id'])|| isset($input['has_other_work_resources'])) {

                $worksInput=['working','can_work','work_job_id','work_status_id','work_wage_id','work_reason_id','work_location'];
                $CreateWork = [];
                if (isset($input['working']) || isset($input['can_work'])) {
                    foreach ($input as $key => $value) {
                        if(in_array($key, $worksInput) &&  !(!$value || $value == "")) {
                            if($key == 'work_location'){
                                $CreateWork[$key] = $value;
                            }else{
                                $CreateWork[$key] = (int)$value;
                            }
                        }
                    }

                    $CreateWork['has_other_work_resources'] = 0;
                    if (isset($input['has_other_work_resources'])) {
                        if (($input['has_other_work_resources'] != null && $input['has_other_work_resources'] != "")) {
                            $CreateWork['has_other_work_resources'] = $input['has_other_work_resources'];
                        }
                    }

                    if (isset($CreateWork['working'])){
                        if($CreateWork['working'] !=1){
                            $CreateWork['work_job_id']=$CreateWork['work_status_id']=$CreateWork['work_wage_id']=$CreateWork['work_location']=null;
                        }
                    }
                    if ( isset($CreateWork['can_work'])){
                        if($CreateWork['can_work'] !=1){
                            $CreateWork['work_reason_id']=null;
                        }
                    }

                    if (sizeof($CreateWork) != 0) {
                        CasesWork::updateOrCreate(['search_cases_id' => $search_cases_id],$CreateWork);
                    }
                }

            }

            if (isset($input['property_type_id']) || isset($input['roof_material_id']) || isset($input['residence_condition']) ||
                isset($input['indoor_condition']) || isset($input['habitable']) || isset($input['house_condition']) || isset($input['rooms']) ||
                isset($input['area']) || isset($input['rent_value']) || isset($input['need_repair']) || isset($input['repair_notes'])) {

                $residenceInput=['property_type_id','roof_material_id','residence_condition','need_repair',
                    'repair_notes','indoor_condition','habitable','house_condition','rooms','area','rent_value'];
                $residenceInputInt=['property_type_id','roof_material_id','residence_condition','indoor_condition',
                    'habitable','house_condition','rooms','rent_value','need_repair'];
                $CreateResidence = [];

                foreach ($residenceInput as $var) {
                    if(isset($input[$var])){
                        if($input[$var] != null && $input[$var] != "") {
                            if(!in_array($var,$residenceInputInt)){
                                $CreateResidence[$var] = $input[$var];
                            }else{
                                $CreateResidence[$var] = (int)$input[$var];
                            }
                        }else{
                            $CreateResidence[$var] = null;
                        }
                    }
                }
                if (sizeof($CreateResidence) != 0) {
                    CasesResidence::saveResidence($search_cases_id,$CreateResidence);
                }
            }


            if (isset($input['refugee']) || isset($input['unrwa_card_number']) || isset($input['is_qualified']) ||
                isset($input['receivables']) || isset($input['receivables_sk_amount']) || isset($input['qualified_card_number']) ||
                isset($input['monthly_income']) || isset($input['actual_monthly_income']) || isset($input['needs']) ||
                isset($input['promised']) || isset($input['reconstructions_organization_name']) || isset($input['visitor']) ||
                isset($input['visitor_card']) || isset($input['visited_at']) || isset($input['visitor_evaluation'])||
                isset($input['visitor_opinion']) || isset($input['notes'])) {

                $detailsInput=["refugee","unrwa_card_number",
                    "is_qualified", "receivables","receivables_sk_amount",
                    "qualified_card_number","monthly_income","actual_monthly_income",
                    "needs"  ,"promised", "reconstructions_organization_name","visitor","visitor_card", "visited_at",
                    "visitor_evaluation", "notes","visitor_opinion"]
                ;
                $detailsInputInt=["refugee","unrwa_card_number", "is_qualified", "receivables",
                    "qualified_card_number","monthly_income","actual_monthly_income",
                    "promised","visitor_card", "visitor_evaluation"];
                $CreateDetails = [];

                foreach ($detailsInput as $var) {
                    if(isset($input[$var])){
                        if($input[$var] != null && $input[$var] != "") {
                            if(!in_array($var,$detailsInputInt)){
                                $CreateDetails[$var] = $input[$var];
                            }else{
                                $CreateDetails[$var] = (int)$input[$var];
                            }
                        }else{
                            $CreateDetails[$var] = null;
                        }
                    }
                }
                if (sizeof($CreateDetails) != 0) {
                    CasesDetails::saveDetails($search_cases_id,$CreateDetails);
                }
            }

            if (isset($input['condition']) || isset($input['details']) || isset($input['disease_id'])  ||
                isset($input['has_health_insurance']) || isset($input['health_insurance_number']) ||
                isset($input['health_insurance_type']) || isset($input['has_device'])|| isset($input['used_device_name'])) {

                $CreateHealth = [];

                if (isset($input['has_device'])) {
                    if (($input['has_device'] != null && $input['has_device'] != "")) {
                        $CreateHealth['has_device'] = $input['has_device'];

                        if ($CreateHealth['has_device'] == 0) {
                            $CreateHealth['used_device_name'] =  null;
                        } else {

                            if (isset($input['used_device_name'])) {
                                if (($input['used_device_name'] != null && $input['used_device_name'] != "")) {
                                    $CreateHealth['used_device_name'] = $input['used_device_name'];
                                }
                            }
                        }
                    }
                }

                if (isset($input['has_health_insurance'])) {
                    if (($input['has_health_insurance'] != null && $input['has_health_insurance'] != "")) {
                        $CreateHealth['has_health_insurance'] = $input['has_health_insurance'];

                        if ($CreateHealth['has_health_insurance'] == 0) {
                            $CreateHealth['health_insurance_number'] =  null;
                            $CreateHealth['health_insurance_type'] = null;

                        } else {

                            if (isset($input['health_insurance_number'])) {
                                if (($input['health_insurance_number'] != null && $input['health_insurance_number'] != "")) {
                                    $CreateHealth['health_insurance_number'] = $input['health_insurance_number'];
                                }
                            }
                            if (isset($input['health_insurance_type'])) {
                                if (($input['health_insurance_type'] != null && $input['health_insurance_type'] != "")) {
                                    $CreateHealth['health_insurance_type'] = $input['health_insurance_type'];
                                }
                            }

                        }
                    }
                }

                if (isset($input['condition'])) {
                    if (($input['condition'] != null && $input['condition'] != "")) {
                        $CreateHealth['condition'] = $input['condition'];

                        if ($CreateHealth['condition'] == 1) {
                            $CreateHealth['disease_id'] =  null;
                            $CreateHealth['details'] = null;
                        } else {

                            if (isset($input['details'])) {
                                if (($input['details'] != null && $input['details'] != "")) {
                                    $CreateHealth['details'] = $input['details'];
                                }
                            }
                            if (isset($input['disease_id'])) {
                                if (($input['disease_id'] != null && $input['disease_id'] != "")) {
                                    $CreateHealth['disease_id'] = $input['disease_id'];
                                }
                            }
                        }

                    }
                }

                if (sizeof($CreateHealth) != 0) {
                    CasesHealth::updateOrCreate(['search_cases_id' => $search_cases_id],$CreateHealth);
                }

            }

        }
        \DB::commit();
        return $return;

    }

    public static function refreshGroup($id)
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $result = \DB::table('char_social_search_cases')
                     ->join('char_social_search', 'char_social_search.id', '=', 'char_social_search_cases.search_id')
                     ->leftjoin('char_social_search_cases_details', 'char_social_search_cases_details.search_cases_id', '=', 'char_social_search_cases.id')
                     ->leftjoin('char_social_search_cases_health','char_social_search_cases_health.search_cases_id', '=', 'char_social_search_cases.id')
                     ->leftjoin('char_social_search_cases_residence', 'char_social_search_cases_residence.search_cases_id', '=', 'char_social_search_cases.id')
                     ->leftjoin('char_social_search_cases_work', 'char_social_search_cases_work.search_cases_id', '=', 'char_social_search_cases.id')
                     ->where('char_social_search_cases.search_id',$id)
                     ->selectRaw("char_social_search_cases.*,
                                          char_social_search_cases.id as search_cases_id,
                                          char_social_search_cases.country_id as adscountry_id,
                                          char_social_search_cases.district_id as adsdistrict_id,
                                          char_social_search_cases.region_id as adsregion_id,
                                          char_social_search_cases.neighborhood_id as adsneighborhood_id,
                                          char_social_search_cases.square_id as adssquare_id,
                                          char_social_search_cases.mosque_id as adsmosques_id,
                                          char_social_search_cases_details.*,
                                          char_social_search_cases.watanya as wataniya,
                                          char_social_search_cases.mosque_id as mosques_id,
                                          char_social_search_cases_health.*,
                                          char_social_search_cases_residence.*,
                                          char_social_search.*,
                                          char_social_search_cases_work.*")
                     ->get();

        foreach($result as $item) {

                $item->family_members= CasesFamily::where(['search_cases_id'=>$item->search_cases_id])->get();
                $item->banks= CasesBanks::where(['search_cases_id'=>$item->search_cases_id])->get();
                $item->essentials= CasesEssentials::where(['search_cases_id'=>$item->search_cases_id])->get();
                $item->properties= CasesProperties::where(['search_cases_id'=>$item->search_cases_id])->get();
                $item->aids_source= CasesAids::where(['search_cases_id'=>$item->search_cases_id])->get();
                Person::saveFromSocialSearch($item);
        }

        if(sizeof($result) > 0 ){
            return ['status' => 'success','msg'=>trans('common::application.The row is updated')];
        }
        return ['status' => 'failed','msg'=>trans('common::application.There is no update')];
    }

    public static function exportGroup($forms,$action)
    {

        $user = \Auth::user();
        $organization_id = $user->organization_id;

        $language_id =  \App\Http\Helpers::getLocale();

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $output=array();
        $all_organization=null;

        $active = trans('common::application.active');
        $inactive = trans('common::application.inactive');

        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $refugee = trans('common::application.refugee');
        $citizen = trans('common::application.citizen');

        $qualified = trans('common::application.qualified');
        $not_qualified = trans('common::application.not qualified');

        $perfect = trans('common::application.perfect');
        $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
        $special_needs = trans('common::application.special_needs');


        $result = \DB::table('char_social_search_cases')
                ->join('char_social_search', function($q) use ($language_id){
                    $q->on('char_social_search.id', '=', 'char_social_search_cases.search_id');
                    $q->whereNull('char_social_search.deleted_at');
                })
                ->join('char_organizations as org','org.id',  '=', 'char_social_search.organization_id')
                ->join('char_categories','char_categories.id',  '=', 'char_social_search.category_id')
                ->leftjoin('char_marital_status_i18n as char_marital_status', function($q) use ($language_id) {
                    $q->on('char_marital_status.marital_status_id', '=', 'char_social_search_cases.marital_status_id');
                    $q->where('char_marital_status.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As district_name', function($join) use ($language_id)  {
                    $join->on('char_social_search_cases.district_id', '=','district_name.location_id' )
                        ->where('district_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As region_name', function($join)  use ($language_id) {
                    $join->on('char_social_search_cases.region_id', '=','region_name.location_id' )
                        ->where('region_name.language_id',$language_id);
                })
                ->leftjoin('char_social_search_cases_details', 'char_social_search_cases_details.search_cases_id', '=', 'char_social_search_cases.id')
                ->leftjoin('char_aids_locations_i18n As country_name', function($join)  use ($language_id) {
                    $join->on('char_social_search_cases.country_id', '=','country_name.location_id' )
                        ->where('country_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)   {
                    $join->on('char_social_search_cases.neighborhood_id', '=','location_name.location_id' )
                        ->where('location_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As square_name', function($join)  use ($language_id) {
                    $join->on('char_social_search_cases.square_id', '=','square_name.location_id' )
                        ->where('square_name.language_id',$language_id);
                })
                ->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
                    $join->on('char_social_search_cases.mosque_id', '=','mosques_name.location_id' )
                        ->where('mosques_name.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L', function($q) use ($language_id){
                    $q->on('L.location_id', '=', 'char_social_search_cases.birth_place');
                    $q->where('L.language_id',$language_id);
                })
                ->leftjoin('char_locations_i18n As L0', function($q) use ($language_id){
                    $q->on('L0.location_id', '=', 'char_social_search_cases.nationality');
                    $q->where('L0.language_id',$language_id);
                })
                ->leftjoin('char_social_search_cases_health','char_social_search_cases_health.search_cases_id', '=', 'char_social_search_cases.id')
                ->leftjoin('char_diseases_i18n', function($q) use ($language_id){
                    $q->on('char_diseases_i18n.disease_id', '=', 'char_social_search_cases_health.disease_id');
                    $q->where('char_diseases_i18n.language_id',$language_id);
                })
                ->leftjoin('char_social_search_cases_residence', 'char_social_search_cases_residence.search_cases_id', '=', 'char_social_search_cases.id')
                ->leftjoin('char_property_types_i18n', function($q) use ($language_id){
                    $q->on('char_property_types_i18n.property_type_id', '=', 'char_social_search_cases_residence.property_type_id');
                    $q->where('char_property_types_i18n.language_id',$language_id);
                })
                ->leftjoin('char_roof_materials_i18n', function($q) use ($language_id){
                    $q->on('char_roof_materials_i18n.roof_material_id', '=', 'char_social_search_cases_residence.roof_material_id');
                    $q->where('char_roof_materials_i18n.language_id',$language_id);
                })
                ->leftjoin('char_building_status_i18n', function($q) use ($language_id){
                    $q->on('char_building_status_i18n.building_status_id', '=', 'char_social_search_cases_residence.house_condition');
                    $q->where('char_building_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_furniture_status_i18n', function($q) use ($language_id){
                    $q->on('char_furniture_status_i18n.furniture_status_id', '=', 'char_social_search_cases_residence.indoor_condition');
                    $q->where('char_furniture_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_habitable_status_i18n', function($q) use ($language_id){
                    $q->on('char_habitable_status_i18n.habitable_status_id', '=', 'char_social_search_cases_residence.habitable');
                    $q->where('char_habitable_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_house_status_i18n', function($q) use ($language_id){
                    $q->on('char_house_status_i18n.house_status_id', '=', 'char_social_search_cases_residence.residence_condition');
                    $q->where('char_house_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_social_search_cases_work', 'char_social_search_cases_work.search_cases_id', '=', 'char_social_search_cases.id')
                ->leftjoin('char_work_jobs_i18n as work_job', function($q) use ($language_id){
                    $q->on('work_job.work_job_id', '=', 'char_social_search_cases_work.work_job_id');
                    $q->where('work_job.language_id',$language_id);
                })
                ->leftjoin('char_work_wages as work_wage', function($q) use ($language_id){
                    $q->on('work_wage.id', '=', 'char_social_search_cases_work.work_wage_id');
                })
                ->leftjoin('char_work_status_i18n as work_status', function($q) use ($language_id){
                    $q->on('work_status.work_status_id', '=', 'char_social_search_cases_work.work_status_id');
                    $q->where('work_status.language_id',$language_id);
                })
                ->leftjoin('char_work_reasons_i18n as work_reason', function($q) use ($language_id){
                    $q->on('work_reason.work_reason_id', '=', 'char_social_search_cases_work.work_reason_id');
                    $q->where('work_reason.language_id',$language_id);
                })
                ->whereIn('char_social_search_cases.id',$forms)
                ->orderBy('char_social_search_cases.first_name',
                         'char_social_search_cases.second_name',
                         'char_social_search_cases.third_name',
                         'char_social_search_cases.last_name');

        if($action =='ExportToWord' || $action =='pdf'){
            $result= $result->selectRaw("char_social_search_cases.id")->get();
            foreach($result as $item) {
                $output[] = self::fetch($item->id);
            }
        }
        else if($action =='FamilyMember'){

            $result= $result->selectRaw("char_social_search_cases.id,
                              CONCAT(ifnull(char_social_search_cases.first_name, ' '), ' ' ,ifnull(char_social_search_cases.second_name, ' '),' ',ifnull(char_social_search_cases.third_name, ' '),' ', ifnull(char_social_search_cases.last_name,' ')) AS full_name,
                              CASE WHEN char_social_search_cases.id_card_number is null THEN ' ' Else char_social_search_cases.id_card_number  END  AS id_card_number,
                              CASE
                                           WHEN char_social_search_cases.card_type is null THEN ' '
                                           WHEN char_social_search_cases.card_type = 1 THEN '$id_card'
                                           WHEN char_social_search_cases.card_type = 2 THEN '$id_number'
                                           WHEN char_social_search_cases.card_type = 3 THEN '$passport'
                                     END
                                     AS card_type                            
                              ")->get();

           foreach($result as $item) {
                $members = CasesFamily::getList($item->id);
                if (sizeof($members) > 0) {
                    foreach ($members as $key => $member) {
                        $member->father_name = $item->full_name;
                        $member->father_id_card_number = $item->id_card_number;
                        array_push($output, $member);
                    }
                }
            }

            return $output;
        }
        else if($action =='ExportToExcel') {

            $result->selectRaw("char_social_search_cases.id,
                                         char_social_search_cases.id as person_id,
                                         char_social_search.title as search_title,char_social_search.date_from,
                                         char_social_search.date_to,
                                         CONCAT(ifnull(char_social_search_cases.first_name, ' '), ' ' ,ifnull(char_social_search_cases.second_name, ' '),' ',ifnull(char_social_search_cases.third_name, ' '),' ', ifnull(char_social_search_cases.last_name,' ')) AS name,
                                         CASE WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS category_name,
                                         org.name as organization_name,
                                         CASE WHEN char_social_search_cases.id_card_number is null THEN ' ' Else char_social_search_cases.id_card_number  END  AS id_card_number,
                                         CASE
                                                   WHEN char_social_search_cases.card_type is null THEN ' '
                                                   WHEN char_social_search_cases.card_type = 1 THEN '$id_card'
                                                   WHEN char_social_search_cases.card_type = 2 THEN '$id_number'
                                                   WHEN char_social_search_cases.card_type = 3 THEN '$passport'
                                             END
                                             AS card_type ,
                                         CASE WHEN char_social_search_cases.birthday is null THEN ' ' Else DATE_FORMAT(char_social_search_cases.birthday,'%Y/%m/%d')  END  AS birthday,
                                         CASE
                                                   WHEN char_social_search_cases.gender is null THEN ' '
                                                   WHEN char_social_search_cases.gender = 1 THEN '$male'
                                                   WHEN char_social_search_cases.gender = 2 THEN '$female'
                                         END  AS gender,
                                         CASE WHEN char_social_search_cases.marital_status_id is null THEN ' '
                                                  Else char_marital_status.name  END  
                                         AS marital_status_id,
                                         CASE WHEN char_social_search_cases.deserted is null THEN ' '
                                                 WHEN char_social_search_cases.deserted = 1 THEN '$yes'
                                                 WHEN char_social_search_cases.deserted = 0 THEN '$no'
                                             END
                                             AS deserted,
                                         CASE
                                               WHEN char_social_search_cases_details.refugee is null THEN ' '
                                               WHEN char_social_search_cases_details.refugee = 0 THEN ' '
                                               WHEN char_social_search_cases_details.refugee = 1 THEN ' $citizen '
                                               WHEN char_social_search_cases_details.refugee = 2 THEN '$refugee'
                                         END
                                         AS refugee,
                                         CASE
                                               WHEN char_social_search_cases_details.refugee is null THEN ' '
                                               WHEN char_social_search_cases_details.refugee = 0 THEN ' '
                                               WHEN char_social_search_cases_details.refugee = 1 THEN ' '
                                               WHEN char_social_search_cases_details.refugee = 2 THEN char_social_search_cases_details.unrwa_card_number
                                         END
                                         AS unrwa_card_number,
                                         CASE WHEN char_social_search_cases.birth_place is null THEN ' ' Else L.name END AS birth_place,
                                         CASE WHEN char_social_search_cases.nationality is null THEN ' ' Else L0.name END AS nationality,
                                               char_social_search_cases.family_cnt AS family_count,
                                               char_social_search_cases.spouses AS spouses,
                                               char_social_search_cases.male_live AS male_count,
                                               char_social_search_cases.female_live AS female_count ,
                                              
                                        CASE WHEN char_social_search_cases.country_id  is null THEN ' ' Else country_name.name END   AS country,
                                        CASE WHEN char_social_search_cases.district_id  is null THEN ' ' Else district_name.name END   AS governarate ,
                                        CASE WHEN char_social_search_cases.region_id  is null THEN ' ' Else region_name.name END   AS region_name,
                                        CASE WHEN char_social_search_cases.neighborhood_id is null THEN ' ' Else location_name.name END   AS nearLocation,
                                        CASE WHEN char_social_search_cases.square_id is null THEN ' ' Else square_name.name END   AS square_name,
                                        CASE WHEN char_social_search_cases.mosque_id is null THEN ' ' Else mosques_name.name END   AS mosque_name,
                                        CASE WHEN char_social_search_cases.street_address is null THEN ' ' Else char_social_search_cases.street_address END   AS street_address,
                                        CONCAT(ifnull(country_name.name,' '),' _ ',
                                                            ifnull(district_name.name,' '),'  ',
                                                            ifnull(region_name.name,' '),'  ',
                                                            ifnull(location_name.name,' '),'  ',
                                                            ifnull(square_name.name,' '),'  ',
                                                            ifnull(mosques_name.name,' '),' ',
                                                            ifnull(char_social_search_cases.street_address,' '))
                                            AS address,
                                        CASE WHEN char_social_search_cases.phone is null THEN ' ' Else char_social_search_cases.phone  END  AS phone,
                                        CASE WHEN char_social_search_cases.primary_mobile is null THEN ' ' Else char_social_search_cases.primary_mobile  END  AS primary_mobile,
                                        CASE WHEN char_social_search_cases.secondary_mobile is null THEN ' ' Else char_social_search_cases.secondary_mobile  END  AS secondary_mobile,
                                        CASE WHEN char_social_search_cases.watanya is null THEN ' ' Else char_social_search_cases.watanya  END  AS watanya ,
                                        CASE WHEN char_social_search_cases_details.receivables = 1 THEN '$yes' Else '$no' END AS receivables,
                                        CASE
                                              WHEN char_social_search_cases_details.receivables is null THEN '0'
                                              WHEN char_social_search_cases_details.receivables = 0 THEN '0'
                                              WHEN char_social_search_cases_details.receivables = 1 THEN char_social_search_cases_details.receivables_sk_amount
                                        END AS receivables_sk_amount ,
                                        CASE
                                                   WHEN char_social_search_cases_details.is_qualified is null THEN '-'
                                                   WHEN char_social_search_cases_details.is_qualified = 0 THEN '$not_qualified '
                                                   WHEN char_social_search_cases_details.is_qualified = 1 THEN ' $qualified '
                                             END
                                        AS is_qualified,
                                        CASE WHEN char_social_search_cases_details.qualified_card_number is null THEN '-' Else char_social_search_cases_details.qualified_card_number  END  AS qualified_card_number,
                                        CASE WHEN char_social_search_cases_details.monthly_income is null THEN ' ' Else char_social_search_cases_details.monthly_income  END  AS monthly_income,
                                        CASE WHEN char_social_search_cases_details.actual_monthly_income is null THEN ' ' Else char_social_search_cases_details.actual_monthly_income  END  AS actual_monthly_income,
                                        CASE
                                           WHEN char_social_search_cases_work.working is null THEN ' '
                                           WHEN char_social_search_cases_work.working = 1 THEN '$yes'
                                           WHEN char_social_search_cases_work.working = 2 THEN '$no'
                                         END
                                         AS working,
                                         CASE
                                               WHEN char_social_search_cases_work.working is null THEN ' '
                                               WHEN char_social_search_cases_work.working = 1 THEN work_status.name
                                               WHEN char_social_search_cases_work.working = 2 THEN ' '
                                         END
                                         AS work_status,
                                         CASE
                                               WHEN char_social_search_cases_work.working is null THEN ' '
                                               WHEN char_social_search_cases_work.working = 1 THEN work_job.name
                                               WHEN char_social_search_cases_work.working = 2 THEN ' '
                                         END
                                         AS work_job,
                                         CASE
                                               WHEN char_social_search_cases_work.working is null THEN ' '
                                               WHEN char_social_search_cases_work.working = 1 THEN work_wage.name
                                               WHEN char_social_search_cases_work.working = 2 THEN ' '
                                         END
                                         AS work_wage,
                                         CASE
                                               WHEN char_social_search_cases_work.working is null THEN ' '
                                               WHEN char_social_search_cases_work.working = 1 THEN char_social_search_cases_work.work_location
                                               WHEN char_social_search_cases_work.working = 2 THEN ' '
                                         END
                                         AS work_location,
                                         CASE
                                               WHEN char_social_search_cases_work.can_work is null THEN ' '
                                               WHEN char_social_search_cases_work.can_work = 1 THEN '$yes'
                                               WHEN char_social_search_cases_work.can_work = 2 THEN '$no'
                                         END
                                         AS can_work,
                                         CASE
                                               WHEN char_social_search_cases_work.can_work is null THEN ' '
                                               WHEN char_social_search_cases_work.can_work = 1 THEN ' '
                                               WHEN char_social_search_cases_work.can_work = 2 THEN work_reason.name
                                         END
                                         AS work_reason,
                                         CASE WHEN char_social_search_cases_work.has_other_work_resources is null THEN ' '
                                              WHEN char_social_search_cases_work.has_other_work_resources = 1 THEN '$yes'
                                              WHEN char_social_search_cases_work.has_other_work_resources = 0 THEN '$no'
                                         END AS has_other_work_resources,
                                         CASE WHEN char_building_status_i18n.name is null THEN ' ' Else char_building_status_i18n.name  END  AS house_condition,
                                         CASE WHEN char_furniture_status_i18n.name is null THEN ' ' Else char_furniture_status_i18n.name  END  AS indoor_condition,
                                         CASE WHEN char_habitable_status_i18n.name is null THEN ' ' Else char_habitable_status_i18n.name  END  AS habitable,
                                         CASE WHEN char_house_status_i18n.name is null     THEN ' ' Else char_house_status_i18n.name  END  AS residence_condition,
                                         CASE WHEN char_property_types_i18n.name is null   THEN ' ' Else char_property_types_i18n.name  END  AS property_types,
                                         CASE WHEN char_roof_materials_i18n.name is null   THEN ' ' Else char_roof_materials_i18n.name  END  AS roof_materials,
                                         CASE WHEN char_social_search_cases_residence.area is null             THEN ' ' Else char_social_search_cases_residence.area  END  AS area,
                                         CASE WHEN char_social_search_cases_residence.rooms is null            THEN ' ' Else char_social_search_cases_residence.rooms  END  AS rooms,
                                         CASE WHEN char_social_search_cases_residence.rent_value is null       THEN ' ' Else char_social_search_cases_residence.rent_value  END  AS rent_value,
                                         CASE WHEN char_social_search_cases_residence.need_repair is null THEN ' '
                                              WHEN char_social_search_cases_residence.need_repair = 1 THEN '$yes'
                                              WHEN char_social_search_cases_residence.need_repair = 2 THEN '$no'
                                            END AS need_repair ,
                                         CASE WHEN char_social_search_cases_residence.repair_notes is null   THEN ' ' Else char_social_search_cases_residence.repair_notes  END  AS repair_notes,
                                         CASE
                                                          WHEN char_social_search_cases_health.condition is null THEN ' '
                                                          WHEN char_social_search_cases_health.condition = 1 THEN '$perfect'
                                                          WHEN char_social_search_cases_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                          WHEN char_social_search_cases_health.condition = 3 THEN '$special_needs'
                                                     END
                                                     AS health_status,
                                                     CASE WHEN char_social_search_cases_health.details is null THEN ' ' Else char_social_search_cases_health.details END   AS details,
                                                     CASE WHEN char_diseases_i18n.name is null THEN ' ' Else char_diseases_i18n.name END   AS disease_id,
                                                     CASE
                                                          WHEN char_social_search_cases_health.has_health_insurance is null THEN ' '
                                                          WHEN char_social_search_cases_health.has_health_insurance = 1 THEN '$yes'
                                                          WHEN char_social_search_cases_health.has_health_insurance = 0 THEN '$no'
                                                     END
                                                     AS has_health_insurance,
                                                     CASE WHEN char_social_search_cases_health.health_insurance_number is null THEN ' ' Else char_social_search_cases_health.health_insurance_number END   AS health_insurance_number,
                                                     CASE WHEN char_social_search_cases_health.health_insurance_type is null THEN ' ' Else char_social_search_cases_health.health_insurance_type END   AS health_insurance_type,
                                                     CASE
                                                          WHEN char_social_search_cases_health.has_device is null THEN ' '
                                                          WHEN char_social_search_cases_health.has_device = 1 THEN '$yes'
                                                          WHEN char_social_search_cases_health.has_device = 0 THEN '$no'
                                                     END
                                                     AS has_device,
                                                     CASE WHEN char_social_search_cases_health.used_device_name is null THEN ' ' Else char_social_search_cases_health.used_device_name END   AS used_device_name,
                                         CASE
                                                WHEN char_social_search_cases_details.promised is null THEN ' '
                                                WHEN char_social_search_cases_details.promised = 1 THEN '$no'
                                                WHEN char_social_search_cases_details.promised = 2 THEN '$yes'
                                            END
                                          AS promised,
                                          CASE
                                              WHEN char_social_search_cases_details.promised = 1 THEN ' '
                                              WHEN char_social_search_cases_details.promised is null THEN ' '
                                              WHEN char_social_search_cases_details.promised = 2 THEN char_social_search_cases_details.reconstructions_organization_name
                                          END
                                          AS ReconstructionOrg,
                                          CASE WHEN char_social_search_cases_details.visited_at is null THEN ' ' Else char_social_search_cases_details.visited_at END AS visited_at,
                                          CASE WHEN char_social_search_cases_details.visitor is null THEN ' ' Else char_social_search_cases_details.visitor END AS visitor,
                                          CASE WHEN char_social_search_cases_details.notes is null THEN ' ' Else char_social_search_cases_details.notes END AS notes,
                                          CASE WHEN char_social_search_cases_details.visitor_evaluation is null THEN ' ' Else char_social_search_cases_details.visitor_evaluation END AS visitor_evaluation,
                                          CASE WHEN char_social_search_cases_details.visitor_opinion is null THEN ' ' Else char_social_search_cases_details.visitor_opinion END AS visitor_opinion,
                                          CASE WHEN char_social_search_cases_details.visitor_card is null THEN ' ' Else char_social_search_cases_details.visitor_card END AS visitor_card
                                   ");

            $result=$result->get();

            $aid_source= \DB::table('char_aid_sources')
                ->selectRaw(" char_aid_sources.name, char_aid_sources.id")
                ->orderBy('char_aid_sources.name')->get();

            $properties = \DB::table('char_properties_i18n')
                ->selectRaw(" char_properties_i18n.name, char_properties_i18n.property_id as id")
                ->where('char_properties_i18n.language_id', $language_id)
                ->orderBy('char_properties_i18n.name')->get();

            $essentials = \DB::table('char_essentials')
                ->selectRaw(" char_essentials.id as id,char_essentials.name")
                ->orderBy('char_essentials.name')->get();

            foreach($result as $item) {

                $financial_aid_source_= \DB::table('char_social_search_cases_aids')
                    ->where('char_social_search_cases_aids.aid_type', '=', 1)
                    ->where('char_social_search_cases_aids.search_cases_id', '=', $item->id)
                    ->selectRaw("aid_source_id ,CASE WHEN char_social_search_cases_aids.aid_value is null THEN '0' 
                                                                  WHEN char_social_search_cases_aids.aid_take = 0 THEN '0'
                                                                                   WHEN char_social_search_cases_aids.aid_value is null THEN '0'  
                                                                                     Else char_social_search_cases_aids.aid_value END 
                                                                                AS  aid_value
                                                         ")->get();

                $data_ = [];
                $item->financial_aid_source_elements = [];
                foreach($financial_aid_source_ as $kk=>$i) {
                    if(!isset($data_[$i->aid_source_id])){
                        $item->financial_aid_source_elements[]=$i->aid_source_id;
                        $data_[$i->aid_source_id]=$i->aid_value;
                    }
                }
                $item->financial_aid_source = $data_;

                $non_financial_aid_source_= \DB::table('char_social_search_cases_aids')
                    ->where('char_social_search_cases_aids.aid_type', '=', 2)
                    ->where('char_social_search_cases_aids.search_cases_id', '=', $item->id)
                    ->selectRaw("aid_source_id ,CASE WHEN char_social_search_cases_aids.aid_value is null THEN '0' 
                                                                  WHEN char_social_search_cases_aids.aid_take = 0 THEN '0'
                                                                                   WHEN char_social_search_cases_aids.aid_value is null THEN '0'  
                                                                                     Else char_social_search_cases_aids.aid_value END 
                                                                                AS  aid_value
                                                         ")->get();

                $data_ = [];
                $item->non_financial_aid_source_elements = [];
                foreach($non_financial_aid_source_ as $kk=>$i) {
                    if(!isset($data_[$i->aid_source_id])){
                        $item->non_financial_aid_source_elements[]=$i->aid_source_id;
                        $data_[$i->aid_source_id]=$i->aid_value;
                    }
                }
                $item->non_financial_aid_source = $data_;

                $persons_properties= \DB::table('char_social_search_cases_properties')
                    ->where('char_social_search_cases_properties.search_cases_id', '=', $item->id)
                    ->selectRaw("char_social_search_cases_properties.property_id as property_id ,
                                                             CASE WHEN char_social_search_cases_properties.has_property is null THEN '0' 
                                                                  WHEN char_social_search_cases_properties.has_property = 0 THEN 0  
                                                                  WHEN char_social_search_cases_properties.quantity is null THEN 0  
                                                                  Else char_social_search_cases_properties.quantity END
                                                             AS  quantity")
                    ->get();

                $data_=[];
                $item->persons_properties_elements = [];
                foreach($persons_properties as $kk=>$i) {
                    if(!isset($data_[$i->property_id])){
                        $item->persons_properties_elements[]=$i->property_id;
                        $data_[$i->property_id]=$i->quantity;
                    }
                }

                $item->persons_properties = $data_;

                $persons_essentials= \DB::table('char_social_search_cases_essentials')
                    ->where('char_social_search_cases_essentials.search_cases_id', '=', $item->id)
                    ->selectRaw("char_social_search_cases_essentials.essential_id ,
                                          char_social_search_cases_essentials.needs as needs")
                    ->get();

                $data_=[];
                $item->persons_essentials_elements = [];
                foreach($persons_essentials as $kk=>$i) {
                    if(!isset($data_[$i->essential_id])){
                        $item->persons_essentials_elements[]=$i->essential_id;
                        $data_[$i->essential_id]=$i->needs;
                    }
                }

                $item->persons_essentials = $data_;

            }


            $output = array('statistic'=>$result,
                'essentials'=>$essentials ,'properties'=>$properties , 'aid_source'=>$aid_source);
        }
        else {
            $result->selectRaw("
                               char_social_search.title as search_title,
                                         char_social_search.date_from,
                                         char_social_search.date_to,
                                             CASE WHEN char_social_search.organization_id = '$organization_id' THEN 1 Else 0  END  AS is_mine,
                                    CONCAT(ifnull(char_social_search_cases.first_name, ' '), ' ' ,ifnull(char_social_search_cases.second_name, ' '),' ',ifnull(char_social_search_cases.third_name, ' '),' ', ifnull(char_social_search_cases.last_name,' ')) AS full_name,
                                    CASE WHEN char_social_search_cases.id_card_number is null THEN ' ' Else char_social_search_cases.id_card_number  END  AS id_card_number,
                                    CASE WHEN char_social_search_cases.gender is null THEN ' '
                                         WHEN char_social_search_cases.gender = 1 THEN '$male'
                                         WHEN char_social_search_cases.gender = 2 THEN '$female'
                                    END AS case_gender,char_social_search_cases.gender,
                                    char_social_search_cases.id,
                                    char_social_search_cases.id as case_id,
                                    char_social_search.category_id ,
                                    CASE WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS category_name,
                                    char_social_search.organization_id,org.name as organization_name,
                                    CASE WHEN  char_social_search_cases.birthday is null THEN ' ' Else 
                                    EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(), char_social_search_cases.birthday)))) END AS age,    
                                    char_social_search_cases_details.visitor,
                                    CASE WHEN char_social_search_cases.status = 1 THEN '$inactive'WHEN char_social_search_cases.status = 0 THEN '$active' END AS cases_status,
                                    char_social_search_cases.status,
                                    CASE WHEN char_social_search_cases.marital_status_id is null THEN ' ' Else char_marital_status.name  END  AS marital_status,
                                    char_social_search_cases.family_cnt AS family_count,
                                    char_social_search_cases.spouses AS spouses,
                                    char_social_search_cases.male_live AS male_count,
                                    char_social_search_cases.female_live AS female_count,
                                    CASE WHEN char_social_search_cases.primary_mobile is null THEN ' ' Else char_social_search_cases.primary_mobile  END  AS primary_mobile,
                                    CASE WHEN char_social_search_cases.district_id  is null THEN ' ' Else district_name.name END   AS governarate ,
                                    CASE WHEN char_social_search_cases.region_id  is null THEN ' ' Else region_name.name END   AS region_name
                                   ");

            $records_per_page = 50;
            $output=$result->paginate($records_per_page);
        }

        return $output;
    }


}