<?php

namespace Aid\Model;

use App\Http\Helpers;
use Common\Model\CaseModel;
use Organization\Model\OrgLocations;

class VoucherPersons  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_vouchers_persons';
    protected $fillable = [ 'person_id','individual_id','voucher_id','status','receipt_date','receipt_time',
                            'receipt_location','type','document_id'];
    public $timestamps = false;

    public function voucher()
    {
        return $this->belongsTo('Aid\Model\Vouchers','voucher_id','id');
    }

    public function file()
    {
        return $this->belongsTo('Document\Model\File','document_id','id');
    }

    public function person()
    {
        return $this->belongsTo('Common\Model\Person','person_id','id');
    }

    public function individual()
    {
        return $this->belongsTo('Common\Model\Person','individual_id','id');
    }

    public static function checkPersons($via,$id,$voucher_id,$organization_id,$amount,$user_id,$individual_id=0){
        $person_id=null;

        if($via == 'id_card_number') {
            $person = \DB::table('char_persons')->where('id_card_number', $id)->first();

            if($person){
                $full_name = $person->first_name .' ' .$person->second_name.' ' .$person->third_name.' ' .$person->last_name .' ';
                $id_card_number = $person->id_card_number;
                $person->full_name = $full_name;
                $passed = true;
                $mainConnector=OrgLocations::getConnected($organization_id);
                CaseModel::DisablePersonInNoConnected($person,$organization_id,$user_id);
                if(!is_null($person->adsdistrict_id)){
                    if(!in_array($person->adsdistrict_id,$mainConnector)){
                        $passed = false;
                    }else{
                        if(!is_null($person->adsregion_id)){
                            if(!in_array($person->adsregion_id,$mainConnector)){
                                $passed = false;
                            }else{

                                if(!is_null($person->adsneighborhood_id)){
                                    if(!in_array($person->adsneighborhood_id,$mainConnector)){
                                        $passed = false;
                                    }else{

                                        if(!is_null($person->adssquare_id)){
                                            if(!in_array($person->adssquare_id,$mainConnector)){
                                                $passed = false;
                                            }else{
                                                if(!is_null($person->adsmosques_id)){
                                                    if(!in_array($person->adsmosques_id,$mainConnector)){
                                                        $passed = false;
                                                    }
                                                }else{
                                                    $passed = false;
                                                }
                                            }
                                        }else{
                                            $passed = false;
                                        }
                                    }
                                }else{
                                    $passed = false;
                                }
                            }
                        }else{
                            $passed = false;
                        }
                    }
                }else{
                    $passed = false;
                }

                if($passed == false){
                    return array('status'=>false,'id'=>$person->id,'reason'=>'out_of_regions','name'=>$full_name);
                }

                $cases = \DB::table('char_cases')
                            ->join('char_categories as ca', function($q) {
                                $q->on('ca.id', '=', 'char_cases.category_id');
                                $q->where('ca.type',2);
                            })
                            ->where('person_id', $person->id)
                            ->selectRaw('char_cases.*');

                $user = \Auth::user();
                $UserType=$user->type;

                if($UserType == 2) {
                    $cases->where(function ($anq) use ($organization_id,$user) {
                        $anq->where('char_cases.organization_id',$organization_id);
                        $anq->orwherein('char_cases.organization_id', function($quer) use($user) {
                            $quer->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });
                }else{
                    $cases->where(function ($anq) use ($organization_id) {
                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                        $anq->wherein('char_cases.organization_id', function($quer) use($organization_id) {
                            $quer->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $organization_id);
                        });
                    });
                }

                $cases = $cases->get();

                if(sizeof($cases) != 0){
                    $result=[];

                    foreach($cases as $k=>$v){
                        $isPass=self::checkPersons('id',$v->id,null,$organization_id,$amount,$user_id);
                        if ($isPass['status'] == true) {
                            $result[]=$isPass;
                        }
                    }

                    if(sizeof($result) == 0){
                        return array('status'=>false,'id'=>$person->id,'reason'=>'policy_restricted','name'=>$full_name);
                    }

                    return array('status'=>true,'id'=>$person->id,'name'=>$person->full_name,'organization_id'=>$organization_id);
                } else{
                    return array('status'=>false,'id'=>$person->id,'reason'=>'not_case','name'=>$full_name);
                }
            }else{
                return array('status'=>false,'id'=>'id_card_number','reason'=>'not_person','name' => Null);
            }
        }
        else {

            $case=\DB::table('char_cases')
                ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
                ->leftjoin('char_block_id_card_number', function($q)  {
                    $q->on('char_block_id_card_number.id_card_number','=','char_persons.id_card_number');
                    $q->where('char_block_id_card_number.type','=',2);
                })
                ->leftjoin('char_block_categories', function($q)  {
                    $q->on('char_block_categories.block_id', '=', 'char_block_id_card_number.id');
                    $q->on('char_block_categories.category_id', '=', 'char_cases.category_id');
                })
                ->where('char_cases.id',$id)
                ->selectRaw("char_cases.id,char_cases.person_id,char_cases.category_id,char_persons.id_card_number,char_persons.old_id_card_number,
                             CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS name,
                             char_persons.family_cnt as family_count,
                             char_block_categories.block_id")
                ->first();

            $category_id=$case->category_id;
            $person_id=$case->person_id;

            $person = \DB::table('char_persons')->where('id', $person_id)->first();

            $id_card_number = $person->id_card_number;
            $full_name = $person->first_name .' ' .$person->second_name.' ' .$person->third_name.' ' .$person->last_name .' ';
            $person->full_name = $full_name;
            $passed = true;
            $mainConnector=OrgLocations::getConnected($organization_id);
            CaseModel::DisablePersonInNoConnected($person,$organization_id,$user_id);
            if(!is_null($person->adsdistrict_id)){
                if(!in_array($person->adsdistrict_id,$mainConnector)){
                    $passed = false;
                }else{
                    if(!is_null($person->adsregion_id)){
                        if(!in_array($person->adsregion_id,$mainConnector)){
                            $passed = false;
                        }else{

                            if(!is_null($person->adsneighborhood_id)){
                                if(!in_array($person->adsneighborhood_id,$mainConnector)){
                                    $passed = false;
                                }else{

                                    if(!is_null($person->adssquare_id)){
                                        if(!in_array($person->adssquare_id,$mainConnector)){
                                            $passed = false;
                                        }else{
                                            if(!is_null($person->adsmosques_id)){
                                                if(!in_array($person->adsmosques_id,$mainConnector)){
                                                    $passed = false;
                                                }
                                            }else{
                                                $passed = false;
                                            }
                                        }
                                    }else{
                                        $passed = false;
                                    }
                                }
                            }else{
                                $passed = false;
                            }
                        }
                    }else{
                        $passed = false;
                    }
                }
            }else{
                $passed = false;
            }
            if($passed == false){
                return array('status'=>false,'person_id'=>$person_id,'reason'=>'out_of_regions','name'=>$case->name,'id_card_number'=>$case->id_card_number);
            }

            if($case->block_id != null){
                return array('status'=>false,'person_id'=>$person_id,'reason'=>'blocked','name'=>$case->name,'id_card_number'=>$case->id_card_number);
            }else{
                $first =date('Y-01-01');
                $last =date('Y-12-t');

                $ancestor_id = \Organization\Model\Organization::getAncestor($organization_id);
                $max_total=0;

                $max_total_amount_of_family=\DB::table('char_settings')
                    ->where('char_settings.id','=','max_total_amount_of_voucher')
                    ->where('char_settings.organization_id',$ancestor_id)
                    ->first();

                if($max_total_amount_of_family){
                    if($max_total_amount_of_family->value && $max_total_amount_of_family->value != 0){
                        $max_total=$max_total_amount_of_family->value;
                    }
                }

                $person_policy_restricted= \DB::table('char_categories_policy')
                    ->where('char_categories_policy.category_id','=',$category_id)
                    ->where('char_categories_policy.level','=',1)
                    ->selectRaw("char_categories_policy.*")
                    ->first();

                if($person_policy_restricted){
                    $voucher_persons_cnt=
                        \DB::table('char_persons')
                            ->where('char_persons.id','=',$id)
                            ->selectRaw("char_get_voucher_persons_count ( char_persons.id  ,'$person_policy_restricted->started_at','$person_policy_restricted->ends_at') AS amount")
                            ->first();


                    $person_current_year_amount=
                        \DB::table('char_persons')
                            ->where('char_persons.id','=',$id)
                            ->selectRaw("char_get_voucher_persons_count ( char_persons.id  ,'$first','$last') AS amount")
                            ->first();

                    if($voucher_persons_cnt){
                        if($person_policy_restricted->count_of_family != 0){
                            if($person_policy_restricted->count_of_family <= $case->family_count){
                                return array('status'=>false,'person_id'=>$person_id,'reason'=>'policy_restricted','sub'=>'restricted_count_of_family','name'=>$case->name,'id_card_number'=>$case->id_card_number);
                            }
                        }

                        if($person_policy_restricted->amount != null && $person_policy_restricted->amount != 0){
                            if($person_policy_restricted->amount <= ($voucher_persons_cnt->amount + $amount)){
                                return array('status'=>false,'person_id'=>$person_id,'reason'=>'policy_restricted','sub'=>'restricted_amount','name'=>$case->name,'id_card_number'=>$case->id_card_number);
                            }
                        }

                        if($max_total_amount_of_family){
                            if($max_total <= ($person_current_year_amount->amount + $amount)){
                                return array('status'=>false,'person_id'=>$person_id,'reason'=>'policy_restricted','sub'=>'restricted_total_payment_persons','name'=>$case->name,'id_card_number'=>$case->id_card_number);
                            }
                        }
                    }
                }

                if($voucher_id != null){
                    
                    $is_nominated= \DB::table('char_vouchers_persons')
                        ->where('char_vouchers_persons.voucher_id','=',$voucher_id)
                        ->where('char_vouchers_persons.person_id','=',$person_id);
                    
                    if ($individual_id) {
                        $is_nominated->where('char_vouchers_persons.individual_id','=',$individual_id);
                    } 

                    $is_nominated->first();

                    if($is_nominated){

                        return array('status'=>false,'person_id'=>$person_id,'reason'=>'nominated','name'=>$case->name,'id_card_number'=>$case->id_card_number);
                    }
                }

            }
        }

        return array('status'=>true,'person_id'=>$person_id , 'name'=>$full_name,'id_card_number'=>$id_card_number);
    }

    public static function checkMainPersons($id_card_number,$organization_id,$category_id){

        $person = \DB::table('char_persons')->where('id_card_number', $id_card_number)->first();
        if($person){
            $add_case=false;
            $full_name = $person->first_name .' ' .$person->second_name.' ' .$person->third_name.' ' .$person->last_name .' ';
            $person->full_name = $full_name;
            $person_id = $person->id ;

            $count = \DB::table('char_cases')
                        ->where(function ($anq) use ($organization_id,$person_id,$category_id) {
                            $anq->where('person_id', $person_id);
                            $anq->where('category_id',$category_id);
                            $anq->where('organization_id',$organization_id);
                        })
                        ->count();

            if($count == 0){
                $add_case =true;
            }

            return array('status'=>true,'id'=>$person->id,'add_case'=>$add_case,'name'=>$full_name);

        }

        return array('status'=>false,'id'=>null,'reason'=>'not_person','name' => '_');
    }

    public  static function filter($options = array()){

        $defaults = array('all_organization' => false, 'action' => "filter");
        $ignore = array('all_organization','action','page','target' ,
                         'organization_id','offset' , 'all' , 'organization_ids','itemsCount','sortKeyArr_rev','sortKeyArr_un_rev');

        $options = array_merge($defaults, $options);
        $condition = [];
        $organization_id = $options['organization_id'];
        $user = \Auth::user();

        $org = $user->organization_id;

        $language_id =  \App\Http\Helpers::getLocale();
        foreach ($options as $key => $value) {
            if (!in_array($key, $ignore)) {
                if ( $value != "" ) {
                    $data = [$key => $value];
                    array_push($condition, $data);
                }
            }
        }

        if($language_id == 1){
            $vouchers= \DB::table('char_vouchers_persons_view');
        }else{
            $vouchers= \DB::table('char_vouchers_persons_view_en');
        }
        if (count($condition) != 0) {
            $vouchers =  $vouchers
                ->where(function ($q) use ($condition) {
                    $str = ['title','content','header',
                        'first_name', 'second_name', 'third_name', 'last_name',
                        'individual_first_name', 'individual_second_name',
                        'individual_third_name', 'individual_last_name',
                        'footer','notes'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $str)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });
        }

        /*
                if(isset($options['all_organization'])){
                    if($options['all_organization'] ===true){
                        $all_organization=0;
                    }elseif($options['all_organization'] ===false){
                        $all_organization=1;

                    }
                }

                if($all_organization ==0){

                    $organizations=[];
                    if(isset($options['organization_ids']) && $options['organization_ids'] !=null && $options['organization_ids'] !="" && $options['organization_ids'] !=[] && sizeof($options['organization_ids']) != 0) {
                        if($options['organization_ids'][0]==""){
                            unset($options['organization_ids'][0]);
                        }
                        $organizations =$options['organization_ids'];
                    }

                    if(!empty($organizations)){
                        $vouchers->wherein('organization_id',$organizations);
                    }else{

                        $user = \Auth::user();
                        $UserType=$user->type;

                        if($UserType == 2) {
                            $vouchers->where(function ($anq) use ($organization_id,$user) {
                                $anq->where('organization_id',$organization_id);
                                $anq->orwherein('organization_id', function($quer) use($user) {
                                    $quer->select('organization_id')
                                        ->from('char_user_organizations')
                                        ->where('user_id', '=', $user->id);
                                });
                            });
                        }else{

                            $vouchers->where(function ($anq) use ($organization_id) {
                                $anq->wherein('organization_id', function($quer) use($organization_id) {
                                    $quer->select('descendant_id')
                                        ->from('char_organizations_closure')
                                        ->where('ancestor_id', '=', $organization_id);
                                });
                            });
                        }


                    }
                }
                elseif($all_organization ==1){
                    $vouchers->where('organization_id',$organization_id);

                }

                */

        $min_value=null;
        $max_value=null;

        if(isset($options['max_value']) && $options['max_value'] !=null && $options['max_value'] !=""){
            $max_value=$options['max_value'];
        }

        if(isset($options['min_value']) && $options['min_value'] !=null && $options['min_value'] !=""){
            $min_value=$options['min_value'];
        }

        if($min_value != null && $max_value != null) {
            $vouchers = $vouchers->whereRaw(" $max_value >= value");
            $vouchers = $vouchers->whereRaw(" $min_value <= value");
        }elseif($max_value != null && $min_value == null) {
            $vouchers = $vouchers->whereRaw(" $max_value >= value");
        }elseif($max_value == null && $min_value != null) {
            $vouchers = $vouchers->whereRaw(" $min_value <= value");
        }

        $voucher_source=null;

        if(isset($options['voucher_source']) && $options['voucher_source'] !=null && $options['voucher_source'] !=""){
            $voucher_source=$options['voucher_source'];
        }

        if($voucher_source != null){
            if($voucher_source == 0){
                $vouchers->whereNotNull('project_id');
            }
            elseif($voucher_source == 1){
                $vouchers->whereNull('project_id');
            }
        }


        $min_count=null;
        $max_count=null;

        if(isset($options['max_count']) && $options['max_count'] !=null && $options['max_count'] !=""){
            $max_count=$options['max_count'];
        }

        if(isset($options['min_count']) && $options['min_count'] !=null && $options['min_count'] !=""){
            $min_count=$options['min_count'];
        }

        if($min_count != null && $max_count != null) {
            $vouchers = $vouchers->whereRaw(" $max_count >= count");
            $vouchers = $vouchers->whereRaw(" $min_count <= count");
        }elseif($max_count != null && $min_count == null) {
            $vouchers = $vouchers->whereRaw(" $max_count >= count");
        }elseif($max_count == null && $min_count != null) {
            $vouchers = $vouchers->whereRaw(" $min_count <= count");
        }

        $date_to=null;
        $date_from=null;

        if(isset($options['date_to']) && $options['date_to'] !=null){
            $date_to=date('Y-m-d',strtotime($options['date_to']));
        }
        if(isset($options['date_from']) && $options['date_from'] !=null){
            $date_from=date('Y-m-d',strtotime($options['date_from']));
        }
        if($date_from != null && $date_to != null) {
            $vouchers = $vouchers->whereDate('created_at', '>=', $date_from);
            $vouchers = $vouchers->whereDate('created_at', '<=', $date_to);
        }elseif($date_from != null && $date_to == null) {
            $vouchers = $vouchers->whereDate('created_at', '>=', $date_from);
        }elseif($date_from == null && $date_to != null) {
            $vouchers = $vouchers->whereDate('created_at', '<=', $date_to);
        }

        $voucher_date_to=null;
        $voucher_date_from=null;

        if(isset($options['voucher_date_to']) && $options['voucher_date_to'] !=null){
            $voucher_date_to=date('Y-m-d',strtotime($options['voucher_date_to']));
        }
        if(isset($options['voucher_date_from']) && $options['voucher_date_from'] !=null){
            $voucher_date_from=date('Y-m-d',strtotime($options['voucher_date_from']));
        }

        if($voucher_date_from != null && $voucher_date_to != null) {
            $vouchers = $vouchers->whereDate('voucher_date', '>=', $voucher_date_from);
            $vouchers = $vouchers->whereDate('voucher_date', '<=', $voucher_date_to);
        }elseif($voucher_date_from != null && $voucher_date_to == null) {
            $vouchers = $vouchers->whereDate('voucher_date', '>=', $voucher_date_from);
        }elseif($voucher_date_from == null && $voucher_date_to != null) {
            $vouchers = $vouchers->whereDate('voucher_date', '<=', $voucher_date_to);
        }

        $map = [
            "name" => 'name',
            "primary_mobile" => 'primary_mobile',
            "id_card_number" => 'id_card_number',
            "title" => 'title',
            "serial" => 'serial',
            "receipt_status_name" => 'receipt_status_name',
            "organization_name" => 'organization_name',
            "currency_name" => 'currency_name',
            "voucher_sponsor_name" => 'sponsor_name',
            "category_type" => 'category_type',
            "value" => 'value',
            "shekel_value" => 'shekel_value',
            "count" => 'count',
            "exchange_rate" => 'exchange_rate',
            "individual_name" => 'individual_name',
            "individual_id_card_number" => 'individual_id_card_number',
            "beneficiary" => 'beneficiary_name',
            "project_id" => 'project_id',
            "voucher_date" => 'voucher_date',
            "status" => 'status',
            "receipt_date" => 'receipt_date',
            "receipt_time" => 'receipt_time',
            "receipt_location" => 'receipt_location',
            "receipt_status" => 'status'
        ];

        $order = false;

        if (isset($options['sortKeyArr_rev']) && $options['sortKeyArr_rev'] != null && $options['sortKeyArr_rev'] != "" &&
            $options['sortKeyArr_rev'] != [] && sizeof($options['sortKeyArr_rev']) != 0
        ) {

            $order = true;

            foreach ($options['sortKeyArr_rev'] as $key_) {
                $vouchers->orderBy($map[$key_],'desc');
            }
        }


        if (isset($options['sortKeyArr_un_rev']) && $options['sortKeyArr_un_rev'] != null && $options['sortKeyArr_un_rev'] != "" &&
            $options['sortKeyArr_un_rev'] != [] && sizeof($options['sortKeyArr_un_rev']) != 0
        ) {

            $order = true;
            foreach ($options['sortKeyArr_un_rev'] as $key_) {
                $vouchers->orderBy($map[$key_],'asc');
            }
        }


        if(!$order){
            $vouchers->orderBy('created_at','desc');
        }


        $male = trans('common::application.male');
        $female = trans('common::application.female');
        $receipt = trans('common::application.receipt');
        $not_receipt = trans('common::application.not_receipt');
        $financial = trans('common::application.financial');
        $non_financial = trans('common::application.non_financial');

        $from_project = trans('aid::application.from_project');
        $manually_ = trans('aid::application.manually_');
        $urgent = trans('aid::application.urgent');
        $not_urgent = trans('aid::application.not_urgent');
        $transfer_company = trans('aid::application.transfer_company');
        $direct = trans('aid::application.direct');


        $head = trans('aid::application.head of family');
        $other = trans('aid::application.other person');

        if($options['action'] =='xls') {

            $vouchers->selectRaw("serial as voucher_serial,title as voucher_title, organization_name,sponsor_name,
                                  CASE  WHEN beneficiary = 1 THEN '$head' Else '$other' END  AS beneficiary_name, 
                                  name, marital_status, gender,id_card_number,family_count");

            if($options['beneficiary'] != 1){
                $vouchers->selectRaw("individual_name, 
                                        individual_id_card_number, 
                                        individual_gender, individual_marital_status");
            }

             $vouchers->selectRaw("primary_mobile, wataniya, secondery_mobile, phone, 
                                        country, district, region, nearlocation, square, mosque, street_address, full_address,
                                        voucher_date,
                                        receipt_status_name,
                                        receipt_date,    
                                         receipt_time_,receipt_location,category_type, value as voucher_value,
                                        currency_name,round(shekel_value,2) as shekel_value,
                                        CASE  WHEN project_id is null THEN '$from_project'
                                          Else '$manually_'
                                         END  AS voucher_source,
                                     CASE
                                           WHEN type = 1 THEN '$non_financial'
                                           WHEN type = 2 THEN '$financial'
                                      END
                                     AS voucher_type,
                                     CASE
                                         WHEN urgent = 0 THEN '$not_urgent'
                                         WHEN urgent = 1 THEN '$urgent'
                                     END
                                     AS voucher_urgent,
                                      
                                     CASE
                                         WHEN transfer = 0 THEN '$direct'
                                         WHEN transfer = 1 THEN '$transfer_company'
                                      END
                                     AS transfer,
                                     char_persons_receipt_count(voucher_id,person_id) as receipt_count,
                                     transfer_company_name");

            return  ['beneficiary' => $vouchers->get() ,'receipt' => $vouchers->groupBy('person_id')->get()];

        }
        elseif($options['action'] =='pdf') {

            $vouchers->selectRaw("title as voucher_title, organization_name,sponsor_name,
                                      CASE  WHEN beneficiary = 1 THEN '$head' Else '$other' END  AS beneficiary_name, 
                                      name, marital_status, gender,id_card_number,family_count");

            if($options['beneficiary'] != 1){
                $vouchers->selectRaw("individual_name, 
                                            individual_id_card_number, 
                                            individual_gender, individual_marital_status");
            }

            $vouchers->selectRaw("primary_mobile, wataniya, secondery_mobile, phone, 
                                        country, district, region, nearlocation, square, mosque, street_address, full_address,
                                        voucher_date,
                                        receipt_status_name,
                                        receipt_date,     receipt_time_,receipt_location,category_type, value as voucher_value,
                                        currency_name,round(shekel_value,2) as shekel_value,
                                        CASE  WHEN project_id is null THEN '$from_project'
                                          Else '$manually_'
                                         END  AS voucher_source,
                                     CASE
                                           WHEN type = 1 THEN '$non_financial'
                                           WHEN type = 2 THEN '$financial'
                                      END
                                     AS voucher_type,
                                     CASE
                                         WHEN urgent = 0 THEN '$not_urgent'
                                         WHEN urgent = 1 THEN '$urgent'
                                     END
                                     AS voucher_urgent,
                                      
                                     CASE
                                         WHEN transfer = 0 THEN '$direct'
                                         WHEN transfer = 1 THEN '$transfer_company'
                                      END
                                     AS transfer,
                                     char_persons_receipt_count(voucher_id,person_id) as receipt_count,
                 transfer_company_name");
            $vouchers->groupBy('person_id');

            return $vouchers->get();
    }
        else{

            if($language_id == 1){
                $vouchers->selectRaw("char_vouchers_persons_view.* , CASE WHEN organization_id = '$org' THEN 1 Else 0  END  AS is_mine");
            }else{
                $vouchers->selectRaw("char_vouchers_persons_view_en.* , CASE WHEN organization_id = '$org' THEN 1 Else 0  END  AS is_mine");
                $vouchers= \DB::table('char_vouchers_persons_view_en');
            }

            $paginate = $vouchers;
            $itemsCount = isset($options['itemsCount'])?$options['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$paginate->count());
            $paginate = $paginate->paginate($records_per_page);
            return $paginate;
        }
    }

    public static function filterPersons($page,$voucher_id)
    {
        $male = trans('common::application.male');
        $female = trans('common::application.female');
        $receipt = trans('common::application.receipt');
        $not_receipt = trans('common::application.not_receipt');

        $language_id =  \App\Http\Helpers::getLocale();

        return \DB::table('char_vouchers_persons')
            ->join('char_persons as c','c.id',  '=', 'char_vouchers_persons.person_id')
            ->leftjoin('char_marital_status_i18n as m', function($q) {
                $q->on('m.marital_status_id', '=', 'c.marital_status_id');
                $q->where('m.language_id',1);
            })
            ->leftjoin('char_persons_contact as primary_mob_number', function($join) {
                $join->on('c.id', '=','primary_mob_number.person_id')
                    ->where('primary_mob_number.contact_type','primary_mobile');
            })
            ->leftjoin('char_persons_contact as wataniya_mob_number', function($join) {
                $join->on('c.id', '=','wataniya_mob_number.person_id')
                    ->where('wataniya_mob_number.contact_type','wataniya');
            })
            ->leftjoin('char_persons_contact as secondary_mob_number', function($join) {
                $join->on('c.id', '=','secondary_mob_number.person_id')
                    ->where('secondary_mob_number.contact_type','secondery_mobile');
            })
            ->leftjoin('char_persons_contact as phone_number', function($join) {
                $join->on('c.id', '=','phone_number.person_id')
                    ->where('phone_number.contact_type','phone');
            })

            ->leftjoin('char_aids_locations_i18n As country_name', function($join) use ($language_id)  {
                $join->on('c.adscountry_id', '=','country_name.location_id' )
                    ->where('country_name.language_id',$language_id);
            })
            ->leftjoin('char_aids_locations_i18n As district_name', function($join) use ($language_id)  {
                $join->on('c.adsdistrict_id', '=','district_name.location_id' )
                    ->where('district_name.language_id',$language_id);
            })
            ->leftjoin('char_aids_locations_i18n As region_name', function($join) use ($language_id)  {
                $join->on('c.adsregion_id', '=','region_name.location_id' )
                    ->where('region_name.language_id',$language_id);
            })
            ->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)  {
                $join->on('c.adsneighborhood_id', '=','location_name.location_id' )
                    ->where('location_name.language_id',$language_id);
            })
            ->leftjoin('char_aids_locations_i18n As square_name',function($join) use ($language_id)  {
                $join->on('c.adssquare_id', '=','square_name.location_id' )
                    ->where('square_name.language_id',$language_id);
            })
            ->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
                $join->on('c.adsmosques_id', '=','mosques_name.location_id' )
                    ->where('mosques_name.language_id',$language_id);
            })

            ->where('char_vouchers_persons.voucher_id', '=', $voucher_id)
            ->selectRaw("
                            char_vouchers_persons.voucher_id as voucher_id,
                            c.id as person_id,
                             CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ',ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' ')) AS name,
                            CASE WHEN c.gender is null THEN '-'
                                 WHEN c.gender = 1 THEN '$male'
                                 WHEN c.gender = 2 THEN '$female'
                            END  AS gender,
                            CASE WHEN c.id_card_number is null THEN '-' Else c.id_card_number  END  AS id_card_number,
                            CASE WHEN c.old_id_card_number is null THEN '-' Else c.old_id_card_number  END  AS old_id_card_number,
                            CASE WHEN c.marital_status_id is null THEN '-' Else m.name  END  AS marital_status,
                            CASE WHEN primary_mob_number.contact_value is null THEN '-' Else primary_mob_number.contact_value  END  AS primary_mobile,
                            CASE WHEN wataniya_mob_number.contact_value is null THEN '-' Else wataniya_mob_number.contact_value  END  AS wataniya,
                            CASE WHEN secondary_mob_number.contact_value is null THEN '-' Else secondary_mob_number.contact_value  END  AS secondery_mobile,
                            CASE WHEN phone_number.contact_value is null THEN '-' Else phone_number.contact_value  END  AS phone,
                           CONCAT(ifnull(country_name.name,' '),' - ',
                                                            ifnull(district_name.name,' '),' - ',
                                                            ifnull(region_name.name,' '),' - ',
                                                            ifnull(location_name.name,' '),' - ',
                                                            ifnull(square_name.name,' '),' - ',
                                                            ifnull(mosques_name.name,' '),' - ',
                                                            ifnull(c.street_address,' '))
                                            AS address,
                            char_vouchers_persons.receipt_date as receipt_date,
                            char_vouchers_persons.receipt_time as receipt_time,
                            TIME_FORMAT(char_vouchers_persons.receipt_time, '%r') as receipt_time_,
                            char_vouchers_persons.receipt_location as receipt_location,
                            char_vouchers_persons.status as status,
                            CASE
                                                WHEN char_vouchers_persons.status = 1 THEN '$receipt'
                                                WHEN char_vouchers_persons.status = 2 THEN ' $not_receipt'
                                                END
                                   as receipt_status"
            )
            ->orderBy('c.first_name')
            ->groupBy('c.id')
            ->paginate(config('constants.records_per_page'));

    }
    public static function listPerson($id)
    {
        $persons = array();

        $items= self::query()->where('voucher_id',$id)->selectRaw("char_vouchers_persons.person_id as id")->get();
        foreach ($items as $key =>$value) {
            array_push($persons, $value->id);
        }

        return $persons;
    }
    public static function VoucherBeneficiary($voucher_id,$person_id)
    {
        $language_id =  \App\Http\Helpers::getLocale();

        $return= $list= \DB::table('char_vouchers_persons')
            ->join('char_persons as c','c.id',  '=', 'char_vouchers_persons.person_id')
            ->leftjoin('char_marital_status_i18n as m', function($q) {
                $q->on('m.marital_status_id', '=', 'c.marital_status_id');
                $q->where('m.language_id',1);
            })
            ->leftjoin('char_persons_contact as cont1', function($q) {
                $q->on('cont1.person_id', '=', 'c.id');
                $q->where('cont1.contact_type','primary_mobile');
            })
            ->leftjoin('char_persons_contact as cont2', function($q) {
                $q->on('cont2.person_id', '=', 'c.id');
                $q->where('cont2.contact_type','secondery_mobile');
            })
            ->leftjoin('char_persons_contact as cont3', function($q) {
                $q->on('cont3.person_id', '=', 'c.id');
                $q->where('cont3.contact_type','phone');
            })
            ->leftjoin('char_persons_contact as cont4', function($q) {
                $q->on('cont4.person_id', '=', 'c.id');
                $q->where('cont4.contact_type','wataniya');
            })
                         ->leftjoin('char_aids_locations_i18n As country_name', function($join) use ($language_id)  {
                                            $join->on('c.adscountry_id', '=','country_name.location_id' )
                                                ->where('country_name.language_id',1);
                        })
                         ->leftjoin('char_aids_locations_i18n As district_name', function($join) use ($language_id)  {
                            $join->on('c.adsdistrict_id', '=','district_name.location_id' )
                                ->where('district_name.language_id',$language_id);
                        })
                            ->leftjoin('char_aids_locations_i18n As region_name', function($join) use ($language_id)  {
                            $join->on('c.adsregion_id', '=','region_name.location_id' )
                                ->where('region_name.language_id',$language_id);
                        })
                        ->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)  {
                            $join->on('c.adsneighborhood_id', '=','location_name.location_id' )
                                ->where('location_name.language_id',$language_id);
                        })
                         ->leftjoin('char_aids_locations_i18n As square_name', function($join) use ($language_id)  {
                            $join->on('c.adssquare_id', '=','square_name.location_id' )
                                ->where('square_name.language_id',$language_id);
                        })
                         ->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
                             $join->on('c.adsmosques_id', '=','mosques_name.location_id' )
                                 ->where('mosques_name.language_id',$language_id);
                         })


            ->where('char_vouchers_persons.voucher_id', '=', $voucher_id);

        if($person_id != -1){
            $return=$return ->where('c.id', '=', $person_id);
        }

        return $return
            ->orderBy('c.first_name')
            ->selectRaw(
                "CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ',ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' ')) AS name,
                             cont1.contact_value AS primary_mobile,
                             cont1.contact_value AS wataniya,
                             cont2.contact_value AS secondery_mobile,
                             cont3.contact_value AS phone,
                             c.id_card_number,c.old_id_card_number,
                            CONCAT(ifnull(country_name.name,' '),' - ',
                                                            ifnull(district_name.name,' '),' - ',
                                                            ifnull(region_name.name,' '),' - ',
                                                            ifnull(location_name.name,' '),' - ',
                                                            ifnull(square_name.name,' '),' - ',
                                                            ifnull(mosques_name.name,' '),' - ',
                                                            ifnull(c.street_address,' '))
                                            AS address,
                                             char_vouchers_persons.receipt_date as receipt_date,
                                             char_vouchers_persons.receipt_time as receipt_time,
                             CASE WHEN char_vouchers_persons.receipt_time is null THEN '-' Else TIME_FORMAT(char_vouchers_persons.receipt_time, '%r')  END  AS receipt_time_,
                             char_vouchers_persons.receipt_location as receipt_location"
            )
            ->get();
        
    }

    public static function beneficiaryDetails($voucher_id,$person_id)
    {

        return  \DB::table('char_vouchers_persons')
                    ->join('char_vouchers','char_vouchers.id',  '=', 'char_vouchers_persons.voucher_id')
                    ->where(function ($q) use ($person_id,$voucher_id) {
                        $q->where('char_vouchers_persons.person_id', '=', $person_id);
                        $q->where('char_vouchers_persons.voucher_id', '=', $voucher_id);
                    })
                    ->selectRaw("
                         char_vouchers.title as voucher_title,
                         char_vouchers_persons.receipt_date as receipt_date,
                         char_vouchers_persons.receipt_time as receipt_time,
                         char_vouchers.value as voucher_value,
                         char_vouchers.exchange_rate,
                         (char_vouchers.exchange_rate * char_vouchers.value) sh_value,
                         CASE WHEN char_vouchers_persons.receipt_time is null THEN '-' 
                         Else TIME_FORMAT(char_vouchers_persons.receipt_time, '%r')  END  AS receipt_time_,
                         char_vouchers_persons.receipt_location as receipt_location"
                    )
                    ->first();

    }

    public static function allBeneficiary($voucher_id,$target)
    {
        $language_id =  \App\Http\Helpers::getLocale();

        $list= \DB::table('char_vouchers_persons')
            ->join('char_persons as c','c.id',  '=', 'char_vouchers_persons.person_id')
            ->leftjoin('char_marital_status_i18n as m', function($q) use ($language_id)  {
                $q->on('m.marital_status_id', '=', 'c.marital_status_id');
                $q->where('m.language_id',$language_id);
            })
            ->leftjoin('char_persons_contact as cont1', function($q) {
                $q->on('cont1.person_id', '=', 'c.id');
                $q->where('cont1.contact_type','primary_mobile');
            })
            ->leftjoin('char_persons_contact as cont2', function($q) {
                $q->on('cont2.person_id', '=', 'c.id');
                $q->where('cont2.contact_type','secondery_mobile');
            })
            ->leftjoin('char_persons_contact as cont3', function($q) {
                $q->on('cont3.person_id', '=', 'c.id');
                $q->where('cont3.contact_type','phone');
            })
            ->leftjoin('char_persons_contact as cont4', function($q) {
                $q->on('cont4.person_id', '=', 'c.id');
                $q->where('cont4.contact_type','wataniya');
            })
            ->leftjoin('char_aids_locations_i18n As country_name',function($join) use ($language_id)  {
                $join->on('c.adscountry_id', '=','country_name.location_id' )
                    ->where('country_name.language_id',$language_id);
            })
            ->leftjoin('char_aids_locations_i18n As district_name', function($join) use ($language_id)  {
                $join->on('c.adsdistrict_id', '=','district_name.location_id' )
                    ->where('district_name.language_id',$language_id);
            })
            ->leftjoin('char_aids_locations_i18n As region_name', function($join) use ($language_id)  {
                $join->on('c.adsregion_id', '=','region_name.location_id' )
                    ->where('region_name.language_id',$language_id);
            })
            ->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)  {
                $join->on('c.adsneighborhood_id', '=','location_name.location_id' )
                    ->where('location_name.language_id',$language_id);
            })
            ->leftjoin('char_aids_locations_i18n As square_name', function($join) use ($language_id)  {
                $join->on('c.adssquare_id', '=','square_name.location_id' )
                    ->where('square_name.language_id',$language_id);
            })
            ->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
                $join->on('c.adsmosques_id', '=','mosques_name.location_id' )
                    ->where('mosques_name.language_id',$language_id);
            })
            ->where('char_vouchers_persons.voucher_id', '=', $voucher_id)
            ->orderBy('c.first_name');

        $male = trans('common::application.male');
        $female = trans('common::application.female');
        $receipt = trans('common::application.receipt');
        $not_receipt = trans('common::application.not_receipt');

        if($target ==1){
            
            return $list->selectRaw("
                                    CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ',ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' ')) AS name,
                                   c.id_card_number,c.old_id_card_number,
                                     CASE WHEN c.marital_status_id is null THEN '-' Else m.name  END  AS marital_status_id,
                                     CASE WHEN c.gender is null THEN '-'
                                         WHEN c.gender = 1 THEN '$male'
                                         WHEN c.gender = 2 THEN '$female'
                                     END  AS gender,
                                     CASE WHEN cont1.contact_value is null THEN '-' Else cont1.contact_value  END  AS primary_mobile,
                                     CASE WHEN cont4.contact_value is null THEN '-' Else cont4.contact_value  END  AS wataniya,
                                     CASE WHEN cont2.contact_value is null THEN '-' Else cont2.contact_value  END  AS secondery_mobile,
                                     CASE WHEN cont3.contact_value is null THEN '-' Else cont3.contact_value  END  AS phone,
                                     CASE WHEN c.adscountry_id  is null THEN '-' Else country_name.name END   AS country,
                                     CASE WHEN c.adsdistrict_id  is null THEN '-' Else district_name.name END   AS governarate,
                                     CASE WHEN c.adsregion_id is null THEN '-' Else region_name.name END   AS region_name,
                                     CASE WHEN c.adsneighborhood_id is null THEN '-' Else location_name.name END   AS nearLocation,
                                     CASE WHEN c.adssquare_id is null THEN '-' Else square_name.name END   AS square_name,
                                     CASE WHEN c.adsmosques_id is null THEN '-' Else mosques_name.name END   AS mosque_name,
                                     CASE WHEN c.street_address  is null THEN '-' Else c.street_address END AS street_address,
                                     CASE WHEN char_vouchers_persons.receipt_date is null THEN '-' Else char_vouchers_persons.receipt_date  END  AS receipt_date,
                                     char_vouchers_persons.receipt_time,
                                     CASE WHEN char_vouchers_persons.receipt_time is null THEN '-' Else TIME_FORMAT(char_vouchers_persons.receipt_time, '%r')  END  AS receipt_time_,
                                     CASE WHEN char_vouchers_persons.receipt_location is null THEN '-' Else char_vouchers_persons.receipt_location  END  AS receipt_location
                               ")
                ->get();

    }else{
            return $list->selectRaw("CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ',ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' ')) AS name,
                                     c.id_card_number,c.old_id_card_number,
                                     CASE WHEN c.marital_status_id is null THEN '-' Else m.name  END  AS marital_status_id,
                                     CASE WHEN c.gender is null THEN '-'
                                         WHEN c.gender = 1 THEN '$male'
                                         WHEN c.gender = 2 THEN '$female'
                                     END  AS gender,
                                     CASE WHEN cont1.contact_value is null THEN '-' Else cont1.contact_value  END  AS primary_mobile,
                                     CASE WHEN cont4.contact_value is null THEN '-' Else cont4.contact_value  END  AS wataniya,
                                     CASE WHEN cont2.contact_value is null THEN '-' Else cont2.contact_value  END  AS secondery_mobile,
                                     CASE WHEN cont3.contact_value is null THEN '-' Else cont3.contact_value  END  AS phone,
                                     CASE WHEN c.adscountry_id  is null THEN '-' Else country_name.name END   AS country,
                                     CASE WHEN c.adsdistrict_id  is null THEN '-' Else district_name.name END   AS governarate,
                                     CASE WHEN c.adsregion_id is null THEN '-' Else region_name.name END   AS region_name,
                                     CASE WHEN c.adsneighborhood_id is null THEN '-' Else location_name.name END   AS nearLocation,
                                     CASE WHEN c.adssquare_id is null THEN '-' Else square_name.name END   AS square_name,
                                     CASE WHEN c.adsmosques_id is null THEN '-' Else mosques_name.name END   AS mosque_name,
                                     CASE WHEN c.street_address  is null THEN '-' Else c.street_address END AS street_address,
                                     CASE WHEN char_vouchers_persons.receipt_date is null THEN '-' Else char_vouchers_persons.receipt_date  END  AS receipt_date,
                                     CASE WHEN char_vouchers_persons.receipt_time is null THEN '-' Else TIME_FORMAT(char_vouchers_persons.receipt_time, '%r')  END  AS receipt_time_,
                                     char_vouchers_persons.receipt_time,
                                     CASE WHEN char_vouchers_persons.receipt_location is null THEN '-' Else char_vouchers_persons.receipt_location  END  AS receipt_location,
                                     CASE
                                                WHEN char_vouchers_persons.status = 1 THEN '$receipt'
                                                WHEN char_vouchers_persons.status = 2 THEN ' $not_receipt'
                                                END
                                   as receipt_status
                               ")
                ->get();
        }
    }

    public static function getDetails($voucher_id,$person_id)
    {
        $male = trans('common::application.male');
        $female = trans('common::application.female');
        $receipt = trans('common::application.receipt');
        $not_receipt = trans('common::application.not_receipt');

        $financial = trans('common::application.financial');
        $non_financial = trans('common::application.non_financial');

        $from_project = trans('aid::application.from_project');
        $manually_ = trans('aid::application.manually_');
        $urgent = trans('aid::application.urgent');
        $not_urgent = trans('aid::application.not_urgent');
        $transfer_company = trans('aid::application.transfer_company');
        $direct = trans('aid::application.direct');

        return \DB::table('char_vouchers_persons')
            ->join('char_persons as c','c.id','char_vouchers_persons.person_id')
            ->join('char_vouchers','char_vouchers.id',  '=', 'char_vouchers_persons.voucher_id')
            ->join('char_vouchers_categories', 'char_vouchers_categories.id', '=', 'char_vouchers.category_id')
            ->join('char_organizations as org','org.id',  '=', 'char_vouchers.organization_id')
            ->join('char_organizations as sponsor','sponsor.id',  '=', 'char_vouchers.sponsor_id')
            ->leftJoin('char_currencies', 'char_currencies.id', '=', 'char_vouchers.currency_id')
            ->leftJoin('char_transfer_company', 'char_transfer_company.id', '=', 'char_vouchers.transfer_company_id')
            ->where(function ($q) use ($person_id,$voucher_id) {
                $q->where('char_vouchers_persons.person_id', '=', $person_id);
                $q->where('char_vouchers_persons.voucher_id', '=', $voucher_id);
            })
            ->selectRaw("
                            char_vouchers_persons.voucher_id as voucher_id,
                             CONCAT(ifnull(c.first_name, ' '), ' ' ,ifnull(c.second_name, ' '),' ',ifnull(c.third_name, ' '),' ', ifnull(c.last_name,' ')) AS name,
                            CASE WHEN c.gender is null THEN '-'
                                 WHEN c.gender = 1 THEN '$male'
                                 WHEN c.gender = 2 THEN '$female'
                            END  AS gender,
                            CASE WHEN c.id_card_number is null THEN '-' Else c.id_card_number  END  AS id_card_number,
                            CASE WHEN c.old_id_card_number is null THEN '-' Else c.old_id_card_number  END  AS old_id_card_number,
                            char_vouchers_persons.receipt_date as receipt_date,
                            char_vouchers_persons.receipt_time as receipt_time,
                            TIME_FORMAT(char_vouchers_persons.receipt_time, '%r') as receipt_time_,
                            char_vouchers_persons.receipt_location as receipt_location,
                            char_vouchers_persons.status as status,
                            CASE
                                                WHEN char_vouchers_persons.status = 1 THEN '$receipt'
                                                WHEN char_vouchers_persons.status = 2 THEN ' $not_receipt'
                                                END
                                   as receipt_status,
                             char_vouchers.title as voucher_title,
                            char_total_voucher_amount(char_vouchers.id) as voucher_total_sk_value,
                            char_vouchers.value as voucher_value,
                              char_vouchers.exchange_rate,
                             (char_vouchers.exchange_rate * char_vouchers.value) shekel_value,
                             CASE
                                  WHEN char_vouchers.currency_id is null THEN '-'
                                  Else char_currencies.name
                             END  AS currency,
                             char_vouchers_categories.name as category_type,
                             CASE
                                  WHEN char_vouchers.project_id is null THEN '$from_project'
                                  Else '$manually_'
                             END  AS voucher_source,
                             CASE
                                   WHEN char_vouchers.type = 1 THEN '$non_financial'
                                   WHEN char_vouchers.type = 2 THEN '$financial'
                              END
                             AS voucher_type,
                             CASE
                                 WHEN char_vouchers.urgent = 0 THEN '$not_urgent'
                                 WHEN char_vouchers.urgent = 1 THEN '$urgent'
                             END
                             AS voucher_urgent,
                             org.name as organization_name,
                             sponsor.name as sponsor_name,
                             CASE
                                 WHEN char_vouchers.transfer = 0 THEN '$direct'
                                 WHEN char_vouchers.transfer = 1 THEN '$transfer_company'
                              END
                             AS transfer,
                             CASE
                                  WHEN char_vouchers.transfer_company_id is null THEN '-'
                                  Else char_transfer_company.name
                             END  AS transfer_company_name,

                             char_vouchers.content as voucher_content,
                             ifnull(char_vouchers.notes,'-') as voucher_note"
            )
            ->get();


    }
    public static function getPersonForSponsor($filters){
        
        $language_id =  \App\Http\Helpers::getLocale();

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $first =date('Y-01-01');
        $last =date('Y-12-t');


        $condition =array();

        $active = trans('common::application.active');
        $inactive = trans('common::application.inactive');

        $yes = trans('common::application.yes');
        $no = trans('common::application.no');

        $id_card = trans('common::application.id_card');
        $id_number = trans('common::application.id_number');
        $passport = trans('common::application.passport');

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $refugee = trans('common::application.refugee');
        $citizen = trans('common::application.citizen');

        $not_exist = trans('common::application.not_exist');
        $exist = trans('common::application.exist');

        $not_work = trans('common::application.not_work');
        $work = trans('common::application.work');

        $qualified = trans('common::application.qualified');
        $not_qualified = trans('common::application.not qualified');

        $is_alive = trans('common::application.is_alive');
        $deceased = trans('common::application.deceased');

        $vocational = trans('common::application.vocational');
        $academic = trans('common::application.academic');

        $perfect = trans('common::application.perfect');
        $suffers_from_a_chronic_disease = trans('common::application.suffers_from_a_chronic_disease');
        $special_needs = trans('common::application.special_needs');

        $excellent = trans('common::application.excellent');
        $acceptable = trans('common::application.acceptable');

        $good = trans('common::application.good');
        $very_good = trans('common::application.very_good');
        $weak = trans('common::application.weak');
        $very_bad_condition = trans('common::application.very_bad_condition');
        $bad_condition = trans('common::application.bad_condition');

        $the_first  = trans('common::application.the_first') ;
        $the_second = trans('common::application.the_second') ;
        $the_third  = trans('common::application.the_third') ;
        $the_fourth = trans('common::application.the_fourth') ;
        $the_Fifth  = trans('common::application.the_Fifth');
        $the_Sixth  = trans('common::application.the_Sixth');
        $the_Seventh = trans('common::application.the_Seventh');
        $the_Eighth= trans('common::application.the_Eighth');
        $the_Ninth = trans('common::application.the_Ninth');
        $the_tenth = trans('common::application.the_tenth');
        $the_eleventh = trans('common::application.the_eleventh');
        $the_twelveth= trans('common::application.the_twelveth');

        $special_studies= trans('common::application.special_studies');
        $vocational_training= trans('common::application.vocational_training');
        $first_year_of_university= trans('common::application.first_year_of_university');
        $second_year_of_university= trans('common::application.second_year_of_university');
        $third_year_of_university= trans('common::application.third_year_of_university');
        $fourth_year_of_university= trans('common::application.fourth_year_of_university');
        $fifth_year_of_university= trans('common::application.fifth_year_of_university');

        $regularly = trans('common::application.regularly');
        $does_not_pray = trans('common::application.does_not_pray');
        $sometimes = trans('common::application.sometimes');

        
        $user = \Auth::user();
        $id = $user->organization_id ;

        $char_persons = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number', 'gender',
            'marital_status_id','birth_place','nationality','refugee','unrwa_card_number','location_id','country','governarate','city',
            'street_address','monthly_income','mosques_id',"adscountry_id","is_qualified","qualified_card_number",
            "adsdistrict_id", "adsmosques_id", "card_type", "adsneighborhood_id", "adsregion_id",
            "adssquare_id" ,'actual_monthly_income', 'receivables',  'receivables_sk_amount',
            'has_commercial_records','active_commercial_records', 'gov_commercial_records_details'];
        $char_residence = ['property_type_id', 'roof_material_id','residence_condition','indoor_condition',
            'house_condition','habitable','rent_value','need_repair','repair_notes'];
        $char_persons_work = ['working','can_work','work_status_id','work_reason_id','work_job_id','work_wage_id','work_location',
            'gov_work_details'];
        $char_persons_health = ['condition','disease_id','has_health_insurance', 'health_insurance_number', 'health_insurance_type',
            'has_device', 'used_device_name', 'gov_health_details'];
       

        $char_persons_health_cnt =
        $char_persons_education_cnt =
        $char_persons_i18n_cnt  =
        $char_residence_cnt =
        $char_persons_work_cnt = 0;

        foreach ($filters as $key => $value) {
            if(in_array($key, $char_persons)) {
                if ( $value != "" ) {
                    $data = ['char_persons.' . $key => $value];
                    array_push($condition, $data);
                }
            }
            if(in_array($key, $char_residence)) {
                if ( $value != "" ) {
                    $data = ['char_residence.' . $key =>  $value];
                    $char_residence_cnt++;
                    array_push($condition, $data);
                }
            }

            if(in_array($key, $char_persons_work)) {

                if ( $value != "" ) {
                    $data = ['char_persons_work.' . $key => $value];
                    $char_persons_work_cnt ++;
                    array_push($condition, $data);
                }

            }
            if(in_array($key, $char_persons_health)) {
                if ( $value != "" ) {
                    $data = ['char_persons_health.' . $key =>  $value];
                    $char_persons_health_cnt++;
                    array_push($condition, $data);
                }
            }
        }

        $result = \DB::table('char_persons')
                ->leftjoin('char_persons_i18n', function($q) use ($language_id){
                            $q->on('char_persons_i18n.person_id', '=', 'char_persons.id');
                            $q->where('char_persons_i18n.language_id',2);
                      })
                     ->leftjoin('char_marital_status_i18n as char_marital_status', function($q) use ($language_id) {
                        $q->on('char_marital_status.marital_status_id', '=', 'char_persons.marital_status_id');
                        $q->where('char_marital_status.language_id',$language_id);
                     })
                    ->leftjoin('char_aids_locations_i18n As country_name', function($join)  use ($language_id) {
                        $join->on('char_persons.adscountry_id', '=','country_name.location_id' )
                            ->where('country_name.language_id',$language_id);
                    })
                     ->leftjoin('char_aids_locations_i18n As district_name', function($join) use ($language_id)  {
                        $join->on('char_persons.adsdistrict_id', '=','district_name.location_id' )
                            ->where('district_name.language_id',$language_id);
                    })
                    ->leftjoin('char_aids_locations_i18n As region_name', function($join)  use ($language_id) {
                        $join->on('char_persons.adsregion_id', '=','region_name.location_id' )
                            ->where('region_name.language_id',$language_id);
                    })
                    ->leftjoin('char_aids_locations_i18n As location_name', function($join) use ($language_id)   {
                        $join->on('char_persons.adsneighborhood_id', '=','location_name.location_id' )
                            ->where('location_name.language_id',$language_id);
                    })
                    ->leftjoin('char_aids_locations_i18n As square_name', function($join)  use ($language_id) {
                        $join->on('char_persons.adssquare_id', '=','square_name.location_id' )
                            ->where('square_name.language_id',$language_id);
                    })
                    ->leftjoin('char_aids_locations_i18n As mosques_name', function($join) use ($language_id)  {
                        $join->on('char_persons.adsmosques_id', '=','mosques_name.location_id' )
                            ->where('mosques_name.language_id',$language_id);
                    });
            
        if($filters['action'] =='ExportToExcel' || $filters['action'] =='ExportToExcel_'  || $filters['action'] =='csv') {

          $result->leftjoin('char_locations_i18n As L0', function($q) use ($language_id){
                    $q->on('L0.location_id', '=', 'char_persons.nationality');
                    $q->where('L0.language_id',$language_id);
                })->leftjoin('char_locations_i18n As L', function($q) use ($language_id){
                    $q->on('L.location_id', '=', 'char_persons.birth_place');
                    $q->where('L.language_id',$language_id);
                })
                ->leftjoin('char_persons_health','char_persons_health.person_id', '=', 'char_persons.id')
                    ->leftjoin('char_diseases_i18n', function($q) use ($language_id){
                        $q->on('char_diseases_i18n.disease_id', '=', 'char_persons_health.disease_id');
                        $q->where('char_diseases_i18n.language_id',$language_id);
                })
                ->leftjoin('char_residence', 'char_residence.person_id', '=', 'char_persons.id')
                ->leftjoin('char_property_types_i18n', function($q) use ($language_id){
                    $q->on('char_property_types_i18n.property_type_id', '=', 'char_residence.property_type_id');
                    $q->where('char_property_types_i18n.language_id',$language_id);
                })
                ->leftjoin('char_roof_materials_i18n', function($q) use ($language_id){
                    $q->on('char_roof_materials_i18n.roof_material_id', '=', 'char_residence.roof_material_id');
                    $q->where('char_roof_materials_i18n.language_id',$language_id);
                })
                ->leftjoin('char_building_status_i18n', function($q) use ($language_id){
                    $q->on('char_building_status_i18n.building_status_id', '=', 'char_residence.house_condition');
                    $q->where('char_building_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_furniture_status_i18n', function($q) use ($language_id){
                    $q->on('char_furniture_status_i18n.furniture_status_id', '=', 'char_residence.indoor_condition');
                    $q->where('char_furniture_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_habitable_status_i18n', function($q) use ($language_id){
                    $q->on('char_habitable_status_i18n.habitable_status_id', '=', 'char_residence.habitable');
                    $q->where('char_habitable_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_house_status_i18n', function($q) use ($language_id){
                    $q->on('char_house_status_i18n.house_status_id', '=', 'char_residence.residence_condition');
                    $q->where('char_house_status_i18n.language_id',$language_id);
                })
                ->leftjoin('char_persons_work', 'char_persons_work.person_id', '=', 'char_persons.id')
                ->leftjoin('char_work_jobs_i18n as work_job', function($q) use ($language_id){
                    $q->on('work_job.work_job_id', '=', 'char_persons_work.work_job_id');
                    $q->where('work_job.language_id',$language_id);
                })
                ->leftjoin('char_work_wages as work_wage', function($q) use ($language_id){
                    $q->on('work_wage.id', '=', 'char_persons_work.work_wage_id');
                })
                ->leftjoin('char_work_status_i18n as work_status', function($q) use ($language_id){
                    $q->on('work_status.work_status_id', '=', 'char_persons_work.work_status_id');
                    $q->where('work_status.language_id',$language_id);
                })
                ->leftjoin('char_work_reasons_i18n as work_reason', function($q) use ($language_id){
                    $q->on('work_reason.work_reason_id', '=', 'char_persons_work.work_reason_id');
                    $q->where('work_reason.language_id',$language_id);
                });

        }
        else{

                
            if($char_residence_cnt != 0) {
                $result->leftjoin('char_residence', 'char_residence.person_id', '=', 'char_persons.id');
            }
            if($char_persons_work_cnt != 0){
                $result ->leftjoin('char_persons_work','char_persons_work.person_id', '=', 'char_persons.id');
            }
            if($char_persons_health_cnt!= 0 ){
                $result->leftjoin('char_persons_health','char_persons_health.person_id','=','char_persons.id');
            }
            if($char_persons_education_cnt!= 0 ){
                $result->leftjoin('char_persons_education','char_persons_education.person_id','=','char_persons.id');
            }
        }

        
        $result->where(function ($Squery) use ($condition , $filters , $id ,$first, $last) {
        
                $Squery->whereIn('char_persons.id',function ($Sq) use ($condition , $filters , $id ){
                    $Sq->select('person_id')
                       ->from('char_vouchers_persons')
                       ->where(function ($Sq_) use ($condition , $filters , $id ){
                            $Sq_->whereIn('char_vouchers_persons.voucher_id',function ($Sq) use ($condition , $filters , $id ){
                                $Sq->select('id')
                                   ->from('char_vouchers')
                                   ->where(function ($q_) use ($condition , $filters , $id ){
                                       $q_->whereNull('deleted_at');
                                       $q_->where('sponsor_id',$id);

                                         if(isset($filters['voucher_organization'])){
                                           if(sizeof($filters['voucher_organization']) > 0 ){
                                                $q_->whereIn('char_vouchers.organization_id',$filters['voucher_organization']);
                                           } 
                                         }
                                         
                                         if(isset($filters['category_id'])){
                                           if(!is_null($filters['category_id']) && $filters['category_id'] != ''  && $filters['category_id'] != ' ') {
                                                $q_->where('char_vouchers.category_id',$filters['category_id']);
                                           } 
                                         }
                                });
                                 
                            });

                        });
                    
                });

                if (count($condition) != 0) {
                    $Squery->where(function ($q) use ($condition,$first, $last) {
                            $names = ['char_persons.street_address','char_persons.first_name', 'char_persons.second_name',
                                'char_persons.third_name', 'char_persons.last_name','char_residence.repair_notes',
                                'char_persons_work.work_location','char_persons_banks.account_owner',
                                'char_persons_health.health_insurance_number','char_persons_health.used_device_name',
                                'char_persons_health.gov_health_details',
                                'char_persons.gov_commercial_records_details','char_residence.repair_notes',
                                'char_persons_work.work_location', 'char_persons_work.gov_work_details' ,
                                'char_persons_health.used_device_name', 'char_persons_health.gov_health_details'
                            ];

                            $live = ['m.death_date','f.death_date'];
                           foreach ($condition as $key => $value) {
                                    if(in_array($key, $names)) {
                                        $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                                    } else if(in_array($key, $live)){
                                        if($value == 0){
                                            $q->where($key, '=', null);
                                        }else{
                                            $q->where($key, '!=', null);
                                        }
                                    }else {
                                        $q->where($key, '=', $value);
                                    }
                                }
                        });
                }

                if((isset($filters['birthday_to']) && $filters['birthday_to'] !=null) || 
                   (isset($filters['birthday_from']) && $filters['birthday_from'] !=null)){

                        $birthday_to=null;
                        $birthday_from=null;

                        if(isset($filters['birthday_to']) && $filters['birthday_to'] !=null){
                            $Squery=date('Y-m-d',strtotime($filters['birthday_to']));
                        }
                        if(isset($filters['birthday_from']) && $filters['birthday_from'] !=null){
                            $Squery=date('Y-m-d',strtotime($filters['birthday_from']));
                        }
                        if($birthday_from != null && $birthday_to != null) {
                            $Squery->whereBetween( 'char_persons.birthday', [ $birthday_from, $birthday_to]);
                        }elseif($birthday_from != null && $birthday_to == null) {
                            $Squery->whereDate('char_persons.birthday', '>=', $birthday_from);
                        }elseif($birthday_from == null && $birthday_to != null) {
                            $Squery->whereDate('char_persons.birthday', '<=', $birthday_to);
                        }

                }
                
                 if((isset($filters['monthly_income_to']) && $filters['monthly_income_to'] !=null 
                         && $filters['monthly_income_to'] !="")||
                    (isset($filters['monthly_income_from']) && $filters['monthly_income_from'] !=null && 
                    $filters['monthly_income_from'] !="")){

                    $monthly_income_from=null;
                    $monthly_income_to=null;
                    if(isset($filters['monthly_income_to']) && $filters['monthly_income_to'] !=null && $filters['monthly_income_to'] !=""){
                        $monthly_income_to=$filters['monthly_income_to'];
                    }
                    if(isset($filters['monthly_income_from']) && $filters['monthly_income_from'] !=null && $filters['monthly_income_from'] !=""){
                        $monthly_income_from=$filters['monthly_income_from'];
                    }

                    if($monthly_income_from != null && $monthly_income_to != null) {
                        $Squery->whereRaw(" $monthly_income_to >= char_persons.monthly_income")
                               ->whereRaw(" $monthly_income_from <= char_persons.monthly_income");
                    }
                    elseif($monthly_income_to != null && $monthly_income_from == null) {
                        $Squery->whereRaw(" $monthly_income_to >= char_persons.monthly_income");
                    }
                    elseif($monthly_income_to == null && $monthly_income_from != null) {
                        $Squery->whereRaw(" $monthly_income_from <= char_persons.monthly_income");
                    }
                }

                if((isset($filters['max_family_count']) && $filters['max_family_count'] !=null && $filters['max_family_count'] !="")||
                    (isset($filters['min_family_count']) && $filters['min_family_count'] !=null && $filters['min_family_count'] !="")){

                    $max_family_count=null;
                    $min_family_count=null;

                    if(isset($filters['max_family_count']) && $filters['max_family_count'] !=null && $filters['max_family_count'] !=""){
                        $max_family_count=$filters['max_family_count'];
                    }
                    if(isset($filters['min_family_count']) && $filters['min_family_count'] !=null && $filters['min_family_count'] !=""){
                        $min_family_count=$filters['min_family_count'];
                    }
                    if($min_family_count != null && $max_family_count != null) {
                        $Squery->whereRaw(" ( char_persons.family_cnt between ? and  ?)", array($min_family_count, $max_family_count));
                    }elseif($max_family_count != null && $min_family_count == null) {
                        $Squery->whereRaw(" $max_family_count >= char_persons.family_cnt");
                    }elseif($max_family_count == null && $min_family_count != null) {
                        $Squery->whereRaw(" $min_family_count <= char_persons.family_cnt");
                    }
                }

                if(isset($filters['Properties'])) {
                    for ($x = 0; $x < sizeof($filters['Properties']); $x++) {
                        $item=$filters['Properties'][$x];
                        if(isset($item['exist'])){
                            if($item['exist'] != "") {
                                $has_property=(int)$item['exist'];

                                if($has_property == 1){
                                    $op = '=';
                                    $has_property_val = 1;

                                    $Squery->whereNotIn('char_persons.id', function ($query) use ($item) {
                                           $query->select('person_id')
                                                 ->from('char_persons_properties')
                                                 ->where(function ($sq) use ($item){
                                                       $sq->where('has_property', 1)
                                                           ->where('property_id', $item['id']);
                                                 });
                                       });
                                }else{
                                    $op = '!=';
                                    $has_property_val = 1; 

                                    $Squery->whereNotIn('char_persons.id', function ($query) use ($item) {
                                           $query->select('person_id')
                                                 ->from('char_persons_properties')
                                                 ->where(function ($sq) use ($item){
                                                       $sq->where('has_property', 1)
                                                           ->where('property_id', $item['id']);
                                                 });
                                       });
                                }
                            }
                        }

                    }
                }
                
                if(isset($filters['AidSources'])) {
                    for ($x = 0; $x < sizeof($filters['AidSources']); $x++) {
                        $item=$filters['AidSources'][$x];
                        if(isset($item['aid_take'])){
                            if($item['aid_take'] != "") {
                                $aid_take=(int)$item['aid_take'];
                                if($aid_take == 1){
                                    $op = '=';
                                    $aid_take_val = 1;
                                    $Squery->whereIn('char_persons.id', function ($query) use ($item) {
                                           $query->select('person_id')
                                                 ->from('char_persons_aids')
                                                 ->where(function ($sq) use ($item){
                                                       $sq->where('aid_take','=', 1)
                                                           ->where('aid_source_id', $item['id']);
                                                 });
                                       });

                                }else{
                                    $op = '!=';
                                    $aid_take_val = 1;  
                                     $Squery->whereNotIn('char_persons.id', function ($query) use ($item) {
                                           $query->select('person_id')
                                                 ->from('char_persons_aids')
                                                 ->where(function ($sq) use ($item){
                                                       $sq->where('aid_take','=', 1)
                                                           ->where('aid_source_id', $item['id']);
                                                 });
                                       });

                                }
                            }
                        }

                    }
                }
                
               if((isset($filters['max_total_vouchers']) && $filters['max_total_vouchers'] !=null && $filters['max_total_vouchers'] !="")||
                  (isset($filters['min_total_vouchers']) && $filters['min_total_vouchers'] !=null && $filters['min_total_vouchers'] !="")){

                $max_total_vouchers=null;
                $min_total_vouchers=null;
                if(isset($filters['max_total_vouchers']) && $filters['max_total_vouchers'] !=null && $filters['max_total_vouchers'] !=""){
                    $max_total_vouchers=$filters['max_total_vouchers'];
                }
                if(isset($filters['min_total_vouchers']) && $filters['min_total_vouchers'] !=null && $filters['min_total_vouchers'] !=""){
                    $min_total_vouchers=$filters['min_total_vouchers'];
                }

                if($min_total_vouchers != null && $max_total_vouchers != null) {
                    $Squery->whereRaw(" ( char_get_sponsor_voucher_person_amount ( char_persons.id ,'$id','$first', '$last') between ? and  ?)", array($min_total_vouchers, $max_total_vouchers));
                }
                elseif($max_total_vouchers != null && $min_total_vouchers == null) {
                    $Squery->whereRaw(" $max_total_vouchers >= char_get_sponsor_voucher_person_amount ( char_persons.id ,'$id','$first', '$last')");
                }
                elseif($max_total_vouchers == null && $min_total_vouchers != null) {
                    $Squery->whereRaw(" $min_total_vouchers <= char_get_sponsor_voucher_person_amount ( char_persons.id ,'$id','$first', '$last')");
                }
            }

             if($filters['action'] =='ExportToExcel' || $filters['action'] =='ExportToExcel_'  || $filters['action'] =='csv') {

               if(isset($filters['persons'])){
                if(sizeof($filters['persons']) > 0 ){
                       $Squery->whereIn('char_persons.id',$filters['persons']);
                  } 
                    }  
                    
            }

        });

        
        $map = [
            "name" => 'char_persons.first_name',
            "id_card_number" => 'char_persons.id_card_number',
            "gender" => 'char_persons.gender',
            "case_gender" => 'char_persons.gender',
            "marital_status" => 'char_marital_status.name'

        ];
            $map['region_name'] = 'region_name.name';
            $map['governarate'] = 'district_name.name';
       
        $order = false;

        if (isset($filters['sortKeyArr_rev']) && $filters['sortKeyArr_rev'] != null && $filters['sortKeyArr_rev'] != "" &&
            $filters['sortKeyArr_rev'] != [] && sizeof($filters['sortKeyArr_rev']) != 0
        ) {

            $order = true;

            foreach ($filters['sortKeyArr_rev'] as $key_) {
                if($key_ == 'name' || $key_ == 'full_name'){
                    $result->orderBy('char_persons.first_name','desc');
                    $result->orderBy('char_persons.second_name','desc');
                    $result->orderBy('char_persons.third_name','desc');
                    $result->orderBy('char_persons.last_name','desc');
                }else{
                    $result->orderBy($map[$key_],'desc');
                }
            }
        }


        if (isset($filters['sortKeyArr_un_rev']) && $filters['sortKeyArr_un_rev'] != null && $filters['sortKeyArr_un_rev'] != "" &&
            $filters['sortKeyArr_un_rev'] != [] && sizeof($filters['sortKeyArr_un_rev']) != 0
        ) {

            $order = true;
            foreach ($filters['sortKeyArr_un_rev'] as $key_) {
                if($key_ == 'name' || $key_ == 'full_name'){
                    $result->orderBy('char_persons.first_name','asc');
                    $result->orderBy('char_persons.second_name','asc');
                    $result->orderBy('char_persons.third_name','asc');
                    $result->orderBy('char_persons.last_name','asc');
                }elseif($key_ == 'guardian_name'){
                    $result->orderBy('g.first_name','asc');
                    $result->orderBy('g.second_name','asc');
                    $result->orderBy('g.third_name','asc');
                    $result->orderBy('g.last_name','asc');
                }else{
                    $result->orderBy($map[$key_],'asc');
                }
            }
        }

        if(!$order){
            $result->orderBy('char_persons.first_name','char_persons.second_name','char_persons.third_name','char_persons.last_name');
        }

        
         $result->selectRaw("char_persons.id as person_id,
                                        CASE
                                              WHEN char_persons.card_type is null THEN ' '
                                              WHEN char_persons.card_type = 1 THEN '$id_card'
                                              WHEN char_persons.card_type = 2 THEN '$id_number'
                                              WHEN char_persons.card_type = 3 THEN '$passport'
                                        END
                                        AS card_type,
                                        CASE WHEN char_persons.id_card_number is null THEN ' ' Else char_persons.id_card_number  END  AS id_card_number,
                                        CASE WHEN char_persons.old_id_card_number is null THEN ' ' Else char_persons.old_id_card_number  END  AS old_id_card_number,
                                        CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,ifnull(char_persons.second_name, ' '),' ',ifnull(char_persons.third_name, ' '),' ', ifnull(char_persons.last_name,' ')) AS full_name,
                                        CONCAT(ifnull(char_persons_i18n.first_name, ' '), ' ' ,ifnull(char_persons_i18n.second_name, ' '),' ',ifnull(char_persons_i18n.third_name, ' '),' ', ifnull(char_persons_i18n.last_name,' ')) AS en_name,
                                        CASE WHEN char_persons.gender is null THEN ' '
                                             WHEN char_persons.gender = 1 THEN '$male'
                                             WHEN char_persons.gender = 2 THEN '$female'
                                        END AS gender,
                                        CASE WHEN  char_persons.birthday is null THEN ' ' Else 
                                             EXTRACT(YEAR FROM (FROM_DAYS(DATEDIFF(NOW(), char_persons.birthday)))) END AS age ,
                                        CASE WHEN char_persons.marital_status_id is null THEN ' ' Else char_marital_status.name  END  AS marital_status ,
                                        CASE WHEN char_persons.deserted is null THEN ' '
                                             WHEN char_persons.deserted = 1 THEN '$yes'
                                             WHEN char_persons.deserted = 0 THEN '$no'
                                        END
                                        AS deserted,
                                        char_persons.family_cnt AS family_count,
                                        char_persons.spouses AS spouses,
                                        char_persons.male_live AS male_count,
                                        char_persons.female_live AS female_count,
                                        CASE WHEN char_persons.adscountry_id  is null THEN ' ' Else country_name.name END   AS country,
                                        CASE WHEN char_persons.adsdistrict_id  is null THEN ' ' Else district_name.name END   AS governarate ,
                                        CASE WHEN char_persons.adsregion_id  is null THEN ' ' Else region_name.name END   AS region_name,
                                        CASE WHEN char_persons.adsneighborhood_id is null THEN ' ' Else location_name.name END   AS nearLocation,
                                        CASE WHEN char_persons.adssquare_id is null THEN ' ' Else square_name.name END   AS square_name,
                                        CASE WHEN char_persons.adsmosques_id is null THEN ' ' Else mosques_name.name END   AS mosque_name,
                                        CASE WHEN char_persons.street_address is null THEN ' ' Else char_persons.street_address END   AS street_address,
                                        CONCAT(ifnull(country_name.name,' '),' _ ',
                                                            ifnull(district_name.name,' '),'  ',
                                                            ifnull(region_name.name,' '),'  ',
                                                            ifnull(location_name.name,' '),'  ',
                                                            ifnull(square_name.name,' '),'  ',
                                                            ifnull(mosques_name.name,' '),' ',
                                                            ifnull(char_persons.street_address,' '))
                                            AS address , 
                                            CASE WHEN char_get_sponsor_voucher_person_amount ( char_persons.id ,'$id','$first', '$last') is null THEN 0
                                                 Else char_get_sponsor_voucher_person_amount ( char_persons.id ,'$id','$first', '$last')
                                             END AS voucher_count ,
                                       ");
                        
                    if($filters['action'] =='ExportToExcel' || $filters['action'] =='ExportToExcel_'  || $filters['action'] =='csv') {
                            $result->selectRaw("CASE WHEN char_persons.birth_place is null THEN ' ' Else L.name END AS birth_place,
                                                CASE WHEN char_persons.nationality is null THEN ' ' Else L0.name END AS nationality");
                    }
                                        
                    $result->selectRaw("CASE WHEN char_persons.receivables = 1 THEN '$yes' Else '$no' END AS receivables,
                                        CASE
                                             WHEN char_persons.receivables is null THEN '0'
                                             WHEN char_persons.receivables = 0 THEN '0'
                                             WHEN char_persons.receivables = 1 THEN char_persons.receivables_sk_amount
                                        END AS receivables_sk_amount ,
                                        CASE WHEN char_persons.monthly_income is null THEN ' ' Else char_persons.monthly_income  END  AS monthly_income,
                                        CASE WHEN char_persons.actual_monthly_income is null THEN ' ' Else char_persons.actual_monthly_income  END  AS actual_monthly_income,
                                        CASE
                                           WHEN char_persons.refugee is null THEN ' '
                                           WHEN char_persons.refugee = 0 THEN ' '
                                           WHEN char_persons.refugee = 1 THEN ' $citizen '
                                           WHEN char_persons.refugee = 2 THEN '$refugee'
                                     END
                                     AS refugee,
                                     CASE
                                           WHEN char_persons.refugee is null THEN ' '
                                           WHEN char_persons.refugee = 0 THEN ' '
                                           WHEN char_persons.refugee = 1 THEN ' '
                                           WHEN char_persons.refugee = 2 THEN char_persons.unrwa_card_number
                                     END
                                     AS unrwa_card_number");
                
        if($filters['action'] =='ExportToExcel' || $filters['action'] =='ExportToExcel_'  || $filters['action'] =='csv') {
            
            return $result->selectRaw("
                                   CASE
                                                          WHEN char_persons_health.condition is null THEN ' '
                                                          WHEN char_persons_health.condition = 1 THEN '$perfect'
                                                          WHEN char_persons_health.condition = 2 THEN '$suffers_from_a_chronic_disease'
                                                          WHEN char_persons_health.condition = 3 THEN '$special_needs'
                                                     END
                                                     AS health_status,
                                                     CASE WHEN char_persons_health.details is null THEN ' ' Else char_persons_health.details END   AS details,
                                                     CASE WHEN char_diseases_i18n.name is null THEN ' ' Else char_diseases_i18n.name END   AS disease_id,
                                                     CASE
                                                          WHEN char_persons_health.has_health_insurance is null THEN ' '
                                                          WHEN char_persons_health.has_health_insurance = 1 THEN '$yes'
                                                          WHEN char_persons_health.has_health_insurance = 0 THEN '$no'
                                                     END
                                                     AS has_health_insurance,
                                                     CASE WHEN char_persons_health.health_insurance_number is null THEN ' ' Else char_persons_health.health_insurance_number END   AS health_insurance_number,
                                                     CASE WHEN char_persons_health.health_insurance_type is null THEN ' ' Else char_persons_health.health_insurance_type END   AS health_insurance_type,
                                                     CASE
                                                          WHEN char_persons_health.has_device is null THEN ' '
                                                          WHEN char_persons_health.has_device = 1 THEN '$yes'
                                                          WHEN char_persons_health.has_device = 0 THEN '$no'
                                                     END
                                                     AS has_device,
                                                     CASE WHEN char_persons_health.used_device_name is null THEN ' ' Else char_persons_health.used_device_name END   AS used_device_name,
                                                     CASE
                                       WHEN char_persons_work.working is null THEN ' '
                                       WHEN char_persons_work.working = 1 THEN '$yes'
                                       WHEN char_persons_work.working = 2 THEN '$no'
                                     END
                                     AS working,
                                     CASE
                                           WHEN char_persons_work.working is null THEN ' '
                                           WHEN char_persons_work.working = 1 THEN work_status.name
                                           WHEN char_persons_work.working = 2 THEN ' '
                                     END
                                     AS work_status,
                                     CASE
                                           WHEN char_persons_work.working is null THEN ' '
                                           WHEN char_persons_work.working = 1 THEN work_job.name
                                           WHEN char_persons_work.working = 2 THEN ' '
                                     END
                                     AS work_job,
                                     CASE
                                           WHEN char_persons_work.working is null THEN ' '
                                           WHEN char_persons_work.working = 1 THEN work_wage.name
                                           WHEN char_persons_work.working = 2 THEN ' '
                                     END
                                     AS work_wage,
                                     CASE
                                           WHEN char_persons_work.working is null THEN ' '
                                           WHEN char_persons_work.working = 1 THEN char_persons_work.work_location
                                           WHEN char_persons_work.working = 2 THEN ' '
                                     END
                                     AS work_location,
                                     CASE
                                           WHEN char_persons_work.can_work is null THEN ' '
                                           WHEN char_persons_work.can_work = 1 THEN '$yes'
                                           WHEN char_persons_work.can_work = 2 THEN '$no'
                                     END
                                     AS can_work,
                                     CASE
                                           WHEN char_persons_work.can_work is null THEN ' '
                                           WHEN char_persons_work.can_work = 1 THEN ' '
                                           WHEN char_persons_work.can_work = 2 THEN work_reason.name
                                     END
                                     AS work_reason,
                                     CASE WHEN char_persons_work.has_other_work_resources is null THEN ' '
                                          WHEN char_persons_work.has_other_work_resources = 1 THEN '$yes'
                                          WHEN char_persons_work.has_other_work_resources = 0 THEN '$no'
                                     END AS has_other_work_resources,
                                     CASE WHEN char_building_status_i18n.name is null THEN ' ' Else char_building_status_i18n.name  END  AS house_condition,
                                     CASE WHEN char_furniture_status_i18n.name is null THEN ' ' Else char_furniture_status_i18n.name  END  AS indoor_condition,
                                     CASE WHEN char_habitable_status_i18n.name is null THEN ' ' Else char_habitable_status_i18n.name  END  AS habitable,
                                     CASE WHEN char_house_status_i18n.name is null     THEN ' ' Else char_house_status_i18n.name  END  AS residence_condition,
                                     CASE WHEN char_property_types_i18n.name is null   THEN ' ' Else char_property_types_i18n.name  END  AS property_types,
                                     CASE WHEN char_roof_materials_i18n.name is null   THEN ' ' Else char_roof_materials_i18n.name  END  AS roof_materials,
                                     CASE WHEN char_residence.area is null             THEN ' ' Else char_residence.area  END  AS area,
                                     CASE WHEN char_residence.rooms is null            THEN ' ' Else char_residence.rooms  END  AS rooms,
                                     CASE WHEN char_residence.rent_value is null       THEN ' ' Else char_residence.rent_value  END  AS rent_value,
                                     CASE WHEN char_residence.need_repair is null THEN ' '
                                          WHEN char_residence.need_repair = 1 THEN '$yes'
                                          WHEN char_residence.need_repair = 2 THEN '$no'
                                        END AS need_repair ,
                                     CASE WHEN char_residence.repair_notes is null   THEN ' ' Else char_residence.repair_notes  END  AS repair_notes
                                                     
                                   ")->get();
            
        }
        
        
        $itemsCount = isset($filters['itemsCount'])? $filters['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$result->count());
            
            return $result->paginate($records_per_page);

       
    }

}