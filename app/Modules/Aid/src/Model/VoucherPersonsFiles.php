<?php

namespace Aid\Model;


class VoucherPersonsFiles  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_vouchers_persons_files';
    protected $fillable = [ 'voucher_id','file_id','user_id'];


    public function voucher()
    {
        return $this->belongsTo('Aid\Model\Vouchers','voucher_id','id');
    }

    public function file()
    {
        return $this->belongsTo('Document\Model\File','document_id','id');
    }

    public function user()
    {
        return $this->belongsTo('Auth\Model\User','user_id','id');
    }

    public static function filter($voucher_id)
    {
        return self::with(['user','file'])
                   ->where('voucher_id', '=', $voucher_id)
                   ->orderBy('created_at','desc')
                   ->get();
    }
}