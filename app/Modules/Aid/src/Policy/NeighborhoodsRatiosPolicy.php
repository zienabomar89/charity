<?php

namespace Aid\Policy;
use Auth\Model\User;
use Aid\Model\neighborhoodsRatios;

class neighborhoodsRatiosPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('aid.neighborhoodsRatios.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('aid.neighborhoodsRatios.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, neighborhoodsRatios $neighborhoodsRatios = null)
    {
        if ($user->hasPermission('aid.neighborhoodsRatios.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, neighborhoodsRatios $neighborhoodsRatios = null)
    {
        if ($user->hasPermission('aid.neighborhoodsRatios.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, neighborhoodsRatios $neighborhoodsRatios = null)
    {
        if ($user->hasPermission(['aid.neighborhoodsRatios.view','aid.neighborhoodsRatios.update'])) {
            return true;
        }

        return false;
    }

    public function upload(User $user, neighborhoodsRatios $neighborhoodsRatios = null)
    {
        if ($user->hasPermission(['aid.neighborhoodsRatios.view','aid.neighborhoodsRatios.update'])) {
            return true;
        }

        return false;
    }}

