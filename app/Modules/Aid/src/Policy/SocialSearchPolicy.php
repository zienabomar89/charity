<?php

namespace Aid\Policy;
use Auth\Model\User;
use Aid\Model\SocialSearch\SocialSearch;

class SocialSearchPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('org.SocialSearch.manage')) {
            return true;
        }

        return false;
    }

    public function manageCases(User $user)
    {
        if ($user->hasPermission('aid.SocialSearch.manageCases')) {
            return true;
        }

        return false;
    }

    public function templates(User $user)
    {
        if ($user->hasPermission('aid.SocialSearch.templates')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('org.SocialSearch.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, SocialSearch $search = null)
    {
        if ($user->hasPermission('org.SocialSearch.update')) {
            return true;
        }

        return false;
    }


    public function view(User $user, SocialSearch $search = null)
    {
        if ($user->hasPermission(['org.SocialSearch.view','org.SocialSearch.update'])) {
            return true;
        }

        return false;
    }

    public function delete(User $user, SocialSearch $search = null)
    {
        if ($user->hasPermission('org.SocialSearch.delete')) {
            return true;
        }

        return false;
    }

    public function import(User $user, SocialSearch $search = null)
    {
        if ($user->hasPermission('org.SocialSearch.import')) {
            return true;
        }

        return false;
    }

}

