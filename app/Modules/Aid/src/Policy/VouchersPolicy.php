<?php

namespace Aid\Policy;
use Auth\Model\User;
use Aid\Model\Vouchers;

class VouchersPolicy
{

    public function center(User $user)
    {
        if ($user->hasPermission('aid.voucher.center')) {
            return true;
        }

        return false;
    }

    public function manage(User $user)
    {
        if ($user->hasPermission('aid.voucher.manage')) {
            return true;
        }

        return false;
    }

    public function managePersonsVoucher(User $user)
    {
        if ($user->hasPermission('aid.personsVoucher.manage')) {
            return true;
        }

        return false;
    }

    public function updateSerial(User $user)
    {
        if ($user->hasPermission('aid.voucher.updateSerial')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('aid.voucher.create')) {
            return true;
        }

        return false;
    }
    public function createPersonsVoucher(User $user)
    {
        if ($user->hasPermission('aid.personsVoucher.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, Vouchers $voucher = null)
    {
        if ($user->hasPermission('aid.voucher.update')) {
            return true;
        }

        return false;
    }
    public function updatePersonsVoucher(User $user, Vouchers $voucher = null)
    {
        if ($user->hasPermission('aid.personsVoucher.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, Vouchers $voucher = null)
    {
        if ($user->hasPermission('aid.voucher.delete')) {
            return true;
        }

        return false;
    }
    public function deletePersonsVoucher(User $user, Vouchers $voucher = null)
    {
        if ($user->hasPermission('aid.personsVoucher.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, Vouchers $voucher = null)
    {
        if ($user->hasPermission(['aid.voucher.view','aid.voucher.update'])) {
            return true;
        }

        return false;
    }
    public function viewPersonsVoucher(User $user, Vouchers $voucher = null)
    {
        if ($user->hasPermission(['aid.personsVoucher.view','aid.personsVoucher.update'])) {
            return true;
        }

        return false;
    }

    public function export(User $user, Vouchers $voucher = null)
    {
        if ($user->hasPermission('aid.voucher.export')) {
            return true;
        }

        return false;
    }
    public function exportPersonsVoucher(User $user, Vouchers $voucher = null)
    {
        if ($user->hasPermission('aid.personsVoucher.export')){
            return true;
        }

        return false;
    }

    public function upload(User $user, Vouchers $voucher = null)
    {
        if ($user->hasPermission('aid.voucher.upload')) {
            return true;
        }

        return false;
    }

    public function sentSms(User $user, Vouchers $voucher = null)
    {
        if ($user->hasPermission('aid.voucher.sentSms')) {
            return true;
        }

        return false;
    }
}

