<?php

namespace Aid\Policy;
use Auth\Model\User;
use Aid\Model\VoucherCategories;
class VoucherCategoriesPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('aid.voucherCategory.manage')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('aid.voucherCategory.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, VoucherCategories $voucherCategory = null)
    {
        if ($user->hasPermission('aid.voucherCategory.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, VoucherCategories $voucherCategory = null)
    {
        if ($user->hasPermission('aid.voucherCategory.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, VoucherCategories $voucherCategory = null)
    {
        if ($user->hasPermission(['aid.voucherCategory.view','aid.voucherCategory.update'])) {
            return true;
        }

        return false;
    }

}

