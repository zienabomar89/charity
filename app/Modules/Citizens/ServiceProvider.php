<?php

namespace App\Modules\Citizens;

class ServiceProvider extends \App\Providers\AuthServiceProvider
{
    
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        \Citizens\Model\CitizenRequest::class => \Citizens\Policy\CitizenRequestPolicy::class,

    ];
    
    public function boot()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/config/routes.php';
        }
        $this->loadViewsFrom(__DIR__ . '/views', 'citizens');
        $this->loadTranslationsFrom(__DIR__.'/languages', 'citizens');
        
        require __DIR__ . '/config/autoload.php';
        
        $this->registerPolicies();
    }

    public function register()
    {
        /*$this->mergeConfigFrom(
            __DIR__ . '/config/config.php', 'auth'
        );*/
    }
}
