<?php

namespace Citizens\Controller;

use App\Http\Controllers\Controller;

class EntitiesController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth:api');
        \Application\Model\I18nableEntity::setGlobalLanguageId(1);
    }

    // get constant entities
    public function index()
    {
        $result = [];
        $entities = explode(',', request()->query('c'));
        foreach ($entities as $entity) {
            if (($data = $this->get($entity)) !== false) {
                $result[$entity] = $data;
            }
        }
        
        return response()->json($result);
    }

    // get constant model entities by name
    protected function get($name)
    {
        $entities = [
            'aidSources' => function() { return \Setting\Model\AidSource::all(); },
            'banks' => function() { return \Setting\Model\Bank::all(); },
            'currencies' => function() { return \Setting\Model\Currency::all(); },
            'deathCauses' => function() { return \Setting\Model\DeathCause::i18n()->get(); },
            'diseases' => function() { return \Setting\Model\Disease::i18n()->get(); },
            'documentTypes' => function() { return \Setting\Model\DocumentType::i18n()->get(); },
            'educationAuthorities' => function() { return \Setting\Model\Education\Authority::i18n()->get(); },
            'educationDegrees' => function() { return \Setting\Model\Education\Degree::i18n()->get(); },
            'educationStages' => function() { return \Setting\Model\Education\Stage::i18n()->get(); },
            'essentials' => function() { return \Setting\Model\Essential::all(); },
            'maritalStatus' => function() { return \Setting\Model\MaritalStatus::i18n()->get(); },
            'kinship' => function() { return \Setting\Model\Kinship::i18n()->get(); },
            'languages' => function() { return \Setting\Model\Language::all(); },
            'countries' => function() { return \Setting\Model\Location::i18n()->countries()->get(); },
            'aidCountries' => function() { return \Setting\Model\aidsLocation::i18n()->countries()->get(); },
            'properties' => function() { return \Setting\Model\Property::i18n()->get(); },
            'propertyTypes' => function() { return \Setting\Model\PropertyType::i18n()->get(); },
            'roofMaterials' => function() { return \Setting\Model\RoofMaterial::i18n()->get(); },
            'furnitureStatus' => function() { return \Setting\Model\FurnitureStatus::i18n()->get(); },
            'houseStatus' => function() { return \Setting\Model\HouseStatus::i18n()->get(); },
            'habitableStatus' => function() { return \Setting\Model\HabitableStatus::i18n()->get(); },
            'buildingStatus' => function() { return \Setting\Model\BuildingStatus::i18n()->get(); },
            'workJobs' => function() { return \Setting\Model\Work\Job::i18n()->get(); },
            'workReasons' => function() { return \Setting\Model\Work\Reason::i18n()->get(); },
            'workStatus' => function() { return \Setting\Model\Work\Status::i18n()->get(); },
            'transferCompany' => function() { return \Setting\Model\TransferCompany::all(); },
            'paymentCategory' => function() { return \Setting\Model\PaymentCategory::i18n()->get(); },
            'workWages' => function() { return \Setting\Model\Work\Wage::all(); },
        ];


        if (!array_key_exists($name, $entities)) {
            return false;
        }
        
        return call_user_func($entities[$name]);
    }
}