<?php

namespace Citizens\Controller;

use function GuzzleHttp\Promise\all;
use Organization\Model\OrgLocations;
use Setting\Model\aidsLocation;
use Setting\Model\aidsLocationI18n;

class aidsLocationsController extends AbstractController
{

    public function __construct()
    {
        $language_id = 1;
        aidsLocation::setGlobalLanguageId($language_id);
        parent::__construct();
    }

    // initialize aidsLocation model object to use in process
    protected function newModel()
    {
        $aidsLocation = new aidsLocation();
        $routeName = request()->route()->getName();
        if ($routeName == 'scountries.store') {
            $aidsLocation->Location_type = aidsLocation::TYPE_COUNTRY;
        }
        else if ($routeName == 'sdistricts.store') {
            $aidsLocation->Location_type = aidsLocation::TYPE_DISTRICT;
        }
        else if ($routeName == 'sregions.store') {
            $aidsLocation->Location_type = aidsLocation::TYPE_REGION;
        }
        else if ($routeName == 'sneighborhoods.store') {
            $aidsLocation->Location_type = aidsLocation::TYPE_NEIGHBORHOOD;
        }
        else if ($routeName == 'ssquares.store') {
            $aidsLocation->Location_type = aidsLocation::TYPE_SQUARE;
        }
        else if ($routeName == 'smosques.store') {
            $aidsLocation->Location_type = aidsLocation::TYPE_MOSQUES;
        }
        return $aidsLocation;
    }

    // get aidsLocation row using id
    protected function findOrFail($id)
    {
        $entry = aidsLocation::i18n()->findOrFail($id);
        return $entry;
    }

    // get aidsLocation rows searching by name (according location type)
    public function index($id = null)
    {
        $routeName = request()->route()->getName();
        if ($routeName == 'scountries.index') {
            return $this->countries();
        }
        else if ($routeName == 'sdistricts.index') {
            return $this->districts($id);
        }
        else if ($routeName == 'sregions.index') {
            return $this->regions($id);
        }
        else if ($routeName == 'sneighborhoods.index') {
            return $this->neighborhoods($id);
        }
        else if ($routeName == 'ssquares.store') {
            return $this->squares($id);
        }
        else if ($routeName == 'smosques.index') {
            return $this->mosques($id);
        }
        $entries = aidsLocation::i18n()->descendants($id)->paginate();
        return response()->json($entries);
    }

    // get all aidsLocation rows (according location type)
    public function getList($id = null)
    {
        $routeName = request()->route()->getName();

        if ($routeName == 'scountries.list') {
            return $this->countries();
        }
        else if ($routeName == 'sdistricts.list') {
            return $this->districts($id);
        }
        else if ($routeName == 'sregions.list') {
            return $this->regions($id);
        }
        else if ($routeName == 'sneighborhoods.list') {
            return $this->neighborhoods($id);
        }
         else if ($routeName == 'ssquares.list') {
            return $this->squares($id);
        }
        else if ($routeName == 'smosques.list') {
            return $this->neighborhoods($id);
        }

        $entries = aidsLocation::i18n()->descendants($id)->get();
        return response()->json($entries);
    }

    // get all aidsLocation rows (according location type)
    public function typeList($type)
    {
        $entries = aidsLocation::i18n()->where('Location_type', '=', $type)->get();
        return response()->json($entries);
    }

    // create new aidsLocation model (according location type)
    public function store(\Illuminate\Http\Request $request)
    {
        $this->authorize('create', aidsLocation::class);
        $rules = aidsLocation::getValidatorRules();
        $routeName = request()->route()->getName();
        if ($routeName == 'scountries.store') {
            unset($rules['parent_id']);
        }

        $this->validate($request, $rules);

        $where = array('parent_id'=> $request->parent_id);

        if ($routeName == 'scountries.store') {
            $where['parent_id'] = Null;
            $where['location_type'] = aidsLocation::TYPE_COUNTRY;
        }else if ($routeName == 'sdistricts.store') {
            $where['location_type'] = aidsLocation::TYPE_DISTRICT;
        }
        else if ($routeName == 'sregions.store') {
            $where['location_type'] = aidsLocation::TYPE_REGION;
        }
        else if ($routeName == 'sneighborhoods.store') {
            $where['location_type'] = aidsLocation::TYPE_NEIGHBORHOOD;
        }
        else if ($routeName == 'ssquares.store') {
            $where['location_type'] = aidsLocation::TYPE_SQUARE;
        }
        else if ($routeName == 'smosques.store') {
            $where['location_type'] = aidsLocation::TYPE_MOSQUES;
        }

        $sum =aidsLocation::where($where)->sum('ratio');
        $ratio = $request->ratio;
        if(($ratio + $sum) > 100){
            return response()->json(array('error' => trans('common::application.Total input ratios must be equal 100% for the current tree level')), 422);
        }


        $entity = $this->newModel();
        if ($entity instanceof \Application\Model\I18nableInterface) {
            $entity->setLanguageId(1);
            $entity->setI18nAttributes(array(
                'name' => $request->input('name'),
            ));
        } else {
            $entity->name = $request->input('name');
        }

        if ($entity instanceof \Setting\Model\RankEntity) {
            if(!$request->input('weight') or is_null(!$request->input('weight'))){
                $entity->weight = 0;
            }else{
                $entity->weight = $request->input('weight');
            }
        }

        if($request->input('ratio')){
            $entity->ratio = $request->input('ratio');
        }

        if (method_exists($entity, 'setAttributesFromRequest')) {
            $entity->setAttributesFromRequest($request);
        }

        $entity->saveOrFail();
        if ($entity instanceof \Application\Model\I18nableInterface) {
            $entity->name = $request->input('name');
        }

        OrgLocations::create([ 'organization_id' => 1 ,'location_id' => $entity->id , 'created_at' => date('Y-m-d H:i:s')]);
        return response()->json($entity);

    }

    // get countries from aidsLocation rows searching by name
    public function countries()
    {
        $entries = aidsLocation::i18n()->countries();
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get districts from aidsLocation rows searching by name , id (parent)
    function districts($id = null)
    {
        $entries = aidsLocation::i18n()->districts($id);
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get regions from aidsLocation rows searching by name , id (parent)
    public function regions($id = null)
    {
        $entries = aidsLocation::i18n()->regions($id);
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get neighborhoods from aidsLocation rows searching by name , id (parent)
    public function neighborhoods($id = null)
    {
        $entries = aidsLocation::i18n()->neighborhoods($id);
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get mosques from aidsLocation models searching by name , id (parent)
    public function mosques($id = null)
    {
        $entries = aidsLocation::i18n()->mosques($id);
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get squares from aidsLocation models searching by name , id (parent)
    public function squares($id = null)
    {
        $entries = aidsLocation::i18n()->squares($id);
        $name = trim(request()->input('name'));
        if (!empty($name)) {
            $entries->whereRaw("normalize_text_ar(name) like  normalize_text_ar(?)", "%{$name}%");
        }
        return response()->json($entries->paginate());
    }

    // get countries list from aidsLocation models searching id (parent)
    public function countrieslist()
    {
        $entries = aidsLocation::i18n()->countries()->get();
        return response()->json($entries);
    }

    // get districts list from aidsLocation models searching id (parent)
    public function districtsList($id = null)
    {
        $entries = aidsLocation::i18n()->districts($id)->get();
        return response()->json($entries);
    }

    // get regions list from aidsLocation models searching id (parent)
    public function regionsList($id = null)
    {
        $entries = aidsLocation::i18n()->regions($id)->get();
        return response()->json($entries);
    }

    // get neighborhoods list from aidsLocation models searching id (parent)
    public function neighborhoodsList($id = null)
    {
        $entries = aidsLocation::i18n()->neighborhoods($id)->get();
        return response()->json($entries);
    }

    // get squares list from aidsLocation models searching id (parent)
    public function squaresList($id = null)
    {
        $entries = aidsLocation::i18n()->squares($id)->get();
        return response()->json($entries);
    }

    // get mosques list from aidsLocation models searching id (parent)
    public function mosquesList($id = null)
    {
        $entries = aidsLocation::i18n()->mosques($id)->get();
        return response()->json($entries);
    }

}
