<?php

namespace Citizens\Controller;

use Aid\Model\VoucherPersons;
use Aid\Model\Vouchers;
use App\Http\Controllers\Controller;
use Citizens\Model\Citizen;
use Citizens\Model\CitizenRequest;
use Common\Model\GovServices;
use Common\Model\Person;
use Illuminate\Http\Request;

class CitizenController  extends Controller
{

    // check whether the card is correct, and if it is valid
    // check if the date of birth is correct or not for the card
    public function check(Request $request)
    {

        $error = \App\Http\Helpers::isValid($request->all(),['id_card_number'=> 'required','birthday'=> 'required|date']);
        if($error)
            return response()->json($error);

        $card = (int)$request->id_card_number;
        if (GovServices::checkCard($card)) {
            $row = GovServices::byCard($card);
            if($row['status'] != false){
                $person =  $row['row'];

                if(!is_null($person->DETH_DT)){
                    return response()->json(['status'=>false ,'msg'=>trans('common::application.this_card_for_dead_person')]);
                }
                $birthday =date('Y-m-d',strtotime(str_replace("/","-",$person->BIRTH_DT)));
                $birth_dt = date('Y-m-d',strtotime($request->birthday));
                $date = new \DateTime($birthday);
                $match_date = new \DateTime($birth_dt);
                $interval = $date->diff($match_date);
                if($interval->days > 0 ){
                    return response()->json(['status'=>false ,'msg'=>trans('common::application.the enter data is not compatible with citizen repository')]);
                }

                return response()->json(['status'=>true ,'person' => $person]);
            }
            return response()->json(['status'=>false ,'msg'=> $row['message']]);
        }
        return response()->json(['status'=>false ,'msg'=>trans('common::application.invalid_id_card_number')]);
    }

    // save the citizen request on database
    public function store(Request $request)
    {
        $rules = array("adscountry_id"=>'required', "adsdistrict_id"=>'required', "type"=>'required',"notes"=>'required',
                       "adsmosques_id"=>'required', "adsneighborhood_id"=>'required', "adsregion_id"=>'required',
                       "adssquare_id"=>'required', "id_card_number"=>'required',
                       "mobile"=>'required');

       $error = \App\Http\Helpers::isValid($request->all(),$rules);
        if($error)
            return response()->json($error);

        $card = (int)$request->id_card_number;
        if (GovServices::checkCard($card)) {
            $row = GovServices::byCard($card);
            if($row['status'] != false){
                $pass =  $request->get('pass');

                if($pass != 'valid'){
                    return response()->json(['status'=>false ,'msg'=>trans('common::application.please approved your data')]);

                }

                $person =  $row['row'];

                if(!is_null($person->DETH_DT)){
                    return response()->json(['status'=>false ,'msg'=>trans('common::application.this_card_for_dead_person')]);
                }
                $birthday =date('Y-m-d',strtotime(str_replace("/","-",$person->BIRTH_DT)));
                $birth_dt = date('Y-m-d',strtotime($request->birthday));
                $date = new \DateTime($birthday);
                $match_date = new \DateTime($birth_dt);
                $interval = $date->diff($match_date);
                if($interval->days > 0 ){
                    return response()->json(['status'=>false ,'msg'=>trans('common::application.the enter data is not compatible with citizen repository')]);
                }

                $citizen = Citizen::where('id_card_number',$card)->first();

                if(is_null($citizen)){
                    $citizen = new Citizen();
                    $output = \Common\Model\Person::PersonMap($person);

                    $citizen_data = ["first_name", "second_name", "third_name", "last_name", "prev_family_name",
                        "gender", "marital_status_id", "birthday"];

                    $value = array();
                    foreach ($citizen_data as $key){
                        if(isset($output[$key])){
                            $citizen->$key =$output[$key];
                        }
                    }
                }

                $fields = ["adscountry_id","adsdistrict_id", "wataniya","street_address","phone","id_card_number",
                    "adsmosques_id", "adsneighborhood_id", "adsregion_id",
                    "adssquare_id", "id_card_number","mobile"];

                foreach ($fields as $key){
                    if(isset($request->$key)){
                        $citizen->$key = $request->$key;
                    }
                }

                if($citizen->save()){
                    $citizen_request = new CitizenRequest();
                    $citizen_request->citizen_id =  $citizen->id;
                    $citizen_request->type =  $request->type;
                    $citizen_request->notes =  $request->notes;
                    if($citizen_request->save()){
                        return response()->json(['status'=>true , 'msg' => trans('application::application.action success')]);
                    }
                }
                return response()->json(['status'=>false , 'msg' => trans('common::application.An error occurred during during process')]);
            }
            return response()->json(['status'=>false ,'msg'=> $row['message']]);
        }
        return response()->json(['status'=>false ,'msg'=>trans('common::application.invalid_id_card_number')]);

    }

    // check whether the card is correct, and if it is valid
    // check if the date of birth is correct or not for the card
    // then get if citizen is beneficiary of linked voucher or not
    public function voucher(Request $request)
    {

        $error = \App\Http\Helpers::isValid($request->all(),['id_card_number'=> 'required','birthday'=> 'required|date']);
        if($error)
            return response()->json($error);

        $card = (int)$request->id_card_number;
        if (GovServices::checkCard($card)) {
            $row = GovServices::byCard($card);
            if($row['status'] != false){
                $person =  $row['row'];

                if(!is_null($person->DETH_DT)){
                    return response()->json(['status'=>false ,'msg'=>trans('common::application.this_card_for_dead_person')]);
                }
                $birthday =date('Y-m-d',strtotime(str_replace("/","-",$person->BIRTH_DT)));
                $birth_dt = date('Y-m-d',strtotime($request->birthday));
                $date = new \DateTime($birthday);
                $match_date = new \DateTime($birth_dt);
                $interval = $date->diff($match_date);
                if($interval->days > 0 ){
                    return response()->json(['status'=>false ,'msg'=>trans('common::application.the enter data is not compatible with citizen repository')]);
                }
                $existPerson = Person::where(function ($q) use ($card){
                                            $q->where('id_card_number',$card);
                                        })->first();
                if(!is_null($existPerson)){
                    $voucher = Vouchers::where(['token' => $request->token, 'valid_token'=> 1 ])->first();
                    if(!is_null($voucher)){
                        $personVoucher =VoucherPersons::getDetails($voucher->id,$existPerson->id);

                        if(sizeof($personVoucher) ==  0){
                            return response()->json(['status'=>false ,'msg'=>trans('common::application.You are not a beneficiary of the voucher')]);
                        }

                        $details = $personVoucher[0];

                        $details->receipt_time_ =   str_replace('PM', trans('common::application.PM'), $details->receipt_time_);
                        $details->receipt_time_ =   str_replace('AM', trans('common::application.AM'), $details->receipt_time_);

                        return response()->json(['status'=>true ,
                                                 'person'=>$person ,
                                                 'details'=>$details ,
                                                 'value' => $details->voucher_value *sizeof($personVoucher)  ,
                                                 'shekel_value' =>$details->shekel_value *sizeof($personVoucher)  ]);
                    }

                    return response()->json(['status'=>false ,'msg'=>trans('common::application.un_active_url') , 'redirect' => true]);
                }

                return response()->json(['status'=>false ,'msg'=>trans('common::application.person_not_register')]);
            }
            return response()->json(['status'=>false ,'msg'=> $row['message']]);
        }
        return response()->json(['status'=>false ,'msg'=>trans('common::application.invalid_id_card_number')]);
    }

}
