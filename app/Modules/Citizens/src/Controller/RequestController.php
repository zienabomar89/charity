<?php

namespace Citizens\Controller;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Citizens\Model\CitizenRequest;
use Excel;

class RequestController  extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // filter citizen request using many filter options
    public function filter(Request $request)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $this->authorize('manageCitizenRequest', \Setting\Model\BackupVersions::class);

        $items=CitizenRequest::filter($request->page, $request->all());
        if($request->action=='xlsx'){
            $data=array();
            foreach($items as $key =>$value){
                $data[$key][trans('aid::application.#')]=$key+1;
                foreach($value as $k =>$v){
                    $data[$key][trans('aid::application.' . $k)]= $v;
                }
            }
            $token = md5(uniqid());
            \Maatwebsite\Excel\Facades\Excel::create('export_' . $token, function($excel) use($data) {
                $excel->sheet(trans('common::application.request_sheet'),function($sheet) use($data) {
                    $sheet->setRightToLeft(true);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);
                    $sheet->setOrientation('landscape');
                    $sheet->setRightToLeft(true);
                    $sheet->setStyle([
                        'font' => [
                            'name' => 'Simplified Arabic',
                            'size' => 11,
                            'bold' => true
                        ]
                    ]);

                    $sheet->getStyle("A1:V1")->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      => [ 'name' =>'Simplified Arabic','size'=> 11, 'bold'=>  true ]
                    ]);
                    $sheet->fromArray($data);

                });
            })->store('xlsx', storage_path('tmp/'));
            return response()->json([
                'download_token' => $token,
            ]);
        }
        return response()->json($items);

    }

    // get citizen cases list using id_card_number
    public function cases(Request $request)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $items=\Common\Model\Person::cardCasesList($request->card);
        if($request->action =='xlsx') {

       }
        return response()->json(['cases'=>$items]);
    }

    // update request status  using request id
    public function status(Request $request)
    {
        $action = $request->action ;

        if($action =='single'){
            $id = $request->target ;
            $entity = CitizenRequest::findOrFail($id);
            $entity->status = $request->status;

            if($entity->saveOrFail()) {
//            \Log\Model\Log::saveNewLog('CITIZEN_REQUEST_STATUS_UPDATE', trans('sms::application.Edit the messages provider data')  . ' " '.$entry['Label']  . ' " ');
                $response["status"]= 'success';
                $response["msg"]= trans('sms::application.The row is updated');

            }else{
                $response["status"]= 'failed';
                $response["msg"]= trans('sms::application.There is no update');
            }
            return response()->json($response);
        }else{

            $requests = $request->requests;
            $entries = CitizenRequest::whereIn('id',$requests)->get();
            if(CitizenRequest::whereIn('id',$requests)->update(['status' => $request->status])) {
//            \Log\Model\Log::saveNewLog('CITIZEN_REQUEST_STATUS_UPDATE', trans('sms::application.Edit the messages provider data')  . ' " '.$entry['Label']  . ' " ');
                $response["status"]= 'success';
                $response["msg"]= trans('sms::application.The row is updated');

            }else{
                $response["status"]= 'failed';
                $response["msg"]= trans('sms::application.There is no update');
            }
            return response()->json($response);

        }
    }

    public function UpdateFromFile(Request $request)
    {

        $importFile = $request->file;
        if ($importFile->isValid()) {
            $path = $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load($path);
            $sheets = $excel->getSheetNames();

            if(in_array('data', $sheets)) {
                $first = Excel::selectSheets('data')->load($path)->first();
                if (!is_null($first)) {
                    $keys = $first->keys()->toArray();
                    if (sizeof($keys) > 0) {
                        $constraint = ["rkm_altlb"];
                        $validFile = true;
                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if ($validFile) {
                            $records = \Excel::selectSheets('data')->load($path)->get();
                            $total = sizeof($records);

                            if ($total > 0) {
                                $requests = [];
                                foreach ($records as $key => $value) {
                                    if($value['rkm_altlb']){
                                        if(!(is_null($value['rkm_altlb']) || $value['rkm_altlb'] ==' ' || $value['rkm_altlb'] =='')){
                                            $requests[] = $value['rkm_altlb'];
                                        }
                                    }
                                }

                                if(sizeof($requests) > 0){
                                    CitizenRequest::whereIn('id',$requests)->update(['status' => $request->status]);
                                    $requests[]= 2;
                                    $not_update_rows = CitizenRequest::where(function ($q) use ($request,$requests) {
                                                                 $q->where('status','!=',$request->status);
                                                                 $q->whereIn('id',$requests);
                                                            })->get();

                                    if( sizeof($not_update_rows) == 0 ) {
                                        $response["status"]= 'success';
                                        $response["msg"]= trans('sms::application.The row is updated');
                                    }else{
                                        $token = md5(uniqid());
                                        \Excel::create('export_' . $token, function($excel) use($not_update_rows) {
                                            $excel->sheet(date('y-m-d'), function($sheet) use($not_update_rows) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                   'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' =>[
                                                        'name'      =>  'Simplified Arabic',
                                                        'size'      =>  12,
                                                        'bold'      =>  true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:D1")->applyFromArray($style);
                                                $sheet->setHeight(1,30);
                                                $sheet->setWidth(['A'=>25]);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],'font' =>[
                                                        'name'      =>  'Simplified Arabic',
                                                        'size'      =>  12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
                                                $z = 2;
                                                $sheet->setCellValue('A1',trans('citizens::application.request_id'));
                                                foreach ($not_update_rows as $key => $value) {
                                                    $sheet->setCellValue('A' . $z, $value->id);
                                                    $z++;
                                                }
                                            });
                                        })->store('xlsx', storage_path('tmp/'));
                                        return response()->json(['status' => 'failed' ,
                                                                 'download_token' =>$token ,
                                                                 'error_type' =>' ' , 'msg' => trans('common::application.some request update and other not') ]);
                                    }
                                }
                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }
}
