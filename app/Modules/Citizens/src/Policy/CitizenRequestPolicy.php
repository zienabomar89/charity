<?php

namespace Citizens\Policy;
use Auth\Model\User;
use Citizens\Model\CitizenRequest;

class CitizenRequestPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('reports.CitizenRequest.manage')) {
            return true;
        }

        return false;
    }

}

