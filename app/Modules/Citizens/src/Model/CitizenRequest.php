<?php
namespace Citizens\Model;

use Organization\Model\Organization;
use Organization\Model\OrgLocations;
use Setting\Model\aidsLocation;
use Setting\Model\aidsLocationI18n;
use App\Http\Helpers;


class CitizenRequest  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_citizen_requests';
    protected $fillable = ['citizen_id','type','reason','user_id','notes'];
    protected $primaryKey = 'id';
    protected $hidden = ['created_at','updated_at'];

    public static function filter($page,$filters)
    {
        $condition = [];
        $int_ =  [ 'adscountry_id', 'adsdistrict_id','type','status',
                 'adsmosques_id', 'adsneighborhood_id', 'adsregion_id', 'adssquare_id',
            'user_id'];

        $filters_=['first_name', 'second_name', 'third_name', 'last_name','id_card_number','street_address','status'];
        foreach ($filters as $key => $value) {
            if(in_array($key,$filters_) || in_array($key, $int_)) {
                if($value != ""){
                    if(in_array($key, $int_)) {
                        $data = [$key => (int) $value];
                        array_push($condition, $data);

                    }else{
                        array_push($condition, [$key => $value]);
                    }
               }
            }
        }

        $user = \Illuminate\Support\Facades\Auth::user();
        $UserOrg=$user->organization;
        $organization_id = $user->organization_id;
        $UserOrgLevel =$UserOrg->level;

        $language_id =  \App\Http\Helpers::getLocale();

        if($language_id == 1){
            $query=\DB::table('char_citizen_requests_view');
        }else{
            $query=\DB::table('char_citizen_requests_view_en');
        }
        $whereRaw = null;
        if ($UserOrgLevel == Organization::LEVEL_BRANCH_CENTER) {
            $mainConnector = OrgLocations::getConnected($organization_id);
            $districts = aidsLocationI18n::getConnectedIds(aidsLocation::TYPE_DISTRICT, $mainConnector, true);
            $whereRaw = "(
                                                adsdistrict_id is null   OR
                                                ( adsdistrict_id  is not null  AND adsdistrict_id IN (" . join(",", $districts) . ") )
                                              )";
        }
        elseif ($UserOrgLevel == Organization::LEVEL_BRANCHES) {
            $mainConnector = OrgLocations::getConnected($organization_id);
            $mosques = aidsLocationI18n::getConnectedIds(aidsLocation::TYPE_MOSQUES, $mainConnector, true);
            $districts = aidsLocationI18n::getConnectedIds(aidsLocation::TYPE_DISTRICT, $mainConnector, true);
            $whereRaw ="(
                                          (
                                              adsdistrict_id  is not null AND
                                              adsmosques_id IN (" . join(",", $mosques) . ")
                                          )
                                           or
           
           (
                                             adsdistrict_id   IN (" . join(",", $districts) . ")  and
                (   adsregion_id  is null OR  adssquare_id  is null OR    adsneighborhood_id  is null OR  adsmosques_id  is null   )  
            )
             or
           
           (
               adsdistrict_id   is  null  and
                (   adsregion_id  is null OR  adssquare_id  is null OR    adsneighborhood_id  is null OR  adsmosques_id  is null   )  
            )
                                         
                                          )";

        }

        if(!is_null($whereRaw)){
            $query->whereRaw($whereRaw);
        }

        if (count($condition) != 0) {
            $query =  $query
                ->where(function ($q) use ($condition) {
                    $names = ['first_name', 'second_name', 'third_name', 'last_name','street_address'];
                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $names)) {
//                                $q->where($key ,'like'  ,'%' . $value . '%');
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });
        }

        $created_at_to=null;
        $created_at_from=null;

        if(isset($filters['created_at_to']) && $filters['created_at_to'] !=null){
            $created_at_to=date('Y-m-d',strtotime($filters['created_at_to']));
        }
        if(isset($filters['created_at_from']) && $filters['created_at_from'] !=null){
            $created_at_from=date('Y-m-d',strtotime($filters['created_at_from']));
        }
        if($created_at_from != null && $created_at_to != null) {
            $query->whereBetween( 'created_at', [ $created_at_from, $created_at_to]);
        }elseif($created_at_from != null && $created_at_to == null) {
            $query->whereDate('created_at', '>=', $created_at_from);
        }elseif($created_at_from == null && $created_at_to != null) {
            $query->whereDate('created_at', '<=', $created_at_to);
        }


        $order = false;

        if (isset($filters['sortKeyArr_rev']) && $filters['sortKeyArr_rev'] != null && $filters['sortKeyArr_rev'] != "" &&
            $filters['sortKeyArr_rev'] != [] && sizeof($filters['sortKeyArr_rev']) != 0
        ) {

            $order = true;

            foreach ($filters['sortKeyArr_rev'] as $key_) {
                $query->orderBy($key_,'desc');
            }
        }


        if (isset($filters['sortKeyArr_un_rev']) && $filters['sortKeyArr_un_rev'] != null && $filters['sortKeyArr_un_rev'] != "" &&
            $filters['sortKeyArr_un_rev'] != [] && sizeof($filters['sortKeyArr_un_rev']) != 0
        ) {

            $order = true;
            foreach ($filters['sortKeyArr_rev'] as $key_) {
                $query->orderBy($key_,'asc');
            }
        }



        if(!$order){
            $query->orderBy('created_at','desc');
        }

        $male = trans('common::application.male');
        $female = trans('common::application.female');

        $Suggestion = trans('citizens::application.Suggestion');
        $aid = trans('citizens::application.aid request');
        $interception = trans('citizens::application.interception');

        $new = trans('common::application.new');
        $pre_exist = trans('common::application.pre_exist');

        if($filters['action'] =='xlsx'){
            
           if(isset($filters['items'])){
              if(sizeof($filters['items']) > 0 ){
                $query->whereIn('id',$filters['items']);  
             }      
           } 
            $query->selectRaw("CASE  WHEN type is null THEN ' '
                                      WHEN type = 1 THEN '$Suggestion'
                                      WHEN type = 2 THEN '$interception'
                                      WHEN type = 3 THEN '$aid'
                                END  AS type_name,
                                CASE  WHEN category is null THEN '$new'
                                      WHEN category = 0 THEN '$new'
                                      WHEN category = 1 THEN '$pre_exist'
                                END  AS request_category,
                                id_card_number, full_name,
                                 CASE WHEN gender is null THEN ' '
                                     WHEN gender = 1 THEN '$male'
                                     WHEN gender = 2 THEN '$female'
                                END  AS gender,
                                 marital_status,prev_family_name,birthday,
                                 mobile,   wataniya, phone, 
                                 country_name, district_name,
                                 region_name, nearLocation_name, square_name, mosque_name, street_address, 
                                 notes as note, 
                                 status, 
                                 reason, created_at");
            return  $query->get();
        }

        if($language_id == 1){
            $query->selectRaw(" char_citizen_requests_view.*");
        }else{
           $query->selectRaw(" char_citizen_requests_view_en.*");
        }

        $query->selectRaw("full_name, mobile,   
                               CASE  WHEN category is null THEN '$new'
                                      WHEN category = 0 THEN '$new'
                                      WHEN category = 1 THEN '$pre_exist'
                                END  AS request_category,
                                 CASE WHEN gender is null THEN ' '
                                     WHEN gender = 1 THEN '$male'
                                     WHEN gender = 2 THEN '$female'
                                END  AS gender,
                                CASE  WHEN type is null THEN ' '
                                      WHEN type = 1 THEN '$Suggestion'
                                      WHEN type = 2 THEN '$interception'
                                      WHEN type = 3 THEN '$aid'
                                END  AS type_name,
                                 DATE_FORMAT(created_at,'%Y-%m-%d') as date,
                                           DATE_FORMAT(created_at,'%h-%i-%s') as time,
                                           DATE_FORMAT(created_at,'%p') as pm
                                           
                                           ");

        $itemsCount = isset($filters['itemsCount'])?$filters['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
        return $query->paginate($records_per_page);
    }

}