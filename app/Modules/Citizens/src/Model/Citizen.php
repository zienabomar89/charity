<?php
namespace Citizens\Model;

use App\Http\Helpers;

class Citizen  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_citizens';
    protected $primaryKey = 'id';
    protected $hidden = ['created_at','updated_at'];
    protected $fillable = ["first_name", "second_name", "third_name", "last_name", "prev_family_name", "id_card_number",
                           "card_type", "gender", "deserted", "mobile", "wataniya", "phone", "marital_status_id", "birthday", "spouses",
                           "refugee", "unrwa_card_number", "monthly_income", "family_cnt", "male_live", "female_live",
                           "adscountry_id", "adsdistrict_id", "adsregion_id", "adsneighborhood_id",
                           "adssquare_id", "adsmosques_id", "street_address"];


    public static function getMobileNumbers($cont_type,$source ,$target)
    {

        $query = \DB::table('char_citizens');
        $query->selectRaw("CONCAT(ifnull(char_citizens.first_name, ' '), ' ' ,
                                               ifnull(char_citizens.second_name, ' '),' ',ifnull(char_citizens.third_name, ' '),' ',
                                                ifnull(char_citizens.last_name,' ')) AS name,
                                                char_citizens.id_card_number");

        if($cont_type == 0 || $cont_type == '0'){
            $query->selectRaw("char_citizens.wataniya as mobile");
        }else{
            $query->selectRaw("char_citizens.mobile as mobile");
        }

        $query->whereIn('id',$target);

        return $query->get();

    }
}
