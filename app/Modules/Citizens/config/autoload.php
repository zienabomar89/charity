<?php

$loader = new Composer\Autoload\ClassLoader();
$loader->addPsr4('Citizens\\', __DIR__ . '/../src');
$loader->register();