<?php

Route::group(['middleware' => ['api','lang','auth.check'], 'prefix' => 'api/v1.0/citizens','namespace' => 'Citizens\Controller'], function() {
    Route::post('request/UpdateFromFile', 'RequestController@UpdateFromFile')->name('api.v1.0.Request.UpdateFromFile');
    Route::post('request/status', 'RequestController@status')->name('api.v1.0.Request.status');
    Route::post('request/cases', 'RequestController@cases')->name('api.v1.0.Request.cases');
    Route::post('request/filter', 'RequestController@filter')->name('api.v1.0.Request.filter');
    Route::resource('request', 'RequestController');
});

Route::group(['middleware' => ['auth.check'],'prefix' => 'api/v1.0/','namespace' => 'Citizens\Controller'], function() {

    Route::group(['prefix' => 'citizens'], function() {
        Route::post('voucher', 'CitizenController@voucher')->name('api.v1.0.Citizen.voucher');
        Route::post('check', 'CitizenController@check')->name('api.v1.0.Citizen.check');
    });
    Route::resource('citizens', 'CitizenController');
});

Route::group(['middleware' => ['auth.check'],'prefix' => 'api/v1.0/citizensConst' ,'namespace' => 'Citizens\Controller'], function() {
    Route::get('entities', 'EntitiesController@index');
    // *********************************  aid addressing ********************************** //
    Route::get('scountries/list', 'aidsLocationsController@countriesList')->name('aidsCountries.countriesList');
    Route::get('scountries/{id}/translations', 'aidsLocationsController@translations');
    Route::post('scountries/{id}/translations', 'aidsLocationsController@saveTranslations');
    Route::resource('scountries', 'aidsLocationsController');

    Route::get('scountries/{id}/sdistricts/list', 'aidsLocationsController@districtsList')->name('aidsCountries.districtsList');

    Route::get('sdistricts/{id}/translations', 'aidsLocationsController@translations');
    Route::post('sdistricts/{id}/translations', 'aidsLocationsController@saveTranslations');
    Route::resource('scountries/{id}/sdistricts', 'aidsLocationsController');

    Route::get('sdistricts/{id}/sregions/list', 'aidsLocationsController@regionsList')->name('aidsCountries.regionsList');
    Route::get('sregions/{id}/translations', 'aidsLocationsController@translations');
    Route::post('sregions/{id}/translations', 'aidsLocationsController@saveTranslations');
    Route::resource('sdistricts/{id}/sregions', 'aidsLocationsController');

    Route::get('sregions/{id}/sneighborhoods/list', 'aidsLocationsController@neighborhoodsList')->name('aidsCountries.neighborhoodsList');
    Route::get('sneighborhoods/{id}/translations', 'aidsLocationsController@translations');
    Route::post('sneighborhoods/{id}/translations', 'aidsLocationsController@saveTranslations');
    Route::resource('sregions/{id}/sneighborhoods', 'aidsLocationsController');

    Route::get('sneighborhoods/{id}/ssquares/list', 'aidsLocationsController@squaresList')->name('aidsCountries.squaresList');
    Route::get('ssquares/{id}/translations', 'aidsLocationsController@translations');
    Route::post('ssquares/{id}/translations', 'aidsLocationsController@saveTranslations');
    Route::resource('sneighborhoods/{id}/ssquares', 'aidsLocationsController');

    Route::get('ssquares/{id}/smosques/list', 'aidsLocationsController@mosquesList')->name('aidsCountries.mosquesList');
    Route::get('smosques/{id}/translations', 'aidsLocationsController@translations');
    Route::post('smosques/{id}/translations', 'aidsLocationsController@saveTranslations');
    Route::resource('ssquares/{id}/smosques', 'aidsLocationsController');

});

