<?php

namespace Organization\Policy;

use Auth\Model\User;
use Organization\Model\Organization;

class OrganizationPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('org.organizations.manage')) {
            return true;
        }
        
        return false;
    }
    
    public function create(User $user)
    {
        if ($user->hasPermission('org.organizations.create')) {
            return true;
        }
        
        return false;
    }
    
    public function update(User $user, Organization $organization = null)
    {
        if ($user->hasPermission('org.organizations.update')) {
            return true;
        }
        
        return false;
    }
    
    public function delete(User $user, Organization $organization = null)
    {
        if ($user->hasPermission('org.organizations.delete')) {
            return true;
        }
        
        return false;
    }

    public function connectSponsor(User $user, Organization $organization = null)
    {
        if ($user->hasPermission('org.organizations.connectSponsor')) {
            return true;
        }

        return false;
    }

    public function connectLocations(User $user, Organization $organization = null)
    {
        if ($user->hasPermission('org.organizations.connectLocations')) {
            return true;
        }

        return false;
    }
    public function view(User $user, Organization $organization = null)
    {
        if ($user->hasPermission(['org.organizations.view', 'org.organizations.update'])) {
            return true;
        }
        
        return false;
    }
    
    public function upload(User $user, Organization $organization = null)
    {
        if ($user->hasPermission(['org.organizations.update', 'org.organizations.create'])) {
            return true;
        }
        
        return false;
    }

    public function orgSearch(User $user)
    {
        if ($user->hasPermission('reports.organization.orgSearch')) {
            return true;
        }

        return false;
    }

}

