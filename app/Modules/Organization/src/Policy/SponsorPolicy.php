<?php

namespace Organization\Policy;

use Auth\Model\User;
use Organization\Model\Sponsor;

class SponsorPolicy
{
    public function manage(User $user)
    {
        if ($user->hasPermission('org.sponsors.manage')) {
            return true;
        }
        
        return false;
    }
    
    public function create(User $user)
    {
        if ($user->hasPermission('org.sponsors.create')) {
            return true;
        }
        
        return false;
    }
    
    public function update(User $user, Sponsor $sponsor = null)
    {
        if ($user->hasPermission('org.sponsors.update')) {
            return true;
        }
        
        return false;
    }
    
    public function delete(User $user, Sponsor $sponsor = null)
    {
        if ($user->hasPermission('org.sponsors.delete')) {
            return true;
        }
        
        return false;
    }
    
    public function view(User $user, Sponsor $sponsor = null)
    {
        if ($user->hasPermission('org.sponsors.view')) {
            return true;
        }
        
        return false;
    }
    
    public function upload(User $user, Sponsor $sponsor = null)
    {
        if ($user->hasPermission('org.sponsors.view')) {
            return true;
        }
        
        return false;
    }
    public function download(User $user, Sponsor $sponsor = null)
    {
        if ($user->hasPermission('org.sponsors.view')) {
            return true;
        }

        return false;
    }

//    public function upload(User $user, Sponsor $sponsor = null)
//    {
//        if ($user->hasPermission(['org.sponsors.update', 'org.sponsors.create'])) {
//            return true;
//        }
//
//        return false;
//    }public function upload(User $user, Sponsor $sponsor = null)
//    {
//        if ($user->hasPermission(['org.sponsors.update', 'org.sponsors.create'])) {
//            return true;
//        }
//
//        return false;
//    }
}

