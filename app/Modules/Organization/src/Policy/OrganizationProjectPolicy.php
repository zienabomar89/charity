<?php

namespace Organization\Policy;

use Auth\Model\User;
use Organization\Model\OrganizationProject;

class OrganizationProjectPolicy
{

    public function manageTwo(User $user)
    {
        if ($user->hasPermission(['org.organizationsProjects.manage','org.organizationsProjects.trashed'])) {
            return true;
        }

        return false;
    }
    public function manage(User $user)
    {
        if ($user->hasPermission('org.organizationsProjects.manage')) {
            return true;
        }

        return false;
    }

    public function trashed(User $user)
    {
        if ($user->hasPermission('org.organizationsProjects.trashed')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('org.organizationsProjects.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, OrganizationProject $report = null)
    {
        if ($user->hasPermission('org.organizationsProjects.update')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, OrganizationProject $project = null)
    {
        if ($user->hasPermission('org.organizationsProjects.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, OrganizationProject $project = null)
    {
        if ($user->hasPermission('org.organizationsProjects.view')) {
            return true;
        }

        return false;
    }

}

