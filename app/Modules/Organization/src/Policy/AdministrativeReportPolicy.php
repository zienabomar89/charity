<?php

namespace Organization\Policy;

use Auth\Model\User;
use Organization\Model\AdministrativeReports\AdministrativeReport;

class AdministrativeReportPolicy
{

    public function manage(User $user)
    {
        if ($user->hasPermission('org.AdministrativeReport.manage')) {
            return true;
        }

        return false;
    }

    public function manageOrg(User $user)
    {
        if ($user->hasPermission('org.AdministrativeReport.manageOrg')) {
            return true;
        }

        return false;
    }

    public function create(User $user)
    {
        if ($user->hasPermission('org.AdministrativeReport.create')) {
            return true;
        }

        return false;
    }

    public function update(User $user, AdministrativeReport $report = null)
    {
        if ($user->hasPermission('org.AdministrativeReport.update')) {
            return true;
        }

        return false;
    }

    public function orgUpdate(User $user, AdministrativeReport $report = null)
    {
        if ($user->hasPermission('org.AdministrativeReport.OrgUpdate')) {
            return true;
        }

        return false;
    }

    public function delete(User $user, AdministrativeReport $report = null)
    {
        if ($user->hasPermission('org.AdministrativeReport.delete')) {
            return true;
        }

        return false;
    }

    public function view(User $user, AdministrativeReport $report = null)
    {
        if ($user->hasPermission('org.AdministrativeReport.view')) {
            return true;
        }

        return false;
    }

    public function upload(User $user, AdministrativeReport $report = null)
    {
        if ($user->hasPermission('org.AdministrativeReport.upload')) {
            return true;
        }

        return false;
    }

    public function download(User $user, AdministrativeReport $report = null)
    {
        if ($user->hasPermission('org.AdministrativeReport.download')) {
            return true;
        }

        return false;
    }

}

