<?php

namespace Organization\Controller;

use Organization\Model\AbstractOrganization;
use Organization\Model\Sponsor;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SponsorsController extends AbstractController
{
    protected $type = AbstractOrganization::TYPE_SPONSOR;

    // initialize Sponsor model object to use in process
    protected function getModel()
    {
        return new Sponsor();
    }

    // get Sponsor model using id
    protected function findOrFail($id)
    {
        $org = Sponsor::findOrFail($id);
        if ($org->type != $this->type) {
            throw (new ModelNotFoundException())->setModel(get_class($org));
        }
        
        return $org;
    }

    // get Sponsor model searching by name
    public function index()
    {
        $this->authorize('manage', Sponsor::class);
        return parent::index();
    }

    // get deleted Sponsor model searching by name
    public function trash(Request $request)
    {
        $this->authorize('manage', Sponsor::class);
        return parent::trash($request);
    }

    // create new Sponsor model
    public function store(Request $request)
    {
        $this->authorize('create', Sponsor::class);
        return parent::store($request);
    }

    // restore deleted Sponsor model by id
    public function restore(Request $request,$id)
    {
//        $this->authorize('restore', Sponsor::class);
        return parent::restore($request,$id);
    }

}