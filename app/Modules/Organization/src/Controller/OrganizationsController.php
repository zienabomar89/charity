<?php

namespace Organization\Controller;

use Organization\Model\AbstractOrganization;
use Organization\Model\Organization;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Organization\Model\OrgSponsors;
use Organization\Model\OrgLocations;
use Setting\Model\aidsLocation;
use Setting\Model\aidsLocationI18n;
use App\Http\Helpers;
use Auth\Model\UserOrg;

class OrganizationsController  extends AbstractController
{
    protected $type = AbstractOrganization::TYPE_ORGANIZATION;

    // initialize Organization model object to use in process
    protected function getModel()
    {
        return new Organization();
    }

    // get Organization model using id
    protected function findOrFail($id)
    {
        $org = Organization::findOrFail($id);
        if ($org->type != $this->type) {
            throw (new ModelNotFoundException())->setModel(get_class($org));
        }
        
        return $org;
    }

    // get Organization model searching by name
    public function index()
    {
        $this->authorize('manage', Organization::class);
        return parent::index();
    }

    // get deleted Organization model searching by name
    public function trash(Request $request)
    {
        $this->authorize('manage', Organization::class);
        return parent::trash($request);
    }

    // get Organization model searching by name
    public function filter(Request $request)
    {

        $language_id =  \App\Http\Helpers::getLocale();
        $this->authorize('orgSearch', Organization::class);
        $all = $this->getModel()
            ->with(['Locations' => function ($query) use($language_id) {
                $query->where('language_id', '=', $language_id);
            }])
            ->with(['district' => function ($query) use($language_id){
                $query->where('language_id', '=', $language_id);
            }])
            ->with(['city' => function ($query) use($language_id){
                $query->where('language_id', '=', $language_id);
            }])
            ->with(['nearLocation' => function ($query) use($language_id){
                $query->where('language_id', '=', $language_id);
            }]);

        $options=['type'=> $this->type];

        if(isset($request->name)){
            if(!is_null($request->name) && $request->name!=''){
                $options['name'] = $request->name;
            }
        }
        if (count($options) != 0) {
            $all =  $all
                ->where(function ($q) use ($options) {
                    $str = ['name'];
                    foreach ($options as $key => $value) {
                        if(in_array($key, $str)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
//                            $q->whereRaw("$key like  ?", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                });
        }
        if($request->action == 'xlsx'){


            $rows = $all->orderBy('name')->get();

            if(sizeof($rows) > 0 ){

                $token = md5(uniqid());
                \Maatwebsite\Excel\Facades\Excel::create('export_' . $token, function($excel) use($rows)  {
                    $excel->setTitle(trans('organization::application.organization'));
                    $excel->setDescription(trans('organization::application.organization'));
                    $excel->sheet(trans('organization::application.organization'),function($sheet)use($rows)   {
                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('solid');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);
                        $sheet->setOrientation('landscape');
                        $sheet->setRightToLeft(true);
                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Simplified Arabic',
                                'size' => 11,
                                'bold' => true
                            ]
                        ]);

                        $sheet->setWidth(['A'=>5 ,'B'=> 35,'C'=> 20,'D'=> 35,'E'=> 35,'F'=> 35, 'G'=> 35]);

                        $range="A1:G1";
                        $sheet->getStyle($range)->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 11, 'bold'=>  true ]
                        ]);

                        $sheet ->setCellValue('A1',trans('organization::application.#'));
                        $sheet ->setCellValue('B1',trans('organization::application.organization_name_'));
                        $sheet ->setCellValue('C1',trans('organization::application.organization_status'));
                        $sheet ->setCellValue('D1',trans('organization::application.organization_country'));
                        $sheet ->setCellValue('E1',trans('organization::application.organization_district'));
                        $sheet ->setCellValue('F1',trans('organization::application.organization_city'));
                        $sheet ->setCellValue('G1',trans('organization::application.organization_location'));
                        $sheet ->setCellValue('H1',trans('organization::application.mobile'));
                        $sheet ->setCellValue('I1',trans('organization::application.phone'));
                        $sheet ->setCellValue('J1',trans('organization::application.email'));
                        $sheet ->setCellValue('K1',trans('organization::application.fax'));

                        $z= 2;
                        foreach($rows as $k=>$v )
                        {
                            $sheet ->setCellValue('A'.$z,$k+1);
                            $sheet ->setCellValue('B'.$z,(is_null($v->name) || $v->name == ' ' ) ? '-' : $v->name);
                            $sheet ->setCellValue('C'.$z,(is_null($v->status_name) || $v->status_name == ' ' ) ? '-' : $v->status_name);
                            $sheet ->setCellValue('D'.$z,(is_null($v->country) || $v->country == ' ' ) ? '-' : $v->locations->name);
                            $sheet ->setCellValue('E'.$z,(is_null($v->district_id) || $v->district_id == ' ' ) ? '-' : $v->district->name);
                            $sheet ->setCellValue('F'.$z,(is_null($v->city_id) || $v->city_id == ' ' ) ? '-' : $v->city->name);
                            $sheet ->setCellValue('G'.$z,(is_null($v->location_id) || $v->location_id == ' ' ) ? '-' : $v->nearLocation->name);
                            $sheet ->setCellValue('H'.$z,(is_null($v->mobile) || $v->mobile == ' ' ) ? '-' : $v->mobile);
                            $sheet ->setCellValue('I'.$z,(is_null($v->phone) || $v->phone == ' ' ) ? '-' : $v->phone);
                            $sheet ->setCellValue('J'.$z,(is_null($v->email) || $v->email == ' ' ) ? '-' : $v->email);
                            $sheet ->setCellValue('K'.$z,(is_null($v->fax) || $v->fax == ' ' ) ? '-' : $v->fax);
                            $z++;
                        }
                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['status' => true , 'download_token' => $token]);

            }

            return response()->json(['status' => false ]);

        }

        $itemsCount = ($request->itemsCount != null)? $request->itemsCount : 0;

        $records_per_page = Helpers::recordsPerPage($itemsCount,$all->count());
        return response()->json($all->orderBy('name')->paginate($records_per_page));
    }

    // create new descendant organization to the logged user organization
    public function descendants()
    {
        $user = \Illuminate\Support\Facades\Auth::user();
        $language_id =  \App\Http\Helpers::getLocale();

        $entities = $this->getModel()
                    ->where(function ($q) use ($user) {
                        $q->where('type', $this->type);
                        $q->where('id', '!=', $user->organization_id);
                        $q->where('parent_id', '=', $user->organization_id);
                 })->orderBy('name');


        $entities->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name 
                                        Else char_organizations.en_name END  AS name ");


        return response()->json($entities->get());
    }

    // create new closure organization to the logged user organization
    public function orgs()
    {
        $user = \Illuminate\Support\Facades\Auth::user();
        $ancestor=Organization::getAncestor($user->organization_id);

        $entities = $this->getModel()->where(function ($q) use ($ancestor) {
                                            $q->where('type', 1);
                                            $q->whereIn('char_organizations.id', function($query) use($ancestor) {
                                                $query->select('descendant_id')
                                                    ->from('char_organizations_closure')
                                                    ->where('ancestor_id', '=', $ancestor);
                                            });
                                    })->get();

        return response()->json($entities);
    }

    // create new Organization model
    public function store(Request $request)
    {
        $this->authorize('create', Organization::class);
        return parent::store($request);
    }

    // restore deleted Organization model by id
    public function restore(Request $request,$id)
    {
//        $this->authorize('restore', Organization::class);
        return parent::restore($request,$id);
    }

    // get repository share of child (organizations)
    public function containerShare($id)
    {
        $user = \Illuminate\Support\Facades\Auth::user();
        $language_id =  \App\Http\Helpers::getLocale();

        if($id == 1){
            $id = $user->organization_id;
        }
        $Org = $this->findOrFail($id);

        $descendants_ratio = $this->getModel()->with(['descendants_'])
            ->where(function ($q) use ($id) {
                $q->where('type', $this->type);
                $q->where('parent_id', '=', $id);
                $q->where('id', '!=', $id);
            });

        $descendants_ratio->selectRaw("char_organizations.* ,
                         CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");



        $Org->descendants_ratio = $descendants_ratio->orderBy('container_share')->get();
        $Org->descendants_ratio_total = 0;

        if(sizeof($Org->descendants_ratio) > 0){
            foreach ($Org->descendants_ratio as $organization) {
                $Org->descendants_ratio_total += (float) $organization->container_share;
            }
        }

        return response()->json($Org);
    }

    // update repository share of child (organizations)
    public function updateRepositoryShare(Request $request)
    {
        $this->authorize('update', Organization::class);
        $organizations = $request->input('organizations');
        $total = 0;
        foreach ($organizations as $organization) {
            $total +=  floatval($organization['container_share']);
        }
        if ($total > 100) {
            return response()->json([
                'status'=>false,
                'total'=>$total,
                'msg' => trans('organization::application.Total ratios of the organizations must equal 100 (current total is') . $total . ')!',
            ]);
        }

        foreach ($organizations as $organization) {
            Organization::where(['id' => $organization['id']])->update(['container_share' =>  $organization['container_share']]);
        }
        
        return response()->json(['status' => true]);
    }

    // get connect Sponsor of organization by id
    public function getSponsors($id)
    {
        $this->authorize('connectSponsor', Organization::class);
        $org = $this->findOrFail($id);

        $user = \Illuminate\Support\Facades\Auth::user();
        $UserOrg=$user->organization;

//        if($org->level == Organization::LEVEL_MASTER_CENTER) {
            $org->list=OrgSponsors::getOrgSponsors($id);
//        }else{
//            $org->list=OrgSponsors::getRelatedOrgSponsors($id,$org->parent_id);
//        }
//        $org->list=OrgSponsors::getRelatedOrgSponsors($id,$org->parent_id);
        return response()->json($org);
    }

    // set or reset connect Sponsor of organization by id
    public function resetSponsors(Request $request,$id)
    {
        $this->authorize('connectSponsor', Organization::class);
        return response()->json(OrgSponsors::resetSponsors($id,$request->sponsors));
    }

    // get connect locations of organization by id
    public function getLocations($id)
    {

        $this->authorize('connectLocations', Organization::class);
        $language_id =  \App\Http\Helpers::getLocale();


        $org = $this->findOrFail($id);
        $org->connected=OrgLocations::getOrgLocations($id);

        if(!is_null($org->parent_id)){
            $mainConnector=OrgLocations::getConnected($org->parent_id);
            $org->mosques=aidsLocationI18n::getRelatedList(aidsLocation::TYPE_MOSQUES,$mainConnector,true);
            $org->squares=aidsLocationI18n::getRelatedList(aidsLocation::TYPE_SQUARE,$mainConnector,true);
            $org->neighborhoods=aidsLocationI18n::getRelatedList(aidsLocation::TYPE_NEIGHBORHOOD,$mainConnector,true);
            $org->regions=aidsLocationI18n::getRelatedList(aidsLocation::TYPE_REGION,$mainConnector,true);
            $org->districts=aidsLocationI18n::getRelatedList(aidsLocation::TYPE_DISTRICT,$mainConnector,true);
        }else{
            $org->districts=aidsLocationI18n::getTypeList(aidsLocation::TYPE_DISTRICT);
            $org->regions=aidsLocationI18n::getTypeList(aidsLocation::TYPE_REGION);
            $org->neighborhoods=aidsLocationI18n::getTypeList(aidsLocation::TYPE_NEIGHBORHOOD);
            $org->squares=aidsLocationI18n::getTypeList(aidsLocation::TYPE_SQUARE);
            $org->mosques=aidsLocationI18n::getTypeList(aidsLocation::TYPE_MOSQUES);
        }
        return response()->json($org);
    }

    // set or reset connect locations of organization by id
    public function resetLocations(Request $request,$id)
    {
        $this->authorize('connectLocations', Organization::class);
        return response()->json(OrgLocations::resetLocations($id,$request->mosques));
    }

    // get Organization by Category
    public function ListOfCategory(Request $request)
    {
        $entries=array();
        $user = \Illuminate\Support\Facades\Auth::user();
        $UserType=$user->type;
        $UserOrg_=$user->organization;
        $organization_id=$user->organization_id;
        $UserOrgType=$UserOrg_->type;

        $language_id =  \App\Http\Helpers::getLocale();

        $ids = $request->ids;
        if($UserType == 2){
            $connector =OrgSponsors::getOrgsConnector(UserOrg::getUserOrgIds($user->id));
            if(sizeof($connector) >0){
                $all = Organization::where(function ($q) use ($connector,$ids) {
                    $q->where('type',$this->type);
                    $q->whereIn('id',$connector);
                    if(sizeof($ids) >0){
                        $q->whereIn('category_id',$ids);
                    }
                });
                $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");

                $entries =  $all->orderBy('name')->get();
            }

        }
        else{
            if($user->super_admin || $UserOrg_->level == Organization::LEVEL_BRANCH_CENTER){
                $all = Organization::where('type', $this->type);

                if(sizeof($ids) >0){
                    $all->whereIn('category_id',$ids);
                }

                $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");

                $entries =  $all->get();
            }else{
                $connector =OrgSponsors::getConnector($user->organization_id);
                if(sizeof($connector) >0){
                    $all = Organization::where(function ($q) use ($connector,$ids) {
                                        $q->where('type',$this->type);
                                        $q->whereIn('id',$connector);
                                        if(sizeof($ids) >0){
                                            $q->whereIn('category_id',$ids);
                                        }
                                    });

                    $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");

                    $entries =  $all->orderBy('name')->get();
                }
            }
        }

        return response()->json(['list'=>$entries]);
    }
}