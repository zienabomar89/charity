<?php

namespace Organization\Controller\Sponsor;

use App\Http\Controllers\Controller;
use App\Http\Helpers;
use Illuminate\Http\Request;
use League\Flysystem\Exception;
use Organization\Model\Organization;
use Setting\Model\aidsLocation;
use Setting\Model\Setting;
use Aid\Model\Vouchers;
use Aid\Model\VoucherPersons;
use Aid\Model\VoucherDocuments;
use Excel;
use Elibyy\TCPDF\TCPDF;
use PDF;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Common\Model\CaseModel;


class AidsController  extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
   
    // filter voucher statistic
    public function vouchers(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $this->authorize('manageSponsor',\Common\Model\SponsorshipsCases::class);

        $status=$request->action;

        
        $user = \Auth::user();
        $Org=Organization::findorfail($user->organization_id);
        $master=true;
        if($Org->parent_id){
            $master=false;
        }
        $sponsor_id = $user->organization_id;
        
        $vouchers=Vouchers::filterForSponsor($request->all(),$sponsor_id);
         if($status =='ExportToExcel'){
            if(sizeof($vouchers) !=0){
                $data=array();
                foreach($vouchers as $key =>$value){
                    $data[$key][trans('aid::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        $data[$key][trans('aid::application.' . $k)]= str_replace("-","_",$v) ;
                    }
                }
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($data) {
                    $excel->setTitle(trans('aid::application.voucher_name'));
                    $excel->setDescription(trans('aid::application.voucher_name'));
                    $excel->sheet(trans('aid::application.vouchers_sheet'), function($sheet) use($data) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);
                        $sheet->getStyle("A1:P1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->fromArray($data);
                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['status' => true  ,'download_token' => $token]);
            }
            return response()->json(['status' => false]);
        }
        return response()->json(['Vouchers' => $vouchers['list'], 'total' => $vouchers['total']]);

    }

    // filter general beneficiary voucher statistic
    public function beneficiaries(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $this->authorize('manageSponsor',\Common\Model\SponsorshipsCases::class);

        $status=$request->action;

//        $this->authorize('manage', Vouchers::class);

        $user = \Auth::user();
        $Org=Organization::findorfail($user->organization_id);
        $sponsor_id = $user->organization_id;

        $vouchers=Vouchers::personsFilterForSponsor($request->all(),$sponsor_id);

        if($status =='ExportToExcel'){
            if(sizeof($vouchers) !=0){
                $data=array();
                foreach($vouchers as $key =>$value){
                    $data[$key][trans('aid::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        $data[$key][trans('aid::application.' . $k)]= str_replace("-","_",$v) ;
                    }
                }
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($data) {
                    $excel->setTitle(trans('aid::application.vouchers_sheet'));
                    $excel->setDescription(trans('aid::application.vouchers_sheet'));
                    $excel->sheet(trans('aid::application.vouchers_sheet'), function($sheet) use($data) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);
                        $sheet->getStyle("A1:AO1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->fromArray($data);
                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['status' => true  ,'download_token' => $token]);
            }
            return response()->json(['status' => false]);
        }
        return response()->json(['data' => $vouchers['list'],'total' => $vouchers['total']]);

    }

           // get cases of Sponsor according filters ( ExportToWord , paginate , xlsx )
    public function persons(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $this->authorize('manageSponsor',\Common\Model\SponsorshipsCases::class);

//           $this->authorize('manageSponsor',\Common\Model\SponsorshipsCases::class);
            $user = \Auth::user();
            $items=VoucherPersons::getPersonForSponsor($request->all());
            
            if($request->action =='export'){
                     if(sizeof($items) !=0){
                        foreach($items as $key =>$value){
                            $dat[$key][trans('setting::application.#')]=$key+1;
                            foreach($value as $k =>$v){
                                if($k != 'id' && $k != 'sponsor_id'&& $k != 'flag'&& $k != 'date_flag'){
                                    if(substr( $k, 0, 7 ) === "mother_"){
                                        $dat[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.mother')  ." ) "]= $v;
                                    }elseif(substr( $k, 0, 7 ) === "father_"){
                                        $dat[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.father')  ." ) "]= $v;
                                    }elseif(substr( $k, 0, 9 ) === "guardian_"){
                                        $dat[$key][trans('sponsorship::application.'.substr($k,9))." ( ".trans('sponsorship::application.guardian')  ." ) " ]= $v;
                                    }else{
                                        $dat[$key][trans('sponsorship::application.' . $k)]= $v;
                                    }
                                }
                            }
                        }
                        $data=$dat;
                         $token = md5(uniqid());
                         \Excel::create('export_' . $token, function($excel) use($data) {
                            $excel->sheet('Sheetname', function($sheet) use($data) {


                                $sheet->setStyle([
                                    'font' => [
                                        'name' => 'Calibri',
                                        'size' => 11,
                                        'bold' => true
                                    ]
                                ]);
                                $sheet->setAllBorders('thin');
                                $sheet->setfitToWidth(true);
                                $sheet->setRightToLeft(true);
                                $style = [
                                   'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font' =>[
                                        'name'      =>  'Simplified Arabic',
                                        'size'      =>  12,
                                        'bold'      =>  true
                                    ]
                                ];

                                $sheet->getStyle("A1:CE1")->applyFromArray($style);
                                $sheet->setHeight(1,30);

                                $style = [
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true]
                                    ,'font' =>[
                                        'name'      =>  'Simplified Arabic',
                                        'size'      =>  12,
                                        'bold' => false
                                    ]
                                ];

                                $sheet->getDefaultStyle()->applyFromArray($style);
                                $sheet->fromArray($data);
                            });
                        })
                             ->store('xlsx', storage_path('tmp/'));
                         return response()->json([ 'status' =>true , 'download_token' => $token ]);
                    }
                    
                    return response()->json([ 'status' =>false ]);
       
            }

            return response()->json(['cases'  =>$items ] );

    }
    
     public function person(Request $request) {
     
         
        $user = \Auth::user();
        $items=array();
        $sheetName=null;
        $r_person=null;
        $HeadTo=null;
        $id = $request->get('person_id');
                
        $request->request->add(['sponsor_id' => $user->organization_id]); //add request               

        if($request->get('target') =='info'){

            $response =\Common\Model\Person::fetch(array(
                'id' => $id,
                'action' => $request->get('mode'),
                'full_name'     => $request->get('full_name'),
                'organization_id'     => $user->organization_id,
                'person'        => $request->get('person'),
                'persons_i18n'  => $request->get('persons_i18n'),
                'wives'      => $request->get('wives'),
                'contacts'      => $request->get('contacts'),
                'work'          => $request->get('work'),
                'education'     => $request->get('education'),
                'health'        => $request->get('health'),
                'islamic'        => $request->get('islamic'),
                'residence'     => $request->get('residence'),
                'persons_properties' => $request->get('persons_properties'),
                'persons_documents'  => $request->get('persons_documents'),
                'case_sponsorships'  => $request->get('case_sponsorships'),
                'case_payments'      => $request->get('case_payments'),
                'father_id'     => $request->get('father_id'),
                'mother_id'     => $request->get('mother_id'),
                'father'        => $request->get('father'),
                'mother'        => $request->get('mother'),
                'family_payments'    => $request->get('family_payments'),
            ));
            return response()->json($response);

            if (!$response) {
                //throw (new \Illuminate\Database\Eloquent\ModelNotFoundException)->setModel(CaseModel::class);
                return response()->json(array('error' => 'Not found!'), 404);
            }


        }
        elseif($request->get('target') =='vouchers'){
            $items= CaseModel::getCaseVouchersForSponsor($request->all(),$user->organization_id);
            if($request->get('action') =='export') {
                $sheetName='case_vouchers_';
                $HeadTo='L';
            }
        }
        
        elseif($request->get('target') =='individuals'){
           $request->request->add(['guardian_id' => $id]); //add request                          
            $items=\Common\Model\CaseModel::getGuardians($request->all());
            if($request->get('action') =='export') {
                $sheetName='guardians';
                $HeadTo='L';
            }
        }
        elseif($request->get('target') =='sponsorships'){
            $items=CaseModel::getSponsorshipsForSponsor($request->all());
            if($request->get('action') =='export') {
                $sheetName='case_sponsorships';
                $HeadTo='J';
            }
        }
        elseif($request->get('target') =='payments'){
            if($request->get('sub_target') =='guardians'){
                $person_id=$request->get('guardian_id');
            }else{
                $person_id=$request->get('person_id');
            }
            $items=CaseModel::getPaymentForSponsor($request->get('sub_target'),$request->all(),$person_id,null,null,$request->page,null,null);
            if($request->get('action') =='export') {
                $sheetName='case_payments';
                $HeadTo='P';
            }
        }

        if($request->get('action') =='filters') {
            return response()->json($items);
        }

        return \Common\Model\CaseModel::createExcelToExport($items,$sheetName,$HeadTo);
     }
}
