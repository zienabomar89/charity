<?php
namespace Organization\Controller\Sponsor;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;
use App\Http\Helpers;
use Illuminate\Http\Request;
use Mockery\Exception;
use Sponsorship\Model\Sponsorships;
use Sponsorship\Model\SponsorshipsPerson;
use Setting\Model\Setting;
use Common\Model\BlockIDCard;
use Sponsorship\Model\SponsorshipCases;
use Sponsorship\Model\SponsorshipCasesStatusLog;
use Organization\Model\Organization;
use Sponsorship\Model\Payments;
use Sponsorship\Model\PaymentsCases;

class SponsorshipsController extends Controller
{
        public function __construct()
        {
            $this->middleware('auth:api');
        }

        // paginate all payments of logged sponsor
        public function payments(Request $request)
        {

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $this->authorize('manageSponsor',\Common\Model\SponsorshipsCases::class);

    //        $this->authorize('manage', Payments::class);
            $user = \Auth::user();
            $return = Payments::filterForSponsor($request->page, $user->organization_id, $request->all());

            if ($request->action == 'export') {
                if (sizeof($return) != 0) {
                    $data = [];
                    $attachTypes = ['organization_sheet', 'organization_sheet', 'receipt_of_receipt'];
                    foreach ($return as $key => $value) {
                        $data[$key][trans('sponsorship::application.#')] = $key + 1;
                        foreach ($value as $k => $v) {
                            if(in_array($k, $attachTypes)) {
                                if ($v == '0' || $v == 0) {
                                    $data[$key][trans('sponsorship::application.' . $k)] = trans('sponsorship::application.exist');
                                } else {
                                    $data[$key][trans('sponsorship::application.' . $k)] = trans('sponsorship::application.not exist');
                                }
                            } else {
                                $data[$key][trans('sponsorship::application.' . $k)] = $v;
                            }
                        }
                    }
                    $token = md5(uniqid());
                    \Excel::create('export_' . $token, function ($excel) use ($data) {
                        $excel->sheet(trans('sponsorship::application.payments'), function ($sheet) use ($data) {
                            $sheet->setStyle([
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 11,
                                    'bold' => true
                                ]
                            ]);
                            $sheet->setAllBorders('thin');
                            $sheet->setfitToWidth(true);
                            $sheet->setRightToLeft(true);
                            $style = [
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Simplified Arabic',
                                    'size' => 12,
                                    'bold' => true
                                ]
                            ];

                            $sheet->getStyle("A1:U1")->applyFromArray($style);
                            $sheet->setHeight(1, 30);

                            $style = [
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Simplified Arabic',
                                    'size' => 12,
                                    'bold' => false
                                ]
                            ];

                            $sheet->getDefaultStyle()->applyFromArray($style);
                            $sheet->fromArray($data);
                        });
                    })
                        ->store('xlsx', storage_path('tmp/'));
                    \Log\Model\Log::saveNewLog('PAYMENTS_EXPORTED', trans('sponsorship::application.Exported the payments report'));
                    return response()->json(['status' => true ,'download_token' => $token ]);
                } else {
                    return response()->json(['status' => false]);
                }
            }

            return response()->json(['Payments' => $return['list'], 'total' => $return['total']]);
        }

        // paginate statistic payment's beneficiary of logged sponsor
        public function beneficiaries(Request $request)
        {

            ini_set('max_execution_time',0);
            set_time_limit(0);
            Ini_set('memory_limit','2048M');
            $this->authorize('manageSponsor',\Common\Model\SponsorshipsCases::class);


    //        $this->authorize('manage', Payments::class);
            $user = \Auth::user();

            $result = PaymentsCases::filterForSponsor($request->page, $user->organization_id, $request->all());

            if ($request->action == 'export') {
                if (sizeof($result) != 0) {
                    $data = [];
                    foreach ($result as $key => $value) {
                        $data[$key][trans('sponsorship::application.#')] = $key + 1;
                        foreach ($value as $k => $v) {
                            if ($k != 'id' && $k != 'case_id' && $k != 'pre_guardian_secondary_mobile' && $k != 'pre_guardian_primary_mobile' && $k != 'pre_guardian_phone') {
                                if (!$v) {
                                    $v = '-';
                                }
                                if (substr($k, 0, 10) === "recipient_") {
                                    $data[$key][trans('sponsorship::application.' . substr($k, 10)) . " ( " . trans('sponsorship::application.recipient') . " ) "] = $v;
                                } elseif (substr($k, 0, 7) === "mother_") {
                                    $data[$key][trans('sponsorship::application.' . substr($k, 7)) . " ( " . trans('sponsorship::application.mother') . " ) "] = $v;
                                } elseif (substr($k, 0, 7) === "father_") {
                                    $data[$key][trans('sponsorship::application.' . substr($k, 7)) . " ( " . trans('sponsorship::application.father') . " ) "] = $v;
                                } elseif (substr($k, 0, 9) === "guardian_") {
                                    $data[$key][trans('sponsorship::application.' . substr($k, 9)) . " ( " . trans('sponsorship::application.guardian') . " ) "] = $v;
                                } else {
                                    $data[$key][trans('sponsorship::application.' . $k)] = $v;
                                }
                            }
                        }
                    }
                    $token = md5(uniqid());
                    \Excel::create('export_' . $token, function ($excel) use ($data) {
                        $excel->sheet(trans('sponsorship::application.payments report'), function ($sheet) use ($data) {
                            $sheet->setStyle([
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 11,
                                    'bold' => true
                                ]
                            ]);
                            $sheet->setAllBorders('thin');
                            $sheet->setfitToWidth(true);
                            $sheet->setRightToLeft(true);
                            $style = [
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Simplified Arabic',
                                    'size' => 12,
                                    'bold' => true
                                ]
                            ];

                            $sheet->getStyle("A1:Z1")->applyFromArray($style);
                            $sheet->setHeight(1, 30);

                            $style = [
                               'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Simplified Arabic',
                                    'size' => 12,
                                    'bold' => false
                                ]
                            ];

                            $sheet->getDefaultStyle()->applyFromArray($style);
                            $sheet->fromArray($data);
                        });
                    })
                        ->store('xlsx', storage_path('tmp/'));
                    return response()->json([ 'status' => true , 'download_token' => $token ]);

                } else {
                    return response()->json(['status' => false]);
                }
            }
            return response()->json(['cases' => $result['list'], 'total' => $result['total']]);
        }

        // get cases of Sponsor according filters ( ExportToWord , paginate , xlsx )
        public function persons(Request $request)
        {
        
            ini_set('max_execution_time',0);
            set_time_limit(0);
            Ini_set('memory_limit','2048M');
            $this->authorize('manageSponsor',\Common\Model\SponsorshipsCases::class);
            
            $user = \Auth::user();
            if($request->action =='Basic') {
               $items= SponsorshipCases::getBasicPerson($request->page,$user->organization_id,$request->all());
            }else{
               $items= SponsorshipCases::getPerson($request->page,$user->organization_id,$request->all());
            }
            
            if($request->action =='export' || $request->action =='Basic'){
                     if(sizeof($items) !=0){
                        foreach($items as $key =>$value){
                            $dat[$key][trans('setting::application.#')]=$key+1;
                            foreach($value as $k =>$v){
                                if($k != 'id' && $k != 'sponsor_id'&& $k != 'flag'&& $k != 'date_flag'){
                                    if(substr( $k, 0, 7 ) === "mother_"){
                                        $dat[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.mother')  ." ) "]= $v;
                                    }elseif(substr( $k, 0, 7 ) === "father_"){
                                        $dat[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.father')  ." ) "]= $v;
                                    }elseif(substr( $k, 0, 9 ) === "guardian_"){
                                        $dat[$key][trans('sponsorship::application.'.substr($k,9))." ( ".trans('sponsorship::application.guardian')  ." ) " ]= $v;
                                    }else{
                                        $dat[$key][trans('sponsorship::application.' . $k)]= $v;
                                    }
                                }
                            }
                        }
                        $data=$dat;
                         $token = md5(uniqid());
                         \Excel::create('export_' . $token, function($excel) use($data) {
                            $excel->sheet('Sheetname', function($sheet) use($data) {


                                $sheet->setStyle([
                                    'font' => [
                                        'name' => 'Calibri',
                                        'size' => 11,
                                        'bold' => true
                                    ]
                                ]);
                                $sheet->setAllBorders('thin');
                                $sheet->setfitToWidth(true);
                                $sheet->setRightToLeft(true);
                                $style = [
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font' =>[
                                        'name'      =>  'Simplified Arabic',
                                        'size'      =>  12,
                                        'bold'      =>  true
                                    ]
                                ];

                                $sheet->getStyle("A1:CE1")->applyFromArray($style);
                                $sheet->setHeight(1,30);

                                $style = [
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font' =>[
                                        'name'      =>  'Simplified Arabic',
                                        'size'      =>  12,
                                        'bold' => false
                                    ]
                                ];

                                $sheet->getDefaultStyle()->applyFromArray($style);
                                $sheet->fromArray($data);
                            });
                        })
                             ->store('xlsx', storage_path('tmp/'));
                         return response()->json([ 'status' =>true , 'download_token' => $token ]);
                    }
                    
                    return response()->json([ 'status' =>false ]);
       
            }

            return response()->json(['cases'  =>$items ] );

        }

       // get Sponsor case info according ( sponsorship_case_id , case_id ,sponsor_id)
        public function person(Request $request)
        {

            $id = $request->sponsorship_case_id ;
            $sponsorshipCases =SponsorshipCases::where('id',$id)->first();
            $case = \Common\Model\CaseModel::fetch(array(
                        'action' => $request->get('action'),
                        'category_type' => 1,
                        'category'      => true,
                        'full_name'     => true,
                        'father_id'     => true,
                        'mother_id'     => true,
                        'guardian_id'   => true,
                    ),$sponsorshipCases->case_id);

            if (!$case) {
                //throw (new \Illuminate\Database\Eloquent\ModelNotFoundException)->setModel(CaseModel::class);
                return response()->json(array(
                    'error' => 'Not found!'
                ), 404);
            }
            //$this->authorize('view', $case);
            $case->sponsor_id=$sponsorshipCases->sponsor_id;
            return response()->json($case);
        }        
        


}
