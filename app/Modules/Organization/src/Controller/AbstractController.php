<?php

namespace Organization\Controller;

use App\Http\Controllers\Controller;
use Auth\Model\UserOrg;
use Illuminate\Http\Request;
use Organization\Model\Organization;
use Common\Model\Template;
use Illuminate\Support\Facades\Storage;
use App\Http\Helpers;
use Illuminate\Validation\Rule;
use Organization\Model\OrgSponsors;

class AbstractController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get Organization model searching by name (according to organization type)
    public function index()
    {
        $user = \Illuminate\Support\Facades\Auth::user();
        $UserOrg=$user->organization;
        $request= Request();
//        if($this->type == 2){
//            $organization_id = \Organization\Model\Organization::getAncestor($user->organization_id);
//        }else{
//            $organization_id=$user->organization_id;
//        }

        $language_id =  \App\Http\Helpers::getLocale();

        $all = $this->getModel()->with(['Locations' => function ($query) use($language_id) {
                                $query->where('language_id', '=', $language_id);
                            }]);

        $options=['type'=> $this->type];

        if($this->type == 1) {
//         descendants($organization_id)->
            $all->with(['category' => function ($query) use($language_id)  {
                $query->where('language_id', '=', $language_id);
            }]);
            $all->with(['district' => function ($query) use($language_id)  {
                $query->where('language_id', '=', $language_id);
            }]);
            $all->descendants($user->organization_id);
        }else{
//            $options=['type'=> $this->type,'parent_id'=>$user->organization_id];
            if($UserOrg->level == Organization::LEVEL_MASTER_CENTER) {
//                $options['parent_id']= $user->organization_id;
            }
        }

        if(isset($request->name)){
            if(!is_null($request->name) && $request->name!=''){
                if ($language_id == 2) {
                    $options['en_name'] = $request->name;
                }else{
                    $options['name'] = $request->name;
                }

            }
        }
        if (count($options) != 0) {
            $all =  $all
                ->where(function ($q) use ($options) {
                    $str = ['name'];
                    foreach ($options as $key => $value) {
                        if(in_array($key, $str)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
//                            $q->whereRaw("$key like  ?", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                });
        }

        if($this->type == 1) {
            $all->orderBy('level');
        }

        $itemsCount = isset($request->itemsCount)? $request->itemsCount : 0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$all->count());


        $all->selectRaw("char_organizations.* ,
                         CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");


        return response()->json($all->orderBy('name')->paginate($records_per_page));
    }


    // paginate or export Organization model using search filter 
    public function paginate(Request $request){
        $language_id =  \App\Http\Helpers::getLocale();
        $user = \Illuminate\Support\Facades\Auth::user();

        $all = $this->getModel()
            ->with(['Locations' => function ($query) use($language_id) {
                $query->where('language_id', '=', $language_id);
            }]);
            
            if($this->type == 1) {
                $all->with('parent')->with(['district' => function ($query) use($language_id){
                    $query->where('language_id', '=', $language_id);
                }])
                ->with(['city' => function ($query) use($language_id){
                    $query->where('language_id', '=', $language_id);
                }])
                    ->with(['category' => function ($query) use($language_id){
                    $query->where('language_id', '=', $language_id);
                }])
                ->with(['nearLocation' => function ($query) use($language_id){
                    $query->where('language_id', '=', $language_id);
                }]);
            }
           

        $char_organizations =['country', 'name', 'district_id', 'city_id','category_id',
                               'location_id','level','en_name','email','status',
                               'mobile','wataniya','phone','fax','container_share',
                               'registration_number'];
        
        $options = $request->all();
        
        if (!$user->super_admin) {
            $options['parent_id']= $user->organization_id;
        }
        
        $condition =['type'=> $this->type];
        foreach ($options as $key => $value) {
            if($key != 'page' && $key != 'itemsCount')
                if(in_array($key, $char_organizations)) {
                    if ( $value != "" ) {
                        $condition[$key] = $value;
                    }
                }
        }

        if (count($condition) != 0) {
            $all =  $all
                ->where(function ($q) use ($condition) {
                    $str = ['name','en_name'];
                    foreach ($condition as $key => $value) {
                        if(in_array($key, $str)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
//                            $q->whereRaw("$key like  ?", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                });
        }
        
        $to=null;
        $from=null;

        if(isset($request->created_at_to)){
            if($request->created_at_to !='null' && $request->created_at_to !=null){
                $to=date('Y-m-d',strtotime($request->created_at_to));
            }
        }

        if(isset($request->created_at_from)){
            if($request->created_at_from !='null'  && $request->created_at_from !=null){
                $from=date('Y-m-d',strtotime($request->created_at_from));
            }
        }

        if($from != null && $to != null) {
            $all->whereDate('created_at', '>=', $from);
            $all->whereDate('created_at', '<=', $to);
        }elseif($from != null && $to == null) {
            $all->whereDate('created_at', '>=', $from);
        }elseif($from == null && $to != null) {
            $all->whereDate('created_at', '<=', $to);
        }
        
        $dl_to=null;
        $dl_from=null;

        if(isset($request->deleted_at_to)){
            if($request->deleted_at_to !='null' && $request->deleted_at_to !=null){
                $dl_to=date('Y-m-d',strtotime($request->deleted_at_to));
            }
        }

        if(isset($request->deleted_at_from)){
            if($request->deleted_at_from !='null'  && $request->deleted_at_from !=null){
                $dl_from=date('Y-m-d',strtotime($request->created_at_from));
            }
        }

        if($dl_from != null && $dl_to != null) {
            $all->whereDate('deleted_at', '>=', $dl_from);
            $all->whereDate('deleted_at', '<=', $dl_to);
        }elseif($dl_from != null && $dl_to == null) {
            $all->whereDate('deleted_at', '>=', $dl_from);
        }elseif($dl_from == null && $dl_to != null) {
            $all->whereDate('deleted_at', '<=', $dl_to);
        }
                
        if($request->action == 'xlsx'){

            
            if(isset($request->items)){
               if(sizeof($request->items) > 0 ){
                $all->whereIn('id',$request->items);  
              }      
            } 
            
            $rows = $all->orderBy('name')->get();

            if(sizeof($rows) > 0 ){
                
                $token = md5(uniqid());
                \Maatwebsite\Excel\Facades\Excel::create('export_' . $token, function($excel) use($rows,$condition)  {
                    $excel->setTitle(trans('organization::application.organization'));
                    $excel->setDescription(trans('organization::application.organization'));
                    $excel->sheet(trans('organization::application.organization'),function($sheet)use($rows,$condition)   {
                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('solid');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11,
                                          'bold' => false]
                        ]);
                        $sheet->setOrientation('landscape');
                        $sheet->setRightToLeft(true);
                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Simplified Arabic',
                                'size' => 11,
                                'bold' => false
                            ]
                        ]);

                        if($condition['type'] == 1) {
                       
                             $sheet->setWidth(['A'=>5 ,'B'=> 40,'C'=> 40,'D'=> 35,'E'=> 15,'F'=> 15, 'G'=> 15,
                                          'H'=> 30,'I'=> 15,'J'=> 15,'K'=> 15,'L'=> 20,
                                          'M'=> 15, 'N'=> 15, 'O'=> 15, 'P'=> 15]);

                            $range="A1:P1";
                            $sheet->getStyle($range)->applyFromArray([
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font'      => [ 'name' =>'Simplified Arabic','size'=> 11, 'bold'=>  true ]
                            ]);

                       
                            $sheet ->setCellValue('A1',trans('organization::application.#'));
                            $sheet ->setCellValue('B1',trans('organization::application.organization_name_'));
                            $sheet ->setCellValue('C1',trans('organization::application.en_organization_name'));
                            $sheet ->setCellValue('D1',trans('organization::application.organization_parent'));
                            $sheet ->setCellValue('E1',trans('organization::application.organization_level'));
                            $sheet ->setCellValue('F1',trans('organization::application.organization_status'));
                            $sheet ->setCellValue('G1',trans('organization::application.Registration number'));
                            $sheet ->setCellValue('H1',trans('organization::application.email'));
                            $sheet ->setCellValue('I1',trans('organization::application.wataniya no'));
                            $sheet ->setCellValue('J1',trans('organization::application.mobile'));
                            $sheet ->setCellValue('K1',trans('organization::application.phone'));
                            $sheet ->setCellValue('L1',trans('organization::application.fax'));
                            $sheet ->setCellValue('M1',trans('organization::application.organization_country'));
                            $sheet ->setCellValue('N1',trans('organization::application.organization_district'));
                            $sheet ->setCellValue('O1',trans('organization::application.organization_city'));
                            $sheet ->setCellValue('P1',trans('organization::application.organization_location'));
                            $sheet ->setCellValue('Q1',trans('organization::application.organization_category'));

                            $z= 2;
                            foreach($rows as $k=>$v )
                            {
                                $sheet ->setCellValue('A'.$z,$k+1);
                                $sheet ->setCellValue('B'.$z,(is_null($v->name) || $v->name == ' ' ) ? '-' : $v->name);
                                $sheet ->setCellValue('C'.$z,(is_null($v->en_name) || $v->en_name == ' ' ) ? '-' : $v->en_name);
                                $sheet ->setCellValue('D'.$z,(is_null($v->parent_id) || $v->parent_id == ' ' ) ? '-' : $v->parent->name);
                                $sheet ->setCellValue('E'.$z,(is_null($v->level_name) || $v->level_name == ' ' ) ? '-' : $v->level_name);
                                $sheet ->setCellValue('F'.$z,(is_null($v->status_name) || $v->status_name == ' ' ) ? '-' : $v->status_name);
                                $sheet ->setCellValue('G'.$z,(is_null($v->registration_number) || $v->registration_number == ' ' ) ? '-' : $v->registration_number);
                                $sheet ->setCellValue('H'.$z,(is_null($v->email) || $v->email == ' ' ) ? '-' : $v->email);
                                $sheet ->setCellValue('I'.$z,(is_null($v->wataniya) || $v->wataniya == ' ' ) ? '-' : $v->wataniya);
                                $sheet ->setCellValue('J'.$z,(is_null($v->mobile) || $v->mobile == ' ' ) ? '-' : $v->mobile);
                                $sheet ->setCellValue('K'.$z,(is_null($v->phone) || $v->phone == ' ' ) ? '-' : $v->phone);
                                $sheet ->setCellValue('L'.$z,(is_null($v->fax) || $v->fax == ' ' ) ? '-' : $v->fax);
                                $sheet ->setCellValue('M'.$z,(is_null($v->country) || $v->country == ' ' ) ? '-' : $v->locations->name);
                                $sheet ->setCellValue('N'.$z,(is_null($v->district_id) || $v->district_id == ' ' ) ? '-' : $v->district->name);
                                $sheet ->setCellValue('O'.$z,(is_null($v->city_id) || $v->city_id == ' ' ) ? '-' : $v->city->name);
                                $sheet ->setCellValue('P'.$z,(is_null($v->location_id) || $v->location_id == ' ' ) ? '-' : $v->nearLocation->name);
                                $sheet ->setCellValue('Q'.$z,(is_null($v->category_id) || $v->category_id == ' ' ) ? '-' : $v->category->name);
                                $z++;
                            }
                        }else{
                            
                             $sheet->setWidth(['A'=>5 ,'B'=> 40,'C'=> 40,'D'=> 35,'E'=> 15,'F'=> 30, 'G'=> 15,
                                          'H'=> 15,'I'=> 15,'J'=> 15,'K'=> 15]);

                                $range="A1:K1";
                                $sheet->getStyle($range)->applyFromArray([
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font'      => [ 'name' =>'Simplified Arabic','size'=> 11, 'bold'=>  true ]
                                ]);


                            
                            $sheet ->setCellValue('A1',trans('organization::application.#'));
                            $sheet ->setCellValue('B1',trans('organization::application.organization_name_'));
                            $sheet ->setCellValue('C1',trans('organization::application.en_organization_name'));
                            $sheet ->setCellValue('D1',trans('organization::application.organization_status'));
                            $sheet ->setCellValue('E1',trans('organization::application.Registration number'));
                            $sheet ->setCellValue('F1',trans('organization::application.email'));
                            $sheet ->setCellValue('G1',trans('organization::application.wataniya no'));
                            $sheet ->setCellValue('H1',trans('organization::application.mobile'));
                            $sheet ->setCellValue('I1',trans('organization::application.phone'));
                            $sheet ->setCellValue('J1',trans('organization::application.fax'));
                            $sheet ->setCellValue('K1',trans('organization::application.organization_country'));

                            $z= 2;
                            foreach($rows as $k=>$v )
                            {
                                $sheet ->setCellValue('A'.$z,$k+1);
                                $sheet ->setCellValue('B'.$z,(is_null($v->name) || $v->name == ' ' ) ? '-' : $v->name);
                                $sheet ->setCellValue('C'.$z,(is_null($v->en_name) || $v->en_name == ' ' ) ? '-' : $v->en_name);
                                $sheet ->setCellValue('D'.$z,(is_null($v->status_name) || $v->status_name == ' ' ) ? '-' : $v->status_name);
                                $sheet ->setCellValue('E'.$z,(is_null($v->registration_number) || $v->registration_number == ' ' ) ? '-' : $v->registration_number);
                                $sheet ->setCellValue('F'.$z,(is_null($v->email) || $v->email == ' ' ) ? '-' : $v->email);
                                $sheet ->setCellValue('G'.$z,(is_null($v->wataniya) || $v->wataniya == ' ' ) ? '-' : $v->wataniya);
                                $sheet ->setCellValue('H'.$z,(is_null($v->mobile) || $v->mobile == ' ' ) ? '-' : $v->mobile);
                                $sheet ->setCellValue('I'.$z,(is_null($v->phone) || $v->phone == ' ' ) ? '-' : $v->phone);
                                $sheet ->setCellValue('J'.$z,(is_null($v->fax) || $v->fax == ' ' ) ? '-' : $v->fax);
                                $sheet ->setCellValue('K'.$z,(is_null($v->country) || $v->country == ' ' ) ? '-' : $v->locations->name);
                                $z++;
                            }
                        }
            
                        
                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['status' => true , 'download_token' => $token]);

            }

            return response()->json(['status' => false ]);

        }

        $itemsCount = ($request->itemsCount != null)? $request->itemsCount : 0;

        $records_per_page = Helpers::recordsPerPage($itemsCount,$all->count());
        return response()->json($all->orderBy('name')->paginate($records_per_page));
    }

    // get deleted Organization model searching by name (according to organization type)
    public function trash(Request $request)
    {
        $user = \Illuminate\Support\Facades\Auth::user();
        $organization_id = $user->organization_id;
        $UserOrg=$user->organization;

        
        $language_id =  \App\Http\Helpers::getLocale();
        
        $all = $this->getModel()->query()->onlyTrashed()->with('parent');
        $all->selectRaw("char_organizations.* ,
                         CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");

        $char_organizations =['country', 'name', 'district_id', 'city_id',
                               'location_id','level','en_name','email','status',
                               'mobile','wataniya','phone','fax','container_share',
                               'registration_number'];
        
        $options = $request->all();
        
        if (!$user->super_admin) {
            $options['parent_id']= $user->organization_id;
        }
        
        $condition =['type'=> $this->type];
        foreach ($options as $key => $value) {
            if($key != 'page' && $key != 'itemsCount')
                if(in_array($key, $char_organizations)) {
                    if ( $value != "" ) {
                        $condition[$key] = $value;
                    }
                }
        }

        if (count($condition) != 0) {
            $all =  $all
                ->where(function ($q) use ($condition) {
                    $str = ['name','en_name'];
                    foreach ($condition as $key => $value) {
                        if(in_array($key, $str)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
//                            $q->whereRaw("$key like  ?", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                });
        }
        
        $to=null;
        $from=null;

        if(isset($request->created_at_to)){
            if($request->created_at_to !='null' && $request->created_at_to !=null && 
                    $request->created_at_to !=' '  && $request->created_at_to !=''){
                $to=date('Y-m-d',strtotime($request->created_at_to));
            }
        }

        if(isset($request->created_at_from)){
            if($request->created_at_from !='null'  && $request->created_at_from !=null && 
                    $request->created_at_from !=' '  && $request->created_at_from !=''){
                $from=date('Y-m-d',strtotime($request->created_at_from));
            }
        }

        if($from != null && $to != null) {
            $all->whereDate('created_at', '>=', $from);
            $all->whereDate('created_at', '<=', $to);
        }elseif($from != null && $to == null) {
            $all->whereDate('created_at', '>=', $from);
        }elseif($from == null && $to != null) {
            $all->whereDate('created_at', '<=', $to);
        }
        
        $dl_to=null;
        $dl_from=null;

        if(isset($request->deleted_at_to)){
            if($request->deleted_at_to !='null' && $request->deleted_at_to !=null && 
                    $request->deleted_at_to !=' '  && $request->deleted_at_to !=''){
                $dl_to=date('Y-m-d',strtotime($request->deleted_at_to));
            }
        }

        if(isset($request->deleted_at_from)){
            if($request->deleted_at_from !='null'  && $request->deleted_at_from !=null && 
                    $request->deleted_at_from !=' '  && $request->deleted_at_from !=''){
                $dl_from=date('Y-m-d',strtotime($request->created_at_from));
            }
        }

        if($dl_from != null && $dl_to != null) {
            $all->whereDate('deleted_at', '>=', $dl_from);
            $all->whereDate('deleted_at', '<=', $dl_to);
        }elseif($dl_from != null && $dl_to == null) {
            $all->whereDate('deleted_at', '>=', $dl_from);
        }elseif($dl_from == null && $dl_to != null) {
            $all->whereDate('deleted_at', '<=', $dl_to);
        }
                
        if($request->action == 'xlsx'){

            
            if(isset($request->items)){
               if(sizeof($request->items) > 0 ){
                $all->whereIn('id',$request->items);  
              }      
            } 
            
            $rows = $all->orderBy('name')->get();

            if(sizeof($rows) > 0 ){
                
                $token = md5(uniqid());
                \Maatwebsite\Excel\Facades\Excel::create('export_' . $token, function($excel) use($rows,$condition)  {
                    $excel->setTitle(trans('organization::application.organization'));
                    $excel->setDescription(trans('organization::application.organization'));
                    $excel->sheet(trans('organization::application.organization'),function($sheet)use($rows,$condition)   {
                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('solid');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11,
                                          'bold' => false]
                        ]);
                        $sheet->setOrientation('landscape');
                        $sheet->setRightToLeft(true);
                        $sheet->setStyle([
                            'font' => [
                                'name' => 'Simplified Arabic',
                                'size' => 11,
                                'bold' => false
                            ]
                        ]);

                        if($condition['type'] == 1) {
                       
                             $sheet->setWidth(['A'=>5 ,'B'=> 40,'C'=> 40,'D'=> 35,'E'=> 15,'F'=> 15, 'G'=> 15,
                                          'H'=> 30,'I'=> 15,'J'=> 15,'K'=> 15,'L'=> 20,
                                          'M'=> 15, 'N'=> 15, 'O'=> 15, 'P'=> 15, 'Q'=> 25, 'R'=> 25]);

                            $range="A1:P1";
                            $sheet->getStyle($range)->applyFromArray([
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font'      => [ 'name' =>'Simplified Arabic','size'=> 11, 'bold'=>  true ]
                            ]);

                       
                            $sheet ->setCellValue('A1',trans('organization::application.#'));
                            $sheet ->setCellValue('B1',trans('organization::application.organization_name_'));
                            $sheet ->setCellValue('C1',trans('organization::application.en_organization_name'));
                            $sheet ->setCellValue('D1',trans('organization::application.organization_parent'));
                            $sheet ->setCellValue('E1',trans('organization::application.organization_level'));
                            $sheet ->setCellValue('F1',trans('organization::application.organization_status'));
                            $sheet ->setCellValue('G1',trans('organization::application.Registration number'));
                            $sheet ->setCellValue('H1',trans('organization::application.email'));
                            $sheet ->setCellValue('I1',trans('organization::application.wataniya no'));
                            $sheet ->setCellValue('J1',trans('organization::application.mobile'));
                            $sheet ->setCellValue('K1',trans('organization::application.phone'));
                            $sheet ->setCellValue('L1',trans('organization::application.fax'));
                            $sheet ->setCellValue('M1',trans('organization::application.organization_country'));
                            $sheet ->setCellValue('N1',trans('organization::application.organization_district'));
                            $sheet ->setCellValue('O1',trans('organization::application.organization_city'));
                            $sheet ->setCellValue('P1',trans('organization::application.organization_location'));
                            $sheet ->setCellValue('Q1',trans('organization::application.created_at'));
                            $sheet ->setCellValue('R1',trans('organization::application.deleted_at'));
                            $z= 2;
                            foreach($rows as $k=>$v )
                            {
                                $sheet ->setCellValue('A'.$z,$k+1);
                                $sheet ->setCellValue('B'.$z,(is_null($v->name) || $v->name == ' ' ) ? '-' : $v->name);
                                $sheet ->setCellValue('C'.$z,(is_null($v->en_name) || $v->en_name == ' ' ) ? '-' : $v->en_name);
                                $sheet ->setCellValue('D'.$z,(is_null($v->parent_id) || $v->parent_id == ' ' ) ? '-' : $v->parent->name);
                                $sheet ->setCellValue('E'.$z,(is_null($v->level_name) || $v->level_name == ' ' ) ? '-' : $v->level_name);
                                $sheet ->setCellValue('F'.$z,(is_null($v->status_name) || $v->status_name == ' ' ) ? '-' : $v->status_name);
                                $sheet ->setCellValue('G'.$z,(is_null($v->registration_number) || $v->registration_number == ' ' ) ? '-' : $v->registration_number);
                                $sheet ->setCellValue('H'.$z,(is_null($v->email) || $v->email == ' ' ) ? '-' : $v->email);
                                $sheet ->setCellValue('I'.$z,(is_null($v->wataniya) || $v->wataniya == ' ' ) ? '-' : $v->wataniya);
                                $sheet ->setCellValue('J'.$z,(is_null($v->mobile) || $v->mobile == ' ' ) ? '-' : $v->mobile);
                                $sheet ->setCellValue('K'.$z,(is_null($v->phone) || $v->phone == ' ' ) ? '-' : $v->phone);
                                $sheet ->setCellValue('L'.$z,(is_null($v->fax) || $v->fax == ' ' ) ? '-' : $v->fax);
                                $sheet ->setCellValue('M'.$z,(is_null($v->country) || $v->country == ' ' ) ? '-' : $v->locations->name);
                                $sheet ->setCellValue('N'.$z,(is_null($v->district_id) || $v->district_id == ' ' ) ? '-' : $v->district->name);
                                $sheet ->setCellValue('O'.$z,(is_null($v->city_id) || $v->city_id == ' ' ) ? '-' : $v->city->name);
                                $sheet ->setCellValue('P'.$z,(is_null($v->location_id) || $v->location_id == ' ' ) ? '-' : $v->nearLocation->name);
                                $sheet ->setCellValue('Q'.$z,(is_null($v->created_at) || $v->created_at == ' ' ) ? '-' : $v->created_at);
                                $sheet ->setCellValue('R'.$z,(is_null($v->deleted_at) || $v->deleted_at == ' ' ) ? '-' : $v->deleted_at);
                                $z++;
                            }
                        }else{
                            
                             $sheet->setWidth(['A'=>5 ,'B'=> 40,'C'=> 40,'D'=> 35,'E'=> 15,'F'=> 30, 'G'=> 15,
                                          'H'=> 15,'I'=> 15,'J'=> 15,'K'=> 15, 'L'=> 25, 'M'=> 25]);

                                $range="A1:M1";
                                $sheet->getStyle($range)->applyFromArray([
                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font'      => [ 'name' =>'Simplified Arabic','size'=> 11, 'bold'=>  true ]
                                ]);


                            
                            $sheet ->setCellValue('A1',trans('organization::application.#'));
                            $sheet ->setCellValue('B1',trans('organization::application.organization_name_'));
                            $sheet ->setCellValue('C1',trans('organization::application.en_organization_name'));
                            $sheet ->setCellValue('D1',trans('organization::application.organization_status'));
                            $sheet ->setCellValue('E1',trans('organization::application.Registration number'));
                            $sheet ->setCellValue('F1',trans('organization::application.email'));
                            $sheet ->setCellValue('G1',trans('organization::application.wataniya no'));
                            $sheet ->setCellValue('H1',trans('organization::application.mobile'));
                            $sheet ->setCellValue('I1',trans('organization::application.phone'));
                            $sheet ->setCellValue('J1',trans('organization::application.fax'));
                            $sheet ->setCellValue('K1',trans('organization::application.organization_country'));
                            $sheet ->setCellValue('L1',trans('organization::application.created_at'));
                            $sheet ->setCellValue('M1',trans('organization::application.deleted_at'));
                            
                            $z= 2;
                            foreach($rows as $k=>$v )
                            {
                                $sheet ->setCellValue('A'.$z,$k+1);
                                $sheet ->setCellValue('B'.$z,(is_null($v->name) || $v->name == ' ' ) ? '-' : $v->name);
                                $sheet ->setCellValue('C'.$z,(is_null($v->en_name) || $v->en_name == ' ' ) ? '-' : $v->en_name);
                                $sheet ->setCellValue('D'.$z,(is_null($v->status_name) || $v->status_name == ' ' ) ? '-' : $v->status_name);
                                $sheet ->setCellValue('E'.$z,(is_null($v->registration_number) || $v->registration_number == ' ' ) ? '-' : $v->registration_number);
                                $sheet ->setCellValue('F'.$z,(is_null($v->email) || $v->email == ' ' ) ? '-' : $v->email);
                                $sheet ->setCellValue('G'.$z,(is_null($v->wataniya) || $v->wataniya == ' ' ) ? '-' : $v->wataniya);
                                $sheet ->setCellValue('H'.$z,(is_null($v->mobile) || $v->mobile == ' ' ) ? '-' : $v->mobile);
                                $sheet ->setCellValue('I'.$z,(is_null($v->phone) || $v->phone == ' ' ) ? '-' : $v->phone);
                                $sheet ->setCellValue('J'.$z,(is_null($v->fax) || $v->fax == ' ' ) ? '-' : $v->fax);
                                $sheet ->setCellValue('K'.$z,(is_null($v->country) || $v->country == ' ' ) ? '-' : $v->locations->name);
                                $sheet ->setCellValue('L'.$z,(is_null($v->created_at) || $v->created_at == ' ' ) ? '-' : $v->created_at);
                                $sheet ->setCellValue('M'.$z,(is_null($v->deleted_at) || $v->deleted_at == ' ' ) ? '-' : $v->deleted_at);
                                $z++;
                            }
                        }
            
                        
                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['status' => true , 'download_token' => $token]);

            }

            return response()->json(['status' => false ]);

        }

        $itemsCount = ($request->itemsCount != null)? $request->itemsCount : 0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$all->count());
        
        if($this->type == 1) {
            $all->orderBy('level');
        }
        return response()->json($all->orderBy('name')->paginate($records_per_page));
        
    }

    // get all Organization (according to organization type)
    public function collection()
    {
        $entries=array();
        $user = \Illuminate\Support\Facades\Auth::user();
        $UserType=$user->type;
        $UserOrg_=$user->organization;
        $organization_id=$user->organization_id;
        $UserOrgType=$UserOrg_->type;

        $language_id =  \App\Http\Helpers::getLocale();

        if($this->type == 2){
            if($UserType == 2){
                $connector =OrgSponsors::getOrgsConnector(UserOrg::getUserOrgIds($user->id));
                if(sizeof($connector) >0){
                    $all = Organization::where(function ($q) use ($connector) {
                        $q->where('type',$this->type);
                        $q->whereIn('id',$connector);
                    });
                    $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");

                    $entries =  $all->orderBy('name')->get();
                }

            }
            else{
                if($user->super_admin || $UserOrg_->level == Organization::LEVEL_BRANCH_CENTER){
                    $all = Organization::where('type', $this->type);

                    $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");

                    $entries =  $all->get();
                }else{
                    $connector =OrgSponsors::getConnector($user->organization_id);
                    if(sizeof($connector) >0){
                        $all = Organization::where(function ($q) use ($connector) {
                            $q->where('type',$this->type);
                            $q->whereIn('id',$connector);
                        });

                        $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");

                        $entries =  $all->orderBy('name')->get();
                    }
                }
            }
        }
        else{
            if($UserType == 2){
                $all = Organization::wherein('id', function ($query) use ($user) {
                                             $query->select('organization_id')
                                                   ->from('char_user_organizations')
                                                   ->where('user_id', '=', $user->id);
                                        });
                $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");

                $entries =  $all->orderBy('name')->get();
            }else{
                if($UserOrgType == 2) {
                    $connector = OrgSponsors::getSponsorOrg($user->organization_id);
                    if (sizeof($connector) > 0) {
                        $all = Organization::where(function ($q) use ($connector) {
                            $q->whereIn('id', $connector);
                        });
                        $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");

                        $entries =  $all->orderBy('name')->get();
                    }
                }else{


                    $direct = request()->query('direct', 0);
                    $all = Organization::descendants($organization_id, $direct? 1 : null)->where('type', $this->type);
                    $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");


                    $entries =  $all->orderBy('name')->get();
                }
            }
        }

        return response()->json($entries);
    }
    public function compositeCollection()
    {
        $entries=array();
        $user = \Illuminate\Support\Facades\Auth::user();
        $UserType=$user->type;
        $UserOrg_=$user->organization;
        $organization_id=$user->organization_id;
        $UserOrgType=$UserOrg_->type;

        $language_id =  \App\Http\Helpers::getLocale();

        if($this->type == 2){
            if($UserType == 2){
                $connector =OrgSponsors::getOrgsConnector(UserOrg::getUserOrgIds($user->id));
                $connector[]=$organization_id;
                if(sizeof($connector) >0){
                    $all = Organization::where(function ($q) use ($connector) {
                        $q->where('type',$this->type);
                        $q->whereIn('id',$connector);
                    });
                    $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");

                    $entries =  $all->orderBy('name')->get();
                }

            }
            else{
                if($user->super_admin || $UserOrg_->level == Organization::LEVEL_BRANCH_CENTER){
                    $all = Organization::where('type', $this->type);

                    $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");

                    $entries =  $all->get();
                }else{
                    $connector =OrgSponsors::getConnector($user->organization_id);
                    $connector[]=$organization_id;
                    if(sizeof($connector) >0){
                        $all = Organization::where(function ($q) use ($connector) {
                            $q->where('type',$this->type);
                            $q->whereIn('id',$connector);
                        });

                        $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");

                        $entries =  $all->orderBy('name')->get();
                    }
                }
            }
        }
        else{
            if($UserType == 2){
                $all = Organization::wherein('id', function ($query) use ($user) {
                                             $query->select('organization_id')
                                                   ->from('char_user_organizations')
                                                   ->where('user_id', '=', $user->id);
                                        });
                $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");

                $entries =  $all->orderBy('name')->get();
            }else{
                if($UserOrgType == 2) {
                    $connector = OrgSponsors::getSponsorOrg($user->organization_id);
                    if (sizeof($connector) > 0) {
                        $all = Organization::where(function ($q) use ($connector) {
                            $q->whereIn('id', $connector);
                        });
                        $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");

                        $entries =  $all->orderBy('name')->get();
                    }
                }else{


                    $direct = request()->query('direct', 0);
                    $all = Organization::descendants($organization_id, $direct? 1 : null)->where('type', $this->type);
                    $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");


                    $entries =  $all->orderBy('name')->get();
                }
            }
        }

        return response()->json($entries);
    }

    // get all Organization (according to organization type)
    public function all()
    {
        $user = \Illuminate\Support\Facades\Auth::user();
        $UserOrg=$user->organization;
        $UserOrgType=$UserOrg->type;
        $UserType=$user->type;

        $entries = array();
        $language_id =  \App\Http\Helpers::getLocale();

        if($UserOrgType == 2) {
            if($UserType == 2){
                $connector =OrgSponsors::getOrgsConnector(UserOrg::getUserOrgIds($user->id));
                if(sizeof($connector) >0){
                    $all = Organization::where(function ($q) use ($connector) {
                        $q->where('type',$this->type);
                        $q->whereIn('id',$connector);
                    });
                    $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");

                    $entries =  $all->orderBy('name')->get();
                }

            }else{
                $connector = OrgSponsors::getSponsorOrg($user->organization_id);
                if (sizeof($connector) > 0) {
                    $all = Organization::where(function ($q) use ($connector) {
                        $q->whereIn('id', $connector);
                    });
                    $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");

                    $entries =  $all->orderBy('name')->get();
                }
            }

        }else{

            if($UserType == 2){
                $all = Organization::wherein('id', function ($query) use ($user) {
                    $query->select('organization_id')
                        ->from('char_user_organizations')
                        ->where('user_id', '=', $user->id);
                });
                $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");

                $entries =  $all->orderBy('name')->get();
            }else{
                $all = Organization::descendants($user->organization_id);
                $all->selectRaw("char_organizations.* ,
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ");

                $entries =  $all->orderBy('name')->get();
            }
        }

        return response()->json($entries);
    }

    // create new Organization model (according to organization type)
    public function store(Request $request)
    {
        try {
            $user = \Illuminate\Support\Facades\Auth::user();
            $organization_id = $user->organization_id;
            $response = array();
            $input = [
                'name' => trim(strip_tags($request->get('name'))),
                'type' => $this->type,
                'country' => $request->get('country'),
                'status' => $request->get('status'),
                'parent_id' => $request->get('parent_id')
            ];
            $type= $this->type;
            $rules = [
                'name' => Rule::unique('char_organizations')->where(function ($query) use($type,$organization_id){
                        $query->where('type', $type);
                        $query->where('parent_id', $organization_id);
                     }),
                'country' => 'required|integer',
                'status' => 'required'
            ];

            if ($this->type == \Organization\Model\AbstractOrganization::TYPE_ORGANIZATION) {
                $input['level']   = strip_tags($request->get('level'));
                $rules['level']   = 'required|integer';
                $rules['category_id']   = 'required';
                $input['category_id']   = $request->get('category_id');
                $input['district_id']   = strip_tags($request->get('district_id'));
                $input['city_id']   = $request->get('city_id');
                $input['location_id']   = $request->get('location_id');
                $rules['district_id'] = $rules['city_id']  = $rules['location_id'] = $rules['country'];
            }

            $error = \App\Http\Helpers::isValid($input,$rules);
            if($error)
                return response()->json($error);


            $org = $this->getModel();

            $inputs = ['en_name','email','mobile','phone','fax','wataniya','registration_number'];
            foreach ($inputs as $key) {
                if (isset($request->$key)) {
                    $org->$key=$request->$key;
                }
            }

            $org->name = $input['name'];
            $org->country = $input['country'];
            $org->status = $input['status'];
            $org->type = $this->type;

            if ($this->type == \Organization\Model\AbstractOrganization::TYPE_SPONSOR) {
                $org->container_share =0;
//                $org->container_share = $request->input('container_share');
//                $org->parent_id = $user->organization_id;
                $org->parent_id = \Organization\Model\Organization::getAncestor($user->organization_id);
                $action='SPONSOR_CREATED';
                $message=trans('organization::application.Added a sponsor') ;
            }else{
                $org->level = $input['level'];
                $org->parent_id = $input['parent_id'];
                $org->district_id = $input['district_id'];
                $org->city_id = $input['city_id'];
                $org->location_id = $input['location_id'];
                $action='ORGANIZATION_CREATED';
                $message= trans('organization::application.Has added an organization') ; 
            }
            if ($org->save()) {
                \Log\Model\Log::saveNewLog($action,$message . ' " '.$input['name']  . ' " ');
                return response()->json(["status" => 'success' ,
                                          "id" => $org->id ,
                                          "msg" => trans('setting::application.The row is inserted to db')]);
            }
            return response()->json(["status" => 'failed' , "msg" => trans('setting::application.The row is not insert to db')]);
        } catch (Exception $e) {
            $response["status"] = 'error';
            $response["error_code"] = $e->getCode();
            $response["msg"] = 'Databaseerror'; /* $response["msg"] = $e->getMessage(); */
            return response()->json($response);
        }
    }

    // get existed object model by id (according to organization type)
    public function show($id)
    {
        $org = $this->findOrFail($id);
        $this->authorize('view', $org);

        $org->template = Template::where('organization_id', $org->id)
                ->where('template', Template::TPL_CANDIDATE)
                ->first();
        return response()->json($org);
    }

    // update exist Organization object model by id (according to organization type)
    public function update(Request $request, $id)
    {

        try {

            $org = $this->findOrFail($id);
            $this->authorize('update', $org);

            $response = array();

            $founded =Organization::where([
                'type'=>$org->type,
                'parent_id'=>$org->parent_id,
                'name'=>$request->input('name')
            ])->first();

            if($founded){
                if($founded->id != $id){
                    if($this->type == 1){
                        $subMsg =trans('organization::application.The name entered is used as the name of this organization');
                    }else{
                        $subMsg =trans('organization::application.The name entered is used as the name of a sponsor of this organization');

                    }
                    return ['type'=>$this->type ,'status'=>false ,'error_type' =>'validate_inputs' ,'msg' => ['name'=>[$subMsg]]];
                }
            }

            $input = [
                'name' => $request->input('name'),
                'country' => $request->input('country'),
                'status' => $request->input('status'),
                'parent_id' => $request->input('parent_id')
            ];

            $type= $this->type;
            $rules = [
//                'name' => Rule::unique('char_organizations')
//                             ->where(function ($query) use($type,$id){
//                                $query->where('type', $type);
//            //                    $query->where('id', $id);
//                            })->ignore($id),
//                'name'=> Rule::unique('char_organizations')->where(function ($query) use($type,$id){
//                    $query->where('type', $type);
//                })->ignore($id, 'id')
//,
//              'name' => 'required|unique:char_organizations,name,type,'.$id.','.$this->type,
                'country' => 'required|integer',
                'status' => 'required|integer'
            ];
            if ($this->type == \Organization\Model\AbstractOrganization::TYPE_ORGANIZATION) {
                $input['district_id']   = strip_tags($request->get('district_id'));
                $input['category_id']   = $request->get('category_id');
                $input['city_id']   = $request->get('city_id');
                $input['location_id']   = $request->get('location_id');
                $rules['district_id'] = $rules['city_id']  = $rules['location_id'] = $rules['country'];
                $action='ORGANIZATION_UPDATED';
                $message=trans('organization::application.edited the organization data') ;
            }

            $error = \App\Http\Helpers::isValid($input,$rules);
            if($error)
                return response()->json($error);

            if ($this->type == \Organization\Model\AbstractOrganization::TYPE_SPONSOR) {
                $input['container_share'] = $request->input('container_share');
                $action='SPONSOR_UPDATED';
                $message= trans('organization::application.edited sponsor data');
            }

            $inputs = ['en_name','email','mobile','phone','fax','wataniya','registration_number'];
            foreach ($inputs as $key) {
                if (isset($request->$key)) {
                    $input[$key]=$request->$key;
                }
            }

            if (Organization::where("id", $id)->update($input)) {
                \Log\Model\Log::saveNewLog($action,$message . ' " '.$input['name']  . ' " ');
                $response["data"] = $org;
                $response["status"] = 'success';
                $response["msg"] = trans('setting::application.The row is updated');
            } else {
                $response["status"] = 'failed';
                $response["msg"] = trans('setting::application.There is no update');
            }

            return response()->json($response);
        } catch (Exception $e) {
            $response["status"] = 'error';
            $response["error_code"] = $e->getCode();
            $response["msg"] = 'Databaseerror'; /* $response["msg"] = $e->getMessage(); */
            return response()->json($response);
        }
    }

    // restore deleted Organization model by id (according to organization type)
    public function restore(Request $request,$id)
    {
        $user = \Illuminate\Support\Facades\Auth::user();

        $entity = $this->getModel()->query()->onlyTrashed()->where('id',$id)->first();

        $entity->deleted_at = Null;
        if ($entity->save()) {
            $message=trans('organization::application.restore organization') . ' " '.$entity->name  . ' " ';
            \Log\Model\Log::saveNewLog('ORGANIZATION_RESTORE',$message);
            return response()->json(["status" => 'success' , "msg" => $message]);
        }
        return response()->json(["status" => 'failed' , "msg" =>trans('organization::application.error on restore organization') . ' : " '.$entity->name  . ' " ']);
    }

    // update existed object model by id  (according to organization type)
    public function destroy(Request $request, $id)
    {
        $org = $this->findOrFail($id);
        $this->authorize('delete', $org);
        $name=$org->name;

        if ($this->type == \Organization\Model\AbstractOrganization::TYPE_SPONSOR) {
            $action='SPONSOR_DELETED';
            $message=trans('organization::application.Deleted a sponsor');
        }else{
            $action='ORGANIZATION_DELETED';
            $message= trans('organization::application.Deleted an organization');
        }

        if ($org->delete()) {
            $response["status"] = 'success';
            \Log\Model\Log::saveNewLog($action,$message . ' " '.$name. ' " ');
            $response["msg"] = trans('setting::application.The row is deleted from db');
        } else {
            $response["status"] = 'failed';
            $response["msg"] = trans('setting::application.The row is not deleted from db');
        }
        return response()->json($response);
    }

    // get connect Sponsor of organization by id
    public function getSponsors($id)
    {
        $this->authorize('connectSponsor', Organization::class);
        $org = $this->findOrFail($id);

        $user = \Illuminate\Support\Facades\Auth::user();
        $UserOrg=$user->organization;

//        if($org->level == Organization::LEVEL_MASTER_CENTER) {
        $org->list=OrgSponsors::getOrgSponsors($id);
//        }else{
//            $org->list=OrgSponsors::getRelatedOrgSponsors($id,$org->parent_id);
//        }
//        $org->list=OrgSponsors::getRelatedOrgSponsors($id,$org->parent_id);
        return response()->json($org);
    }

    // get logo to the organization of login user
    public function thumbnail(Request $request)
    {

        $user = \Illuminate\Support\Facades\Auth::user();
        $org = $this->findOrFail($user->organization_id);

        $image=base_path('storage/app/emptyUser.png');

        if($org->logo){
            $LogoImage = base_path('storage/app/' . $org->logo);
            if (is_file($LogoImage)) {
                $image = base_path('storage/app/' . $org->logo);
            }
        }

        return response()->file($image);
    }

    // update logo of organization by id
    public function logoUpload(Request $request)
    {
        $organization_id = $request->input('organization_id');
        $org = $this->findOrFail($organization_id);
        $this->authorize('upload', $org);

        $logoFile = $request->file('file');

        $uploadFile = $request->file('file');
        if ($uploadFile->isValid()) {
            $less = 100*1024 ;
            $bigger = 300*1024 ;

            $size = $logoFile->getSize() ;
            if ($less < $size && $size < $bigger){
                $path = $logoFile->store('logos');
                if ($path) {
                    if (Storage::has($org->logo)) {
                        Storage::delete($org->logo);
                    }

                    $org->logo = $path;
                    $org->save();
                }

                if ($this->type == \Organization\Model\AbstractOrganization::TYPE_SPONSOR) {
                    $action='SPONSOR_UPLOAD';
                    $message=trans('organization::application.Uploaded the logo of a sponsor') ;
                }else{
                    $action='ORGANIZATION_UPLOAD';
                    $message= trans('organization::application.Uploaded the logo of an organization');
                }
                \Log\Model\Log::saveNewLog($action,$message . ' " '.$org->name. ' " ');
                return response()->json($org);

            }else {
                return response()->json(['status' =>'error','msg'=>trans('document::application.the uploaded file is out of allowed range for file size')]);
            }
        }
        return response()->json(['status' =>'error','msg'=>trans('document::application.invalid file')]);

    }

    // get logo of organization by id
    public function logo($id)
    {
        $org = $this->findOrFail($id);
        $this->authorize('view', $org);

        $image=base_path('storage/app/emptyUser.png');

        if($org->logo){
            $LogoImage = base_path('storage/app/' . $org->logo);
            if (is_file($LogoImage)) {
                $image = base_path('storage/app/' . $org->logo);
            }
        }

        return response()->file($image);

    }

    // get template ( forms or reports ) of organization by id
    public function template($id)
    {
        $org = $this->findOrFail($id);
        $this->authorize('view', $org);

        $template = Template::where('organization_id', $id)
            ->where('template', Template::TPL_CANDIDATE)
            ->first();
        if (!$template) {
            throw (new ModelNotFoundException())->setModel(Template::class);
        }

        return response()->file(base_path('storage/app/' . $template->filename));
    }

    // update template ( forms or reports ) of organization by id
    public function templateUpload(Request $request)
    {
        $organization_id = $request->input('organization_id');
        $org = $this->findOrFail($organization_id);
        $this->authorize('upload', $org);

        $templateFile = $request->file('file');
        $templatePath = $templateFile->store('templates');
        if ($templatePath) {
            $template = Template::where('organization_id', $org->id)
                ->where('template', Template::TPL_CANDIDATE)
                ->first();
            if (!$template) {
                $template = new Template();
                $template->organization_id = $org->id;
                $template->template = Template::TPL_CANDIDATE;
                $template->filename = $templatePath;
                $template->save();
            } else {
                if (Storage::has($template->filename)) {
                    Storage::delete($template->filename);
                }
                Template::where('organization_id', $org->id)
                    ->where('template', Template::TPL_CANDIDATE)
                    ->update(['filename' => $templatePath]);
            }

            if ($this->type == \Organization\Model\AbstractOrganization::TYPE_SPONSOR) {
                $action='SPONSOR_UPLOAD';
                $message=trans('organization::application.Uploaded a sponsor template');
            }else{
                $action='ORGANIZATION_UPLOAD';
                $message= trans('organization::application.Uploaded an organization template');
            }
            \Log\Model\Log::saveNewLog($action,$message . ' " '.$org->name. ' " ');

            return response()->json($template);
        }

        return false;
    }

}
