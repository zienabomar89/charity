<?php

namespace Organization\Controller\reports;

use App\Http\Controllers\Controller;
use Auth\Model\UserOrg;
use Illuminate\Http\Request;
use Organization\Model\AdministrativeReports\AdministrativeReport;
use Organization\Model\AdministrativeReports\AdministrativeReportData;
use Organization\Model\AdministrativeReports\AdministrativeReportTab;
use Organization\Model\AdministrativeReports\AdministrativeReportTabColumns;
use Organization\Model\AdministrativeReports\AdministrativeReportTabColumnsList;
use Organization\Model\AdministrativeReports\AdministrativeReportTabOption;
use Organization\Model\AdministrativeReports\AdministrativeReportTabOptionList;
use Organization\Model\Organization;
use Illuminate\Support\Facades\Storage;
use App\Http\Helpers;
use Illuminate\Validation\Rule;

class TabController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // create new AdministrativeReportTab model
    public function store(Request $request)
    {
//        $this->authorize('create', AdministrativeReportTab::class);
//        'description'=> 'required'
        $rules =  ['report_id'=> 'required',  'type'=> 'required', 'title'=> 'required'];


        $error = \App\Http\Helpers::isValid($request->all(),$rules);
        if($error)
            return response()->json($error);

        $founded=AdministrativeReportTab::where(['title' =>$request->title,'report_id' =>$request->report_id])->first();
        if(!is_null($founded)){
            $response["status"]= 'failed_valid';
            $response["msg"]['title']=[trans('forms::application.This value is already in use')];
            return response()->json($response);
        }

        $entry = new AdministrativeReportTab();
        $inputs =  ['report_id','title', 'description','type'];

        foreach ($inputs as $key){
            if(isset($request->$key)){
                $entry->$key = $request->$key;
            }
        }

        if($entry->save()) {
            \Log\Model\Log::saveNewLog('ADMINSTRATIVE_REPORT_TAB_CREATED', trans('organization::application.He added a new report tab') . ' "' . $entry->title . '" ');
            return response()->json(['status' => 'success','msg'=>trans('organization::application.The row is inserted to db')]);
        }

        return response()->json(['status' => 'failed','msg'=>trans('organization::application.The row is not insert to db')]);
    }

    // get existed object model by id
    public function show($id)
    {
        $entry = AdministrativeReportTab::findOrFail($id);
        $entry->options= AdministrativeReportTabOption::with(['Options'])->where(['tab_id'=>$id])->get();
//        $this->authorize('view', $entry);
        return response()->json($entry);

    }

    // get columns of tab by id
    public function columns($id)
    {
        $entry = AdministrativeReportTab::findOrFail($id);
//        $this->authorize('view', $entry);
        return response()->json(['columns' => AdministrativeReportTabColumns::with(['Options'])->where(['tab_id'=>$id])->get()]);
    }
    // get elements of tab by id
    public function elements($id)
    {
        $entry = AdministrativeReportTab::findOrFail($id);
//        $this->authorize('view', $entry);
        return response()->json(['elements' => AdministrativeReportTabOption::with(['Options'])->where(['tab_id'=>$id])->get()]);
    }

    // update exist AdministrativeReportTab object model by id
    public function update(Request $request,$id)
    {

        $entry = AdministrativeReportTab::findOrFail($id);
//            $this->authorize('update', $entry);
//        , 'description'=> 'required'
        $rules =  ['report_id'=> 'required','title'=> 'required', 'type'=> 'required'];

        $error = \App\Http\Helpers::isValid($request->all(),$rules);
        if($error)
            return response()->json($error);

        $founded=AdministrativeReportTab::where(['title' =>$request->title,'report_id' =>$request->report_id])->first();
        if(!is_null($founded)){
            if($founded->id != $id){
                $response["status"]= 'failed_valid';
                $response["msg"]['title']=[trans('forms::application.This value is already in use')];
                return response()->json($response);
            }
        }


        $inputs =  ['report_id','title', 'description','type'];
        foreach ($inputs as $key){
            if(isset($request->$key)){
                $entry->$key = $request->$key;
            }
        }

        if($entry->save()) {
            \Log\Model\Log::saveNewLog('ADMINSTRATIVE_REPORT_TAB_UPDATED', trans('organization::application.He edited the properties of the report tab') . ' "' . $entry->title . '" ');
            return response()->json(['status' => 'success','msg'=>trans('organization::application.The row is updated')]);
        }
        return response()->json(['status' => 'failed','msg'=>trans('organization::application.There is no update')]);

    }

    // update existed object model by id
    public function destroy($id,Request $request)
    {
        $entry = AdministrativeReportTab::findOrFail($id);
//        $this->authorize('delete', $entry);
        $title = $entry->title;

        \DB::beginTransaction();

        $data = AdministrativeReportData::whereIn('option_id',function ($q) use ($id){
            $q->select('id')
                ->from('char_administrative_report_tab_options')
                ->where('tab_id', '=', $id);
        })->delete();
        $elements_list = AdministrativeReportTabColumnsList::whereIn('column_id',function ($q) use ($id){
                        $q->select('id')
                            ->from('char_administrative_report_tab_columns')
                            ->where('tab_id', '=', $id);
                    })->delete();

        $elements = AdministrativeReportTabColumns::where('tab_id', '=', $id)->delete();

        $options_list = AdministrativeReportTabOptionList::whereIn('option_id',function ($q) use ($id){
                        $q->select('id')
                            ->from('char_administrative_report_tab_options')
                            ->where('tab_id', '=', $id);
                    })->delete();

        $options = AdministrativeReportTabOption::where('tab_id', '=', $id)->delete();

        if($entry->destroy($id))
        {
            \DB::commit();
            \Log\Model\Log::saveNewLog('ADMINSTRATIVE_REPORT_TAB_DELETED',trans('organization::application.He deleted the report')  . ' "' . $title . '" ');
            return response()->json(['status' => 'success','msg'=>trans('organization::application.The row is deleted from db')]);
        }
        \DB::rollBack();
        return response()->json(['status' => 'failed','msg'=>trans('organization::application.The row is not deleted from db')]);
    }
}
