<?php

namespace Organization\Controller\reports;

use App\Http\Controllers\Controller;
use Auth\Model\UserOrg;
use Illuminate\Http\Request;
use Organization\Model\AdministrativeReports\AdministrativeReportData;
use Organization\Model\AdministrativeReports\AdministrativeReportTab;
use Organization\Model\AdministrativeReports\AdministrativeReportTabColumns;
use Organization\Model\AdministrativeReports\AdministrativeReportTabColumnsList;
use App\Http\Helpers;
use Illuminate\Validation\Rule;
use Organization\Model\AdministrativeReports\AdministrativeReportTabOptionList;

class ColumnsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // create new AdministrativeReportTabColumns model
    public function store(Request $request)
    {
//        $this->authorize('create', AdministrativeReportTabColumns::class);

        \DB::beginTransaction();
        $rules =  ['tab_id'=> 'required','label'=> 'required', 'name'=> 'required', 'type'=> 'required', 'priority'=> 'required'];

        $error = \App\Http\Helpers::isValid($request->all(),$rules);
        if($error)
            return response()->json($error);

        $tab = AdministrativeReportTab::findOrFail($request->tab_id);
        $exist = AdministrativeReportTabColumns::where(function ($q) use ($request, $tab) {
            $q->where('name', $request->name);
            $q->whereIn('tab_id', function ($sq) use ($tab) {
                $sq->select('id')
                    ->from('char_administrative_report_tab')
                    ->where('report_id', $tab->report_id);
            });
        })->first();
        if($exist){
            $msg['name']= [trans('organization::application.exist')];
            return response()->json( ['status'=>'failed_valid' ,'error_type' =>'validate_inputs' ,'msg' => $msg]);
        }

        $entry = new AdministrativeReportTabColumns();
        $inputs = ['tab_id','label','name','type','priority'];

        foreach ($inputs as $key){
            if(isset($request->$key)){
                $entry->$key = $request->$key;
            }
        }

        if($entry->save()) {
            if($entry->type == 5 || $entry->type == '5'){
                $options = $request->options;
                if(sizeof($options) !=0) {
                    $options_ = [];
                    foreach ($options as $item) {
                        $options_[]= [
                            'column_id' => $entry->id,
                            'label' => $item['label'],
                            'value' => $item['value'],
                        ];
                    }

                    ;
                }

                if(AdministrativeReportTabColumnsList::insert($options_)){

                }else{
                    DB::rollBack();
                    return response()->json(['status' => 'failed','msg'=>trans('organization::application.The row is not insert to db')]);
                }

            }
            \Log\Model\Log::saveNewLog('ADMINSTRATIVE_TAB_COLUMNS_CREATED', trans('organization::application.He added a new tab option') . ' "' . $entry->label . '" ');
            \DB::commit();
            return response()->json(['status' => 'success','msg'=>trans('organization::application.The row is inserted to db')]);
        }

        return response()->json(['status' => 'failed','msg'=>trans('organization::application.The row is not insert to db')]);
    }

    // get existed object model by id
    public function show($id)
    {
        $entry = AdministrativeReportTabColumns::findOrFail($id);
//        $this->authorize('view', $entry);
        return response()->json($entry);

    }

    // update exist AdministrativeReportTabColumns object model by id
    public function update(Request $request,$id)
    {

        \DB::beginTransaction();
        $entry = AdministrativeReportTabColumns::findOrFail($id);
//            $this->authorize('update', $entry);

        $rules =  ['tab_id'=> 'required','label'=> 'required', 'name'=> 'required', 'type'=> 'required', 'priority'=> 'required'];

        $error = \App\Http\Helpers::isValid($request->all(),$rules);
        if($error)
            return response()->json($error);

        $tab = AdministrativeReportTab::findOrFail($entry->tab_id);
        $exist = AdministrativeReportTabColumns::where(function ($q) use ($request, $tab) {
            $q->where('name', $request->name);
            $q->whereIn('tab_id', function ($sq) use ($tab) {
                $sq->select('id')
                    ->from('char_administrative_report_tab')
                    ->where('report_id', $tab->report_id);
            });
        })->first();

        if($exist){
            if($exist->id != $id){
                $msg['name']= [trans('organization::application.exist')];
                return response()->json( ['status'=>'failed_valid' ,'error_type' =>'validate_inputs' ,'msg' => $msg]);
            }

        }

        $inputs = ['label','name','type','priority'];
        foreach ($inputs as $key){
            if(isset($request->$key)){
                $entry->$key = $request->$key;
            }
        }

        if($entry->save()) {
            $action_1=true;

            if($entry->type == 5 || $entry->type == '5'){
                $options = $request->options;
                if(sizeof($options) !=0) {
                    $action_2=false;
                    $count = AdministrativeReportTabColumnsList::where('column_id', '=' , $id)->count();
                    if($count == 0){
                        $options_ = [];
                        foreach ($options as $item) {
                            $options_[]= [
                                'column_id' => $entry->id,
                                'label' => $item['label'],
                                'value' => $item['value'],
                            ];
                        };

                        if(AdministrativeReportTabColumnsList::insert($options_)){
                            $action_2=true;
                        }
                    }
                    else{
                        $options_ = [];

                        $RemovedOption = $request->RemovedOptions ;

                        if(sizeof($RemovedOption) == 0 ){
                            foreach ($options as $item) {
                                if($item != null){
                                    if(isset($item['id'])){
                                        AdministrativeReportTabColumnsList::where(['id' => $item['id'] , 'column_id' =>$id ])->update([
                                            'label' => $item['label'],
                                            'value' => $item['value'],
                                        ]);
                                    }else{
                                        $options_[] = ['column_id' => $id, 'label' => $item['label'], 'value' => $item['value'], 'weight' => $item['weight']];
                                    }
                                }
                            }


                        }
                        else {
                            foreach ($RemovedOption as $item) {
                                if($item != null){
                                    AdministrativeReportTabColumnsList::destroy($item['id']);
                                    AdministrativeReportData::where(['column_id'=>$entry->id,'column_list_id'=>$item['id']])->update(['column_list_id'=>null]);

                                }
                            }
                            foreach ($options as $item) {
                                if($item != null){

                                    if(isset($item['id'])){
                                        AdministrativeReportTabColumnsList::where(['id' => $item['id'] , 'column_id' =>$id ])->update( [
                                            'label' => $item['label'],
                                            'value' => $item['value'],
                                            'weight' => $item['weight']
                                        ]);
                                    }else{
                                        $options_[] = ['column_id' => $id, 'label' => $item['label'], 'value' => $item['value'], 'weight' => $item['weight']];
                                    }
                                }
                            }
                        }

                        if(sizeof($options_) !=0){
                            if(AdministrativeReportTabColumnsList::insert($options_)){
                                $action_2=true;
                            }
                        }

                    }
                }else{
                    $action_2=true;
                }
            }else{
                $action_2=true;
                AdministrativeReportTabColumnsList::where(['column_id' =>$entry->id] )->delete();
                AdministrativeReportData::where(['column_id'=>$entry->id])->update(['column_list_id'=>null]);
            }


            if( $action_2 || $action_1){
                \DB::commit();
                \Log\Model\Log::saveNewLog('ADMINSTRATIVE_TAB_COLUMNS_UPDATED', trans('organization::application.He edited the properties of the report tab option') . ' "' . $entry->label . '" ');
                return response()->json(['status' => 'success','msg'=>trans('organization::application.The row is updated')]);
            }
        }

        DB::rollBack();
        return response()->json(['status' => 'failed','msg'=>trans('organization::application.There is no update')]);

    }

    // update existed object model by id
    public function destroy($id,Request $request)
    {
        $entry = AdministrativeReportTabColumns::findOrFail($id);
//        $this->authorize('delete', $entry);
        $label = $entry->label;
        $data = AdministrativeReportData::where('column_id',$id)->delete();
        $elements_list = AdministrativeReportTabColumnsList::where('column_id',$id)->delete();
        \DB::beginTransaction();
        if($entry->destroy($id)) {
            \DB::commit();
            \Log\Model\Log::saveNewLog('ADMINSTRATIVE_TAB_COLUMNS_DELETED',trans('organization::application.He deleted the report')  . ' "' . $label . '" ');
            return response()->json(['status' => 'success','msg'=>trans('organization::application.The row is deleted from db')]);
        }
        \DB::rollBack();
        return response()->json(['status' => 'failed','msg'=>trans('organization::application.The row is not deleted from db')]);
    }
}
