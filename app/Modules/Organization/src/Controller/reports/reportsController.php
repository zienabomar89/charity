<?php

namespace Organization\Controller\reports;

use App\Http\Controllers\Controller;
use Auth\Model\User;
use Auth\Model\UserOrg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Organization\Model\AdministrativeReports\AdministrativeReport;
use Organization\Model\AdministrativeReports\AdministrativeReportData;
use Organization\Model\AdministrativeReports\AdministrativeReportTab;
use Organization\Model\AdministrativeReports\AdministrativeReportTabOption;
use Organization\Model\AdministrativeReports\AdministrativeReportTabOptionList;
use Organization\Model\AdministrativeReports\OrgReports;
use Organization\Model\Organization;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class reportsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // filter user report
    public function filter(Request $request){
        $this->authorize('manage', AdministrativeReport::class);

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit', '2048M');

        $reports = AdministrativeReport::filter($request->all());
        if($request->action == 'excel'){
            if(sizeof($reports) > 0) {

                $main = time();
                $name = 'storage/'.$main;
                $dirName = base_path($name);
                if (!is_dir($dirName)) {
                    @mkdir($dirName, 0777, true);
                }

                $storage_path = $main.'/';
                $tabs = [];
                $on_reports = [];
                foreach ($reports as $k1=>$report) {
                    $report_title = (is_null($report->title) || $report->title == ' ') ? null : $report->title;
                    if (!is_null($report_title)) {
                        $title_ = $report->title . '-' . $report->organization->name ;
                            if(sizeof($report->tabs) > 0 ){
                              if (!in_array($title_, $on_reports)) {
                               \Maatwebsite\Excel\Facades\Excel::create($title_, function ($excel) use ($report, $tabs) {
                                $excel->setTitle(trans('organization::application.report data'));
                                 $excel->setDescription(trans('organization::application.report data'));
                                foreach ($report->tabs as $ktab => $vtab) {
                                    $Org = OrgReports::getReportOrg($report->id,array());
                                    $tb_title = (is_null($vtab->title) || $vtab->title == ' ') ? null : $vtab->title;
                                    if (!is_null($tb_title)) {
                                        if (!in_array($tb_title, $tabs)) {
                                            $tab_name = trans('organization::application.tab_') . ' ( ' . ( $ktab+1 ). ' ) ';
                                            if ($report->category == 2 || $report->category == '2') {
                                                $excel->sheet($tab_name, function ($sheet) use ($report, $vtab,$Org,$tb_title) {
                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setAllBorders('solid');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setHeight(1, 40);
                                                    $sheet->getDefaultStyle()->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => ['name' => 'Simplified Arabic', 'size' => 11]
                                                    ]);
                                                    $sheet->setOrientation('landscape');
                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setStyle(['font' => ['name' => 'Simplified Arabic', 'size' => 11, 'bold' => true]]);
                                                    $sheet->setWidth(['A' => 5, 'B' => 20]);
                                                    $range = "A1:B1";
                                                    $sheet->getStyle($range)->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => ['name' => 'Simplified Arabic', 'size' => 11, 'bold' => true]
                                                    ]);

                                                    $Index = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                                        'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
                                                        'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ',
                                                        'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ',
                                                        'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ',
                                                        'EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EU', 'EV', 'EW', 'EX', 'EY', 'EZ',
                                                        'FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG', 'FH', 'FI', 'FJ', 'FK', 'FL', 'FM', 'FN', 'FO', 'FP', 'FQ', 'FR', 'FS', 'FT', 'FU', 'FV', 'FW', 'FX', 'FY', 'FZ',
                                                        'GA', 'GB', 'GC', 'GD', 'GE', 'GF', 'GG', 'GH', 'GI', 'GJ', 'GK', 'GL', 'GM', 'GN', 'GO', 'GP', 'GQ', 'GR', 'GS', 'GT', 'GU', 'GV', 'GW', 'GX', 'GY', 'GZ',
                                                        'HA', 'HB', 'HC', 'HD', 'HE', 'HF', 'HG', 'HH', 'HI', 'HJ', 'HK', 'HL', 'HM', 'HN', 'HO', 'HP', 'HQ', 'HR', 'HS', 'HT', 'HU', 'HV', 'HW', 'HX', 'HY', 'HZ',
                                                        'IA', 'IB', 'IC', 'ID', 'IE', 'IF', 'IG', 'IH', 'II', 'IJ', 'IK', 'IL', 'IM', 'IN', 'IO', 'IP', 'IQ', 'IR', 'IS', 'IT', 'IU', 'IV', 'IW', 'IX', 'IY', 'IZ',
                                                        'JA', 'JB', 'JC', 'JD', 'JE', 'JF', 'JG', 'JH', 'JI', 'JJ', 'JK', 'JL', 'JM', 'JN', 'JO', 'JP', 'JQ', 'JR', 'JS', 'JT', 'JU', 'JV', 'JW', 'JX', 'JY', 'JZ',
                                                        'KA', 'KB', 'KC', 'KD', 'KE', 'KF', 'KG', 'KH', 'KI', 'KJ', 'KK', 'KL', 'KM', 'KN', 'KO', 'KP', 'KQ', 'KR', 'KS', 'KT', 'KU', 'KV', 'KW', 'KX', 'KY', 'KZ',
                                                        'LA', 'LB', 'LC', 'LD', 'LE', 'LF', 'LG', 'LH', 'LI', 'LJ', 'LK', 'LL', 'LM', 'LN', 'LO', 'LP', 'LQ', 'LR', 'LS', 'LT', 'LU', 'LV', 'LW', 'LX', 'LY', 'LZ',
                                                        'MA', 'MB', 'MC', 'MD', 'ME', 'MF', 'MG', 'MH', 'MI', 'MJ', 'MK', 'ML', 'MM', 'MN', 'MO', 'MP', 'MQ', 'MR', 'MS', 'MT', 'MU', 'MV', 'MW', 'MX', 'MY', 'MZ',
                                                        'NA', 'NB', 'NC', 'ND', 'NE', 'NF', 'NG', 'NH', 'NI', 'NJ', 'NK', 'NL', 'NM', 'NN', 'NO', 'NP', 'NQ', 'NR', 'NS', 'NT', 'NU', 'NV', 'NW', 'NX', 'NY', 'NZ',
                                                        'OA', 'OB', 'OC', 'OD', 'OE', 'OF', 'OG', 'OH', 'OI', 'OJ', 'OK', 'OL', 'OM', 'ON', 'OO', 'OP', 'OQ', 'OR', 'OS', 'OT', 'OU', 'OV', 'OW', 'OX', 'OY', 'OZ'
                                                    ];

                                                    if (sizeof($vtab->options) > 0 && sizeof($vtab->columns) > 0) {
                                                        $sheet->setCellValue('A1', $tb_title);
                                                        $sheet->getRowDimension(1)->setRowHeight(-1);
                                                        $sheet->getRowDimension(2)->setRowHeight(-1);
                                                        $step_ = (sizeof($Org) * sizeof($vtab->columns));
                                                        $sheet->mergeCells('A1:'.$Index[$step_].'1');
                                                        $sheet->cells('A1:'.$Index[$step_].'1', function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('left');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });
                                                        $sheet->setCellValue('A2', trans('organization::application.#'));
                                                        $sheet->setCellValue('B2', trans('organization::application.for'));
                                                        $sheet->mergeCells('A2:A3');
                                                        $sheet->mergeCells('B2:B3');
                                                        $sheet->getRowDimension(1)->setRowHeight(-1);
                                                        $sheet->getRowDimension(2)->setRowHeight(-1);
                                                        $sheet->cells('A1:B2', function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('center');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });

                                                        $opInc = 4;
                                                        foreach ($vtab->options as $kop => $vop) {
                                                            $sheet->setCellValue('A' . $opInc, $kop + 1);
                                                            $sheet->setCellValue('B' . $opInc, (is_null($vop->label) || $vop->label == ' ') ? ' ' : $vop->label);

                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $sheet->getStyle('A' . $opInc)->getAlignment()->setWrapText(true);
                                                            $sheet->getStyle('B' . $opInc)->getAlignment()->setWrapText(true);

                                                            $sheet->cells('A' . $opInc . ':' . 'B' . $opInc, function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $opInc++;
                                                        }

                                                        $offSet = 2;
                                                        $step = sizeof($vtab->columns);
                                                        foreach ($Org as $k => $v) {
                                                            $sheet->setCellValue($Index[$offSet] . '2', (is_null($v->organization_name) || $v->organization_name == ' ') ? ' ' : $v->organization_name);
                                                            $sheet->mergeCells($Index[$offSet] . '2:' . $Index[$offSet + $step - 1] . '2');
                                                            $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $i = $offSet;
                                                            foreach ($vtab->columns as $kcol => $vcol) {
                                                                $sheet->setCellValue($Index[$i] . '3', (is_null($vcol->label) || $vcol->label == ' ') ? ' ' : $vcol->label);
                                                                $sheet->setWidth([$Index[$i] => 15]);
                                                                $sheet->getStyle($Index[$i] . '3')->getAlignment()->setWrapText(true);
                                                                $sheet->getStyle($Index[$i])->getAlignment()->setWrapText(true);
                                                                $i++;
                                                            }

                                                            $sheet->getStyle($Index[$offSet] . '2')->getAlignment()->setWrapText(true);
                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $sheet->cells($Index[$offSet] . '2:' . $Index[$i] . '2', function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $sheet->cells($Index[$offSet] . '3:' . $Index[$i] . '3', function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $offSet += $step;

                                                        }

                                                        $opInc = 4;
                                                        foreach ($vtab->options as $kop => $vop) {
                                                            $sheet->setCellValue('A' . $opInc, $kop + 1);
                                                            $sheet->setCellValue('B' . $opInc, (is_null($vop->label) || $vop->label == ' ') ? ' ' : $vop->label);

                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $sheet->getStyle('A' . $opInc)->getAlignment()->setWrapText(true);
                                                            $sheet->getStyle('B' . $opInc)->getAlignment()->setWrapText(true);

                                                            $sheet->cells('A' . $opInc . ':' . 'B' . $opInc, function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $offSet = 2;
                                                            foreach ($Org as $k => $v) {
                                                                foreach ($vtab->columns as $kcol => $vcol) {

                                                                    $orgData = AdministrativeReportData::OrgOptionData($vop->id, $v->id, $vcol->id);
                                                                    if(!($vcol->type == 1 || $vcol->type == 2)){
                                                                        if(is_null($orgData['value'])){
                                                                            $sheet->setCellValue($Index[$offSet] . $opInc,'_');
                                                                        }else{
                                                                            $sheet->setCellValue($Index[$offSet] . $opInc,$orgData['value']);
                                                                        }
                                                                        $sheet->setWidth([$Index[$offSet] => 35]);
                                                                    }else{
                                                                        if(is_null($orgData['value']) || $orgData['value'] == '' || $orgData['value'] == ' '){
                                                                            if($vcol->type == 1 || $vcol->type == 2 ){
                                                                                $sheet->setCellValue($Index[$offSet] . $opInc,0);
                                                                            }
                                                                        }else{
                                                                            $sheet->setCellValue($Index[$offSet] . $opInc,$orgData['value']);
                                                                        }
                                                                        $sheet->setWidth([$Index[$offSet] => 13]);
                                                                    }

                                                                    $sheet->setWidth([$Index[$offSet] => 16]);
                                                                    $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc . ':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });
                                                                    $sheet->getStyle($Index[$offSet] . $opInc)->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });
                                                                    $offSet += 1;
                                                                }
                                                            }
                                                            $opInc++;
                                                        }
                                                        $sheet->setCellValue('A' . $opInc, trans('organization::application.total'));
                                                        $sheet->mergeCells("A".$opInc .":B" . $opInc);
                                                        $sheet->getRowDimension(1)->setRowHeight(-1);
                                                        $sheet->getRowDimension(2)->setRowHeight(-1);
                                                        $sheet->cells("A".$opInc .":B" . $opInc, function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('center');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });

                                                        $offSet = 2;
                                                        foreach ($Org as $k => $v) {
                                                            foreach ($vtab->columns as $kcol => $vcol) {
                                                                if($vcol->type == 1 || $vcol->type == 2 ){
                                                                    $sum = AdministrativeReportData::OrgOptionSum($v->id, $vcol->id);
                                                                    $sheet->setCellValue($Index[$offSet] . $opInc,$sum);
                                                                    $sheet->setWidth([$Index[$offSet] => 13]);
                                                                }else{
                                                                    $sheet->setCellValue($Index[$offSet] . $opInc,'_');
                                                                    $sheet->setWidth([$Index[$offSet] => 35]);
                                                                }

                                                                $sheet->setWidth([$Index[$offSet] => 16]);
                                                                $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . $opInc . ':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });
                                                                $sheet->getStyle($Index[$offSet] . $opInc)->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . $opInc, function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });
                                                                $offSet += 1;
                                                            }
                                                        }

                                                    }
                                                });
                                            }
                                            else {
                                                $excel->sheet($tab_name, function ($sheet) use ($report, $vtab,$Org,$tb_title) {
                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setAllBorders('solid');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setHeight(1, 40);
                                                    $sheet->getDefaultStyle()->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => ['name' => 'Simplified Arabic', 'size' => 11]
                                                    ]);
                                                    $sheet->setOrientation('landscape');
                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setWidth(['A' => 5, 'B' => 20]);
                                                    $range = "A1:B1";
                                                    $sheet->getStyle($range)->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => ['name' => 'Simplified Arabic', 'size' => 11, 'bold' => true]
                                                    ]);

                                                    $Index = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                                        'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
                                                        'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ',
                                                        'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ',
                                                        'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ',
                                                        'EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EU', 'EV', 'EW', 'EX', 'EY', 'EZ',
                                                        'FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG', 'FH', 'FI', 'FJ', 'FK', 'FL', 'FM', 'FN', 'FO', 'FP', 'FQ', 'FR', 'FS', 'FT', 'FU', 'FV', 'FW', 'FX', 'FY', 'FZ',
                                                        'GA', 'GB', 'GC', 'GD', 'GE', 'GF', 'GG', 'GH', 'GI', 'GJ', 'GK', 'GL', 'GM', 'GN', 'GO', 'GP', 'GQ', 'GR', 'GS', 'GT', 'GU', 'GV', 'GW', 'GX', 'GY', 'GZ',
                                                        'HA', 'HB', 'HC', 'HD', 'HE', 'HF', 'HG', 'HH', 'HI', 'HJ', 'HK', 'HL', 'HM', 'HN', 'HO', 'HP', 'HQ', 'HR', 'HS', 'HT', 'HU', 'HV', 'HW', 'HX', 'HY', 'HZ',
                                                        'IA', 'IB', 'IC', 'ID', 'IE', 'IF', 'IG', 'IH', 'II', 'IJ', 'IK', 'IL', 'IM', 'IN', 'IO', 'IP', 'IQ', 'IR', 'IS', 'IT', 'IU', 'IV', 'IW', 'IX', 'IY', 'IZ',
                                                        'JA', 'JB', 'JC', 'JD', 'JE', 'JF', 'JG', 'JH', 'JI', 'JJ', 'JK', 'JL', 'JM', 'JN', 'JO', 'JP', 'JQ', 'JR', 'JS', 'JT', 'JU', 'JV', 'JW', 'JX', 'JY', 'JZ',
                                                        'KA', 'KB', 'KC', 'KD', 'KE', 'KF', 'KG', 'KH', 'KI', 'KJ', 'KK', 'KL', 'KM', 'KN', 'KO', 'KP', 'KQ', 'KR', 'KS', 'KT', 'KU', 'KV', 'KW', 'KX', 'KY', 'KZ',
                                                        'LA', 'LB', 'LC', 'LD', 'LE', 'LF', 'LG', 'LH', 'LI', 'LJ', 'LK', 'LL', 'LM', 'LN', 'LO', 'LP', 'LQ', 'LR', 'LS', 'LT', 'LU', 'LV', 'LW', 'LX', 'LY', 'LZ',
                                                        'MA', 'MB', 'MC', 'MD', 'ME', 'MF', 'MG', 'MH', 'MI', 'MJ', 'MK', 'ML', 'MM', 'MN', 'MO', 'MP', 'MQ', 'MR', 'MS', 'MT', 'MU', 'MV', 'MW', 'MX', 'MY', 'MZ',
                                                        'NA', 'NB', 'NC', 'ND', 'NE', 'NF', 'NG', 'NH', 'NI', 'NJ', 'NK', 'NL', 'NM', 'NN', 'NO', 'NP', 'NQ', 'NR', 'NS', 'NT', 'NU', 'NV', 'NW', 'NX', 'NY', 'NZ',
                                                        'OA', 'OB', 'OC', 'OD', 'OE', 'OF', 'OG', 'OH', 'OI', 'OJ', 'OK', 'OL', 'OM', 'ON', 'OO', 'OP', 'OQ', 'OR', 'OS', 'OT', 'OU', 'OV', 'OW', 'OX', 'OY', 'OZ'
                                                    ];

                                                    if (sizeof($vtab->options) > 0) {
                                                        $sheet->setCellValue('A2', trans('organization::application.#'));
                                                        $sheet->setCellValue('B2', trans('organization::application.for'));
                                                        $sheet->mergeCells('A2:A3');
                                                        $sheet->mergeCells('B2:B3');
                                                        $sheet->getRowDimension(1)->setRowHeight(-1);
                                                        $sheet->getRowDimension(2)->setRowHeight(-1);
                                                        $sheet->cells('A1:B2', function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('center');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });

                                                        if ($vtab->type == 3) {
                                                            $step_ = (sizeof($Org) * 2) + 1 ;
                                                        }else{
                                                            $step_ = sizeof($Org)  + 1 ;
                                                        }

                                                        $sheet->setCellValue('A1', $tb_title);
                                                        $sheet->mergeCells('A1:'.$Index[$step_].'1');
                                                        $sheet->cells('A1:'.$Index[$step_].'1', function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('right');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });
                                                        $offSet = 2;
                                                        foreach ($Org as $k => $v) {
                                                            $sheet->setCellValue($Index[$offSet] . '2', (is_null($v->organization_name) || $v->organization_name == ' ') ? ' ' : $v->organization_name);
                                                            if ($vtab->type == 3) {
                                                                $sheet->setCellValue($Index[$offSet] . '3', trans('organization::application.count'));
                                                                $sheet->setCellValue($Index[$offSet + 1] . '3', trans('organization::application.amount'));
                                                                $sheet->mergeCells($Index[$offSet] . '2:' . $Index[$offSet + 1] . '2');
                                                                $sheet->setWidth([$Index[$offSet] => 10, $Index[$offSet + 1] => 10]);
                                                                $sheet->getStyle($Index[$offSet + 1])->getAlignment()->setWrapText(true);
                                                                $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . '3:' . $Index[$offSet + 1] . '3', function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });

                                                                $sheet->getStyle($Index[$offSet] . '2')->getAlignment()->setWrapText(true);
                                                                $sheet->getStyle($Index[$offSet + 1] . '2')->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . '2:' . $Index[$offSet + 1] . '2', function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });
                                                                $offSet += 2;
                                                            }
                                                            else {
                                                                $sheet->setCellValue($Index[$offSet] . '3', trans('organization::application.value'));
                                                                $sheet->setWidth([$Index[$offSet] => 16]);
                                                                $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . '3:' . $Index[$offSet + 1] . '3', function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });

                                                                $sheet->getStyle($Index[$offSet] . '2')->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . '2', function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });
                                                                $offSet += 1;
                                                            }
                                                        }

                                                        $opInc = 4;
                                                        foreach ($vtab->options as $kop => $vop) {
                                                            $sheet->setCellValue('A' . $opInc, $kop + 1);
                                                            $sheet->setCellValue('B' . $opInc, (is_null($vop->label) || $vop->label == ' ') ? ' ' : $vop->label);

                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $sheet->getStyle('A' . $opInc)->getAlignment()->setWrapText(true);
                                                            $sheet->getStyle('B' . $opInc)->getAlignment()->setWrapText(true);

                                                            $sheet->cells('A' . $opInc . ':' . 'B' . $opInc, function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $offSet = 2;
                                                            foreach ($Org as $k => $v) {
                                                                $orgData = AdministrativeReportData::OrgOptionData($vop->id ,$v->id , null);

                                                                if ($vtab->type == 3) {
                                                                    if(is_numeric($orgData['value']) || is_double($orgData['value']) || is_float($orgData['value']) || is_int($orgData['value'])) {
                                                                        $sheet->setCellValue($Index[$offSet] . $opInc, $orgData['value']);
                                                                    }else{
                                                                        $sheet->setCellValue($Index[$offSet] . $opInc, 0);
                                                                    }

                                                                    if(is_numeric($orgData['amount']) || is_double($orgData['amount']) || is_float($orgData['amount']) || is_int($orgData['amount'])) {
                                                                        $sheet->setCellValue($Index[$offSet + 1] . $opInc, $orgData['amount']);
                                                                    }else{
                                                                        $sheet->setCellValue($Index[$offSet + 1] . $opInc, 0);
                                                                    }

                                                                    $sheet->setWidth([$Index[$offSet] => 10, $Index[$offSet + 1] => 10]);
                                                                    $sheet->getStyle($Index[$offSet + 1])->getAlignment()->setWrapText(true);
                                                                    $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc.':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });

                                                                    $sheet->getStyle($Index[$offSet] .$opInc)->getAlignment()->setWrapText(true);
                                                                    $sheet->getStyle($Index[$offSet + 1] . $opInc)->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc .':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });
                                                                    $offSet += 2;
                                                                }
                                                                else {
                                                                    if(is_null($orgData['value'])){
                                                                        $sheet->setCellValue($Index[$offSet] . $opInc,'_');
                                                                    }else{
                                                                        $sheet->setCellValue($Index[$offSet] . $opInc,$orgData['value']);
                                                                    }
                                                                    $sheet->setWidth([$Index[$offSet] => 16]);
                                                                    $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc .':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });

                                                                    $sheet->getStyle($Index[$offSet] . $opInc)->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });
                                                                    $offSet += 1;
                                                                }
                                                            }
                                                            $opInc++;
                                                        }
                                                    }
                                                });
                                            }
                                            $tabs[] = $tab_name;
                                        }
                                    }
                                }
                                $excel->setActiveSheetIndex(0);
                                })
                                ->store('xlsx', storage_path($storage_path));
                                 $on_reports[]=$title_;
                            }
                    }
                    }
                }

                $token = md5(uniqid());
                $zipFileName = storage_path('tmp/export_' . $token . '.zip');
                $zip = new \ZipArchive;

                if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true ) {
                    foreach ( glob( $dirName . '/*' ) as $fileName ) {
                        $file = basename( $fileName );
                        $zip->addFile( $fileName, $file );
                    }
                    $zip->close();
                    array_map('unlink', glob("$dirName/*.*"));
                    rmdir($dirName);
                    return response()->json(['status' => 'success' , 'download_token' => $token]);
                }

            }
            return response()->json(['status' => 'failed','msg'=>trans('organization::application.no report data')]);
        }

        $reports->map(function ($report) {
            $report->organizations  = OrgReports::getReportOrgs($report->id);
            $report->organizations_data  = OrgReports::getReportData($report->id);
            return $report;
        });

        return response()->json(['items' => $reports]);
    }

    // filter active report of organization
    public function OrgReport(Request $request){
        $this->authorize('manageOrg', AdministrativeReport::class);

        $reports = AdministrativeReport::OrgReport($request->all());
        $reports->map(function ($report,$user) {
            $report->organizations  = OrgReports::getReportRelatedOrg($report->id);
            $user = \Auth::user();
            $row = OrgReports::where(['organization_id'=>$user->organization_id,'report_id'=>$report->id])->first();
            if(is_null($row)){
                $report->has_form = false ;
            }else{
                $report->has_form = true ;
                $report->report_organization_status = $row->status ;
            }
            return $report;
        });
        return response()->json(['items' => $reports]);
    }

    // create new report with option
    public function store(Request $request)   {
        $this->authorize('create', AdministrativeReport::class);

           $rules = ['title'=> 'required', 'type'=> 'required','category'=> 'required',
                         'date_from'=> 'required', 'date_to'=> 'required','status'=> 'required'];


            $error = \App\Http\Helpers::isValid($request->all(),$rules);
            if($error)
                return response()->json($error);

            $user = \Auth::user();

            $entry = new AdministrativeReport();
            $entry->user_id = $user->id;
            $entry->organization_id = $user->organization_id;
            $entry->date = date('Y-m-d');
            $inputs = ['title','type','date','month','year','date_from','date_to','category','status','notes','template'];
            $dates = ['date','date_from','date_to'];

            $founded=AdministrativeReport::where(['title' =>$request->title,'organization_id' =>$user->organization_id])->first();
            if(!is_null($founded)){
                $response["status"]= 'failed_valid';
                $response["msg"]['title']=[trans('forms::application.This value is already in use')];
                return response()->json($response);
            }
            foreach ($inputs as $key){
                if(isset($request->$key)){
                    if(in_array($key,$dates)){
                        $entry->$key =date('Y-m-d',strtotime($request->$key));
                    }
                    else{
                        $entry->$key = $request->$key;
                    }
                }
            }

            if($entry->save()) {
                if(isset($request->duplicate_id)){
                    $tabs = AdministrativeReportTab::with(['options','options.optionList'])
                        ->where('report_id',$request->duplicate_id)->get();
                    foreach ($tabs as $key=>$value) {
                        unset($value->id);
                        unset($value->created_at);
                        unset($value->updated_at);
                        unset($value->deleted_at);

                        $tab_entry = new AdministrativeReportTab();
                        $inputs_t =  ['title', 'description','type'];

                        $tab_entry->report_id =$entry->id;
                        foreach ($inputs_t as $key){
                            if(isset($value->$key)){
                                $tab_entry->$key = $value->$key;
                            }
                        }
                        $tab_entry->save();

                        foreach ($value->options as $k=>$v) {

                            $old_option = $v->id;
                            unset($v->id);
                            unset($v->tab_id);
                            $v['tab_id']=$tab_entry->id;
                            $v_ = (array) $v;

                            $option_entry = new AdministrativeReportTabOption();
                            $option_entry->tab_id= $tab_entry->id;
                            $inputs_o = ['label','name','type','priority'];

                            foreach ($inputs_o as $key){
                                if(isset($v->$key)){
                                    $option_entry->$key = $v->$key;
                                }
                            }

                            $option_entry->save();

                            if($v->type == 2){
                                $options_ =[];

                                $options = AdministrativeReportTabOptionList::where('option_id',$old_option)->get();
                                foreach ($options as $item) {
                                    $options_[]= [
                                        'option_id' => $option_entry->id,
                                        'label' => $item['label'],
                                        'value' => $item['value'],
                                    ];
                                }
                                AdministrativeReportTabOptionList::insert($options_);
                            }
                        }
                    }
                }

                $Orgs = $request->target_organizations;
                $insert = [];
                foreach ($Orgs as $Org){
                    $insert[]=  ['organization_id'=> $Org,'user_id' => User::OrgManager($Org),'report_id' => $entry->id];
                }
                OrgReports::where(['report_id'=> $entry->id])->delete();
                OrgReports::insert($insert);

                \Log\Model\Log::saveNewLog('ADMINSTRATIVE_REPORT_CREATED', trans('organization::application.He added a new report') . ' "' . $entry->title . '" ');
                return response()->json(['status' => 'success','report_id'=>$entry->id,'msg'=>trans('organization::application.The row is inserted to db')]);
            }

        return response()->json(['status' => 'failed','msg'=>trans('organization::application.The row is not insert to db')]);
    }

    // get existed object model by id
    public function show($id)    {

        $report = AdministrativeReport::with(['tabs' => function ($query) {
                        $query->orderBy('tab_order', 'asc');
                        $query->orderBy('title', 'asc');
                    },'tabs.columns','tabs.columns.optionList','tabs.options','tabs.options.optionList'])->findOrFail($id);
        $this->authorize('view', $report);

        $user = \Auth::user();
        $report->is_mine = 0;
        if($report->organization_id == $user->organization_id){
            $report->is_mine = 1;
        }
        return response()->json($report);
    }

    // get existed organizations for report by id
    public function organizations($id)   {
        $report = AdministrativeReport::findOrFail($id);
        $report->getLocale = \App\Http\Helpers::getLocale();
        $report->organizations = OrgReports::getReportOrgsDetail($id);
        $this->authorize('view', $report);
        return response()->json($report);
    }

    // get existed related organizations for report by id
    public function related($id)   {
        $report = AdministrativeReport::findOrFail($id);
        $report->getLocale = \App\Http\Helpers::getLocale();
        $report->organizations = OrgReports::getReportRelatedOrgDetail($id);
        $this->authorize('view', $report);
        return response()->json($report);
    }

    // update exist report with option by id
    public function update(Request $request,$id)    {

        $entry = AdministrativeReport::findOrFail($id);
        $this->authorize('update', $entry);

        $user = \Auth::user();
        $rules = ['title'=> 'required', 'type'=> 'required', 'category'=> 'required', 'date_from'=> 'required', 'date_to'=> 'required','status'=> 'required'];

        $error = \App\Http\Helpers::isValid($request->all(),$rules);
        if($error)
            return response()->json($error);

        $founded=AdministrativeReportTab::where(['title' =>$request->title,'report_id' =>$request->report_id])->first();
        if(!is_null($founded)){
            if($founded->id != $id){
                $response["status"]= 'failed_valid';
                $response["msg"]['title']=[trans('forms::application.This value is already in use')];
                return response()->json($response);
            }
        }

        $inputs = ['title','type','date','month','year','date_from','date_to','status','category','notes','template'];
        $dates = ['date_from','date_to'];

        foreach ($inputs as $key){
            if(isset($request->$key)){
                if(in_array($key,$dates)){
                    $entry->$key =date('Y-m-d',strtotime($request->$key));
                }
                else{
                    $entry->$key = $request->$key;
                }
            }
        }

        if($entry->save()) {

            //                $UserType=$user->type;

            $Orgs = $request->target_organizations;
            $insert = [];
            foreach ($Orgs as $Org){
                $insert[]=  ['organization_id'=> $Org,'user_id' => User::OrgManager($Org),'report_id' => $entry->id];
            }
            OrgReports::where(['report_id'=> $entry->id])->delete();
            OrgReports::insert($insert);

            \Log\Model\Log::saveNewLog('ADMINSTRATIVE_REPORT_UPDATED', trans('organization::application.He edited the properties of the report') . ' "' . $entry->title . '" ');
            return response()->json(['status' => 'success','msg'=>trans('organization::application.The row is updated')]);
        }
        return response()->json(['status' => 'failed','msg'=>trans('organization::application.There is no update')]);
    }

    // update existed object model by id
    public function destroy($id,Request $request)  {
        $entry = AdministrativeReport::findOrFail($id);
        $this->authorize('delete', $entry);
        $title = $entry->title;
        if($entry->destroy($id)) {
            \Log\Model\Log::saveNewLog('ADMINSTRATIVE_REPORT_DELETED',trans('organization::application.He deleted the report')  . ' "' . $title . '" ');
            return response()->json(['status' => 'success','msg'=>trans('organization::application.The row is deleted from db')]);
        }
        return response()->json(['status' => 'failed','msg'=>trans('organization::application.The row is not deleted from db')]);
    }

    // get orgData of logged user organization using report_id
    public function getOrgData($id,Request $request)   {

        $report = AdministrativeReport::with(['tabs','tabs.columns','tabs.columns.optionList','tabs.options','tabs.options.optionList'])->findOrFail($id);
        $user = \Auth::user();

        if($report->category == 2 || $report->category == '2'){
            foreach ($report->tabs as $key=>$value){
                $value->saved = true;
                $value->required = true;

                if($key == 0){
                    $value->selected = true;
                    $value->disable = false;
                }else{
                    $value->selected = false;
                    $value->disable = true;
                }

                foreach ($value->columns as $k1=>$v1) {
                    $data = [];
                    if($v1->type == 1 || $v1->type == 2){
                        $v1->sum = 0 ;
                    }else{
                        $v1->sum = '_' ;
                    }

                    foreach ($value->options as $k=>$v) {
                        if($v1->type == 1 || $v1->type == 2){
                            $option_val =0 ;
                        }else if($v1->type == 6){
                            if($report->data_status == 3 || $report->data_status == '3'){
                                $option_val ="" ;
                            }else{
                                $option_val = date('Y/m/d');
                            }

                        }else{
                            $option_val =null ;
                        }

                        $founded =  AdministrativeReportData::where(['column_id' => $v1->id,'option_id' => $v->id,
                                                                     'organization_id'=>$user->organization_id])->first();

                        if(!is_null($founded)) {
                            if($v1->type == 5){
                                $option_val = $founded->column_list_id;
                            }else{
                                if(!is_null($founded->value)) {
                                    $vl = $founded->value ;
                                    if($v1->type == 1 || $v1->type == 2){
                                        if(is_numeric($vl) || is_double($vl) || is_float($vl) || is_int($vl)) {
                                            $option_val = $vl;
                                            $v1->sum += $vl;
                                        }
                                    }else if($v1->type == 6){
                                        $option_val = date('Y/m/d',strtotime($vl));

                                    }else{
                                        $option_val = $vl;
                                    }
                                }
                            }
                        }

                        $data[$v->id]=$option_val;
                    }

                    $v1->data  = $data;
                }
            }
        }
        else{
            foreach ($report->tabs as $key=>$value){
                $value->saved = true;
                $value->disable = true;
                $value->required = true;

                if($key == 0){
                    $value->selected = true;
                    $value->disable = false;
                }
                $value->sum_value = 0;
                $value->sum_amount = 0;
                foreach ($value->options as $k=>$v){
                    $v['value']= null ;
                    $v['amount']= null ;

                    $founded =  AdministrativeReportData::where(['option_id' => $v['id'],'organization_id'=>$user->organization_id])->first();

                    if(!is_null($founded)){
                        if($v['type'] == 2){
                            $v['value']= (int) $founded->list_id;
                        }else{
                            $v['value']= $founded->value;

                            if ($value->type == 3) {

                                if(!is_null($founded->amount)) {
                                    $v['amount']= $founded->amount;
                                }

                                if(is_numeric($v['value']) || is_double($v['value']) || is_float($v['value']) || is_int($v['value'])) {
                                    $value->sum_value += $v['value'];
                                }else{
                                    $v['value'] = 0;
                                }

                                if(is_numeric($v['amount']) || is_double($v['amount']) || is_float($v['amount']) || is_int($v['amount'])) {
                                    $value->sum_amount += $v['amount'];
                                }else{
                                    $v['amount'] = 0;
                                }

                            }
                        }

                    }else{
                        if ($value->type == 3) {
                            $v['amount'] = 0;
                            $v['value'] = 0;
                        }
                        $value->saved = false;
                    }

                }

                $value->saved = true;
                $value->disable = true;
                $value->required = true;

                if($value->saved){
                    $value->disable = false;
                }
            }
        }
        return response()->json($report);
    }

    // get orgData of logged user organization using report_id
    public function getTargetOrgData($id,Request $request) {

        $report = AdministrativeReport::with(['tabs','tabs.columns','tabs.columns.optionList','tabs.options','tabs.options.optionList'])->findOrFail($id);

        if($report->category == 2 || $report->category == '2') {
            foreach ($report->tabs as $key=>$value){
                $value->saved = true;
                $value->required = true;

                if($key == 0){
                    $value->selected = true;
                    $value->disable = false;
                }else{
                    $value->selected = false;
                    $value->disable = true;
                }

                foreach ($value->columns as $k1=>$v1) {
                    $data = [];
                    if($v1->type == 1 || $v1->type == 2){
                        $v1->sum = 0 ;
                    }else{
                        $v1->sum = '_' ;
                    }

                    foreach ($value->options as $k=>$v) {
                        if($v1->type == 1 || $v1->type == 2){
                            $option_val =0 ;
                        }else{
                            $option_val =null ;
                        }
                        $founded =  AdministrativeReportData::where(['column_id' => $v1->id,'option_id' => $v->id,
                            'organization_id'=>$request->organization_id])->first();

                        if(!is_null($founded)) {
                            if(!is_null($founded->value)) {
                                $vl = $founded->value ;
                                if($v1->type == 1 || $v1->type == 2){
                                    if(is_numeric($vl) || is_double($vl) || is_float($vl) || is_int($vl)) {
                                        $option_val = $vl;
                                        $v1->sum += $vl;
                                    }
                                }else if($v1->type == 6){
                                    $option_val = date('Y/m/d',strtotime($vl));

                                }else{
                                    $option_val = $vl;
                                }
                            }
                        }

                        $data[$v->id]=$option_val;
                    }

                    $v1->data  = $data;
                }
            }
        }else{
            foreach ($report->tabs as $key=>$value) {
                $value->sum_value = 0;
                $value->sum_amount = 0;
                foreach ($value->options as $k => $v) {
                    $v['value'] = null;
                    $v['amount'] = null;

                    $founded = AdministrativeReportData::where(['option_id' => $v['id'], 'organization_id' => $request->organization_id])->first();

                    if (!is_null($founded)) {
                        if ($v['type'] == 2) {
                            $list = AdministrativeReportTabOptionList::where(['id' => $founded->list_id])->first();
                            $v['value'] = $list->label;
                        } else {
                            $v['value'] = $founded->value;
                            if (!is_null($founded->amount)) {
                                $v['amount'] = $founded->amount;
                            }

                            if ($value->type == 3) {
                                $value->sum_value += $v['value'];
                                $value->sum_amount += $v['amount'];

                            }

                        }

                    }
                }
            }
        }

        return response()->json($report);
    }

    // update orgData of report  by organization id
    public function OrgData(Request $request)  {

        $this->authorize('orgUpdate', AdministrativeReport::class);

            $user = \Auth::user();

            $options = $request->options;
            $columns = $request->columns;
            $category = $request->category;
            $tab_type = $request->tab_type;
            $entries_data=[];
            $inset = [];

            $entries=[];
            $inset = [];
            $required = 0;

            if($category == 2 || $category == '2'){
                foreach($columns as $key => $value_) {
                    $value = (object) $value_ ;
                    $value_['required'] = false;
                    foreach($value->data as $k => $v) {
                        $inset[] = $k;
                        $column_list_id =null ;

                        if(is_null($v) || $v =='' || $v ==' '){
                            if ($value->priority == 1) {
                                $value_['required'] = true;
                                $required++;
                            }

                            if($value->type == 1 || $value->type == 2){
                                $option_val =0 ;
                            }else{
                                $option_val =null ;
                            }
                        }else{
                            if($value->type == 5 ) {
                                $column_list_id =$v ;
                                $option_val =null ;
                            } else if($value->type == 6 ){
                                $option_val = date('Y-m-d',strtotime($v)) ;
                            }else{
                                $option_val =$v ;
                            }
                        }

                        $temp = ['organization_id' => $user->organization_id, 'option_id'=>$k ,
                                 'column_id'=>$value->id, 'column_list_id' => $column_list_id , 'value' => $option_val ];
                        $entries_data[]=$temp;
                    }
                    $entries [$value->id] = $value_['required'];
                }

                if($required > 0){
                    return response()->json( ['status'=>'failed_valid' ,'error_type' =>'validate_inputs' ,'columns' => $entries]);
                }

            }
            else{
                foreach($options as $key => $value) {
                    $temp = (object) $value;
                    $temp->required = false;

                    if ($temp->priority == 1) {
                        if (!isset($temp->value)){
                            $temp->required = true;
                            $required++;
                        }else{
                            if (is_null($temp->value)){
                                $temp->required = true;
                                $required++;
                            }elseif ($temp->value =="" || $temp->value ==" "){
                                $temp->required = true;
                                $required++;
                            }
                        }
                    }


                    if($tab_type == 3){
                        if ($temp->priority == 1) {
                            if (!isset($temp->amount)){
                                $temp->required_amount = true;
                                $required++;
                            }else{
                                if (is_null($temp->amount)){
                                    $temp->required_amount = true;
                                    $required++;
                                }elseif ($temp->amount =="" || $temp->amount ==" "){
                                    $temp->required_amount = true;
                                    $required++;
                                }
                            }
                        }
                    }

                    $entries [] = $temp;
                }

                if($required > 0){
                    return response()->json( ['status'=>'failed_valid' ,'error_type' =>'validate_inputs' ,'options' => $entries]);
                }

                foreach ($options as $key=> $value) {
                    $temp = ['option_id' => $value['id'],'organization_id' => $user->organization_id, 'value'=>null,'amount'=>0];

                    if(isset($value['value'])){
                        if($value['type'] == 2){
                            $temp['list_id']=$value['value'];
                        }else{
                            $temp['list_id']=null;
                            $temp['value']=$value['value'];
                        }
                    }

                    if(isset($value['amount'])){
                        $temp['amount']=$value['amount'];
                    }

                    $entries_data [] = $temp;
                    $inset [] = $value['id'];

                }

            }

            \DB::beginTransaction();
            AdministrativeReportData::where(['organization_id'=>$user->organization_id])->whereIn('option_id',$inset)->delete();
            if(AdministrativeReportData::insert($entries_data)) {
                \DB::commit();
                //          \Log\Model\Log::saveNewLog('ADMINSTRATIVE_REPORT_DATA_UPDATED', trans('organization::application.He edited the properties of the report') . ' "' . $entry->title . '" ');
                return response()->json(['status' => 'success','msg'=>trans('organization::application.The row is updated')]);
            }
            DB::rollBack();
        return response()->json(['status' => 'failed','msg'=>trans('organization::application.There is no update')]);
    }

    // upload template word file of report
    public function templateUpload(Request $request) {
        $id = $request->input('report_id');
        $entry = AdministrativeReport::findOrFail($id);
//        $this->authorize('upload', $entry);

        $templateFile = $request->file('file');
        $templatePath = $templateFile->store('templates');
        if ($templatePath) {
            if (!$entry->template) {
                $entry->template = $templatePath;
                $entry->save();
            } else {
                if (\Storage::has($entry->template)) {
                    \Storage::delete($entry->template);
                }
                AdministrativeReport::where(['id'=> $id])->update(['template' => $templatePath]);
            }
            \Log\Model\Log::saveNewLog('REPORT_UPDATED',trans('organization::application.Uploaded a report data export template for') . ' "' . $entry->title . '" ');
            return response()->json($templatePath);
        }

        return false;
    }

    // export report data according logged user
    public function excel($id){

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit', '2048M');

        $ids = [$id];
        $reports = AdministrativeReport::with(['organization','tabs','tabs.columns','tabs.options'])->whereIn('id',$ids)->get();
         if(sizeof($reports) > 0) {

                $main = time();
                $name = 'storage/'.$main;
                $dirName = base_path($name);
                if (!is_dir($dirName)) {
                    @mkdir($dirName, 0777, true);
                }

                $storage_path = $main.'/';
                $tabs = [];
                $on_reports = [];
                foreach ($reports as $k1=>$report) {
                    $report_title = (is_null($report->title) || $report->title == ' ') ? null : $report->title;
                    if (!is_null($report_title)) {
                        $title_ = $report->title . '-' . $report->organization->name ;
                            if(sizeof($report->tabs) > 0 ){
                              if (!in_array($title_, $on_reports)) {
                               \Maatwebsite\Excel\Facades\Excel::create($title_, function ($excel) use ($report, $tabs) {
                                $excel->setTitle(trans('organization::application.report data'));
                                 $excel->setDescription(trans('organization::application.report data'));
                                foreach ($report->tabs as $ktab => $vtab) {
                                    $Org = OrgReports::getReportOrg($report->id,array());
                                    $tb_title = (is_null($vtab->title) || $vtab->title == ' ') ? null : $vtab->title;
                                    if (!is_null($tb_title)) {
                                        if (!in_array($tb_title, $tabs)) {
                                            $tab_name = trans('organization::application.tab_') . ' ( ' . ( $ktab+1 ). ' ) ';
                                            if ($report->category == 2 || $report->category == '2') {
                                                $excel->sheet($tab_name, function ($sheet) use ($report, $vtab,$Org,$tb_title) {
                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setAllBorders('solid');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setHeight(1, 40);
                                                    $sheet->getDefaultStyle()->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => ['name' => 'Simplified Arabic', 'size' => 11]
                                                    ]);
                                                    $sheet->setOrientation('landscape');
                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setStyle(['font' => ['name' => 'Simplified Arabic', 'size' => 11, 'bold' => true]]);
                                                    $sheet->setWidth(['A' => 5, 'B' => 20]);
                                                    $range = "A1:B1";
                                                    $sheet->getStyle($range)->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => ['name' => 'Simplified Arabic', 'size' => 11, 'bold' => true]
                                                    ]);

                                                    $Index = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                                        'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
                                                        'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ',
                                                        'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ',
                                                        'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ',
                                                        'EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EU', 'EV', 'EW', 'EX', 'EY', 'EZ',
                                                        'FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG', 'FH', 'FI', 'FJ', 'FK', 'FL', 'FM', 'FN', 'FO', 'FP', 'FQ', 'FR', 'FS', 'FT', 'FU', 'FV', 'FW', 'FX', 'FY', 'FZ',
                                                        'GA', 'GB', 'GC', 'GD', 'GE', 'GF', 'GG', 'GH', 'GI', 'GJ', 'GK', 'GL', 'GM', 'GN', 'GO', 'GP', 'GQ', 'GR', 'GS', 'GT', 'GU', 'GV', 'GW', 'GX', 'GY', 'GZ',
                                                        'HA', 'HB', 'HC', 'HD', 'HE', 'HF', 'HG', 'HH', 'HI', 'HJ', 'HK', 'HL', 'HM', 'HN', 'HO', 'HP', 'HQ', 'HR', 'HS', 'HT', 'HU', 'HV', 'HW', 'HX', 'HY', 'HZ',
                                                        'IA', 'IB', 'IC', 'ID', 'IE', 'IF', 'IG', 'IH', 'II', 'IJ', 'IK', 'IL', 'IM', 'IN', 'IO', 'IP', 'IQ', 'IR', 'IS', 'IT', 'IU', 'IV', 'IW', 'IX', 'IY', 'IZ',
                                                        'JA', 'JB', 'JC', 'JD', 'JE', 'JF', 'JG', 'JH', 'JI', 'JJ', 'JK', 'JL', 'JM', 'JN', 'JO', 'JP', 'JQ', 'JR', 'JS', 'JT', 'JU', 'JV', 'JW', 'JX', 'JY', 'JZ',
                                                        'KA', 'KB', 'KC', 'KD', 'KE', 'KF', 'KG', 'KH', 'KI', 'KJ', 'KK', 'KL', 'KM', 'KN', 'KO', 'KP', 'KQ', 'KR', 'KS', 'KT', 'KU', 'KV', 'KW', 'KX', 'KY', 'KZ',
                                                        'LA', 'LB', 'LC', 'LD', 'LE', 'LF', 'LG', 'LH', 'LI', 'LJ', 'LK', 'LL', 'LM', 'LN', 'LO', 'LP', 'LQ', 'LR', 'LS', 'LT', 'LU', 'LV', 'LW', 'LX', 'LY', 'LZ',
                                                        'MA', 'MB', 'MC', 'MD', 'ME', 'MF', 'MG', 'MH', 'MI', 'MJ', 'MK', 'ML', 'MM', 'MN', 'MO', 'MP', 'MQ', 'MR', 'MS', 'MT', 'MU', 'MV', 'MW', 'MX', 'MY', 'MZ',
                                                        'NA', 'NB', 'NC', 'ND', 'NE', 'NF', 'NG', 'NH', 'NI', 'NJ', 'NK', 'NL', 'NM', 'NN', 'NO', 'NP', 'NQ', 'NR', 'NS', 'NT', 'NU', 'NV', 'NW', 'NX', 'NY', 'NZ',
                                                        'OA', 'OB', 'OC', 'OD', 'OE', 'OF', 'OG', 'OH', 'OI', 'OJ', 'OK', 'OL', 'OM', 'ON', 'OO', 'OP', 'OQ', 'OR', 'OS', 'OT', 'OU', 'OV', 'OW', 'OX', 'OY', 'OZ'
                                                    ];

                                                    if (sizeof($vtab->options) > 0 && sizeof($vtab->columns) > 0) {
                                                        $sheet->setCellValue('A1', $tb_title);
                                                        $sheet->getRowDimension(1)->setRowHeight(-1);
                                                        $sheet->getRowDimension(2)->setRowHeight(-1);
                                                        $step_ = (sizeof($Org) * sizeof($vtab->columns));
                                                        $sheet->mergeCells('A1:'.$Index[$step_].'1');
                                                        $sheet->cells('A1:'.$Index[$step_].'1', function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('left');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });
                                                        $sheet->setCellValue('A2', trans('organization::application.#'));
                                                        $sheet->setCellValue('B2', trans('organization::application.for'));
                                                        $sheet->mergeCells('A2:A3');
                                                        $sheet->mergeCells('B2:B3');
                                                        $sheet->getRowDimension(1)->setRowHeight(-1);
                                                        $sheet->getRowDimension(2)->setRowHeight(-1);
                                                        $sheet->cells('A1:B2', function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('center');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });

                                                        $opInc = 4;
                                                        foreach ($vtab->options as $kop => $vop) {
                                                            $sheet->setCellValue('A' . $opInc, $kop + 1);
                                                            $sheet->setCellValue('B' . $opInc, (is_null($vop->label) || $vop->label == ' ') ? ' ' : $vop->label);

                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $sheet->getStyle('A' . $opInc)->getAlignment()->setWrapText(true);
                                                            $sheet->getStyle('B' . $opInc)->getAlignment()->setWrapText(true);

                                                            $sheet->cells('A' . $opInc . ':' . 'B' . $opInc, function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $opInc++;
                                                        }

                                                        $offSet = 2;
                                                        $step = sizeof($vtab->columns);
                                                        foreach ($Org as $k => $v) {
                                                            $sheet->setCellValue($Index[$offSet] . '2', (is_null($v->organization_name) || $v->organization_name == ' ') ? ' ' : $v->organization_name);
                                                            $sheet->mergeCells($Index[$offSet] . '2:' . $Index[$offSet + $step - 1] . '2');
                                                            $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $i = $offSet;
                                                            foreach ($vtab->columns as $kcol => $vcol) {
                                                                $sheet->setCellValue($Index[$i] . '3', (is_null($vcol->label) || $vcol->label == ' ') ? ' ' : $vcol->label);
                                                                $sheet->setWidth([$Index[$i] => 15]);
                                                                $sheet->getStyle($Index[$i] . '3')->getAlignment()->setWrapText(true);
                                                                $sheet->getStyle($Index[$i])->getAlignment()->setWrapText(true);
                                                                $i++;
                                                            }

                                                            $sheet->getStyle($Index[$offSet] . '2')->getAlignment()->setWrapText(true);
                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $sheet->cells($Index[$offSet] . '2:' . $Index[$i] . '2', function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $sheet->cells($Index[$offSet] . '3:' . $Index[$i] . '3', function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $offSet += $step;

                                                        }

                                                        $opInc = 4;
                                                        foreach ($vtab->options as $kop => $vop) {
                                                            $sheet->setCellValue('A' . $opInc, $kop + 1);
                                                            $sheet->setCellValue('B' . $opInc, (is_null($vop->label) || $vop->label == ' ') ? ' ' : $vop->label);

                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $sheet->getStyle('A' . $opInc)->getAlignment()->setWrapText(true);
                                                            $sheet->getStyle('B' . $opInc)->getAlignment()->setWrapText(true);

                                                            $sheet->cells('A' . $opInc . ':' . 'B' . $opInc, function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $offSet = 2;
                                                            foreach ($Org as $k => $v) {
                                                                foreach ($vtab->columns as $kcol => $vcol) {

                                                                    $orgData = AdministrativeReportData::OrgOptionData($vop->id, $v->id, $vcol->id);
                                                                    if(!($vcol->type == 1 || $vcol->type == 2)){
                                                                        if(is_null($orgData['value'])){
                                                                            $sheet->setCellValue($Index[$offSet] . $opInc,'_');
                                                                        }else{
                                                                            $sheet->setCellValue($Index[$offSet] . $opInc,$orgData['value']);
                                                                        }
                                                                        $sheet->setWidth([$Index[$offSet] => 35]);
                                                                    }else{
                                                                        if(is_null($orgData['value']) || $orgData['value'] == '' || $orgData['value'] == ' '){
                                                                            if($vcol->type == 1 || $vcol->type == 2 ){
                                                                                $sheet->setCellValue($Index[$offSet] . $opInc,0);
                                                                            }
                                                                        }else{
                                                                            $sheet->setCellValue($Index[$offSet] . $opInc,$orgData['value']);
                                                                        }
                                                                        $sheet->setWidth([$Index[$offSet] => 13]);
                                                                    }

                                                                    $sheet->setWidth([$Index[$offSet] => 16]);
                                                                    $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc . ':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });
                                                                    $sheet->getStyle($Index[$offSet] . $opInc)->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });
                                                                    $offSet += 1;
                                                                }
                                                            }
                                                            $opInc++;
                                                        }
                                                        $sheet->setCellValue('A' . $opInc, trans('organization::application.total'));
                                                        $sheet->mergeCells("A".$opInc .":B" . $opInc);
                                                        $sheet->getRowDimension(1)->setRowHeight(-1);
                                                        $sheet->getRowDimension(2)->setRowHeight(-1);
                                                        $sheet->cells("A".$opInc .":B" . $opInc, function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('center');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });

                                                        $offSet = 2;
                                                        foreach ($Org as $k => $v) {
                                                            foreach ($vtab->columns as $kcol => $vcol) {
                                                                if($vcol->type == 1 || $vcol->type == 2 ){
                                                                    $sum = AdministrativeReportData::OrgOptionSum($v->id, $vcol->id);
                                                                    $sheet->setCellValue($Index[$offSet] . $opInc,$sum);
                                                                    $sheet->setWidth([$Index[$offSet] => 13]);
                                                                }else{
                                                                    $sheet->setCellValue($Index[$offSet] . $opInc,'_');
                                                                    $sheet->setWidth([$Index[$offSet] => 35]);
                                                                }

                                                                $sheet->setWidth([$Index[$offSet] => 16]);
                                                                $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . $opInc . ':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });
                                                                $sheet->getStyle($Index[$offSet] . $opInc)->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . $opInc, function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });
                                                                $offSet += 1;
                                                            }
                                                        }

                                                    }
                                                });
                                            }
                                            else {
                                                $excel->sheet($tab_name, function ($sheet) use ($report, $vtab,$Org,$tb_title) {
                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setAllBorders('solid');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setHeight(1, 40);
                                                    $sheet->getDefaultStyle()->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => ['name' => 'Simplified Arabic', 'size' => 11]
                                                    ]);
                                                    $sheet->setOrientation('landscape');
                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setWidth(['A' => 5, 'B' => 20]);
                                                    $range = "A1:B1";
                                                    $sheet->getStyle($range)->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => ['name' => 'Simplified Arabic', 'size' => 11, 'bold' => true]
                                                    ]);

                                                    $Index = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                                        'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
                                                        'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ',
                                                        'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ',
                                                        'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ',
                                                        'EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EU', 'EV', 'EW', 'EX', 'EY', 'EZ',
                                                        'FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG', 'FH', 'FI', 'FJ', 'FK', 'FL', 'FM', 'FN', 'FO', 'FP', 'FQ', 'FR', 'FS', 'FT', 'FU', 'FV', 'FW', 'FX', 'FY', 'FZ',
                                                        'GA', 'GB', 'GC', 'GD', 'GE', 'GF', 'GG', 'GH', 'GI', 'GJ', 'GK', 'GL', 'GM', 'GN', 'GO', 'GP', 'GQ', 'GR', 'GS', 'GT', 'GU', 'GV', 'GW', 'GX', 'GY', 'GZ',
                                                        'HA', 'HB', 'HC', 'HD', 'HE', 'HF', 'HG', 'HH', 'HI', 'HJ', 'HK', 'HL', 'HM', 'HN', 'HO', 'HP', 'HQ', 'HR', 'HS', 'HT', 'HU', 'HV', 'HW', 'HX', 'HY', 'HZ',
                                                        'IA', 'IB', 'IC', 'ID', 'IE', 'IF', 'IG', 'IH', 'II', 'IJ', 'IK', 'IL', 'IM', 'IN', 'IO', 'IP', 'IQ', 'IR', 'IS', 'IT', 'IU', 'IV', 'IW', 'IX', 'IY', 'IZ',
                                                        'JA', 'JB', 'JC', 'JD', 'JE', 'JF', 'JG', 'JH', 'JI', 'JJ', 'JK', 'JL', 'JM', 'JN', 'JO', 'JP', 'JQ', 'JR', 'JS', 'JT', 'JU', 'JV', 'JW', 'JX', 'JY', 'JZ',
                                                        'KA', 'KB', 'KC', 'KD', 'KE', 'KF', 'KG', 'KH', 'KI', 'KJ', 'KK', 'KL', 'KM', 'KN', 'KO', 'KP', 'KQ', 'KR', 'KS', 'KT', 'KU', 'KV', 'KW', 'KX', 'KY', 'KZ',
                                                        'LA', 'LB', 'LC', 'LD', 'LE', 'LF', 'LG', 'LH', 'LI', 'LJ', 'LK', 'LL', 'LM', 'LN', 'LO', 'LP', 'LQ', 'LR', 'LS', 'LT', 'LU', 'LV', 'LW', 'LX', 'LY', 'LZ',
                                                        'MA', 'MB', 'MC', 'MD', 'ME', 'MF', 'MG', 'MH', 'MI', 'MJ', 'MK', 'ML', 'MM', 'MN', 'MO', 'MP', 'MQ', 'MR', 'MS', 'MT', 'MU', 'MV', 'MW', 'MX', 'MY', 'MZ',
                                                        'NA', 'NB', 'NC', 'ND', 'NE', 'NF', 'NG', 'NH', 'NI', 'NJ', 'NK', 'NL', 'NM', 'NN', 'NO', 'NP', 'NQ', 'NR', 'NS', 'NT', 'NU', 'NV', 'NW', 'NX', 'NY', 'NZ',
                                                        'OA', 'OB', 'OC', 'OD', 'OE', 'OF', 'OG', 'OH', 'OI', 'OJ', 'OK', 'OL', 'OM', 'ON', 'OO', 'OP', 'OQ', 'OR', 'OS', 'OT', 'OU', 'OV', 'OW', 'OX', 'OY', 'OZ'
                                                    ];

                                                    if (sizeof($vtab->options) > 0) {
                                                        $sheet->setCellValue('A2', trans('organization::application.#'));
                                                        $sheet->setCellValue('B2', trans('organization::application.for'));
                                                        $sheet->mergeCells('A2:A3');
                                                        $sheet->mergeCells('B2:B3');
                                                        $sheet->getRowDimension(1)->setRowHeight(-1);
                                                        $sheet->getRowDimension(2)->setRowHeight(-1);
                                                        $sheet->cells('A1:B2', function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('center');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });

                                                        if ($vtab->type == 3) {
                                                            $step_ = (sizeof($Org) * 2) + 1 ;
                                                        }else{
                                                            $step_ = sizeof($Org)  + 1 ;
                                                        }

                                                        $sheet->setCellValue('A1', $tb_title);
                                                        $sheet->mergeCells('A1:'.$Index[$step_].'1');
                                                        $sheet->cells('A1:'.$Index[$step_].'1', function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('right');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });
                                                        $offSet = 2;
                                                        foreach ($Org as $k => $v) {
                                                            $sheet->setCellValue($Index[$offSet] . '2', (is_null($v->organization_name) || $v->organization_name == ' ') ? ' ' : $v->organization_name);
                                                            if ($vtab->type == 3) {
                                                                $sheet->setCellValue($Index[$offSet] . '3', trans('organization::application.count'));
                                                                $sheet->setCellValue($Index[$offSet + 1] . '3', trans('organization::application.amount'));
                                                                $sheet->mergeCells($Index[$offSet] . '2:' . $Index[$offSet + 1] . '2');
                                                                $sheet->setWidth([$Index[$offSet] => 10, $Index[$offSet + 1] => 10]);
                                                                $sheet->getStyle($Index[$offSet + 1])->getAlignment()->setWrapText(true);
                                                                $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . '3:' . $Index[$offSet + 1] . '3', function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });

                                                                $sheet->getStyle($Index[$offSet] . '2')->getAlignment()->setWrapText(true);
                                                                $sheet->getStyle($Index[$offSet + 1] . '2')->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . '2:' . $Index[$offSet + 1] . '2', function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });
                                                                $offSet += 2;
                                                            }
                                                            else {
                                                                $sheet->setCellValue($Index[$offSet] . '3', trans('organization::application.value'));
                                                                $sheet->setWidth([$Index[$offSet] => 16]);
                                                                $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . '3:' . $Index[$offSet + 1] . '3', function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });

                                                                $sheet->getStyle($Index[$offSet] . '2')->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . '2', function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });
                                                                $offSet += 1;
                                                            }
                                                        }

                                                        $opInc = 4;
                                                        foreach ($vtab->options as $kop => $vop) {
                                                            $sheet->setCellValue('A' . $opInc, $kop + 1);
                                                            $sheet->setCellValue('B' . $opInc, (is_null($vop->label) || $vop->label == ' ') ? ' ' : $vop->label);

                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $sheet->getStyle('A' . $opInc)->getAlignment()->setWrapText(true);
                                                            $sheet->getStyle('B' . $opInc)->getAlignment()->setWrapText(true);

                                                            $sheet->cells('A' . $opInc . ':' . 'B' . $opInc, function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $offSet = 2;
                                                            foreach ($Org as $k => $v) {
                                                                $orgData = AdministrativeReportData::OrgOptionData($vop->id ,$v->id , null);

                                                                if ($vtab->type == 3) {
                                                                    if(is_numeric($orgData['value']) || is_double($orgData['value']) || is_float($orgData['value']) || is_int($orgData['value'])) {
                                                                        $sheet->setCellValue($Index[$offSet] . $opInc, $orgData['value']);
                                                                    }else{
                                                                        $sheet->setCellValue($Index[$offSet] . $opInc, 0);
                                                                    }

                                                                    if(is_numeric($orgData['amount']) || is_double($orgData['amount']) || is_float($orgData['amount']) || is_int($orgData['amount'])) {
                                                                        $sheet->setCellValue($Index[$offSet + 1] . $opInc, $orgData['amount']);
                                                                    }else{
                                                                        $sheet->setCellValue($Index[$offSet + 1] . $opInc, 0);
                                                                    }

                                                                    $sheet->setWidth([$Index[$offSet] => 10, $Index[$offSet + 1] => 10]);
                                                                    $sheet->getStyle($Index[$offSet + 1])->getAlignment()->setWrapText(true);
                                                                    $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc.':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });

                                                                    $sheet->getStyle($Index[$offSet] .$opInc)->getAlignment()->setWrapText(true);
                                                                    $sheet->getStyle($Index[$offSet + 1] . $opInc)->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc .':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });
                                                                    $offSet += 2;
                                                                }
                                                                else {
                                                                    if(is_null($orgData['value'])){
                                                                        $sheet->setCellValue($Index[$offSet] . $opInc,'_');
                                                                    }else{
                                                                        $sheet->setCellValue($Index[$offSet] . $opInc,$orgData['value']);
                                                                    }
                                                                    $sheet->setWidth([$Index[$offSet] => 16]);
                                                                    $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc .':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });

                                                                    $sheet->getStyle($Index[$offSet] . $opInc)->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });
                                                                    $offSet += 1;
                                                                }
                                                            }
                                                            $opInc++;
                                                        }
                                                    }
                                                });
                                            }
                                            $tabs[] = $tab_name;
                                        }
                                    }
                                }
                                $excel->setActiveSheetIndex(0);
                                })
                                ->store('xlsx', storage_path($storage_path));
                                 $on_reports[]=$title_;
                            }
                    }
                    }
                }

                $token = md5(uniqid());
                $zipFileName = storage_path('tmp/export_' . $token . '.zip');
                $zip = new \ZipArchive;

                if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true ) {
                    foreach ( glob( $dirName . '/*' ) as $fileName ) {
                        $file = basename( $fileName );
                        $zip->addFile( $fileName, $file );
                    }
                    $zip->close();
                    array_map('unlink', glob("$dirName/*.*"));
                    rmdir($dirName);
                    return response()->json(['status' => 'success' , 'download_token' => $token]);
                }

            }

        return response()->json(['status' => 'failed','msg'=>trans('organization::application.no report data')]);
    }
    public function excelRelated($id){

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit', '2048M');

        $ids = [$id];
        $reports = AdministrativeReport::with(['organization','tabs','tabs.columns','tabs.options'])->whereIn('id',$ids)->get();
         if(sizeof($reports) > 0) {

             $user = \Auth::user();
             $organization=$user->organization;
             $main = time();
                $name = 'storage/'.$main;
                $dirName = base_path($name);
                if (!is_dir($dirName)) {
                    @mkdir($dirName, 0777, true);
                }

                $storage_path = $main.'/';
                $tabs = [];
                $on_reports = [];
                foreach ($reports as $k1=>$report) {
                    $report_title = (is_null($report->title) || $report->title == ' ') ? null : $report->title;
                    if (!is_null($report_title)) {
                        $title_ = $report->title . '-' . $organization->name ;
                            if(sizeof($report->tabs) > 0 ){
                              if (!in_array($title_, $on_reports)) {
                               \Maatwebsite\Excel\Facades\Excel::create($title_, function ($excel) use ($report, $tabs) {
                                $excel->setTitle(trans('organization::application.report data'));
                                 $excel->setDescription(trans('organization::application.report data'));
                                foreach ($report->tabs as $ktab => $vtab) {
                                    $Org = OrgReports::getReportOrgRelated($report->id,array());
                                    $tb_title = (is_null($vtab->title) || $vtab->title == ' ') ? null : $vtab->title;
                                    if (!is_null($tb_title)) {
                                        if (!in_array($tb_title, $tabs)) {
                                            $tab_name = trans('organization::application.tab_') . ' ( ' . ( $ktab+1 ). ' ) ';
                                            if ($report->category == 2 || $report->category == '2') {
                                                $excel->sheet($tab_name, function ($sheet) use ($report, $vtab,$Org,$tb_title) {
                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setAllBorders('solid');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setHeight(1, 40);
                                                    $sheet->getDefaultStyle()->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => ['name' => 'Simplified Arabic', 'size' => 11]
                                                    ]);
                                                    $sheet->setOrientation('landscape');
                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setStyle(['font' => ['name' => 'Simplified Arabic', 'size' => 11, 'bold' => true]]);
                                                    $sheet->setWidth(['A' => 5, 'B' => 20]);
                                                    $range = "A1:B1";
                                                    $sheet->getStyle($range)->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => ['name' => 'Simplified Arabic', 'size' => 11, 'bold' => true]
                                                    ]);

                                                    $Index = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                                        'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
                                                        'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ',
                                                        'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ',
                                                        'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ',
                                                        'EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EU', 'EV', 'EW', 'EX', 'EY', 'EZ',
                                                        'FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG', 'FH', 'FI', 'FJ', 'FK', 'FL', 'FM', 'FN', 'FO', 'FP', 'FQ', 'FR', 'FS', 'FT', 'FU', 'FV', 'FW', 'FX', 'FY', 'FZ',
                                                        'GA', 'GB', 'GC', 'GD', 'GE', 'GF', 'GG', 'GH', 'GI', 'GJ', 'GK', 'GL', 'GM', 'GN', 'GO', 'GP', 'GQ', 'GR', 'GS', 'GT', 'GU', 'GV', 'GW', 'GX', 'GY', 'GZ',
                                                        'HA', 'HB', 'HC', 'HD', 'HE', 'HF', 'HG', 'HH', 'HI', 'HJ', 'HK', 'HL', 'HM', 'HN', 'HO', 'HP', 'HQ', 'HR', 'HS', 'HT', 'HU', 'HV', 'HW', 'HX', 'HY', 'HZ',
                                                        'IA', 'IB', 'IC', 'ID', 'IE', 'IF', 'IG', 'IH', 'II', 'IJ', 'IK', 'IL', 'IM', 'IN', 'IO', 'IP', 'IQ', 'IR', 'IS', 'IT', 'IU', 'IV', 'IW', 'IX', 'IY', 'IZ',
                                                        'JA', 'JB', 'JC', 'JD', 'JE', 'JF', 'JG', 'JH', 'JI', 'JJ', 'JK', 'JL', 'JM', 'JN', 'JO', 'JP', 'JQ', 'JR', 'JS', 'JT', 'JU', 'JV', 'JW', 'JX', 'JY', 'JZ',
                                                        'KA', 'KB', 'KC', 'KD', 'KE', 'KF', 'KG', 'KH', 'KI', 'KJ', 'KK', 'KL', 'KM', 'KN', 'KO', 'KP', 'KQ', 'KR', 'KS', 'KT', 'KU', 'KV', 'KW', 'KX', 'KY', 'KZ',
                                                        'LA', 'LB', 'LC', 'LD', 'LE', 'LF', 'LG', 'LH', 'LI', 'LJ', 'LK', 'LL', 'LM', 'LN', 'LO', 'LP', 'LQ', 'LR', 'LS', 'LT', 'LU', 'LV', 'LW', 'LX', 'LY', 'LZ',
                                                        'MA', 'MB', 'MC', 'MD', 'ME', 'MF', 'MG', 'MH', 'MI', 'MJ', 'MK', 'ML', 'MM', 'MN', 'MO', 'MP', 'MQ', 'MR', 'MS', 'MT', 'MU', 'MV', 'MW', 'MX', 'MY', 'MZ',
                                                        'NA', 'NB', 'NC', 'ND', 'NE', 'NF', 'NG', 'NH', 'NI', 'NJ', 'NK', 'NL', 'NM', 'NN', 'NO', 'NP', 'NQ', 'NR', 'NS', 'NT', 'NU', 'NV', 'NW', 'NX', 'NY', 'NZ',
                                                        'OA', 'OB', 'OC', 'OD', 'OE', 'OF', 'OG', 'OH', 'OI', 'OJ', 'OK', 'OL', 'OM', 'ON', 'OO', 'OP', 'OQ', 'OR', 'OS', 'OT', 'OU', 'OV', 'OW', 'OX', 'OY', 'OZ'
                                                    ];

                                                    if (sizeof($vtab->options) > 0 && sizeof($vtab->columns) > 0) {
                                                        $sheet->setCellValue('A1', $tb_title);
                                                        $sheet->getRowDimension(1)->setRowHeight(-1);
                                                        $sheet->getRowDimension(2)->setRowHeight(-1);
                                                        $step_ = (sizeof($Org) * sizeof($vtab->columns));
                                                        $sheet->mergeCells('A1:'.$Index[$step_].'1');
                                                        $sheet->cells('A1:'.$Index[$step_].'1', function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('left');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });
                                                        $sheet->setCellValue('A2', trans('organization::application.#'));
                                                        $sheet->setCellValue('B2', trans('organization::application.for'));
                                                        $sheet->mergeCells('A2:A3');
                                                        $sheet->mergeCells('B2:B3');
                                                        $sheet->getRowDimension(1)->setRowHeight(-1);
                                                        $sheet->getRowDimension(2)->setRowHeight(-1);
                                                        $sheet->cells('A1:B2', function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('center');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });

                                                        $opInc = 4;
                                                        foreach ($vtab->options as $kop => $vop) {
                                                            $sheet->setCellValue('A' . $opInc, $kop + 1);
                                                            $sheet->setCellValue('B' . $opInc, (is_null($vop->label) || $vop->label == ' ') ? ' ' : $vop->label);

                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $sheet->getStyle('A' . $opInc)->getAlignment()->setWrapText(true);
                                                            $sheet->getStyle('B' . $opInc)->getAlignment()->setWrapText(true);

                                                            $sheet->cells('A' . $opInc . ':' . 'B' . $opInc, function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $opInc++;
                                                        }

                                                        $offSet = 2;
                                                        $step = sizeof($vtab->columns);
                                                        foreach ($Org as $k => $v) {
                                                            $sheet->setCellValue($Index[$offSet] . '2', (is_null($v->organization_name) || $v->organization_name == ' ') ? ' ' : $v->organization_name);
                                                            $sheet->mergeCells($Index[$offSet] . '2:' . $Index[$offSet + $step - 1] . '2');
                                                            $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $i = $offSet;
                                                            foreach ($vtab->columns as $kcol => $vcol) {
                                                                $sheet->setCellValue($Index[$i] . '3', (is_null($vcol->label) || $vcol->label == ' ') ? ' ' : $vcol->label);
                                                                $sheet->setWidth([$Index[$i] => 15]);
                                                                $sheet->getStyle($Index[$i] . '3')->getAlignment()->setWrapText(true);
                                                                $sheet->getStyle($Index[$i])->getAlignment()->setWrapText(true);
                                                                $i++;
                                                            }

                                                            $sheet->getStyle($Index[$offSet] . '2')->getAlignment()->setWrapText(true);
                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $sheet->cells($Index[$offSet] . '2:' . $Index[$i] . '2', function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $sheet->cells($Index[$offSet] . '3:' . $Index[$i] . '3', function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $offSet += $step;

                                                        }

                                                        $opInc = 4;
                                                        foreach ($vtab->options as $kop => $vop) {
                                                            $sheet->setCellValue('A' . $opInc, $kop + 1);
                                                            $sheet->setCellValue('B' . $opInc, (is_null($vop->label) || $vop->label == ' ') ? ' ' : $vop->label);

                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $sheet->getStyle('A' . $opInc)->getAlignment()->setWrapText(true);
                                                            $sheet->getStyle('B' . $opInc)->getAlignment()->setWrapText(true);

                                                            $sheet->cells('A' . $opInc . ':' . 'B' . $opInc, function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $offSet = 2;
                                                            foreach ($Org as $k => $v) {
                                                                foreach ($vtab->columns as $kcol => $vcol) {

                                                                    $orgData = AdministrativeReportData::OrgOptionData($vop->id, $v->id, $vcol->id);
                                                                    if(!($vcol->type == 1 || $vcol->type == 2)){
                                                                        if(is_null($orgData['value'])){
                                                                            $sheet->setCellValue($Index[$offSet] . $opInc,'_');
                                                                        }else{
                                                                            $sheet->setCellValue($Index[$offSet] . $opInc,$orgData['value']);
                                                                        }
                                                                        $sheet->setWidth([$Index[$offSet] => 35]);
                                                                    }else{
                                                                        if(is_null($orgData['value']) || $orgData['value'] == '' || $orgData['value'] == ' '){
                                                                            if($vcol->type == 1 || $vcol->type == 2 ){
                                                                                $sheet->setCellValue($Index[$offSet] . $opInc,0);
                                                                            }
                                                                        }else{
                                                                            $sheet->setCellValue($Index[$offSet] . $opInc,$orgData['value']);
                                                                        }
                                                                        $sheet->setWidth([$Index[$offSet] => 13]);
                                                                    }

                                                                    $sheet->setWidth([$Index[$offSet] => 16]);
                                                                    $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc . ':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });
                                                                    $sheet->getStyle($Index[$offSet] . $opInc)->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });
                                                                    $offSet += 1;
                                                                }
                                                            }
                                                            $opInc++;
                                                        }
                                                        $sheet->setCellValue('A' . $opInc, trans('organization::application.total'));
                                                        $sheet->mergeCells("A".$opInc .":B" . $opInc);
                                                        $sheet->getRowDimension(1)->setRowHeight(-1);
                                                        $sheet->getRowDimension(2)->setRowHeight(-1);
                                                        $sheet->cells("A".$opInc .":B" . $opInc, function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('center');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });

                                                        $offSet = 2;
                                                        foreach ($Org as $k => $v) {
                                                            foreach ($vtab->columns as $kcol => $vcol) {
                                                                if($vcol->type == 1 || $vcol->type == 2 ){
                                                                    $sum = AdministrativeReportData::OrgOptionSum($v->id, $vcol->id);
                                                                    $sheet->setCellValue($Index[$offSet] . $opInc,$sum);
                                                                    $sheet->setWidth([$Index[$offSet] => 13]);
                                                                }else{
                                                                    $sheet->setCellValue($Index[$offSet] . $opInc,'_');
                                                                    $sheet->setWidth([$Index[$offSet] => 35]);
                                                                }

                                                                $sheet->setWidth([$Index[$offSet] => 16]);
                                                                $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . $opInc . ':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });
                                                                $sheet->getStyle($Index[$offSet] . $opInc)->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . $opInc, function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });
                                                                $offSet += 1;
                                                            }
                                                        }

                                                    }
                                                });
                                            }
                                            else {
                                                $excel->sheet($tab_name, function ($sheet) use ($report, $vtab,$Org,$tb_title) {
                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setAllBorders('solid');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setHeight(1, 40);
                                                    $sheet->getDefaultStyle()->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => ['name' => 'Simplified Arabic', 'size' => 11]
                                                    ]);
                                                    $sheet->setOrientation('landscape');
                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setWidth(['A' => 5, 'B' => 20]);
                                                    $range = "A1:B1";
                                                    $sheet->getStyle($range)->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => ['name' => 'Simplified Arabic', 'size' => 11, 'bold' => true]
                                                    ]);

                                                    $Index = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                                        'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
                                                        'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ',
                                                        'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ',
                                                        'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ',
                                                        'EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EU', 'EV', 'EW', 'EX', 'EY', 'EZ',
                                                        'FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG', 'FH', 'FI', 'FJ', 'FK', 'FL', 'FM', 'FN', 'FO', 'FP', 'FQ', 'FR', 'FS', 'FT', 'FU', 'FV', 'FW', 'FX', 'FY', 'FZ',
                                                        'GA', 'GB', 'GC', 'GD', 'GE', 'GF', 'GG', 'GH', 'GI', 'GJ', 'GK', 'GL', 'GM', 'GN', 'GO', 'GP', 'GQ', 'GR', 'GS', 'GT', 'GU', 'GV', 'GW', 'GX', 'GY', 'GZ',
                                                        'HA', 'HB', 'HC', 'HD', 'HE', 'HF', 'HG', 'HH', 'HI', 'HJ', 'HK', 'HL', 'HM', 'HN', 'HO', 'HP', 'HQ', 'HR', 'HS', 'HT', 'HU', 'HV', 'HW', 'HX', 'HY', 'HZ',
                                                        'IA', 'IB', 'IC', 'ID', 'IE', 'IF', 'IG', 'IH', 'II', 'IJ', 'IK', 'IL', 'IM', 'IN', 'IO', 'IP', 'IQ', 'IR', 'IS', 'IT', 'IU', 'IV', 'IW', 'IX', 'IY', 'IZ',
                                                        'JA', 'JB', 'JC', 'JD', 'JE', 'JF', 'JG', 'JH', 'JI', 'JJ', 'JK', 'JL', 'JM', 'JN', 'JO', 'JP', 'JQ', 'JR', 'JS', 'JT', 'JU', 'JV', 'JW', 'JX', 'JY', 'JZ',
                                                        'KA', 'KB', 'KC', 'KD', 'KE', 'KF', 'KG', 'KH', 'KI', 'KJ', 'KK', 'KL', 'KM', 'KN', 'KO', 'KP', 'KQ', 'KR', 'KS', 'KT', 'KU', 'KV', 'KW', 'KX', 'KY', 'KZ',
                                                        'LA', 'LB', 'LC', 'LD', 'LE', 'LF', 'LG', 'LH', 'LI', 'LJ', 'LK', 'LL', 'LM', 'LN', 'LO', 'LP', 'LQ', 'LR', 'LS', 'LT', 'LU', 'LV', 'LW', 'LX', 'LY', 'LZ',
                                                        'MA', 'MB', 'MC', 'MD', 'ME', 'MF', 'MG', 'MH', 'MI', 'MJ', 'MK', 'ML', 'MM', 'MN', 'MO', 'MP', 'MQ', 'MR', 'MS', 'MT', 'MU', 'MV', 'MW', 'MX', 'MY', 'MZ',
                                                        'NA', 'NB', 'NC', 'ND', 'NE', 'NF', 'NG', 'NH', 'NI', 'NJ', 'NK', 'NL', 'NM', 'NN', 'NO', 'NP', 'NQ', 'NR', 'NS', 'NT', 'NU', 'NV', 'NW', 'NX', 'NY', 'NZ',
                                                        'OA', 'OB', 'OC', 'OD', 'OE', 'OF', 'OG', 'OH', 'OI', 'OJ', 'OK', 'OL', 'OM', 'ON', 'OO', 'OP', 'OQ', 'OR', 'OS', 'OT', 'OU', 'OV', 'OW', 'OX', 'OY', 'OZ'
                                                    ];

                                                    if (sizeof($vtab->options) > 0) {
                                                        $sheet->setCellValue('A2', trans('organization::application.#'));
                                                        $sheet->setCellValue('B2', trans('organization::application.for'));
                                                        $sheet->mergeCells('A2:A3');
                                                        $sheet->mergeCells('B2:B3');
                                                        $sheet->getRowDimension(1)->setRowHeight(-1);
                                                        $sheet->getRowDimension(2)->setRowHeight(-1);
                                                        $sheet->cells('A1:B2', function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('center');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });

                                                        if ($vtab->type == 3) {
                                                            $step_ = (sizeof($Org) * 2) + 1 ;
                                                        }else{
                                                            $step_ = sizeof($Org)  + 1 ;
                                                        }

                                                        $sheet->setCellValue('A1', $tb_title);
                                                        $sheet->mergeCells('A1:'.$Index[$step_].'1');
                                                        $sheet->cells('A1:'.$Index[$step_].'1', function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('right');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });
                                                        $offSet = 2;
                                                        foreach ($Org as $k => $v) {
                                                            $sheet->setCellValue($Index[$offSet] . '2', (is_null($v->organization_name) || $v->organization_name == ' ') ? ' ' : $v->organization_name);
                                                            if ($vtab->type == 3) {
                                                                $sheet->setCellValue($Index[$offSet] . '3', trans('organization::application.count'));
                                                                $sheet->setCellValue($Index[$offSet + 1] . '3', trans('organization::application.amount'));
                                                                $sheet->mergeCells($Index[$offSet] . '2:' . $Index[$offSet + 1] . '2');
                                                                $sheet->setWidth([$Index[$offSet] => 10, $Index[$offSet + 1] => 10]);
                                                                $sheet->getStyle($Index[$offSet + 1])->getAlignment()->setWrapText(true);
                                                                $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . '3:' . $Index[$offSet + 1] . '3', function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });

                                                                $sheet->getStyle($Index[$offSet] . '2')->getAlignment()->setWrapText(true);
                                                                $sheet->getStyle($Index[$offSet + 1] . '2')->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . '2:' . $Index[$offSet + 1] . '2', function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });
                                                                $offSet += 2;
                                                            }
                                                            else {
                                                                $sheet->setCellValue($Index[$offSet] . '3', trans('organization::application.value'));
                                                                $sheet->setWidth([$Index[$offSet] => 16]);
                                                                $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . '3:' . $Index[$offSet + 1] . '3', function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });

                                                                $sheet->getStyle($Index[$offSet] . '2')->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . '2', function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });
                                                                $offSet += 1;
                                                            }
                                                        }

                                                        $opInc = 4;
                                                        foreach ($vtab->options as $kop => $vop) {
                                                            $sheet->setCellValue('A' . $opInc, $kop + 1);
                                                            $sheet->setCellValue('B' . $opInc, (is_null($vop->label) || $vop->label == ' ') ? ' ' : $vop->label);

                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $sheet->getStyle('A' . $opInc)->getAlignment()->setWrapText(true);
                                                            $sheet->getStyle('B' . $opInc)->getAlignment()->setWrapText(true);

                                                            $sheet->cells('A' . $opInc . ':' . 'B' . $opInc, function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $offSet = 2;
                                                            foreach ($Org as $k => $v) {
                                                                $orgData = AdministrativeReportData::OrgOptionData($vop->id ,$v->id , null);

                                                                if ($vtab->type == 3) {
                                                                    if(is_numeric($orgData['value']) || is_double($orgData['value']) || is_float($orgData['value']) || is_int($orgData['value'])) {
                                                                        $sheet->setCellValue($Index[$offSet] . $opInc, $orgData['value']);
                                                                    }else{
                                                                        $sheet->setCellValue($Index[$offSet] . $opInc, 0);
                                                                    }

                                                                    if(is_numeric($orgData['amount']) || is_double($orgData['amount']) || is_float($orgData['amount']) || is_int($orgData['amount'])) {
                                                                        $sheet->setCellValue($Index[$offSet + 1] . $opInc, $orgData['amount']);
                                                                    }else{
                                                                        $sheet->setCellValue($Index[$offSet + 1] . $opInc, 0);
                                                                    }

                                                                    $sheet->setWidth([$Index[$offSet] => 10, $Index[$offSet + 1] => 10]);
                                                                    $sheet->getStyle($Index[$offSet + 1])->getAlignment()->setWrapText(true);
                                                                    $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc.':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });

                                                                    $sheet->getStyle($Index[$offSet] .$opInc)->getAlignment()->setWrapText(true);
                                                                    $sheet->getStyle($Index[$offSet + 1] . $opInc)->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc .':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });
                                                                    $offSet += 2;
                                                                }
                                                                else {
                                                                    if(is_null($orgData['value'])){
                                                                        $sheet->setCellValue($Index[$offSet] . $opInc,'_');
                                                                    }else{
                                                                        $sheet->setCellValue($Index[$offSet] . $opInc,$orgData['value']);
                                                                    }
                                                                    $sheet->setWidth([$Index[$offSet] => 16]);
                                                                    $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc .':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });

                                                                    $sheet->getStyle($Index[$offSet] . $opInc)->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });
                                                                    $offSet += 1;
                                                                }
                                                            }
                                                            $opInc++;
                                                        }
                                                    }
                                                });
                                            }
                                            $tabs[] = $tab_name;
                                        }
                                    }
                                }
                                $excel->setActiveSheetIndex(0);
                                })
                                ->store('xlsx', storage_path($storage_path));
                                 $on_reports[]=$title_;
                            }
                    }
                    }
                }

                $token = md5(uniqid());
                $zipFileName = storage_path('tmp/export_' . $token . '.zip');
                $zip = new \ZipArchive;

                if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true ) {
                    foreach ( glob( $dirName . '/*' ) as $fileName ) {
                        $file = basename( $fileName );
                        $zip->addFile( $fileName, $file );
                    }
                    $zip->close();
                    array_map('unlink', glob("$dirName/*.*"));
                    rmdir($dirName);
                    return response()->json(['status' => 'success' , 'download_token' => $token]);
                }

            }

        return response()->json(['status' => 'failed','msg'=>trans('organization::application.no report data')]);
    }
    public function excelOrg(Request $request){

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit', '2048M');

        $id = $request->report_id;
        $target = $request->target;
        $ids = [$id];
        $user = \Auth::user();
        $organization_id = $user->organization_id;
        $orgs = [];
        if($target == 'user'){
            $orgs []= $user->organization_id;
        }else{
            $orgs = $request->organizations;

        }
        $reports = AdministrativeReport::with(['organization','tabs','tabs.columns','tabs.options'])->whereIn('id',$ids)->get();
        if(sizeof($reports) > 0) {

            $main = time();
            $name = 'storage/'.$main;
            $dirName = base_path($name);
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }

            $storage_path = $main.'/';
            $tabs = [];
            $on_reports = [];
            foreach ($reports as $k1=>$report) {
                $report_title = (is_null($report->title) || $report->title == ' ') ? null : $report->title;
                if (!is_null($report_title)) {
                    $title_ = $report->title . '-' . $report->organization->name ;
                    if(sizeof($report->tabs) > 0 ){
                        if (!in_array($title_, $on_reports)) {
                            \Maatwebsite\Excel\Facades\Excel::create($title_, function ($excel) use ($report, $tabs,$orgs) {
                                $excel->setTitle(trans('organization::application.report data'));
                                $excel->setDescription(trans('organization::application.report data'));
                                foreach ($report->tabs as $ktab => $vtab) {
                                    $Org = OrgReports::getReportOrg($report->id,$orgs);
                                    $tb_title = (is_null($vtab->title) || $vtab->title == ' ') ? null : $vtab->title;
                                    if (!is_null($tb_title)) {
                                        if (!in_array($tb_title, $tabs)) {
                                            $tab_name = trans('organization::application.tab_') . ' ( ' . ( $ktab+1 ). ' ) ';
                                            if ($report->category == 2 || $report->category == '2') {
                                                $excel->sheet($tab_name, function ($sheet) use ($report, $vtab,$Org,$tb_title) {
                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setAllBorders('solid');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setHeight(1, 40);
                                                    $sheet->getDefaultStyle()->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => ['name' => 'Simplified Arabic', 'size' => 11]
                                                    ]);
                                                    $sheet->setOrientation('landscape');
                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setStyle(['font' => ['name' => 'Simplified Arabic', 'size' => 11, 'bold' => true]]);
                                                    $sheet->setWidth(['A' => 5, 'B' => 20]);
                                                    $range = "A1:B1";
                                                    $sheet->getStyle($range)->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => ['name' => 'Simplified Arabic', 'size' => 11, 'bold' => true]
                                                    ]);

                                                    $Index = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                                        'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
                                                        'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ',
                                                        'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ',
                                                        'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ',
                                                        'EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EU', 'EV', 'EW', 'EX', 'EY', 'EZ',
                                                        'FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG', 'FH', 'FI', 'FJ', 'FK', 'FL', 'FM', 'FN', 'FO', 'FP', 'FQ', 'FR', 'FS', 'FT', 'FU', 'FV', 'FW', 'FX', 'FY', 'FZ',
                                                        'GA', 'GB', 'GC', 'GD', 'GE', 'GF', 'GG', 'GH', 'GI', 'GJ', 'GK', 'GL', 'GM', 'GN', 'GO', 'GP', 'GQ', 'GR', 'GS', 'GT', 'GU', 'GV', 'GW', 'GX', 'GY', 'GZ',
                                                        'HA', 'HB', 'HC', 'HD', 'HE', 'HF', 'HG', 'HH', 'HI', 'HJ', 'HK', 'HL', 'HM', 'HN', 'HO', 'HP', 'HQ', 'HR', 'HS', 'HT', 'HU', 'HV', 'HW', 'HX', 'HY', 'HZ',
                                                        'IA', 'IB', 'IC', 'ID', 'IE', 'IF', 'IG', 'IH', 'II', 'IJ', 'IK', 'IL', 'IM', 'IN', 'IO', 'IP', 'IQ', 'IR', 'IS', 'IT', 'IU', 'IV', 'IW', 'IX', 'IY', 'IZ',
                                                        'JA', 'JB', 'JC', 'JD', 'JE', 'JF', 'JG', 'JH', 'JI', 'JJ', 'JK', 'JL', 'JM', 'JN', 'JO', 'JP', 'JQ', 'JR', 'JS', 'JT', 'JU', 'JV', 'JW', 'JX', 'JY', 'JZ',
                                                        'KA', 'KB', 'KC', 'KD', 'KE', 'KF', 'KG', 'KH', 'KI', 'KJ', 'KK', 'KL', 'KM', 'KN', 'KO', 'KP', 'KQ', 'KR', 'KS', 'KT', 'KU', 'KV', 'KW', 'KX', 'KY', 'KZ',
                                                        'LA', 'LB', 'LC', 'LD', 'LE', 'LF', 'LG', 'LH', 'LI', 'LJ', 'LK', 'LL', 'LM', 'LN', 'LO', 'LP', 'LQ', 'LR', 'LS', 'LT', 'LU', 'LV', 'LW', 'LX', 'LY', 'LZ',
                                                        'MA', 'MB', 'MC', 'MD', 'ME', 'MF', 'MG', 'MH', 'MI', 'MJ', 'MK', 'ML', 'MM', 'MN', 'MO', 'MP', 'MQ', 'MR', 'MS', 'MT', 'MU', 'MV', 'MW', 'MX', 'MY', 'MZ',
                                                        'NA', 'NB', 'NC', 'ND', 'NE', 'NF', 'NG', 'NH', 'NI', 'NJ', 'NK', 'NL', 'NM', 'NN', 'NO', 'NP', 'NQ', 'NR', 'NS', 'NT', 'NU', 'NV', 'NW', 'NX', 'NY', 'NZ',
                                                        'OA', 'OB', 'OC', 'OD', 'OE', 'OF', 'OG', 'OH', 'OI', 'OJ', 'OK', 'OL', 'OM', 'ON', 'OO', 'OP', 'OQ', 'OR', 'OS', 'OT', 'OU', 'OV', 'OW', 'OX', 'OY', 'OZ'
                                                    ];

                                                    if (sizeof($vtab->options) > 0 && sizeof($vtab->columns) > 0) {
                                                        $sheet->setCellValue('A1', $tb_title);
                                                        $sheet->getRowDimension(1)->setRowHeight(-1);
                                                        $sheet->getRowDimension(2)->setRowHeight(-1);
                                                        $step_ = (sizeof($Org) * sizeof($vtab->columns));
                                                        $sheet->mergeCells('A1:'.$Index[$step_].'1');
                                                        $sheet->cells('A1:'.$Index[$step_].'1', function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('left');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });
                                                        $sheet->setCellValue('A2', trans('organization::application.#'));
                                                        $sheet->setCellValue('B2', trans('organization::application.for'));
                                                        $sheet->mergeCells('A2:A3');
                                                        $sheet->mergeCells('B2:B3');
                                                        $sheet->getRowDimension(1)->setRowHeight(-1);
                                                        $sheet->getRowDimension(2)->setRowHeight(-1);
                                                        $sheet->cells('A1:B2', function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('center');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });

                                                        $opInc = 4;
                                                        foreach ($vtab->options as $kop => $vop) {
                                                            $sheet->setCellValue('A' . $opInc, $kop + 1);
                                                            $sheet->setCellValue('B' . $opInc, (is_null($vop->label) || $vop->label == ' ') ? ' ' : $vop->label);

                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $sheet->getStyle('A' . $opInc)->getAlignment()->setWrapText(true);
                                                            $sheet->getStyle('B' . $opInc)->getAlignment()->setWrapText(true);

                                                            $sheet->cells('A' . $opInc . ':' . 'B' . $opInc, function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $opInc++;
                                                        }

                                                        $offSet = 2;
                                                        $step = sizeof($vtab->columns);
                                                        foreach ($Org as $k => $v) {
                                                            $sheet->setCellValue($Index[$offSet] . '2', (is_null($v->organization_name) || $v->organization_name == ' ') ? ' ' : $v->organization_name);
                                                            $sheet->mergeCells($Index[$offSet] . '2:' . $Index[$offSet + $step - 1] . '2');
                                                            $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $i = $offSet;
                                                            foreach ($vtab->columns as $kcol => $vcol) {
                                                                $sheet->setCellValue($Index[$i] . '3', (is_null($vcol->label) || $vcol->label == ' ') ? ' ' : $vcol->label);
                                                                $sheet->setWidth([$Index[$i] => 15]);
                                                                $sheet->getStyle($Index[$i] . '3')->getAlignment()->setWrapText(true);
                                                                $sheet->getStyle($Index[$i])->getAlignment()->setWrapText(true);
                                                                $i++;
                                                            }

                                                            $sheet->getStyle($Index[$offSet] . '2')->getAlignment()->setWrapText(true);
                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $sheet->cells($Index[$offSet] . '2:' . $Index[$i] . '2', function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $sheet->cells($Index[$offSet] . '3:' . $Index[$i] . '3', function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $offSet += $step;

                                                        }

                                                        $opInc = 4;
                                                        foreach ($vtab->options as $kop => $vop) {
                                                            $sheet->setCellValue('A' . $opInc, $kop + 1);
                                                            $sheet->setCellValue('B' . $opInc, (is_null($vop->label) || $vop->label == ' ') ? ' ' : $vop->label);

                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $sheet->getStyle('A' . $opInc)->getAlignment()->setWrapText(true);
                                                            $sheet->getStyle('B' . $opInc)->getAlignment()->setWrapText(true);

                                                            $sheet->cells('A' . $opInc . ':' . 'B' . $opInc, function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $offSet = 2;
                                                            foreach ($Org as $k => $v) {
                                                                foreach ($vtab->columns as $kcol => $vcol) {

                                                                    $orgData = AdministrativeReportData::OrgOptionData($vop->id, $v->id, $vcol->id);
                                                                    if(!($vcol->type == 1 || $vcol->type == 2)){
                                                                        if(is_null($orgData['value'])){
                                                                            $sheet->setCellValue($Index[$offSet] . $opInc,'_');
                                                                        }else{
                                                                            $sheet->setCellValue($Index[$offSet] . $opInc,$orgData['value']);
                                                                        }
                                                                        $sheet->setWidth([$Index[$offSet] => 35]);
                                                                    }else{
                                                                        if(is_null($orgData['value']) || $orgData['value'] == '' || $orgData['value'] == ' '){
                                                                            if($vcol->type == 1 || $vcol->type == 2 ){
                                                                                $sheet->setCellValue($Index[$offSet] . $opInc,0);
                                                                            }
                                                                        }else{
                                                                            $sheet->setCellValue($Index[$offSet] . $opInc,$orgData['value']);
                                                                        }
                                                                        $sheet->setWidth([$Index[$offSet] => 13]);
                                                                    }

                                                                    $sheet->setWidth([$Index[$offSet] => 16]);
                                                                    $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc . ':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });
                                                                    $sheet->getStyle($Index[$offSet] . $opInc)->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });
                                                                    $offSet += 1;
                                                                }
                                                            }
                                                            $opInc++;
                                                        }
                                                        $sheet->setCellValue('A' . $opInc, trans('organization::application.total'));
                                                        $sheet->mergeCells("A".$opInc .":B" . $opInc);
                                                        $sheet->getRowDimension(1)->setRowHeight(-1);
                                                        $sheet->getRowDimension(2)->setRowHeight(-1);
                                                        $sheet->cells("A".$opInc .":B" . $opInc, function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('center');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });

                                                        $offSet = 2;
                                                        foreach ($Org as $k => $v) {
                                                            foreach ($vtab->columns as $kcol => $vcol) {
                                                                if($vcol->type == 1 || $vcol->type == 2 ){
                                                                    $sum = AdministrativeReportData::OrgOptionSum($v->id, $vcol->id);
                                                                    $sheet->setCellValue($Index[$offSet] . $opInc,$sum);
                                                                    $sheet->setWidth([$Index[$offSet] => 13]);
                                                                }else{
                                                                    $sheet->setCellValue($Index[$offSet] . $opInc,'_');
                                                                    $sheet->setWidth([$Index[$offSet] => 35]);
                                                                }

                                                                $sheet->setWidth([$Index[$offSet] => 16]);
                                                                $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . $opInc . ':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });
                                                                $sheet->getStyle($Index[$offSet] . $opInc)->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . $opInc, function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });
                                                                $offSet += 1;
                                                            }
                                                        }

                                                    }
                                                });
                                            }
                                            else {
                                                $excel->sheet($tab_name, function ($sheet) use ($report, $vtab,$Org,$tb_title) {
                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setAllBorders('solid');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setHeight(1, 40);
                                                    $sheet->getDefaultStyle()->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => ['name' => 'Simplified Arabic', 'size' => 11]
                                                    ]);
                                                    $sheet->setOrientation('landscape');
                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setWidth(['A' => 5, 'B' => 20]);
                                                    $range = "A1:B1";
                                                    $sheet->getStyle($range)->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => ['name' => 'Simplified Arabic', 'size' => 11, 'bold' => true]
                                                    ]);

                                                    $Index = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                                                        'AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH', 'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV', 'AW', 'AX', 'AY', 'AZ',
                                                        'BA', 'BB', 'BC', 'BD', 'BE', 'BF', 'BG', 'BH', 'BI', 'BJ', 'BK', 'BL', 'BM', 'BN', 'BO', 'BP', 'BQ', 'BR', 'BS', 'BT', 'BU', 'BV', 'BW', 'BX', 'BY', 'BZ',
                                                        'CA', 'CB', 'CC', 'CD', 'CE', 'CF', 'CG', 'CH', 'CI', 'CJ', 'CK', 'CL', 'CM', 'CN', 'CO', 'CP', 'CQ', 'CR', 'CS', 'CT', 'CU', 'CV', 'CW', 'CX', 'CY', 'CZ',
                                                        'DA', 'DB', 'DC', 'DD', 'DE', 'DF', 'DG', 'DH', 'DI', 'DJ', 'DK', 'DL', 'DM', 'DN', 'DO', 'DP', 'DQ', 'DR', 'DS', 'DT', 'DU', 'DV', 'DW', 'DX', 'DY', 'DZ',
                                                        'EA', 'EB', 'EC', 'ED', 'EE', 'EF', 'EG', 'EH', 'EI', 'EJ', 'EK', 'EL', 'EM', 'EN', 'EO', 'EP', 'EQ', 'ER', 'ES', 'ET', 'EU', 'EV', 'EW', 'EX', 'EY', 'EZ',
                                                        'FA', 'FB', 'FC', 'FD', 'FE', 'FF', 'FG', 'FH', 'FI', 'FJ', 'FK', 'FL', 'FM', 'FN', 'FO', 'FP', 'FQ', 'FR', 'FS', 'FT', 'FU', 'FV', 'FW', 'FX', 'FY', 'FZ',
                                                        'GA', 'GB', 'GC', 'GD', 'GE', 'GF', 'GG', 'GH', 'GI', 'GJ', 'GK', 'GL', 'GM', 'GN', 'GO', 'GP', 'GQ', 'GR', 'GS', 'GT', 'GU', 'GV', 'GW', 'GX', 'GY', 'GZ',
                                                        'HA', 'HB', 'HC', 'HD', 'HE', 'HF', 'HG', 'HH', 'HI', 'HJ', 'HK', 'HL', 'HM', 'HN', 'HO', 'HP', 'HQ', 'HR', 'HS', 'HT', 'HU', 'HV', 'HW', 'HX', 'HY', 'HZ',
                                                        'IA', 'IB', 'IC', 'ID', 'IE', 'IF', 'IG', 'IH', 'II', 'IJ', 'IK', 'IL', 'IM', 'IN', 'IO', 'IP', 'IQ', 'IR', 'IS', 'IT', 'IU', 'IV', 'IW', 'IX', 'IY', 'IZ',
                                                        'JA', 'JB', 'JC', 'JD', 'JE', 'JF', 'JG', 'JH', 'JI', 'JJ', 'JK', 'JL', 'JM', 'JN', 'JO', 'JP', 'JQ', 'JR', 'JS', 'JT', 'JU', 'JV', 'JW', 'JX', 'JY', 'JZ',
                                                        'KA', 'KB', 'KC', 'KD', 'KE', 'KF', 'KG', 'KH', 'KI', 'KJ', 'KK', 'KL', 'KM', 'KN', 'KO', 'KP', 'KQ', 'KR', 'KS', 'KT', 'KU', 'KV', 'KW', 'KX', 'KY', 'KZ',
                                                        'LA', 'LB', 'LC', 'LD', 'LE', 'LF', 'LG', 'LH', 'LI', 'LJ', 'LK', 'LL', 'LM', 'LN', 'LO', 'LP', 'LQ', 'LR', 'LS', 'LT', 'LU', 'LV', 'LW', 'LX', 'LY', 'LZ',
                                                        'MA', 'MB', 'MC', 'MD', 'ME', 'MF', 'MG', 'MH', 'MI', 'MJ', 'MK', 'ML', 'MM', 'MN', 'MO', 'MP', 'MQ', 'MR', 'MS', 'MT', 'MU', 'MV', 'MW', 'MX', 'MY', 'MZ',
                                                        'NA', 'NB', 'NC', 'ND', 'NE', 'NF', 'NG', 'NH', 'NI', 'NJ', 'NK', 'NL', 'NM', 'NN', 'NO', 'NP', 'NQ', 'NR', 'NS', 'NT', 'NU', 'NV', 'NW', 'NX', 'NY', 'NZ',
                                                        'OA', 'OB', 'OC', 'OD', 'OE', 'OF', 'OG', 'OH', 'OI', 'OJ', 'OK', 'OL', 'OM', 'ON', 'OO', 'OP', 'OQ', 'OR', 'OS', 'OT', 'OU', 'OV', 'OW', 'OX', 'OY', 'OZ'
                                                    ];

                                                    if (sizeof($vtab->options) > 0) {
                                                        $sheet->setCellValue('A2', trans('organization::application.#'));
                                                        $sheet->setCellValue('B2', trans('organization::application.for'));
                                                        $sheet->mergeCells('A2:A3');
                                                        $sheet->mergeCells('B2:B3');
                                                        $sheet->getRowDimension(1)->setRowHeight(-1);
                                                        $sheet->getRowDimension(2)->setRowHeight(-1);
                                                        $sheet->cells('A1:B2', function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('center');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });

                                                        if ($vtab->type == 3) {
                                                            $step_ = (sizeof($Org) * 2) + 1 ;
                                                        }else{
                                                            $step_ = sizeof($Org)  + 1 ;
                                                        }
                                                        $sheet->setCellValue('A1', $tb_title);
                                                        $sheet->mergeCells('A1:'.$Index[$step_].'1');
                                                        $sheet->cells('A1:'.$Index[$step_].'1', function ($cells) {
                                                            $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                            $cells->setAlignment('right');
                                                            $cells->setValignment('center');
                                                            $cells->setFontWeight('bold');
                                                        });
                                                        $offSet = 2;
                                                        foreach ($Org as $k => $v) {
                                                            $sheet->setCellValue($Index[$offSet] . '2', (is_null($v->organization_name) || $v->organization_name == ' ') ? ' ' : $v->organization_name);
                                                            if ($vtab->type == 3) {
                                                                $sheet->setCellValue($Index[$offSet] . '3', trans('organization::application.count'));
                                                                $sheet->setCellValue($Index[$offSet + 1] . '3', trans('organization::application.amount'));
                                                                $sheet->mergeCells($Index[$offSet] . '2:' . $Index[$offSet + 1] . '2');
                                                                $sheet->setWidth([$Index[$offSet] => 10, $Index[$offSet + 1] => 10]);
                                                                $sheet->getStyle($Index[$offSet + 1])->getAlignment()->setWrapText(true);
                                                                $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . '3:' . $Index[$offSet + 1] . '3', function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });

                                                                $sheet->getStyle($Index[$offSet] . '2')->getAlignment()->setWrapText(true);
                                                                $sheet->getStyle($Index[$offSet + 1] . '2')->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . '2:' . $Index[$offSet + 1] . '2', function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });
                                                                $offSet += 2;
                                                            }
                                                            else {
                                                                $sheet->setCellValue($Index[$offSet] . '3', trans('organization::application.value'));
                                                                $sheet->setWidth([$Index[$offSet] => 16]);
                                                                $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . '3:' . $Index[$offSet + 1] . '3', function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });

                                                                $sheet->getStyle($Index[$offSet] . '2')->getAlignment()->setWrapText(true);
                                                                $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                $sheet->cells($Index[$offSet] . '2', function ($cells) {
                                                                    $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                    $cells->setAlignment('center');
                                                                    $cells->setValignment('center');
                                                                    $cells->setFontWeight('bold');
                                                                });
                                                                $offSet += 1;
                                                            }
                                                        }

                                                        $opInc = 4;
                                                        foreach ($vtab->options as $kop => $vop) {
                                                            $sheet->setCellValue('A' . $opInc, $kop + 1);
                                                            $sheet->setCellValue('B' . $opInc, (is_null($vop->label) || $vop->label == ' ') ? ' ' : $vop->label);

                                                            $sheet->getRowDimension(1)->setRowHeight(-1);
                                                            $sheet->getRowDimension(2)->setRowHeight(-1);
                                                            $sheet->getStyle('A' . $opInc)->getAlignment()->setWrapText(true);
                                                            $sheet->getStyle('B' . $opInc)->getAlignment()->setWrapText(true);

                                                            $sheet->cells('A' . $opInc . ':' . 'B' . $opInc, function ($cells) {
                                                                $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                $cells->setAlignment('center');
                                                                $cells->setValignment('center');
                                                                $cells->setFontWeight('bold');
                                                            });

                                                            $offSet = 2;
                                                            foreach ($Org as $k => $v) {
                                                                $orgData = AdministrativeReportData::OrgOptionData($vop->id ,$v->id , null);

                                                                if ($vtab->type == 3) {
                                                                    if(is_numeric($orgData['value']) || is_double($orgData['value']) || is_float($orgData['value']) || is_int($orgData['value'])) {
                                                                        $sheet->setCellValue($Index[$offSet] . $opInc, $orgData['value']);
                                                                    }else{
                                                                        $sheet->setCellValue($Index[$offSet] . $opInc, 0);
                                                                    }

                                                                    if(is_numeric($orgData['amount']) || is_double($orgData['amount']) || is_float($orgData['amount']) || is_int($orgData['amount'])) {
                                                                        $sheet->setCellValue($Index[$offSet + 1] . $opInc, $orgData['amount']);
                                                                    }else{
                                                                        $sheet->setCellValue($Index[$offSet + 1] . $opInc, 0);
                                                                    }

                                                                    $sheet->setWidth([$Index[$offSet] => 10, $Index[$offSet + 1] => 10]);
                                                                    $sheet->getStyle($Index[$offSet + 1])->getAlignment()->setWrapText(true);
                                                                    $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc.':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });

                                                                    $sheet->getStyle($Index[$offSet] .$opInc)->getAlignment()->setWrapText(true);
                                                                    $sheet->getStyle($Index[$offSet + 1] . $opInc)->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc .':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });
                                                                    $offSet += 2;
                                                                }
                                                                else {
                                                                    if(is_null($orgData['value'])){
                                                                        $sheet->setCellValue($Index[$offSet] . $opInc,'_');
                                                                    }else{
                                                                        $sheet->setCellValue($Index[$offSet] . $opInc,$orgData['value']);
                                                                    }
                                                                    $sheet->setWidth([$Index[$offSet] => 16]);
                                                                    $sheet->getStyle($Index[$offSet])->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc .':' . $Index[$offSet + 1] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });

                                                                    $sheet->getStyle($Index[$offSet] . $opInc)->getAlignment()->setWrapText(true);
                                                                    $sheet->getRowDimension(1)->setRowHeight(-1);
                                                                    $sheet->getRowDimension(2)->setRowHeight(-1);
                                                                    $sheet->cells($Index[$offSet] . $opInc, function ($cells) {
                                                                        $cells->setBorder('solid', 'solid', 'solid', 'solid');
                                                                        $cells->setAlignment('center');
                                                                        $cells->setValignment('center');
                                                                        $cells->setFontWeight('bold');
                                                                    });
                                                                    $offSet += 1;
                                                                }
                                                            }
                                                            $opInc++;
                                                        }
                                                    }
                                                });
                                            }
                                            $tabs[] = $tab_name;
                                        }
                                    }
                                }
                                $excel->setActiveSheetIndex(0);
                            })
                                ->store('xlsx', storage_path($storage_path));
                            $on_reports[]=$title_;
                        }
                    }
                }
            }

            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');
            $zip = new \ZipArchive;

            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true ) {
                foreach ( glob( $dirName . '/*' ) as $fileName ) {
                    $file = basename( $fileName );
                    $zip->addFile( $fileName, $file );
                }
                $zip->close();
                array_map('unlink', glob("$dirName/*.*"));
                rmdir($dirName);
                return response()->json(['status' => 'success' , 'download_token' => $token]);
            }

        }

        return response()->json(['status' => 'failed','msg'=>trans('organization::application.no report data')]);
    }

    // export word file of report data according logged user
    public function word($id){

        try {

            $report = AdministrativeReport::with(['tabs'])->findOrFail($id);

            if(is_null($report->template)){
                return response()->json(['status' => 'failed','msg'=>trans('organization::application.no report template')]);
            }

            $user = \Auth::user();
            $entries = OrgReports::where('report_id',$id)->get();
            $OrgIds = [];

            if (sizeof($entries) > 0){
                foreach ($entries as $key => $value){
                    $OrgIds[]=$value->organization_id;
                }
            }

            if($report->category == 2 || $report->category == '2') {

                $tag_map = [];
                $type = [];
                $final_map = [];

                foreach ($report->tabs as $key=>$value){
                    $value->options = AdministrativeReportData::getOrgCustomTabData($OrgIds,$value->id);
                    foreach ($value->options as $k=>$v) {
                        $tar_key = $v->name.'**'. $v->col_name;

                        if(!isset($tag_map[$tar_key])){
                            $tag_map[$tar_key]=[];
                        }

                        if(!is_null($v->value)){
                            if( !($v->value == "" || $v->value == " ") ){
                                $tag_map[$tar_key][] = $v->value;
                            }
                        }

                        if(!isset($type[$tar_key])){
                            $type[$tar_key]=$v->col_type;
                        }
                    }
                }

                foreach ($tag_map as $k=>$v) {

                    $content_type = $type[$k] ;
                    if($content_type == 1 || $content_type == 2){
                        $sum = 0 ;
                        if(sizeof($v) > 0){
                            foreach ($v as $v_) {
                                $sum += $v_ ;
                            }
                        }
                        $final_map[] = ['name'=> $k ,'value'=> $sum ];
                    }else{
                        $sum = implode(" , ",$v);
                        $final_map[] = ['name'=> $k ,'value'=> $sum ];
                    }
                }

                $logo = null;
                if(!is_null($user->organization->logo)){
                    $logo = $user->organization->logo;
                }

                $path = base_path('storage/app/'.$report->template);

                $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);

                if (!is_null($logo))
                    $templateProcessor->setImg('logo', array('src' => base_path('storage/app/' . $logo), 'swh' => '150'));

                if (is_null($logo))
                    $templateProcessor->setValue('logo', ' ');


                $templateProcessor->setValue('report_title', $report->title);
                $templateProcessor->setValue('year', $report->year);
                $templateProcessor->setValue('month', $report->month);
                $templateProcessor->setValue('organization_name', $user->organization->name);
//                $templateProcessor->setValue('total_amount', $total_amount);
//                $templateProcessor->setValue('total_count', $total_count);
                foreach ($final_map as $key => $value) {
                    $templateProcessor->setValue($value['name'], $value['value']);
                }

                $token = md5(uniqid());

                $templateProcessor->saveAs(storage_path('tmp/export_' . $token . '.docx'));
                return response()->json(['status' => 'success' , 'download_token' => $token]);

            }
            else{
                $tag_map = [];
                $total_amount = 0;
                $total_count  = 0;
                foreach ($report->tabs as $key=>$value){
                    $value->options = AdministrativeReportData::getOrgTabData_($OrgIds,$value->id);
                    foreach ($value->options as $k=>$v) {
                        if(isset($tag_map[$v->option_id])){
                            $tag_map[$v->option_id]['value'][] = $v->value;
                            $tag_map[$v->option_id]['amount'][] = $v->amount;
                        }else{
                            $tag_map[$v->option_id] = [ 'type' => $value->type ,'name'=> $v->name ,'value'=> array() ,'amount'=> array()];
                            $tag_map[$v->option_id]['value'] [] = $v->value;
                            $tag_map[$v->option_id]['amount'][] = $v->amount;
                        }

                    }
                }
                $final_map = [];
                foreach ($tag_map as $k=>$v) {
                    if($v['type'] == 3){
                        $value = 0;
                        $amount = 0;

                        foreach ($v['value'] as $k_=>$v_) {
                            $value += $v_ ;
                            $total_count += (int) $v_;
                        }

                        foreach ($v['amount'] as $k_=>$v_) {
                            $amount += $v_ ;
                            $total_amount += (int) $v_;
                        }

                    }else{
                        $value = implode(" , ",$v['value']);
                        $amount = '';

                    }

                    $final_map[] = [ 'type' => $v['type'] ,'name'=>$v['name'] ,'value'=> $value ,'amount'=>$amount ];
                }

                $logo = null;
                if(!is_null($user->organization->logo)){
                    $logo = $user->organization->logo;
                }

                $path = base_path('storage/app/'.$report->template);

                $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);

                if (!is_null($logo))
                    $templateProcessor->setImg('logo', array('src' => base_path('storage/app/' . $logo), 'swh' => '150'));

                $templateProcessor->setValue('report_title', $report->title);
                $templateProcessor->setValue('year', $report->year);
                $templateProcessor->setValue('month', $report->month);
                $templateProcessor->setValue('organization_name', $user->organization->name);
                $templateProcessor->setValue('total_amount', $total_amount);
                $templateProcessor->setValue('total_count', $total_count);
                foreach ($final_map as $key => $value) {
                    if ($value['type'] == 3) {
                        $templateProcessor->setValue($value['name'] . '_count', $value['value']);
                        $templateProcessor->setValue($value['name'] . '_amount', $value['amount']);

                    } else {
                        $templateProcessor->setValue($value['name'], $value['value']);

                    }
                }

                $token = md5(uniqid());

                $templateProcessor->saveAs(storage_path('tmp/export_' . $token . '.docx'));
                return response()->json(['status' => 'success' , 'download_token' => $token]);
            }

        }
        catch (\Exception $e){
            return response()->json(['status' => false , 'msg' =>trans('organization::application.no report data to export')]);
        }


    }
    public function wordRelated($id){

//        try {

            $report = AdministrativeReport::with(['tabs'])->findOrFail($id);

            if(is_null($report->template)){
                return response()->json(['status' => 'failed','msg'=>trans('organization::application.no report template')]);
            }

            $user = \Auth::user();
            $entries = OrgReports::where(function ($anq_) use ($user,$id) {
                $anq_->where('report_id',$id);
                if($user->type == 2) {
                    $anq_->where(function ($anq) use ($user) {
                        $anq->where('organization_id',$user->organization_id);
                        $anq->orwherein('organization_id', function($quy) use($user) {
                            $quy->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });
                }
                else{
                    if (!$user->super_admin) {
                        $anq_->where(function ($anq) use ($user) {
                            $anq->where('organization_id',$user->organization_id);
                            $anq->orwherein('organization_id', function($qery) use($user) {
                                $qery->select('descendant_id')
                                    ->from('char_organizations_closure')
                                    ->where('ancestor_id', '=', $user->organization_id);
                            });
                        });
                    }}
                })
                ->get();

            $OrgIds = [];

            if (sizeof($entries) > 0){
                foreach ($entries as $key => $value){
                    $OrgIds[]=$value->organization_id;
                }
            }

            if($report->category == 2 || $report->category == '2') {

                $tag_map = [];
                $type = [];
                $final_map = [];

                foreach ($report->tabs as $key=>$value){
                    $value->options = AdministrativeReportData::getOrgCustomTabData($OrgIds,$value->id);
                    foreach ($value->options as $k=>$v) {
                        $tar_key = $v->name.'**'. $v->col_name;

                        if(!isset($tag_map[$tar_key])){
                            $tag_map[$tar_key]=[];
                        }

                        if(!is_null($v->value)){
                            if( !($v->value == "" || $v->value == " ") ){
                                $tag_map[$tar_key][] = $v->value;
                            }
                        }

                        if(!isset($type[$tar_key])){
                            $type[$tar_key]=$v->col_type;
                        }
                    }
                }

                foreach ($tag_map as $k=>$v) {

                    $content_type = $type[$k] ;
                    if($content_type == 1 || $content_type == 2){
                        $sum = 0 ;
                        if(sizeof($v) > 0){
                            foreach ($v as $v_) {
                                $sum += $v_ ;
                            }
                        }
                        $final_map[] = ['name'=> $k ,'value'=> $sum ];
                    }else{
                        $sum = implode(" , ",$v);
                        $final_map[] = ['name'=> $k ,'value'=> $sum ];
                    }
                }

                $logo = null;
                if(!is_null($user->organization->logo)){
                    $logo = $user->organization->logo;
                }

                $path = base_path('storage/app/'.$report->template);

                $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);

                if (!is_null($logo))
                    $templateProcessor->setImg('logo', array('src' => base_path('storage/app/' . $logo), 'swh' => '150'));

                if (is_null($logo))
                    $templateProcessor->setValue('logo', ' ');


                $templateProcessor->setValue('report_title', $report->title);
                $templateProcessor->setValue('year', $report->year);
                $templateProcessor->setValue('month', $report->month);
                $templateProcessor->setValue('organization_name', $user->organization->name);
//                $templateProcessor->setValue('total_amount', $total_amount);
//                $templateProcessor->setValue('total_count', $total_count);
                foreach ($final_map as $key => $value) {
                    $templateProcessor->setValue($value['name'], $value['value']);
                }

                $token = md5(uniqid());

                $templateProcessor->saveAs(storage_path('tmp/export_' . $token . '.docx'));
                return response()->json(['status' => 'success' , 'download_token' => $token]);

            }
            else{
                $tag_map = [];
                $total_amount = 0;
                $total_count  = 0;
                foreach ($report->tabs as $key=>$value){
                    $value->options = AdministrativeReportData::getOrgTabData_($OrgIds,$value->id);
                    foreach ($value->options as $k=>$v) {
                        if(isset($tag_map[$v->option_id])){
                            $tag_map[$v->option_id]['value'][] = $v->value;
                            $tag_map[$v->option_id]['amount'][] = $v->amount;
                        }else{
                            $tag_map[$v->option_id] = [ 'type' => $value->type ,'name'=> $v->name ,'value'=> array() ,'amount'=> array()];
                            $tag_map[$v->option_id]['value'] [] = $v->value;
                            $tag_map[$v->option_id]['amount'][] = $v->amount;
                        }

                    }
                }
                $final_map = [];
                foreach ($tag_map as $k=>$v) {
                    if($v['type'] == 3){
                        $value = 0;
                        $amount = 0;

                        foreach ($v['value'] as $k_=>$v_) {
                            $value += $v_ ;
                            $total_count += (int) $v_;
                        }

                        foreach ($v['amount'] as $k_=>$v_) {
                            $amount += $v_ ;
                            $total_amount += (int) $v_;
                        }

                    }else{
                        $value = implode(" , ",$v['value']);
                        $amount = '';

                    }

                    $final_map[] = [ 'type' => $v['type'] ,'name'=>$v['name'] ,'value'=> $value ,'amount'=>$amount ];
                }

                $logo = null;
                if(!is_null($user->organization->logo)){
                    $logo = $user->organization->logo;
                }

                $path = base_path('storage/app/'.$report->template);

                $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);

                if (!is_null($logo))
                    $templateProcessor->setImg('logo', array('src' => base_path('storage/app/' . $logo), 'swh' => '150'));

                $templateProcessor->setValue('report_title', $report->title);
                $templateProcessor->setValue('year', $report->year);
                $templateProcessor->setValue('month', $report->month);
                $templateProcessor->setValue('organization_name', $user->organization->name);
                $templateProcessor->setValue('total_amount', $total_amount);
                $templateProcessor->setValue('total_count', $total_count);
                foreach ($final_map as $key => $value) {
                    if ($value['type'] == 3) {
                        $templateProcessor->setValue($value['name'] . '_count', $value['value']);
                        $templateProcessor->setValue($value['name'] . '_amount', $value['amount']);

                    } else {
                        $templateProcessor->setValue($value['name'], $value['value']);

                    }
                }

                $token = md5(uniqid());

                $templateProcessor->saveAs(storage_path('tmp/export_' . $token . '.docx'));
                return response()->json(['status' => 'success' , 'download_token' => $token]);
            }

//        }
//        catch (\Exception $e){
//            $response["status"] = 'error';
//            $response["error_code"] = $e->getCode();
//            $response["msg"] = $e->getMessage();
//            return response()->json($response);
//            return response()->json(['status' => false , 'msg' =>trans('organization::application.no report data to export')]);
//        }


    }
    public function wordOrg(Request $request){

        try {

            $main = time();
            $name = 'storage/'.$main;
            $dirName = base_path($name);
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }

            $storage_path = $main.'/';

            $id = $request->report_id;
            $target = $request->target;
            $report = AdministrativeReport::with(['tabs'])->findOrFail($id);

            if(is_null($report->template)){
                return response()->json(['status' => 'failed','msg'=>trans('organization::application.no report template')]);
            }

            $user = \Auth::user();
            $organization=$user->organization;
            $organization_id = $user->organization_id;

            $OrgIds = [];
            if($target == 'user'){
                $OrgIds []= $user->organization_id;
            }else{
                $OrgIds = $request->organizations;

            }

            $Org = OrgReports::getReportOrg($id,$OrgIds);

            foreach ($Org as $k=>$vl) {
                if($report->category == 2 || $report->category == '2') {

                    $tag_map = [];
                    $type = [];
                    $final_map = [];

                    foreach ($report->tabs as $key=>$value){
                        $value->options = AdministrativeReportData::getOrgCustomTabData($OrgIds,$value->id);
                        foreach ($value->options as $k=>$v) {
                            $tar_key = $v->name.'**'. $v->col_name;

                            if(!isset($tag_map[$tar_key])){
                                $tag_map[$tar_key]=[];
                            }

                            if(!is_null($v->value)){
                                if( !($v->value == "" || $v->value == " ") ){
                                    $tag_map[$tar_key][] = $v->value;
                                }
                            }

                            if(!isset($type[$tar_key])){
                                $type[$tar_key]=$v->col_type;
                            }
                        }
                    }

                    foreach ($tag_map as $k=>$v) {

                        $content_type = $type[$k] ;
                        if($content_type == 1 || $content_type == 2){
                            $sum = 0 ;
                            if(sizeof($v) > 0){
                                foreach ($v as $v_) {
                                    $sum += $v_ ;
                                }
                            }
                            $final_map[] = ['name'=> $k ,'value'=> $sum ];
                        }else{
                            $sum = implode(" , ",$v);
                            $final_map[] = ['name'=> $k ,'value'=> $sum ];
                        }
                    }

                    $logo = null;
                    if(!is_null($user->organization->logo)){
                        $logo = $user->organization->logo;
                    }

                    $path = base_path('storage/app/'.$report->template);

                    $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);

                    if (!is_null($logo))
                        $templateProcessor->setImg('logo', array('src' => base_path('storage/app/' . $logo), 'swh' => '150'));

                    if (is_null($logo))
                        $templateProcessor->setValue('logo', ' ');


                    $templateProcessor->setValue('report_title', $report->title);
                    $templateProcessor->setValue('year', $report->year);
                    $templateProcessor->setValue('month', $report->month);
                    $templateProcessor->setValue('organization_name', $user->organization->name);
                    //                $templateProcessor->setValue('total_amount', $total_amount);
                    //                $templateProcessor->setValue('total_count', $total_count);
                    foreach ($final_map as $ke_ => $val_) {
                        $templateProcessor->setValue($val_['name'], $val_['value']);
                    }

                    $token = md5(uniqid());
                    $templateProcessor->saveAs(storage_path($storage_path) . '' . $vl->organization_name . '.docx');
//               return response()->json(['status' => 'success' , 'download_token' => $token]);

                }
                else{
                    $tag_map = [];
                    $total_amount = 0;
                    $total_count  = 0;
                    foreach ($report->tabs as $key=>$value){
                        $value->options = AdministrativeReportData::getOrgTabData_($OrgIds,$value->id);
                        foreach ($value->options as $k=>$v) {
                            if(isset($tag_map[$v->option_id])){
                                $tag_map[$v->option_id]['value'][] = $v->value;
                                $tag_map[$v->option_id]['amount'][] = $v->amount;
                            }else{
                                $tag_map[$v->option_id] = [ 'type' => $value->type ,'name'=> $v->name ,'value'=> array() ,'amount'=> array()];
                                $tag_map[$v->option_id]['value'] [] = $v->value;
                                $tag_map[$v->option_id]['amount'][] = $v->amount;
                            }

                        }
                    }
                    $final_map = [];
                    foreach ($tag_map as $k=>$v) {
                        if($v['type'] == 3){
                            $value = 0;
                            $amount = 0;

                            foreach ($v['value'] as $k_=>$v_) {
                                $value += $v_ ;
                                $total_count += (int) $v_;
                            }

                            foreach ($v['amount'] as $k_=>$v_) {
                                $amount += $v_ ;
                                $total_amount += (int) $v_;
                            }

                        }else{
                            $value = implode(" , ",$v['value']);
                            $amount = '';

                        }

                        $final_map[] = [ 'type' => $v['type'] ,'name'=>$v['name'] ,'value'=> $value ,'amount'=>$amount ];
                    }

                    $logo = null;
                    if(!is_null($user->organization->logo)){
                        $logo = $user->organization->logo;
                    }

                    $path = base_path('storage/app/'.$report->template);

                    $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);

                    if (!is_null($logo))
                        $templateProcessor->setImg('logo', array('src' => base_path('storage/app/' . $logo), 'swh' => '150'));

                    $templateProcessor->setValue('report_title', $report->title);
                    $templateProcessor->setValue('year', $report->year);
                    $templateProcessor->setValue('month', $report->month);
                    $templateProcessor->setValue('organization_name', $user->organization->name);
                    $templateProcessor->setValue('total_amount', $total_amount);
                    $templateProcessor->setValue('total_count', $total_count);
                    foreach ($final_map as $key_ => $val_) {
                        if ($value['type'] == 3) {
                            $templateProcessor->setValue($val_['name'] . '_count', $val_['value']);
                            $templateProcessor->setValue($val_['name'] . '_amount', $val_['amount']);

                        } else {
                            $templateProcessor->setValue($value['name'], $value['value']);

                        }
                    }

                    $token = md5(uniqid());

                    $templateProcessor->saveAs(storage_path($storage_path) . '' . $vl->organization_name . '.docx');
//               return response()->json(['status' => 'success' , 'download_token' => $token]);
                }
            }

            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');
            $zip = new \ZipArchive;

            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true ) {
                foreach ( glob( $dirName . '/*' ) as $fileName ) {
                    $file = basename( $fileName );
                    $zip->addFile( $fileName, $file );
                }
                $zip->close();
                array_map('unlink', glob("$dirName/*.*"));
                rmdir($dirName);
                return response()->json(['status' => 'success' , 'download_token' => $token]);
            }

            return response()->json(['status' => false , 'msg' =>trans('organization::application.no report data to export')]);

        }
        catch (\Exception $e){
            return response()->json(['status' => false , 'msg' =>trans('organization::application.no report data to export')]);
        }
    }

    // export instruction of generate template of report
    public function instruction($id){

        $report = AdministrativeReport::with(['tabs','tabs.columns','tabs.columns.optionList','tabs.options','tabs.options.optionList'])->findOrFail($id);

        $rows = [];
        $rows[]=['tab_name'=> ' ', 'option_label'=>trans('organization::application.report_title' ), 'option_tag'=>'${report_title}'];
        $rows[]=['tab_name'=> ' ', 'option_label'=>trans('organization::application.year') , 'option_tag'=>'${year}'];
        $rows[]=['tab_name'=> ' ', 'option_label'=>trans('organization::application.month'), 'option_tag'=>'${month}'];
        $rows[]=['tab_name'=> ' ', 'option_label'=>trans('organization::application.organization_name'), 'option_tag'=>'${organization_name}'];
        $rows[]=['tab_name'=> ' ', 'option_label'=>trans('organization::application.organization_logo'), 'option_tag'=>'${logo}'];

        if($report->category == 2 || $report->category == '2') {
            foreach ($report->tabs as $key=>$value){
                foreach ($value->options as $k=>$v){
                    foreach ($value->columns as $k1=>$v1){
                        $rows[]=['tab_name'=>$value->title, 'option_label'=>$v->label .'  ('. $v1->label .')  ', 'option_tag'=>'${'.$v->name.'**'. $v1->name.'}'];
                    }
                }
            }
        }else{
            foreach ($report->tabs as $key=>$value){
                foreach ($value->options as $k=>$v){
                    $rows[]=['tab_name'=>$value->title, 'option_label'=>$v->label, 'option_tag'=>'${'.$v->name.'}'];
                }
            }
        }

        if(sizeof($rows) !=0){
            $data=array();
            foreach($rows as $key =>$value){
                $data[$key][trans('aid::application.#')]=$key+1;
                foreach($value as $k =>$v){
                    $data[$key][trans('organization::application.' . $k)]= $v;
                }
            }
            $token = md5(uniqid());
            \Excel::create('export_' . $token, function($excel) use($data) {
                $excel->setTitle(trans('organization::application.report_tag'));
                $excel->setDescription(trans('organization::application.report_tag'));
                $excel->sheet(trans('organization::application.report_tag'), function($sheet) use($data) {

                    $sheet->setRightToLeft(true);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);
                    $sheet->getStyle("A1:D1")->applyFromArray(['font' => ['bold' => true]]);
                    $sheet->fromArray($data);
                });
            })->store('xlsx', storage_path('tmp/'));
            return response()->json(['status' => 'success' , 'download_token' => $token]);
        }
        return response()->json(['status' => 'failed','msg'=>trans('organization::application.no report data')]);

    }

    // list all orgs can list on organization reports
    public function Org(){

        $user = \Illuminate\Support\Facades\Auth::user();

        $super_admin= $user->super_admin;
        $user_id = $user->id;
        $user_type = $user->type;
        $organization_id=$user->organization_id;
        $user_organization =$user->organization;
        $user_organization_type = $user_organization->type;

        $language_id =  \App\Http\Helpers::getLocale();

        $organizations = Organization::where(function ($q) use ($user_id,$super_admin , $user_type, $organization_id , $user_organization_type ) {

                            if(!($super_admin == 1 || $super_admin == '1')){
                                if($user_type == 2) {
                                    $q->where(function ($anq) use ($user_id,$organization_id) {
                                        $anq->where('organization_id',$organization_id);
                                        $anq->orwherein('organization_id', function($quer) use($user_id) {
                                            $quer->select('char_user_organizations.organization_id')
                                                ->from('char_user_organizations')
                                                ->where('user_id', '=', $user_id);
                                        });
                                    });
                                }else{
                                    $q->where(function ($anq) use ($organization_id) {
                                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                                        $anq->wherein('organization_id', function($quer) use($organization_id) {
                                            $quer->select('descendant_id')
                                                ->from('char_organizations_closure')
                                                ->where('ancestor_id', '=', $organization_id);
                                        });
                                    });
                                }

                                $q->orwherein('organization_id', function ($q) use ($organization_id) {
                                    $q->select('char_organization_sponsors.sponsor_id')
                                        ->from('char_organization_sponsors')
                                        ->where('organization_id', '=', $organization_id);
                                });
                            }
                   })
                        ->selectRaw("char_organizations.* ,
                                               CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ")
                        ->orderBy('name','asc')
                        ->get();

        return response()->json(['organizations' => $organizations ]);
    }

    // update request status  using request id
    public function status(Request $request)
    {
        $action = $request->action ;
        $field = $request->field ;

        if($action =='single'){
            $id = $request->target ;
            $entity = AdministrativeReport::findOrFail($id);
            $entity->$field = $request->status;

            if($entity->saveOrFail()) {
                \Log\Model\Log::saveNewLog('ADMINSTRATIVE_REPORT_UPDATED', trans('organization::application.He edited the properties of the report') . ' "' . $entity->title . '" ');
                $response["status"]= 'success';
                $response["msg"]= trans('sms::application.The row is updated');

            }else{
                $response["status"]= 'failed';
                $response["msg"]= trans('sms::application.There is no update');
            }
            return response()->json($response);
        }else{

            $requests = $request->requests;
            if(AdministrativeReport::whereIn('id',$requests)->update([ $field => $request->status])) {
                \Log\Model\Log::saveNewLog('ADMINSTRATIVE_REPORT_UPDATED', trans('organization::application.He edited the properties of the report'));
                $response["status"]= 'success';
                $response["msg"]= trans('sms::application.The row is updated');

            }else{
                $response["status"]= 'failed';
                $response["msg"]= trans('sms::application.There is no update');
            }
            return response()->json($response);

        }
    }

    public function OrgStatus(Request $request)
    {
        $action = $request->action ;
        $field = $request->field ;

        if($action =='single'){
            $id = $request->target ;
            $entity = OrgReports::findOrFail($id);
            $entity->status = $request->status;
            $entity_ = AdministrativeReport::findOrFail($entity->report_id);

            if($entity->saveOrFail()) {
                \Log\Model\Log::saveNewLog('ADMINSTRATIVE_REPORT_ORGANIZATION_DATA_STATUS_UPDATED', trans('organization::application.He edited the properties of the report data status') . ' "' . $entity_->title . '" ');
                $response["status"]= 'success';
                $response["msg"]= trans('sms::application.The row is updated');

            }else{
                $response["status"]= 'failed';
                $response["msg"]= trans('sms::application.There is no update');
            }
            return response()->json($response);
        }else{

            $requests = $request->requests;
            if(OrgReports::whereIn('id',$requests)->update([ $field => $request->status])) {
                \Log\Model\Log::saveNewLog('ADMINSTRATIVE_REPORT_ORGANIZATION_DATA_STATUS_UPDATED', trans('organization::application.He edited the properties of the report data status'));
                $response["status"]= 'success';
                $response["msg"]= trans('sms::application.The row is updated');

            }else{
                $response["status"]= 'failed';
                $response["msg"]= trans('sms::application.There is no update');
            }
            return response()->json($response);

        }
    }

    public function usersList(Request $request)
    {
        return response()->json(['users' => User::OrgUser($request->organization_id,$request->user_id)]);
    }

    public function setUser(Request $request)
    {
        $action = $request->action ;
        $field = $request->field ;
        $response=[];
        $where = ['report_id' => $request->report_id, 'organization_id' => $request->organization_id];
        if(OrgReports::where($where)->update([ 'user_id' => $request->user_id])) {
            \Log\Model\Log::saveNewLog('ADMINSTRATIVE_REPORT_DATA_UPDATED', trans('organization::application.He edited the properties of the report data'));
            $response["status"]= 'success';
            $response["msg"]= trans('sms::application.The row is updated');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sms::application.There is no update');
        }

        return response()->json($response);

    }

}
