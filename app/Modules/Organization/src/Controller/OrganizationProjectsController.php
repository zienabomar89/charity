<?php

namespace Organization\Controller;

use Aid\Model\Vouchers;
use App\Http\Controllers\Controller;
use Auth\Model\UserOrg;
use Illuminate\Http\Request;
use Organization\Model\OrganizationProject;
use Organization\Model\OrganizationProjectAttachments;
use Organization\Model\Organization;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Document\Model\File;

class OrganizationProjectsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // filter user report
    public function filter(Request $request)
    {
        $this->authorize('manageTwo', OrganizationProject::class);

        $entries = OrganizationProject::filter($request->all());
        if ($request->action == 'excel') {
            if (sizeof($entries) > 0) {
                $data=array();
                foreach($entries as $key =>$value){
                    $data[$key][trans('organization::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        $data[$key][trans('organization::application.' . $k)]= $v;
                    }
                }
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($data) {
                    $excel->setTitle(trans('organization::application.organizations-projects'));
                    $excel->setDescription(trans('organization::application.organizations-projects'));
                    $excel->sheet(trans('organization::application.organizations-projects'), function ($sheet) use ($data) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->setWidth(array('A'=> 8, 'B'=>40,'C' => 45, 'D' => 40,
                            'E' => 20 ,'F' => 40 ,'G' => 20,'H' => 20,
                            'I' => 20 ,'J' => 20 ,'K' => 20,'L' => 20 ,'M' => 20,'N' => 20 ,'O' => 20  ,'P' => 20  ,'Q' => 40  ,'R' => 20 ,'S' => 20
                        ,'T' => 40  ,'U' => 15  ,'V' => 15 ,'W' => 40,'X' => 15,'Y' => 45,'Z' => 15,'AA' => 40,'AB' => 40,'AC' => 40));
                        
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);
                        $sheet->getStyle("A1:AB1")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' => ['bold' => true]]);
                        $sheet->fromArray($data);
                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['download_token' => $token]);
            }
            return response()->json(['status' => 'failed', 'msg' => trans('organization::application.no report data')]);
        }

        $entries->map(function ($q) {
            $q->voucher_row = Vouchers::fetch($q->voucher_id);
            return $q;
        });

        return response()->json(['items' => $entries]);

    }

    // create new project with option
    public function store(Request $request)
    {
        $this->authorize('create', OrganizationProject::class);

        $user = \Auth::user();
        $UserOrg_=$user->organization;
        $organization_id=$user->organization_id;
        $UserOrgType=$UserOrg_->type;


        $rules = ['title'=> 'required', 'description' => 'required','date' => 'required',
            'category_id' => 'required','sub_category_id' => 'required','type_id'=> 'required','distribute'=> 'required',
            'activity_category_id' => 'required','amount'=> 'required',
            'currency_id'=> 'required', 'date_from' => 'required','date_to' => 'required' ,
            'region' => 'required','target_group' => 'required', 'count' => 'required',
            'beneficiary_category_id' => 'required', /*'sponsor_id' => 'required','country_id' => 'required',*/
            'shared' => 'required','shared_organizations'=> 'required_if:shared,1',
            'has_sponsor' => 'required','sponsor_organizations'=> 'required_if:has_sponsor,1'

        ];


        if($UserOrgType == 1){
            $rules['sponsor_id'] = 'required' ;
            $rules['country_id'] = 'required' ;
        }
        $error = \App\Http\Helpers::isValid($request->all(), $rules);
        if ($error)
            return response()->json($error);


        $entry = new OrganizationProject();
        $entry->user_id = $user->id;
        $entry->organization_id = $user->organization_id;

        $inputs = ['title', 'description' ,'date' ,'type_id','distribute','sub_category_id','category_id','activity_category_id'  ,'amount',
            'currency_id', 'date_from' ,'date_to'  ,'region' ,'target_group', 'count' ,'has_sponsor','sponsor_organizations',
            'beneficiary_category_id' ,'sponsor_id' ,'country_id' ,'shared' ,'shared_organizations' , 'notes'];
        $dates = ['date_from', 'date_to','date'];


        foreach ($inputs as $key) {
            if (isset($request->$key)) {
                if(in_array($key, $dates)) {
                    $entry->$key = date('Y-m-d', strtotime($request->$key));
                } else {
                    $entry->$key = $request->$key;
                }
            }
        }

        if ($entry->save()) {
            \Log\Model\Log::saveNewLog('ORGANIZATION_PROJECT_CREATED', trans('organization::application.He added a new project') . ' "' . $entry->title . '" ');
            return response()->json(['status' => 'success','row'=>$entry, 'report_id' => $entry->id, 'msg' => trans('organization::application.The row is inserted to db')]);
        }

        return response()->json(['status' => 'failed', 'msg' => trans('organization::application.The row is not insert to db')]);
    }

    // get existed object model by id
    public function show($id)
    {
        $entry = OrganizationProject::with(['attachments','attachments.file'])->findOrFail($id);
        $this->authorize('view', $entry);
        return response()->json($entry);

    }

    // get existed object model by id
    public function fetch($id)
    {
        $entry = OrganizationProject::fetch($id);
        $entry->attachments = OrganizationProjectAttachments::with('file')
                                               ->where('project_id' ,$id)
                                               ->get();

        return response()->json($entry);

    }

    // update exist project with option by id
    public function update(Request $request, $id)
    {

        $entry = OrganizationProject::findOrFail($id);
        $this->authorize('update', $entry);

        $user = \Auth::user();
        $UserOrg_=$user->organization;
        $organization_id=$user->organization_id;
        $UserOrgType=$UserOrg_->type;


        $rules = ['title'=> 'required', 'description' => 'required','date' => 'required',
                  'category_id' => 'required','sub_category_id' => 'required','type_id'=> 'required','distribute'=> 'required',
                  'activity_category_id' => 'required','amount'=> 'required',
                  'currency_id'=> 'required', 'date_from' => 'required','date_to' => 'required' ,
                  'region' => 'required','target_group' => 'required', 'count' => 'required',
                  'beneficiary_category_id' => 'required', /*'sponsor_id' => 'required','country_id' => 'required',*/
                  'shared' => 'required','shared_organizations'=> 'required_if:shared,1',
                  'has_sponsor' => 'required','sponsor_organizations'=> 'required_if:has_sponsor,1'];


        if($UserOrgType == 1){
            $rules['sponsor_id'] = 'required' ;
            $rules['country_id'] = 'required' ;
        }

        $error = \App\Http\Helpers::isValid($request->all(), $rules);
        if ($error)
            return response()->json($error);

        $inputs = ['title', 'description' ,'date' ,'type_id','distribute','category_id','sub_category_id','activity_category_id'  ,'amount',
                   'currency_id', 'date_from' ,'date_to'  ,'region' ,'target_group', 'count' ,'has_sponsor','sponsor_organizations',
                   'beneficiary_category_id' ,'sponsor_id' ,'country_id' ,'shared' ,'shared_organizations' , 'notes'];
        $dates = ['date_from', 'date_to','date'];

        foreach ($inputs as $key) {
            if (isset($request->$key)) {
                if(in_array($key, $dates)) {
                    $entry->$key = date('Y-m-d', strtotime($request->$key));
                } else {
                    $entry->$key = $request->$key;
                }
            }
        }

        if ($entry->save()) {
            \Log\Model\Log::saveNewLog('ORGANIZATION_PROJECT_UPDATED', trans('organization::application.He edited the properties of the project') . ' "' . $entry->title . '" ');
            return response()->json(['status' => 'success','row'=>$entry, 'msg' => trans('organization::application.The row is updated')]);
        }
        return response()->json(['status' => 'failed', 'msg' => trans('organization::application.There is no update')]);

    }

    
    // update existed object model by id
    public function destroy($id, Request $request)
    {
        $entry = OrganizationProject::findOrFail($id);
        $this->authorize('delete', $entry);
        $title = $entry->title;
        if ($entry->destroy($id)) {
            \Log\Model\Log::saveNewLog('ORGANIZATION_PROJECT_DELETED', trans('organization::application.He deleted the project') . ' "' . $title . '" ');
            return response()->json(['status' => 'success', 'msg' => trans('organization::application.The row is deleted from db')]);
        }
        return response()->json(['status' => 'failed', 'msg' => trans('organization::application.The row is not deleted from db')]);
    }


    // restore trashed project by id
    public function restore(Request $request, $id)
    {
        $entity = OrganizationProject::onlyTrashed()->findOrFail($id);
//        $this->authorize('restore', $entity);
        if ($entity->trashed()) {
            $entity->restore();
            $title = $entity->title;
            \Log\Model\Log::saveNewLog('ORGANIZATION_PROJECT_RESTORE',trans('organization::application.He restore the project')  . ' "' . $title . '" ');
            return response()->json(['status' => 'success','msg'=>trans('organization::application.action success')]);
        }
        return response()->json(['status' => 'failed','msg'=>trans('organization::application.action failed')]);
    }

    //**************************************************************************//
    // save  attachment of project 
    public function saveAttachments(Request $request, $id)
    {

        $entry = OrganizationProject::findOrFail($id);
        $this->authorize('update', $entry);

        $attachment = new OrganizationProjectAttachments();
        $attachment->project_id = $id;
        $attachment->document_id = $request->document_id;

        if ($attachment->save()) {   
        $entity = OrganizationProjectAttachments::with('file')->findOrFail($attachment->id);

             \Log\Model\Log::saveNewLog('ORGANIZATION_PROJECT_ATTAHMENT_UPLOAD',
                              trans('organization::application.add project attachment') . ' "' . $entry->title . '" ');
            return response()->json(['status' => 'success', 'attachment' => $entity, 'msg' => trans('organization::application.The row is inserted to db')]);
        }

        return response()->json(['status' => 'failed', 'msg' => trans('organization::application.The row is not insert to db')]);
    
    }

    // delete  attachment of project 
    public function deleteAttachments($id, Request $request)
    {
       $entry = OrganizationProjectAttachments::with(['project','file'])->findOrFail($id);
       $title = $entry->project->title;
       $file = $entry->file; 
       \DB::beginTransaction();
       if ($entry->destroy($id)) {
           \Illuminate\Support\Facades\Storage::delete($file->filepath);
            foreach (\Document\Model\FileRevision::allforFile($file->id) as $revision) {
                \Illuminate\Support\Facades\Storage::delete($revision->filepath);
            }
            $file->delete();
           \Log\Model\Log::saveNewLog('ORGANIZATION_PROJECT_ATTAHMENT_DELETE',
                                  trans('organization::application.delete project attachment') . ' "' . $entry->title . '" ');
            \DB::commit();
            return response()->json(['status' => 'success', 'msg' => trans('organization::application.The row is deleted from db')]);
        }
        \DB::rollBack();
        return response()->json(['status' => 'failed', 'msg' => trans('organization::application.The row is not deleted from db')]);
    }
}
