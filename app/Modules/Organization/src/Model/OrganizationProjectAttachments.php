<?php

namespace Organization\Model;

use App\Http\Helpers;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrganizationProjectAttachments extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_organization_projects_attachments';   
    protected $fillable = ['document_id','project_id'];
    public $timestamps = false;
    
    public function project()
    {
        return $this->belongsTo('Organization\Model\OrganizationProject','project_id','id');
    }
    public function file()
    {
        return $this->belongsTo('Document\Model\File','document_id','id');
    }

}

