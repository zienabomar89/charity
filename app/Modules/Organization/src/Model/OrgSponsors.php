<?php

namespace Organization\Model;

class OrgSponsors extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_organization_sponsors';
    protected $fillable = ['organization_id','sponsor_id'];

    public $timestamps = false;

    public function Organization()
    {
        return $this->BelongsTo('Organization\Model\Organization','organization_id', 'id');
    }

    public function Sponsor()
    {
        return $this->BelongsTo('Organization\Model\Organization','organization_id', 'id');
    }


  public static function getOrgSponsors($organization_id)
    {

        $language_id =  \App\Http\Helpers::getLocale();

        $connected = self::where(['organization_id'=>$organization_id])->get();

        $sponsors= \DB::table('char_organizations')->where('char_organizations.type',2)
            ->selectRaw("char_organizations.id as sponsor_id ,
                         CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name ")
            ->orderBy('char_organizations.name')
            ->get();

      if(sizeof($connected) >0 ){
          $selected=[];
          foreach ($connected as $key => $value){
              $selected[]=$value->sponsor_id;
          }
          foreach ($sponsors as $key => $value){
              if(in_array($value->sponsor_id,$selected)){
                  $value->selected = true;
              }else{
                  $value->selected = false;
              }
          }
      }else{

          foreach ($sponsors as $key => $value){
              $value->selected = false;
          }
      }
      return $sponsors;

    }

    public static function getSponsorOrg($sponsor_id)
    {

        $connected = self::where(['sponsor_id'=>$sponsor_id])->get();

          $selected=[];
          if(sizeof($connected) >0 ){
              foreach ($connected as $key => $value){
                  $selected[]=$value->organization_id;
              }

          }

       return $selected;

    }

    public static function getRelatedOrgSponsors($child_id,$parent_id)
    {


        $related= \DB::table('char_organization_sponsors')
            ->join('char_organizations','char_organizations.id',  '=', 'char_organization_sponsors.organization_id')
            ->where('char_organization_sponsors.organization_id',$parent_id)
            ->selectRaw("char_organizations.name,char_organization_sponsors.*")
            ->orderBy('char_organizations.name')
            ->get();


        if(sizeof($related) >0 ) {

            $connected = self::where(['organization_id'=>$child_id])->get();
            $selected=[];
            foreach ($connected as $key => $value){
                $selected[]=$value->sponsor_id;
            }

            if(sizeof($selected) > 0){
                foreach ($related as $key => $value){
                    if(in_array($value->sponsor_id,$selected)){
                        $value->selected = true;
                    }else{
                        $value->selected = false;
                    }
                }
            }else{
                foreach ($related as $key => $value){
                    $value->selected = false;
                }
            }

        }


        return [];


    }


    public static function getConnector($organization_id)
    {
        $connector=[];
        $sponsors = self::where(['organization_id'=>$organization_id])->get();

        if(sizeof($sponsors) >0){
            foreach ($sponsors as $key => $value){
                $connector[]=$value->sponsor_id;
            }

        }
      return $connector;
  }
  public static function getOrgsConnector($Orgs)
    {
        $connector=[];
        $sponsors = self::whereIn('organization_id',$Orgs)->get();

        if(sizeof($sponsors) >0){
            foreach ($sponsors as $key => $value){
                $connector[]=$value->sponsor_id;
            }

        }
      return $connector;
  }
  public static function resetSponsors($organization_id,$sponsors)
    {
        self::where(['organization_id'=>$organization_id])->delete();
        $insert=[];

        foreach ($sponsors as $item){
            $insert[]=[ 'organization_id' => $organization_id ,'sponsor_id' => $item , 'created_at' => date('Y-m-d H:i:s')];
        }

        self::insert($insert);

        return ['status' => true,'msg'=>trans('organization::application.Sponsors have been successfully connect') ];

    }
}

