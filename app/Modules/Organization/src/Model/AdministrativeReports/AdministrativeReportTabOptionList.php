<?php

namespace Organization\Model\AdministrativeReports;

class AdministrativeReportTabOptionList extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_administrative_report_tab_option_list';
    protected $fillable = ['option_id','label', 'value','weight'];
    public $timestamps = false;


}

