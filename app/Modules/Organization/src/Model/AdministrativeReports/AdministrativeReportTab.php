<?php
namespace Organization\Model\AdministrativeReports;

class AdministrativeReportTab extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_administrative_report_tab';
    protected $fillable = ['report_id','title', 'description','order','status','type','tab_order'];
    public $timestamps = false;


    public function options() {
        return $this->hasMany(\Organization\Model\AdministrativeReports\AdministrativeReportTabOption::class, 'tab_id', 'id');
    }
    public function columns() {
        return $this->hasMany(\Organization\Model\AdministrativeReports\AdministrativeReportTabColumns::class, 'tab_id', 'id');
    }
}


