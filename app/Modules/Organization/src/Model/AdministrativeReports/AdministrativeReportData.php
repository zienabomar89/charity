<?php
namespace Organization\Model\AdministrativeReports;

use Setting\Model\Setting;
class AdministrativeReportData extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_administrative_report_data';
    protected $fillable = ['option_id','list_id','organization_id','value','amount','column_id','list_id','column_list_id'];
    public $timestamps = false;


    public static function OrgOptionSum ($organization_id,$column_id){

        $total    = \DB::table('char_administrative_report_data')
                       ->whereRaw("column_id = $column_id")
                       ->whereRaw("organization_id = $organization_id and column_id = $column_id")
                       ->selectRaw("sum(char_administrative_report_data.value) as total")
                       ->first();

        if($total){
            if(!is_null($total->total)){
                return $total->total;
            }
        }
        return '0';
    }

    public static function OrgOptionData ($option_id,$organization_id,$column_id){


        $output = ['value' => null , 'amount' => null];

        if(!is_null($column_id)){
            $return = \DB::table('char_administrative_report_data')
                ->where(function($q) use ($option_id,$organization_id,$column_id) {
                    $q->where('char_administrative_report_data.organization_id',$organization_id);
                    $q->where('char_administrative_report_data.option_id',$option_id);
                    $q->where('char_administrative_report_data.column_id',$column_id);
                })
                ->leftjoin('char_administrative_report_tab_column_list', function($q)  {
                    $q->on('char_administrative_report_tab_column_list.id','=','char_administrative_report_data.column_list_id');
                    $q->on('char_administrative_report_tab_column_list.column_id','=','char_administrative_report_data.column_id');
                })
                ->selectRaw("char_administrative_report_data.amount , 
                                 CASE WHEN char_administrative_report_data.column_list_id is null THEN char_administrative_report_data.value
                                      Else char_administrative_report_tab_column_list.label  END  AS value
                                 ")
                ->first();

            if(!is_null($return)){
                if(!is_null($return->value) && ! ($return->value =='' || $return->value ==' ')){
                    $output['value'] = $return->value;
                }
                if(!is_null($return->amount) && ! ($return->amount =='' || $return->amount ==' ')){
                    $output['amount'] = $return->amount;
                }
            }
        }else{
            $return = \DB::table('char_administrative_report_data')
                ->where(function($q) use ($option_id,$organization_id) {
                    $q->where('char_administrative_report_data.organization_id',$organization_id);
                    $q->where('char_administrative_report_data.option_id',$option_id);
                })
                ->leftjoin('char_administrative_report_tab_option_list', function($q)  {
                    $q->on('char_administrative_report_tab_option_list.id','=','char_administrative_report_data.list_id');
                    $q->on('char_administrative_report_tab_option_list.option_id','=','char_administrative_report_data.option_id');
                })
                ->selectRaw("char_administrative_report_data.amount , 
                                 CASE WHEN char_administrative_report_data.list_id is null THEN char_administrative_report_data.value
                                      Else char_administrative_report_tab_option_list.label  END  AS value
                                 ")
                ->first();


            if(!is_null($return)){
                if(!is_null($return->value) && ! ($return->value =='' || $return->value ==' ')){
                    $output['value'] = $return->value;
                }
                if(!is_null($return->amount) && ! ($return->amount =='' || $return->amount ==' ')){
                    $output['amount'] = $return->amount;
                }
            }
        }
        return $output;
    }

    public static function OrgTabData ($option_id,$report_id){

        $return = \DB::table('char_organizations')
                    ->whereIn('char_organizations.id', function ($q) use ($report_id) {
                        $q->select('char_administrative_report_organization.organization_id')
                            ->from('char_administrative_report_organization')
                            ->where('char_administrative_report_organization.report_id', '=', $report_id);
                    })
                    ->leftjoin('char_administrative_report_data', function($q) use ($option_id) {
                        $q->on('char_administrative_report_data.organization_id','=','char_organizations.id');
                        $q->where('char_administrative_report_data.option_id',$option_id);
})
                    ->leftjoin('char_administrative_report_tab_option_list', function($q)  {
                        $q->on('char_administrative_report_tab_option_list.id','=','char_administrative_report_data.list_id');
                        $q->on('char_administrative_report_tab_option_list.option_id','=','char_administrative_report_data.option_id');
                    })
                    ->selectRaw("char_administrative_report_data.* , 
                                 '$option_id' as option_id ,
                                 char_organizations.id as organization_id ,
                                  char_organizations.name  ,
                                 CASE WHEN char_administrative_report_data.list_id is null THEN char_administrative_report_data.value
                                      Else char_administrative_report_tab_option_list.label  END  AS value
                                  ")
                    ->get();

        return $return;
        return self::query()
            ->join('char_organizations', 'char_organizations.id', '=', 'char_administrative_report_data.organization_id')
            ->leftjoin('char_administrative_report_tab_option_list', function($q)  {
                $q->on('char_administrative_report_tab_option_list.id','=','char_administrative_report_data.list_id');
                $q->on('char_administrative_report_tab_option_list.option_id','=','char_administrative_report_data.option_id');
            })
            ->selectRaw('char_administrative_report_data.* ,  char_organizations.name ,
                                   CASE WHEN char_administrative_report_data.list_id is null THEN char_administrative_report_data.value
                                        Else char_administrative_report_tab_option_list.label  END  AS value
                                  ')
            ->where('char_administrative_report_data.option_id',$id)
//            ->whereIn('char_administrative_report_data.option_id', function ($q) use ($tab_id) {
//                $q->select('char_administrative_report_tab_options.id')->from('char_administrative_report_tab_options')
//                    ->where('char_administrative_report_tab_options.tab_id', '=', $tab_id);
//            })
            ->get();

    }

    public static function getOrgTabData ($tab_id){

        return self::query()
            ->join('char_organizations', 'char_organizations.id', '=', 'char_administrative_report_data.organization_id')
            ->leftjoin('char_administrative_report_tab_option_list', function($q)  {
                $q->on('char_administrative_report_tab_option_list.id','=','char_administrative_report_data.list_id');
                $q->on('char_administrative_report_tab_option_list.option_id','=','char_administrative_report_data.option_id');
            })
            ->selectRaw('char_administrative_report_data.* ,  char_organizations.name ,
                                   CASE WHEN char_administrative_report_data.list_id is null THEN char_administrative_report_data.value
                                        Else char_administrative_report_tab_option_list.label  END  AS value
                                  ')
            ->whereIn('char_administrative_report_data.option_id', function ($q) use ($tab_id) {
                $q->select('char_administrative_report_tab_options.id')->from('char_administrative_report_tab_options')
                    ->where('char_administrative_report_tab_options.tab_id', '=', $tab_id);
            })
            ->count();

    }

    public static function getOrgTabData_ ($organization_id ,$tab_id){

        return self::query()
            ->leftjoin('char_administrative_report_tab_options', function($q)  {
                $q->on('char_administrative_report_tab_options.id','=','char_administrative_report_data.option_id');
            })
            ->leftjoin('char_administrative_report_tab_columns', function($q)  {
                $q->on('char_administrative_report_tab_columns.id','=','char_administrative_report_data.column_id');
            })
            ->leftjoin('char_administrative_report_tab_option_list', function($q)  {
                $q->on('char_administrative_report_tab_option_list.id','=','char_administrative_report_data.list_id');
                $q->on('char_administrative_report_tab_option_list.option_id','=','char_administrative_report_data.option_id');
            })
            ->selectRaw('char_administrative_report_data.* , 
                                   char_administrative_report_tab_options.name,
                                   char_administrative_report_tab_columns.name as col_name,
                                   char_administrative_report_tab_columns.type as col_type,
                                   CASE WHEN char_administrative_report_data.list_id is null THEN char_administrative_report_data.value
                                        Else char_administrative_report_tab_option_list.label  END  AS value
                                  ')

            ->whereIn('char_administrative_report_data.organization_id',$organization_id)
            ->whereIn('char_administrative_report_data.option_id', function ($q) use ($tab_id) {
                $q->select('char_administrative_report_tab_options.id')->from('char_administrative_report_tab_options')
                    ->where('char_administrative_report_tab_options.tab_id', '=', $tab_id);
            })
                ->orderBy('char_administrative_report_data.option_id')
            ->get();

    }

    public static function getOrgCustomTabData ($organization_id ,$tab_id){

        return self::query()
            ->leftjoin('char_administrative_report_tab_options', function($q)  {
                $q->on('char_administrative_report_tab_options.id','=','char_administrative_report_data.option_id');
            })
            ->leftjoin('char_administrative_report_tab_columns', function($q)  {
                $q->on('char_administrative_report_tab_columns.id','=','char_administrative_report_data.column_id');
            })
            ->leftjoin('char_administrative_report_tab_column_list', function($q)  {
                $q->on('char_administrative_report_tab_column_list.id','=','char_administrative_report_data.column_list_id');
                $q->on('char_administrative_report_tab_column_list.column_id','=','char_administrative_report_data.column_id');
            })
            ->selectRaw('char_administrative_report_data.* , 
                                   char_administrative_report_tab_options.name,
                                   char_administrative_report_tab_columns.name as col_name,
                                   char_administrative_report_tab_columns.type as col_type,
                                   CASE WHEN char_administrative_report_data.column_list_id is null THEN char_administrative_report_data.value
                                        Else char_administrative_report_tab_column_list.label  END  AS value
                                  ')

            ->whereIn('char_administrative_report_data.organization_id',$organization_id)
            ->whereIn('char_administrative_report_data.option_id', function ($q) use ($tab_id) {
                $q->select('char_administrative_report_tab_options.id')->from('char_administrative_report_tab_options')
                    ->where('char_administrative_report_tab_options.tab_id', '=', $tab_id);
            })
            ->orderBy('char_administrative_report_data.option_id')
            ->get();

    }

}

