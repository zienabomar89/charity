<?php

namespace Organization\Model\AdministrativeReports;

class AdministrativeReportTabColumnsList extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_administrative_report_tab_column_list';
    protected $fillable = ['column_id','label', 'value','weight'];
    public $timestamps = false;


}

