<?php

namespace Organization\Model\AdministrativeReports;

class AdministrativeReportTabColumns extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_administrative_report_tab_columns';
    protected $fillable = ['tab_id','label','name','type','priority'];
    public $timestamps = false;


    public function optionList() {
        return $this->hasMany(\Organization\Model\AdministrativeReports\AdministrativeReportTabColumnsList::class, 'column_id', 'id');
    }

    public function Options() {
        return $this->hasMany(\Organization\Model\AdministrativeReports\AdministrativeReportTabColumnsList::class, 'column_id', 'id');
    }
    public static function gerTabColumnCount ($tab_id){

        return self::query()
            ->where('tab_id', '=', $tab_id)->count();

    }

}

