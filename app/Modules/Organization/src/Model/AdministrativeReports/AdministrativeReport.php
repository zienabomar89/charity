<?php
namespace Organization\Model\AdministrativeReports;

use Illuminate\Database\Eloquent\SoftDeletes;
use App\Http\Helpers;

class AdministrativeReport  extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_administrative_report';
    protected $fillable = ['user_id','organization_id','title','type','category','date','month','year','date_from','date_to','status','data_status','notes','template'];
    protected $dates = ['deleted_at'];
    protected $hidden = ['created_at','updated_at'];
//    public $timestamps = false;
    use SoftDeletes;


    public function tabs() {
        return $this->hasMany(\Organization\Model\AdministrativeReports\AdministrativeReportTab::class, 'report_id', 'id');
    }


    public function organization() {
        return $this->belongsTo(\Organization\Model\Organization::class, 'organization_id', 'id');
    }

    public function organizations() {
        return $this->hasMany(\Organization\Model\AdministrativeReports\OrgReports::class, 'report_id', 'id');
    }

    public static function filter($options)
    {

        $condition = [];
        $user = \Auth::user();
        $organization_id = $user->organization_id;
//        'user_id',
        $filters = ['title','type','category','data_status','status','notes','month','year'];

        foreach ($options as $key => $value) {
            if(in_array($key,$filters)) {
                if ($value != "" ) {
                    $data = ['char_administrative_report.'.$key => (int) $value];
                    array_push($condition,$data);
                }
            }
        }

        $query = self::query()
            ->join('char_organizations','char_organizations.id',  '=', 'char_administrative_report.organization_id')
        ;
        if($options['action'] == 'excel') {
            $query = $query->with(['tabs','tabs.options']);
        }
//        ->where('user_id','=',$user->id)
        $query->whereNull('char_administrative_report.deleted_at');


        $all_organization=1;
        if(isset($options['all_organization'])){
            if($options['all_organization'] ===true){
                $all_organization=0;
            }
        }

        if($all_organization ==0){

            $organizations=[];
            if(isset($options['organization_ids']) && $options['organization_ids'] !=null && $options['organization_ids'] !="" && $options['organization_ids'] !=[] && sizeof($options['organization_ids']) != 0) {
                if($options['organization_ids'][0]==""){
                    unset($options['organization_ids'][0]);
                }
                $organizations =$options['organization_ids'];
            }

            if(!empty($organizations)){
                $query->wherein('char_administrative_report.organization_id',$organizations);
            }else{
                $user = \Auth::user();
                $UserType=$user->type;

                if($UserType == 2) {
                    $query->where(function ($anq) use ($organization_id,$user) {
                        $anq->where('char_administrative_report.organization_id',$organization_id);
                        $anq->orwherein('char_administrative_report.organization_id', function($quy) use($user) {
                            $quy->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });

                }else{
                    if (!$user->super_admin) {
                        $query->where(function ($anq) use ($organization_id) {
                            /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                            $anq->wherein('char_administrative_report.organization_id', function($qery) use($organization_id) {
                                $qery->select('descendant_id')
                                    ->from('char_organizations_closure')
                                    ->where('ancestor_id', '=', $organization_id);
                            });
                        });
                    }else{
//                        $query->where(function ($anq) use ($organization_id) {
//                            /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
//                            $anq->wherein('char_administrative_report.organization_id', function($qery) use($organization_id) {
//                                $qery->select('descendant_id')
//                                    ->from('char_organizations_closure')
//                                    ->where('ancestor_id', '=', $organization_id);
//                            });
//                        });
                    }
                }
            }
        }
        elseif($all_organization ==1){
            $query->where('char_administrative_report.organization_id',$organization_id);

        }


        $begin_date_from=null;
        $begin_date_to=null;

        if(isset($options['begin_date_from']) && $options['begin_date_from'] !=null){
            $begin_date_from=date('Y-m-d',strtotime($options['begin_date_from']));
        }
        if(isset($options['begin_date_to']) && $options['begin_date_to'] !=null){
            $begin_date_to=date('Y-m-d',strtotime($options['begin_date_to']));
        }

        if($begin_date_from != null && $begin_date_to != null) {
            $query =  $query->whereBetween( 'char_administrative_report.date_from', [ $begin_date_from, $begin_date_to]);
        }elseif($begin_date_from != null && $begin_date_to == null) {
            $query =  $query->whereDate('char_administrative_report.date_from', '>=', $begin_date_from);
        }elseif($begin_date_from == null && $begin_date_to != null) {
            $query =  $query->whereDate('char_administrative_report.date_from', '<=', $begin_date_to);
        }

        $end_date_from=null;
        $end_date_to=null;


        if(isset($options['end_date_from']) && $options['end_date_from'] !=null){
            $end_date_from=date('Y-m-d',strtotime($options['end_date_from']));
        }

        if(isset($options['end_date_to']) && $options['end_date_to'] !=null){
            $end_date_to=date('Y-m-d',strtotime($options['end_date_to']));
        }

        if($end_date_from != null && $end_date_to != null) {
            $query =  $query->whereBetween( 'char_administrative_report.date_to', [ $end_date_from, $end_date_to]);
        }elseif($end_date_from != null && $end_date_to == null) {
            $query =  $query->whereDate('char_administrative_report.date_to', '>=', $end_date_from);
        }elseif($end_date_from == null && $end_date_to != null) {
            $query =  $query->whereDate('char_administrative_report.date_to', '<=', $end_date_to);
        }


        $created_at_to=null;
        $created_at_from=null;
        if(isset($options['created_at_from']) && $options['created_at_from'] !=null){
            $created_at_from=date('Y-m-d',strtotime($options['created_at_from']));
        }
        if(isset($options['created_at_to']) && $options['created_at_to'] !=null){
            $created_at_to=date('Y-m-d',strtotime($options['created_at_to']));
        }

        if($created_at_from != null && $created_at_to != null) {
            $query =  $query->whereBetween('char_administrative_report.created_at', [ $created_at_from, $created_at_to]);
        }elseif($created_at_from != null && $created_at_to == null) {
            $query =  $query->whereDate('char_administrative_report.created_at', '>=', $created_at_from);
        }elseif($created_at_from == null && $created_at_to != null) {
            $query =  $query->whereDate('char_administrative_report.created_at', '<=', $created_at_to);
        }

        if (count($condition) != 0) {
            $query =  $query->where(function ($q) use ($condition) {
                $names = ['title', 'notes'];
                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $names)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });
        }

        $Annual = trans('organization::application.Annual');
        $mid_Annual = trans('organization::application.mid Annual');
        $quarter_Annual = trans('organization::application.quarter Annual');
        $monthly = trans('organization::application.monthly');
        $urgent = trans('organization::application.urgent');
        $center = trans('organization::application.center');
        $custom = trans('organization::application.custom');

        $language_id =  \App\Http\Helpers::getLocale();
        if($options['action'] =='paginate') {
            $query->orderBy('char_administrative_report.created_at','desc');
            $query->selectRaw("char_administrative_report.* ,
                                       CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name,
                                       CASE WHEN char_administrative_report.organization_id = '$organization_id' THEN 1 Else 0  END  AS is_mine,
                                       CASE WHEN char_administrative_report.type is null THEN '-'
                                           WHEN char_administrative_report.type = 1 THEN '$Annual'
                                           WHEN char_administrative_report.type = 2 THEN '$mid_Annual'
                                           WHEN char_administrative_report.type = 3 THEN '$quarter_Annual'
                                           WHEN char_administrative_report.type = 4 THEN '$monthly'
                                           WHEN char_administrative_report.type = 5 THEN '$urgent'
                                      END  AS report_type ,
                                       CASE WHEN char_administrative_report.category is null THEN '-'
                                           WHEN char_administrative_report.category = 1 THEN '$center'
                                           WHEN char_administrative_report.category = 2 THEN '$custom'
                                      END  AS report_category                                 
                                     ");

            $itemsCount = isset($options['itemsCount'])? $options['itemsCount']:0;
            $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
            return $query=$query->paginate($records_per_page);
        }else{
            if(isset($options['items'])){
              if(sizeof($options['items']) > 0 ){
                $query->whereIn('char_administrative_report.id',$options['items']);  
             }      
        } 
        }

           
        $query->orderBy('char_administrative_report.created_at','desc');
        $query->selectRaw("char_administrative_report.*");
        $list = $query->get();

        if(sizeof($list)>0){
            $ids =[];
            foreach ($list as $k1=>$report) {
                $ids[]=$report->id;
            }
            return self::query()->with(['tabs','organization','tabs.columns','tabs.options'])->whereIn('id',$ids)->get();
        }
        return  [] ;

    }

    public static function OrgReport($options)
    {

        $condition = [];
        $language_id =  \App\Http\Helpers::getLocale();
        $user = \Auth::user();
        $organization_id = $user->organization_id;
        $UserType=$user->type;
//        'user_id',
        $filters = ['title','type','category','data_status','status','notes','month','year','report_organization_status'];

        foreach ($options as $key => $value) {
            if(in_array($key,$filters)) {

                if ($value != "" ) {
                    if($key == 'report_organization_status'){
                        $data = ['char_administrative_report_organization.status'  => (int) $value];
                    }else{
                        $data = ['char_administrative_report.' . $key => (int) $value];
                    }

                    array_push($condition,$data);
                }
            }
        }

        $query=\DB::table('char_administrative_report')
            ->join('char_organizations','char_organizations.id',  '=', 'char_administrative_report.organization_id')
            ->join('char_administrative_report_organization as report_organization', function($q) use ($organization_id) {
                $q->on('report_organization.report_id','=','char_administrative_report.id');
//                $q->where('report_organization.organization_id','=',$organization_id);
            })
            ->whereNull('char_administrative_report.deleted_at');


        $all_organization=1;
        if(isset($options['all_organization'])){
            if($options['all_organization'] ===true){
                $all_organization=0;
            }
        }

        if($all_organization ==0){

            $organizations=[];
            if(isset($options['organization_ids']) && $options['organization_ids'] !=null && $options['organization_ids'] !="" && $options['organization_ids'] !=[] && sizeof($options['organization_ids']) != 0) {
                if($options['organization_ids'][0]==""){
                    unset($options['organization_ids'][0]);
                }
                $organizations =$options['organization_ids'];
            }

            if(!empty($organizations)){

                $query->where(function ($anq) use ($organizations,$user) {
                    $anq->wherein('report_organization.organization_id',$organizations);
                    $anq->wherein('char_administrative_report.report_id', function($quy) use($user) {
                        $quy->select('report_id')
                            ->from('char_administrative_report_organization')
                            ->where('user_id', '=', $user->id);
                    });

                });

            }else{
                if($UserType == 2) {
                    $query->where(function ($anq) use ($organization_id,$user) {
                        $anq->where('report_organization.organization_id',$organization_id);
                        $anq->orwherein('report_organization.organization_id', function($quy) use($user) {
                            $quy->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                        $anq->wherein('char_administrative_report.report_id', function($quy) use($user) {
                            $quy->select('report_id')
                                ->from('char_administrative_report_organization')
                                ->where('user_id', '=', $user->id);
                        });
                    });

                }
                else{
                    if (!$user->super_admin) {
                        $query->where(function ($anq) use ($organization_id,$user) {
                            /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                            $anq->wherein('report_organization.organization_id', function($qery) use($organization_id) {
                                $qery->select('descendant_id')
                                    ->from('char_organizations_closure')
                                    ->where('ancestor_id', '=', $organization_id);
                            });
                            $anq->wherein('char_administrative_report.report_id', function($quy) use($user) {
                                $quy->select('report_id')
                                    ->from('char_administrative_report_organization')
                                    ->where('user_id', '=', $user->id);
                            });
                        });
                    }else{
//                        $query->where(function ($anq) use ($organization_id) {
//                            /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
//                            $anq->wherein('char_administrative_report.organization_id', function($qery) use($organization_id) {
//                                $qery->select('descendant_id')
//                                    ->from('char_organizations_closure')
//                                    ->where('ancestor_id', '=', $organization_id);
//                            });
//                        });
                    }
                }
            }


        }
        elseif($all_organization ==1){
            $query->where(function ($anq) use ($organization_id,$user) {
                $anq->where('report_organization.organization_id',$organization_id);
                $anq->where('report_organization.user_id',$user->id);

            });
        }


        //        'date','date_from','date_to'
        $begin_date_from=null;
        $end_date_from=null;


        if(isset($options['begin_date_from']) && $options['begin_date_from'] !=null){
            $begin_date_from=date('Y-m-d',strtotime($options['begin_date_from']));
        }

        if(isset($options['end_date_from']) && $options['end_date_from'] !=null){
            $end_date_from=date('Y-m-d',strtotime($options['end_date_from']));
        }


        if($begin_date_from != null && $end_date_from != null) {
            $query->whereBetween( 'date_from', [ $begin_date_from, $end_date_from]);
        }elseif($begin_date_from != null && $end_date_from == null) {
            $query->whereDate('date_from', '>=', $begin_date_from);
        }elseif($begin_date_from == null && $end_date_from != null) {
            $query->whereDate('date_from', '<=', $end_date_from);
        }

        $begin_date_to=null;
        $end_date_to=null;

        if(isset($options['end_date_to']) && $options['end_date_to'] !=null){
            $end_date_to=date('Y-m-d',strtotime($options['end_date_to']));
        }

        if(isset($options['begin_date_to']) && $options['begin_date_to'] !=null){
            $begin_date_to=date('Y-m-d',strtotime($options['begin_date_to']));
        }

        if($begin_date_to != null && $end_date_to != null) {
            $query->whereBetween( 'date_from', [ $begin_date_to, $end_date_to]);
        }elseif($begin_date_to != null && $end_date_to == null) {
            $query->whereDate('date_from', '>=', $begin_date_to);
        }elseif($begin_date_to == null && $end_date_to != null) {
            $query->whereDate('date_from', '<=', $end_date_to);
        }


        $created_at_to=null;
        $created_at_from=null;
        if(isset($options['created_at_from']) && $options['created_at_from'] !=null){
            $created_at_from=date('Y-m-d',strtotime($options['created_at_from']));
        }
        if(isset($options['created_at_to']) && $options['created_at_to'] !=null){
            $created_at_to=date('Y-m-d',strtotime($options['created_at_to']));
        }

        if($created_at_from != null && $created_at_to != null) {
            $query =  $query->whereBetween('char_administrative_report.created_at', [ $created_at_from, $created_at_to]);
        }elseif($created_at_from != null && $created_at_to == null) {
            $query =  $query->whereDate('char_administrative_report.created_at', '>=', $created_at_from);
        }elseif($created_at_from == null && $created_at_to != null) {
            $query =  $query->whereDate('char_administrative_report.created_at', '<=', $created_at_to);
        }

        if (count($condition) != 0) {
            $query =  $query->where(function ($q) use ($condition) {
                $names = ['char_administrative_report.title', 'char_administrative_report.notes'];
                for ($i = 0; $i < count($condition); $i++) {
                    foreach ($condition[$i] as $key => $value) {
                        if(in_array($key, $names)) {
                            $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                        } else {
                            $q->where($key, '=', $value);
                        }
                    }
                }
            });
        }

        $query->groupBy('char_administrative_report.id');
        $query->orderBy('char_administrative_report.created_at','desc');

        $Annual = trans('organization::application.Annual');
        $mid_Annual = trans('organization::application.mid Annual');
        $quarter_Annual = trans('organization::application.quarter Annual');
        $monthly = trans('organization::application.monthly');
        $urgent = trans('organization::application.urgent');
        $center = trans('organization::application.center');
        $custom = trans('organization::application.custom');

        if($options['action'] =='export') {
            return $query->selectRaw("char_administrative_report.title AS report_title ,
                                                                             CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name,
                                      CASE WHEN char_administrative_report.type is null THEN '-'
                                           WHEN char_administrative_report.type = 1 THEN '$Annual'
                                           WHEN char_administrative_report.type = 2 THEN '$mid_Annual'
                                           WHEN char_administrative_report.type = 3 THEN '$quarter_Annual'
                                           WHEN char_administrative_report.type = 4 THEN '$monthly'
                                           WHEN char_administrative_report.type = 5 THEN '$urgent'
                                      END  AS report_type,
                                      char_administrative_report.date,
                                      char_administrative_report.date_from,
                                      char_administrative_report.date_to,
                                      char_administrative_report.notes,
                                       CASE WHEN category is null THEN '-'
                                           WHEN category = 1 THEN '$center'
                                           WHEN category = 2 THEN '$custom'
                                      END  AS report_category                                    
                                     ")
                ->get();
        }
        else{
            $query->selectRaw("char_administrative_report.* ,
                              report_organization.status as report_organization_status,
                              report_organization.id as report_organization_id,
                               CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name,
                               CASE WHEN char_administrative_report.type is null THEN '-'
                                           WHEN char_administrative_report.type = 1 THEN '$Annual'
                                           WHEN char_administrative_report.type = 2 THEN '$mid_Annual'
                                           WHEN char_administrative_report.type = 3 THEN '$quarter_Annual'
                                           WHEN char_administrative_report.type = 4 THEN '$monthly'
                                           WHEN char_administrative_report.type = 5 THEN '$urgent'
                                      END  AS report_type          ,
                                       CASE WHEN category is null THEN '-'
                                           WHEN category = 1 THEN '$center'
                                           WHEN category = 2 THEN '$custom'
                                      END  AS report_category                        
                                     ");
        }

        $itemsCount = isset($options['itemsCount'])? $options['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$query->count());
        return $query=$query->paginate($records_per_page);


    }

}
