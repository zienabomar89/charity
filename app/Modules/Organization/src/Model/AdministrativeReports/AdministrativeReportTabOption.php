<?php

namespace Organization\Model\AdministrativeReports;

class AdministrativeReportTabOption extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_administrative_report_tab_options';
    protected $fillable = ['tab_id','label','name','type'];
    public $timestamps = false;


    public function optionList() {
        return $this->hasMany(\Organization\Model\AdministrativeReports\AdministrativeReportTabOptionList::class, 'option_id', 'id');
    }

    public function Options() {
        return $this->hasMany(\Organization\Model\AdministrativeReports\AdministrativeReportTabOptionList::class, 'option_id', 'id');
    }


    public static function gerTabOptionCount ($tab_id){

        return self::query()
            ->where('tab_id', '=', $tab_id)->count();

    }

}

