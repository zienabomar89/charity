<?php

namespace Organization\Model\AdministrativeReports;

class OrgReports extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_administrative_report_organization';
    protected $fillable = ['organization_id','user_id','report_id','status'];

    public $timestamps = false;

    public function organization() {
        return $this->hasMany(\Organization\Model\Organization::class, 'organization_id', 'id');
    }

    public static function getReportOrg ($report_id,$org){

        $language_id =  \App\Http\Helpers::getLocale();

        $organizations = \DB::table('char_organizations') ;
        if(sizeof($org) > 0 ){
            $organizations->whereIn('char_organizations.id', $org);
        }else{
            $organizations->whereIn('char_organizations.id', function($query) use($report_id) {
                $query->select('organization_id')
                    ->from('char_administrative_report_organization')
                    ->where('report_id', '=', $report_id);
            });
        }

        return    $organizations->selectRaw("char_organizations.id,CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name")
                   ->get();
     }

    public static function getReportOrgRelated ($report_id,$org){

        $language_id =  \App\Http\Helpers::getLocale();
        $user = \Auth::user();
        $organization_id=$user->organization_id;

        return \DB::table('char_organizations')
                  ->where(function ($anq_) use ($organization_id,$user,$report_id , $org) {
                      if(sizeof($org) > 0 ){
                          $anq_->whereIn('char_organizations.id', $org);
                      }else{
                          $anq_->whereIn('char_organizations.id', function($query) use($report_id) {
                              $query->select('organization_id')
                                  ->from('char_administrative_report_organization')
                                  ->where('report_id', '=', $report_id);
                          });

                          if($user->type == 2) {
                              $anq_->where(function ($anq) use ($organization_id,$user) {
                                  $anq->where('char_organizations.id',$organization_id);
                                  $anq->orwherein('char_organizations.id', function($quy) use($user) {
                                      $quy->select('organization_id')
                                          ->from('char_user_organizations')
                                          ->where('user_id', '=', $user->id);
                                  });
                              });
                          }
                          else{
                              if (!$user->super_admin) {
                                  $anq_->where(function ($anq) use ($organization_id) {
                                      $anq->where('char_organizations.id',$organization_id);
                                      $anq->orwherein('char_organizations.id', function($qery) use($organization_id) {
                                          $qery->select('descendant_id')
                                              ->from('char_organizations_closure')
                                              ->where('ancestor_id', '=', $organization_id);
                                      });
                                  });
                              }
                          }
                      }
                  })
                  ->selectRaw("char_organizations.id,CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name")
                  ->get();
     }

    public static function getOrgReportsCount ($report_id){

        return self::query()
            ->where('report_id', '=', $report_id)->count();

    }

    public static function getReportOrgs ($report_id){

        $query = self::query()->where('report_id', '=', $report_id)->get();

        $return = array();

        foreach ($query as $key=>$value){
            $return[] = (int) $value->organization_id;
        }

        return $return;

    }

     public static function getReportData ($report_id){

         $language_id =  \App\Http\Helpers::getLocale();
         return \DB::table('char_organizations')
                     ->wherein('char_organizations.id', function ($q) use ($report_id) {
                       $q->select('organization_id')
                           ->from('char_administrative_report_organization')
                           ->where('report_id', '=', $report_id);
                   })->selectRaw("char_organizations.id ,
                                   CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name")->get();
    }

    public static function getReportOrgsDetail ($report_id){

        $language_id =  \App\Http\Helpers::getLocale();

        $report = AdministrativeReport::with(['tabs','tabs.options','tabs.options.optionList'])->findOrFail($report_id);
        $options = array();
        foreach ($report->tabs as $key=>$value){
            foreach ($value->options as $k=>$v){
                $options[] = $v['id'];
            }
        }
        $comma_separated = implode(",", $options);


        $query = \DB::table('char_administrative_report_organization')
                    ->join('char_organizations as org','org.id',  '=', 'char_administrative_report_organization.organization_id')
                    ->leftjoin('char_users','char_users.id',  '=', 'char_administrative_report_organization.user_id')
                    ->where('report_id', '=', $report_id)
                    ->selectRaw("char_administrative_report_organization.*,
                                 CASE  WHEN $language_id = 1 THEN org.name Else org.en_name END  AS organization_name,
                                 org.level  AS organization_level,
                                 org.type  AS organization_type,
                                 CONCAT(ifnull(char_users.firstname, ' '), ' ' ,ifnull(char_users.lastname, ' '))as user_name,
                                 get_org_report_data_count(org.id ,'$comma_separated') as has_data
                                ")
                    ->get();


//        foreach ($query as $k_=>$v_){
//            $v_->has_data =  AdministrativeReportData::where(function ($q) use ($v_,$options){
//                $q->where ('organization_id',$v_->organization_id);
//                $q->whereIn ('option_id',$options);
//            })->count();
//        }

        return $query;

    }

    public static function getReportRelatedOrg ($report_id){

        $user = \Auth::user();
        $organization_id=$user->organization_id;
        $query = self::query()
            ->where(function ($anq_) use ($organization_id,$user,$report_id) {
                $anq_->where('char_administrative_report_organization.report_id', '=', $report_id);
                if($user->type == 2) {
                    $anq_->where(function ($anq) use ($organization_id,$user) {
                        $anq->where('char_administrative_report_organization.organization_id',$organization_id);
                        $anq->orwherein('char_administrative_report_organization.organization_id', function($quy) use($user) {
                            $quy->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });

                }
                else{
                    if (!$user->super_admin) {
                        $anq_->where(function ($anq) use ($organization_id) {
                            $anq->where('char_administrative_report_organization.organization_id',$organization_id);
                            $anq->orwherein('char_administrative_report_organization.organization_id', function($qery) use($organization_id) {
                                $qery->select('descendant_id')
                                    ->from('char_organizations_closure')
                                    ->where('ancestor_id', '=', $organization_id);
                            });
                        });
                    }
                }
            })->get();





        $return = array();

        foreach ($query as $key=>$value){
            $return[] = (int) $value->organization_id;
        }

        return $return;

    }

    public static function getReportRelatedOrgDetail ($report_id){

        $language_id =  \App\Http\Helpers::getLocale();
        $user = \Auth::user();
        $organization_id=$user->organization_id;

        $report = AdministrativeReport::with(['tabs','tabs.options','tabs.options.optionList'])->findOrFail($report_id);
        $options = array();
        foreach ($report->tabs as $key=>$value){
            foreach ($value->options as $k=>$v){
                $options[] = $v['id'];
            }
        }
        $comma_separated = implode(",", $options);


        $query = \DB::table('char_administrative_report_organization')
            ->join('char_organizations as org','org.id',  '=', 'char_administrative_report_organization.organization_id')
            ->where(function ($anq_) use ($organization_id,$user,$report_id) {
                $anq_->where('char_administrative_report_organization.report_id', '=', $report_id);
                if($user->type == 2) {
                    $anq_->where(function ($anq) use ($organization_id,$user) {
                        $anq->where('char_administrative_report_organization.organization_id',$organization_id);
                        $anq->orwherein('char_administrative_report_organization.organization_id', function($quy) use($user) {
                            $quy->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });

                }
                else{
                    if (!$user->super_admin) {
                        $anq_->where(function ($anq) use ($organization_id) {
                            $anq->where('char_administrative_report_organization.organization_id',$organization_id);
                            $anq->orwherein('char_administrative_report_organization.organization_id', function($qery) use($organization_id) {
                                $qery->select('descendant_id')
                                    ->from('char_organizations_closure')
                                    ->where('ancestor_id', '=', $organization_id);
                            });
                        });
                    }
                }
            })
            ->selectRaw("char_administrative_report_organization.*,
                                 CASE  WHEN $language_id = 1 THEN org.name Else org.en_name END  AS organization_name,
                                 get_org_report_data_count(org.id ,'$comma_separated') as has_data
                                ")
            ->orderBy('org.name','desc')
            ->get();


//        foreach ($query as $k_=>$v_){
//            $v_->has_data =  AdministrativeReportData::where(function ($q) use ($v_,$options){
//                $q->where ('organization_id',$v_->organization_id);
//                $q->whereIn ('option_id',$options);
//            })->count();
//        }

        return $query;

    }

}

