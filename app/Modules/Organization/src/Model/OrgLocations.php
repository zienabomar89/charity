<?php

namespace Organization\Model;

use Common\Model\Transfers\Transfers;
use Setting\Model\aidsLocation;
use Common\Model\CaseModel;

class OrgLocations extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_organization_locations';
    protected $fillable = ['organization_id','location_id'];

    public $timestamps = false;

    public function Organization()
    {
        return $this->BelongsTo('Organization\Model\Organization','organization_id', 'id');
    }

    public function Location()
    {
        return $this->BelongsTo('Setting\Model\Location','location_id', 'id');
    }

    public static function getOrgLocations($organization_id)
    {
        return self::where(['organization_id'=>$organization_id])->get();
    }

    public static function getConnected($organization_id)
    {
        $connected=[];
        $locations =self::where(['organization_id'=>$organization_id])->get();

        foreach ($locations as $key=>$value){
            $connected[]= $value['location_id'];
        }
        return $connected;
    }

    public static function resetLocations($organization_id,$locations)
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $user = \Auth::user();

        self::where(['organization_id'=>$organization_id])->delete();

        if(sizeof($locations) > 0){
            $fullTree = aidsLocation::getFullTree($locations);
            $insert=[];
            foreach ($fullTree as $value){
                $insert[]=[ 'organization_id' => $organization_id ,'location_id' => $value , 'created_at' => date('Y-m-d H:i:s')];
            }

            if(sizeof($insert) > 0){
                self::insert($insert);
            }

            $persons = \DB::table('char_cases')
                        ->join('char_persons', 'char_cases.person_id', '=', 'char_persons.id')
                        ->selectRaw("CONCAT(ifnull(char_persons.first_name, ' '), ' ' ,
                                             ifnull(char_persons.second_name, ' '),' ',
                                             ifnull(char_persons.third_name, ' '),' ', 
                                             ifnull(char_persons.last_name,' ')) AS name,                                            
                                             char_persons.adscountry_id, 
                                             char_persons.adsdistrict_id,
                                             char_persons.adsmosques_id,
                                             char_persons.adsneighborhood_id,
                                             char_persons.adsregion_id,
                                             char_persons.adssquare_id,
                                             char_cases.id,char_cases.person_id,char_cases.category_id")
                        ->where(function ($m) use ($fullTree,$organization_id){
                                $m->where(function ($w9) use ($fullTree,$organization_id){
                                    $w9->where('status', 0);
                                    $w9->where('organization_id', $organization_id);
                                    $w9->whereIn('category_id',function ($wi){
                                        $wi->select('id')
                                            ->from('char_categories')
                                            ->where('type', 2);
                                    });
                                });

                                $m->where(function ($wo) use ($fullTree){
                                    $wo->whereNull('adsmosques_id');
                                    $wo->orWhereNotIn('adsmosques_id',$fullTree);
                                });
                          })
                          ->get();

            foreach ($persons as $key => $value){
                $transferObj =  Transfers::where(['person_id'=> $value->person_id,'category_id' => $value->category_id,
                                                'prev_adscountry_id'=> $value->adscountry_id, 'prev_adsdistrict_id'=> $value->adsdistrict_id,
                                                'prev_adsregion_id'=> $value->adsregion_id, 'prev_adsneighborhood_id'=> $value->adsneighborhood_id,
                                                'prev_adssquare_id'=> $value->adssquare_id, 'prev_adsmosques_id'=> $value->adsmosques_id,
                                                'adscountry_id'=> $value->adscountry_id, 'adsdistrict_id'=> $value->adsdistrict_id,
                                                'adsregion_id'=> $value->adsregion_id, 'adsneighborhood_id'=> $value->adsneighborhood_id,
                                                'adssquare_id'=> $value->adssquare_id, 'adsmosques_id'=> $value->adsmosques_id])->first();

                if(!$transferObj){
                    Transfers::create(['created_at' => date('Y-m-d H:i:s'), 'person_id'=> $value->person_id ,
                        'prev_adscountry_id'=> $value->adscountry_id, 'prev_adsdistrict_id'=> $value->adsdistrict_id,
                        'prev_adsregion_id'=> $value->adsregion_id, 'prev_adsneighborhood_id'=> $value->adsneighborhood_id,
                        'prev_adssquare_id'=> $value->adssquare_id, 'prev_adsmosques_id'=> $value->adsmosques_id,

                        'adscountry_id'=> $value->adscountry_id, 'adsdistrict_id'=> $value->adsdistrict_id,
                        'adsregion_id'=> $value->adsregion_id, 'adsneighborhood_id'=> $value->adsneighborhood_id,
                        'adssquare_id'=> $value->adssquare_id, 'adsmosques_id'=> $value->adsmosques_id,
                        'category_id' => $value->category_id, 'user_id'=> $user->id,
                        'organization_id' => $user->organization_id]);

                    CaseModel::where(['id' => $value->id ])->update(['status' =>1]);
                    $action='CASE_UPDATED';

                    $message=trans('common::application.edited data') . '  : '.' "'.$value->name. ' " ';

                    \Log\Model\Log::saveNewLog($action,$message);
                    \Common\Model\CasesStatusLog::create(['case_id'=>$value->id, 'user_id'=>$user->id,
                        'reason'=>trans('common::application.Disabled your recorded status for') . ' "'.
                            $value->name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization'),
                        'status'=>1, 'date'=>date("Y-m-d")]);

                    \Log\Model\Log::saveNewLog('CASE_UPDATED',trans('common::application.Disabled your recorded status for') . ' "'.$value->name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization'));

                }
            }

        }

        return ['status' => true ,'msg'=>trans('organization::application.Locations have been successfully connect') ];

    }

    public static function LocationOrgs($location,$person_id,$category_id)
    {

        $query = self::where(['location_id'=>$location])
                     ->whereNotin('organization_id', function($subQuery)use($person_id,$category_id){
                        $subQuery->select('char_cases.organization_id')
                            ->where('char_cases.person_id','=', $person_id )
                            ->where('char_cases.category_id','=', $category_id )
                            ->whereNull('deleted_at')
                            ->from('char_cases');
                    })
                    ->get();

        $return=[];
        if(sizeof($query) > 0){
            foreach ($query as $key => $value){
                $return[]= $value->organization_id;
            }
        }
        return $return;

    }
}

