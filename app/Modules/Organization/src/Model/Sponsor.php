<?php

namespace Organization\Model;

class Sponsor extends AbstractOrganization
{
    protected $type = AbstractOrganization::TYPE_SPONSOR;
}
