<?php

namespace Organization\Model;

class Organization extends AbstractOrganization
{
    protected $type = AbstractOrganization::TYPE_ORGANIZATION;

    public static function getAncestor($organization_id)
    {
            $ancestor_id= \DB::select("SELECT a.ancestor_id FROM char_organizations_closure a
                                       WHERE a.depth = (SELECT max(b.depth) FROM char_organizations_closure b WHERE b.descendant_id = a.descendant_id)
                                       AND(a.descendant_id = '$organization_id')");
            return $ancestor_id[0]->ancestor_id;

    }

    public static function getMobileNumbers($cont_type,$source ,$target)
    {

        $user = \Auth::user();

        $query = \DB::table('char_organizations');

        if($cont_type == 0 || $cont_type == '0'){
            $query->selectRaw("char_organizations.name,char_organizations.wataniya as mobile");
        }else{
            $query->selectRaw("char_organizations.name,char_organizations.mobile");
        }

        $query->where(function ($q) use ($user,$source,$target) {
                     $q->where('type', 1);
                     $q->where('id', '!=', $user->organization_id);

                        
                    if($source == 'project'){
                        $q->whereIn('id', function ($q) use ($target) {
                                    $q->select('organization_id')
                                      ->from('aid_projects_organizations')
                                      ->where( function ($qi) use ($target) {
                                           $qi->where('ratio', '!=', 0)
                                                   ->whereIn('project_id', $target);
                                      });
                           });
                    }elseif($source == 'central_project'){
                        if($user->type == 2){
                            $q->whereIn('id', function($quer) use($user) {
                                            $quer->select('organization_id')
                                                 ->from('char_user_organizations')
                                                 ->where('user_id', '=', $user->id);
                               });
                         }
                         else{
                            $q->whereIn('id', function($query) use($user) {
                                          $query->select('descendant_id')
                                                ->from('char_organizations_closure')
                                                ->where('ancestor_id', '=', $user->organization_id);
                               });
                         }
                        $q->whereIn('id', function ($q) use ($target) {
                                    $q->select('organization_id')
                                      ->from('aid_projects_organizations')
                                      ->where( function ($qi) use ($target) {
                                           $qi->where('ratio', '!=', 0)->whereIn('project_id', $target);
                                      });
                            });
                    }else{
                              $q->whereIn('id',$target);
                    }
         });
        

        return $query->get();

    }


}
