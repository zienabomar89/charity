<?php

namespace Organization\Model;

use App\Http\Helpers;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrganizationProject extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'char_organization_projects';
    protected $fillable = ['organization_id' ,'user_id' ,'sub_category_id','title', 'description' ,'date' ,'type_id','distribute','category_id','activity_category_id'  ,'amount',
                           'currency_id', 'date_from' ,'date_to'  ,'region' ,'target_group', 'count' , 'voucher_id',
                           'beneficiary_category_id' ,'sponsor_id' ,'country_id' ,'shared' ,'shared_organizations' ,
                          'has_sponsor','sponsor_organizations', 'notes'];
    protected $hidden = ['created_at','updated_at'];
    protected $dates = ['deleted_at'];
    use SoftDeletes;

    public function attachments()
    {
        return $this->hasMany('Organization\Model\OrganizationProjectAttachments','project_id','id');
    }
    
    public  static function fetch($id)
    {

        $language_id = Helpers::getLocale();

        $yes = trans('aid::application.yes');
        $no = trans('aid::application.no');

        $financial = trans('organization::application.financial');
        $non_financial = trans('organization::application.non_financial');

        return \DB::table('char_organization_projects')
                ->leftjoin('char_vouchers', function ($join) {
                    $join->on('char_vouchers.id', '=', 'char_organization_projects.voucher_id')
                        ->whereNull('char_vouchers.deleted_at');
                })
//            ->leftjoin('char_vouchers', 'char_vouchers.id', '=', 'char_organization_projects.voucher_id')
            ->join('char_organizations', 'char_organizations.id', '=', 'char_organization_projects.organization_id')
            ->leftjoin('char_organizations as sponsor', 'sponsor.id', '=', 'char_organization_projects.sponsor_id')
            ->join('char_users', 'char_users.id', '=', 'char_organization_projects.user_id')
            ->join('char_currencies', 'char_currencies.id', '=', 'char_organization_projects.currency_id')
            ->join('char_project_category_i18n', function ($join) use ($language_id) {
                $join->on('char_project_category_i18n.category_id', '=', 'char_organization_projects.category_id')
                    ->where('char_project_category_i18n.language_id', $language_id);
            })
            ->join('char_project_category_i18n as sub_category_i18n', function ($join) use ($language_id) {
                $join->on('sub_category_i18n.category_id', '=', 'char_organization_projects.sub_category_id')
                    ->where('sub_category_i18n.language_id', $language_id);
            })
            ->join('char_project_type_i18n', function ($join) use ($language_id) {
                $join->on('char_project_type_i18n.type_id', '=', 'char_organization_projects.type_id')
                    ->where('char_project_type_i18n.language_id', $language_id);
            })
            ->join('char_project_activities_category_i18n', function ($join) use ($language_id) {
                $join->on('char_project_activities_category_i18n.activity_category_id', '=', 'char_organization_projects.activity_category_id')
                    ->where('char_project_activities_category_i18n.language_id', $language_id);
            })
            ->join('char_project_regions_i18n', function ($join) use ($language_id) {
                $join->on('char_project_regions_i18n.region_id', '=', 'char_organization_projects.region')
                    ->where('char_project_regions_i18n.language_id', $language_id);
            })
            ->join('char_project_beneficiary_category_i18n', function ($join) use ($language_id) {
                $join->on('char_project_beneficiary_category_i18n.beneficiary_category_id', '=', 'char_organization_projects.beneficiary_category_id')
                    ->where('char_project_beneficiary_category_i18n.language_id', $language_id);
            })
            ->leftjoin('char_locations_i18n', function ($join) use ($language_id) {
                $join->on('char_organization_projects.country_id', '=', 'char_locations_i18n.location_id')
                    ->where('char_locations_i18n.language_id', $language_id);
            })
            ->where('char_organization_projects.id', $id)
            ->selectRaw("char_organization_projects.title AS title,
                                      char_organization_projects.description  AS description,
                                      char_organization_projects.voucher_id  AS voucher_id,
                                      char_vouchers.title  AS voucher,
                                      char_organization_projects.date AS date,
                                      CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name,
                                      char_project_type_i18n.name as project_type,
                                      char_project_category_i18n.name as project_category,
                                      sub_category_i18n.name as project_sub_category,
                                      CASE
                                             WHEN char_organization_projects.distribute = 1 THEN '$financial'
                                             WHEN char_organization_projects.distribute != 1 THEN '$non_financial'
                                       END AS distribute ,
                                      char_project_activities_category_i18n.name as activity_category,
                                      char_organization_projects.amount  AS amount,
                                      CASE  WHEN $language_id = 1 THEN char_currencies.name Else char_currencies.en_name END AS currency_name,
                                      char_organization_projects.date_from  AS date_from,
                                      char_organization_projects.date_to  AS date_to,           
                                      char_project_regions_i18n.name as region,
                                      char_organization_projects.target_group,
                                      char_organization_projects.count,
                                      char_organization_projects.shared as shared_,
                                      char_organization_projects.has_sponsor as has_sponsor_,
                                      char_project_beneficiary_category_i18n.name as beneficiary_category,

                                      CASE  WHEN $language_id = 1 THEN sponsor.name Else sponsor.en_name END  AS sponsor_name,
                                      char_locations_i18n.name as country_name ,
                                      CASE
                                             WHEN char_organization_projects.shared = 1 THEN '$yes'
                                             WHEN char_organization_projects.shared != 1 THEN '$no'
                                       END AS shared ,
                                       CASE
                                             WHEN char_organization_projects.shared = 1 THEN char_organization_projects.shared_organizations
                                             WHEN char_organization_projects.shared != 1 THEN ' '
                                       END AS shared_organizations ,
                                       CASE
                                             WHEN char_organization_projects.has_sponsor = 1 THEN '$yes'
                                             WHEN char_organization_projects.has_sponsor != 1 THEN '$no'
                                       END AS has_sponsor ,
                                       CASE
                                             WHEN char_organization_projects.has_sponsor = 1 THEN char_organization_projects.sponsor_organizations
                                             WHEN char_organization_projects.has_sponsor != 1 THEN ' '
                                       END AS sponsor_organizations ,
                                      char_organization_projects.notes,
                                      CONCAT(char_users.firstname,' ',char_users.lastname)as username,
                                      char_organization_projects.created_at       ,                             
                                      char_organization_projects.deleted_at                                    
                                      ")->first();

    }

    public  static function filter($options)
    {


        $condition = [];
        $user = \Auth::user();
        $UserType=$user->type;
        $organization_id=$user->organization_id;
        $UserOrg_=$user->organization;
        $organization_id=$user->organization_id;
        $UserOrgType=$UserOrg_->type;



        $language_id =  \App\Http\Helpers::getLocale();
//        'organization_id' ,
        $char_organization_projects =['user_id' ,'activity_category_id','currency_id',
                                      'beneficiary_category_id' ,'sponsor_id' ,'country_id','type_id','distribute','category_id',
                                      'title','description','amount','region','target_group','count' ,
                                      'shared' ,'shared_organizations' , 'notes','has_sponsor','sponsor_organizations'];

        foreach ($options as $key => $value) {
            if(in_array($key, $char_organization_projects)) {
                if ( $value != "" ) {
                    $data = ['char_organization_projects.'. $key => $value];
                    array_push($condition, $data);
                }
            }
        }


        $yes = trans('aid::application.yes');
        $no = trans('aid::application.no');

        $financial = trans('organization::application.financial');
        $non_financial = trans('organization::application.non_financial');

        $query= \DB::table('char_organization_projects')
                    ->leftjoin('char_vouchers', function ($join) {
                        $join->on('char_vouchers.id', '=', 'char_organization_projects.voucher_id')
                            ->whereNull('char_vouchers.deleted_at');
                    })
                    ->join('char_organizations','char_organizations.id',  '=', 'char_organization_projects.organization_id')
                    ->leftjoin('char_organizations as sponsor','sponsor.id',  '=', 'char_organization_projects.sponsor_id')
                    ->join('char_users','char_users.id',  '=', 'char_organization_projects.user_id')
                    ->join('char_currencies', 'char_currencies.id', '=', 'char_organization_projects.currency_id')
                    ->join('char_project_category_i18n', function($join)  use ($language_id) {
                        $join->on('char_project_category_i18n.category_id', '=','char_organization_projects.category_id' )
                            ->where('char_project_category_i18n.language_id',$language_id);
                    })
                    ->join('char_project_category_i18n as sub_category_i18n', function($join)  use ($language_id) {
                        $join->on('sub_category_i18n.category_id', '=','char_organization_projects.sub_category_id' )
                            ->where('sub_category_i18n.language_id',$language_id);
                    })
                    ->join('char_project_type_i18n', function($join)  use ($language_id) {
                        $join->on('char_project_type_i18n.type_id', '=','char_organization_projects.type_id' )
                            ->where('char_project_type_i18n.language_id',$language_id);
                    })
                    ->join('char_project_activities_category_i18n', function($join)  use ($language_id) {
                        $join->on('char_project_activities_category_i18n.activity_category_id', '=','char_organization_projects.activity_category_id' )
                            ->where('char_project_activities_category_i18n.language_id',$language_id);
                    })
                    ->join('char_project_regions_i18n', function($join)  use ($language_id) {
                        $join->on('char_project_regions_i18n.region_id', '=','char_organization_projects.region' )
                            ->where('char_project_regions_i18n.language_id',$language_id);
                    })
                    ->join('char_project_beneficiary_category_i18n', function($join)  use ($language_id) {
                        $join->on('char_project_beneficiary_category_i18n.beneficiary_category_id', '=','char_organization_projects.beneficiary_category_id' )
                            ->where('char_project_beneficiary_category_i18n.language_id',$language_id);
                    })
                    ->leftjoin('char_locations_i18n', function($join)  use ($language_id) {
                        $join->on('char_organization_projects.country_id', '=','char_locations_i18n.location_id' )
                            ->where('char_locations_i18n.language_id',$language_id);
                    });

        if (count($condition) != 0) {
            $query =  $query
                ->where(function ($q) use ($condition) {
                    $str = ['char_organization_projects.title','char_organization_projects.description',
                            'char_organization_projects.target_group', 'char_organization_projects.shared_organizations',
                            'char_organization_projects.notes','char_organization_projects.sponsor_organizations'];

                    for ($i = 0; $i < count($condition); $i++) {
                        foreach ($condition[$i] as $key => $value) {
                            if(in_array($key, $str)) {
                                $q->whereRaw("normalize_text_ar($key) like  normalize_text_ar(?)", "%".$value."%");
                            } else {
                                $q->where($key, '=', $value);
                            }
                        }
                    }
                });
        }

//        has_voucher

        $has_voucher=null;
        if(isset($options['has_voucher']) && $options['has_voucher'] !=null && $options['has_voucher'] !=""){
            $has_voucher=$options['has_voucher'];
        }

        if($has_voucher != null){
            if($has_voucher == 0){
                $query->whereNull('char_organization_projects.voucher_id');
            }
            elseif($has_voucher == 1){
                $query->whereNotNull('char_organization_projects.voucher_id');
            }
        }

        
        $all_organization=null;
        if(isset($options['all_organization'])){
            if($options['all_organization'] ===true){
                $all_organization=0;
            }elseif($options['all_organization'] ===false){
                $all_organization=1;

            }
        }

        if($all_organization ==0){

            $organizations=[];
            if(isset($options['organization_ids']) && $options['organization_ids'] !=null && $options['organization_ids'] !="" && $options['organization_ids'] !=[] && sizeof($options['organization_ids']) != 0) {
                if($options['organization_ids'][0]==""){
                    unset($options['organization_ids'][0]);
                }
                $organizations =$options['organization_ids'];
            }

            if(!empty($organizations)){
                $query->wherein('char_organization_projects.organization_id',$organizations);
            }else{
                if($UserType == 2) {
                    $query->where(function ($anq) use ($organization_id,$user) {
                        $anq->where('char_organization_projects.organization_id',$organization_id);
                        $anq->orwherein('char_organization_projects.organization_id', function($quer) use($user) {
                            $quer->select('organization_id')
                                ->from('char_user_organizations')
                                ->where('user_id', '=', $user->id);
                        });
                    });
                }else{

                    $query->where(function ($anq) use ($organization_id) {
                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                        $anq->wherein('char_organization_projects.organization_id', function($quersy) use($organization_id) {
                            $quersy->select('descendant_id')
                                ->from('char_organizations_closure')
                                ->where('ancestor_id', '=', $organization_id);
                        });
                    });
                }
            }
        }
        elseif($all_organization ==1){
            $query->where('char_organization_projects.organization_id',$organization_id);

        }


        $date_to=null;
        $date_from=null;

        if(isset($options['date_to']) && $options['date_to'] !=null){
            $date_to=date('Y-m-d',strtotime($options['date_to']));
        }
        if(isset($options['date_from']) && $options['date_from'] !=null){
            $date_from=date('Y-m-d',strtotime($options['date_from']));
        }
        if($date_from != null && $date_to != null) {
            $query->whereDate('char_organization_projects.date', '>=', $date_from);
            $query->whereDate('char_organization_projects.date', '<=', $date_to);
        }elseif($date_from != null && $date_to == null) {
            $query->whereDate('char_organization_projects.date', '>=', $date_from);
        }elseif($date_from == null && $date_to != null) {
            $query->whereDate('char_organization_projects.date', '<=', $date_to);
        }


        $begin_date_from=null;
        $end_date_from=null;

        if(isset($options['begin_date_from']) && $options['begin_date_from'] !=null){
            $begin_date_from=date('Y-m-d',strtotime($options['begin_date_from']));
        }

        if(isset($options['end_date_from']) && $options['end_date_from'] !=null){
            $end_date_from=date('Y-m-d',strtotime($options['end_date_from']));
        }

        if($begin_date_from != null && $end_date_from != null) {
            $query->whereBetween( 'char_organization_projects.date_from', [ $begin_date_from, $end_date_from]);
        }elseif($begin_date_from != null && $end_date_from == null) {
            $query->whereDate('char_organization_projects.date_from', '>=', $begin_date_from);
        }elseif($begin_date_from == null && $end_date_from != null) {
            $query->whereDate('char_organization_projects.date_from', '<=', $end_date_from);
        }

        $begin_date_to=null;
        $end_date_to=null;

        if(isset($options['end_date_to']) && $options['end_date_to'] !=null){
            $end_date_to=date('Y-m-d',strtotime($options['end_date_to']));
        }

        if(isset($options['begin_date_to']) && $options['begin_date_to'] !=null){
            $begin_date_to=date('Y-m-d',strtotime($options['begin_date_to']));
        }

        if($begin_date_to != null && $end_date_to != null) {
            $query->whereBetween( 'char_organization_projects.date_from', [ $begin_date_to, $end_date_to]);
        }elseif($begin_date_to != null && $end_date_to == null) {
            $query->whereDate('char_organization_projects.date_from', '>=', $begin_date_to);
        }elseif($begin_date_to == null && $end_date_to != null) {
            $query->whereDate('char_organization_projects.date_from', '<=', $end_date_to);
        }

        $created_at_to=null;
        $created_at_from=null;
        if(isset($options['created_at_from']) && $options['created_at_from'] !=null){
            $created_at_from=date('Y-m-d',strtotime($options['created_at_from']));
        }
        if(isset($options['created_at_to']) && $options['created_at_to'] !=null){
            $created_at_to=date('Y-m-d',strtotime($options['created_at_to']));
        }

        if($created_at_from != null && $created_at_to != null) {
            $query->whereBetween('char_organization_projects.created_at', [ $created_at_from, $created_at_to]);
        }elseif($created_at_from != null && $created_at_to == null) {
            $query->whereDate('char_organization_projects.created_at', '>=', $created_at_from);
        }elseif($created_at_from == null && $created_at_to != null) {
            $query->whereDate('char_organization_projects.created_at', '<=', $created_at_to);
        }

        $deleted_at_to=null;
        $deleted_at_from=null;
        if(isset($options['deleted_at_from']) && $options['deleted_at_from'] !=null){
            $deleted_at_from=date('Y-m-d',strtotime($options['deleted_at_from']));
        }
        if(isset($options['deleted_at_to']) && $options['deleted_at_to'] !=null){
            $deleted_at_to=date('Y-m-d',strtotime($options['deleted_at_to']));
        }

        if($deleted_at_from != null && $deleted_at_to != null) {
            $query->whereBetween('char_organization_projects.deleted_at', [ $deleted_at_from, $deleted_at_to]);
        }elseif($deleted_at_from != null && $deleted_at_to == null) {
            $query->whereDate('char_organization_projects.deleted_at', '>=', $deleted_at_from);
        }elseif($deleted_at_from == null && $deleted_at_to != null) {
            $query->whereDate('char_organization_projects.deleted_at', '<=', $deleted_at_to);
        }


        if(isset($options['trashed'])){
            if($options['trashed'] == true || $options['trashed'] == 'true'){
                $query->whereNotNull('char_organization_projects.deleted_at');
//                $query->onlyTrashed();
            }else{
                $query->whereNull('char_organization_projects.deleted_at');
            }

        }else{
            $query->whereNull('char_organization_projects.deleted_at');
        }

        $map = [
            "organization_name" => 'char_organizations.name',
            "sponsor_name" => 'sponsor.name',
            "username" => 'char_users.firstname',
            "currency_name" => 'char_currencies.name',
            "activity_category" => 'char_project_activities_category_i18n.name',
            "beneficiary_category" => 'char_project_beneficiary_category_i18n.name',
            "title" => 'char_organization_projects.title',
            "description" => 'char_organization_projects.description',
            "amount" => 'char_organization_projects.amount',
            "count" => 'char_organization_projects.count',
            "region" => 'char_organization_projects.region',
            "deleted_at" => 'char_organization_projects.deleted_at',
            "project_date" => 'char_organization_projects.date',
            "beneficiary_count" => 'char_organization_projects.count',
            "date_from" => 'char_organization_projects.date_from',
            "date_to" => 'char_organization_projects.date_to',
            "target_group" => 'char_organization_projects.target_group',
            "shared_organizations" => 'char_organization_projects.shared_organizations',
            "sponsor_organizations" => 'char_organization_projects.sponsor_organizations',
            "notes" => 'char_organization_projects.notes'
        ];

        $order = false;
        if (isset($options['sortKeyArr_rev']) && $options['sortKeyArr_rev'] != null && $options['sortKeyArr_rev'] != "" &&
            $options['sortKeyArr_rev'] != [] && sizeof($options['sortKeyArr_rev']) != 0) {

            $order = true;
            foreach ($options['sortKeyArr_rev'] as $key_) {
                $query->orderBy($map[$key_],'desc');
            }

        }

        if (isset($options['sortKeyArr_un_rev']) && $options['sortKeyArr_un_rev'] != null && $options['sortKeyArr_un_rev'] != "" &&
            $options['sortKeyArr_un_rev'] != [] && sizeof($options['sortKeyArr_un_rev']) != 0
        ) {

            $order = true;
            foreach ($options['sortKeyArr_un_rev'] as $key_) {
                $query->orderBy($map[$key_],'asc');
            }

        }

        if(!$order){
            $query->orderBy('char_organization_projects.created_at','desc');
        }

        if($options['action'] =='excel') {
            if(isset($options['items'])){
                if(sizeof($options['items']) > 0 ){
                  $query->whereIn('char_organization_projects.id',$options['items']);  
              }
              
            }
            return $query->selectRaw("char_organization_projects.title as project_title,
                                      char_organization_projects.description  as project_description,
                                      char_vouchers.title  AS voucher,
                                      char_organization_projects.date as project_date,
                                      CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name,
                                      char_project_type_i18n.name as project_type,
                                      char_project_category_i18n.name as project_category,
                                      sub_category_i18n.name as project_sub_category,
                                      CASE
                                             WHEN char_organization_projects.distribute = 1 THEN '$financial'
                                             WHEN char_organization_projects.distribute != 1 THEN '$non_financial'
                                       END AS distribute ,
                                      char_project_activities_category_i18n.name as activity_category,
                                      char_organization_projects.amount  as project_amount,
                                      CASE  WHEN $language_id = 1 THEN char_currencies.name Else char_currencies.en_name END AS currency_name,
                                      char_organization_projects.date_from  as project_date_from,
                                      char_organization_projects.date_to  as project_date_to,           
                                      char_project_regions_i18n.name as project_region,
                                      char_organization_projects.target_group,
                                      char_organization_projects.count beneficiary_count,
                                      char_project_beneficiary_category_i18n.name as beneficiary_category,

                                      CASE  WHEN $language_id = 1 THEN sponsor.name Else sponsor.en_name END  AS sponsor_name,
                                      char_locations_i18n.name as sponsor_country ,
                                      CASE
                                             WHEN char_organization_projects.shared = 1 THEN '$yes'
                                             WHEN char_organization_projects.shared != 1 THEN '$no'
                                       END AS shared_project ,
                                       CASE
                                             WHEN char_organization_projects.shared = 1 THEN char_organization_projects.shared_organizations
                                             WHEN char_organization_projects.shared != 1 THEN ' '
                                       END AS shared_organizations ,
                                       CASE
                                             WHEN char_organization_projects.has_sponsor = 1 THEN '$yes'
                                             WHEN char_organization_projects.has_sponsor != 1 THEN '$no'
                                       END AS has_sponsor ,
                                       CASE
                                             WHEN char_organization_projects.has_sponsor = 1 THEN char_organization_projects.sponsor_organizations
                                             WHEN char_organization_projects.has_sponsor != 1 THEN ' '
                                       END AS sponsor_organizations ,                                       
                                      char_organization_projects.notes,
                                      CONCAT(char_users.firstname,' ',char_users.lastname)as username,
                                      char_organization_projects.created_at       ,                             
                                      char_organization_projects.deleted_at                                    
                                      ")->get();

        }

        $paginate = $query->selectRaw("char_organization_projects.id,
                                      char_organization_projects.title,
                                      char_organization_projects.description,
                                       char_organization_projects.voucher_id  AS voucher_id,
                                      char_vouchers.title  AS voucher,
                                            char_organization_projects.date as project_date,
                                      CASE WHEN char_organization_projects.organization_id = '$organization_id' THEN 1 Else 0  END  AS is_mine,
                                      CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS organization_name,
                                      char_project_type_i18n.name as project_type,
                                      char_project_category_i18n.name as project_category,
                                      CASE
                                             WHEN char_organization_projects.distribute = 1 THEN '$financial'
                                             WHEN char_organization_projects.distribute != 1 THEN '$non_financial'
                                       END AS distribute ,
                                      char_project_activities_category_i18n.name as activity_category,
                                      char_organization_projects.amount,
                                      char_organization_projects.currency_id,
                                      CASE  WHEN $language_id = 1 THEN char_currencies.name Else char_currencies.en_name END AS currency_name,
                                      char_organization_projects.date_from,
                                      char_organization_projects.date_to,
                                      char_project_regions_i18n.name as region,
                                      char_organization_projects.target_group,
                                      char_organization_projects.count beneficiary_count,
                                      char_organization_projects.sponsor_id,
                                      char_project_beneficiary_category_i18n.name as beneficiary_category,
                                      CASE  WHEN $language_id = 1 THEN sponsor.name Else sponsor.en_name END  AS sponsor_name,
                                      char_locations_i18n.name as sponsor_country ,
                                      CASE
                                             WHEN char_organization_projects.shared = 1 THEN '$yes'
                                             WHEN char_organization_projects.shared != 1 THEN '$no'
                                       END AS shared_project ,
                                       CASE
                                             WHEN char_organization_projects.shared = 1 THEN char_organization_projects.shared_organizations
                                             WHEN char_organization_projects.shared != 1 THEN ' '
                                       END AS shared_organizations ,
                                       CASE
                                             WHEN char_organization_projects.has_sponsor = 1 THEN '$yes'
                                             WHEN char_organization_projects.has_sponsor != 1 THEN '$no'
                                       END AS has_sponsor ,
                                       CASE
                                             WHEN char_organization_projects.has_sponsor = 1 THEN char_organization_projects.sponsor_organizations
                                             WHEN char_organization_projects.has_sponsor != 1 THEN ' '
                                       END AS sponsor_organizations ,
                                      char_organization_projects.notes,
                                      CONCAT(char_users.firstname,' ',char_users.lastname)as username,
                                      char_organization_projects.created_at ,
                                      char_organization_projects.deleted_at                                    
                                      ");

        $itemsCount = isset($options['itemsCount'])?$options['itemsCount']:0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$paginate->count());
        $paginate = $paginate->paginate($records_per_page);
        return $paginate;

    }


}

