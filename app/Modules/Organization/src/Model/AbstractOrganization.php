<?php

namespace Organization\Model;

use Illuminate\Database\Eloquent\SoftDeletes;
//use Franzose\ClosureTable\Models\Entity;
use Illuminate\Database\Eloquent\Model;

abstract class AbstractOrganization extends Model
{
    use SoftDeletes;
    
    const STATUS_ACTIVE   = 1;
    const STATUS_INACTIVE = 2;
    const STATUS_CLOSED   = 3;
    const STATUS_DELETED  = 4;
    
    const TYPE_ORGANIZATION = 1;
    const TYPE_SPONSOR      = 2;

    const LEVEL_MASTER_CENTER = 1;
    const LEVEL_BRANCH_CENTER = 2;
    const LEVEL_BRANCHES = 3;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'char_organizations';

    /**
     * ClosureTable model instance.
     *
     * @var OrganizationClosure
     */
    protected $closure = 'Organization\Model\OrganizationClosure';
    protected $fillable = ['logo', 'type', 'category_id', 'country', 'name', 'district_id', 'city_id', 'location_id','level','en_name','email',
                           'mobile','wataniya','phone','fax','container_share','registration_number'];
    protected $dates = ['deleted_at'];
    protected $hidden = ['created_at', 'updated_at'];
    
    protected $appends = ['status_name', 'type_name',  'level_name', 'status_options', 'type_options', 'level_options'];
    
    /**
     * Scope a query to only children.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeDescendants($query, $ancestor_id, $depth = null)
    {
        return $query->whereIn('id', function($query) use($ancestor_id, $depth) {
            $query->select('descendant_id')
                  ->from('char_organizations_closure')
                  ->where('ancestor_id', '=', $ancestor_id);
            if (null !== $depth) {
                $query->where('depth', '=', $depth);
            }
        });
    }

    public function isValid($attributes, $rules) {
        $this->validator = \Validator::make($attributes, $rules);
        return $this->validator->passes();
    }

    public function getMessages() {
        return $this->validator->messages();
    }

    public function category() {
        return $this->belongsTo('Setting\Model\OrganizationsCategoryI18n', 'category_id', 'category_id');
    }
    public function Locations() {
        return $this->belongsTo('Setting\Model\LocationsI18n', 'country', 'location_id');
    }

    public function district() {
        return $this->belongsTo('Setting\Model\LocationsI18n', 'district_id', 'location_id');
    }
    public function city() {
        return $this->belongsTo('Setting\Model\LocationsI18n', 'city_id', 'location_id');
    }

    public function nearLocation() {
        return $this->belongsTo('Setting\Model\LocationsI18n', 'location_id', 'location_id');
    }

    public function descendants_() {
        return $this->hasMany('Organization\Model\Organization', 'parent_id', 'id');
    }

    public function parent() {
        return $this->belongsTo('Organization\Model\Organization', 'parent_id', 'id');
    }

    public function customforms() {
        return $this->hasMany('Forms\Model\CustomForms', 'organization_id', 'id');
    }

    public function getOrglist($page) {

        $STATUS_ACTIVE   = trans('organization::application.active');
        $STATUS_INACTIVE = trans('organization::application.inactive');
        $STATUS_CLOSED   = trans('organization::application.closed');
        $STATUS_DELETED  = trans('organization::application.deleted');

        $language_id =  \App\Http\Helpers::getLocale();

        return \DB::table('char_organizations')
                ->join('char_locations_i18n As l', function($join) use ($language_id) {
                    $join->on('l.location_id', '=', 'char_organizations.country' )
                        ->where('l.language_id',$language_id);
                })
                ->SelectRaw("char_organizations.id as id ,
                             char_organizations.name as name ,
                             char_organizations.logo as logo,
                             char_organizations.registration_number as registration_number,
                             CASE WHEN char_organizations.status = 1 THEN '$STATUS_ACTIVE'
                               WHEN char_organizations.status = 2 THEN '$STATUS_INACTIVE'
                               WHEN char_organizations.status = 3 THEN '$STATUS_CLOSED'
                               WHEN char_organizations.status = 4 THEN '$STATUS_DELETED'
                        END) AS status,
                        CASE WHEN char_organizations.country is null THEN '-' Else l.name  END  AS country")
                ->where('deleted_at', '=', null)
                ->where('type', '=', 1)
                ->paginate(config('constants.records_per_page'));
    }
    
    public static function status($value = null)
    {
        $options = array(
            self::STATUS_ACTIVE => trans('organization::application.active'),
            self::STATUS_INACTIVE => trans('organization::application.inactive'),
            self::STATUS_CLOSED => trans('organization::application.closed'),
            self::STATUS_DELETED => trans('organization::application.deleted'),
        );
        
        if (null === $value) {
            return $options;
        }
        
        if(array_key_exists($value, $options)) {
            return $options[$value];
        }
    }

    public static function level($value = null)
    {
        $options = array(
            self::LEVEL_MASTER_CENTER => trans('organization::application.master_center'),
            self::LEVEL_BRANCH_CENTER => trans('organization::application.branch_center'),
            self::LEVEL_BRANCHES => trans('organization::application.branches'),
        );

        if (null === $value) {
            return $options;
        }

        if(array_key_exists($value, $options)) {
            return $options[$value];
        }
    }
    
    public function getStatus()
    {
        return self::status($this->status);
    }

    public function getLevel()
    {
        return self::level($this->level);
    }
    
    public static function type($value = null)
    {
        $options = array(
            self::TYPE_ORGANIZATION => trans('organization::application.organization'),
            self::TYPE_SPONSOR => trans('organization::application.sponsor'),
        );
        
        if (null === $value) {
            return $options;
        }
        
        if(array_key_exists($value, $options)) {
            return $options[$value];
        }
    }
    
    public function getType()
    {
        return self::type($this->type);
    }
    
    public function getStatusNameAttribute()
    {
        return $this->getStatus();
    }
    
    public function getTypeNameAttribute()
    {
        return $this->getType();
    }

    public function getLevelNameAttribute()
    {
        return $this->getLevel();
    }

    public function getStatusOptionsAttribute()
    {
        return self::status();
    }
    
    public function getTypeOptionsAttribute()
    {
        return self::type();
    }

    public function getLevelOptionsAttribute()
    {
        return self::level();
    }


    public static function getAncestor($organization_id)
    {
        $ancestor_id= \DB::select("SELECT a.ancestor_id FROM char_organizations_closure a
                                       WHERE a.depth = (SELECT max(b.depth) FROM char_organizations_closure b WHERE b.descendant_id = a.descendant_id)
                                       AND(a.descendant_id = '$organization_id')");
        return $ancestor_id[0]->ancestor_id;

    }
}
