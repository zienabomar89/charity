<?php

namespace App\Modules\Organization;

class ServiceProvider extends \App\Providers\AuthServiceProvider
{
    
    protected $policies = [
        \Organization\Model\Organization::class => \Organization\Policy\OrganizationPolicy::class,
        \Organization\Model\Sponsor::class => \Organization\Policy\SponsorPolicy::class,
        \Organization\Model\AdministrativeReports\AdministrativeReport::class => \Organization\Policy\AdministrativeReportPolicy::class,
        \Organization\Model\OrganizationProject::class => \Organization\Policy\OrganizationProjectPolicy::class,
    ];

    public function boot()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/config/routes.php';
        }
        $this->loadViewsFrom(__DIR__ . '/views', 'organization');
        $this->loadTranslationsFrom(__DIR__.'/languages', 'organization');
        
        require __DIR__ . '/config/autoload.php';
        
        $this->registerPolicies();
    }

    public function register()
    {
        /*$this->mergeConfigFrom(
            __DIR__ . '/config/config.php', 'user'
        );*/
    }

}
