<?php

$loader = new Composer\Autoload\ClassLoader();
$loader->addPsr4('Organization\\', __DIR__ . '/../src');
$loader->register();