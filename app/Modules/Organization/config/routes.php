<?php

Route::get('/org/logo', 'Organization\Controller\OrganizationsController@thumbnail');

Route::group(['middleware' => ['api','lang','auth.check'], 'prefix' => 'api/v1.0/org'  ,'namespace' => 'Organization\Controller'], function() {

    Route::group(['prefix' => 'reports','namespace' => 'reports'], function() {

        Route::post('usersList','reportsController@usersList');
        Route::post('setUser','reportsController@setUser');

        Route::post('template-upload','reportsController@templateUpload');
        Route::resource('options', 'OptionsController');
        Route::resource('columns', 'ColumnsController');

        Route::get('Org', 'reportsController@Org');
        Route::get('tabs/columns/{id}', 'TabController@columns');
        Route::get('tabs/elements/{id}', 'TabController@elements');
        Route::resource('tabs', 'TabController');

        Route::post('OrgStatus', 'reportsController@OrgStatus');
        Route::post('status', 'reportsController@status');

        Route::post('wordOrg', 'reportsController@wordOrg');
        Route::get('wordRelated/{id}', 'reportsController@wordRelated');
        Route::get('word/{id}', 'reportsController@word');

        Route::post('excelOrg', 'reportsController@excelOrg');
        Route::get('excelRelated/{id}', 'reportsController@excelRelated');
        Route::get('excel/{id}', 'reportsController@excel');

        Route::get('getTargetOrgData/{id}', 'reportsController@getTargetOrgData');
        Route::get('related/{id}', 'reportsController@related');
        Route::get('organizations/{id}', 'reportsController@organizations');
        Route::get('instruction/{id}', 'reportsController@instruction');
        Route::get('getOrgData/{id}', 'reportsController@getOrgData');
        Route::post('OrgData', 'reportsController@OrgData');
        Route::post('OrgReport', 'reportsController@OrgReport');
        Route::post('filter', 'reportsController@filter');
    });

    Route::resource('reports', 'reports\reportsController');

    Route::group(['prefix' => 'organizations'], function() {
        Route::post('paginate', 'OrganizationsController@paginate');
        Route::post('filter', 'OrganizationsController@filter');
        Route::get('Org', 'OrganizationsController@orgs');
        Route::get('all', 'OrganizationsController@all');
        Route::get('compositeList', 'OrganizationsController@compositeCollection');
        Route::get('list', 'OrganizationsController@collection');
        Route::post('trash', 'OrganizationsController@trash');
        Route::put('restore/{id}', 'OrganizationsController@restore');
        Route::get('descendants', 'OrganizationsController@descendants');
        Route::put('repository-share', 'OrganizationsController@updateRepositoryShare');
        Route::post('logo-upload', 'OrganizationsController@logoUpload');
        Route::post('template-upload', 'OrganizationsController@templateUpload');

    });

    Route::get('organizations/{id}/logo', 'OrganizationsController@logo');
    Route::get('organizations/{id}/template', 'OrganizationsController@template');
    Route::put('organizations/{id}/resetLocations', 'OrganizationsController@resetLocations');
    Route::get('organizations/{id}/getLocations', 'OrganizationsController@getLocations');
    Route::put('organizations/{id}/resetSponsors', 'OrganizationsController@resetSponsors');
    Route::get('organizations/{id}/containerShare', 'OrganizationsController@containerShare');
    Route::get('organizations/{id}/getSponsors', 'OrganizationsController@getSponsors');
    Route::post('organizations/ListOfCategory', 'OrganizationsController@ListOfCategory');
    Route::resource('organizations', 'OrganizationsController');

    Route::group(['prefix' => 'sponsors'], function() {
        Route::post('paginate', 'SponsorsController@paginate');
        Route::get('list', 'SponsorsController@collection');
        Route::post('logo-upload', 'SponsorsController@logoUpload');
        Route::put('restore/{id}', 'SponsorsController@restore');
        Route::post('trash', 'SponsorsController@trash');
        Route::post('template-upload', 'SponsorsController@templateUpload');
    });

    Route::get('sponsors/{id}/logo', 'SponsorsController@logo');
    Route::get('sponsors/{id}/template', 'SponsorsController@template');
    Route::resource('sponsors', 'SponsorsController');

    Route::get('projects/deleteAttachments/{id}', 'OrganizationProjectsController@deleteAttachments');
    Route::put('projects/saveAttachments/{id}', 'OrganizationProjectsController@saveAttachments');
    Route::get('projects/attachments/{id}', 'OrganizationProjectsController@attachments');
    Route::get('projects/fetch/{id}', 'OrganizationProjectsController@fetch');
    Route::put('projects/restore/{id}', 'OrganizationProjectsController@restore');
    Route::post('projects/filter', 'OrganizationProjectsController@filter');
    Route::resource('projects', 'OrganizationProjectsController');
    
    Route::group(['prefix' => 'sponsor','namespace' => 'Sponsor'], function() {
        
        Route::group(['prefix' => 'sponsorships'], function() {
            Route::post('persons', 'SponsorshipsController@persons');
            Route::post('person', 'SponsorshipsController@person');
            Route::post('beneficiaries', 'SponsorshipsController@beneficiaries');
            Route::get('beneficiary/{id}', 'SponsorshipsController@beneficiary');
        });
        Route::post('payments', 'SponsorshipsController@payments');
        
        Route::group(['prefix' => 'aids'], function() {
            Route::post('persons', 'AidsController@persons');
            Route::post('person', 'AidsController@person');
            Route::post('beneficiaries', 'AidsController@beneficiaries');
        });
        Route::post('vouchers', 'AidsController@vouchers');
        
    });


});
