<?php

Route::group(['middleware' => ['api','lang','auth.check'], 'prefix' => 'api/v1.0/dashboard' ], function() {
    Route::get('stats', '\Dashboard\Controller\IndexController@index');
});

?>