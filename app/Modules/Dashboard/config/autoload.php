<?php

$loader = new Composer\Autoload\ClassLoader();
$loader->addPsr4('Dashboard\\', __DIR__ . '/../src');
$loader->register();