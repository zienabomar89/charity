<?php

namespace Dashboard\Controller;

use App\Http\Controllers\Controller;
use Auth\Model\Setting;
use Illuminate\Support\Facades\Auth;

use Log\Model\InternalMessageReceipt;
use Organization\Model\Organization;
use Organization\Model\OrgLocations;
use Setting\Model\aidsLocation;
use Setting\Model\aidsLocationI18n;
use App\Http\Helpers;
use Common\Model\Person;
use Common\Model\AidsCases;


class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // get dashboard content
    public function index()
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit', '2048M');

        $user = Auth::user();
        $request = new \Illuminate\Http\Request();
        if($request->LogIn != 'true' || $request->LogIn != true){
            \Log\Model\Log::saveNewLog('USER_LOG_IN',trans('dashboard::application.The user is logged on to the system'));
        }

        $stats = [
            'unread_message' => InternalMessageReceipt::unReadCount(),
            'transfers' => $this->getTransfersStats($user),
            'sponsorship' => $this->getSponsorshipStats($user),
            'aid' => $this->getAidStats($user),
        ];
        return response()->json($stats);
    }

    // get sponsorship statistic to dashboard content
    protected function getSponsorshipStats($user)
    {

        $data = [
            'candidate' => 0,
            'send' => 0,
            'sponsored' => 0,
            'stoped' => 0,
            'suspended' => 0,
        ];
//        return $data;

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit', '2048M');

        $query = \Sponsorship\Model\SponsorshipCases::query();
        if ($user && !$user->super_admin) {
            $query->join('char_cases', 'char_cases.id', '=', 'char_sponsorship_cases.case_id')
                ->where('char_cases.organization_id', '=', $user->organization_id);
        }
        $query->selectRaw('char_sponsorship_cases.status, COUNT(char_sponsorship_cases.case_id) AS cases')
            ->groupBy('char_sponsorship_cases.status');

        $status = [
            1 => 'candidate',
            2 => 'send',
            3 => 'sponsored',
            4 => 'stoped',
            5 => 'suspended',
        ];
        $data = [
            'candidate' => 0,
            'send' => 0,
            'sponsored' => 0,
            'stoped' => 0,
            'suspended' => 0,
        ];
        foreach ($query->get() as $entry) {
            if (!isset($status[$entry->status])) {
                continue;
            }
            $data[$status[$entry->status]] = $entry->cases;
        }

        return $data;
    }

    // get aid statistic to dashboard content
    protected function getAidStats($user)
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit', '2048M');

        $UserType=$user->type;
        $UserOrg=$user->organization;
        $organization_id = $user->organization_id;
        $UserOrgLevel =$UserOrg->level;

        $query = \Aid\Model\VoucherPersons::query()
                        ->join('char_vouchers', function($join){
                            $join->on('char_vouchers.id', '=','char_vouchers_persons.voucher_id')
                                ->whereNull('char_vouchers.deleted_at');
                        })
                        ->join('char_vouchers_categories', 'char_vouchers_categories.id', '=', 'char_vouchers.category_id');

                        if ($user && !$user->super_admin) {
                                 if($UserType == 2) {
                                     $query->where(function ($anq) use ($user) {
                                         $anq->where('char_vouchers.organization_id',$user->organization_id);
                                         $anq->orwherein('char_vouchers.organization_id', function($quer) use($user) {
                                             $quer->select('organization_id')
                                                 ->from('char_user_organizations')
                                                 ->where('user_id', '=', $user->id);
                                         });
                                     });
                                 }
                                 else{
                                     $query->where(function ($anq) use ($user) {
                                         $anq->where('char_vouchers.organization_id',$user->organization_id);
                                         $anq->orwherein('char_vouchers.organization_id', function($decq) use($user) {
                                             $decq->select('descendant_id')
                                                 ->from('char_organizations_closure')
                                                 ->where('ancestor_id', '=', $user->organization_id);
                                         });
                                     });
                                 }
                        }

        $query->selectRaw('char_vouchers.category_id, char_vouchers_categories.name  as category_name , 
                                    COUNT(char_vouchers_persons.person_id) AS persons')
              ->groupBy('char_vouchers.category_id', 'char_vouchers_categories.name')
              ->orderBy('char_vouchers_categories.name');

        return $query->get();
    }

    // get aid statistic to transfers
    protected function getTransfersStats($user)
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit', '2048M');

        $transfers = [];
        $UserType=$user->type;
        $UserOrg=$user->organization;
        $organization_id = $user->organization_id;
        $UserOrgLevel =$UserOrg->level;

        $query=\DB::table('char_transfers_view')
                  ->leftjoin('char_cases', function($join)  use ($organization_id) {
                        $join->on('char_cases.person_id','char_transfers_view.person_id' );
                        $join->on('char_cases.category_id','char_transfers_view.category_id' );
                        $join->whereRaw("char_cases.organization_id = $organization_id and date(char_cases.created_at) >  date(char_transfers_view.created_at)");
                  })
                  ->whereRaw("(char_transfers_view.organization_id != $organization_id)")
                  ->groupBy('char_transfers_view.id');

        // get cases due to org locations
        $whereRaw = null;
        if ($UserOrgLevel == Organization::LEVEL_BRANCH_CENTER) {
            $mainConnector = OrgLocations::getConnected($organization_id);
            $districts = aidsLocationI18n::getConnectedIds(aidsLocation::TYPE_DISTRICT, $mainConnector, true);
            $whereRaw = "(
                                                adsdistrict_id is null   OR
                                                ( adsdistrict_id  is not null  AND adsdistrict_id IN (" . join(",", $districts) . ") )
                                              )";
        }
        elseif ($UserOrgLevel == Organization::LEVEL_BRANCHES) {
            $mainConnector = OrgLocations::getConnected($organization_id);
            $mosques = aidsLocationI18n::getConnectedIds(aidsLocation::TYPE_MOSQUES, $mainConnector, true);
            $districts = aidsLocationI18n::getConnectedIds(aidsLocation::TYPE_DISTRICT, $mainConnector, true);
            $whereRaw ="(
                                          (
                                              adsdistrict_id  is not null AND
                                              adsmosques_id IN (" . join(",", $mosques) . ")
                                          )
                                           or
           
           (
                                             adsdistrict_id   IN (" . join(",", $districts) . ")  and
                (   adsregion_id  is null OR  adssquare_id  is null OR    adsneighborhood_id  is null OR  adsmosques_id  is null   )  
            )
             or
           
           (
               adsdistrict_id   is  null  and
                (   adsregion_id  is null OR  adssquare_id  is null OR    adsneighborhood_id  is null OR  adsmosques_id  is null   )  
            )
                                         
                                          )";

        }

        if(!is_null($whereRaw)){
            $query->whereRaw($whereRaw);
        }

        $transfers ['count'] = $query->count();


        // ********************************************************************** //

        $transfers ['accepted'] =\DB::table('char_transfers_log')
                    ->join('char_transfers_view', function($join)  use ($organization_id) {
                        $join->on('char_transfers_view.id', 'char_transfers_log.transfer_id')
                            ->where('char_transfers_view.organization_id', $organization_id);
                    })->where('char_transfers_log.status',1)
            ->count();

        return $transfers;
    }
}
