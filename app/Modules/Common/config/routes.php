<?php

Route::POST('api/v1.0/common/category/map','Common\Controller\Categories\AbstractCategoriesController@categoryMap');
Route::group(['prefix' => 'api/v1.0/common','namespace' => 'Common\Controller'], function() {
    Route::group(['prefix' => 'cases','namespace' => 'Cases'], function() {
        Route::get('getPhoto/{id}','reportsController@getPhoto');
        Route::get('loadPhoto/{id}','reportsController@loadPhoto');
        Route::post('savePhoto','reportsController@savePhoto');
    });
});

Route::group(['middleware' => ['api','lang','auth.check'], 'prefix' => 'api/v1.0/common','namespace' => 'Common\Controller'], function() {

    Route::get ('attachmentInstruction','Cases\AbstractCasesController@attachmentInstruction');
    Route::get('files/{type}/export', 'CaseDocumentsController@exportFile');
    // -------------------------------------------------------------------------------------------------------//
    Route::resource('updateSetting', 'updateSettingController');
    // ------------------------------------------ CATEGORIES_POLICY ------------------------------------------//
    Route::post ('categories_policy/getPolicy/','CategoriesPolicy\AbstractCategoriespolicyController@getPolicy');
    Route::resource('categories_policy', 'CategoriesPolicy\AbstractCategoriespolicyController');

    // ------------------------------------------ Transfers ------------------------------------------//
    Route::group(['prefix' => 'Transfers','namespace' => 'Cases'], function() {

        Route::post ('acceptGroup','TransfersController@acceptGroup');
        Route::put ('accept/{id}','TransfersController@accept');

        Route::post ('confirmGroup','TransfersController@confirmGroup');
        Route::put ('confirm/{id}','TransfersController@confirm');

        Route::post ('acceptedFilter','TransfersController@acceptedFilter');
        Route::post ('confirmFilter','TransfersController@confirmFilter');
        Route::post ('filter','TransfersController@filter');
    });
    Route::resource('Transfers','Cases\TransfersController');
    // ------------------------------------------ RELAYS ------------------------------------------//
    Route::group(['prefix' => 'Relays','namespace' => 'Cases'], function() {
        Route::get ('Confirm/aids/template','RelaysController@ConfirmTemplate');
        Route::get ('Confirm/aids/template','RelaysController@ConfirmTemplate');
        Route::get ('aids/template','RelaysController@aidsTemplate');
        Route::get ('sponsorships/template','RelaysController@sponsorshipsTemplate');
        Route::post ('confirmGroup','RelaysController@confirmGroup');
        Route::post ('confirm','RelaysController@confirm');
        Route::post ('relayOne','RelaysController@relayOne');
        Route::post ('relayFromExcel','relayFromExcel@relayFromExcel');
//        Route::post ('relayFromExcel','RelaysController@relayFromExcel');
        Route::post ('confirmFilter','RelaysController@confirmFilter');
        Route::post ('filter','RelaysController@filter');
    });
    Route::resource('Relays','Cases\RelaysController');
    // ------------------------------------------ REPORTS ------------------------------------------//
    Route::group(['prefix' => 'reports'], function() {
        Route::post ('tabContent','Cases\reportsController@tabContent');
        Route::get ('template','Cases\reportsController@template');
        Route::group(['prefix' => 'citizenRepository' ,'namespace' => 'Cases'], function() {
            Route::post ('searchFilter','reportsController@citizenSearchFilter');
            Route::post ('update','reportsController@citizenRepositoryUpdateData');
            Route::post ('check','reportsController@citizenRepositoryCheck');
            Route::post ('filter','reportsController@citizenRepository');
        });

        Route::group(['prefix' => 'searchAll' ,'namespace' => 'Cases'], function() {
            Route::post ('searchFilter','reportsController@searchAllFilter');
            Route::post ('check','reportsController@searchAllCheck');
            Route::post ('filter','reportsController@searchAll');
        });

        Route::group(['prefix' => 'socialAffairs' ,'namespace' => 'Cases'], function() {
            Route::post ('searchFilter','reportsController@affairsSearchFilter');
            Route::post ('update','reportsController@saveSocialAffairsStatus');
            Route::post ('check','reportsController@socialAffairsCheck');
            Route::post ('filter','reportsController@socialAffairs');
        });

        Route::group(['prefix' => 'socialAffairsReceipt' ,'namespace' => 'Cases'], function() {
            Route::post ('searchFilter','reportsController@socialAffairsReceiptFilter');
            Route::post ('check','reportsController@socialAffairsReceiptCheck');
            Route::post ('filter','reportsController@socialAffairsReceipt');
        });

        Route::group(['prefix' => 'aidsCommittee','namespace' => 'Cases'], function() {
            Route::post ('searchFilter','reportsController@aidsSearchFilter');
            Route::post ('update','reportsController@saveAidsCommitteeStatus');
            Route::post ('check','reportsController@aidsCommitteeCheck');
            Route::post ('filter','reportsController@aidsCommittee');
        });

        Route::group(['prefix' => 'voucherSearch','namespace' => 'Cases'], function() {
            Route::post ('check','reportsController@voucherSearchCheck');
            Route::post ('filter','reportsController@voucherSearch');
        });

    });
    Route::resource('reports', 'reportsController');
    // ------------------------------------------ BLOCKED ------------------------------------------//
    Route::group(['prefix' => 'block'], function() {
        Route::get ('export/{id}','BlocksController@export');
        Route::post('filter','BlocksController@filter');
    });
    Route::resource('block', 'BlocksController');
    // ------------------------------------------ sponsorships ------------------------------------------//
    Route::group(['prefix' => 'sponsorships'], function() {
        // ------------------------------------------ CATEGORIES ------------------------------------------//
        Route::group(['prefix' => 'category'], function() {
            Route::put('setForm/{id}','Categories\SponsorshipCategoriesController@setForm');
            Route::put('setFormStatus/{id}','Categories\SponsorshipCategoriesController@setFormStatus');
            Route::get('forms','Categories\SponsorshipCategoriesController@forms');
            Route::post('getCategoryDocuments','Categories\SponsorshipCategoriesController@getCategoryDocuments');
            Route::put('updateElement/{id}','Categories\SponsorshipCategoriesController@updateElement');
            Route::get('getElements/{id}','Categories\SponsorshipCategoriesController@getElements');
            Route::get('getPriority/','Categories\SponsorshipCategoriesController@getPriority');
            Route::post('getCategoryFormsSections/{id}','Categories\SponsorshipCategoriesController@getCategoryFormsSections');
            Route::post('getCategoryFormsSections/{id}','Categories\SponsorshipCategoriesController@getCategoryFormsSections');
            Route::post('template-upload','Categories\SponsorshipCategoriesController@templateUpload');
            Route::get('getCategoryDocument/{id}','Categories\SponsorshipCategoriesController@getCategoryDocument');
            Route::get('getSponsorTemplates/{id}','Categories\SponsorshipCategoriesController@getSponsorTemplates');
            Route::get('getSponsorReports/{id}','Categories\SponsorshipCategoriesController@getSponsorReports');
            Route::get('getCategoryName/{id}','Categories\SponsorshipCategoriesController@getCategoryName');
            Route::get('getCategory/','Categories\SponsorshipCategoriesController@getCategory');
            Route::get('defaultTemplate','Categories\SponsorshipCategoriesController@defaultTemplate');
            Route::get('templateInstruction/','Categories\SponsorshipCategoriesController@templateInstruction');
            Route::get('template/{id}','Categories\SponsorshipCategoriesController@template');
            Route::get('getCategoriesTemplate/','Categories\SponsorshipCategoriesController@getCategoriesTemplate');
            Route::get('getSectionForEachCategory/','Categories\SponsorshipCategoriesController@getSectionForEachCategory');
            });
        Route::resource('category', 'Categories\SponsorshipCategoriesController');
        // ------------------------------------------ VISITOR_NOTES ------------------------------------------//
        Route::get('visitor_notes/templateInstruction/','VisitorNotes\SponsorshipVisitorNotesController@templateInstruction');
        Route::resource('visitor_notes', 'VisitorNotes\SponsorshipVisitorNotesController');
        // ------------------------------------------ CATEGORIES_POLICY ------------------------------------------//
        Route::post ('categories_policy/getPolicy/','CategoriesPolicy\SponsorshipCategoriespolicyController@getPolicy');
        Route::resource('categories_policy', 'CategoriesPolicy\SponsorshipCategoriespolicyController');
        // ------------------------------------------ CASES ------------------------------------------//
        Route::group(['prefix' => 'cases' ,'namespace' => 'Cases'], function() {
            Route::post('goverment_update','SponsorshipsCasesController@goverment_update');
            Route::post ('check','SponsorshipsCasesController@check');
            Route::post('add_bank_account','SponsorshipsCasesController@add_bank_account');
            Route::put ('changeCategoryToGroup/{id}','SponsorshipsCasesController@changeCategoryToGroup');
            Route::put ('changeCategory/{id}','SponsorshipsCasesController@changeCategory');
            Route::post('pdf','SponsorshipsCasesController@pdf');
            Route::post('word','SponsorshipsCasesController@word');
            Route::get('import-family-template','SponsorshipsCasesController@importFamilyTemplate');
            Route::get('import-to-update-template/','SponsorshipsCasesController@importTpUpdateTemplate');
            Route::get('import-template/','SponsorshipsCasesController@importTemplate');
            Route::post('import-family','SponsorshipsCasesController@importFamily');
            Route::post('importToUpdate','SponsorshipsCasesController@importToUpdate');
            Route::post('import/','SponsorshipsCasesController@import');
            Route::post ('getSponsorList','SponsorshipsCasesController@getSponsorList');
            Route::put('setCaseAttachment/{id}','SponsorshipsCasesController@setCaseAttachment');
            Route::put('setAttachment/{id}','SponsorshipsCasesController@setAttachment');
            Route::get('getVisitorNote/{id}','SponsorshipsCasesController@getVisitorNote');
            Route::put('restore/{id}','SponsorshipsCasesController@restore');
            Route::get('getStatusLogs/{id}','SponsorshipsCasesController@getStatusLogs');
            Route::get('exportCaseDocument/{id}','SponsorshipsCasesController@exportCaseDocument');
            Route::post('getCaseAttachments/','SponsorshipsCasesController@getCaseAttachments');
            Route::post('getInCompleteCases/','SponsorshipsCasesController@getInCompleteCases');
            Route::post('filterCases/','SponsorshipsCasesController@filterCases');
            Route::post('updateStatusAndCategories/','SponsorshipsCasesController@updateStatusAndCategories');

        });
        Route::resource('cases', 'Cases\SponsorshipsCasesController');
    });
    // ------------------------------------------ aids ------------------------------------------//
    Route::group(['prefix' => 'aids'], function() {
        // ------------------------------------------ CATEGORIES ------------------------------------------//
        Route::group(['prefix' => 'category'], function() {
            Route::put('setForm/{id}','Categories\AidsCategoriesController@setForm');
            Route::put('setFormStatus/{id}','Categories\AidsCategoriesController@setFormStatus');
            Route::get('forms','Categories\AidsCategoriesController@forms');
            Route::put('updateElement/{id}','Categories\AidsCategoriesController@updateElement');
            Route::get('getElements/{id}', 'Categories\AidsCategoriesController@getElements');
            Route::get('getPriority/', 'Categories\AidsCategoriesController@getPriority');
            Route::post('getCategoryFormsSections/{id}', 'Categories\AidsCategoriesController@getCategoryFormsSections');
            Route::post('template-upload', 'Categories\AidsCategoriesController@templateUpload');
            Route::get('getCategoryDocument/{id}', 'Categories\AidsCategoriesController@getCategoryDocument');
            Route::get('getCategoryName/{id}', 'Categories\AidsCategoriesController@getCategoryName');
            Route::get('getCategory/', 'Categories\AidsCategoriesController@getCategory');
            Route::get('defaultTemplate', 'Categories\AidsCategoriesController@defaultTemplate');
            Route::get('templateInstruction/', 'Categories\AidsCategoriesController@templateInstruction');
            Route::get('template/{id}', 'Categories\AidsCategoriesController@template');
            Route::get('getCategoriesTemplate/', 'Categories\AidsCategoriesController@getCategoriesTemplate');
            Route::get('getSectionForEachCategory/', 'Categories\AidsCategoriesController@getSectionForEachCategory');
        });
        Route::resource('category', 'Categories\AidsCategoriesController');
        // ------------------------------------------ VISITOR_NOTES ------------------------------------------//
        Route::get('visitor_notes/templateInstruction/','VisitorNotes\AidsVisitorNotesController@templateInstruction');
        Route::resource('visitor_notes', 'VisitorNotes\AidsVisitorNotesController');
        // ------------------------------------------ CATEGORIES_POLICY ------------------------------------------//
        Route::post ('categories_policy/getPolicy/','CategoriesPolicy\AidsCategoriespolicyController@getPolicy');
        Route::resource('categories_policy', 'CategoriesPolicy\AidsCategoriespolicyController');
        // ------------------------------------------ CASES ------------------------------------------//
        Route::group(['prefix' => 'cases','namespace' => 'Cases'], function() {
            Route::post('import-essentials','AidsCasesController@importEssentials');
            Route::post('import-properties','AidsCasesController@importProperties');

            Route::post('import-banks','AidsCasesController@importBanks');
            Route::post('import-fin-aids','AidsCasesController@importFinAids');
            Route::post('import-non-fin-aids','AidsCasesController@importNonFinAids');
            Route::post('import-family','AidsCasesController@importFamily');
            Route::post('import/','AidsCasesController@import');

            Route::post('goverment_update','AidsCasesController@goverment_update');
            Route::post ('check','AidsCasesController@check');
            Route::put ('changeCategory/{id}','AidsCasesController@changeCategory');
            Route::put ('changeCategoryToGroup/{id}','AidsCasesController@changeCategoryToGroup');
            Route::post('pdf','AidsCasesController@pdf');
            Route::post('word','AidsCasesController@word');
            Route::get('import-family-template','AidsCasesController@importFamilyTemplate');
            Route::get('import-to-update-template/','AidsCasesController@importTpUpdateTemplate');
            Route::get('import-template/','AidsCasesController@importTemplate');
            Route::post('importToUpdate','AidsCasesController@importToUpdate');
            Route::put ('setCaseAttachment/{id}','AidsCasesController@setCaseAttachment');
            Route::put ('setAttachment/{id}','AidsCasesController@setAttachment');
            Route::get ('getVisitorNote/{id}','AidsCasesController@getVisitorNote');
            Route::put ('restore/{id}','AidsCasesController@restore');
            Route::get ('getStatusLogs/{id}','AidsCasesController@getStatusLogs');
            Route::get ('exportCaseDocument/{id}','AidsCasesController@exportCaseDocument');
            Route::post('getCaseAttachments/','AidsCasesController@getCaseAttachments');
            Route::post('getInCompleteCases/','AidsCasesController@getInCompleteCases');
            Route::put('saveRecommendations/{id}','AidsCasesController@saveRecommendations');
            Route::put('saveOthersData/{id}','AidsCasesController@saveOthersData');
            Route::put('saveHomeIndoor/{id}','AidsCasesController@saveHomeIndoor');
            Route::put('saveResidence/{id}','AidsCasesController@saveResidence');
            Route::post('filterCases/','AidsCasesController@filterCases');
            Route::post('updateStatusAndCategories/','AidsCasesController@updateStatusAndCategories');
            Route::post('transferGroup','AidsCasesController@transferGroup');
            Route::post('transferOne','AidsCasesController@transferOne');
        });
        Route::resource('cases', 'Cases\AidsCasesController');
    });
    // ------------------------------------------------------------------------------------------------//
    Route::group(['prefix' => 'cases'], function() {
        Route::get('checkAccount/{id}/', 'Cases\AbstractCasesController@checkAccount');
        Route::post('exportRestricted/', 'Cases\AbstractCasesController@exportRestricted');
        Route::post('exportRestrictedCard/', 'Cases\AbstractCasesController@exportRestrictedCard');
        Route::post('findPerson/', 'CasesController@findPerson');
        Route::post('getCaseReports/', 'CasesController@getCaseReports');
        Route::post('getSponsorships/', 'CasesController@getSponsorships');
        Route::post('getCaseVouchers/', 'CasesController@getCaseVouchers');
        Route::post('individual', 'CasesController@individual');
    });
    Route::resource('cases', 'CasesController');
    Route::group(['prefix' => 'persons'], function() {
        Route::post('getStatistic/','statisticsController@getStatistic');
        Route::post('find/','PersonsController@find');
        Route::post('getPersonReports/','PersonsController@getPersonReports');
        Route::post('voucherFilter/','PersonsController@voucherFilter');
        Route::post('getPersons/','PersonsController@getPersons');
    });
    Route::resource('persons', 'PersonsController');

    Route::group(['prefix' => 'cases-documents'], function() {
        Route::post('prepare', 'CaseDocumentsController@prepare')->middleware('auth:api');
        Route::get('export', 'CaseDocumentsController@export');
        Route::post('import', 'CaseDocumentsController@import')->middleware('auth:api');
    });
    
});

?>