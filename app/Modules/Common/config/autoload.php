<?php

$loader = new Composer\Autoload\ClassLoader();
$loader->addPsr4('Common\\', __DIR__ . '/../src');
$loader->register();