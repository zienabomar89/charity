<?php

namespace Common\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Common\Model\CaseModel;

class CaseDocumentsController extends Controller
{

    // export download_token of document compressed
    public function export(Request $request)
    {
        $token = $request->input('download_token');
        $filename = storage_path('tmp/import_documents_' . $token . '.zip');
        if (!file_exists($filename)) {
            return response()->json([
                'error' => 'Invalid token',
            ]);
        }

        return response()->download($filename);
    }

    // prepare folders to import  case documents
    public function prepare(Request $request)
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $user = Auth::user();
        $cases = CaseModel::relatedDocuments([
            'document_type_name' => true,
            'person_name' => true,
            'father_id' => true,
            'mother_id' => true,
            'guardian_id' => true,
            'category_id' => $request->get('category_id'),
            'category_type' => $request->get('category_type'),
            'organization_id' => $user->organization_id,
//            'organization_id' => $user->super_admin? null: $user->organization_id,
            'super_admin' => $user->super_admin,
            'sponsor_id' => $request->get('sponsor_id'),
            'sponsor_status' => $request->get('sponsor_status'),
            'language_id' => 1,
            'organizations'=> $request->get('organization_id'),
            'first_name'=> $request->get('first_name'),
            'second_name'=> $request->get('second_name'),
            'third_name'=> $request->get('third_name'),
            'last_name'=> $request->get('last_name'),
            'id_card_number'=> $request->get('id_card_number'),
            'all_organization'=> $request->get('all_organization'),
            'items'=> $request->get('items')
        ]);

        if(sizeof($cases) >0){
            //        $documentTypes = \Setting\Model\DocumentTypeI18n::where('language_id', $language_id)->get();

            $token = md5(uniqid());
            $filename = storage_path('tmp/import_documents_' . $token . '.zip');
            $zip = new \ZipArchive();
            $res = $zip->open($filename, \ZipArchive::CREATE);
            foreach ($cases as $case) {
                //$folder = $case->id . '__' . $case->person_id . '__' . $case->getPersonFullName();
                $folder = $case['case_id'] . '__' . $case['person_id'] . '__' . $case['name'];
                $zip->addEmptyDir($folder);
                //foreach ($documentTypes as $type) {
                foreach ($case['documents'] as $type) {
                    //$subFolder = $type->document_type_id . '__' . $type->name;
                    $subFolder = $type['id'] . '__' . $type['target'] . '__' . $type['name'];
                    $zip->addEmptyDir($folder . '/' . $subFolder);
                }
            }
            $zip->close();

            return response()->json([
                'download_token' => $token,
            ]);
        }

        return response()->json([
            'status' => false,
        ]);
    }

    // do import of case documents
    public function import(Request $request)
    {
        $guesser = \Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser::getInstance();
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $user = Auth::user();
        $uploadFile = $request->file('file');
        if ($uploadFile->isValid()) {
            $files = $this->readZipFile($uploadFile);
            $new = $updated = 0;
            $entries = \Common\Model\PersonDocument::whereIn('person_id', array_keys($files))->get();
            foreach ($entries as $entry) {
                if (isset($files[$entry->person_id][$entry->document_type_id])) {
                    $file = $files[$entry->person_id][$entry->document_type_id];
                    \Document\Model\File::createRevision([
                        'id' => $entry->file_id,
                        'notes' => null,
                        'filepath' => $file['filepath'],
                        'size' => Storage::size($file['filepath']),
                        'mimetype' => $guesser->guess(storage_path('app/' . $file['filepath'])),
                    ]);
                    $updated++;

                    unset($files[$entry->person_id][$entry->document_type_id]);
                }
            }

            $newFiles = [];
            if (isset($files) && is_array($files)) {
                //$finfo = \finfo_open(\FILEINFO_MIME_TYPE);

                foreach ($files as $person) {
                    foreach ($person as $file) {
                        if (!isset($file['filepath']) || empty($file['filepath'])) {
                            continue;
                        }
                        $newFiles[] = \Common\Model\PersonDocument::createNew([
                            'name' => $file['document_type_name'] . ' - ' . $file['name'],
                            'filepath' => $file['filepath'],
                            'size' => Storage::size($file['filepath']),
                            'mimetype' => $guesser->guess(storage_path('app/' . $file['filepath'])),
                            'user_id' => $user->id,
                            'case_id' => $file['case_id'],
                            'document_type_id' => $file['document_type_id'],
                            'person_id' => $file['target_id'],
                        ]);
                        $new++;
                    }
                }
                //\finfo_close($finfo);
            }

            return response()->json([
                'total'  => $new + $updated,
                'insert' => $new,
                'update' => $updated,
            ]);
        }
    }

    // read import compressed of case documents
    protected function readZipFile($uploadFile)
    {
        $files = [];
        $zip = new \ZipArchive();
        $res = $zip->open($uploadFile->getPathname());
        for ($i = 0; $i < $zip->numFiles; $i++) {
            $filename = $zip->getNameIndex($i);
            $parts = explode('/', trim($filename, '/'));
            if (count($parts) == 3) {
                list($case_id, $person_id, $name) = explode('__', $parts[0]);
                list($document_type_id, $target_id, $document_type_name) = explode('__', $parts[1]);

                $content = $zip->getFromIndex($i);
                $fn = md5($content) . substr($parts[2], strrpos($parts[2], '.'));
                Storage::put('documents/' . $fn, $content);

                $files[$target_id][$document_type_id] = [
                    'case_id' => $case_id,
                    'person_id' => $person_id,
                    'name' => $name,
                    'target_id' => $target_id,
                    'document_type_id' => $document_type_id,
                    'document_type_name' => $document_type_name,
                    'filename' => $parts[2],
                    'filepath' => 'documents/' . $fn,
                ];
                unset($content);
            }
        }

        return $files;
    }

    // export file to download with condition
    public function exportFile(Request $request,$type)
    {

        $token = $request->input('download_token');

        $deleteFileAfterSend=false;
        if ($request->input('template')) {
            $filename = storage_path('app/templates/' . $token .'.'. $type);
        }elseif ($request->input('backup')) {
            $filename = storage_path('backup/' . $token .'.'. $type);
        }elseif ($request->input('documents')) {
            $filename = storage_path('app/documents/' . $token .'.'. $type);
        }elseif ($request->input('cheques')) {
            $filename = storage_path('app/archiveCheques/export_' . $token .'.'. $type);
//            $deleteFileAfterSend=$request->input('deleted');
        }elseif ($request->input('reports')) {
            $filename = storage_path('app/reports/' . $token .'.'. $type);
        }else{
            $filename = storage_path('tmp/export_' . $token .'.'. $type);
            $deleteFileAfterSend=true;
        }


        if (!file_exists($filename)) {
            return response()->json([
                'error' => 'Invalid token',$filename]);
        }

        if ($deleteFileAfterSend == true) {
            return response()->download( $filename)->deleteFileAfterSend(true);
        }else{
            return response()->download( $filename);
        }
    }

}