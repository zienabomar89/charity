<?php

namespace Common\Controller;

use App\Http\Controllers\Controller;
use Common\Model\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Setting\Model\DocumentI18n;

class importAttachment extends Controller
{

    // do import of case documents
    public function import(Request $request)
    {
        $guesser = \Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser::getInstance();

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','6144M');
        $user = Auth::user();
        $uploadFile = $request->file('file');
        if ($uploadFile->isValid()) {
            $files = $this->readZipFile($uploadFile);
            $new = $updated = 0;
            $entries = \Common\Model\PersonDocument::whereIn('person_id', array_keys($files))->get();
            foreach ($entries as $entry) {
                if (isset($files[$entry->person_id][$entry->document_type_id])) {
                    $file = $files[$entry->person_id][$entry->document_type_id];
                    \Document\Model\File::createRevision([
                        'id' => $entry->file_id,
                        'notes' => null,
                        'filepath' => $file['filepath'],
                        'size' => Storage::size($file['filepath']),
                        'mimetype' => $guesser->guess(storage_path('app/' . $file['filepath'])),
                    ]);
                    $updated++;

                    unset($files[$entry->person_id][$entry->document_type_id]);
                }
            }

            $newFiles = [];
            if (isset($files) && is_array($files)) {
                //$finfo = \finfo_open(\FILEINFO_MIME_TYPE);

                foreach ($files as $person) {
                    foreach ($person as $file) {
                        if (!isset($file['filepath']) || empty($file['filepath'])) {
                            continue;
                        }
                        $newFiles[] = \Common\Model\PersonDocument::createNew([
                            'name' => $file['document_type_name'] . ' - ' . $file['name'],
                            'filepath' => $file['filepath'],
                            'size' => Storage::size($file['filepath']),
                            'mimetype' => $guesser->guess(storage_path('app/' . $file['filepath'])),
                            'user_id' => $user->id,
//                            'case_id' => $file['case_id'],
                            'document_type_id' => $file['document_type_id'],
                            'person_id' => $file['target_id'],
                        ]);
                        $new++;
                    }
                }
                //\finfo_close($finfo);
            }

            return response()->json([
                'total'  => $new + $updated,
                'insert' => $new,
                'update' => $updated,
            ]);
        }
    }

    // read import compressed of case documents
    protected function readZipFile($uploadFile)
    {
        $files = [];
        $zip = new \ZipArchive();
        $res = $zip->open($uploadFile->getPathname());

        for ($i = 0; $i < $zip->numFiles; $i++) {
            $filename = $zip->getNameIndex($i);
            $parts = explode('/', trim($filename, '/'));
            if (count($parts) == 3) {
                list($person_id) = explode('_', $parts[0]);
                list($document_type_id, $target_id) = explode('_', $parts[1]);
                $extension =substr($parts[2], strrpos($parts[2], '.'));
                $content = $zip->getFromIndex($i);
                $fn = md5($content) . substr($parts[2], strrpos($parts[2], '.'));
                Storage::put('documents/' . $fn, $content);

                $files[$target_id][$document_type_id] = [
                    'person_id' => $person_id,
                    'name' => Person::getFullName($person_id),
                    'target_id' => $target_id,
                    'document_type_id' => $document_type_id,
                    'document_type_name' => DocumentI18n::getDocumentName($document_type_id),
                    'extension' => $extension,
                    'filename' => $parts[2],
                    'filepath' => 'documents/' . $fn,
                ];
                unset($content);
            }
        }

        return $files;
    }

}