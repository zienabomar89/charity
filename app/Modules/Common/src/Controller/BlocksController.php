<?php

namespace Common\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Common\Model\BlockIDCard;
use Common\Model\BlockCategories;

class BlocksController  extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // filter blocked card records according some filters
    public function filter(Request $request)
    {
        $response=BlockCategories::getBlocked($request->page,$request->get('type'),$request->all());
        if($request->get('status')=='export'){
            $this->authorize('export', BlockIDCard::class);
            $rows=[];

            foreach($response as $key =>$value){
                $rows[$key][trans('aid::application.#')]=$key+1;
               foreach($value as $k =>$v){
                   $rows[$key][trans('aid::application.' . $k)]= $v;
               }
            }

            $token = md5(uniqid());
            \Maatwebsite\Excel\Facades\Excel::create('export_' . $token, function($excel) use($rows) {
                $excel->sheet(trans('aid::application.#'),function($sheet) use($rows) {
                    $sheet->setRightToLeft(true);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);
                    $sheet->getStyle("A1:E1")->applyFromArray(['font' => ['bold' => true]]);
                    $sheet->fromArray($rows);
                });
            })->store('xlsx', storage_path('tmp/'));
            return response()->json([
                'download_token' => $token,
            ]);


        }else{
            $this->authorize('manage', BlockIDCard::class);
            return response()->json($response);
        }
    }

    // export blocked card records according type
    public function export($id)
    {
        $this->authorize('export', BlockIDCard::class);
        $result =BlockCategories::getBlockedList($id);
        if(sizeof($result) !=0){
            $rows=[];
            foreach($result as $key =>$value){
                $rows[$key][trans('aid::application.#')]=$key+1;
                foreach($value as $k =>$v){
                    $rows[$key][trans('aid::application.' . $k)]= $v;
                }

            }

            $token = md5(uniqid());
            \Maatwebsite\Excel\Facades\Excel::create('export_' . $token, function($excel) use($rows) {
                $excel->sheet(trans('common::application.Block detection'),function($sheet) use($rows) {
                    $sheet->setRightToLeft(true);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);
                    $sheet->getStyle("A1:E1")->applyFromArray(['font' => ['bold' => true]]);
                    $sheet->fromArray($rows);
                });
            })->store('xlsx', storage_path('tmp/'));

            return response()->json(['download_token' => $token,]);
        }else{
            return 0;
        }

    }

    // save blocked card record
    public function store(Request $request)
    {
        try {
            $this->authorize('create', BlockIDCard::class);
            $response=array();

            if(!$request->get('sponsor_id') && $request->get('checkbox') == 2) {

                $path = \Input::file('file')->getRealPath();

                $fileObj = \PHPExcel_IOFactory::load($path);
                $sheetObj = $fileObj->getActiveSheet();
                $coordinate = [];
                if (\Input::hasFile('file')) {
                    foreach ($sheetObj->getRowIterator(0, 1) as $row) {
                        foreach ($row->getCellIterator() as $cell) {
                            $value = $cell->getCalculatedValue();
                            if ($value != null) {
                                $coordinate[] = $value;
                            }
                        }
                    }
                    $data = \Excel::load($path, function ($reader) {
                    })->get();

                    $persons = [];
                    foreach ($data as $key => $value) {
                        array_push($persons, ['id_card_number' => $value->rkm_alhoy, 'type' => strip_tags($request->get('type'))]);
                    }

                    $categories = $request->get('category_id');
                    $is_array = 0;
                    $exist_1 = 0;
                    if (strpos($categories, ',') !== false) {
                        $is_array = 1;
                    }
                    if ($is_array == 1) {

                        $blk_categories = explode(',', $categories);

                        foreach ($blk_categories as $im) {
                            foreach ($persons as $item) {
                                $count = BlockIDCard::where($item)->count();
                                if ($count == 0) {
                                    $inserted_1 = BlockIDCard::create($item);
                                    BlockCategories::create(['block_id' =>$inserted_1->id,'category_id' => $im]);

                                    $category=\Common\Model\Categories\AidsCategories::where('id',$im)->first();
                                    \Log\Model\Log::saveNewLog('BLOCK_CATEGORIES_CREATED',
                                        trans('common::application.Blocked ID numbers') .' " ' .$category->name  . ' " ');

                                } else {
                                    $i=BlockIDCard::where($item)->first();
                                    if(BlockCategories::where(['block_id' =>$i->id,'category_id' => $im])->count() ==0){
                                        BlockCategories::create(['block_id' =>$i->id,'category_id' => $im]);
                                        $category=\Common\Model\Categories\AidsCategories::where('id',$im)->first();
                                        \Log\Model\Log::saveNewLog('BLOCK_CATEGORIES_CREATED',
                                            trans('common::application.Blocked ID numbers') .' " ' .$category->name  . ' " ');
                                    }else{
                                        $exist_1++;
                                    }

                                }
                            }
                        }
                    }
                    else {
                        foreach ($persons as $item) {
                            $count = BlockIDCard::where($item)->count();
                            if ($count == 0) {
                                $inserted_1 = BlockIDCard::create($item);
                                BlockCategories::create(['block_id' =>$inserted_1->id,'category_id' => $categories]);
                                $category=\Common\Model\Categories\AidsCategories::where('id',$categories)->first();
                                \Log\Model\Log::saveNewLog('BLOCK_CATEGORIES_CREATED',
                                    trans('common::application.Blocked ID numbers') .' " ' .$category->name  . ' " ');
                            } else {
                                $i=BlockIDCard::where($item)->first();
                                if(BlockCategories::where(['block_id' =>$i->id,'category_id' => $categories])->count() ==0){
                                    BlockCategories::create(['block_id' =>$i->id,'category_id' => $categories]);
                                }else{
                                    $exist_1++;
                                }

                            }
                        }
                    }

                    if($exist_1==0){
                        $response["status"]= 'success';
                        $response["msg"]= trans('aid::application.The blocked has successfully');
                    }
                    else if($exist_1==sizeof($categories)){
                        $response["status"]= 'exist';
                        $response["msg"]= trans('aid::application.The selected number is previously block');
                    }
                    else{
                        $response["status"]= 'exist';
                        $response["msg"]= trans('aid::application.Some of selected number is previously block');
//                        . ' ( '. trans('aid::application.total') .': ' . sizeof($categories)
//                        .' ، '. trans('aid::application.new block')
//                        .' : '  . ( sizeof($categories) - $exist_1)
//                        .' ، '. trans('aid::application.blocked') .' : '  . $exist_1 . ' ) ';
                    }
                }
                $response["status"] = 'error';
                $response["msg"] = trans('common::application.Unexpected bug occurred') ;
                return response()->json($response);


            }
            if($request->get('checkbox') == 1 ||$request->get('sponsor_id')) {
                $rules=[];
                $input=[];
                if($request->get('checkbox') == 1) {
                    $input=['id_card_number' => strip_tags($request->get('id_card_number')), 'type' => strip_tags($request->get('type'))];
                    $rules= ['id_card_number' => 'required|integer','type' =>'required|integer'];
                }
                if($request->get('sponsor_id')){
                    $input=['id_card_number' => strip_tags($request->get('sponsor_id')), 'type' => strip_tags($request->get('type'))];
                    $rules= ['id_card_number' => 'required|integer','type' =>'required|integer'];
                }

                $error = \App\Http\Helpers::isValid($input,$rules);
                if($error)
                    return response()->json($error);


                $count=BlockIDCard::where($input)->count();
                if($count==0){
                    $inserted=BlockIDCard::create($input);
                }else{
                    $inserted=BlockIDCard::where($input)->first();
                }

                if($request->get('sponsor_id')){
                    $Org=\Organization\Model\Organization::findorfail($request->get('sponsor_id'));
                    $action=  trans('common::application.blocked a sponsor') . $Org->name;
                }else{
                    $action= trans('common::application.Blocked ID number')  . $input['id_card_number'];
                }


                $exist=0;
                if($inserted)
                {
                    $categories=$request->get('category_id');
                    if(is_array($categories)){
                        foreach($categories as $item){
                            if(BlockCategories::where(['block_id' =>$inserted->id,'category_id' => $item])->count() ==0){
                                BlockCategories::create(['block_id' =>$inserted->id,'category_id' => $item]);
                                $category=\Common\Model\Categories\AidsCategories::where('id',$item)->first();
                                \Log\Model\Log::saveNewLog('BLOCK_CATEGORIES_CREATED',
                                    $action .' " '  .$category->name  . ' " ');
                            }
                            else{
                                $exist++;
                            }
                        }
                    }else{
                        if(BlockCategories::where(['block_id' =>$inserted->id,'category_id' => $categories])->count() ==0){
                            BlockCategories::create(['block_id' =>$inserted->id,'category_id' => $categories]);
                            $category=\Common\Model\Categories\AidsCategories::where('id',$categories)->first();
                            \Log\Model\Log::saveNewLog('BLOCK_CATEGORIES_CREATED',
                                $action .' " '  .$category->name  . ' " ');
                        }else{
                            $exist++;
                        }
                    }

                    if($exist==0){
                        $response["status"]= 'success';
                        $response["msg"]= trans('aid::application.The blocked has successfully');
                    }else if($exist==sizeof($categories)){
                        $response["status"]= 'exist';
                        $response["msg"]= trans('aid::application.The selected number is previously block');
                    }else{
                        $response["status"]= 'exist';
                        $response["msg"]= trans('aid::application.Some of selected number is previously block')
                                  . ' ( '. trans('aid::application.total') .': ' .sizeof($categories)
                                  .' ، '. trans('aid::application.new block')
                                  .' : '  . (sizeof($categories) - $exist)
                                  .' ، '. trans('aid::application.blocked') .' : '  . $exist . ' ) ';
                    }

                }else{
                    $response["status"]= 'failed';
                    $response["msg"]= trans('aid::application.The blocked fails');
                }
            }

            return response()->json($response);

        } catch (Exception $e) {
            $response["status"] = 'error';
            $response["error_code"] = $e->getCode();
            $response["msg"] = 'Databaseerror'; /* $response["msg"] = $e->getMessage();*/
            return response()->json($response);
        }
    }

    // delete blocked card by id
    public function destroy($id,Request $request)
    {
        $BlockIDCard=BlockIDCard::findorfail($id);
        $this->authorize('delete', $BlockIDCard);
        $response = array();

        $type=$BlockIDCard->type;
        $category=\Common\Model\Categories\AidsCategories::where('id',$request->get('category_id'))->first();

        if($type ==3){
            $sponsor=\Organization\Model\Sponsor::findorfail($BlockIDCard->id_card_number);
            $message=trans('common::application.remove the ban') .' " '.$category->name  . ' " '.trans('common::application.on_').' " ' .$sponsor->name .' " ';

        }else{
            $target=$BlockIDCard->id_card_number;
            $message= trans('common::application.remove the ban') .' " '.$category->name  . ' " '.trans('common::application.on ID number').': " ' .$target . ' " ';
        }


        if(BlockCategories::where(['block_id'=>$id,'category_id'=>$request->get('category_id')])->delete())
        {
            if(BlockCategories::where(['block_id'=>$id])->count()==0){
                BlockIDCard::where(['id'=>$id])->delete();
            }

            \Log\Model\Log::saveNewLog('BLOCK_CATEGORIES_DELETED', $message);
            $response["status"]= 'success';
            $response["msg"]= trans('aid::application.The row is deleted from db');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('aid::application.The row is not deleted from db');

        }
        return response()->json($response);
    }

}
