<?php

namespace Common\Controller\Categories;

use App\Http\Controllers\Controller;
use Common\Model\Categories\CategoriesForm;
use Forms\Model\CustomForms;
use Illuminate\Http\Request;
use Common\Model\Categories\AbstractCategories;
use App\Http\Helpers;
use Common\Model\CategoriesDocument;
use Setting\Model\Setting;

class AbstractCategoriesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }


    // get all AidsCategories rows
    public function index(Request $request)
    {
        $result = $this->getModel()->where(['type'=> $this->type])->orderBy('name');

        $language_id =  \App\Http\Helpers::getLocale();

        if ($language_id == 2) {
            $result->selectRaw('char_categories.* , char_categories.en_name as name_');
        }else{
            $result->selectRaw('char_categories.* , char_categories.name as name_');
        }
        $itemsCount = ($request->itemsCount != null) ? $request->itemsCount: 0;
        $records_per_page = Helpers::recordsPerPage($itemsCount,$result->count());
        return response()->json($result->paginate($records_per_page));
//        return parent::index();
    }

    public function categoryMap(Request $request)
    {
        return response()->json(['map' =>CategoriesDocument::getCategoryDocumentsMap()]);
    }
 
    // create new object model
    public function store(Request $request)
    {
        $response = array();
        $input=['name' => strip_tags($request->get('name')),
            'en_name' => strip_tags($request->get('en_name'))];
        $rules=['name' => 'required|max:255|unique:char_categories',
                'en_name' => 'required|max:255|unique:char_categories'];

        if($this->type ==1){
            $input['case_is'] = strip_tags($request->get('case_is'));
            $rules['case_is'] =  'required|integer';

            $input['family_structure'] = strip_tags($request->get('family_structure'));
            $rules['family_structure'] =  'required|integer';

            $input['has_mother'] = strip_tags($request->get('has_mother'));
            $rules['has_mother'] =  'required|integer';

            $input['father_dead'] = strip_tags($request->get('father_dead'));
            $rules['father_dead'] =  'required|integer';

            $input['has_Wives'] = strip_tags($request->get('has_Wives'));
            $rules['has_Wives'] =  'required|integer';
        }

        $error = \App\Http\Helpers::isValid($input,$rules);
        if($error)
            return response()->json($error);

        $category=null;

        $category=$this->getModel();
        $category->name=$input['name'];
        $category->en_name=$input['en_name'];
        $category->type=$this->type;

        if($this->type ==1){
            $category->case_is=$input['case_is'];
            $category->family_structure=$input['family_structure'];
            $category->has_mother=$input['has_mother'];
            $category->father_dead=$input['father_dead'];
            $category->has_Wives=$input['has_Wives'];

            $formSections=['CaseData','ParentData','providerData','FamilyData','familyDocuments','caseDocuments'];
            $form_id= 2;
        }else{
            $form_id=1;
            $formSections=['personalInfo','residenceData','homeIndoorData','otherData','familyStructure','CaseDocumentData', 'recommendations'];
        }

        if($category->save()) {
            $category_id=$category->id;

            $form_sections=$request->get('form_sections');
            $CategoriesFormsSections=[];

            foreach($formSections as $item){
                if( $form_sections[$item] == true || $form_sections[$item] == 'true'){
                    array_push($CategoriesFormsSections, ['id' => $item   , 'category_id' =>$category_id]);
                }
            }
            \Common\Model\CategoriesFormsSections::insert($CategoriesFormsSections);

            $document= $request->get('documents');
            $CategoriesDocument=[];

            foreach($document as $item){

                if(isset($item['checked'])){
                    if($item['checked']){

                        if($this->type ==1) {
                            if(!isset($item['owner'])){
                                if($input['case_is']==1){
                                    $item['owner']=3;
                                }else{
                                    $item['owner']=4;
                                }
                            }

                            if($item['owner']==""){
                                if($input['case_is']==1){
                                    $item['owner']=3;
                                }else{
                                    $item['owner']=4;
                                }
                            }

                        }else{
                            if(!isset($item['owner'])){
                                $item['owner']=3;
                            }
                        }

                        array_push($CategoriesDocument,['category_id' =>$category_id ,'document_type_id' => $item['document_type_id'] ,'scope' => $item['owner'] ]);

                    }
                }

            }
            \Common\Model\CategoriesDocument::insert($CategoriesDocument);

            $elements= \Common\Model\FormElement::where('form_id','=',$form_id)->get();
            $CategoriesFormElement=[];
            foreach($elements as $item){
                array_push($CategoriesFormElement,['category_id' =>$category_id ,'element_id'=> $item->id,'priority'=>4 ]);
            }
            \Common\Model\CategoriesFormElement::insert($CategoriesFormElement);

            \Log\Model\Log::saveNewLog('CATEGORIES_CREATED',trans('common::application.add_new_sponsorship_category') . ' "' .$category->name . '" ');

            $response["category_id"]= $category_id;
            $response["status"]= 'success';
            $response["msg"]= trans('sponsorship::application.The row is inserted to db');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('sponsorship::application.The row is not insert to db');

        }
        return response()->json($response);
    }

    // get existed object model by id
    public function show($id)
    {
        return $this->findOrFail($id);
    }

    // get existed category template model by id
    public function getCategoriesTemplate(Request $request)
    {
        $user = \Auth::user();
        return response()->json(AbstractCategories::getTemplates($user->organization_id,$this->type));
    }

    // get existed category elements model by id
    public function getElements($id)
    {
        return response()->json(\Common\Model\CategoriesFormElement::getElements($id));
    }

    // get forms category by type
    public function forms(Request $request)
    {
//            $this->authorize('forms', CategoriesForm);
        $user = \Auth::user();
        $organization_id=$user->organization_id;

        $deafult_form =null;
        $form_name = 'aid_custom_form';
        if($this->type == 1){
            $form_name = 'sponsorship_custom_form';
        }

        $form= Setting::where(['id'=>$form_name,'organization_id'=>$organization_id])->first();
        if(!is_null($form)){
            $ActiveForm=CustomForms::where(function ($q) use($form){
                $q->where('id','=',$form->value);
                $q->where('status','=',1);
            })->first();

            if($ActiveForm){
                $deafult_form= $form->value;
            }
        }

        return response()->json(['deafult_form'=>$deafult_form ,'data' => CategoriesForm::forms($this->type,$organization_id) ]);
    }

    // set form of AidsCategories rows

    public function setForm(Request $request, $id)
    {

        $response = array('status'=>'failed' , 'msg'=>trans('aid::application.There is no update'));
        $user = \Auth::user();
        $organization_id=$user->organization_id;

//            $this->authorize('forms', CategoriesForm);

        if($id == -1){
            $id = 'aid_custom_form';
            if($this->type == 1){
                $id = 'sponsorship_custom_form';
            }
            $setting = Setting::where(['id' =>$id , 'organization_id'=> $organization_id])->first();

            if(!$setting){
                $setting = new Setting();
                $setting->id=$id;
                $setting->organization_id=$organization_id;
            }

            $setting->value=$request->form_id;
            if($setting->save()) {
                $form =\Forms\Model\CustomForms::findOrFail($request->form_id);
                \Log\Model\Log::saveNewLog('CUSTOM_FORM_UPDATED', trans('setting::application.Assigned') . ' ' . $form->name  . ' ' .  trans('setting::application.As a custom form'));
                $response["status"]= 'success';
                $response["msg"]= trans('setting::application.The custom form is updated');
            }

        }else{
            $form = CategoriesForm::where(['category_id' => $id , 'organization_id' => $organization_id])->first();

            if(is_null($form)){
                $form = new CategoriesForm();
                $form->organization_id = $organization_id ;
                $form->category_id = $id ;
            }

            $form->form_id = $request->form_id ;

            if($form->save()) {
                $form =\Forms\Model\CustomForms::findOrFail($request->form_id);
                \Log\Model\Log::saveNewLog('CUSTOM_FORM_UPDATED', trans('setting::application.Assigned') . ' ' . $form->name  . ' ' .  trans('setting::application.As a custom form'));
                $response["category_id"]= $id;
                $response["status"]= 'success';
                $response["msg"]= trans('aid::application.The row is updated');
            }
        }

        return response()->json($response);

    }
    
    
     public function setFormStatus(Request $request, $id)
    {

        $response = array('status'=>'failed' , 'msg'=>trans('aid::application.There is no update'));
        $user = \Auth::user();
        $organization_id=$user->organization_id;

//            $this->authorize('forms', CategoriesForm);

       $form = CategoriesForm::where(['category_id' => $id , 'organization_id' => $organization_id])
                             ->update(['status' => $request->status ]);


        if($form) {
//            \Log\Model\Log::saveNewLog('CUSTOM_FORM_UPDATED', trans('setting::application.Assigned') . ' ' . $form->name  . ' ' .  trans('setting::application.As a custom form'));
            $response["status"]= 'success';
            $response["msg"]= trans('aid::application.The row is updated');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('aid::application.There is no update');
        }

        return response()->json($response);

    }
    // get existed category elements priority by id
    public function getPriority(Request $request)
    {
        $response['priority']=\Common\Model\FormElement::getPriority($request->get('target'),$request->get('category_id'));
        if($request->get('category')){
            $response['category']=\Common\Model\Categories\SponsorshipCategories::findorfail($request->get('category_id'));
        }
        return response()->json($response);
    }

    // get existed object name by id
    public function getCategoryName($id)
    {
        $category=$this->findOrFail($id);

        $language_id =  \App\Http\Helpers::getLocale();
        $name = '';
        if($language_id == 1){
            $name = $category->name;
        }else{
            $name = $category->en_name;
        }

        return response()->json( ['name' =>$name]);
    }

    // get existed category documents by id
    public function getCategoryDocument($id){
        return response()->json(['documents'=>\Common\Model\CategoriesDocument::getCategoryDocuments($id,null)]);
    }

    // update existed object model by id
    public function update(Request $request, $id)
    {
        $response = array();

        $category=$this->findOrFail($id);
//            $this->authorize('update', $category);

        $response = array();
        $input=['name' => strip_tags($request->get('name')) ,
                'en_name' => strip_tags($request->get('en_name'))];

        $rules= ['name' => 'required|max:255|unique:char_categories,name,'.$id];

        if($this->type ==1){
            $input['case_is'] = strip_tags($request->get('case_is'));
            $rules['case_is'] =  'required|integer';

            $input['family_structure'] = strip_tags($request->get('family_structure'));
            $rules['family_structure'] =  'required|integer';

            $input['has_mother'] = strip_tags($request->get('has_mother'));
            $rules['has_mother'] =  'required|integer';

            $input['father_dead'] = strip_tags($request->get('father_dead'));
            $rules['father_dead'] =  'required|integer';

            $input['has_Wives'] = strip_tags($request->get('has_Wives'));
            $rules['has_Wives'] =  'required|integer';
        }

        $error = \App\Http\Helpers::isValid($input,$rules);
        if($error)
            return response()->json($error);


        $category->name=$input['name'];
        $category->en_name=$input['en_name'];
        $category->type=$this->type;

        if($this->type ==1){
            $category->case_is=$input['case_is'];
            $category->family_structure=$input['family_structure'];
            $category->has_mother=$input['has_mother'];
            $category->father_dead=$input['father_dead'];
            $category->has_Wives=$input['has_Wives'];
            $formSections=['CaseData','ParentData','providerData','FamilyData','familyDocuments','caseDocuments'];
        }else{
            $formSections=['personalInfo','residenceData','homeIndoorData','otherData','familyStructure','CaseDocumentData', 'recommendations'];
        }

        $form_sections=$request->get('form_sections');
        $document= $request->get('documents');

        unset($category->form_sections);
        unset($category->documents);

//            return $category;

        if($category->save())
        {
            $CategoriesFormsSections=[];

            foreach($formSections as $item){
                if( $form_sections[$item] == true || $form_sections[$item] == 'true'){
                    array_push($CategoriesFormsSections, ['id' => $item   , 'category_id' =>$id]);
                }
            }
            \Common\Model\CategoriesFormsSections::where('category_id' ,$id)->delete();
            \Common\Model\CategoriesFormsSections::insert($CategoriesFormsSections);

            $CategoriesDocument=[];

            foreach($document as $item){

                if(isset($item['checked'])){
                    if($item['checked']){

                        if($this->type ==1) {
                            if(!isset($item['owner'])){
                                if($input['case_is']==1){
                                    $item['owner']=3;
                                }else{
                                    $item['owner']=4;
                                }
                            }

                            if($item['owner']==""){
                                if($input['case_is']==1){
                                    $item['owner']=3;
                                }else{
                                    $item['owner']=4;
                                }
                            }

                        }else{
                            if(!isset($item['owner'])){
                                $item['owner']=3;
                            }
                        }

                        array_push($CategoriesDocument,['category_id' =>$id ,'document_type_id' => $item['document_type_id'] ,'scope' => $item['owner'] ]);

                    }
                }

            }
            \Common\Model\CategoriesDocument::where('category_id' ,$id)->delete();
            \Common\Model\CategoriesDocument::insert($CategoriesDocument);

            \Log\Model\Log::saveNewLog('CATEGORIES_UPDATED',trans('common::application.Edited aid type record') . ' "' . $category->name . '" ');

            $response["category_id"]= $id;
            $response["status"]= 'success';
            $response["msg"]= trans('aid::application.The row is updated');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('aid::application.There is no update');

        }
        return response()->json($response);

    }

    // update existed category elements model by id
    public function updateElement(Request $request,$id)
    {

        try {
            $response = array();
            $input=['priority' => strip_tags($request->get('priority'))];
            $rules=['priority' => 'required|integer'];

            $error = \App\Http\Helpers::isValid($input,$rules);
            if($error)
                return response()->json($error);


            if(\Common\Model\CategoriesFormElement::where(['category_id' => $request->get('category_id') ,'element_id'=>$id])->update($input))
            {
                \Log\Model\Log::saveNewLog('CATEGORIES_FORM_ELEMENTS_UPDATED',
                    trans('common::application.Prioritized the element of the aid form').' " '  .$request->Label  . ' " ');

                $response["status"]= 'success';
                $response["msg"]= trans('setting::application.The row is updated');

            }else{
                $response["status"]= 'failed';
                $response["msg"]= trans('setting::application.There is no update');
            }
            return response()->json($response);

        } catch (\Exception $e) {
            $response["status"] = 'error';
            $response["error_code"] = $e->getCode();
            $response["msg"] = 'Databaseerror';
            return response()->json($response);
        }
    }

    // delete existed object model by id
    public function destroy($id,Request $request)
    {
        $category = $this->findOrFail($id);
//            $this->authorize('delete', $org);
        if($category->delete())
        {
            \Log\Model\Log::saveNewLog('CATEGORY_DELETED',trans('common::application.Deleted Category Record ') . ': " '.$category->name  . ' " ');
            $response["status"]= 'success';
            $response["msg"]= trans('setting::application.The row is deleted from db');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('setting::application.The row is not deleted from db');

        }
        return response()->json($response);

    }

    // upload category forms template
    public function templateUpload(Request $request){
        $category_id = $request->input('category_id');
        $user = \Auth::user();

        if($category_id != -1){
            $category = $this->findOrFail($category_id);

            if($request->sponsor){
                $organization_id=$request->sponsor;
            }else{
                $organization_id=$user->organization_id;
            }

            $template_type=\Common\Model\Template::TPL_CANDIDATE;

            if($request->template){
                if($request->template=='candidate'){
                    $template_type=\Common\Model\Template::TPL_CANDIDATE;
                }else if($request->template=='reports'){
                    $template_type=\Common\Model\Template::TPL_REPORTS;
                }
            }

            $templateFile = $request->file('file');
            $templatePath = $templateFile->store('templates');
            if ($templatePath) {
                $template = \Common\Model\Template::where(['organization_id'=> $organization_id, 'category_id' =>$category_id, 'template'=> $template_type])->first();
                if (!$template) {
                    $template = new \Common\Model\Template();
                    $template->organization_id =$organization_id;
                    $template->template = $template_type;
                    $template->category_id = $category_id;
                    if($request->template=='reports') {
                        if($request->get('other')==1 || $request->get('other')=='1'){
                             if($request->lang){
                                if($request->lang== 'en'){
                                    $template->en_other_filename = $templatePath;
                                }else{
                                    $template->other_filename = $templatePath;
                                }
                            }else{
                                $template->other_filename = $templatePath;
                            }

                        }else{
                            if($request->lang){
                                if($request->lang== 'en'){
                                    $template->en_filename = $templatePath;
                                }else{
                                    $template->filename = $templatePath;
                                }
                            }else{
                                $template->filename = $templatePath;
                            }
                        }
                    }
                    else{
                        if($request->lang){
                            if($request->lang== 'en'){
                                $template->en_filename = $templatePath;
                            }else{
                                $template->filename = $templatePath;
                            }
                        }else{
                            $template->filename = $templatePath;
                        }
                    }

                    $template->save();
                }
                else {
                        if($request->template=='reports'){
                            if($request->get('other')==1  || $request->get('other')=='1'){


                                if($request->lang){
                                    if($request->lang == 'en'){
                                        $updated['en_other_filename'] = $templatePath;
                                        if (\Storage::has($template->en_other_filename)) {
                                            \Storage::delete($template->en_other_filename);
                                        }
                                    }else{
                                        $updated['other_filename'] = $templatePath;
                                        if (\Storage::has($template->other_filename)) {
                                            \Storage::delete($template->other_filename);
                                        }

                                    }
                                }else{
                                    $updated['other_filename'] = $templatePath;
                                    if (\Storage::has($template->other_filename)) {
                                        \Storage::delete($template->other_filename);
                                    }
                                }

                                \Common\Model\Template::where(['organization_id'=> $organization_id, 'category_id' =>$category_id,
                                                                     'template'=> $template_type
                                                               ])->update($updated);
                            }
                            else{

                                if($request->lang){
                                    if($request->lang == 'en'){
                                        $updated['en_filename'] = $templatePath;
                                        if (\Storage::has($template->en_filename)) {
                                            \Storage::delete($template->en_filename);
                                        }
                                    }else{
                                        $updated['filename'] = $templatePath;
                                        if (\Storage::has($template->filename)) {
                                            \Storage::delete($template->filename);
                                        }
                                    }
                                }else{
                                    $updated['filename'] = $templatePath;
                                    if (\Storage::has($template->filename)) {
                                        \Storage::delete($template->filename);
                                    }
                                }
                                \Common\Model\Template::where([
                                    'organization_id'=> $organization_id,
                                    'category_id' =>$category_id,
                                    'template'=> $template_type
                                ])->update($updated);

                            }

                        }else{

                            if($request->lang){
                                if($request->lang == 'en'){
                                    $updated['en_filename'] = $templatePath;
                                    if (\Storage::has($template->en_filename)) {
                                        \Storage::delete($template->en_filename);
                                    }
                                }else{
                                    $updated['filename'] = $templatePath;
                                    if (\Storage::has($template->filename)) {
                                        \Storage::delete($template->filename);
                                    }
                                }
                            }else{
                                $updated['filename'] = $templatePath;
                                if (\Storage::has($template->filename)) {
                                    \Storage::delete($template->filename);
                                }
                            }

                            \Common\Model\Template::where(['organization_id'=> $organization_id,
                                                            'category_id' =>$category_id,
                                                            'template'=> $template_type
                                                          ])->update($updated);
                        }
                    }

                if($this->type ==1) {
                    $message=trans('common::application.uploaded sponsorship form export template') . ' "' . $category->name . '" ';
                }else{
                    $message=trans('common::application.uploaded aid form export template'). ' "' . $category->name . '" ';
                }

                \Log\Model\Log::saveNewLog('CATEGORIES_TEMPLATE_UPLOADED',$message);
                return response()->json($templatePath);
            }
            return false;
        }
        else{
            $templateFile = $request->file('file');
            $templatePath = $templateFile->store('templates');
            if ($templatePath) {
                if($this->type ==1) {
                    $this_template='Sponsorship-default-template';
                    $message=trans('common::application.Upload the default export template for aid forms');
                }else{
                    $this_template='aid-default-template';
                    $message=trans('common::application.This value is already in use');
                }
                    $count= \Setting\Model\Setting::where(['id'=>$this_template,'organization_id'=>$user->organization_id])->count();
                if($count==1){
                    $template=\Setting\Model\Setting::where(['id'=>$this_template,'organization_id'=>$user->organization_id])->first();
                    if (\Storage::has($template->value)) {
                        \Storage::delete($template->value);
                    }
                    \Setting\Model\Setting::where('id',$this_template)->update(['value' => $templatePath]);
                }else{
                    \Setting\Model\Setting::insert(['id'=>$this_template,'value'=>$templatePath,'organization_id'=>$user->organization_id]);
                }
                \Log\Model\Log::saveNewLog('CATEGORIES_TEMPLATE_UPLOADED',$message );

                return response()->json($templatePath);
            }

            return false;

        }
    }

    // get category forms template
    public function template($id,Request $request){

        if($id != -1){
            $template = \Common\Model\Template::findOrFail($id);

            if($template->template =='reports') {
                if($request->get('other')==1 || $request->get('other')=='1'){
                    if($request->lang){
                        if($request->lang== 'en'){
                            $filename= $template->en_other_filename;
                        }else{
                            $filename= $template->other_filename;
                        }
                    }else{
                        $filename= $template->other_filename;
                    }

                }else{
                    if($request->lang){
                        if($request->lang== 'en'){
                            $filename= $template->en_filename;
                        }else{
                            $filename= $template->filename;
                        }
                    }else{
                        $filename= $template->filename;
                    }
                }
            }else{
                if($request->lang){
                    if($request->lang== 'en'){
                        $filename= $template->en_filename;
                    }else{
                        $filename= $template->filename;
                    }
                }else{
                    $filename= $template->filename;
                }
            }

        }else{
            $user = \Auth::user();
            if($this->type ==1) {
                $this_template='Sponsorship-default-template';
            }else{
                $this_template='aid-default-template';
            }

            $template= \Setting\Model\Setting::where(['id'=>$this_template,'organization_id'=>$user->organization_id])->first();
//            $this->authorize('view', $template);
            $filename= $template->value;
        }

        $sub = explode(".", $filename);
        $token = explode("/", $sub[0]);

        return response()->json(['download_token' => $token[1]]);
    }

    // get template instruction by type
    public function templateInstruction(){
        if($this->type ==1) {
            $instruction_template='instruction-sponsorship-template';
        }else{
            $instruction_template='instruction-aid-template';
        }
        return response()->json(['download_token' => $instruction_template]);
    }

    // get default template by type
    public function defaultTemplate(){
        if($this->type == 1){
            $default_template='Sponsorship-default-template';
        }else{
            $default_template='aid-default-template';
        }

        return response()->json(['download_token' => $default_template]);
    }


}
