<?php

namespace Common\Controller\Categories;

use Common\Model\Categories\AbstractCategories;
use Common\Model\Categories\AidsCategories;
use Common\Model\Categories\CategoriesForm;
use Setting\Model\Setting;
use Forms\Model\CustomForms;
use Illuminate\Http\Request;
use App\Http\Helpers;


class AidsCategoriesController extends AbstractCategoriesController
{
    protected $type = AbstractCategories::TYPE_AIDS;


    // initialize AidsCategories model object to use in process
    protected function getModel()
    {
        return new AidsCategories();
    }

    // get AidsCategories model using id
    protected function findOrFail($id)
    {
        $aid = AidsCategories::findOrFail($id);
        if ($aid->type != $this->type) {
            throw (new ModelNotFoundException())->setModel(get_class($aid));
        }

        $aid->form_sections = \Common\Model\CategoriesFormsSections::getCategoryRequiredSections($id,$this->type);
        $aid->documents = \Common\Model\CategoriesDocument::getCategoryRequiredDocument($id);

        return $aid;

    }

    // get all AidsCategories rows
    public function index(Request $request)
    {
        $this->authorize('manage', AidsCategories::class);
        return parent::index($request);
    }

    // get forms of AidsCategories rows
    public function forms(Request $request)
    {
//        $this->authorize('manage', AidsCategories::class);
        return parent::forms($request);
    }

    // set form of AidsCategories rows
    public function setForm(Request $request, $id)
    {
//        $this->authorize('manage', AidsCategories::class);
        return parent::setForm($request,$id);
    }
    
    // set form status of AidsCategories rows
    public function setFormStatus(Request $request, $id)
    {
//        $this->authorize('manage', AidsCategories::class);
        return parent::setFormStatus($request,$id);
    }
    // create new object model
    public function store(Request $request)
    {
//        $this->authorize('create', AidsCategories::class);
        return parent::store($request);

    }

    // get existed object model by id
    public function show($id)
    {
//        $this->authorize('view', AidsCategories::class);
        return parent::show($id);
    }

    // get existed category template model by id
    public function getCategoriesTemplate(Request $request)
    {
        //        $this->authorize('upload', AidsCategories::class);
        return parent::getCategoriesTemplate($request);
    }

    // get existed category template model by id
    public function getCategory(Request $request)
    {
        return response()->json(AidsCategories::where(['type'=> $this->type])->orderBy('name')->get());
    }

    // get existed category elements model by id
    public function getElements($id)
    {
        $this->authorize('manageCategoriesFormElement', AidsCategories::class);
        return parent::getElements($id);
    }

    // get existed category elements priority by id
    public function getPriority(Request $request)
    {
        return parent::getPriority($request);
    }

    // get existed object name by id
    public function getCategoryName($id)
    {
        return parent::getCategoryName($id);
    }

    // get existed category documents by id
    public function getCategoryDocument($id)
    {
        return parent::getCategoryDocument($id);
    }

    // get section for each category (generic)
    public function getSectionForEachCategory(Request $request)
    {
        return response()->json(['Categories' =>$this->getModel()->getSectionForEachCategory(false)]);
    }

    // get section for category by id
    public function getCategoryFormsSections(Request $request,$id)
    {
        $user = \Auth::user();
        $has_custom=false;
        
        $org_form = CategoriesForm::where(['category_id'=>$id,'organization_id'=>$user->organization_id])->first();
        
        if(!is_null($org_form)){
            
            if($org_form->status == 0){
                $has_custom=true;
            }
            
        }else{
            $form = \Setting\Model\Setting::where(['id'=>'aid_custom_form','organization_id'=>$user->organization_id])->first();

            if($form){
                $ActiveForm=CustomForms::where(function ($q) use($form){
                    $q->where('id','=',$form->value);
                    $q->where('status','=',1);
                })->first();

                if($ActiveForm){
                    $has_custom=true;
                }
            }
        }        
        
        return response()->json(['FormSection' =>$this->getModel()->getCategoriesFormSections($id,$has_custom,$request->get('mode'))]);
    }

    // update existed category elements model by id
    public function updateElement(Request $request,$id)
    {
        $this->authorize('updateCategoriesFormElement', AidsCategories::class);
        return parent::updateElement($request,$id);
    }

    // upload category forms template
    public function templateUpload(Request $request)
    {
//        $this->authorize('upload', AidsCategories::class);
        return parent::templateUpload($request);
    }

    // get category forms template
    public function template($id,Request $request)
    {
//        $this->authorize('download', AidsCategories::class);
        return parent::template($id,$request);
    }

    // get template instruction
    public function templateInstruction()
    {
        return parent::templateInstruction();
    }

    // get default template by type
    public function defaultTemplate()
    {
        return parent::defaultTemplate();
    }

}