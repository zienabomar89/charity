<?php

namespace Common\Controller\Categories;

use Common\Model\Categories\AbstractCategories;
use Common\Model\Categories\SponsorshipCategories;
use Common\Model\Categories\CategoriesForm;
use Forms\Model\CustomForms;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Helpers;

class SponsorshipCategoriesController  extends AbstractCategoriesController
{
    protected $type = AbstractCategories::TYPE_SPONSORSHIPS;

    // initialize SponsorshipCategories model object to use in process
    protected function getModel()
    {
        return new SponsorshipCategories();
    }

    // get SponsorshipCategories model using id
    protected function findOrFail($id)
    {
        $sponsorship = SponsorshipCategories::findOrFail($id);
        if ($sponsorship->type != $this->type) {
            throw (new ModelNotFoundException())->setModel(get_class($sponsorship));
        }

        $sponsorship->form_sections = \Common\Model\CategoriesFormsSections::getCategoryRequiredSections($id,$this->type);
        $sponsorship->documents = \Common\Model\CategoriesDocument::getCategoryRequiredDocument($id);

        return $sponsorship;

    }

    // get all SponsorshipCategories rows
    public function index(Request $request)
    {

        $this->authorize('manage', SponsorshipCategories::class);
        return parent::index($request);
   }

  // get forms of SponsorshipCategories rows
    public function forms(Request $request)
    {
//        $this->authorize('manage', SponsorshipCategories::class);
        return parent::forms($request);
   }
  // set form of SponsorshipCategories rows
    public function setForm(Request $request, $id)
    {
//        $this->authorize('manage', SponsorshipCategories::class);
        return parent::setForm($request,$id);
   }
   
    // set form status of SponsorshipCategories rows
    public function setFormStatus(Request $request, $id)
    {
//        $this->authorize('manage', SponsorshipCategories::class);
        return parent::setFormStatus($request,$id);
    }

    // create new object model
    public function store(Request $request)
    {
        $this->authorize('create', SponsorshipCategories::class);
        return parent::store($request);
    }

    // get existed object model by id
    public function show($id)
    {
//        $this->authorize('view', SponsorshipCategories::class);
        return parent::show($id);
    }

    // get existed category template model by id
    public function getCategoriesTemplate(Request $request)
    {
        $this->authorize('upload', SponsorshipCategories::class);
        return parent::getCategoriesTemplate($request);
    }

    // get existed category template model by id
    public function getCategory(Request $request)
    {
        return response()->json(SponsorshipCategories::where(['type'=> $this->type])->orderBy('name')->get());
    }

    // get existed category elements model by id
    public function getElements($id)
    {
        $this->authorize('manageCategoriesFormElement', SponsorshipCategories::class);
        return parent::getElements($id);
    }

    // get existed category elements priority by id
    public function getPriority(Request $request)
    {
        return parent::getPriority($request);
    }

    // get existed object name by id
    public function getCategoryName($id){
        return parent::getCategoryName($id);
    }

    // get existed category documents by id
    public function getCategoryDocument($id){
        return parent::getCategoryDocument($id);
    }

    // get section for each category (generic)
    public function getSectionForEachCategory(Request $request)
    {
       return response()->json(['Categories' =>$this->getModel()->getSectionForEachCategory(false)]);
    }

    // get section for category by id
    public function getCategoryFormsSections(Request $request,$id){
        $user = \Auth::user();
        $has_custom=false;
        
        $category = $this->findOrFail($id);       
        $org_form = CategoriesForm::where(['category_id'=>$id,'status'=>0,'organization_id'=>$user->organization_id])->first();
        
        if($category->family_structure == 1){
               
            if(!is_null($org_form)){
               $has_custom=true;
            }
            else{
                $form = \Setting\Model\Setting::where(['id'=>'sponsorship_custom_form','organization_id'=>$user->organization_id])->first();

                if($form){
                    $ActiveForm=CustomForms::where(function ($q) use($form){
                        $q->where('id','=',$form->value);
                        $q->where('status','=',1);
                    })->first();

                    if($ActiveForm){
                        $has_custom=true;
                    }
                }
            }
        
        }  
        
        return response()->json(['FormSection' =>$this->getModel()->getCategoriesFormSections($id,$has_custom,$request->get('mode'))]);
    }

    // get existed for some person to category documents by category_id , case_id , person
    public function getCategoryDocuments(Request $request){

        if($request->get('category_id')){

        }
        $category_id=null;
        $person_id=null;
        $father_id  =null;
        $guardian_id=null;
        $mother_id=null;
        $mother_id=null;

        $case=null;
        $result=[];
        $documents=array();

        $target=$request->get('target');

        if($target == 'case'){
            $case=\Common\Model\CaseModel::fetch(array(),$request->get('case_id'));
            $documents= \Common\Model\CategoriesDocument::getPersonCategoryDocuments($case->category_id,$case->person_id,3,'edit',1);
        }

        if($target == 'common') {
            $category_id=$request->get('category_id');
            $persons=$request->get('person');
            foreach($persons as $key =>$value) {
                if(isset($value['id'])){
                    if($value['id'] != null){
                        $sub=\Common\Model\CategoriesDocument::getPersonCategoryDocuments($category_id,$value['id'],$value['scope'],'edit',1);
                        if(sizeof($sub) != 0){
                            foreach ($sub as $doc){
                                array_push($documents,$doc);
                            }
                        }
                    }
                }
            }
        }

        return response()->json(['documents' =>$documents]);
    }

    // get category forms template (sponsor)
    public function getSponsorTemplates($id,Request $request){
        $this->authorize('view', \Organization\Model\Sponsor::class);
        return response()->json(SponsorshipCategories::getSponsorTemplates($id));
    }

    // get category reports template (sponsor)
    public function getSponsorReports($id,Request $request){
        $this->authorize('view', \Organization\Model\Sponsor::class);
        return response()->json(SponsorshipCategories::getSponsorReports($id));
    }

    // update existed category elements model by id
    public function updateElement(Request $request,$id)
    {
        $this->authorize('updateCategoriesFormElement', SponsorshipCategories::class);
        return parent::updateElement($request,$id);
    }

    // upload category forms template
    public function templateUpload(Request $request)
    {
        if($request->get('sponsor')){
            $this->authorize('upload', \Organization\Model\Sponsor::class);
        }else{
            $this->authorize('upload', SponsorshipCategories::class);
        }
        return parent::templateUpload($request);
    }

    // get category forms template
    public function template($id,Request $request)
    {
//        $this->authorize('download', SponsorshipCategories::class);
        return parent::template($id,$request);
    }

    // get template instruction
    public function templateInstruction(){
        return parent::templateInstruction();
    }

    // get default template by type
    public function defaultTemplate(){
        return parent::defaultTemplate();
    }

}