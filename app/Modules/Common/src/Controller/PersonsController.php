<?php

namespace Common\Controller;

use App\Http\Controllers\Controller;
use Auth\Model\Setting;
use Common\Model\CloneGovernmentPersons;
use Common\Model\PersonDocument;
use Common\Model\PersonModels\PersonContact;
use Common\Model\PersonModels\PersonKinship;
use Common\Model\PersonModels\PersonEducation;
use Common\Model\PersonModels\PersonHealth;
use Common\Model\PersonModels\PersonIslamic;
use Common\Model\PersonModels\PersonResidence;
use Document\Model\File;
use Illuminate\Http\Request;
use Common\Model\Person;
use Common\Model\GovServices;
use Common\Model\ValidatorModel;
use Log\Model\CardLog ;
use Organization\Model\OrgLocations;
use Log\Model\Log;

class PersonsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // filter person according some filters
    public function index()
    {
        $entries = Person::fetch(array());
        return response()->json($entries);
    }

    // filter person according some filters
    public function getPersons(Request $request)
    {
        // public search and guardian search
//        $this->authorize('view', Cases::class);
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $user = \Auth::user();

        if ($request->id_card_number){
            CardLog::saveCardLog($user->id,$request->id_card_number);
        }

        return response()->json(Person::filterPersons($request->all()));
    }

    // save person record by id
    public function store(Request $request)
    {
        try {

            $father_kinship_id = 1;
            $mother_kinship_id = 5;
            $husband_kinship_id = 29;
            $wife_kinship_id = 21;
            $daughter_kinship_id = 22;
            $son_kinship_id = 2;

            $options = ['mode' =>'create'];

            if($request->get('no_en')){
                $options['no_en']=true;
            }

            if($request->get('person_id')){
                $options['mode']= 'update';
                $options['person_id']= $request->get('person_id');
            }

            if($request->input('relay') == true) {
                $options['aidsLocations']=true;
            }
            $rules=ValidatorModel::getPersonValidatorRules($request->get('priority'),$options);
            $person_alive=null;
            if($request->input('target') == 'father'){
                if($request->category_id){
                    $category=\Common\Model\Categories\SponsorshipCategories::findorfail($request->input('category_id'));
                    if($category->father_dead == 0){
                        $person_alive=0;
                    }else{
                        $person_alive=1;
                    }
                }

            }

            if(!$request->input('target') || $request->input('target') != 'father' ){
                if($request->get('alive') || $request->input('live')){
                    if($request->get('alive')){
                        $person_alive=$request->input('alive');
                    }

                    if($request->input('live')){
                        $person_alive=$request->input('live');
                    }

                }else{
                    $rules['death_date']='';
                    $rules['death_cause_id']='';
                }
            }

            if(!is_null($person_alive)){
                if($person_alive ==0){
                    $rules['death_date']='';
                    $rules['death_cause_id']='';
                }
                else if($person_alive ==1) {

                    $Bd=$request->input('birthday');
                    $Dd=$request->input('death_date');

                    if(isset($rules['birthday'])){
                        $old_1=$rules['birthday'];

                        if($Dd!=""&& $Dd!=null)
                        {
                            $rules['birthday'] = $old_1 . '|before:' . $Dd;
                        }else{
                            $rules['birthday'] = $old_1.'|before:' . date("Y-m-d", strtotime('tomorrow'));
                        }
                    }
                    if(isset($rules['condition'])){
                        $rules['condition']='';
                    }

                    if(isset($rules['death_date'])){
                        $old_2=$rules['death_date'];

                        if($Bd !=""&&$Bd!=null)
                        {
                            $rules['death_date'] = $old_2 .'|before:' . date("Y-m-d", strtotime('tomorrow')). '|after:' . $Bd;
                        }else{
                            $rules['death_date'] = $old_2.'|before:' . date("Y-m-d", strtotime('tomorrow'));
                        }
                    }
                }
            }

            if($request->input('outerEdit') == true) {
                $live=$request->input('live');
                if($live =='' || $live == null){
                    $rules['death_date']='';
                    $rules['death_cause_id']='';
                    $rules['condition'] ='';
                }else{
                    if($live ==0){
                        $rules['death_date']='';
                        $rules['death_cause_id']='';
                    }
                    if($live ==1){
                        $rules['condition'] ='';
                    }
                }
                $rules['WhoIsGuardian']='';
            }

            if($request->input('target') == 'guardian' && !$request->input('outerEdit')){
                if(isset($request->category_id)){
                    $category=\Common\Model\Categories\SponsorshipCategories::findorfail($request->input('category_id'));
                    if($category->father_dead == 1){
                        $rules['kinship_id']='';
                    }
                    if($category->case_is == 2){
                        $request->request->add(['target'=> 'case']);
//                    $request->request->add(['rel_kinship'=> 'same']);
                    }
                }

            }

            $error = \App\Http\Helpers::isValid($request->all(),$rules);
            if($error)
                return response()->json($error);

            $user = \Auth::user();
            $request->request->add(['organization_id'=> $user->organization_id]);
            $request->request->add(['user_id'=> $user->id]);

            $person =Person::savePerson($request->all());

            if($request->input('descendant') == true || $request->input('descendant') == true) {
                $card = $person['id_card_number'] ;
                $row = GovServices::byCard($card);
                if($row['status'] == true) {
                    $dt = $row['row'];
                    $map = GovServices::personDataMap($dt);
                    foreach ($map as $key =>$value){
                        $toUpdate[$key] = $value;
                    }

                    $toUpdate = [];
                    $toUpdate['has_commercial_records']=0;
                    $toUpdate['active_commercial_records']=0;
                    $toUpdate['gov_commercial_records_details']=  ' ' ;
                    $dt->commercial_data =[];
                    $CommercialRecords = GovServices::commercialRecords($card);
                    if($CommercialRecords['status'] != false){
                        $commercial_ =$CommercialRecords['row'];
                        $toUpdate['has_commercial_records'] = 1 ;
                        $gov_commercial_records_details = ' ' ;

                        foreach ($commercial_ as $ke_ => $v_){
                            if($v_->IS_VALID_DESC == "فعال"){
                                $toUpdate['active_commercial_records']++;
                            }

                            $gov_commercial_records_details .= '( ' . $v_->REGISTER_NO  . '   -   ' .
                                $v_->COMP_NAME  . '   -   ' .
                                $v_->START_DATE  . '   -   ' .
                                $v_->IS_VALID_DESC
                                .' )';
                        }
                        $toUpdate['gov_commercial_records_details'] = $gov_commercial_records_details;
                        $dt->commercial_data =$commercial_;
                    }

                    foreach ($dt->parent as $km => $prt) {
                        $parentMap = GovServices::inputsMap($prt);
                        $parentMap['l_person_id']= $person['id'];
                        $parentMap['l_person_update']= true;

                        if($prt->SEX_CD == 1){
                            $parentMap['kinship_id']= $father_kinship_id;
                        }else{
                            $parentMap['kinship_id']= $mother_kinship_id;
                        }

                        $parent = Person::savePerson($parentMap);
                        if($prt->SEX_CD == 1){
                            $toUpdate['father_id'] = $parent['id'];
                        }else{
                            $toUpdate['mother_id'] = $parent['id'];
                        }

                        $prt->IDNO = $prt->IDNO_RELATIVE;
                        CloneGovernmentPersons::saveNewWithRelation($card,$prt);
                    }

                    foreach ($dt->wives as $km => $wif) {
                        $wifeMap = GovServices::inputsMap($wif);
                        $wifeMap['l_person_id']= $person['id'];;
                        $wifeMap['l_person_update']= true;

                        if($dt->SEX_CD == 1){
                            $wifeMap['kinship_id']= $wife_kinship_id;
                        }else{
                            $wifeMap['kinship_id']= $husband_kinship_id;
                        }
                        $wife = Person::savePerson($wifeMap);

                        if($dt->SEX_CD == 2){
                            $toUpdate['husband_id']= $wife['id'];
                        }

                        $wif->IDNO = $wif->IDNO_RELATIVE;
                        CloneGovernmentPersons::saveNewWithRelation($card,$wif);
                    }

                    foreach ($dt->childrens as $km => $child) {
                        $childMap = GovServices::inputsMap($child);
                        $childMap['l_person_id']= $person['id'];;
                        $childMap['l_person_update']= true;

                        if($child->SEX_CD == 1){
                            $childMap['kinship_id']= $son_kinship_id;
                        }else{
                            $childMap['kinship_id']= $daughter_kinship_id;
                        }
                        Person::savePerson($childMap);
                        $child->IDNO = $child->IDNO_RELATIVE;
                        CloneGovernmentPersons::saveNewWithRelation($card,$child);
                    }

                    CloneGovernmentPersons::saveNew($dt);
                    Person::where('id',$person['id'])->update($toUpdate);
                }
            }
            $name = $person['full_name'];

            if($request->get('target') == 'father'){
                $target =trans('common::application.father_');
            }elseif($request->get('target') == 'mother'){
                $target = trans('common::application.mother_');
            }elseif($request->get('target') == 'guardian') {
                $target = trans('common::application.guardian_');
            }else{
                $target = trans('common::application.member');
            }

            if($request->get('person_id')){
                $action='PERSON_UPDATED';
                $message= trans('common::application.edited data'). ' ' . $target .'  : '.' "'.$name. ' " ';
            }else{
                $action='PERSON_CREATED';
                $message= trans('common::application.Added data') . ' ' . $target .'  : '.' "'.$name. ' " ';
            }
            Log::saveNewLog($action,$message);

            return response()->json(['status'=>'success','person'=>$person,'msg' => trans('common::application.action success')]);
        }
        catch (\Exception $e) {
        }

    }

    // get person record by id
    public function show($id)
    {
        $person = Person::fetch(array('id' => $id,));
        if (!$person) {
            //throw (new \Illuminate\Database\Eloquent\ModelNotFoundException)->setModel(Person::class);
            return response()->json(array(
                'error' => 'Not found!'
            ), 404);
        }
        return response()->json($person);
    }

    // find person using card number
    public function find(\Illuminate\Http\Request $request)
    {

//        try {
            ini_set('max_execution_time',0);
            set_time_limit(0);
            Ini_set('memory_limit','2048M');

            $card = $request->id_card_number;
            $user = \Auth::user();
            $block=false;
            $check=false;
            $category_check=false;
            $r_person=null;
            $individual_id=null;

            if($request->category_id){
                $check=true;
                if($request->get('target')){
                    $whoIsCase=\Common\Model\Categories\SponsorshipCategories::whoIsCase($request->get('category_id'));
                    if($whoIsCase =='guardian'){
                        $category_check=true;
                        $individual_id=$request->get('individual_id');
                    }
                }else{
                    $category_check=true;
                }

                if($category_check){
                    $block=\Common\Model\BlockCategories::isBlock($card,$request->get('category_id'));
                }
            }

            if($request->get('outer')){
                $individual_id=$request->get('individual_id');
            }

            if($block && $check){
                return response()->json(['status'=>false,'person'=>[],'blocked'=>true,'msg' =>trans('sponsorship::application.person was restriction add to selected category')]);
            }
            else{

             $per = Person::where(function ($q) use ($card){
                                $q->where('id_card_number',$card);
                                $q->orwhere('old_id_card_number',$card);
                            })->first();

                if($per) {

                    if(strstr($card, '700')){
                        if(strpos($card, '700') == 0 ){
                            if ($per->id_card_number != $card){
                                return response()->json(['status'=>false,'person'=>[],'blocked'=>true,
                                    'msg' =>trans('common::application.this is old card number , you should use the new instead of the old') . $per->id_card_number]);

                            }
                        }
                    }

                    if($request->get('r_person_case_id')){
                        $father =\Common\Model\CaseModel::findorfail($request->get('r_person_case_id'));
                        $r_person=$father->person_id;
                    }
                    if($request->get('r_person_id')){
                        $r_person=$request->get('r_person_id');
                    }

                    $person=Person::fetch(array(
                        'id' => $request->get('person_id'),
                        'id_card_number' => $card,
                        'category_id' => $request->get('category_id'),
                        'organization_id' => $user->organization_id,
                        'action'         => $request->get('mode'),
                        'full_name'      => $request->get('full_name'),
                        'person'         => $request->get('person'),
                        'persons_i18n'   => $request->get('persons_i18n'),
                        'contacts'       => $request->get('contacts'),
                        'work'           => $request->get('work'),
                        'education'      => $request->get('education'),
                        'health'         => $request->get('health'),
                        'islamic'        => $request->get('islamic'),
                        'residence'      => $request->get('residence'),
                        'cases'          => $request->get('cases'),
                        'wives'          => $request->get('wives'),
                        'banks'          => $request->get('banks'),
                        'home_indoor'    => $request->get('home_indoor'),
                        'financial_aid_source'     => $request->get('financial_aid_source'),
                        'non_financial_aid_source' => $request->get('non_financial_aid_source'),
                        'persons_properties' => $request->get('persons_properties'),
                        'persons_documents'  => $request->get('persons_documents'),
                        'case_sponsorships'  => $request->get('case_sponsorships'),
                        'case_payments'      => $request->get('case_payments'),
                        'father_id'     => $request->get('father_id'),
                        'mother_id'     => $request->get('mother_id'),
                        'father'        => $request->get('father'),
                        'mother'        => $request->get('mother'),
                        'family_payments'    => $request->get('family_payments'),
                        'r_person_id' => $r_person,
                        'l_person_id' => $request->get('l_person_id'),
                        'individual_id' => $individual_id
                    ));

                    if($category_check){
                        $category_type=\Common\Model\Categories\SponsorshipCategories::CatType($request->get('category_id'));
                        $person->has_image=PersonDocument::hasPersonalImagePath($person->person_id,$category_type);
                    }
                    if($request->get('category_id') && $person->case_id != null && $category_check) {

                        if(!is_null($person->death_date)){
                            return response()->json(['status'=>false,'person'=>[],'blocked'=>true,'msg' =>
                                trans('aid::application.You Can not add this case because the person was dead')]);
                        }

                        if($person->deleted_at != null){
                            return response()->json(['status'=>false,'person'=>[],'blocked'=>true,'msg' =>
                                trans('sponsorship::application.person was deleted go to deleted cases to restore it')]);

                        }else{
                            if($person->case_id != null){
                                $msg=trans('sponsorship::application.person was a case of selected category');

                            }else{
                                $cases=\Common\Model\AbstractCases::fatchCasesIds($person->id,null,$request->get('category_id'));

                                if(sizeof($cases) ==0){
                                    $msg=trans('sponsorship::application.person has been added as case to another organization');
                                }else{
                                    $msg=trans('sponsorship::application.person data is exist');
                                }
                            }
                        }

                    }
                    else{
                        $msg=trans('sponsorship::application.person data is exist');
                    }

                    if($category_check) {
                        $category_=\Common\Model\Categories\SponsorshipCategories::where('id',$request->category_id)->first();
                        if($category_->type == 2){
                            $passed = true;
                            $full_address = true;
                            $mainConnector=OrgLocations::getConnected($user->organization_id);

                            if(!is_null($person->adsdistrict_id)){
                                if(!in_array($person->adsdistrict_id,$mainConnector)){
                                    $passed = false;
                                }else{
                                    if(!is_null($person->adsregion_id)){
                                        if(!in_array($person->adsregion_id,$mainConnector)){
                                            $passed = false;
                                        }
                                        else{

                                            if(!is_null($person->adsneighborhood_id)){
                                                if(!in_array($person->adsneighborhood_id,$mainConnector)){
                                                    $passed = false;
                                                }else{

                                                    if(!is_null($person->adssquare_id)){
                                                        if(!in_array($person->adssquare_id,$mainConnector)){
                                                            $passed = false;
                                                        }else{
                                                            if(!is_null($person->adsmosques_id)){
                                                                if(!in_array($person->adsmosques_id,$mainConnector)){
                                                                    $passed = false;
                                                                }
                                                            }else{
                                                                $full_address = false;
                                                            }
                                                        }
                                                    }else{
                                                        $full_address = false;
                                                    }
                                                }
                                            }else{
                                                $full_address = false;
                                            }
                                        }
                                    }else{
                                        $full_address = false;
                                    }
                                }
                            }else{
                                $full_address = false;
                                $passed = true;
                            }

                            if($full_address){
                                if(!$passed){
                                    $category_id = $request->category_id;
                                    $organization_id = $user->organization_id;

                                    $list_org = \DB::table('char_organizations')
                                        ->whereIn('id',function ($w) use ($person,$category_id,$organization_id){
                                            $w->select('organization_id')
                                                ->from('char_cases')
                                                ->where(function ($q) use ($person,$category_id,$organization_id){
//                                          $q->whereIn('category_id' , function ($q_) use ($person,$category_id){
//                                               $q_->select('id')
//                                                   ->from('char_categories')
//                                                   ->where('char_categories.type',2);
//                                            });
                                                    $q->where('organization_id','!=',$organization_id);
                                                    $q->where('category_id',$category_id);
                                                    $q->where('person_id',$person->id);
                                                    $q->where('status',0);
                                                });
                                        })
                                        ->selectRaw('name')
                                        ->get();

                                    if(sizeof($list_org) > 0 ){
                                        $Orgs = [];
                                        foreach ($list_org as $key=>$value){
                                            $Orgs[] = $value->name;
                                        }
                                        $Orgs_ = implode($Orgs,' , ');

                                        return response()->json(['status'=>false,'person'=>[],
                                            'blocked'=>true,'msg' =>
                                                trans('common::application.person not on organization locations range , the person should go to') .
                                                ' ( ' . $Orgs_ . ' ) ' .
                                                trans('common::application.to_change_location')
                                        ]);
                                    }
                                }
                            }

                        }
                    }

                    $document_type_id = 16;
                    $person->photo_empty = true ;
                    $person->photo_url = null ;
                    $person->photo_src = 'internal' ;
                    $person->photo_update_date = null ;

                    $per_photo = PersonDocument::where(['person_id' => $person->id ,'document_type_id'=>$document_type_id])->first();
                    if(!is_null($per_photo)){
                        $file = File::findOrFail($per_photo->document_id);

                        if(!is_null($file)){
                            $person->photo_update_date = $file->updated_at ;
                            $image = base_path('storage/app/' . $file->filepath);
                            if (!is_file($image)) {
                                $person->photo_empty = false ;
                                $person->photo_url = base_path('storage/app/emptyUser.png') ;
                            }
                        }
                    }

                    return response()->json(['status'=>true,'person'=>$person,'blocked'=>false,'msg' =>$msg]);
                }
                else{
                    if (Setting::getIgnoreGovServices()){
                        return response()->json(['status'=>false,'person'=>[],'blocked'=>true,'msg' =>trans('common::application.unregistered_person')]);
                    }
                    else{
                        $row = GovServices::byCard($request->id_card_number);
                        $person = null;
                        if($row['status'] == true){
                            $record = $row['row'] ;
                            $person = Person::PersonMap($record);
                            $person['banks'] = [] ;
                            $person['photo_url'] = $record->photo_url ;
                            $person['photo_src'] = 'external' ;
                            $person['photo_update_date'] = $record->photo_update_date ;

                            $person['card_type']='1';
                            if (substr($request->id_card_number, 0, 1) === '7') {
                                $person['card_type']='2';
                            }

                            $recordWithDetails = GovServices::setCaseFormDetails($record,$person,$request->id_card_number);

                            $person = $recordWithDetails['person'];
                            CloneGovernmentPersons::saveNew((Object) $recordWithDetails['record']);

                            if(isset($request->category_id)){
                                if(isset($person['death_date'])){
                                    if(!is_null($person['death_date'])){
                                        return response()->json(['status'=>false,'person'=>[],'blocked'=>true,'msg' =>
                                            trans('aid::application.You Can not add this case because the person was dead')]);
                                    }
                                }
                            }

                            return response()->json(['status'=>false,'person'=>$person]);
                        }
                        return response()->json(['status'=>false,'msg'=>$row['message']]);
                    }
                }
            }

//        } catch (\Exception $e) {
//            return response()->json(['status'=>false,'msg'=> $e->getMessage()]);
//
//        }
    }

    // get some person details by id
    public function getPersonReports(\Illuminate\Http\Request $request)
    {
//        $this->authorize('view', Cases::class);

        $user = \Auth::user();
        $items=array();
        $sheetName=null;
        $r_person=null;
        $HeadTo=null;
        $father_kinship_id = 1;
        $mother_kinship_id = 5;
        $husband_kinship_id = 29;
        $wife_kinship_id = 21;
        $daughter_kinship_id = 22;
        $son_kinship_id = 2;

        $id=$request->get('person_id');

        if($request->get('target') =='info'){

            if($request->get('r_person_case_id')){
                $father =\Common\Model\AidsCases::findorfail($request->get('r_person_case_id'));
                $r_person=$father->person_id;
            }

            $response =Person::fetch(array(
                'id' => $id,
                'action' => $request->get('mode'),
                'full_name'     => $request->get('full_name'),
                'organization_id'     => $user->organization_id,
                'person'        => $request->get('person'),
                'persons_i18n'  => $request->get('persons_i18n'),
                'wives'      => $request->get('wives'),
                'contacts'      => $request->get('contacts'),
                'work'          => $request->get('work'),
                'education'     => $request->get('education'),
                'health'        => $request->get('health'),
                'islamic'        => $request->get('islamic'),
                'residence'     => $request->get('residence'),
                'cases'    => $request->get('cases'),
                'banks'         => $request->get('banks'),
                'home_indoor'   => $request->get('home_indoor'),
                'financial_aid_source'     => $request->get('financial_aid_source'),
                'non_financial_aid_source' => $request->get('non_financial_aid_source'),
                'persons_properties' => $request->get('persons_properties'),
                'persons_documents'  => $request->get('persons_documents'),
                'case_sponsorships'  => $request->get('case_sponsorships'),
                'case_payments'      => $request->get('case_payments'),
                'father_id'     => $request->get('father_id'),
                'mother_id'     => $request->get('mother_id'),
                'father'        => $request->get('father'),
                'mother'        => $request->get('mother'),
                'l_person_id'    => $request->get('l_person_id'),
                'individual_id'    => $request->get('individual_id'),
                'family_payments'    => $request->get('family_payments'),
                'r_person_id'    => $r_person
            ));

            if (!$response) {
                //throw (new \Illuminate\Database\Eloquent\ModelNotFoundException)->setModel(CaseModel::class);
                return response()->json(array('error' => 'Not found!'), 404);
            }

            if($request->citizen_wives == true){
                if($request->wives == true){

                    $wives = GovServices::byCardRelated($response->id_card_number,'wives');

                    $wives_not_inserted=[];
                    $exist=[];
                    if(sizeof($response->wives) > 0 ){
                        foreach ($response->wives as $key=>$value){
                            $exist[]= $value->id_card_number;
                        }
                        $not_insert = array_diff($wives['cards'],$exist);
                    }else{
                        $not_insert = $wives['cards'];
                    }

                    if(sizeof($not_insert) > 0 ){
                        foreach ($wives['rows']as $km => $wif) {
                            if(in_array($not_insert ,$wif->IDNO_RELATIVE)) {
                                $wifeMap = GovServices::inputsMap($wif);
//                                $wifeMap['l_person_id'] = $id;
//                                $wifeMap['l_person_update'] = true;

                                if ($response->gender == 1) {
//                                    $wifeMap['kinship_id'] = $wife_kinship_id;
                                } else {
//                                    $wifeMap['kinship_id'] = $husband_kinship_id;
                                }

                                $wife = Person::savePerson($wifeMap);

                                $wif->IDNO = $wif->IDNO_RELATIVE;
                                CloneGovernmentPersons::saveNewWithRelation($response->id_card_number, $wif);

                                $ifExist = Person::where(function ($q) use ($wif){
                                    $q->where('id_card_number',$wif->IDNO_RELATIVE);
                                })->first();

                                if($ifExist){
                                    $wifeMap['person_id']=$ifExist->id;
                                }

                                $wives_not_inserted[]=$wifeMap;
                            }
                        }
                    }

                    $response->wives_not_inserted = $wives_not_inserted;
                }
            }

            return response()->json($response);

        }
        elseif($request->get('target') =='cases'){
            $items=Person::filterPersonCases($id,$request->all());
            if($request->get('action') =='export') {
                $sheetName='case';
                $HeadTo='H';
            }
        }
        elseif($request->get('target') =='vouchers'){
            $items=\Common\Model\CaseModel::getCaseVouchers($request->all(),$user->organization_id);
            if($request->get('action') =='export') {
                $sheetName='case_vouchers_';
                $HeadTo='L';
            }
        }
        elseif($request->get('target') =='sponsorships'){
            $items=\Common\Model\CaseModel::getSponsorships($request->all());
            if($request->get('action') =='export') {
                $sheetName='case_sponsorships';
                $HeadTo='J';
            }
        }
        elseif($request->get('target') =='guardians' || $request->get('target') =='individuals'){
            $items=\Common\Model\CaseModel::getGuardians($request->all());
            if($request->get('action') =='export') {
                $sheetName='guardians';
                $HeadTo='L';
            }
        }
        elseif($request->get('target') =='payments'){
            if($request->get('sub_target') =='guardians'){
                $person_id=$request->get('guardian_id');
            }else{
                $person_id=$request->get('person_id');
            }
            $items=\Common\Model\CaseModel::getPaymentRecords($request->get('sub_target'),$request->all(),$person_id,null,null,$request->page,null,null);
            if($request->get('action') =='export') {
                $sheetName='case_payments';
                $HeadTo='P';
            }
        }
        elseif($request->get('target') =='brothers'){
            $items=Person::brothers($id);

            if($request->get('action') =='export') {
                $sheetName='brothers';
                $HeadTo='P';
            }else{
                $temp['brothers']=$items;
                $items=$temp;
            }
        }

        if($request->get('action') =='filters') {
            return response()->json($items);
        }

        return \Common\Model\CaseModel::createExcelToExport($items,$sheetName,$HeadTo);

    }

    // get person vouchers by card
    public function voucherFilter(Request $request)
    {
        return response()->json(Person::voucherFilter($request->all()));
    }

    // update person record by id
    public function update(Request $request, $id)
    {
        $this->validate($request, Person::getValidatorRules());
        $person = Person::findOrFail($id);
        $this->authorize('update', $person);
        
        $person->forceFill(array(
            'first_name' => $request->input('first_name'),
            'second_name' => $request->input('second_name'),
            'third_name' => $request->input('third_name'),
            'last_name' => $request->input('last_name'),
            'id_card_number' => $request->input('id_card_number'),
            'card_type' => $request->input('card_type'),
            'gender' => $request->input('gender'),
            'marital_status_id' => $request->input('marital_status_id'),
            'birthday' => $request->input('birthday'),
            'birth_place' => $request->input('birth_place'),
            'nationality' => $request->input('nationality'),
            'death_date' => $request->input('death_date'),
            'death_cause_id' => $request->input('death_cause_id'),
            'father_id' => $request->input('father_id'),
            'mother_id' => $request->input('mother_id'),
            'location_id' => $request->input('location_id'),
            'mosques_id' => $request->input('mosques_id'),
            'street_address' => $request->input('street_address'),
            'refugee' => $request->input('refugee'),
            'unrwa_card_number' => $request->input('unrwa_card_number'),
            'spouses' => $request->input('spouses'),
        ))->save();
        
        return response()->json($person);
    }

    // delete person record by id
    public function destroy(Request $request,$id)
    {
        $this->authorize('delete', \Common\Model\AidsCases::class);
        $response = array();
        $person = Person::fetch(array('id' => $id));
        $name=$person->first_name.' '.$person->second_name.' '.$person->third_name.' '.$person->last_name;

        if($request->l_person_id){
                PersonKinship::where(['l_person_id'=>$request->get('l_person_id'),'r_person_id'=>$id])->delete();
        }

        if($request->rel){
            PersonKinship::where(['l_person_id'=>$id])->delete();
            PersonKinship::where(['r_person_id'=>$id])->delete();
            $response["status"]= 'success';
            $response["msg"]= trans('setting::application.The row is deleted from db');
            return response()->json($response);
        }

        $cases = \Common\Model\CaseModel::where(['person_id'=>$id])->get();

        if(sizeof($cases) > 0 ){
            $response["status"]= 'failed';
            $response["msg"]= trans('setting::application.can not delete person because the person is case on system');
            return response()->json($response);
        }

        PersonContact::where(['person_id'=>$id])->delete();
        PersonEducation::where(['person_id'=>$id])->delete();
        PersonHealth::where(['person_id'=>$id])->delete();
        PersonIslamic::where(['person_id'=>$id])->delete();
        PersonResidence::where(['person_id'=>$id])->delete();

        if(Person::destroy($id))
        {
            Log::saveNewLog('PERSON_DELETED',trans('common::application.Deleted family member record') . ' "'.$name. '" ');
            $response["status"]= 'success';
            $response["msg"]= trans('setting::application.The row is deleted from db');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('setting::application.The row is not deleted from db');
        }
        return response()->json($response);
    }

}
