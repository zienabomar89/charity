<?php

namespace Common\Controller;

use App\Http\Controllers\Controller;
use Auth\Model\Setting;
use Common\Model\CaseModel;
use Common\Model\CloneGovernmentPersons;
use Common\Model\GovServices;
use Common\Model\Person;
use Common\Model\PersonModels\PersonKinship;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CasesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // paginate or export  individual according some filters
    public function individual(Request $request)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $individual = PersonKinship::individual($request->all());
        if($request->get('action') == 'ExportToExcel'){
            if(sizeof($individual) !=0){
                $data=array();
                foreach($individual as $key =>$value){
                    $data[$key][trans('aid::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        if(substr( $k, 0, 7 ) === "individual_"){
                            $data[$key][trans('aid::application.'.substr($k,11))." ( ".trans('aid::application.individual')  ." ) "]= str_replace("-","،",$v);
                        }else{
                            $data[$key][trans('aid::application.' . $k)]= str_replace("-"," ",$v) ;
                        }
                    }
                }
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($data) {
                    $excel->setTitle(trans('aid::application.voucher_name'));
                    $excel->setDescription(trans('aid::application.voucher_name'));
                    $excel->sheet(trans('aid::application.vouchers_sheet'), function($sheet) use($data) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);
                        $sheet->getStyle("A1:R1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->fromArray($data);
                    });
                })->store('xlsx', storage_path('tmp/'));
                return response()->json(['download_token' => $token]);
            }
        }
        return response()->json($individual);
    }

    // get case details according condition using id (case_id)
    public function show($id,Request $request)
   {
        $category_type= 2;
        if($request->get('category_type')){
            $category_type=$request->get('category_type');
        }

        $case =CaseModel::fetch(array(
            'action' => $request->get('action'),
            'category_type' => $category_type,
            'category'      => $request->get('category'),
            'full_name'     => $request->get('full_name'),
            'person'        => $request->get('person'),
            'persons_i18n'  => $request->get('persons_i18n'),
            'contacts'      => $request->get('contacts'),
            'work'          => $request->get('work'),
            'residence'     => $request->get('residence'),
            'case_needs'    => $request->get('case_needs'),
            'banks'         => $request->get('banks'),
            'home_indoor'   => $request->get('home_indoor'),
            'reconstructions' => $request->get('reconstructions'),
            'financial_aid_source'     => $request->get('financial_aid_source'),
            'non_financial_aid_source' => $request->get('non_financial_aid_source'),
            'persons_properties' => $request->get('persons_properties'),
            'persons_documents'  => $request->get('persons_documents'),
            'case_sponsorships'  => $request->get('case_sponsorships'),
            'case_payments'      => $request->get('case_payments'),
            'father_id'     => $request->get('father_id'),
            'mother_id'     => $request->get('mother_id'),
            'guardian_id'   => $request->get('guardian_id'),
            'father'        => $request->get('father'),
            'mother'        => $request->get('mother'),
            'guardian'      => $request->get('guardian'),
            'family_payments'    => $request->get('family_payments'),
            'guardian_payments'  => $request->get('guardian_payments')
        ),$id);
        if (!$case) {
                //throw (new \Illuminate\Database\Eloquent\ModelNotFoundException)->setModel(CaseModel::class);
            return response()->json(array(
                'error' => 'Not found!'
            ), 404);
        }
            //$this->authorize('view', $case);
        return response()->json($case);
    }

    // get case details according condition using id (case_id)
    public function getCaseReports(\Illuminate\Http\Request $request)
    {
    //        $this->authorize('view', Cases::class);

        $user = \Auth::user();
        $items=array();
        $sheetName=null;
        $HeadTo=null;
        $father_kinship_id = 1;
        $mother_kinship_id = 5;
        $husband_kinship_id = 29;
        $wife_kinship_id = 21;
        $daughter_kinship_id = 22;
        $son_kinship_id = 2;

        if($request->get('target') =='info'){
            $items =CaseModel::fetch(array(
                'action' => $request->get('mode'),
                'category_type'      => $request->get('category_type'),
                'category'      => $request->get('category'),
                'gender'      => $request->get('gender'),
                'family_member'      => $request->get('family_member'),
                'full_name'     => $request->get('full_name'),
                'visitor_note'        => $request->get('visitor_note'),
                'person'        => $request->get('person'),
                'persons_i18n'  => $request->get('persons_i18n'),
                'contacts'      => $request->get('contacts'),
                'education'      => $request->get('education'),
                'islamic'      => $request->get('islamic'),
                'health'      => $request->get('health'),
                'work'          => $request->get('work'),
                'residence'     => $request->get('residence'),
                'case_needs'    => $request->get('case_needs'),
                'banks'         => $request->get('banks'),
                'home_indoor'   => $request->get('home_indoor'),
                'reconstructions' => $request->get('reconstructions'),
                'financial_aid_source'     => $request->get('financial_aid_source'),
                'non_financial_aid_source' => $request->get('non_financial_aid_source'),
                'persons_properties' => $request->get('persons_properties'),
                'persons_documents'  => $request->get('persons_documents'),
                'case_sponsorships'  => $request->get('case_sponsorships'),
                'case_payments'      => $request->get('case_payments'),
                'father_id'     => $request->get('father_id'),
                'mother_id'     => $request->get('mother_id'),
                'guardian_id'   => $request->get('guardian_id'),
                'father'        => $request->get('father'),
                'mother'        => $request->get('mother'),
                'guardian'      => $request->get('guardian'),
                'family_payments'    => $request->get('family_payments'),
                'guardian_payments'  => $request->get('guardian_payments')
            ),$request->get('case_id'));

            if($items->marital_status_id != 10 ){
                if($request->citizen_target == 'aids'){
                    if($request->citizen == true){
                        $items->family_member_not_inserted = [];
                        if (!Setting::getIgnoreGovServices()){

                            $row = GovServices::byCard($items->id_card_number);
                            if($row['status'] == true) {
                                $dt = $row['row'];
                                $items->family_cnt = $dt->family_cnt ;
                                $items->family_count = $dt->family_cnt;
                                $items->spouses = $dt->spouses ;
                                $items->male_live = $dt->male_live ;
                                $items->female_live = $dt->female_live ;

                                $map = GovServices::personDataMap($dt);

                                $toUpdate = [];
                                foreach ($map as $key => $value) {
                                    $toUpdate[$key] = $value;
                                }

                                $exist = [];
                                $not_inserted_on_db = [];
                                $not_exist = [];

                                if (sizeof($items->family_member) > 0) {
                                    foreach ($items->family_member as $key => $value) {
                                        $card = $value->id_card_number;
                                        if(!in_array($card,$exist)) {
                                            if(is_null($value->death_date) &&
                                                (  $value->kinship_id == $wife_kinship_id ||
                                                    $value->kinship_id == $husband_kinship_id ||
                                                    (($value->kinship_id == $son_kinship_id || $value->kinship_id == $daughter_kinship_id) && $value->marital_status_id == 10)
                                                )) {
                                                $exist[] = $value->id_card_number;
                                            }
                                        }
                                    }
                                }

                                foreach ($dt->all_relatives as $key=> $value) {
                                    $card = $value->IDNO_RELATIVE;
                                    if(!in_array($card,$exist)) {
                                        $not_exist[] = $card;
                                        $map = $this->inputsMap($value);
                                        $map['kinship_id'] =$value->kinship_id;
                                        $map['kinship_name']=$value->kinship_name;
                                        $find=Person::where('id_card_number',$card)->first();
                                        if(!is_null($find)){
                                            $map['id'] = $find->id;
                                        }
                                        $not_inserted_on_db[]=$map;
                                    }
                                    CloneGovernmentPersons::saveNewWithRelation($items->id_card_number,$value);
                                }
                                $items->family_member_not_inserted=$not_inserted_on_db;
                                CloneGovernmentPersons::saveNew($dt);
                            }

                        }

                    }
                }
            }
            else{
                $items->family_cnt = 0 ;
                $items->spouses = 0 ;
                $items->male_live = 0 ;
                $items->female_live = 0 ;
            }

            if($items){
                $items->user_organization_id=$user->organization_id;
            }
        }
        elseif($request->get('target') =='case_vouchers'){
            $items=CaseModel::getCaseVouchers($request->all(),$user->organization_id);
            if($request->get('action') =='export') {
                $sheetName='case_vouchers_';
                $HeadTo='N';
            }
        }
        elseif($request->get('target') =='case_sponsorships'){
            $items=CaseModel::getSponsorships($request->all());
            if($request->get('action') =='export') {
                $sheetName='case_sponsorships';
                $HeadTo='J';
            }
        }
        elseif($request->get('target') =='guardians'){
            $items=CaseModel::getGuardians($request->all());
            if($request->get('action') =='export') {
                $sheetName='guardians';
                $HeadTo='L';
            }
        }
        elseif($request->get('target') =='case_payments'){
            $case=CaseModel::fetch(array(),$request->get('case_id'));
            $items=CaseModel::getPaymentRecords('case',$request->all(),$case->person_id,null,null,$request->page,$case->organization_id,null);
            if($request->get('action') =='export') {
                $sheetName='case_payments';
                $HeadTo='P';
            }
        }
        elseif($request->get('target') =='family_payments'){
            $case=CaseModel::fetch(['father_id'=>true,'mother_id'=>true,'category_type'=>1],$request->get('case_id'));
            $items=CaseModel::getPaymentRecords('family',$request->all(),$case->person_id,$case->father_id,$case->mother_id,$request->page,$case->organization_id,null);
            if($request->get('action') =='export') {
                $sheetName='family_payments';
                $HeadTo='P';
            }
        }
        elseif($request->get('target') =='guardian_payments'){
            $case=CaseModel::fetch(['guardian'=>true,'guardian_id'=>true],$request->get('case_id'));
            $items=CaseModel::getPaymentRecords('guardian',$request->all(),$case->guardian_id,null,null,$request->page,$case->organization_id,null);

            if($request->get('action') =='export') {
                $sheetName='guardian_payments';
                $HeadTo='P';
            }else{
                $items['guardian']=$case->guardian_id;
                $items['guardian_name']=$case->guardian_name;
            }
        }

        if($request->get('action') =='filters') {
            return response()->json($items);
        }

        if($request->get('sub_target') =='family_member') {
            $sheetName='family_member';
            $HeadTo='H';
            $items=$items->family_member;
        }

        return CaseModel::createExcelToExport($items,$sheetName,$HeadTo);

    }

    function inputsMap($data){

        $row = array("death_date"=>null,'card_type'=>"1");
        $_translator =array(
            "IDNO_RELATIVE" =>  "id_card_number",
            "FNAME_ARB" =>  "first_name",
            "SNAME_ARB" =>  "second_name",
            "TNAME_ARB" => "third_name",
            "LNAME_ARB" =>  "last_name",
            "PREV_LNAME_ARB" =>  "prev_family_name",
            "ENG_NAME" =>  "en_name",
            "SEX_CD" => "gender",
            "SOCIAL_STATUS_CD" =>  "marital_status_id",
            "BIRTH_DT" =>  "birthday",
            "DETH_DT" =>  "death_date",
            "CI_CITY" =>  "governarate_name",
            "RELATIVE_CD" =>  "kinship",
            "MOBILE" =>  "primary_mobile",
            "TEL" =>  "phone",
        );

        $dates = ['birthday' , 'death_date'];
        $SEX_CD_MAP = ['531'=> 1 ,'532'=> 2];
        $SOCIAL_STATUS_MAP = ['526'=> 20 ,'525'=> 10 ,'528' => 30 , '529'=>40 , '527' =>21];

        foreach ($data as $k=>$v) {

            if(isset($_translator[$k])){
                if($k == 'ENG_NAME'){
                    $parts = explode(" ",$v);
                    $row['en_first_name'] = $parts[0];
                    $row['en_second_name'] = $parts[1];
                    $row['en_third_name'] = $parts[2];
                    $row['en_last_name'] = $parts[3];
                }
                elseif($k == 'SEX_CD'){
                    if(isset($SEX_CD_MAP[$v])){
                        $row [$_translator[$k]]=$SEX_CD_MAP[$v];
                    }else{
                        $row [$_translator[$k]]=$v;
                    }
                }
                elseif($k == 'SOCIAL_STATUS_CD'){
                    $row [$_translator[$k]]=$SOCIAL_STATUS_MAP[$v];
                }
                elseif(in_array($_translator[$k],$dates)){
                    if(!is_null($v) && $v != " "){
                        //                    $row[$_translator[$k]]=date('Y-m-d',strtotime($v));
                        $row[$_translator[$k]]=date('Y-m-d',strtotime(str_replace("/","-",$v)));
                    }
                }
                else{
                    $row [$_translator[$k]]=$v;
                }
            }
        }

        if(isset($row['DETH_DT'])){
            if(!is_null($row['DETH_DT'])){
                $row ['marital_status_id']=4;
            }
        }

        $street_address = '';
        if(isset($row['city_name']) || isset($row['governarate_name'])  || isset($row['street_address'])){
            if(isset($row['governarate_name'])){
                $street_address = $row['governarate_name'];
            }

            if(isset($row['city_name'])){
                if(isset($row['governarate_name'])){
                    $street_address .= ' - ' . $row['city_name'];
                }else{
                    $street_address .= $row['city_name'];
                }
            }
            if(isset($row['street_address'])){
                if(isset($row['city_name']) || isset($row['governarate_name'])){
                    $street_address .= ' - ' . $row['street_address'];
                }else{
                    $street_address .= $row['street_address'];
                }
            }
            if(isset($row['governarate_name'])){ unset($row['governarate_name']); }
            if(isset($row['city_name'])){ unset($row['city_name']); }

            $row['street_address'] = $street_address;
        }

        return $row;
    }


}
