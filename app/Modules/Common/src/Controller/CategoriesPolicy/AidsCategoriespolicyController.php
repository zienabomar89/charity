<?php

namespace Common\Controller\CategoriesPolicy;

use Common\Model\AbstractCategoriespolicy;
use Common\Model\AidsCategoriespolicy;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AidsCategoriespolicyController extends AbstractCategoriespolicyController
{
    protected $target = AbstractCategoriespolicy::TARGET_AIDS;


    // init new model object
    protected function getModel()
    {
        return new AidsCategoriespolicy();
    }

    // get AidsCategoriespolicy model using id
    protected function findOrFail($id)
    {
        $aid = AidsCategoriespolicy::findOrFail($id);
        $obj= \Common\Model\Categories\AidsCategories::findOrFail($aid->category_id);
        if ($obj->type != $this->target) {
            throw (new ModelNotFoundException())->setModel(get_class($aid));
        }
        return $aid;
    }

    // paginate aid categories policy
    public function index()
    {
        $this->authorize('manage', AidsCategoriespolicy::class);
        return parent::index();
    }

    // create new object model
    public function store(Request $request)
    {
        $this->authorize('create', AidsCategoriespolicy::class);
        return parent::store($request);

    }

    // get existed object model by id
    public function show($id)
    {
        //        $this->authorize('view', AidsCategoriespolicy::class);
        return parent::show($id);
    }

}