<?php

namespace Common\Controller\CategoriesPolicy;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Common\Model\AbstractCategoriespolicy;

class AbstractCategoriespolicyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
//        return response()->json(AbstractCategoriespolicy::getData($this->target,$request->all()));
    }

    // get existed policy  by target and other options
    public function getPolicy(Request $request)
    {
        return response()->json(AbstractCategoriespolicy::getData($this->target,$request->all()));
    }

    // create new object model
    public function store(Request $request)
    {

            $response = array();
            $input=[
                'category_id' => $request->get('category_id'),
                'amount' => strip_tags($request->get('amount')),
                'level' => strip_tags($request->get('level')),
                'count_of_family' => strip_tags($request->get('count_of_family')),
            ];

            $rules= [
                'category_id' => 'required',
                'amount' => 'integer',
                'level' => 'required|integer',
                'count_of_family' => 'integer',
            ];

            $checked=$request->get('checkedAll');
            if(!$checked){
                $input['started_at']= strip_tags($request->get('started_at'));
                $input['ends_at']= strip_tags($request->get('ends_at'));
                $rules['started_at']= 'date|before:'.$input['ends_at'];
                $rules['ends_at']= 'date|after:'.$input['started_at'];
            }


            $error = \App\Http\Helpers::isValid($input,$rules);
            if($error)
                return response()->json($error);

            if(!$checked){
                $input['started_at']= date('Y-m-d',strtotime($input['started_at']));
                $input['ends_at']= date('Y-m-d',strtotime($input['ends_at']));
            }else{
                $input['started_at']= date('Y-01-01');
                $input['ends_at']=date('Y-12-31');
            }

            $total=sizeof($input['category_id']);
            $insert=0;
            $failed=0;
            $exist=0;

             foreach($input['category_id'] as $item){
                $count=$this->getModel()->where(['category_id'=>$item,'level'=>$input['level'],'type'=>$this->target])->count();
                if($count !=1) {
                    $inserted=$this->getModel();
                    $inserted->category_id=$item;
                    $inserted->level=$input['level'];
                    $inserted->type=$this->target;
                    $inserted->amount=$input['amount'];
                    $inserted->started_at=$input['started_at'];
                    $inserted->ends_at=$input['ends_at'];
                    $inserted->count_of_family=$input['count_of_family'];
                    if($inserted->save())
                    {
                        $insert++;
                    }else{
                        $failed++;
                    }
                }else{
                    $exist++;
                }
            }

            if($insert != 0){

                if ($this->target == \Common\Model\AbstractCategoriespolicy::TARGET_SPONSORSHIPS) {
                   
                    $message= trans('common::application. He added a constraint for the new sponsorships types');
                }else{
                    $message= trans('common::application. He added a constraint to the new aids types');
                }

                \Log\Model\Log::saveNewLog('CATEGORY_POLICY_CREATED',$message);

                if($total == $insert){
                     $response["status"]= 'success';
                    $response["msg"]= trans('setting::application.The row is inserted to db');
                }else{

                    $response["status"]= 'success';
                    $response["msg"]= trans('common::application.Some constraints have been added and others already exist') ;
                }

            }else{

                if($total ==$failed){
                    $response["status"]= 'failed';
                    $response["msg"]= trans('setting::application.The row is not insert to db');
                }

                if($total ==$exist){
                    $response["status"]= 'exist';
                    $response["msg"]= trans('common::application.All existing constraints already exist') ;
                }
            }



            return response()->json($response);

    }

    // get existed object model by id
    public function show($id)
    {
        $obj=$this->findOrFail($id);
//            $this->authorize('view', $obj);
        return $this->findOrFail($id);
    }

    // update existed object model by id
    public function update(Request $request, $id)
    {
        $response = array();

        $target=$this->findOrFail($id);
        $this->authorize('update', $target);

        $input=['category_id' => strip_tags($request->get('category_id')),
            'amount' => strip_tags($request->get('amount')),
            'level' => strip_tags($request->get('level')),
            'count_of_family' => strip_tags($request->get('count_of_family')),
        ];

        $rules= [
            'category_id' => 'required|integer',
            'amount' => 'integer',
            'level' => 'required|integer',
            'count_of_family' => 'integer',
        ];

        $checked=$request->get('checkedAll');
        if(!$checked){
            $input['started_at']= strip_tags($request->get('started_at'));
            $input['ends_at']= strip_tags($request->get('ends_at'));
            $rules['started_at']= 'date|before:'.$input['ends_at'];
            $rules['ends_at']= 'date|after:'.$input['started_at'];
        }

        $error = \App\Http\Helpers::isValid($input,$rules);
        if($error)
            return response()->json($error);


        if(!$checked){
            $input['started_at']= date('Y-m-d',strtotime($input['started_at']));
            $input['ends_at']= date('Y-m-d',strtotime($input['ends_at']));
        }else{
            $input['started_at']= date('Y-01-01');
            $input['ends_at']=date('Y-12-31');
        }

        $type=$this->target;
        $count=$this->getModel()->where(function ($q) use ($input,$id,$type) {
            $q->where('category_id', '=',$input['category_id']);
            $q->where('level', '=',$input['level']);
            $q->where('type', '=',$type);
            $q->where('id', '<>',$id);
        })->count();

        if($count ==1) {
            $response["status"]= 'failed';
            $response["msg"]= trans('aid::application.This policy is already exists');
        }else{
            $updated = $this->findOrFail($id);
            $updated->amount=$input['amount'];
//                $updated->category_id=$input['category_id'];
            $updated->level=$input['level'];
            $updated->started_at=$input['started_at'];
            $updated->ends_at=$input['ends_at'];
            $updated->count_of_family=$input['count_of_family'];

            if($updated->save())
            {
                $category = \Common\Model\Categories\AidsCategories::where('id',$input['category_id'])->first();

                \Log\Model\Log::saveNewLog('CATEGORY_POLICY_UPDATED',trans('common::application.Edited constraint row for') . ' " '.$category->name  . ' " ');

                $response["status"]= 'success';
                $response["msg"]= trans('setting::application.The row is updated');
            }else{
                $response["status"]= 'failed';
                $response["msg"]= trans('setting::application.There is no update');
            }

        }
        return response()->json($response);
    }

    // delete existed object model by id
    public function destroy($id,Request $request)
    {
        $policy = \Common\Model\AidsCategoriespolicy::where('id',$id)->first();
        $this->authorize('delete', $policy);
        $category = \Common\Model\Categories\AidsCategories::where('id',$policy->category_id)->first();

        if($policy->delete())
        {
            \Log\Model\Log::saveNewLog('CATEGORY_POLICY_DELETED',trans('common::application.Delete constraint row for') . ' " '.$category->name  . ' " ');
            $response["status"]= 'success';
            $response["msg"]= trans('setting::application.The row is deleted from db');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('setting::application.The row is not deleted from db');

        }
        return response()->json($response);

    }



}
