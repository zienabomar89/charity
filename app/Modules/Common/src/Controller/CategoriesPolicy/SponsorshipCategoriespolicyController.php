<?php

namespace Common\Controller\CategoriesPolicy;

use Common\Model\AbstractCategoriespolicy;
use Common\Model\SponsorshipCategoriespolicy;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SponsorshipCategoriespolicyController extends AbstractCategoriespolicyController
{
    protected $target = AbstractCategoriespolicy::TARGET_SPONSORSHIPS;

    // init new model object
    protected function getModel()
    {
        return new SponsorshipCategoriespolicy();
    }

    // get SponsorshipCategoriespolicy model using id
    protected function findOrFail($id)
    {
        $aid = SponsorshipCategoriespolicy::findOrFail($id);
        $obj= \Common\Model\SponsorshipCategoriespolicy::findOrFail($aid->category_id);
        if ($obj->type != $this->target) {
            throw (new ModelNotFoundException())->setModel(get_class($aid));
        }
        return $aid;
    }

    // paginate sponsorship categories policy
    public function index()
    {
        $this->authorize('manage', SponsorshipCategoriespolicy::class);
        return parent::index();
    }

    // create new object model
    public function store(Request $request)
    {
        $this->authorize('create', SponsorshipCategoriespolicy::class);
        return parent::store($request);

    }

    // get existed object model by id
    public function show($id)
    {
//        $this->authorize('view', SponsorshipCategoriespolicy::class);
        return parent::show($id);
    }

}