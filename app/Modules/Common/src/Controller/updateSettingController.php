<?php
namespace Common\Controller;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Common\Model\UpdateDataSetting;
use Illuminate\Support\Facades\Auth;

class updateSettingController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // paginate update data setting
    public function index(Request $request)
    {
        $this->authorize('manage',UpdateDataSetting::class);
        $user = Auth::user();
        return response()->json( UpdateDataSetting::getDate($user->organization_id,$request->get('type'),$request->page,$request->itemsCount));
    }

    // save new update data setting
     public function store(Request $request)
    {
        $this->authorize('create',UpdateDataSetting::class);
        $response = array();

        $input=[
            'type' => $request->get('type'),
            'target_id' => $request->get('target_id'),
            'updated_period' => $request->get('updated_period'),
            'allowed_period' => $request->get('allowed_period')
        ];

        $rules= [
            'type' => 'required',
            'target_id' => 'required',
            'updated_period' => 'required',
            'allowed_period' => 'required'
        ];

        $exist=UpdateDataSetting::where(['type' =>$input['type'],'target_id' =>$input['target_id']])->count();
        if($exist !=0){
            $response["status"]= 'failed_valid';
            $response["msg"]['target_id']=[trans('common::application.this value is previously used')];
            return response()->json($response);
        }

        $error = \App\Http\Helpers::isValid($input,$rules);
        if($error)
            return response()->json($error);

        $user = Auth::user();
        $input['organization_id']=$user->organization_id;

        if(UpdateDataSetting::create($input))
        {
            $message=trans('common::application.Added data update setting by');

            if($input['type'] ==0){
                $message=$message.' '. trans('common::application.sponsor_name') ;
            }elseif($input['type'] ==1){
                $message=$message.' '.trans('common::application.ADDRESS')  ;
            }else{
                $message=$message.trans('common::application.request_category') ;
            }

            \Log\Model\Log::saveNewLog('UPDATED_DATA_SETTING_CREATED', $message);


            $response["status"]= 'success';
            $response["msg"]= trans('setting::application.The row is inserted to db');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('setting::application.The row is not insert to db');

        }
        return response()->json($response);

    }

    // get update data setting by id
    public function show($id)
    {
        $obj=UpdateDataSetting::findOrFail($id);
        $this->authorize('view',$obj);
        return response()->json($obj);
    }

    // update the record of update data setting by id
    public function update(Request $request, $id)
    {

        $obj=UpdateDataSetting::findOrFail($id);
        $this->authorize('update',$obj);

        $response = array();
        $input=[
            'updated_period' => (int)$request->get('updated_period'),
            'allowed_period' => (int)$request->get('allowed_period')
        ];

        $rules= [
            'updated_period' => 'required',
            'allowed_period' => 'required'
        ];

        $error = \App\Http\Helpers::isValid($input,$rules);
        if($error)
            return response()->json($error);


        $obj= UpdateDataSetting::findorfail($id);
        $obj->updated_period=$input['updated_period'];
        $obj->allowed_period=$input['allowed_period'];

        if($obj->save())
        {

            $message= trans('setting::application.Edit Setup Update Data Record by') ;

            if($obj->type ==0){
                $message=$message.' '.trans('setting::application.sponsor_name');
            }elseif($obj->type ==1){
                $message=$message.' '.trans('setting::application.ADDRESS');
            }else{
                $message=$message.trans('setting::application.request_category');
            }

            \Log\Model\Log::saveNewLog('UPDATED_DATA_SETTING_UPDATED', $message);

            $response["status"]= 'success';
            $response["msg"]= trans('setting::application.The row is updated');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('setting::application.There is no update');
        }
        return response()->json($response);

    }

    // delete update data setting by id
    public function destroy($id,Request $request)
    {
        $obj=UpdateDataSetting::findOrFail($id);
        $this->authorize('update',$obj);
        $response = array();
        $type=$obj->type;

        if(UpdateDataSetting::destroy($id)) {
            $message= trans('common::application."Deleted Setup Update Data By record"');

            if($type ==0){
                $message .=' '. trans('common::application.sponsor_name');
            }elseif($type ==1){
                $message .=' '.trans('common::application.ADDRESS');
            }else{
                $message .=' '.trans('common::application.request_category');
            }

            \Log\Model\Log::saveNewLog('UPDATED_DATA_SETTING_DELETED', $message);

            $response["status"]= 'success';
            $response["msg"]= trans('setting::application.The row is deleted from db');
        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('setting::application.The row is not deleted from db');
        }
        return response()->json($response);
    }
   
}