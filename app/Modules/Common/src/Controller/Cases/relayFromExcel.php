<?php

namespace Common\Controller\Cases;

use App\Http\Controllers\Controller;
use Auth\Model\User;
use Common\Model\AidsCases;
use Common\Model\BlockIDCard;
use Common\Model\CaseModel;
use Common\Model\CloneGovernmentPersons;
use Common\Model\Person;
use Common\Model\PersonModels\PersonI18n;
use Common\Model\GovServices;
use Common\Model\SponsorshipsCases;
use Illuminate\Http\Request;
use Common\Model\Relays\Relays;
use Excel;
use Organization\Model\OrgLocations;

class relayFromExcel  extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public static $_translator = [
        "rkm_alhoy" =>'id_card_number',
        "aldol" => "adscountry_id",
        "almhafth" => "adsdistrict_id",
        "almntk" => "adsregion_id",
        "alhy" => "adsneighborhood_id",
        "almrbaa"=> "adssquare_id",
        "almsjd" => "adsmosques_id",
        "tfasyl_alaanoan"=>'street_address',
        "rkm_joal_asasy"=>'primary_mobile',
        "rkm_alotny"=>'wataniya',
        "rkm_joal_ahtyaty"=>'secondary_mobile',
        "rkm_hatf"=>'phone'
    ];

    public static function filterInputs($data){

        $constant =  [
            'country', 'governarate', 'city', 'location_id','mosques_id',"adscountry_id",
            "adsdistrict_id", "adsmosques_id", "adsneighborhood_id", "adsregion_id", "adssquare_id"];

        $hasParent =  ['governarate', 'city', 'location_id','mosques_id',
                       "adsdistrict_id", "adsmosques_id", "adsneighborhood_id", "adsregion_id", "adssquare_id"];

        $Parents = ['governarate'=>'country', 'city' => 'governarate',
                    'location_id' => 'city', 'mosques_id' => 'location_id',
                    "adsdistrict_id" =>'adscountry_id', "adsregion_id" =>'adsdistrict_id',
                    "adsneighborhood_id" =>'adsregion_id', "adssquare_id" =>'adsneighborhood_id',
                    "adsmosques_id" =>'adssquare_id'];

        $ParentsNames = ['governarate'=>'aldol', 'city' => 'almhafth', 'location_id' => 'almdyn',
                        'mosques_id' => 'almntk',"adsdistrict_id" =>'aldol', "adsregion_id" =>'almhafth',
                         "adsneighborhood_id" =>'almntk', "adssquare_id" =>'alhy', "adsmosques_id" =>'almrbaa'];

        $numeric =  ['id_card_number','primary_mobile', 'phone'];

        $row=[];
        $translator =self::$_translator;
        $row['errors']=[];

        foreach ($data as $k=>$v) {
            $key=null;

            $key_=$k;

            if(isset($translator[$key_])){
                $key=$translator[$key_];
            }
            if(!is_null($key)){
               $pass = true ;
                if(in_array($key,$numeric)){
                    if(!is_null($v) && $v != " "){
                        if(!is_numeric($v)){
                            $pass = false ;
                            $row['errors'][$key]=trans('common::application.invalid_number');
                        }
                    }
                }


                if(in_array($key,$constant)){
                    $parent =null;
                    $isChild =false;
                    if(in_array($key,$hasParent)){
                        $isChild =true;
                        $parentKey=$Parents[$key];
                        $parentName=$ParentsNames[$key];
                        if(isset($row[$parentKey])){
                            $parent=$row[$parentKey];
                        }
                    }
                    $vlu = $v;
                    if(!is_null($vlu) && $vlu != '') {
                        $cons_val = CaseModel::constantId($key,$vlu,$isChild,$parent);
                        if(!is_null($cons_val)) {
                            $row[$key] = $cons_val;
                        }else{
                            $row['errors'][$key]=trans('common::application.invalid_constant');
                        }
                    }
                }
                elseif(in_array($key,$numeric)){
                    if(!is_null($v) && $v != " "){
                        if(is_numeric($v)){
                            $row[$key]=(int)$v;
                        }
                    }
                }else{
                    if(!is_null($v) && $v != " "){
                        $row[$key]=$v;
                    }
                }

                if($pass == false){
                   unset($row[$key]);
                }
            }

        }

        return $row;
    }

    public static function savePerson($input = array())
    {

        $en_language_id = 2;

        $person_Input = ['first_name', 'second_name', 'third_name', 'last_name', 'id_card_number',
                         'birthday', 'gender', 'marital_status_id', 'prev_family_name', 'death_date',
            "card_type", "adscountry_id", "adsdistrict_id","adsregion_id",
                          "adsneighborhood_id", "adssquare_id", "adsmosques_id", 'street_address'
                        ];

        $IntegerInput =  ['card_type','marital_status_id'];

        $insert=[];
        foreach ($person_Input as $var) {
            if(isset($input[$var])){
                if($input[$var] != null && $input[$var] != "") {
                    if(in_array($var, $IntegerInput)){
                        $insert[$var] =(int)$input[$var];
                    }else{

                        if ($var == 'birthday' || $var == 'death_date') {
                            $insert[$var] = date('Y-m-d', strtotime($input[$var]));
                        }else{
                            $insert[$var] =$input[$var];
                        }
                    }

                }else{
                    $insert[$var] = null;
                }
            }
        }

        $return=array();
        $person=null;


        $person_id = null;
        if(isset($input['id'])) {
            $person = Person::findorfail($input['id']);
        }
        else{
            $person = Person::where(['id_card_number' => (int) $insert['id_card_number']])->first();
            if(is_null($person)){
                $person = new Person();
           }
        }

        if(sizeof($insert) != 0){
            foreach ($insert as $key => $value) {
                $person->$key = $value;
            }
            $person->save();
        }

        if ($person) {
            $person_id = $person->id;
            $return['id']= $person->id;
            $return['id_card_number']= $person->id_card_number;
            $return['name'] =((is_null($person->first_name) || $person->first_name == ' ' ) ? ' ' : $person->first_name) . ' ' .
                                  ((is_null($person->second_name) || $person->second_name == ' ' ) ? ' ' : $person->second_name) . ' ' .
                                  ((is_null($person->third_name) || $person->third_name == ' ' ) ? ' ' : $person->third_name) . ' ' .
                                  ((is_null($person->last_name) || $person->last_name == ' ' ) ? ' ' : $person->last_name) ;

            $return['adscountry_id']= $person->adscountry_id;
            $return['adsregion_id']= $person->adsregion_id;
            $return['adsdistrict_id']= $person->adsdistrict_id;
            $return['adsneighborhood_id']= $person->adsneighborhood_id;
            $return['adssquare_id']= $person->adssquare_id;
            $return['adsmosques_id']= $person->adsmosques_id;

            if(isset($input['en_first_name']) || isset($input['en_second_name']) || isset($input['en_third_name']) || isset($input['en_last_name'])) {

                $field_ref = ['en_first_name'=>'first_name','en_second_name'=>'en_second_name','en_third_name'=>'en_third_name','en_last_name'=>'en_last_name'];
                $I18n=[];
                $keyToUpdate = ['en_first_name','en_second_name','en_third_name','en_last_name'];

                foreach ($keyToUpdate as $key_ref){
                    if(isset($input[$key_ref])){
                        if(!(is_null($input[$key_ref]) || $input[$key_ref] == '' || $input[$key_ref] == ' '))
                            $I18n[$field_ref[$key_ref]] = $input[$key_ref];
                    }
                }

                if(sizeof($I18n) > 0 )
                    PersonI18n::updateOrCreate(['person_id' => $person_id,'language_id' => $en_language_id],$I18n);

            }
            if(isset($input['phone'])){
                if($input['phone'] != null && $input['phone'] !="")
                   PersonI18n::updateOrCreate(['person_id'=>$person_id,'contact_type'=>'phone'], ['contact_value'=>$input['phone']]);
            }
            if(isset($input['primary_mobile'])){
                if($input['primary_mobile'] != null && $input['primary_mobile'] !="")
                   PersonI18n::updateOrCreate(['person_id'=>$person_id,'contact_type'=>'primary_mobile'],['contact_value'=>$input['primary_mobile']]);
            }
        }

        return $return;

    }

    public static function DisableIfNotNoConnected($value,$CurOrg,$CurUserId){

        $query = \DB::table('char_cases')
            ->join('char_categories', function ($join){
                $join->on('char_categories.id', '=', 'char_cases.category_id');
                $join->where('char_categories.type', 2);
            })
            ->where('char_cases.person_id',$value['person_id'])
            ->selectRaw('char_cases.*')->get();

        foreach ($query as $k=>$v) {
            $passed = true;
            $mainConnector=OrgLocations::getConnected($v->organization_id);
            if(!is_null($value['adsdistrict_id'])){
                if(!in_array($value['adsdistrict_id'],$mainConnector)){
                    $passed = false;
                }else{
                    if(!is_null($value['adsregion_id'])){
                        if(!in_array($value['adsregion_id'],$mainConnector)){
                            $passed = false;
                        }else{

                            if(!is_null($value['adsneighborhood_id'])){
                                if(!in_array($value['adsneighborhood_id'],$mainConnector)){
                                    $passed = false;
                                }else{

                                    if(!is_null($value['adssquare_id'])){
                                        if(!in_array($value['adssquare_id'],$mainConnector)){
                                            $passed = false;
                                        }else{
                                            if(!is_null($value['adsmosques_id'])){
                                                if(!in_array($value['adsmosques_id'],$mainConnector)){
                                                    $passed = false;
                                                }
                                            }else{
                                                $passed = false;
                                            }
                                        }
                                    }else{
                                        $passed = false;
                                    }
                                }
                            }else{
                                $passed = false;
                            }
                        }
                    }else{
                        $passed = false;
                    }
                }
            }else{
                $passed = false;
            }

            if(!$passed){
                \Common\Model\AidsCases::where('id',$v->id)->update(['status' => 1]);
                $action='CASE_UPDATED';
                $message=trans('common::application.edited data') . '  : '.' "'.$value['name']. ' " ';
                \Log\Model\Log::saveNewLog($action,$message);
                \Common\Model\CasesStatusLog::create(['case_id'=>$v->id, 'user_id'=>$CurUserId,
                    'reason'=>trans('common::application.Disabled your recorded status for') . ' "'.$value['name']. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization'), 'status'=>1, 'date'=>date("Y-m-d")]);
                \Log\Model\Log::saveNewLog('CASE_UPDATED',trans('common::application.Disabled your recorded status for') . ' "'.$value['name']. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization'));
            }
        }

        return true;
    }

    public function relayFromExcel(Request $request)
    {

        $this->authorize('citizenRepository', \Common\Model\AidsCases::class);

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $user = \Auth::user();
        $category_id = $request->category_id;
        if ($category_id == ''){
            return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.you must select category before relay') ]);
        }
        $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$category_id)->first();
        if (is_null($categoryObj)){
            return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.you must select category before relay') ]);
        }
        $save = $request->get('save');
        $category_type = $categoryObj->type;

        $importFile=$request->file ;
        if ($importFile->isValid()) {
            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){
                        $validFile=true;

                        if($category_type == 1) {
                            $constraint = ["rkm_alhoy"];
                            foreach ($constraint as $item) {
                                if ( !in_array($item, $keys) ) {
                                    $validFile = false;
                                    break;
                                }
                            }
                        }
                        else{
                            $constraint =  [ "rkm_alhoy", "aldol", "almhafth", "almntk", "alhy",
                                "almrbaa", "almsjd", "tfasyl_alaanoan", "rkm_joal_asasy", "rkm_hatf"];

                            foreach ($constraint as $item) {
                                if ( !in_array($item, $keys) ) {
                                    $validFile = false;
                                    break;
                                }
                            }
                        }

                        if($validFile) {

                            $success =0;
                            $old = 0;
                            $death_person =0;
                            $invalid_cards =0;
                            $duplicated =0;
                            $done_relay= [];
                            $relaysObj_=[];
                            $processed=[];
                            $final =[];
                            $cardToFind =[];
                            $relayed =[];
                            $CardToUpdate = [];
                            $createOrUpdate = [];
                            $PersonToRelay = [];
                            $errors =[];
                            $result =[];
                            $records= \Excel::selectSheets('data')->load($path)->get();

                            $total=sizeof($records);

                            if($total == 0){
                                return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                            }
                            if (($total > 2000)) {
                                return response()->json(['status' => 'failed' ,'error_type' =>' ' ,$total, 'msg' => trans('common::application.The number of records in the file must not exceed 2000 records') ]);
                            }
                            if($category_type == 1) {
                                foreach ($records as $key =>$value) {
                                    $card = (int)$value['rkm_alhoy'];
                                    if(!(is_null($card) || $card == ''  || $card == ' ')){
                                        if(in_array($card,$processed)){
                                            $result[] = ['id_card_number' =>  $card, 'reason' => trans('common::application.duplicated')];
                                            $duplicated++;
                                        }
                                        else{
                                            if (GovServices::checkCard($card)) {
                                                $cardToFind[]=$card;
                                            }else{
                                                $result[] = ['id_card_number' =>  $card, 'reason' => trans('sponsorship::application.invalid_id_card_number')];
                                                $invalid_cards++ ;
                                            }
                                            $processed[]=$card;
                                        }
                                    }
                                }

                                $founded = \DB::table('char_persons')
                                              ->whereIn('id_card_number', $cardToFind)
                                              ->selectRaw("id,id_card_number,street_address,death_date,
                                                         CONCAT(ifnull(first_name, ' '),' ',ifnull(second_name, ' '),' ',
                                                                ifnull(third_name,' '),' ',ifnull(last_name,' '))as name")
                                              ->get();


                                if (sizeof($founded) > 0) {
                                    foreach ($founded as $key => $value) {
                                        if (!in_array($value->id_card_number, $CardToUpdate)) {
                                            if(!is_null($value->death_date)){
                                                $result[] = ['id_card_number' =>  $value->id_card_number , 'reason' => trans('common::application.deceased')];
                                                $death_person ++;
                                            }else{
                                                $relayed[] = $value->id;
                                                $PersonToRelay[] = ['id_card_number' =>  $value->id_card_number , 'name' => $value->name , 'person_id'=> $value->id] ;
                                            }
                                            $CardToUpdate[]=$value->id_card_number;
                                        }
                                    }
                                }

                                $CardToGetFromGov = array_diff($cardToFind, $CardToUpdate);
                                $death_person_card= [];
                                foreach ($CardToGetFromGov as $cardTo_){
                                    $row = GovServices::byCard($cardTo_);
                                    if($row['status'] == true) {
                                        CloneGovernmentPersons::saveNew((Object) $row['row']);
                                        $record = Person::PersonMap($row['row']);
                                        if(!is_null($value->death_date)){
                                            $death_person_card[] = $cardTo_;
                                            $result[] = ['id_card_number' =>  $cardTo_, 'reason' => trans('common::application.deceased')];
                                            $death_person ++;
                                        }else{
                                            $createOrUpdate[]= $record;
                                        }
                                    }else{
                                        $result[] = ['card'=> $value['rkm_alhoy'] , 'reason' =>  trans('aid::application.invalid_card')];
                                        $invalid_cards++;
                                    }
                                }

                                if(sizeof($createOrUpdate) > 0 ){
                                    foreach ($createOrUpdate as $key=>$value){
                                        $person =self::savePerson($value);
                                        if (!in_array($person['id_card_number'], $death_person_card)){
                                            if (!in_array($person['id'], $relayed)){
                                                $relayed[] = $person['id'];
                                                $PersonToRelay[] = ['person_id'=> $person['id'],
                                                                   'id_card_number' =>  $person['id_card_number'], 'name' => $person['name']] ;
                                            }
                                        }
                                    }
                                }

                                if(sizeof($PersonToRelay) > 0){
                                    foreach ($PersonToRelay as $key=>$value){
                                            if($save == 'save' || $save == "save") {
                                                $inputs_ = ['organization_id' => $user->organization_id, 'person_id' => $value['person_id'], 'category_id' => $category_id];
                                                $case = SponsorshipsCases::where($inputs_)->first();

                                                if (is_null($case)) {
                                                    $inputs_['user_id'] = $user->id;
                                                    $inputs_['created_at'] = date('Y-m-d H:i:s');
                                                    $inputs_['status'] = 0;
                                                    $case = \Common\Model\SponsorshipsCases::create($inputs_);
                                                    \Common\Model\CasesStatusLog::create(['case_id' => $case->id, 'user_id' => $user->id, 'reason' => trans('common::application.case was created'),
                                                        'status' => 0, 'date' => date("Y-m-d")]);
                                                }
                                            }
                                            $relaysObj =  Relays::where(['person_id'=> $value['person_id'],'category_id' => $category_id])->first();
                                            if(!is_null($relaysObj)){
                                                $done_relay[] =['id_card_number' =>  $value['id_card_number'] , 'name' => $value['name']];
                                                Relays::create(['person_id'=> $value['person_id'],
                                                    'category_id' => $category_id,
                                                    'user_id'=> $user->id,
                                                    'organization_id' => $user->organization_id]);

                                                $users =User::userHasPermission('reports.relays.confirm',[]);
                                                $url = "#/relays/sponsorships";
                                                \Log\Model\Notifications::saveNotification(['user_id_to' =>$users, 'url' =>$url,
                                                    'message' => trans('common::application.he migrated').': ' . ' ' . $value['name'] . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name
                                                ]);

                                                \Log\Model\Log::saveNewLog('RELAY_CREATED',trans('common::application.he migrated').': ' . ' ' . $value['name'] . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);
                                                $success++;

                                            }else{
                                                $old++;
                                            }
                                    }
                                }


                            }
                            else{

                                foreach ($records as $key =>$value) {
                                    $card = (int)$value['rkm_alhoy'];
                                    if(!(is_null($card) || $card == ''  || $card == ' ')){
                                        if(in_array($card,$processed)){
                                            $result[] = ['id_card_number' =>  $card, 'reason' => trans('common::application.duplicated')];
                                            $duplicated++;
                                        }
                                        else{
                                            if (GovServices::checkCard($card)) {
                                                $filterMap=$this->filterInputs($value);
                                                if(sizeof($filterMap['errors']) >0 ){
                                                    foreach ($filterMap['errors'] as $ek =>$ev){
                                                        $errors[] =['id_card_number'=>$card , 'error' => trans('common::application.'.$ek) , 'reason' => $ev];
                                                    }
                                                }
                                                $cardToFind[]=$card;
                                                $final[$card]=$filterMap;
                                            }else{
                                                $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => trans('sponsorship::application.invalid_id_card_number')];
                                                $invalid_cards++;
                                            }
                                            $processed[]=$card;
                                        }
                                    }
                                }

                                $founded =
                                    \DB::table('char_persons')
                                        ->leftjoin('char_persons_contact as mobile_', function($q) {
                                            $q->on('mobile_.person_id', '=', 'char_persons.id');
                                            $q->where('mobile_.contact_type','primary_mobile');
                                        })
                                        ->leftjoin('char_persons_contact as phone_', function($q) {
                                            $q->on('phone_.person_id', '=', 'char_persons.id');
                                            $q->where('phone_.contact_type','phone');
                                        })
                                        ->whereIn('id_card_number', $cardToFind)
                                        ->selectRaw("id,CONCAT(ifnull(first_name, ' '),' ',ifnull(second_name, ' '),' ',
                                                       ifnull(third_name,' '),' ',ifnull(last_name,' '))as name,
                                             id_card_number,street_address,death_date,
                                             adscountry_id ,adsdistrict_id ,adsregion_id,
                                             adsneighborhood_id ,adssquare_id ,adsmosques_id,
                                             mobile_.contact_value as primary_mobile , phone_.contact_value as phone")
                                        ->get();

                                if (sizeof($founded) > 0) {
                                    foreach ($founded as $key => $value) {
                                        $source = (Object) $final[$value->id_card_number];
                                        if (!in_array($value->id_card_number, $CardToUpdate)) {
                                            $CardToUpdate[] = $value->id_card_number ;

                                            $PersonToUpdateData_ =[];
                                            $keyToUpdate = ['adscountry_id','adsdistrict_id','adsregion_id','adssquare_id','adsmosques_id','street_address','primary_mobile','phone'];

                                            foreach ($keyToUpdate as $key_ref){
                                                if(isset($source->$key_ref)){
                                                    if(!is_null($source->$key_ref) && $source->$key_ref != $value->$key_ref){
                                                        $PersonToUpdateData_[$key_ref] = $source->$key_ref;
                                                    }
                                                }
                                            }

                                            if(sizeof($PersonToUpdateData_) > 0){
                                                $PersonToUpdateData_['id'] = $value->id ;
                                                $createOrUpdate[]= $PersonToUpdateData_;
                                            }

                                            if(!is_null($value->death_date)){
                                                $result[] = ['id_card_number' =>  $value->id_card_number , 'reason' => trans('common::application.deceased')];
                                                $death_person ++;
                                            }else{
                                                $relayed[] = $value->id;
                                                $PersonToRelay[] = ['id_card_number' =>  $value->id_card_number , 'person_id'=> $value->id,'name' => $value->name ,
                                                                    'adscountry_id' => $value->adscountry_id ,'adsdistrict_id' => $value->adsdistrict_id ,
                                                                    'adsregion_id' => $value->adsregion_id,'adsneighborhood_id' => $value->adsneighborhood_id,
                                                                    'adssquare_id' => $value->adssquare_id,'adsmosques_id' => $value->adsmosques_id] ;
                                            }
                                        }
                                    }
                                }

                                $CardToGetFromGov = array_diff($cardToFind, $CardToUpdate);
                                $death_person_card= [];
                                foreach ($CardToGetFromGov as $cardTo_){
                                    $row = GovServices::byCard($cardTo_);
                                    if($row['status'] == true) {
                                        CloneGovernmentPersons::saveNew((Object) $row['row']);
                                        $record = Person::PersonMap($row['row']);
                                        $source = (Object) $final[$cardTo_];
                                        if(!is_null($value->death_date)){
                                            $result[] = ['id_card_number' =>  $cardTo_ , 'reason' => trans('common::application.deceased')];
                                            $death_person_card[]=$cardTo_;
                                            $death_person ++;
                                        }
                                        $keyToUpdate = ['adscountry_id','adsdistrict_id','adsregion_id','adssquare_id','adsmosques_id','street_address','primary_mobile','phone'];
                                        foreach ($keyToUpdate as $key_ref){
                                            if(isset($source->$key_ref)){
                                                if(!is_null($source->$key_ref) && $source->$key_ref != $value->$key_ref){
                                                    $PersonToUpdateData_[$key_ref] = $source->$key_ref;
                                                }
                                            }
                                        }

                                        if(sizeof($record) > 0)
                                            $createOrUpdate[]= $record;

                                    }else{
                                        $result[] = ['card'=> $value['rkm_alhoy'] , 'reason' =>  trans('aid::application.invalid_card')];
                                        $invalid_cards++;
                                    }
                                }

                                if(sizeof($createOrUpdate) > 0 ){
                                    foreach ($createOrUpdate as $key=>$value){
                                        $person =self::savePerson($value);
                                        if (!in_array($person['id_card_number'], $death_person_card)){
                                            if (!in_array($person['id'], $relayed)){
                                                $relayed[] =$person['id'];
                                                $PersonToRelay[] = ['id_card_number' =>   $person['id_card_number'] , 'name' => $person['name'] , 'person_id'=> $person['id'] ,
                                                                    'adscountry_id' => $person['adscountry_id'] ,'adsdistrict_id' => $person['adsdistrict_id'] ,
                                                                    'adsregion_id' => $person['adsregion_id'],'adsneighborhood_id' => $person['adsneighborhood_id'],
                                                                    'adssquare_id' => $person['adssquare_id'],'adsmosques_id' => $person['adsmosques_id']
                                                                  ] ;
                                            }
                                        }
                                    }
                                }

                                if(sizeof($PersonToRelay) > 0){
                                    $mainConnector=OrgLocations::getConnected($user->organization_id);
                                    foreach ($PersonToRelay as $key=>$value){
                                        self::DisableIfNotNoConnected($value,$user->organization_id,$user->id);
                                        if($save == 'save' || $save == "save") {
                                            $inputs_ = [ 'organization_id'=>$user->organization_id,
                                                'person_id' =>$value['person_id'] ,'category_id' => $category_id];
                                            $passed = true;
                                            if(sizeof($mainConnector) > 0){
                                                if(!is_null($value['adsdistrict_id'])){
                                                    if(!in_array($value['adsdistrict_id'],$mainConnector)){
                                                        $passed = false;
                                                    }else{
                                                        if(!is_null($value['adsregion_id'])){
                                                            if(!in_array($value['adsregion_id'],$mainConnector)){
                                                                $passed = false;
                                                            }
                                                            else{

                                                                if(!is_null($value['adsneighborhood_id'])){
                                                                    if(!in_array($value['adsneighborhood_id'],$mainConnector)){
                                                                        $passed = false;
                                                                    }else{

                                                                        if(!is_null($value['adssquare_id'])){
                                                                            if(!in_array($value['adssquare_id'],$mainConnector)){
                                                                                $passed = false;
                                                                            }else{
                                                                                if(!is_null($value['adsmosques_id'])){
                                                                                    if(!in_array($value['adsmosques_id'],$mainConnector)){
                                                                                        $passed = false;
                                                                                    }
                                                                                }else{
                                                                                    $passed = false;
                                                                                }
                                                                            }
                                                                        }else{
                                                                            $passed = false;
                                                                        }
                                                                    }
                                                                }else{
                                                                    $passed = false;
                                                                }
                                                            }
                                                        }else{
                                                            $passed = false;
                                                        }
                                                    }
                                                }else{
                                                    $passed = false;
                                                }
                                            }else{
                                                $passed = false;
                                            }

                                            if($passed == true){
                                                $case = AidsCases::where($inputs_)->first();
                                                if(is_null($case)){
                                                    $inputs_['user_id']=$user->id;
                                                    $inputs_['created_at']=date('Y-m-d H:i:s');
                                                    $inputs_['status']=0;
                                                    $case=  \Common\Model\AidsCases::create($inputs_);
                                                    \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id, 'reason'=>trans('common::application.case was created'),
                                                        'status'=>0, 'date'=>date("Y-m-d")]);
                                                }
                                            }
                                        }
                                        $relaysObj =  Relays::where(['person_id'=> $value['person_id'],'category_id' => $category_id])->first();
                                        if(!is_null($relaysObj)){
                                            $done_relay[] =['id_card_number' =>  $value['id_card_number'] , 'name' => $value['name']];
                                            Relays::create(['person_id'=> $value['person_id'],
                                                'category_id' => $category_id,
                                                'user_id'=> $user->id,
                                                'organization_id' => $user->organization_id]);

                                            $levelId = null;
                                            if(is_null($value['adsmosques_id'])) {
                                                if(is_null($value['adssquare_id'])) {
                                                    if(is_null($value['adsneighborhood_id'])) {
                                                        if(is_null($value['adsregion_id'])) {
                                                            if(is_null($value['adsdistrict_id'])) {
                                                                if(is_null($value['adscountry_id'])) {
                                                                    $levelId = $value['adscountry_id'] ;
                                                                }
                                                            }else{
                                                                $levelId = $value['adsdistrict_id'] ;
                                                            }
                                                        }else{
                                                            $levelId = $value['adsregion_id'] ;
                                                        }
                                                    }else{
                                                        $levelId = $value['adsneighborhood_id'] ;
                                                    }
                                                }else{
                                                    $levelId = $value['adssquare_id'] ;
                                                }
                                            }
                                            else{
                                                $levelId = $value['adsmosques_id'] ;
                                            }

                                            $orgs =  OrgLocations::LocationOrgs($levelId,$value['person_id'],$category_id);
                                            $users =User::userHasPermission('reports.relays.confirm',$orgs);
                                            $url = "#/relays/sponsorships";
                                            \Log\Model\Notifications::saveNotification(['user_id_to' =>$users, 'url' =>$url,
                                                'message' => trans('common::application.he migrated').': ' . ' ' . $value['name'] . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name
                                            ]);

                                            \Log\Model\Log::saveNewLog('RELAY_CREATED',trans('common::application.he migrated').': ' . ' ' . $value['name'] . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);
                                            $success++;

                                        }else{
                                            $old++;
                                        }
                                    }
                                }

                            }

                            if( sizeof($done_relay) == 0 ){
                                $response = ['status'=>'failed','res'=>$result];
                            }

                            $response['msg'] = trans('common::application.Some numbers were successfully migrated, as the total number').' :  '. sizeof($records) . ' ,  ';
                            if( sizeof($records) == $success){
                                $response['msg'] = trans('common::application.action success').' ,  '.trans('common::application.number of returned row is').' :  '. sizeof($records) . ' ,  ';
                            }

                            $response['msg'] .= trans('common::application.Number of migrated names') .' :  ' .$success . ' ,  ' .
                                                    trans('common::application.Number of pre-migrated names') .' :  ' .$old . ' ,  ' .
                                                    trans('common::application.Number of names not registered in the Civil Registry') .' :  ' .$invalid_cards . ' ,  ' .
                                                    trans('common::application.count of death person') .' :  '.$death_person  . ' ,  ' .
                                                    trans('common::application.Number of repeated names') .' :  '.$duplicated;

                            if(sizeof($result) > 0 || sizeof($relaysObj_) > 0 || sizeof($errors) > 0 ){
                                $token = md5(uniqid());
                                $response['download_token'] = $token;

                                \Excel::create('export_' . $token, function($excel) use($result,$relaysObj_,$errors) {

                                    $excel->setTitle(trans('common::application.relays'));
                                    $excel->setDescription(trans('common::application.relays'));

                                    if(sizeof($relaysObj_) > 0){
                                        $excel->sheet(trans('common::application.relays'), function($sheet) use($relaysObj_) {

                                            $sheet->setRightToLeft(true);
                                            $sheet->setAllBorders('thin');
                                            $sheet->setfitToWidth(true);
                                            $sheet->setHeight(1,40);
                                            $sheet->getDefaultStyle()->applyFromArray([
                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                            ]);

                                            $style = [
                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                'font' => [
                                                    'name' => 'Simplified Arabic',
                                                    'size' => 12,
                                                    'bold' => true
                                                ]
                                            ];

                                            $sheet->getStyle("A1:C1")->applyFromArray($style);
                                            $sheet->setHeight(1, 30);
                                            $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                            $sheet ->setCellValue('B1',trans('common::application.name'));

                                            $z= 2;
                                            foreach($relaysObj_ as $key=>$value){
                                                $sheet ->setCellValue('A'.$z,$value['id_card_number']);
                                                $sheet ->setCellValue('B'.$z,$value['name']);
                                                $z++;
                                            }

                                        });
                                    }
                                    if(sizeof($result) > 0){
                                        $excel->sheet(trans('common::application.invalid_cards'), function($sheet) use($result) {

                                            $sheet->setRightToLeft(true);
                                            $sheet->setAllBorders('thin');
                                            $sheet->setfitToWidth(true);
                                            $sheet->setHeight(1,40);
                                            $sheet->getDefaultStyle()->applyFromArray([
                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                            ]);

                                            $style = [
                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                'font' => [
                                                    'name' => 'Simplified Arabic',
                                                    'size' => 12,
                                                    'bold' => true
                                                ]
                                            ];

                                            $sheet->getStyle("A1:C1")->applyFromArray($style);
                                            $sheet->setHeight(1, 30);

                                            $sheet->setCellValue('A1',trans('common::application.id_card_number'));
                                            $sheet->setCellValue('B1',trans('common::application.reason'));

                                            $z= 2;
                                            foreach($result as $k=>$v){
                                                $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                $sheet ->setCellValue('B'.$z,$v['reason']);
                                                $z++;
                                            }

                                        });
                                    }
                                    if(sizeof($errors) > 0){
                                        $excel->sheet(trans('common::application.data_errors'), function ($sheet) use ($errors) {
                                            $sheet->setStyle([
                                                'font' => [
                                                    'name' => 'Calibri',
                                                    'size' => 11,
                                                    'bold' => true
                                                ]
                                            ]);
                                            $sheet->setAllBorders('thin');
                                            $sheet->setfitToWidth(true);
                                            $sheet->setRightToLeft(true);
                                            $style = [
                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                'font' => [
                                                    'name' => 'Simplified Arabic',
                                                    'size' => 12,
                                                    'bold' => true
                                                ]
                                            ];

                                            $sheet->getStyle("A1:C1")->applyFromArray($style);
                                            $sheet->setHeight(1, 30);

                                            $style = [
                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                'font' => [
                                                    'name' => 'Simplified Arabic',
                                                    'size' => 12,
                                                    'bold' => false
                                                ]
                                            ];

                                            $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                            $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                            $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                            $sheet ->setCellValue('B1',trans('common::application.field_name'));
                                            $sheet ->setCellValue('C1',trans('common::application.reason'));
                                            $z= 2;
                                            foreach($errors as $v){
                                                $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                $sheet ->setCellValue('B'.$z,$v['error']);
                                                $sheet ->setCellValue('C'.$z,$v['reason']);
                                                $z++;
                                            }
                                        });

                                    }

                                })->store('xlsx', storage_path('tmp/'));
                            }

                            return response()->json($response);
                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }

}