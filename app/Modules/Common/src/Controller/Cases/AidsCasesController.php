<?php

namespace Common\Controller\Cases;

use Auth\Model\User;
use Common\Model\AbstractCases;
use Common\Model\CaseModel;
use Common\Model\Categories\AidsCategories;
use Common\Model\GovServices;
use Common\Model\Person;
use Common\Model\PersonModels\PersonAid;
use Common\Model\PersonModels\PersonBank;
use Common\Model\PersonModels\PersonProperties;
use Common\Model\Transfers\Transfers;
use Common\Model\ValidatorModel;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Organization\Model\OrgLocations;
use Setting\Model\AidSource;
use Setting\Model\PropertyI18n;
use Setting\Model\Essential;

class AidsCasesController extends AbstractCasesController
{
    protected $type = AbstractCases::TYPE_AIDS;

    /************ IMPORT CASE Essentials OF cases  ************/
    public function importEssentials(Request $request){

//        $this->authorize('import', SocialSearch::class);

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $importFile=$request->file ;

        $user = \Auth::user();

        if ($importFile->isValid()) {

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
//            $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
//            $objReader->setReadDataOnly(true);
//            $excel = $objReader->load($path);
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){

                        $validFile=true;
                        $constraint =  ["rkm_alhoy"];

                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if($validFile){

                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {

                                $result =[];
                                $processed_ =[];
                                $errors =[];
                                $processed=[];
                                $invalid_data=0;
                                $invalid_card=0;
                                $duplicated =0;
                                $old_person=0;
                                $not_case = 0;
                                $not_person=0;
                                $success =0;
                                $no_data = 0;
                                $case_not_enter_on_search =0;

                                $coordinate =[];

                                $ActiveSheet = array_search('data',$sheets,true);
                                $excel->setActiveSheetIndex($ActiveSheet);
                                $sheetObj = $excel->getActiveSheet();
                                foreach( $sheetObj->getRowIterator(0,1) as $row ){
                                    $cellIterator = $row->getCellIterator();
                                    $cellIterator->setIterateOnlyExistingCells(false);
                                    foreach ($cellIterator as $cell) {
                                        $val = $cell->getValue();
                                        $col = $cell->getColumn();
                                        if($col != 'A'){
                                            if(!(is_null($val) || $val ==' '|| $val =='')){
                                                $entry = Essential::where('name',$val)->first();
                                                if(is_null($entry)){
                                                    $errors[] =['id_card_number'=>' ' , 'error' => $val , 'reason' => trans('common::application.invalid_constant')];
                                                }else{
                                                    $colIndex = \PHPExcel_Cell::columnIndexFromString($col);
                                                    $adjustedColumnIndex = $colIndex;
                                                    $adjustedColumn = \PHPExcel_Cell::stringFromColumnIndex($adjustedColumnIndex);
                                                    $coordinate[$col]=['name'=>$cell->getValue(),'next'=> $adjustedColumn ,'id' => $entry->id];
                                                }
                                            }
                                        }
                                    }
                                }
                                $inc = 3;
                                foreach ($records as $key =>$value) {
                                    $id_card_number =(int)$value['rkm_alhoy'];
                                    if(in_array($id_card_number,$processed)){
                                        $result[] = ['id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                                        $duplicated++;
                                    }
                                    else{
                                        if(GovServices::checkCard($id_card_number)) {
                                            
                                            $essentials = [];

                                                foreach ($coordinate as $ix => $det ) {
                                                    $CEL = $ix.$inc;
                                                    $condition = Null;
                                                    $condition_ = $sheetObj->getCell($CEL)->getValue();
                                                    if(!(is_null($condition_) || $condition_ ==' '|| $condition_ =='')) {
                                                        $condition = 0;
                                                        if($condition_ == 'ممتاز'){
                                                            $condition = 5;
                                                        }elseif ($condition_ == 'جيد جدا'){
                                                            $condition = 4;
                                                        }elseif ($condition_ == 'جيد'){
                                                            $condition = 3;
                                                        }elseif ($condition_ == 'سيء'){
                                                            $condition = 2;
                                                        }elseif ($condition_ == 'سيء جدا'){
                                                            $condition = 1;
                                                        }

                                                    }

                                                    $CEL_1 = $det['next'].$inc;
                                                    $needs = 0;
                                                    $needs_ = $sheetObj->getCell($CEL_1)->getValue();
                                                    if(!(is_null($needs_) || $needs_ ==' '|| $needs_ =='')) {
                                                        $needs = (float)$needs_;
                                                    }

                                                    $essentials[]=[
                                                        'case_id'=>null ,
                                                        'essential_id'=>$det['id'] ,
                                                        'condition'=> $condition ,
                                                        'needs'=> $needs
                                                    ];
                                                }
                                                
                                                if(sizeof($essentials)> 0 ){
                                                    
                                                  $cases = \Common\Model\CaseModel::caseList($id_card_number,$user);
                                                  
                                                    if(sizeof($cases)> 0 ){
                                                        foreach ($cases as $k => $case ){
                                                            
                                                            $essentials_deatils = [];
                                                            foreach ($essentials as $ke_ => $essential ){
                                                                $temp = $essential;
                                                                $temp['case_id'] =$case->id;
                                                                $essentials_deatils[]=$temp;
                                                            }
                                                            
                                                            \Common\Model\CasesEssentials::where(['case_id' => $case->id ])->delete();
                                                            \Common\Model\CasesEssentials::insert($essentials_deatils);
                                                        }
                                                        
                                                        $processed_[]=['id_card_number' => $id_card_number];
                                                        $success++;
                                                    }else{
                                                         $result[] = ['id_card_number' => $id_card_number, 'reason' =>'not_case'];
                                                        $not_case++;
                                                    }
                                                    
                                                }else{
                                                    $result[] = ['id_card_number' => $id_card_number, 'reason' =>'invalid_data'];
                                                    $invalid_data++;
                                                }

                                        }else{
                                            $result[] = ['id_card_number' => $id_card_number, 'reason' => 'invalid_card'];
                                            $invalid_card++;
                                        }
                                        $processed[]=$id_card_number;
                                    }
                                }


                                if( $success == $total && ($invalid_card ==0 && $invalid_data == 0  && $not_person == 0  && $not_case == 0)){
                                    $response["status"] = 'success';
                                    $response["msg"] = trans('common::application.Successfully import');
                                }
                                else{
                                    if($success != 0){
                                        $response["status"] = 'success';
                                        $response["msg"] =
                                            trans('common::application.Some individuals were imported successfully, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.Number of invalid_data') .' :  ' .$invalid_data . ' ,  ' .
                                            trans('common::application.number of case that not has data') .' :  ' .$no_data . ' ,  ' .
                                            trans('aid::application.not_case') .' :  ' .$not_case . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }
                                    else{
                                        $response["status"] = 'failed';
                                        $response["msg"] =
                                            trans('common::application.No cases were imported, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.Number of invalid_data') .' :  ' .$invalid_data . ' ,  ' .
                                            trans('common::application.number of case that not has data') .' :  ' .$no_data . ' ,  ' .
                                            trans('aid::application.not_case') .' :  ' .$not_case . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }
                                }

                                if (sizeof($result) > 0 || sizeof($errors) > 0 || sizeof($processed_) > 0) {
                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function ($excel) use ($result,$processed_,$errors) {

                                        if(sizeof($processed_) > 0){
                                            $excel->sheet(trans('common::application.saved'), function($sheet) use($processed_) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);


                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;

                                                foreach($processed_ as $key=>$v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $z++;
                                                }

                                            });
                                        }

                                        if(sizeof($result) > 0){
                                            $excel->sheet(trans('common::application.excluded'), function ($sheet) use ($result) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.reason'));
                                                $z= 2;
                                                foreach($result as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,trans('common::application.'.$v['reason']));
                                                    $z++;
                                                }
                                            });

                                        }

                                        if(sizeof($errors) > 0){
                                            $excel->sheet(trans('common::application.data_errors'), function ($sheet) use ($errors) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.field_name'));
                                                $sheet ->setCellValue('C1',trans('common::application.reason'));
                                                $z= 2;
                                                foreach($errors as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,$v['error']);
                                                    $sheet ->setCellValue('C'.$z,$v['reason']);
                                                    $z++;
                                                }
                                            });

                                        }

                                    })->store('xlsx', storage_path('tmp/'));

                                    $response["download_token"] = $token;
                                    $response["msg"] = $response["msg"] . '  , ' . trans('common::application.data restricted');
                                }
                                return response()->json($response);
                            }

                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);

                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }
    /************ IMPORT CASE Properties OF cases ************/
    public function importProperties(Request $request){


        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $importFile=$request->file ;


        try {
            if ($importFile->isValid()) {

                $path =  $importFile->getRealPath();
                $excel = \PHPExcel_IOFactory::load( $path );
//            $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
//            $objReader->setReadDataOnly(true);
//            $excel = $objReader->load($path);
                $sheets = $excel->getSheetNames();
                if(\App\Http\Helpers::sheetFound("data",$sheets)){
                    $first=Excel::selectSheets('data')->load($path)->first();
                    if(!is_null($first)){
                        $keys = $first->keys()->toArray();
                        if(sizeof($keys) >0 ){

                            $validFile=true;
                            $constraint =  ["rkm_alhoy"];

                            foreach ($constraint as $item) {
                                if ( !in_array($item, $keys) ) {
                                    $validFile = false;
                                    break;
                                }
                            }

                            if($validFile){

                                $records= \Excel::selectSheets('data')->load($path)->get();
                                $total=sizeof($records);

                                if ($total > 0) {

                                    $processed_ =[];
                                    $errors =[];
                                    $processed=[];
                                    $invalid_data=0;
                                    $invalid_card=0;
                                    $duplicated =0;
                                    $old_person=0;
                                    $not_person=0;
                                    $success =0;
                                    $no_data = 0;
                                    $result =[];

                                    $coordinate =[];

                                    $ActiveSheet = array_search('data',$sheets,true);
                                    $excel->setActiveSheetIndex($ActiveSheet);
                                    $sheetObj = $excel->getActiveSheet();
                                    foreach( $sheetObj->getRowIterator(0,1) as $row ){
                                        $cellIterator = $row->getCellIterator();
                                        $cellIterator->setIterateOnlyExistingCells(false);
                                        foreach ($cellIterator as $cell) {
                                            $val = $cell->getValue();
                                            $col = $cell->getColumn();
                                            if($col != 'A'){
                                                if(!(is_null($val) || $val ==' '|| $val =='')){
                                                    $entry = PropertyI18n::where(['name'=>$val,'language_id'=>1])->first();

                                                    if(is_null($entry)){
                                                        $errors[] =['id_card_number'=>' ' , 'error' => $val , 'reason' => trans('common::application.invalid_constant')];
                                                    }else{
                                                        $coordinate[$col]=['name'=>$cell->getValue(),'id' => $entry->property_id];
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    $inc = 2;
                                    foreach ($records as $key =>$value) {
                                        $id_card_number =(int)$value['rkm_alhoy'];
                                        if(in_array($id_card_number,$processed)){
                                            $result[] = ['id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                                            $duplicated++;
                                        }
                                        else{
                                            if(GovServices::checkCard($id_card_number)) {

                                                $person = Person::where(function ($q) use ($id_card_number){
                                                                    $q->where('id_card_number',$id_card_number);
                                                                })->first();
                                                if(!is_null($person)){
                                                    $properties = [];

                                                    foreach ($coordinate as $ix => $det ) {
                                                        $CEL = $ix.$inc;
                                                        $val_ = $sheetObj->getCell($CEL)->getValue();
                                                        $has_property = 0;
                                                        $quantity = 0;
                                                        if(!(is_null($val_) || $val_ ==' '|| $val_ =='')) {
                                                            $quantity = (float)$val_;
                                                        }
                                                        if($quantity >0 ){
                                                            $has_property = 1;
                                                        }
                                                        $properties[]=[
                                                            'person_id'=>$person->id ,
                                                            'property_id'=>$det['id'] ,
                                                            'has_property'=> $has_property,
                                                            'quantity'=> $quantity
                                                        ];
                                                    }

                                                    if(sizeof($properties)> 0 ){
                                                        PersonProperties::where(['person_id' => $person->id ])->delete();
                                                        PersonProperties::insert($properties);
                                                        $processed_[]=['id_card_number' => $id_card_number];
                                                        $success++;
                                                    }else{
                                                        $result[] = ['id_card_number' => $id_card_number, 'reason' =>'invalid_data'];
                                                        $invalid_data++;
                                                    }

                                                }else{
                                                    $result[] = ['id_card_number' => $id_card_number, 'reason' =>'not_person'];
                                                    $not_person++;
                                                }

                                            }else{
                                                $result[] = ['id_card_number' => $id_card_number, 'reason' => 'invalid_card'];
                                                $invalid_card++;
                                            }
                                            $processed[]=$id_card_number;
                                        }
                                        $inc++;
                                    }


                                    if( $success == $total && ($old_person ==0 && $not_person == 0 && $invalid_data == 0 )){
                                        $response["status"] = 'success';
                                        $response["msg"] = trans('common::application.Successfully import');
                                    }
                                    else{
                                        if($success != 0){
                                            $response["status"] = 'success';
                                            $response["msg"] =
                                                trans('common::application.Some individuals were imported successfully, as the total number of records') .' :  '. $total . ' ,  '.
                                                trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                                trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                                trans('common::application.Number of invalid_data') .' :  ' .$invalid_data . ' ,  ' .
                                                trans('common::application.number of case that not has data') .' :  ' .$no_data . ' ,  ' .
                                                trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                        }
                                        else{
                                            $response["status"] = 'failed';
                                            $response["msg"] =
                                                trans('common::application.No cases were imported, as the total number of records') .' :  '. $total . ' ,  '.
                                                trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                                trans('common::application.Number of invalid_data') .' :  ' .$invalid_data . ' ,  ' .
                                                trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                                trans('common::application.number of case that not has data') .' :  ' .$no_data . ' ,  ' .
                                                trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                        }
                                    }

                                    if (sizeof($result) > 0 || sizeof($errors) > 0 || sizeof($processed_) > 0) {
                                        $token = md5(uniqid());
                                        \Excel::create('export_' . $token, function ($excel) use ($result,$processed_,$errors) {

                                            if(sizeof($processed_) > 0){
                                                $excel->sheet(trans('common::application.saved'), function($sheet) use($processed_) {

                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setHeight(1,40);
                                                    $sheet->getDefaultStyle()->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                    ]);


                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));

                                                    $z= 2;

                                                    foreach($processed_ as $key=>$v){
                                                        $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                        $z++;
                                                    }

                                                });
                                            }

                                            if(sizeof($result) > 0){
                                                $excel->sheet(trans('common::application.excluded'), function ($sheet) use ($result) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                       'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);
                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('B1',trans('common::application.reason'));
                                                    $z= 2;
                                                    foreach($result as $v){
                                                        $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                        $sheet ->setCellValue('B'.$z,trans('common::application.'.$v['reason']));
                                                        $z++;
                                                    }
                                                });

                                            }

                                            if(sizeof($errors) > 0){
                                                $excel->sheet(trans('common::application.data_errors'), function ($sheet) use ($errors) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('B1',trans('common::application.field_name'));
                                                    $sheet ->setCellValue('C1',trans('common::application.reason'));
                                                    $z= 2;
                                                    foreach($errors as $v){
                                                        $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                        $sheet ->setCellValue('B'.$z,$v['error']);
                                                        $sheet ->setCellValue('C'.$z,$v['reason']);
                                                        $z++;
                                                    }
                                                });

                                            }

                                        })->store('xlsx', storage_path('tmp/'));

                                        $response["download_token"] = $token;
                                        $response["msg"] = $response["msg"] . '  , ' . trans('common::application.data restricted');
                                    }
                                    return response()->json($response);
                                }

                                return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);

                            }

                            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                        }
                    }
                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
                }
                return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
            }

        } catch (\Exception $e) {
            return response()->json(['status'=>false,'msg' => $e]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }
    /************ IMPORT CASE Bank Accounts ************/
    public function importBanks(Request $request){


        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $importFile=$request->file ;

        if ($importFile->isValid()) {

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();

            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){

                        $validFile=true;
                        $constraint =  ["rkm_alhoy", "asm_albnk", "alfraa", "rkm_alhsab", "malk_alhsab"];

                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {

                                $processed_ =[];
                                $errors =[];
                                $processed=[];
                                $result =[];

                                $no_data=0;
                                $invalid_card=0;
                                $invalid_data=0;

                                $duplicated =0;

                                $old_person=0;
                                $not_person=0;
                                $success =0;
                                $account_number_previously_add_to_you =0;
                                $account_number_owned_by_other =0;

                                foreach ($records as $key =>$value) {

                                    $id_card_number =(int)$value['rkm_alhoy'];
                                    if(in_array($id_card_number,$processed)){
                                        $result[] = ['id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                                        $duplicated++;
                                    }
                                    else{
                                        if(GovServices::checkCard($id_card_number)) {
                                            $details = [ 'id_card_number' => $id_card_number];

                                            $bank_id = null;
                                            $account_number = null;
                                            $account_owner = null;
                                            $branch_name = null;


                                            if(isset($value['rkm_alhsab'])) {
                                                if(!(is_null($value['rkm_alhsab']) || $value['rkm_alhsab'] =='' || $value['rkm_alhsab'] ==' ')){
                                                    $account_number = $value['rkm_alhsab'];
                                                }
                                            }

                                            if(!is_null($account_number)){
                                                $details['account_number']=$account_number;

                                                if(isset($value['asm_albnk'])) {
                                                    if(!(is_null($value['asm_albnk']) || $value['asm_albnk'] =='' || $value['asm_albnk'] ==' ')){
                                                        $object= \Setting\Model\Bank::where(['name'=>$value['asm_albnk'] ])->first();
                                                        $bank_id = is_null($object)? null : (int) $object->id;
                                                        if(is_null($bank_id) ){
                                                            $errors[] =['id_card_number'=>$id_card_number , 'error' => trans('common::application.bank_name') , 'reason' => trans('common::application.invalid_constant')];
                                                        }else{
                                                            $details['bank_id']=$bank_id;
                                                            if(isset($value['alfraa'])) {
                                                                if(!(is_null($value['alfraa']) || $value['alfraa'] =='' || $value['alfraa'] ==' ')){
                                                                    $branch_name = \Setting\Model\Branch::getBranchId($bank_id,$value['alfraa']);
                                                                    if( is_null($branch_name) ){
                                                                        $errors[] =['id_card_number'=>$id_card_number , 'error' => trans('common::application.branch_name') , 'reason' => trans('common::application.invalid_constant')];
                                                                    }else{
                                                                        $details['branch_name']=$branch_name;
                                                                    }
                                                                }
                                                            }

                                                            if(isset($value['malk_alhsab'])) {
                                                                if(!(is_null($value['malk_alhsab']) || $value['malk_alhsab'] =='' || $value['malk_alhsab'] ==' ')){
                                                                    $details['account_owner']=$value['malk_alhsab'];
                                                                }
                                                            }
                                                        }
                                                    }
                                                }

                                                if(is_null($bank_id) ) {
                                                    $result[] = ['id_card_number' => $id_card_number, 'reason' => 'invalid_data'];
                                                    $invalid_data++;
                                                }else{
                                                    $saveAccount = PersonBank::saveAccount($details);
                                                    if($saveAccount['status'] == true ){
                                                        $processed_[]=['id_card_number' => $id_card_number,
                                                            'bank_name' => $value['asm_albnk'],
                                                            'account_number' => $account_number];
                                                        $processed[]=$details;
                                                        $success++;
                                                    }else{
                                                        $result[] = ['id_card_number' => $id_card_number, 'reason' =>$saveAccount['reason']];

                                                        if($saveAccount['reason'] == 'person_not_register'){
                                                            $not_person++;
                                                        }elseif($saveAccount['reason'] == 'account_number_previously_add_to_you'){
                                                            $account_number_previously_add_to_you++;
                                                        }else{
                                                            $account_number_owned_by_other++;
                                                        }
                                                    }

                                                }


                                            }else{
                                                $result[] = ['id_card_number' => $id_card_number, 'reason' => 'no_data'];
                                                $no_data++;
                                            }

                                        }else{
                                            $result[] = ['id_card_number' => $id_card_number, 'reason' => 'invalid_card'];
                                            $invalid_card++;
                                        }
                                        $processed[]=$id_card_number;
                                    }
                                }


                                if( $success == $total && ($old_person ==0 && $not_person == 0 )){
                                    $response["status"] = 'success';
                                    $response["msg"] = trans('common::application.Successfully import');
                                }
                                else{

                                    if($success != 0){
                                        $response["status"] = 'success';
                                        $response["msg"] =
                                            trans('common::application.Some individuals were imported successfully, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.number of people not registered') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.Number of account_number_previously_add_to_you') .' :  ' .$account_number_previously_add_to_you . ' ,  ' .
                                            trans('common::application.Number of account_number_owned_by_other') .' :  ' .$account_number_owned_by_other . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }else{
                                        $response["status"] = 'failed';
                                        $response["msg"] =
                                            trans('common::application.No cases were imported, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.number of people not registered') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.Number of account_number_previously_add_to_you') .' :  ' .$account_number_previously_add_to_you . ' ,  ' .
                                            trans('common::application.Number of account_number_owned_by_other') .' :  ' .$account_number_owned_by_other . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }
                                }

                                if (sizeof($result) > 0 || sizeof($errors) > 0 || sizeof($processed_) > 0) {
                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function ($excel) use ($result,$processed_,$errors) {

                                        if(sizeof($processed_) > 0){
                                            $excel->sheet(trans('common::application.saved'), function($sheet) use($processed_) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);


                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.bank_name'));
                                                $sheet ->setCellValue('C1',trans('common::application.account_number'));

                                                $z= 2;

                                                foreach($processed_ as $key=>$v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,$v['bank_name']);
                                                    $sheet ->setCellValue('C'.$z,$v['account_number']);
                                                    $z++;
                                                }

                                            });
                                        }

                                        if(sizeof($result) > 0){
                                            $excel->sheet(trans('common::application.excluded'), function ($sheet) use ($result) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.reason'));
                                                $z= 2;
                                                foreach($result as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,trans('common::application.'.$v['reason']));
                                                    $z++;
                                                }
                                            });

                                        }

                                        if(sizeof($errors) > 0){
                                            $excel->sheet(trans('common::application.data_errors'), function ($sheet) use ($errors) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                   'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.field_name'));
                                                $sheet ->setCellValue('C1',trans('common::application.reason'));
                                                $z= 2;
                                                foreach($errors as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,$v['error']);
                                                    $sheet ->setCellValue('C'.$z,$v['reason']);
                                                    $z++;
                                                }
                                            });

                                        }

                                    })->store('xlsx', storage_path('tmp/'));

                                    $response["download_token"] = $token;
                                    $response["msg"] = $response["msg"] . '  , ' . trans('common::application.account restricted');
                                }
                                return response()->json($response);
                            }

                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);

                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }

    /************ IMPORT CASE Aids Source OF cases ************/
    public function importFinAids(Request $request){


        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $importFile=$request->file ;


        if ($importFile->isValid()) {

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
//            $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
//            $objReader->setReadDataOnly(true);
//            $excel = $objReader->load($path);
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){

                        $validFile=true;
                        $constraint =  ["rkm_alhoy"];

                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if($validFile){

                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {

                                $processed_ =[];
                                $result =[];
                                $errors =[];
                                $processed=[];
                                $invalid_card=0;
                                $duplicated =0;
                                $old_person=0;
                                $invalid_data=0;
                                $not_person=0;
                                $success =0;
                                $no_data = 0;

                                $coordinate =[];

                                $ActiveSheet = array_search('data',$sheets,true);
                                $excel->setActiveSheetIndex($ActiveSheet);
                                $sheetObj = $excel->getActiveSheet();
                                foreach( $sheetObj->getRowIterator(0,1) as $row ){
                                    $cellIterator = $row->getCellIterator();
                                    $cellIterator->setIterateOnlyExistingCells(false);
                                    foreach ($cellIterator as $cell) {
                                        $val = $cell->getValue();
                                        $col = $cell->getColumn();
                                        if($col != 'A'){
                                            if(!(is_null($val) || $val ==' '|| $val =='')){
                                                $entry = AidSource::where('name',$val)->first();

                                                if(is_null($entry)){
                                                    $errors[] =['id_card_number'=>' ' , 'error' => $val , 'reason' => trans('common::application.invalid_constant')];
                                                }else{
                                                    $coordinate[$col]=['name'=>$cell->getValue(),'id' => $entry->id];
                                                }
                                            }
                                        }
                                    }
                                }

                                $inc = 2;
                                foreach ($records as $key =>$value) {
                                    $id_card_number =(int)$value['rkm_alhoy'];
                                    if(in_array($id_card_number,$processed)){
                                        $result[] = ['id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                                        $duplicated++;
                                    }
                                    else{
                                        if(GovServices::checkCard($id_card_number)) {

                                            $person = Person::where(function ($q) use ($id_card_number){
                                                                $q->where('id_card_number',$id_card_number);
                                                            })->first();

                                            if(!is_null($person)){
                                                $aid_type = 1;
                                                $aids = [];

                                                foreach ($coordinate as $ix => $det ) {
                                                    $CEL = $ix.$inc;
                                                    $val_ = $sheetObj->getCell($CEL)->getValue();
                                                    $needs = 0;
                                                    $aid_take = 0;
                                                    if(!(is_null($val_) || $val_ ==' '|| $val_ =='')) {
                                                        $needs = (float)$val_;
                                                       if($needs > 0){
                                                            $aid_take = 1;
                                                        }
                                                    }
                                                    $aids[]=[
                                                        'person_id'=>$person->id ,
                                                        'aid_source_id'=>$det['id'] ,
                                                        'aid_take'=> $aid_take,
                                                        'aid_value'=> $needs,
                                                        'aid_type'=> $aid_type,
                                                        'currency_id'=>4
                                                    ];
                                                }

                                                if(sizeof($aids)> 0 ){
                                                    PersonAid::where(['person_id' => $person->id ,'aid_type'=>$aid_type])->delete();
                                                    PersonAid::insert($aids);
                                                    $processed_[]=['id_card_number' => $id_card_number];
                                                    $success++;
                                                }else{
                                                    $result[] = ['id_card_number' => $id_card_number, 'reason' =>'invalid_data'];
                                                    $invalid_data++;
                                                }

                                            }else{
                                                $result[] = ['id_card_number' => $id_card_number, 'reason' =>'not_person'];
                                                $not_person++;
                                            }

                                        }else{
                                            $result[] = ['id_card_number' => $id_card_number, 'reason' => 'invalid_card'];
                                            $invalid_card++;
                                        }
                                        $processed[]=$id_card_number;
                                    }
                                }


                                if( $success == $total && ($old_person ==0 && $not_person == 0 )){
                                    $response["status"] = 'success';
                                    $response["msg"] = trans('common::application.Successfully import');
                                }
                                else{
                                    if($success != 0){
                                        $response["status"] = 'success';
                                        $response["msg"] =
                                            trans('common::application.Some individuals were imported successfully, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.number of case that not has data') .' :  ' .$no_data . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }
                                    else{
                                        $response["status"] = 'failed';
                                        $response["msg"] =
                                            trans('common::application.No cases were imported, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.number of case that not has data') .' :  ' .$no_data . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }
                                }

                                if (sizeof($result) > 0 || sizeof($errors) > 0 || sizeof($processed_) > 0) {
                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function ($excel) use ($result,$processed_,$errors) {

                                        if(sizeof($processed_) > 0){
                                            $excel->sheet(trans('common::application.saved'), function($sheet) use($processed_) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);


                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;

                                                foreach($processed_ as $key=>$v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $z++;
                                                }

                                            });
                                        }

                                        if(sizeof($result) > 0){
                                            $excel->sheet(trans('common::application.excluded'), function ($sheet) use ($result) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                   'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.reason'));
                                                $z= 2;
                                                foreach($result as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,trans('common::application.'.$v['reason']));
                                                    $z++;
                                                }
                                            });

                                        }

                                        if(sizeof($errors) > 0){
                                            $excel->sheet(trans('common::application.data_errors'), function ($sheet) use ($errors) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.field_name'));
                                                $sheet ->setCellValue('C1',trans('common::application.reason'));
                                                $z= 2;
                                                foreach($errors as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,$v['error']);
                                                    $sheet ->setCellValue('C'.$z,$v['reason']);
                                                    $z++;
                                                }
                                            });

                                        }

                                    })->store('xlsx', storage_path('tmp/'));

                                    $response["download_token"] = $token;
                                    $response["msg"] = $response["msg"] . '  , ' . trans('common::application.data restricted');
                                }
                                return response()->json($response);
                            }

                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);

                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }
    public function importNonFinAids(Request $request){


        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $importFile=$request->file ;


        if ($importFile->isValid()) {

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
//            $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
//            $objReader->setReadDataOnly(true);
//            $excel = $objReader->load($path);
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){

                        $validFile=true;
                        $constraint =  ["rkm_alhoy"];

                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if($validFile){

                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {

                                $processed_ =[];
                                $result =[];
                                $errors =[];
                                $processed=[];
                                $invalid_card=0;
                                $duplicated =0;
                                $old_person=0;
                                $invalid_data=0;
                                $not_person=0;
                                $success =0;
                                $no_data = 0;

                                $coordinate =[];

                                $ActiveSheet = array_search('data',$sheets,true);
                                $excel->setActiveSheetIndex($ActiveSheet);
                                $sheetObj = $excel->getActiveSheet();
                                foreach( $sheetObj->getRowIterator(0,1) as $row ){
                                    $cellIterator = $row->getCellIterator();
                                    $cellIterator->setIterateOnlyExistingCells(false);
                                    foreach ($cellIterator as $cell) {
                                        $val = $cell->getValue();
                                        $col = $cell->getColumn();
                                        if($col != 'A'){
                                            if(!(is_null($val) || $val ==' '|| $val =='')){
                                                $entry = AidSource::where('name',$val)->first();

                                                if(is_null($entry)){
                                                    $errors[] =['id_card_number'=>' ' , 'error' => $val , 'reason' => trans('common::application.invalid_constant')];
                                                }else{
                                                    $coordinate[$col]=['name'=>$cell->getValue(),'id' => $entry->id];
                                                }
                                            }
                                        }
                                    }
                                }

                                $inc = 2;
                                foreach ($records as $key =>$value) {
                                    $id_card_number =(int)$value['rkm_alhoy'];
                                    if(in_array($id_card_number,$processed)){
                                        $result[] = ['id_card_number' => $id_card_number, 'reason' => 'duplicated'];
                                        $duplicated++;
                                    }
                                    else{
                                        if(GovServices::checkCard($id_card_number)) {

                                            $person = Person::where(function ($q) use ($id_card_number){
                                                                    $q->where('id_card_number',$id_card_number);
                                                                })->first();

                                            if(!is_null($person)){
                                                $aid_type = 2;
                                                $aids = [];

                                                foreach ($coordinate as $ix => $det ) {
                                                    $CEL = $ix.$inc;
                                                    $val_ = $sheetObj->getCell($CEL)->getValue();
                                                    $needs = 0;
                                                    $aid_take = 0;
                                                    if(!(is_null($val_) || $val_ ==' '|| $val_ =='')) {
                                                        $needs = (float)$val_;
                                                       if($needs > 0){
                                                            $aid_take = 1;
                                                        }
                                                    }
                                                    $aids[]=[
                                                        'person_id'=>$person->id ,
                                                        'aid_source_id'=>$det['id'] ,
                                                        'aid_take'=> $aid_take,
                                                        'aid_value'=> $needs,
                                                        'aid_type'=> $aid_type,
                                                        'currency_id'=>4
                                                    ];
                                                }

                                                if(sizeof($aids)> 0 ){
                                                    PersonAid::where(['person_id' => $person->id ,'aid_type'=>$aid_type])->delete();
                                                    PersonAid::insert($aids);
                                                    $processed_[]=['id_card_number' => $id_card_number];
                                                    $success++;
                                                }else{
                                                    $result[] = ['id_card_number' => $id_card_number, 'reason' =>'invalid_data'];
                                                    $invalid_data++;
                                                }

                                            }else{
                                                $result[] = ['id_card_number' => $id_card_number, 'reason' =>'not_person'];
                                                $not_person++;
                                            }

                                        }else{
                                            $result[] = ['id_card_number' => $id_card_number, 'reason' => 'invalid_card'];
                                            $invalid_card++;
                                        }
                                        $processed[]=$id_card_number;
                                    }
                                }


                                if( $success == $total && ($old_person ==0 && $not_person == 0 )){
                                    $response["status"] = 'success';
                                    $response["msg"] = trans('common::application.Successfully import');
                                }
                                else{
                                    if($success != 0){
                                        $response["status"] = 'success';
                                        $response["msg"] =
                                            trans('common::application.Some individuals were imported successfully, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.number of case that not has data') .' :  ' .$no_data . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }
                                    else{
                                        $response["status"] = 'failed';
                                        $response["msg"] =
                                            trans('common::application.No cases were imported, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of invalid_card') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.number of case that not has data') .' :  ' .$no_data . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }
                                }

                                if (sizeof($result) > 0 || sizeof($errors) > 0 || sizeof($processed_) > 0) {
                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function ($excel) use ($result,$processed_,$errors) {

                                        if(sizeof($processed_) > 0){
                                            $excel->sheet(trans('common::application.saved'), function($sheet) use($processed_) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);


                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;

                                                foreach($processed_ as $key=>$v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $z++;
                                                }

                                            });
                                        }

                                        if(sizeof($result) > 0){
                                            $excel->sheet(trans('common::application.excluded'), function ($sheet) use ($result) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.reason'));
                                                $z= 2;
                                                foreach($result as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,trans('common::application.'.$v['reason']));
                                                    $z++;
                                                }
                                            });

                                        }

                                        if(sizeof($errors) > 0){
                                            $excel->sheet(trans('common::application.data_errors'), function ($sheet) use ($errors) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet ->setCellValue('B1',trans('common::application.field_name'));
                                                $sheet ->setCellValue('C1',trans('common::application.reason'));
                                                $z= 2;
                                                foreach($errors as $v){
                                                    $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                    $sheet ->setCellValue('B'.$z,$v['error']);
                                                    $sheet ->setCellValue('C'.$z,$v['reason']);
                                                    $z++;
                                                }
                                            });

                                        }

                                    })->store('xlsx', storage_path('tmp/'));

                                    $response["download_token"] = $token;
                                    $response["msg"] = $response["msg"] . '  , ' . trans('common::application.data restricted');
                                }
                                return response()->json($response);
                            }

                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);

                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }
    // update goverment data of cases
    public function goverment_updtryate(Request $request)
    {
        return parent::goverment_update($request);
    }

    // update status and category of cases
    public function updateStatusAndCategories(Request $request)
    {
        return parent::updateStatusAndCategories($request);
    }

    /************ GET CASES*************/
    public function filterCases(Request $request)
    {
        if($request->get('action') =='ExportToExcel' || $request->get('action') =='ExportToWord') {
            $this->authorize('export', \Common\Model\AidsCases::class);
        }else{
            $this->authorize('manage', \Common\Model\AidsCases::class);
        }
        return parent::filterCases($request);
    }
    /************ GET CASE InCompletes *************/
    public function getInCompleteCases(Request $request)
    {
        $this->authorize('manage',\Common\Model\AidsCases::class);
        return parent::getInCompleteCases($request);
    }
    /************ CHANGE CATEGORY *************/
    public function changeCategory(Request $request,$id)
    {
        $this->authorize('changeCategory',\Common\Model\AidsCases::class);
        return parent::changeCategory($request,$id);
    }
    public function changeCategoryToGroup(Request $request,$id)
    {
        $this->authorize('changeCategory',\Common\Model\AidsCases::class);
        return parent::changeCategoryToGroup($request,$id);
    }
    /************ GET CASES ATTACHMENTS *************/
    public function getCaseAttachments(Request $request)
    {
        $this->authorize('manage',\Common\Model\AidsCases::class);
        return parent::getCaseAttachments($request);
    }
    /************ GET CASE FORM *************/
    public function word(Request $request)
    {
        $this->authorize('export',\Common\Model\AidsCases::class);
        return parent::word($request);
    }
    /************ GET CASE ATTACHMENTS *************/
    public function exportCaseDocument($id,Request $request)
    {
        $this->authorize('export',\Common\Model\AidsCases::class);
        return parent::exportCaseDocument($id,$request);
    }
    /************ UPDATE CASE STATUS *************/
    public function update(Request $request ,$id)
    {
        $this->authorize('update',\Common\Model\AidsCases::class);
        return parent::update($request,$id);
    }
    /************ SAVE CASE STATUS *************/
    public function store(Request $request)
    {
        $this->authorize('create',\Common\Model\AidsCases::class);
        $request->request->add(['category_type'=> $this->type]);
        $request->request->add(['aidsLocations'=> true]);
        return parent::store($request);
    }
    /************ SOFT DELETE OF CASE *************/
    public function destroy($id,Request $request)
    {
        $this->authorize('delete',\Common\Model\AidsCases::class);
        return parent::destroy($id,$request);
    }
    /************ RESTORE DELETED CASE *************/
    public function restore($id)
    {
        $this->authorize('update', \Common\Model\AidsCases::class);
        return parent::restore($id);
    }
    /************ GET CASE STATUS LOGS*************/
    public function getStatusLogs($id)
    {
        $this->authorize('view', \Common\Model\AidsCases::class);
        return parent::getStatusLogs($id);
    }

    // set case attachment
     public function setAttachment(Request $request ,$id)
     {
        $this->authorize('create', \Common\Model\AidsCases::class);
        $this->authorize('update', \Common\Model\AidsCases::class);
         return parent::setAttachment($request,$id);
     }
    // set case attachment
    public function setCaseAttachment(Request $request ,$id)
     {
//        $this->authorize('create', \Common\Model\AidsCases::class);
//        $this->authorize('update', \Common\Model\AidsCases::class);
         return parent::setCaseAttachment($request,$id);
     }

    /*********************** SAVING ***********************/
    // save residence of case
    public function saveResidence(\Illuminate\Http\Request $request, $id)
    {
        $this->authorize('update', \Common\Model\AidsCases::class);

        $input=[
            'property_type_id'    => $request->property_type_id,
            'roof_material_id'    => $request->roof_material_id,
            'residence_condition'    => $request->residence_condition,
            'indoor_condition'    => $request->indoor_condition,
            'habitable'    => $request->habitable,
            'house_condition'    => $request->house_condition,
            'rooms'    => $request->rooms,
            'area'    => $request->area,
            'rent_value'    => $request->rent_value,
            'need_repair'    => $request->need_repair,
            'repair_notes'    => $request->repair_notes,
        ];


        $fillable =  ['property_type_id','rent_value','roof_material_id','residence_condition',
            'habitable','indoor_condition','house_condition','rooms','area','need_repair','repair_notes'] ;

        foreach ($fillable as $key){
            if(isset($request->$key)){
                if($key =='property_type_id' || $key =='roof_material_id'){
                    if($request->$key =='' || $request->$key == " "){
                        $input[$key] = null;
                    }else{
                        $input[$key] = $request->$key;
                    }
                }else{
                    $input[$key] = $request->$key;
                }
            }
        }

        $options=['sub' =>true];

        $error = \App\Http\Helpers::isValid($input,ValidatorModel::getPersonValidatorRules($request->priority,$options));
        if($error)
            return response()->json($error);

        $case =\Common\Model\CaseModel::fetch(array('action' => 'edit', 'person'=> true,'full_name'=>true),$id);
        $updated=\Common\Model\PersonModels\PersonResidence::saveResidence($case->person_id,$input);
        \Log\Model\Log::saveNewLog('PERSON_RESIDENCE_SAVED',trans('common::application.Edited housing data for') . ' "'.$case->full_name. '" ');

        $passed = true;
        $mainConnector=OrgLocations::getConnected($case->organization_id);
        if(!is_null($case->adsdistrict_id)){
            if(!in_array($case->adsdistrict_id,$mainConnector)){
                $passed = false;
            }else{
                if(!is_null($case->adsregion_id)){
                    if(!in_array($case->adsregion_id,$mainConnector)){
                        $passed = false;
                    }else{

                        if(!is_null($case->adsneighborhood_id)){
                            if(!in_array($case->adsneighborhood_id,$mainConnector)){
                                $passed = false;
                            }else{

                                if(!is_null($case->adssquare_id)){
                                    if(!in_array($case->adssquare_id,$mainConnector)){
                                        $passed = false;
                                    }else{
                                        if(!is_null($case->adsmosques_id)){
                                            if(!in_array($case->adsmosques_id,$mainConnector)){
                                                $passed = false;
                                            }
                                        }else{
                                            $passed = false;
                                        }
                                    }
                                }else{
                                    $passed = false;
                                }
                            }
                        }else{
                            $passed = false;
                        }
                    }
                }else{
                    $passed = false;
                }
            }
        }else{
            $passed = false;
        }

        if($passed == false){
            $user = \Auth::user();
            \Common\Model\AidsCases::where('id',$id)->update(['status' => 1]);
            $action='CASE_UPDATED';
            $message= trans('common::application.edited data') . '  : '.' "'.$case->full_name. ' " ';
            \Log\Model\Log::saveNewLog($action,$message);
            \Common\Model\CasesStatusLog::create(['case_id'=>$id, 'user_id'=>$user->id, 'reason'=> trans('common::application.Disabled your recorded status for') . ' "'.$case->full_name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization') , 'status'=>1, 'date'=>date("Y-m-d")]);
            \Log\Model\Log::saveNewLog('CASE_UPDATED',trans('common::application.Disabled your recorded status for') . ' "'.$case->full_name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization'));
            $msg = trans('common::application.saved and  disabled because it is not in organization locations');

            $transferObj =  Transfers::where(['person_id'=> $case->person_id,
                                              'prev_adscountry_id'=> $case->adscountry_id, 'prev_adsdistrict_id'=> $case->adsdistrict_id,
                                              'prev_adsregion_id'=> $case->adsregion_id, 'prev_adsneighborhood_id'=> $case->adsneighborhood_id,
                                              'prev_adssquare_id'=> $case->adssquare_id, 'prev_adsmosques_id'=> $case->adsmosques_id,

                                              'adscountry_id'=> $case->adscountry_id, 'adsdistrict_id'=> $case->adsdistrict_id,
                                              'adsregion_id'=> $case->adsregion_id, 'adsneighborhood_id'=> $case->adsneighborhood_id,
                                              'adssquare_id'=> $case->adssquare_id, 'adsmosques_id'=> $case->adsmosques_id,
                                              'category_id' => $request->category_id])->first();

            if(!$transferObj){
               Transfers::create(['created_at' => date('Y-m-d H:i:s'), 'person_id'=> $case->person_id,
                                   'prev_adscountry_id'=> $case->adscountry_id, 'prev_adsdistrict_id'=> $case->adsdistrict_id,
                                   'prev_adsregion_id'=> $case->adsregion_id, 'prev_adsneighborhood_id'=> $case->adsneighborhood_id,
                                   'prev_adssquare_id'=> $case->adssquare_id, 'prev_adsmosques_id'=> $case->adsmosques_id,

                                   'adscountry_id'=> $case->adscountry_id, 'adsdistrict_id'=> $case->adsdistrict_id,
                                   'adsregion_id'=> $case->adsregion_id, 'adsneighborhood_id'=> $case->adsneighborhood_id,
                                   'adssquare_id'=> $case->adssquare_id, 'adsmosques_id'=> $case->adsmosques_id,
                                   'category_id' => $request->category_id, 'user_id'=> $user->id,
                                   'organization_id' => $user->organization_id]);

                $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$request->category_id)->first();
                \Log\Model\Log::saveNewLog('TRANSFER_CREATED',trans('common::application.he migrated')  . ' ' . $case->full_name . ' ,' .trans('common::application.As a case') .':' . $categoryObj->name);
            }
            return response()->json(["status" => 'failed_disabled', "msg"  =>$msg ]);
        }

        return response()->json(['status'=>$updated]);

    }

    // save home indoor of case
    public function saveHomeIndoor(\Illuminate\Http\Request $request, $id)
    {
        try {

            $this->authorize('update', \Common\Model\AidsCases::class);
            $priority= $request->get('priority');

            $case =\Common\Model\CaseModel::fetch(array('action' => 'edit', 'person'=> true,'full_name'=>true),$id);
            if($request->get('priority')){
                $response=\Common\Model\CasesEssentials::saveEssentials($id,$request->get('items'),$priority['essential_condition']);

                if(isset($response['inserted'])){
                    \Log\Model\Log::saveNewLog('CASE_UPDATED',trans('common::application.edited the home furniture status data for the case') . ' "'.$case->full_name. '" ');
                }

                $passed = true;
                $mainConnector=OrgLocations::getConnected($case->organization_id);
                if(!is_null($case->adsdistrict_id)){
                    if(!in_array($case->adsdistrict_id,$mainConnector)){
                        $passed = false;
                    }else{
                        if(!is_null($case->adsregion_id)){
                            if(!in_array($case->adsregion_id,$mainConnector)){
                                $passed = false;
                            }else{

                                if(!is_null($case->adsneighborhood_id)){
                                    if(!in_array($case->adsneighborhood_id,$mainConnector)){
                                        $passed = false;
                                    }else{

                                        if(!is_null($case->adssquare_id)){
                                            if(!in_array($case->adssquare_id,$mainConnector)){
                                                $passed = false;
                                            }else{
                                                if(!is_null($case->adsmosques_id)){
                                                    if(!in_array($case->adsmosques_id,$mainConnector)){
                                                        $passed = false;
                                                    }
                                                }else{
                                                    $passed = false;
                                                }
                                            }
                                        }else{
                                            $passed = false;
                                        }
                                    }
                                }else{
                                    $passed = false;
                                }
                            }
                        }else{
                            $passed = false;
                        }
                    }
                }else{
                    $passed = false;
                }


                if($passed == false){
                    $user = \Auth::user();
                    \Common\Model\AidsCases::where('id',$id)->update(['status' => 1]);
                    $action='CASE_UPDATED';
                    $message= trans('common::application.Has edited data') . '  : '.' "'.$case->full_name. ' " ';
                    \Log\Model\Log::saveNewLog($action,$message);
                    \Common\Model\CasesStatusLog::create(['case_id'=>$id, 'user_id'=>$user->id, 'reason'=>trans('common::application.Disabled your recorded status for') . ' "'.$case->full_name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization'), 'status'=>1, 'date'=>date("Y-m-d")]);
                    \Log\Model\Log::saveNewLog('CASE_UPDATED',trans('common::application.Disabled your recorded status for') . ' "'.$case->full_name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization'));
                    $msg = trans('common::application.saved and  disabled because it is not in organization locations');

                    $transferObj =  Transfers::where(['person_id'=> $case->person_id,
                        'prev_adscountry_id'=> $case->adscountry_id, 'prev_adsdistrict_id'=> $case->adsdistrict_id,
                        'prev_adsregion_id'=> $case->adsregion_id, 'prev_adsneighborhood_id'=> $case->adsneighborhood_id,
                        'prev_adssquare_id'=> $case->adssquare_id, 'prev_adsmosques_id'=> $case->adsmosques_id,

                        'adscountry_id'=> $case->adscountry_id, 'adsdistrict_id'=> $case->adsdistrict_id,
                        'adsregion_id'=> $case->adsregion_id, 'adsneighborhood_id'=> $case->adsneighborhood_id,
                        'adssquare_id'=> $case->adssquare_id, 'adsmosques_id'=> $case->adsmosques_id,
                        'category_id' => $request->category_id])->first();

                    if(!$transferObj){
                        Transfers::create(['created_at' => date('Y-m-d H:i:s'), 'person_id'=> $case->person_id,
                            'prev_adscountry_id'=> $case->adscountry_id, 'prev_adsdistrict_id'=> $case->adsdistrict_id,
                            'prev_adsregion_id'=> $case->adsregion_id, 'prev_adsneighborhood_id'=> $case->adsneighborhood_id,
                            'prev_adssquare_id'=> $case->adssquare_id, 'prev_adsmosques_id'=> $case->adsmosques_id,

                            'adscountry_id'=> $case->adscountry_id, 'adsdistrict_id'=> $case->adsdistrict_id,
                            'adsregion_id'=> $case->adsregion_id, 'adsneighborhood_id'=> $case->adsneighborhood_id,
                            'adssquare_id'=> $case->adssquare_id, 'adsmosques_id'=> $case->adsmosques_id,
                            'category_id' => $request->category_id, 'user_id'=> $user->id,
                            'organization_id' => $user->organization_id]);

                        $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$request->category_id)->first();
                        \Log\Model\Log::saveNewLog('TRANSFER_CREATED',trans('common::application.he migrated') .': ' . ' ' . $case->full_name . ' ,'. trans('common::application.As a case') .': ' . $categoryObj->name);
                    }
                    return response()->json(["status" => 'failed_disabled', "msg"  =>$msg ]);
                }

                return response()->json($response);
            }else{
                return response()->json(['status'=>false,'msg' =>  trans('common::application.save not successful')]);
            }

        } catch (\Exception $e) {
            return response()->json(['status'=>false,'msg' =>  trans('common::application.save not successful')]);
        }


    }

    // save ( properties , aids , case_needs , reconstructions promised )
    public function saveOthersData(\Illuminate\Http\Request $request, $id)
    {
        try {
            $this->authorize('update', \Common\Model\AidsCases::class);
            $options['sub']= true;
            $options['mode']=$request->mode;

            $case =\Common\Model\CaseModel::fetch(array('action' => 'edit', 'person'=> true,'full_name'=>true),$id);
            $person_id=$case->person_id;

            $inputs=['needs' =>strip_tags($request->get('needs')),
                'promised' => strip_tags($request->get('promised')),
                'organization_name' => strip_tags($request->get('reconstructions_organization_name')),
                'financial_aid_condition' => \Common\Model\PersonModels\PersonAid::getItems($person_id,1,$request->get('financial_aid_source')),
                'non_financial_aid_condition' =>\Common\Model\PersonModels\PersonAid::getItems($person_id,2,$request->get('non_financial_aid_source')),
                'properties_condition'=>\Common\Model\PersonModels\PersonProperties::getItems($person_id,$request->get('persons_properties'))
            ];

            $error = \App\Http\Helpers::isValid($inputs,ValidatorModel::getPersonValidatorRules($request->get('priority'),$options));
            if($error)
                return response()->json($error);

            \Common\Model\CaseNeeds::saveNeeds($id,['needs' => $inputs['needs']]);
            \Common\Model\Reconstructions::saveReconstruction($id,['promised' => $inputs['promised'] , 'organization_name' => $inputs['organization_name']]);

            if(sizeof($inputs['properties_condition']) != 0) {
                \Common\Model\PersonModels\PersonProperties::saveProperties($person_id,$inputs['properties_condition']);
            }
            if(sizeof($inputs['financial_aid_condition']) != 0) {
                \Common\Model\PersonModels\PersonAid::saveAids($person_id,1,$inputs['financial_aid_condition']);
            }
            if(sizeof($inputs['non_financial_aid_condition']) != 0) {
                \Common\Model\PersonModels\PersonAid::saveAids($person_id,2,$inputs['non_financial_aid_condition']);
            }

            $name=$case->first_name.' '.$case->second_name.' '.$case->third_name.' '.$case->last_name;
            \Log\Model\Log::saveNewLog('CASE_UPDATED',trans('common::application.Updated other data (property - outer aid - family needs) for') . ' "'.$name. '" ');

            $passed = true;
            $mainConnector=OrgLocations::getConnected($case->organization_id);
            if(!is_null($case->adsdistrict_id)){
                if(!in_array($case->adsdistrict_id,$mainConnector)){
                    $passed = false;
                }else{
                    if(!is_null($case->adsregion_id)){
                        if(!in_array($case->adsregion_id,$mainConnector)){
                            $passed = false;
                        }else{

                            if(!is_null($case->adsneighborhood_id)){
                                if(!in_array($case->adsneighborhood_id,$mainConnector)){
                                    $passed = false;
                                }else{

                                    if(!is_null($case->adssquare_id)){
                                        if(!in_array($case->adssquare_id,$mainConnector)){
                                            $passed = false;
                                        }else{
                                            if(!is_null($case->adsmosques_id)){
                                                if(!in_array($case->adsmosques_id,$mainConnector)){
                                                    $passed = false;
                                                }
                                            }else{
                                                $passed = false;
                                            }
                                        }
                                    }else{
                                        $passed = false;
                                    }
                                }
                            }else{
                                $passed = false;
                            }
                        }
                    }else{
                        $passed = false;
                    }
                }
            }else{
                $passed = false;
            }


            if($passed == false){
                $user = \Auth::user();
                \Common\Model\AidsCases::where('id',$id)->update(['status' => 1]);
                $action='CASE_UPDATED';
                $message=trans('common::application.edited data'). '  : '.' "'.$case->full_name. ' " ';
                \Log\Model\Log::saveNewLog($action,$message);
                \Common\Model\CasesStatusLog::create(['case_id'=>$id, 'user_id'=>$user->id, 'reason'=>trans('common::application.Disabled your recorded status for'). ' "'.$case->full_name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization') , 'status'=>1, 'date'=>date("Y-m-d")]);
                \Log\Model\Log::saveNewLog('CASE_UPDATED',trans('common::application.Disabled your recorded status for') . ' "'.$case->full_name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization'));
                $msg = trans('common::application.saved and  disabled because it is not in organization locations');

                $transferObj =  Transfers::where(['person_id'=> $case->person_id,
                                                  'prev_adscountry_id'=> $case->adscountry_id, 'prev_adsdistrict_id'=> $case->adsdistrict_id,
                                                  'prev_adsregion_id'=> $case->adsregion_id, 'prev_adsneighborhood_id'=> $case->adsneighborhood_id,
                                                  'prev_adssquare_id'=> $case->adssquare_id, 'prev_adsmosques_id'=> $case->adsmosques_id,

                                                  'adscountry_id'=> $case->adscountry_id, 'adsdistrict_id'=> $case->adsdistrict_id,
                                                  'adsregion_id'=> $case->adsregion_id, 'adsneighborhood_id'=> $case->adsneighborhood_id,
                                                  'adssquare_id'=> $case->adssquare_id, 'adsmosques_id'=> $case->adsmosques_id,
                                                  'category_id' => $request->category_id])->first();

                if(!$transferObj){
                    Transfers::create(['created_at' => date('Y-m-d H:i:s'), 'person_id'=> $case->person_id,
                        'prev_adscountry_id'=> $case->adscountry_id, 'prev_adsdistrict_id'=> $case->adsdistrict_id,
                        'prev_adsregion_id'=> $case->adsregion_id, 'prev_adsneighborhood_id'=> $case->adsneighborhood_id,
                        'prev_adssquare_id'=> $case->adssquare_id, 'prev_adsmosques_id'=> $case->adsmosques_id,

                        'adscountry_id'=> $case->adscountry_id, 'adsdistrict_id'=> $case->adsdistrict_id,
                        'adsregion_id'=> $case->adsregion_id, 'adsneighborhood_id'=> $case->adsneighborhood_id,
                        'adssquare_id'=> $case->adssquare_id, 'adsmosques_id'=> $case->adsmosques_id,
                        'category_id' => $request->category_id, 'user_id'=> $user->id,
                        'organization_id' => $user->organization_id]);


                    $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$request->category_id)->first();
                    \Log\Model\Log::saveNewLog('TRANSFER_CREATED',trans('common::application.he migrated')  . ': ' . ' ' . $case->full_name . ' , '.trans('common::application.As a case')  .': ' . $categoryObj->name);
                }
                return response()->json(["status" => 'failed_disabled', "msg"  =>$msg ]);
            }

            return response()->json(['status'=>true]);

        } catch (\Exception $e) {
        }
    }

    // save recommendations of case
    public function saveRecommendations(\Illuminate\Http\Request $request, $id) {

        try {
            $this->authorize('update', \Common\Model\AidsCases::class);
            $options['sub']=true;
            $options['mode']=$request->mode;

            $input=[
                    'visitor' =>$request->visitor ,
                    'visitor_card' =>$request->visitor_card ,
                    'visitor_opinion' =>$request->visitor_opinion ,
                    'visitor_evaluation' =>$request->visitor_evaluation ,
                    'visited_at'=> $request->visited_at,
                    'notes' =>$request->notes
                   ];

            $error = \App\Http\Helpers::isValid($input,ValidatorModel::getPersonValidatorRules($request->get('priority'),$options));
            if($error)
                return response()->json($error);

            $input['visited_at']=date('Y-m-d',strtotime($input['visited_at']));
            \Common\Model\AidsCases::where('id',$id)->update($input);

            $case =\Common\Model\CaseModel::fetch(array('action' => 'edit', 'person'=> true,'full_name'=>true),$id);
            \Log\Model\Log::saveNewLog('CASE_UPDATED',trans("common::application.He edited the status data related to the social researcher's recommendations for the case") .': ' . ' "'.$case->full_name. '" ');

            $user = \Auth::user();
            $passed = true;
            \Common\Model\CaseModel::DisableCaseInNoConnected($case,$user->organization_id,$user->id);
            $mainConnector=OrgLocations::getConnected($case->organization_id);
            if(!is_null($case->adsdistrict_id)){
                if(!in_array($case->adsdistrict_id,$mainConnector)){
                    $passed = false;
                }else{
                    if(!is_null($case->adsregion_id)){
                        if(!in_array($case->adsregion_id,$mainConnector)){
                            $passed = false;
                        }
                        else{

                            if(!is_null($case->adsneighborhood_id)){
                                if(!in_array($case->adsneighborhood_id,$mainConnector)){
                                    $passed = false;
                                }else{

                                    if(!is_null($case->adssquare_id)){
                                        if(!in_array($case->adssquare_id,$mainConnector)){
                                            $passed = false;
                                        }else{
                                            if(!is_null($case->adsmosques_id)){
                                                if(!in_array($case->adsmosques_id,$mainConnector)){
                                                    $passed = false;
                                                }
                                            }else{
                                                $passed = false;
                                            }
                                        }
                                    }else{
                                        $passed = false;
                                    }
                                }
                            }else{
                                $passed = false;
                            }
                        }
                    }else{
                        $passed = false;
                    }
                }
            }else{
                $passed = false;
            }

            if($passed == false){
                $user = \Auth::user();
                \Common\Model\AidsCases::where('id',$id)->update(['status' => 1]);
                $action='CASE_UPDATED';
                $message= trans('common::application.edited data') . '  : '.' "'.$case->full_name. ' " ';
                \Log\Model\Log::saveNewLog($action,$message);
                \Common\Model\CasesStatusLog::create(['case_id'=>$id, 'user_id'=>$user->id, 'reason'=>trans('common::application.Disabled your recorded status for'). ' "'.$case->full_name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization') , 'status'=>1, 'date'=>date("Y-m-d")]);
                \Log\Model\Log::saveNewLog('CASE_UPDATED',trans('common::application.Disabled your recorded status for') . ' "'.$case->full_name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization'));
                $msg = trans('common::application.saved and disabled because it is not in organization locations');

                $transferObj =  Transfers::where(['person_id'=> $case->person_id,
                                                  'prev_adscountry_id'=> $case->adscountry_id, 'prev_adsdistrict_id'=> $case->adsdistrict_id,
                                                  'prev_adsregion_id'=> $case->adsregion_id, 'prev_adsneighborhood_id'=> $case->adsneighborhood_id,
                                                  'prev_adssquare_id'=> $case->adssquare_id, 'prev_adsmosques_id'=> $case->adsmosques_id,
                                                  'adscountry_id'=> $case->adscountry_id, 'adsdistrict_id'=> $case->adsdistrict_id,
                                                  'adsregion_id'=> $case->adsregion_id, 'adsneighborhood_id'=> $case->adsneighborhood_id,
                                                  'adssquare_id'=> $case->adssquare_id, 'adsmosques_id'=> $case->adsmosques_id,
                                                  'category_id' => $request->category_id])->first();

                if(!$transferObj){
                    Transfers::create(['created_at' => date('Y-m-d H:i:s'), 'person_id'=> $case->person_id,
                        'prev_adscountry_id'=> $case->adscountry_id, 'prev_adsdistrict_id'=> $case->adsdistrict_id,
                        'prev_adsregion_id'=> $case->adsregion_id, 'prev_adsneighborhood_id'=> $case->adsneighborhood_id,
                        'prev_adssquare_id'=> $case->adssquare_id, 'prev_adsmosques_id'=> $case->adsmosques_id,
                        'adscountry_id'=> $case->adscountry_id, 'adsdistrict_id'=> $case->adsdistrict_id,
                        'adsregion_id'=> $case->adsregion_id, 'adsneighborhood_id'=> $case->adsneighborhood_id,
                        'adssquare_id'=> $case->adssquare_id, 'adsmosques_id'=> $case->adsmosques_id,
                        'category_id' => $request->category_id, 'user_id'=> $user->id,
                        'organization_id' => $user->organization_id]);


                    $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$request->category_id)->first();
                    \Log\Model\Log::saveNewLog('TRANSFER_CREATED', trans('common::application.he migrated').': ' . ' ' . $case->full_name . ' ,'.trans('common::application.As a case').': ' . $categoryObj->name);
                }
                return response()->json(["status" => 'failed_disabled', "msg"  =>$msg ]);
            }

            return response()->json(['status'=>'success','msg' => trans('common::application.save successful')]);
        } catch (\Exception $e) {
            return response()->json(["status" => 'error' ,"error_code" => $e->getCode(),"error_message"=> $e->getMessage()]);
        }
    }

    // import from excel to update case data
    public function importToUpdate(Request $request)
    {
//        $this->authorize('import', \Common\Model\AidsCases::class);
        return parent::importToUpdate($request);
    }

    // import from excel to add case
    public function import(Request $request)
    {
//        $this->authorize('import', \Common\Model\AidsCases::class);
        if (isset($request->import_type)) {
            if ($request->import_type == 2) {
                return parent::importCards($request);
            }
        }

        return parent::import($request);
    }


    // import from excel to add case family
    public function importFamily(Request $request)
    {
//        $this->authorize('import', \Common\Model\AidsCases::class);
        return parent::importFamily($request);
    }

    // import case template
    public function importTemplate(){
        return response()->json(['download_token' => 'import-cases-template']);
    }

    // import case family template
    public function importFamilyTemplate(){
        return response()->json(['download_token' => 'aid-family-import-template']);
    }

    // import update case template
    public function importTpUpdateTemplate(){
        return parent::importTpUpdateTemplate();
    }

    // check Cases using excel sheet according card number
    public function check(Request $request)
    {
        return parent::check($request);
    }

    // *****************************************************************************
    // transfer person using id
    public function transferOne(Request $request)
    {

        $this->authorize('create', Transfers::class);

        $rules= [
            'category_id' => 'required',
            'adscountry_id' => 'required',
            'adsdistrict_id' => 'required',
            'adsregion_id' => 'required',
            'adsneighborhood_id' => 'required',
            'adssquare_id' => 'required',
            'adsmosques_id' => 'required'
        ];

        $person =Person::where('id',$request->person_id)->first();

        $inputs = ['person_id'=> $request->person_id ,
                   'prev_adscountry_id'=> $person->adscountry_id, 'prev_adsdistrict_id'=> $person->adsdistrict_id,
                   'prev_adsregion_id'=> $person->adsregion_id, 'prev_adsneighborhood_id'=> $person->adsneighborhood_id,
                   'prev_adssquare_id'=> $person->adssquare_id, 'prev_adsmosques_id'=> $person->adsmosques_id,

                   'adscountry_id'=> $request->adscountry_id, 'adsdistrict_id'=> $request->adsdistrict_id,
                   'adsregion_id'=> $request->adsregion_id, 'adsneighborhood_id'=> $request->adsneighborhood_id,
                   'adssquare_id'=> $request->adssquare_id, 'adsmosques_id'=> $request->adsmosques_id,
                   'category_id' => $request->category_id];

        $error = \App\Http\Helpers::isValid($inputs,$rules);
        if($error)
            return response()->json($error);


        $full_name = $person->first_name .' ' .$person->second_name.' ' .$person->third_name.' ' .$person->last_name .' ';

        if(!is_null($person->death_date)){
            return response()->json(['status' => 'failed' , 'msg' =>trans('common::application.this_card_for_dead_person')]);
        }

        $transferObj =  Transfers::where($inputs)->first();

        if(!$transferObj){
            $user = \Auth::user();
            $inputs['created_at'] = date('Y-m-d H:i:s');
            $inputs['user_id'] = $user->organization_id ;
            $inputs['organization_id'] = $user->id ;

            Transfers::create($inputs);
            $categoryObj = AidsCategories::where('id',$request->category_id)->first();
            $organizations =  OrgLocations::LocationOrgs($request->adsmosques_id,$request->person_id,$request->category_id);
            $users =User::userHasPermission('reports.transfers.manage',$organizations);
            $url = "#/transfers";

            \Log\Model\Notifications::saveNotification(['user_id_to' =>$users, 'url' =>$url,
                                                        'message' => trans('common::application.he migrated') . ': ' . ' ' . $full_name . ' , '.
                                                                     trans('common::application.As a case') . ': ' . $categoryObj->name]);

            \Log\Model\Log::saveNewLog('TRANSFER_CREATED',trans('common::application.he migrated') . ': ' . ' ' . $full_name . ' , '.
                                                                         trans('common::application.As a case') . ': ' . $categoryObj->name);

            return response()->json(['status' => 'success' , 'msg' =>trans('common::application.action success')]);
        }

        return response()->json(['status' => 'success' , 'msg' =>trans('common::application.has_transfer_request')]);

    }

    // transfer persons using id
    public function transferGroup(Request $request)
    {
//        $this->authorize('create', Transfers::class);

        $rules= [
            'category_id' => 'required',
            'adscountry_id' => 'required',
            'adsdistrict_id' => 'required',
            'adsregion_id' => 'required',
            'adsneighborhood_id' => 'required',
            'adssquare_id' => 'required',
            'adsmosques_id' => 'required'
        ];

        $inputs = ['adscountry_id'=> $request->adscountry_id, 'adsdistrict_id'=> $request->adsdistrict_id,
            'adsregion_id'=> $request->adsregion_id, 'adsneighborhood_id'=> $request->adsneighborhood_id,
            'adssquare_id'=> $request->adssquare_id, 'adsmosques_id'=> $request->adsmosques_id,
            'category_id' => $request->category_id];

        $error = \App\Http\Helpers::isValid($inputs,$rules);
        if($error)
            return response()->json($error);

        if($request->source == 'file'){
            $validFile=true;
            $duplicated =0;
            $success =0;

            $final_records=[];
            $invalid_cards=[];
            $processed=[];
            $importFile=$request->file ;
            if ($importFile->isValid()) {
                $path =  $importFile->getRealPath();
                $excel = \PHPExcel_IOFactory::load( $path );
                $sheets = $excel->getSheetNames();
                if(\App\Http\Helpers::sheetFound("data",$sheets)){
                    $first=Excel::selectSheets('data')->load($path)->first();

                    if(!is_null($first)){
                        $keys = $first->keys()->toArray();
                        if(sizeof($keys) >0 ){

                            $validFile = false;
                            foreach ($keys as $item) {
                                if ( $item == 'rkm_alhoy' ) {
                                    $validFile = true;
                                    break;
                                }
                            }

                            if($validFile){
                                $records= \Excel::selectSheets('data')->load($path)->get();
                                $total=sizeof($records);


                                return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                            }
                            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                        }
                    }
                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
                }
                return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
            }
            return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

        }else{
            $items = $request->persons;
            $size = sizeof($items);
            if($size > 0){
                $user = \Auth::user();
                $categoryObj = AidsCategories::where('id',$request->category_id)->first();
                $inputs['user_id'] = $user->organization_id ;
                $inputs['organization_id'] = $user->id ;

                $success = 0;
                $old = 0;
                $deceased = 0;

                foreach ($items as $item){
                    $person=Person::where(['id'=>$item])->first();
                    $full_name = $person->first_name .' ' .$person->second_name.' ' .$person->third_name.' ' .$person->last_name .' ';

                    if(!is_null($person->death_date)){
                        $deceased++;
                    }else{

                        $inputs_ = $inputs;

                        $inputs_['prev_adscountry_id'] = $person->adscountry_id;
                        $inputs_['prev_adsdistrict_id'] = $person->adsdistrict_id;
                        $inputs_['prev_adsregion_id'] = $person->adsregion_id;
                        $inputs_['prev_adsneighborhood_id'] = $person->adsneighborhood_id;
                        $inputs_['prev_adssquare_id'] = $person->adssquare_id;
                        $inputs_['prev_adsmosques_id'] = $person->adsmosques_id;
                        $inputs_['person_id'] = $person->id;

                        $transferObj =  Transfers::where($inputs_)->first();

                        if(!$transferObj){
                            $inputs_['created_at'] = date('Y-m-d H:i:s');

                            Transfers::create($inputs_);
                            $organizations =  OrgLocations::LocationOrgs($request->adsmosques_id,$person->id,$request->category_id);
                            $users =User::userHasPermission('reports.transfers.manage',$organizations);
                            $url = "#/transfers";

                            \Log\Model\Notifications::saveNotification(['user_id_to' =>$users, 'url' =>$url,
                                'message' => trans('common::application.he migrated') . ': ' . ' ' . $full_name . ' , '.
                                    trans('common::application.As a case') . ': ' . $categoryObj->name]);

                            \Log\Model\Log::saveNewLog('TRANSFER_CREATED',trans('common::application.he migrated') . ': ' . ' ' . $full_name . ' , '.
                                trans('common::application.As a case') . ': ' . $categoryObj->name);

                            $success++;
                        }else{
                            $old++;
                        }
                    }

                }

                if( $success == $size ){
                    return response()->json(['status' => 'success' , 'msg' =>trans('common::application.action success')]);
                }
                else if( $old == $size ){
                    return response()->json(['status' => 'success' , 'msg' =>trans('common::application.all_person_has_transfer_request')]);
                }
                else{

                    if($success != 0){
                        $response =['status' => 'success','msg'=>
                            trans('common::application.Not all cases have been migrated, number of cases').' :  '. $size . ' ,  '.
                            trans('common::application.Number of migrated cases').' :  ' .$success . ' ,  ' .
                            trans('aid::application.dead_person').' :  ' .$deceased . ' ,  ' .
                            trans('common::application.has_transfer_request') .' :  ' .$old
                        ];
                    }
                    else{
                        $response = ['status' => 'failed','msg'=>
                            trans('common::application.Not all cases have been migrated, number of cases').' :  '. $size . ' ,  '.
                            trans('common::application.Number of migrated cases').' :  ' .$success . ' ,  ' .
                            trans('aid::application.dead_person').' :  ' .$deceased . ' ,  ' .
                            trans('common::application.has_transfer_request') .' :  ' .$old
                        ];
                    }

                }
                return response()->json($response);
        }
        }
        return response()->json(['status' => 'error' , 'msg' =>trans('common::application.no persons to make transfer request')]);

    }


}