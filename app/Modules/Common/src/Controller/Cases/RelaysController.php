<?php

namespace Common\Controller\Cases;

use App\Http\Controllers\Controller;
use Auth\Model\User;
use Common\Model\AidsCases;
use Common\Model\BlockIDCard;
use Common\Model\CaseModel;
use Common\Model\CloneGovernmentPersons;
use Common\Model\Person;
use Common\Model\GovServices;
use Common\Model\SponsorshipsCases;
use Illuminate\Http\Request;
use Common\Model\Relays\Relays;
use Common\Model\Relays\ConfirmRelays;
use Excel;
use Organization\Model\OrgLocations;

class RelaysController  extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // filter un confirm relays according conditions
    public function filter(Request $request)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $this->authorize('manage', Relays::class);
        $user = \Auth::user();
        $items=Relays::filter($request->page, $request->all());
//        return response()->json($items);
        if($request->get('action')=='xlsx'){

            $token = md5(uniqid());
            \Maatwebsite\Excel\Facades\Excel::create('export_' . $token, function($excel) use($items) {
                $excel->sheet(trans('common::application.relays'),function($sheet) use($items) {
                    $sheet->setRightToLeft(true);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);
                    $sheet->setOrientation('landscape');
                    $sheet->setRightToLeft(true);
                    $sheet->setStyle([
                        'font' => [
                            'name' => 'Simplified Arabic',
                            'size' => 11,
                            'bold' => true
                        ]
                    ]);
                    $sheet->setWidth(['A'=>5 ,'B'=> 15,'C'=> 20,'D'=> 20,'E'=> 20,'F'=> 20, 'G'=> 20,'H'=> 30,'I'=> 20,'J'=> 20]);

                    $sheet->row(1, function($row) { $row->setBackground('#CCCCCC'); });
                    $sheet->row(1, function($row) {  $row->setValignment('center');});

                    $sheet->getStyle("A1:J1")->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      => [ 'name' =>'Simplified Arabic','size'=> 11, 'bold'=>  true ]
                    ]);

                    $sheet ->setCellValue('A1',trans('common::application.#'));
                    $sheet ->setCellValue('B1',trans('common::application.id_card_number'));
                    $sheet ->setCellValue('C1',trans('common::application.first_name'));
                    $sheet ->setCellValue('D1',trans('common::application.second_name'));
                    $sheet ->setCellValue('E1',trans('common::application.third_name'));
                    $sheet ->setCellValue('F1',trans('common::application.last_name'));
                    $sheet ->setCellValue('G1',trans('common::application.category_name_'));
                    $sheet ->setCellValue('H1',trans('common::application.primary_mobile'));
                    $sheet ->setCellValue('I1',trans('common::application.relay_date'));
                    $sheet ->setCellValue('J1',trans('common::application.relay_user_name'));

                    $z= 2;
                    foreach($items as $k=>$v ){
                        $sheet ->setCellValue('A'.$z,$k+1);
                        $sheet ->setCellValue('B'.$z,is_null($v->id_card_number) ? '-' : $v->id_card_number);
                        $sheet ->setCellValue('C'.$z,is_null($v->first_name) ? '-' : $v->first_name);
                        $sheet ->setCellValue('D'.$z,is_null($v->second_name) ? '-' : $v->second_name);
                        $sheet ->setCellValue('E'.$z,is_null($v->third_name) ? '-' : $v->third_name);
                        $sheet ->setCellValue('F'.$z,is_null($v->last_name) ? '-' : $v->last_name);
                        $sheet ->setCellValue('G'.$z,is_null($v->category_name) ? '-' : $v->category_name);
                        $sheet ->setCellValue('H'.$z,is_null($v->primary_mobile) ? '-' : $v->primary_mobile);
                        $sheet ->setCellValue('I'.$z,is_null($v->created_at) ? '-' : $v->created_at);
                        $sheet ->setCellValue('J'.$z,is_null($v->user_name) ? '-' : $v->user_name);
                        $z++;
                    }

                });
            })->store('xlsx', storage_path('tmp/'));
            return response()->json([
                'download_token' => $token,
            ]);
        }
        return response()->json($items);

    }

    // filter confirm relays according conditions
    public function confirmFilter(Request $request)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $this->authorize('manageConfirm', Relays::class);
        $user = \Auth::user();
        $items=ConfirmRelays::filter($request->page, $user->organization_id, $request->all());
        if($request->get('action')=='xlsx'){

            $token = md5(uniqid());
            $category_type = $request->category_type;
            \Maatwebsite\Excel\Facades\Excel::create('export_' . $token, function($excel) use($items,$category_type) {
                $excel->sheet(trans('common::application.confirm relays'),function($sheet) use($items , $category_type) {
                    $sheet->setRightToLeft(true);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);
                    $sheet->setOrientation('landscape');
                    $sheet->setRightToLeft(true);
                    $sheet->setStyle([
                        'font' => [
                            'name' => 'Simplified Arabic',
                            'size' => 11,
                            'bold' => true
                        ]
                    ]);


                    if($category_type == 1){
                        $sheet->setWidth(['A'=>5 ,'B'=> 35,'C'=> 20,'D'=> 20,'E'=> 20,'F'=> 20, 'G'=> 20,'H'=> 30,'I'=> 20,
                            'J'=> 30,'K'=> 20,'L'=> 20,'M'=> 20,'N'=> 25,'O'=> 25,'P'=> 25,'Q'=> 25,'R'=> 25,'S'=> 25,'T'=> 45]);
                        $sheet->getStyle( "A1:T1")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 11, 'bold'=>  true ]
                        ]);
                    }else{
                        $sheet->setWidth(['A'=>5 ,'B'=> 35,'C'=> 20,'D'=> 20,'E'=> 20,'F'=> 20, 'G'=> 20,'H'=> 30,'I'=> 20,
                            'J'=> 30,'K'=> 20,'L'=> 20,'M'=> 20,'N'=> 25,'O'=> 25,'P'=> 25,'Q'=> 25,'R'=> 25,'S'=> 25,'T'=> 25,'U'=> 45]);

                        $sheet->getStyle( "A1:U1")->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      => [ 'name' =>'Simplified Arabic','size'=> 11, 'bold'=>  true ]
                        ]);

                    }


                    $sheet ->setCellValue('A1',trans('common::application.#'));
                    $sheet ->setCellValue('B1',trans('common::application.organization_name'));
                    $sheet ->setCellValue('C1',trans('common::application.id_card_number'));
                    $sheet ->setCellValue('D1',trans('common::application.first_name'));
                    $sheet ->setCellValue('E1',trans('common::application.second_name'));
                    $sheet ->setCellValue('F1',trans('common::application.third_name'));
                    $sheet ->setCellValue('G1',trans('common::application.last_name'));
                    $sheet ->setCellValue('H1',trans('common::application.category_name_'));
                    $sheet ->setCellValue('I1',trans('common::application.primary_mobile'));
                    $sheet ->setCellValue('J1',trans('common::application.relay_date'));
                    $sheet ->setCellValue('K1',trans('common::application.relay_user_name'));
                    $sheet ->setCellValue('L1',trans('common::application.confirm_relay_date'));
                    $sheet ->setCellValue('M1',trans('common::application.confirm_relay_user_name'));

                    if($category_type == 1){
                        $sheet ->setCellValue('N1',trans('common::application.country'));
                        $sheet ->setCellValue('O1',trans('common::application.district'));
                        $sheet ->setCellValue('P1',trans('common::application.city'));
                        $sheet ->setCellValue('Q1',trans('common::application.nearlocation'));
                        $sheet ->setCellValue('R1',trans('common::application.mosques'));
                        $sheet ->setCellValue('S1',trans('common::application.street_address_'));

                    }else{
                        $sheet ->setCellValue('N1',trans('common::application.country'));
                        $sheet ->setCellValue('O1',trans('common::application.district'));
                        $sheet ->setCellValue('P1',trans('common::application.region_'));
                        $sheet ->setCellValue('Q1',trans('common::application.nearlocation_'));
                        $sheet ->setCellValue('R1',trans('common::application.square'));
                        $sheet ->setCellValue('S1',trans('common::application.mosques'));
                        $sheet ->setCellValue('T1',trans('common::application.street_address_'));
                    }
                    $z= 2;
                    foreach($items as $k=>$v ){
                        $sheet ->setCellValue('A'.$z,$k+1);
                        $sheet ->setCellValue('B'.$z,is_null($v->organization_name) ? '-' : $v->organization_name);
                        $sheet ->setCellValue('C'.$z,is_null($v->id_card_number) ? '-' : $v->id_card_number);
                        $sheet ->setCellValue('D'.$z,is_null($v->first_name) ? '-' : $v->first_name);
                        $sheet ->setCellValue('E'.$z,is_null($v->second_name) ? '-' : $v->second_name);
                        $sheet ->setCellValue('F'.$z,is_null($v->third_name) ? '-' : $v->third_name);
                        $sheet ->setCellValue('G'.$z,is_null($v->last_name) ? '-' : $v->last_name);
                        $sheet ->setCellValue('H'.$z,is_null($v->category_name) ? '-' : $v->category_name);
                        $sheet ->setCellValue('I'.$z,is_null($v->primary_mobile) ? '-' : $v->primary_mobile);
                        $sheet ->setCellValue('J'.$z,is_null($v->relay_date) ? '-' : $v->relay_date);
                        $sheet ->setCellValue('K'.$z,is_null($v->user_name) ? '-' : $v->user_name);
                        $sheet ->setCellValue('L'.$z,is_null($v->confirm_relay_date) ? '-' : $v->confirm_relay_date);
                        $sheet ->setCellValue('M'.$z,is_null($v->confirm_user_name) ? '-' : $v->confirm_user_name);

                        if($category_type == 1){
                            $sheet ->setCellValue('N'.$z,is_null($v->country_) ? '-' : $v->country_);
                            $sheet ->setCellValue('O'.$z,is_null($v->governarate_) ? '-' : $v->governarate_);
                            $sheet ->setCellValue('P'.$z,is_null($v->city_) ? '-' : $v->city_);
                            $sheet ->setCellValue('Q'.$z,is_null($v->nearLocation_) ? '-' : $v->nearLocation_);
                            $sheet ->setCellValue('R'.$z,is_null($v->mosque_name_) ? '-' : $v->mosque_name_);
                            $sheet ->setCellValue('S'.$z,is_null($v->street_address) ? '-' : $v->street_address);

                        }else{
                            $sheet ->setCellValue('N'.$z,is_null($v->country_) ? '-' : $v->country_);
                            $sheet ->setCellValue('O'.$z,is_null($v->governarate_) ? '-' : $v->governarate_);
                            $sheet ->setCellValue('P'.$z,is_null($v->region_name_) ? '-' : $v->region_name_);
                            $sheet ->setCellValue('Q'.$z,is_null($v->nearLocation_) ? '-' : $v->nearLocation_);
                            $sheet ->setCellValue('R'.$z,is_null($v->square_name_) ? '-' : $v->square_name_);
                            $sheet ->setCellValue('S'.$z,is_null($v->mosque_name_) ? '-' : $v->mosque_name_);
                            $sheet ->setCellValue('T'.$z,is_null($v->street_address) ? '-' : $v->street_address);
                        }

                        $z++;
                    }

                });
            })->store('xlsx', storage_path('tmp/'));
            return response()->json([
                'download_token' => $token,
            ]);
        }

        return response()->json($items);

    }

    // confirm relay person using id
    public function confirm(Request $request)
    {
        $this->authorize('confirm', Relays::class);
        $relay=Relays::where(['id'=>$request->relay_id])->first();
        if($relay){
            $person=Person::where(['id'=>$relay->person_id])->first();
            if($person){
                $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$relay->category_id)->first();
                if(BlockIDCard::isBlock($person->id_card_number,$relay->category_id,1)){
                    return response()->json(['status' => 'error' , 'msg' => trans('common::application.The owner of the ID is forbidden to become as case'). ':' . $categoryObj->name]);
                }

                $user = \Auth::user();
                $passed = true;
                if($request->categoryType == 2){
                    $person->full_name = $person->first_name .' ' .$person->second_name.' ' .$person->third_name.' ' .$person->last_name .' ';
                    CaseModel::DisablePersonInNoConnected($person,$user->organization_id,$user->id);
                    $mainConnector=OrgLocations::getConnected($user->organization_id);
                    if(!is_null($person->adsdistrict_id)){
                        if(!in_array($person->adsdistrict_id,$mainConnector)){
                            $passed = false;
                        }else{
                            if(!is_null($person->adsregion_id)){
                                if(!in_array($person->adsregion_id,$mainConnector)){
                                    $passed = false;
                                }else{

                                    if(!is_null($person->adsneighborhood_id)){
                                        if(!in_array($person->adsneighborhood_id,$mainConnector)){
                                            $passed = false;
                                        }else{

                                            if(!is_null($person->adssquare_id)){
                                                if(!in_array($person->adssquare_id,$mainConnector)){
                                                    $passed = false;
                                                }else{
                                                    if(!is_null($person->adsmosques_id)){
                                                        if(!in_array($person->adsmosques_id,$mainConnector)){
                                                            $passed = false;
                                                        }
                                                    }else{
                                                        $passed = false;
                                                    }
                                                }
                                            }else{
                                                $passed = false;
                                            }
                                        }
                                    }else{
                                        $passed = false;
                                    }
                                }
                            }else{
                                $passed = false;
                            }
                        }
                    }else{
                        $passed = false;
                    }
                }

                if($passed == true){
                    $logs = ConfirmRelays::where(['relay_id'=> $request->relay_id, 'organization_id' => $user->organization_id])->first();
                    if(is_null($logs)){
                        $inputs_ = ['person_id' => $relay->person_id,
                            'category_id' => $relay->category_id,
                            'organization_id' => $user->organization_id,
                        ];

                        $save= false;
                        $case = null;
                        if($request->categoryType == 1){
                            $case = SponsorshipsCases::where($inputs_)->first();
                            if(is_null($case)){
                                $inputs_['user_id']=$user->id;
                                $inputs_['created_at']=date('Y-m-d H:i:s');
                                $inputs_['status']=0;
                                $case=  \Common\Model\SponsorshipsCases::create($inputs_);
                                \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id, 'reason'=>trans('common::application.case was created'),
                                    'status'=>0, 'date'=>date("Y-m-d")]);
                                \Common\Model\CaseModel::where(['id' => $case->id ])->update(['rank' =>\Common\Model\AbstractCases::updateRank($case->id)]);
                                \Log\Model\Log::saveNewLog('RELAY_CREATED',trans('common::application.confirm').': ' . ' ' . $person->full_name . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);

                                $save= true;
                            }
                        }else{
                            $case = AidsCases::where($inputs_)->first();
                            if(is_null($case)){
                                $inputs_['user_id']=$user->id;
                                $inputs_['created_at']=date('Y-m-d H:i:s');
                                $inputs_['status']=0;
                                $case=  \Common\Model\AidsCases::create($inputs_);
                                \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id, 'reason'=>trans('common::application.case was created'), 'status'=>0, 'date'=>date("Y-m-d")]);
                                \Common\Model\CaseModel::where(['id' => $case->id ])->update(['rank' =>\Common\Model\AbstractCases::updateRank($case->id)]);
                                \Log\Model\Log::saveNewLog('RELAY_CREATED',trans('common::application.confirm').': ' . ' ' . $person->full_name . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);
                                $save= true;
                            }
                        }

                        if($save){
                            ConfirmRelays::create(['relay_id'=> $request->relay_id,
                                'user_id'=> $user->id,
                                'organization_id' => $user->organization_id]);


                            if($request->categoryType == 2) {
                                $url = "#/relay/confirm/aids";
                            }else{
                                $url = "#/relay/confirm/sponsorships";
                            }

                            \Log\Model\Notifications::saveNotification([
                                'user_id_to' =>[$relay->user_id],
                                'url' =>$url,
                                'message' => trans('common::application.confirm').': ' . ' ' . $person->full_name . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name
                            ]);
                            \Log\Model\Log::saveNewLog('RELAY_CREATED', trans('common::application.confirm').': ' . ' ' . $person->full_name . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);

                        }
                        return response()->json(['status' => 'success' , 'msg' =>trans('common::application.The person successfully approved') , 'case' => $case]);
                    }
                    return response()->json(['status' => 'error' , 'msg' =>trans('common::application.Someone already saved by an employee of your org')]);

                }
                else{
                    $action='PERSON_UPDATED';
                    $message=trans('common::application.edited data') . '  : '.' "'.$person->full_name. ' " ';
                    \Log\Model\Log::saveNewLog($action,$message);
                    $message = trans('common::application.The data of the deported person was saved').': ' . ' ' . $person->full_name . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name;
                    \Log\Model\Log::saveNewLog('RELAY_CREATED',$message);
                    return response()->json(["status" => 'failed_disabled', "msg"  =>$message ]);
                }
            }
            return response()->json(['status' => 'error' , 'msg' => trans('common::application.Someone not in the system')]);
        }
        return response()->json(['status' => 'error' , 'msg' =>trans('common::application.action failed')]);

    }

    // confirm relay persons using id
    public function confirmGroup(Request $request)
    {
        $this->authorize('confirm', Relays::class);

        $items = $request->persons;
        $relays = sizeof($items);
        if($relays > 0){
            $success = 0;
            $not_person = 0;
            $restricted = 0;
            $invalid = 0;
            $restricted_ = [];

            foreach ($items as $item){
                $relay=Relays::where(['id'=>$item])->first();
                if($relay){
                    $person=Person::where(['id'=>$relay->person_id])->first();
                    if($person){
                        $person->full_name = $person->first_name .' ' .$person->second_name.' ' .$person->third_name.' ' .$person->last_name .' ';
                        $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$relay->category_id)->first();
                        if(BlockIDCard::isBlock($person->id_card_number,$relay->category_id,1)){
                            $restricted_[]=['id_card_number' => $person->id_card_number ,
                                           'name' =>$person->full_name ,
                                           'reason' => trans('common::application.The owner of the ID is forbidden to become as case'). ':' . $categoryObj->name];
                            $restricted++;
                        }else{
                            $user = \Auth::user();
                            if($request->categoryType == 2){
                                $passed = true;
                                CaseModel::DisablePersonInNoConnected($person,$user->organization_id,$user->id);
                                $mainConnector=OrgLocations::getConnected($user->organization_id);
                                if(!is_null($person->adsdistrict_id)){
                                    if(!in_array($person->adsdistrict_id,$mainConnector)){
                                        $passed = false;
                                    }else{
                                        if(!is_null($person->adsregion_id)){
                                            if(!in_array($person->adsregion_id,$mainConnector)){
                                                $passed = false;
                                            }else{

                                                if(!is_null($person->adsneighborhood_id)){
                                                    if(!in_array($person->adsneighborhood_id,$mainConnector)){
                                                        $passed = false;
                                                    }else{

                                                        if(!is_null($person->adssquare_id)){
                                                            if(!in_array($person->adssquare_id,$mainConnector)){
                                                                $passed = false;
                                                            }else{
                                                                if(!is_null($person->adsmosques_id)){
                                                                    if(!in_array($person->adsmosques_id,$mainConnector)){
                                                                        $passed = false;
                                                                    }
                                                                }else{
                                                                    $passed = false;
                                                                }
                                                            }
                                                        }else{
                                                            $passed = false;
                                                        }
                                                    }
                                                }else{
                                                    $passed = false;
                                                }
                                            }
                                        }else{
                                            $passed = false;
                                        }
                                    }
                                }else{
                                    $passed = false;
                                }
                            }

                            if($passed == true){
                                $success++;
                                $success_[]=['id_card_number' => $person->id_card_number , 'name' =>$person->full_name ];
                                $logs = ConfirmRelays::where(['relay_id'=> $item, 'organization_id' => $user->organization_id])->first();
                                if(is_null($logs)){
                                    $inputs_ = ['person_id' => $relay->person_id,
                                        'category_id' => $relay->category_id,
                                        'organization_id' => $user->organization_id,
                                    ];

                                    $save= false;
                                    $case = null;
                                    if($request->categoryType == 1){
                                        $case = SponsorshipsCases::where($inputs_)->first();
                                        if(is_null($case)){
                                            $inputs_['user_id']=$user->id;
                                            $inputs_['created_at']=date('Y-m-d H:i:s');
                                            $inputs_['status']=0;
                                            $case=  \Common\Model\SponsorshipsCases::create($inputs_);
                                            \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id, 'reason'=>trans('common::application.case was created'),
                                                'status'=>0, 'date'=>date("Y-m-d")]);
                                            \Common\Model\CaseModel::where(['id' => $case->id ])->update(['rank' =>\Common\Model\AbstractCases::updateRank($case->id)]);
                                            \Log\Model\Log::saveNewLog('RELAY_CREATED',trans('common::application.confirm').': ' . ' ' . $person->full_name . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);

                                            $save= true;
                                        }
                                    }else{
                                        $case = AidsCases::where($inputs_)->first();
                                        if(is_null($case)){
                                            $inputs_['user_id']=$user->id;
                                            $inputs_['created_at']=date('Y-m-d H:i:s');
                                            $inputs_['status']=0;
                                            $case=  \Common\Model\AidsCases::create($inputs_);
                                            \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id, 'reason'=>trans('common::application.case was created'), 'status'=>0, 'date'=>date("Y-m-d")]);
                                            \Common\Model\CaseModel::where(['id' => $case->id ])->update(['rank' =>\Common\Model\AbstractCases::updateRank($case->id)]);
                                            \Log\Model\Log::saveNewLog('RELAY_CREATED',trans('common::application.confirm').': ' . ' ' . $person->full_name . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);
                                            $save= true;
                                        }
                                    }

                                    if($save){
                                        ConfirmRelays::create(['relay_id'=> $item,
                                            'user_id'=> $user->id,
                                            'organization_id' => $user->organization_id]);


                                        if($request->categoryType == 2) {
                                            $url = "#/relay/confirm/aids";
                                        }else{
                                            $url = "#/relay/confirm/sponsorships";
                                        }

                                        \Log\Model\Notifications::saveNotification([
                                            'user_id_to' =>[$relay->user_id],
                                            'url' =>$url,
                                            'message' => trans('common::application.confirm').': ' . ' ' . $person->full_name . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name
                                        ]);
                                        \Log\Model\Log::saveNewLog('RELAY_CREATED',trans('common::application.confirm').': ' . ' ' . $person->full_name . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);

                                    }

                                }




                                return response()->json(['status' => 'success' , 'msg' =>trans('common::application.The person successfully approved') , 'case' => $case]);

                            }
                            else{
                                $restricted_[]=['id_card_number' => $person->id_card_number ,
                                    'name' =>$person->full_name ,
                                    'reason' => trans('common::application.The data of the deported person was saved').': ' . ' ' . $person->full_name];
                                $restricted++;
                            }
                        }

                    }else{
                        $not_person++;
                    }
                }else{
                    $invalid++;
                }
            }

            if( $success == $relays ){
                $response = ['status' => 'success','msg'=> trans('common::application.All cases migrated for your organization') ];
            }
            else if( $restricted == $relays ){
                $response = ['status' => 'failed','msg'=>
                    trans('common::application.Not all cases have been migrated, number of cases')  .' :  '. $relays . ' ,  '.
                    trans('common::application.number of people not registered') .' :  ' .$not_person . ' ,  ' .
                    trans('common::application.Number of people who have not been deported either to ban or because they are outside your organization') .' :  '.$restricted
                ];
            }else{

                if($success != 0){
                    $response =['status' => 'success','msg'=>
                        trans('common::application.Not all cases have been migrated, number of cases').' :  '. $relays . ' ,  '.
                        trans('common::application.Number of migrated cases').' :  ' .$success . ' ,  ' .
                        trans('common::application.number of people not registered') .' :  ' .$not_person . ' ,  ' .
                        trans('common::application.Number of people who have not been deported either to ban or because they are outside your organization').' :  '.$restricted
                    ];
                }
                else{
                    $response = ['status' => 'failed','msg'=>
                        trans('common::application.Not all cases have been migrated, number of cases').' :  '. $relays . ' ,  '.
                        trans('common::application.Number of migrated cases').' :  ' .$success . ' ,  ' .
                        trans('common::application.number of people not registered') .' :  ' .$not_person . ' ,  ' .
                        trans('common::application.Number of people who have not been deported either to ban or because they are outside your organization').' :  '.$restricted
                    ];
                }

            }
            if($restricted > 0 ){
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($restricted_) {
                    $excel->setTitle(trans('common::application.family_sheet'));
                    $excel->setDescription(trans('common::application.restricted_cards'));
                    $excel->sheet(trans('common::application.restricted_cards'), function($sheet) use($restricted_) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:C1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->setCellValue('A1',trans('common::application.id_card_number'));
                        $sheet->setCellValue('B1',trans('common::application.name'));
                        $sheet->setCellValue('C1',trans('common::application.reason'));

                        $z= 2;
                        foreach($restricted_ as $k=>$v){
                            $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                            $sheet ->setCellValue('B'.$z,$v['name']);
                            $sheet ->setCellValue('C'.$z,$v['reason']);
                            $z++;
                        }

                    });
                })->store('xlsx', storage_path('tmp/'));

                $response['msg'] = $response['msg'] .'<br>' .trans('common::application.The list of people who have not been deported will be downloaded') ;
                $response['download_token'] = $token;
            }

            return response()->json($response);
        }

        return response()->json(['status' => 'error' , 'msg' =>trans('common::application.You have not specified any confirmed update status for migration')]);

    }

    // relay persons using id
    public function relayOne(Request $request)
    {
        $this->authorize('RelayCitizenRepository', \Common\Model\AidsCases::class);

        $record = GovServices::personDataMap($request->reMap);

        if($request->type == 1){
            $rules= [
                'country' => 'required',
                'governarate' => 'required',
                'city' => 'required',
                'location_id' => 'required',
                'mosques_id' => 'required'
            ];

        }
        else{
            $rules= [
                'adscountry_id' => 'required',
                'adsdistrict_id' => 'required',
                'adsregion_id' => 'required',
                'adsneighborhood_id' => 'required',
                'adssquare_id' => 'required',
                'adsmosques_id' => 'required'
            ];
        }

        $inputs = $request->inputs;

        $error = \App\Http\Helpers::isValid($inputs,$rules);
        if($error)
            return response()->json($error);


        foreach ($inputs as $key=>$value){
            $record[$key]=$value;
        }

        $ifExist = Person::where(function ($q) use ($record){
                                            $q->where('id_card_number',$record['id_card_number']);
                                        })->first();

        if($ifExist){
            $record['person_id']=$ifExist->id;
        }
        $person =Person::savePerson($record);
        $name = $person['first_name'] .' ' .$person['second_name'].' ' .$person['third_name'].' ' .$person['last_name'] .' ';
        $person_id =$person['id'];

        if(!is_null($person['death_date'])){
            return response()->json(['status' => 'failed' , 'msg' =>trans('common::application.this_card_for_dead_person')]);
        }

        $user = \Auth::user();
        $categories = $request->categories;
        foreach ($categories as $category){
            $relaysObj =  Relays::where(['person_id'=> $person_id,'category_id' => $category])->first();

            if(!$relaysObj){
                Relays::create(['person_id'=> $person_id,
                    'category_id' => $category,
                    'user_id'=> $user->id,
                    'organization_id' => $user->organization_id]);

                $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$category)->first();

                $levelId = null;
                if(is_null($person['adsmosques_id'])) {
                    if(is_null($person['adssquare_id'])) {
                        if(is_null($person['adsneighborhood_id'])) {
                            if(is_null($person['adsregion_id'])) {
                                if(is_null($person['adsdistrict_id'])) {
                                    if(is_null($person['adscountry_id'])) {
                                        $levelId = $person['adscountry_id'] ;
                                    }
                                }else{
                                    $levelId = $person['adsdistrict_id'] ;
                                }
                            }else{
                                $levelId = $person['adsregion_id'] ;
                            }
                        }else{
                            $levelId = $person['adsneighborhood_id'] ;
                        }
                    }else{
                        $levelId = $person['adssquare_id'] ;
                    }
                }
                else{
                    $levelId = $person['adsmosques_id'] ;
                }

                $orgs =  OrgLocations::LocationOrgs($levelId,$person['id'],$category);
                $users =User::userHasPermission('reports.relays.confirm',$orgs);
                if($request->type == 2) {
                    $url = "#/relays/aids";
                }else{
                    $url = "#/relays/sponsorships";
                }

                \Log\Model\Notifications::saveNotification([
                    'user_id_to' =>$users,
                    'url' =>$url,
                    'message' => trans('common::application.he migrated').': ' . ' ' . $name . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name
                ]);

                \Log\Model\Log::saveNewLog('RELAY_CREATED',trans('common::application.he migrated').': ' . ' ' . $name . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);
            }

        }
        return response()->json(['status' => 'success' , 'msg' =>trans('common::application.action success')]);

    }

    // relay persons using excel data sheet
    public function relayFromExcel(Request $request)
    {
        $this->authorize('citizenRepository', \Common\Model\AidsCases::class);
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $validFile=true;
        $death_person =0;
        $invalid_cards_ =0;
        $duplicated =0;
        $success =0;
        $success_ =0;
        $done_ =0;

        $final_records=[];
        $invalid_cards=[];
        $processed=[];
        $user = \Auth::user();
        $user_id = $user->id;
        $organization_id = $user->organization_id;
        $category_id = $request->category_id;
        $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$category_id)->first();
        $save = $request->get('save');
        $type = $categoryObj->type;

        $importFile=$request->file ;
        if ($importFile->isValid()) {
            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();

                    if(sizeof($keys) >0 ){
                        if($request->category_type == 1){
                            $constraint =  [ "rkm_alhoy"];
                        }else{
                            $constraint =  [ "rkm_alhoy", "aldol", "almhafth", "almntk", "alhy",
                                "almrbaa", "almsjd", "tfasyl_alaanoan", "rkm_joal_asasy", "rkm_hatf"];
                        }

                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if($total == 0){
                                return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                            }

                            if ($total > 2000) {
                                foreach ($records as $key =>$value) {
                                    if(in_array($value['rkm_alhoy'],$processed)){
                                        $duplicated++;
                                    }
                                    else{
                                        if(strlen($value['rkm_alhoy'])  <= 9) {
                                            $card =(int)$value['rkm_alhoy'];
                                            $ifExist = \Common\Model\where(function ($q) use ($card){
                                                $q->where('id_card_number',$card);
                                            })->first();

                                            if($ifExist){
                                                $save_ = false;

                                                if(isset($value['tfasyl_alaanoan'])){
                                                    if(!is_null($value['tfasyl_alaanoan']) && $value['tfasyl_alaanoan'] != " " && $value['tfasyl_alaanoan'] != ""){
                                                        $save_ = true;
                                                        $ifExist->street_address = $value['tfasyl_alaanoan'];
                                                    }
                                                }

                                                if(isset($value['aldol'])){
                                                    if(!is_null($value['aldol']) && $value['aldol'] != " " && $value['aldol'] != ""){
                                                        $adscountryId =CaseModel::constantId('adscountry_id',$value['aldol'],false,null);
                                                        if(!is_null($adscountryId) && $adscountryId != " "){
                                                            $save_ = true;
                                                            $ifExist->adscountry_id = $adscountryId;
                                                            $ifExist->adsdistrict_id = null;
                                                            $ifExist->adsregion_id = null;
                                                            $ifExist->adsneighborhood_id  = null;
                                                            $ifExist->adssquare_id  = null;
                                                            $ifExist->adsmosques_id = null;
                                                        }
                                                    }
                                                }

                                                if(isset($value['almhafth'])){
                                                    if(!is_null($value['almhafth']) && $value['almhafth'] != " " && $value['almhafth'] != ""){
                                                        $adsdistrictId = CaseModel::constantId('adsdistrict_id',$value['almhafth'],true,$adscountryId);
                                                        if(!is_null($adsdistrictId) && $adsdistrictId != " "){
                                                            $save_ = true;
                                                            $ifExist->adsdistrict_id = $adsdistrictId;
                                                        }
                                                    }
                                                }

                                                if(isset($value['almntk'])){
                                                    if(!is_null($value['almntk']) && $value['almntk'] != " " && $value['almntk'] != ""){
                                                        $adsregionId = CaseModel::constantId('adsregion_id',$value['almntk'],true,$ifExist->adsdistrict_id);
                                                        if(!is_null($adsregionId) && $adsregionId != " "){
                                                            $save_ = true;
                                                            $ifExist->adsregion_id = $adsregionId;
                                                            $ifExist->adsneighborhood_id  = null;
                                                            $ifExist->adssquare_id  = null;
                                                            $ifExist->adsmosques_id = null;
                                                        }
                                                    }
                                                }

                                                if(isset($value['alhy'])){
                                                    if(!is_null($value['alhy']) && $value['alhy'] != " " && $value['alhy'] != ""){
                                                        $adsneighborhoodId = CaseModel::constantId('adsneighborhood_id',$value['alhy'],true,$ifExist->adsregion_id);
                                                        if(!is_null($adsneighborhoodId) && $adsneighborhoodId != " "){
                                                            $save_ = true;
                                                            $ifExist->adsneighborhood_id  = $adsneighborhoodId;
                                                        }
                                                    }
                                                }

                                                if(isset($value['almrbaa'])){
                                                    if(!is_null($value['almrbaa']) && $value['almrbaa'] != " " && $value['almrbaa'] != ""){
                                                        $adssquareId = CaseModel::constantId('adssquare_id',$value['almrbaa'],true,$ifExist->adsneighborhood_id);
                                                        if(!is_null($adssquareId) && $adssquareId != " "){
                                                            $save_ = true;
                                                            $ifExist->adssquare_id  = $adssquareId;
                                                            $ifExist->adsmosques_id = null;
                                                        }
                                                    }
                                                }

                                                if(isset($value['almsjd'])){
                                                    if(!is_null($value['almsjd']) && $value['almsjd'] != " " && $value['almsjd'] != ""){
                                                        $adsmosquesId = CaseModel::constantId('adsmosques_id',$value['almsjd'],true,$ifExist->adssquare_id);
                                                        if(!is_null($adsmosquesId) && $adsmosquesId != " "){
                                                            $save_ = true;
                                                            $ifExist->adsmosques_id = $adsmosquesId;
                                                        }
                                                    }
                                                }

                                                if(isset($value['rkm_joal_asasy'])){
                                                    if(!is_null($value['rkm_joal_asasy']) && $value['rkm_joal_asasy'] != " " && $value['rkm_joal_asasy'] != ""){
                                                        $primary_mobile=$value['rkm_joal_asasy'];
                                                        if(\Common\Model\PersonModels\PersonContact::where(['person_id' => $ifExist->id, 'contact_type' => 'primary_mobile'])->first()){
                                                            \Common\Model\PersonModels\PersonContact::where(['person_id' => $ifExist->id, 'contact_type' => 'primary_mobile'])
                                                                ->update(['contact_value' => $primary_mobile]);
                                                        }else{
                                                            \Common\Model\PersonModels\PersonContact::create(['person_id' => $ifExist->id, 'contact_type' => 'primary_mobile','contact_value' => $primary_mobile]);
                                                        }
                                                    }
                                                }

                                                if(isset($value['rkm_hatf'])){
                                                    if(!is_null($value['rkm_hatf']) && $value['rkm_hatf'] != " " && $value['rkm_hatf'] != ""){
                                                        $phone=$value['rkm_hatf'];
                                                        if(\Common\Model\PersonModels\PersonContact::where(['person_id' => $ifExist->id, 'contact_type' => 'phone'])->first()){
                                                            \Common\Model\PersonModels\PersonContact::where(['person_id' => $ifExist->id, 'contact_type' => 'phone'])
                                                                ->update(['contact_value' => $phone]);
                                                        }else{
                                                            \Common\Model\PersonModels\PersonContact::create(['person_id' => $ifExist->id, 'contact_type' => 'phone','contact_value' => $phone]);
                                                        }
                                                    }
                                                }

                                                if($save_){
                                                    $ifExist->save();
                                                }
                                                $processed[]=$card;

                                                CaseModel::DisablePersonInNoConnected($ifExist,$user->organization_id,$user->id);

                                                if($save == 'save' || $save == "save"){
                                                    $inputs_ = [ 'organization_id'=>$user->organization_id,
                                                        'person_id' =>$ifExist->id ,'category_id' => $category_id];
                                                    if($type == 1){
                                                        $case = SponsorshipsCases::where($inputs_)->first();

                                                        if(is_null($case)){
                                                            $inputs_['user_id']=$user->id;
                                                            $inputs_['created_at']=date('Y-m-d H:i:s');
                                                            $inputs_['status']=0;
                                                            $case=  \Common\Model\SponsorshipsCases::create($inputs_);
                                                            \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id, 'reason'=>trans('common::application.case was created'),
                                                                'status'=>0, 'date'=>date("Y-m-d")]);
                                                        }
                                                    }
                                                    else{
                                                        $passed = true;
                                                        $mainConnector=OrgLocations::getConnected($organization_id);
                                                        if(!is_null($ifExist->adsdistrict_id)){
                                                            if(!in_array($ifExist->adsdistrict_id,$mainConnector)){
                                                                $passed = false;
                                                            }else{
                                                                if(!is_null($ifExist->adsregion_id)){
                                                                    if(!in_array($ifExist->adsregion_id,$mainConnector)){
                                                                        $passed = false;
                                                                    }
                                                                    else{

                                                                        if(!is_null($ifExist->adsneighborhood_id)){
                                                                            if(!in_array($ifExist->adsneighborhood_id,$mainConnector)){
                                                                                $passed = false;
                                                                            }else{

                                                                                if(!is_null($ifExist->adssquare_id)){
                                                                                    if(!in_array($ifExist->adssquare_id,$mainConnector)){
                                                                                        $passed = false;
                                                                                    }else{
                                                                                        if(!is_null($ifExist->adsmosques_id)){
                                                                                            if(!in_array($ifExist->adsmosques_id,$mainConnector)){
                                                                                                $passed = false;
                                                                                            }
                                                                                        }else{
                                                                                            $passed = false;
                                                                                        }
                                                                                    }
                                                                                }else{
                                                                                    $passed = false;
                                                                                }
                                                                            }
                                                                        }else{
                                                                            $passed = false;
                                                                        }
                                                                    }
                                                                }else{
                                                                    $passed = false;
                                                                }
                                                            }
                                                        }else{
                                                            $passed = false;
                                                        }
                                                        if($passed == true){
                                                            $case = AidsCases::where($inputs_)->first();
                                                            if(is_null($case)){
                                                                $inputs_['user_id']=$user->id;
                                                                $inputs_['created_at']=date('Y-m-d H:i:s');
                                                                $inputs_['status']=0;
                                                                $case=  \Common\Model\AidsCases::create($inputs_);
                                                                \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id, 'reason'=>trans('common::application.case was created'),
                                                                    'status'=>0, 'date'=>date("Y-m-d")]);
                                                            }
                                                        }
                                                    }
                                                }
                                                $processed_person[]=$ifExist->id;
                                                $success++;
                                            }
                                            else{
                                                $row = GovServices::byCard($card);
                                                if($row['status'] == true) {
                                                    $r = $row['row'] ;
                                                    $record = Person::PersonMap($r);
                                                    $adscountryId=null;
                                                    $adsdistrictId=null;
                                                    $adsregionId=null;
                                                    $adsneighborhoodId=null;
                                                    $adssquareId=null;
                                                    $adsmosquesId=null;

                                                    CloneGovernmentPersons::saveNew((Object)$r);

                                                    if(isset($value['tfasyl_alaanoan'])){
                                                        if(!is_null($value['tfasyl_alaanoan']) && $value['tfasyl_alaanoan'] != " " && $value['tfasyl_alaanoan'] != ""){
                                                            $record['street_address'] = $value['tfasyl_alaanoan'];
                                                        }
                                                    }

                                                    if(isset($value['aldol'])){
                                                        if(!is_null($value['aldol']) && $value['aldol'] != " " && $value['aldol'] != ""){
                                                            $adscountryId =CaseModel::constantId('adscountry_id',$value['aldol'],false,null);
                                                            if(!is_null($adscountryId) && $adscountryId != " "){
                                                                $record['adscountry_id']=$adscountryId;

                                                            }
                                                        }
                                                    }

                                                    if(isset($value['almhafth'])){
                                                        if(!is_null($value['almhafth']) && $value['almhafth'] != " " && $value['almhafth'] != ""){
                                                            $adsdistrictId = CaseModel::constantId('adsdistrict_id',$value['almhafth'],true,$adscountryId);
                                                            if(!is_null($adsdistrictId) && $adsdistrictId != " "){
                                                                $record['adsdistrict_id']=$adsdistrictId;
                                                            }
                                                        }
                                                    }

                                                    if(isset($value['almntk'])){
                                                        if(!is_null($value['almntk']) && $value['almntk'] != " " && $value['almntk'] != ""){
                                                            $adsregionId = CaseModel::constantId('adsregion_id',$value['almntk'],true,$adsdistrictId);
                                                            if(!is_null($adsregionId) && $adsregionId != " "){
                                                                $record['adsregion_id']=$adsregionId;
                                                            }
                                                        }
                                                    }

                                                    if(isset($value['alhy'])){
                                                        if(!is_null($value['alhy']) && $value['alhy'] != " " && $value['alhy'] != ""){
                                                            $adsneighborhoodId = CaseModel::constantId('adsneighborhood_id',$value['alhy'],true,$adsregionId);
                                                            if(!is_null($adsneighborhoodId) && $adsneighborhoodId != " "){
                                                                $record['adsneighborhood_id']=$adsneighborhoodId;
                                                            }
                                                        }
                                                    }

                                                    if(isset($value['almrbaa'])){
                                                        if(!is_null($value['almrbaa']) && $value['almrbaa'] != " " && $value['almrbaa'] != ""){
                                                            $adssquareId = CaseModel::constantId('adssquare_id',$value['almrbaa'],true,$adsneighborhoodId);
                                                            if(!is_null($adssquareId) && $adssquareId != " "){
                                                                $record['adssquare_id']=$adssquareId;
                                                            }
                                                        }
                                                    }

                                                    if(isset($value['almsjd'])){
                                                        if(!is_null($value['almsjd']) && $value['almsjd'] != " " && $value['almsjd'] != ""){
                                                            $adsmosquesId = CaseModel::constantId('adsmosques_id',$value['almsjd'],true,$adssquareId);
                                                            if(!is_null($adsmosquesId) && $adsmosquesId != " "){
                                                                $record['adsmosques_id']=$adsmosquesId;
                                                            }
                                                        }
                                                    }

                                                    if(isset($value['rkm_joal_asasy'])){
                                                        if(!is_null($value['rkm_joal_asasy']) && $value['rkm_joal_asasy'] != " " && $value['rkm_joal_asasy'] != ""){
                                                            if(!is_null($record['primary_mobile']) || $record['primary_mobile'] != " "){
                                                                $record['primary_mobile']=$value['rkm_joal_asasy'];
                                                            }
                                                        }
                                                    }

                                                    if(isset($value['rkm_hatf'])){
                                                        if(!is_null($value['rkm_hatf']) && $value['rkm_hatf'] != " " && $value['rkm_hatf'] != ""){
                                                            if(!is_null($record['phone']) || $record['phone'] != " "){
                                                                $record['phone']=$value['rkm_hatf'];
                                                            }
                                                        }
                                                    }

                                                    $person =Person::savePerson($record);
                                                    $ifExist = (object)$person;


                                                    if($save == 'save' || $save == "save" ){
                                                        $inputs_ = [ 'organization_id'=>$user->organization_id,
                                                            'person_id' =>$ifExist->id ,'category_id' => $category_id];
                                                        if($type == 1){
                                                            $case = SponsorshipsCases::where($inputs_)->first();

                                                            if(is_null($case)){
                                                                $inputs_['user_id']=$user->id;
                                                                $inputs_['created_at']=date('Y-m-d H:i:s');
                                                                $inputs_['status']=0;
                                                                $case=  \Common\Model\SponsorshipsCases::create($inputs_);
                                                                \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id, 'reason'=>trans('common::application.case was created'),
                                                                    'status'=>0, 'date'=>date("Y-m-d")]);
                                                            }
                                                        }
                                                        else{
                                                            $passed = true;
                                                            $mainConnector=OrgLocations::getConnected($organization_id);
                                                            if(!is_null($ifExist->adsdistrict_id)){
                                                                if(!in_array($ifExist->adsdistrict_id,$mainConnector)){
                                                                    $passed = false;
                                                                }else{
                                                                    if(!is_null($ifExist->adsregion_id)){
                                                                        if(!in_array($ifExist->adsregion_id,$mainConnector)){
                                                                            $passed = false;
                                                                        }
                                                                        else{

                                                                            if(!is_null($ifExist->adsneighborhood_id)){
                                                                                if(!in_array($ifExist->adsneighborhood_id,$mainConnector)){
                                                                                    $passed = false;
                                                                                }else{

                                                                                    if(!is_null($ifExist->adssquare_id)){
                                                                                        if(!in_array($ifExist->adssquare_id,$mainConnector)){
                                                                                            $passed = false;
                                                                                        }else{
                                                                                            if(!is_null($ifExist->adsmosques_id)){
                                                                                                if(!in_array($ifExist->adsmosques_id,$mainConnector)){
                                                                                                    $passed = false;
                                                                                                }
                                                                                            }else{
                                                                                                $passed = false;
                                                                                            }
                                                                                        }
                                                                                    }else{
                                                                                        $passed = false;
                                                                                    }
                                                                                }
                                                                            }else{
                                                                                $passed = false;
                                                                            }
                                                                        }
                                                                    }else{
                                                                        $passed = false;
                                                                    }
                                                                }
                                                            }else{
                                                                $passed = false;
                                                            }
                                                            if($passed == true){
                                                                $case = AidsCases::where($inputs_)->first();
                                                                if(is_null($case)){
                                                                    $inputs_['user_id']=$user->id;
                                                                    $inputs_['created_at']=date('Y-m-d H:i:s');
                                                                    $inputs_['status']=0;
                                                                    $case=  \Common\Model\AidsCases::create($inputs_);
                                                                    \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id, 'reason'=>trans('common::application.case was created'),
                                                                        'status'=>0, 'date'=>date("Y-m-d")]);
                                                                }
                                                            }
                                                        }
                                                    }
                                                    CaseModel::DisablePersonInNoConnected($ifExist,$user->organization_id,$user->id);
                                                    $processed[]=$card;
                                                    $processed_person[]=$ifExist->id;
                                                    $success++;


                                                }else{
                                                    $invalid_cards[] = ['card'=> $value['rkm_alhoy'] , 'reason' =>  trans('aid::application.invalid_card')];
                                                    $invalid_cards_++;
                                                }
                                            }



                                        }else{
                                            $invalid_cards[] = ['card'=> $value['rkm_alhoy'] , 'reason' =>  trans('aid::application.invalid_card')];
                                            $invalid_cards_++;
                                        }
                                    }
                                }

                                if($total == sizeof($invalid_cards)){
                                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.All names entered are not registered in the Civil Registry')  ]);
                                }
                                else{

                                    foreach ($processed_person as $it){
                                        $person =Person::where( 'id',$it)->first();
                                        $name = $person->first_name .' ' .$person->second_name.' ' .$person->third_name.' ' .$person->last_name .' ';

                                        if(!is_null($person->death_date)){
                                            $invalid_cards[] = ['card'=> $value['rkm_alhoy'] , 'reason' =>  trans('common::application.deceased')];
                                            $death_person ++;
                                        }else{

                                            $relaysObj =  Relays::where(['person_id'=> $it,'category_id' => $category_id])->first();
                                            if(!$relaysObj){

                                                Relays::create(['person_id'=> $it,
                                                    'category_id' => $category_id,
                                                    'user_id'=> $user->id,
                                                    'organization_id' => $user->organization_id]);

                                                $levelId = null;
                                                if(is_null($person->adsmosques_id)) {
                                                    if(is_null($person->adssquare_id)) {
                                                        if(is_null($person->adsneighborhood_id)) {
                                                            if(is_null($person->adsregion_id)) {
                                                                if(is_null($person->adsdistrict_id)) {
                                                                    if(is_null($person->adscountry_id)) {
                                                                        $levelId = $person->adscountry_id ;
                                                                    }
                                                                }else{
                                                                    $levelId = $person->adsdistrict_id ;
                                                                }
                                                            }else{
                                                                $levelId = $person->adsregion_id ;
                                                            }
                                                        }else{
                                                            $levelId = $person->adsneighborhood_id ;
                                                        }
                                                    }else{
                                                        $levelId = $person->adssquare_id ;
                                                    }
                                                }
                                                else{
                                                    $levelId = $person->adsmosques_id ;
                                                }

                                                $orgs =  OrgLocations::LocationOrgs($levelId,$person->id,$category_id);
                                                $users =User::userHasPermission('reports.relays.confirm',$orgs);
                                                if($request->type == 2) {
                                                    $url = "#/relays/aids";
                                                }else{
                                                    $url = "#/relays/sponsorships";
                                                }

                                                \Log\Model\Notifications::saveNotification([
                                                    'user_id_to' =>$users,
                                                    'url' =>$url,
                                                    'message' => trans('common::application.he migrated').': ' . ' ' . $name . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name
                                                ]);

                                                \Log\Model\Log::saveNewLog('RELAY_CREATED',trans('common::application.he migrated').': ' . ' ' . $name . ' , '.trans('common::application.As a case').' : ' . $categoryObj->name);
                                                $done_++;
                                            }else{
                                                $success_++;
                                            }
                                        }
                                    }
                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function($excel) use($invalid_cards,$processed) {
                                        $excel->setTitle(trans('common::application.relays'));
                                        $excel->setDescription(trans('common::application.relays'));
                                        if(sizeof($processed) > 0){
                                            $excel->sheet(trans('common::application.relays'), function($sheet) use($processed) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;
                                                foreach($processed as $v){
                                                    $sheet ->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }

                                            });
                                        }
                                        if(sizeof($invalid_cards) > 0){
                                            $excel->sheet(trans('common::application.invalid_cards'), function($sheet) use($invalid_cards) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));
                                                $sheet->setCellValue('B1',trans('common::application.reason'));

                                                $z= 2;
                                                foreach($invalid_cards as $k=> $v){
                                                    $sheet ->setCellValue('A'.$z,$v->card);
                                                    $sheet ->setCellValue('B'.$z,$v->reason);
                                                    $z++;
                                                }

                                            });
                                        }
                                    })->store('xlsx', storage_path('tmp/'));

                                    if( $total == ( $success +$duplicated)){
                                        return response()->json(['status' => 'success','msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.All numbers successfully migrated') ,'download_token' => $token]);
                                    }
                                    else{
                                        return response()->json([ 'status' => 'success',
                                            'msg'=>trans('common::application.the results file is being downloaded,').
                                                trans('common::application.Some numbers were successfully migrated, as the total number') .' :  '. $total . ' ,  '.
                                                trans('common::application.Number of migrated names') .' :  ' .$done_ . ' ,  ' .
                                                trans('common::application.Number of pre-migrated names') .' :  ' .$success_ . ' ,  ' .
                                                trans('common::application.Number of names not registered in the Civil Registry') .' :  ' .$invalid_cards_ . ' ,  ' .
                                                trans('common::count of death person') .' :  '.$death_person ,
                                            trans('common::application.Number of repeated names') .' :  '.$duplicated ,
                                            'download_token' => $token ]);
                                    }

                                }

                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.The number of records in the file must not exceed 2000 records') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);

                    }
                }

                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);

            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }

    // card check template
    public function sponsorshipsTemplate()
    {
        return response()->json(['download_token' => 'card-check-template']);
    }

    // card check template
    public function aidsTemplate()
    {
        return response()->json(['download_token' => 'aid-card-check-template']);
    }

    // card confirm Template
    public function ConfirmTemplate()
    {
        return response()->json(['download_token' => 'aid-card-relay-template']);
    }

}
