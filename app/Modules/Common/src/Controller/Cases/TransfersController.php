<?php

namespace Common\Controller\Cases;

use App\Http\Controllers\Controller;
use Auth\Model\User;
use Common\Model\AidsCases;
use Common\Model\BlockIDCard;
use Common\Model\CaseModel;
use Common\Model\Person;
use Common\Model\SponsorshipsCases;
use Illuminate\Http\Request;
use Common\Model\Transfers\Transfers;
use Common\Model\Transfers\ConfirmTransfers;
use Excel;
use Organization\Model\OrgLocations;

class TransfersController  extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // filter un confirm transfers according conditions
    public function filter(Request $request)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
//        $this->authorize('manage', Transfers::class);
//        $user = \Auth::user();
        $items=Transfers::filter($request->page, $request->all());
        if($request->get('action')=='xlsx'){

            $token = md5(uniqid());
            \Maatwebsite\Excel\Facades\Excel::create('export_' . $token, function($excel) use($items) {
                $excel->sheet(trans('common::application.transfers'),function($sheet) use($items) {
                    $sheet->setRightToLeft(true);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);
                    $sheet->setOrientation('landscape');
                    $sheet->setRightToLeft(true);
                    $sheet->setStyle([
                        'font' => [
                            'name' => 'Simplified Arabic',
                            'size' => 11,
                            'bold' => true
                        ]
                    ]);
                    $sheet->setWidth(['A'=>5 ,'B'=> 15,'C'=> 20,'D'=> 20,'E'=> 20,'F'=> 20, 'G'=> 20,'H'=> 30,'I'=> 20,'J'=> 20]);

                    $sheet->row(1, function($row) { $row->setBackground('#CCCCCC'); });
                    $sheet->row(1, function($row) {  $row->setValignment('center');});

                    $sheet->getStyle("A1:J1")->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      => [ 'name' =>'Simplified Arabic','size'=> 11, 'bold'=>  true ]
                    ]);

                    $sheet ->setCellValue('A1',trans('common::application.#'));
                    $sheet ->setCellValue('B1',trans('common::application.id_card_number'));
                    $sheet ->setCellValue('C1',trans('common::application.first_name'));
                    $sheet ->setCellValue('D1',trans('common::application.second_name'));
                    $sheet ->setCellValue('E1',trans('common::application.third_name'));
                    $sheet ->setCellValue('F1',trans('common::application.last_name'));
                    $sheet ->setCellValue('G1',trans('common::application.category_name_'));
                    $sheet ->setCellValue('H1',trans('common::application.primary_mobile'));
                    $sheet ->setCellValue('I1',trans('common::application.transfer_date'));
                    $sheet ->setCellValue('J1',trans('common::application.transfer_user_name'));

                    $z= 2;
                    foreach($items as $k=>$v ){
                        $sheet ->setCellValue('A'.$z,$k+1);
                        $sheet ->setCellValue('B'.$z,is_null($v->id_card_number) ? '-' : $v->id_card_number);
                        $sheet ->setCellValue('C'.$z,is_null($v->first_name) ? '-' : $v->first_name);
                        $sheet ->setCellValue('D'.$z,is_null($v->second_name) ? '-' : $v->second_name);
                        $sheet ->setCellValue('E'.$z,is_null($v->third_name) ? '-' : $v->third_name);
                        $sheet ->setCellValue('F'.$z,is_null($v->last_name) ? '-' : $v->last_name);
                        $sheet ->setCellValue('G'.$z,is_null($v->category_name) ? '-' : $v->category_name);
                        $sheet ->setCellValue('H'.$z,is_null($v->primary_mobile) ? '-' : $v->primary_mobile);
                        $sheet ->setCellValue('I'.$z,is_null($v->created_at) ? '-' : $v->created_at);
                        $sheet ->setCellValue('J'.$z,is_null($v->user_name) ? '-' : $v->user_name);
                        $z++;
                    }

                });
            })->store('xlsx', storage_path('tmp/'));
            return response()->json([
                'download_token' => $token,
            ]);
        }


        if($request->get('action') =='paginate') {
            $items->map(function ($row) {
                $row->reject_permission  = false;
                if($row->adsmosques_id == $row->prev_adsmosques_id)
                    $row->reject_permission  = true;

                return $row;
            });
        }

        return response()->json($items);

    }

    // transfer persons using id
    public function accept(Request $request,$id)
    {
//        $this->authorize('RelayCitizenRepository', \Common\Model\AidsCases::class);

        $user = \Auth::user();

        $status = $request->status;
        $transfer= Transfers::with(['category'])->where('id',$id)->first();
        $person = Person::where('id',$transfer->person_id)->first();
        $category =$transfer->category;

        $full_name = $person->first_name .' ' .$person->second_name .' ' .$person->third_name .' ' .$person->last_name  .' ';

        if($status == 2){
            $logs = ConfirmTransfers::where(['transfer_id'=> $id, 'organization_id' => $user->organization_id])->first();
            if(is_null($logs)){
                ConfirmTransfers::create(['status'=> 2, 'transfer_id'=> $id, 'reason'=> $request->reason,
                    'user_id'=> $user->id, 'organization_id' => $user->organization_id]);
                \Log\Model\Notifications::saveNotification([
                    'user_id_to' =>[$transfer->user_id],
                    'url' =>"#/confirm-transfers",
                    'message' => trans('common::application.reject').': ' . ' ' . $full_name . ' , '.
                        trans('common::application.As a case').' : ' . $category->name
                ]);

                \Log\Model\Log::saveNewLog('TRANSFER_REJECT', trans('common::application.reject').': ' .
                    ' ' . $full_name . ' , '.trans('common::application.As a case').' : ' . $category->name);

                return response()->json(['status' => 'success' , 'msg' =>trans('common::application.The person successfully approved')]);
            }
            return response()->json(['status' => 'error' , 'msg' =>trans('common::application.Someone already saved by an employee of your org')]);

        }
        else{

            if(!is_null($person->death_date)){
                return response()->json(['status' => 'failed' , 'msg' =>trans('common::application.this_card_for_dead_person')]);
            }


            if(BlockIDCard::isBlock($person->id_card_number,$transfer->category_id,1)){
                return response()->json(['status' => 'error' ,
                    'msg' => trans('common::application.The owner of the ID is forbidden to become as case'). ':' . $category->name]);
            }

            $passed = true ;
            CaseModel::DisablePersonInNoConnected($person,$user->organization_id,$user->id);
            $mainConnector=OrgLocations::getConnected($user->organization_id);
            if(!is_null($transfer->adsdistrict_id)){
                if(!in_array($transfer->adsdistrict_id,$mainConnector)){
                    $passed = false;
                }else{
                    if(!is_null($transfer->adsregion_id)){
                        if(!in_array($transfer->adsregion_id,$mainConnector)){
                            $passed = false;
                        }else{

                            if(!is_null($transfer->adsneighborhood_id)){
                                if(!in_array($transfer->adsneighborhood_id,$mainConnector)){
                                    $passed = false;
                                }else{

                                    if(!is_null($transfer->adssquare_id)){
                                        if(!in_array($transfer->adssquare_id,$mainConnector)){
                                            $passed = false;
                                        }else{
                                            if(!is_null($transfer->adsmosques_id)){
                                                if(!in_array($transfer->adsmosques_id,$mainConnector)){
                                                    $passed = false;
                                                }
                                            }else{
                                                $passed = false;
                                            }
                                        }
                                    }else{
                                        $passed = false;
                                    }
                                }
                            }else{
                                $passed = false;
                            }
                        }
                    }else{
                        $passed = false;
                    }
                }
            }else{
                $passed = false;
            }

            if($passed == true){
                $logs = ConfirmTransfers::where(['transfer_id'=> $id, 'organization_id' => $user->organization_id])->first();
                if(is_null($logs)){
                    ConfirmTransfers::create(['status'=> 1, 'transfer_id'=> $id, 'user_id'=> $user->id, 'organization_id' => $user->organization_id]);
                    \Log\Model\Notifications::saveNotification([
                        'user_id_to' =>[$transfer->user_id],
                        'url' =>"#/confirm-transfers",
                        'message' => trans('common::application.confirm').': ' . ' ' . $full_name . ' , '.trans('common::application.As a case').' : ' . $category->name
                    ]);
                    \Log\Model\Log::saveNewLog('TRANSFER_CREATED', trans('common::application.confirm').': ' .
                        ' ' . $full_name . ' , '.trans('common::application.As a case').' : ' . $category->name);

                    return response()->json(['status' => 'success' , 'msg' =>trans('common::application.The person successfully approved')]);
                }
                return response()->json(['status' => 'error' , 'msg' =>trans('common::application.Someone already saved by an employee of your org')]);

            }
            else{

                $message = trans('common::application.reject').': ' . ' ' . $full_name . ' , '.
                    trans('common::application.As a case').' : ' . $category->name .
                    ' (' .trans('aid::application.out_of_org_regions') .' ) ';
                \Log\Model\Log::saveNewLog('TRANSFER_REJECT',$message );
                return response()->json(["status" => 'failed_disabled', "msg"  =>$message ]);
            }

        }

    }

    // transfer persons using persons using id
    public function acceptGroup(Request $request)
    {
        $this->authorize('confirm', Transfers::class);

        $items = $request->persons;
        $transfers = sizeof($items);
        $user = \Auth::user();

        if($transfers > 0){
            $success = 0;
            $not_person = 0;
            $restricted = 0;
            $invalid = 0;
            $restricted_ = [];

            foreach ($items as $item){
                $transfer=Transfers::with(['category'])->where('id',$item)->first();
                if($transfer){
                    $person = Person::where('id',$transfer->person_id)->first();
                    $full_name = $person->first_name .' ' .$person->second_name .' ' .$person->third_name .' ' .$person->last_name  .' ';
                    $category = $transfer->category;

                    if($person){
                        if(BlockIDCard::isBlock($person->id_card_number,$transfer->category_id,1)){
                            $restricted_[]=['id_card_number' => $person->id_card_number ,
                                'name' =>$full_name ,
                                'reason' => trans('common::application.The owner of the ID is forbidden to become as case'). ':' . $category->name];
                            $restricted++;
                        }
                        else{

                            $passed = true;
                            CaseModel::DisablePersonInNoConnected($person,$user->organization_id,$user->id);
                            $mainConnector=OrgLocations::getConnected($user->organization_id);
                            if(!is_null($transfer->adsdistrict_id)){
                                if(!in_array($transfer->adsdistrict_id,$mainConnector)){
                                    $passed = false;
                                }else{
                                    if(!is_null($transfer->adsregion_id)){
                                        if(!in_array($transfer->adsregion_id,$mainConnector)){
                                            $passed = false;
                                        }else{

                                            if(!is_null($transfer->adsneighborhood_id)){
                                                if(!in_array($transfer->adsneighborhood_id,$mainConnector)){
                                                    $passed = false;
                                                }else{

                                                    if(!is_null($transfer->adssquare_id)){
                                                        if(!in_array($transfer->adssquare_id,$mainConnector)){
                                                            $passed = false;
                                                        }else{
                                                            if(!is_null($transfer->adsmosques_id)){
                                                                if(!in_array($transfer->adsmosques_id,$mainConnector)){
                                                                    $passed = false;
                                                                }
                                                            }else{
                                                                $passed = false;
                                                            }
                                                        }
                                                    }else{
                                                        $passed = false;
                                                    }
                                                }
                                            }else{
                                                $passed = false;
                                            }
                                        }
                                    }else{
                                        $passed = false;
                                    }
                                }
                            }else{
                                $passed = false;
                            }

                            if($passed == true){
                                $success++;
                                $success_[]=['id_card_number' => $person->id_card_number , 'name' =>$full_name ];
                                $logs = ConfirmTransfers::where(['transfer_id'=> $item, 'organization_id' => $user->organization_id])->first();
                                if(is_null($logs)){
                                    ConfirmTransfers::create(['status'=> 1, 'transfer_id'=> $item, 'user_id'=> $user->id, 'organization_id' => $user->organization_id]);
                                    \Log\Model\Notifications::saveNotification([
                                        'user_id_to' =>[$transfer->user_id],
                                        'url' =>"#/confirm-transfers",
                                        'message' => trans('common::application.confirm').': ' . ' ' . $full_name . ' , '.trans('common::application.As a case').' : ' . $category->name
                                    ]);
                                    \Log\Model\Log::saveNewLog('TRANSFER_CREATED',trans('common::application.confirm').': ' . ' ' . $full_name . ' , '.trans('common::application.As a case').' : ' . $category->name);
                                }
                            }
                            else{
                                $restricted_[]=['id_card_number' => $person->id_card_number ,
                                    'name' =>$full_name ,
                                    'reason' => trans('common::application.The data of the deported person was saved').': ' . ' ' . $full_name];
                                $restricted++;
                            }
                        }

                    }else{
                        $not_person++;
                    }
                }else{
                    $invalid++;
                }
            }

            if( $success == $transfers ){
                $response = ['status' => 'success','msg'=> trans('common::application.All cases migrated for your organization') ];
            }
            else if( $restricted == $transfers ){
                $response = ['status' => 'failed','msg'=>
                    trans('common::application.Not all cases have been migrated, number of cases')  .' :  '. $transfers . ' ,  '.
                    trans('common::application.number of people not registered') .' :  ' .$not_person . ' ,  ' .
                    trans('common::application.Number of people who have not been deported either to ban or because they are outside your organization') .' :  '.$restricted
                ];
            }
            else{

                if($success != 0){
                    $response =['status' => 'success','msg'=>
                        trans('common::application.Not all cases have been migrated, number of cases').' :  '. $transfers . ' ,  '.
                        trans('common::application.Number of migrated cases').' :  ' .$success . ' ,  ' .
                        trans('common::application.number of people not registered') .' :  ' .$not_person . ' ,  ' .
                        trans('common::application.Number of people who have not been deported either to ban or because they are outside your organization').' :  '.$restricted
                    ];
                }
                else{
                    $response = ['status' => 'failed','msg'=>
                        trans('common::application.Not all cases have been migrated, number of cases').' :  '. $transfers . ' ,  '.
                        trans('common::application.Number of migrated cases').' :  ' .$success . ' ,  ' .
                        trans('common::application.number of people not registered') .' :  ' .$not_person . ' ,  ' .
                        trans('common::application.Number of people who have not been deported either to ban or because they are outside your organization').' :  '.$restricted
                    ];
                }

            }

            if($restricted > 0 ){
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($restricted_) {
                    $excel->setTitle(trans('common::application.family_sheet'));
                    $excel->setDescription(trans('common::application.restricted_cards'));
                    $excel->sheet(trans('common::application.restricted_cards'), function($sheet) use($restricted_) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:C1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->setCellValue('A1',trans('common::application.id_card_number'));
                        $sheet->setCellValue('B1',trans('common::application.name'));
                        $sheet->setCellValue('C1',trans('common::application.reason'));

                        $z= 2;
                        foreach($restricted_ as $k=>$v){
                            $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                            $sheet ->setCellValue('B'.$z,$v['name']);
                            $sheet ->setCellValue('C'.$z,$v['reason']);
                            $z++;
                        }

                    });
                })->store('xlsx', storage_path('tmp/'));

                $response['msg'] = $response['msg'] .'<br>' .trans('common::application.The list of people who have not been deported will be downloaded') ;
                $response['download_token'] = $token;
            }

            return response()->json($response);
        }

        return response()->json(['status' => 'error' , 'msg' =>trans('common::application.You have not specified any confirmed update status for migration')]);

    }

    ////////////////////////////////////////////////////////////////////////////

    // filter accepted transfers according conditions
    public function acceptedFilter(Request $request)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $this->authorize('manageConfirm', Transfers::class);
//        $user = \Auth::user();
        $items=ConfirmTransfers::filter($request->page, $request->all());
        if($request->get('action') =='paginate') {
            $items->map(function ($row) {
                $row->reject_permission  = false;
                if($row->adsmosques_id == $row->prev_adsmosques_id)
                    $row->reject_permission  = true;

                return $row;
            });
        }

        return response()->json($items);
    }

    // transfer persons using id
    public function confirm(Request $request,$id)
    {
//        $this->authorize('RelayCitizenRepository', \Common\Model\AidsCases::class);

        $user = \Auth::user();

        $status = $request->status;
        $cTransfer=ConfirmTransfers::where('id',$id)->first();

        $transfer=Transfers::with(['category'])->where('id',$cTransfer->transfer_id)->first();
        $person = Person::where('id',$transfer->person_id)->first();
        $category =$transfer->category;
        $full_name = $person->first_name .' ' .$person->second_name .' ' .$person->third_name .' ' .$person->last_name  .' ';

        if($status == 2){
            $cTransfer->confirm_user_id = $user->id;
            $cTransfer->confirm_date = date('Y-m-d');
            $cTransfer->confirm = 1;
            $cTransfer->status = 2;
            $cTransfer->confirm_reason = $request->reason;
            $cTransfer->save();
            \Log\Model\Notifications::saveNotification([
                'user_id_to' =>[$cTransfer->user_id],
                'url' =>"#/transfers",
                'message' => trans('common::application.reject').': ' . ' ' . $full_name . ' , '.
                    trans('common::application.As a case').' : ' . $category->name
            ]);

            \Log\Model\Log::saveNewLog('TRANSFER_REJECT', trans('common::application.reject').': ' .
                ' ' . $full_name . ' , '.trans('common::application.As a case').' : ' . $category->name);

            return response()->json(['status' => 'success' , 'msg' =>trans('common::application.The person successfully approved')]);
        }
        else{

            if(!is_null($person->death_date)){
                return response()->json(['status' => 'failed' , 'msg' =>trans('common::application.this_card_for_dead_person')]);
            }

            if(BlockIDCard::isBlock($person->id_card_number,$transfer->category_id,1)){
                return response()->json(['status' => 'error' ,
                    'msg' => trans('common::application.The owner of the ID is forbidden to become as case'). ':' . $category->name]);
            }

            $person->adscountry_id = $transfer->adscountry_id ;
            $person->adsdistrict_id = $transfer->adsdistrict_id ;
            $person->adsmosques_id = $transfer->adsmosques_id ;
            $person->adsneighborhood_id = $transfer->adsneighborhood_id ;
            $person->adsregion_id = $transfer->adsregion_id ;
            $person->adssquare_id = $transfer->adssquare_id ;
            $person->adsmosques_id = $transfer->adsmosques_id ;

            $passed = true ;
            CaseModel::DisablePersonInNoConnected($person,$cTransfer->organization_id,$user->id);
            $mainConnector=OrgLocations::getConnected($cTransfer->organization_id);
            if(!is_null($transfer->adsdistrict_id)){
                if(!in_array($transfer->adsdistrict_id,$mainConnector)){
                    $passed = false;
                }else{
                    if(!is_null($transfer->adsregion_id)){
                        if(!in_array($transfer->adsregion_id,$mainConnector)){
                            $passed = false;
                        }else{

                            if(!is_null($transfer->adsneighborhood_id)){
                                if(!in_array($transfer->adsneighborhood_id,$mainConnector)){
                                    $passed = false;
                                }else{

                                    if(!is_null($transfer->adssquare_id)){
                                        if(!in_array($transfer->adssquare_id,$mainConnector)){
                                            $passed = false;
                                        }else{
                                            if(!is_null($transfer->adsmosques_id)){
                                                if(!in_array($transfer->adsmosques_id,$mainConnector)){
                                                    $passed = false;
                                                }
                                            }else{
                                                $passed = false;
                                            }
                                        }
                                    }else{
                                        $passed = false;
                                    }
                                }
                            }else{
                                $passed = false;
                            }
                        }
                    }else{
                        $passed = false;
                    }
                }
            }else{
                $passed = false;
            }

            if($passed == true){
                $person->save();
                $save= false;
                $cTransfer->confirm_user_id = $user->id;
                $cTransfer->confirm_date = date('Y-m-d');
                $cTransfer->confirm = 1;
                $cTransfer->status = 3;
                $cTransfer->save();

                $inputs_ = ['person_id' => $transfer->person_id, 'category_id' => $transfer->category_id,
                    'organization_id' => $cTransfer->organization_id];

                $case = AidsCases::where($inputs_)->first();
                if(is_null($case)){
                    $inputs_['user_id']=$user->id;
                    $inputs_['created_at']=date('Y-m-d H:i:s');
                    $inputs_['status']=0;
                    $case=  \Common\Model\AidsCases::create($inputs_);
                    \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id, 'reason'=>trans('common::application.case was created'), 'status'=>0, 'date'=>date("Y-m-d")]);
                    \Common\Model\CaseModel::where(['id' => $case->id ])->update(['rank' =>\Common\Model\AbstractCases::updateRank($case->id)]);
                    \Log\Model\Log::saveNewLog('TRANSFER_CREATED',trans('common::application.confirm').': ' . ' ' . $full_name . ' , '.trans('common::application.As a case').' : ' . $category->name);
                    $save= true;
                }

                if($save){
                    \Log\Model\Notifications::saveNotification([
                        'user_id_to' =>[$cTransfer->user_id],
                        'url' =>"#/transfers",
                        'message' => trans('common::application.confirm').': ' . ' ' . $full_name . ' , '.trans('common::application.As a case').' : ' . $category->name
                    ]);
                    \Log\Model\Log::saveNewLog('TRANSFER_CREATED', trans('common::application.confirm').': ' .
                        ' ' . $full_name . ' , '.trans('common::application.As a case').' : ' . $category->name);

                }
                return response()->json(['status' => 'success' , 'msg' =>trans('common::application.The person successfully approved')]);
            }
            else{
                $cTransfer->confirm_user_id = $user->id;
                $cTransfer->confirm_date = date('Y-m-d');
                $cTransfer->confirm = 1;
                $cTransfer->status = 2;
                $cTransfer->confirm_reason = $request->reason;
                $cTransfer->save();
                \Log\Model\Notifications::saveNotification([
                    'user_id_to' =>[$cTransfer->user_id],
                    'url' =>"#/transfers",
                    'message' => trans('common::application.reject').': ' . ' ' . $full_name . ' , '.
                        trans('common::application.As a case').' : ' . $category->name .
                                 ' (' .trans('aid::application.out_of_org_regions') .' ) '
                ]);

                \Log\Model\Log::saveNewLog('TRANSFER_REJECT', trans('common::application.reject').': ' . ' ' . $full_name . ' , '.
                    trans('common::application.As a case').' : ' . $category->name .
                    ' (' .trans('aid::application.out_of_org_regions') .' ) ');

                return response()->json(['status' => 'error' , 'msg' =>trans('common::application.reject').': ' . ' ' . $full_name . ' , '.
                    trans('common::application.As a case').' : ' . $category->name .
                    ' (' .trans('aid::application.out_of_org_regions') .' ) ']);

            }

        }

    }

    // confirm transfer persons using id
    public function confirmGroup(Request $request)
    {
        $this->authorize('confirm', Transfers::class);

        $items = $request->persons;
        $transfers = sizeof($items);
        $user = \Auth::user();

        if($transfers > 0){
            $success = 0;
            $not_person = 0;
            $restricted = 0;
            $invalid = 0;
            $restricted_ = [];

            foreach ($items as $item){
                $cTransfer=ConfirmTransfers::where('id',$item)->first();
                $cTransfer->confirm_user_id = $user->id;
                $cTransfer->confirm_date = date('Y-m-d');
                $cTransfer->status = 3;
                $cTransfer->confirm = 1;

                $transfer=Transfers::with(['category'])->where('id',$cTransfer->transfer_id)->first();
                $person = Person::where('id',$transfer->person_id)->first();
                $category =$transfer->category;
                $full_name = $person->first_name .' ' .$person->second_name .' ' .$person->third_name .' ' .$person->last_name  .' ';

                if($person){
                    if(BlockIDCard::isBlock($person->id_card_number,$transfer->category_id,1)){
                        $restricted_[]=['id_card_number' => $person->id_card_number ,
                            'name' =>$full_name ,
                            'reason' => trans('common::application.The owner of the ID is forbidden to become as case'). ':' . $category->name];
                        $restricted++;
                    }
                    else{

                        $person->adscountry_id = $transfer->adscountry_id ;
                        $person->adsdistrict_id = $transfer->adsdistrict_id ;
                        $person->adsmosques_id = $transfer->adsmosques_id ;
                        $person->adsneighborhood_id = $transfer->adsneighborhood_id ;
                        $person->adsregion_id = $transfer->adsregion_id ;
                        $person->adssquare_id = $transfer->adssquare_id ;

                        $passed = true;
                        CaseModel::DisablePersonInNoConnected($person,$cTransfer->organization_id,$user->id);
                        $mainConnector=OrgLocations::getConnected($cTransfer->organization_id);
                        if(!is_null($transfer->adsdistrict_id)){
                            if(!in_array($transfer->adsdistrict_id,$mainConnector)){
                                $passed = false;
                            }else{
                                if(!is_null($transfer->adsregion_id)){
                                    if(!in_array($transfer->adsregion_id,$mainConnector)){
                                        $passed = false;
                                    }else{

                                        if(!is_null($transfer->adsneighborhood_id)){
                                            if(!in_array($transfer->adsneighborhood_id,$mainConnector)){
                                                $passed = false;
                                            }else{

                                                if(!is_null($transfer->adssquare_id)){
                                                    if(!in_array($transfer->adssquare_id,$mainConnector)){
                                                        $passed = false;
                                                    }else{
                                                        if(!is_null($transfer->adsmosques_id)){
                                                            if(!in_array($transfer->adsmosques_id,$mainConnector)){
                                                                $passed = false;
                                                            }
                                                        }else{
                                                            $passed = false;
                                                        }
                                                    }
                                                }else{
                                                    $passed = false;
                                                }
                                            }
                                        }else{
                                            $passed = false;
                                        }
                                    }
                                }else{
                                    $passed = false;
                                }
                            }
                        }else{
                            $passed = false;
                        }

                        if($passed == true){
                            $person->save();
                            $cTransfer->save();
                            $success++;
                            $success_[]=['id_card_number' => $person->id_card_number , 'name' =>$full_name ];
                            $inputs_ = ['person_id' => $transfer->person_id,
                                'category_id' => $transfer->category_id,
                                'organization_id' => $cTransfer->organization_id,
                            ];

                            $save= false;
                            $case = null;
                            $case = AidsCases::where($inputs_)->first();
                            if(is_null($case)){
                                $inputs_['user_id']=$user->id;
                                $inputs_['created_at']=date('Y-m-d H:i:s');
                                $inputs_['status']=0;
                                $case=  \Common\Model\AidsCases::create($inputs_);
                                \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id, 'reason'=>trans('common::application.case was created'), 'status'=>0, 'date'=>date("Y-m-d")]);
                                \Common\Model\CaseModel::where(['id' => $case->id ])->update(['rank' =>\Common\Model\AbstractCases::updateRank($case->id)]);
                                \Log\Model\Log::saveNewLog('TRANSFER_CREATED',trans('common::application.confirm').': ' . ' ' . $full_name . ' , '.trans('common::application.As a case').' : ' . $category->name);
                                $save= true;
                            }

                            if($save){
                                \Log\Model\Notifications::saveNotification([
                                    'user_id_to' =>[$cTransfer->user_id],
                                    'url' =>"#/transfers",
                                    'message' => trans('common::application.confirm').': ' . ' ' . $full_name . ' , '.trans('common::application.As a case').' : ' . $category->name
                                ]);
                                \Log\Model\Log::saveNewLog('TRANSFER_CREATED',trans('common::application.confirm').': ' . ' ' . $full_name . ' , '.trans('common::application.As a case').' : ' . $category->name);
                            }

                        }
                        else{
                            $restricted_[]=['id_card_number' => $person->id_card_number ,
                                'name' =>$full_name ,
                                'reason' => trans('common::application.The data of the deported person was saved').': ' . ' ' . $full_name];
                            $restricted++;
                        }
                    }
                }else{
                    $not_person++;
                }
            }

            if( $success == $transfers ){
                $response = ['status' => 'success','msg'=> trans('common::application.All cases migrated for your organization') ];
            }
            else if( $restricted == $transfers ){
                $response = ['status' => 'failed','msg'=>
                    trans('common::application.Not all cases have been migrated, number of cases')  .' :  '. $transfers . ' ,  '.
                    trans('common::application.number of people not registered') .' :  ' .$not_person . ' ,  ' .
                    trans('common::application.Number of people who have not been deported either to ban or because they are outside your organization') .' :  '.$restricted
                ];
            }
            else{

                if($success != 0){
                    $response =['status' => 'success','msg'=>
                        trans('common::application.Not all cases have been migrated, number of cases').' :  '. $transfers . ' ,  '.
                        trans('common::application.Number of migrated cases').' :  ' .$success . ' ,  ' .
                        trans('common::application.number of people not registered') .' :  ' .$not_person . ' ,  ' .
                        trans('common::application.Number of people who have not been deported either to ban or because they are outside your organization').' :  '.$restricted
                    ];
                }
                else{
                    $response = ['status' => 'failed','msg'=>
                        trans('common::application.Not all cases have been migrated, number of cases').' :  '. $transfers . ' ,  '.
                        trans('common::application.Number of migrated cases').' :  ' .$success . ' ,  ' .
                        trans('common::application.number of people not registered') .' :  ' .$not_person . ' ,  ' .
                        trans('common::application.Number of people who have not been deported either to ban or because they are outside your organization').' :  '.$restricted
                    ];
                }

            }

            if($restricted > 0 ){
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($restricted_) {
                    $excel->setTitle(trans('common::application.family_sheet'));
                    $excel->setDescription(trans('common::application.restricted_cards'));
                    $excel->sheet(trans('common::application.restricted_cards'), function($sheet) use($restricted_) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:C1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->setCellValue('A1',trans('common::application.id_card_number'));
                        $sheet->setCellValue('B1',trans('common::application.name'));
                        $sheet->setCellValue('C1',trans('common::application.reason'));

                        $z= 2;
                        foreach($restricted_ as $k=>$v){
                            $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                            $sheet ->setCellValue('B'.$z,$v['name']);
                            $sheet ->setCellValue('C'.$z,$v['reason']);
                            $z++;
                        }

                    });
                })->store('xlsx', storage_path('tmp/'));

                $response['msg'] = $response['msg'] .'<br>' .trans('common::application.The list of people who have not been deported will be downloaded') ;
                $response['download_token'] = $token;
            }

            return response()->json($response);
        }

        return response()->json(['status' => 'error' , 'msg' =>trans('common::application.You have not specified any confirmed update status for migration')]);

    }

}
