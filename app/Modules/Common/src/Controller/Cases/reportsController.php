<?php

namespace Common\Controller\Cases;

use Common\Model\CloneGovernmentPersons;
use Common\Model\Person;
use Common\Model\PersonDocument;
use App\Http\Controllers\Controller;
use Common\Model\PersonModels\PersonAid;
use Document\Model\File;
use Illuminate\Http\Request;
use Common\Model\CaseModel;
use Common\Model\GovServices;
use Log\Model\CardLog;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpWord\IOFactory;
use Elibyy\TCPDF\TCPDF;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PDF;
use Common\Model\PersonModels\PersonI18n;
use Auth\Model\User;
use App\Http\Helpers;
class reportsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /************ card check template ************/
    public function template()
    {
        return response()->json(['download_token' => 'card-check-template']);
    }
    /*********************** citizenRepository ***********************/
    // filter citizen according ( card and tab name )
    public function tabContent(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $row = GovServices::tabByCard($request->id_card_number,$request->action);
        if($row['status'] == false){
            return response()->json(['status'=>false,'msg'=>$row['message']]);
        }

        $record=$row['row'];
        return response()->json([ 'status'=>true, 'data'=>$record ]);

    }

    // filter citizen according ( card or name )
    public function citizenSearchFilter(Request $request)
    {
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $this->authorize('citizenRepository', \Common\Model\AidsCases::class);

        $user = \Auth::user();
        $records = [];
        $using = $request->using;

        if($using == 1){
            $card = $request->id_card_number;
            $row = GovServices::byCard($card,false);
            CardLog::saveCardLog($user->id,$card);
            if($row['status'] != false){
                CloneGovernmentPersons::saveNew($row['row']);
                $records[] = $row['row'];
            }else{
                return response()->json(['status'=>false,'msg'=>$row['message']]);
            }

        }
        else{
            $row = GovServices::byName($request->all());
            if($row['status'] != false){
                $rows= $row['row'];
                foreach ($rows as $key=>$value){
                    $row = GovServices::byCard($value->IDNO,false);
                    CardLog::saveCardLog($user->id,$value->IDNO);
                    if($row['status'] != false){
                        CloneGovernmentPersons::saveNew($row['row']);
                        $records[] = $row['row'];
                    }
                }
            }else{
                return response()->json(['status'=>false,'msg'=>$row['message']]);
            }

        }

        if(sizeof($records) > 0 ){
            return response()->json(['status'=>true,'items'=>$records]);
        }

        return response()->json(['status'=>false]);

    }

    // get citizen using card
    public function citizenRepository(Request $request)
    {

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $this->authorize('citizenRepository', \Common\Model\AidsCases::class);

        $card = $request->id_card_number;

        if($request->action =='xlsx'){
            $person = null;
            $row = GovServices::allByCard($card);
            if($row['status'] == false){
                return response()->json(['status'=>false,'msg'=>$row['message']]);
            }

            $record=$row['row'];
            CloneGovernmentPersons::saveNew($record);

            if (sizeof($record->all_relatives) == 0 ){
                $repeated[] = (Object)[
                    'IDNO_RELATIVE' => '-' ,
                    'RELATIVE_DESC' => '-' ,
                    'FNAME_ARB' => '-' ,
                    'SNAME_ARB' => '-' ,
                    'TNAME_ARB' => '-' ,
                    'LNAME_ARB' => '-' ,
                    'SEX' => '-' ,
                    'SOCIAL_STATUS' => '-' ,
                    'BIRTH_DT' => '-' ,
                    'DETH_DT' => '-' ,
                    'BIRTH_PMAIN' => '-' ,
                    'BIRTH_PSUB' => '-' ,
                    'PREV_LNAME_ARB' => '-' ,
                    'MOTHER_ARB' => '-' ,
                ];
            }else{
                $repeated = $record->all_relatives;
            }

            $token = md5(uniqid());
            \Excel::create('export_' . $token, function($excel) use($card,$record,$repeated) {
                $excel->setTitle(trans('common::application.family_sheet'));
                $excel->setDescription(trans('common::application.family_sheet'));

                $excel->sheet(trans('common::application.family_sheet'), function($sheet) use($card,$record,$repeated) {


                    $full_name=  GovServices::refDashIfNull($record,'FULLNAME');
                    $en_name=  GovServices::refDashIfNull($record,'ENG_NAME');
                    $mother_name=  GovServices::refDashIfNull($record,'MOTHER_ARB');
                    $prev_family_name=  GovServices::refDashIfNull($record,'PREV_LNAME_ARB');

                    $gender=  GovServices::refDashIfNull($record,'SEX');
                    $marital_status=  GovServices::refDashIfNull($record,'SOCIAL_STATUS');
                    $birthday=  GovServices::refDashIfNull($record,'BIRTH_DT');
                    $death_date=  GovServices::refDashIfNull($record,'DETH_DT');
                    $birth_place_main=  GovServices::refDashIfNull($record,'BIRTH_PMAIN');
                    $birth_place_sub=  GovServices::refDashIfNull($record,'BIRTH_PMAIN');

                    $address = $record->CI_REGION .' - ' .$record->CI_CITY  . ' - ' . $record->STREET_ARB;

                    $CTZN_STATUS=  GovServices::refDashIfNull($record,'CTZN_STATUS');
                    $CTZN_TRANS_DT=  GovServices::refDashIfNull($record,'CTZN_TRANS_DT');
                    $VISIT_PURPOSE_DESC=  GovServices::refDashIfNull($record,'VISIT_PURPOSE_DESC');

                    $family_cnt=  GovServices::refDashIfNull($record,'family_cnt');
                    $female_live=  GovServices::refDashIfNull($record,'female_live');
                    $male_live=  GovServices::refDashIfNull($record,'male_live');
                    $spouses=  GovServices::refDashIfNull($record,'spouses');

                    $primary_mobile=  GovServices::refDashIfNull($record,'MOBILE');
                    $phone=  GovServices::refDashIfNull($record,'TEL');

                    $GOV_NAME=  GovServices::refDashIfNull($record,'GOV_NAME');
                    $CITY_NAME=  GovServices::refDashIfNull($record,'CITY_NAME');
                    $PART_NAME=  GovServices::refDashIfNull($record,'PART_NAME');
                    $ADDRESS_DET=  GovServices::refDashIfNull($record,'ADDRESS_DET');

// passport info
                    $PASS_TYPE = $PASSPORT_NO =  $ISSUED_ON = '-';
                    if(!is_null($record->passport)){
                        $PASS_TYPE = (is_null($record->passport->PASS_TYPE) ||$record->passport->PASS_TYPE == ' ' ) ? '-' :$record->passport->PASS_TYPE ;
                        $PASSPORT_NO = (is_null($record->passport->PASSPORT_NO) ||$record->passport->PASSPORT_NO == ' ' ) ? '-' :$record->passport->PASSPORT_NO ;
                        $ISSUED_ON = (is_null($record->passport->ISSUED_ON) ||$record->passport->ISSUED_ON == ' ' ) ? '-' :$record->passport->ISSUED_ON ;
                    }

// health_insurance_info info
                    $CARD_NO = $EXP_DATE =  $INS_STATUS_DESC = $INS_TYPE_DESC = $WORK_SITE_DESC = '-';
                    if(!is_null($record->health_insurance_data)){
                        $CARD_NO = (is_null($record->health_insurance_data->CARD_NO) ||$record->health_insurance_data->CARD_NO == ' ' ) ? '-' : $record->health_insurance_data->CARD_NO ;
                        $EXP_DATE = (is_null($record->health_insurance_data->EXP_DATE) ||$record->health_insurance_data->EXP_DATE == ' ' ) ? '-' : $record->health_insurance_data->EXP_DATE ;
                        $INS_STATUS_DESC = (is_null($record->health_insurance_data->INS_STATUS_DESC) ||$record->health_insurance_data->INS_STATUS_DESC == ' ' ) ? '-' : $record->health_insurance_data->INS_STATUS_DESC ;
                        $INS_TYPE_DESC = (is_null($record->health_insurance_data->INS_TYPE_DESC) ||$record->health_insurance_data->INS_TYPE_DESC == ' ' ) ? '-' : $record->health_insurance_data->INS_TYPE_DESC ;
                        $WORK_SITE_DESC = (is_null($record->health_insurance_data->WORK_SITE_DESC) ||$record->health_insurance_data->WORK_SITE_DESC == ' ' ) ? '-' : $record->health_insurance_data->WORK_SITE_DESC ;
                    }

// social affairs
                    $social_affairs_status = trans('common::application.un_beneficiary');
                    $aid_class = $aid_type = $aid_amount = $aid_source = $aid_periodic = $aid_st_ben_date = $aid_end_ben_date = '-';
                    if(!is_null($record->social_affairs)){
                        $social_affairs_status = GovServices::refDashIfNull($record->social_affairs,'social_affairs_status');
                        $aid_class = GovServices::refDashIfNull($record->social_affairs,'AID_CLASS');
                        $aid_type = GovServices::refDashIfNull($record->social_affairs,'AID_TYPE');
                        $aid_amount = GovServices::refDashIfNull($record->social_affairs,'AID_AMOUNT');
                        $aid_source = GovServices::refDashIfNull($record->social_affairs,'AID_SOURCE');
                        $aid_periodic = GovServices::refDashIfNull($record->social_affairs,'AID_PERIODIC');
                        $aid_st_ben_date = GovServices::refDashIfNull($record->social_affairs,'ST_BENEFIT_DATE');
                        $aid_end_ben_date = GovServices::refDashIfNull($record->social_affairs,'END_BENEFIT_DATE');
                    }

// aids_committee
                    /*
                    $paterfamilias_mobile = $near_mosque = $home_address = $telephone = $affected_by_wars = $current_career =
                    $father_death_reason = $mother_death_reason = $building_type = $home_type =  $furniture_type =
                    $home_status = $home_description = $total_family_income = $health_status = $wife_mobile = '-';

                    if(!is_null($record->aids_committee)){
                        $unsetKeys = ['home_address','near_mosque','paterfamilias_mobile','telephone','current_career','father_death_reason',
                            'mother_death_reason','building_type','home_type','furniture_type','home_status','home_description',
                            'total_family_income','health_status','wife_mobile'];

                        foreach ($unsetKeys as $unsetKey){
                            $$unsetKey =GovServices::refDashIfNull($record->aids_committee,$unsetKey);
                        }

                        $affected_by_wars = ($record->aids_committee->affected_by_wars == 1) ? 'yes' :'no' ;
                    }
*/
                    $sheet->setRightToLeft(true);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);

                    $sheet->getStyle("A1:V1")->applyFromArray(['font' => ['bold' => true]]);

                    $start_from =1;
                    $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                    $sheet->setCellValue('B' . $start_from ,trans('common::application.id_card_number'));
                    $sheet->setCellValue('C' . $start_from ,trans('common::application.full_name'));
                    $sheet->setCellValue('D' . $start_from ,trans('common::application.en_name'));
                    $sheet->setCellValue('E' . $start_from ,trans('common::application.gender'));
                    $sheet->setCellValue('F' . $start_from ,trans('common::application.marital_status'));
                    $sheet->setCellValue('G' . $start_from ,trans('common::application.birthday'));
                    $sheet->setCellValue('H' . $start_from ,trans('common::application.death_date'));
                    $sheet->setCellValue('I' . $start_from ,trans('common::application.birth_place_main'));
                    $sheet->setCellValue('J' . $start_from ,trans('common::application.birth_place_sub'));
                    $sheet->setCellValue('K' . $start_from ,trans('common::application.mother_name'));
                    $sheet->setCellValue('L' . $start_from ,trans('common::application.prev_family_name'));
                    $sheet->setCellValue('M' . $start_from ,trans('common::application.address'));
                    $sheet->setCellValue('N' . $start_from ,trans('common::application.sso info') .'( ' .trans('common::application.address') .'-' .trans('common::application.GOV_NAME'). ' )');
                    $sheet->setCellValue('O' . $start_from ,trans('common::application.sso info') .'( ' .trans('common::application.address') .'-' .trans('common::application.CITY_NAME'). ' )');
                    $sheet->setCellValue('P' . $start_from ,trans('common::application.sso info') .'( ' .trans('common::application.address') .'-' .trans('common::application.PART_NAME'). ' )');
                    $sheet->setCellValue('Q' . $start_from ,trans('common::application.sso info') .'( ' .trans('common::application.address') .'-' .trans('common::application.ADDRESS_DET'). ' )');
                    $sheet->setCellValue('R' . $start_from ,trans('common::application.mobile'));
                    $sheet->setCellValue('S' . $start_from ,trans('common::application.phone'));
                    $sheet->setCellValue('T' . $start_from ,trans('common::application.family_cnt'));
                    $sheet->setCellValue('U' . $start_from ,trans('common::application.spouses'));
                    $sheet->setCellValue('V' . $start_from ,trans('common::application.male_live'));
                    $sheet->setCellValue('W' . $start_from ,trans('common::application.female_live'));

                    $sheet->setCellValue('x' . $start_from ,trans('common::application.CTZN_STATUS'));
                    $sheet->setCellValue('y' . $start_from ,trans('common::application.CTZN_TRANS_DT'));
                    $sheet->setCellValue('z' . $start_from ,trans('common::application.VISIT_PURPOSE_DESC'));

                    $sheet->setCellValue('AA' . $start_from ,trans('common::application.PASS_TYPE'));
                    $sheet->setCellValue('AB' . $start_from ,trans('common::application.PASSPORT_NO'));
                    $sheet->setCellValue('AC' . $start_from ,trans('common::application.ISSUED_ON'));

                    $sheet->setCellValue('AD' . $start_from ,trans('common::application.CARD_NO'));
                    $sheet->setCellValue('AE' . $start_from ,trans('common::application.EXP_DATE'));
                    $sheet->setCellValue('AF' . $start_from ,trans('common::application.INS_STATUS_DESC'));
                    $sheet->setCellValue('AG' . $start_from ,trans('common::application.INS_TYPE_DESC'));
                    $sheet->setCellValue('AH' . $start_from ,trans('common::application.WORK_SITE_DESC'));

                    $sheet->setCellValue('AI' . $start_from ,trans('common::application.aid_status'));
                    $sheet->setCellValue('AJ' . $start_from ,trans('common::application.AID_CLASS'));
                    $sheet->setCellValue('AK' . $start_from ,trans('common::application.AID_TYPE'));
                    $sheet->setCellValue('AL' . $start_from ,trans('common::application.AID_AMOUNT'));
                    $sheet->setCellValue('AM' . $start_from ,trans('common::application.AID_SOURCE'));
                    $sheet->setCellValue('AN' . $start_from ,trans('common::application.AID_PERIODIC'));
                    $sheet->setCellValue('AO' . $start_from ,trans('common::application.ST_BENEFIT_DATE'));
                    $sheet->setCellValue('AP' . $start_from ,trans('common::application.END_BENEFIT_DATE'));

//                    $sheet->setCellValue('AQ' . $start_from ,trans('common::application.address'));
//                    $sheet->setCellValue('AR' . $start_from ,trans('common::application.near_mosque'));
//                    $sheet->setCellValue('AS' . $start_from ,trans('common::application.mobile'));
//                    $sheet->setCellValue('AT' . $start_from ,trans('common::application.phone'));
//                    $sheet->setCellValue('AU' . $start_from ,trans('common::application.wife_mobile'));
//                    $sheet->setCellValue('AV' . $start_from ,trans('common::application.current_career'));
//                    $sheet->setCellValue('AW' . $start_from ,trans('common::application.father_death_reason'));
//                    $sheet->setCellValue('AX' . $start_from ,trans('common::application.mother_death_reason'));
//                    $sheet->setCellValue('AY' . $start_from ,trans('common::application.building_type'));
//                    $sheet->setCellValue('AZ' . $start_from ,trans('common::application.home_type'));
//                    $sheet->setCellValue('BA' . $start_from ,trans('common::application.furniture_type'));
//                    $sheet->setCellValue('BB' . $start_from ,trans('common::application.home_status'));
//                    $sheet->setCellValue('BV' . $start_from ,trans('common::application.home_description'));
//                    $sheet->setCellValue('BD' . $start_from ,trans('common::application.total_family_income'));
//                    $sheet->setCellValue('BE' . $start_from ,trans('common::application.health_status'));

                    $sheet->setCellValue('AQ' . $start_from ,trans('common::application.id_card_number').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AR' . $start_from ,trans('common::application.kinship_name') .' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AS' . $start_from ,trans('common::application.full_name').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AT' . $start_from ,trans('common::application.gender').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AU' . $start_from ,trans('common::application.marital_status').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AV' . $start_from ,trans('common::application.birthday').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AW' . $start_from ,trans('common::application.death_date').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AX' . $start_from ,trans('common::application.prev_family_name').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AY' . $start_from ,trans('common::application.mother_name') .' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AZ' . $start_from ,trans('common::application.mobile') .' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('BA' . $start_from ,trans('common::application.phone') .' ('.trans('common::application.related') .' )');

                    $z= 2;
                    foreach($repeated as $k=>$v )
                    {

                        $sheet->setCellValue('A'.$z,$k+1);
                        $sheet->setCellValue('B' . $z ,$card);
                        $sheet->setCellValue('C' . $z ,$full_name);
                        $sheet->setCellValue('D' . $z ,$en_name);
                        $sheet->setCellValue('E' . $z ,$gender);
                        $sheet->setCellValue('F' . $z ,$marital_status);
                        $sheet->setCellValue('G' . $z ,$birthday);
                        $sheet->setCellValue('H' . $z ,$death_date);
                        $sheet->setCellValue('I' . $z ,$birth_place_main);
                        $sheet->setCellValue('J' . $z ,$birth_place_sub);
                        $sheet->setCellValue('K' . $z ,$mother_name);
                        $sheet->setCellValue('L' . $z ,$prev_family_name);
                        $sheet->setCellValue('M' . $z ,$address);
                        $sheet->setCellValue('N' . $z ,$GOV_NAME);
                        $sheet->setCellValue('O' . $z ,$CITY_NAME);
                        $sheet->setCellValue('P' . $z ,$PART_NAME);
                        $sheet->setCellValue('Q' . $z ,$ADDRESS_DET);
                        $sheet->setCellValue('R' . $z ,$primary_mobile);
                        $sheet->setCellValue('S' . $z ,$phone);
                        $sheet->setCellValue('T' . $z ,$family_cnt);
                        $sheet->setCellValue('U' . $z ,$spouses);
                        $sheet->setCellValue('V' . $z ,$male_live);
                        $sheet->setCellValue('W' . $z ,$female_live);

                        $sheet->setCellValue('X' . $z ,$CTZN_STATUS);
                        $sheet->setCellValue('Y' . $z ,$CTZN_TRANS_DT);
                        $sheet->setCellValue('Z' . $z ,$VISIT_PURPOSE_DESC);

                        $sheet->setCellValue('AA' . $z ,$PASS_TYPE);
                        $sheet->setCellValue('AB' . $z ,$PASSPORT_NO);
                        $sheet->setCellValue('AC' . $z ,$ISSUED_ON);

                        $sheet->setCellValue('AD' . $z ,$CARD_NO);
                        $sheet->setCellValue('AE' . $z ,$EXP_DATE);
                        $sheet->setCellValue('AF' . $z ,$INS_STATUS_DESC);
                        $sheet->setCellValue('AG' . $z ,$INS_TYPE_DESC);
                        $sheet->setCellValue('AH' . $z ,$WORK_SITE_DESC);
                        $sheet->setCellValue('AI' . $z ,$social_affairs_status);
                        $sheet->setCellValue('AJ' . $z ,$aid_class);
                        $sheet->setCellValue('AK' . $z ,$aid_type);
                        $sheet->setCellValue('AL' . $z ,$aid_amount);
                        $sheet->setCellValue('AM' . $z ,$aid_source);
                        $sheet->setCellValue('AN' . $z ,$aid_periodic);
                        $sheet->setCellValue('AO' . $z ,$aid_st_ben_date);
                        $sheet->setCellValue('AP' . $z ,$aid_end_ben_date);
//                        $sheet->setCellValue('AQ' . $z ,$home_address);
//                        $sheet->setCellValue('AR' . $z ,$near_mosque);
//                        $sheet->setCellValue('AS' . $z ,$paterfamilias_mobile);
//                        $sheet->setCellValue('AT' . $z ,$telephone);
//                        $sheet->setCellValue('AU' . $z ,$wife_mobile);
//                        $sheet->setCellValue('AV' . $z ,$current_career);
//                        $sheet->setCellValue('AW' . $z ,$father_death_reason);
//                        $sheet->setCellValue('AX' . $z ,$mother_death_reason);
//                        $sheet->setCellValue('AY' . $z ,$building_type);
//                        $sheet->setCellValue('AZ' . $z ,$home_type);
//                        $sheet->setCellValue('BA' . $z ,$furniture_type);
//                        $sheet->setCellValue('BB' . $z ,$home_status);
//                        $sheet->setCellValue('BC' . $z ,$home_description);
//                        $sheet->setCellValue('BD' . $z ,$total_family_income);
//                        $sheet->setCellValue('BE' . $z ,$health_status);

                        $sheet->setCellValue('AQ' . $z ,GovServices::refDashIfNull($v,'IDNO_RELATIVE'));
                        $sheet->setCellValue('AR' . $z ,GovServices::refDashIfNull($v,'RELATIVE_DESC'));
                        $sheet->setCellValue('AS' . $z ,GovServices::refDashIfNull($v,'FULLNAME'));
                        $sheet->setCellValue('AT' . $z ,GovServices::refDashIfNull($v,'SEX'));
                        $sheet->setCellValue('AU' . $z ,GovServices::refDashIfNull($v,'SOCIAL_STATUS'));
                        $sheet->setCellValue('AV' . $z ,GovServices::refDashIfNull($v,'BIRTH_DT'));
                        $sheet->setCellValue('AW' . $z ,GovServices::refDashIfNull($v,'DETH_DT'));
                        $sheet->setCellValue('AX' . $z ,GovServices::refDashIfNull($v,'PREV_LNAME_ARB'));
                        $sheet->setCellValue('AY' . $z ,GovServices::refDashIfNull($v,'MOTHER_ARB') );
                        $sheet->setCellValue('AZ' . $z ,GovServices::refDashIfNull($v,'MOBILE') );
                        $sheet->setCellValue('BA' . $z ,GovServices::refDashIfNull($v,'TEL'));
                        $sheet->getRowDimension($z)->setRowHeight(-1);
                        $z++;
                    }

                });

                if(sizeof($record->marriage) > 0){
                    $excel->sheet(trans('common::application.marriage'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:E1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.CONTRACT_NO'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.CONTRACT_DT'));
                        if ($record->SEX_CD == 2){
                            $sheet->setCellValue('D' . $start_from ,trans('common::application.HUSBAND_SSN'));
                            $sheet->setCellValue('E' . $start_from ,trans('common::application.HUSBAND_NAME'));
                        }else{
                            $sheet->setCellValue('D' . $start_from ,trans('common::application.WIFE_SSN'));
                            $sheet->setCellValue('E' . $start_from ,trans('common::application.WIFE_NAME'));
                        }

                        $z= 2;
                        foreach($record->marriage as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'CONTRACT_NO'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'CONTRACT_DT'));

                            if ($record->SEX_CD == 2){
                                $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'HUSBAND_SSN'));
                                $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'HUSBAND_NAME'));
                            }else{
                                $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'WIFE_SSN'));
                                $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'WIFE_NAME'));
                            }

                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C','D','E'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if(sizeof($record->divorce) > 0){
                    $excel->sheet(trans('common::application.divorce'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:F1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.DIV_CERTIFIED_NO'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.CONTRACT_DT'));
                        $sheet->setCellValue('D' . $start_from ,trans('common::application.DIV_TYPE_NAME'));
                        if ($record->SEX_CD == 2){
                            $sheet->setCellValue('E' . $start_from ,trans('common::application.HUSBAND_SSN'));
                            $sheet->setCellValue('F' . $start_from ,trans('common::application.HUSBAND_NAME'));
                        }else{
                            $sheet->setCellValue('E' . $start_from ,trans('common::application.WIFE_SSN'));
                            $sheet->setCellValue('F' . $start_from ,trans('common::application.WIFE_NAME'));
                        }

                        $z= 2;
                        foreach($record->divorce as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'DIV_CERTIFIED_NO'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'CONTRACT_DT'));
                            $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'DIV_TYPE_NAME'));

                            if ($record->SEX_CD == 2){
                                $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'HUSBAND_SSN'));
                                $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'HUSBAND_NAME'));
                            }else{
                                $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'WIFE_SSN'));
                                $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'WIFE_NAME'));
                            }

                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C','D','E','F'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if(sizeof($record->vehicles) > 0){
                    $excel->sheet(trans('common::application.vehicles info'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:C1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.MODEL_YEAR'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.OWNER_TYPE_NAME'));

                        $z= 2;
                        foreach($record->vehicles as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MODEL_YEAR'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'OWNER_TYPE_NAME'));
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if(sizeof($record->employment) > 0){
                    $excel->sheet(trans('common::application.work info'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getStyle("A1:I1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.EMP_DOC_ID'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.MINISTRY_NAME'));
                        $sheet->setCellValue('D' . $start_from ,trans('common::application.JOB_START_DT'));
                        $sheet->setCellValue('E' . $start_from ,trans('common::application.DEGREE_NAME'));
                        $sheet->setCellValue('F' . $start_from ,trans('common::application.EMP_STATE_DESC'));
                        $sheet->setCellValue('G' . $start_from ,trans('common::application.EMP_WORK_STATUS'));
                        $sheet->setCellValue('H' . $start_from ,trans('common::application.JOB_DESC'));
                        $sheet->setCellValue('I' . $start_from ,trans('common::application.MIN_DATA_SOURCE'));

                        $z= 2;
                        foreach($record->employment as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'EMP_DOC_ID'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'MINISTRY_NAME'));
                            $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'JOB_START_DT'));
                            $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'DEGREE_NAME'));
                            $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'EMP_STATE_DESC'));
                            $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'EMP_WORK_STATUS'));
                            $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'JOB_DESC'));
                            $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($v,'MIN_DATA_SOURCE'));
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G' ,'H','I'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if(sizeof($record->properties) > 0){
                    $excel->sheet(trans('common::application.properties info'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:C1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.DOC_NO'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.REAL_OWNER_AREA'));

                        $z= 2;
                        foreach($record->properties as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'DOC_NO'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'REAL_OWNER_AREA'));
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if(sizeof($record->commercial_data) > 0){
                    $excel->sheet(trans('common::application.commercials info'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:M1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.COMP_NAME'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.REC_CODE'));
                        $sheet->setCellValue('D' . $start_from ,trans('common::application.PERSON_TYPE_DESC'));
                        $sheet->setCellValue('E' . $start_from ,trans('common::application.IS_VALID'));
                        $sheet->setCellValue('F' . $start_from ,trans('common::application.STATUS_ID_DESC'));
                        $sheet->setCellValue('G' . $start_from ,trans('common::application.COMP_TYPE_DESC'));
                        $sheet->setCellValue('H' . $start_from ,trans('common::application.REC_TYPE'));
                        $sheet->setCellValue('I' . $start_from ,trans('common::application.REGISTER_NO'));
                        $sheet->setCellValue('J' . $start_from ,trans('common::application.START_DATE'));
                        $sheet->setCellValue('K' . $start_from ,trans('common::application.WORK_CLASS_DESC'));
                        $sheet->setCellValue('L' . $start_from ,trans('common::application.BRAND_NAME'));
                        $sheet->setCellValue('M' . $start_from ,trans('common::application.CITY_DESC'));

                        $z= 2;
                        foreach($record->commercial_data as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'COMP_NAME'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'REC_CODE'));
                            $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'PERSON_TYPE_DESC'));
                            $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'IS_VALID_DESC'));
                            $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'STATUS_ID_DESC'));
                            $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'COMP_TYPE_DESC'));
                            $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'REC_TYPE_DESC'));
                            $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($v,'REGISTER_NO'));
                            $sheet->setCellValue('J'.$z,GovServices::refDashIfNull($v,'START_DATE'));
                            $sheet->setCellValue('K'.$z,GovServices::refDashIfNull($v,'WORK_CLASS_DESC'));
                            $sheet->setCellValue('L'.$z,GovServices::refDashIfNull($v,'BRAND_NAME'));
                            $sheet->setCellValue('M'.$z,GovServices::refDashIfNull($v,'CITY_DESC'));
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G' ,'H','I','J','K','L','M' ];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if(sizeof($record->health) > 0){
                    $excel->sheet(trans('common::application.health info'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:K1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.MR_CODE'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.MR_PATIENT_CD'));
                        $sheet->setCellValue('D' . $start_from ,trans('common::application.LOC_NAME_AR'));
                        $sheet->setCellValue('E' . $start_from ,trans('common::application.DOCTOR_NAME'));
                        $sheet->setCellValue('F' . $start_from ,trans('common::application.MR_CREATED_ON'));
                        $sheet->setCellValue('G' . $start_from ,trans('common::application.DREF_NAME_AR'));
                        $sheet->setCellValue('H' . $start_from ,trans('common::application.MR_DIAGNOSIS_AR'));
                        $sheet->setCellValue('I' . $start_from ,trans('common::application.MR_DIAGNOSIS_EN'));
                        $sheet->setCellValue('J' . $start_from ,trans('common::application.MR_COMPLAINT'));
                        $sheet->setCellValue('K' . $start_from ,trans('common::application.MR_EXAMINATION'));

                        $z= 2;
                        foreach($record->health as $k=>$v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MR_CODE'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'LOC_NAME_AR'));
                            $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'MR_PATIENT_CD'));
                            $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'DOCTOR_NAME'));
                            $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'MR_CREATED_ON'));
                            $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'DREF_NAME_AR'));
                            $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'MR_DIAGNOSIS_AR'));
                            $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($v,'MR_DIAGNOSIS_EN'));
                            $sheet->setCellValue('J'.$z,GovServices::refDashIfNull($v,'MR_COMPLAINT'));
                            $sheet->setCellValue('K'.$z,GovServices::refDashIfNull($v,'MR_EXAMINATION'));
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }

                        $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G' ,'H','I','J','K'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if(sizeof($record->travel) > 0){
                    $excel->sheet(trans('common::application.travel records'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:D1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.CTZN_TRANS_DT'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.CTZN_TRANS_TYPE'));
                        $sheet->setCellValue('D' . $start_from ,trans('common::application.CTZN_TRANS_BORDER'));

                        $z= 2;
                        foreach($record->travel as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'CTZN_TRANS_DT'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'CTZN_TRANS_TYPE'));
                            $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'CTZN_TRANS_BORDER'));
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C','D'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if(sizeof($record->reg48_license) > 0){
                    $excel->sheet(trans('common::application.reg48_license'), function($sheet) use($reg48_license) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:K1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.MAIN_IDO'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.WORKER_ID'));
                        $sheet->setCellValue('D' . $start_from ,trans('common::application.STATUS_NAME'));
                        $sheet->setCellValue('E' . $start_from ,trans('common::application.DATE_FROM'));
                        $sheet->setCellValue('F' . $start_from ,trans('common::application.DATE_TO'));
                        $sheet->setCellValue('G' . $start_from ,trans('common::application.LICENSE_NAME'));
                        $sheet->setCellValue('H' . $start_from ,trans('common::application.LIC_REC_STATUS_NAME'));
                        $sheet->setCellValue('I' . $start_from ,trans('common::application.LIC_REC_DATE'));

                        $z= 2;
                        foreach($reg48_license as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MAIN_IDO'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'WORKER_ID'));
                            $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'STATUS_NAME'));
                            $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'DATE_FROM'));
                            $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'DATE_TO'));
                            $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'LICENSE_NAME'));
                            $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'LIC_REC_STATUS_NAME'));
                            $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($v,'LIC_REC_DATE'));
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C','D','E'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if(sizeof($record->social_affairs_receipt) > 0){
                    $excel->sheet(trans('common::application.social affairs'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:G1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.SRV_INF_NAME'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.ORG_NM_MON'));
                        $sheet->setCellValue('D' . $start_from ,trans('common::application.SRV_TYPE_MAIN_NM'));
                        $sheet->setCellValue('E' . $start_from ,trans('common::application.CURRENCY'));
                        $sheet->setCellValue('F' . $start_from ,trans('common::application.RECP_AID_AMOUNT'));
                        $sheet->setCellValue('G' . $start_from ,trans('common::application.RECP_DELV_DT'));

                        $z= 2;
                        foreach($record->social_affairs_receipt as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'SRV_INF_NAME'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'ORG_NM_MON'));
                            $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'SRV_TYPE_MAIN_NM'));
                            $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'CURRENCY'));
                            $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'RECP_AID_AMOUNT'));
                            $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'RECP_DELV_DT'));
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if (CaseModel::hasPermission('reports.case.RelayCitizenRepository') && sizeof($record->cases_list) > 0) {
                    $excel->sheet(trans('common::application.cases_list'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:G1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->setCellValue('A1',trans('common::application.#'));
                        $sheet->setCellValue('B1',trans('common::application.case_category_type'));
                        $sheet->setCellValue('C1',trans('common::application.organization'));
                        $sheet->setCellValue('D1',trans('common::application.status'));
                        $sheet->setCellValue('E1',trans('common::application.visitor'));
                        $sheet->setCellValue('F1',trans('common::application.visited_at'));
                        $sheet->setCellValue('G1',trans('common::application.created_at'));

                        $z= 2;
                        foreach($record->cases_list as $k=>$v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,(is_null($v->category_name) ||$v->category_name == ' ' ) ? '-' :$v->category_name);
                            $sheet->setCellValue('C'.$z,(is_null($v->organization_name) ||$v->organization_name == ' ' ) ? '-' :$v->organization_name);
                            $sheet->setCellValue('D'.$z,(is_null($v->status) ||$v->status == ' ' ) ? '-' :$v->status);
                            $sheet->setCellValue('E'.$z,(is_null($v->visitor) ||$v->visitor == ' ' ) ? '-' :$v->visitor);
                            $sheet->setCellValue('F'.$z,(is_null($v->visited_at) ||$v->visited_at == ' ' ) ? '-' :$v->visited_at);
                            $sheet->setCellValue('G'.$z,(is_null($v->created_date) ||$v->created_date == ' ' ) ? '-' :$v->created_date);
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }

                        $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G' ];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }
                    });

                }

            })->store('xlsx', storage_path('tmp/'));

            return response()->json(['download_token' => $token , 'dd' =>$record ]);
        }
        elseif($request->action =='pdf') {
            $person = null;
            $row = GovServices::allByCard($card);
            if($row['status'] == false){
                return response()->json(['status'=>false,'msg'=>$row['message']]);
            }

            $record=$row['row'];
            CloneGovernmentPersons::saveNew($record);

            $full_name=  GovServices::refDashIfNull($record,'FULLNAME');
            $en_name=  GovServices::refDashIfNull($record,'ENG_NAME');
            $mother_name=  GovServices::refDashIfNull($record,'MOTHER_ARB');
            $prev_family_name=  GovServices::refDashIfNull($record,'PREV_LNAME_ARB');

            $gender=  GovServices::refDashIfNull($record,'SEX');
            $marital_status=  GovServices::refDashIfNull($record,'SOCIAL_STATUS');
            $birthday=  GovServices::refDashIfNull($record,'BIRTH_DT');
            $death_date=  GovServices::refDashIfNull($record,'DETH_DT');
            $birth_place_main=  GovServices::refDashIfNull($record,'BIRTH_PMAIN');
            $birth_place_sub=  GovServices::refDashIfNull($record,'BIRTH_PMAIN');

            $address = $record->CI_REGION .' - ' .$record->CI_CITY  . ' - ' . $record->STREET_ARB;

            $family_cnt=  GovServices::refDashIfNull($record,'family_cnt');
            $female_live=  GovServices::refDashIfNull($record,'female_live');
            $male_live=  GovServices::refDashIfNull($record,'male_live');
            $spouses=  GovServices::refDashIfNull($record,'spouses');

            $primary_mobile=  GovServices::refDashIfNull($record,'MOBILE');
            $phone=  GovServices::refDashIfNull($record,'TEL');

            $GOV_NAME=  GovServices::refDashIfNull($record,'GOV_NAME');
            $CITY_NAME=  GovServices::refDashIfNull($record,'CITY_NAME');
            $PART_NAME=  GovServices::refDashIfNull($record,'PART_NAME');
            $PART_NAME=  GovServices::refDashIfNull($record,'PART_NAME');
            $MAMRK_TYPE_NAME=  GovServices::refDashIfNull($record,'MAMRK_TYPE_NAME');
            $ADDRESS_DET=  GovServices::refDashIfNull($record,'ADDRESS_DET');
            $EMAIL=  GovServices::refDashIfNull($record,'EMAIL');

            $CTZN_STATUS=  GovServices::refDashIfNull($record,'CTZN_STATUS');
            $CTZN_TRANS_DT=  GovServices::refDashIfNull($record,'CTZN_TRANS_DT');
            $VISIT_PURPOSE_DESC=  GovServices::refDashIfNull($record,'VISIT_PURPOSE_DESC');

            $html='';
            PDF::SetTitle($full_name);
            PDF::SetFont('aealarabiya', '', 18);
            PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);
            PDF::SetMargins(10, 10, 10);
            PDF::SetAutoPageBreak(TRUE);
            $lg = Array();
            $lg['a_meta_charset'] = 'UTF-8';
            $lg['a_meta_dir'] = 'rtl';
            $lg['a_meta_language'] = 'fa';
            $lg['w_page'] = 'page';
            PDF::setLanguageArray($lg);
            PDF::setCellHeightRatio(1.5);
            $pageNo = 1;
            $totalPageCount = 1;

            if ((CaseModel::hasPermission('reports.case.MinistryOfTransportation') && sizeof($record->vehicles) > 0 ) ||
                (CaseModel::hasPermission('reports.case.LandAuthority') && sizeof($record->properties) > 0 )||
                (CaseModel::hasPermission('reports.case.MinistryOfJustice') && (sizeof($record->marriage) > 0 || sizeof($record->divorce) > 0) ) ||
                (sizeof($record->parent) > 0) ){
                $totalPageCount ++;
            }

            if (sizeof($record->cases_list) > 0){
//                $count = count($record->employment);
                $totalPageCount ++;
            }
            if (sizeof($record->childrens) > 0 || sizeof($record->wives) > 0){
                $totalPageCount ++;
            }

            if(sizeof($record->social_affairs_receipt) > 0) {
                $count = count($record->social_affairs_receipt);
                $totalPageCount += ceil($count / 25);
            }

            if(sizeof($record->health) > 0) {
                $count = count($record->health);
                $totalPageCount += ceil($count / 4);
            }

            if(sizeof($record->travel) > 0) {
                $count = count($record->travel);
                $totalPageCount += ceil($count / 50);
            }

            if(sizeof($record->reg48_license) > 0) {
                $count = count($record->reg48_license);
                $totalPageCount += ceil($count / 28);
            }

            if(sizeof($record->employment) > 0) {
                $count = count($record->employment);
                $totalPageCount += ceil($count / 25);
            }

            $date = date('Y-m-d');
            $data_ =new Request();

            $token = md5(uniqid());
            $img = base_path('storage/app/emptyUser1.png');
            $fileContents = file_get_contents($record->photo_url);
            $disk = "local";
            $filename = $token.'.png'; // The name you want to give to the downloaded image
            if ($fileContents !== false) {
                Storage::disk($disk)->put('documents/' . $filename, $fileContents);
                $img = storage_path('app/documents/').$token.'.png';
            }

            $html = '<!doctype html><html lang="ar">
                 <head>
                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                 <style>
                     .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                     table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                     * , body {font-weight:normal !important; font-size: 10px;}
                     .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                     .bolder-content{text-align: right !important; vertical-align: middle!important;}
                     .vertical-middle{ vertical-align: middle !important;}
                </style>
                </head>
                 <body style="font-weight: normal !important;">';

            $html.= '<h2 style="text-align:center;"> '.$full_name.'</h2>';
            $html .= '<table border="1" style="border: 1px solid black; box-sizing:border-box;">';

            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.full_name').' </b></td>
                       <td width="330" align="center" >'.$full_name.'</td>
                        <td width="98" align="center" rowspan="6" style="background-color: white" >'
                .'<img src="'.$img.'" alt="" height="140" />'.
                '</td>
                       
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.en_name').' </b></td>
                       <td width="330" align="center" >'.$en_name.'</td>
                    
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.id_card_number').' </b></td>
                       <td width="115" align="center" >'.$card.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.gender').' </b></td>
                       <td width="115" align="center" >'.$gender.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.birthday').' </b></td>
                       <td width="115" align="center" >'.$birthday.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.death_date').' </b></td>
                       <td width="115" align="center" >'.$death_date.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.marital_status').' </b></td>
                       <td width="115" align="center" >'.$marital_status.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.primary_mobile').' </b></td>
                       <td width="115" align="center" >'.$primary_mobile.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.prev_family_name').' </b></td>
                       <td width="115" align="center" >'.$prev_family_name.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.phone').' </b></td>
                       <td width="115" align="center" >'.$phone.'</td>
                   </tr>';

            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.birth_place_main').' </b></td>
                       <td width="115" align="center" >'.$birth_place_main.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.birth_place_sub').' </b></td>
                       <td width="213" align="center" >'.$birth_place_sub.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b>'.trans('common::application.mother_name').' </b></td>
                       <td width="88" align="center" >'.$mother_name.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="55" align="center"><b>'.trans('common::application.family_cnt').' </b></td>
                       <td width="40" align="center" >'.$family_cnt.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="55" align="center"><b>'.trans('common::application.spouses').' </b></td>
                       <td width="40" align="center" >'.$spouses.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="55" align="center"><b>'.trans('common::application.male_live').' </b></td>
                       <td width="40" align="center" >'.$male_live.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="55" align="center"><b>'.trans('common::application.female_live').' </b></td>
                       <td width="40" align="center" >'.$female_live.'</td>
                   </tr>';
            $html .='</table>';

            $html .= '<table border="1" style="border: 1px solid black; box-sizing:border-box;">';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.address').' </b></td>
                       <td width="428" align="center" >'.$address.'</td>
                   </tr>';

            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.CTZN_STATUS').' </b></td>
                       <td width="115" align="center" >'.$CTZN_STATUS.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.CTZN_TRANS_DT').' </b></td>
                       <td width="213" align="center" >'.$CTZN_TRANS_DT.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.VISIT_PURPOSE_DESC').' </b></td>
                       <td width="428" align="center" >'.$VISIT_PURPOSE_DESC.'</td>
                   </tr>';


            $html .='</table>';

            if (CaseModel::hasPermission('reports.case.MinistryOfCommunications')) {
                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="528" align="center"><b>  
                        '.trans('common::application.sso info') .'( ' .trans('common::application.address'). ' )'.'
                        </b></td>
                   </tr>';
                $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b>'.trans('common::application.GOV_NAME').' </b></td>
                       <td width="184" align="center">'.$GOV_NAME.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b>'.trans('common::application.CITY_NAME').' </b></td>
                       <td width="184" align="center">'.$CITY_NAME.'</td>
                   </tr>';

                $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b>'.trans('common::application.PART_NAME').' </b></td>
                       <td width="134" align="center">'.$PART_NAME.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b>'.trans('common::application.MAMRK_TYPE_NAME').' </b></td>
                       <td width="234" align="center">'.$MAMRK_TYPE_NAME.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b>'.trans('common::application.ADDRESS_DET').' </b></td>
                       <td width="134" align="center">'.$ADDRESS_DET.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b>'.trans('common::application.EMAIL').' </b></td>
                       <td width="234" align="center">'.$EMAIL.'</td>
                   </tr>';
                $html .='</table>';
            }

            // passport info
            $PASS_TYPE = $PASSPORT_NO =  $ISSUED_ON = '-';
            if(!is_null($record->passport)){
                $PASS_TYPE = (is_null($record->passport->PASS_TYPE) ||$record->passport->PASS_TYPE == ' ' ) ? '-' :$record->passport->PASS_TYPE ;
                $PASSPORT_NO = (is_null($record->passport->PASSPORT_NO) ||$record->passport->PASSPORT_NO == ' ' ) ? '-' :$record->passport->PASSPORT_NO ;
                $ISSUED_ON = (is_null($record->passport->ISSUED_ON) ||$record->passport->ISSUED_ON == ' ' ) ? '-' :$record->passport->ISSUED_ON ;
            }

            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="528" align="center"><b>  
                        '.trans('common::application.passport info').'
                        </b></td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.PASS_TYPE').'</b></td>
                       <td width="56" align="center">'.$PASS_TYPE.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.PASSPORT_NO').'</b></td>
                       <td width="56" align="center">'.$PASSPORT_NO.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.ISSUED_ON').'</b></td>
                       <td width="176" align="center">'.$ISSUED_ON.'</td>
                   </tr>';
            $html .='</table>';

            // health_insurance_info info
            if (CaseModel::hasPermission('reports.case.MinistryOfHealth')) {
                $CARD_NO = $EXP_DATE =  $INS_STATUS_DESC = $INS_TYPE_DESC = $WORK_SITE_DESC = '-';
                if(!is_null($record->health_insurance_data)){
                    $CARD_NO = (is_null($record->health_insurance_data->CARD_NO) ||$record->health_insurance_data->CARD_NO == ' ' ) ? '-' : $record->health_insurance_data->CARD_NO ;
                    $EXP_DATE = (is_null($record->health_insurance_data->EXP_DATE) ||$record->health_insurance_data->EXP_DATE == ' ' ) ? '-' : $record->health_insurance_data->EXP_DATE ;
                    $INS_STATUS_DESC = (is_null($record->health_insurance_data->INS_STATUS_DESC) ||$record->health_insurance_data->INS_STATUS_DESC == ' ' ) ? '-' : $record->health_insurance_data->INS_STATUS_DESC ;
                    $INS_TYPE_DESC = (is_null($record->health_insurance_data->INS_TYPE_DESC) ||$record->health_insurance_data->INS_TYPE_DESC == ' ' ) ? '-' : $record->health_insurance_data->INS_TYPE_DESC ;
                    $WORK_SITE_DESC = (is_null($record->health_insurance_data->WORK_SITE_DESC) ||$record->health_insurance_data->WORK_SITE_DESC == ' ' ) ? '-' : $record->health_insurance_data->WORK_SITE_DESC ;
                }

                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="528" align="center"><b>  
                        '.trans('common::application.health_insurance_info').'
                        </b></td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.CARD_NO').'</b></td>
                       <td width="96" align="center">'.$CARD_NO.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.EXP_DATE').'</b></td>
                       <td width="96" align="center">'.$EXP_DATE.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.INS_STATUS_DESC').'</b></td>
                       <td width="96" align="center">'.$INS_STATUS_DESC.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.INS_TYPE_DESC').'</b></td>
                       <td width="96" align="center">'.$INS_TYPE_DESC.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.WORK_SITE_DESC').'</b></td>
                       <td width="272" align="center">'.$WORK_SITE_DESC.'</td>
                   </tr>';
                $html .='</table>';
            }

            // social affairs
            if (CaseModel::hasPermission('reports.case.MinistryOfSocialDevelopment')){
                $social_affairs_status = trans('common::application.un_beneficiary');
                $aid_class = $aid_type = $aid_amount = $aid_source = $aid_periodic = $aid_st_ben_date = $aid_end_ben_date = '-';
                if(!is_null($record->social_affairs)){
                    $social_affairs_status = GovServices::refDashIfNull($record->social_affairs,'social_affairs_status');
                    $aid_class = GovServices::refDashIfNull($record->social_affairs,'AID_CLASS');
                    $aid_type = GovServices::refDashIfNull($record->social_affairs,'AID_TYPE');
                    $aid_amount = GovServices::refDashIfNull($record->social_affairs,'AID_AMOUNT');
                    $aid_source = GovServices::refDashIfNull($record->social_affairs,'AID_SOURCE');
                    $aid_periodic = GovServices::refDashIfNull($record->social_affairs,'AID_PERIODIC');
                    $aid_st_ben_date = GovServices::refDashIfNull($record->social_affairs,'ST_BENEFIT_DATE');
                    $aid_end_ben_date = GovServices::refDashIfNull($record->social_affairs,'END_BENEFIT_DATE');
                }

                $html .='<div style="height: 5px; color: white"> - </div>';

                $html .= '<table border="1" style="">';

                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="528" align="center"><b>  
                        '.trans('common::application.social affairs').'
                        </b></td>
                   </tr>';

                $html .= ' <tr> 
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_status').'</b></td>
                       <td width="96" align="center">'.$social_affairs_status.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_class').'</b></td>
                       <td width="96" align="center">'.$aid_class.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_type').'</b></td>
                       <td width="96" align="center">'.$aid_type.'</td>
                    </tr>';


                $html .= ' <tr> 
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_amount').'</b></td>
                       <td width="96" align="center">'.$aid_amount.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_source').'</b></td>
                       <td width="96" align="center">'.$aid_source.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_periodic').'</b></td>
                       <td width="96" align="center">'.$aid_periodic.'</td>
                    </tr>';


                $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_st_ben_date').'</b></td>
                       <td width="184" align="center">'.$aid_st_ben_date.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_end_ben_date').'</b></td>
                       <td width="184" align="center">'.$aid_end_ben_date.'</td> 
                   </tr>';
                $html .='</table>';
            }

            /*
            $paterfamilias_mobile = $near_mosque = $home_address = $telephone = $affected_by_wars = $current_career =
            $father_death_reason = $mother_death_reason = $building_type = $home_type =  $furniture_type =
            $home_status = $home_description = $total_family_income = $health_status = $wife_mobile = '-';

            if(!is_null($record->aids_committee)){
                $unsetKeys = ['home_address','near_mosque','paterfamilias_mobile','telephone','current_career','father_death_reason',
                    'mother_death_reason','building_type','home_type','furniture_type','home_status','home_description',
                    'total_family_income','health_status','wife_mobile'];

                foreach ($unsetKeys as $unsetKey){
                    $$unsetKey =GovServices::refDashIfNull($record->aids_committee,$unsetKey);
                }

                $affected_by_wars = ($record->aids_committee->affected_by_wars == 1) ? 'yes' :'no' ;
            }

            $html .='<div style="height: 5px; color: white"> - </div>';

            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="528" align="center"><b>  
                        '.trans('common::application.aids committee data').'
                        </b></td>
                   </tr>';

            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.primary_mobile').'</b></td>
                       <td width="96" align="center">'.$paterfamilias_mobile.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.phone').'</b></td>
                       <td width="96" align="center">'.$telephone.'</td> 
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.wife_mobile').'</b></td>
                       <td width="96" align="center">'.$wife_mobile .'</td>
                   </tr>';

            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.health_status').'</b></td>
                       <td width="96" align="center">'.$health_status.'</td> 
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.current_career').'</b></td>
                       <td width="96" align="center">'.$current_career.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.total_family_income').'</b></td>
                       <td width="96" align="center">'.$total_family_income.'</td> 
                     </tr>';

            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.address').'</b></td>
                       <td width="184" align="center">'.$home_address.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.near_mosque').'</b></td>
                       <td width="184" align="center">'.$near_mosque.'</td> 
                   </tr>';

            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.father_death_reason').'</b></td>
                       <td width="184" align="center">'.$father_death_reason.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.mother_death_reason').'</b></td>
                       <td width="184" align="center">'.$mother_death_reason.'</td> 
                   </tr>';

            $html .= ' <tr>                       
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.affected_by_wars').'</b></td>
                       <td width="96" align="center">'.$affected_by_wars.'</td> 
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.building_type').'</b></td>
                       <td width="96" align="center">'.$building_type.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.home_type').'</b></td>
                       <td width="96" align="center">'.$home_type.'</td> 
                   </tr>';
            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.furniture_type').'</b></td>
                       <td width="184" align="center">'.$furniture_type.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.home_status').'</b></td>
                       <td width="184" align="center">'.$home_status.'</td> 
                   </tr>';

            $html .= ' <tr>
                           <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.home_description').'</b></td>
                           <td width="458" align="center">'.$home_description.'</td>
                       </tr>';

            $html .='</table>';

            */
            $html .='</body></html>';

            PDF::AddPage('P','A4');
            PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
            });
            $pageNo++;
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

            if ((CaseModel::hasPermission('reports.case.MinistryOfTransportation') && sizeof($record->vehicles) > 0 ) ||
                (CaseModel::hasPermission('reports.case.LandAuthority') && sizeof($record->properties) > 0 )||
                (CaseModel::hasPermission('reports.case.MinistryOfJustice') && (sizeof($record->marriage) > 0 || sizeof($record->divorce) > 0) ) ||
                (sizeof($record->parent) > 0) ){

// properties / vehicles / commercials / cases list
                $html = '<!doctype html><html lang="ar">
                 <head>
                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                 <style>
                     .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                     table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                     * , body {font-weight:normal !important; font-size: 10px;}
                     .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                     .bolder-content{text-align: right !important; vertical-align: middle!important;}
                     .vertical-middle{ vertical-align: middle !important;}
                </style>
                </head>
                 <body style="font-weight: normal !important;">';
                $html .='<div style="height: 5px; color: white"> - </div>';

                if ((CaseModel::hasPermission('reports.case.MinistryOfTransportation') && sizeof($record->vehicles) > 0 ) ||
                    (CaseModel::hasPermission('reports.case.LandAuthority') && sizeof($record->properties) > 0 )||
                    (CaseModel::hasPermission('reports.case.MinistryOfJustice') && (sizeof($record->marriage) > 0 || sizeof($record->divorce) > 0) ) ||
                    (sizeof($record->parent) > 0) ){


                }
                // vehicles
                if (CaseModel::hasPermission('reports.case.MinistryOfTransportation')) {
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.vehicles info').'
                        </b></td>
                   </tr>';
                    if (sizeof($record->vehicles) > 0) {
                        $html .= '<tr> 
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="55" align="center"><b> ' . trans('common::application.#') . '</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="240" align="center"><b> ' . trans('common::application.MODEL_YEAR') . '</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="240" align="center"><b> ' . trans('common::application.OWNER_TYPE_NAME') . '</b></td>
                              </tr>';
                    }
                    $z = 1;

                    foreach($record->vehicles as $ke=>$r_) {
                        $MODEL_YEAR = $OWNER_TYPE_NAME  = '-';
                        $unsetKeys = ['MODEL_YEAR','OWNER_TYPE_NAME'];
                        foreach ($unsetKeys as $unsetKey){
                            $$unsetKey =GovServices::refDashIfNull($r_,$unsetKey);
                        }

                        $html .= '<tr> 
                                    <td align="center">'.( $ke + 1 ).'</td>
                                    <td align="center">'.$MODEL_YEAR .'</td>
                                    <td align="center">'.$OWNER_TYPE_NAME.'</td>
                                  </tr>';
                        $z++;
                    }

                    if (sizeof($record->vehicles) == 0){

                        $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
                    }

                    $html .='</table>';
                }

                // properties
                if (CaseModel::hasPermission('reports.case.LandAuthority')) {
                    $html .='<div style="height: 5px; color: white"> - </div>';

                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.properties info').'
                        </b></td>
                   </tr>';
                    if (sizeof($record->properties) > 0){
                        $html .= '<tr> 
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="55" align="center"><b> '.trans('common::application.#'). '</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="240" align="center"><b> '.trans('common::application.DOC_NO').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="240" align="center"><b> '.trans('common::application.REAL_OWNER_AREA').'</b></td>
                              </tr>';
                    }  $z = 1;

                    foreach($record->properties as $ke=>$r_) {
                        $DOC_NO = $REAL_OWNER_AREA  = '-';
                        $unsetKeys = ['DOC_NO','REAL_OWNER_AREA'];
                        foreach ($unsetKeys as $unsetKey){
                            $$unsetKey =GovServices::refDashIfNull($r_,$unsetKey);
                        }

                        $html .= '<tr> 
                                    <td align="center">'.( $ke + 1 ).'</td>
                                    <td align="center">'.$DOC_NO .'</td>
                                    <td align="center">'.$REAL_OWNER_AREA.'</td>
                                  </tr>';
                        $z++;
                    }

                    if (sizeof($record->properties) == 0){
                        $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
                    }
                    $html .='</table>';

                }

                //commercial
                if (CaseModel::hasPermission('reports.case.MinistryOfEconomy')) {

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.commercials info').'
                        </b></td>
                   </tr>';
                    if (sizeof($record->commercial_data) > 0) {
                        $html .= '<tr> 
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.COMP_NAME') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.REC_CODE') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.PERSON_TYPE_DESC') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.IS_VALID') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.STATUS_ID_DESC') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.COMP_TYPE_DESC') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="40" align="center"><b>  ' . trans('common::application.REC_TYPE') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.REGISTER_NO') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.START_DATE') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.WORK_CLASS_DESC') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.BRAND_NAME') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.CITY_DESC') . '</b></td>
                      </tr>';
                    }
                    $z = 1;

                    foreach($record->commercial_data as $ke=>$r_) {

                        $COMP_NAME = $REC_CODE = $PERSON_TYPE_DESC  = $IS_VALID_DESC  = $STATUS_ID_DESC  = $COMP_TYPE_DESC  =
                        $REC_TYPE_DESC  = $REGISTER_NO  = $START_DATE  = $WORK_CLASS_DESC  = $BRAND_NAME  = $CITY_DESC  = '-';

                        $unsetKeys = ['COMP_NAME','REC_CODE','PERSON_TYPE_DESC','IS_VALID_DESC','STATUS_ID_DESC',
                            'COMP_TYPE_DESC','REC_TYPE_DESC','REGISTER_NO','START_DATE',
                            'WORK_CLASS_DESC','BRAND_NAME','CITY_DESC'];

                        foreach ($unsetKeys as $unsetKey){
                            $$unsetKey =GovServices::refDashIfNull($r_,$unsetKey);
                        }

                        $html .= '<tr> 
                                    <td align="center">'.$COMP_NAME .'</td>
                                    <td align="center">'.$REC_CODE .'</td>
                                    <td align="center">'.$PERSON_TYPE_DESC .'</td>
                                    <td align="center">'.$IS_VALID_DESC .'</td>
                                    <td align="center">'.$STATUS_ID_DESC .'</td>
                                    <td align="center">'.$COMP_TYPE_DESC.'</td>
                                    <td align="center">'.$REC_TYPE_DESC.'</td>
                                    <td align="center">'.$REGISTER_NO.'</td>
                                    <td align="center">'.$START_DATE.'</td>
                                    <td align="center">'.$WORK_CLASS_DESC.'</td>
                                    <td align="center">'.$BRAND_NAME.'</td>
                                    <td align="center">'.$CITY_DESC.'</td>
                                  </tr>';
                        $z++;
                    }

                    if (sizeof($record->commercial_data) == 0){
                        $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
                    }
                    $html .='</table>';
                }

//                parents
                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';

                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.parents').'
                        </b></td>
                   </tr>';
                if (sizeof($record->parent) > 0) {
                    $html .= ' <tr> 
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> ' . trans('common::application.id_card_number') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="123" align="center"><b> ' . trans('common::application.name') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="74" align="center"><b> ' . trans('common::application.prev_family_name') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> ' . trans('common::application.marital_status') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="64" align="center"><b> ' . trans('common::application.birthday') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="64" align="center"><b> ' . trans('common::application.death_date') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> ' . trans('common::application.mobile') . '</b></td>
                </tr>';
                }
                $z = 1;
//                parents
                foreach($record->parent as $row) {
                    $html .= ' <tr> 
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.$z.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'IDNO_RELATIVE').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="123" align="center"><b> '. GovServices::refDashIfNull($row,'FULLNAME') .'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="74" align="center"><b> '.GovServices::refDashIfNull($row,'PREV_LNAME_ARB').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'SOCIAL_STATUS').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="64" align="center"><b> '.GovServices::refDashIfNull($row,'BIRTH_DT').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="64" align="center"><b> '.GovServices::refDashIfNull($row,'DETH_DT').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="70" align="center"><b> '.GovServices::refDashIfNull($row,'MOBILE').'</b></td>
                    </tr>';
                    $z++;
                }
                if (sizeof($record->parent) == 0){
                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
                }

                $html .='</table>';

//                marriage - divorce
                if (CaseModel::hasPermission('reports.case.MinistryOfJustice')) {
                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';

                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>
                        '.trans('common::application.marriage').'
                        </b></td>
                   </tr>';

                    $z = 1;
                    if (sizeof($record->marriage) > 0) {
                        $html .= ' <tr>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="30" align="center"><b>#</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="95" align="center"><b> ' . trans('common::application.CONTRACT_NO') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="95" align="center"><b> ' . trans('common::application.CONTRACT_DT') . '</b></td>';

                        if ($record->SEX_CD == 2){
                            $html .= '
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> ' . trans('common::application.HUSBAND_SSN') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="215" align="center"><b> ' . trans('common::application.HUSBAND_NAME') . '</b></td>
                    </tr>';
                        }else{
                            $html .= '
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> ' . trans('common::application.WIFE_SSN') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="215" align="center"><b> ' . trans('common::application.WIFE_NAME') . '</b></td>
                </tr>';
                        }
                    }

//                marriage
                    foreach($record->marriage as $row) {
                        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="30" align="center"><b>'.$z.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="95" align="center"><b> '.GovServices::refDashIfNull($row,'CONTRACT_NO').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="95" align="center"><b> '. GovServices::refDashIfNull($row,'CONTRACT_DT') .'</b></td>';

                        if ($record->SEX_CD == 2){
                            $html .= '
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="100" align="center"><b> '.GovServices::refDashIfNull($row,'HUSBAND_SSN').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="215" align="center"><b> '.GovServices::refDashIfNull($row,'HUSBAND_NAME').'</b></td> </tr>';

                        }else{
                            $html .= '
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="100" align="center"><b> '.GovServices::refDashIfNull($row,'WIFE_SSN').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="215" align="center"><b> '.GovServices::refDashIfNull($row,'WIFE_NAME').'</b></td>
                    </tr>';
                        }

                        $z++;
                    }

                    if (sizeof($record->marriage) == 0){
                        $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
                    }
                    $html .='</table>';


//                divorce
                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';

                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>
                        '.trans('common::application.divorce').'
                        </b></td>
                   </tr>';
                    if (sizeof($record->divorce) > 0) {
                        $html .= ' <tr>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="65" align="center"><b> ' . trans('common::application.DIV_CERTIFIED_NO') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="65" align="center"><b> ' . trans('common::application.CONTRACT_DT') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="195" align="center"><b> ' . trans('common::application.DIV_TYPE_NAME') . '</b></td>';

                        if ($record->SEX_CD == 2){
                            $html .= '<td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="65" align="center"><b> ' . trans('common::application.HUSBAND_SSN') . '</b></td>
                              <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="125" align="center"><b> ' . trans('common::application.HUSBAND_NAME') . '</b></td>
                               </tr>';

                        }else{
                            $html .= '
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="65" align="center"><b> ' . trans('common::application.WIFE_SSN') . '</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="125" align="center"><b> ' . trans('common::application.WIFE_NAME') . '</b></td>
                        </tr>';
                        }
                    }
                    $z = 1;

//                divorce
                    foreach($record->divorce as $row) {
                        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.$z.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="65" align="center"><b> '.GovServices::refDashIfNull($row,'DIV_CERTIFIED_NO').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="65" align="center"><b> '. GovServices::refDashIfNull($row,'CONTRACT_DT') .'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="195" align="center"><b> '.GovServices::refDashIfNull($row,'DIV_TYPE_NAME').'</b></td>';

                        if ($record->SEX_CD == 2){
                            $html .= '
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="65" align="center"><b> '.GovServices::refDashIfNull($row,'HUSBAND_SSN').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="125" align="center"><b> '.GovServices::refDashIfNull($row,'HUSBAND_NAME').'</b></td>
                        </tr>';

                        }else{
                            $html .= '
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="65" align="center"><b> '.GovServices::refDashIfNull($row,'WIFE_SSN').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="125" align="center"><b> '.GovServices::refDashIfNull($row,'WIFE_NAME').'</b></td>
                    </tr>';
                        }
                        $z++;
                    }
                    if (sizeof($record->divorce) == 0){
                        $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
                    }
                    $html .='</table>';

                }

                $html .='</body></html>';

                PDF::AddPage('P','A4');
                PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                    $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                    PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
                });
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                $pageNo++;
            }

            if (sizeof($record->wives) > 0 || sizeof($record->childrens) > 0){
                $html = '<!doctype html><html lang="ar">
                 <head>
                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                 <style>
                     .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                     table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                     * , body {font-weight:normal !important; font-size: 10px;}
                     .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                     .bolder-content{text-align: right !important; vertical-align: middle!important;}
                     .vertical-middle{ vertical-align: middle !important;}
                </style>
                </head>
                 <body style="font-weight: normal !important;">';

                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.wives or husbands').'
                        </b></td>
                   </tr>';

                if (sizeof($record->wives) > 0){
                    $html .= ' <tr> 
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="27" align="center"><b>#</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.id_card_number').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="123" align="center"><b> '.trans('common::application.name').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="75" align="center"><b> '.trans('common::application.prev_family_name').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.marital_status').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.birthday').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.death_date').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.mobile').'</b></td>
                             </tr>';
                }

                $z = 1;
                foreach($record->wives as $row) {

                    $html .= ' <tr> 
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="27" align="center"><b>'.$z.'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'IDNO_RELATIVE').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="123" align="center"><b> '. GovServices::refDashIfNull($row,'FULLNAME') .'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="75" align="center"><b> '.GovServices::refDashIfNull($row,'PREV_LNAME_ARB').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'SOCIAL_STATUS').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'BIRTH_DT').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'DETH_DT').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="70" align="center"><b> '.GovServices::refDashIfNull($row,'MOBILE').'</b></td>
                    </tr>';
                    $z++;
                }


                if (sizeof($record->wives) == 0){
                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
                }
                $html .='</table>';

// childs
                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.childs').'
                        </b></td>
                   </tr>';

                if (sizeof($record->childrens) > 0){
                    $html .= ' <tr> 
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="27" align="center"><b>#</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.id_card_number').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.name').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="30" align="center"><b> '.trans('common::application.gender').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="53" align="center"><b> '.trans('common::application.marital_status').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.birthday').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.death_date').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="55" align="center"><b> '.trans('common::application.mother_name').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.mobile').'</b></td>
                        </tr>';
                }

                $z = 1;
                foreach($record->childrens as $row) {
                    $html .= ' <tr> 
                           <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="27" align="center"><b>'.$z.'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'IDNO_RELATIVE').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="120" align="center"><b> '. GovServices::refDashIfNull($row,'FULLNAME') .'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="30" align="center"><b> '.GovServices::refDashIfNull($row,'SEX').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="53" align="center"><b> '.GovServices::refDashIfNull($row,'SOCIAL_STATUS').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'BIRTH_DT').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'DETH_DT').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="55" align="center"><b> '.GovServices::refDashIfNull($row,'MOTHER_ARB').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="70" align="center"><b> '.GovServices::refDashIfNull($row,'MOBILE').'</b></td>
                        </tr>';
                    $z++;
                }
                if (sizeof($record->childrens) == 0){
                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
                }
                $html .='</table>';
                $html .='</body></html>';

                PDF::AddPage('P','A4');
                PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                    $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                    PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
                });
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                $pageNo++;
            }

// cases_list
            if (sizeof($record->cases_list) > 0){
                $html = '<!doctype html><html lang="ar">
                 <head>
                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                 <style>
                     .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                     table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                     * , body {font-weight:normal !important; font-size: 10px;}
                     .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                     .bolder-content{text-align: right !important; vertical-align: middle!important;}
                     .vertical-middle{ vertical-align: middle !important;}
                </style>
                </head>
                 <body style="font-weight: normal !important;">';

                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.cases list').'
                        </b></td>
                   </tr>';

                if (sizeof($record->cases_list) > 0){
                    $html .= ' <tr> 
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="75" align="center"><b> '.trans('common::application.case_category_type').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="150" align="center"><b> '.trans('common::application.organization name').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="40" align="center"><b> '.trans('common::application.status').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="89" align="center"><b> '.trans('common::application.visitor').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.visited_at').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.created_at').'</b></td>
                           </tr>';
                }

                $z = 1;
                foreach($record->cases_list as $row) {
                    $html .= ' <tr> 
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.$z.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="75" align="center"><b> '.$row->category_name.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="150" align="center"><b> '.$row->organization_name.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="40" align="center"><b> '.$row->status.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="89" align="center"><b> '.$row->visitor.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="80" align="center"><b> '.$row->visited_at.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="80" align="center"><b> '.$row->created_dt.'</b></td>
                    </tr>';
                    $z++;
                }
                if (sizeof($record->cases_list) == 0){
                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
                }
                $html .='</table>';
                $html .='</body></html>';

                PDF::AddPage('P','A4');
                PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                    $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                    PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
                });
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                $pageNo++;
            }

            // health
            if(CaseModel::hasPermission('reports.case.MinistryOfHealth') && sizeof($record->health) > 0){
                $count = count($record->health);
                $ceil=ceil($count/4);
                for ($x = 0; $x < $ceil; $x++) {
                    $curOffset = ($x) * 4;
                    $index=$curOffset;

                    $html = '<!doctype html><html lang="ar">
                         <head>
                         <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                         <style>
                             .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                             table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                             * , body {font-weight:normal !important; font-size: 10px;}
                             .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                             .bolder-content{text-align: right !important; vertical-align: middle!important;}
                             .vertical-middle{ vertical-align: middle !important;}
                        </style>
                        </head>
                         <body style="font-weight: normal !important;">';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';

                    $html .= ' <tr>
                               <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="534" align="center"><b>  
                                '.trans('common::application.health info').'
                                </b></td>
                           </tr>';


                    $html .= ' <tr> 
                    <td style="height: 40px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="50" align="center"><b> '.trans('common::application.MR_CODE'). '</b></td>
                    <td style="height: 40px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="50" align="center"><b> '.trans('common::application.MR_PATIENT_CD').'</b></td>
                    <td style="height: 40px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="50" align="center"><b> '.trans('common::application.LOC_NAME_AR').'</b></td>
                    <td style="height: 40px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="50" align="center"><b> '.trans('common::application.DOCTOR_NAME').'</b></td>
                    <td style="height: 40px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="60" align="center"><b> '.trans('common::application.MR_CREATED_ON').'</b></td>
                    <td style="height: 40px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="60" align="center"><b> '.trans('common::application.DREF_NAME_AR').'</b></td>
                    <td style="height: 40px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="215" align="center"><b> '.trans('common::application.MR_DIAGNOSIS_AR').'</b></td>
                  </tr>';

                    if($index < $count) {
                        for ($z = 0; $z < 4; $z++) {
                            if($index < $count){
                                $row = $record->health[$index];
                                $html .= ' <tr> 
                                  <td align="center">'.GovServices::refDashIfNull($row,'MR_CODE') .'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'MR_PATIENT_CD').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'LOC_NAME_AR').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'DOCTOR_NAME').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'MR_CREATED_ON').'</td> 
                                   <td align="center">'.GovServices::refDashIfNull($row,'DREF_NAME_AR').'</td> 
                                   <td align="center">'.GovServices::refDashIfNull($row,'MR_DIAGNOSIS_AR').'</td>
                                    </tr>';
                                $index++;
                            }
                        }
                    }
                    $html .='</table>';
                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                        $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                        PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
                    });
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                    $pageNo++;
                }
            }

            // travel
            if(sizeof($record->travel) > 0){
                $count = count($record->travel);
                $ceil=ceil($count/50);
                for ($x = 0; $x < $ceil; $x++) {
                    $curOffset = ($x) * 50;
                    $index=$curOffset;

                    $html = '<!doctype html><html lang="ar">
                         <head>
                         <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                         <style>
                             .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                             table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                             * , body {font-weight:normal !important; font-size: 10px;}
                             .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                             .bolder-content{text-align: right !important; vertical-align: middle!important;}
                             .vertical-middle{ vertical-align: middle !important;}
                        </style>
                        </head>
                         <body style="font-weight: normal !important;">';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';

                    $html .= ' <tr>
                               <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.travel records').'
                                </b></td>
                           </tr>';


                    $html .= '<tr> 
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="55" align="center"><b> '.trans('common::application.#'). '</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="180" align="center"><b> '.trans('common::application.CTZN_TRANS_DT').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="150" align="center"><b> '.trans('common::application.CTZN_TRANS_TYPE').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="150" align="center"><b> '.trans('common::application.CTZN_TRANS_BORDER').'</b></td>
                              </tr>';

                    if($index < $count) {
                        for ($z = 0; $z < 50; $z++) {
                            if($index < $count){
                                $row = $record->travel[$index];
                                $html .= ' <tr> 
                                  <td align="center">'.($index+1).'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'CTZN_TRANS_DT').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'CTZN_TRANS_TYPE').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'CTZN_TRANS_BORDER').'</td>
                                    </tr>';
                                $index++;
                            }
                        }
                    }
                    $html .='</table>';
                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                        $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                        PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
                    });
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                    $pageNo++;
                }
            }

            // reg48_license
            if(CaseModel::hasPermission('reports.case.MinistryOfLabor') && sizeof($record->reg48_license) > 0){
                $count = count($record->reg48_license);
                $ceil=ceil($count/28);
                for ($x = 0; $x < $ceil; $x++) {
                    $curOffset = ($x) * 28;
                    $index=$curOffset;

                    $html = '<!doctype html><html lang="ar">
                         <head>
                         <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                         <style>
                             .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                             table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                             * , body {font-weight:normal !important; font-size: 10px;}
                             .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                             .bolder-content{text-align: right !important; vertical-align: middle!important;}
                             .vertical-middle{ vertical-align: middle !important;}
                        </style>
                        </head>
                         <body style="font-weight: normal !important;">';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';

                    $html .= ' <tr>
                               <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.reg48_license').'
                                </b></td>
                           </tr>';


                    $html .= '<tr> 
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="55" align="center"><b> '.trans('common::application.#'). '</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="180" align="center"><b> '.trans('common::application.WORKER_ID').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="150" align="center"><b> '.trans('common::application.STATUS_NAME').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="150" align="center"><b> '.trans('common::application.DATE_FROM').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="150" align="center"><b> '.trans('common::application.DATE_TO').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="150" align="center"><b> '.trans('common::application.LICENSE_NAME').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="150" align="center"><b> '.trans('common::application.LIC_REC_STATUS_NAME').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="150" align="center"><b> '.trans('common::application.LIC_REC_DATE').'</b></td>
                              </tr>';

                    if($index < $count) {
                        for ($z = 0; $z < 28; $z++) {
                            if($index < $count){
                                $row = $record->reg48_license[$index];
                                $html .= ' <tr> 
                                  <td align="center">'.($index+1).'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'WORKER_ID').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'STATUS_NAME').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'DATE_FROM').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'DATE_TO').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'LICENSE_NAME').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'LIC_REC_STATUS_NAME').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'LIC_REC_DATE').'</td>
                                    </tr>';
                                $index++;
                            }
                        }
                    }
                    $html .='</table>';
                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                        $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                        PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
                    });
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                    $pageNo++;
                }
            }

            // employment
            if(CaseModel::hasPermission('reports.case.MinistryOfFinance') && sizeof($record->employment) > 0){
                $count = count($record->employment);
                $ceil=ceil($count/25);
                for ($x = 0; $x < $ceil; $x++) {
                    $curOffset = ($x) * 25;
                    $index=$curOffset;

                    $html = '<!doctype html><html lang="ar">
                         <head>
                         <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                         <style>
                             .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                             table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                             * , body {font-weight:normal !important; font-size: 10px;}
                             .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                             .bolder-content{text-align: right !important; vertical-align: middle!important;}
                             .vertical-middle{ vertical-align: middle !important;}
                        </style>
                        </head>
                         <body style="font-weight: normal !important;">';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';

                    $html .= ' <tr>
                               <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.work info').'
                                </b></td>
                           </tr>';


                    $html .= '<tr> 
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="20" align="center"><b> '.trans('common::application.#'). '</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="55" align="center"><b> '.trans('common::application.EMP_DOC_ID').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="100" align="center"><b> '.trans('common::application.MINISTRY_NAME').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="75" align="center"><b> '.trans('common::application.JOB_START_DT').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b> '.trans('common::application.DEGREE_NAME').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b> '.trans('common::application.EMP_STATE_DESC').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b> '.trans('common::application.EMP_WORK_STATUS').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="75" align="center"><b> '.trans('common::application.JOB_DESC').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="75" align="center"><b> '.trans('common::application.MIN_DATA_SOURCE').'</b></td>
                              </tr>';

                    if($index < $count) {
                        for ($z = 0; $z < 25; $z++) {
                            if($index < $count){
                                $row = $record->employment[$index];
                                $html .= ' <tr> 
                                  <td align="center">'.($index+1).'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'EMP_DOC_ID').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'MINISTRY_NAME').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'JOB_START_DT').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'DEGREE_NAME').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'EMP_STATE_DESC').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'EMP_WORK_STATUS').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'JOB_DESC').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'MIN_DATA_SOURCE').'</td>
                                    </tr>';
                                $index++;
                            }
                        }
                    }
                    $html .='</table>';
                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                        $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                        PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
                    });
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                    $pageNo++;
                }
            }

            // social_affairs_receipt
            if(CaseModel::hasPermission('reports.case.MinistryOfSocialDevelopment') && sizeof($record->social_affairs_receipt) > 0){
                $count = count($record->social_affairs_receipt);
                $ceil=ceil($count/25);
                for ($x = 0; $x < $ceil; $x++) {
                    $curOffset = ($x) * 25;
                    $index=$curOffset;

                    $html = '<!doctype html><html lang="ar">
                         <head>
                         <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                         <style>
                             .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                             table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                             * , body {font-weight:normal !important; font-size: 10px;}
                             .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                             .bolder-content{text-align: right !important; vertical-align: middle!important;}
                             .vertical-middle{ vertical-align: middle !important;}
                        </style>
                        </head>
                         <body style="font-weight: normal !important;">';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';

                    $html .= ' <tr>
                               <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="534" align="center"><b>  
                                '.trans('common::application.social affairs').'
                                </b></td>
                           </tr>';


                    $html .= ' <tr> 
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="146" align="center"><b> '.trans('common::application.SRV_INF_NAME').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="145" align="center"><b> '.trans('common::application.ORG_NM_MON').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="54" align="center"><b> '.trans('common::application.SRV_TYPE_MAIN_NM').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="54" align="center"><b> '.trans('common::application.CURRENCY').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="50" align="center"><b> '.trans('common::application.RECP_AID_AMOUNT').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="65" align="center"><b> '.trans('common::application.RECP_DELV_DT').'</b></td>
                    </tr>';

                    if($index < $count) {
                        for ($z = 0; $z < 25; $z++) {
                            if($index < $count){
                                $row = $record->social_affairs_receipt[$index];
                                $html .= ' <tr> 
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.($index+1).'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="146" align="center"><b> '.GovServices::refDashIfNull($row,'SRV_INF_NAME').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="145" align="center"><b> '.GovServices::refDashIfNull($row,'ORG_NM_MON').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="54" align="center"><b> '.GovServices::refDashIfNull($row,'SRV_TYPE_MAIN_NM').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="54" align="center"><b> '.GovServices::refDashIfNull($row,'CURRENCY').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="50" align="center"><b> '.GovServices::refDashIfNull($row,'RECP_AID_AMOUNT').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="65" align="center"><b> '.GovServices::refDashIfNull($row,'RECP_DELV_DT').'</b></td>
                                </tr>';
                                $index++;
                            }
                        }
                    }
                    $html .='</table>';
                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                        $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                        PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
                    });
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                    $pageNo++;

                }
            }

            $dirName = base_path('storage/app/citizen_pdf');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }

            PDF::Output($dirName.'/'.$full_name . '.pdf', 'F');

            unlink(storage_path('app/documents/'.$token.'.png'));
            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');

            $zip = new \ZipArchive;
            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                foreach ( glob( $dirName . '/*' ) as $fileName )
                {
                    $file = basename( $fileName );
                    $zip->addFile( $fileName, $file );
                }

                $zip->close();
                array_map('unlink', glob("$dirName/*.*"));
                rmdir($dirName);
            }

            return response()->json(['download_token' => $token]);

        }

        $person = null;
        $row = GovServices::byCard($card);
        if($row['status'] == false){
            return response()->json(['status'=>false,'msg'=>$row['message']]);
        }

        $record=$row['row'];

        $passport_data = GovServices::passportInfo($card);
        $record->passport=null;
        if($passport_data['status'] != false){
            if(isset($passport_data['row']->ISSUED_ON)){
                $passport_data['row']->ISSUED_ON = date('d/m/Y',strtotime($passport_data['row']->ISSUED_ON));
            }
            $record->passport=$passport_data['row'];
        }

        $travel_data = GovServices::travelRecords($card);
        $record->travel=[];
        if($travel_data['status'] != false){
            $record->travel=$travel_data['row'];
        }

        $record->CASE_STATUS=false;
        $record->cases_list=[];
        $record->person_id = Person::getPersonId([$card]);
        if (CaseModel::hasPermission('reports.case.RelayCitizenRepository')) {
            $record->cases_list = Person::getPersonCardCases([$card]);
        }


        CloneGovernmentPersons::saveNew($record);
        return response()->json([ 'status'=>true, 'data'=>$record ]);
    }

    // check citizen using excel sheet according card number
    public function citizenRepositoryCheck(Request $request)
    {
        $this->authorize('citizenRepository', \Common\Model\AidsCases::class);
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $type = $request->type;
        $validFile=true;
        $duplicated =0;
        $success =0;

        $final_records=[];
        $invalid_cards=[];
        $processed=[];

        $vehicles=[];
        $reg48_license=[];
        $properties=[];
        $marriage=[];
        $divorce=[];
        $travel=[];
        $employment=[];
        $health=[];
        $commercial_data=[];
        $social_affairs_receipt=[];
        $cases_list=[];

        $importFile=$request->file ;
        if ($importFile->isValid()) {
            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){

                        $validFile = false;
                        foreach ($keys as $item) {
                            if ( $item == 'rkm_alhoy' ) {
                                $validFile = true;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);
                            $health = [];

                            if ($total > 0) {
                                foreach ($records as $key =>$value) {
                                    if(in_array($value['rkm_alhoy'],$processed)){
                                        $duplicated++;
                                    }
                                    else{
                                        $card= (int) $value['rkm_alhoy'];
                                        if(strlen(($card))  <= 9) {
                                            if(GovServices::checkCard($card)){
                                                if ($type == 2){
                                                    $row = GovServices::allByCard($card);
                                                }else{
                                                    $pre_row = GovServices::byCard($card);
                                                    $row = GovServices::setMain($pre_row['row'],$card);
                                                }
                                                if($row['status'] == true) {
                                                    $temp = $row['row'];

                                                    $main=(array) $temp;
                                                    $main['related']= null;

                                                    if($type == 2){
                                                        if (sizeof($main['all_relatives']) > 0){
                                                            $rel_keys = ['parent','childrens','wives'];
                                                            foreach ($rel_keys as $key){
                                                                foreach ($temp->$key as $k=>$v){
                                                                    if ($key == 'childrens'){
                                                                        if ($temp->SEX_CD == 1){
                                                                            $v->FATHER_IDNO = $request->id_card_number;
                                                                        }else{
                                                                            $v->MOTHER_IDNO = $request->id_card_number;
                                                                        }
                                                                    }
                                                                    $v->IDNO = $v->IDNO_RELATIVE;
                                                                    CloneGovernmentPersons::saveNewWithRelation($card,$v);
                                                                    $main['related']= $v;
                                                                    $final_records[] = $main;
                                                                }
                                                            }
                                                        }
                                                        else{
                                                            $main['related']= null;
                                                            $final_records[]=$main;
                                                        }

                                                        $ref_keys = ['vehicles','reg48_license','properties','commercial_data','cases_list',
                                                            'health','travel','employment','social_affairs_receipt'];

                                                        foreach ($ref_keys as $ref_key){
                                                            $sub_array = $temp->$ref_key;
                                                            if (sizeof($sub_array) > 0 ){
                                                                $main_array = $$ref_key;
                                                                $append_arr = GovServices::pushToArray($main_array,$sub_array,$card);
                                                                $$ref_key = $append_arr;
                                                            }
                                                        }

                                                        $ref_keys = ['marriage','divorce'];

                                                        foreach ($ref_keys as $ref_key){
                                                            $sub_array = $temp->marriage_divorce[$ref_key];
                                                            if (sizeof($sub_array) > 0 ){
                                                                $main_array = $$ref_key;
                                                                $append_arr = GovServices::pushToArray($main_array,$sub_array,$card);
                                                                $$ref_key = $append_arr;
                                                            }
                                                        }
                                                        CloneGovernmentPersons::saveNew((Object) $main);

                                                    }
                                                    else if($type == 3){
                                                        CloneGovernmentPersons::saveNew($temp);
                                                        if (sizeof($main['wives']) > 0){
                                                            foreach ($temp->wives as $k=>$v){
                                                                $main['related']= $v;
                                                                $final_records[] = $main;
                                                            }
                                                        }else{
                                                            $main['related']= null;
                                                            $final_records[]=$main;
                                                        }
                                                    }
                                                    else if($type == 4){
                                                        CloneGovernmentPersons::saveNew($temp);
                                                        if (sizeof($temp->childrens) > 0){
                                                            foreach ($temp->childrens as $k=>$v){
                                                                if ($temp->SEX_CD == 1){
                                                                    $v->FATHER_IDNO = $request->id_card_number;
                                                                }else{
                                                                    $v->MOTHER_IDNO = $request->id_card_number;
                                                                }
                                                                $v->IDNO = $v->IDNO_RELATIVE;
                                                                CloneGovernmentPersons::saveNewWithRelation($card,$v);
                                                                $main['related']= $v;
                                                                $final_records[] = $main;
                                                            }
                                                        }else{
                                                            $main['related']= null;
                                                            $final_records[]=$main;
                                                        }

                                                    }
                                                    else{
                                                        CloneGovernmentPersons::saveNew($temp);
                                                        $final_records[] = $main;
                                                    }
                                                    $processed[]=$value['rkm_alhoy'];
                                                    $success++;
                                                }else{
                                                    $invalid_cards[]=$value['rkm_alhoy'];
                                                }
                                            }else{
                                                $invalid_cards[]=$value['rkm_alhoy'];
                                            }

                                        }else{
                                            $invalid_cards[]=$value['rkm_alhoy'];
                                        }
                                    }
                                }

                                if($total == sizeof($invalid_cards)){
                                    return response()->json(['status' => 'failed' ,
                                        'error_type' =>' ' , 'msg' =>trans('common::application.All names entered are not registered in the Civil Registry')  ]);
                                }else{

                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function($excel) use($final_records,$invalid_cards,$vehicles, $reg48_license,
                                        $properties, $marriage, $divorce, $travel, $employment, $health,
                                        $commercial_data, $social_affairs_receipt, $cases_list,$type) {
                                        $excel->setTitle(trans('common::application.family_sheet'));
                                        $excel->setDescription(trans('common::application.family_sheet'));

                                        $excel->sheet(trans('common::application.family_sheet'), function($sheet) use($final_records,$type) {

                                            $sheet->setRightToLeft(true);
                                            $sheet->setAllBorders('thin');
                                            $sheet->setfitToWidth(true);
                                            $sheet->setHeight(1,40);
                                            $sheet->getDefaultStyle()->applyFromArray([
                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                            ]);

                                            $sheet->getStyle("A1:V1")->applyFromArray(['font' => ['bold' => true]]);

                                            $start_from =1;

                                            $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                            $sheet->setCellValue('B' . $start_from ,trans('common::application.id_card_number'));
                                            $sheet->setCellValue('C' . $start_from ,trans('common::application.full_name'));
                                            $sheet->setCellValue('D' . $start_from ,trans('common::application.en_name'));
                                            $sheet->setCellValue('E' . $start_from ,trans('common::application.gender'));
                                            $sheet->setCellValue('F' . $start_from ,trans('common::application.marital_status'));
                                            $sheet->setCellValue('G' . $start_from ,trans('common::application.birthday'));
                                            $sheet->setCellValue('H' . $start_from ,trans('common::application.death_date'));
                                            $sheet->setCellValue('I' . $start_from ,trans('common::application.birth_place_main'));
                                            $sheet->setCellValue('J' . $start_from ,trans('common::application.birth_place_sub'));
                                            $sheet->setCellValue('K' . $start_from ,trans('common::application.mother_name'));
                                            $sheet->setCellValue('L' . $start_from ,trans('common::application.prev_family_name'));
                                            $sheet->setCellValue('M' . $start_from ,trans('common::application.address'));
                                            $sheet->setCellValue('N' . $start_from ,trans('common::application.sso info') .'( ' .trans('common::application.address') .'-' .trans('common::application.GOV_NAME'). ' )');
                                            $sheet->setCellValue('O' . $start_from ,trans('common::application.sso info') .'( ' .trans('common::application.address') .'-' .trans('common::application.CITY_NAME'). ' )');
                                            $sheet->setCellValue('P' . $start_from ,trans('common::application.sso info') .'( ' .trans('common::application.address') .'-' .trans('common::application.PART_NAME'). ' )');
                                            $sheet->setCellValue('Q' . $start_from ,trans('common::application.sso info') .'( ' .trans('common::application.address') .'-' .trans('common::application.ADDRESS_DET'). ' )');
                                            $sheet->setCellValue('R' . $start_from ,trans('common::application.mobile'));
                                            $sheet->setCellValue('S' . $start_from ,trans('common::application.phone'));
                                            $sheet->setCellValue('T' . $start_from ,trans('common::application.family_cnt'));
                                            $sheet->setCellValue('U' . $start_from ,trans('common::application.spouses'));
                                            $sheet->setCellValue('V' . $start_from ,trans('common::application.male_live'));
                                            $sheet->setCellValue('W' . $start_from ,trans('common::application.female_live'));

                                            $sheet->setCellValue('x' . $start_from ,trans('common::application.CTZN_STATUS'));
                                            $sheet->setCellValue('y' . $start_from ,trans('common::application.CTZN_TRANS_DT'));
                                            $sheet->setCellValue('z' . $start_from ,trans('common::application.VISIT_PURPOSE_DESC'));

                                            $sheet->setCellValue('AA' . $start_from ,trans('common::application.PASS_TYPE'));
                                            $sheet->setCellValue('AB' . $start_from ,trans('common::application.PASSPORT_NO'));
                                            $sheet->setCellValue('AC' . $start_from ,trans('common::application.ISSUED_ON'));

                                            $sheet->setCellValue('AD' . $start_from ,trans('common::application.CARD_NO'));
                                            $sheet->setCellValue('AE' . $start_from ,trans('common::application.EXP_DATE'));
                                            $sheet->setCellValue('AF' . $start_from ,trans('common::application.INS_STATUS_DESC'));
                                            $sheet->setCellValue('AG' . $start_from ,trans('common::application.INS_TYPE_DESC'));
                                            $sheet->setCellValue('AH' . $start_from ,trans('common::application.WORK_SITE_DESC'));

                                            $sheet->setCellValue('AI' . $start_from ,trans('common::application.aid_status'));
                                            $sheet->setCellValue('AJ' . $start_from ,trans('common::application.AID_CLASS'));
                                            $sheet->setCellValue('AK' . $start_from ,trans('common::application.AID_TYPE'));
                                            $sheet->setCellValue('AL' . $start_from ,trans('common::application.AID_AMOUNT'));
                                            $sheet->setCellValue('AM' . $start_from ,trans('common::application.AID_SOURCE'));
                                            $sheet->setCellValue('AN' . $start_from ,trans('common::application.AID_PERIODIC'));
                                            $sheet->setCellValue('AO' . $start_from ,trans('common::application.ST_BENEFIT_DATE'));
                                            $sheet->setCellValue('AP' . $start_from ,trans('common::application.END_BENEFIT_DATE'));

//                                            $sheet->setCellValue('AQ' . $start_from ,trans('common::application.address'));
//                                            $sheet->setCellValue('AR' . $start_from ,trans('common::application.near_mosque'));
//                                            $sheet->setCellValue('AS' . $start_from ,trans('common::application.mobile'));
//                                            $sheet->setCellValue('AT' . $start_from ,trans('common::application.phone'));
//                                            $sheet->setCellValue('AU' . $start_from ,trans('common::application.wife_mobile'));
//                                            $sheet->setCellValue('AV' . $start_from ,trans('common::application.current_career'));
//                                            $sheet->setCellValue('AW' . $start_from ,trans('common::application.father_death_reason'));
//                                            $sheet->setCellValue('AX' . $start_from ,trans('common::application.mother_death_reason'));
//                                            $sheet->setCellValue('AY' . $start_from ,trans('common::application.building_type'));
//                                            $sheet->setCellValue('AZ' . $start_from ,trans('common::application.home_type'));
//                                            $sheet->setCellValue('BA' . $start_from ,trans('common::application.furniture_type'));
//                                            $sheet->setCellValue('BB' . $start_from ,trans('common::application.home_status'));
//                                            $sheet->setCellValue('BV' . $start_from ,trans('common::application.home_description'));
//                                            $sheet->setCellValue('BD' . $start_from ,trans('common::application.total_family_income'));
//                                            $sheet->setCellValue('BE' . $start_from ,trans('common::application.health_status'));

                                            if($type != 1){
                                                $sheet->setCellValue('AQ' . $start_from ,trans('common::application.id_card_number').' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('AR' . $start_from ,trans('common::application.kinship_name') .' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('AS' . $start_from ,trans('common::application.full_name').' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('AT' . $start_from ,trans('common::application.gender').' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('AU' . $start_from ,trans('common::application.marital_status').' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('AV' . $start_from ,trans('common::application.birthday').' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('AW' . $start_from ,trans('common::application.death_date').' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('AX' . $start_from ,trans('common::application.prev_family_name').' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('AY' . $start_from ,trans('common::application.mother_name') .' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('AZ' . $start_from ,trans('common::application.mobile') .' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('BA' . $start_from ,trans('common::application.phone') .' ('.trans('common::application.related') .' )');
                                            }

                                            $z= 2;
                                            foreach($final_records as $k=>$main )
                                            {
                                                $record = (Object) $main;
                                                $v = (Object) $record->related;

                                                $sheet->setCellValue('A'.$z,$k+1);
                                                $sheet->setCellValue('B' . $z ,GovServices::refDashIfNull($record,'IDNO'));
                                                $sheet->setCellValue('C' . $z ,GovServices::refDashIfNull($record,'FULLNAME'));
                                                $sheet->setCellValue('D' . $z ,GovServices::refDashIfNull($record,'ENG_NAME'));
                                                $sheet->setCellValue('E' . $z ,GovServices::refDashIfNull($record,'SEX'));
                                                $sheet->setCellValue('F' . $z ,GovServices::refDashIfNull($record,'SOCIAL_STATUS'));
                                                $sheet->setCellValue('G' . $z ,GovServices::refDashIfNull($record,'BIRTH_DT'));
                                                $sheet->setCellValue('H' . $z ,GovServices::refDashIfNull($record,'DETH_DT'));
                                                $sheet->setCellValue('I' . $z ,GovServices::refDashIfNull($record,'BIRTH_PMAIN'));
                                                $sheet->setCellValue('J' . $z ,GovServices::refDashIfNull($record,'BIRTH_PMAIN'));

                                                $sheet->setCellValue('K' . $z ,GovServices::refDashIfNull($record,'MOTHER_ARB'));
                                                $sheet->setCellValue('L' . $z ,GovServices::refDashIfNull($record,'PREV_LNAME_ARB'));
                                                $sheet->setCellValue('M' . $z ,( (is_null($record->CI_REGION) ||$record->CI_REGION == ' ' ) ? ' ' :$record->CI_REGION) .' '.
                                                    ( (is_null($record->CI_CITY) ||$record->CI_CITY == ' ' ) ? ' ' :$record->CI_CITY) .' '.
                                                    ( (is_null($record->STREET_ARB) ||$record->STREET_ARB == ' ' ) ? ' ' :$record->STREET_ARB));

                                                $sheet->setCellValue('N' . $z ,GovServices::refDashIfNull($record,'GOV_NAME'));
                                                $sheet->setCellValue('O' . $z ,GovServices::refDashIfNull($record,'CITY_NAME'));
                                                $sheet->setCellValue('P' . $z ,GovServices::refDashIfNull($record,'PART_NAME'));
                                                $sheet->setCellValue('Q' . $z ,GovServices::refDashIfNull($record,'ADDRESS_DET'));
                                                $sheet->setCellValue('R' . $z ,GovServices::refDashIfNull($record,'MOBILE'));
                                                $sheet->setCellValue('S' . $z ,GovServices::refDashIfNull($v,'TEL'));
                                                $sheet->setCellValue('T' . $z ,GovServices::refDashIfNull($record,'family_cnt'));
                                                $sheet->setCellValue('U' . $z ,GovServices::refDashIfNull($record,'spouses'));
                                                $sheet->setCellValue('V' . $z ,GovServices::refDashIfNull($record,'male_live'));
                                                $sheet->setCellValue('W' . $z ,GovServices::refDashIfNull($record,'female_live'));

                                                $sheet->setCellValue('X' . $z ,GovServices::refDashIfNull($record,'CTZN_STATUS'));
                                                $sheet->setCellValue('Y' . $z ,GovServices::refDashIfNull($record,'CTZN_TRANS_DT'));
                                                $sheet->setCellValue('Z' . $z ,GovServices::refDashIfNull($record,'VISIT_PURPOSE_DESC'));

                                                $sheet->setCellValue('AA' . $z ,GovServices::refDashIfNull($record->passport,'PASS_TYPE'));
                                                $sheet->setCellValue('AB' . $z ,GovServices::refDashIfNull($record->passport,'PASSPORT_NO'));
                                                $sheet->setCellValue('AC' . $z ,GovServices::refDashIfNull($record->passport,'ISSUED_ON'));

                                                $sheet->setCellValue('AD' . $z ,GovServices::refDashIfNull($record->health_insurance_data,'CARD_NO'));
                                                $sheet->setCellValue('AE' . $z ,GovServices::refDashIfNull($record->health_insurance_data,'EXP_DATE'));
                                                $sheet->setCellValue('AF' . $z ,GovServices::refDashIfNull($record->health_insurance_data,'INS_STATUS_DESC'));
                                                $sheet->setCellValue('AG' . $z ,GovServices::refDashIfNull($record->health_insurance_data,'INS_TYPE_DESC'));
                                                $sheet->setCellValue('AH' . $z ,GovServices::refDashIfNull($record->health_insurance_data,'WORK_SITE_DESC'));

                                                $sheet->setCellValue('AI' . $z ,GovServices::refDashIfNull($record->social_affairs,'social_affairs_status'));
                                                $sheet->setCellValue('AJ' . $z ,GovServices::refDashIfNull($record->social_affairs,'AID_CLASS'));
                                                $sheet->setCellValue('AK' . $z ,GovServices::refDashIfNull($record->social_affairs,'AID_TYPE'));
                                                $sheet->setCellValue('AL' . $z ,GovServices::refDashIfNull($record->social_affairs,'AID_AMOUNT'));
                                                $sheet->setCellValue('AM' . $z ,GovServices::refDashIfNull($record->social_affairs,'AID_SOURCE'));
                                                $sheet->setCellValue('AN' . $z ,GovServices::refDashIfNull($record->social_affairs,'AID_PERIODIC'));
                                                $sheet->setCellValue('AO' . $z ,GovServices::refDashIfNull($record->social_affairs,'ST_BENEFIT_DATE'));
                                                $sheet->setCellValue('AP' . $z ,GovServices::refDashIfNull($record->social_affairs,'END_BENEFIT_DATE'));

//                                                $affected_by_wars = ($record->aids_committee->affected_by_wars == 1) ? 'yes' :'no' ;
//                                                $sheet->setCellValue('AQ' . $z ,GovServices::refDashIfNull($record->aids_committee,'home_address'));
//                                                $sheet->setCellValue('AR' . $z ,GovServices::refDashIfNull($record->aids_committee,'near_mosque'));
//                                                $sheet->setCellValue('AS' . $z ,GovServices::refDashIfNull($record->aids_committee,'paterfamilias_mobile'));
//                                                $sheet->setCellValue('AT' . $z ,GovServices::refDashIfNull($record->aids_committee,'telephone'));
//                                                $sheet->setCellValue('AU' . $z ,GovServices::refDashIfNull($record->aids_committee,'wife_mobile'));
//                                                $sheet->setCellValue('AV' . $z ,GovServices::refDashIfNull($record->aids_committee,'current_career'));
//                                                $sheet->setCellValue('AW' . $z ,GovServices::refDashIfNull($record->aids_committee,'father_death_reason'));
//                                                $sheet->setCellValue('AZ' . $z ,GovServices::refDashIfNull($record->aids_committee,'mother_death_reason'));
//                                                $sheet->setCellValue('AY' . $z ,GovServices::refDashIfNull($record->aids_committee,'building_type'));
//                                                $sheet->setCellValue('AZ' . $z ,GovServices::refDashIfNull($record->aids_committee,'home_type'));
//                                                $sheet->setCellValue('BA' . $z ,GovServices::refDashIfNull($record->aids_committee,'furniture_type'));
//                                                $sheet->setCellValue('BB' . $z ,GovServices::refDashIfNull($record->aids_committee,'home_status'));
//                                                $sheet->setCellValue('BC' . $z ,GovServices::refDashIfNull($record->aids_committee,'home_description'));
//                                                $sheet->setCellValue('BD' . $z ,GovServices::refDashIfNull($record->aids_committee,'total_family_income'));
//                                                $sheet->setCellValue('BE' . $z ,GovServices::refDashIfNull($record->aids_committee,'health_status'));

                                                if($type != 1) {
                                                    $sheet->setCellValue('AQ' . $z ,GovServices::refDashIfNull($v,'IDNO_RELATIVE'));
                                                    $sheet->setCellValue('AR' . $z ,GovServices::refDashIfNull($v,'RELATIVE_DESC'));
                                                    $sheet->setCellValue('AS' . $z ,GovServices::refDashIfNull($v,'FULLNAME'));
                                                    $sheet->setCellValue('AT' . $z ,GovServices::refDashIfNull($v,'SEX'));
                                                    $sheet->setCellValue('AU' . $z ,GovServices::refDashIfNull($v,'SOCIAL_STATUS'));
                                                    $sheet->setCellValue('AV' . $z ,GovServices::refDashIfNull($v,'BIRTH_DT'));
                                                    $sheet->setCellValue('AW' . $z ,GovServices::refDashIfNull($v,'DETH_DT'));
                                                    $sheet->setCellValue('AX' . $z ,GovServices::refDashIfNull($v,'PREV_LNAME_ARB'));
                                                    $sheet->setCellValue('AY' . $z ,GovServices::refDashIfNull($v,'MOTHER_ARB') );
                                                    $sheet->setCellValue('AZ' . $z ,GovServices::refDashIfNull($v,'MOBILE') );
                                                    $sheet->setCellValue('BA' . $z ,GovServices::refDashIfNull($v,'TEL'));
                                                }

                                                $sheet->getRowDimension($z)->setRowHeight(-1);
                                                $z++;
                                            }

                                        });

                                        if(CaseModel::hasPermission('reports.case.MinistryOfJustice') && sizeof($marriage) > 0){
                                            $excel->sheet(trans('common::application.marriage'), function($sheet) use($marriage) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:E1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.CONTRACT_NO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.CONTRACT_DT'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.HUSBAND_SSN'));
                                                $sheet->setCellValue('E' . $start_from ,trans('common::application.HUSBAND_NAME'));
                                                $sheet->setCellValue('F' . $start_from ,trans('common::application.WIFE_SSN'));
                                                $sheet->setCellValue('G' . $start_from ,trans('common::application.WIFE_NAME'));

                                                $z= 2;
                                                foreach($marriage as $k => $v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'CONTRACT_NO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'CONTRACT_DT'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'HUSBAND_SSN'));
                                                    $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'HUSBAND_NAME'));
                                                    $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'WIFE_SSN'));
                                                    $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'WIFE_NAME'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }
                                                $keys = ['A','B','C','D','E','F','G'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(CaseModel::hasPermission('reports.case.MinistryOfJustice') && sizeof($divorce) > 0){
                                            $excel->sheet(trans('common::application.divorce'), function($sheet) use($divorce) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:H1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.DIV_CERTIFIED_NO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.CONTRACT_DT'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.DIV_TYPE_NAME'));
                                                $sheet->setCellValue('E' . $start_from ,trans('common::application.HUSBAND_SSN'));
                                                $sheet->setCellValue('F' . $start_from ,trans('common::application.HUSBAND_NAME'));
                                                $sheet->setCellValue('G' . $start_from ,trans('common::application.WIFE_SSN'));
                                                $sheet->setCellValue('H' . $start_from ,trans('common::application.WIFE_NAME'));

                                                $z= 2;
                                                foreach($divorce as $k => $v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'DIV_CERTIFIED_NO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'CONTRACT_DT'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'DIV_TYPE_NAME'));
                                                    $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'HUSBAND_SSN'));
                                                    $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'HUSBAND_NAME'));
                                                    $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'WIFE_SSN'));
                                                    $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'WIFE_NAME'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }
                                                $keys = ['A','B','C','D','E','F'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(CaseModel::hasPermission('reports.case.MinistryOfTransportation') && sizeof($vehicles) > 0){
                                            $excel->sheet(trans('common::application.vehicles info'), function($sheet) use($vehicles) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:D1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.MAIN_IDO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.MODEL_YEAR'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.OWNER_TYPE_NAME'));

                                                $z= 2;
                                                foreach($vehicles as $k => $v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MAIN_IDO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'MODEL_YEAR'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'OWNER_TYPE_NAME'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }
                                                $keys = ['A','B','C','D'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(CaseModel::hasPermission('reports.case.MinistryOfFinance') && sizeof($employment) > 0){
                                            $excel->sheet(trans('common::application.work info'), function($sheet) use($employment) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getStyle("A1:J1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.MAIN_IDO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.EMP_DOC_ID'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.MINISTRY_NAME'));
                                                $sheet->setCellValue('E' . $start_from ,trans('common::application.JOB_START_DT'));
                                                $sheet->setCellValue('F' . $start_from ,trans('common::application.DEGREE_NAME'));
                                                $sheet->setCellValue('G' . $start_from ,trans('common::application.EMP_STATE_DESC'));
                                                $sheet->setCellValue('H' . $start_from ,trans('common::application.EMP_WORK_STATUS'));
                                                $sheet->setCellValue('I' . $start_from ,trans('common::application.JOB_DESC'));
                                                $sheet->setCellValue('J' . $start_from ,trans('common::application.MIN_DATA_SOURCE'));

                                                $z= 2;
                                                foreach($employment as $k => $v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MAIN_IDO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'EMP_DOC_ID'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'MINISTRY_NAME'));
                                                    $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'JOB_START_DT'));
                                                    $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'DEGREE_NAME'));
                                                    $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'EMP_STATE_DESC'));
                                                    $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'EMP_WORK_STATUS'));
                                                    $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($v,'JOB_DESC'));
                                                    $sheet->setCellValue('J'.$z,GovServices::refDashIfNull($v,'MIN_DATA_SOURCE'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }
                                                $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G' ,'H','I'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(CaseModel::hasPermission('reports.case.LandAuthority') && sizeof($properties) > 0){
                                            $excel->sheet(trans('common::application.properties info'), function($sheet) use($properties) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:D1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.MAIN_IDO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.DOC_NO'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.REAL_OWNER_AREA'));

                                                $z= 2;
                                                foreach($properties as $k => $v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MAIN_IDO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'DOC_NO'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'REAL_OWNER_AREA'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }
                                                $keys = ['A','B','C'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(CaseModel::hasPermission('reports.case.MinistryOfEconomy') && sizeof($commercial_data) > 0){
                                            $excel->sheet(trans('common::application.commercials info'), function($sheet) use($commercial_data) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:N1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.MAIN_IDO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.COMP_NAME'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.REC_CODE'));
                                                $sheet->setCellValue('E' . $start_from ,trans('common::application.PERSON_TYPE_DESC'));
                                                $sheet->setCellValue('F' . $start_from ,trans('common::application.IS_VALID'));
                                                $sheet->setCellValue('G' . $start_from ,trans('common::application.STATUS_ID_DESC'));
                                                $sheet->setCellValue('H' . $start_from ,trans('common::application.COMP_TYPE_DESC'));
                                                $sheet->setCellValue('I' . $start_from ,trans('common::application.REC_TYPE'));
                                                $sheet->setCellValue('J' . $start_from ,trans('common::application.REGISTER_NO'));
                                                $sheet->setCellValue('K' . $start_from ,trans('common::application.START_DATE'));
                                                $sheet->setCellValue('L' . $start_from ,trans('common::application.WORK_CLASS_DESC'));
                                                $sheet->setCellValue('M' . $start_from ,trans('common::application.BRAND_NAME'));
                                                $sheet->setCellValue('N' . $start_from ,trans('common::application.CITY_DESC'));

                                                $z= 2;
                                                foreach($commercial_data as $k => $v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MAIN_IDO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'COMP_NAME'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'REC_CODE'));
                                                    $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'PERSON_TYPE_DESC'));
                                                    $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'IS_VALID_DESC'));
                                                    $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'STATUS_ID_DESC'));
                                                    $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'COMP_TYPE_DESC'));
                                                    $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($v,'REC_TYPE_DESC'));
                                                    $sheet->setCellValue('J'.$z,GovServices::refDashIfNull($v,'REGISTER_NO'));
                                                    $sheet->setCellValue('K'.$z,GovServices::refDashIfNull($v,'START_DATE'));
                                                    $sheet->setCellValue('L'.$z,GovServices::refDashIfNull($v,'WORK_CLASS_DESC'));
                                                    $sheet->setCellValue('M'.$z,GovServices::refDashIfNull($v,'BRAND_NAME'));
                                                    $sheet->setCellValue('N'.$z,GovServices::refDashIfNull($v,'CITY_DESC'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }
                                                $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G' ,'H','I','J','K','L','M' ];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(CaseModel::hasPermission('reports.case.MinistryOfHealth') && sizeof($health) > 0){
                                            $excel->sheet(trans('common::application.health info'), function($sheet) use($health) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:L1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.MAIN_IDO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.MR_CODE'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.MR_PATIENT_CD'));
                                                $sheet->setCellValue('E' . $start_from ,trans('common::application.LOC_NAME_AR'));
                                                $sheet->setCellValue('F' . $start_from ,trans('common::application.DOCTOR_NAME'));
                                                $sheet->setCellValue('G' . $start_from ,trans('common::application.MR_CREATED_ON'));
                                                $sheet->setCellValue('H' . $start_from ,trans('common::application.DREF_NAME_AR'));
                                                $sheet->setCellValue('I' . $start_from ,trans('common::application.MR_DIAGNOSIS_AR'));
                                                $sheet->setCellValue('J' . $start_from ,trans('common::application.MR_DIAGNOSIS_EN'));
                                                $sheet->setCellValue('K' . $start_from ,trans('common::application.MR_COMPLAINT'));
                                                $sheet->setCellValue('L' . $start_from ,trans('common::application.MR_EXAMINATION'));

                                                $z= 2;
                                                foreach($health as $k=>$v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MAIN_IDO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'MR_CODE'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'LOC_NAME_AR'));
                                                    $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'MR_PATIENT_CD'));
                                                    $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'DOCTOR_NAME'));
                                                    $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'MR_CREATED_ON'));
                                                    $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'DREF_NAME_AR'));
                                                    $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($v,'MR_DIAGNOSIS_AR'));
                                                    $sheet->setCellValue('J'.$z,GovServices::refDashIfNull($v,'MR_DIAGNOSIS_EN'));
                                                    $sheet->setCellValue('K'.$z,GovServices::refDashIfNull($v,'MR_COMPLAINT'));
                                                    $sheet->setCellValue('L'.$z,GovServices::refDashIfNull($v,'MR_EXAMINATION'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }

                                                $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G' ,'H','I','J','K','L'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(sizeof($travel) > 0){
                                            $excel->sheet(trans('common::application.travel records'), function($sheet) use($travel) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:E1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.MAIN_IDO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.CTZN_TRANS_DT'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.CTZN_TRANS_TYPE'));
                                                $sheet->setCellValue('E' . $start_from ,trans('common::application.CTZN_TRANS_BORDER'));

                                                $z= 2;
                                                foreach($travel as $k => $v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MAIN_IDO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'CTZN_TRANS_DT'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'CTZN_TRANS_TYPE'));
                                                    $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'CTZN_TRANS_BORDER'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }
                                                $keys = ['A','B','C','D','E'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(CaseModel::hasPermission('reports.case.MinistryOfLabor') && sizeof($reg48_license) > 0){
                                            $excel->sheet(trans('common::application.reg48_license'), function($sheet) use($reg48_license) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:K1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.MAIN_IDO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.WORKER_ID'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.STATUS_NAME'));
                                                $sheet->setCellValue('E' . $start_from ,trans('common::application.DATE_FROM'));
                                                $sheet->setCellValue('F' . $start_from ,trans('common::application.DATE_TO'));
                                                $sheet->setCellValue('G' . $start_from ,trans('common::application.LICENSE_NAME'));
                                                $sheet->setCellValue('H' . $start_from ,trans('common::application.LIC_REC_STATUS_NAME'));
                                                $sheet->setCellValue('I' . $start_from ,trans('common::application.LIC_REC_DATE'));

                                                $z= 2;
                                                foreach($reg48_license as $k => $v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MAIN_IDO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'WORKER_ID'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'STATUS_NAME'));
                                                    $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'DATE_FROM'));
                                                    $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'DATE_TO'));
                                                    $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'LICENSE_NAME'));
                                                    $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'LIC_REC_STATUS_NAME'));
                                                    $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($v,'LIC_REC_DATE'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }
                                                $keys = ['A','B','C','D','E'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(CaseModel::hasPermission('reports.case.MinistryOfSocialDevelopment') && sizeof($social_affairs_receipt) > 0){
                                            $excel->sheet(trans('common::application.social affairs'), function($sheet) use($social_affairs_receipt) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:H1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.MAIN_IDO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.SRV_INF_NAME'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.ORG_NM_MON'));
                                                $sheet->setCellValue('E' . $start_from ,trans('common::application.SRV_TYPE_MAIN_NM'));
                                                $sheet->setCellValue('F' . $start_from ,trans('common::application.CURRENCY'));
                                                $sheet->setCellValue('G' . $start_from ,trans('common::application.RECP_AID_AMOUNT'));
                                                $sheet->setCellValue('H' . $start_from ,trans('common::application.RECP_DELV_DT'));

                                                $z= 2;
                                                foreach($social_affairs_receipt as $k => $v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MAIN_IDO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'SRV_INF_NAME'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'ORG_NM_MON'));
                                                    $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'SRV_TYPE_MAIN_NM'));
                                                    $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'CURRENCY'));
                                                    $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'RECP_AID_AMOUNT'));
                                                    $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'RECP_DELV_DT'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }
                                                $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G' ,'H'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if (CaseModel::hasPermission('reports.case.MinistryOfSocialDevelopment') && sizeof($cases_list) > 0) {
                                            $excel->sheet(trans('common::application.cases_list'), function($sheet) use($cases_list) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:H1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.#'));
                                                $sheet->setCellValue('B1',trans('common::application.MAIN_IDO'));
                                                $sheet->setCellValue('C1',trans('common::application.case_category_type'));
                                                $sheet->setCellValue('D1',trans('common::application.organization'));
                                                $sheet->setCellValue('E1',trans('common::application.status'));
                                                $sheet->setCellValue('F1',trans('common::application.visitor'));
                                                $sheet->setCellValue('G1',trans('common::application.visited_at'));
                                                $sheet->setCellValue('H1',trans('common::application.created_at'));

                                                $z= 2;
                                                foreach($cases_list as $k=>$v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,(is_null($v->MAIN_IDO) ||$v->MAIN_IDO == ' ' ) ? '-' :$v->MAIN_IDO);
                                                    $sheet->setCellValue('C'.$z,(is_null($v->category_name) ||$v->category_name == ' ' ) ? '-' :$v->category_name);
                                                    $sheet->setCellValue('D'.$z,(is_null($v->organization_name) ||$v->organization_name == ' ' ) ? '-' :$v->organization_name);
                                                    $sheet->setCellValue('E'.$z,(is_null($v->status) ||$v->status == ' ' ) ? '-' :$v->status);
                                                    $sheet->setCellValue('F'.$z,(is_null($v->visitor) ||$v->visitor == ' ' ) ? '-' :$v->visitor);
                                                    $sheet->setCellValue('G'.$z,(is_null($v->visited_at) ||$v->visited_at == ' ' ) ? '-' :$v->visited_at);
                                                    $sheet->setCellValue('H'.$z,(is_null($v->created_date) ||$v->created_date == ' ' ) ? '-' :$v->created_date);
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }

                                                $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G'  ,'H' ];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }
                                            });
                                        }

                                        if(sizeof($invalid_cards) > 0){
                                            $excel->sheet(trans('common::application.invalid_cards'), function($sheet) use($invalid_cards) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;
                                                foreach($invalid_cards as $v){
                                                    $sheet->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }

                                            });
                                        }

                                    })->store('xlsx', storage_path('tmp/'));

                                    \Log\Model\Log::saveNewLog('CARD_CKECK', trans('common::application.Some numbers were successfully checked, as the total number') .$total);
                                    if( $total == ( $success +$duplicated)){
                                        return response()->json(['status' => 'success',
                                            'msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.All numbers successfully checked') ,'download_token' => $token]);
                                    }
                                    else{
                                        return response()->json([ 'status' => 'success',
                                            'msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.Some numbers were successfully checked, as the total number') .' :  '. $total . ' ,  '.
                                                trans('common::application.The number of names that have fetched data') .' :  ' .$success . ' ,  ' .
                                                trans('common::application.Number of names not registered in civil registry')  .' :  ' .sizeof($invalid_cards) . ' ,  ' .
                                                trans('common::application.Number of duplicates').' :  '.$duplicated ,
                                            'download_token' => $token ]);
                                    }

                                }
                            }

                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }

    // update citizen using excel sheet according card number
    public function citizenRepositoryUpdateData(Request $request)
    {
        $this->authorize('citizenRepository', \Common\Model\AidsCases::class);

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $father_kinship_id = 1;
        $mother_kinship_id = 5;
        $husband_kinship_id = 29;
        $wife_kinship_id = 21;
        $daughter_kinship_id = 22;
        $son_kinship_id = 2;

        $importFile=$request->file ;
        if ($importFile->isValid()) {
//            \DB::beginTransaction();

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();

            if(Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();

                    if(sizeof($keys) >0 ){
                        $validFile = false;
                        foreach ($keys as $item) {
                            if ( $item == 'rkm_alhoy' ) {
                                $validFile = true;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {

                                $duplicated =0;
                                $success =0;

                                $final_records=[];
                                $invalid_cards=[];
                                $un_register_cards=[];
                                $not_update=[];
                                $processed=[];

                                foreach ($records as $key =>$value) {
                                    $card= (int) $value['rkm_alhoy'];
                                    if(in_array($card,$processed)){
                                        $duplicated++;
                                    }
                                    else{
                                        if(strlen($card)  <= 9) {
                                            if(GovServices::checkCard($card)){
                                                $person_ = Person::where('id_card_number',$card)->first();
                                                if($person_){
                                                    $row = GovServices::byCard($card);
                                                    if($row['status'] == true) {
                                                        $dt = $row['row'];
                                                        $map = GovServices::personDataMap($dt);

                                                        $to_update =array('first_name' , 'second_name' ,'third_name','last_name' ,
                                                            "prev_family_name",'last_name', "marital_status_id", "birthday", "death_date"
                                                        , "family_cnt", "spouses", "male_live", "female_live",);
                                                        foreach ($to_update as $key){
                                                            if(isset($map[$key])){
                                                                $person_->$key = $map[$key];
                                                            }
                                                        }

                                                        $en = ['en_first_name' , 'en_second_name' ,'en_third_name','en_last_name'];
                                                        $per_en=[];
                                                        foreach ($en as $ino){
                                                            if(isset($map[$ino])){
                                                                $per_en[$ino] =$map[$ino];
                                                            }
                                                        }

                                                        if(sizeof($per_en) == 4){
                                                            if(PersonI18n::where(['person_id' => $person_->id,'language_id' => 2])->first()){
                                                                PersonI18n::where(['person_id' => $person_->id,'language_id' => 2])
                                                                    ->update(['first_name'  => $per_en['en_first_name'], 'second_name' => $per_en['en_second_name'],
                                                                        'third_name'  => $per_en['en_third_name'], 'last_name'   => $per_en['en_last_name']
                                                                    ]);
                                                            }else{
                                                                PersonI18n::create(['person_id' => $person_->id,'language_id' => 2,
                                                                    'first_name'  => $per_en['en_first_name'], 'second_name' => $per_en['en_second_name'],
                                                                    'third_name'  => $per_en['en_third_name'], 'last_name'   => $per_en['en_last_name']
                                                                ]);
                                                            }
                                                        }

                                                        $person_->has_commercial_records = 0 ;
                                                        $person_->active_commercial_records = 0 ;
                                                        $person_->gov_commercial_records_details = ' ' ;
                                                        $dt->commercial_data = [] ;
                                                        $CommercialRecords = GovServices::commercialRecords($card);
                                                        if($CommercialRecords['status'] != false){
                                                            $commercial_ =$CommercialRecords['row'];
                                                            $person_->has_commercial_records = 1 ;
                                                            $gov_commercial_records_details = ' ' ;

                                                            foreach ($commercial_ as $ke_ => $v_){
                                                                if($v_->IS_VALID_DESC == "فعال"){
                                                                    $person_->active_commercial_records++;
                                                                }

                                                                $gov_commercial_records_details .= '( ' . $v_->REGISTER_NO  . '   -   ' .
                                                                    $v_->COMP_NAME  . '   -   ' .
                                                                    $v_->START_DATE  . '   -   ' .
                                                                    $v_->IS_VALID_DESC
                                                                    .' )';
                                                            }
                                                            $dt->commercial_data = $commercial_;
                                                            $person_->gov_commercial_records_details = $gov_commercial_records_details;
                                                        }

                                                        foreach ($dt->parent as $km => $prt) {
                                                            $crd = $prt->IDNO_RELATIVE;
                                                            $parentMap = GovServices::inputsMap($prt);
                                                            $parentMap['l_person_id']= $person_->id;
                                                            $parentMap['l_person_update']= true;

                                                            if($prt->SEX_CD == 1){
                                                                $parentMap['kinship_id']= $father_kinship_id;
                                                            }else{
                                                                $parentMap['kinship_id']= $mother_kinship_id;
                                                            }

                                                            $parent = Person::savePerson($parentMap);

                                                            if($prt->SEX_CD == 1){
                                                                $person_->father_id= $parent['id'];
                                                            }else{
                                                                $person_->mother_id= $parent['id'];
                                                            }
                                                            CloneGovernmentPersons::saveNewWithRelation($crd,$prt);
                                                        }

                                                        foreach ($dt->wives as $km => $wif) {
                                                            $crd = $prt->IDNO_RELATIVE;
                                                            $wifeMap = GovServices::inputsMap($wif);
                                                            $wifeMap['l_person_id']= $person_->id;
                                                            $wifeMap['l_person_update']= true;

                                                            if($person_->SEX_CD == 1){
                                                                $wifeMap['kinship_id']= $wife_kinship_id;
                                                            }else{
                                                                $wifeMap['kinship_id']= $husband_kinship_id;
                                                            }
                                                            $wife = Person::savePerson($wifeMap);

                                                            if($person_->SEX_CD == 2){
                                                                $person_->husband_id= $wife['id'];
                                                            }
                                                            CloneGovernmentPersons::saveNewWithRelation($crd,$wif);
                                                        }

                                                        foreach ($dt->childrens as $km => $child) {
                                                            $crd = $prt->IDNO_RELATIVE;
                                                            $childMap = GovServices::inputsMap($child);
                                                            $childMap['l_person_id']= $person_->id;
                                                            $childMap['l_person_update']= true;

                                                            if($child->SEX_CD == 1){
                                                                $childMap['kinship_id']= $son_kinship_id;
                                                            }else{
                                                                $childMap['kinship_id']= $daughter_kinship_id;
                                                            }
                                                            CloneGovernmentPersons::saveNewWithRelation($crd,$child);
                                                            Person::savePerson($childMap);
                                                        }

                                                        CloneGovernmentPersons::saveNew($dt);
                                                        $person_->save();


                                                        $main= (array)$row['row'];
                                                        $final_records[]=$main;
                                                        $success++;

                                                    }else{
                                                        if(!in_array($card,$invalid_cards)){
                                                            $invalid_cards[]=$card;
                                                        }
                                                    }
                                                }
                                                else{

                                                    $row = GovServices::byCard($card);
                                                    if($row['status'] == true) {
                                                        $dt = $row['row'];
                                                        $map = GovServices::personDataMap($dt);

                                                        $map['has_commercial_records']=0;
                                                        $map['active_commercial_records']=0;
                                                        $map['gov_commercial_records_details']=  ' ' ;
                                                        $CommercialRecords = GovServices::commercialRecords($card);
                                                        $dt->commercial_data =[];
                                                        if($CommercialRecords['status'] != false){
                                                            $commercial_ =$CommercialRecords['row'];
                                                            $map['has_commercial_records'] = 1 ;
                                                            $gov_commercial_records_details = ' ' ;

                                                            foreach ($commercial_ as $ke_ => $v_){
                                                                if($v_->IS_VALID_DESC == "فعال"){
                                                                    $map['active_commercial_records']++;
                                                                }

                                                                $gov_commercial_records_details .= '( ' . $v_->REGISTER_NO  . '   -   ' .
                                                                    $v_->COMP_NAME  . '   -   ' .
                                                                    $v_->START_DATE  . '   -   ' .
                                                                    $v_->IS_VALID_DESC
                                                                    .' )';
                                                            }
                                                            $map['gov_commercial_records_details'] = $gov_commercial_records_details;
                                                            $dt->commercial_data =$commercial_;
                                                        }

                                                        $person_ = (Object) Person::insertPerson($map);

                                                        $toUpdate =[];

                                                        foreach ($dt->parent as $km => $prt) {
                                                            $crd = $prt->IDNO_RELATIVE;
                                                            $parentMap = GovServices::inputsMap($prt);
                                                            $parentMap['l_person_id']= $person_->id;
                                                            $parentMap['l_person_update']= true;

                                                            if($prt->SEX_CD == 1){
                                                                $parentMap['kinship_id']= $father_kinship_id;
                                                            }else{
                                                                $parentMap['kinship_id']= $mother_kinship_id;
                                                            }

                                                            $parent = Person::savePerson($parentMap);

                                                            if($prt->SEX_CD == 1){
                                                                $toUpdate['father_id'] = $parent['id'];
                                                            }else{
                                                                $toUpdate['mother_id'] = $parent['id'];
                                                            }
                                                            CloneGovernmentPersons::saveNewWithRelation($crd,$prt);
                                                        }

                                                        foreach ($dt->wives as $km => $wif) {
                                                            $crd = $wif->IDNO_RELATIVE;
                                                            $wifeMap = GovServices::inputsMap($wif);
                                                            $wifeMap['l_person_id']= $person_->id;
                                                            $wifeMap['l_person_update']= true;

                                                            if($dt->SEX_CD == 1){
                                                                $wifeMap['kinship_id']= $wife_kinship_id;
                                                            }else{
                                                                $wifeMap['kinship_id']= $husband_kinship_id;
                                                            }
                                                            $wife = Person::savePerson($wifeMap);

                                                            if($dt->SEX_CD == 2){
                                                                $toUpdate['husband_id']= $wife['id'];
                                                            }
                                                            CloneGovernmentPersons::saveNewWithRelation($crd,$wif);
                                                        }

                                                        foreach ($dt->childrens as $km => $child) {
                                                            $crd = $child->IDNO_RELATIVE;
                                                            $childMap = GovServices::inputsMap($child);
                                                            $childMap['l_person_id']= $person_->id;
                                                            $childMap['l_person_update']= true;

                                                            if($child->SEX_CD == 1){
                                                                $map_['kinship_id']= $son_kinship_id;
                                                            }else{
                                                                $map_['kinship_id']= $daughter_kinship_id;
                                                            }
                                                            Person::savePerson($childMap);
                                                            CloneGovernmentPersons::saveNewWithRelation($crd,$child);
                                                        }

                                                        Person::where('id',$person_['id'])->update($toUpdate);

                                                        $main= (array)$row['row'];
                                                        $final_records[]=$main;
                                                        $success++;

                                                    }else{
                                                        if(!in_array($card,$invalid_cards)){
                                                            $invalid_cards[]=$card;
                                                        }
                                                    }

                                                }
                                            }else{
                                                if(!in_array($card,$invalid_cards)){
                                                    $invalid_cards[]=$card;
                                                }
                                            }
                                        }else{
                                            if(!in_array($card,$invalid_cards)){
                                                $invalid_cards[]=$card;
                                            }
                                        }

                                        $processed[]=$card;
                                    }
                                }

                                if($total == sizeof($invalid_cards)){
                                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.All names entered are not registered in the Civil Registry')  ]);
                                }
                                elseif($total == sizeof($un_register_cards)){
                                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.All names entered are not registered in the system')  ]);
                                }
                                elseif($total == sizeof($not_update)){
                                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.All names entered date has not been updated because it does not exist')  ]);
                                }
                                else{

                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function($excel) use($final_records,$invalid_cards,$processed,$un_register_cards,$not_update) {
                                        $excel->setTitle(trans('common::application.un_register_cards'));
                                        $excel->setDescription(trans('common::application.not_update'));
                                        if(sizeof($final_records) > 0){
                                            $excel->sheet(trans('common::application.family_sheet'), function($sheet) use($final_records) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);
                                                $sheet->getStyle("A1:G1")->applyFromArray(['font' => ['bold' => true]]);

                                                $sheet->setCellValue('A1',trans('common::application.#'));
                                                $sheet->setCellValue('B1',trans('common::application.full_name'));
                                                $sheet->setCellValue('C1',trans('common::application.id_card_number'));
                                                $sheet->setCellValue('D1',trans('common::application.gender'));
                                                $sheet->setCellValue('E1',trans('common::application.marital_status'));
                                                $sheet->setCellValue('F1',trans('common::application.birthday'));
                                                $sheet->setCellValue('G1',trans('common::application.death_date'));

                                                $z= 2;
                                                foreach($final_records as $k=>$main ) {

                                                    $main = (Object) $main;

                                                    $sheet->setCellValue('A' . $z, $k+1);
                                                    $sheet->setCellValue('B' . $z,
                                                        ((is_null($main->FNAME_ARB) || $main->FNAME_ARB == ' ') ? ' ' : $main->FNAME_ARB) . ' ' .
                                                        ((is_null($main->SNAME_ARB) || $main->SNAME_ARB == ' ') ? ' ' : $main->SNAME_ARB) . ' ' .
                                                        ((is_null($main->TNAME_ARB) || $main->TNAME_ARB == ' ') ? ' ' : $main->TNAME_ARB) . ' ' .
                                                        ((is_null($main->LNAME_ARB) || $main->LNAME_ARB == ' ') ? ' ' : $main->LNAME_ARB)
                                                    );
                                                    $sheet->setCellValue('C' . $z, (is_null($main->IDNO) || $main->IDNO == ' ') ? '-' : $main->IDNO);
                                                    $sheet->setCellValue('D' . $z, (is_null($main->SEX) || $main->SEX == ' ') ? '-' : $main->SEX);
                                                    $sheet->setCellValue('E' . $z, (is_null($main->SOCIAL_STATUS) || $main->SOCIAL_STATUS == ' ') ? '-' : $main->SOCIAL_STATUS);
                                                    $sheet->setCellValue('F' . $z, (is_null($main->BIRTH_DT) || $main->BIRTH_DT == ' ') ? '-' : $main->BIRTH_DT);
                                                    $sheet->setCellValue('G' . $z, (is_null($main->DETH_DT) || $main->DETH_DT == ' ') ? '-' : $main->DETH_DT);
                                                    $z++;
                                                }

                                            });
                                        }
                                        if(sizeof($not_update) > 0){
                                            $excel->sheet(trans('common::application.not_update'), function($sheet) use($not_update) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;
                                                foreach($not_update as $v){
                                                    $sheet->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }

                                            });
                                        }
                                        if(sizeof($un_register_cards) > 0){
                                            $excel->sheet(trans('common::application.un_register_cards'), function($sheet) use($un_register_cards) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;
                                                foreach($un_register_cards as $v){
                                                    $sheet->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }

                                            });
                                        }
                                        if(sizeof($invalid_cards) > 0){
                                            $excel->sheet(trans('common::application.invalid_cards'), function($sheet) use($invalid_cards) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;
                                                foreach($invalid_cards as $v){
                                                    $sheet->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }

                                            });
                                        }

                                    })->store('xlsx', storage_path('tmp/'));

                                    \Log\Model\Log::saveNewLog('CARD_CKECK', trans('common::application.The user has checked the identification numbers fetching data from the civil register, the number of records checked') .$total);

                                    if( $total == ( $success +$duplicated)){
                                        return response()->json(['status' => 'success','msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.All numbers successfully checked') ,'download_token' => $token]);
                                    }
                                    else{
                                        return response()->json([ 'status' => 'success',
                                            'msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.Some numbers were successfully checked, as the total number') .' :  '. $total . ' ,  '.
                                                trans('common::application.The number of names that have fetched data') .' :  ' .$success . ' ,  ' .
                                                trans('common::application.Number of names not registered in civil registry')  .' :  ' .sizeof($invalid_cards) . ' ,  ' .
                                                trans('common::application.Number of names with no death history in civil registry') .' :  ' .sizeof($not_update) . ' ,  ' .
                                                trans('common::application.Number of names not registered in the system') .' :  ' .sizeof($un_register_cards) . ' ,  ' .
                                                trans('common::application.Number of duplicates').' :  '.$duplicated ,
                                            'download_token' => $token ]);
                                    }

                                }
                            }

                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }

//            \DB::commit();
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }

    /*********************** generalSearch ***********************/
    public function searchAllFilter(Request $request)
    {

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $user = \Auth::user();
        $records = [];
        $using = $request->using;
        $display = [
            'social' => $request->social,
            'government' => $request->government,
            'commercial' => $request->commercial,
            'health' => $request->health,
            'aids_committee' => $request->aids_committee,
        ];
        if($using == 1){
            $card = $request->id_card_number;
            $row = GovServices::byCard($card,false);
            CardLog::saveCardLog($user->id,$card);
            if($row['status'] != false){
                $records[] = $row['row'];
                foreach ($records as $key=>$record){
                    CloneGovernmentPersons::saveNew($record);
                    $record->social_affairs= trans('common::application.not beneficiary');
                    if($request->social){
                        $socialAffairs=GovServices::socialAffairsStatus($record->IDNO);
                        if($socialAffairs['status'] == true){
                            $record->social_affairs_status=  trans('common::application.beneficiary_');
                        }
                    }

//                    $record->aids_committee= trans('common::application.not beneficiary');
//                    if($request->aids_committee){
//                        $aidsCommittee=GovServices::aidsCommitteeStatus($record->IDNO);
//                        if($aidsCommittee['status'] == true){
//                            $record->aids_committee=  trans('common::application.beneficiary_');
//                        }
//                    }

                    $record->government= trans('common::application.no');
                    if ($request->government == true){
                        if($request->government){
                            $employment_data = GovServices::workDetails($card);
                            if($employment_data['status'] == true){
                                $record->government= trans('common::application.yes');
                            }
                        }
                    }


                    $record->commercial= trans('common::application.not has');
                    if($request->commercial){
                        $commercial_data = GovServices::commercialRecords($card);
                        if($commercial_data['status'] == true){
                            $record->commercial= trans('common::application.has');
                        }
                    }

                    $record->health= trans('common::application.not has');
                    if($request->health){
                        $health_data = GovServices::medicalReports($card);
                        if($health_data['status'] == true){
                            $record->health= trans('common::application.has');
                        }
                    }

                }
            }else{
                return response()->json(['status'=>false,'msg'=>$row['message']]);
            }

        }
        else{
            $row = GovServices::byName($request->all());
            if($row['status'] != false){
                $rows= $row['row'];
                foreach ($rows as $key=>$value){
                    $row = GovServices::byCard($value->IDNO,false);
                    CardLog::saveCardLog($user->id,$value->IDNO);
                    if($row['status'] != false){
                        $record = $row['row'];
                        CloneGovernmentPersons::saveNew($record);
                        $record->social_affairs= trans('common::application.not beneficiary');
                        if($request->social){
                            $socialAffairs=GovServices::socialAffairsStatus($record->IDNO);
                            if($socialAffairs['status'] == true){
                                $record->social_affairs_status=  trans('common::application.beneficiary_');
                            }
                        }

                        $record->aids_committee= trans('common::application.not beneficiary');
                        if($request->aids_committee){
                            $aidsCommittee=GovServices::aidsCommitteeStatus($record->IDNO);
                            if($aidsCommittee['status'] == true){
                                $record->aids_committee=  trans('common::application.beneficiary_');
                            }
                        }

                        $record->government= trans('common::application.no');
                        if ($request->government == true){
                            if($request->government){
                                $employment_data = GovServices::workDetails($record->IDNO);
                                if($employment_data['status'] == true){
                                    $record->government= trans('common::application.yes');
                                }
                            }
                        }


                        $record->commercial= trans('common::application.not has');
                        if($request->commercial){
                            $commercial_data = GovServices::commercialRecords($record->IDNO);
                            if($commercial_data['status'] == true){
                                $record->commercial= trans('common::application.has');
                            }
                        }

                        $record->health= trans('common::application.not has');
                        if($request->health){
                            $health_data = GovServices::medicalReports($record->IDNO);
                            if($health_data['status'] == true){
                                $record->health= trans('common::application.has');
                            }
                        }

                        $records[] = $record;
                    }
                }
            }else{
                return response()->json(['status'=>false,'msg'=>$row['message']]);
            }

        }

        if(sizeof($records) > 0 ){
            return response()->json(['status'=>true,'items'=>$records,'display'=>$display]);
        }

        return response()->json(['status'=>false]);

    }

    // get citizen using card
    public function searchAll(Request $request)
    {

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $this->authorize('citizenRepository', \Common\Model\AidsCases::class);

        $card = $request->id_card_number;

        if($request->action =='xlsx'){
            $person = null;
            $row = GovServices::allByCard($card);
            if($row['status'] == false){
                return response()->json(['status'=>false,'msg'=>$row['message']]);
            }

            $record=$row['row'];
            CloneGovernmentPersons::saveNew($record);

            if (sizeof($record->all_relatives) == 0 ){
                $repeated[] = (Object)[
                    'IDNO_RELATIVE' => '-' ,
                    'RELATIVE_DESC' => '-' ,
                    'FNAME_ARB' => '-' ,
                    'SNAME_ARB' => '-' ,
                    'TNAME_ARB' => '-' ,
                    'LNAME_ARB' => '-' ,
                    'SEX' => '-' ,
                    'SOCIAL_STATUS' => '-' ,
                    'BIRTH_DT' => '-' ,
                    'DETH_DT' => '-' ,
                    'BIRTH_PMAIN' => '-' ,
                    'BIRTH_PSUB' => '-' ,
                    'PREV_LNAME_ARB' => '-' ,
                    'MOTHER_ARB' => '-' ,
                ];
            }else{
                $repeated = $record->all_relatives;
            }

            $token = md5(uniqid());
            \Excel::create('export_' . $token, function($excel) use($card,$record,$repeated) {
                $excel->setTitle(trans('common::application.family_sheet'));
                $excel->setDescription(trans('common::application.family_sheet'));

                $excel->sheet(trans('common::application.family_sheet'), function($sheet) use($card,$record,$repeated) {


                    $full_name=  GovServices::refDashIfNull($record,'FULLNAME');
                    $en_name=  GovServices::refDashIfNull($record,'ENG_NAME');
                    $mother_name=  GovServices::refDashIfNull($record,'MOTHER_ARB');
                    $prev_family_name=  GovServices::refDashIfNull($record,'PREV_LNAME_ARB');

                    $gender=  GovServices::refDashIfNull($record,'SEX');
                    $marital_status=  GovServices::refDashIfNull($record,'SOCIAL_STATUS');
                    $birthday=  GovServices::refDashIfNull($record,'BIRTH_DT');
                    $death_date=  GovServices::refDashIfNull($record,'DETH_DT');
                    $birth_place_main=  GovServices::refDashIfNull($record,'BIRTH_PMAIN');
                    $birth_place_sub=  GovServices::refDashIfNull($record,'BIRTH_PMAIN');

                    $address = $record->CI_REGION .' - ' .$record->CI_CITY  . ' - ' . $record->STREET_ARB;

                    $CTZN_STATUS=  GovServices::refDashIfNull($record,'CTZN_STATUS');
                    $CTZN_TRANS_DT=  GovServices::refDashIfNull($record,'CTZN_TRANS_DT');
                    $VISIT_PURPOSE_DESC=  GovServices::refDashIfNull($record,'VISIT_PURPOSE_DESC');

                    $family_cnt=  GovServices::refDashIfNull($record,'family_cnt');
                    $female_live=  GovServices::refDashIfNull($record,'female_live');
                    $male_live=  GovServices::refDashIfNull($record,'male_live');
                    $spouses=  GovServices::refDashIfNull($record,'spouses');

                    $primary_mobile=  GovServices::refDashIfNull($record,'MOBILE');
                    $phone=  GovServices::refDashIfNull($record,'TEL');

                    $GOV_NAME=  GovServices::refDashIfNull($record,'GOV_NAME');
                    $CITY_NAME=  GovServices::refDashIfNull($record,'CITY_NAME');
                    $PART_NAME=  GovServices::refDashIfNull($record,'PART_NAME');
                    $ADDRESS_DET=  GovServices::refDashIfNull($record,'ADDRESS_DET');

// passport info
                    $PASS_TYPE = $PASSPORT_NO =  $ISSUED_ON = '-';
                    if(!is_null($record->passport)){
                        $PASS_TYPE = (is_null($record->passport->PASS_TYPE) ||$record->passport->PASS_TYPE == ' ' ) ? '-' :$record->passport->PASS_TYPE ;
                        $PASSPORT_NO = (is_null($record->passport->PASSPORT_NO) ||$record->passport->PASSPORT_NO == ' ' ) ? '-' :$record->passport->PASSPORT_NO ;
                        $ISSUED_ON = (is_null($record->passport->ISSUED_ON) ||$record->passport->ISSUED_ON == ' ' ) ? '-' :$record->passport->ISSUED_ON ;
                    }

// health_insurance_info info
                    $CARD_NO = $EXP_DATE =  $INS_STATUS_DESC = $INS_TYPE_DESC = $WORK_SITE_DESC = '-';
                    if(!is_null($record->health_insurance_data)){
                        $CARD_NO = (is_null($record->health_insurance_data->CARD_NO) ||$record->health_insurance_data->CARD_NO == ' ' ) ? '-' : $record->health_insurance_data->CARD_NO ;
                        $EXP_DATE = (is_null($record->health_insurance_data->EXP_DATE) ||$record->health_insurance_data->EXP_DATE == ' ' ) ? '-' : $record->health_insurance_data->EXP_DATE ;
                        $INS_STATUS_DESC = (is_null($record->health_insurance_data->INS_STATUS_DESC) ||$record->health_insurance_data->INS_STATUS_DESC == ' ' ) ? '-' : $record->health_insurance_data->INS_STATUS_DESC ;
                        $INS_TYPE_DESC = (is_null($record->health_insurance_data->INS_TYPE_DESC) ||$record->health_insurance_data->INS_TYPE_DESC == ' ' ) ? '-' : $record->health_insurance_data->INS_TYPE_DESC ;
                        $WORK_SITE_DESC = (is_null($record->health_insurance_data->WORK_SITE_DESC) ||$record->health_insurance_data->WORK_SITE_DESC == ' ' ) ? '-' : $record->health_insurance_data->WORK_SITE_DESC ;
                    }

// social affairs
                    $social_affairs_status = trans('common::application.un_beneficiary');
                    $aid_class = $aid_type = $aid_amount = $aid_source = $aid_periodic = $aid_st_ben_date = $aid_end_ben_date = '-';
                    if(!is_null($record->social_affairs)){
                        $social_affairs_status = GovServices::refDashIfNull($record->social_affairs,'social_affairs_status');
                        $aid_class = GovServices::refDashIfNull($record->social_affairs,'AID_CLASS');
                        $aid_type = GovServices::refDashIfNull($record->social_affairs,'AID_TYPE');
                        $aid_amount = GovServices::refDashIfNull($record->social_affairs,'AID_AMOUNT');
                        $aid_source = GovServices::refDashIfNull($record->social_affairs,'AID_SOURCE');
                        $aid_periodic = GovServices::refDashIfNull($record->social_affairs,'AID_PERIODIC');
                        $aid_st_ben_date = GovServices::refDashIfNull($record->social_affairs,'ST_BENEFIT_DATE');
                        $aid_end_ben_date = GovServices::refDashIfNull($record->social_affairs,'END_BENEFIT_DATE');
                    }

// aids_committee
                    /*
                    $paterfamilias_mobile = $near_mosque = $home_address = $telephone = $affected_by_wars = $current_career =
                    $father_death_reason = $mother_death_reason = $building_type = $home_type =  $furniture_type =
                    $home_status = $home_description = $total_family_income = $health_status = $wife_mobile = '-';

                    if(!is_null($record->aids_committee)){
                        $unsetKeys = ['home_address','near_mosque','paterfamilias_mobile','telephone','current_career','father_death_reason',
                            'mother_death_reason','building_type','home_type','furniture_type','home_status','home_description',
                            'total_family_income','health_status','wife_mobile'];

                        foreach ($unsetKeys as $unsetKey){
                            $$unsetKey =GovServices::refDashIfNull($record->aids_committee,$unsetKey);
                        }

                        $affected_by_wars = ($record->aids_committee->affected_by_wars == 1) ? 'yes' :'no' ;
                    }
                    */

                    $sheet->setRightToLeft(true);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);

                    $sheet->getStyle("A1:V1")->applyFromArray(['font' => ['bold' => true]]);

                    $start_from =1;
                    $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                    $sheet->setCellValue('B' . $start_from ,trans('common::application.id_card_number'));
                    $sheet->setCellValue('C' . $start_from ,trans('common::application.full_name'));
                    $sheet->setCellValue('D' . $start_from ,trans('common::application.en_name'));
                    $sheet->setCellValue('E' . $start_from ,trans('common::application.gender'));
                    $sheet->setCellValue('F' . $start_from ,trans('common::application.marital_status'));
                    $sheet->setCellValue('G' . $start_from ,trans('common::application.birthday'));
                    $sheet->setCellValue('H' . $start_from ,trans('common::application.death_date'));
                    $sheet->setCellValue('I' . $start_from ,trans('common::application.birth_place_main'));
                    $sheet->setCellValue('J' . $start_from ,trans('common::application.birth_place_sub'));
                    $sheet->setCellValue('K' . $start_from ,trans('common::application.mother_name'));
                    $sheet->setCellValue('L' . $start_from ,trans('common::application.prev_family_name'));
                    $sheet->setCellValue('M' . $start_from ,trans('common::application.address'));
                    $sheet->setCellValue('N' . $start_from ,trans('common::application.sso info') .'( ' .trans('common::application.address') .'-' .trans('common::application.GOV_NAME'). ' )');
                    $sheet->setCellValue('O' . $start_from ,trans('common::application.sso info') .'( ' .trans('common::application.address') .'-' .trans('common::application.CITY_NAME'). ' )');
                    $sheet->setCellValue('P' . $start_from ,trans('common::application.sso info') .'( ' .trans('common::application.address') .'-' .trans('common::application.PART_NAME'). ' )');
                    $sheet->setCellValue('Q' . $start_from ,trans('common::application.sso info') .'( ' .trans('common::application.address') .'-' .trans('common::application.ADDRESS_DET'). ' )');
                    $sheet->setCellValue('R' . $start_from ,trans('common::application.mobile'));
                    $sheet->setCellValue('S' . $start_from ,trans('common::application.phone'));
                    $sheet->setCellValue('T' . $start_from ,trans('common::application.family_cnt'));
                    $sheet->setCellValue('U' . $start_from ,trans('common::application.spouses'));
                    $sheet->setCellValue('V' . $start_from ,trans('common::application.male_live'));
                    $sheet->setCellValue('W' . $start_from ,trans('common::application.female_live'));

                    $sheet->setCellValue('x' . $start_from ,trans('common::application.CTZN_STATUS'));
                    $sheet->setCellValue('y' . $start_from ,trans('common::application.CTZN_TRANS_DT'));
                    $sheet->setCellValue('z' . $start_from ,trans('common::application.VISIT_PURPOSE_DESC'));

                    $sheet->setCellValue('AA' . $start_from ,trans('common::application.PASS_TYPE'));
                    $sheet->setCellValue('AB' . $start_from ,trans('common::application.PASSPORT_NO'));
                    $sheet->setCellValue('AC' . $start_from ,trans('common::application.ISSUED_ON'));

                    $sheet->setCellValue('AD' . $start_from ,trans('common::application.CARD_NO'));
                    $sheet->setCellValue('AE' . $start_from ,trans('common::application.EXP_DATE'));
                    $sheet->setCellValue('AF' . $start_from ,trans('common::application.INS_STATUS_DESC'));
                    $sheet->setCellValue('AG' . $start_from ,trans('common::application.INS_TYPE_DESC'));
                    $sheet->setCellValue('AH' . $start_from ,trans('common::application.WORK_SITE_DESC'));

                    $sheet->setCellValue('AI' . $start_from ,trans('common::application.aid_status'));
                    $sheet->setCellValue('AJ' . $start_from ,trans('common::application.AID_CLASS'));
                    $sheet->setCellValue('AK' . $start_from ,trans('common::application.AID_TYPE'));
                    $sheet->setCellValue('AL' . $start_from ,trans('common::application.AID_AMOUNT'));
                    $sheet->setCellValue('AM' . $start_from ,trans('common::application.AID_SOURCE'));
                    $sheet->setCellValue('AN' . $start_from ,trans('common::application.AID_PERIODIC'));
                    $sheet->setCellValue('AO' . $start_from ,trans('common::application.ST_BENEFIT_DATE'));
                    $sheet->setCellValue('AP' . $start_from ,trans('common::application.END_BENEFIT_DATE'));

//                    $sheet->setCellValue('AQ' . $start_from ,trans('common::application.address'));
//                    $sheet->setCellValue('AR' . $start_from ,trans('common::application.near_mosque'));
//                    $sheet->setCellValue('AS' . $start_from ,trans('common::application.mobile'));
//                    $sheet->setCellValue('AT' . $start_from ,trans('common::application.phone'));
//                    $sheet->setCellValue('AU' . $start_from ,trans('common::application.wife_mobile'));
//                    $sheet->setCellValue('AV' . $start_from ,trans('common::application.current_career'));
//                    $sheet->setCellValue('AW' . $start_from ,trans('common::application.father_death_reason'));
//                    $sheet->setCellValue('AX' . $start_from ,trans('common::application.mother_death_reason'));
//                    $sheet->setCellValue('AY' . $start_from ,trans('common::application.building_type'));
//                    $sheet->setCellValue('AZ' . $start_from ,trans('common::application.home_type'));
//                    $sheet->setCellValue('BA' . $start_from ,trans('common::application.furniture_type'));
//                    $sheet->setCellValue('BB' . $start_from ,trans('common::application.home_status'));
//                    $sheet->setCellValue('BV' . $start_from ,trans('common::application.home_description'));
//                    $sheet->setCellValue('BD' . $start_from ,trans('common::application.total_family_income'));
//                    $sheet->setCellValue('BE' . $start_from ,trans('common::application.health_status'));

                    $sheet->setCellValue('AQ' . $start_from ,trans('common::application.id_card_number').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AR' . $start_from ,trans('common::application.kinship_name') .' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AS' . $start_from ,trans('common::application.full_name').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AT' . $start_from ,trans('common::application.gender').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AU' . $start_from ,trans('common::application.marital_status').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AV' . $start_from ,trans('common::application.birthday').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AW' . $start_from ,trans('common::application.death_date').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AX' . $start_from ,trans('common::application.prev_family_name').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AY' . $start_from ,trans('common::application.mother_name') .' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AZ' . $start_from ,trans('common::application.mobile') .' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('BA' . $start_from ,trans('common::application.phone') .' ('.trans('common::application.related') .' )');

                    $z= 2;
                    foreach($repeated as $k=>$v )
                    {

                        $sheet->setCellValue('A'.$z,$k+1);
                        $sheet->setCellValue('B' . $z ,$card);
                        $sheet->setCellValue('C' . $z ,$full_name);
                        $sheet->setCellValue('D' . $z ,$en_name);
                        $sheet->setCellValue('E' . $z ,$gender);
                        $sheet->setCellValue('F' . $z ,$marital_status);
                        $sheet->setCellValue('G' . $z ,$birthday);
                        $sheet->setCellValue('H' . $z ,$death_date);
                        $sheet->setCellValue('I' . $z ,$birth_place_main);
                        $sheet->setCellValue('J' . $z ,$birth_place_sub);
                        $sheet->setCellValue('K' . $z ,$mother_name);
                        $sheet->setCellValue('L' . $z ,$prev_family_name);
                        $sheet->setCellValue('M' . $z ,$address);
                        $sheet->setCellValue('N' . $z ,$GOV_NAME);
                        $sheet->setCellValue('O' . $z ,$CITY_NAME);
                        $sheet->setCellValue('P' . $z ,$PART_NAME);
                        $sheet->setCellValue('Q' . $z ,$ADDRESS_DET);
                        $sheet->setCellValue('R' . $z ,$primary_mobile);
                        $sheet->setCellValue('S' . $z ,$phone);
                        $sheet->setCellValue('T' . $z ,$family_cnt);
                        $sheet->setCellValue('U' . $z ,$spouses);
                        $sheet->setCellValue('V' . $z ,$male_live);
                        $sheet->setCellValue('W' . $z ,$female_live);

                        $sheet->setCellValue('X' . $z ,$CTZN_STATUS);
                        $sheet->setCellValue('Y' . $z ,$CTZN_TRANS_DT);
                        $sheet->setCellValue('Z' . $z ,$VISIT_PURPOSE_DESC);

                        $sheet->setCellValue('AA' . $z ,$PASS_TYPE);
                        $sheet->setCellValue('AB' . $z ,$PASSPORT_NO);
                        $sheet->setCellValue('AC' . $z ,$ISSUED_ON);

                        $sheet->setCellValue('AD' . $z ,$CARD_NO);
                        $sheet->setCellValue('AE' . $z ,$EXP_DATE);
                        $sheet->setCellValue('AF' . $z ,$INS_STATUS_DESC);
                        $sheet->setCellValue('AG' . $z ,$INS_TYPE_DESC);
                        $sheet->setCellValue('AH' . $z ,$WORK_SITE_DESC);
                        $sheet->setCellValue('AI' . $z ,$social_affairs_status);
                        $sheet->setCellValue('AJ' . $z ,$aid_class);
                        $sheet->setCellValue('AK' . $z ,$aid_type);
                        $sheet->setCellValue('AL' . $z ,$aid_amount);
                        $sheet->setCellValue('AM' . $z ,$aid_source);
                        $sheet->setCellValue('AN' . $z ,$aid_periodic);
                        $sheet->setCellValue('AO' . $z ,$aid_st_ben_date);
                        $sheet->setCellValue('AP' . $z ,$aid_end_ben_date);

//                        $sheet->setCellValue('AQ' . $z ,$home_address);
//                        $sheet->setCellValue('AR' . $z ,$near_mosque);
//                        $sheet->setCellValue('AS' . $z ,$paterfamilias_mobile);
//                        $sheet->setCellValue('AT' . $z ,$telephone);
//                        $sheet->setCellValue('AU' . $z ,$wife_mobile);
//                        $sheet->setCellValue('AV' . $z ,$current_career);
//                        $sheet->setCellValue('AW' . $z ,$father_death_reason);
//                        $sheet->setCellValue('AX' . $z ,$mother_death_reason);
//                        $sheet->setCellValue('AY' . $z ,$building_type);
//                        $sheet->setCellValue('AZ' . $z ,$home_type);
//                        $sheet->setCellValue('BA' . $z ,$furniture_type);
//                        $sheet->setCellValue('BB' . $z ,$home_status);
//                        $sheet->setCellValue('BC' . $z ,$home_description);
//                        $sheet->setCellValue('BD' . $z ,$total_family_income);
//                        $sheet->setCellValue('BE' . $z ,$health_status);

                        $sheet->setCellValue('AQ' . $z ,GovServices::refDashIfNull($v,'IDNO_RELATIVE'));
                        $sheet->setCellValue('AR' . $z ,GovServices::refDashIfNull($v,'RELATIVE_DESC'));
                        $sheet->setCellValue('AS' . $z ,GovServices::refDashIfNull($v,'FULLNAME'));
                        $sheet->setCellValue('AT' . $z ,GovServices::refDashIfNull($v,'SEX'));
                        $sheet->setCellValue('AU' . $z ,GovServices::refDashIfNull($v,'SOCIAL_STATUS'));
                        $sheet->setCellValue('AV' . $z ,GovServices::refDashIfNull($v,'BIRTH_DT'));
                        $sheet->setCellValue('AW' . $z ,GovServices::refDashIfNull($v,'DETH_DT'));
                        $sheet->setCellValue('AX' . $z ,GovServices::refDashIfNull($v,'PREV_LNAME_ARB'));
                        $sheet->setCellValue('AY' . $z ,GovServices::refDashIfNull($v,'MOTHER_ARB') );
                        $sheet->setCellValue('AZ' . $z ,GovServices::refDashIfNull($v,'MOBILE') );
                        $sheet->setCellValue('BA' . $z ,GovServices::refDashIfNull($v,'TEL'));
                        $sheet->getRowDimension($z)->setRowHeight(-1);
                        $z++;
                    }

                });

                if(sizeof($record->marriage) > 0){
                    $excel->sheet(trans('common::application.marriage'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:E1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.CONTRACT_NO'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.CONTRACT_DT'));
                        if ($record->SEX_CD == 2){
                            $sheet->setCellValue('D' . $start_from ,trans('common::application.HUSBAND_SSN'));
                            $sheet->setCellValue('E' . $start_from ,trans('common::application.HUSBAND_NAME'));
                        }else{
                            $sheet->setCellValue('D' . $start_from ,trans('common::application.WIFE_SSN'));
                            $sheet->setCellValue('E' . $start_from ,trans('common::application.WIFE_NAME'));
                        }

                        $z= 2;
                        foreach($record->marriage as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'CONTRACT_NO'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'CONTRACT_DT'));

                            if ($record->SEX_CD == 2){
                                $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'HUSBAND_SSN'));
                                $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'HUSBAND_NAME'));
                            }else{
                                $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'WIFE_SSN'));
                                $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'WIFE_NAME'));
                            }

                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C','D','E'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if(sizeof($record->divorce) > 0){
                    $excel->sheet(trans('common::application.divorce'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:F1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.DIV_CERTIFIED_NO'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.CONTRACT_DT'));
                        $sheet->setCellValue('D' . $start_from ,trans('common::application.DIV_TYPE_NAME'));
                        if ($record->SEX_CD == 2){
                            $sheet->setCellValue('E' . $start_from ,trans('common::application.HUSBAND_SSN'));
                            $sheet->setCellValue('F' . $start_from ,trans('common::application.HUSBAND_NAME'));
                        }else{
                            $sheet->setCellValue('E' . $start_from ,trans('common::application.WIFE_SSN'));
                            $sheet->setCellValue('F' . $start_from ,trans('common::application.WIFE_NAME'));
                        }

                        $z= 2;
                        foreach($record->divorce as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'DIV_CERTIFIED_NO'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'CONTRACT_DT'));
                            $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'DIV_TYPE_NAME'));

                            if ($record->SEX_CD == 2){
                                $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'HUSBAND_SSN'));
                                $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'HUSBAND_NAME'));
                            }else{
                                $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'WIFE_SSN'));
                                $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'WIFE_NAME'));
                            }

                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C','D','E','F'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if(sizeof($record->vehicles) > 0){
                    $excel->sheet(trans('common::application.vehicles info'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:C1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.MODEL_YEAR'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.OWNER_TYPE_NAME'));

                        $z= 2;
                        foreach($record->vehicles as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MODEL_YEAR'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'OWNER_TYPE_NAME'));
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if(sizeof($record->employment) > 0){
                    $excel->sheet(trans('common::application.work info'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getStyle("A1:I1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.EMP_DOC_ID'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.MINISTRY_NAME'));
                        $sheet->setCellValue('D' . $start_from ,trans('common::application.JOB_START_DT'));
                        $sheet->setCellValue('E' . $start_from ,trans('common::application.DEGREE_NAME'));
                        $sheet->setCellValue('F' . $start_from ,trans('common::application.EMP_STATE_DESC'));
                        $sheet->setCellValue('G' . $start_from ,trans('common::application.EMP_WORK_STATUS'));
                        $sheet->setCellValue('H' . $start_from ,trans('common::application.JOB_DESC'));
                        $sheet->setCellValue('I' . $start_from ,trans('common::application.MIN_DATA_SOURCE'));

                        $z= 2;
                        foreach($record->employment as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'EMP_DOC_ID'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'MINISTRY_NAME'));
                            $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'JOB_START_DT'));
                            $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'DEGREE_NAME'));
                            $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'EMP_STATE_DESC'));
                            $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'EMP_WORK_STATUS'));
                            $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'JOB_DESC'));
                            $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($v,'MIN_DATA_SOURCE'));
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G' ,'H','I'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if(sizeof($record->properties) > 0){
                    $excel->sheet(trans('common::application.properties info'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:C1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.DOC_NO'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.REAL_OWNER_AREA'));

                        $z= 2;
                        foreach($record->properties as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'DOC_NO'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'REAL_OWNER_AREA'));
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if(sizeof($record->commercial_data) > 0){
                    $excel->sheet(trans('common::application.commercials info'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:M1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.COMP_NAME'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.REC_CODE'));
                        $sheet->setCellValue('D' . $start_from ,trans('common::application.PERSON_TYPE_DESC'));
                        $sheet->setCellValue('E' . $start_from ,trans('common::application.IS_VALID'));
                        $sheet->setCellValue('F' . $start_from ,trans('common::application.STATUS_ID_DESC'));
                        $sheet->setCellValue('G' . $start_from ,trans('common::application.COMP_TYPE_DESC'));
                        $sheet->setCellValue('H' . $start_from ,trans('common::application.REC_TYPE'));
                        $sheet->setCellValue('I' . $start_from ,trans('common::application.REGISTER_NO'));
                        $sheet->setCellValue('J' . $start_from ,trans('common::application.START_DATE'));
                        $sheet->setCellValue('K' . $start_from ,trans('common::application.WORK_CLASS_DESC'));
                        $sheet->setCellValue('L' . $start_from ,trans('common::application.BRAND_NAME'));
                        $sheet->setCellValue('M' . $start_from ,trans('common::application.CITY_DESC'));

                        $z= 2;
                        foreach($record->commercial_data as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'COMP_NAME'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'REC_CODE'));
                            $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'PERSON_TYPE_DESC'));
                            $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'IS_VALID_DESC'));
                            $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'STATUS_ID_DESC'));
                            $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'COMP_TYPE_DESC'));
                            $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'REC_TYPE_DESC'));
                            $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($v,'REGISTER_NO'));
                            $sheet->setCellValue('J'.$z,GovServices::refDashIfNull($v,'START_DATE'));
                            $sheet->setCellValue('K'.$z,GovServices::refDashIfNull($v,'WORK_CLASS_DESC'));
                            $sheet->setCellValue('L'.$z,GovServices::refDashIfNull($v,'BRAND_NAME'));
                            $sheet->setCellValue('M'.$z,GovServices::refDashIfNull($v,'CITY_DESC'));
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G' ,'H','I','J','K','L','M' ];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if(sizeof($record->health) > 0){
                    $excel->sheet(trans('common::application.health info'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:K1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.MR_CODE'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.MR_PATIENT_CD'));
                        $sheet->setCellValue('D' . $start_from ,trans('common::application.LOC_NAME_AR'));
                        $sheet->setCellValue('E' . $start_from ,trans('common::application.DOCTOR_NAME'));
                        $sheet->setCellValue('F' . $start_from ,trans('common::application.MR_CREATED_ON'));
                        $sheet->setCellValue('G' . $start_from ,trans('common::application.DREF_NAME_AR'));
                        $sheet->setCellValue('H' . $start_from ,trans('common::application.MR_DIAGNOSIS_AR'));
                        $sheet->setCellValue('I' . $start_from ,trans('common::application.MR_DIAGNOSIS_EN'));
                        $sheet->setCellValue('J' . $start_from ,trans('common::application.MR_COMPLAINT'));
                        $sheet->setCellValue('K' . $start_from ,trans('common::application.MR_EXAMINATION'));

                        $z= 2;
                        foreach($record->health as $k=>$v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MR_CODE'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'LOC_NAME_AR'));
                            $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'MR_PATIENT_CD'));
                            $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'DOCTOR_NAME'));
                            $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'MR_CREATED_ON'));
                            $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'DREF_NAME_AR'));
                            $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'MR_DIAGNOSIS_AR'));
                            $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($v,'MR_DIAGNOSIS_EN'));
                            $sheet->setCellValue('J'.$z,GovServices::refDashIfNull($v,'MR_COMPLAINT'));
                            $sheet->setCellValue('K'.$z,GovServices::refDashIfNull($v,'MR_EXAMINATION'));
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }

                        $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G' ,'H','I','J','K'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if(sizeof($record->travel) > 0){
                    $excel->sheet(trans('common::application.travel records'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:D1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.CTZN_TRANS_DT'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.CTZN_TRANS_TYPE'));
                        $sheet->setCellValue('D' . $start_from ,trans('common::application.CTZN_TRANS_BORDER'));

                        $z= 2;
                        foreach($record->travel as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'CTZN_TRANS_DT'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'CTZN_TRANS_TYPE'));
                            $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'CTZN_TRANS_BORDER'));
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C','D'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if(sizeof($record->reg48_license) > 0){
                    $excel->sheet(trans('common::application.reg48_license'), function($sheet) use($reg48_license) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:K1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.MAIN_IDO'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.WORKER_ID'));
                        $sheet->setCellValue('D' . $start_from ,trans('common::application.STATUS_NAME'));
                        $sheet->setCellValue('E' . $start_from ,trans('common::application.DATE_FROM'));
                        $sheet->setCellValue('F' . $start_from ,trans('common::application.DATE_TO'));
                        $sheet->setCellValue('G' . $start_from ,trans('common::application.LICENSE_NAME'));
                        $sheet->setCellValue('H' . $start_from ,trans('common::application.LIC_REC_STATUS_NAME'));
                        $sheet->setCellValue('I' . $start_from ,trans('common::application.LIC_REC_DATE'));

                        $z= 2;
                        foreach($reg48_license as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MAIN_IDO'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'WORKER_ID'));
                            $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'STATUS_NAME'));
                            $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'DATE_FROM'));
                            $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'DATE_TO'));
                            $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'LICENSE_NAME'));
                            $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'LIC_REC_STATUS_NAME'));
                            $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($v,'LIC_REC_DATE'));
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C','D','E'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if(sizeof($record->social_affairs_receipt) > 0){
                    $excel->sheet(trans('common::application.social affairs'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:G1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.SRV_INF_NAME'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.ORG_NM_MON'));
                        $sheet->setCellValue('D' . $start_from ,trans('common::application.SRV_TYPE_MAIN_NM'));
                        $sheet->setCellValue('E' . $start_from ,trans('common::application.CURRENCY'));
                        $sheet->setCellValue('F' . $start_from ,trans('common::application.RECP_AID_AMOUNT'));
                        $sheet->setCellValue('G' . $start_from ,trans('common::application.RECP_DELV_DT'));

                        $z= 2;
                        foreach($record->social_affairs_receipt as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'SRV_INF_NAME'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'ORG_NM_MON'));
                            $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'SRV_TYPE_MAIN_NM'));
                            $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'CURRENCY'));
                            $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'RECP_AID_AMOUNT'));
                            $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'RECP_DELV_DT'));
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if (CaseModel::hasPermission('reports.case.RelayCitizenRepository') && sizeof($record->cases_list) > 0) {
                    $excel->sheet(trans('common::application.cases_list'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:G1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->setCellValue('A1',trans('common::application.#'));
                        $sheet->setCellValue('B1',trans('common::application.case_category_type'));
                        $sheet->setCellValue('C1',trans('common::application.organization'));
                        $sheet->setCellValue('D1',trans('common::application.status'));
                        $sheet->setCellValue('E1',trans('common::application.visitor'));
                        $sheet->setCellValue('F1',trans('common::application.visited_at'));
                        $sheet->setCellValue('G1',trans('common::application.created_at'));

                        $z= 2;
                        foreach($record->cases_list as $k=>$v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,(is_null($v->category_name) ||$v->category_name == ' ' ) ? '-' :$v->category_name);
                            $sheet->setCellValue('C'.$z,(is_null($v->organization_name) ||$v->organization_name == ' ' ) ? '-' :$v->organization_name);
                            $sheet->setCellValue('D'.$z,(is_null($v->status) ||$v->status == ' ' ) ? '-' :$v->status);
                            $sheet->setCellValue('E'.$z,(is_null($v->visitor) ||$v->visitor == ' ' ) ? '-' :$v->visitor);
                            $sheet->setCellValue('F'.$z,(is_null($v->visited_at) ||$v->visited_at == ' ' ) ? '-' :$v->visited_at);
                            $sheet->setCellValue('G'.$z,(is_null($v->created_date) ||$v->created_date == ' ' ) ? '-' :$v->created_date);
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }

                        $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G' ];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }
                    });

                }

            })->store('xlsx', storage_path('tmp/'));

            return response()->json(['download_token' => $token , 'dd' =>$record ]);
        }
        elseif($request->action =='pdf') {
            $person = null;
            $row = GovServices::allByCard($card);
            if($row['status'] == false){
                return response()->json(['status'=>false,'msg'=>$row['message']]);
            }

            $record=$row['row'];
            CloneGovernmentPersons::saveNew($record);

            $full_name=  GovServices::refDashIfNull($record,'FULLNAME');
            $en_name=  GovServices::refDashIfNull($record,'ENG_NAME');
            $mother_name=  GovServices::refDashIfNull($record,'MOTHER_ARB');
            $prev_family_name=  GovServices::refDashIfNull($record,'PREV_LNAME_ARB');

            $gender=  GovServices::refDashIfNull($record,'SEX');
            $marital_status=  GovServices::refDashIfNull($record,'SOCIAL_STATUS');
            $birthday=  GovServices::refDashIfNull($record,'BIRTH_DT');
            $death_date=  GovServices::refDashIfNull($record,'DETH_DT');
            $birth_place_main=  GovServices::refDashIfNull($record,'BIRTH_PMAIN');
            $birth_place_sub=  GovServices::refDashIfNull($record,'BIRTH_PMAIN');

            $address = $record->CI_REGION .' - ' .$record->CI_CITY  . ' - ' . $record->STREET_ARB;

            $family_cnt=  GovServices::refDashIfNull($record,'family_cnt');
            $female_live=  GovServices::refDashIfNull($record,'female_live');
            $male_live=  GovServices::refDashIfNull($record,'male_live');
            $spouses=  GovServices::refDashIfNull($record,'spouses');

            $primary_mobile=  GovServices::refDashIfNull($record,'MOBILE');
            $phone=  GovServices::refDashIfNull($record,'TEL');

            $GOV_NAME=  GovServices::refDashIfNull($record,'GOV_NAME');
            $CITY_NAME=  GovServices::refDashIfNull($record,'CITY_NAME');
            $PART_NAME=  GovServices::refDashIfNull($record,'PART_NAME');
            $MAMRK_TYPE_NAME=  GovServices::refDashIfNull($record,'MAMRK_TYPE_NAME');
            $ADDRESS_DET=  GovServices::refDashIfNull($record,'ADDRESS_DET');
            $EMAIL=  GovServices::refDashIfNull($record,'EMAIL');

            $CTZN_STATUS=  GovServices::refDashIfNull($record,'CTZN_STATUS');
            $CTZN_TRANS_DT=  GovServices::refDashIfNull($record,'CTZN_TRANS_DT');
            $VISIT_PURPOSE_DESC=  GovServices::refDashIfNull($record,'VISIT_PURPOSE_DESC');

            $html='';
            PDF::SetTitle($full_name);
            PDF::SetFont('aealarabiya', '', 18);
            PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);
            PDF::SetMargins(10, 10, 10);
            PDF::SetAutoPageBreak(TRUE);
            $lg = Array();
            $lg['a_meta_charset'] = 'UTF-8';
            $lg['a_meta_dir'] = 'rtl';
            $lg['a_meta_language'] = 'fa';
            $lg['w_page'] = 'page';
            PDF::setLanguageArray($lg);
            PDF::setCellHeightRatio(1.5);
            $pageNo = 1;
            $totalPageCount = 1;

            if ((CaseModel::hasPermission('reports.case.MinistryOfTransportation') && sizeof($record->vehicles) > 0 ) ||
                (CaseModel::hasPermission('reports.case.LandAuthority') && sizeof($record->properties) > 0 )||
                (CaseModel::hasPermission('reports.case.MinistryOfJustice') && (sizeof($record->marriage) > 0 || sizeof($record->divorce) > 0) ) ||
                (sizeof($record->parent) > 0) ){
                $totalPageCount ++;
            }

            if (sizeof($record->cases_list) > 0){
//                $count = count($record->employment);
                $totalPageCount ++;
            }
            if (sizeof($record->childrens) > 0 || sizeof($record->wives) > 0){
                $totalPageCount ++;
            }

            if(sizeof($record->social_affairs_receipt) > 0) {
                $count = count($record->social_affairs_receipt);
                $totalPageCount += ceil($count / 25);
            }

            if(sizeof($record->health) > 0) {
                $count = count($record->health);
                $totalPageCount += ceil($count / 4);
            }

            if(sizeof($record->travel) > 0) {
                $count = count($record->travel);
                $totalPageCount += ceil($count / 50);
            }

            if(sizeof($record->reg48_license) > 0) {
                $count = count($record->reg48_license);
                $totalPageCount += ceil($count / 28);
            }

            if(sizeof($record->employment) > 0) {
                $count = count($record->employment);
                $totalPageCount += ceil($count / 25);
            }

            $date = date('Y-m-d');
            $data_ =new Request();

            $token = md5(uniqid());
            $img = base_path('storage/app/emptyUser1.png');
            $fileContents = file_get_contents($record->photo_url);
            $disk = "local";
            $filename = $token.'.png'; // The name you want to give to the downloaded image
            if ($fileContents !== false) {
                Storage::disk($disk)->put('documents/' . $filename, $fileContents);
                $img = storage_path('app/documents/').$token.'.png';
            }

            $html = '<!doctype html><html lang="ar">
                 <head>
                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                 <style>
                     .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                     table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                     * , body {font-weight:normal !important; font-size: 10px;}
                     .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                     .bolder-content{text-align: right !important; vertical-align: middle!important;}
                     .vertical-middle{ vertical-align: middle !important;}
                </style>
                </head>
                 <body style="font-weight: normal !important;">';

            $html.= '<h2 style="text-align:center;"> '.$full_name.'</h2>';
            $html .= '<table border="1" style="border: 1px solid black; box-sizing:border-box;">';

            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.full_name').' </b></td>
                       <td width="330" align="center" >'.$full_name.'</td>
                        <td width="98" align="center" rowspan="6" style="background-color: white" >'
                .'<img src="'.$img.'" alt="" height="140" />'.
                '</td>
                       
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.en_name').' </b></td>
                       <td width="330" align="center" >'.$en_name.'</td>
                    
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.id_card_number').' </b></td>
                       <td width="115" align="center" >'.$card.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.gender').' </b></td>
                       <td width="115" align="center" >'.$gender.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.birthday').' </b></td>
                       <td width="115" align="center" >'.$birthday.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.death_date').' </b></td>
                       <td width="115" align="center" >'.$death_date.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.marital_status').' </b></td>
                       <td width="115" align="center" >'.$marital_status.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.primary_mobile').' </b></td>
                       <td width="115" align="center" >'.$primary_mobile.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.prev_family_name').' </b></td>
                       <td width="115" align="center" >'.$prev_family_name.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.phone').' </b></td>
                       <td width="115" align="center" >'.$phone.'</td>
                   </tr>';

            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.birth_place_main').' </b></td>
                       <td width="115" align="center" >'.$birth_place_main.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.birth_place_sub').' </b></td>
                       <td width="213" align="center" >'.$birth_place_sub.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b>'.trans('common::application.mother_name').' </b></td>
                       <td width="88" align="center" >'.$mother_name.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="55" align="center"><b>'.trans('common::application.family_cnt').' </b></td>
                       <td width="40" align="center" >'.$family_cnt.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="55" align="center"><b>'.trans('common::application.spouses').' </b></td>
                       <td width="40" align="center" >'.$spouses.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="55" align="center"><b>'.trans('common::application.male_live').' </b></td>
                       <td width="40" align="center" >'.$male_live.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="55" align="center"><b>'.trans('common::application.female_live').' </b></td>
                       <td width="40" align="center" >'.$female_live.'</td>
                   </tr>';
            $html .='</table>';

            $html .= '<table border="1" style="border: 1px solid black; box-sizing:border-box;">';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.address').' </b></td>
                       <td width="428" align="center" >'.$address.'</td>
                   </tr>';

            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.CTZN_STATUS').' </b></td>
                       <td width="115" align="center" >'.$CTZN_STATUS.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.CTZN_TRANS_DT').' </b></td>
                       <td width="213" align="center" >'.$CTZN_TRANS_DT.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.VISIT_PURPOSE_DESC').' </b></td>
                       <td width="428" align="center" >'.$VISIT_PURPOSE_DESC.'</td>
                   </tr>';


            $html .='</table>';

            if (CaseModel::hasPermission('reports.case.MinistryOfCommunications')) {
                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="528" align="center"><b>  
                        '.trans('common::application.sso info') .'( ' .trans('common::application.address'). ' )'.'
                        </b></td>
                   </tr>';
                $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b>'.trans('common::application.GOV_NAME').' </b></td>
                       <td width="184" align="center">'.$GOV_NAME.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b>'.trans('common::application.CITY_NAME').' </b></td>
                       <td width="184" align="center">'.$CITY_NAME.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b>'.trans('common::application.PART_NAME').' </b></td>
                       <td width="134" align="center">'.$PART_NAME.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b>'.trans('common::application.MAMRK_TYPE_NAME').' </b></td>
                       <td width="234" align="center">'.$MAMRK_TYPE_NAME.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b>'.trans('common::application.ADDRESS_DET').' </b></td>
                       <td width="134" align="center">'.$ADDRESS_DET.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b>'.trans('common::application.EMAIL').' </b></td>
                       <td width="234" align="center">'.$EMAIL.'</td>
                   </tr>';
                $html .='</table>';
            }

            // passport info
            $PASS_TYPE = $PASSPORT_NO =  $ISSUED_ON = '-';
            if(!is_null($record->passport)){
                $PASS_TYPE = (is_null($record->passport->PASS_TYPE) ||$record->passport->PASS_TYPE == ' ' ) ? '-' :$record->passport->PASS_TYPE ;
                $PASSPORT_NO = (is_null($record->passport->PASSPORT_NO) ||$record->passport->PASSPORT_NO == ' ' ) ? '-' :$record->passport->PASSPORT_NO ;
                $ISSUED_ON = (is_null($record->passport->ISSUED_ON) ||$record->passport->ISSUED_ON == ' ' ) ? '-' :$record->passport->ISSUED_ON ;
            }

            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="528" align="center"><b>  
                        '.trans('common::application.passport info').'
                        </b></td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.PASS_TYPE').'</b></td>
                       <td width="56" align="center">'.$PASS_TYPE.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.PASSPORT_NO').'</b></td>
                       <td width="56" align="center">'.$PASSPORT_NO.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.ISSUED_ON').'</b></td>
                       <td width="176" align="center">'.$ISSUED_ON.'</td>
                   </tr>';
            $html .='</table>';

            // health_insurance_info info
            if (CaseModel::hasPermission('reports.case.MinistryOfHealth')) {
                $CARD_NO = $EXP_DATE =  $INS_STATUS_DESC = $INS_TYPE_DESC = $WORK_SITE_DESC = '-';
                if(!is_null($record->health_insurance_data)){
                    $CARD_NO = (is_null($record->health_insurance_data->CARD_NO) ||$record->health_insurance_data->CARD_NO == ' ' ) ? '-' : $record->health_insurance_data->CARD_NO ;
                    $EXP_DATE = (is_null($record->health_insurance_data->EXP_DATE) ||$record->health_insurance_data->EXP_DATE == ' ' ) ? '-' : $record->health_insurance_data->EXP_DATE ;
                    $INS_STATUS_DESC = (is_null($record->health_insurance_data->INS_STATUS_DESC) ||$record->health_insurance_data->INS_STATUS_DESC == ' ' ) ? '-' : $record->health_insurance_data->INS_STATUS_DESC ;
                    $INS_TYPE_DESC = (is_null($record->health_insurance_data->INS_TYPE_DESC) ||$record->health_insurance_data->INS_TYPE_DESC == ' ' ) ? '-' : $record->health_insurance_data->INS_TYPE_DESC ;
                    $WORK_SITE_DESC = (is_null($record->health_insurance_data->WORK_SITE_DESC) ||$record->health_insurance_data->WORK_SITE_DESC == ' ' ) ? '-' : $record->health_insurance_data->WORK_SITE_DESC ;
                }

                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="528" align="center"><b>  
                        '.trans('common::application.health_insurance_info').'
                        </b></td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.CARD_NO').'</b></td>
                       <td width="96" align="center">'.$CARD_NO.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.EXP_DATE').'</b></td>
                       <td width="96" align="center">'.$EXP_DATE.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.INS_STATUS_DESC').'</b></td>
                       <td width="96" align="center">'.$INS_STATUS_DESC.'</td>
                   </tr>';
                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.INS_TYPE_DESC').'</b></td>
                       <td width="96" align="center">'.$INS_TYPE_DESC.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.WORK_SITE_DESC').'</b></td>
                       <td width="272" align="center">'.$WORK_SITE_DESC.'</td>
                   </tr>';
                $html .='</table>';
            }

            // social affairs
            if (CaseModel::hasPermission('reports.case.MinistryOfSocialDevelopment')){
                $social_affairs_status = trans('common::application.un_beneficiary');
                $aid_class = $aid_type = $aid_amount = $aid_source = $aid_periodic = $aid_st_ben_date = $aid_end_ben_date = '-';
                if(!is_null($record->social_affairs)){
                    $social_affairs_status = GovServices::refDashIfNull($record->social_affairs,'social_affairs_status');
                    $aid_class = GovServices::refDashIfNull($record->social_affairs,'AID_CLASS');
                    $aid_type = GovServices::refDashIfNull($record->social_affairs,'AID_TYPE');
                    $aid_amount = GovServices::refDashIfNull($record->social_affairs,'AID_AMOUNT');
                    $aid_source = GovServices::refDashIfNull($record->social_affairs,'AID_SOURCE');
                    $aid_periodic = GovServices::refDashIfNull($record->social_affairs,'AID_PERIODIC');
                    $aid_st_ben_date = GovServices::refDashIfNull($record->social_affairs,'ST_BENEFIT_DATE');
                    $aid_end_ben_date = GovServices::refDashIfNull($record->social_affairs,'END_BENEFIT_DATE');
                }

                $html .='<div style="height: 5px; color: white"> - </div>';

                $html .= '<table border="1" style="">';

                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="528" align="center"><b>  
                        '.trans('common::application.social affairs').'
                        </b></td>
                   </tr>';

                $html .= ' <tr> 
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_status').'</b></td>
                       <td width="96" align="center">'.$social_affairs_status.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_class').'</b></td>
                       <td width="96" align="center">'.$aid_class.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_type').'</b></td>
                       <td width="96" align="center">'.$aid_type.'</td>
                    </tr>';


                $html .= ' <tr> 
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_amount').'</b></td>
                       <td width="96" align="center">'.$aid_amount.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_source').'</b></td>
                       <td width="96" align="center">'.$aid_source.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_periodic').'</b></td>
                       <td width="96" align="center">'.$aid_periodic.'</td>
                    </tr>';


                $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_st_ben_date').'</b></td>
                       <td width="184" align="center">'.$aid_st_ben_date.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_end_ben_date').'</b></td>
                       <td width="184" align="center">'.$aid_end_ben_date.'</td> 
                   </tr>';
                $html .='</table>';
            }

            /*
            $paterfamilias_mobile = $near_mosque = $home_address = $telephone = $affected_by_wars = $current_career =
            $father_death_reason = $mother_death_reason = $building_type = $home_type =  $furniture_type =
            $home_status = $home_description = $total_family_income = $health_status = $wife_mobile = '-';

            if(!is_null($record->aids_committee)){
                $unsetKeys = ['home_address','near_mosque','paterfamilias_mobile','telephone','current_career','father_death_reason',
                    'mother_death_reason','building_type','home_type','furniture_type','home_status','home_description',
                    'total_family_income','health_status','wife_mobile'];

                foreach ($unsetKeys as $unsetKey){
                    $$unsetKey =GovServices::refDashIfNull($record->aids_committee,$unsetKey);
                }

                $affected_by_wars = ($record->aids_committee->affected_by_wars == 1) ? 'yes' :'no' ;
            }

            $html .='<div style="height: 5px; color: white"> - </div>';

            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="528" align="center"><b>  
                        '.trans('common::application.aids committee data').'
                        </b></td>
                   </tr>';

            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.primary_mobile').'</b></td>
                       <td width="96" align="center">'.$paterfamilias_mobile.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.phone').'</b></td>
                       <td width="96" align="center">'.$telephone.'</td> 
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.wife_mobile').'</b></td>
                       <td width="96" align="center">'.$wife_mobile .'</td>
                   </tr>';

            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.health_status').'</b></td>
                       <td width="96" align="center">'.$health_status.'</td> 
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.current_career').'</b></td>
                       <td width="96" align="center">'.$current_career.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.total_family_income').'</b></td>
                       <td width="96" align="center">'.$total_family_income.'</td> 
                     </tr>';

            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.address').'</b></td>
                       <td width="184" align="center">'.$home_address.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.near_mosque').'</b></td>
                       <td width="184" align="center">'.$near_mosque.'</td> 
                   </tr>';

            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.father_death_reason').'</b></td>
                       <td width="184" align="center">'.$father_death_reason.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.mother_death_reason').'</b></td>
                       <td width="184" align="center">'.$mother_death_reason.'</td> 
                   </tr>';

            $html .= ' <tr>                       
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.affected_by_wars').'</b></td>
                       <td width="96" align="center">'.$affected_by_wars.'</td> 
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.building_type').'</b></td>
                       <td width="96" align="center">'.$building_type.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.home_type').'</b></td>
                       <td width="96" align="center">'.$home_type.'</td> 
                   </tr>';
            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.furniture_type').'</b></td>
                       <td width="184" align="center">'.$furniture_type.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.home_status').'</b></td>
                       <td width="184" align="center">'.$home_status.'</td> 
                   </tr>';

            $html .= ' <tr>
                           <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.home_description').'</b></td>
                           <td width="458" align="center">'.$home_description.'</td>
                       </tr>';

            $html .='</table>';
*/
            $html .='</body></html>';

            PDF::AddPage('P','A4');
            PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
            });
            $pageNo++;
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

            if ((CaseModel::hasPermission('reports.case.MinistryOfTransportation') && sizeof($record->vehicles) > 0 ) ||
                (CaseModel::hasPermission('reports.case.LandAuthority') && sizeof($record->properties) > 0 )||
                (CaseModel::hasPermission('reports.case.MinistryOfJustice') && (sizeof($record->marriage) > 0 || sizeof($record->divorce) > 0) ) ||
                (sizeof($record->parent) > 0) ){

// properties / vehicles / commercials / cases list
                $html = '<!doctype html><html lang="ar">
                 <head>
                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                 <style>
                     .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                     table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                     * , body {font-weight:normal !important; font-size: 10px;}
                     .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                     .bolder-content{text-align: right !important; vertical-align: middle!important;}
                     .vertical-middle{ vertical-align: middle !important;}
                </style>
                </head>
                 <body style="font-weight: normal !important;">';
                $html .='<div style="height: 5px; color: white"> - </div>';

                if ((CaseModel::hasPermission('reports.case.MinistryOfTransportation') && sizeof($record->vehicles) > 0 ) ||
                    (CaseModel::hasPermission('reports.case.LandAuthority') && sizeof($record->properties) > 0 )||
                    (CaseModel::hasPermission('reports.case.MinistryOfJustice') && (sizeof($record->marriage) > 0 || sizeof($record->divorce) > 0) ) ||
                    (sizeof($record->parent) > 0) ){


                }
                // vehicles
                if (CaseModel::hasPermission('reports.case.MinistryOfTransportation')) {
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.vehicles info').'
                        </b></td>
                   </tr>';
                    if (sizeof($record->vehicles) > 0) {
                        $html .= '<tr> 
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="55" align="center"><b> ' . trans('common::application.#') . '</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="240" align="center"><b> ' . trans('common::application.MODEL_YEAR') . '</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="240" align="center"><b> ' . trans('common::application.OWNER_TYPE_NAME') . '</b></td>
                              </tr>';
                    }
                    $z = 1;

                    foreach($record->vehicles as $ke=>$r_) {
                        $MODEL_YEAR = $OWNER_TYPE_NAME  = '-';
                        $unsetKeys = ['MODEL_YEAR','OWNER_TYPE_NAME'];
                        foreach ($unsetKeys as $unsetKey){
                            $$unsetKey =GovServices::refDashIfNull($r_,$unsetKey);
                        }

                        $html .= '<tr> 
                                    <td align="center">'.( $ke + 1 ).'</td>
                                    <td align="center">'.$MODEL_YEAR .'</td>
                                    <td align="center">'.$OWNER_TYPE_NAME.'</td>
                                  </tr>';
                        $z++;
                    }

                    if (sizeof($record->vehicles) == 0){

                        $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
                    }

                    $html .='</table>';
                }

                // properties
                if (CaseModel::hasPermission('reports.case.LandAuthority')) {
                    $html .='<div style="height: 5px; color: white"> - </div>';

                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.properties info').'
                        </b></td>
                   </tr>';
                    if (sizeof($record->properties) > 0){
                        $html .= '<tr> 
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="55" align="center"><b> '.trans('common::application.#'). '</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="240" align="center"><b> '.trans('common::application.DOC_NO').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="240" align="center"><b> '.trans('common::application.REAL_OWNER_AREA').'</b></td>
                              </tr>';
                    }  $z = 1;

                    foreach($record->properties as $ke=>$r_) {
                        $DOC_NO = $REAL_OWNER_AREA  = '-';
                        $unsetKeys = ['DOC_NO','REAL_OWNER_AREA'];
                        foreach ($unsetKeys as $unsetKey){
                            $$unsetKey =GovServices::refDashIfNull($r_,$unsetKey);
                        }

                        $html .= '<tr> 
                                    <td align="center">'.( $ke + 1 ).'</td>
                                    <td align="center">'.$DOC_NO .'</td>
                                    <td align="center">'.$REAL_OWNER_AREA.'</td>
                                  </tr>';
                        $z++;
                    }

                    if (sizeof($record->properties) == 0){
                        $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
                    }
                    $html .='</table>';

                }

                //commercial
                if (CaseModel::hasPermission('reports.case.MinistryOfEconomy')) {

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.commercials info').'
                        </b></td>
                   </tr>';
                    if (sizeof($record->commercial_data) > 0) {
                        $html .= '<tr> 
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.COMP_NAME') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.REC_CODE') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.PERSON_TYPE_DESC') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.IS_VALID') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.STATUS_ID_DESC') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.COMP_TYPE_DESC') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="40" align="center"><b>  ' . trans('common::application.REC_TYPE') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.REGISTER_NO') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.START_DATE') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.WORK_CLASS_DESC') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.BRAND_NAME') . '</b></td>
                            <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b>  ' . trans('common::application.CITY_DESC') . '</b></td>
                      </tr>';
                    }
                    $z = 1;

                    foreach($record->commercial_data as $ke=>$r_) {

                        $COMP_NAME = $REC_CODE = $PERSON_TYPE_DESC  = $IS_VALID_DESC  = $STATUS_ID_DESC  = $COMP_TYPE_DESC  =
                        $REC_TYPE_DESC  = $REGISTER_NO  = $START_DATE  = $WORK_CLASS_DESC  = $BRAND_NAME  = $CITY_DESC  = '-';

                        $unsetKeys = ['COMP_NAME','REC_CODE','PERSON_TYPE_DESC','IS_VALID_DESC','STATUS_ID_DESC',
                            'COMP_TYPE_DESC','REC_TYPE_DESC','REGISTER_NO','START_DATE',
                            'WORK_CLASS_DESC','BRAND_NAME','CITY_DESC'];

                        foreach ($unsetKeys as $unsetKey){
                            $$unsetKey =GovServices::refDashIfNull($r_,$unsetKey);
                        }

                        $html .= '<tr> 
                                    <td align="center">'.$COMP_NAME .'</td>
                                    <td align="center">'.$REC_CODE .'</td>
                                    <td align="center">'.$PERSON_TYPE_DESC .'</td>
                                    <td align="center">'.$IS_VALID_DESC .'</td>
                                    <td align="center">'.$STATUS_ID_DESC .'</td>
                                    <td align="center">'.$COMP_TYPE_DESC.'</td>
                                    <td align="center">'.$REC_TYPE_DESC.'</td>
                                    <td align="center">'.$REGISTER_NO.'</td>
                                    <td align="center">'.$START_DATE.'</td>
                                    <td align="center">'.$WORK_CLASS_DESC.'</td>
                                    <td align="center">'.$BRAND_NAME.'</td>
                                    <td align="center">'.$CITY_DESC.'</td>
                                  </tr>';
                        $z++;
                    }

                    if (sizeof($record->commercial_data) == 0){
                        $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
                    }
                    $html .='</table>';
                }

//                parents
                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';

                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.parents').'
                        </b></td>
                   </tr>';
                if (sizeof($record->parent) > 0) {
                    $html .= ' <tr> 
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> ' . trans('common::application.id_card_number') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="123" align="center"><b> ' . trans('common::application.name') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="74" align="center"><b> ' . trans('common::application.prev_family_name') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> ' . trans('common::application.marital_status') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="64" align="center"><b> ' . trans('common::application.birthday') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="64" align="center"><b> ' . trans('common::application.death_date') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> ' . trans('common::application.mobile') . '</b></td>
                </tr>';
                }
                $z = 1;
//                parents
                foreach($record->parent as $row) {
                    $html .= ' <tr> 
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.$z.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'IDNO_RELATIVE').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="123" align="center"><b> '. GovServices::refDashIfNull($row,'FULLNAME') .'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="74" align="center"><b> '.GovServices::refDashIfNull($row,'PREV_LNAME_ARB').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'SOCIAL_STATUS').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="64" align="center"><b> '.GovServices::refDashIfNull($row,'BIRTH_DT').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="64" align="center"><b> '.GovServices::refDashIfNull($row,'DETH_DT').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="70" align="center"><b> '.GovServices::refDashIfNull($row,'MOBILE').'</b></td>
                    </tr>';
                    $z++;
                }
                if (sizeof($record->parent) == 0){
                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
                }

                $html .='</table>';

//                marriage - divorce
                if (CaseModel::hasPermission('reports.case.MinistryOfJustice')) {
                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';

                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>
                        '.trans('common::application.marriage').'
                        </b></td>
                   </tr>';

                    $z = 1;
                    if (sizeof($record->marriage) > 0) {
                        $html .= ' <tr>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="30" align="center"><b>#</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="95" align="center"><b> ' . trans('common::application.CONTRACT_NO') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="95" align="center"><b> ' . trans('common::application.CONTRACT_DT') . '</b></td>';

                        if ($record->SEX_CD == 2){
                            $html .= '
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> ' . trans('common::application.HUSBAND_SSN') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="215" align="center"><b> ' . trans('common::application.HUSBAND_NAME') . '</b></td>
                    </tr>';
                        }else{
                            $html .= '
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> ' . trans('common::application.WIFE_SSN') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="215" align="center"><b> ' . trans('common::application.WIFE_NAME') . '</b></td>
                </tr>';
                        }
                    }

//                marriage
                    foreach($record->marriage as $row) {
                        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="30" align="center"><b>'.$z.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="95" align="center"><b> '.GovServices::refDashIfNull($row,'CONTRACT_NO').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="95" align="center"><b> '. GovServices::refDashIfNull($row,'CONTRACT_DT') .'</b></td>';

                        if ($record->SEX_CD == 2){
                            $html .= '
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="100" align="center"><b> '.GovServices::refDashIfNull($row,'HUSBAND_SSN').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="215" align="center"><b> '.GovServices::refDashIfNull($row,'HUSBAND_NAME').'</b></td> </tr>';

                        }else{
                            $html .= '
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="100" align="center"><b> '.GovServices::refDashIfNull($row,'WIFE_SSN').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="215" align="center"><b> '.GovServices::refDashIfNull($row,'WIFE_NAME').'</b></td>
                    </tr>';
                        }

                        $z++;
                    }

                    if (sizeof($record->marriage) == 0){
                        $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
                    }
                    $html .='</table>';


//                divorce
                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';

                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>
                        '.trans('common::application.divorce').'
                        </b></td>
                   </tr>';
                    if (sizeof($record->divorce) > 0) {
                        $html .= ' <tr>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="65" align="center"><b> ' . trans('common::application.DIV_CERTIFIED_NO') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="65" align="center"><b> ' . trans('common::application.CONTRACT_DT') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="195" align="center"><b> ' . trans('common::application.DIV_TYPE_NAME') . '</b></td>';

                        if ($record->SEX_CD == 2){
                            $html .= '<td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="65" align="center"><b> ' . trans('common::application.HUSBAND_SSN') . '</b></td>
                              <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="125" align="center"><b> ' . trans('common::application.HUSBAND_NAME') . '</b></td>
                               </tr>';

                        }else{
                            $html .= '
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="65" align="center"><b> ' . trans('common::application.WIFE_SSN') . '</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="125" align="center"><b> ' . trans('common::application.WIFE_NAME') . '</b></td>
                        </tr>';
                        }
                    }
                    $z = 1;

//                divorce
                    foreach($record->divorce as $row) {
                        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.$z.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="65" align="center"><b> '.GovServices::refDashIfNull($row,'DIV_CERTIFIED_NO').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="65" align="center"><b> '. GovServices::refDashIfNull($row,'CONTRACT_DT') .'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="195" align="center"><b> '.GovServices::refDashIfNull($row,'DIV_TYPE_NAME').'</b></td>';

                        if ($record->SEX_CD == 2){
                            $html .= '
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="65" align="center"><b> '.GovServices::refDashIfNull($row,'HUSBAND_SSN').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="125" align="center"><b> '.GovServices::refDashIfNull($row,'HUSBAND_NAME').'</b></td>
                        </tr>';

                        }else{
                            $html .= '
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="65" align="center"><b> '.GovServices::refDashIfNull($row,'WIFE_SSN').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="125" align="center"><b> '.GovServices::refDashIfNull($row,'WIFE_NAME').'</b></td>
                    </tr>';
                        }
                        $z++;
                    }
                    if (sizeof($record->divorce) == 0){
                        $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
                    }
                    $html .='</table>';

                }

                $html .='</body></html>';

                PDF::AddPage('P','A4');
                PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                    $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                    PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
                });
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                $pageNo++;
            }

            if (sizeof($record->wives) > 0 || sizeof($record->childrens) > 0){
                $html = '<!doctype html><html lang="ar">
                 <head>
                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                 <style>
                     .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                     table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                     * , body {font-weight:normal !important; font-size: 10px;}
                     .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                     .bolder-content{text-align: right !important; vertical-align: middle!important;}
                     .vertical-middle{ vertical-align: middle !important;}
                </style>
                </head>
                 <body style="font-weight: normal !important;">';

                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.wives or husbands').'
                        </b></td>
                   </tr>';

                if (sizeof($record->wives) > 0){
                    $html .= ' <tr> 
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="27" align="center"><b>#</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.id_card_number').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="123" align="center"><b> '.trans('common::application.name').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="75" align="center"><b> '.trans('common::application.prev_family_name').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.marital_status').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.birthday').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.death_date').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.mobile').'</b></td>
                             </tr>';
                }

                $z = 1;
                foreach($record->wives as $row) {

                    $html .= ' <tr> 
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="27" align="center"><b>'.$z.'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'IDNO_RELATIVE').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="123" align="center"><b> '. GovServices::refDashIfNull($row,'FULLNAME') .'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="75" align="center"><b> '.GovServices::refDashIfNull($row,'PREV_LNAME_ARB').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'SOCIAL_STATUS').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'BIRTH_DT').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'DETH_DT').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="70" align="center"><b> '.GovServices::refDashIfNull($row,'MOBILE').'</b></td>
                    </tr>';
                    $z++;
                }


                if (sizeof($record->wives) == 0){
                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
                }
                $html .='</table>';

// childs
                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.childs').'
                        </b></td>
                   </tr>';

                if (sizeof($record->childrens) > 0){
                    $html .= ' <tr> 
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="27" align="center"><b>#</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.id_card_number').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.name').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="30" align="center"><b> '.trans('common::application.gender').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="53" align="center"><b> '.trans('common::application.marital_status').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.birthday').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.death_date').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="55" align="center"><b> '.trans('common::application.mother_name').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.mobile').'</b></td>
                        </tr>';
                }

                $z = 1;
                foreach($record->childrens as $row) {
                    $html .= ' <tr> 
                           <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="27" align="center"><b>'.$z.'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'IDNO_RELATIVE').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="120" align="center"><b> '. GovServices::refDashIfNull($row,'FULLNAME') .'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="30" align="center"><b> '.GovServices::refDashIfNull($row,'SEX').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="53" align="center"><b> '.GovServices::refDashIfNull($row,'SOCIAL_STATUS').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'BIRTH_DT').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'DETH_DT').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="55" align="center"><b> '.GovServices::refDashIfNull($row,'MOTHER_ARB').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="70" align="center"><b> '.GovServices::refDashIfNull($row,'MOBILE').'</b></td>
                        </tr>';
                    $z++;
                }
                if (sizeof($record->childrens) == 0){
                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
                }
                $html .='</table>';
                $html .='</body></html>';

                PDF::AddPage('P','A4');
                PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                    $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                    PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
                });
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                $pageNo++;
            }

// cases_list
            if (sizeof($record->cases_list) > 0){
                $html = '<!doctype html><html lang="ar">
                 <head>
                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                 <style>
                     .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                     table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                     * , body {font-weight:normal !important; font-size: 10px;}
                     .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                     .bolder-content{text-align: right !important; vertical-align: middle!important;}
                     .vertical-middle{ vertical-align: middle !important;}
                </style>
                </head>
                 <body style="font-weight: normal !important;">';

                $html .='<div style="height: 5px; color: white"> - </div>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.cases list').'
                        </b></td>
                   </tr>';

                if (sizeof($record->cases_list) > 0){
                    $html .= ' <tr> 
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="75" align="center"><b> '.trans('common::application.case_category_type').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="150" align="center"><b> '.trans('common::application.organization name').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="40" align="center"><b> '.trans('common::application.status').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="89" align="center"><b> '.trans('common::application.visitor').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.visited_at').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.created_at').'</b></td>
                           </tr>';
                }

                $z = 1;
                foreach($record->cases_list as $row) {
                    $html .= ' <tr> 
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.$z.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="75" align="center"><b> '.$row->category_name.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="150" align="center"><b> '.$row->organization_name.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="40" align="center"><b> '.$row->status.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="89" align="center"><b> '.$row->visitor.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="80" align="center"><b> '.$row->visited_at.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="80" align="center"><b> '.$row->created_dt.'</b></td>
                    </tr>';
                    $z++;
                }
                if (sizeof($record->cases_list) == 0){
                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
                }
                $html .='</table>';
                $html .='</body></html>';

                PDF::AddPage('P','A4');
                PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                    $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                    PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
                });
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                $pageNo++;
            }

            // health
            if(CaseModel::hasPermission('reports.case.MinistryOfHealth') && sizeof($record->health) > 0){
                $count = count($record->health);
                $ceil=ceil($count/4);
                for ($x = 0; $x < $ceil; $x++) {
                    $curOffset = ($x) * 4;
                    $index=$curOffset;

                    $html = '<!doctype html><html lang="ar">
                         <head>
                         <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                         <style>
                             .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                             table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                             * , body {font-weight:normal !important; font-size: 10px;}
                             .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                             .bolder-content{text-align: right !important; vertical-align: middle!important;}
                             .vertical-middle{ vertical-align: middle !important;}
                        </style>
                        </head>
                         <body style="font-weight: normal !important;">';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';

                    $html .= ' <tr>
                               <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="534" align="center"><b>  
                                '.trans('common::application.health info').'
                                </b></td>
                           </tr>';


                    $html .= ' <tr> 
                    <td style="height: 40px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="50" align="center"><b> '.trans('common::application.MR_CODE'). '</b></td>
                    <td style="height: 40px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="50" align="center"><b> '.trans('common::application.MR_PATIENT_CD').'</b></td>
                    <td style="height: 40px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="50" align="center"><b> '.trans('common::application.LOC_NAME_AR').'</b></td>
                    <td style="height: 40px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="50" align="center"><b> '.trans('common::application.DOCTOR_NAME').'</b></td>
                    <td style="height: 40px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="60" align="center"><b> '.trans('common::application.MR_CREATED_ON').'</b></td>
                    <td style="height: 40px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="60" align="center"><b> '.trans('common::application.DREF_NAME_AR').'</b></td>
                    <td style="height: 40px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="215" align="center"><b> '.trans('common::application.MR_DIAGNOSIS_AR').'</b></td>
                  </tr>';

                    if($index < $count) {
                        for ($z = 0; $z < 4; $z++) {
                            if($index < $count){
                                $row = $record->health[$index];
                                $html .= ' <tr> 
                                  <td align="center">'.GovServices::refDashIfNull($row,'MR_CODE') .'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'MR_PATIENT_CD').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'LOC_NAME_AR').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'DOCTOR_NAME').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'MR_CREATED_ON').'</td> 
                                   <td align="center">'.GovServices::refDashIfNull($row,'DREF_NAME_AR').'</td> 
                                   <td align="center">'.GovServices::refDashIfNull($row,'MR_DIAGNOSIS_AR').'</td>
                                    </tr>';
                                $index++;
                            }
                        }
                    }
                    $html .='</table>';
                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                        $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                        PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
                    });
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                    $pageNo++;
                }
            }

            // travel
            if(sizeof($record->travel) > 0){
                $count = count($record->travel);
                $ceil=ceil($count/50);
                for ($x = 0; $x < $ceil; $x++) {
                    $curOffset = ($x) * 50;
                    $index=$curOffset;

                    $html = '<!doctype html><html lang="ar">
                         <head>
                         <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                         <style>
                             .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                             table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                             * , body {font-weight:normal !important; font-size: 10px;}
                             .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                             .bolder-content{text-align: right !important; vertical-align: middle!important;}
                             .vertical-middle{ vertical-align: middle !important;}
                        </style>
                        </head>
                         <body style="font-weight: normal !important;">';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';

                    $html .= ' <tr>
                               <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.travel records').'
                                </b></td>
                           </tr>';


                    $html .= '<tr> 
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="55" align="center"><b> '.trans('common::application.#'). '</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="180" align="center"><b> '.trans('common::application.CTZN_TRANS_DT').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="150" align="center"><b> '.trans('common::application.CTZN_TRANS_TYPE').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="150" align="center"><b> '.trans('common::application.CTZN_TRANS_BORDER').'</b></td>
                              </tr>';

                    if($index < $count) {
                        for ($z = 0; $z < 50; $z++) {
                            if($index < $count){
                                $row = $record->travel[$index];
                                $html .= ' <tr> 
                                  <td align="center">'.($index+1).'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'CTZN_TRANS_DT').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'CTZN_TRANS_TYPE').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'CTZN_TRANS_BORDER').'</td>
                                    </tr>';
                                $index++;
                            }
                        }
                    }
                    $html .='</table>';
                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                        $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                        PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
                    });
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                    $pageNo++;
                }
            }

            // reg48_license
            if(CaseModel::hasPermission('reports.case.MinistryOfLabor') && sizeof($record->reg48_license) > 0){
                $count = count($record->reg48_license);
                $ceil=ceil($count/28);
                for ($x = 0; $x < $ceil; $x++) {
                    $curOffset = ($x) * 28;
                    $index=$curOffset;

                    $html = '<!doctype html><html lang="ar">
                         <head>
                         <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                         <style>
                             .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                             table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                             * , body {font-weight:normal !important; font-size: 10px;}
                             .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                             .bolder-content{text-align: right !important; vertical-align: middle!important;}
                             .vertical-middle{ vertical-align: middle !important;}
                        </style>
                        </head>
                         <body style="font-weight: normal !important;">';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';

                    $html .= ' <tr>
                               <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.reg48_license').'
                                </b></td>
                           </tr>';


                    $html .= '<tr> 
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="55" align="center"><b> '.trans('common::application.#'). '</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="180" align="center"><b> '.trans('common::application.WORKER_ID').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="150" align="center"><b> '.trans('common::application.STATUS_NAME').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="150" align="center"><b> '.trans('common::application.DATE_FROM').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="150" align="center"><b> '.trans('common::application.DATE_TO').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="150" align="center"><b> '.trans('common::application.LICENSE_NAME').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="150" align="center"><b> '.trans('common::application.LIC_REC_STATUS_NAME').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="150" align="center"><b> '.trans('common::application.LIC_REC_DATE').'</b></td>
                              </tr>';

                    if($index < $count) {
                        for ($z = 0; $z < 28; $z++) {
                            if($index < $count){
                                $row = $record->reg48_license[$index];
                                $html .= ' <tr> 
                                  <td align="center">'.($index+1).'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'WORKER_ID').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'STATUS_NAME').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'DATE_FROM').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'DATE_TO').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'LICENSE_NAME').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'LIC_REC_STATUS_NAME').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'LIC_REC_DATE').'</td>
                                    </tr>';
                                $index++;
                            }
                        }
                    }
                    $html .='</table>';
                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                        $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                        PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
                    });
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                    $pageNo++;
                }
            }

            // employment
            if(CaseModel::hasPermission('reports.case.MinistryOfFinance') && sizeof($record->employment) > 0){
                $count = count($record->employment);
                $ceil=ceil($count/25);
                for ($x = 0; $x < $ceil; $x++) {
                    $curOffset = ($x) * 25;
                    $index=$curOffset;

                    $html = '<!doctype html><html lang="ar">
                         <head>
                         <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                         <style>
                             .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                             table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                             * , body {font-weight:normal !important; font-size: 10px;}
                             .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                             .bolder-content{text-align: right !important; vertical-align: middle!important;}
                             .vertical-middle{ vertical-align: middle !important;}
                        </style>
                        </head>
                         <body style="font-weight: normal !important;">';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';

                    $html .= ' <tr>
                               <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.work info').'
                                </b></td>
                           </tr>';


                    $html .= '<tr> 
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="20" align="center"><b> '.trans('common::application.#'). '</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="55" align="center"><b> '.trans('common::application.EMP_DOC_ID').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="100" align="center"><b> '.trans('common::application.MINISTRY_NAME').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="75" align="center"><b> '.trans('common::application.JOB_START_DT').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b> '.trans('common::application.DEGREE_NAME').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b> '.trans('common::application.EMP_STATE_DESC').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="45" align="center"><b> '.trans('common::application.EMP_WORK_STATUS').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="75" align="center"><b> '.trans('common::application.JOB_DESC').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000; vertical-align: middle" width="75" align="center"><b> '.trans('common::application.MIN_DATA_SOURCE').'</b></td>
                              </tr>';

                    if($index < $count) {
                        for ($z = 0; $z < 25; $z++) {
                            if($index < $count){
                                $row = $record->employment[$index];
                                $html .= ' <tr> 
                                  <td align="center">'.($index+1).'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'EMP_DOC_ID').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'MINISTRY_NAME').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'JOB_START_DT').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'DEGREE_NAME').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'EMP_STATE_DESC').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'EMP_WORK_STATUS').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'JOB_DESC').'</td>
                                   <td align="center">'.GovServices::refDashIfNull($row,'MIN_DATA_SOURCE').'</td>
                                    </tr>';
                                $index++;
                            }
                        }
                    }
                    $html .='</table>';
                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                        $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                        PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
                    });
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                    $pageNo++;
                }
            }

            // social_affairs_receipt
            if(CaseModel::hasPermission('reports.case.MinistryOfSocialDevelopment') && sizeof($record->social_affairs_receipt) > 0){
                $count = count($record->social_affairs_receipt);
                $ceil=ceil($count/25);
                for ($x = 0; $x < $ceil; $x++) {
                    $curOffset = ($x) * 25;
                    $index=$curOffset;

                    $html = '<!doctype html><html lang="ar">
                         <head>
                         <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                         <style>
                             .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                             table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                             * , body {font-weight:normal !important; font-size: 10px;}
                             .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                             .bolder-content{text-align: right !important; vertical-align: middle!important;}
                             .vertical-middle{ vertical-align: middle !important;}
                        </style>
                        </head>
                         <body style="font-weight: normal !important;">';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';

                    $html .= ' <tr>
                               <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="534" align="center"><b>  
                                '.trans('common::application.social affairs').'
                                </b></td>
                           </tr>';


                    $html .= ' <tr> 
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="146" align="center"><b> '.trans('common::application.SRV_INF_NAME').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="145" align="center"><b> '.trans('common::application.ORG_NM_MON').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="54" align="center"><b> '.trans('common::application.SRV_TYPE_MAIN_NM').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="54" align="center"><b> '.trans('common::application.CURRENCY').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="50" align="center"><b> '.trans('common::application.RECP_AID_AMOUNT').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="65" align="center"><b> '.trans('common::application.RECP_DELV_DT').'</b></td>
                    </tr>';

                    if($index < $count) {
                        for ($z = 0; $z < 25; $z++) {
                            if($index < $count){
                                $row = $record->social_affairs_receipt[$index];
                                $html .= ' <tr> 
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.($index+1).'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="146" align="center"><b> '.GovServices::refDashIfNull($row,'SRV_INF_NAME').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="145" align="center"><b> '.GovServices::refDashIfNull($row,'ORG_NM_MON').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="54" align="center"><b> '.GovServices::refDashIfNull($row,'SRV_TYPE_MAIN_NM').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="54" align="center"><b> '.GovServices::refDashIfNull($row,'CURRENCY').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="50" align="center"><b> '.GovServices::refDashIfNull($row,'RECP_AID_AMOUNT').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="65" align="center"><b> '.GovServices::refDashIfNull($row,'RECP_DELV_DT').'</b></td>
                                </tr>';
                                $index++;
                            }
                        }
                    }
                    $html .='</table>';
                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                        $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                        PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
                    });
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                    $pageNo++;

                }
            }

            $dirName = base_path('storage/app/citizen_pdf');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }

            PDF::Output($dirName.'/'.$full_name . '.pdf', 'F');

            unlink(storage_path('app/documents/'.$token.'.png'));
            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');

            $zip = new \ZipArchive;
            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                foreach ( glob( $dirName . '/*' ) as $fileName )
                {
                    $file = basename( $fileName );
                    $zip->addFile( $fileName, $file );
                }

                $zip->close();
                array_map('unlink', glob("$dirName/*.*"));
                rmdir($dirName);
            }

            return response()->json(['download_token' => $token]);

        }

        $person = null;
        $row = GovServices::byCard($card);
        if($row['status'] == false){
            return response()->json(['status'=>false,'msg'=>$row['message']]);
        }

        $record=$row['row'];

        $passport_data = GovServices::passportInfo($card);
        $record->passport=null;
        if($passport_data['status'] != false){
            if(isset($passport_data['row']->ISSUED_ON)){
                $passport_data['row']->ISSUED_ON = date('d/m/Y',strtotime($passport_data['row']->ISSUED_ON));
            }
            $record->passport=$passport_data['row'];
        }

        $travel_data = GovServices::travelRecords($card);
        $record->travel=[];
        if($travel_data['status'] != false){
            $record->travel=$travel_data['row'];
        }

        $record->CASE_STATUS=false;
        $record->cases_list=[];
        $record->person_id = Person::getPersonId([$card]);
        if (CaseModel::hasPermission('reports.case.RelayCitizenRepository')) {
            $record->cases_list = Person::getPersonCardCases([$card]);
        }

        CloneGovernmentPersons::saveNew($record);
        return response()->json([ 'status'=>true, 'data'=>$record ]);
    }

    // check citizen using excel sheet according card number
    public function searchAllCheck(Request $request)
    {
        $this->authorize('citizenRepository', \Common\Model\AidsCases::class);
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $type = $request->type;
        $validFile=true;
        $duplicated =0;
        $success =0;

        $final_records=[];
        $invalid_cards=[];
        $processed=[];

        $vehicles=[];
        $reg48_license=[];
        $properties=[];
        $marriage=[];
        $divorce=[];
        $travel=[];
        $employment=[];
        $health=[];
        $commercial_data=[];
        $social_affairs_receipt=[];
        $cases_list=[];

        $importFile=$request->file ;
        if ($importFile->isValid()) {
            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){

                        $validFile = false;
                        foreach ($keys as $item) {
                            if ( $item == 'rkm_alhoy' ) {
                                $validFile = true;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);
                            $health = [];

                            if ($total > 0) {
                                foreach ($records as $key =>$value) {
                                    if(in_array($value['rkm_alhoy'],$processed)){
                                        $duplicated++;
                                    }
                                    else{
                                        $card= (int) $value['rkm_alhoy'];
                                        if(strlen(($card))  <= 9) {
                                            if(GovServices::checkCard($card)){
                                                if ($type == 2){
                                                    $row = GovServices::allByCard($card);
                                                }else{
                                                    $pre_row = GovServices::byCard($card);
                                                    $row = GovServices::setMain($pre_row['row'],$card);
                                                }
                                                if($row['status'] == true) {
                                                    $temp = $row['row'];

                                                    $main=(array) $temp;
                                                    $main['related']= null;

                                                    if($type == 2){
                                                        if (sizeof($main['all_relatives']) > 0){
                                                            $rel_keys = ['parent','childrens','wives'];
                                                            foreach ($rel_keys as $key){
                                                                foreach ($temp->$key as $k=>$v){
                                                                    if ($key == 'childrens'){
                                                                        if ($temp->SEX_CD == 1){
                                                                            $v->FATHER_IDNO = $request->id_card_number;
                                                                        }else{
                                                                            $v->MOTHER_IDNO = $request->id_card_number;
                                                                        }
                                                                    }
                                                                    $v->IDNO = $v->IDNO_RELATIVE;
                                                                    CloneGovernmentPersons::saveNewWithRelation($card,$v);
                                                                    $main['related']= $v;
                                                                    $final_records[] = $main;
                                                                }
                                                            }
                                                        }
                                                        else{
                                                            $main['related']= null;
                                                            $final_records[]=$main;
                                                        }

                                                        $ref_keys = ['vehicles','reg48_license','properties','commercial_data','cases_list',
                                                            'health','travel','employment','social_affairs_receipt'];

                                                        foreach ($ref_keys as $ref_key){
                                                            $sub_array = $temp->$ref_key;
                                                            if (sizeof($sub_array) > 0 ){
                                                                $main_array = $$ref_key;
                                                                $append_arr = GovServices::pushToArray($main_array,$sub_array,$card);
                                                                $$ref_key = $append_arr;
                                                            }
                                                        }

                                                        $ref_keys = ['marriage','divorce'];

                                                        foreach ($ref_keys as $ref_key){
                                                            $sub_array = $temp->marriage_divorce[$ref_key];
                                                            if (sizeof($sub_array) > 0 ){
                                                                $main_array = $$ref_key;
                                                                $append_arr = GovServices::pushToArray($main_array,$sub_array,$card);
                                                                $$ref_key = $append_arr;
                                                            }
                                                        }
                                                        CloneGovernmentPersons::saveNew((Object) $main);

                                                    }
                                                    else if($type == 3){
                                                        CloneGovernmentPersons::saveNew($temp);
                                                        if (sizeof($main['wives']) > 0){
                                                            foreach ($temp->wives as $k=>$v){
                                                                $main['related']= $v;
                                                                $final_records[] = $main;
                                                            }
                                                        }else{
                                                            $main['related']= null;
                                                            $final_records[]=$main;
                                                        }
                                                    }
                                                    else if($type == 4){
                                                        CloneGovernmentPersons::saveNew($temp);
                                                        if (sizeof($temp->childrens) > 0){
                                                            foreach ($temp->childrens as $k=>$v){
                                                                if ($temp->SEX_CD == 1){
                                                                    $v->FATHER_IDNO = $request->id_card_number;
                                                                }else{
                                                                    $v->MOTHER_IDNO = $request->id_card_number;
                                                                }
                                                                $v->IDNO = $v->IDNO_RELATIVE;
                                                                CloneGovernmentPersons::saveNewWithRelation($card,$v);
                                                                $main['related']= $v;
                                                                $final_records[] = $main;
                                                            }
                                                        }else{
                                                            $main['related']= null;
                                                            $final_records[]=$main;
                                                        }

                                                    }
                                                    else{
                                                        CloneGovernmentPersons::saveNew($temp);
                                                        $final_records[] = $main;
                                                    }
                                                    $processed[]=$value['rkm_alhoy'];
                                                    $success++;
                                                }else{
                                                    $invalid_cards[]=$value['rkm_alhoy'];
                                                }
                                            }else{
                                                $invalid_cards[]=$value['rkm_alhoy'];
                                            }

                                        }else{
                                            $invalid_cards[]=$value['rkm_alhoy'];
                                        }
                                    }
                                }

                                if($total == sizeof($invalid_cards)){
                                    return response()->json(['status' => 'failed' ,
                                        'error_type' =>' ' , 'msg' =>trans('common::application.All names entered are not registered in the Civil Registry')  ]);
                                }else{

                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function($excel) use($final_records,$invalid_cards,$vehicles, $reg48_license,
                                        $properties, $marriage, $divorce, $travel, $employment, $health,
                                        $commercial_data, $social_affairs_receipt, $cases_list,$type) {
                                        $excel->setTitle(trans('common::application.family_sheet'));
                                        $excel->setDescription(trans('common::application.family_sheet'));

                                        $excel->sheet(trans('common::application.family_sheet'), function($sheet) use($final_records,$type) {

                                            $sheet->setRightToLeft(true);
                                            $sheet->setAllBorders('thin');
                                            $sheet->setfitToWidth(true);
                                            $sheet->setHeight(1,40);
                                            $sheet->getDefaultStyle()->applyFromArray([
                                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                            ]);

                                            $sheet->getStyle("A1:V1")->applyFromArray(['font' => ['bold' => true]]);

                                            $start_from =1;

                                            $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                            $sheet->setCellValue('B' . $start_from ,trans('common::application.id_card_number'));
                                            $sheet->setCellValue('C' . $start_from ,trans('common::application.full_name'));
                                            $sheet->setCellValue('D' . $start_from ,trans('common::application.en_name'));
                                            $sheet->setCellValue('E' . $start_from ,trans('common::application.gender'));
                                            $sheet->setCellValue('F' . $start_from ,trans('common::application.marital_status'));
                                            $sheet->setCellValue('G' . $start_from ,trans('common::application.birthday'));
                                            $sheet->setCellValue('H' . $start_from ,trans('common::application.death_date'));
                                            $sheet->setCellValue('I' . $start_from ,trans('common::application.birth_place_main'));
                                            $sheet->setCellValue('J' . $start_from ,trans('common::application.birth_place_sub'));
                                            $sheet->setCellValue('K' . $start_from ,trans('common::application.mother_name'));
                                            $sheet->setCellValue('L' . $start_from ,trans('common::application.prev_family_name'));
                                            $sheet->setCellValue('M' . $start_from ,trans('common::application.address'));
                                            $sheet->setCellValue('N' . $start_from ,trans('common::application.sso info') .'( ' .trans('common::application.address') .'-' .trans('common::application.GOV_NAME'). ' )');
                                            $sheet->setCellValue('O' . $start_from ,trans('common::application.sso info') .'( ' .trans('common::application.address') .'-' .trans('common::application.CITY_NAME'). ' )');
                                            $sheet->setCellValue('P' . $start_from ,trans('common::application.sso info') .'( ' .trans('common::application.address') .'-' .trans('common::application.PART_NAME'). ' )');
                                            $sheet->setCellValue('Q' . $start_from ,trans('common::application.sso info') .'( ' .trans('common::application.address') .'-' .trans('common::application.ADDRESS_DET'). ' )');
                                            $sheet->setCellValue('R' . $start_from ,trans('common::application.mobile'));
                                            $sheet->setCellValue('S' . $start_from ,trans('common::application.phone'));
                                            $sheet->setCellValue('T' . $start_from ,trans('common::application.family_cnt'));
                                            $sheet->setCellValue('U' . $start_from ,trans('common::application.spouses'));
                                            $sheet->setCellValue('V' . $start_from ,trans('common::application.male_live'));
                                            $sheet->setCellValue('W' . $start_from ,trans('common::application.female_live'));

                                            $sheet->setCellValue('x' . $start_from ,trans('common::application.CTZN_STATUS'));
                                            $sheet->setCellValue('y' . $start_from ,trans('common::application.CTZN_TRANS_DT'));
                                            $sheet->setCellValue('z' . $start_from ,trans('common::application.VISIT_PURPOSE_DESC'));

                                            $sheet->setCellValue('AA' . $start_from ,trans('common::application.PASS_TYPE'));
                                            $sheet->setCellValue('AB' . $start_from ,trans('common::application.PASSPORT_NO'));
                                            $sheet->setCellValue('AC' . $start_from ,trans('common::application.ISSUED_ON'));

                                            $sheet->setCellValue('AD' . $start_from ,trans('common::application.CARD_NO'));
                                            $sheet->setCellValue('AE' . $start_from ,trans('common::application.EXP_DATE'));
                                            $sheet->setCellValue('AF' . $start_from ,trans('common::application.INS_STATUS_DESC'));
                                            $sheet->setCellValue('AG' . $start_from ,trans('common::application.INS_TYPE_DESC'));
                                            $sheet->setCellValue('AH' . $start_from ,trans('common::application.WORK_SITE_DESC'));

                                            $sheet->setCellValue('AI' . $start_from ,trans('common::application.aid_status'));
                                            $sheet->setCellValue('AJ' . $start_from ,trans('common::application.AID_CLASS'));
                                            $sheet->setCellValue('AK' . $start_from ,trans('common::application.AID_TYPE'));
                                            $sheet->setCellValue('AL' . $start_from ,trans('common::application.AID_AMOUNT'));
                                            $sheet->setCellValue('AM' . $start_from ,trans('common::application.AID_SOURCE'));
                                            $sheet->setCellValue('AN' . $start_from ,trans('common::application.AID_PERIODIC'));
                                            $sheet->setCellValue('AO' . $start_from ,trans('common::application.ST_BENEFIT_DATE'));
                                            $sheet->setCellValue('AP' . $start_from ,trans('common::application.END_BENEFIT_DATE'));

//                                            $sheet->setCellValue('AQ' . $start_from ,trans('common::application.address'));
//                                            $sheet->setCellValue('AR' . $start_from ,trans('common::application.near_mosque'));
//                                            $sheet->setCellValue('AS' . $start_from ,trans('common::application.mobile'));
//                                            $sheet->setCellValue('AT' . $start_from ,trans('common::application.phone'));
//                                            $sheet->setCellValue('AU' . $start_from ,trans('common::application.wife_mobile'));
//                                            $sheet->setCellValue('AV' . $start_from ,trans('common::application.current_career'));
//                                            $sheet->setCellValue('AW' . $start_from ,trans('common::application.father_death_reason'));
//                                            $sheet->setCellValue('AX' . $start_from ,trans('common::application.mother_death_reason'));
//                                            $sheet->setCellValue('AY' . $start_from ,trans('common::application.building_type'));
//                                            $sheet->setCellValue('AZ' . $start_from ,trans('common::application.home_type'));
//                                            $sheet->setCellValue('BA' . $start_from ,trans('common::application.furniture_type'));
//                                            $sheet->setCellValue('BB' . $start_from ,trans('common::application.home_status'));
//                                            $sheet->setCellValue('BV' . $start_from ,trans('common::application.home_description'));
//                                            $sheet->setCellValue('BD' . $start_from ,trans('common::application.total_family_income'));
//                                            $sheet->setCellValue('BE' . $start_from ,trans('common::application.health_status'));

                                            if($type != 1){
                                                $sheet->setCellValue('AQ' . $start_from ,trans('common::application.id_card_number').' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('AR' . $start_from ,trans('common::application.kinship_name') .' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('AS' . $start_from ,trans('common::application.full_name').' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('AT' . $start_from ,trans('common::application.gender').' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('AU' . $start_from ,trans('common::application.marital_status').' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('AV' . $start_from ,trans('common::application.birthday').' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('AW' . $start_from ,trans('common::application.death_date').' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('AX' . $start_from ,trans('common::application.prev_family_name').' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('AY' . $start_from ,trans('common::application.mother_name') .' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('AZ' . $start_from ,trans('common::application.mobile') .' ('.trans('common::application.related') .' )');
                                                $sheet->setCellValue('BA' . $start_from ,trans('common::application.phone') .' ('.trans('common::application.related') .' )');
                                            }

                                            $z= 2;
                                            foreach($final_records as $k=>$main )
                                            {
                                                $record = (Object) $main;
                                                $v = (Object) $record->related;

                                                $sheet->setCellValue('A'.$z,$k+1);
                                                $sheet->setCellValue('B' . $z ,GovServices::refDashIfNull($record,'IDNO'));
                                                $sheet->setCellValue('C' . $z ,GovServices::refDashIfNull($record,'FULLNAME'));
                                                $sheet->setCellValue('D' . $z ,GovServices::refDashIfNull($record,'ENG_NAME'));
                                                $sheet->setCellValue('E' . $z ,GovServices::refDashIfNull($record,'SEX'));
                                                $sheet->setCellValue('F' . $z ,GovServices::refDashIfNull($record,'SOCIAL_STATUS'));
                                                $sheet->setCellValue('G' . $z ,GovServices::refDashIfNull($record,'BIRTH_DT'));
                                                $sheet->setCellValue('H' . $z ,GovServices::refDashIfNull($record,'DETH_DT'));
                                                $sheet->setCellValue('I' . $z ,GovServices::refDashIfNull($record,'BIRTH_PMAIN'));
                                                $sheet->setCellValue('J' . $z ,GovServices::refDashIfNull($record,'BIRTH_PMAIN'));

                                                $sheet->setCellValue('K' . $z ,GovServices::refDashIfNull($record,'MOTHER_ARB'));
                                                $sheet->setCellValue('L' . $z ,GovServices::refDashIfNull($record,'PREV_LNAME_ARB'));
                                                $sheet->setCellValue('M' . $z ,( (is_null($record->CI_REGION) ||$record->CI_REGION == ' ' ) ? ' ' :$record->CI_REGION) .' '.
                                                    ( (is_null($record->CI_CITY) ||$record->CI_CITY == ' ' ) ? ' ' :$record->CI_CITY) .' '.
                                                    ( (is_null($record->STREET_ARB) ||$record->STREET_ARB == ' ' ) ? ' ' :$record->STREET_ARB));

                                                $sheet->setCellValue('N' . $z ,GovServices::refDashIfNull($record,'GOV_NAME'));
                                                $sheet->setCellValue('O' . $z ,GovServices::refDashIfNull($record,'CITY_NAME'));
                                                $sheet->setCellValue('P' . $z ,GovServices::refDashIfNull($record,'PART_NAME'));
                                                $sheet->setCellValue('Q' . $z ,GovServices::refDashIfNull($record,'ADDRESS_DET'));
                                                $sheet->setCellValue('R' . $z ,GovServices::refDashIfNull($record,'MOBILE'));
                                                $sheet->setCellValue('S' . $z ,GovServices::refDashIfNull($v,'TEL'));
                                                $sheet->setCellValue('T' . $z ,GovServices::refDashIfNull($record,'family_cnt'));
                                                $sheet->setCellValue('U' . $z ,GovServices::refDashIfNull($record,'spouses'));
                                                $sheet->setCellValue('V' . $z ,GovServices::refDashIfNull($record,'male_live'));
                                                $sheet->setCellValue('W' . $z ,GovServices::refDashIfNull($record,'female_live'));

                                                $sheet->setCellValue('X' . $z ,GovServices::refDashIfNull($record,'CTZN_STATUS'));
                                                $sheet->setCellValue('Y' . $z ,GovServices::refDashIfNull($record,'CTZN_TRANS_DT'));
                                                $sheet->setCellValue('Z' . $z ,GovServices::refDashIfNull($record,'VISIT_PURPOSE_DESC'));

                                                $sheet->setCellValue('AA' . $z ,GovServices::refDashIfNull($record->passport,'PASS_TYPE'));
                                                $sheet->setCellValue('AB' . $z ,GovServices::refDashIfNull($record->passport,'PASSPORT_NO'));
                                                $sheet->setCellValue('AC' . $z ,GovServices::refDashIfNull($record->passport,'ISSUED_ON'));

                                                $sheet->setCellValue('AD' . $z ,GovServices::refDashIfNull($record->health_insurance_data,'CARD_NO'));
                                                $sheet->setCellValue('AE' . $z ,GovServices::refDashIfNull($record->health_insurance_data,'EXP_DATE'));
                                                $sheet->setCellValue('AF' . $z ,GovServices::refDashIfNull($record->health_insurance_data,'INS_STATUS_DESC'));
                                                $sheet->setCellValue('AG' . $z ,GovServices::refDashIfNull($record->health_insurance_data,'INS_TYPE_DESC'));
                                                $sheet->setCellValue('AH' . $z ,GovServices::refDashIfNull($record->health_insurance_data,'WORK_SITE_DESC'));

                                                $sheet->setCellValue('AI' . $z ,GovServices::refDashIfNull($record->social_affairs,'social_affairs_status'));
                                                $sheet->setCellValue('AJ' . $z ,GovServices::refDashIfNull($record->social_affairs,'AID_CLASS'));
                                                $sheet->setCellValue('AK' . $z ,GovServices::refDashIfNull($record->social_affairs,'AID_TYPE'));
                                                $sheet->setCellValue('AL' . $z ,GovServices::refDashIfNull($record->social_affairs,'AID_AMOUNT'));
                                                $sheet->setCellValue('AM' . $z ,GovServices::refDashIfNull($record->social_affairs,'AID_SOURCE'));
                                                $sheet->setCellValue('AN' . $z ,GovServices::refDashIfNull($record->social_affairs,'AID_PERIODIC'));
                                                $sheet->setCellValue('AO' . $z ,GovServices::refDashIfNull($record->social_affairs,'ST_BENEFIT_DATE'));
                                                $sheet->setCellValue('AP' . $z ,GovServices::refDashIfNull($record->social_affairs,'END_BENEFIT_DATE'));

//                                                $affected_by_wars = ($record->aids_committee->affected_by_wars == 1) ? 'yes' :'no' ;
//                                                $sheet->setCellValue('AQ' . $z ,GovServices::refDashIfNull($record->aids_committee,'home_address'));
//                                                $sheet->setCellValue('AR' . $z ,GovServices::refDashIfNull($record->aids_committee,'near_mosque'));
//                                                $sheet->setCellValue('AS' . $z ,GovServices::refDashIfNull($record->aids_committee,'paterfamilias_mobile'));
//                                                $sheet->setCellValue('AT' . $z ,GovServices::refDashIfNull($record->aids_committee,'telephone'));
//                                                $sheet->setCellValue('AU' . $z ,GovServices::refDashIfNull($record->aids_committee,'wife_mobile'));
//                                                $sheet->setCellValue('AV' . $z ,GovServices::refDashIfNull($record->aids_committee,'current_career'));
//                                                $sheet->setCellValue('AW' . $z ,GovServices::refDashIfNull($record->aids_committee,'father_death_reason'));
//                                                $sheet->setCellValue('AZ' . $z ,GovServices::refDashIfNull($record->aids_committee,'mother_death_reason'));
//                                                $sheet->setCellValue('AY' . $z ,GovServices::refDashIfNull($record->aids_committee,'building_type'));
//                                                $sheet->setCellValue('AZ' . $z ,GovServices::refDashIfNull($record->aids_committee,'home_type'));
//                                                $sheet->setCellValue('BA' . $z ,GovServices::refDashIfNull($record->aids_committee,'furniture_type'));
//                                                $sheet->setCellValue('BB' . $z ,GovServices::refDashIfNull($record->aids_committee,'home_status'));
//                                                $sheet->setCellValue('BC' . $z ,GovServices::refDashIfNull($record->aids_committee,'home_description'));
//                                                $sheet->setCellValue('BD' . $z ,GovServices::refDashIfNull($record->aids_committee,'total_family_income'));
//                                                $sheet->setCellValue('BE' . $z ,GovServices::refDashIfNull($record->aids_committee,'health_status'));

                                                if($type != 1) {
                                                    $sheet->setCellValue('AQ' . $z ,GovServices::refDashIfNull($v,'IDNO_RELATIVE'));
                                                    $sheet->setCellValue('AR' . $z ,GovServices::refDashIfNull($v,'RELATIVE_DESC'));
                                                    $sheet->setCellValue('AS' . $z ,GovServices::refDashIfNull($v,'FULLNAME'));
                                                    $sheet->setCellValue('AT' . $z ,GovServices::refDashIfNull($v,'SEX'));
                                                    $sheet->setCellValue('AU' . $z ,GovServices::refDashIfNull($v,'SOCIAL_STATUS'));
                                                    $sheet->setCellValue('AV' . $z ,GovServices::refDashIfNull($v,'BIRTH_DT'));
                                                    $sheet->setCellValue('AW' . $z ,GovServices::refDashIfNull($v,'DETH_DT'));
                                                    $sheet->setCellValue('AX' . $z ,GovServices::refDashIfNull($v,'PREV_LNAME_ARB'));
                                                    $sheet->setCellValue('AY' . $z ,GovServices::refDashIfNull($v,'MOTHER_ARB') );
                                                    $sheet->setCellValue('AZ' . $z ,GovServices::refDashIfNull($v,'MOBILE') );
                                                    $sheet->setCellValue('BA' . $z ,GovServices::refDashIfNull($v,'TEL'));
                                                }

                                                $sheet->getRowDimension($z)->setRowHeight(-1);
                                                $z++;
                                            }

                                        });

                                        if(CaseModel::hasPermission('reports.case.MinistryOfJustice') && sizeof($marriage) > 0){
                                            $excel->sheet(trans('common::application.marriage'), function($sheet) use($marriage) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:E1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.CONTRACT_NO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.CONTRACT_DT'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.HUSBAND_SSN'));
                                                $sheet->setCellValue('E' . $start_from ,trans('common::application.HUSBAND_NAME'));
                                                $sheet->setCellValue('F' . $start_from ,trans('common::application.WIFE_SSN'));
                                                $sheet->setCellValue('G' . $start_from ,trans('common::application.WIFE_NAME'));

                                                $z= 2;
                                                foreach($marriage as $k => $v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'CONTRACT_NO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'CONTRACT_DT'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'HUSBAND_SSN'));
                                                    $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'HUSBAND_NAME'));
                                                    $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'WIFE_SSN'));
                                                    $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'WIFE_NAME'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }
                                                $keys = ['A','B','C','D','E','F','G'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(CaseModel::hasPermission('reports.case.MinistryOfJustice') && sizeof($divorce) > 0){
                                            $excel->sheet(trans('common::application.divorce'), function($sheet) use($divorce) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:H1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.DIV_CERTIFIED_NO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.CONTRACT_DT'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.DIV_TYPE_NAME'));
                                                $sheet->setCellValue('E' . $start_from ,trans('common::application.HUSBAND_SSN'));
                                                $sheet->setCellValue('F' . $start_from ,trans('common::application.HUSBAND_NAME'));
                                                $sheet->setCellValue('G' . $start_from ,trans('common::application.WIFE_SSN'));
                                                $sheet->setCellValue('H' . $start_from ,trans('common::application.WIFE_NAME'));

                                                $z= 2;
                                                foreach($divorce as $k => $v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'DIV_CERTIFIED_NO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'CONTRACT_DT'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'DIV_TYPE_NAME'));
                                                    $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'HUSBAND_SSN'));
                                                    $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'HUSBAND_NAME'));
                                                    $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'WIFE_SSN'));
                                                    $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'WIFE_NAME'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }
                                                $keys = ['A','B','C','D','E','F'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(CaseModel::hasPermission('reports.case.MinistryOfTransportation') && sizeof($vehicles) > 0){
                                            $excel->sheet(trans('common::application.vehicles info'), function($sheet) use($vehicles) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:D1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.MAIN_IDO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.MODEL_YEAR'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.OWNER_TYPE_NAME'));

                                                $z= 2;
                                                foreach($vehicles as $k => $v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MAIN_IDO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'MODEL_YEAR'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'OWNER_TYPE_NAME'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }
                                                $keys = ['A','B','C','D'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(CaseModel::hasPermission('reports.case.MinistryOfFinance') && sizeof($employment) > 0){
                                            $excel->sheet(trans('common::application.work info'), function($sheet) use($employment) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getStyle("A1:J1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.MAIN_IDO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.EMP_DOC_ID'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.MINISTRY_NAME'));
                                                $sheet->setCellValue('E' . $start_from ,trans('common::application.JOB_START_DT'));
                                                $sheet->setCellValue('F' . $start_from ,trans('common::application.DEGREE_NAME'));
                                                $sheet->setCellValue('G' . $start_from ,trans('common::application.EMP_STATE_DESC'));
                                                $sheet->setCellValue('H' . $start_from ,trans('common::application.EMP_WORK_STATUS'));
                                                $sheet->setCellValue('I' . $start_from ,trans('common::application.JOB_DESC'));
                                                $sheet->setCellValue('J' . $start_from ,trans('common::application.MIN_DATA_SOURCE'));

                                                $z= 2;
                                                foreach($employment as $k => $v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MAIN_IDO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'EMP_DOC_ID'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'MINISTRY_NAME'));
                                                    $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'JOB_START_DT'));
                                                    $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'DEGREE_NAME'));
                                                    $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'EMP_STATE_DESC'));
                                                    $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'EMP_WORK_STATUS'));
                                                    $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($v,'JOB_DESC'));
                                                    $sheet->setCellValue('J'.$z,GovServices::refDashIfNull($v,'MIN_DATA_SOURCE'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }
                                                $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G' ,'H','I'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(CaseModel::hasPermission('reports.case.LandAuthority') && sizeof($properties) > 0){
                                            $excel->sheet(trans('common::application.properties info'), function($sheet) use($properties) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:D1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.MAIN_IDO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.DOC_NO'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.REAL_OWNER_AREA'));

                                                $z= 2;
                                                foreach($properties as $k => $v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MAIN_IDO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'DOC_NO'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'REAL_OWNER_AREA'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }
                                                $keys = ['A','B','C'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(CaseModel::hasPermission('reports.case.MinistryOfEconomy') && sizeof($commercial_data) > 0){
                                            $excel->sheet(trans('common::application.commercials info'), function($sheet) use($commercial_data) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:N1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.MAIN_IDO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.COMP_NAME'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.REC_CODE'));
                                                $sheet->setCellValue('E' . $start_from ,trans('common::application.PERSON_TYPE_DESC'));
                                                $sheet->setCellValue('F' . $start_from ,trans('common::application.IS_VALID'));
                                                $sheet->setCellValue('G' . $start_from ,trans('common::application.STATUS_ID_DESC'));
                                                $sheet->setCellValue('H' . $start_from ,trans('common::application.COMP_TYPE_DESC'));
                                                $sheet->setCellValue('I' . $start_from ,trans('common::application.REC_TYPE'));
                                                $sheet->setCellValue('J' . $start_from ,trans('common::application.REGISTER_NO'));
                                                $sheet->setCellValue('K' . $start_from ,trans('common::application.START_DATE'));
                                                $sheet->setCellValue('L' . $start_from ,trans('common::application.WORK_CLASS_DESC'));
                                                $sheet->setCellValue('M' . $start_from ,trans('common::application.BRAND_NAME'));
                                                $sheet->setCellValue('N' . $start_from ,trans('common::application.CITY_DESC'));

                                                $z= 2;
                                                foreach($commercial_data as $k => $v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MAIN_IDO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'COMP_NAME'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'REC_CODE'));
                                                    $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'PERSON_TYPE_DESC'));
                                                    $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'IS_VALID_DESC'));
                                                    $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'STATUS_ID_DESC'));
                                                    $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'COMP_TYPE_DESC'));
                                                    $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($v,'REC_TYPE_DESC'));
                                                    $sheet->setCellValue('J'.$z,GovServices::refDashIfNull($v,'REGISTER_NO'));
                                                    $sheet->setCellValue('K'.$z,GovServices::refDashIfNull($v,'START_DATE'));
                                                    $sheet->setCellValue('L'.$z,GovServices::refDashIfNull($v,'WORK_CLASS_DESC'));
                                                    $sheet->setCellValue('M'.$z,GovServices::refDashIfNull($v,'BRAND_NAME'));
                                                    $sheet->setCellValue('N'.$z,GovServices::refDashIfNull($v,'CITY_DESC'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }
                                                $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G' ,'H','I','J','K','L','M' ];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(CaseModel::hasPermission('reports.case.MinistryOfHealth') && sizeof($health) > 0){
                                            $excel->sheet(trans('common::application.health info'), function($sheet) use($health) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:L1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.MAIN_IDO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.MR_CODE'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.MR_PATIENT_CD'));
                                                $sheet->setCellValue('E' . $start_from ,trans('common::application.LOC_NAME_AR'));
                                                $sheet->setCellValue('F' . $start_from ,trans('common::application.DOCTOR_NAME'));
                                                $sheet->setCellValue('G' . $start_from ,trans('common::application.MR_CREATED_ON'));
                                                $sheet->setCellValue('H' . $start_from ,trans('common::application.DREF_NAME_AR'));
                                                $sheet->setCellValue('I' . $start_from ,trans('common::application.MR_DIAGNOSIS_AR'));
                                                $sheet->setCellValue('J' . $start_from ,trans('common::application.MR_DIAGNOSIS_EN'));
                                                $sheet->setCellValue('K' . $start_from ,trans('common::application.MR_COMPLAINT'));
                                                $sheet->setCellValue('L' . $start_from ,trans('common::application.MR_EXAMINATION'));

                                                $z= 2;
                                                foreach($health as $k=>$v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MAIN_IDO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'MR_CODE'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'LOC_NAME_AR'));
                                                    $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'MR_PATIENT_CD'));
                                                    $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'DOCTOR_NAME'));
                                                    $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'MR_CREATED_ON'));
                                                    $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'DREF_NAME_AR'));
                                                    $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($v,'MR_DIAGNOSIS_AR'));
                                                    $sheet->setCellValue('J'.$z,GovServices::refDashIfNull($v,'MR_DIAGNOSIS_EN'));
                                                    $sheet->setCellValue('K'.$z,GovServices::refDashIfNull($v,'MR_COMPLAINT'));
                                                    $sheet->setCellValue('L'.$z,GovServices::refDashIfNull($v,'MR_EXAMINATION'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }

                                                $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G' ,'H','I','J','K','L'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(sizeof($travel) > 0){
                                            $excel->sheet(trans('common::application.travel records'), function($sheet) use($travel) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:E1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.MAIN_IDO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.CTZN_TRANS_DT'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.CTZN_TRANS_TYPE'));
                                                $sheet->setCellValue('E' . $start_from ,trans('common::application.CTZN_TRANS_BORDER'));

                                                $z= 2;
                                                foreach($travel as $k => $v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MAIN_IDO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'CTZN_TRANS_DT'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'CTZN_TRANS_TYPE'));
                                                    $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'CTZN_TRANS_BORDER'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }
                                                $keys = ['A','B','C','D','E'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(CaseModel::hasPermission('reports.case.MinistryOfLabor') && sizeof($reg48_license) > 0){
                                            $excel->sheet(trans('common::application.reg48_license'), function($sheet) use($reg48_license) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:K1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.MAIN_IDO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.WORKER_ID'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.STATUS_NAME'));
                                                $sheet->setCellValue('E' . $start_from ,trans('common::application.DATE_FROM'));
                                                $sheet->setCellValue('F' . $start_from ,trans('common::application.DATE_TO'));
                                                $sheet->setCellValue('G' . $start_from ,trans('common::application.LICENSE_NAME'));
                                                $sheet->setCellValue('H' . $start_from ,trans('common::application.LIC_REC_STATUS_NAME'));
                                                $sheet->setCellValue('I' . $start_from ,trans('common::application.LIC_REC_DATE'));

                                                $z= 2;
                                                foreach($reg48_license as $k => $v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MAIN_IDO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'WORKER_ID'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'STATUS_NAME'));
                                                    $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'DATE_FROM'));
                                                    $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'DATE_TO'));
                                                    $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'LICENSE_NAME'));
                                                    $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'LIC_REC_STATUS_NAME'));
                                                    $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($v,'LIC_REC_DATE'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }
                                                $keys = ['A','B','C','D','E'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(CaseModel::hasPermission('reports.case.MinistryOfSocialDevelopment') && sizeof($social_affairs_receipt) > 0){
                                            $excel->sheet(trans('common::application.social affairs'), function($sheet) use($social_affairs_receipt) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:H1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from ,trans('common::application.MAIN_IDO'));
                                                $sheet->setCellValue('C' . $start_from ,trans('common::application.SRV_INF_NAME'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.ORG_NM_MON'));
                                                $sheet->setCellValue('E' . $start_from ,trans('common::application.SRV_TYPE_MAIN_NM'));
                                                $sheet->setCellValue('F' . $start_from ,trans('common::application.CURRENCY'));
                                                $sheet->setCellValue('G' . $start_from ,trans('common::application.RECP_AID_AMOUNT'));
                                                $sheet->setCellValue('H' . $start_from ,trans('common::application.RECP_DELV_DT'));

                                                $z= 2;
                                                foreach($social_affairs_receipt as $k => $v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'MAIN_IDO'));
                                                    $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'SRV_INF_NAME'));
                                                    $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'ORG_NM_MON'));
                                                    $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'SRV_TYPE_MAIN_NM'));
                                                    $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'CURRENCY'));
                                                    $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'RECP_AID_AMOUNT'));
                                                    $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v,'RECP_DELV_DT'));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }
                                                $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G' ,'H'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if (CaseModel::hasPermission('reports.case.MinistryOfSocialDevelopment') && sizeof($cases_list) > 0) {
                                            $excel->sheet(trans('common::application.cases_list'), function($sheet) use($cases_list) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:H1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.#'));
                                                $sheet->setCellValue('B1',trans('common::application.MAIN_IDO'));
                                                $sheet->setCellValue('C1',trans('common::application.case_category_type'));
                                                $sheet->setCellValue('D1',trans('common::application.organization'));
                                                $sheet->setCellValue('E1',trans('common::application.status'));
                                                $sheet->setCellValue('F1',trans('common::application.visitor'));
                                                $sheet->setCellValue('G1',trans('common::application.visited_at'));
                                                $sheet->setCellValue('H1',trans('common::application.created_at'));

                                                $z= 2;
                                                foreach($cases_list as $k=>$v )
                                                {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,(is_null($v->MAIN_IDO) ||$v->MAIN_IDO == ' ' ) ? '-' :$v->MAIN_IDO);
                                                    $sheet->setCellValue('C'.$z,(is_null($v->category_name) ||$v->category_name == ' ' ) ? '-' :$v->category_name);
                                                    $sheet->setCellValue('D'.$z,(is_null($v->organization_name) ||$v->organization_name == ' ' ) ? '-' :$v->organization_name);
                                                    $sheet->setCellValue('E'.$z,(is_null($v->status) ||$v->status == ' ' ) ? '-' :$v->status);
                                                    $sheet->setCellValue('F'.$z,(is_null($v->visitor) ||$v->visitor == ' ' ) ? '-' :$v->visitor);
                                                    $sheet->setCellValue('G'.$z,(is_null($v->visited_at) ||$v->visited_at == ' ' ) ? '-' :$v->visited_at);
                                                    $sheet->setCellValue('H'.$z,(is_null($v->created_date) ||$v->created_date == ' ' ) ? '-' :$v->created_date);
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }

                                                $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G'  ,'H' ];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }
                                            });
                                        }

                                        if(sizeof($invalid_cards) > 0){
                                            $excel->sheet(trans('common::application.invalid_cards'), function($sheet) use($invalid_cards) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;
                                                foreach($invalid_cards as $v){
                                                    $sheet->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }

                                            });
                                        }

                                    })->store('xlsx', storage_path('tmp/'));

                                    \Log\Model\Log::saveNewLog('CARD_CKECK', trans('common::application.Some numbers were successfully checked, as the total number') .$total);
                                    if( $total == ( $success +$duplicated)){
                                        return response()->json(['status' => 'success',
                                            'msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.All numbers successfully checked') ,'download_token' => $token]);
                                    }
                                    else{
                                        return response()->json([ 'status' => 'success',
                                            'msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.Some numbers were successfully checked, as the total number') .' :  '. $total . ' ,  '.
                                                trans('common::application.The number of names that have fetched data') .' :  ' .$success . ' ,  ' .
                                                trans('common::application.Number of names not registered in civil registry')  .' :  ' .sizeof($invalid_cards) . ' ,  ' .
                                                trans('common::application.Number of duplicates').' :  '.$duplicated ,
                                            'download_token' => $token ]);
                                    }

                                }
                            }

                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }
    /*********************** socialAffairs ***********************/
    // filter citizen benefit of social affairs according ( card or name )
    public function affairsSearchFilter(Request $request)
    {

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $this->authorize('socialAffairs', \Common\Model\AidsCases::class);

        $records = [];
        $using = $request->using;
        $user = \Auth::user();

        if($using == 1){
            $card = $request->id_card_number;
            CardLog::saveCardLog($user->id,$card);
            $row = GovServices::byCard($card);
            if($row['status'] != false){
                $temp = $row['row'];
                $records[] = $temp;
            }
        }
        else{
            $row = GovServices::byName($request->all());
            if($row['status'] != false){
                $records= $row['row'];


            }
        }

        if(sizeof($records) > 0 ){
            foreach ($records as $key=>$value){
                if(!isset($value->MOBILE)){
                    $value->MOBILE=null;
                    $value->TEL=null;
                    $contact_data = GovServices::ssoInfo($value->IDNO);
                    if($contact_data['status'] != false){
                        $value->MOBILE=GovServices::ObjectValue($contact_data['row'],'USERMOBILE');
                        $value->TEL=GovServices::ObjectValue($contact_data['row'],'USERTELEPHONE');
                    }
                }
                $value->social_affairs= false;
                $value->social_affairs_status= trans('common::application.un_beneficiary');
                $value->AID_CLASS=null;
                $value->AID_TYPE=null;
                $value->AID_AMOUNT=null;
                $value->AID_PERIODIC=null;
                $value->AID_SOURCE=null;
                $value->ST_BENEFIT_DATE=null;
                $value->END_BENEFIT_DATE=null;
                $socialAffairs=GovServices::socialAffairsStatus($value->IDNO);
                if($socialAffairs['status'] == true){
                    $socialAffairs_=$socialAffairs['row'];
                    $value->social_affairs= true;
                    $value->social_affairs_status=  trans('common::application.beneficiary_');
                    $value->AID_CLASS=$socialAffairs_->AID_CLASS;
                    $value->AID_TYPE=$socialAffairs_->AID_TYPE;
                    $value->AID_AMOUNT=$socialAffairs_->AID_AMOUNT;
                    $value->AID_PERIODIC=$socialAffairs_->AID_PERIODIC;
                    $value->AID_SOURCE=$socialAffairs_->AID_SOURCE;
                    $value->ST_BENEFIT_DATE=$socialAffairs_->ST_BENEFIT_DATE;
                    $value->END_BENEFIT_DATE=$socialAffairs_->END_BENEFIT_DATE;
                }

                CloneGovernmentPersons::saveNew($value);

            }
            return response()->json(['status'=>true,'items'=>$records]);
        }

        return response()->json(['status'=>false]);

    }

    // get citizen benefit of social affairs using card
    public function socialAffairs(Request $request)
    {
        $this->authorize('socialAffairs', \Common\Model\AidsCases::class);


        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $card = $request->id_card_number;

        $socialAffairs=GovServices::socialAffairsStatus($card);
        if($socialAffairs['status'] == false){
            return response()->json(['status'=>false,'msg'=>$socialAffairs['message']]);
        }

        $person = null;
        $row = GovServices::byCard($card);
        if($row['status'] == false){
            return response()->json(['status'=>false,'msg'=>$row['message']]);
        }

        $record=$row['row'];
        $socialAffairs_=$socialAffairs['row'];
        $record->social_affairs= true;
        $record->social_affairs_status=  trans('common::application.beneficiary_');
        $record->AID_CLASS=$socialAffairs_->AID_CLASS;
        $record->AID_TYPE=$socialAffairs_->AID_TYPE;
        $record->AID_AMOUNT=$socialAffairs_->AID_AMOUNT;
        $record->AID_PERIODIC=$socialAffairs_->AID_PERIODIC;
        $record->AID_SOURCE=$socialAffairs_->AID_SOURCE;
        $record->ST_BENEFIT_DATE=$socialAffairs_->ST_BENEFIT_DATE;
        $record->END_BENEFIT_DATE=$socialAffairs_->END_BENEFIT_DATE;

        $record->social_affairs_receipt=[];
        $socialAffairsRecipt=GovServices::socialAffairsReceipt($card);
        if($socialAffairsRecipt['status'] != false) {
            $record->social_affairs_receipt = $socialAffairsRecipt['row'];
        }


        $keys = ['parent','childrens','wives'];
        foreach ($keys as $key){
            foreach ($record->$key as $k=>$v){
                $v->social_affairs= false;
                $v->social_affairs_status= trans('common::application.un_beneficiary');
                $v->AID_CLASS=null;
                $v->AID_TYPE=null;
                $v->AID_AMOUNT=null;
                $v->AID_PERIODIC=null;
                $v->AID_SOURCE=null;
                $v->ST_BENEFIT_DATE=null;
                $v->END_BENEFIT_DATE=null;

                $socialAffairs=GovServices::socialAffairsStatus($v->IDNO_RELATIVE);
                if($socialAffairs['status'] != false){
                    $socialAffairs_=$socialAffairs['row'];
                    $v->social_affairs= true;
                    $v->social_affairs_status=  trans('common::application.beneficiary_');
                    $v->AID_CLASS=$socialAffairs_->AID_CLASS;
                    $v->AID_TYPE=$socialAffairs_->AID_TYPE;
                    $v->AID_AMOUNT=$socialAffairs_->AID_AMOUNT;
                    $v->AID_PERIODIC=$socialAffairs_->AID_PERIODIC;
                    $v->AID_SOURCE=$socialAffairs_->AID_SOURCE;
                    $v->ST_BENEFIT_DATE=$socialAffairs_->ST_BENEFIT_DATE;
                    $v->END_BENEFIT_DATE=$socialAffairs_->END_BENEFIT_DATE;
                }
            }
        }

        CloneGovernmentPersons::saveNew($record);
        $record->CASE_STATUS=false;
        $record->cases_list=[];
        $record->person_id = Person::getPersonId([$card]);
        if (CaseModel::hasPermission('reports.case.RelayCitizenRepository')) {
            $record->cases_list = Person::getPersonCardCases([$card]);
        }

        if($request->action =='xlsx'){

            if (sizeof($record->all_relatives) == 0 ){
                $record[] = (Object)[
                    'IDNO_RELATIVE' => '-' ,
                    'kinship_name' => '-' ,
                    'FNAME_ARB' => '-' ,
                    'SNAME_ARB' => '-' ,
                    'TNAME_ARB' => '-' ,
                    'LNAME_ARB' => '-' ,
                    'SEX' => '-' ,
                    'SOCIAL_STATUS' => '-' ,
                    'BIRTH_DT' => '-' ,
                    'DETH_DT' => '-' ,
                    'BIRTH_PMAIN' => '-' ,
                    'BIRTH_PSUB' => '-' ,
                    'PREV_LNAME_ARB' => '-' ,
                    'MOTHER_ARB' => '-' ,
                    'STATUS' => '-' ,
                ];
            }
            else{
                foreach ($record->all_relatives as $k=>$v){
                    $v->social_affairs= false;
                    $v->social_affairs_status= trans('common::application.un_beneficiary');
                    $v->AID_CLASS=null;
                    $v->AID_TYPE=null;
                    $v->AID_AMOUNT=null;
                    $v->AID_PERIODIC=null;
                    $v->AID_SOURCE=null;
                    $v->ST_BENEFIT_DATE=null;
                    $v->END_BENEFIT_DATE=null;

                    $socialAffairs=GovServices::socialAffairsStatus($v->IDNO_RELATIVE);
                    if($socialAffairs['status'] != false){
                        $socialAffairs_=$socialAffairs['row'];
                        $v->social_affairs= true;
                        $v->social_affairs_status=  trans('common::application.beneficiary_');
                        $v->AID_CLASS=$socialAffairs_->AID_CLASS;
                        $v->AID_TYPE=$socialAffairs_->AID_TYPE;
                        $v->AID_AMOUNT=$socialAffairs_->AID_AMOUNT;
                        $v->AID_PERIODIC=$socialAffairs_->AID_PERIODIC;
                        $v->AID_SOURCE=$socialAffairs_->AID_SOURCE;
                        $v->ST_BENEFIT_DATE=$socialAffairs_->ST_BENEFIT_DATE;
                        $v->END_BENEFIT_DATE=$socialAffairs_->END_BENEFIT_DATE;
                    }
                }
                $repeated = $record->all_relatives;
            }

            $token = md5(uniqid());
            \Excel::create('export_' . $token, function($excel) use($record,$repeated) {
                $excel->setTitle(trans('common::application.family_sheet'));
                $excel->setDescription(trans('common::application.family_sheet'));
                $excel->sheet(trans('common::application.family_sheet'), function($sheet) use($record,$repeated) {

                    $sheet->setRightToLeft(true);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);

                    $sheet->getStyle("A1:R1")->applyFromArray(['font' => ['bold' => true]]);
                    $sheet->setCellValue('A1',trans('common::application.#'));
                    $sheet->setCellValue('B1',trans('common::application.full_name'));
                    $sheet->setCellValue('C1',trans('common::application.id_card_number'));
                    $sheet->setCellValue('D1',trans('common::application.gender'));
                    $sheet->setCellValue('E1',trans('common::application.marital_status'));
                    $sheet->setCellValue('F1',trans('common::application.address'));
                    $sheet->setCellValue('G1',trans('common::application.mobile'));
                    $sheet->setCellValue('H1',trans('common::application.phone'));
                    $sheet->setCellValue('I1',trans('common::application.aid_status'));
                    $sheet->setCellValue('J1',trans('common::application.id_card_number').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('K1',trans('common::application.kinship_name') .' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('L1',trans('common::application.full_name').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('M1',trans('common::application.gender').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('N1',trans('common::application.marital_status').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('O1',trans('common::application.birthday').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('P1',trans('common::application.prev_family_name').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('Q1',trans('common::application.mother_name') .' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('R1',trans('common::application.aid_status') .' ('.trans('common::application.related') .' )');

                    $z= 2;
                    foreach($repeated as $k=>$v )
                    {
                        $sheet->setCellValue('A'.$z,$k+1);
                        $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($record,'FULLNAME'));
                        $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($record,'IDNO'));
                        $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($record,'SEX'));
                        $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($record,'SOCIAL_STATUS'));
                        $sheet->setCellValue('F'.$z,
                            ( (is_null($record->CI_REGION) ||$record->CI_REGION == ' ' ) ? ' ' :$record->CI_REGION) .' '.
                            ( (is_null($record->CI_CITY) ||$record->CI_CITY == ' ' ) ? ' ' :$record->CI_CITY) .' '.
                            ( (is_null($record->STREET_ARB) ||$record->STREET_ARB == ' ' ) ? ' ' :$record->STREET_ARB)
                        );

                        $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($record,'MOBILE'));
                        $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($record,'TEL'));
                        $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($record,'social_affairs_status'));
                        $sheet->setCellValue('J'.$z,GovServices::refDashIfNull($v,'IDNO_RELATIVE'));
                        $sheet->setCellValue('K'.$z,GovServices::refDashIfNull($v,'RELATIVE_DESC'));
                        $sheet->setCellValue('L'.$z,GovServices::refDashIfNull($v,'FULLNAME'));
                        $sheet->setCellValue('M'.$z,GovServices::refDashIfNull($v,'SEX'));
                        $sheet->setCellValue('N'.$z,GovServices::refDashIfNull($v,'SOCIAL_STATUS'));
                        $sheet->setCellValue('O'.$z,GovServices::refDashIfNull($v,'BIRTH_DT'));
                        $sheet->setCellValue('P'.$z,GovServices::refDashIfNull($v,'PREV_LNAME_ARB'));
                        $sheet->setCellValue('Q'.$z,GovServices::refDashIfNull($v,'MOTHER_ARB'));
                        $sheet->setCellValue('R'.$z,GovServices::refDashIfNull($v,'social_affairs_status'));
                        $z++;
                    }

                });
                if(sizeof($record->social_affairs_receipt) > 0){
                    $excel->sheet(trans('common::application.social affairs'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:G1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.SRV_INF_NAME'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.ORG_NM_MON'));
                        $sheet->setCellValue('D' . $start_from ,trans('common::application.SRV_TYPE_MAIN_NM'));
                        $sheet->setCellValue('E' . $start_from ,trans('common::application.CURRENCY'));
                        $sheet->setCellValue('F' . $start_from ,trans('common::application.RECP_AID_AMOUNT'));
                        $sheet->setCellValue('G' . $start_from ,trans('common::application.RECP_DELV_DT'));

                        $z= 2;
                        foreach($record->social_affairs_receipt as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'SRV_INF_NAME'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'ORG_NM_MON'));
                            $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'SRV_TYPE_MAIN_NM'));
                            $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'CURRENCY'));
                            $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'RECP_AID_AMOUNT'));
                            $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'RECP_DELV_DT'));
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if (CaseModel::hasPermission('reports.case.RelayCitizenRepository') && sizeof($record->cases_list) > 0) {
                    $excel->sheet(trans('common::application.cases_list'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:G1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->setCellValue('A1',trans('common::application.#'));
                        $sheet->setCellValue('B1',trans('common::application.case_category_type'));
                        $sheet->setCellValue('C1',trans('common::application.organization'));
                        $sheet->setCellValue('D1',trans('common::application.status'));
                        $sheet->setCellValue('E1',trans('common::application.visitor'));
                        $sheet->setCellValue('F1',trans('common::application.visited_at'));
                        $sheet->setCellValue('G1',trans('common::application.created_at'));

                        $z= 2;
                        foreach($record->cases_list as $k=>$v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,(is_null($v->category_name) ||$v->category_name == ' ' ) ? '-' :$v->category_name);
                            $sheet->setCellValue('C'.$z,(is_null($v->organization_name) ||$v->organization_name == ' ' ) ? '-' :$v->organization_name);
                            $sheet->setCellValue('D'.$z,(is_null($v->status) ||$v->status == ' ' ) ? '-' :$v->status);
                            $sheet->setCellValue('E'.$z,(is_null($v->visitor) ||$v->visitor == ' ' ) ? '-' :$v->visitor);
                            $sheet->setCellValue('F'.$z,(is_null($v->visited_at) ||$v->visited_at == ' ' ) ? '-' :$v->visited_at);
                            $sheet->setCellValue('G'.$z,(is_null($v->created_date) ||$v->created_date == ' ' ) ? '-' :$v->created_date);
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }

                    });

                }

            })->store('xlsx', storage_path('tmp/'));
            return response()->json(['download_token' => $token ]);
        }
        elseif($request->action =='pdf') {

            $full_name=  GovServices::refDashIfNull($record,'FULLNAME');
            $en_name=  GovServices::refDashIfNull($record,'ENG_NAME');
            $mother_name=  GovServices::refDashIfNull($record,'MOTHER_ARB');
            $prev_family_name=  GovServices::refDashIfNull($record,'PREV_LNAME_ARB');

            $gender=  GovServices::refDashIfNull($record,'SEX');
            $marital_status=  GovServices::refDashIfNull($record,'SOCIAL_STATUS');
            $birthday=  GovServices::refDashIfNull($record,'BIRTH_DT');
            $death_date=  GovServices::refDashIfNull($record,'DETH_DT');
            $birth_place_main=  GovServices::refDashIfNull($record,'BIRTH_PMAIN');
            $birth_place_sub=  GovServices::refDashIfNull($record,'BIRTH_PMAIN');

            $address = $record->CI_REGION .' - ' .$record->CI_CITY  . ' - ' . $record->STREET_ARB;

            $family_cnt=  GovServices::refDashIfNull($record,'family_cnt');
            $female_live=  GovServices::refDashIfNull($record,'female_live');
            $male_live=  GovServices::refDashIfNull($record,'male_live');
            $spouses=  GovServices::refDashIfNull($record,'spouses');

            $primary_mobile=  GovServices::refDashIfNull($record,'MOBILE');
            $phone=  GovServices::refDashIfNull($record,'TEL');

            $GOV_NAME=  GovServices::refDashIfNull($record,'GOV_NAME');
            $CITY_NAME=  GovServices::refDashIfNull($record,'CITY_NAME');
            $PART_NAME=  GovServices::refDashIfNull($record,'PART_NAME');
            $ADDRESS_DET=  GovServices::refDashIfNull($record,'ADDRESS_DET');

            $socialAffairsRecipt=GovServices::socialAffairsReceipt($card);
            $record->social_affairs_receipt=[];
            if($socialAffairsRecipt['status'] == true) {
                $record->social_affairs_receipt = $socialAffairsRecipt['row'];
            }

            PDF::SetTitle($full_name);
            PDF::SetFont('aealarabiya', '', 18);
            PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);
            PDF::SetMargins(10, 10, 10);
            PDF::SetAutoPageBreak(TRUE);
            $lg = Array();
            $lg['a_meta_charset'] = 'UTF-8';
            $lg['a_meta_dir'] = 'rtl';
            $lg['a_meta_language'] = 'fa';
            $lg['w_page'] = 'page';
            PDF::setLanguageArray($lg);
            PDF::setCellHeightRatio(1.5);
            $pageNo = 1;
            $totalPageCount = 2;

            if(sizeof($record->social_affairs_receipt) > 0) {
                $count = count($record->social_affairs_receipt);
                $totalPageCount += ceil($count / 25);
            }

            $token = md5(uniqid());
            $img = base_path('storage/app/emptyUser1.png');
            $fileContents = file_get_contents($record->photo_url);
            $disk = "local";
            $filename = $token.'.png'; // The name you want to give to the downloaded image
            if ($fileContents !== false) {
                Storage::disk($disk)->put('documents/' . $filename, $fileContents);
                $img = storage_path('app/documents/').$token.'.png';
            }

            // social affairs
            $social_affairs_status = trans('common::application.un_beneficiary');
            $aid_class = $aid_type = $aid_amount = $aid_source = $aid_periodic = $aid_st_ben_date = $aid_end_ben_date = '-';
            if(!is_null($record->social_affairs)){
                $social_affairs_status = GovServices::refDashIfNull($record,'social_affairs_status');
                $aid_class = GovServices::refDashIfNull($record,'AID_CLASS');
                $aid_type = GovServices::refDashIfNull($record,'AID_TYPE');
                $aid_amount = GovServices::refDashIfNull($record,'AID_AMOUNT');
                $aid_source = GovServices::refDashIfNull($record,'AID_SOURCE');
                $aid_periodic = GovServices::refDashIfNull($record,'AID_PERIODIC');
                $aid_st_ben_date = GovServices::refDashIfNull($record,'ST_BENEFIT_DATE');
                $aid_end_ben_date = GovServices::refDashIfNull($record,'END_BENEFIT_DATE');
            }
            $html = '<!doctype html><html lang="ar">
                 <head>
                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                 <style>
                     .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                     table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                     * , body {font-weight:normal !important; font-size: 10px;}
                     .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                     .bolder-content{text-align: right !important; vertical-align: middle!important;}
                     .vertical-middle{ vertical-align: middle !important;}
                </style>
                </head>
                 <body style="font-weight: normal !important;">';

            $html.= '<h2 style="text-align:center;"> '.$full_name.'</h2>';
            $html .= '<table border="1" style="border: 1px solid black; box-sizing:border-box;">';

            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.full_name').' </b></td>
                       <td width="330" align="center" >'.$full_name.'</td>
                        <td width="98" align="center" rowspan="6" style="background-color: white" >'
                .'<img src="'.$img.'" alt="" height="140" />'.
                '</td>
                       
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.en_name').' </b></td>
                       <td width="330" align="center" >'.$en_name.'</td>
                    
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.id_card_number').' </b></td>
                       <td width="115" align="center" >'.$card.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.gender').' </b></td>
                       <td width="115" align="center" >'.$gender.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.birthday').' </b></td>
                       <td width="115" align="center" >'.$birthday.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.marital_status').' </b></td>
                       <td width="115" align="center" >'.$marital_status.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.primary_mobile').' </b></td>
                       <td width="115" align="center" >'.$primary_mobile.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.phone').' </b></td>
                       <td width="115" align="center" >'.$phone.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.prev_family_name').' </b></td>
                       <td width="115" align="center" >'.$prev_family_name.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.family_cnt').' </b></td>
                       <td width="115" align="center" >'.$family_cnt.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.spouses').' </b></td>
                       <td width="70" align="center" >'.$spouses.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.male_live').' </b></td>
                       <td width="70" align="center" >'.$male_live.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.female_live').' </b></td>
                       <td width="70" align="center" >'.$female_live.'</td>
                   </tr>';
            $html .='</table>';

            $html .= '<table border="1" style="border: 1px solid black; box-sizing:border-box;">';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.address').' </b></td>
                       <td width="428" align="center" >'.$address.'</td>
                   </tr>';

            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="128" align="center"><b>'.trans('common::application.aid_status') .'('.trans('common::application.social affairs').') </b></td>
                       <td width="400" align="center" >'.$social_affairs_status.'</td>
                   </tr>';

            $html .= ' <tr> 
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_class').'</b></td>
                       <td width="96" align="center">'.$aid_class.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_type').'</b></td>
                       <td width="96" align="center">'.$aid_type.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_amount').'</b></td>
                       <td width="96" align="center">'.$aid_amount.'</td>
                    </tr>';

            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_source').'</b></td>
                       <td width="184" align="center">'.$aid_source.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_periodic').'</b></td>
                       <td width="184" align="center">'.$aid_periodic.'</td> 
                   </tr>';

            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_st_ben_date').'</b></td>
                       <td width="184" align="center">'.$aid_st_ben_date.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_end_ben_date').'</b></td>
                       <td width="184" align="center">'.$aid_end_ben_date.'</td> 
                   </tr>';
            $html .='</table>';

//                parents
            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';

            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.parents').'
                        </b></td>
                   </tr>';
            if (sizeof($record->parent) > 0) {
                $html .= ' <tr> 
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> ' . trans('common::application.id_card_number') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="123" align="center"><b> ' . trans('common::application.name') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="64" align="center"><b> ' . trans('common::application.marital_status') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="64" align="center"><b> ' . trans('common::application.birthday') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="64" align="center"><b> ' . trans('common::application.death_date') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> ' . trans('common::application.mobile') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> ' . trans('common::application.aid_status') . '</b></td>
                </tr>';

            }
            $z = 1;

//                parents
            foreach($record->parent as $row) {
                $html .= ' <tr> 
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.$z.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'IDNO_RELATIVE').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="123" align="center"><b> '. GovServices::refDashIfNull($row,'FULLNAME') .'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="64" align="center"><b> '.GovServices::refDashIfNull($row,'SOCIAL_STATUS').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="64" align="center"><b> '.GovServices::refDashIfNull($row,'BIRTH_DT').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="64" align="center"><b> '.GovServices::refDashIfNull($row,'DETH_DT').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="70" align="center"><b> '.GovServices::refDashIfNull($row,'MOBILE').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="70" align="center"><b> '.GovServices::refDashIfNull($row,'social_affairs_status').'</b></td>
                    </tr>';
                $z++;
            }
            if (sizeof($record->parent) == 0){
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
            }

            $html .='</table>';

            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.wives or husbands').'
                        </b></td>
                   </tr>';

            if (sizeof($record->wives) > 0){
                $html .= ' <tr> 
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.id_card_number').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="123" align="center"><b> '.trans('common::application.name').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="64" align="center"><b> '.trans('common::application.marital_status').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="64" align="center"><b> '.trans('common::application.birthday').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="64" align="center"><b> '.trans('common::application.death_date').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.mobile').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> ' . trans('common::application.aid_status') . '</b></td>
          </tr>';
            }

            $z = 1;
            foreach($record->wives as $row) {

                $html .= ' <tr> 
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.$z.'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'IDNO_RELATIVE').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="123" align="center"><b> '. GovServices::refDashIfNull($row,'FULLNAME') .'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="64" align="center"><b> '.GovServices::refDashIfNull($row,'SOCIAL_STATUS').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="64" align="center"><b> '.GovServices::refDashIfNull($row,'BIRTH_DT').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="64" align="center"><b> '.GovServices::refDashIfNull($row,'DETH_DT').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="70" align="center"><b> '.GovServices::refDashIfNull($row,'MOBILE').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="70" align="center"><b> '.GovServices::refDashIfNull($row,'social_affairs_status').'</b></td>
     </tr>';
                $z++;
            }

            if (sizeof($record->wives) == 0){
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
            }
            $html .='</table>';

// childs
            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.childs').'
                        </b></td>
                   </tr>';

            if (sizeof($record->childrens) > 0){
                $html .= ' <tr> 
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.id_card_number').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.name').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="30" align="center"><b> '.trans('common::application.gender').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.marital_status').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.birthday').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.death_date').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="65" align="center"><b> '.trans('common::application.mobile').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> ' . trans('common::application.aid_status') . '</b></td>
 </tr>';
            }

            $z = 1;
            foreach($record->childrens as $row) {
                $html .= ' <tr> 
                           <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.$z.'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'IDNO_RELATIVE').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="120" align="center"><b> '. GovServices::refDashIfNull($row,'FULLNAME') .'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="30" align="center"><b> '.GovServices::refDashIfNull($row,'SEX').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'SOCIAL_STATUS').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'BIRTH_DT').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'DETH_DT').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="65" align="center"><b> '.GovServices::refDashIfNull($row,'MOBILE').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'social_affairs_status').'</b></td>
                    </tr>';
                $z++;
            }
            if (sizeof($record->childrens) == 0){
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
            }
            $html .='</table>';


            $html .='</body></html>';

            PDF::AddPage('P','A4');
            PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
            });
            $pageNo++;
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);



// cases_list
            $html = '<!doctype html><html lang="ar">
                 <head>
                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                 <style>
                     .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                     table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                     * , body {font-weight:normal !important; font-size: 10px;}
                     .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                     .bolder-content{text-align: right !important; vertical-align: middle!important;}
                     .vertical-middle{ vertical-align: middle !important;}
                </style>
                </head>
                 <body style="font-weight: normal !important;">';

            $html .='<div style="height: 5px; color: white"> - </div>';

// cases_list
            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.cases list').'
                        </b></td>
                   </tr>';

            if (sizeof($record->cases_list) > 0){
                $html .= ' <tr> 
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="75" align="center"><b> '.trans('common::application.case_category_type').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="150" align="center"><b> '.trans('common::application.organization name').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="40" align="center"><b> '.trans('common::application.status').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="89" align="center"><b> '.trans('common::application.visitor').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.visited_at').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.created_at').'</b></td>
                           </tr>';
            }


            $z = 1;
            foreach($record->cases_list as $row) {
                $html .= ' <tr> 
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.$z.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="75" align="center"><b> '.$row->category_name.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="150" align="center"><b> '.$row->organization_name.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="40" align="center"><b> '.$row->status.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="89" align="center"><b> '.$row->visitor.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="80" align="center"><b> '.$row->visited_at.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="80" align="center"><b> '.$row->created_dt.'</b></td>
                    </tr>';
                $z++;
            }
            if (sizeof($record->cases_list) == 0){
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
            }
            $html .='</table>';
            $html .='</body></html>';

            PDF::AddPage('P','A4');
            PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
            });
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
            $pageNo++;


            // social_affairs_receipt
            if(sizeof($record->social_affairs_receipt) > 0){
                $count = count($record->social_affairs_receipt);
                $ceil=ceil($count/25);
                for ($x = 0; $x < $ceil; $x++) {
                    $curOffset = ($x) * 25;
                    $index=$curOffset;

                    $html = '<!doctype html><html lang="ar">
                         <head>
                         <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                         <style>
                             .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                             table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                             * , body {font-weight:normal !important; font-size: 10px;}
                             .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                             .bolder-content{text-align: right !important; vertical-align: middle!important;}
                             .vertical-middle{ vertical-align: middle !important;}
                        </style>
                        </head>
                         <body style="font-weight: normal !important;">';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';

                    $html .= ' <tr>
                               <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="534" align="center"><b>  
                                '.trans('common::application.social affairs').'
                                </b></td>
                           </tr>';


                    $html .= ' <tr> 
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="146" align="center"><b> '.trans('common::application.SRV_INF_NAME').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="145" align="center"><b> '.trans('common::application.ORG_NM_MON').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="54" align="center"><b> '.trans('common::application.SRV_TYPE_MAIN_NM').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="54" align="center"><b> '.trans('common::application.CURRENCY').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="50" align="center"><b> '.trans('common::application.RECP_AID_AMOUNT').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="65" align="center"><b> '.trans('common::application.RECP_DELV_DT').'</b></td>
                    </tr>';

                    if($index < $count) {
                        for ($z = 0; $z < 25; $z++) {
                            if($index < $count){
                                $row = $record->social_affairs_receipt[$index];
                                $html .= ' <tr> 
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.($index+1).'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="146" align="center"><b> '.GovServices::refDashIfNull($row,'SRV_INF_NAME').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="145" align="center"><b> '.GovServices::refDashIfNull($row,'ORG_NM_MON').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="54" align="center"><b> '.GovServices::refDashIfNull($row,'SRV_TYPE_MAIN_NM').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="54" align="center"><b> '.GovServices::refDashIfNull($row,'CURRENCY').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="50" align="center"><b> '.GovServices::refDashIfNull($row,'RECP_AID_AMOUNT').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="65" align="center"><b> '.GovServices::refDashIfNull($row,'RECP_DELV_DT').'</b></td>
                                </tr>';
                                $index++;
                            }
                        }
                    }
                    $html .='</table>';
                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                        $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                        PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
                    });
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                    $pageNo++;

                }
            }

            $dirName = base_path('storage/app/citizen_pdf');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }

            PDF::Output($dirName.'/'.$full_name . '.pdf', 'F');

            unlink(storage_path('app/documents/'.$token.'.png'));
            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');

            $zip = new \ZipArchive;
            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                foreach ( glob( $dirName . '/*' ) as $fileName )
                {
                    $file = basename( $fileName );
                    $zip->addFile( $fileName, $file );
                }

                $zip->close();
                array_map('unlink', glob("$dirName/*.*"));
                rmdir($dirName);
            }

            return response()->json(['download_token' => $token]);

        }
        else{

            CloneGovernmentPersons::saveNew($record);
            return response()->json(['status'=>true,'data'=>$record]);
        }

    }

    // check citizen benefit of social affairs using excel sheet according card number
    public function socialAffairsCheck(Request $request)
    {
        $this->authorize('socialAffairs', \Common\Model\AidsCases::class);
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $type = $request->type;
        $validFile=true;
        $duplicated =0;
        $success =0;

        $final_records=[];
        $invalid_cards=[];
        $processed=[];

        $importFile=$request->file ;
        if ($importFile->isValid()) {
            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();

                    if(sizeof($keys) >0 ){
                        $validFile = false;
                        foreach ($keys as $item) {
                            if ( $item == 'rkm_alhoy' ) {
                                $validFile = true;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {
                                foreach ($records as $key =>$value) {
                                    if(in_array($value['rkm_alhoy'],$processed)){
                                        $duplicated++;
                                    }
                                    else{
                                        $card= (int) $value['rkm_alhoy'];

                                        if(strlen($card)  <= 9) {
                                            $socialAffairs=GovServices::socialAffairsStatus($card);
                                            if($socialAffairs['status'] == false){
                                                $invalid_cards[]=$value['rkm_alhoy'];
                                            }else{
                                                $row = GovServices::byCard($card);
                                                if($row['status'] == true) {
                                                    $record = $row['row'];
                                                    $record->social_affairs= false;
                                                    $record->social_affairs_status= trans('common::application.un_beneficiary');
                                                    $record->AID_CLASS='-';
                                                    $record->AID_TYPE='-';
                                                    $record->AID_AMOUNT='-';
                                                    $record->AID_PERIODIC='-';
                                                    $record->AID_SOURCE='-';
                                                    $record->ST_BENEFIT_DATE='-';
                                                    $record->END_BENEFIT_DATE='-';

                                                    $socialAffairs=GovServices::socialAffairsStatus($card);
                                                    if($socialAffairs['status'] != false){
                                                        $socialAffairs_=$socialAffairs['row'];
                                                        $record->social_affairs= true;
                                                        $record->social_affairs_status=  trans('common::application.beneficiary_');
                                                        $record->AID_CLASS=$socialAffairs_->AID_CLASS;
                                                        $record->AID_TYPE=$socialAffairs_->AID_TYPE;
                                                        $record->AID_AMOUNT=$socialAffairs_->AID_AMOUNT;
                                                        $record->AID_PERIODIC=$socialAffairs_->AID_PERIODIC;
                                                        $record->AID_SOURCE=$socialAffairs_->AID_SOURCE;
                                                        $record->ST_BENEFIT_DATE=$socialAffairs_->ST_BENEFIT_DATE;
                                                        $record->END_BENEFIT_DATE=$socialAffairs_->END_BENEFIT_DATE;
                                                    }


                                                    CloneGovernmentPersons::saveNew($record);
                                                    $main=(array) $record;
                                                    $main['related']= null;

                                                    if($type != 1) {
                                                        if(sizeof($record->all_relatives) > 0){
                                                            foreach ($record->all_relatives as $k=>$v){
                                                                $v->social_affairs= false;
                                                                $v->social_affairs_status= trans('common::application.un_beneficiary');
                                                                $v->AID_CLASS=null;
                                                                $v->AID_TYPE=null;
                                                                $v->AID_AMOUNT=null;
                                                                $v->AID_PERIODIC=null;
                                                                $v->AID_SOURCE=null;
                                                                $v->ST_BENEFIT_DATE=null;
                                                                $v->END_BENEFIT_DATE=null;

                                                                $socialAffairs=GovServices::socialAffairsStatus($v->IDNO_RELATIVE);
                                                                if($socialAffairs['status'] != false){
                                                                    $socialAffairs_=$socialAffairs['row'];
                                                                    $v->social_affairs= true;
                                                                    $v->social_affairs_status=  trans('common::application.beneficiary_');
                                                                    $v->AID_CLASS=$socialAffairs_->AID_CLASS;
                                                                    $v->AID_TYPE=$socialAffairs_->AID_TYPE;
                                                                    $v->AID_AMOUNT=$socialAffairs_->AID_AMOUNT;
                                                                    $v->AID_PERIODIC=$socialAffairs_->AID_PERIODIC;
                                                                    $v->AID_SOURCE=$socialAffairs_->AID_SOURCE;
                                                                    $v->ST_BENEFIT_DATE=$socialAffairs_->ST_BENEFIT_DATE;
                                                                    $v->END_BENEFIT_DATE=$socialAffairs_->END_BENEFIT_DATE;
                                                                }

                                                                CloneGovernmentPersons::saveNewWithRelation($v->IDNO_RELATIVE,$v);

                                                                $main['related']= $v;
                                                                $final_records[] = $main;
                                                            }
                                                        }
                                                        else{
                                                            $final_records[] = $main;
                                                        }
                                                    }else{
                                                        $final_records[] = $main;
                                                    }

                                                    $processed[]=$value['rkm_alhoy'];
                                                    $success++;
                                                }else{
                                                    $invalid_cards[]=$value['rkm_alhoy'];
                                                }
                                            }
                                        }else{
                                            $invalid_cards[]=$value['rkm_alhoy'];
                                        }
                                    }
                                }

                                if($total == sizeof($invalid_cards)){
                                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.All names entered are not registered benefiting from social affairs') ]);
                                }else{

                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function($excel) use($final_records,$invalid_cards,$processed,$type) {
                                        $excel->setTitle(trans('common::application.beneficiary_family_sheet'));
                                        $excel->setDescription(trans('common::application.beneficiary_family_sheet'));

                                        if(sizeof($final_records) > 0){
                                            $excel->sheet(trans('common::application.beneficiary_family_sheet'), function($sheet) use($final_records,$type) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);


                                                if($type == 1){
                                                    $sheet->getStyle("A1:I1")->applyFromArray(['font' => ['bold' => true]]);
                                                }else{
                                                    $sheet->getStyle("A1:R1")->applyFromArray(['font' => ['bold' => true]]);
                                                }

                                                $sheet->setCellValue('A1',trans('common::application.#'));
                                                $sheet->setCellValue('B1',trans('common::application.id_card_number'));
                                                $sheet->setCellValue('C1',trans('common::application.full_name'));
                                                $sheet->setCellValue('D1',trans('common::application.gender'));
                                                $sheet->setCellValue('E1',trans('common::application.marital_status'));
                                                $sheet->setCellValue('F1',trans('common::application.address'));
                                                $sheet->setCellValue('G1',trans('common::application.mobile'));
                                                $sheet->setCellValue('H1',trans('common::application.phone'));
                                                $sheet->setCellValue('I1',trans('common::application.aid_status'));

                                                if($type != 1){
                                                    $sheet->setCellValue('J1',trans('common::application.id_card_number').' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('K1',trans('common::application.kinship_name') .' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('L1',trans('common::application.full_name').' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('M1',trans('common::application.gender').' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('N1',trans('common::application.marital_status').' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('O1',trans('common::application.birthday').' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('P1',trans('common::application.prev_family_name').' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('Q1',trans('common::application.mother_name') .' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('R1',trans('common::application.aid_status') .' ('.trans('common::application.related') .' )');
                                                }

                                                $z= 2;
                                                foreach($final_records as $k=>$main ) {

                                                    $main = (Object) $main;
                                                    $v = (Object) $main->related;

                                                    $sheet->setCellValue('A' . $z, $k+1);

                                                    $sheet->setCellValue('B' . $z ,GovServices::refDashIfNull($main,'IDNO'));
                                                    $sheet->setCellValue('C' . $z ,GovServices::refDashIfNull($main,'FULLNAME'));
                                                    $sheet->setCellValue('D' . $z ,GovServices::refDashIfNull($main,'SEX'));
                                                    $sheet->setCellValue('E' . $z ,GovServices::refDashIfNull($main,'SOCIAL_STATUS'));
                                                    $sheet->setCellValue('F'.$z,
                                                        ( (is_null($main->CI_REGION) ||$main->CI_REGION == ' ' ) ? ' ' :$main->CI_REGION) .' '.
                                                        ( (is_null($main->CI_CITY) ||$main->CI_CITY == ' ' ) ? ' ' :$main->CI_CITY) .' '.
                                                        ( (is_null($main->STREET_ARB) ||$main->STREET_ARB == ' ' ) ? ' ' :$main->STREET_ARB)
                                                    );
                                                    $sheet->setCellValue('G' . $z ,GovServices::refDashIfNull($main,'MOBILE'));
                                                    $sheet->setCellValue('H' . $z ,GovServices::refDashIfNull($main,'TEL'));
                                                    $sheet->setCellValue('I' . $z ,GovServices::refDashIfNull($main,'social_affairs_status'));

                                                    if($type != 1){
                                                        if(!is_null($main->related)){
                                                            $sheet->setCellValue('J' . $z ,GovServices::refDashIfNull($v,'IDNO_RELATIVE'));
                                                            $sheet->setCellValue('K' . $z ,GovServices::refDashIfNull($v,'RELATIVE_DESC'));
                                                            $sheet->setCellValue('L' . $z ,GovServices::refDashIfNull($v,'FULLNAME'));
                                                            $sheet->setCellValue('M' . $z ,GovServices::refDashIfNull($v,'SEX'));
                                                            $sheet->setCellValue('N' . $z ,GovServices::refDashIfNull($v,'SOCIAL_STATUS'));
                                                            $sheet->setCellValue('O' . $z ,GovServices::refDashIfNull($v,'BIRTH_DT'));
                                                            $sheet->setCellValue('P' . $z ,GovServices::refDashIfNull($v,'PREV_LNAME_ARB'));
                                                            $sheet->setCellValue('Q' . $z ,GovServices::refDashIfNull($v,'MOTHER_ARB'));
                                                            $sheet->setCellValue('R' . $z ,GovServices::refDashIfNull($v,'social_affairs_status'));
                                                        } else{
                                                            $sheet->setCellValue('J'.$z,'-');
                                                            $sheet->setCellValue('K'.$z,'-');
                                                            $sheet->setCellValue('L'.$z,'-');
                                                            $sheet->setCellValue('M'.$z,'-');
                                                            $sheet->setCellValue('N'.$z,'-');
                                                            $sheet->setCellValue('O'.$z,'-');
                                                            $sheet->setCellValue('P'.$z,'-');
                                                            $sheet->setCellValue('Q'.$z,'-');
                                                            $sheet->setCellValue('R'.$z,'-');
                                                        }
                                                    }

                                                    $z++;
                                                }
                                            });
                                        }
                                        if(sizeof($invalid_cards) > 0){
                                            $excel->sheet(trans('common::application.un_beneficiary'), function($sheet) use($invalid_cards) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;
                                                foreach($invalid_cards as $v){
                                                    $sheet->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }

                                            });
                                        }

                                    })->store('xlsx', storage_path('tmp/'));

                                    \Log\Model\Log::saveNewLog('CARD_CKECK', trans('common::application.The user has checked the identification numbers fetching data from the civil register, the number of records checked') .$total);

                                    if( $total == ( $success +$duplicated)){
                                        return response()->json(['status' => 'success','msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.All numbers successfully checked') ,'download_token' => $token]);
                                    }
                                    else{
                                        return response()->json([ 'status' => 'success',
                                            'msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.Some numbers were successfully checked, as the total number') .' :  '. $total . ' ,  '.
                                                trans('common::application.The number of names that have fetched data') .' :  ' .$success . ' ,  ' .
                                                trans('common::application.Number of unregistered names benefiting') .' :  ' .sizeof($invalid_cards) . ' ,  ' .
                                                trans('common::application.Number of duplicates').' :  '.$duplicated ,
                                            'download_token' => $token ]);
                                    }

                                }
                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);

                    }
                }

                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);

            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }

    // update citizen benefit of social affairs status using excel sheet according card number
    public function saveSocialAffairsStatus(Request $request)
    {
        $this->authorize('socialAffairs', \Common\Model\AidsCases::class);
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $validFile=true;
        $duplicated =0;
        $success =0;

        $final_records=[];
        $invalid_cards=[];
        $not_registered=[];
        $processed=[];

        $importFile=$request->file ;
        if ($importFile->isValid()) {
            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();

                    if(sizeof($keys) >0 ){
                        $validFile = false;
                        foreach ($keys as $item) {
                            if ( $item == 'rkm_alhoy' ) {
                                $validFile = true;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {
                                $CreateOrUpdate_=[];
                                foreach ($records as $key =>$value) {
                                    if(isset($value['rkm_alhoy'])){
                                        if($value['rkm_alhoy'] != '' && $value['rkm_alhoy'] != ' '){
                                            if(in_array($value['rkm_alhoy'],$processed)){
                                                $duplicated++;
                                            }
                                            else{
                                                $card= (int) $value['rkm_alhoy'];

                                                if(strlen($card)  <= 9) {
                                                    $person = Person::where('id_card_number',$card)->first();
                                                    if(is_null($person)){
                                                        $not_registered[]=$value['rkm_alhoy'];
                                                    }else{
                                                        $input = ['aid_value'=>0,'aid_take'=> 0 ];
                                                        $socialAffairs=GovServices::socialAffairsStatus($card);
                                                        if($socialAffairs['status'] != false){
                                                            $socialAffairs_=$socialAffairs['row'];
                                                            $input['aid_take'] = 1;
                                                            $input['currency_id'] = 4;
                                                            $input['aid_value'] = $socialAffairs_->AID_AMOUNT;
                                                        }
                                                        $CreateOrUpdate_[]=['person_id'=>$person->id ,'input' => $input];
                                                        $processed[]=$value['rkm_alhoy'];
                                                        $success++;
                                                    }
                                                }else{
                                                    $invalid_cards[]=$value['rkm_alhoy'];
                                                }
                                            }
                                        }
                                    }
                                }


                                if(sizeof($CreateOrUpdate_) > 0){
                                    foreach ($CreateOrUpdate_ as $key =>$value) {
                                        PersonAid::CreateOrUpdate($value['person_id'],1,$value['input'],10);
                                    }
                                }
                                if($total == sizeof($invalid_cards)){
                                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.All names entered are incorrect')  ]);
                                }else if($total == sizeof($not_registered)){
                                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.All names entered are not registered in the system')  ]);
                                }
                                else{

                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function($excel) use($final_records,$invalid_cards,$processed,$not_registered) {
                                        $excel->setTitle(trans('common::application.beneficiary_family_sheet'));
                                        $excel->setDescription(trans('common::application.beneficiary_family_sheet'));

                                        if(sizeof($processed) > 0){
                                            $excel->sheet(trans('common::application.beneficiary_family_sheet'), function($sheet) use($processed) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;
                                                foreach($processed as $v){
                                                    $sheet->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }

                                            });
                                        }
                                        if(sizeof($not_registered) > 0){
                                            $excel->sheet(trans('common::application.not_registered'), function($sheet) use($not_registered) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;
                                                foreach($not_registered as $v){
                                                    $sheet->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }

                                            });
                                        }
                                        if(sizeof($invalid_cards) > 0){
                                            $excel->sheet(trans('common::application.invalid_card'), function($sheet) use($invalid_cards) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;
                                                foreach($invalid_cards as $v){
                                                    $sheet->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }

                                            });
                                        }

                                    })->store('xlsx', storage_path('tmp/'));

                                    \Log\Model\Log::saveNewLog('CARD_CKECK', trans('common::application.The user has checked the identification numbers fetching data from the civil register, the number of records checked') .$total);

                                    if( $total == ( $success +$duplicated)){
                                        return response()->json(['status' => 'success','msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.All numbers successfully checked') ,'download_token' => $token]);
                                    }
                                    else{
                                        return response()->json([ 'status' => 'success',
                                            'msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.Some numbers were successfully checked, as the total number') .' :  '. $total . ' ,  '.
                                                trans('common::application.The number of names whose status has been modified') .' :  ' .$success . ' ,  ' .
                                                trans('common::application.The number of incorrect or not registered ID numbers in the system') .' :  ' .sizeof($not_registered) . ' ,  ' .
                                                trans('common::application.The number of incorrect or not registered ID numbers in the system').' :  ' .sizeof($invalid_cards) . ' ,  ' .
                                                trans('common::application.Number of duplicates').' :  '.$duplicated ,
                                            'download_token' => $token ]);
                                    }

                                }

                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);

                    }
                }

                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);

            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }

    /*********************** socialAffairsReceipt ***********************/
    // filter citizen benefit of social affairs according ( card or name )
    public function socialAffairsReceiptFilter(Request $request)
    {

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $this->authorize('socialAffairs', \Common\Model\AidsCases::class);

        $records = [];
        $using = $request->using;
        $user = \Auth::user();

        if($using == 1){
            $card = $request->id_card_number;
            CardLog::saveCardLog($user->id,$card);
            $row = GovServices::byCard($card);
            if($row['status'] != false){
                $temp = $row['row'];
                $records[] = $temp;
            }
        }
        else{
            $row = GovServices::byName($request->all());
            if($row['status'] != false){
                $records= $row['row'];
            }
        }

        if(sizeof($records) > 0 ){
            $social_affairs = [];
            foreach ($records as $key=>$value){
                if(!isset($value->MOBILE)){
                    $value->MOBILE=null;
                    $value->TEL=null;
                    $contact_data = GovServices::ssoInfo($value->IDNO);
                    if($contact_data['status'] != false){
                        $value->MOBILE=GovServices::ObjectValue($contact_data['row'],'USERMOBILE');
                        $value->TEL=GovServices::ObjectValue($contact_data['row'],'USERTELEPHONE');
                    }
                }

                $socialAffairsRecipt=GovServices::socialAffairsReceipt($value->IDNO);
                $social_affairs_receipt=[];
                if($socialAffairsRecipt['status'] == true) {
                    $social_affairs_receipt = $socialAffairsRecipt['row'];
                }

                foreach ($social_affairs_receipt as $k1=>$v1){
                    $value->social_affairs = $v1;
                    $social_affairs[]=$value;
                }
                CloneGovernmentPersons::saveNew($value);

            }
            return response()->json(['status'=>true,'items'=>$social_affairs]);
        }

        return response()->json(['status'=>false]);

    }

    // get citizen benefit of social affairs using card
    public function socialAffairsReceipt(Request $request)
    {
        $this->authorize('socialAffairs', \Common\Model\AidsCases::class);


        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $card = $request->id_card_number;

        $person = null;
        $row = GovServices::byCard($card);
        if($row['status'] == false){
            return response()->json(['status'=>false,'msg'=>$row['message']]);
        }

        $record=$row['row'];

        $record->social_affairs= false;
        $record->social_affairs_status= trans('common::application.un_beneficiary');
        $record->AID_CLASS=null;
        $record->AID_TYPE=null;
        $record->AID_AMOUNT=null;
        $record->AID_PERIODIC=null;
        $record->AID_SOURCE=null;
        $record->ST_BENEFIT_DATE=null;
        $record->END_BENEFIT_DATE=null;

        $socialAffairs=GovServices::socialAffairsStatus($card);
        if($socialAffairs['status'] != false){
            $socialAffairs_=$socialAffairs['row'];
            $record->social_affairs= true;
            $record->social_affairs_status=  trans('common::application.beneficiary_');
            $record->AID_CLASS=$socialAffairs_->AID_CLASS;
            $record->AID_TYPE=$socialAffairs_->AID_TYPE;
            $record->AID_AMOUNT=$socialAffairs_->AID_AMOUNT;
            $record->AID_PERIODIC=$socialAffairs_->AID_PERIODIC;
            $record->AID_SOURCE=$socialAffairs_->AID_SOURCE;
            $record->ST_BENEFIT_DATE=$socialAffairs_->ST_BENEFIT_DATE;
            $record->END_BENEFIT_DATE=$socialAffairs_->END_BENEFIT_DATE;
        }

        $record->social_affairs_receipt= null;
        $socialAffairsRecipt=GovServices::socialAffairsReceipt($card);
        if($socialAffairsRecipt['status'] != false) {
            $record->social_affairs_receipt = $socialAffairsRecipt['row'];
        }


        $keys = ['parent','childrens','wives'];
        foreach ($keys as $key){
            foreach ($record->$key as $k=>$v){
                $v->social_affairs= false;
                $v->social_affairs_status= trans('common::application.un_beneficiary');
                $v->AID_CLASS=null;
                $v->AID_TYPE=null;
                $v->AID_AMOUNT=null;
                $v->AID_PERIODIC=null;
                $v->AID_SOURCE=null;
                $v->ST_BENEFIT_DATE=null;
                $v->END_BENEFIT_DATE=null;

                $socialAffairs=GovServices::socialAffairsStatus($v->IDNO_RELATIVE);
                if($socialAffairs['status'] != false){
                    $socialAffairs_=$socialAffairs['row'];
                    $v->social_affairs= true;
                    $v->social_affairs_status=  trans('common::application.beneficiary_');
                    $v->AID_CLASS=$socialAffairs_->AID_CLASS;
                    $v->AID_TYPE=$socialAffairs_->AID_TYPE;
                    $v->AID_AMOUNT=$socialAffairs_->AID_AMOUNT;
                    $v->AID_PERIODIC=$socialAffairs_->AID_PERIODIC;
                    $v->AID_SOURCE=$socialAffairs_->AID_SOURCE;
                    $v->ST_BENEFIT_DATE=$socialAffairs_->ST_BENEFIT_DATE;
                    $v->END_BENEFIT_DATE=$socialAffairs_->END_BENEFIT_DATE;
                }
            }
        }

        CloneGovernmentPersons::saveNew($record);
        $record->CASE_STATUS=false;
        $record->cases_list=[];
        $record->person_id = Person::getPersonId([$card]);
        if (CaseModel::hasPermission('reports.case.RelayCitizenRepository')) {
            $record->cases_list = Person::getPersonCardCases([$card]);
        }

        if($request->action =='xlsx'){

            if (sizeof($record->all_relatives) == 0 ){
                $record[] = (Object)[
                    'IDNO_RELATIVE' => '-' ,
                    'kinship_name' => '-' ,
                    'FNAME_ARB' => '-' ,
                    'SNAME_ARB' => '-' ,
                    'TNAME_ARB' => '-' ,
                    'LNAME_ARB' => '-' ,
                    'SEX' => '-' ,
                    'SOCIAL_STATUS' => '-' ,
                    'BIRTH_DT' => '-' ,
                    'DETH_DT' => '-' ,
                    'BIRTH_PMAIN' => '-' ,
                    'BIRTH_PSUB' => '-' ,
                    'PREV_LNAME_ARB' => '-' ,
                    'MOTHER_ARB' => '-' ,
                    'STATUS' => '-' ,
                ];
            }else{
                foreach ($record->all_relatives as $k=>$v){
                    $v->social_affairs= false;
                    $v->social_affairs_status= trans('common::application.un_beneficiary');
                    $v->AID_CLASS=null;
                    $v->AID_TYPE=null;
                    $v->AID_AMOUNT=null;
                    $v->AID_PERIODIC=null;
                    $v->AID_SOURCE=null;
                    $v->ST_BENEFIT_DATE=null;
                    $v->END_BENEFIT_DATE=null;

                    $socialAffairs=GovServices::socialAffairsStatus($v->IDNO_RELATIVE);
                    if($socialAffairs['status'] != false){
                        $socialAffairs_=$socialAffairs['row'];
                        $v->social_affairs= true;
                        $v->social_affairs_status=  trans('common::application.beneficiary_');
                        $v->AID_CLASS=$socialAffairs_->AID_CLASS;
                        $v->AID_TYPE=$socialAffairs_->AID_TYPE;
                        $v->AID_AMOUNT=$socialAffairs_->AID_AMOUNT;
                        $v->AID_PERIODIC=$socialAffairs_->AID_PERIODIC;
                        $v->AID_SOURCE=$socialAffairs_->AID_SOURCE;
                        $v->ST_BENEFIT_DATE=$socialAffairs_->ST_BENEFIT_DATE;
                        $v->END_BENEFIT_DATE=$socialAffairs_->END_BENEFIT_DATE;
                    }
                }
                $repeated = $record->all_relatives;
            }

            $token = md5(uniqid());
            \Excel::create('export_' . $token, function($excel) use($record,$repeated) {
                $excel->setTitle(trans('common::application.family_sheet'));
                $excel->setDescription(trans('common::application.family_sheet'));
                $excel->sheet(trans('common::application.family_sheet'), function($sheet) use($record,$repeated) {

                    $sheet->setRightToLeft(true);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);

                    $sheet->getStyle("A1:R1")->applyFromArray(['font' => ['bold' => true]]);
                    $sheet->setCellValue('A1',trans('common::application.#'));
                    $sheet->setCellValue('B1',trans('common::application.full_name'));
                    $sheet->setCellValue('C1',trans('common::application.id_card_number'));
                    $sheet->setCellValue('D1',trans('common::application.gender'));
                    $sheet->setCellValue('E1',trans('common::application.marital_status'));
                    $sheet->setCellValue('F1',trans('common::application.address'));
                    $sheet->setCellValue('G1',trans('common::application.mobile'));
                    $sheet->setCellValue('H1',trans('common::application.phone'));
                    $sheet->setCellValue('I1',trans('common::application.aid_status'));
                    $sheet->setCellValue('J1',trans('common::application.id_card_number').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('K1',trans('common::application.kinship_name') .' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('L1',trans('common::application.full_name').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('M1',trans('common::application.gender').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('N1',trans('common::application.marital_status').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('O1',trans('common::application.birthday').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('P1',trans('common::application.prev_family_name').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('Q1',trans('common::application.mother_name') .' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('R1',trans('common::application.aid_status') .' ('.trans('common::application.related') .' )');

                    $z= 2;
                    foreach($repeated as $k=>$v )
                    {
                        $sheet->setCellValue('A'.$z,$k+1);
                        $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($record,'FULLNAME'));
                        $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($record,'IDNO'));
                        $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($record,'SEX'));
                        $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($record,'SOCIAL_STATUS'));
                        $sheet->setCellValue('F'.$z,
                            ( (is_null($record->CI_REGION) ||$record->CI_REGION == ' ' ) ? ' ' :$record->CI_REGION) .' '.
                            ( (is_null($record->CI_CITY) ||$record->CI_CITY == ' ' ) ? ' ' :$record->CI_CITY) .' '.
                            ( (is_null($record->STREET_ARB) ||$record->STREET_ARB == ' ' ) ? ' ' :$record->STREET_ARB)
                        );

                        $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($record,'MOBILE'));
                        $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($record,'TEL'));
                        $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($record,'social_affairs_status'));
                        $sheet->setCellValue('J'.$z,GovServices::refDashIfNull($v,'IDNO_RELATIVE'));
                        $sheet->setCellValue('K'.$z,GovServices::refDashIfNull($v,'RELATIVE_DESC'));
                        $sheet->setCellValue('L'.$z,GovServices::refDashIfNull($v,'FULLNAME'));
                        $sheet->setCellValue('M'.$z,GovServices::refDashIfNull($v,'SEX'));
                        $sheet->setCellValue('N'.$z,GovServices::refDashIfNull($v,'SOCIAL_STATUS'));
                        $sheet->setCellValue('O'.$z,GovServices::refDashIfNull($v,'BIRTH_DT'));
                        $sheet->setCellValue('P'.$z,GovServices::refDashIfNull($v,'PREV_LNAME_ARB'));
                        $sheet->setCellValue('Q'.$z,GovServices::refDashIfNull($v,'MOTHER_ARB'));
                        $sheet->setCellValue('R'.$z,GovServices::refDashIfNull($v,'social_affairs_status'));
                        $z++;
                    }

                });
                if(sizeof($record->social_affairs_receipt) > 0){
                    $excel->sheet(trans('common::application.social affairs'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:G1")->applyFromArray(['font' => ['bold' => true]]);

                        $start_from =1;
                        $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                        $sheet->setCellValue('B' . $start_from ,trans('common::application.SRV_INF_NAME'));
                        $sheet->setCellValue('C' . $start_from ,trans('common::application.ORG_NM_MON'));
                        $sheet->setCellValue('D' . $start_from ,trans('common::application.SRV_TYPE_MAIN_NM'));
                        $sheet->setCellValue('E' . $start_from ,trans('common::application.CURRENCY'));
                        $sheet->setCellValue('F' . $start_from ,trans('common::application.RECP_AID_AMOUNT'));
                        $sheet->setCellValue('G' . $start_from ,trans('common::application.RECP_DELV_DT'));

                        $z= 2;
                        foreach($record->social_affairs_receipt as $k => $v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,GovServices::refDashIfNull($v,'SRV_INF_NAME'));
                            $sheet->setCellValue('C'.$z,GovServices::refDashIfNull($v,'ORG_NM_MON'));
                            $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v,'SRV_TYPE_MAIN_NM'));
                            $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v,'CURRENCY'));
                            $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v,'RECP_AID_AMOUNT'));
                            $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v,'RECP_DELV_DT'));
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }
                        $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G'];
                        foreach($keys as $key ) {
                            $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                        }

                    });
                }

                if (CaseModel::hasPermission('reports.case.RelayCitizenRepository') && sizeof($record->cases_list) > 0) {
                    $excel->sheet(trans('common::application.cases_list'), function($sheet) use($record) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:G1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->setCellValue('A1',trans('common::application.#'));
                        $sheet->setCellValue('B1',trans('common::application.case_category_type'));
                        $sheet->setCellValue('C1',trans('common::application.organization'));
                        $sheet->setCellValue('D1',trans('common::application.status'));
                        $sheet->setCellValue('E1',trans('common::application.visitor'));
                        $sheet->setCellValue('F1',trans('common::application.visited_at'));
                        $sheet->setCellValue('G1',trans('common::application.created_at'));

                        $z= 2;
                        foreach($record->cases_list as $k=>$v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,(is_null($v->category_name) ||$v->category_name == ' ' ) ? '-' :$v->category_name);
                            $sheet->setCellValue('C'.$z,(is_null($v->organization_name) ||$v->organization_name == ' ' ) ? '-' :$v->organization_name);
                            $sheet->setCellValue('D'.$z,(is_null($v->status) ||$v->status == ' ' ) ? '-' :$v->status);
                            $sheet->setCellValue('E'.$z,(is_null($v->visitor) ||$v->visitor == ' ' ) ? '-' :$v->visitor);
                            $sheet->setCellValue('F'.$z,(is_null($v->visited_at) ||$v->visited_at == ' ' ) ? '-' :$v->visited_at);
                            $sheet->setCellValue('G'.$z,(is_null($v->created_date) ||$v->created_date == ' ' ) ? '-' :$v->created_date);
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }

                    });

                }

            })->store('xlsx', storage_path('tmp/'));
            return response()->json(['download_token' => $token]);
        }
        elseif($request->action =='pdf') {

            $full_name=  GovServices::refDashIfNull($record,'FULLNAME');
            $en_name=  GovServices::refDashIfNull($record,'ENG_NAME');
            $mother_name=  GovServices::refDashIfNull($record,'MOTHER_ARB');
            $prev_family_name=  GovServices::refDashIfNull($record,'PREV_LNAME_ARB');

            $gender=  GovServices::refDashIfNull($record,'SEX');
            $marital_status=  GovServices::refDashIfNull($record,'SOCIAL_STATUS');
            $birthday=  GovServices::refDashIfNull($record,'BIRTH_DT');
            $death_date=  GovServices::refDashIfNull($record,'DETH_DT');
            $birth_place_main=  GovServices::refDashIfNull($record,'BIRTH_PMAIN');
            $birth_place_sub=  GovServices::refDashIfNull($record,'BIRTH_PMAIN');

            $address = $record->CI_REGION .' - ' .$record->CI_CITY  . ' - ' . $record->STREET_ARB;

            $family_cnt=  GovServices::refDashIfNull($record,'family_cnt');
            $female_live=  GovServices::refDashIfNull($record,'female_live');
            $male_live=  GovServices::refDashIfNull($record,'male_live');
            $spouses=  GovServices::refDashIfNull($record,'spouses');

            $primary_mobile=  GovServices::refDashIfNull($record,'MOBILE');
            $phone=  GovServices::refDashIfNull($record,'TEL');

            $GOV_NAME=  GovServices::refDashIfNull($record,'GOV_NAME');
            $CITY_NAME=  GovServices::refDashIfNull($record,'CITY_NAME');
            $PART_NAME=  GovServices::refDashIfNull($record,'PART_NAME');
            $ADDRESS_DET=  GovServices::refDashIfNull($record,'ADDRESS_DET');

            $socialAffairsRecipt=GovServices::socialAffairsReceipt($card);
            $record->social_affairs_receipt=[];
            if($socialAffairsRecipt['status'] == true) {
                $record->social_affairs_receipt = $socialAffairsRecipt['row'];
            }

            PDF::SetTitle($full_name);
            PDF::SetFont('aealarabiya', '', 18);
            PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);
            PDF::SetMargins(10, 10, 10);
            PDF::SetAutoPageBreak(TRUE);
            $lg = Array();
            $lg['a_meta_charset'] = 'UTF-8';
            $lg['a_meta_dir'] = 'rtl';
            $lg['a_meta_language'] = 'fa';
            $lg['w_page'] = 'page';
            PDF::setLanguageArray($lg);
            PDF::setCellHeightRatio(1.5);
            $pageNo = 1;
            $totalPageCount = 2;

            if(sizeof($record->social_affairs_receipt) > 0) {
                $count = count($record->social_affairs_receipt);
                $totalPageCount += ceil($count / 25);
            }

            $token = md5(uniqid());
            $img = base_path('storage/app/emptyUser1.png');
            $fileContents = file_get_contents($record->photo_url);
            $disk = "local";
            $filename = $token.'.png'; // The name you want to give to the downloaded image
            if ($fileContents !== false) {
                Storage::disk($disk)->put('documents/' . $filename, $fileContents);
                $img = storage_path('app/documents/').$token.'.png';
            }

            // social affairs
            $social_affairs_status = trans('common::application.un_beneficiary');
            $aid_class = $aid_type = $aid_amount = $aid_source = $aid_periodic = $aid_st_ben_date = $aid_end_ben_date = '-';
            if(!is_null($record->social_affairs)){
                $social_affairs_status = GovServices::refDashIfNull($record,'social_affairs_status');
                $aid_class = GovServices::refDashIfNull($record,'AID_CLASS');
                $aid_type = GovServices::refDashIfNull($record,'AID_TYPE');
                $aid_amount = GovServices::refDashIfNull($record,'AID_AMOUNT');
                $aid_source = GovServices::refDashIfNull($record,'AID_SOURCE');
                $aid_periodic = GovServices::refDashIfNull($record,'AID_PERIODIC');
                $aid_st_ben_date = GovServices::refDashIfNull($record,'ST_BENEFIT_DATE');
                $aid_end_ben_date = GovServices::refDashIfNull($record,'END_BENEFIT_DATE');
            }
            $html = '<!doctype html><html lang="ar">
                 <head>
                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                 <style>
                     .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                     table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                     * , body {font-weight:normal !important; font-size: 10px;}
                     .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                     .bolder-content{text-align: right !important; vertical-align: middle!important;}
                     .vertical-middle{ vertical-align: middle !important;}
                </style>
                </head>
                 <body style="font-weight: normal !important;">';

            $html.= '<h2 style="text-align:center;"> '.$full_name.'</h2>';
            $html .= '<table border="1" style="border: 1px solid black; box-sizing:border-box;">';

            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.full_name').' </b></td>
                       <td width="330" align="center" >'.$full_name.'</td>
                        <td width="98" align="center" rowspan="6" style="background-color: white" >'
                .'<img src="'.$img.'" alt="" height="140" />'.
                '</td>
                       
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.en_name').' </b></td>
                       <td width="330" align="center" >'.$en_name.'</td>
                    
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.id_card_number').' </b></td>
                       <td width="115" align="center" >'.$card.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.gender').' </b></td>
                       <td width="115" align="center" >'.$gender.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.birthday').' </b></td>
                       <td width="115" align="center" >'.$birthday.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.marital_status').' </b></td>
                       <td width="115" align="center" >'.$marital_status.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.primary_mobile').' </b></td>
                       <td width="115" align="center" >'.$primary_mobile.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.phone').' </b></td>
                       <td width="115" align="center" >'.$phone.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.prev_family_name').' </b></td>
                       <td width="115" align="center" >'.$prev_family_name.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.family_cnt').' </b></td>
                       <td width="115" align="center" >'.$family_cnt.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.spouses').' </b></td>
                       <td width="70" align="center" >'.$spouses.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.male_live').' </b></td>
                       <td width="70" align="center" >'.$male_live.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.female_live').' </b></td>
                       <td width="70" align="center" >'.$female_live.'</td>
                   </tr>';
            $html .='</table>';

            $html .= '<table border="1" style="border: 1px solid black; box-sizing:border-box;">';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.address').' </b></td>
                       <td width="428" align="center" >'.$address.'</td>
                   </tr>';

            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="128" align="center"><b>'.trans('common::application.aid_status') .'('.trans('common::application.social affairs').') </b></td>
                       <td width="400" align="center" >'.$social_affairs_status.'</td>
                   </tr>';

            $html .= ' <tr> 
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_class').'</b></td>
                       <td width="96" align="center">'.$aid_class.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_type').'</b></td>
                       <td width="96" align="center">'.$aid_type.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_amount').'</b></td>
                       <td width="96" align="center">'.$aid_amount.'</td>
                    </tr>';

            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_source').'</b></td>
                       <td width="184" align="center">'.$aid_source.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_periodic').'</b></td>
                       <td width="184" align="center">'.$aid_periodic.'</td> 
                   </tr>';

            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_st_ben_date').'</b></td>
                       <td width="184" align="center">'.$aid_st_ben_date.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.aid_end_ben_date').'</b></td>
                       <td width="184" align="center">'.$aid_end_ben_date.'</td> 
                   </tr>';
            $html .='</table>';

//                parents
            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';

            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.parents').'
                        </b></td>
                   </tr>';
            if (sizeof($record->parent) > 0) {
                $html .= ' <tr> 
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> ' . trans('common::application.id_card_number') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="123" align="center"><b> ' . trans('common::application.name') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="64" align="center"><b> ' . trans('common::application.marital_status') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="64" align="center"><b> ' . trans('common::application.birthday') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="64" align="center"><b> ' . trans('common::application.death_date') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> ' . trans('common::application.mobile') . '</b></td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> ' . trans('common::application.aid_status') . '</b></td>
                </tr>';

            }
            $z = 1;

//                parents
            foreach($record->parent as $row) {
                $html .= ' <tr> 
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.$z.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'IDNO_RELATIVE').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="123" align="center"><b> '. GovServices::refDashIfNull($row,'FULLNAME') .'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="64" align="center"><b> '.GovServices::refDashIfNull($row,'SOCIAL_STATUS').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="64" align="center"><b> '.GovServices::refDashIfNull($row,'BIRTH_DT').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="64" align="center"><b> '.GovServices::refDashIfNull($row,'DETH_DT').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="70" align="center"><b> '.GovServices::refDashIfNull($row,'MOBILE').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="70" align="center"><b> '.GovServices::refDashIfNull($row,'social_affairs_status').'</b></td>
                    </tr>';
                $z++;
            }
            if (sizeof($record->parent) == 0){
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
            }

            $html .='</table>';

            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.wives or husbands').'
                        </b></td>
                   </tr>';

            if (sizeof($record->wives) > 0){
                $html .= ' <tr> 
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.id_card_number').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="123" align="center"><b> '.trans('common::application.name').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="64" align="center"><b> '.trans('common::application.marital_status').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="64" align="center"><b> '.trans('common::application.birthday').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="64" align="center"><b> '.trans('common::application.death_date').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.mobile').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> ' . trans('common::application.aid_status') . '</b></td>
          </tr>';
            }

            $z = 1;
            foreach($record->wives as $row) {

                $html .= ' <tr> 
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.$z.'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'IDNO_RELATIVE').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="123" align="center"><b> '. GovServices::refDashIfNull($row,'FULLNAME') .'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="64" align="center"><b> '.GovServices::refDashIfNull($row,'SOCIAL_STATUS').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="64" align="center"><b> '.GovServices::refDashIfNull($row,'BIRTH_DT').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="64" align="center"><b> '.GovServices::refDashIfNull($row,'DETH_DT').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="70" align="center"><b> '.GovServices::refDashIfNull($row,'MOBILE').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="70" align="center"><b> '.GovServices::refDashIfNull($row,'social_affairs_status').'</b></td>
     </tr>';
                $z++;
            }

            if (sizeof($record->wives) == 0){
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
            }
            $html .='</table>';

// childs
            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.childs').'
                        </b></td>
                   </tr>';

            if (sizeof($record->childrens) > 0){
                $html .= ' <tr> 
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.id_card_number').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.name').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="30" align="center"><b> '.trans('common::application.gender').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.marital_status').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.birthday').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> '.trans('common::application.death_date').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="65" align="center"><b> '.trans('common::application.mobile').'</b></td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="60" align="center"><b> ' . trans('common::application.aid_status') . '</b></td>
 </tr>';
            }

            $z = 1;
            foreach($record->childrens as $row) {
                $html .= ' <tr> 
                           <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.$z.'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'IDNO_RELATIVE').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="120" align="center"><b> '. GovServices::refDashIfNull($row,'FULLNAME') .'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="30" align="center"><b> '.GovServices::refDashIfNull($row,'SEX').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'SOCIAL_STATUS').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'BIRTH_DT').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'DETH_DT').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="65" align="center"><b> '.GovServices::refDashIfNull($row,'MOBILE').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="60" align="center"><b> '.GovServices::refDashIfNull($row,'social_affairs_status').'</b></td>
                    </tr>';
                $z++;
            }
            if (sizeof($record->childrens) == 0){
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
            }
            $html .='</table>';


            $html .='</body></html>';

            PDF::AddPage('P','A4');
            PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
            });
            $pageNo++;
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);



// cases_list
            $html = '<!doctype html><html lang="ar">
                 <head>
                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                 <style>
                     .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                     table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                     * , body {font-weight:normal !important; font-size: 10px;}
                     .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                     .bolder-content{text-align: right !important; vertical-align: middle!important;}
                     .vertical-middle{ vertical-align: middle !important;}
                </style>
                </head>
                 <body style="font-weight: normal !important;">';

            $html .='<div style="height: 5px; color: white"> - </div>';

// cases_list
            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="535" align="center"><b>  
                        '.trans('common::application.cases list').'
                        </b></td>
                   </tr>';

            if (sizeof($record->cases_list) > 0){
                $html .= ' <tr> 
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="75" align="center"><b> '.trans('common::application.case_category_type').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="150" align="center"><b> '.trans('common::application.organization name').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="40" align="center"><b> '.trans('common::application.status').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="89" align="center"><b> '.trans('common::application.visitor').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.visited_at').'</b></td>
                               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.created_at').'</b></td>
                           </tr>';
            }


            $z = 1;
            foreach($record->cases_list as $row) {
                $html .= ' <tr> 
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.$z.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="75" align="center"><b> '.$row->category_name.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="150" align="center"><b> '.$row->organization_name.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="40" align="center"><b> '.$row->status.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="89" align="center"><b> '.$row->visitor.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="80" align="center"><b> '.$row->visited_at.'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="80" align="center"><b> '.$row->created_dt.'</b></td>
                    </tr>';
                $z++;
            }
            if (sizeof($record->cases_list) == 0){
                $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;color:#000000;" width="535" align="center"><b>  
                        <br><br>'.trans('common::application.there is no data to show').'<br><br>
                        </b></td>
                   </tr>';
            }
            $html .='</table>';
            $html .='</body></html>';

            PDF::AddPage('P','A4');
            PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
            });
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
            $pageNo++;


            // social_affairs_receipt
            if(sizeof($record->social_affairs_receipt) > 0){
                $count = count($record->social_affairs_receipt);
                $ceil=ceil($count/25);
                for ($x = 0; $x < $ceil; $x++) {
                    $curOffset = ($x) * 25;
                    $index=$curOffset;

                    $html = '<!doctype html><html lang="ar">
                         <head>
                         <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                         <style>
                             .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                             table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                             * , body {font-weight:normal !important; font-size: 10px;}
                             .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                             .bolder-content{text-align: right !important; vertical-align: middle!important;}
                             .vertical-middle{ vertical-align: middle !important;}
                        </style>
                        </head>
                         <body style="font-weight: normal !important;">';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';

                    $html .= ' <tr>
                               <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="534" align="center"><b>  
                                '.trans('common::application.social affairs').'
                                </b></td>
                           </tr>';


                    $html .= ' <tr> 
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="146" align="center"><b> '.trans('common::application.SRV_INF_NAME').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="145" align="center"><b> '.trans('common::application.ORG_NM_MON').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="54" align="center"><b> '.trans('common::application.SRV_TYPE_MAIN_NM').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="54" align="center"><b> '.trans('common::application.CURRENCY').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="50" align="center"><b> '.trans('common::application.RECP_AID_AMOUNT').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="65" align="center"><b> '.trans('common::application.RECP_DELV_DT').'</b></td>
                    </tr>';

                    if($index < $count) {
                        for ($z = 0; $z < 25; $z++) {
                            if($index < $count){
                                $row = $record->social_affairs_receipt[$index];
                                $html .= ' <tr> 
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.($index+1).'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="146" align="center"><b> '.GovServices::refDashIfNull($row,'SRV_INF_NAME').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="145" align="center"><b> '.GovServices::refDashIfNull($row,'ORG_NM_MON').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="54" align="center"><b> '.GovServices::refDashIfNull($row,'SRV_TYPE_MAIN_NM').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="54" align="center"><b> '.GovServices::refDashIfNull($row,'CURRENCY').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="50" align="center"><b> '.GovServices::refDashIfNull($row,'RECP_AID_AMOUNT').'</b></td>
                                   <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="65" align="center"><b> '.GovServices::refDashIfNull($row,'RECP_DELV_DT').'</b></td>
                                </tr>';
                                $index++;
                            }
                        }
                    }
                    $html .='</table>';
                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::setFooterCallback(function($pdf) use ($pageNo , $totalPageCount){
                        $footer = '<p style="text-align:center; font-size: 11px">('.'  '. $totalPageCount .' / ' . $pageNo .'  ) </h3>';
                        PDF::writeHTMLCell(0, 0,20, 290, $footer, 0, 1, 0, true, '', true);
                    });
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                    $pageNo++;

                }
            }

            $dirName = base_path('storage/app/citizen_pdf');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }

            PDF::Output($dirName.'/'.$full_name . '.pdf', 'F');

            unlink(storage_path('app/documents/'.$token.'.png'));
            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');

            $zip = new \ZipArchive;
            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                foreach ( glob( $dirName . '/*' ) as $fileName )
                {
                    $file = basename( $fileName );
                    $zip->addFile( $fileName, $file );
                }

                $zip->close();
                array_map('unlink', glob("$dirName/*.*"));
                rmdir($dirName);
            }

            return response()->json(['download_token' => $token]);

        }
        else{
            CloneGovernmentPersons::saveNew($record);
            return response()->json(['status'=>true,'data'=>$record]);
        }

    }

    // check citizen benefit of social affairs using excel sheet according card number
    public function socialAffairsReceiptCheck(Request $request)
    {
        $this->authorize('socialAffairs', \Common\Model\AidsCases::class);
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $type = $request->type;
        $validFile=true;
        $duplicated =0;
        $success =0;

        $final_records=[];
        $receipt=[];
        $invalid_cards=[];
        $processed=[];

        $importFile=$request->file ;
        if ($importFile->isValid()) {
            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();

                    if(sizeof($keys) >0 ){
                        $validFile = false;
                        foreach ($keys as $item) {
                            if ( $item == 'rkm_alhoy' ) {
                                $validFile = true;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {
                                foreach ($records as $key =>$value) {
                                    if(in_array($value['rkm_alhoy'],$processed)){
                                        $duplicated++;
                                    }
                                    else{
                                        $card= (int) $value['rkm_alhoy'];

                                        if(strlen($card)  <= 9) {
                                            $socialAffairs=GovServices::socialAffairsStatus($card);
                                            if($socialAffairs['status'] == false){
                                                $invalid_cards[]=$value['rkm_alhoy'];
                                            }else{
                                                $row = GovServices::byCard($card);
                                                if($row['status'] == true) {
                                                    $record = $row['row'];
                                                    $record->social_affairs= false;
                                                    $record->social_affairs_status= trans('common::application.un_beneficiary');
                                                    $record->AID_CLASS='-';
                                                    $record->AID_TYPE='-';
                                                    $record->AID_AMOUNT='-';
                                                    $record->AID_PERIODIC='-';
                                                    $record->AID_SOURCE='-';
                                                    $record->ST_BENEFIT_DATE='-';
                                                    $record->END_BENEFIT_DATE='-';

                                                    $socialAffairs=GovServices::socialAffairsStatus($card);
                                                    if($socialAffairs['status'] != false){
                                                        $socialAffairs_=$socialAffairs['row'];
                                                        $record->social_affairs= true;
                                                        $record->social_affairs_status=  trans('common::application.beneficiary_');
                                                        $record->AID_CLASS=$socialAffairs_->AID_CLASS;
                                                        $record->AID_TYPE=$socialAffairs_->AID_TYPE;
                                                        $record->AID_AMOUNT=$socialAffairs_->AID_AMOUNT;
                                                        $record->AID_PERIODIC=$socialAffairs_->AID_PERIODIC;
                                                        $record->AID_SOURCE=$socialAffairs_->AID_SOURCE;
                                                        $record->ST_BENEFIT_DATE=$socialAffairs_->ST_BENEFIT_DATE;
                                                        $record->END_BENEFIT_DATE=$socialAffairs_->END_BENEFIT_DATE;
                                                    }


                                                    $record->social_affairs_receipt= [];
                                                    $socialAffairsRecipt=GovServices::socialAffairsReceipt($card);
                                                    if($socialAffairsRecipt['status'] != false) {
                                                        $record->social_affairs_receipt = $socialAffairsRecipt['row'];
                                                        $receipt[]=$record;
                                                    }

                                                    CloneGovernmentPersons::saveNew($record);
                                                    $main=(array) $record;
                                                    $main['related']= null;

                                                    if($type != 1) {
                                                        if(sizeof($record->all_relatives) > 0){
                                                            foreach ($record->all_relatives as $k=>$v){
                                                                $v->social_affairs= false;
                                                                $v->social_affairs_status= trans('common::application.un_beneficiary');
                                                                $v->AID_CLASS=null;
                                                                $v->AID_TYPE=null;
                                                                $v->AID_AMOUNT=null;
                                                                $v->AID_PERIODIC=null;
                                                                $v->AID_SOURCE=null;
                                                                $v->ST_BENEFIT_DATE=null;
                                                                $v->END_BENEFIT_DATE=null;

                                                                $socialAffairs=GovServices::socialAffairsStatus($v->IDNO_RELATIVE);
                                                                if($socialAffairs['status'] != false){
                                                                    $socialAffairs_=$socialAffairs['row'];
                                                                    $v->social_affairs= true;
                                                                    $v->social_affairs_status=  trans('common::application.beneficiary_');
                                                                    $v->AID_CLASS=$socialAffairs_->AID_CLASS;
                                                                    $v->AID_TYPE=$socialAffairs_->AID_TYPE;
                                                                    $v->AID_AMOUNT=$socialAffairs_->AID_AMOUNT;
                                                                    $v->AID_PERIODIC=$socialAffairs_->AID_PERIODIC;
                                                                    $v->AID_SOURCE=$socialAffairs_->AID_SOURCE;
                                                                    $v->ST_BENEFIT_DATE=$socialAffairs_->ST_BENEFIT_DATE;
                                                                    $v->END_BENEFIT_DATE=$socialAffairs_->END_BENEFIT_DATE;
                                                                }

                                                                CloneGovernmentPersons::saveNewWithRelation($v->IDNO_RELATIVE,$v);

                                                                $main['related']= $v;
                                                                $final_records[] = $main;
                                                            }
                                                        }
                                                        else{
                                                            $final_records[] = $main;
                                                        }
                                                    }else{
                                                        $final_records[] = $main;
                                                    }

                                                    $processed[]=$value['rkm_alhoy'];
                                                    $success++;
                                                }else{
                                                    $invalid_cards[]=$value['rkm_alhoy'];
                                                }
                                            }
                                        }else{
                                            $invalid_cards[]=$value['rkm_alhoy'];
                                        }
                                    }
                                }

                                if($total == sizeof($invalid_cards)){
                                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.All names entered are not registered benefiting from social affairs') ]);
                                }else{

                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function($excel) use($final_records,$invalid_cards,$processed,$type,$receipt) {
                                        $excel->setTitle(trans('common::application.beneficiary_family_sheet'));
                                        $excel->setDescription(trans('common::application.beneficiary_family_sheet'));

                                        if(sizeof($final_records) > 0){
                                            $excel->sheet(trans('common::application.beneficiary_family_sheet'), function($sheet) use($final_records,$type) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);


                                                if($type == 1){
                                                    $sheet->getStyle("A1:I1")->applyFromArray(['font' => ['bold' => true]]);
                                                }else{
                                                    $sheet->getStyle("A1:R1")->applyFromArray(['font' => ['bold' => true]]);
                                                }

                                                $sheet->setCellValue('A1',trans('common::application.#'));
                                                $sheet->setCellValue('B1',trans('common::application.id_card_number'));
                                                $sheet->setCellValue('C1',trans('common::application.full_name'));
                                                $sheet->setCellValue('D1',trans('common::application.gender'));
                                                $sheet->setCellValue('E1',trans('common::application.marital_status'));
                                                $sheet->setCellValue('F1',trans('common::application.address'));
                                                $sheet->setCellValue('G1',trans('common::application.mobile'));
                                                $sheet->setCellValue('H1',trans('common::application.phone'));
                                                $sheet->setCellValue('I1',trans('common::application.aid_status'));

                                                if($type != 1){
                                                    $sheet->setCellValue('J1',trans('common::application.id_card_number').' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('K1',trans('common::application.kinship_name') .' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('L1',trans('common::application.full_name').' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('M1',trans('common::application.gender').' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('N1',trans('common::application.marital_status').' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('O1',trans('common::application.birthday').' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('P1',trans('common::application.prev_family_name').' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('Q1',trans('common::application.mother_name') .' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('R1',trans('common::application.aid_status') .' ('.trans('common::application.related') .' )');
                                                }

                                                $z= 2;
                                                foreach($final_records as $k=>$main ) {

                                                    $main = (Object) $main;
                                                    $v = (Object) $main->related;

                                                    $sheet->setCellValue('A' . $z, $k+1);

                                                    $sheet->setCellValue('B' . $z ,GovServices::refDashIfNull($main,'IDNO'));
                                                    $sheet->setCellValue('C' . $z ,GovServices::refDashIfNull($main,'FULLNAME'));
                                                    $sheet->setCellValue('D' . $z ,GovServices::refDashIfNull($main,'SEX'));
                                                    $sheet->setCellValue('E' . $z ,GovServices::refDashIfNull($main,'SOCIAL_STATUS'));
                                                    $sheet->setCellValue('F'.$z,
                                                        ( (is_null($main->CI_REGION) ||$main->CI_REGION == ' ' ) ? ' ' :$main->CI_REGION) .' '.
                                                        ( (is_null($main->CI_CITY) ||$main->CI_CITY == ' ' ) ? ' ' :$main->CI_CITY) .' '.
                                                        ( (is_null($main->STREET_ARB) ||$main->STREET_ARB == ' ' ) ? ' ' :$main->STREET_ARB)
                                                    );
                                                    $sheet->setCellValue('G' . $z ,GovServices::refDashIfNull($main,'MOBILE'));
                                                    $sheet->setCellValue('H' . $z ,GovServices::refDashIfNull($main,'TEL'));
                                                    $sheet->setCellValue('I' . $z ,GovServices::refDashIfNull($main,'social_affairs_status'));

                                                    if($type != 1){
                                                        if(!is_null($main->related)){
                                                            $sheet->setCellValue('J' . $z ,GovServices::refDashIfNull($v,'IDNO_RELATIVE'));
                                                            $sheet->setCellValue('K' . $z ,GovServices::refDashIfNull($v,'RELATIVE_DESC'));
                                                            $sheet->setCellValue('L' . $z ,GovServices::refDashIfNull($v,'FULLNAME'));
                                                            $sheet->setCellValue('M' . $z ,GovServices::refDashIfNull($v,'SEX'));
                                                            $sheet->setCellValue('N' . $z ,GovServices::refDashIfNull($v,'SOCIAL_STATUS'));
                                                            $sheet->setCellValue('O' . $z ,GovServices::refDashIfNull($v,'BIRTH_DT'));
                                                            $sheet->setCellValue('P' . $z ,GovServices::refDashIfNull($v,'PREV_LNAME_ARB'));
                                                            $sheet->setCellValue('Q' . $z ,GovServices::refDashIfNull($v,'MOTHER_ARB'));
                                                            $sheet->setCellValue('R' . $z ,GovServices::refDashIfNull($v,'social_affairs_status'));
                                                        } else{
                                                            $sheet->setCellValue('J'.$z,'-');
                                                            $sheet->setCellValue('K'.$z,'-');
                                                            $sheet->setCellValue('L'.$z,'-');
                                                            $sheet->setCellValue('M'.$z,'-');
                                                            $sheet->setCellValue('N'.$z,'-');
                                                            $sheet->setCellValue('O'.$z,'-');
                                                            $sheet->setCellValue('P'.$z,'-');
                                                            $sheet->setCellValue('Q'.$z,'-');
                                                            $sheet->setCellValue('R'.$z,'-');
                                                        }
                                                    }

                                                    $z++;
                                                }
                                            });
                                        }
                                        if(sizeof($receipt) > 0){
                                            $excel->sheet(trans('common::application.social affairs'), function($sheet) use($receipt) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal' => 'center' , 'vertical'=>    'center'],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:I1")->applyFromArray(['font' => ['bold' => true]]);

                                                $start_from =1;
                                                $sheet->setCellValue('A' . $start_from ,trans('common::application.#'));
                                                $sheet->setCellValue('B' . $start_from,trans('common::application.id_card_number'));
                                                $sheet->setCellValue('C' . $start_from,trans('common::application.full_name'));
                                                $sheet->setCellValue('D' . $start_from ,trans('common::application.SRV_INF_NAME'));
                                                $sheet->setCellValue('E' . $start_from ,trans('common::application.ORG_NM_MON'));
                                                $sheet->setCellValue('F' . $start_from ,trans('common::application.SRV_TYPE_MAIN_NM'));
                                                $sheet->setCellValue('G' . $start_from ,trans('common::application.CURRENCY'));
                                                $sheet->setCellValue('H' . $start_from ,trans('common::application.RECP_AID_AMOUNT'));
                                                $sheet->setCellValue('I' . $start_from ,trans('common::application.RECP_DELV_DT'));

                                                $z= 2;
                                                $cnt= 1;
                                                foreach($receipt as $k => $v )
                                                {
                                                    foreach($v->social_affairs_receipt as $k1 => $v1 )
                                                    {
                                                        $sheet->setCellValue('A'.$z,$cnt);
                                                        $sheet->setCellValue('B' . $z ,GovServices::refDashIfNull($v,'IDNO'));
                                                        $sheet->setCellValue('C' . $z ,GovServices::refDashIfNull($v,'FULLNAME'));
                                                        $sheet->setCellValue('D'.$z,GovServices::refDashIfNull($v1,'SRV_INF_NAME'));
                                                        $sheet->setCellValue('E'.$z,GovServices::refDashIfNull($v1,'SRV_INF_NAME'));
                                                        $sheet->setCellValue('F'.$z,GovServices::refDashIfNull($v1,'ORG_NM_MON'));
                                                        $sheet->setCellValue('G'.$z,GovServices::refDashIfNull($v1,'SRV_TYPE_MAIN_NM'));
                                                        $sheet->setCellValue('H'.$z,GovServices::refDashIfNull($v1,'CURRENCY'));
                                                        $sheet->setCellValue('I'.$z,GovServices::refDashIfNull($v1,'RECP_AID_AMOUNT'));
                                                        $sheet->setCellValue('J'.$z,GovServices::refDashIfNull($v1,'RECP_DELV_DT'));
                                                        $sheet->getRowDimension($z)->setRowHeight(-1);
                                                        $z++;
                                                        $cnt++;
                                                    }
                                                }
                                                $keys = ['A','B','C' ,'D' ,'E' ,'F' ,'G' ,'H' ,'I' ,'J'];
                                                foreach($keys as $key ) {
                                                    $sheet->getStyle($key)->getAlignment()->setWrapText(true);
                                                }

                                            });
                                        }

                                        if(sizeof($invalid_cards) > 0){
                                            $excel->sheet(trans('common::application.un_beneficiary'), function($sheet) use($invalid_cards) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;
                                                foreach($invalid_cards as $v){
                                                    $sheet->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }

                                            });
                                        }

                                    })->store('xlsx', storage_path('tmp/'));

                                    \Log\Model\Log::saveNewLog('CARD_CKECK', trans('common::application.The user has checked the identification numbers fetching data from the civil register, the number of records checked') .$total);

                                    if( $total == ( $success +$duplicated)){
                                        return response()->json(['status' => 'success','msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.All numbers successfully checked') ,'download_token' => $token]);
                                    }
                                    else{
                                        return response()->json([ 'status' => 'success',
                                            'msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.Some numbers were successfully checked, as the total number') .' :  '. $total . ' ,  '.
                                                trans('common::application.The number of names that have fetched data') .' :  ' .$success . ' ,  ' .
                                                trans('common::application.Number of unregistered names benefiting') .' :  ' .sizeof($invalid_cards) . ' ,  ' .
                                                trans('common::application.Number of duplicates').' :  '.$duplicated ,
                                            'download_token' => $token ]);
                                    }

                                }
                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);

                    }
                }

                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);

            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }

    //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//
    /************ pdf & excel not ready  aidsCommittee ************/
    // filter citizen benefit of aids committee according ( card or name )
    public function aidsSearchFilter(Request $request)
    {

        $this->authorize('aidsCommittee', \Common\Model\AidsCases::class);
        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $records = [];

        $using = $request->using;
        $user = \Auth::user();

        if($using == 1){
            $card = $request->id_card_number;
            $row = GovServices::byCard($card);
            CardLog::saveCardLog($user->id,$card);
            if($row['status'] != false){
                $temp = $row['row'];
                $records[] = $temp;
            }
        }else{
            $row = GovServices::byName($request->all());
            if($row['status'] != false){
                $records= $row['row'];
            }
        }

        if(sizeof($records) > 0 ){
            foreach ($records as $key=>$value){

                $aidsCommittee=GovServices::aidsCommitteeStatus($value->IDNO);
                if($aidsCommittee['status'] == false){
                    $value->aidsCommittee= false;
                    $value->aidsCommitteeStatus= trans('common::application.un_beneficiary');
                    $value->aidsCommitteeStatusTrue= false;
                    $value->mobile= "-";
                    $value->address= "-";
                }
                else{
                    $value->aidsCommittee= true;
                    $value->aidsCommitteeStatus=  trans('common::application.beneficiary_');
                    $value->aidsCommitteeStatusTrue= true;
                    $value->mobile= $aidsCommittee['row']->paterfamilias_mobile;
                    $value->address= $aidsCommittee['row']->home_address;
                }
            }
            return response()->json(['status'=>true,'items'=>$records]);
        }

        return response()->json(['status'=>false]);

    }
    // get citizen benefit of aids committee using card
    public function aidsCommittee(Request $request)
    {
        $this->authorize('aidsCommittee', \Common\Model\AidsCases::class);

        $record = [];

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $card = $request->id_card_number;

        if($request->action =='xlsx'){
            $row = GovServices::byCard($card);
            $main=$row['row'];
            $aidsCommittee=GovServices::aidsCommitteeStatus($card);

            if($aidsCommittee['status'] == false){
                $main->aidsCommittee= false;
                $main->aidsCommitteeStatus= trans('common::application.un_beneficiary');
                $main->aidsCommitteeStatusTrue= false;
                $main->mobile= "-";
                $main->address= "-";
            }
            else{
                $aidsCommittee_=$aidsCommittee['row'];
                $main->aidsCommittee= true;
                $main->aidsCommitteeStatus=  trans('common::application.beneficiary_');
                $main->aidsCommitteeStatusTrue= true;
                $main->mobile= $aidsCommittee_->paterfamilias_mobile;
                $main->address=$aidsCommittee_->home_address;
                $main->aidsCommitteeData = $aidsCommittee_;
            }

            if(sizeof($row->all_relatives) > 0 ) {
                foreach ($row->all_relatives as $key_ => $value_) {
                    $value_->aidsCommittee = false;
                    $value_->aidsCommitteeStatus = trans('common::application.un_beneficiary');
                    $value_->mobile = "-";
                    $value_->address = "-";
                    $aidsCommittee = GovServices::aidsCommitteeStatus($value_->IDNO_RELATIVE);
                    if ($aidsCommittee['status'] != false) {
                        $value_->aidsCommittee = true;
                        $value_->aidsCommitteeStatus = trans('common::application.beneficiary_');
                        $value_->aidsCommitteeStatusTrue = true;
                        $value_->mobile = $aidsCommittee['row']->paterfamilias_mobile;
                        $value_->address = $aidsCommittee['row']->home_address;
                        $value_->aidsCommitteeData = $aidsCommittee['row'];
                    }
                    $record[] = $value_;
                }
            }else{
                $record[] = (Object)[
                    'IDNO_RELATIVE' => '-' ,
                    'RELATIVE_DESC' => '-' ,
                    'FNAME_ARB' => '-' ,
                    'SNAME_ARB' => '-' ,
                    'TNAME_ARB' => '-' ,
                    'LNAME_ARB' => '-' ,
                    'SEX' => '-' ,
                    'SOCIAL_STATUS' => '-' ,
                    'BIRTH_DT' => '-' ,
                    'DETH_DT' => '-' ,
                    'BIRTH_PMAIN' => '-' ,
                    'BIRTH_PSUB' => '-' ,
                    'PREV_LNAME_ARB' => '-' ,
                    'MOTHER_ARB' => '-' ,
                    'STATUS' => '-' ,
                ];
            }


            $cases_list=[];
            if (CaseModel::hasPermission('reports.case.RelayCitizenRepository')) {
                $cases_list = Person::getPersonCardCases([$card]);
            }

            $token = md5(uniqid());
            \Excel::create('export_' . $token, function($excel) use($main,$record,$cases_list) {
                $excel->setTitle(trans('common::application.family_sheet'));
                $excel->setDescription(trans('common::application.family_sheet'));
                $excel->sheet(trans('common::application.family_sheet'), function($sheet) use($main,$record) {

                    $sheet->setRightToLeft(true);
                    $sheet->setAllBorders('thin');
                    $sheet->setfitToWidth(true);
                    $sheet->setHeight(1,40);
                    $sheet->getDefaultStyle()->applyFromArray([
                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                    ]);

                    $sheet->getStyle("A1:AF1")->applyFromArray(['font' => ['bold' => true]]);
                    $sheet->setCellValue('A1',trans('common::application.#'));
                    $sheet->setCellValue('B1',trans('common::application.full_name'));
                    $sheet->setCellValue('C1',trans('common::application.id_card_number'));
                    $sheet->setCellValue('D1',trans('common::application.gender'));
                    $sheet->setCellValue('E1',trans('common::application.marital_status'));
                    $sheet->setCellValue('F1',trans('common::application.address'));
                    $sheet->setCellValue('G1',trans('common::application.near_mosque'));
                    $sheet->setCellValue('H1',trans('common::application.mobile'));
                    $sheet->setCellValue('I1',trans('common::application.phone'));
                    $sheet->setCellValue('J1',trans('common::application.aid_status'));
                    $sheet->setCellValue('K1',trans('common::application.affected_by_wars'));
                    $sheet->setCellValue('L1',trans('common::application.current_career'));
                    $sheet->setCellValue('M1',trans('common::application.father_death_reason'));
                    $sheet->setCellValue('N1',trans('common::application.mother_death_reason'));
                    $sheet->setCellValue('O1',trans('common::application.building_type'));
                    $sheet->setCellValue('P1',trans('common::application.home_type'));
                    $sheet->setCellValue('Q1',trans('common::application.home_status'));
                    $sheet->setCellValue('R1',trans('common::application.home_description'));
                    $sheet->setCellValue('S1',trans('common::application.furniture_type'));
                    $sheet->setCellValue('T1',trans('common::application.furniture_description'));
                    $sheet->setCellValue('U1',trans('common::application.total_family_income'));
                    $sheet->setCellValue('V1',trans('common::application.health_status'));
                    $sheet->setCellValue('W1',trans('common::application.wife_mobile'));

                    $sheet->setCellValue('X1',trans('common::application.id_card_number').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('Y1',trans('common::application.kinship_name') .' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('Z1',trans('common::application.full_name').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AA1',trans('common::application.gender').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AB1',trans('common::application.marital_status').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AC1',trans('common::application.birthday').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AD1',trans('common::application.prev_family_name').' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AE1',trans('common::application.mother_name') .' ('.trans('common::application.related') .' )');
                    $sheet->setCellValue('AF1',trans('common::application.aid_status') .' ('.trans('common::application.related') .' )');

                    $z= 2;
                    foreach($record as $k=>$v )
                    {
                        $sheet->setCellValue('A'.$z,$k+1);
                        $sheet->setCellValue('B'.$z,
                            (is_null($main->aidsCommitteeData->name) ||$main->aidsCommitteeData->name == ' ' ) ? ' ' :$main->aidsCommitteeData->name
                        );
                        $sheet->setCellValue('C'.$z,(is_null($main->IDNO) ||$main->IDNO == ' ' ) ? '-' :$main->IDNO);
                        $sheet->setCellValue('D'.$z,(is_null($main->SEX) ||$main->SEX == ' ' ) ? '-' :$main->SEX);
                        $sheet->setCellValue('E'.$z,(is_null($main->SOCIAL_STATUS) ||$main->SOCIAL_STATUS == ' ' ) ? '-' :$main->SOCIAL_STATUS);
                        $sheet->setCellValue('F'.$z,(is_null($main->address) ||$main->address == ' ' ) ? '-' :$main->address );
                        $sheet->setCellValue('G'.$z,(is_null($main->aidsCommitteeData->near_mosque) ||$main->aidsCommitteeData->near_mosque == ' ' ) ? '-' :$main->aidsCommitteeData->near_mosque );
                        $sheet->setCellValue('H'.$z,(is_null($main->mobile) || $main->mobile == ' ' ) ? '-' : $main->mobile);
                        $sheet->setCellValue('I'.$z,(is_null($main->aidsCommitteeData->telephone) || $main->aidsCommitteeData->telephone == ' ' ) ? '-' : $main->aidsCommitteeData->telephone);

                        $sheet->setCellValue('J'.$z,(is_null($main->aidsCommitteeStatus) || $main->aidsCommitteeStatus == ' ' ) ? '-' : $main->aidsCommitteeStatus);

                        $sheet->setCellValue('K'.$z,(is_null($main->aidsCommitteeData->affected_by_wars) || $main->aidsCommitteeData->affected_by_wars == 0 ) ? trans('common::application.no') : trans('common::application.yes'));

                        $sheet->setCellValue('L'.$z,(is_null($main->aidsCommitteeData->current_career) || $main->aidsCommitteeData->current_career == ' ' ) ? '-' : $main->aidsCommitteeData->current_career);

                        $sheet->setCellValue('M'.$z,(is_null($main->aidsCommitteeData->father_death_reason) || $main->aidsCommitteeData->father_death_reason == ' ' ) ? '-' : $main->aidsCommitteeData->father_death_reason);

                        $sheet->setCellValue('N'.$z,(is_null($main->aidsCommitteeData->mother_death_reason) || $main->aidsCommitteeData->mother_death_reason == ' ' ) ? '-' : $main->aidsCommitteeData->mother_death_reason);

                        $sheet->setCellValue('O'.$z,(is_null($main->aidsCommitteeData->building_type) || $main->aidsCommitteeData->building_type == ' ' ) ? '-' : $main->aidsCommitteeData->building_type);

                        $sheet->setCellValue('P'.$z,(is_null($main->aidsCommitteeData->home_type) || $main->aidsCommitteeData->home_type == ' ' ) ? '-' : $main->aidsCommitteeData->home_type);

                        $sheet->setCellValue('Q'.$z,(is_null($main->aidsCommitteeData->home_status) || $main->aidsCommitteeData->home_status == ' ' ) ? '-' : $main->aidsCommitteeData->home_status);

                        $sheet->setCellValue('R'.$z,(is_null($main->aidsCommitteeData->home_description) || $main->aidsCommitteeData->home_description == ' ' ) ? '-' : $main->aidsCommitteeData->home_description);

                        $sheet->setCellValue('S'.$z,(is_null($main->aidsCommitteeData->furniture_type) || $main->aidsCommitteeData->furniture_type == ' ' ) ? '-' : $main->aidsCommitteeData->furniture_type);

                        $sheet->setCellValue('T'.$z,(is_null($main->aidsCommitteeData->furniture_description) || $main->aidsCommitteeData->furniture_description == ' ' ) ? '-' : $main->aidsCommitteeData->furniture_description);

                        $sheet->setCellValue('U'.$z,(is_null($main->aidsCommitteeData->total_family_income) || $main->aidsCommitteeData->total_family_income == ' ' ) ? '-' : $main->aidsCommitteeData->total_family_income);

                        $sheet->setCellValue('V'.$z,(is_null($main->aidsCommitteeData->health_status) || $main->aidsCommitteeData->health_status == ' ' ) ? '-' : $main->aidsCommitteeData->health_status);

                        $sheet->setCellValue('W'.$z,(is_null($main->aidsCommitteeData->wife_mobile) || $main->aidsCommitteeData->wife_mobile == ' ' ) ? '-' : $main->aidsCommitteeData->wife_mobile);

                        $sheet->setCellValue('X'.$z,(is_null($v->IDNO_RELATIVE) || $v->IDNO_RELATIVE == ' ' ) ? '-' : $v->IDNO_RELATIVE);
                        $sheet->setCellValue('Y'.$z,(is_null($v->RELATIVE_DESC) || $v->RELATIVE_DESC == ' ' ) ? '-' : $v->RELATIVE_DESC);
                        $sheet->setCellValue('Z'.$z,
                            ( (is_null($v->FNAME_ARB) || $v->FNAME_ARB == ' ' ) ? ' ' : $v->FNAME_ARB) .' '.
                            ( (is_null($v->SNAME_ARB) || $v->SNAME_ARB == ' ' ) ? ' ' : $v->SNAME_ARB) .' '.
                            ( (is_null($v->TNAME_ARB) || $v->TNAME_ARB == ' ' ) ? ' ' : $v->TNAME_ARB) .' '.
                            ( (is_null($v->LNAME_ARB) || $v->LNAME_ARB == ' ' ) ? ' ' : $v->LNAME_ARB)
                        );
                        $sheet->setCellValue('AA'.$z,(is_null($v->SEX) || $v->SEX == ' ' ) ? '-' : $v->SEX);
                        $sheet->setCellValue('AB'.$z,(is_null($v->SOCIAL_STATUS) || $v->SOCIAL_STATUS == ' ' ) ? '-' : $v->SOCIAL_STATUS);
                        $sheet->setCellValue('AC'.$z,(is_null($v->BIRTH_DT) || $v->BIRTH_DT == ' ') ? '-' : $v->BIRTH_DT);
                        $sheet->setCellValue('AD'.$z,(is_null($v->PREV_LNAME_ARB) || $v->PREV_LNAME_ARB == ' ' ) ? '-' : $v->PREV_LNAME_ARB);
                        $sheet->setCellValue('AE'.$z,(is_null($v->MOTHER_ARB) || $v->MOTHER_ARB == ' ' ) ? '-' : $v->MOTHER_ARB);
                        $sheet->setCellValue('AF'.$z,(is_null($v->aidsCommitteeStatus) || $v->aidsCommitteeStatus == ' ' ) ? '-' : $v->aidsCommitteeStatus);
                        $sheet->getRowDimension($z)->setRowHeight(-1);
                        $z++;
                    }

                });

                if (CaseModel::hasPermission('reports.case.RelayCitizenRepository') && sizeof($cases_list) > 0) {
                    $excel->sheet(trans('common::application.cases_list'), function($sheet) use($cases_list) {

                        $sheet->setRightToLeft(true);
                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setHeight(1,40);
                        $sheet->getDefaultStyle()->applyFromArray([
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                        ]);

                        $sheet->getStyle("A1:G1")->applyFromArray(['font' => ['bold' => true]]);
                        $sheet->setCellValue('A1',trans('common::application.#'));
                        $sheet->setCellValue('B1',trans('common::application.case_category_type'));
                        $sheet->setCellValue('C1',trans('common::application.organization'));
                        $sheet->setCellValue('D1',trans('common::application.status'));
                        $sheet->setCellValue('E1',trans('common::application.visitor'));
                        $sheet->setCellValue('F1',trans('common::application.visited_at'));
                        $sheet->setCellValue('G1',trans('common::application.created_at'));

                        $z= 2;
                        foreach($cases_list as $k=>$v )
                        {
                            $sheet->setCellValue('A'.$z,$k+1);
                            $sheet->setCellValue('B'.$z,(is_null($v->category_name) ||$v->category_name == ' ' ) ? '-' :$v->category_name);
                            $sheet->setCellValue('C'.$z,(is_null($v->organization_name) ||$v->organization_name == ' ' ) ? '-' :$v->organization_name);
                            $sheet->setCellValue('D'.$z,(is_null($v->status) ||$v->status == ' ' ) ? '-' :$v->status);
                            $sheet->setCellValue('E'.$z,(is_null($v->visitor) ||$v->visitor == ' ' ) ? '-' :$v->visitor);
                            $sheet->setCellValue('F'.$z,(is_null($v->visited_at) ||$v->visited_at == ' ' ) ? '-' :$v->visited_at);
                            $sheet->setCellValue('G'.$z,(is_null($v->created_date) ||$v->created_date == ' ' ) ? '-' :$v->created_date);
                            $sheet->getRowDimension($z)->setRowHeight(-1);
                            $z++;
                        }

                    });

                }

            })->store('xlsx', storage_path('tmp/'));
            return response()->json(['download_token' => $token]);
        }
        elseif($request->action =='pdf') {

            $person = null;
            $row = GovServices::byCard($card);
            if($row['status'] == false){
                return response()->json(['status'=>false,'msg'=>$row['message']]);
            }
            $record=$row['row'];

            $employment_data = GovServices::workDetails($card);
            $record->employment=null;
            if($employment_data['status'] != false){
                $record->employment=$employment_data['row'];
            }

            $health_data = GovServices::medicalReports($card);
            $record->health=null;
            if($health_data['status'] != false){
                $record->health=$health_data['row'];
            }

            $commercial_data = GovServices::commercialRecords($card);
            $record->commercial_data=null;
            if($commercial_data['status'] != false){
                $record->commercial_data=$commercial_data['row'];

                foreach ($record->commercial_data as $ke => $v_){
                    if(isset($v_->START_DATE)){
                        $v_->START_DATE = date('d/m/Y',strtotime($v_->START_DATE));
                    }
                }
            }

            $socialAffairs=GovServices::socialAffairsStatus($card);
            $record->social_affairs=null;
            $record->social_affairs_request=null;
            $record->social_affairs_receipt=null;
            if($socialAffairs['status'] != false){
                $socialAffairs_=$socialAffairs['row'];
                $socialAffairs_->social_affairs= true;
                $socialAffairs_->social_affairs_status=  trans('common::application.beneficiary_');
                $record->social_affairs=$socialAffairs_;
            }
            $socialAffairsRecipt=GovServices::socialAffairsReceipt($card);
            if($socialAffairsRecipt['status'] == true) {
                $record->social_affairs_receipt = $socialAffairsRecipt['row'];
            }

            $aidsCommittee=GovServices::aidsCommitteeStatus($card);
            $record->aids_committee=null;

            if($aidsCommittee['status'] == true){
                $record->aids_committee=$aidsCommittee['row'];
            }


            $keys = ['parent','childrens','wives'];
            foreach ($keys as $key){
                foreach ($record->$key as $k=>$v){
                    $v->aidsCommittee= false;
                    $v->aidsCommitteeStatus= trans('common::application.un_beneficiary');
                    $v->aidsCommitteeStatusTrue= false;
                    $v->mobile= "-";
                    $v->address= "-";

                    $aidsCommittee_=GovServices::aidsCommitteeStatus($v->IDNO_RELATIVE);
                    if($aidsCommittee_['status'] != false){
                        $v->aidsCommittee= true;
                        $v->aidsCommitteeStatus=  trans('common::application.beneficiary_');
                        $v->aidsCommitteeStatusTrue= true;
                        $v->mobile= $aidsCommittee['row']->paterfamilias_mobile;
                        $v->address= $aidsCommittee['row']->home_address;
                    }
                }
            }

            $record->CASE_STATUS=false;
            $record->cases_list=[];
            $record->person_id = Person::getPersonId([$card]);

            if (CaseModel::hasPermission('reports.case.RelayCitizenRepository')) {
                $record->cases_list = Person::getPersonCardCases([$card]);
            }

            $full_name = $record->FNAME_ARB.' '.$record->SNAME_ARB.' '.$record->TNAME_ARB.' '.$record->LNAME_ARB;

            $en_name = $record->ENG_NAME;
            $mother_name = (is_null($record->MOTHER_ARB) ||$record->MOTHER_ARB == ' ' ) ? '-' :$record->MOTHER_ARB ;
            $prev_family_name = (is_null($record->PREV_LNAME_ARB) ||$record->PREV_LNAME_ARB == ' ' ) ? '-' :$record->PREV_LNAME_ARB ;

            $gender = (is_null($record->SEX) ||$record->SEX == ' ' ) ? '-' :$record->SEX ;
            $marital_status = (is_null($record->SOCIAL_STATUS) ||$record->SOCIAL_STATUS == ' ' ) ? '-' :$record->SOCIAL_STATUS ;
            $birthday = (is_null($record->BIRTH_DT) ||$record->BIRTH_DT == ' ' ) ? '-' :$record->BIRTH_DT ;

            $death_date = (is_null($record->DETH_DT) ||$record->DETH_DT == ' ' ) ? '-' :$record->DETH_DT ;
            $birth_place_main = (is_null($record->BIRTH_PMAIN) ||$record->BIRTH_PMAIN == ' ' ) ? '-' :$record->BIRTH_PMAIN ;
            $birth_place_sub = (is_null($record->BIRTH_PMAIN) ||$record->BIRTH_PMAIN == ' ' ) ? '-' :$record->BIRTH_PMAIN ;

            $address = $record->CI_REGION .' - ' .$record->CI_CITY  . ' - ' . $record->STREET_ARB;
            $primary_mobile = (is_null($record->MOBILE) ||$record->MOBILE == ' ' ) ? '-' :$record->MOBILE ;
            $phone = (is_null($record->TEL) ||$record->TEL == ' ' ) ? '-' :$record->TEL ;


            $html='';
            PDF::SetTitle($full_name);
            PDF::SetFont('aealarabiya', '', 18);
            PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);
            PDF::SetMargins(10, 10, 10);
            PDF::SetAutoPageBreak(TRUE);
            $lg = Array();
            $lg['a_meta_charset'] = 'UTF-8';
            $lg['a_meta_dir'] = 'rtl';
            $lg['a_meta_language'] = 'fa';
            $lg['w_page'] = 'page';
            PDF::setLanguageArray($lg);
            PDF::setCellHeightRatio(1.5);

            $date = date('Y-m-d');
            $data_ =new Request();

            $token = md5(uniqid());
            $img = base_path('storage/app/emptyUser1.png');
            $fileContents = file_get_contents($record->photo_url);
            $disk = "local";
            $filename = $token.'.png'; // The name you want to give to the downloaded image
            if ($fileContents !== false) {
                Storage::disk($disk)->put('documents/' . $filename, $fileContents);
                $img = storage_path('app/documents/').$token.'.png';
            }

            $html = '<!doctype html><html lang="ar">
                 <head>
                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                 <style>
                     .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                     table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                     * , body {font-weight:normal !important; font-size: 10px;}
                     .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                     .bolder-content{text-align: right !important; vertical-align: middle!important;}
                     .vertical-middle{ vertical-align: middle !important;}
                </style>
                </head>
                 <body style="font-weight: normal !important;">';

            $html.= '<h2 style="text-align:center;"> '.$full_name.'</h2>';
            $html .= '<table border="1" style="border: 1px solid black; box-sizing:border-box;">';

            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.full_name').' </b></td>
                       <td width="330" align="center" >'.$full_name.'</td>
                        <td width="98" align="center" rowspan="6" style="background-color: white" >'
                .'<img src="'.$img.'" alt="" height="140" />'.
                '</td>
                       
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.en_name').' </b></td>
                       <td width="330" align="center" >'.$en_name.'</td>
                    
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.id_card_number').' </b></td>
                       <td width="115" align="center" >'.$card.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.marital_status').' </b></td>
                       <td width="115" align="center" >'.$marital_status.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.gender').' </b></td>
                       <td width="115" align="center" >'.$gender.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.birthday').' </b></td>
                       <td width="115" align="center" >'.$birthday.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.phone').' </b></td>
                       <td width="115" align="center" >'.$phone.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.primary_mobile').' </b></td>
                       <td width="115" align="center" >'.$primary_mobile.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.mother_name').' </b></td>
                       <td width="115" align="center" >'.$mother_name.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.prev_family_name').' </b></td>
                       <td width="115" align="center" >'.$prev_family_name.'</td>
                   </tr>';

            $html .='</table>';

            $html .= '<table border="1" style="border: 1px solid black; box-sizing:border-box;">';

            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.birth_place_main').' </b></td>
                       <td width="115" align="center" >'.$birth_place_main.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.birth_place_sub').' </b></td>
                       <td width="213" align="center" >'.$birth_place_sub.'</td>
                   </tr>';

            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.death_date').' </b></td>
                       <td width="115" align="center" >'.$death_date.'</td>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.address').' </b></td>
                       <td width="213" align="center" >'.$address.'</td>
                   </tr>';




            $html .='</table>';


            $html .='<div style="height: 5px; color: white"> - </div>';



            // aids committee data
            if(!is_null($record->aids_committee)){
                $address = (is_null($record->aids_committee->home_address) ||$record->aids_committee->home_address == ' ' ) ? '-' :$record->aids_committee->home_address ;
                $near_mosque = (is_null($record->aids_committee->near_mosque) ||$record->aids_committee->near_mosque == ' ' ) ? '-' :$record->aids_committee->near_mosque ;
                $primary_mobile = (is_null($record->aids_committee->paterfamilias_mobile) ||$record->aids_committee->paterfamilias_mobile == ' ' ) ? '-' :$record->aids_committee->paterfamilias_mobile ;
                $phone = (is_null($record->aids_committee->telephone) ||$record->aids_committee->telephone == ' ' ) ? '-' :$record->aids_committee->telephone ;
                $affected_by_wars = ($record->aids_committee->affected_by_wars == 1) ? 'yes' :'no' ;
                $current_career = (is_null($record->aids_committee->current_career) ||$record->aids_committee->current_career == ' ' ) ? '-' :$record->aids_committee->current_career ;
                $father_death_reason = (is_null($record->aids_committee->father_death_reason) ||$record->aids_committee->father_death_reason == ' ' ) ? '-' :$record->aids_committee->father_death_reason;
                $mother_death_reason = (is_null($record->aids_committee->mother_death_reason) ||$record->aids_committee->mother_death_reason == ' ' ) ? '-' :$record->aids_committee->mother_death_reason;
                $building_type = (is_null($record->aids_committee->building_type) ||$record->aids_committee->building_type == ' ' ) ? '-' :$record->aids_committee->building_type ;
                $home_type = (is_null($record->aids_committee->home_type) ||$record->aids_committee->home_type == ' ' ) ? '-' :$record->aids_committee->home_type ;
                $furniture_type = (is_null($record->aids_committee->furniture_type) ||$record->aids_committee->furniture_type == ' ' ) ? '-' :$record->aids_committee->furniture_type ;
                $home_status = (is_null($record->aids_committee->home_status) ||$record->aids_committee->home_status == ' ' ) ? '-' :$record->aids_committee->home_status ;
                $home_description = (is_null($record->aids_committee->home_description) ||$record->aids_committee->home_description == ' ' ) ? '-' :$record->aids_committee->home_description ;
                $total_family_income = (is_null($record->aids_committee->total_family_income) ||$record->aids_committee->total_family_income == ' ' ) ? '-' :$record->aids_committee->total_family_income ;
                $health_status = (is_null($record->aids_committee->health_status) ||$record->aids_committee->health_status == ' ' ) ? '-' :$record->aids_committee->health_status ;
                $wife_mobile = (is_null($record->aids_committee->wife_mobile) ||$record->aids_committee->wife_mobile == ' ' ) ? '-' :$record->aids_committee->wife_mobile ;
            }
            else{
                $address = $near_mosque =  $primary_mobile = $phone = $affected_by_wars = $current_career = $father_death_reason = $mother_death_reason = $building_type = $home_type =  $furniture_type = $home_status = $home_description = $total_family_income = $health_status = $wife_mobile = '-';
            }

            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="528" align="center"><b>  
                        '.trans('common::application.aids committee data').'
                        </b></td>
                   </tr>';


            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.primary_mobile').'</b></td>
                       <td width="184" align="center">'.$primary_mobile.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.phone').'</b></td>
                       <td width="184" align="center">'.$phone.'</td> 
                   </tr>';

            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.wife_mobile').'</b></td>
                       <td width="184" align="center">'.$wife_mobile .'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.health_status').'</b></td>
                       <td width="184" align="center">'.$health_status.'</td> 
                   </tr>';

            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.address').'</b></td>
                       <td width="184" align="center">'.$address.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.near_mosque').'</b></td>
                       <td width="184" align="center">'.$near_mosque.'</td> 
                   </tr>';
            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.current_career').'</b></td>
                       <td width="184" align="center">'.$current_career.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.affected_by_wars').'</b></td>
                       <td width="184" align="center">'.$affected_by_wars.'</td> 
                   </tr>';

            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.father_death_reason').'</b></td>
                       <td width="184" align="center">'.$father_death_reason.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.mother_death_reason').'</b></td>
                       <td width="184" align="center">'.$mother_death_reason.'</td> 
                   </tr>';
            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.building_type').'</b></td>
                       <td width="184" align="center">'.$building_type.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.home_type').'</b></td>
                       <td width="184" align="center">'.$home_type.'</td> 
                   </tr>';
            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.furniture_type').'</b></td>
                       <td width="184" align="center">'.$furniture_type.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.home_status').'</b></td>
                       <td width="184" align="center">'.$home_status.'</td> 
                   </tr>';

            $html .= ' <tr>
                       <td style="height:20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.home_description').'</b></td>
                       <td width="184" align="center">'.$home_description.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.total_family_income').'</b></td>
                       <td width="184" align="center">'.$total_family_income.'</td> 
                   </tr>';

            $html .='</table>';


            $html .='</body></html>';

            PDF::AddPage('P','A4');
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


            // cases list

            $html = '<!doctype html><html lang="ar">
                 <head>
                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                 <style>
                     .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                     table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                     * , body {font-weight:normal !important; font-size: 10px;}
                     .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                     .bolder-content{text-align: right !important; vertical-align: middle!important;}
                     .vertical-middle{ vertical-align: middle !important;}
                </style>
                </head>
                 <body style="font-weight: normal !important;">';
            $html .='<div style="height: 5px; color: white"> - </div>';

            $html .= '<table border="1" style="">';

            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="534" align="center"><b>  
                        '.trans('common::application.cases list').'
                        </b></td>
                   </tr>';
            $html .= ' <tr> 
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="75" align="center"><b> '.trans('common::application.case_category_type').'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="150" align="center"><b> '.trans('common::application.organization name').'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="40" align="center"><b> '.trans('common::application.status').'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="89" align="center"><b> '.trans('common::application.visitor').'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.visited_at').'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.created_at').'</b></td>
            </tr>';

            $z = 1;

            foreach($record->cases_list as $row) {

                $html .= ' <tr> 
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.$z.'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="75" align="center"><b> '.$row->category_name.'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="150" align="center"><b> '.$row->organization_name.'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="40" align="center"><b> '.$row->status.'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="89" align="center"><b> '.$row->visitor.'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="80" align="center"><b> '.$row->visited_at.'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="80" align="center"><b> '.$row->created_date.'</b></td>
            </tr>';
                $z++;
            }

            $html .='</table>';
            $html .='</body></html>';

            PDF::AddPage('P','A4');
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

            $dirName = base_path('storage/app/citizen_pdf');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }

            // wives or husbands

            $html = '<!doctype html><html lang="ar">
                 <head>
                 <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                 <style>
                     .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                     table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                     * , body {font-weight:normal !important; font-size: 10px;}
                     .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                     .bolder-content{text-align: right !important; vertical-align: middle!important;}
                     .vertical-middle{ vertical-align: middle !important;}
                </style>
                </head>
                 <body style="font-weight: normal !important;">';
            $html .='<div style="height: 5px; color: white"> - </div>';

            $html .= '<table border="1" style="">';

            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="528" align="center"><b>  
                        '.trans('common::application.wives or husbands').'
                        </b></td>
                   </tr>';
            $html .= ' <tr> 
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.id_card_number').'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="148" align="center"><b> '.trans('common::application.name').'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="95" align="center"><b> '.trans('common::application.prev_family_name').'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="65" align="center"><b> '.trans('common::application.marital_status').'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="65" align="center"><b> '.trans('common::application.birthday').'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="65" align="center"><b> '.trans('common::application.death_date').'</b></td>
            </tr>';

            $wives = $record->wives;

            $z = 1;

            foreach($wives as $row) {

                $html .= ' <tr> 
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.$z.'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="70" align="center"><b> '.$row->IDNO.'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="148" align="center"><b> '. $row->FNAME_ARB . ' ' . $row->SNAME_ARB  . ' ' .$row->TNAME_ARB  . ' ' . $row->LNAME_ARB .'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="95" align="center"><b> '.$row->PREV_LNAME_ARB.'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="65" align="center"><b> '.$row->SOCIAL_STATUS.'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="65" align="center"><b> '.$row->BIRTH_DT.'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="65" align="center"><b> '.$row->DETH_DT.'</b></td>
            </tr>';
                $z++;
            }

            $html .='</table>';

            // childs

            $html .='<div style="height: 5px; color: white"> - </div>';

            $html .= '<table border="1" style="">';

            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0;background-color:#e4f5ff ;color:#000000;" width="528" align="center"><b>  
                        '.trans('common::application.childs').'
                        </b></td>
                   </tr>';
            $html .= ' <tr> 
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="20" align="center"><b>#</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b> '.trans('common::application.id_card_number').'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="123" align="center"><b> '.trans('common::application.name').'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.gender').'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.marital_status').'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.birthday').'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.death_date').'</b></td>
            </tr>';

            $childs = $record->childs;

            $z = 1;

            foreach($childs as $row) {

                $html .= ' <tr> 
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="20" align="center"><b>'.$z.'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="70" align="center"><b> '.$row->IDNO.'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="123" align="center"><b> '. $row->FNAME_ARB . ' ' . $row->SNAME_ARB  . ' ' .$row->TNAME_ARB  . ' ' . $row->LNAME_ARB .'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="80" align="center"><b> '.$row->SEX.'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="80" align="center"><b> '.$row->SOCIAL_STATUS.'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="80" align="center"><b> '.$row->BIRTH_DT.'</b></td>
               <td style="height: 20px ; padding: 15px 0 12px 0; color:#000000;" width="80" align="center"><b> '.$row->DETH_DT.'</b></td>
            </tr>';
                $z++;
            }

            $html .='</table>';

            $html .='</body></html>';

            PDF::AddPage('P','A4');
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


            PDF::Output($dirName.'/'.$full_name . '.pdf', 'F');
            unlink(storage_path('app/documents/'.$token.'.png'));
            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');

            $zip = new \ZipArchive;
            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                foreach ( glob( $dirName . '/*' ) as $fileName )
                {
                    $file = basename( $fileName );
                    $zip->addFile( $fileName, $file );
                }

                $zip->close();
                array_map('unlink', glob("$dirName/*.*"));
                rmdir($dirName);
            }
            return response()->json(['download_token' => $token]);

        }
        else{
            $aidsCommittee=GovServices::aidsCommitteeStatus($card);

            if($aidsCommittee['status'] == false){
                return response()->json(['status'=>false,'msg'=>$aidsCommittee['message']]);
            }

            $person = null;
            $row = GovServices::byCard($card);
            $record = $row['row'];
            if($row['status'] == false){
                return response()->json(['status'=>false,'msg'=>$row['message']]);
            }
            if($aidsCommittee['status'] == false){
                $record->aidsCommitteeData = [];
            }else{
                $record->aidsCommitteeData = $aidsCommittee['row'];
            }

            $keys = ['parent','childrens','wives'];
            foreach ($keys as $key){
                foreach ($record->$key as $k=>$v){
                    $v->aidsCommittee= false;
                    $v->aidsCommitteeStatus= trans('common::application.un_beneficiary');
                    $v->aidsCommitteeStatusTrue= false;
                    $v->mobile= "-";
                    $v->address= "-";

                    $aidsCommittee_=GovServices::aidsCommitteeStatus($v->IDNO_RELATIVE);
                    if($aidsCommittee_['status'] != false){
                        $v->aidsCommittee= true;
                        $v->aidsCommitteeStatus=  trans('common::application.beneficiary_');
                        $v->aidsCommitteeStatusTrue= true;
                        $v->mobile= $aidsCommittee['row']->paterfamilias_mobile;
                        $v->address= $aidsCommittee['row']->home_address;
                    }
                }
            }

            $record->CASE_STATUS=false;
            $record->cases_list=[];
            $record->person_id = Person::getPersonId([$card]);
            if (CaseModel::hasPermission('reports.case.RelayCitizenRepository')) {
                $record->cases_list = Person::getPersonCardCases([$card]);
            }

            CloneGovernmentPersons::saveNew($record);
            return response()->json(['status'=>true,'data'=>$record]);
        }

    }
    // check citizen benefit of aids committee using excel sheet according card number
    public function aidsCommitteeCheck(Request $request)
    {
        $this->authorize('aidsCommittee', \Common\Model\AidsCases::class);
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $type = $request->type;
        $validFile=true;
        $duplicated =0;
        $success =0;

        $final_records=[];
        $invalid_cards=[];
        $processed=[];
        $importFile=$request->file ;
        if ($importFile->isValid()) {
            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();

                    if(sizeof($keys) >0 ){
                        $validFile = false;
                        foreach ($keys as $item) {
                            if ( $item == 'rkm_alhoy' ) {
                                $validFile = true;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {
                                foreach ($records as $key =>$value) {
                                    if(in_array($value['rkm_alhoy'],$processed)){
                                        $duplicated++;
                                    }
                                    else{
                                        $card= (int) $value['rkm_alhoy'];
                                        if(strlen($card)  <= 9) {
                                            $aidsCommittee=GovServices::aidsCommitteeStatus($card);


                                            if($aidsCommittee['status'] == false){
                                                $invalid_cards[]=$value['rkm_alhoy'];
                                            }else{
                                                $row = GovServices::byCard($card);
                                                if($row['status'] == true) {

                                                    $row['row']->aidsCommittee= true;
                                                    $row['row']->aidsCommitteeStatus=  trans('common::application.beneficiary_');
                                                    $row['row']->aidsCommitteeStatusTrue= true;
                                                    $row['row']->mobile= $aidsCommittee['row']->paterfamilias_mobile;
                                                    $row['row']->address= $aidsCommittee['row']->home_address;

                                                    $aidsCommittee=GovServices::aidsCommitteeStatus($card);
                                                    if($aidsCommittee['status'] != false){
                                                        $row['row']->aidsCommittee= true;
                                                        $row['row']->aidsCommitteeStatus=  trans('common::application.beneficiary_');
                                                        $row['row']->aidsCommitteeStatusTrue= true;
                                                        $row['row']->mobile= $aidsCommittee['row']->paterfamilias_mobile;
                                                        $row['row']->address= $aidsCommittee['row']->home_address;
                                                        $row['row']->aidsCommitteeData= $aidsCommittee['row'];
                                                    }else{
                                                        $row['row']->aidsCommittee= false;
                                                        $row['row']->aidsCommitteeStatus= trans('common::application.un_beneficiary');
                                                        $row['row']->mobile= "-";
                                                        $row['row']->address= "-";
                                                    }

                                                    $main=(array) $row['row'];
                                                    $main['related']= null;

                                                    if($type == 2) {
                                                        if(sizeof($row->all_relatives) > 0 ) {
                                                            foreach ($row->all_relatives as $key_ => $value_) {
                                                                $value_->aidsCommittee = false;
                                                                $value_->aidsCommitteeStatus = trans('common::application.un_beneficiary');
                                                                $value_->mobile = "-";
                                                                $value_->address = "-";
                                                                $aidsCommittee = GovServices::aidsCommitteeStatus($value_->IDNO_RELATIVE);
                                                                if ($aidsCommittee['status'] != false) {
                                                                    $value_->aidsCommittee = true;
                                                                    $value_->aidsCommitteeStatus = trans('common::application.beneficiary_');
                                                                    $value_->aidsCommitteeStatusTrue = true;
                                                                    $value_->mobile = $aidsCommittee['row']->paterfamilias_mobile;
                                                                    $value_->address = $aidsCommittee['row']->home_address;
                                                                    $value_->aidsCommitteeData = $aidsCommittee['row'];
                                                                }
                                                                $main['related'] = $value_;
                                                                $final_records[] = $main;
                                                            }
                                                        }else{
                                                            $final_records[] = $main;
                                                        }
                                                    }
                                                    elseif($type == 3) {
                                                        if(sizeof($row->wives) > 0 ) {
                                                            foreach ($row->wives as $key_ => $value_) {
                                                                $value_->aidsCommittee = false;
                                                                $value_->aidsCommitteeStatus = trans('common::application.un_beneficiary');
                                                                $value_->mobile = "-";
                                                                $value_->address = "-";
                                                                $aidsCommittee = GovServices::aidsCommitteeStatus($value_->IDNO_RELATIVE);
                                                                if ($aidsCommittee['status'] != false) {
                                                                    $value_->aidsCommittee = true;
                                                                    $value_->aidsCommitteeStatus = trans('common::application.beneficiary_');
                                                                    $value_->aidsCommitteeStatusTrue = true;
                                                                    $value_->mobile = $aidsCommittee['row']->paterfamilias_mobile;
                                                                    $value_->address = $aidsCommittee['row']->home_address;
                                                                    $value_->aidsCommitteeData = $aidsCommittee['row'];
                                                                }
                                                                $main['related'] = $value_;
                                                                $final_records[] = $main;
                                                            }
                                                        }else{
                                                            $final_records[] = $main;
                                                        }
                                                    }
                                                    elseif($type == 4) {
                                                        if(sizeof($row->childrens) > 0 ) {
                                                            foreach ($row->childrens as $key_ => $value_) {
                                                                $value_->aidsCommittee = false;
                                                                $value_->aidsCommitteeStatus = trans('common::application.un_beneficiary');
                                                                $value_->mobile = "-";
                                                                $value_->address = "-";
                                                                $aidsCommittee = GovServices::aidsCommitteeStatus($value_->IDNO_RELATIVE);
                                                                if ($aidsCommittee['status'] != false) {
                                                                    $value_->aidsCommittee = true;
                                                                    $value_->aidsCommitteeStatus = trans('common::application.beneficiary_');
                                                                    $value_->aidsCommitteeStatusTrue = true;
                                                                    $value_->mobile = $aidsCommittee['row']->paterfamilias_mobile;
                                                                    $value_->address = $aidsCommittee['row']->home_address;
                                                                    $value_->aidsCommitteeData = $aidsCommittee['row'];
                                                                }
                                                                $main['related'] = $value_;
                                                                $final_records[] = $main;
                                                            }
                                                        }else{
                                                            $final_records[] = $main;
                                                        }
                                                    }
                                                    else{
                                                        $final_records[] = $main;
                                                    }

                                                    $processed[]=$value['rkm_alhoy'];
                                                    $success++;
                                                }else{
                                                    $invalid_cards[]=$value['rkm_alhoy'];
                                                }
                                            }
                                        }else{
                                            $invalid_cards[]=$value['rkm_alhoy'];
                                        }
                                    }
                                }

                                if($total == sizeof($invalid_cards)){
                                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.All names entered are not registered benefiting from social affairs')  ]);
                                }else{

                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function($excel) use($final_records,$invalid_cards,$processed,$type) {
                                        $excel->setTitle(trans('common::application.beneficiary_family_sheet'));
                                        $excel->setDescription(trans('common::application.beneficiary_family_sheet'));

                                        if(sizeof($final_records) > 0){
                                            $excel->sheet(trans('common::application.beneficiary_family_sheet'), function($sheet) use($final_records,$type) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);


                                                if($type == 1){
                                                    $sheet->getStyle("A1:W1")->applyFromArray(['font' => ['bold' => true]]);
                                                }else{
                                                    $sheet->getStyle("A1:AF1")->applyFromArray(['font' => ['bold' => true]]);
                                                }

                                                $sheet->setCellValue('A1',trans('common::application.#'));
                                                $sheet->setCellValue('B1',trans('common::application.full_name'));
                                                $sheet->setCellValue('C1',trans('common::application.id_card_number'));
                                                $sheet->setCellValue('D1',trans('common::application.gender'));
                                                $sheet->setCellValue('E1',trans('common::application.marital_status'));
                                                $sheet->setCellValue('F1',trans('common::application.address'));
                                                $sheet->setCellValue('G1',trans('common::application.near_mosque'));
                                                $sheet->setCellValue('H1',trans('common::application.mobile'));
                                                $sheet->setCellValue('I1',trans('common::application.phone'));
                                                $sheet->setCellValue('J1',trans('common::application.aid_status'));
                                                $sheet->setCellValue('K1',trans('common::application.affected_by_wars'));
                                                $sheet->setCellValue('L1',trans('common::application.current_career'));
                                                $sheet->setCellValue('M1',trans('common::application.father_death_reason'));
                                                $sheet->setCellValue('N1',trans('common::application.mother_death_reason'));
                                                $sheet->setCellValue('O1',trans('common::application.building_type'));
                                                $sheet->setCellValue('P1',trans('common::application.home_type'));
                                                $sheet->setCellValue('Q1',trans('common::application.home_status'));
                                                $sheet->setCellValue('R1',trans('common::application.home_description'));
                                                $sheet->setCellValue('S1',trans('common::application.furniture_type'));
                                                $sheet->setCellValue('T1',trans('common::application.furniture_description'));
                                                $sheet->setCellValue('U1',trans('common::application.total_family_income'));
                                                $sheet->setCellValue('V1',trans('common::application.health_status'));
                                                $sheet->setCellValue('W1',trans('common::application.wife_mobile'));


                                                if($type != 1){
                                                    $sheet->setCellValue('X1',trans('common::application.id_card_number').' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('Y1',trans('common::application.kinship_name') .' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('Z1',trans('common::application.full_name').' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('AA1',trans('common::application.gender').' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('AB1',trans('common::application.marital_status').' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('AC1',trans('common::application.birthday').' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('AD1',trans('common::application.prev_family_name').' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('AE1',trans('common::application.mother_name') .' ('.trans('common::application.related') .' )');
                                                    $sheet->setCellValue('AF1',trans('common::application.aid_status') .' ('.trans('common::application.related') .' )');
                                                }

                                                $z= 2;
                                                foreach($final_records as $k=>$main ) {

                                                    $main = (Object) $main;
                                                    $v = (Object) $main->related;

                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,
                                                        (is_null($main->aidsCommitteeData->name) ||$main->aidsCommitteeData->name == ' ' ) ? ' ' :$main->aidsCommitteeData->name
                                                    );
                                                    $sheet->setCellValue('C'.$z,(is_null($main->IDNO) ||$main->IDNO == ' ' ) ? '-' :$main->IDNO);
                                                    $sheet->setCellValue('D'.$z,(is_null($main->SEX) ||$main->SEX == ' ' ) ? '-' :$main->SEX);
                                                    $sheet->setCellValue('E'.$z,(is_null($main->SOCIAL_STATUS) ||$main->SOCIAL_STATUS == ' ' ) ? '-' :$main->SOCIAL_STATUS);
                                                    $sheet->setCellValue('F'.$z,(is_null($main->address) ||$main->address == ' ' ) ? '-' :$main->address );
                                                    $sheet->setCellValue('G'.$z,(is_null($main->aidsCommitteeData->near_mosque) ||$main->aidsCommitteeData->near_mosque == ' ' ) ? '-' :$main->aidsCommitteeData->near_mosque );
                                                    $sheet->setCellValue('H'.$z,(is_null($main->mobile) || $main->mobile == ' ' ) ? '-' : $main->mobile);
                                                    $sheet->setCellValue('I'.$z,(is_null($main->aidsCommitteeData->telephone) || $main->aidsCommitteeData->telephone == ' ' ) ? '-' : $main->aidsCommitteeData->telephone);

                                                    $sheet->setCellValue('J'.$z,(is_null($main->aidsCommitteeStatus) || $main->aidsCommitteeStatus == ' ' ) ? '-' : $main->aidsCommitteeStatus);

                                                    $sheet->setCellValue('K'.$z,(is_null($main->aidsCommitteeData->affected_by_wars) || $main->aidsCommitteeData->affected_by_wars == 0 ) ? trans('common::application.no') : trans('common::application.yes'));

                                                    $sheet->setCellValue('L'.$z,(is_null($main->aidsCommitteeData->current_career) || $main->aidsCommitteeData->current_career == ' ' ) ? '-' : $main->aidsCommitteeData->current_career);

                                                    $sheet->setCellValue('M'.$z,(is_null($main->aidsCommitteeData->father_death_reason) || $main->aidsCommitteeData->father_death_reason == ' ' ) ? '-' : $main->aidsCommitteeData->father_death_reason);

                                                    $sheet->setCellValue('N'.$z,(is_null($main->aidsCommitteeData->mother_death_reason) || $main->aidsCommitteeData->mother_death_reason == ' ' ) ? '-' : $main->aidsCommitteeData->mother_death_reason);

                                                    $sheet->setCellValue('O'.$z,(is_null($main->aidsCommitteeData->building_type) || $main->aidsCommitteeData->building_type == ' ' ) ? '-' : $main->aidsCommitteeData->building_type);

                                                    $sheet->setCellValue('P'.$z,(is_null($main->aidsCommitteeData->home_type) || $main->aidsCommitteeData->home_type == ' ' ) ? '-' : $main->aidsCommitteeData->home_type);

                                                    $sheet->setCellValue('Q'.$z,(is_null($main->aidsCommitteeData->home_status) || $main->aidsCommitteeData->home_status == ' ' ) ? '-' : $main->aidsCommitteeData->home_status);

                                                    $sheet->setCellValue('R'.$z,(is_null($main->aidsCommitteeData->home_description) || $main->aidsCommitteeData->home_description == ' ' ) ? '-' : $main->aidsCommitteeData->home_description);

                                                    $sheet->setCellValue('S'.$z,(is_null($main->aidsCommitteeData->furniture_type) || $main->aidsCommitteeData->furniture_type == ' ' ) ? '-' : $main->aidsCommitteeData->furniture_type);

                                                    $sheet->setCellValue('T'.$z,(is_null($main->aidsCommitteeData->furniture_description) || $main->aidsCommitteeData->furniture_description == ' ' ) ? '-' : $main->aidsCommitteeData->furniture_description);

                                                    $sheet->setCellValue('U'.$z,(is_null($main->aidsCommitteeData->total_family_income) || $main->aidsCommitteeData->total_family_income == ' ' ) ? '-' : $main->aidsCommitteeData->total_family_income);

                                                    $sheet->setCellValue('V'.$z,(is_null($main->aidsCommitteeData->health_status) || $main->aidsCommitteeData->health_status == ' ' ) ? '-' : $main->aidsCommitteeData->health_status);

                                                    $sheet->setCellValue('W'.$z,(is_null($main->aidsCommitteeData->wife_mobile) || $main->aidsCommitteeData->wife_mobile == ' ' ) ? '-' : $main->aidsCommitteeData->wife_mobile);



                                                    if($type != 1){
                                                        if($main->related){
                                                            $sheet->setCellValue('X'.$z,(is_null($v->IDNO) || $v->IDNO == ' ' ) ? '-' : $v->IDNO);
                                                            $sheet->setCellValue('Y'.$z,(is_null($v->kinship_name) || $v->kinship_name == ' ' ) ? '-' : $v->kinship_name);
                                                            $sheet->setCellValue('Z'.$z,
                                                                ( (is_null($v->FNAME_ARB) || $v->FNAME_ARB == ' ' ) ? ' ' : $v->FNAME_ARB) .' '.
                                                                ( (is_null($v->SNAME_ARB) || $v->SNAME_ARB == ' ' ) ? ' ' : $v->SNAME_ARB) .' '.
                                                                ( (is_null($v->TNAME_ARB) || $v->TNAME_ARB == ' ' ) ? ' ' : $v->TNAME_ARB) .' '.
                                                                ( (is_null($v->LNAME_ARB) || $v->LNAME_ARB == ' ' ) ? ' ' : $v->LNAME_ARB)
                                                            );
                                                            $sheet->setCellValue('AA'.$z,(is_null($v->SEX) || $v->SEX == ' ' ) ? '-' : $v->SEX);
                                                            $sheet->setCellValue('AB'.$z,(is_null($v->SOCIAL_STATUS) || $v->SOCIAL_STATUS == ' ' ) ? '-' : $v->SOCIAL_STATUS);
                                                            $sheet->setCellValue('AC'.$z,(is_null($v->BIRTH_DT) || $v->BIRTH_DT == ' ') ? '-' : $v->BIRTH_DT);
                                                            $sheet->setCellValue('AD'.$z,(is_null($v->PREV_LNAME_ARB) || $v->PREV_LNAME_ARB == ' ' ) ? '-' : $v->PREV_LNAME_ARB);
                                                            $sheet->setCellValue('AE'.$z,(is_null($v->MOTHER_ARB) || $v->MOTHER_ARB == ' ' ) ? '-' : $v->MOTHER_ARB);
                                                            $sheet->setCellValue('AF'.$z,(is_null($v->aidsCommitteeStatus) || $v->aidsCommitteeStatus == ' ' ) ? '-' : $v->aidsCommitteeStatus);
                                                        } else{

                                                            $sheet->setCellValue('J'.$z,'-');
                                                            $sheet->setCellValue('K'.$z,'-');
                                                            $sheet->setCellValue('L'.$z,'-');
                                                            $sheet->setCellValue('M'.$z,'-');
                                                            $sheet->setCellValue('N'.$z,'-');
                                                            $sheet->setCellValue('O'.$z,'-');
                                                            $sheet->setCellValue('P'.$z,'-');
                                                            $sheet->setCellValue('Q'.$z,'-');
                                                            $sheet->setCellValue('R'.$z,'-');
                                                        }
                                                    }

                                                    $z++;
                                                }
                                            });
                                        }
                                        if(sizeof($invalid_cards) > 0){
                                            $excel->sheet(trans('common::application.un_beneficiary'), function($sheet) use($invalid_cards) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;
                                                foreach($invalid_cards as $v){
                                                    $sheet->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }

                                            });
                                        }

                                    })->store('xlsx', storage_path('tmp/'));

                                    \Log\Model\Log::saveNewLog('CARD_CKECK', trans('common::application.The user has checked the identification numbers fetching data from the civil register, the number of records checked') .$total);

                                    if( $total == ( $success +$duplicated)){
                                        return response()->json(['status' => 'success','msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.All numbers successfully checked') ,'download_token' => $token]);
                                    }
                                    else{
                                        return response()->json([ 'status' => 'success',
                                            'msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.Some numbers were successfully checked, as the total number') .' :  '. $total . ' ,  '.
                                                trans('common::application.The number of names that have fetched data') .' :  ' .$success . ' ,  ' .
                                                trans('common::application.Number of unregistered names benefiting') .' :  ' .sizeof($invalid_cards) . ' ,  ' .
                                                trans('common::application.Number of duplicates').' :  '.$duplicated ,
                                            'download_token' => $token ]);
                                    }

                                }



                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);

                    }
                }

                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);

            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }
    // update citizen benefit of aids committee status using excel sheet according card number
    public function saveAidsCommitteeStatus(Request $request)
    {
        $this->authorize('aidsCommittee', \Common\Model\AidsCases::class);
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $validFile=true;
        $duplicated =0;
        $success =0;

        $final_records=[];
        $invalid_cards=[];
        $not_registered=[];
        $processed=[];

        $importFile=$request->file ;
        if ($importFile->isValid()) {
            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();

                    if(sizeof($keys) >0 ){
                        $validFile = false;
                        foreach ($keys as $item) {
                            if ( $item == 'rkm_alhoy' ) {
                                $validFile = true;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {
                                $CreateOrUpdate_ =[];
                                foreach ($records as $key =>$value) {
                                    if(isset($value['rkm_alhoy'])){
                                        if($value['rkm_alhoy'] != '' && $value['rkm_alhoy'] != ' '){
                                            if(in_array($value['rkm_alhoy'],$processed)){
                                                $duplicated++;
                                            }
                                            else{
                                                $card= (int) $value['rkm_alhoy'];
                                                if(strlen($card)  <= 9) {

                                                    $person = Person::where(function ($q) use ($card){
                                                        $q->where('id_card_number',$card);
                                                    })->first();

                                                    if(is_null($person)){
                                                        $not_registered[]=$value['rkm_alhoy'];
                                                    }else{
                                                        $input = ['aid_value'=>0,'aid_take'=> 0 ];
                                                        $aidsCommittee=GovServices::aidsCommitteeStatus($card);
                                                        if($aidsCommittee['status'] != false){
                                                            $input['aid_take'] = 1;
                                                            $input['currency_id'] = 4;
                                                            $input['aid_value'] = 0;
                                                        }
                                                        $CreateOrUpdate_[]=['person_id'=>$person->id ,'input' => $input];
                                                        $processed[]=$value['rkm_alhoy'];
                                                        $success++;
                                                    }
                                                }else{
                                                    $invalid_cards[]=$value['rkm_alhoy'];
                                                }
                                            }
                                        }
                                    }
                                }

                                if(sizeof($CreateOrUpdate_) > 0){
                                    foreach ($records as $key =>$value) {
                                        PersonAid::CreateOrUpdate($value['person_id'],1,$value['input'],15);
                                    }
                                }
                                if($total == sizeof($invalid_cards)){
                                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.All names entered are incorrect')  ]);
                                }else if($total == sizeof($not_registered)){
                                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.All names entered are not registered in the system')  ]);
                                }
                                else{

                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function($excel) use($final_records,$invalid_cards,$processed,$not_registered) {
                                        $excel->setTitle(trans('common::application.beneficiary_family_sheet'));
                                        $excel->setDescription(trans('common::application.beneficiary_family_sheet'));

                                        if(sizeof($processed) > 0){
                                            $excel->sheet(trans('common::application.status has been modified'), function($sheet) use($processed) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;
                                                foreach($processed as $v){
                                                    $sheet->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }

                                            });
                                        }
                                        if(sizeof($not_registered) > 0){
                                            $excel->sheet(trans('common::application.not_registered'), function($sheet) use($not_registered) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;
                                                foreach($not_registered as $v){
                                                    $sheet->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }

                                            });
                                        }
                                        if(sizeof($invalid_cards) > 0){
                                            $excel->sheet(trans('common::application.invalid_card'), function($sheet) use($invalid_cards) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;
                                                foreach($invalid_cards as $v){
                                                    $sheet->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }

                                            });
                                        }

                                    })->store('xlsx', storage_path('tmp/'));

                                    \Log\Model\Log::saveNewLog('CARD_CKECK', trans('common::application.The user has checked the identification numbers fetching data from the civil register, the number of records checked') .$total);

                                    if( $total == ( $success +$duplicated)){
                                        return response()->json(['status' => 'success','msg'=>
                                            trans('common::application.the results file is being downloaded,').
                                            trans('common::application.Some numbers were successfully checked, as the total number') .' :  '. $total . ' ,  '.
                                            trans('common::application.The number of names whose status has been modified') .' :  ' .$success
                                            ,'download_token' => $token]);
                                    }
                                    else{
                                        return response()->json([ 'status' => 'success',
                                            'msg'=>trans('common::application.the results file is being downloaded,').
                                                trans('common::application.Some numbers were successfully checked, as the total number') .' :  '. $total . ' ,  '.
                                                trans('common::application.The number of names whose status has been modified') .' :  ' .$success . ' ,  ' .
                                                trans('common::application.Number of names not registered in the system') .' :  ' .sizeof($not_registered) . ' ,  ' .
                                                trans('common::application.The number of incorrect or not registered ID numbers in the system').' :  ' .sizeof($invalid_cards) . ' ,  ' .
                                                trans('common::application.Number of duplicates').' :  '.$duplicated ,
                                            'download_token' => $token ]);
                                    }

                                }

                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);

                    }
                }

                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);

            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }

    /************ voucherSearch ************/
    // filter citizen benefit of vouchers according ( card or name )
    public function voucherSearch(Request $request){

        ini_set('max_execution_time',0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $record = [];
        $card = $request->id_card_number;
        $user = \Auth::user();
//      $Person = new Person();
        $record = Person::voucherFilter($request->all());

        $token = md5(uniqid());
        \Excel::create('export_' . $token, function($excel) use($record) {
            $excel->setTitle(trans('common::application.Voucher data List'));
            $excel->setDescription(trans('common::application.Voucher data List'));
            $excel->sheet(trans('common::application.Voucher data List'), function($sheet) use($record) {

                $sheet->setRightToLeft(true);
                $sheet->setAllBorders('thin');
                $sheet->setfitToWidth(true);
                $sheet->setHeight(1,40);
                $sheet->getDefaultStyle()->applyFromArray([
                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                ]);

                $sheet->getStyle("A1:O1")->applyFromArray(['font' => ['bold' => true]]);
                $sheet->setCellValue('A1',trans('common::application.#'));
                $sheet->setCellValue('B1',trans('aid::application.full_name'));
                $sheet->setCellValue('C1',trans('aid::application.id_card_number'));
                $sheet->setCellValue('D1',trans('aid::application.voucher_serial'));
                $sheet->setCellValue('E1',trans('aid::application.voucher_title'));
                $sheet->setCellValue('F1',trans('aid::application.organization_name'));
                $sheet->setCellValue('G1',trans('aid::application.voucher_sponsor_name'));
                $sheet->setCellValue('H1',trans('aid::application.case_category_name'));
                $sheet->setCellValue('I1',trans('aid::application.voucher_value'));
                $sheet->setCellValue('J1',trans('aid::application.currency'));
                $sheet->setCellValue('K1',trans('aid::application.shekel_v'));
//                $sheet->setCellValue(I1,trans('aid::application.voucherSource'));
                $sheet->setCellValue('L1',trans('aid::application.voucher_date'));
                $sheet->setCellValue('M1',trans('aid::application.receipt_date'));
                $sheet->setCellValue('N1',trans('aid::application.receipt_time'));
                $sheet->setCellValue('O1',trans('aid::application.receipt_status'));


                $z= 2;
                foreach($record['data'] as $k=>$v )
                {

                    $sheet->setCellValue('A'.$z,$k+1);
                    $sheet->setCellValue('B'.$z,$record['full_name']);
                    $sheet->setCellValue('C'.$z,(is_null($v->id_card_number) ||$v->id_card_number == ' ' ) ? ' ' :$v->id_card_number);
                    $sheet->setCellValue('D'.$z,(is_null($v->serial) ||$v->serial == ' ' ) ? '-' :$v->serial);
                    $sheet->setCellValue('E'.$z,(is_null($v->title) ||$v->title == ' ' ) ? ' ' :$v->title);
                    $sheet->setCellValue('F'.$z,(is_null($v->organization_name) ||$v->organization_name == ' ' ) ? '-' :$v->organization_name);
                    $sheet->setCellValue('G'.$z,(is_null($v->sponsor_name) ||$v->sponsor_name == ' ' ) ? '-' :$v->sponsor_name);
                    $sheet->setCellValue('H'.$z,(is_null($v->category_name) ||$v->category_name == ' ' ) ? '-' :$v->category_name);
                    $sheet->setCellValue('I'.$z,(is_null($v->value) ||$v->value == ' ' ) ? '-' :$v->value);
                    $sheet->setCellValue('J'.$z,(is_null($v->currency_name) ||$v->currency_name == ' ' ) ? '-' :$v->currency_name);
                    $sheet->setCellValue('K'.$z,(is_null($v->sh_value) ||$v->sh_value == ' ' ) ? '-' : round($v->sh_value,0));
//                    $sheet->setCellValue('H'.$z,(is_null($v->project_id) ||$v->project_id == '' ) ? trans('common::application.manual') : trans('common::application.project'));
                    $sheet->setCellValue('L'.$z,(is_null($v->voucher_date) ||$v->voucher_date == ' ' ) ? '-' :$v->voucher_date);
                    $sheet->setCellValue('M'.$z,(is_null($v->receipt_date) ||$v->receipt_date == ' ' ) ? '-' :$v->receipt_date);
                    $sheet->setCellValue('N'.$z,(is_null($v->receipt_time_) ||$v->receipt_time_ == ' ' ) ? '-' :$v->receipt_time_);
                    $sheet->setCellValue('O'.$z,(is_null($v->receipt_status) ||$v->receipt_status == ' ' ) ? '-' :$v->receipt_status);
                    $sheet->getRowDimension($z)->setRowHeight(-1);


                    $z++;
                }

            });

        })->store('xlsx', storage_path('tmp/'));
        return response()->json(['download_token' => $token]);

    }
    // check citizen benefit of vouchers using excel sheet according card number
    public function voucherSearchCheck(Request $request){
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $user = \Auth::user();
        $type = $request->type;
        $validFile=true;
        $duplicated =0;
        $success =0;

        $final_records=[];
        $invalid_cards=[];
        $not_beneficiaries_cards =[];
        $processed=[];

        $importFile=$request->file ;
        if ($importFile->isValid()) {
            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){
                        $validFile = false;
                        foreach ($keys as $item) {
                            if ( $item == 'rkm_alhoy' ) {
                                $validFile = true;
                                break;
                            }
                        }
                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);
                            $pass =[];
                            $params = $request->all();
                            unset($params['file']);

                            if ($total > 0) {
                                foreach ($records as $key =>$value) {
                                    if(in_array($value['rkm_alhoy'],$processed)){
                                        $duplicated++;
                                    }
                                    else{
                                        $card= (int) $value['rkm_alhoy'];
                                        if(strlen($card)  <= 9) {
                                            if(GovServices::checkCard($card)) {
                                                $pass[]=$value['rkm_alhoy'];
                                                $success++;
                                            }else{
                                                $invalid_cards[]=$value['rkm_alhoy'];
                                            }
                                        }else{
                                            $invalid_cards[]=$value['rkm_alhoy'];
                                        }
                                    }
                                }

                                if($total == sizeof($invalid_cards)){
                                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.All names entered are not registered in the Civil Registry')  ]);
                                }else{

                                    $for_all = $request->for_all;

                                    $for_person = true;
                                    $for_parent = true;
                                    $for_wife = true;
                                    $for_children = true;

                                    if(!$for_all){
                                        $for_person = $request->for_person;
                                        $for_parent = $request->for_parent;
                                        $for_wife = $request->for_wife;
                                        $for_children = $request->for_children;
                                    }

                                    $suc = [];
                                    $params['id_card_number'] = $pass;
                                    $params['for_person'] = $for_person;
                                    $params['for_parent'] = $for_parent;
                                    $params['for_wife'] = $for_wife;
                                    $params['for_children'] = $for_children;
                                    $final_records = Person::voucherFilterDetails($params);

                                    if(sizeof($final_records['data']) == 0 && sizeof($final_records['parents']) == 0 &&
                                        sizeof($final_records['wives']) == 0 && sizeof($final_records['childrens']) == 0){
                                        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.all_card_not_benefit')  ]);
                                    }else{
                                        if (sizeof($final_records['data']) > 0){
                                            foreach($final_records['data'] as $k=>$v ) {
                                                if(!in_array($v['id_card_number'],$suc)){
                                                    $suc[] = $v['id_card_number'];
                                                }
                                            }
                                        }

                                        if (sizeof($final_records['parents']) > 0){
                                            foreach($final_records['parents'] as $k=>$v ) {
                                                if(!in_array($v->main_id_card_number,$suc)){
                                                    $suc[] = $v->main_id_card_number;
                                                }
                                            }
                                        }

                                        if (sizeof($final_records['wives']) > 0){
                                            foreach($final_records['wives'] as $k=>$v ) {
                                                if(!in_array($v->main_id_card_number,$suc)){
                                                    $suc[] = $v->main_id_card_number;
                                                }
                                            }
                                        }

                                        if (sizeof($final_records['childrens']) > 0){
                                            foreach($final_records['childrens'] as $k=>$v ) {
                                                if(!in_array($v->main_id_card_number,$suc)){
                                                    $suc[] = $v->main_id_card_number;
                                                }
                                            }
                                        }
                                    }

                                    $not_beneficiaries_cards = array_diff($pass, $suc);
                                    $not_beneficiaries_data = Person::groupDetails($not_beneficiaries_cards);
                                    $not_beneficiaries=[];
                                    foreach($not_beneficiaries_data as $k=>$v ) {
                                        $not_beneficiaries[] = $v->id_card_number;
                                    }

                                    $not_person = array_diff($not_beneficiaries_cards, $not_beneficiaries);
                                    if (sizeof($not_person) > 0){
                                        foreach($not_person as $card ) {
                                            $invalid_cards[] = $card;
                                        }
                                    }

                                    $token = md5(uniqid());
                                    \Excel::create('export_' . $token, function($excel) use($final_records,$pass,$invalid_cards,$processed,$not_beneficiaries_cards,$not_beneficiaries_data) {
                                        $excel->setTitle(trans('common::application.Voucher data List'));
                                        $excel->setDescription(trans('common::application.Voucher data List'));

                                        if(sizeof($final_records['data']) > 0){
                                            $excel->sheet(trans('common::application.Voucher data List'), function($sheet) use($final_records,$pass,$not_beneficiaries_cards,$not_beneficiaries_data) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:P1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.#'));
                                                $sheet->setCellValue('B1',trans('common::application.id_card_number'));
                                                $sheet->setCellValue('C1',trans('common::application.name'));
                                                $sheet->setCellValue('D1',trans('aid::application.voucher_title'));
                                                $sheet->setCellValue('E1',trans('aid::application.organization_name'));
//                                                $sheet->setCellValue('F1',trans('aid::application.voucher_sponsor_name'));
                                                $sheet->setCellValue('F1',trans('aid::application.case_category_name'));
                                                $sheet->setCellValue('G1',trans('aid::application.voucher_value'));
                                                $sheet->setCellValue('H1',trans('aid::application.currency_name'));
                                                $sheet->setCellValue('I1',trans('aid::application.shekel_v'));
                                                $sheet->setCellValue('J1',trans('aid::application.voucher_date'));
                                                $sheet->setCellValue('K1',trans('aid::application.voucher_type'));
                                                $sheet->setCellValue('L1',trans('aid::application.voucher_urgent'));
                                                $sheet->setCellValue('M1',trans('aid::application.receipt_status'));
                                                $sheet->setCellValue('N1',trans('aid::application.receipt_date'));
                                                $sheet->setCellValue('O1',trans('aid::application.receipt_time'));
                                                $sheet->setCellValue('P1',trans('aid::application.receipt_location'));
                                                $z= 2;

                                                foreach($final_records['data'] as $k=>$v ) {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,(is_null($v['id_card_number'] ||$v['id_card_number'] == ' ' ) ? ' ' :$v['id_card_number']));
                                                    $sheet->setCellValue('C'.$z,(is_null($v['fullname'] ||$v['fullname'] == ' ' ) ? ' ' :$v['fullname']));
                                                    $sheet->setCellValue('D'.$z,(is_null($v['title'] ||$v['title'] == ' ' ) ? ' ' :$v['title']));
                                                    $sheet->setCellValue('E'.$z,(is_null($v['organization_name'] ||$v['organization_name'] == ' ' ) ? ' ' :$v['organization_name']));
//                                                    $sheet->setCellValue('F'.$z,(is_null($v['sponsor_name'] ||$v['sponsor_name'] == ' ' ) ? '-' :$v['sponsor_name']));
                                                    $sheet->setCellValue('F'.$z,(is_null($v['category_name'] ||$v['category_name'] == ' ' ) ? '-' :$v['category_name']));
                                                    $sheet->setCellValue('G'.$z,(is_null($v['value'] ||$v['value'] == ' ' ) ? '-' :$v['value']));
                                                    $sheet->setCellValue('H'.$z,(is_null($v['currency_name'] ||$v['currency_name'] == ' ' ) ? '-' :$v['currency_name']));
                                                    $sheet->setCellValue('I'.$z,(is_null($v['sh_value'] ||$v['sh_value'] == ' ' ) ? '-' :$v['sh_value']));
//                                                    $sheet->setCellValue('J'.$z,(is_null($v['project_id'] ||$v['project_id'] == '' ) ? trans('common::application.manual') : trans('common::application.project')));
                                                    $sheet->setCellValue('J'.$z,(is_null($v['voucher_date'] ||$v['voucher_date'] == ' ' ) ? '-' :$v['voucher_date']));
                                                    $sheet->setCellValue('K'.$z,($v['type'] == 1? trans('common::application.non_financial') :($v['type'] == 2? trans('common::application.financial') :'')));
                                                    $sheet->setCellValue('L'.$z,($v['urgent'] == 1? trans('common::application.urgent')  :($v['urgent'] == 0?trans('common::application.normal') :' ')));
                                                    $sheet->setCellValue('M'.$z,($v['status']? trans('common::application.receipt') :trans('common::application.not_receipt') ));
                                                    $sheet->setCellValue('N'.$z,(is_null($v['receipt_date'] ||$v['receipt_date'] == ' ' ) ? '-' :$v['receipt_date']));
                                                    $sheet->setCellValue('O'.$z,(is_null($v['receipt_time_'] ||$v['receipt_time_'] == ' ' ) ? '-' :$v['receipt_time_']));
                                                    $sheet->setCellValue('P'.$z,(is_null($v['receipt_location'] ||$v['receipt_location'] == ' ' ) ? '-' :$v['receipt_location']));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }


                                            });
                                        }



                                        if(sizeof($final_records['parents']) > 0){
                                            $excel->sheet(trans('common::application.parents'), function($sheet) use($final_records,$pass,$not_beneficiaries_cards,$not_beneficiaries_data) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:P1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.#'));
                                                $sheet->setCellValue('B1',trans('common::application.id_card_number'));
                                                $sheet->setCellValue('C1',trans('common::application.name'));
                                                $sheet->setCellValue('D1',trans('aid::application.voucher_title'));
                                                $sheet->setCellValue('E1',trans('aid::application.organization_name'));
//                                                $sheet->setCellValue('F1',trans('aid::application.voucher_sponsor_name'));
                                                $sheet->setCellValue('F1',trans('aid::application.case_category_name'));
                                                $sheet->setCellValue('G1',trans('aid::application.voucher_value'));
                                                $sheet->setCellValue('H1',trans('aid::application.currency_name'));
                                                $sheet->setCellValue('I1',trans('aid::application.shekel_v'));
                                                $sheet->setCellValue('J1',trans('aid::application.voucher_date'));
                                                $sheet->setCellValue('K1',trans('aid::application.voucher_type'));
                                                $sheet->setCellValue('L1',trans('aid::application.voucher_urgent'));
                                                $sheet->setCellValue('M1',trans('aid::application.receipt_status'));
                                                $sheet->setCellValue('N1',trans('aid::application.receipt_date'));
                                                $sheet->setCellValue('O1',trans('aid::application.receipt_time'));
                                                $sheet->setCellValue('P1',trans('aid::application.receipt_location'));
                                                $sheet->setCellValue('Q1',trans('common::application.kinship_name'));
                                                $sheet->setCellValue('R1',trans('common::application.main_id_card_number'));
                                                $z= 2;

                                                foreach($final_records['parents'] as $k=>$v_ ) {
                                                    $v = (array) $v_;
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,(is_null($v['id_card_number'] ||$v['id_card_number'] == ' ' ) ? ' ' :$v['id_card_number']));
                                                    $sheet->setCellValue('C'.$z,(is_null($v['fullname'] ||$v['fullname'] == ' ' ) ? ' ' :$v['fullname']));
                                                    $sheet->setCellValue('D'.$z,(is_null($v['title'] ||$v['title'] == ' ' ) ? ' ' :$v['title']));
                                                    $sheet->setCellValue('E'.$z,(is_null($v['organization_name'] ||$v['organization_name'] == ' ' ) ? ' ' :$v['organization_name']));
//                                                    $sheet->setCellValue('F'.$z,(is_null($v['sponsor_name'] ||$v['sponsor_name'] == ' ' ) ? '-' :$v['sponsor_name']));
                                                    $sheet->setCellValue('F'.$z,(is_null($v['category_name'] ||$v['category_name'] == ' ' ) ? '-' :$v['category_name']));
                                                    $sheet->setCellValue('G'.$z,(is_null($v['value'] ||$v['value'] == ' ' ) ? '-' :$v['value']));
                                                    $sheet->setCellValue('H'.$z,(is_null($v['currency_name'] ||$v['currency_name'] == ' ' ) ? '-' :$v['currency_name']));
                                                    $sheet->setCellValue('I'.$z,(is_null($v['sh_value'] ||$v['sh_value'] == ' ' ) ? '-' :$v['sh_value']));
//                                                    $sheet->setCellValue('J'.$z,(is_null($v['project_id'] ||$v['project_id'] == '' ) ? trans('common::application.manual') : trans('common::application.project')));
                                                    $sheet->setCellValue('J'.$z,(is_null($v['voucher_date'] ||$v['voucher_date'] == ' ' ) ? '-' :$v['voucher_date']));
                                                    $sheet->setCellValue('K'.$z,($v['type'] == 1? trans('common::application.non_financial') :($v['type'] == 2? trans('common::application.financial') :'')));
                                                    $sheet->setCellValue('L'.$z,($v['urgent'] == 1? trans('common::application.urgent')  :($v['urgent'] == 0?trans('common::application.normal') :' ')));
                                                    $sheet->setCellValue('M'.$z,($v['status']? trans('common::application.receipt') :trans('common::application.not_receipt') ));
                                                    $sheet->setCellValue('N'.$z,(is_null($v['receipt_date'] ||$v['receipt_date'] == ' ' ) ? '-' :$v['receipt_date']));
                                                    $sheet->setCellValue('O'.$z,(is_null($v['receipt_time_'] ||$v['receipt_time_'] == ' ' ) ? '-' :$v['receipt_time_']));
                                                    $sheet->setCellValue('P'.$z,(is_null($v['receipt_location'] ||$v['receipt_location'] == ' ' ) ? '-' :$v['receipt_location']));
                                                    $sheet->setCellValue('Q'.$z,(is_null($v['kinship_name'] ||$v['kinship_name'] == ' ' ) ? '-' :$v['main_id_card_number']));
                                                    $sheet->setCellValue('R'.$z,(is_null($v['main_id_card_number'] ||$v['main_id_card_number'] == ' ' ) ? '-' :$v['main_id_card_number']));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }


                                            });
                                        }
                                        if(sizeof($final_records['wives']) > 0){
                                            $excel->sheet(trans('common::application.wives'), function($sheet) use($final_records,$pass,$not_beneficiaries_cards,$not_beneficiaries_data) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:P1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.#'));
                                                $sheet->setCellValue('B1',trans('common::application.id_card_number'));
                                                $sheet->setCellValue('C1',trans('common::application.name'));
                                                $sheet->setCellValue('D1',trans('aid::application.voucher_title'));
                                                $sheet->setCellValue('E1',trans('aid::application.organization_name'));
//                                                $sheet->setCellValue('F1',trans('aid::application.voucher_sponsor_name'));
                                                $sheet->setCellValue('F1',trans('aid::application.case_category_name'));
                                                $sheet->setCellValue('G1',trans('aid::application.voucher_value'));
                                                $sheet->setCellValue('H1',trans('aid::application.currency_name'));
                                                $sheet->setCellValue('I1',trans('aid::application.shekel_v'));
                                                $sheet->setCellValue('J1',trans('aid::application.voucher_date'));
                                                $sheet->setCellValue('K1',trans('aid::application.voucher_type'));
                                                $sheet->setCellValue('L1',trans('aid::application.voucher_urgent'));
                                                $sheet->setCellValue('M1',trans('aid::application.receipt_status'));
                                                $sheet->setCellValue('N1',trans('aid::application.receipt_date'));
                                                $sheet->setCellValue('O1',trans('aid::application.receipt_time'));
                                                $sheet->setCellValue('P1',trans('aid::application.receipt_location'));
                                                $sheet->setCellValue('Q1',trans('common::application.kinship_name'));
                                                $sheet->setCellValue('R1',trans('common::application.main_id_card_number'));
                                                $z= 2;

                                                foreach($final_records['wives'] as $k=>$v_ ) {
                                                    $v = (array) $v_;
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,(is_null($v['id_card_number'] ||$v['id_card_number'] == ' ' ) ? ' ' :$v['id_card_number']));
                                                    $sheet->setCellValue('C'.$z,(is_null($v['fullname'] ||$v['fullname'] == ' ' ) ? ' ' :$v['fullname']));
                                                    $sheet->setCellValue('D'.$z,(is_null($v['title'] ||$v['title'] == ' ' ) ? ' ' :$v['title']));
                                                    $sheet->setCellValue('E'.$z,(is_null($v['organization_name'] ||$v['organization_name'] == ' ' ) ? ' ' :$v['organization_name']));
//                                                    $sheet->setCellValue('F'.$z,(is_null($v['sponsor_name'] ||$v['sponsor_name'] == ' ' ) ? '-' :$v['sponsor_name']));
                                                    $sheet->setCellValue('F'.$z,(is_null($v['category_name'] ||$v['category_name'] == ' ' ) ? '-' :$v['category_name']));
                                                    $sheet->setCellValue('G'.$z,(is_null($v['value'] ||$v['value'] == ' ' ) ? '-' :$v['value']));
                                                    $sheet->setCellValue('H'.$z,(is_null($v['currency_name'] ||$v['currency_name'] == ' ' ) ? '-' :$v['currency_name']));
                                                    $sheet->setCellValue('I'.$z,(is_null($v['sh_value'] ||$v['sh_value'] == ' ' ) ? '-' :$v['sh_value']));
//                                                    $sheet->setCellValue('J'.$z,(is_null($v['project_id'] ||$v['project_id'] == '' ) ? trans('common::application.manual') : trans('common::application.project')));
                                                    $sheet->setCellValue('J'.$z,(is_null($v['voucher_date'] ||$v['voucher_date'] == ' ' ) ? '-' :$v['voucher_date']));
                                                    $sheet->setCellValue('K'.$z,($v['type'] == 1? trans('common::application.non_financial') :($v['type'] == 2? trans('common::application.financial') :'')));
                                                    $sheet->setCellValue('L'.$z,($v['urgent'] == 1? trans('common::application.urgent')  :($v['urgent'] == 0?trans('common::application.normal') :' ')));
                                                    $sheet->setCellValue('M'.$z,($v['status']? trans('common::application.receipt') :trans('common::application.not_receipt') ));
                                                    $sheet->setCellValue('N'.$z,(is_null($v['receipt_date'] ||$v['receipt_date'] == ' ' ) ? '-' :$v['receipt_date']));
                                                    $sheet->setCellValue('O'.$z,(is_null($v['receipt_time_'] ||$v['receipt_time_'] == ' ' ) ? '-' :$v['receipt_time_']));
                                                    $sheet->setCellValue('P'.$z,(is_null($v['receipt_location'] ||$v['receipt_location'] == ' ' ) ? '-' :$v['receipt_location']));
                                                    $sheet->setCellValue('Q'.$z,(is_null($v['kinship_name'] ||$v['kinship_name'] == ' ' ) ? '-' :$v['main_id_card_number']));
                                                    $sheet->setCellValue('R'.$z,(is_null($v['main_id_card_number'] ||$v['main_id_card_number'] == ' ' ) ? '-' :$v['main_id_card_number']));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }


                                            });
                                        }
                                        if(sizeof($final_records['childrens']) > 0){
                                            $excel->sheet(trans('common::application.childrens'), function($sheet) use($final_records,$pass,$not_beneficiaries_cards,$not_beneficiaries_data) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1:P1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.#'));
                                                $sheet->setCellValue('B1',trans('common::application.id_card_number'));
                                                $sheet->setCellValue('C1',trans('common::application.name'));
                                                $sheet->setCellValue('D1',trans('aid::application.voucher_title'));
                                                $sheet->setCellValue('E1',trans('aid::application.organization_name'));
//                                                $sheet->setCellValue('F1',trans('aid::application.voucher_sponsor_name'));
                                                $sheet->setCellValue('F1',trans('aid::application.case_category_name'));
                                                $sheet->setCellValue('G1',trans('aid::application.voucher_value'));
                                                $sheet->setCellValue('H1',trans('aid::application.currency_name'));
                                                $sheet->setCellValue('I1',trans('aid::application.shekel_v'));
                                                $sheet->setCellValue('J1',trans('aid::application.voucher_date'));
                                                $sheet->setCellValue('K1',trans('aid::application.voucher_type'));
                                                $sheet->setCellValue('L1',trans('aid::application.voucher_urgent'));
                                                $sheet->setCellValue('M1',trans('aid::application.receipt_status'));
                                                $sheet->setCellValue('N1',trans('aid::application.receipt_date'));
                                                $sheet->setCellValue('O1',trans('aid::application.receipt_time'));
                                                $sheet->setCellValue('P1',trans('aid::application.receipt_location'));
                                                $sheet->setCellValue('Q1',trans('common::application.kinship_name'));
                                                $sheet->setCellValue('R1',trans('common::application.main_id_card_number'));
                                                $z= 2;

                                                foreach($final_records['childrens'] as $k=>$v ) {
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,(is_null($v['id_card_number'] ||$v['id_card_number'] == ' ' ) ? ' ' :$v['id_card_number']));
                                                    $sheet->setCellValue('C'.$z,(is_null($v['fullname'] ||$v['fullname'] == ' ' ) ? ' ' :$v['fullname']));
                                                    $sheet->setCellValue('D'.$z,(is_null($v['title'] ||$v['title'] == ' ' ) ? ' ' :$v['title']));
                                                    $sheet->setCellValue('E'.$z,(is_null($v['organization_name'] ||$v['organization_name'] == ' ' ) ? ' ' :$v['organization_name']));
//                                                    $sheet->setCellValue('F'.$z,(is_null($v['sponsor_name'] ||$v['sponsor_name'] == ' ' ) ? '-' :$v['sponsor_name']));
                                                    $sheet->setCellValue('F'.$z,(is_null($v['category_name'] ||$v['category_name'] == ' ' ) ? '-' :$v['category_name']));
                                                    $sheet->setCellValue('G'.$z,(is_null($v['value'] ||$v['value'] == ' ' ) ? '-' :$v['value']));
                                                    $sheet->setCellValue('H'.$z,(is_null($v['currency_name'] ||$v['currency_name'] == ' ' ) ? '-' :$v['currency_name']));
                                                    $sheet->setCellValue('I'.$z,(is_null($v['sh_value'] ||$v['sh_value'] == ' ' ) ? '-' :$v['sh_value']));
//                                                    $sheet->setCellValue('J'.$z,(is_null($v['project_id'] ||$v['project_id'] == '' ) ? trans('common::application.manual') : trans('common::application.project')));
                                                    $sheet->setCellValue('J'.$z,(is_null($v['voucher_date'] ||$v['voucher_date'] == ' ' ) ? '-' :$v['voucher_date']));
                                                    $sheet->setCellValue('K'.$z,($v['type'] == 1? trans('common::application.non_financial') :($v['type'] == 2? trans('common::application.financial') :'')));
                                                    $sheet->setCellValue('L'.$z,($v['urgent'] == 1? trans('common::application.urgent')  :($v['urgent'] == 0?trans('common::application.normal') :' ')));
                                                    $sheet->setCellValue('M'.$z,($v['status']? trans('common::application.receipt') :trans('common::application.not_receipt') ));
                                                    $sheet->setCellValue('N'.$z,(is_null($v['receipt_date'] ||$v['receipt_date'] == ' ' ) ? '-' :$v['receipt_date']));
                                                    $sheet->setCellValue('O'.$z,(is_null($v['receipt_time_'] ||$v['receipt_time_'] == ' ' ) ? '-' :$v['receipt_time_']));
                                                    $sheet->setCellValue('P'.$z,(is_null($v['receipt_location'] ||$v['receipt_location'] == ' ' ) ? '-' :$v['receipt_location']));
                                                    $sheet->setCellValue('Q'.$z,(is_null($v['kinship_name'] ||$v['kinship_name'] == ' ' ) ? '-' :$v['main_id_card_number']));
                                                    $sheet->setCellValue('R'.$z,(is_null($v['main_id_card_number'] ||$v['main_id_card_number'] == ' ' ) ? '-' :$v['main_id_card_number']));
                                                    $sheet->getRowDimension($z)->setRowHeight(-1);
                                                    $z++;
                                                }


                                            });
                                        }
                                        if(sizeof($not_beneficiaries_cards) > 0){
                                            $excel->sheet(trans('common::application.un_beneficiary'), function($sheet) use($not_beneficiaries_cards,$not_beneficiaries_data) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.#'));
                                                $sheet->setCellValue('B1',trans('common::application.id_card_number'));
                                                $sheet->setCellValue('C1',trans('common::application.name'));

                                                $z= 2;
                                                foreach($not_beneficiaries_data as $k=>$v ){
                                                    $sheet->setCellValue('A'.$z,$k+1);
                                                    $sheet->setCellValue('B'.$z,(is_null($v->id_card_number ||$v->id_card_number == ' ' ) ? ' ' :$v->id_card_number));
                                                    $sheet->setCellValue('C'.$z,(is_null($v->fullname ||$v->fullname == ' ' ) ? ' ' :$v->fullname));
                                                    $z++;
                                                }

                                            });
                                        }
                                        if(sizeof($invalid_cards) > 0){
                                            $excel->sheet(trans('common::application.invalid_cards'), function($sheet) use($invalid_cards) {

                                                $sheet->setRightToLeft(true);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setHeight(1,40);
                                                $sheet->getDefaultStyle()->applyFromArray([
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                ]);

                                                $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                $z= 2;
                                                foreach($invalid_cards as $v){
                                                    $sheet->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }

                                            });
                                        }

                                    })->store('xlsx', storage_path('tmp/'));

                                    \Log\Model\Log::saveNewLog('CARD_CKECK', trans('common::application.The user has checked the identification numbers fetching data from the civil register, the number of records checked') .$total);
                                    if( $total == ( sizeof($suc) +$duplicated)){
                                        return response()->json(['status' => 'success','msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.All numbers successfully checked') ,'download_token' => $token]);
                                    }
                                    else{
                                        return response()->json([ 'status' => 'success',
//                                            'pass' => $pass,
//                                            'final_records' => $final_records,
//                                            'ben' => $suc,
//                                            'nov_ben_card' => $not_beneficiaries_cards,
//                                            'nov_ben_data' => $not_beneficiaries_data ,
//                                            'not_in_data' => array_diff($not_beneficiaries_cards, $not_beneficiaries_dt) ,
//                                            'invalid_cards' => $invalid_cards ,
                                            'msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.Some numbers were successfully checked, as the total number') .' :  '. $total . ' ,  '.
                                                trans('common::application.The number of names that have fetched data') .' :  ' .sizeof($suc) . ' ,  ' .
                                                trans('common::application.Number of names not registered in civil registry')  .' :  ' .sizeof($invalid_cards) . ' ,  ' .
                                                trans('common::application.Number of names non beneficiaries count')  .' :  ' .sizeof($not_beneficiaries_data) . ' ,  ' .
                                                trans('common::application.Number of duplicates').' :  '.$duplicated ,
                                            'download_token' => $token ]);
                                    }

                                }



                            }

                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }

                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);

            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }


    //----------------------------------------------------------------------------------------------------------------------//
    //----------------------------------------------------------------------------------------------------------------------//

    /************ Photo ************/
    // get citizen photo using card
    public function loadPhoto($id) {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');


        $document_type_id = 16;
        $person = Person::where(function ($q) use ($id){
            $q->where('id_card_number',$id);
        })->first();


        if (!is_null($person)) {
            $per_photo=\Common\Model\PersonDocument::where(['person_id' => $person->id ,'document_type_id'=>$document_type_id])->first();
            if(!is_null($per_photo)){
                $file = File::findOrFail($per_photo->document_id);

                if(is_null($file)){
                    return response()->file(base_path('storage/app/emptyUser.png'));
                }

                $image = base_path('storage/app/' . $file->filepath);
                if (!is_file($image)) {
                    return response()->file(base_path('storage/app/emptyUser.png'));
                }
                $imageMimeTypes = ['image/gif','image/jpeg','image/png','image/psd','image/bmp','image/tiff','image/tiff','image/x-ms-bmp',
                    'image/jp2','image/iff','image/vnd.wap.wbmp','image/xbm','mage/vnd.microsoft.icon','image/webp'];

                if (!in_array($file->mimetype, $imageMimeTypes)) {
                    $fileType = \mime_content_type(base_path('storage/app/' . $file->filepath));
                    if (!in_array($fileType, $imageMimeTypes)) {
                        return response()->file(base_path('storage/app/emptyUser.png'));
                    }
                }
//                return response()->header('not_has_image', true)->file($image);
                return response()->file($image);


            }
        }

        $file_contents = GovServices::getPhoto($id);

        /*
               if (!$file_contents['status']) {
                    return response()->json(['status'=>false,'msg'=>$file_contents['message']]);
               }
        */
//        if (!is_file($file_contents)) {
////            return response()->file(base_path('storage/app/emptyUser.png'));
//        }

        return response($file_contents['image'])
            ->header('Cache-Control', 'no-cache private')
            ->header('Content-Description', 'File Transfer')
            ->header('Content-Type', 'application/octet-stream')
            ->header('Content-length', strlen($file_contents['image']))
            ->header('Content-Disposition', 'attachment; filename=personalPhoto')
            ->header('Content-Transfer-Encoding', 'binary');
    }
    // save citizen photo using card
    public function savePhoto(Request $request) {

        $card = $request->id_card_number;
        $blob = GovServices::getPhoto($card);

//        if (!is_file($blob)) {
//            return response()->json(['status'=>false ,'msg'=> trans('common::application.no return image from Government service')]);
//        }
        if(isset($blob['image'])){
            $token = md5(uniqid());
            $file_put = file_put_contents(storage_path('app/documents/').$token.'.png', $blob['image']);
            if ($file_put) {
                $path = storage_path('app/documents/').$token.'.png';
                $person = Person::where(function ($q) use ($card){
                    $q->where('id_card_number',$card);
                })->first();

                $name=$person->first_name.' '.$person->second_name.' '.$person->third_name.' '.$person->last_name;
                $file = new File();
                $file->file_status_id = 1;
                $file->name = trans('sponsorship::application.per_photo') . ' ' . $name;
                $file->filepath = 'documents/'.$token.'.png';
                $file->size = filesize($path);
                $file->mimetype = 'image/png' ;
                $file->created_by = Auth::user()->id;
                if($file->save()){
                    $document_type_id = 16;
                    $per_photo=\Common\Model\PersonDocument::where(['person_id' => $person->id ,'document_type_id'=>$document_type_id])->first();
                    if(is_null($per_photo)){
                        $per_photo=new \Common\Model\PersonDocument();
                        $per_photo->person_id=$person->id;
                        $per_photo->document_type_id=$document_type_id;
                    }

                    $per_photo->document_id=$file->id;
                    if($per_photo->save()){
                        $msg=trans('common::application.Has updated their profile photo').':  ' . ' "'.$name. '" ';
                        $action='CASE_UPDATED';
                        \Log\Model\Log::saveNewLog($action,$msg);
                        return response()->json(["status" => 'true' ,'path'=>$path ]);
                    }
                }

            }
        }

        return response()->json(['status'=>false ,'msg'=> trans('common::application.no return image from Government service')]);
    }
    // get citizen photo using card
    public function getPhoto($id) {


        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $file_contents = GovServices::getPhoto($id);

//        if (!is_file($file_contents)) {
//            return response()->file(base_path('storage/app/emptyUser.png'));
//        }

        return response($file_contents['image'])
            ->header('Cache-Control', 'no-cache private')
            ->header('Content-Description', 'File Transfer')
            ->header('Content-Type', 'application/octet-stream')
            ->header('Content-length', strlen($file_contents['image']))
            ->header('Content-Disposition', 'attachment; filename=personalPhoto')
            ->header('Content-Transfer-Encoding', 'binary');
    }



}
