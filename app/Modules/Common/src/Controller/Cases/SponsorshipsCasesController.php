<?php

namespace Common\Controller\Cases;

use Common\Model\AbstractCases;
use Illuminate\Http\Request;
use Sponsorship\Model\SponsorshipCases;

class SponsorshipsCasesController extends AbstractCasesController
{
    protected $type = AbstractCases::TYPE_SPONSORSHIPS;

    // update goverment data of cases
    public function goverment_update(Request $request)
    {
        return parent::goverment_update($request);
    }
    // update status and category of cases
    public function updateStatusAndCategories(Request $request)
    {
        return parent::updateStatusAndCategories($request);
    }

    // ************ GET CASES
    public function filterCases(Request $request)
    {
        if($request->get('action') =='ExportToExcel' || $request->get('action') =='ExportToWord') {
            $this->authorize('export', \Common\Model\SponsorshipsCases::class);
        }else{
            $this->authorize('manage', \Common\Model\SponsorshipsCases::class);
        }
        return parent::filterCases($request);
    }
    // ************ GET CASE InCompletes 
    public function getInCompleteCases(Request $request)
    {
        $this->authorize('manage',\Common\Model\SponsorshipsCases::class);
        return parent::getInCompleteCases($request);
    }

    // ************ CHANGE CATEGORY 
    public function changeCategory(Request $request,$id)
    {
        $this->authorize('changeCategory',\Common\Model\SponsorshipsCases::class);
        return parent::changeCategory($request,$id);
    }
    public function changeCategoryToGroup(Request $request,$id)
    {
        $this->authorize('changeCategory',\Common\Model\SponsorshipsCases::class);
        return parent::changeCategoryToGroup($request,$id);
    }
    // ************ GET CASES ATTACHMENTS 
    public function getCaseAttachments(Request $request)
    {
        $this->authorize('manage',\Common\Model\SponsorshipsCases::class);
        return parent::getCaseAttachments($request);
    }

    // ************ GET CASE FORM 
    public function word(Request $request)
    {
        $this->authorize('export',\Common\Model\SponsorshipsCases::class);
        return parent::word($request);
    }

    // ************ GET CASE ATTACHMENTS 
    public function exportCaseDocument($id,Request $request)
    {
        $this->authorize('export',\Common\Model\SponsorshipsCases::class);
        return parent::exportCaseDocument($id,$request);
    }


    // ************ UPDATE CASE STATUS 
    public function update(Request $request ,$id)
    {
        $this->authorize('update',\Common\Model\SponsorshipsCases::class);
        return parent::update($request,$id);
    }

    // ************ SAVE CASE STATUS 
    public function store(Request $request)
    {
        $this->authorize('create',\Common\Model\SponsorshipsCases::class);
        $request->request->add(['category_type'=> $this->type]);
        return parent::store($request);
    }

    // ************ SOFT DELETE OF CASE 
    public function destroy($id,Request $request)
    {
        $this->authorize('delete',\Common\Model\SponsorshipsCases::class);
        return parent::destroy($id,$request);
    }
    // ************ RESTORE DELETED CASE 
    public function restore($id)
    {
        $this->authorize('update', \Common\Model\SponsorshipsCases::class);
        return parent::restore($id);
    }
    // ************ GET CASE STATUS LOGS
    public function getStatusLogs($id)
    {
        $this->authorize('view', \Common\Model\SponsorshipsCases::class);
        return parent::getStatusLogs($id);
    }

    // set case attachment
    public function setAttachment(Request $request ,$id)
    {
        $this->authorize('create', \Common\Model\SponsorshipsCases::class);
        $this->authorize('update', \Common\Model\SponsorshipsCases::class);
        return parent::setAttachment($request,$id);
    }

    // set case attachment
    public function setCaseAttachment(Request $request ,$id)
    {
//        $this->authorize('create', \Common\Model\SponsorshipsCases::class);
//        $this->authorize('update', \Common\Model\SponsorshipsCases::class);
        return parent::setCaseAttachment($request,$id);
    }

    // get case sponsor list
    public function getSponsorList(Request $request)
    {
        return response()->json(['items'=>SponsorshipCases::getSponsorList($request->case_id,$request->status)]);
    }

    // import from excel to update case data
    public function importToUpdate(Request $request)
    {
//        $this->authorize('import', \Common\Model\SponsorshipsCases::class);
        return parent::importToUpdate($request);
    }

    // import from excel to add case
    public function import(Request $request)
    {
//        $this->authorize('import', \Common\Model\SponsorshipsCases::class);
        return parent::import($request);
    }

    // import from excel to add case family
    public function importFamily(Request $request)
    {
//        $this->authorize('import', \Common\Model\SponsorshipsCases::class);
        return parent::importFamily($request);
    }

    // import case template
    public function importTemplate(){
        return response()->json(['download_token' => 'import-sponsorships-cases-template']);
    }
    // import case family template
    public function importFamilyTemplate(){
        return response()->json(['download_token' => 'sponsorship-family-import-template']);
    }

    // import update case template
    public function importTpUpdateTemplate(){
        return parent::importTpUpdateTemplate();
    }

    // check Cases using excel sheet according card number
    public function check(Request $request)
    {
        return parent::check($request);
    }
}