<?php

namespace Common\Controller\Cases;
use Auth\Model\Setting;
use Auth\Model\User;
use Common\Model\AidsCases;
use Common\Model\BlockIDCard;
use Common\Model\CaseFile;
use Common\Model\CloneGovernmentPersons;
use Common\Model\GovServices;
use Common\Model\PersonDocument;
use App\Http\Controllers\Controller;
use Common\Model\PersonModels\PersonAid;
use Common\Model\PersonModels\PersonBank;
use Common\Model\Relays\Relays;
use Common\Model\Transfers\Transfers;
use Document\Model\File;
use Forms\Model\FormsCasesData;
use Illuminate\Http\Request;
use Common\Model\CaseModel;
use Common\Model\ValidatorModel;
use Common\Model\Person;
use Log\Model\CardLog;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\IOFactory;
use Elibyy\TCPDF\TCPDF;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PDF;
use Organization\Model\OrgLocations;
use Setting\Model\AidSource;

class AbstractCasesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // update goverment data of cases
    public function goverment_update(Request $request)
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $father_kinship_id = 1;
        $mother_kinship_id = 5;
        $husband_kinship_id = 29;
        $wife_kinship_id = 21;
        $daughter_kinship_id = 22;
        $son_kinship_id = 2;

        if($request->source =='cards'){
            $records= $request->cases;
            $total=sizeof($records);
            if ($total > 0) {

                $validFile=true;
                $duplicated =0;
                $success =0;

                $final_records=[];
                $invalid_cards=[];
                $un_register_cards=[];
                $not_update=[];
                $processed=[];

                foreach ($records as $card) {
                    if(!in_array($card,$processed)) {
                        $row = GovServices::allByCard($card);
                        if($row['status'] == true) {
                            $mRec = (Object)$row['row'];
                            $person_ = Person::where('id_card_number',$card)->first();
                            $map = GovServices::personDataMap($row['row']);
                            $to_update =array("prev_family_name",'last_name', "marital_status_id", "birthday", "death_date",);

                            foreach ($to_update as $key){
                                if(isset($map[$key])){
                                    $person_->$key = $map[$key];
                                }
                            }

                            $person_->has_commercial_records = 0 ;
                            $person_->active_commercial_records = 0 ;
                            $person_->gov_commercial_records_details = ' ' ;

                            $mRec->commercial_data = [] ;
                            $CommercialRecords = GovServices::commercialRecords($card);
                            if($CommercialRecords['status'] != false){
                                $commercial_ =$CommercialRecords['row'];
                                $person_->has_commercial_records = 1 ;
                                $gov_commercial_records_details = ' ' ;

                                foreach ($commercial_ as $ke_ => $v_){
                                    if($v_->IS_VALID_DESC == "فعال"){
                                        $person_->active_commercial_records++;
                                    }

                                    $gov_commercial_records_details .= '( ' . $v_->REGISTER_NO  . '   -   ' .
                                        $v_->COMP_NAME  . '   -   ' .
                                        $v_->START_DATE  . '   -   ' .
                                        $v_->IS_VALID_DESC
                                        .' )';
                                }
                                $person_->gov_commercial_records_details = $gov_commercial_records_details;

                                $mRec->commercial_data = $commercial_ ;
                            }

                            CloneGovernmentPersons::saveNew($mRec);

                            if (sizeof($row->all_relatives) > 0 ){

                                $PARENT_RELATIVE_DESC = ["أب" , "أم","اب" , "ام"];
                                $SPONSES_RELATIVE_DESC = ["زوج/ة"];
                                $CHILD_RELATIVE_DESC = ["ابن/ة"];

                                foreach ($row->all_relatives as $key=> $relative) {
                                    if(in_array($relative->RELATIVE_DESC ,$CHILD_RELATIVE_DESC) ||
                                        in_array($relative->RELATIVE_DESC ,$SPONSES_RELATIVE_DESC)||
                                        in_array($relative->RELATIVE_DESC ,$PARENT_RELATIVE_DESC)) {
                                        $crd = $relative->IDNO_RELATIVE;
                                        $map = GovServices::inputsMap($relative);
                                        $map['kinship_id'] =$relative->kinship_id;
                                        $map['kinship_name']=$relative->kinship_name;

                                        $find=Person::where('id_card_number',$crd)->first();
                                        if(!is_null($find)){
                                            $map['person_id'] = $find->id;
                                        }

                                        if(in_array($relative->RELATIVE_DESC ,$CHILD_RELATIVE_DESC)){
                                            if($person_->gender == 1){
                                                $map['father_id']= $person_->id;
                                            }else{
                                                $map['mother_id']= $person_->id;
                                            }
                                        }

                                        if(($person_->gender == 1) &&
                                            in_array($relative->RELATIVE_DESC ,$SPONSES_RELATIVE_DESC)){
                                            $map['husband_id']= $person_->id;
                                        }

                                        $relative_row = Person::savePerson($map);

                                        if(in_array($relative->RELATIVE_DESC ,$PARENT_RELATIVE_DESC)){
                                            if($person_->gender == 1){
                                                $person_->father_id= $relative_row['id'];
                                            }else{
                                                $person_->mother_id= $relative_row['id'];
                                            }
                                        }
                                        if(($person_->gender == 2) &&
                                            in_array($relative->RELATIVE_DESC ,$SPONSES_RELATIVE_DESC)){
                                            $person_->husband_id= $relative_row['id'];
                                        }

                                        CloneGovernmentPersons::saveNewWithRelation($crd,$relative);
                                    }
                                }
                            }

                            $person_->save();
                            $main= (array)$row['row'];
                            $final_records[]=$main;
                            $success++;
                        }
                        else{
                            if(!in_array($card,$invalid_cards)){
                                $invalid_cards[]=$card;
                            }
                        }
                        $processed[]=$card;
                    }
                }

                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($final_records,$invalid_cards,$processed) {
                    $excel->setTitle(trans('common::application.un_register_cards'));
                    $excel->setDescription(trans('common::application.not_update'));
                    if(sizeof($final_records) > 0){
                        $excel->sheet(trans('common::application.family_sheet'), function($sheet) use($final_records) {

                            $sheet->setRightToLeft(true);
                            $sheet->setAllBorders('thin');
                            $sheet->setfitToWidth(true);
                            $sheet->setHeight(1,40);
                            $sheet->getDefaultStyle()->applyFromArray([
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                            ]);
                            $sheet->getStyle("A1:G1")->applyFromArray(['font' => ['bold' => true]]);

                            $sheet ->setCellValue('A1',trans('common::application.#'));
                            $sheet ->setCellValue('B1',trans('common::application.full_name'));
                            $sheet ->setCellValue('C1',trans('common::application.id_card_number'));
                            $sheet ->setCellValue('D1',trans('common::application.gender'));
                            $sheet ->setCellValue('E1',trans('common::application.marital_status'));
                            $sheet ->setCellValue('F1',trans('common::application.birthday'));
                            $sheet ->setCellValue('G1',trans('common::application.death_date'));

                            $z= 2;
                            foreach($final_records as $k=>$main ) {

                                $main = (Object) $main;

                                $sheet->setCellValue('A' . $z, $k+1);
                                $sheet->setCellValue('B' . $z,
                                    ((is_null($main->FNAME_ARB) || $main->FNAME_ARB == ' ') ? ' ' : $main->FNAME_ARB) . ' ' .
                                    ((is_null($main->SNAME_ARB) || $main->SNAME_ARB == ' ') ? ' ' : $main->SNAME_ARB) . ' ' .
                                    ((is_null($main->TNAME_ARB) || $main->TNAME_ARB == ' ') ? ' ' : $main->TNAME_ARB) . ' ' .
                                    ((is_null($main->LNAME_ARB) || $main->LNAME_ARB == ' ') ? ' ' : $main->LNAME_ARB)
                                );
                                $sheet->setCellValue('C' . $z, (is_null($main->IDNO) || $main->IDNO == ' ') ? '-' : $main->IDNO);
                                $sheet->setCellValue('D' . $z, (is_null($main->SEX) || $main->SEX == ' ') ? '-' : $main->SEX);
                                $sheet->setCellValue('E' . $z, (is_null($main->SOCIAL_STATUS) || $main->SOCIAL_STATUS == ' ') ? '-' : $main->SOCIAL_STATUS);
                                $sheet->setCellValue('F' . $z, (is_null($main->BIRTH_DT) || $main->BIRTH_DT == ' ') ? '-' : $main->BIRTH_DT);
                                $sheet->setCellValue('G' . $z, (is_null($main->DETH_DT) || $main->DETH_DT == ' ') ? '-' : $main->DETH_DT);
                                $z++;
                            }

                        });
                    }
                    if(sizeof($invalid_cards) > 0){
                        $excel->sheet(trans('common::application.invalid_cards'), function($sheet) use($invalid_cards) {

                            $sheet->setRightToLeft(true);
                            $sheet->setAllBorders('thin');
                            $sheet->setfitToWidth(true);
                            $sheet->setHeight(1,40);
                            $sheet->getDefaultStyle()->applyFromArray([
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                            ]);

                            $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                            $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                            $z= 2;
                            foreach($invalid_cards as $v){
                                $sheet ->setCellValue('A'.$z,$v);
                                $z++;
                            }

                        });
                    }

                })->store('xlsx', storage_path('tmp/'));

                \Log\Model\Log::saveNewLog('CARD_CKECK', trans('common::application.The user has checked the identification numbers fetching data from the civil register, the number of records checked') .$total);

                if( $total == ( $success +$duplicated)){
                    return response()->json(['status' => 'success','msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.All numbers successfully checked') ,'download_token' => $token]);
                }
                else{
                    return response()->json([ 'status' => 'success',
                        'download_token' => $token,
                        'msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.Some numbers were successfully checked, as the total number') .' :  '. $total . ' ,  '.
                            trans('common::application.The number of names that have fetched data') .' :  ' .$success . ' ,  ' .
                            trans('common::application.Number of names not registered in civil registry')  .' :  ' .sizeof($invalid_cards)
                    ]);
                }

            }
        }
        else{
            $importFile=$request->file ;
            if ($importFile->isValid()) {

                $path =  $importFile->getRealPath();
                $excel = \PHPExcel_IOFactory::load( $path );
                $sheets = $excel->getSheetNames();

                if(\App\Http\Helpers::sheetFound("data",$sheets)){
                    $first=Excel::selectSheets('data')->load($path)->first();
                    if(!is_null($first)){
                        $keys = $first->keys()->toArray();

                        if(sizeof($keys) >0 ){
                            $validFile = false;
                            foreach ($keys as $item) {
                                if ( $item == 'rkm_alhoy' ) {
                                    $validFile = true;
                                    break;
                                }
                            }

                            if($validFile){
                                $records= \Excel::selectSheets('data')->load($path)->get();
                                $total=sizeof($records);

                                if ($total > 0) {
                                    $duplicated =0;
                                    $success =0;
                                    $final_records=[];
                                    $invalid_cards=[];
                                    $un_register_cards=[];
                                    $not_update=[];
                                    $processed=[];

                                    foreach ($records as $key =>$value) {
                                        $card =(int)$value['rkm_alhoy'];
                                        if(in_array($card,$processed)){
                                            $duplicated++;
                                        }
                                        else{
                                            if(strlen($card)  <= 9) {
                                                if(GovServices::checkCard($card)){
                                                    $person_ = Person::where('id_card_number',$card)->first();
                                                    $row = GovServices::byCard($card);

                                                    if($row['status'] == true){
                                                        $dt = $row['row'];
                                                        $map = GovServices::personDataMap($dt);
                                                        $map['has_commercial_records']=0;
                                                        $map['active_commercial_records']=0;
                                                        $map['gov_commercial_records_details']=  ' ' ;

                                                        $dt->commercial_data = [];
                                                        $CommercialRecords = GovServices::commercialRecords($card);
                                                        if($CommercialRecords['status'] != false){
                                                            $commercial_ =$CommercialRecords['row'];
                                                            $map['has_commercial_records'] = 1 ;
                                                            $gov_commercial_records_details = ' ' ;

                                                            foreach ($commercial_ as $ke_ => $v_){
                                                                if($v_->IS_VALID_DESC == "فعال"){
                                                                    $map['active_commercial_records']++;
                                                                }

                                                                $gov_commercial_records_details .= '( ' . $v_->REGISTER_NO  . '   -   ' .
                                                                    $v_->COMP_NAME  . '   -   ' .
                                                                    $v_->START_DATE  . '   -   ' .
                                                                    $v_->IS_VALID_DESC
                                                                    .' )';
                                                            }
                                                            $map['gov_commercial_records_details'] = $gov_commercial_records_details;
                                                            $dt->commercial_data =$commercial_;
                                                        }

                                                        if (!is_null($person_)){
                                                            $map['person_id']=$person_->id;
                                                        }else{
                                                            $person_ = (Object) Person::insertPerson($map);
                                                        }
                                                        CloneGovernmentPersons::saveNew($dt);
                                                        if (sizeof($dt->all_relatives) > 0 ){

                                                            $PARENT_RELATIVE_DESC = ["أب" , "أم","اب" , "ام"];
                                                            $SPONSES_RELATIVE_DESC = ["زوج/ة"];
                                                            $CHILD_RELATIVE_DESC = ["ابن/ة"];

                                                            foreach ($dt->all_relatives as $kk=> $relative) {

                                                                if(in_array($relative->RELATIVE_DESC ,$CHILD_RELATIVE_DESC) ||
                                                                   in_array($relative->RELATIVE_DESC ,$SPONSES_RELATIVE_DESC)||
                                                                   in_array($relative->RELATIVE_DESC ,$PARENT_RELATIVE_DESC)) {

                                                                    $crd = $relative->IDNO_RELATIVE;
                                                                    $map = GovServices::inputsMap($relative);
                                                                    $map['kinship_id'] =$relative->kinship_id;
                                                                    $map['kinship_name']=$relative->kinship_name;

                                                                    $find=Person::where('id_card_number',$crd)->first();
                                                                    if(!is_null($find)){
                                                                        $map['person_id'] = $find->id;
                                                                    }

                                                                    if(in_array($relative->RELATIVE_DESC ,$CHILD_RELATIVE_DESC)){
                                                                        if($person_->gender == 1){
                                                                            $map['father_id']= $person_->id;
                                                                        }else{
                                                                            $map['mother_id']= $person_->id;
                                                                        }
                                                                    }

                                                                    if(($person_->gender == 1) &&
                                                                        in_array($relative->RELATIVE_DESC ,$SPONSES_RELATIVE_DESC)){
                                                                        $map['husband_id']= $person_->id;
                                                                    }

                                                                    $relative_row = Person::savePerson($map);

                                                                    if(in_array($relative->RELATIVE_DESC ,$PARENT_RELATIVE_DESC)){
                                                                        if($person_->gender == 1){
                                                                            $person_->father_id= $relative_row['id'];
                                                                        }else{
                                                                            $person_->mother_id= $relative_row['id'];
                                                                        }
                                                                    }

                                                                    if(($person_->gender == 2) &&
                                                                        in_array($relative->RELATIVE_DESC ,$SPONSES_RELATIVE_DESC)){
                                                                        $person_->husband_id= $relative_row['id'];
                                                                    }

                                                                    CloneGovernmentPersons::saveNewWithRelation($crd,$relative);
                                                                }
                                                            }
                                                        }

                                                        $person_->save();
                                                        $main= (array) $dt;
                                                        $final_records[]=$main;
                                                        $success++;

                                                    }else{
                                                        if(!in_array($card,$invalid_cards)){
                                                            $invalid_cards[]=$card;
                                                        }
                                                    }


                                                }
                                                else{
                                                    if(!in_array($card,$invalid_cards)){
                                                        $invalid_cards[]=$card;
                                                    }
                                                }
                                            }
                                            else{
                                                if(!in_array($card,$invalid_cards)){
                                                    $invalid_cards[]=$card;
                                                }
                                            }

                                            $processed[]=$card;
                                        }
                                    }

                                    if($total == sizeof($invalid_cards)){
                                        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.All names entered are not registered in the Civil Registry')  ]);
                                    }
                                    elseif($total == sizeof($un_register_cards)){
                                        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.All names entered are not registered in the system')  ]);
                                    }
                                    elseif($total == sizeof($not_update)){
                                        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.All names entered date has not been updated because it does not exist')  ]);
                                    }
                                    else{

                                        $token = md5(uniqid());
                                        \Excel::create('export_' . $token, function($excel) use($final_records,$invalid_cards,$processed,$un_register_cards,$not_update) {
                                            $excel->setTitle(trans('common::application.un_register_cards'));
                                            $excel->setDescription(trans('common::application.not_update'));
                                            if(sizeof($final_records) > 0){
                                                $excel->sheet(trans('common::application.family_sheet'), function($sheet) use($final_records) {

                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setHeight(1,40);
                                                    $sheet->getDefaultStyle()->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                    ]);
                                                    $sheet->getStyle("A1:G1")->applyFromArray(['font' => ['bold' => true]]);

                                                    $sheet ->setCellValue('A1',trans('common::application.#'));
                                                    $sheet ->setCellValue('B1',trans('common::application.full_name'));
                                                    $sheet ->setCellValue('C1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('D1',trans('common::application.gender'));
                                                    $sheet ->setCellValue('E1',trans('common::application.marital_status'));
                                                    $sheet ->setCellValue('F1',trans('common::application.birthday'));
                                                    $sheet ->setCellValue('G1',trans('common::application.death_date'));

                                                    $z= 2;
                                                    foreach($final_records as $k=>$main ) {

                                                        $main = (Object) $main;

                                                        $sheet->setCellValue('A' . $z, $k+1);
                                                        $sheet->setCellValue('B' . $z,
                                                            ((is_null($main->FNAME_ARB) || $main->FNAME_ARB == ' ') ? ' ' : $main->FNAME_ARB) . ' ' .
                                                            ((is_null($main->SNAME_ARB) || $main->SNAME_ARB == ' ') ? ' ' : $main->SNAME_ARB) . ' ' .
                                                            ((is_null($main->TNAME_ARB) || $main->TNAME_ARB == ' ') ? ' ' : $main->TNAME_ARB) . ' ' .
                                                            ((is_null($main->LNAME_ARB) || $main->LNAME_ARB == ' ') ? ' ' : $main->LNAME_ARB)
                                                        );
                                                        $sheet->setCellValue('C' . $z, (is_null($main->IDNO) || $main->IDNO == ' ') ? '-' : $main->IDNO);
                                                        $sheet->setCellValue('D' . $z, (is_null($main->SEX) || $main->SEX == ' ') ? '-' : $main->SEX);
                                                        $sheet->setCellValue('E' . $z, (is_null($main->SOCIAL_STATUS) || $main->SOCIAL_STATUS == ' ') ? '-' : $main->SOCIAL_STATUS);
                                                        $sheet->setCellValue('F' . $z, (is_null($main->BIRTH_DT) || $main->BIRTH_DT == ' ') ? '-' : $main->BIRTH_DT);
                                                        $sheet->setCellValue('G' . $z, (is_null($main->DETH_DT) || $main->DETH_DT == ' ') ? '-' : $main->DETH_DT);
                                                        $z++;
                                                    }

                                                });
                                            }
                                            if(sizeof($not_update) > 0){
                                                $excel->sheet(trans('common::application.not_update'), function($sheet) use($not_update) {

                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setHeight(1,40);
                                                    $sheet->getDefaultStyle()->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                    ]);

                                                    $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                    $z= 2;
                                                    foreach($not_update as $v){
                                                        $sheet ->setCellValue('A'.$z,$v);
                                                        $z++;
                                                    }

                                                });
                                            }
                                            if(sizeof($un_register_cards) > 0){
                                                $excel->sheet(trans('common::application.un_register_cards'), function($sheet) use($un_register_cards) {

                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setHeight(1,40);
                                                    $sheet->getDefaultStyle()->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                    ]);

                                                    $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                    $z= 2;
                                                    foreach($un_register_cards as $v){
                                                        $sheet ->setCellValue('A'.$z,$v);
                                                        $z++;
                                                    }

                                                });
                                            }
                                            if(sizeof($invalid_cards) > 0){
                                                $excel->sheet(trans('common::application.invalid_cards'), function($sheet) use($invalid_cards) {

                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setHeight(1,40);
                                                    $sheet->getDefaultStyle()->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                    ]);

                                                    $sheet->getStyle("A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet->setCellValue('A1',trans('common::application.id_card_number'));

                                                    $z= 2;
                                                    foreach($invalid_cards as $v){
                                                        $sheet ->setCellValue('A'.$z,$v);
                                                        $z++;
                                                    }

                                                });
                                            }

                                        })->store('xlsx', storage_path('tmp/'));

                                        \Log\Model\Log::saveNewLog('CARD_CKECK', trans('common::application.The user has checked the identification numbers fetching data from the civil register, the number of records checked') .$total);

                                        if( $total == ( $success +$duplicated)){
                                            return response()->json(['status' => 'success','msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.All numbers successfully checked') ,'download_token' => $token]);
                                        }
                                        else{
                                            return response()->json([ 'status' => 'success',
                                                'msg'=>trans('common::application.the results file is being downloaded,'). trans('common::application.Some numbers were successfully checked, as the total number') .' :  '. $total . ' ,  '.
                                                    trans('common::application.The number of names that have fetched data') .' :  ' .$success . ' ,  ' .
                                                    trans('common::application.Number of names not registered in civil registry')  .' :  ' .sizeof($invalid_cards) . ' ,  ' .
                                                    trans('common::application.Number of names with no death history in civil registry') .' :  ' .sizeof($not_update) . ' ,  ' .
                                                    trans('common::application.Number of names not registered in the system') .' :  ' .sizeof($un_register_cards) . ' ,  ' .
                                                    trans('common::application.Number of duplicates').' :  '.$duplicated ,
                                                'download_token' => $token ]);
                                        }

                                    }
                                }

                                return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                            }
                            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                        }
                    }
                    return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
                }

                return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
            }
            return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
        }

        return response()->json(["status" => 'failed', "msg"  => trans('aid::application.There is no update')]);
    }

    // update status and category of cases
    public function updateStatusAndCategories(Request $request)
    {
        $user = \Auth::user();
        $response = array();
        $restricted = 0;
        $single_person = 0;
        $dead_person = 0;
        $not_update_count = 0;
        $update_count = 0;
        $active_on_others = 0;

        if($request->cases){
            foreach ($request->cases as $key => $value) {
                $case = CaseModel::fetch(array('category_name' => true,'full_name'=>true),$value);

                if(!is_null($case->death_date) && $request->status == 0){
                    $dead_person++;
                    $restricted++;
                }else{
                    if($request->type == 'aids'){
                        if($request->category_id == 5  && $request->status == 0  && ( $case->marital_status_id == 10 ||  $case->marital_status_id == '10')){
                            $single_person++;
                            $not_update_count++;
                        }else{
                            if($request->status == 0  && ( $case->active_case_count != 0)){
                                $active_on_others++;
                                $not_update_count++;
                            }else{
                                $update_op = \Common\Model\AidsCases::where('id',$value)->update(['status' => $request->status]);
//                                \Common\Model\AidsCases::where('id',$value)->update(['category_id'=>$request->category_id]);
                                if($update_op) {
                                    $update_count++;
                                    \Common\Model\CasesStatusLog::create(['case_id'=>$value, 'user_id'=>$user->id, 'reason'=>$request->reason , 'status'=>$request->status,  'date'=>date("Y-m-d")]);
                                    $message =trans('common::application.Change rating') .$case->full_name .trans('common::application.for').$case->category_name;
                                    \Log\Model\Log::saveNewLog('CASE_UPDATED',$message);
                                }else{
                                    $not_update_count++;
                                }
                            }
                        }
                    }
                    else{
                        $update_op = \Common\Model\SponsorshipsCases::where('id',$value)->update(['status' => $request->status]);
//                        \Common\Model\SponsorshipsCases::where('id',$value)->update(['category_id'=>$request->category_id]);

                        if($update_op) {
                            $update_count++;
                            \Common\Model\CasesStatusLog::create(['case_id'=>$value, 'user_id'=>$user->id, 'reason'=>$request->reason , 'status'=>$request->status,  'date'=>date("Y-m-d")]);
                            $message =trans('common::application.Change rating') .$case->full_name .trans('common::application.for').$case->category_name;
                            \Log\Model\Log::saveNewLog('CASE_UPDATED',$message);
                        }else{
                            $not_update_count++;
                        }
                    }
                }

            }
            if($not_update_count == sizeof($request->cases)){
                $response["status"]= 'failed';
                $response["msg"]= trans('aid::application.There is no update');
            }else{
                if($update_count == sizeof($request->cases)){
                    $response["status"]= 'success';
                    $response["msg"]= trans('aid::application.The row is updated');
                    return response()->json(['status' => 'success','msg'=>trans('aid::application.The row is updated')]);
                }else{

                    $msg =  trans('common::application.some cases was updated, the total selected cases') .' :  '. sizeof($request->cases) . ' ,  '.
                        trans('common::application.number of updated is') .' :  ' .$update_count . ' ,  ' .
                        trans('common::application.number of not updated is') .' :  ' .$not_update_count ;

                    if($single_person > 0 ){
                        $msg .= ' ,  ' . trans('common::application.number of single_person') .' :  ' .$single_person;
                    }

                    if($dead_person > 0 ){
                        $msg .= ' ,  ' . trans('common::application.number of dead_person') .' :  ' .$dead_person;

                    }


                    return response()->json(['status' => 'success','msg'=> $msg]);

                }
            }

        }
        return response()->json($response);

    }

    /************ GET CASES*************/
    public function filterCases(Request $request)
    {
        $status=$request->get('action');
        $user = \Auth::user();

        $organization_id=$user->organization_id;
        $Org=\Organization\Model\Organization::findorfail($user->organization_id);

        $master=true;
        $filters= $request->all();
        if($Org->parent_id){
            $master=false;
        }

        $filters['category_type'] = $this->type;

        if($request->get('action') =='Basic') {
            $result= CaseModel::filterBasicCases($request->page,$filters,$organization_id,$master);
        }else{
            $result= CaseModel::filterCases($request->page,$filters,$organization_id,$master);

            if($request->get('action') =='filter') {
                $result->map(function ($row) {
                    $category_id  = $row->category_id;
                    $person_id  = $row->person_id;
                    $row->transfers  = Transfers::where(function ($query) use ($category_id,$person_id) {
                        $query->where('category_id', $category_id);
                        $query->where('person_id', $person_id);
                        $query->whereNotIn('id', function ($q) {
                            $q->select('transfer_id')->from('char_transfers_log');
                        });
                    })->count();

                    $row->accepted_transfers  = Transfers::where(function ($query) use ($category_id,$person_id) {
                                                    $query->where('category_id', $category_id);
                                                    $query->where('person_id', $person_id);
                                                    $query->whereIn('id', function ($q) {
                                                        $q->select('transfer_id')->from('char_transfers_log')->where('status', 1);
                                                    });
                                                })->count();


                    $row->approved_transfers  = Transfers::where(function ($query) use ($category_id,$person_id) {
                        $query->where('category_id', $category_id);
                        $query->where('person_id', $person_id);
                        $query->whereIn('id', function ($q) {
                            $q->select('transfer_id')->from('char_transfers_log')->where('status', 3);
                        });
                    })->count();

                    return $row;
                });
            }
        }

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        if($request->get('action') =='filter') {
            return response()->json(['organization_id'=>$organization_id,'master'=>$master,'Cases'  => $result,'date' => date('y-m-d')] );
        }
        else if($request->get('action') =='csv') {
            if($request->get('deleted') == true){
                $statistic=$result;
                $elements=array();
            }else{
                $statistic=$result['statistic'];
                $elements=$result['elements'];

            }

            $data=[];
            if(sizeof($statistic) !=0){

                $header_src=$statistic[0];
                $exl_header=[];
                $exl_header[]=trans('aid::application.#');
                foreach($header_src as $k =>$v){
                    if(!in_array($k,['case_id','person_id','data','elements'])){
                        if(substr( $k, 0, 7 ) === "mother_"){
                            $exl_header[]=trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.mother')  ." ) ";
                        }elseif(substr( $k, 0, 7 ) === "father_"){
                            $exl_header[]=trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.father')  ." ) ";
                        }elseif(substr( $k, 0, 9 ) === "guardian_"){
                            $exl_header[]=trans('sponsorship::application.'.substr($k,9))." ( ".trans('sponsorship::application.guardian')  ." ) ";
                        }else{
                            $exl_header[]=trans('aid::application.' . $k);
                        }
                    }
                }
                foreach($elements as $k1 =>$v2){
                    $exl_header[]=$v2->label;
                }
                $data[]=$exl_header;

                foreach($statistic as $key =>$value){
                    $temp=[];
                    $temp[]=$key+1;
                    foreach($value as $k =>$v){
                        if(!in_array($k,['case_id','person_id','data','elements'])){
                            $temp[]=$v;
                        }
                    }
                    foreach($elements as $k1 =>$v2){
                        $cuVal='__';
                        if(in_array($v2->id,$value->elements)){
                            $cuVal=$value->data[$v2->id];
                        }
                        if($cuVal == null){
                            $cuVal='__';
                        }
                        $temp[]= $cuVal;
                    }
                    $data[]=$temp;
                }

                $token = md5(uniqid());
                $file = fopen(storage_path('tmp/export_' . $token .'.csv'), 'w');
                fputs( $file, "\xEF\xBB\xBF" ); // UTF-8 BOM !!!!!

                foreach ($data as $row)
                {
                    fputcsv($file, $row,';');
                }

                return response()->json(['download_token' => $token,'organization_id'=>$organization_id,'master'=>$master,'Cases'  => $result,'date' => date('y-m-d')]);
            }

        }
        else if($request->get('action') =='ExportToExcel' || $request->get('action') =='Basic') {

            if($request->get('deleted') == true){
                $statistic=$result;
                $elements=array();
                $aid_source=array();
                $properties=array();
                $essentials=array();

            }else{
                $statistic=$result['statistic'];
                $elements=$result['elements'];
                $aid_source=$result['aid_source'];
                $properties=$result['properties'];
                $essentials=$result['essentials'];

            }

            $data=[];
            if(sizeof($statistic) !=0){
                foreach($statistic as $key =>$value){
                    $n=0;
                    $data[$key][trans('aid::application.#')]=$key+1;
                    foreach($value as $k =>$v){
                        if(!in_array($k,['case_id','person_id','data','elements','financial_aid_source_elements','financial_aid_source',
                            'non_financial_aid_source_elements','non_financial_aid_source','active_case_count',
                            'persons_properties_elements','persons_properties','persons_essentials_elements','persons_essentials'
                        ])){

                            if($v == null){
                                $v=' ';
                            }else{
                                $str = str_replace("-"," ",$v);
                                $str = ltrim($str);
                                $v = rtrim($str);
                            }
                            if(substr( $k, 0, 7 ) === "mother_"){
                                $data[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.mother')  ." ) "]= $v;
                            }elseif(substr( $k, 0, 7 ) === "father_"){
                                $data[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.father')  ." ) "]= $v;
                            }elseif(substr( $k, 0, 9 ) === "guardian_"){
                                $data[$key][trans('sponsorship::application.'.substr($k,9))." ( ".trans('sponsorship::application.guardian')  ." ) " ]= $v;
                            }else{
                                $data[$key][trans('aid::application.' . $k)]= $v;
                            }
                            $n++;
                        }
                    }

                    if(sizeof($aid_source)>0){
                        foreach($aid_source as $k1_ =>$v2_){
                            $cuVal='0';
                            if(in_array($v2_->id,$value->financial_aid_source_elements)){
                                $cuVal=$value->financial_aid_source[$v2_->id];
                            }
                            $data[$key][$v2_->name . '(' . trans('common::application.financial'). ')']= $cuVal; ;
                        }
                        foreach($aid_source as $k1_ =>$v2_){
                            $cuVal='0';
                            if(in_array($v2_->id,$value->non_financial_aid_source_elements)){
                                $cuVal=$value->non_financial_aid_source[$v2_->id];
                            }
                            $data[$key][$v2_->name . ' (' . trans('common::application.non_financial'). ') ']= $cuVal; ;
                        }
                    }

                    if(sizeof($properties)>0){
                        foreach($properties as $k1_0 =>$v2_0){
                            $cuVal='0';
                            if(in_array($v2_0->id,$value->persons_properties_elements)){
                                $cuVal=$value->persons_properties[$v2_0->id];
                            }
                            $data[$key][$v2_0->name]= $cuVal; ;
                        }
                    }

                    if(sizeof($essentials)>0){
                        foreach($essentials as $k1_0 =>$v2_0){
                            $cuVal='0';
                            if(in_array($v2_0->id,$value->persons_essentials_elements)){
                                $cuVal=$value->persons_essentials[$v2_0->id];
                            }
                            $data[$key][$v2_0->name]= $cuVal; ;
                        }
                    }

                    if(sizeof($elements)>0){
                        foreach($elements as $k1 =>$v2){
                            $cuVal=' ';
                            if(in_array($v2->id,$value->elements)){
                                $cuVal=$value->data[$v2->id];
                            }
                            if($cuVal == null){
                                $cuVal=' ';
                            }
                            $data[$key][$v2->label]= str_replace("-","،",$cuVal); ;
                        }
                    }

                }

                $n +=sizeof($aid_source);
                $n +=sizeof($aid_source);
                $n +=sizeof($properties);
                $n +=sizeof($essentials);
                $n +=sizeof($elements);
                for($r = ""; $n >= 0; $n = intval($n / 26) - 1)
                    $r = chr($n%26 + 0x41) . $r;

                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($data,$r) {
                    $excel->sheet('sheet', function($sheet) use($data,$r) {

                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $sheet->setHeight(1,30);

                        $sheet->getStyle("A1:".$r."1")->applyFromArray( [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold'      =>  true
                            ]
                        ]);
                        $sheet->getDefaultStyle()->applyFromArray( [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold' => false
                            ]
                        ]);
                        $sheet->fromArray($data);
                    });
                })->store('xlsx', storage_path('tmp/'));

                \Log\Model\Log::saveNewLog('CASE_EXPORTED',trans('common::application.Exported a list of aid cases'));
                return response()->json(['download_token' => $token,'organization_id'=>$organization_id,'master'=>$master,'Cases'  => $result,'date' => date('y-m-d')]);
            }

        }
        else if($request->get('action') =='FamilyMember' || $request->get('action') =='wives'||
                $request->get('action') =='childs' || $request->get('action') =='parents') {

            if($this->type == 1){
                $keys=["father_name","father_id_card_number", "kinship_name", "mother_name","mother_id_card_number", "guardian_name","guardian_id_card_number", "guardian_kinship", "id_card_number", "name", "gender", "birthday","age",
                    "birth_place" , "marital_status", "study" ,'study_type', "authority", "degree",
                    "stage", "grade", "school" , "monthly_income" , "working" , "work_job", "health_status" , "diseases_name" , "diseases_details"  ];
            }else{
                $keys=["father_id_card_number", "father_name", "id_card_number", "name", "gender", "birthday",
                    "birth_place" , "marital_status", "study" ,'study_type', "authority", "degree",
                    "stage", "grade", "school" , "monthly_income" , "working" , "work_job", "health_status" , "diseases_name" , "diseases_details" , "kinship_name" ];
            }


            $data=[];
            if(sizeof($result) !=0){
                foreach($result as $key =>$value){
                    $data[$key][trans('sponsorship::application.#')]=$key+1;
                    foreach($keys as $ke){
                        $f_ke=null;
                        $vlu = is_null($value->$ke) ? '__' : $value->$ke;
                        if(substr( $ke, 0, 7 ) === "mother_"){
                            $f_ke = trans('sponsorship::application.'.substr($ke,7))." ( ".trans('sponsorship::application.mother')  ." ) ";
                        }elseif(substr( $ke, 0, 7 ) === "father_"){
                            if($this->type == 1){
                                $f_ke = trans('sponsorship::application.'.substr($ke,7))." ( ".trans('sponsorship::application.father')  ." ) ";
                            }else{
                                $f_ke = trans('sponsorship::application.'.substr($ke,7))." ( ".trans('sponsorship::application.family_res')  ." ) ";
                            }
                        }elseif(substr( $ke, 0, 9 ) === "guardian_"){
                            $f_ke = trans('sponsorship::application.'.substr($ke,9))." ( ".trans('sponsorship::application.guardian')  ." ) ";
                        }elseif(substr( $ke, 0, 10 ) === "recipient_"){
                            $f_ke = trans('sponsorship::application.'.substr($ke,10))." ( ".trans('sponsorship::application.recipient')  ." ) ";
                        }elseif($ke === "kinship_name"){
                            $f_ke = trans('sponsorship::application.kinship_name')." ( ".trans('sponsorship::application.family_res')  ." ) ";
                        }else{
                            $f_ke = trans('sponsorship::application.' . $ke);
                        }
                        $data[$key][$f_ke]= $vlu;
                    }
                }

                $type= $this->type;
                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($data,$type) {
                    $excel->sheet(trans('common::application.Detection of family members - brothers') , function($sheet) use($data,$type) {

                        $sheet->setAllBorders('thin');
                        $sheet->setfitToWidth(true);
                        $sheet->setRightToLeft(true);
                        $sheet->setHeight(1,30);
                        $r='W';
                        if($this->type == 1){
                            $r='AC';
                        }
                        $sheet->getStyle("A1:".$r."1")->applyFromArray( [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold'      =>  true
                            ]
                        ]);
                        $sheet->getDefaultStyle()->applyFromArray( [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' =>[
                                'name'      =>  'Simplified Arabic',
                                'size'      =>  12,
                                'bold'      =>  false
                            ]
                        ]);
                        $sheet->fromArray($data);
                    });
                })->store('xlsx', storage_path('tmp/'));

                \Log\Model\Log::saveNewLog('CASE_EXPORTED',trans('common::application.Exported a list of aid cases'));
                return response()->json(['download_token' => $token,'organization_id'=>$organization_id,'master'=>$master,'Cases'  => $result,'date' => date('y-m-d')]);
            }

        }
        else if($request->get('action') =='ExportToWord') {
            $dirName = base_path('storage/app/templates/temp');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }

            foreach($result as $k=>$item){
                $dirName = base_path('storage/app/templates/CaseForm');
                if (!is_dir($dirName)) {
                    @mkdir($dirName, 0777, true);
                }
                $Template=\Common\Model\Template::where(['category_id'=>$item->category_id,'organization_id'=>$user->organization_id])->first();
                if($Template){
                    $path = base_path('storage/app/'.$Template->filename);
                }else{
                    if($this->type == 1){
                        $default_template_id='Sponsorship-default-template';
                    }else{
                        $default_template_id='aid-default-template';
                    }

                    $default_template=\Setting\Model\Setting::where(['id'=>$default_template_id,'organization_id'=>$user->organization_id])->first();

                    if($default_template){
                        $path = base_path('storage/app/'.$default_template->value);
                    }else{
                        $path = base_path('storage/app/templates/'.$default_template_id .'.docx');
                    }
                }

                $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);
                $templateProcessor->setValue('date', date('y-m-d'));
                $personalImagePath=PersonDocument::personalImagePath($item->person_id,$this->type);

                if(!is_null($personalImagePath)){
                    $templateProcessor->setImg('img', array('src'=> $personalImagePath,'size'=>[200,350]));
                }else{
                    $templateProcessor->setImg('img', array('src'=> base_path('storage/app/emptyUser.png'),'size'=>[200,350]));
                }

                $templateProcessor->setValue('date', date('y-m-d'));
                $templateProcessor->setValue('organization_name', $item->organization_name);
                $templateProcessor->setValue('category_name', $item->category_name);

                $templateProcessor->setValue('first_name', $item->first_name);
                $templateProcessor->setValue('second_name', $item->second_name);
                $templateProcessor->setValue('third_name', $item->third_name );
                $templateProcessor->setValue('last_name', $item->last_name );
                $templateProcessor->setValue('name', $item->full_name);
                $templateProcessor->setValue('en_first_name', $item->en_first_name);
                $templateProcessor->setValue('en_second_name', $item->en_second_name);
                $templateProcessor->setValue('en_third_name', $item->en_third_name);
                $templateProcessor->setValue('en_last_name', $item->en_last_name);
                $templateProcessor->setValue('en_name', $item->en_name);
                $templateProcessor->setValue('id_card_number', $item->id_card_number);
                $templateProcessor->setValue('card_type', $item->card_type);
                $templateProcessor->setValue('birthday', $item->birthday);
                $templateProcessor->setValue('age', $item->age);
                $templateProcessor->setValue('gender', $item->gender);
                $templateProcessor->setValue('gender_male', $item->gender_male);
                $templateProcessor->setValue('gender_female', $item->gender_female);
                $templateProcessor->setValue('birth_place', $item->birth_place);
                $templateProcessor->setValue('nationality', $item->nationality);
                $templateProcessor->setValue('refugee', $item->refugee);
                $templateProcessor->setValue('is_refugee', $item->is_refugee);
                $templateProcessor->setValue('per_refugee', $item->per_refugee);
                $templateProcessor->setValue('is_citizen', $item->is_citizen);
                $templateProcessor->setValue('per_citizen', $item->per_citizen);
                $templateProcessor->setValue('unrwa_card_number', $item->unrwa_card_number);
                $templateProcessor->setValue('marital_status_name', $item->marital_status_name);

                $templateProcessor->setValue('deserted', $item->deserted);
                $templateProcessor->setValue('is_deserted', $item->is_deserted);
                $templateProcessor->setValue('not_deserted', $item->not_deserted);

                $templateProcessor->setValue('receivables', $item->receivables);
                $templateProcessor->setValue('is_receivables', $item->is_receivables);
                $templateProcessor->setValue('is_ receivables', $item->not_receivables);
                $templateProcessor->setValue('not_receivables', $item->not_receivables);
                $templateProcessor->setValue('receivables_sk_amount', $item->receivables_sk_amount);

                $templateProcessor->setValue('is_qualified', $item->is_qualified);
                $templateProcessor->setValue('qualified', $item->qualified);
                $templateProcessor->setValue('not_qualified', $item->not_qualified);
                $templateProcessor->setValue('qualified_card_number', $item->qualified_card_number);

                $templateProcessor->setValue('has_commercial_records', $item->has_commercial_records);
                $templateProcessor->setValue('have_commercial_records', $item->have_commercial_records);
                $templateProcessor->setValue('not_have_commercial_records', $item->not_have_commercial_records);
                $templateProcessor->setValue('active_commercial_records', $item->active_commercial_records);
                $templateProcessor->setValue('gov_commercial_records_details', $item->gov_commercial_records_details);

                $templateProcessor->setValue('monthly_income', $item->monthly_income);
                $templateProcessor->setValue('actual_monthly_income', $item->actual_monthly_income);

                $templateProcessor->setValue('address', $item->address);
                $templateProcessor->setValue('country', $item->country);
                $templateProcessor->setValue('governarate', $item->governarate);
                $templateProcessor->setValue('nearLocation', $item->nearLocation);
                $templateProcessor->setValue('mosque_name', $item->mosque_name);

                if($this->type == 1){
                    $templateProcessor->setValue('city', $item->city);
                }else{
                    $templateProcessor->setValue('region', $item->region_name);
                    $templateProcessor->setValue('square', $item->square_name);
                }

                $templateProcessor->setValue('street_address', $item->street_address);

                $templateProcessor->setValue('family_cnt', $item->family_cnt);
                $templateProcessor->setValue('male_live', $item->male_live);
                $templateProcessor->setValue('female_live', $item->female_live);
                $templateProcessor->setValue('spouses', $item->spouses);

                $templateProcessor->setValue('individual_count', $item->individual_count);
                $templateProcessor->setValue('male_brothers', $item->male_brothers);
                $templateProcessor->setValue('female_brothers', $item->female_brothers);
                $templateProcessor->setValue('family_count', $item->family_count);
                $templateProcessor->setValue('male_count', $item->male_count);
                $templateProcessor->setValue('female_count', $item->female_count);

                $templateProcessor->setValue('father_brothers', $item->father_brothers);
                $templateProcessor->setValue('female_father_brothers', $item->female_father_brothers);
                $templateProcessor->setValue('male_father_brothers', $item->male_father_brothers);

                $templateProcessor->setValue('mother_brothers', $item->mother_brothers);
                $templateProcessor->setValue('female_mother_brothers', $item->female_mother_brothers);
                $templateProcessor->setValue('male_mother_brothers', $item->male_mother_brothers);

                $templateProcessor->setValue('brothers', $item->brothers);
                $templateProcessor->setValue('children', $item->children);
                $templateProcessor->setValue('male_children', $item->male_children);
                $templateProcessor->setValue('female_children', $item->female_children);

                $templateProcessor->setValue('phone', $item->phone);
                $templateProcessor->setValue('primary_mobile', $item->primary_mobile);
                $templateProcessor->setValue('wataniya', $item->wataniya);
                $templateProcessor->setValue('secondary_mobile', $item->secondary_mobile );

                $templateProcessor->setValue('working', $item->working);
                $templateProcessor->setValue('works', $item->works);
                $templateProcessor->setValue('is_working', $item->is_working);
                $templateProcessor->setValue('not_working', $item->not_working);
                $templateProcessor->setValue('can_work', $item->can_work);
                $templateProcessor->setValue('can_works', $item->can_works);
                $templateProcessor->setValue('can_working', $item->can_working);
                $templateProcessor->setValue('can_not_working', $item->can_not_working);
                $templateProcessor->setValue('work_job', $item->work_job);
                $templateProcessor->setValue('work_reason', $item->work_reason);
                $templateProcessor->setValue('work_status', $item->work_status);
                $templateProcessor->setValue('work_wage', $item->work_wage);
                $templateProcessor->setValue('work_location', $item->work_location);
                $templateProcessor->setValue('has_other_work_resources', $item->has_other_work_resources);
                $templateProcessor->setValue('has_other_resources', $item->has_other_resources);
                $templateProcessor->setValue('not_has_other_resources', $item->not_has_other_resources);
                $templateProcessor->setValue('gov_work_details', $item->gov_work_details);

                $templateProcessor->setValue('health_status', $item->health_status);
                $templateProcessor->setValue('good_health_status', $item->good_health_status);
                $templateProcessor->setValue('chronic_disease', $item->chronic_disease);
                $templateProcessor->setValue('disabled', $item->disabled);
                $templateProcessor->setValue('diseases_name', $item->diseases_name);
                $templateProcessor->setValue('has_disease', $item->has_disease);
                $templateProcessor->setValue('has_health_insurance', $item->has_health_insurance);
                $templateProcessor->setValue('have_health_insurance', $item->have_health_insurance);
                $templateProcessor->setValue('not_have_health_insurance', $item->not_have_health_insurance);
                $templateProcessor->setValue('health_insurance_number', $item->health_insurance_number);
                $templateProcessor->setValue('health_insurance_type', $item->health_insurance_type);
                $templateProcessor->setValue('has_device', $item->has_device);
                $templateProcessor->setValue('use_device', $item->use_device);
                $templateProcessor->setValue('not_use_device', $item->not_use_device);
                $templateProcessor->setValue('used_device_name', $item->used_device_name);
                $templateProcessor->setValue('health_details', $item->health_details);
                $templateProcessor->setValue('gov_health_details', $item->gov_health_details);
                $templateProcessor->setValue('property_types', $item->property_types);
                $templateProcessor->setValue('rent_value', $item->rent_value);
                $templateProcessor->setValue('roof_materials', $item->roof_materials);
                $templateProcessor->setValue('habitable', $item->habitable);
                $templateProcessor->setValue('house_condition', $item->house_condition);
                $templateProcessor->setValue('residence_condition', $item->residence_condition);
                $templateProcessor->setValue('indoor_condition', $item->indoor_condition);
                $templateProcessor->setValue('rooms', $item->rooms);
                $templateProcessor->setValue('area', $item->area);
                $templateProcessor->setValue('repair_notes', $item->repair_notes);
                $templateProcessor->setValue('need_repair', $item->need_repair);
                $templateProcessor->setValue('repair', $item->repair);
                $templateProcessor->setValue('not_repair', $item->not_repair);

                $templateProcessor->setValue('needs', $item->needs);
                $templateProcessor->setValue('promised', $item->promised);
                $templateProcessor->setValue('if_promised', $item->if_promised);
                $templateProcessor->setValue('was_promised', $item->was_promised);
                $templateProcessor->setValue('not_promised', $item->not_promised);
                $templateProcessor->setValue('reconstructions_organization_name', $item->reconstructions_organization_name);

                $templateProcessor->setValue('visitor', $item->visitor);
                $templateProcessor->setValue('notes', $item->notes);
                $templateProcessor->setValue('visited_at', $item->visited_at);
                $templateProcessor->setValue('visitor_opinion', $item->visitor_opinion);
                $templateProcessor->setValue('visitor_card', $item->visitor_card);
                $templateProcessor->setValue('visitor_evaluation', $item->visitor_evaluation);
                $templateProcessor->setValue('very_bad_condition_evaluation', $item->very_bad_condition_evaluation);
                $templateProcessor->setValue('bad_condition_evaluation', $item->bad_condition_evaluation);
                $templateProcessor->setValue('good_evaluation', $item->good_evaluation);
                $templateProcessor->setValue('very_good_evaluation', $item->very_good_evaluation);
                $templateProcessor->setValue('excellent_evaluation', $item->excellent_evaluation);

                $templateProcessor->setValue('study', $item->study);
                $templateProcessor->setValue('type', $item->type);
                $templateProcessor->setValue('level', $item->level);
                $templateProcessor->setValue('year', $item->year);
                $templateProcessor->setValue('points', $item->points);
                $templateProcessor->setValue('school', $item->school);
                $templateProcessor->setValue('grade', $item->grade);
                $templateProcessor->setValue('authority', $item->authority);
                $templateProcessor->setValue('stage', $item->stage);
                $templateProcessor->setValue('degree', $item->degree);
                $templateProcessor->setValue('specialization', $item->specialization);
                $templateProcessor->setValue('prayer', $item->prayer);
                $templateProcessor->setValue('prayer_reason', $item->prayer_reason);
                $templateProcessor->setValue('quran_reason', $item->quran_reason);
                $templateProcessor->setValue('quran_parts', $item->quran_parts);
                $templateProcessor->setValue('quran_chapters', $item->quran_chapters);
                $templateProcessor->setValue('quran_center', $item->quran_center);
                $templateProcessor->setValue('save_quran', $item->save_quran);
                $templateProcessor->setValue('visitor_note', $item->visitor_note);
                $templateProcessor->setValue('guardian_kinship', $item->guardian_kinship);
                $templateProcessor->setValue('guardian_is', $item->guardian_is);
                $templateProcessor->setValue('mother_is_guardian', $item->mother_is_guardian);

                if (sizeof($item->banks)!=0){
                    try{
                        $templateProcessor->cloneRow('r_bank_name', sizeof($item->banks));
                        $zxs=1;
                        foreach($item->banks as $rec) {
                            $templateProcessor->setValue('r_bank_name#' . $zxs, $rec->bank_name);
                            $templateProcessor->setValue('r_branch_name#' . $zxs, $rec->branch);
                            $templateProcessor->setValue('r_account_owner#' . $zxs, $rec->account_owner);
                            $templateProcessor->setValue('r_account_number#' . $zxs, $rec->account_number);
                            if($rec->check == true){
                                $templateProcessor->setValue('r_default#' . $zxs, trans('aid::application.yes') );
                            }else{
                                $templateProcessor->setValue('r_default#' . $zxs, trans('aid::application.no') );
                            }
                            $zxs++;
                        }
                    }catch (\Exception $e){}
                }else{
                    try{
                        $templateProcessor->cloneRow('r_bank_name', 1);
                        $templateProcessor->setValue('r_bank_name#' . 1, ' ');
                        $templateProcessor->setValue('r_branch_name#' . 1, ' ');
                        $templateProcessor->setValue('r_account_owner#' . 1, ' ');
                        $templateProcessor->setValue('r_account_number#' . 1,' ');
                        $templateProcessor->setValue('r_default#' . 1, ' ');
                    }catch (\Exception $e){  }
                }

                if($this->type == 1){
                    $setting_id = 'sponsorship_custom_form';
                }else{
                    $setting_id = 'aid_custom_form';
                }

                $CaseCustomData =FormsCasesData::getCaseCustomData($item->case_id,$item->category_id,$item->organization_id,$setting_id);

                if (sizeof($CaseCustomData)!=0){
                    try{
                        $templateProcessor->cloneRow('row_custom_name', sizeof($CaseCustomData));
                        $z=1;
                        foreach($CaseCustomData as $cValue) {
                            $templateProcessor->setValue('row_custom_name#' . $z, $cValue['label']);
                            $templateProcessor->setValue('row_custom_value#' . $z, $cValue['value']);
                            $z++;
                        }
                    }catch (\Exception $e){

                    }
                }
                else{
                    try{
                        $templateProcessor->cloneRow('row_custom_name',1);
                        $templateProcessor->setValue('row_custom_name#' . 1, ' ');
                        $templateProcessor->setValue('row_custom_value#' . 1, ' ');
                    }catch (\Exception $e){  }
                }

                if (sizeof($item->home_indoor)!=0){
                    try{
                        $templateProcessor->cloneRow('row_es_name', sizeof($item->home_indoor));
                        $z=1;
                        foreach($item->home_indoor as $record) {
                            $templateProcessor->setValue('row_es_name#' . $z, $record->name);
                            $templateProcessor->setValue('row_es_if_exist#' . $z, $record->exist);
                            $templateProcessor->setValue('row_es_needs#' . $z, $record->needs);
                            $templateProcessor->setValue('row_es_condition#' . $z, $record->essentials_condition);
                            $templateProcessor->setValue('row_es_not_exist#' . $z, $record->not_exist);
                            $templateProcessor->setValue('row_es_very_bad_condition#' . $z, $record->very_bad_condition);
                            $templateProcessor->setValue('row_es_bad_condition#' . $z, $record->bad_condition);
                            $templateProcessor->setValue('row_es_good#' . $z, $record->good);
                            $templateProcessor->setValue('row_es_very_good#' . $z, $record->very_good);
                            $templateProcessor->setValue('row_es_excellent#' . $z, $record->excellent);
                            $z++;
                        }
                    }catch (\Exception $e){  }
                }
                else{
                    try{
                        $templateProcessor->cloneRow('row_es_name', 1);
                        $templateProcessor->setValue('row_es_name#' . 1, ' ');
                        $templateProcessor->setValue('row_es_if_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_es_needs#' . 1, ' ');
                        $templateProcessor->setValue('row_es_condition#' . 1, ' ');
                        $templateProcessor->setValue('row_es_not_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_es_very_bad_condition#' . 1, ' ');
                        $templateProcessor->setValue('row_es_bad_condition#' . 1, ' ');
                        $templateProcessor->setValue('row_es_good#' . 1, ' ');
                        $templateProcessor->setValue('row_es_very_good#' . 1, ' ');
                        $templateProcessor->setValue('row_es_excellent#' . 1, ' ');

                    }catch (\Exception $e){  }
                }
                if (sizeof($item->persons_properties)!=0){
                    try{
                        $templateProcessor->cloneRow('row_pro_name', sizeof($item->persons_properties));
                        $z2=1;
                        foreach($item->persons_properties as $sub_record) {
                            $templateProcessor->setValue('row_pro_name#' . $z2, $sub_record->name);
                            $templateProcessor->setValue('row_pro_if_exist#' . $z2, $sub_record->has_property);
                            $templateProcessor->setValue('row_pro_exist#' . $z2, $sub_record->exist);
                            $templateProcessor->setValue('row_pro_not_exist#' . $z2, $sub_record->not_exist);
                            $templateProcessor->setValue('row_pro_count#' . $z2, $sub_record->quantity);
                            $z2++;
                        }
                    }catch (\Exception $e){  }
                }else{
                    try{
                        $templateProcessor->cloneRow('row_pro_name#', 1);
                        $templateProcessor->setValue('row_pro_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_pro_not_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_pro_name#' . 1, ' ');
                        $templateProcessor->setValue('row_pro_if_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_pro_count#' . 1, ' ');
                    }catch (\Exception $e){  }
                }
                if (sizeof($item->financial_aid_source)!=0){
                    try{
                        $templateProcessor->cloneRow('row_fin_name', sizeof($item->financial_aid_source));
                        $z3=1;
                        foreach($item->financial_aid_source as $record) {
                            $templateProcessor->setValue('row_fin_name#' . $z3, $record->name);
                            $templateProcessor->setValue('row_fin_if_exist#' . $z3, $record->aid_take);
                            $templateProcessor->setValue('row_fin_take#' . $z3, $record->take);
                            $templateProcessor->setValue('row_fin_not_take#' . $z3, $record->not_take);
                            $templateProcessor->setValue('row_fin_count#' . $z3, $record->aid_value);
                            $templateProcessor->setValue('row_fin_currency#' . $z3, $record->currency_name);
                            $z3++;
                        }
                    }catch (\Exception $e){  }
                }else{
                    try{
                        $templateProcessor->cloneRow('row_fin_name', 1);
                        $templateProcessor->setValue('row_fin_name#' . 1, ' ');
                        $templateProcessor->setValue('row_fin_if_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_fin_count#' . 1, ' ');
                        $templateProcessor->setValue('row_fin_take#' . 1, ' ');
                        $templateProcessor->setValue('row_fin_not_take#' . 1, ' ');
                        $templateProcessor->setValue('row_fin_currency#' . 1, ' ');
                    }catch (\Exception $e){  }
                }
                if (sizeof($item->non_financial_aid_source)!=0){
                    try{
                        $templateProcessor->cloneRow('row_nonfin_name', sizeof($item->non_financial_aid_source));
                        $z3=1;
                        foreach($item->non_financial_aid_source as $record) {
                            $templateProcessor->setValue('row_nonfin_name#' . $z3, $record->name);
                            $templateProcessor->setValue('row_nonfin_if_exist#' . $z3, $record->aid_take);
                            $templateProcessor->setValue('row_nonfin_not_take#' . $z3, $record->not_take);
                            $templateProcessor->setValue('row_nonfin_take#' . $z3, $record->take);
                            $templateProcessor->setValue('row_nonfin_count#' . $z3, $record->aid_value);
                            $z3++;
                        }
                    }catch (\Exception $e){  }
                } else{
                    try{
                        $templateProcessor->cloneRow('row_nonfin_name', 1);
                        $templateProcessor->setValue('row_nonfin_name#' . 1, ' ');
                        $templateProcessor->setValue('row_nonfin_if_exist#' . 1, ' ');
                        $templateProcessor->setValue('row_nonfin_take#' . 1, ' ');
                        $templateProcessor->setValue('row_nonfin_not_take#' . 1, ' ');
                        $templateProcessor->setValue('row_nonfin_count#' . 1, ' ');
                    }catch (\Exception $e){  }
                }
                if (sizeof($item->persons_documents)!=0){
                    try{
                        $templateProcessor->cloneRow('row_doc_name', sizeof($item->persons_documents));
                        $r2=1;
                        foreach($item->persons_documents as $record) {
                            $templateProcessor->setValue('row_doc_name#' . $r2, $record->name);
                            $templateProcessor->setImg('row_doc_img#'. $r2, array('src'=> base_path('storage/app/').$record->filepath,'swh'=>'650'));
                            $r2++;
                        }
                    }catch (\Exception $e){  }
                } else{
                    try{
                        $templateProcessor->cloneRow('row_doc_name', 1);
                        $templateProcessor->setValue('row_doc_name#' . 1, ' ');
                        $templateProcessor->setValue('row_doc_img#' . 1, ' ');
                    }catch (\Exception $e){  }
                }

                if (sizeof($item->family_member)!=0){
                    try{
                        $templateProcessor->cloneRow('fm_nam', sizeof($item->family_member));
                        $z=1;
                        foreach($item->family_member as $record) {
                            $templateProcessor->setValue('fm_inc#' . $z,$z);
                            $templateProcessor->setValue('fm_nam#' . $z, $record->name);
                            $templateProcessor->setValue('fm_bd#' . $z, $record->birthday);
                            $templateProcessor->setValue('fm_gen#' . $z, $record->gender);
                            $templateProcessor->setValue('fm_age#' . $z, $record->age);
                            $templateProcessor->setValue('fm_idc#' . $z, $record->id_card_number);
                            $templateProcessor->setValue('fm_ms#' . $z, $record->marital_status);
                            $templateProcessor->setValue('fm_kin#' . $z, $record->kinship_name);
                            $templateProcessor->setValue('fm_stg#' . $z, $record->stage);
                            $templateProcessor->setValue('fm_yer#' . $z, $record->year);
                            $templateProcessor->setValue('fm_hs#' . $z, $record->health_status);
                            $templateProcessor->setValue('fm_disnm#' . $z, $record->diseases_name);
                            $templateProcessor->setValue('fm_lvl#' . $z, $record->level);
                            $templateProcessor->setValue('fm_grd#' . $z, $record->grade);
                            $templateProcessor->setValue('fm_scl#' . $z, $record->school);
                            $templateProcessor->setValue('fm_working#' . $z, $record->working);
                            $templateProcessor->setValue('fm_job#' . $z, $record->work_job);
                            $z++;
                        }
                    }catch (\Exception $e){  }
                }
                else{

                    try{
                        $templateProcessor->cloneRow('fm_nam',1);
                        $templateProcessor->setValue('fm_inc#' . 1,1);
                        $templateProcessor->setValue('fm_scl#' . 1, ' ');
                        $templateProcessor->setValue('fm_nam#' . 1, ' ');
                        $templateProcessor->setValue('fm_bd#' . 1, ' ');
                        $templateProcessor->setValue('fm_gen#' . 1, ' ');
                        $templateProcessor->setValue('fm_age#' . 1, ' ');
                        $templateProcessor->setValue('fm_idc#' . 1,' ');
                        $templateProcessor->setValue('fm_ms#' . 1, ' ');
                        $templateProcessor->setValue('fm_kin#' . 1, ' ');
                        $templateProcessor->setValue('fm_stg#' . 1, ' ');
                        $templateProcessor->setValue('fm_lvl#' . 1, ' ');
                        $templateProcessor->setValue('fm_yer#' . 1, ' ');
                        $templateProcessor->setValue('fm_grd#' . 1, ' ');
                        $templateProcessor->setValue('fm_hs#' . 1, ' ');
                        $templateProcessor->setValue('fm_disnm#' . 1, ' ');
                        $templateProcessor->setValue('fm_working#' . 1, ' ');
                        $templateProcessor->setValue('fm_job#' . 1,' ');
                    }catch (\Exception $e){  }
                }
                if($this->type == 1){
                    if($item->father_id != null){
                        $templateProcessor->setValue('father_first_name', $item->father->first_name);
                        $templateProcessor->setValue('father_second_name', $item->father->second_name);
                        $templateProcessor->setValue('father_third_name', $item->father->third_name );
                        $templateProcessor->setValue('father_last_name', $item->father->last_name );
                        $templateProcessor->setValue('father_name', $item->father->full_name);
                        $templateProcessor->setValue('father_spouses', $item->father->spouses);
                        $templateProcessor->setValue('father_id_card_number', $item->father->id_card_number);
                        $templateProcessor->setValue('father_card_type', $item->father->card_type);
                        $templateProcessor->setValue('father_birthday', $item->father->birthday);
                        $templateProcessor->setValue('father_en_first_name', $item->father->en_first_name);
                        $templateProcessor->setValue('father_en_second_name', $item->father->en_second_name);
                        $templateProcessor->setValue('father_en_third_name', $item->father->en_third_name);
                        $templateProcessor->setValue('father_en_last_name', $item->father->en_last_name);
                        $templateProcessor->setValue('father_en_name', $item->father->en_name);
                        $templateProcessor->setValue('en_name', $item->en_name);
                        $templateProcessor->setValue('father_health_status', $item->father->health_status);
                        $templateProcessor->setValue('father_health_details', $item->father->health_details);
                        $templateProcessor->setValue('father_diseases_name', $item->father->diseases_name);
                        $templateProcessor->setValue('father_specialization', $item->father->specialization);
                        $templateProcessor->setValue('father_degree', $item->father->degree);
                        $templateProcessor->setValue('father_alive', $item->father->alive);
                        $templateProcessor->setValue('father_is_alive', $item->father->is_alive);
                        $templateProcessor->setValue('father_death_date', $item->father->death_date);
                        $templateProcessor->setValue('father_death_cause_name', $item->father->death_cause_name);
                        $templateProcessor->setValue('father_working', $item->father->working);
                        $templateProcessor->setValue('father_work_job', $item->father->work_job);
                        $templateProcessor->setValue('father_work_location', $item->father->work_location);
                        $templateProcessor->setValue('father_monthly_income', $item->father->monthly_income);
                        $templateProcessor->setValue('father_gender_male', $item->father->gender_male);
                        $templateProcessor->setValue('father_gender_female', $item->father->gender_female);
                        $templateProcessor->setValue('father_is_refugee', $item->father->is_refugee);
                        $templateProcessor->setValue('father_per_refugee', $item->father->per_refugee);
                        $templateProcessor->setValue('father_per_citizen', $item->father->per_citizen);
                        $templateProcessor->setValue('father_father_brothers', $item->father->father_brothers);
                        $templateProcessor->setValue('father_mother_brothers', $item->father->mother_brothers);
                        $templateProcessor->setValue('father_brothers', $item->father->brothers);
                        $templateProcessor->setValue('father_children', $item->father->children);
                        $templateProcessor->setValue('father_male_children', $item->father->male_children);
                        $templateProcessor->setValue('father_female_children', $item->father->female_children);
                        $templateProcessor->setValue('father_is_working', $item->father->is_working);
                        $templateProcessor->setValue('father_not_working', $item->father->not_working);
                        $templateProcessor->setValue('father_can_working', $item->father->can_working);
                        $templateProcessor->setValue('father_can_not_working', $item->father->can_not_working);
                        $templateProcessor->setValue('father_health_status', $item->father->health_status);
                        $templateProcessor->setValue('father_good_health_status', $item->father->good_health_status);
                        $templateProcessor->setValue('father_chronic_disease', $item->father->chronic_disease);
                        $templateProcessor->setValue('father_disabled', $item->father->disabled);
                    }
                    else{
                        $templateProcessor->setValue('father_gender_male', ' ');
                        $templateProcessor->setValue('father_gender_female', ' ');
                        $templateProcessor->setValue('father_is_refugee', ' ');
                        $templateProcessor->setValue('father_per_refugee', ' ');
                        $templateProcessor->setValue('father_per_citizen', ' ');
                        $templateProcessor->setValue('father_father_brothers', ' ');
                        $templateProcessor->setValue('father_mother_brothers', ' ');
                        $templateProcessor->setValue('father_brothers', ' ');
                        $templateProcessor->setValue('father_children', ' ');
                        $templateProcessor->setValue('father_male_children', ' ');
                        $templateProcessor->setValue('father_female_children', ' ');
                        $templateProcessor->setValue('father_is_working', ' ');
                        $templateProcessor->setValue('father_not_working', ' ');
                        $templateProcessor->setValue('father_can_working', ' ');
                        $templateProcessor->setValue('father_can_not_working', ' ');
                        $templateProcessor->setValue('father_health_status', ' ');
                        $templateProcessor->setValue('father_good_health_status', ' ');
                        $templateProcessor->setValue('father_chronic_disease', ' ');
                        $templateProcessor->setValue('father_disabled', ' ');
                        $templateProcessor->setValue('father_first_name', ' ');
                        $templateProcessor->setValue('father_second_name', ' ');
                        $templateProcessor->setValue('father_third_name', ' ');
                        $templateProcessor->setValue('father_last_name', ' ');
                        $templateProcessor->setValue('father_name', ' ');
                        $templateProcessor->setValue('father_spouses', ' ');
                        $templateProcessor->setValue('father_id_card_number',' ');
                        $templateProcessor->setValue('father_card_type',' ');
                        $templateProcessor->setValue('father_birthday', ' ');
                        $templateProcessor->setValue('father_en_first_name',' ');
                        $templateProcessor->setValue('father_en_second_name', ' ');
                        $templateProcessor->setValue('father_en_third_name', ' ');
                        $templateProcessor->setValue('father_en_last_name', ' ');
                        $templateProcessor->setValue('father_en_name',' ');
                        $templateProcessor->setValue('father_health_status', ' ');
                        $templateProcessor->setValue('father_health_details',' ');
                        $templateProcessor->setValue('father_diseases_name', ' ');
                        $templateProcessor->setValue('father_specialization', ' ');
                        $templateProcessor->setValue('father_degree', ' ');
                        $templateProcessor->setValue('father_alive', ' ');
                        $templateProcessor->setValue('father_is_alive', ' ');
                        $templateProcessor->setValue('father_death_date', ' ');
                        $templateProcessor->setValue('father_death_cause_name', ' ');
                        $templateProcessor->setValue('father_working', ' ');
                        $templateProcessor->setValue('father_work_job', ' ');
                        $templateProcessor->setValue('father_work_location', ' ');
                        $templateProcessor->setValue('father_monthly_income', ' ');
                    }
                    if($item->mother_id != null){
                        $templateProcessor->setValue('mother_prev_family_name', $item->mother->prev_family_name);
                        $templateProcessor->setValue('mother_first_name', $item->mother->first_name);
                        $templateProcessor->setValue('mother_second_name', $item->mother->second_name);
                        $templateProcessor->setValue('mother_third_name', $item->mother->third_name);
                        $templateProcessor->setValue('mother_last_name', $item->mother->last_name);
                        $templateProcessor->setValue('mother_name', $item->mother->full_name);
                        $templateProcessor->setValue('mother_id_card_number', $item->mother->id_card_number);
                        $templateProcessor->setValue('mother_card_type', $item->card_type);
                        $templateProcessor->setValue('mother_birthday', $item->mother->birthday);
                        $templateProcessor->setValue('mother_marital_status_name', $item->mother->marital_status_name);
                        $templateProcessor->setValue('mother_nationality', $item->mother->nationality);
                        $templateProcessor->setValue('mother_en_first_name', $item->mother->en_first_name);
                        $templateProcessor->setValue('mother_en_second_name', $item->mother->en_second_name);
                        $templateProcessor->setValue('mother_en_third_name', $item->mother->en_third_name);
                        $templateProcessor->setValue('mother_en_last_name', $item->mother->en_last_name);
                        $templateProcessor->setValue('mother_en_name', $item->mother->en_name);
                        $templateProcessor->setValue('mother_health_status', $item->mother->health_status);
                        $templateProcessor->setValue('mother_health_details', $item->mother->health_details);
                        $templateProcessor->setValue('mother_diseases_name', $item->mother->diseases_name);
                        $templateProcessor->setValue('mother_specialization', $item->mother->specialization);
                        $templateProcessor->setValue('mother_stage', $item->mother->stage);
                        $templateProcessor->setValue('mother_alive', $item->mother->alive);
                        $templateProcessor->setValue('mother_is_alive', $item->mother->is_alive);
                        $templateProcessor->setValue('mother_death_date', $item->mother->death_date);
                        $templateProcessor->setValue('mother_death_cause_name', $item->mother->death_cause_name);
                        $templateProcessor->setValue('mother_working', $item->mother->working);
                        $templateProcessor->setValue('mother_work_job', $item->mother->work_job);
                        $templateProcessor->setValue('mother_work_location', $item->mother->work_location);
                        $templateProcessor->setValue('mother_monthly_income', $item->mother->monthly_income);
                        $templateProcessor->setValue('mother_gender_male', $item->mother->gender_male);
                        $templateProcessor->setValue('mother_gender_female', $item->mother->gender_female);
                        $templateProcessor->setValue('mother_is_refugee', $item->mother->is_refugee);
                        $templateProcessor->setValue('mother_per_refugee', $item->mother->per_refugee);
                        $templateProcessor->setValue('mother_per_citizen', $item->mother->per_citizen);
                        $templateProcessor->setValue('mother_father_brothers', $item->mother->father_brothers);
                        $templateProcessor->setValue('mother_mother_brothers', $item->mother->mother_brothers);
                        $templateProcessor->setValue('mother_brothers', $item->mother->brothers);
                        $templateProcessor->setValue('mother_children', $item->mother->children);
                        $templateProcessor->setValue('mother_male_children', $item->mother->male_children);
                        $templateProcessor->setValue('mother_female_children', $item->mother->female_children);
                        $templateProcessor->setValue('mother_is_working', $item->mother->is_working);
                        $templateProcessor->setValue('mother_not_working', $item->mother->not_working);
                        $templateProcessor->setValue('mother_can_working', $item->mother->can_working);
                        $templateProcessor->setValue('mother_can_not_working', $item->mother->can_not_working);
                        $templateProcessor->setValue('mother_health_status', $item->mother->health_status);
                        $templateProcessor->setValue('mother_good_health_status', $item->mother->good_health_status);
                        $templateProcessor->setValue('mother_chronic_disease', $item->mother->chronic_disease);
                        $templateProcessor->setValue('mother_disabled', $item->mother->disabled);
                    }
                    else{
                        $templateProcessor->setValue('mother_gender_male', ' ');
                        $templateProcessor->setValue('mother_gender_female', ' ');
                        $templateProcessor->setValue('mother_is_refugee', ' ');
                        $templateProcessor->setValue('mother_per_refugee', ' ');
                        $templateProcessor->setValue('mother_per_citizen', ' ');
                        $templateProcessor->setValue('mother_father_brothers', ' ');
                        $templateProcessor->setValue('mother_mother_brothers', ' ');
                        $templateProcessor->setValue('mother_brothers', ' ');
                        $templateProcessor->setValue('mother_children', ' ');
                        $templateProcessor->setValue('mother_male_children', ' ');
                        $templateProcessor->setValue('mother_female_children', ' ');
                        $templateProcessor->setValue('mother_is_working', ' ');
                        $templateProcessor->setValue('mother_not_working', ' ');
                        $templateProcessor->setValue('mother_can_working', ' ');
                        $templateProcessor->setValue('mother_can_not_working', ' ');
                        $templateProcessor->setValue('mother_health_status', ' ');
                        $templateProcessor->setValue('mother_good_health_status', ' ');
                        $templateProcessor->setValue('mother_chronic_disease', ' ');
                        $templateProcessor->setValue('mother_disabled', ' ');
                        $templateProcessor->setValue('mother_prev_family_name',' ');
                        $templateProcessor->setValue('mother_first_name', ' ');
                        $templateProcessor->setValue('mother_second_name', ' ');
                        $templateProcessor->setValue('mother_third_name', ' ');
                        $templateProcessor->setValue('mother_last_name',' ');
                        $templateProcessor->setValue('mother_name', ' ');
                        $templateProcessor->setValue('mother_id_card_number', ' ');
                        $templateProcessor->setValue('mother_card_type', ' ');
                        $templateProcessor->setValue('mother_birthday', ' ');
                        $templateProcessor->setValue('mother_marital_status_name', ' ');
                        $templateProcessor->setValue('mother_nationality', ' ');
                        $templateProcessor->setValue('mother_en_first_name', ' ');
                        $templateProcessor->setValue('mother_en_second_name', ' ');
                        $templateProcessor->setValue('mother_en_third_name', ' ');
                        $templateProcessor->setValue('mother_en_last_name', ' ');
                        $templateProcessor->setValue('mother_en_name', ' ');
                        $templateProcessor->setValue('mother_health_status', ' ');
                        $templateProcessor->setValue('mother_health_details',' ');
                        $templateProcessor->setValue('mother_diseases_name', ' ');
                        $templateProcessor->setValue('mother_specialization',' ');
                        $templateProcessor->setValue('mother_stage', ' ');
                        $templateProcessor->setValue('mother_alive', ' ');
                        $templateProcessor->setValue('mother_is_alive', ' ');
                        $templateProcessor->setValue('mother_death_date', ' ');
                        $templateProcessor->setValue('mother_death_cause_name', ' ');
                        $templateProcessor->setValue('mother_working', ' ');
                        $templateProcessor->setValue('mother_work_job', ' ');
                        $templateProcessor->setValue('mother_work_location', ' ');
                        $templateProcessor->setValue('mother_monthly_income', ' ');
                    }
                    if($item->guardian_id != null){
                        $templateProcessor->setValue('guardian_first_name', $item->guardian->first_name);
                        $templateProcessor->setValue('guardian_second_name', $item->guardian->second_name);
                        $templateProcessor->setValue('guardian_third_name', $item->guardian->third_name );
                        $templateProcessor->setValue('guardian_age', $item->guardian->age );
                        $templateProcessor->setValue('guardian_last_name', $item->guardian->last_name );
                        $templateProcessor->setValue('guardian_name', $item->guardian->full_name);
                        $templateProcessor->setValue('guardian_id_card_number', $item->guardian->id_card_number);
                        $templateProcessor->setValue('guardian_card_type', $item->guardian->card_type);
                        $templateProcessor->setValue('guardian_birthday', $item->guardian->birthday);
                        $templateProcessor->setValue('guardian_marital_status_name', $item->guardian->marital_status_name);
                        $templateProcessor->setValue('guardian_birth_place', $item->guardian->birth_place);
                        $templateProcessor->setValue('guardian_nationality', $item->guardian->nationality);
                        $templateProcessor->setValue('guardian_country', $item->guardian->country);
                        $templateProcessor->setValue('guardian_governarate', $item->guardian->governarate);
                        $templateProcessor->setValue('guardian_city', $item->guardian->city);
                        $templateProcessor->setValue('guardian_nearLocation', $item->guardian->nearLocation);
                        $templateProcessor->setValue('guardian_mosque_name', $item->guardian->mosque_name);
                        $templateProcessor->setValue('guardian_street_address', $item->guardian->street_address);
                        $templateProcessor->setValue('guardian_address', $item->guardian->address);
                        $templateProcessor->setValue('guardian_phone', $item->guardian->phone);
                        $templateProcessor->setValue('guardian_primary_mobile', $item->guardian->primary_mobile);
                        $templateProcessor->setValue('guardian_wataniya', $item->guardian->wataniya);
                        $templateProcessor->setValue('guardian_secondery_mobile', $item->guardian->secondary_mobile );
                        $templateProcessor->setValue('guardian_en_first_name', $item->guardian->en_first_name);
                        $templateProcessor->setValue('guardian_en_second_name', $item->guardian->en_second_name);
                        $templateProcessor->setValue('guardian_en_third_name', $item->guardian->en_third_name);
                        $templateProcessor->setValue('guardian_en_last_name', $item->guardian->en_last_name);
                        $templateProcessor->setValue('guardian_en_name', $item->guardian->en_name);
                        $templateProcessor->setValue('guardian_property_types', $item->guardian->property_types);
                        $templateProcessor->setValue('guardian_roof_materials', $item->guardian->roof_materials);
                        $templateProcessor->setValue('guardian_area', $item->guardian->area);
                        $templateProcessor->setValue('guardian_rooms', $item->guardian->rooms);
                        $templateProcessor->setValue('guardian_residence_condition', $item->guardian->residence_condition);
                        $templateProcessor->setValue('guardian_indoor_condition', $item->guardian->indoor_condition);
                        $templateProcessor->setValue('guardian_stage', $item->guardian->stage);
                        $templateProcessor->setValue('guardian_working', $item->guardian->working);
                        $templateProcessor->setValue('guardian_work_job', $item->guardian->work_job);
                        $templateProcessor->setValue('guardian_work_location', $item->guardian->work_location);
                        $templateProcessor->setValue('guardian_monthly_income', $item->guardian->monthly_income);
                        $templateProcessor->setValue('guardian_gender_male', $item->guardian->gender_male);
                        $templateProcessor->setValue('guardian_gender_female', $item->guardian->gender_female);
                        $templateProcessor->setValue('guardian_is_refugee', $item->guardian->is_refugee);
                        $templateProcessor->setValue('guardian_per_refugee', $item->guardian->per_refugee);
                        $templateProcessor->setValue('guardian_per_citizen', $item->guardian->per_citizen);
                        $templateProcessor->setValue('guardian_father_brothers', $item->guardian->father_brothers);
                        $templateProcessor->setValue('guardian_mother_brothers', $item->guardian->mother_brothers);
                        $templateProcessor->setValue('guardian_brothers', $item->guardian->brothers);
                        $templateProcessor->setValue('guardian_children', $item->guardian->children);
                        $templateProcessor->setValue('guardian_male_children', $item->guardian->male_children);
                        $templateProcessor->setValue('guardian_female_children', $item->guardian->female_children);
                        $templateProcessor->setValue('guardian_is_working', $item->guardian->is_working);
                        $templateProcessor->setValue('guardian_not_working', $item->guardian->not_working);
                        $templateProcessor->setValue('guardian_can_working', $item->guardian->can_working);
                        $templateProcessor->setValue('guardian_can_not_working', $item->guardian->can_not_working);
                        $templateProcessor->setValue('guardian_health_status', $item->guardian->health_status);
                        $templateProcessor->setValue('guardian_good_health_status', $item->guardian->good_health_status);
                        $templateProcessor->setValue('guardian_chronic_disease', $item->guardian->chronic_disease);
                        $templateProcessor->setValue('guardian_disabled', $item->guardian->disabled);
                    }
                    else{
                        $templateProcessor->setValue('guardian_gender_male', ' ');
                        $templateProcessor->setValue('guardian_gender_female', ' ');
                        $templateProcessor->setValue('guardian_is_refugee', ' ');
                        $templateProcessor->setValue('guardian_per_refugee', ' ');
                        $templateProcessor->setValue('guardian_per_citizen', ' ');
                        $templateProcessor->setValue('guardian_father_brothers', ' ');
                        $templateProcessor->setValue('guardian_mother_brothers', ' ');
                        $templateProcessor->setValue('guardian_brothers', ' ');
                        $templateProcessor->setValue('guardian_children', ' ');
                        $templateProcessor->setValue('guardian_male_children', ' ');
                        $templateProcessor->setValue('guardian_female_children', ' ');
                        $templateProcessor->setValue('guardian_is_working', ' ');
                        $templateProcessor->setValue('guardian_not_working', ' ');
                        $templateProcessor->setValue('guardian_can_working', ' ');
                        $templateProcessor->setValue('guardian_can_not_working', ' ');
                        $templateProcessor->setValue('guardian_health_status', ' ');
                        $templateProcessor->setValue('guardian_good_health_status', ' ');
                        $templateProcessor->setValue('guardian_chronic_disease', ' ');
                        $templateProcessor->setValue('guardian_disabled', ' ');
                        $templateProcessor->setValue('guardian_first_name',' ');
                        $templateProcessor->setValue('guardian_age',' ');
                        $templateProcessor->setValue('guardian_second_name', ' ');
                        $templateProcessor->setValue('guardian_third_name', ' ');
                        $templateProcessor->setValue('guardian_last_name', ' ');
                        $templateProcessor->setValue('guardian_name', ' ');
                        $templateProcessor->setValue('guardian_id_card_number', ' ');
                        $templateProcessor->setValue('guardian_card_type', ' ');
                        $templateProcessor->setValue('guardian_birthday', ' ');
                        $templateProcessor->setValue('guardian_marital_status_name',' ');
                        $templateProcessor->setValue('guardian_birth_place', ' ');
                        $templateProcessor->setValue('guardian_nationality', ' ');
                        $templateProcessor->setValue('guardian_country', ' ');
                        $templateProcessor->setValue('guardian_governarate', ' ');
                        $templateProcessor->setValue('guardian_city', ' ');
                        $templateProcessor->setValue('guardian_nearLocation', ' ');
                        $templateProcessor->setValue('guardian_mosque_name', ' ');
                        $templateProcessor->setValue('guardian_street_address', ' ');
                        $templateProcessor->setValue('guardian_address', ' ');
                        $templateProcessor->setValue('guardian_phone', ' ');
                        $templateProcessor->setValue('guardian_primary_mobile', ' ');
                        $templateProcessor->setValue('guardian_wataniya', ' ');
                        $templateProcessor->setValue('guardian_secondery_mobile', ' ');
                        $templateProcessor->setValue('guardian_en_first_name', ' ');
                        $templateProcessor->setValue('guardian_en_second_name', ' ');
                        $templateProcessor->setValue('guardian_en_third_name', ' ');
                        $templateProcessor->setValue('guardian_en_last_name', ' ');
                        $templateProcessor->setValue('guardian_en_name', ' ');
                        $templateProcessor->setValue('guardian_property_types', ' ');
                        $templateProcessor->setValue('guardian_roof_materials', ' ');
                        $templateProcessor->setValue('guardian_area', ' ');
                        $templateProcessor->setValue('guardian_rooms', ' ');
                        $templateProcessor->setValue('guardian_residence_condition', ' ');
                        $templateProcessor->setValue('guardian_indoor_condition', ' ');
                        $templateProcessor->setValue('guardian_stage',' ');
                        $templateProcessor->setValue('guardian_working', ' ');
                        $templateProcessor->setValue('guardian_work_job', ' ');
                        $templateProcessor->setValue('guardian_work_location', ' ');
                        $templateProcessor->setValue('guardian_monthly_income', ' ');
                    }
                }

                $templateProcessor->saveAs($dirName.'/'.$item->category_name.' '.$item->id_card_number.' '.$item->full_name.'.docx');

            }

            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');

            $zip = new \ZipArchive;
            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                foreach ( glob( $dirName . '/*' ) as $fileName )
                {
                    $file = basename( $fileName );
                    $zip->addFile( $fileName, $file );
                }

                $zip->close();
                array_map('unlink', glob("$dirName/*.*"));
                rmdir($dirName);
            }

            if($this->type == 1){
                $message=trans('common::application.exported cases forms').trans('common::application.guarantees');
            }else{
                $message=trans('common::application.exported cases forms').trans('common::application.aids') ;
            }

            \Log\Model\Log::saveNewLog('CASE_EXPORTED',$message);

            return response()->json([
                'download_token' => $token]);




        }
        else if($request->get('action') =='pdf') {

            $dirName = base_path('storage/app/templates/CaseForm');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }
            foreach($result as $k=>$item){

                $date = date('Y-m-d');
                $organization_name = $item->organization_name;
                $category_name = $item->category_name;
                $first_name = $item->first_name;
                $second_name = $item->second_name;
                $third_name = $item->third_name ;
                $last_name = $item->last_name ;
                $name = $item->full_name;
                $en_first_name = $item->en_first_name;
                $en_second_name = $item->en_second_name;
                $en_third_name = $item->en_third_name;
                $en_last_name = $item->en_last_name;
                $en_name = $item->en_name;
                $id_card_number = $item->id_card_number;
                $birthday = $item->birthday;
                $age = $item->age;
                $gender = $item->gender;
                $gender_male = $item->gender_male;
                $gender_female = $item->gender_female;
                $deserted = $item->gender;
                $is_deserted = $item->is_deserted;
                $not_deserted = $item->not_deserted;
                $birth_place = $item->birth_place;
                $nationality = $item->nationality;
                $refugee = $item->refugee;

                $is_refugee = $item->is_refugee;
                $per_refugee = $item->per_refugee;
                $per_citizen = $item->per_citizen;
                $unrwa_card_number = $item->unrwa_card_number;
                $marital_status_name = $item->marital_status_name;
                $monthly_income = $item->monthly_income;
                $address = $item->address;
                $country = $item->country;
                $governarate = $item->governarate;
                $city = $item->city;
                $nearLocation = $item->nearLocation;
                $mosque_name = $item->mosque_name;
                $street_address = $item->street_address;
                $individual_count = $item->individual_count;
                $individual_count = $item->family_count;
                $male_count = $item->male_count;
                $female_count = $item->female_count;
                $father_brothers = $item->father_brothers;
                $mother_brothers = $item->mother_brothers;
                $brothers = $item->brothers;
                $children = $item->children;
                $male_children = $item->male_children;
                $female_children = $item->female_children;
                $phone = $item->phone;
                $primary_mobile = $item->primary_mobile;
                $wataniya = $item->wataniya;
                $secondary_mobile = $item->secondary_mobile ;
                $property_types = $item->property_types;
                $rent_value = $item->rent_value;
                $roof_materials = $item->roof_materials;
                $habitable = $item->habitable;
                $house_condition = $item->house_condition;
                $residence_condition = $item->residence_condition;
                $indoor_condition = $item->indoor_condition;
                $rooms = $item->rooms;
                $area = $item->area;
                $working = $item->working;
                $works = $item->works;
                $is_working = $item->is_working;
                $not_working = $item->not_working;
                $can_work = $item->can_work;
                $can_works = $item->can_works;
                $can_working = $item->can_working;
                $can_not_working = $item->can_not_working;
                $work_job = $item->work_job;
                $work_reason = $item->work_reason;
                $work_status = $item->work_status;
                $work_wage = $item->work_wage;
                $work_location = $item->work_location;
                $needs = $item->needs;
                $promised = $item->promised;
                $if_promised = $item->if_promised;
                $was_promised = $item->was_promised;
                $not_promised = $item->not_promised;
                $reconstructions_organization_name = $item->reconstructions_organization_name;
                $visitor = $item->visitor;
                $notes = $item->notes;
                $visited_at = $item->visited_at;
                $health_status = $item->health_status;
                $good_health_status = $item->good_health_status;
                $chronic_disease = $item->chronic_disease;
                $disabled = $item->disabled;
                $health_details = $item->health_details;
                $diseases_name = $item->diseases_name;
                $study = $item->study;
                $type = $item->type;
                $level = $item->level;
                $year = $item->year;
                $points = $item->points;
                $school = $item->school;
                $grade = $item->grade;
                $authority = $item->authority;
                $stage = $item->stage;
                $degree = $item->degree;
                $specialization = $item->specialization;
                $prayer = $item->prayer;
                $prayer_reason = $item->prayer_reason;
                $quran_reason = $item->quran_reason;
                $quran_parts = $item->quran_parts;
                $quran_chapters = $item->quran_chapters;
                $quran_center = $item->quran_center;
                $save_quran = $item->save_quran;
                $visitor_note = $item->visitor_note;
                $guardian_kinship = $item->guardian_kinship;
                $guardian_is = $item->guardian_is;
                $mother_is_guardian = $item->mother_is_guardian;
                $deserted = $item->deserted;
                $family_cnt = $item->family_cnt;
                $spouses = $item->spouses;
                $male_live = $item->male_live;
                $female_live = $item->female_live;

                $personalImagePath=PersonDocument::personalImagePath($item->person_id,$this->type);
                if(!is_null($personalImagePath)){
                    $img = $personalImagePath;
                }else{
                    $img = base_path('storage/app/emptyUser1.png');
                }


                $html='';
                PDF::reset();
                PDF::SetFont('aealarabiya', '', 18);
                PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
                PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);

                PDF::SetMargins(PDF_MARGIN_LEFT, 20, PDF_MARGIN_RIGHT);
//        PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
//        PDF::SetFooterMargin(PDF_MARGIN_FOOTER);
                PDF::SetAutoPageBreak(TRUE);
                $lg = Array();
                $lg['a_meta_charset'] = 'UTF-8';
                $lg['a_meta_dir'] = 'rtl';
                $lg['a_meta_language'] = 'fa';
                $lg['w_page'] = 'page';
                PDF::setLanguageArray($lg);
                PDF::setCellHeightRatio(1.5);

                if($item->category_type  == 2) {

                    $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                    $html.= '<h2 style="text-align:center;"> '.$category_name.'</h2>';
                    $html .= '<table border="1" style="border: 1px solid black; box-sizing:border-box;">';

                    $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.full_name').' </b></td>
                       <td width="325" align="center" >'.$name.'</td>
                        <td width="88" align="center" rowspan="6" style="background-color: white" >'
                        .'<img src="'.$img.'" alt="" height="140" />'.
                        '</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.id_card').'  </b></td>
                       <td width="120" align="center">'.$id_card_number.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.nationality').'  </b></td>
                       <td width="105" align="center">'.$nationality.'</td>
                      
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.BIRTH_DT').' </b></td>
                       <td width="120" align="center" >'.$birthday.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.birth_place').'</b></td>
                       <td width="105" align="center">'.$birth_place.'</td>
                   </tr>';

                    $html .= ' <tr>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b>  '.trans('common::application.SOCIAL_STATUS').'  </b></td>
                   <td width="70" align="center">'.$marital_status_name.'</td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b>  '.trans('common::application.gender').' </b></td>
                   <td width="70" align="center">'.$gender.'</td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b>  '.trans('aid::application.deserted').'   </b></td>
                   <td width="80" align="center">'.$deserted.'</td>
               </tr>';

                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.citizen').'  / '.trans('common::application.refugee').'</b></td>
                       <td width="120" align="center">'.$refugee.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.unrwa_card_number').'</b></td>
                       <td width="105" align="center">'.$unrwa_card_number.'</td>
                   </tr>';
                    $html .='</table>';
                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';

                    $is_qualified = $item->is_qualified;
                    $qualified_card_number = $item->qualified_card_number;
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.phone').'</b></td>
                       <td width="71" align="center">'.$family_cnt.'</td>
                        <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.is_qualified').'</b></td>
                       <td width="71" align="center">'.$is_qualified.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.qualified_card_number').'</b></td>
                       <td width="71" align="center">'.$qualified_card_number.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.spouses').'</b></td>
                       <td width="71" align="center">'.$spouses.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.male_live').' </b></td>
                       <td width="71" align="center">'.$male_live.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.female_live').'</b></td>
                       <td width="71" align="center">'.$female_live.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="95" align="center"><b>  '.trans('common::application.district').' </b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="105" align="center"><b>  '.trans('common::application.region_').' </b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="105" align="center"><b>   '.trans('common::application.nearlocation_').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="105" align="center"><b>  '.trans('common::application.square').' </b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="103" align="center"><b>  '.trans('common::application.mosques').' </b></td>
                   </tr>';
                    $district = $item->governarate;
                    $region_name = $item->region_name;
                    $nearLocation = $item->nearLocation;
                    $square_name = $item->square_name;
                    $mosque_name = $item->mosque_name;

                    $html .= ' <tr>
                       <td width="95" align="center">'.$district.'</td>
                       <td width="105" align="center">'.$region_name.'</td>
                       <td width="105" align="center">'.$nearLocation.'</td>
                       <td width="105" align="center">'.$square_name.'</td>
                       <td width="103" align="center">'.$mosque_name.'</td>
                   </tr>';


                    $html .='</table>';
                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                        '.trans('common::application.contact information').'
                        </b></td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.mobile').'</b></td>
                       <td width="156" align="center">'.$primary_mobile.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.secondery_mobile').'</b></td>
                       <td width="157" align="center">'.$secondary_mobile.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.phone').'</b></td>
                       <td width="156" align="center">'.$phone.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.wataniya').'</b></td>
                       <td width="157" align="center">'.$wataniya.'</td>
                   </tr>';
                    $html .='</table>';


                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                        '.trans('common::application.commercial_records_info').'
                        </b></td>
                   </tr>';
                    $has_commercial_records = $item->has_commercial_records;
                    $active_commercial_records = $item->active_commercial_records;
                    $gov_commercial_records_details = $item->gov_commercial_records_details;
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.has_commercial_records').'</b></td>
                       <td width="156" align="center">'.$has_commercial_records.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.active_commercial_records').'</b></td>
                       <td width="157" align="center">'.$active_commercial_records.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.gov_commercial_records_details').'</b></td>
                       <td width="412" align="center">'.$gov_commercial_records_details.'</td>
                   </tr>';
                    $html .='</table>';


                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                       '.trans('common::application.works info').' 
                        </b></td>
                   </tr>';

                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.is_she_working?').'</b></td>
                       <td width="156" align="center">'.$working.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_job_').' </b></td>
                       <td width="157" align="center">'.$work_job.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.is_can_work').'</b></td>
                       <td width="156" align="center">'.$can_work.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_reason').' </b></td>
                       <td width="157" align="center">'.$work_reason.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_location').' </b></td>
                       <td width="156" align="center">'.$work_location.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.Work_Status').' </b></td>
                       <td width="157" align="center">'.$work_status.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.Wage category').':  </b></td>
                       <td width="156" align="center">'.$work_wage.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.monthly_income').'</b></td>
                       <td width="157" align="center">'. $monthly_income .'</td>
                   </tr>';

                    $actual_monthly_income = $item->actual_monthly_income;
                    $receivables = $item->receivables;
                    $receivables_sk_amount = $item->receivables_sk_amount;
                    $has_other_work_resources = $item->has_other_work_resources;

                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.actual_monthly_income').'</b></td>
                       <td width="156" align="center">'.$actual_monthly_income.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.has_other_work_resources').' </b></td>
                       <td width="157" align="center">'.$has_other_work_resources.'</td>
                       
                   </tr>';
                    $html .= ' <tr>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.receivables').' </b></td>
                           <td width="156" align="center">'.$receivables.'</td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.receivables_sk_amount').'</b></td>
                           <td width="157" align="center">'.$receivables_sk_amount.'</td>
                           
                       </tr>';

                    $html .='</table>';


                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1">';
                    $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                       '.trans('common::application.home info').'
                        </b></td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.property_type').'</b></td>
                       <td width="156" align="center">'.$property_types.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.property_type').'('.trans('common::application.for renter').') </b></td>
                       <td width="157" align="center">'.$rent_value.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.roof_material').'</b></td>
                       <td width="156" align="center">'.$roof_materials.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.residence_condition').'</b></td>
                       <td width="157" align="center">'.$house_condition.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.indoor_condition').'</b></td>
                       <td width="156" align="center">'.$residence_condition.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.habitable').'</b></td>
                       <td width="157" align="center">'.$habitable.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.home_area').'</b></td>
                       <td width="156" align="center">'.$area.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.rooms').'</b></td>
                       <td width="157" align="center">'.$rooms.'</td>
                   </tr>';
                    $need_repair = $item->need_repair;
                    $repair_notes = $item->repair_notes;
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="200" align="center"><b>'.
                        trans('common::application.need_repair').'</b></td>
                       <td width="312" align="center">'.$need_repair.'</td>
                   </tr>';


                    $html .='</table>';



                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


                    $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                      '.trans('common::application.health info').'
                        </b></td>
                   </tr>';

                    $has_health_insurance = $item->has_health_insurance;
                    $health_insurance_number = $item->health_insurance_number;
                    $health_insurance_type = $item->health_insurance_type;

                    $has_device = $item->has_device;
                    $used_device_name = $item->used_device_name;

                    $gov_health_details = $item->gov_health_details;

                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.has_health_insurance').' </b></td>
                       <td width="156" align="center">'.$has_health_insurance.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.health_insurance_number').'</b></td>
                       <td width="157" align="center">'.$health_insurance_number.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.health_insurance_type').' </b></td>
                       <td width="156" align="center">'.$health_insurance_type.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.has_device').'</b></td>
                       <td width="157" align="center">'.$has_device.'</td>
                   </tr>';

                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.used_device_name').'</b></td>
                       <td width="413" align="center">'.$used_device_name.'</td>
                   </tr>';

                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.disease_/_disability_details').'</b></td>
                       <td width="413" align="center">'.$health_details.'</td>
                   </tr>';

                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.gov_health_details').'</b></td>
                       <td width="413" align="center">'.$gov_health_details.'</td>
                   </tr>';
                    $html .='</table>';

                    $html .= '<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="300" align="center"><b> '.trans('common::application.reconstructions_promised').'? ('.trans('common::application.yes').' /'.trans('common::application.no').') </b></td>
                       <td width="213" align="center">' . $promised . '</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="300" align="center"><b> '.trans('common::application.ReconstructionOrg').'('.trans('common::application.if the answer to the previous question is yes').'):  </b></td>
                       <td width="213" align="center">' . $reconstructions_organization_name . '</td>
                   </tr>';
                    $html .='</table>';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="height: 50px ; padding: 55px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b> 
                       '.trans('common::application.case_needs').'
                       </b></td>
                       <td width="415" align="center">' . $needs . '</td>
                   </tr>';

                    $html .= ' <tr>
                       <td style="height: 50px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b>  '.trans("common::application.repair_notes").'</b></td>
                       <td width="415" align="center">' . $repair_notes . '</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 50px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b>  '.trans("common::application.social researcher's recommendations").'</b></td>
                       <td width="415" align="center">' . $notes . '</td>
                   </tr>';

                    $visitor_opinion = $item->visitor_opinion;
                    $html .= ' <tr>
                       <td style="height: 50px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b>  '.trans("common::application.visitor_opinion").'</b></td>
                       <td width="415" align="center">' . $visitor_opinion . '</td>
                   </tr>';
                    $html .='</table>';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';

                    $visitor_card = $item->visitor_card;
                    $visitor_evaluation = $item->visitor_evaluation_;
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.visitor_card').'</b></td>
                       <td width="156" align="center">' . $visitor_card . '</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.visitor').' </b></td>
                       <td width="157" align="center">' . $visitor . '</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.visited_at').'</b></td>
                       <td width="156" align="center">' . $visited_at . '</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.visitor_evaluation').' </b></td>
                       <td width="157" align="center">' . $visitor_evaluation . '</td>
                   </tr>';



                    $html .='</table>';

                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

                    $setting_id = 'aid_custom_form';
                    $CaseCustomData =FormsCasesData::getCaseCustomData($item->case_id,$item->category_id,$item->organization_id,$setting_id);
                    if (sizeof($CaseCustomData)!=0){

                        $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                        $html.= '<h2 style="text-align:center;">  '.trans('common::application.Custom Form Information').'</h2>';
                        $html .= '<table border="1" style="">';
                        $html .= ' <tr>
                               <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.organization_data').'  </b></td>
                               <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="400" align="center"><b> '.trans('common::application.explanation').'   </b></td>
                           </tr>';
                        $z=1;
                        foreach($CaseCustomData as $record) {
                            $c_label = $record['label'];
                            $c_value = $record['value'];

                            if($record['type'] != 3){
                                $html .= ' <tr>
                                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="120" align="center"><b> '.$c_label.'  </b></td>
                                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="400" align="center"><b> '.$c_value.'   </b></td>
                                   </tr>';
                            }else{
                                $html .= ' <tr>
                       <td style="height: 50px ; line-height: 35px; padding: 25px 0 12px 0;" width="120" align="center"><b> '.$c_label.'  </b></td>
                       <td style="height: 50px ; line-height: 35px; padding: 15px 0 12px 0;" width="400" align="center"><b> '.$c_value.'   </b></td>
                   </tr>';

                            }
                            $z++;
                        }
                        $html .='</table>';
                        $html .='</body></html>';

                        PDF::SetMargins(PDF_MARGIN_LEFT, 2, PDF_MARGIN_RIGHT);
                        PDF::AddPage('P','A4');
                        PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                    }


                    if($request->get('parent_detail')){

                        $get=array('action' => 'show','id' => $item->father_id,
                            'person'  => true, 'persons_i18n'  =>  true,
                            'health'  =>  true,  'education'  =>  true, 'work'  =>  true);

                        $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';

                        if($item->father_id != null){
                            $get['id']=$item->father_id;
                            $father =Person::fetch($get);
                            $father_first_name= $father->first_name;
                            $father_second_name= $father->second_name;
                            $father_third_name= $father->third_name ;
                            $father_last_name= $father->last_name ;
                            $father_name= $father->full_name;
                            $father_spouses= $father->spouses;
                            $father_id_card_number= $father->id_card_number;
                            $father_birthday= $father->birthday;
                            $father_en_first_name= $father->en_first_name;
                            $father_en_second_name= $father->en_second_name;
                            $father_en_third_name= $father->en_third_name;
                            $father_en_last_name= $father->en_last_name;
                            $father_en_name= $father->en_name;
                            $father_health_status= $father->health_status;
                            $father_health_details= $father->health_details;
                            $father_diseases_name= $father->diseases_name;
                            $father_specialization= $father->specialization;
                            $father_degree= $father->degree;
                            $father_alive= $father->alive;
                            $father_is_alive= $father->is_alive;
                            $father_death_date= $father->death_date;
                            $father_death_cause_name= $father->death_cause_name;
                            $father_working= $father->working;
                            $father_work_job= $father->work_job;
                            $father_work_location= $father->work_location;
                            $father_monthly_income= $father->monthly_income;
                            $father_gender_male= $father->gender_male;
                            $father_gender_female= $father->gender_female;
                            $father_is_refugee= $father->is_refugee;
                            $father_per_refugee= $father->per_refugee;
                            $father_per_citizen= $father->per_citizen;
                            $father_father_brothers= $father->father_brothers;
                            $father_mother_brothers= $father->mother_brothers;
                            $father_brothers= $father->brothers;
                            $father_children= $father->children;
                            $father_male_children= $father->male_children;
                            $father_female_children= $father->female_children;
                            $father_is_working= $father->is_working;
                            $father_not_working= $father->not_working;
                            $father_can_working= $father->can_working;
                            $father_can_not_working= $father->can_not_working;
                            $father_health_status= $father->health_status;
                            $father_good_health_status= $father->good_health_status;
                            $father_chronic_disease= $father->chronic_disease;
                            $father_disabled= $father->disabled;


                            $html .='<div style="height: 5px; color: white"> - </div>';
                            $html .= '<table border="1" style="">';
                            $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                    '.trans('common::application.father-info').' 
                        </b></td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.full_name').'</b></td>
                       <td width="413" align="center">'.$father_name.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.id_card').'</b></td>
                       <td width="156" align="center">'.$father_id_card_number.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.BIRTH_DT').'</b></td>
                       <td width="156" align="center">'.$father_birthday.'</td>
                       </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.is_she_alive?').'</b></td>
                       <td width="156" align="center">'.$father_alive.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.death_date').'</b></td>
                       <td width="157" align="center">'.$father_death_date.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.death_cause_id').' </b></td>
                       <td width="413" align="center">'.$father_death_cause_name.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_number_of_spouses').'</b></td>
                       <td width="156" align="center">'.$father_spouses.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.monthly_income').' </b></td>
                       <td width="157" align="center">'.$grade.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.qualification').'</b></td>
                       <td width="156" align="center">'.$father_degree.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.Specialization').' </b></td>
                       <td width="157" align="center">'.$father_specialization.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.is_he_working').'</b></td>
                       <td width="156" align="center">'.$father_working.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_job_').'</b></td>
                       <td width="157" align="center">'.$father_work_job.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_location').' </b></td>
                       <td width="413" align="center">'.$father_work_location.'</td>
                   </tr>';

                            if($item->father_dead == 0){
                                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.health_condition').' </b></td>
                       <td width="156" align="center">'.$father_health_status.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.disease_id').'</b></td>
                       <td width="157" align="center">'.$father_diseases_name.'</td>
                   </tr>';
                                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.disease_/_disability_details').'</b></td>
                       <td width="413" align="center">'.$father_health_details.'</td>
                   </tr>';
                            }
                            $html .='</table>';
                            $html .='<div style="height: 40px; color: white"> - </div>';
                        }

                        if($item->mother_id != null){
                            $get['id']=$item->mother_id;
                            $mother =Person::fetch($get);
                            $mother_prev_family_name= $mother->prev_family_name;
                            $mother_first_name= $mother->first_name;
                            $mother_second_name= $mother->second_name;
                            $mother_third_name= $mother->third_name;
                            $mother_last_name= $mother->last_name;
                            $mother_name= $mother->full_name;
                            $mother_id_card_number= $mother->id_card_number;
                            $mother_birthday= $mother->birthday;
                            $mother_marital_status_name= $mother->marital_status_name;
                            $mother_nationality= $mother->nationality;
                            $mother_en_first_name= $mother->en_first_name;
                            $mother_en_second_name= $mother->en_second_name;
                            $mother_en_third_name= $mother->en_third_name;
                            $mother_en_last_name= $mother->en_last_name;
                            $mother_en_name= $mother->en_name;
                            $mother_health_status= $mother->health_status;
                            $mother_health_details= $mother->health_details;
                            $mother_diseases_name= $mother->diseases_name;
                            $mother_specialization= $mother->specialization;
                            $mother_stage= $mother->stage;
                            $mother_alive= $mother->alive;
                            $mother_is_alive= $mother->is_alive;
                            $mother_death_date= $mother->death_date;
                            $mother_death_cause_name= $mother->death_cause_name;
                            $mother_working= $mother->working;
                            $mother_work_job= $mother->work_job;
                            $mother_work_location= $mother->work_location;
                            $mother_monthly_income= $mother->monthly_income;
                            $mother_gender_male= $mother->gender_male;
                            $mother_gender_female= $mother->gender_female;
                            $mother_is_refugee= $mother->is_refugee;
                            $mother_per_refugee= $mother->per_refugee;
                            $mother_per_citizen= $mother->per_citizen;
                            $mother_father_brothers= $mother->father_brothers;
                            $mother_mother_brothers= $mother->mother_brothers;
                            $mother_brothers= $mother->brothers;
                            $mother_children= $mother->children;
                            $mother_male_children= $mother->male_children;
                            $mother_female_children= $mother->female_children;
                            $mother_is_working= $mother->is_working;
                            $mother_not_working= $mother->not_working;
                            $mother_can_working= $mother->can_working;
                            $mother_can_not_working= $mother->can_not_working;
                            $mother_health_status= $mother->health_status;
                            $mother_good_health_status= $mother->good_health_status;
                            $mother_diseases_name= $mother->diseases_name;
                            $mother_chronic_disease= $mother->chronic_disease;
                            $mother_disabled= $mother->disabled;


                            $html .= '<table border="1" style="">';
                            $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                    '.trans('common::application.mother-info').'  
                        </b></td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.full_name').'</b></td>
                       <td width="413" align="center">'.$mother_name.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.id_card').'</b></td>
                       <td width="156" align="center">'.$mother_id_card_number.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.BIRTH_DT').'</b></td>
                       <td width="156" align="center">'.$mother_birthday.'</td>
                       </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.prev_family_name').'</b></td>
                       <td width="156" align="center">'.$mother_prev_family_name.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.SOCIAL_STATUS').'</b></td>
                       <td width="157" align="center">'.$mother_marital_status_name.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.is_she_alive?').'</b></td>
                       <td width="156" align="center">'.$mother_is_alive.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.death_date').'</b></td>
                       <td width="157" align="center">'.$mother_death_date.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.death_cause_id').' </b></td>
                       <td width="413" align="center">'.$mother_death_cause_name.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_educational_level').' </b></td>
                       <td width="156" align="center">'.$mother_stage.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.Specialization').' </b></td>
                       <td width="157" align="center">'.$mother_specialization.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.is_he_working').'</b></td>
                       <td width="156" align="center">'.$mother_working.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_job_').'</b></td>
                       <td width="157" align="center">'.$mother_work_job.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_location').' </b></td>
                       <td width="156" align="center">'.$mother_work_location.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.monthly_income').'</b></td>
                       <td width="157" align="center">'.$mother_monthly_income.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.health_condition').' </b></td>
                       <td width="156" align="center">'.$mother_health_status.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.disease_id').'</b></td>
                       <td width="157" align="center">'.$mother_diseases_name.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.disease_/_disability_details').'</b></td>
                       <td width="413" align="center">'.$mother_health_details.'</td>
                   </tr>';
                            $html .='</table>';
                        }



                        $html .= '</body></html>';
                        PDF::AddPage('P', 'A4');
                        PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

                    }

                    if (sizeof($item->family_member)!=0){

                        $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                        $html.= '<h2 style="text-align:center;"> '.trans('common::application.family-data').'

    ('. sizeof($item->family_member) .')</h2>';
                        $html .= '<table border="1" style="">';
                        $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.name').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.id_card').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.BIRTH_DT').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="50" align="center"><b> '.trans('common::application.gender').'</b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.SOCIAL_STATUS').'</b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.health_condition').' </b></td>
                   </tr>';
                        $z=1;
                        foreach($item->family_member as $record) {
                            $fm_nam = $record->name;
                            $fm_idc = $record->id_card_number;
                            $fm_bd = $record->birthday;
                            $fm_gen = $record->gender;
                            $fm_age = $record->age;
                            $fm_ms = $record->marital_status;
                            $fm_kin = $record->kinship_name;
                            $fm_stg = $record->stage;
                            $fm_lvl = $record->level;
                            $fm_yer = $record->year;
                            $fm_grd = $record->grade;
                            $fm_scl = $record->school;
                            $fm_hs = $record->health_status;
                            $fm_disnm = $record->diseases_name;
                            $fm_working = $record->working;
                            $fm_job = $record->work_job;
                            $html .= ' <tr>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="120" align="center"><b> '.$fm_nam.'  </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_idc.' </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_bd.'   </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="50" align="center"><b>'.$fm_gen.'</b></td> 
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_ms.' </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$fm_hs.'   </b></td>
                   </tr>';
                            $z++;
//                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="50" align="center"><b>'.$fm_kin.'</b></td>
//                                    <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_working.'   </b></td>
//                <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$fm_job.'   </b></td>
                        }
                        $html .='</table>';
                        $html .='</body></html>';

                        PDF::SetMargins(PDF_MARGIN_LEFT, 2, PDF_MARGIN_RIGHT);
                        PDF::AddPage('P','A4');
                        PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                    }

                    if (sizeof($item->home_indoor)!=0){
                        $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                        $html.= '<h2 style="text-align:center;"> '.trans('common::application.furniture-info').'</h2>';
                        $html .= '<table border="1" style="">';
                        $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="200" align="center"><b> '.trans('common::application.content').'  </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.exist').'/'.trans('common::application.not exist').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_status').'   </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.needs').'</b></td>
                   </tr>';

                        foreach($item->home_indoor as $record) {

                            $row_es_name = $record->name;
                            $row_es_if_exist= $record->exist;
                            $row_es_needs=is_null($record->needs) ? ' ' : $record->needs;
                            $row_es_condition=is_null($record->essentials_condition) ? ' ' : $record->essentials_condition;

                            $html .= ' <tr>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="200" align="center"><b> '.$row_es_name.'  </b></td>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$row_es_if_exist.' </b></td>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$row_es_condition.'   </b></td>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b>'.$row_es_needs .'</b></td>
                   </tr>';
                        }
                        $html .='</table>';
                        $html .='</body></html>';

                        PDF::AddPage('P','A4');
                        PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                    }
                    if (sizeof($item->financial_aid_source)!=0){

                        $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                        $html.= '<h2 style="text-align:center;"> '.trans('common::application.concrete foreign aid data').'</h2>';
                        $html .= '<table border="1" style="">';
                        $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="200" align="center"><b>  '.trans('common::application.Name of the organization source of assistance').'  </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="130" align="center"><b> '.trans('common::application.Does the family receive help from org?').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.quantity').'   </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.currency_name').' </b></td>
                   </tr>';

                        foreach($item->financial_aid_source as $record) {
                            $row_fin_name = $record->name;
                            $row_fin_if_exist= $record->aid_take;
                            $row_fin_count= $record->aid_value;
                            $row_fin_currency= $record->currency_name;
                            $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="200" align="center"><b> '.$row_fin_name.'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="130" align="center"><b> '.$row_fin_if_exist.' </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$row_fin_count.'   </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="80" align="center"><b>'.$row_fin_currency.'</b></td>
                   </tr>';
                        }
                        $html .='</table>';
                        $html .='</body></html>';
                        PDF::AddPage('P','A4');
                        PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


                    }
                    if (sizeof($item->non_financial_aid_source)!=0){
                        $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                        $html.= '<h2 style="text-align:center;"> '.trans('common::application.concrete foreign aid data').'</h2>';
                        $html .= '<table border="1" style="">';
                        $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="250" align="center"><b>  '.trans('common::application.Name of the organization source of assistance').'  </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="150" align="center"><b> '.trans('common::application.Does the family receive help from org?').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.quantity').'   </b></td>
                   </tr>';

                        foreach($item->non_financial_aid_source as $record) {
                            $row_non_fin_name = $record->name;
                            $row_non_fin_if_exist= $record->aid_take;
                            $row_non_fin_count= $record->aid_value;
                            $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="250" align="center"><b> '.$row_non_fin_name.'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="150" align="center"><b> '.$row_non_fin_if_exist.' </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$row_non_fin_count.'   </b></td>
                   </tr>';

                        }

                        $html .='</table>';

                        $html .='</body></html>';
                        PDF::AddPage('P','A4');
                        PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                    }
                    if (sizeof($item->persons_properties)!=0){
                        $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                        $html.= '<h2 style="text-align:center;"> '.trans('common::application.Family property data').'</h2>';
                        $html .= '<table border="1" style="">';
                        $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="250" align="center"><b>  '.trans('common::application.Naming property').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="140" align="center"><b> '.trans('common::application.exist').'/'.trans('common::application.not exist').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.quantity').'   </b></td>
                   </tr>';
                        foreach($item->persons_properties as $record) {
                            $pro_name = $record->name;
                            $has_property= $record->has_property;
                            $quantity= $record->quantity;
                            $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="250" align="center"><b> '.$pro_name.'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="140" align="center"><b> '.$has_property.' </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="120" align="center"><b> '.$quantity.'   </b></td>
                   </tr>';

                        }
                        $html .='</table>';

                        $html .='</body></html>';
                        PDF::AddPage('P','A4');
                        PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);  }

                    if (sizeof($item->persons_documents)!=0){
                        $i=0;
                        foreach($item->persons_documents as $record) {
                            $doc_name = $record->name;
                            $img8=base_path('storage/app/').$record->filepath;
                            $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important; text-align: center !important;">';
                            $hig='700';
                            if($i == 0){
                                $html.= '<h2 style="text-align:center;"> '.trans('common::application.photos of attachments').'</h2>';
                                $hig='650';
                            }
                            $html.= '<h4 style="text-align:right;">'.$doc_name.' : </h4>';
                            $html.='<img src="'.$img8.'" alt="" height="'.$hig.'"  width="510"/>';
                            $html .='</body></html>';
                            PDF::SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
                            PDF::AddPage('P','A4');
                            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


                            $i++;

                        }
                    }

                }
                else{
                    $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                    $html.= '<h2 style="text-align:center;"> '.$category_name.'</h2>';
                    $html .= '<table border="1" style="border: 1px solid black; box-sizing:border-box;">';

                    $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.full_name').' </b></td>
                       <td width="325" align="center" >'.$name.'</td>
                        <td width="88" align="center" rowspan="6" style="background-color: white" >'
                        .'<img src="'.$img.'" alt="" height="140" />'.
                        '</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.id_card').'  </b></td>
                       <td width="120" align="center">'.$id_card_number.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.nationality').'  </b></td>
                       <td width="105" align="center">'.$nationality.'</td>
                      
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.BIRTH_DT').' </b></td>
                       <td width="120" align="center" >'.$birthday.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.birth_place').'</b></td>
                       <td width="105" align="center">'.$birth_place.'</td>
                   </tr>';

                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.SOCIAL_STATUS').' </b></td>
                       <td width="120" align="center">'.$marital_status_name.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.gender').' </b></td>
                       <td width="105" align="center">'.$gender.'</td>
                   </tr>';

                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.citizen').'  / '.trans('common::application.refugee').'</b></td>
                       <td width="120" align="center">'.$refugee.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.unrwa_card_number').'</b></td>
                       <td width="105" align="center">'.$unrwa_card_number.'</td>
                   </tr>';

                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.address').':  </b></td>
                       <td width="325" align="center">'.$address.'</td>
                   </tr>';

                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.guardian_kinship').'</b></td>
                       <td width="413" align="center">' . $guardian_kinship . '</td>
                   </tr>';
                    $html .='</table>';
                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                        '.trans('common::application.contact information').'
                        </b></td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.mobile').'</b></td>
                       <td width="156" align="center">'.$primary_mobile.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.secondery_mobile').'</b></td>
                       <td width="157" align="center">'.$secondary_mobile.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.phone').'</b></td>
                       <td width="156" align="center">'.$phone.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.wataniya').'</b></td>
                       <td width="157" align="center">'.$wataniya.'</td>
                   </tr>';
                    $html .='</table>';
                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                    '.trans('common::application.education info').'
                        </b></td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.is study').'</b></td>
                       <td width="156" align="center">'.$study.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_type_of_study').'</b></td>
                       <td width="157" align="center">'.$type.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.authority_of_study').'</b></td>
                       <td width="156" align="center">'.$authority.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_educational_level').'</b></td>
                       <td width="156" align="center">'.$stage.'</td>
                       </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.qualification').'</b></td>
                       <td width="156" align="center">'.$degree.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_name_of_education_organization').'</b></td>
                       <td width="157" align="center">'.$school.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.year').' </b></td>
                       <td width="156" align="center">'.$year.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.the_grade').' </b></td>
                       <td width="157" align="center">'.$grade.'</td>
                   </tr>';

                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_study_level').'</b></td>
                       <td width="156" align="center">'.$level.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.points').'</b></td>
                       <td width="157" align="center">'.$points.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.Specialization').'</b></td>
                       <td width="156" align="center">'.$specialization.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  </b></td>
                       <td width="157" align="center">'.''.'</td>
                   </tr>';
                    $html .='</table>';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                      '.trans('common::application.health info').'
                        </b></td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.health_condition').' </b></td>
                       <td width="156" align="center">'.$health_status.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.disease_id').'</b></td>
                       <td width="157" align="center">'.$diseases_name.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.disease_/_disability_details').'</b></td>
                       <td width="413" align="center">'.$health_details.'</td>
                   </tr>';
                    $html .='</table>';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                     '.trans('common::application.islamic info').'
                        </b></td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.Dose_he_pray?').'</b></td>
                       <td width="156" align="center">'.$prayer.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.save_quran').'</b></td>
                       <td width="157" align="center">'.$save_quran.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.not_prayer_reason').'</b></td>
                       <td width="413" align="center">'.$prayer_reason.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.quran_parts').'</b></td>
                       <td width="156" align="center">'.$quran_parts.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.quran_chapters').'</b></td>
                       <td width="157" align="center">'.$quran_chapters.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.not save quran reason').'  </b></td>
                       <td width="413" align="center">'.$quran_reason.'</td>
                   </tr>';
                    $html .='</table>';
                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                       '.trans('common::application.works info').' 
                        </b></td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.is_she_working?').'</b></td>
                       <td width="156" align="center">'.$working.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_job_').' </b></td>
                       <td width="157" align="center">'.$work_job.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_location').' </b></td>
                       <td width="156" align="center">'.$work_location.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.Work_Status').' </b></td>
                       <td width="157" align="center">'.$work_status.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.Wage category').':  </b></td>
                       <td width="156" align="center">'.$work_wage.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.monthly_income').'</b></td>
                       <td width="157" align="center">'.' '.'</td>
                   </tr>';
                    $html .='</table>';
                    $html .='</body></html>';
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

                    $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                    $html .= '<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                       '.trans('common::application.home info').'
                        </b></td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.property_type').'</b></td>
                       <td width="156" align="center">' . $property_types . '</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.property_type').'('.trans('common::application.for renter').') </b></td>
                       <td width="157" align="center">' . $rent_value . '</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.roof_material').'</b></td>
                       <td width="156" align="center">' . $roof_materials . '</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.residence_condition').'</b></td>
                       <td width="157" align="center">' . $house_condition . '</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.indoor_condition').'</b></td>
                       <td width="156" align="center">' . $residence_condition . '</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.habitable').'</b></td>
                       <td width="157" align="center">' . $habitable . '</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.home_area').'</b></td>
                       <td width="156" align="center">' . $area . '</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.rooms').'</b></td>
                       <td width="157" align="center">' . $rooms . '</td>
                   </tr>';
                    $html .= '</table>';

                    if($request->get('guardian_detail')){
                        if($item->guardian_id != null){
                            $guardian =Person::fetch(array('action'=>'show', 'id' => $item->guardian_id,'person' =>true, 'persons_i18n' => true,
                                'contacts' => true,  'education' => true,
                                'work' => true, 'residence' => true, 'banks' => true));

                            $guardian_first_name= $guardian->first_name;
                            $guardian_age= $guardian->age ;
                            $guardian_second_name= $guardian->second_name;
                            $guardian_third_name= $guardian->third_name ;
                            $guardian_last_name= $guardian->last_name ;
                            $guardian_name= $guardian->full_name;
                            $guardian_id_card_number= $guardian->id_card_number;
                            $guardian_birthday= $guardian->birthday;
                            $guardian_marital_status_name= $guardian->marital_status_name;
                            $guardian_birth_place= $guardian->birth_place;
                            $guardian_nationality= $guardian->nationality;
                            $guardian_country= $guardian->country;
                            $guardian_governarate= $guardian->governarate;
                            $guardian_city= $guardian->city;
                            $guardian_nearLocation= $guardian->nearLocation;
                            $guardian_mosque_name= $guardian->mosque_name;
                            $guardian_street_address= $guardian->street_address;
                            $guardian_address= $guardian->address;
                            $guardian_phone= $guardian->phone;
                            $guardian_primary_mobile= $guardian->primary_mobile;
                            $guardian_wataniya= $guardian->wataniya;
                            $guardian_secondery_mobile= $guardian->secondary_mobile ;
                            $guardian_en_first_name= $guardian->en_first_name;
                            $guardian_en_second_name= $guardian->en_second_name;
                            $guardian_en_third_name= $guardian->en_third_name;
                            $guardian_en_last_name= $guardian->en_last_name;
                            $guardian_en_name= $guardian->en_name;
                            $guardian_property_types= $guardian->property_types;
                            $guardian_rent_value= $guardian->rent_value;
                            $guardian_roof_materials= $guardian->roof_materials;
                            $guardian_area= $guardian->area;
                            $guardian_rooms= $guardian->rooms;
                            $guardian_residence_condition= $guardian->residence_condition;
                            $guardian_house_condition = $guardian->house_condition;
                            $guardian_indoor_condition= $guardian->indoor_condition;
                            $guardian_stage= $guardian->stage;
                            $guardian_working= $guardian->working;
                            $guardian_can_work= $guardian->can_work;
                            $guardian_work_job= $guardian->work_job;
                            $guardian_work_reason= $guardian->work_reason;
                            $guardian_work_status= $guardian->work_status;
                            $guardian_work_wage= $guardian->work_wage;
                            $guardian_work_location= $guardian->work_location;
                            $guardian_monthly_income= $guardian->monthly_income;
                            $guardian_gender_male= $guardian->gender_male;
                            $guardian_gender_female= $guardian->gender_female;
                            $guardian_is_refugee= $guardian->is_refugee;
                            $guardian_per_refugee= $guardian->per_refugee;
                            $guardian_per_citizen= $guardian->per_citizen;
                            $guardian_father_brothers= $guardian->father_brothers;
                            $guardian_mother_brothers= $guardian->mother_brothers;
                            $guardian_brothers= $guardian->brothers;
                            $guardian_children= $guardian->children;
                            $guardian_male_children= $guardian->male_children;
                            $guardian_female_children= $guardian->female_children;
                            $guardian_is_working= $guardian->is_working;
                            $guardian_not_working= $guardian->not_working;
                            $guardian_can_working= $guardian->can_working;
                            $guardian_can_not_working= $guardian->can_not_working;
                            $guardian_health_status= $guardian->health_status;
                            $guardian_good_health_status= $guardian->good_health_status;
                            $guardian_chronic_disease= $guardian->chronic_disease;
                            $guardian_disabled= $guardian->disabled;
                            $guardian_study = $guardian->study;
                            $guardian_type = $guardian->type;
                            $guardian_level = $guardian->level;
                            $guardian_year = $guardian->year;
                            $guardian_points = $guardian->points;
                            $guardian_school = $guardian->school;
                            $guardian_grade = $guardian->grade;
                            $guardian_authority = $guardian->authority;
                            $guardian_stage = $guardian->stage;
                            $guardian_degree = $guardian->degree;
                            $guardian_specialization = $guardian->specialization;

                            $html .='<div style="height: 5px; color: white"> - </div>';
//                    $html.= '<h2 style="text-align:center;">  '.trans('common::application.maintainer info').' </h2>';

                            $html .= '<table border="1" style="">';
                            $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                        '.trans('common::application.maintainer info').'
                        </b></td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.full_name').'</b></td>
                       <td width="413" align="center">'.$guardian_name.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.id_card').'</b></td>
                       <td width="156" align="center">'.$guardian_id_card_number.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.BIRTH_DT').'</b></td>
                       <td width="156" align="center">'.$guardian_birthday.'</td>
                       </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.SOCIAL_STATUS').'</b></td>
                       <td width="156" align="center">'.$guardian_marital_status_name.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.nationality').'</b></td>
                       <td width="157" align="center">'.$guardian_nationality.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.address').'</b></td>
                       <td width="413" align="center">'.$guardian_address.'</td>
                   </tr>';
                            $html .='</table>';

                            $html .='<div style="height: 5px; color: white"> - </div>';
                            $html .= '<table border="1" style="">';
                            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                                               '.trans('common::application.contact information').'
                        </b></td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.mobile').'</b></td>
                       <td width="156" align="center">'.$guardian_primary_mobile.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.secondery_mobile').'</b></td>
                       <td width="157" align="center">'.$guardian_secondery_mobile.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.phone').'</b></td>
                       <td width="156" align="center">'.$guardian_phone.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.wataniya').'</b></td>
                       <td width="157" align="center">'.$guardian_wataniya.'</td>
                   </tr>';
                            $html .='</table>';

                            $html .='<div style="height: 5px; color: white"> - </div>';
                            $html .= '<table border="1">';
                            $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                      '.trans('common::application.home info').'
                        </b></td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.property_type').'</b></td>
                       <td width="156" align="center">'.$guardian_property_types.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.property_type').'('.trans('common::application.for renter').') </b></td>
                       <td width="157" align="center">'.$guardian_rent_value.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.roof_material').'</b></td>
                       <td width="156" align="center">'.$guardian_roof_materials.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.residence_condition').'</b></td>
                       <td width="157" align="center">'.$guardian_house_condition.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.indoor_condition').'</b></td>
                       <td width="156" align="center">'.$guardian_residence_condition.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.habitable').'</b></td>
                       <td width="157" align="center">'.$guardian_indoor_condition.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.home_area').'</b></td>
                       <td width="156" align="center">'.$guardian_area.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.rooms').'</b></td>
                       <td width="157" align="center">'.$guardian_rooms.'</td>
                   </tr>';
                            $html .='</table>';

                            $html .='<div style="height: 5px; color: white"> - </div>';
                            $html .= '<table border="1" style="">';
                            $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                       '.trans('common::application.works info').' 
                        </b></td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.is_she_working?').'  </b></td>
                       <td width="156" align="center">'.$guardian_working.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_job_').' </b></td>
                       <td width="157" align="center">'.$guardian_work_job.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_location').' </b></td>
                       <td width="156" align="center">'.$guardian_work_location.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.Work_Status').' </b></td>
                       <td width="157" align="center">'.$guardian_work_status.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.Wage category').':  </b></td>
                       <td width="156" align="center">'.$guardian_work_wage.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.monthly_income').'</b></td>
                       <td width="157" align="center">'.$guardian_monthly_income.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.is_can_work').' </b></td>
                       <td width="156" align="center">'.$guardian_can_work.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.work_reason').' </b></td>
                       <td width="157" align="center">'.$guardian_work_reason.'</td>
                   </tr>';
                            $html .='</table>';
                            $html .='<div style="height: 5px; color: white"> - </div>';
                            $html .= '<table border="1" style="">';
                            $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                    '.trans('common::application.education info').'
                        </b></td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.is study').'</b></td>
                       <td width="156" align="center">'.$guardian_study.'</td>
                      <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_educational_level').'</b></td>
                       <td width="157" align="center">'.$guardian_stage.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.qualification').'</b></td>
                       <td width="156" align="center">'.$guardian_degree.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.Specialization').'</b></td>
                       <td width="157" align="center">'.$guardian_specialization.'</td>
                   </tr>';


                            $html .='</table>';

                            $html .='<div style="height: 5px; color: white"> - </div>';
                            $html .= '<table border="1" style="">';
                            $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                      '.trans('common::application.health info').'
                        </b></td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.health_condition').' </b></td>
                       <td width="156" align="center">'.$health_status.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.disease_id').'</b></td>
                       <td width="157" align="center">'.$diseases_name.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.disease_/_disability_details').'</b></td>
                       <td width="413" align="center">'.$health_details.'</td>
                   </tr>';
                            $html .='</table>';

                        }
                    }

                    $html .= '</body></html>';
                    PDF::AddPage('P', 'A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

                    if($request->get('parent_detail')){

                        $get=array('action' => 'show','id' => $item->father_id,
                            'person'  => true, 'persons_i18n'  =>  true,
                            'health'  =>  true,  'education'  =>  true, 'work'  =>  true);

                        $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';

                        if($item->father_id != null){
                            $get['id']=$item->father_id;
                            $father =Person::fetch($get);
                            $father_first_name= $father->first_name;
                            $father_second_name= $father->second_name;
                            $father_third_name= $father->third_name ;
                            $father_last_name= $father->last_name ;
                            $father_name= $father->full_name;
                            $father_spouses= $father->spouses;
                            $father_id_card_number= $father->id_card_number;
                            $father_birthday= $father->birthday;
                            $father_en_first_name= $father->en_first_name;
                            $father_en_second_name= $father->en_second_name;
                            $father_en_third_name= $father->en_third_name;
                            $father_en_last_name= $father->en_last_name;
                            $father_en_name= $father->en_name;
                            $father_health_status= $father->health_status;
                            $father_health_details= $father->health_details;
                            $father_diseases_name= $father->diseases_name;
                            $father_specialization= $father->specialization;
                            $father_degree= $father->degree;
                            $father_alive= $father->alive;
                            $father_is_alive= $father->is_alive;
                            $father_death_date= $father->death_date;
                            $father_death_cause_name= $father->death_cause_name;
                            $father_working= $father->working;
                            $father_work_job= $father->work_job;
                            $father_work_location= $father->work_location;
                            $father_monthly_income= $father->monthly_income;
                            $father_gender_male= $father->gender_male;
                            $father_gender_female= $father->gender_female;
                            $father_is_refugee= $father->is_refugee;
                            $father_per_refugee= $father->per_refugee;
                            $father_per_citizen= $father->per_citizen;
                            $father_father_brothers= $father->father_brothers;
                            $father_mother_brothers= $father->mother_brothers;
                            $father_brothers= $father->brothers;
                            $father_children= $father->children;
                            $father_male_children= $father->male_children;
                            $father_female_children= $father->female_children;
                            $father_is_working= $father->is_working;
                            $father_not_working= $father->not_working;
                            $father_can_working= $father->can_working;
                            $father_can_not_working= $father->can_not_working;
                            $father_health_status= $father->health_status;
                            $father_good_health_status= $father->good_health_status;
                            $father_chronic_disease= $father->chronic_disease;
                            $father_disabled= $father->disabled;


                            $html .='<div style="height: 5px; color: white"> - </div>';
                            $html .= '<table border="1" style="">';
                            $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                    '.trans('common::application.father-info').' 
                        </b></td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.full_name').'</b></td>
                       <td width="413" align="center">'.$father_name.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.id_card').'</b></td>
                       <td width="156" align="center">'.$father_id_card_number.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.BIRTH_DT').'</b></td>
                       <td width="156" align="center">'.$father_birthday.'</td>
                       </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.is_she_alive?').'</b></td>
                       <td width="156" align="center">'.$father_alive.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.death_date').'</b></td>
                       <td width="157" align="center">'.$father_death_date.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.death_cause_id').' </b></td>
                       <td width="413" align="center">'.$father_death_cause_name.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_number_of_spouses').'</b></td>
                       <td width="156" align="center">'.$father_spouses.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.monthly_income').' </b></td>
                       <td width="157" align="center">'.$grade.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.qualification').'</b></td>
                       <td width="156" align="center">'.$father_degree.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.Specialization').' </b></td>
                       <td width="157" align="center">'.$father_specialization.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.is_he_working').'</b></td>
                       <td width="156" align="center">'.$father_working.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_job_').'</b></td>
                       <td width="157" align="center">'.$father_work_job.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_location').' </b></td>
                       <td width="413" align="center">'.$father_work_location.'</td>
                   </tr>';

                            if($item->father_dead == 0){
                                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.health_condition').' </b></td>
                       <td width="156" align="center">'.$father_health_status.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.disease_id').'</b></td>
                       <td width="157" align="center">'.$father_diseases_name.'</td>
                   </tr>';
                                $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.disease_/_disability_details').'</b></td>
                       <td width="413" align="center">'.$father_health_details.'</td>
                   </tr>';
                            }
                            $html .='</table>';
                            $html .='<div style="height: 40px; color: white"> - </div>';
                        }

                        if($item->mother_id != null){
                            $get['id']=$item->mother_id;
                            $mother =Person::fetch($get);
                            $mother_prev_family_name= $mother->prev_family_name;
                            $mother_first_name= $mother->first_name;
                            $mother_second_name= $mother->second_name;
                            $mother_third_name= $mother->third_name;
                            $mother_last_name= $mother->last_name;
                            $mother_name= $mother->full_name;
                            $mother_id_card_number= $mother->id_card_number;
                            $mother_birthday= $mother->birthday;
                            $mother_marital_status_name= $mother->marital_status_name;
                            $mother_nationality= $mother->nationality;
                            $mother_en_first_name= $mother->en_first_name;
                            $mother_en_second_name= $mother->en_second_name;
                            $mother_en_third_name= $mother->en_third_name;
                            $mother_en_last_name= $mother->en_last_name;
                            $mother_en_name= $mother->en_name;
                            $mother_health_status= $mother->health_status;
                            $mother_health_details= $mother->health_details;
                            $mother_diseases_name= $mother->diseases_name;
                            $mother_specialization= $mother->specialization;
                            $mother_stage= $mother->stage;
                            $mother_alive= $mother->alive;
                            $mother_is_alive= $mother->is_alive;
                            $mother_death_date= $mother->death_date;
                            $mother_death_cause_name= $mother->death_cause_name;
                            $mother_working= $mother->working;
                            $mother_work_job= $mother->work_job;
                            $mother_work_location= $mother->work_location;
                            $mother_monthly_income= $mother->monthly_income;
                            $mother_gender_male= $mother->gender_male;
                            $mother_gender_female= $mother->gender_female;
                            $mother_is_refugee= $mother->is_refugee;
                            $mother_per_refugee= $mother->per_refugee;
                            $mother_per_citizen= $mother->per_citizen;
                            $mother_father_brothers= $mother->father_brothers;
                            $mother_mother_brothers= $mother->mother_brothers;
                            $mother_brothers= $mother->brothers;
                            $mother_children= $mother->children;
                            $mother_male_children= $mother->male_children;
                            $mother_female_children= $mother->female_children;
                            $mother_is_working= $mother->is_working;
                            $mother_not_working= $mother->not_working;
                            $mother_can_working= $mother->can_working;
                            $mother_can_not_working= $mother->can_not_working;
                            $mother_health_status= $mother->health_status;
                            $mother_good_health_status= $mother->good_health_status;
                            $mother_diseases_name= $mother->diseases_name;
                            $mother_chronic_disease= $mother->chronic_disease;
                            $mother_disabled= $mother->disabled;


                            $html .= '<table border="1" style="">';
                            $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                    '.trans('common::application.mother-info').'  
                        </b></td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.full_name').'</b></td>
                       <td width="413" align="center">'.$mother_name.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.id_card').'</b></td>
                       <td width="156" align="center">'.$mother_id_card_number.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.BIRTH_DT').'</b></td>
                       <td width="156" align="center">'.$mother_birthday.'</td>
                       </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.prev_family_name').'</b></td>
                       <td width="156" align="center">'.$mother_prev_family_name.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.SOCIAL_STATUS').'</b></td>
                       <td width="157" align="center">'.$mother_marital_status_name.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.is_she_alive?').'</b></td>
                       <td width="156" align="center">'.$mother_is_alive.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.death_date').'</b></td>
                       <td width="157" align="center">'.$mother_death_date.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.death_cause_id').' </b></td>
                       <td width="413" align="center">'.$mother_death_cause_name.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_educational_level').' </b></td>
                       <td width="156" align="center">'.$mother_stage.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.Specialization').' </b></td>
                       <td width="157" align="center">'.$mother_specialization.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.is_he_working').'</b></td>
                       <td width="156" align="center">'.$mother_working.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_job_').'</b></td>
                       <td width="157" align="center">'.$mother_work_job.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_location').' </b></td>
                       <td width="156" align="center">'.$mother_work_location.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.monthly_income').'</b></td>
                       <td width="157" align="center">'.$mother_monthly_income.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.health_condition').' </b></td>
                       <td width="156" align="center">'.$mother_health_status.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.disease_id').'</b></td>
                       <td width="157" align="center">'.$mother_diseases_name.'</td>
                   </tr>';
                            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.disease_/_disability_details').'</b></td>
                       <td width="413" align="center">'.$mother_health_details.'</td>
                   </tr>';
                            $html .='</table>';
                        }



                        $html .= '</body></html>';
                        PDF::AddPage('P', 'A4');
                        PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

                    }

                    $setting_id = 'sponsorship_custom_form';
                    $CaseCustomData =FormsCasesData::getCaseCustomData($item->case_id,$item->category_id,$item->organization_id,$setting_id);
                    if (sizeof($CaseCustomData)!=0){

                        $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                        $html.= '<h2 style="text-align:center;">  '.trans('common::application.Custom Form Information').'</h2>';
                        $html .= '<table border="1" style="">';
                        $html .= ' <tr>
                               <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.organization_data').'  </b></td>
                               <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="400" align="center"><b> '.trans('common::application.explanation').'   </b></td>
                           </tr>';
                        $z=1;
                        foreach($CaseCustomData as $record) {
                            $c_label = $record['label'];
                            $c_value = $record['value'];

                            if($record['type'] != 3){
                                $html .= ' <tr>
                                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="120" align="center"><b> '.$c_label.'  </b></td>
                                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="400" align="center"><b> '.$c_value.'   </b></td>
                                   </tr>';
                            }else{
                                $html .= ' <tr>
                       <td style="height: 50px ; line-height: 35px; padding: 25px 0 12px 0;" width="120" align="center"><b> '.$c_label.'  </b></td>
                       <td style="height: 50px ; line-height: 35px; padding: 15px 0 12px 0;" width="400" align="center"><b> '.$c_value.'   </b></td>
                   </tr>';

                            }
                            $z++;
                        }
                        $html .='</table>';
                        $html .='</body></html>';

                        PDF::SetMargins(PDF_MARGIN_LEFT, 2, PDF_MARGIN_RIGHT);
                        PDF::AddPage('P','A4');
                        PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                    }
                    if (sizeof($item->family_member)!=0){

                        $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                        $html.= '<h2 style="text-align:center;"> '.trans('common::application.family-data').'

    ('. sizeof($item->family_member) .')</h2>';
                        $html .= '<table border="1" style="">';
                        $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.name').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.id_card').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.BIRTH_DT').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="50" align="center"><b> '.trans('common::application.gender').'</b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.SOCIAL_STATUS').'</b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.health_condition').' </b></td>
                   </tr>';
                        $z=1;
                        foreach($item->family_member as $record) {
                            $fm_nam = $record->name;
                            $fm_idc = $record->id_card_number;
                            $fm_bd = $record->birthday;
                            $fm_gen = $record->gender;
                            $fm_age = $record->age;
                            $fm_ms = $record->marital_status;
                            $fm_kin = $record->kinship_name;
                            $fm_stg = $record->stage;
                            $fm_lvl = $record->level;
                            $fm_yer = $record->year;
                            $fm_grd = $record->grade;
                            $fm_scl = $record->school;
                            $fm_hs = $record->health_status;
                            $fm_disnm = $record->diseases_name;
                            $fm_working = $record->working;
                            $fm_job = $record->work_job;
                            $html .= ' <tr>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="120" align="center"><b> '.$fm_nam.'  </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_idc.' </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_bd.'   </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="50" align="center"><b>'.$fm_gen.'</b></td> 
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_ms.' </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$fm_hs.'   </b></td>
                   </tr>';
                            $z++;
//                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="50" align="center"><b>'.$fm_kin.'</b></td>
//                                    <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_working.'   </b></td>
//                <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$fm_job.'   </b></td>
                        }
                        $html .='</table>';
                        $html .='</body></html>';

                        PDF::SetMargins(PDF_MARGIN_LEFT, 2, PDF_MARGIN_RIGHT);
                        PDF::AddPage('P','A4');
                        PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
                    }
                    if (sizeof($item->persons_documents)!=0){
                        $i=0;
                        foreach($item->persons_documents as $record) {
                            $doc_name = $record->name;
                            $img8=base_path('storage/app/').$record->filepath;
                            $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important; text-align: center !important;">';
                            $hig='700';
                            if($i == 0){
                                $html.= '<h2 style="text-align:center;"> '.trans('common::application.photos of attachments').'</h2>';
                                $hig='650';
                            }
                            $html.= '<h4 style="text-align:right;">'.$doc_name.' : </h4>';
                            $html.='<img src="'.$img8.'" alt="" height="'.$hig.'"  width="510"/>';
                            $html .='</body></html>';
                            PDF::SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
                            PDF::AddPage('P','A4');
                            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


                            $i++;

                        }
                    }
                }


                PDF::Output($dirName.'/'.$item->category_name.' '.$item->id_card_number.' '.$item->full_name . '.pdf', 'F');

            }

            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');

            $zip = new \ZipArchive;
            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                foreach ( glob( $dirName . '/*' ) as $fileName )
                {
                    $file = basename( $fileName );
                    $zip->addFile( $fileName, $file );
                }

                $zip->close();
                array_map('unlink', glob("$dirName/*.*"));
                rmdir($dirName);
            }

            if($this->type == 1){
                $message= trans('common::application.exported cases forms').'('. trans('common::application.sponsorships').')' ;
            }else{
                $message= trans('common::application.exported cases forms').'( '. trans('common::application.aids').' ) ';
            }

            \Log\Model\Log::saveNewLog('CASE_EXPORTED',$message);

            return response()->json(['download_token' => $token]);

        }

        return response()->json(['status'=>false]);
    }
    /************ CHANGE CATEGORY *************/
    public function changeCategory(Request $request,$id)
    {
        try {
            $response = array();
            if(\Common\Model\AidsCases::where('id',$id)->update(['category_id'=>$request->category_id]))
            {
                $case = CaseModel::fetch(array('category_name' => true,'full_name'=>true),$id);
                $message = trans('common::application.Has changed a label') .$case->full_name .trans('common::application.for') .$case->category_name;
                \Log\Model\Log::saveNewLog('CASE_UPDATED',$message);
                $response["status"]= 'success';
                $response["msg"]= trans('aid::application.The row is updated');
            }else{
                $response["status"]= 'failed';
                $response["msg"]= trans('aid::application.There is no update');
            }
            return response()->json($response);

        } catch (Exception $e) {
            $response["status"] = 'error';
            $response["error_code"] = $e->getCode();
            $response["msg"] = 'Databaseerror';
//            $response["msg"] = $e->getMessage();
            return response()->json($response);
        }

    }
    public function changeCategoryToGroup(Request $request,$id)
    {
        try {
            $response = array();

            $success=0;
            $total=sizeof($request->cases);
            $restricted=$request->restricted;
            foreach ($request->cases as $item){

                if(\Common\Model\AidsCases::where('id',$item)->update(['category_id'=>$request->category_id])){
                    $case = CaseModel::fetch(array('category_name' => true,'full_name'=>true),$item);
                    $message =  trans('common::application.Has changed a label').$case->full_name .trans('common::application.for').$case->category_name;
                    \Log\Model\Log::saveNewLog('CASE_UPDATED',$message);
                    $success++;
                }

            }
            if($success ==0 ){
                $response["status"]= 'failed';
                $response["msg"]= trans('aid::application.There is no update');
            }else if($restricted ==0 && $total == $success){
                $response["status"]= 'success';
                $response["msg"]= trans('aid::application.The row is updated');
            }else{
                $response["status"]= 'failed';
                $response["msg"]= trans('sponsorship::application.The cases is not successfully change category')
                    . ' ( '. trans('sponsorship::application.total') .': ' .$total
                    .' ، '. trans('sponsorship::application.change category') .' : '. $success.' , '
                    .' ، '. trans('sponsorship::application.same') .' : '. $restricted .' )';
            }
            return response()->json($response);

        } catch (Exception $e) {
            $response["status"] = 'error';
            $response["error_code"] = $e->getCode();
            $response["msg"] = 'Databaseerror';
//            $response["msg"] = $e->getMessage();
            return response()->json($response);
        }

    }

    /************ GET RESTRICTED CASES *************/
    public function exportRestricted(Request $request){

        $data=$request->get('exported');
        $rows=[];
        foreach($data as $key =>$value){
            $rows[$key][trans('aid::application.#')]=$key+1;
            foreach($value as $k =>$v){
                if($k != 'case_id' && $k != 'id' && $k != 'sponsor_id'&& $k != 'flag'&& $k != 'date_flag'){
                    if($k =='sponsor_number'){
                        $rows[$key][trans('aid::application.sponsorship_number')]= $v;
                    }else{
                        $rows[$key][trans('aid::application.' . $k)]= $v;
                    }
                    $translate=' ';
                    if($k =='reason'){
                        if($v =='not_same'){ $translate= trans('aid::application.not_same');}
                        if($v =='blocked'){ $translate= trans('aid::application.blocked');}
                        if($v =='nominated'){ $translate= trans('aid::application.nominated');}
                        if($v =='duplicated'){ $translate= trans('aid::application.duplicated');}
                        if($v =='out_of_regions'){ $translate= trans('aid::application.out_of_regions');}
                        if($v =='out_of_ratio'){ $translate= trans('aid::application.out_of_ratio');}
                        if($v =='policy_restricted'){ $translate= trans('aid::application.policy_restricted');}
                        if($v =='restricted_total_payment_persons'){ $translate= trans('aid::application.restricted_total_payment_persons');}
                        if($v =='restricted_count_of_family'){ $translate= trans('aid::application.restricted_count_of_family');}
                        if($v =='restricted_amount'){ $translate= trans('aid::application.restricted_amounts');}
                        if($v =='the same status'){ $translate= trans('sponsorship::application.the same status');}
                        if($v =='invalid status'){ $translate= trans('sponsorship::application.invalid status');}
                        if($v =='not_nominated'){ $translate= trans('sponsorship::application.not_nominated');}
                        if($v =='not_case'){ $translate= trans('sponsorship::application.not_case');}
                        if($v =='max_limited'){ $translate= trans('aid::application.max_limited');}
                        if($v =='invalid_id_card_number'){ $translate= trans('sponsorship::application.invalid_id_card_number');}
                        $rows[$key][trans('aid::application.reason')]= $translate;
                    }
                }
            }
        }

        $token = md5(uniqid());
        \Excel::create('export_' . $token, function($excel) use($rows) {
            $excel->sheet(date('y-m-d'), function($sheet) use($rows) {
                $sheet->setStyle([
                    'font' => [
                        'name' => 'Calibri',
                        'size' => 11,
                        'bold' => true
                    ]
                ]);
                $sheet->setAllBorders('thin');
                $sheet->setfitToWidth(true);
                $sheet->setRightToLeft(true);
                $style = [
                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                    'font' =>[
                        'name'      =>  'Simplified Arabic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ]
                ];

                $sheet->getStyle("A1:D1")->applyFromArray($style);
                $sheet->setHeight(1,30);

                $style = [
                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                    'font' =>[
                        'name'      =>  'Simplified Arabic',
                        'size'      =>  12,
                        'bold' => false
                    ]
                ];

                $sheet->getDefaultStyle()->applyFromArray($style);
                $sheet->fromArray($rows);

            });
        })->store('xlsx', storage_path('tmp/'));

        return response()->json(['download_token' => $token]);
    }
    public function exportRestrictedCard(Request $request){

        $data=$request->get('exported');
        $rows=[];
        foreach($data as $key =>$value){
            $rows[$key][trans('aid::application.#')]=$key+1;
            foreach($value as $k =>$v){
                if($k != 'case_id' && $k != 'id' && $k != 'sponsor_id'&& $k != 'flag'&& $k != 'date_flag'){
                    if($k =='card'){
                        $rows[$key][trans('aid::application.id_card_number')]= $v;
                    }
                    $translate=' ';
                    if($k =='reason'){
                        if($v =='has_same_status'){ $translate= trans('aid::application.has_same_status');}
                        if($v =='duplicated'){ $translate= trans('aid::application.duplicated');}
                        if($v =='not_case'){ $translate= trans('aid::application.not_case');}
                        if($v =='not_person'){ $translate= trans('aid::application.not_person');}

                        if($v =='not_case_for_selected_category'){ $translate= trans('aid::application.not_case_of_selected_category');}
                        if($v =='invalid_card'){ $translate= trans('aid::application.invalid_card');}
                        $rows[$key][trans('aid::application.not_update_reason')]= $translate;
                    }
                }
            }
        }

        $token = md5(uniqid());
        \Excel::create('export_' . $token, function($excel) use($rows) {
            $excel->sheet(trans('common::application.Detection of individuals whose status has not been modified'), function($sheet) use($rows) {
                $sheet->setStyle([
                    'font' => [
                        'name' => 'Calibri',
                        'size' => 11,
                        'bold' => true
                    ]
                ]);
                $sheet->setAllBorders('thin');
                $sheet->setfitToWidth(true);
                $sheet->setRightToLeft(true);
                $style = [
                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                    'font' =>[
                        'name'      =>  'Simplified Arabic',
                        'size'      =>  12,
                        'bold'      =>  true
                    ]
                ];

                $sheet->getStyle("A1:C1")->applyFromArray($style);
                $sheet->setHeight(1,30);

                $style = [
                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                    'font' =>[
                        'name'      =>  'Simplified Arabic',
                        'size'      =>  12,
                        'bold' => false
                    ]
                ];

                $sheet->getDefaultStyle()->applyFromArray($style);
                $sheet->fromArray($rows);

            });
        })->store('xlsx', storage_path('tmp/'));

        return response()->json(['download_token' => $token]);
    }
    /************ GET INCOMPLETE CASES *************/
    public function getInCompleteCases(Request $request)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $user = \Auth::user();
        $result= CaseModel::getInCompleteCases($request->page,$request->get('type'),$request->all(),$user->organization_id,$request->get('status'));
        $Org =\Organization\Model\Organization::findorfail($user->organization_id);
        $master=true;
        if($Org->parent_id){
            $master=false;
        }
        return response()->json(['master'=>$master,'Cases'  => $result,'organization_id'=>$user->organization_id] );
    }

    /************ GET CASES ATTACHMENTS *************/
    public function getCaseAttachments(Request $request)
    {

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $user = \Auth::user();
        $result = CaseModel::getCaseAttachments($request->page, $request->all(), $user->organization_id, $request->get('status'),$this->type);

        if ($request->get('status') == 'export'){
            if(sizeof($result)!=0){
                $dirName = base_path('storage/app/documents/Attachments');
                if (!is_dir($dirName)) {
                    @mkdir($dirName, 0777, true);
                }

                foreach($result as $key=>$value ) {
                    if (sizeof($value['files']) != 0) {
                        $Subdir = base_path('storage/app/documents/Attachments/' . $value['name']);
                        if (!is_dir($Subdir)) {
                            @mkdir($Subdir, 0777, true);
                        }
                        foreach ($value['files'] as $k => $v) {
                            $extention = explode(".", $v->filepath);
                            copy(base_path('storage/app/') . $v->filepath, $Subdir . '/' . $v->name . '.' . end($extention));
                        }
                    }
                }

                $token = md5(uniqid());
                $zipFileName = storage_path('tmp/export_' . $token . '.zip');

                $zip = new \ZipArchive;
                if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
                {
                    $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dirName), \RecursiveIteratorIterator::LEAVES_ONLY);
                    foreach ($files as $name => $file)
                    {
                        if (!$file->isDir())
                        {
                            $filePath = $file->getRealPath();
                            $relativePath = substr($filePath, strlen($dirName) + 1);
                            $zip->addFile($filePath, $relativePath);
                        }
                    }
                    $zip->close();
                }

                $dir=$dirName;
                if (is_dir($dir)) {
                    $objects = scandir($dir);
                    foreach ($objects as $object) {
                        if ($object != "." && $object != "..") {
                            if (filetype($dir."/".$object) == "dir"){

                                $objects_1 = scandir($dir."/".$object);
                                foreach ($objects_1 as $object1) {
                                    if ($object1 != "." && $object1 != "..") {
                                        unlink   ($dir."/".$object."/".$object1);
                                    }
                                }

                                rmdir($dir."/".$object);
                            }
                            else {
                                unlink   ($dir."/".$object);
                            }
                        }
                    }
                    rmdir($dir);
                }
                \Log\Model\Log::saveNewLog('CASE_EXPORTED',trans('common::application.exported case attachments in a compressed file'));

                return response()->json(['download_token' => $token,'status' => true]);
            }
            return response()->json(['status' => false]);

        }
        elseif($request->get('status') == 'exportToExcel'){
            if(sizeof($result) !=0){
                $selected = $request->document_ids;
                $entries = \DB::table('char_document_types_i18n')
                    ->where('char_document_types_i18n.language_id',1)
                    ->whereIn('char_document_types_i18n.document_type_id',$selected)
                    ->select(
                        'char_document_types_i18n.document_type_id as id',
                        'char_document_types_i18n.name as name'
                    )->orderBy('char_document_types_i18n.name')
                    ->get();

                $data=[];
                $category_type=$request->get('category_type');

                foreach($result as $key =>$value) {
                    $temp=[];
                    if($category_type == 2) {
                        $value->father_dcoument = null;
                        $value->mother_dcoument = null;
                        $value->guardian_dcoument = null;
                    }
                    $item=(object)$value;
                    $item->doc=[];

                    if($value->person_dcoument != null || $value->father_dcoument != null || $value->mother_dcoument != null || $value->guardian_dcoument != null){


                        if($value->person_dcoument != null){
                            $parts = explode(',', $value->person_dcoument);
                            foreach($parts as $part) {
                                if(!in_array($part,$temp)){
                                    $temp[]=$part;
                                }
                            }
                        }
                        if($value->father_dcoument != null){
                            $parts = explode(',', $value->father_dcoument);
                            foreach($parts as $part) {
                                if(!in_array($part,$temp)){
                                    $temp[]=$part;
                                }
                            }
                        }
                        if($value->mother_dcoument != null){
                            $parts = explode(',', $value->mother_dcoument);
                            foreach($parts as $part) {
                                if(!in_array($part,$temp)){
                                    $temp[]=$part;
                                }
                            }
                        }
                        if($value->guardian_dcoument != null){
                            $parts = explode(',', $value->guardian_dcoument);
                            foreach($parts as $part) {
                                if(!in_array($part,$temp)){
                                    $temp[]=$part;
                                }
                            }
                        }

                        if(!empty($temp)){
                            $final=[];
                            foreach($entries as $ke =>$va) {
                                if(in_array($va->id,$temp)){
                                    $final[]=trans('sponsorship::application.exist');
                                }else{
                                    $final[]=trans('sponsorship::application.not exist');
                                }
                            }
                            $item->doc=$final;
                        }

                    }
                    else{
                        $final=[];
                        foreach($entries as $ke =>$va) {
                            $final[]=trans('sponsorship::application.not exist');
                        }
                        $item->doc=$final;
                    }

                    $data[]=$item;
                }

                $token = md5(uniqid());
                \Excel::create('export_' . $token, function($excel) use($data,$entries,$category_type) {
                    $excel->sheet(trans('common::application.attachments report'), function($sheet) use($data,$entries,$category_type) {

                        $sheet->setOrientation('landscape');
                        $sheet->setRightToLeft(true);

                        $sheet ->setCellValue('A1',trans('sponsorship::application.#'));
                        $sheet ->setCellValue('B1',trans('sponsorship::application.name'));
                        $sheet ->setCellValue('C1',trans('sponsorship::application.category_name'));
                        $sheet ->setCellValue('D1',trans('sponsorship::application.organization_name'));
                        $sheet ->setCellValue('E1',trans('sponsorship::application.id_card_number'));
                        $sheet ->setCellValue('F1',trans('sponsorship::application.phone'));
                        $sheet ->setCellValue('G1',trans('sponsorship::application.primary_mobile'));
                        $sheet ->setCellValue('H1',trans('sponsorship::application.secondary_mobile'));


                        if($category_type == 1){
                            $sheet ->setCellValue('I1',trans('sponsorship::application.guardian_name'));
                            $sheet ->setCellValue('J1',trans('sponsorship::application.phone')." ( ".trans('sponsorship::application.guardian') ." ) ");
                            $sheet ->setCellValue('K1',trans('sponsorship::application.primary_mobile')." ( ".trans('sponsorship::application.guardian') ." ) ");
                            $sheet ->setCellValue('L1',trans('sponsorship::application.secondary_mobile')." ( ".trans('sponsorship::application.guardian') ." ) ");
                        }

                        $style_1 = array(
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' => [
                                'name' => 'Calibri',
                                'size' => 11,
                                'bold' => true
                            ],
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'D3D3D3')
                            ) ,'borders' => array(
                                'allborders' => array(
                                    'style' => 'thin'
                                )
                            )

                        );
                        $sheet->setHeight(array(1=> 45));
                        $sheet->getStyle('A1')->applyFromArray($style_1);
                        $sheet->getStyle('B1')->applyFromArray($style_1);
                        $sheet->getStyle('C1')->applyFromArray($style_1);
                        $sheet->getStyle('D1')->applyFromArray($style_1);
                        $sheet->getStyle('E1')->applyFromArray($style_1);
                        $sheet->getStyle('F1')->applyFromArray($style_1);
                        $sheet->getStyle('G1')->applyFromArray($style_1);
                        $sheet->getStyle('H1')->applyFromArray($style_1);

                        if($category_type == 1) {
                            $sheet->getStyle('I1')->applyFromArray($style_1);
                            $sheet->getStyle('J1')->applyFromArray($style_1);
                            $sheet->getStyle('K1')->applyFromArray($style_1);
                            $sheet->getStyle('L1')->applyFromArray($style_1);
                        }

                        $entriesIndex = ['I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                            'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL',
                            'AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
                            'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM',
                            'BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
                            'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO',
                            'CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ'
                        ];

                        $style_10 = array(
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' => [
                                'name' => 'Calibri',
                                'size' => 11,
                                'bold' => true
                            ],
                            'fill' => array(
                                'type' => 'solid',
                                'color' => array('rgb' => 'D3D3D3')
                            ) ,'borders' => array(
                                'allborders' => array(
                                    'style' => 'thin'
                                )
                            )

                        );

                        if($category_type == 1) {
                            $z=4;
                        }else{
                            $z=0;
                        }

                        foreach($entries as $k=>$v )
                        {
                            $sheet ->setCellValue($entriesIndex[$z].'1',$v->name);
                            $sheet->getStyle($entriesIndex[$z].'1')->applyFromArray($style_10);
                            $z++;
                        }

                        $zz= 2;
                        foreach($data as $key => $value)
                        {
                            $sheet ->setCellValue('A'.$zz,$key+1);
                            $sheet ->setCellValue('B'.$zz,$value->name);
                            $sheet ->setCellValue('C'.$zz,$value->category_name);
                            $sheet ->setCellValue('D'.$zz,$value->organization_name);
                            $sheet ->setCellValue('E'.$zz,$value->id_card_number);
                            $sheet ->setCellValue('F'.$zz,$value->phone);
                            $sheet ->setCellValue('G'.$zz,$value->primary_mobile);
                            $sheet ->setCellValue('H'.$zz,$value->secondary_mobile);

                            if($category_type == 1) {
                                $sheet ->setCellValue('I'.$zz,$value->guardian_name);
                                $sheet ->setCellValue('J'.$zz,$value->guardian_phone);
                                $sheet ->setCellValue('K'.$zz,$value->guardian_primary_mobile);
                                $sheet ->setCellValue('L'.$zz,$value->guardian_secondary_mobile);
                            }

                            if($category_type == 1) {
                                $z=4;
                            }else{
                                $z=0;
                            }

                            foreach($value->doc as $kk=>$vv )
                            {
                                $sheet ->setCellValue($entriesIndex[$z].$zz,$vv);
                                $z++;
                            }
                            $zz++;
                        }

                        $style = [
                            'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                            'font' => [
                                'name' => 'Simplified Arabic',
                                'size' => 11,
                                'bold' => true
                            ] ,'borders' => array(
                                'allborders' => array(
                                    'style' => 'thin'
                                )
                            )
                        ];

                        $sheet->getDefaultStyle()->applyFromArray($style);
                    });
                })->store('xlsx', storage_path('tmp/'));

                \Log\Model\Log::saveNewLog('CASE_EXPORTED',trans('common::application.Exported excel list with cases attachments'));

                return response()->json(['download_token' => $token,'status' => true ]);
            }
            return response()->json(['status' => false]);
        }
        return response()->json($result);
    }

    /************ GET CASE Form *************/
    public function word(Request $request){

        $user = \Auth::user();
        $item =CaseModel::fetch(array(
            'action' => 'show',
            'category_type'      => $this->type,
            'category'      => $request->get('category'),
            'full_name'     => $request->get('full_name'),
            'family_member'     => $request->get('family_member'),
            'person'        => $request->get('person'),
            'persons_i18n'  => $request->get('persons_i18n'),
            'contacts'      => $request->get('contacts'),
            'education'      => $request->get('education'),
            'islamic'      => $request->get('islamic'),
            'health'      => $request->get('health'),
            'organization_name'          =>true,
            'work'          => $request->get('work'),
            'residence'     => $request->get('residence'),
            'case_needs'    => $request->get('case_needs'),
            'default_bank'         => $request->get('default_bank'),
            'banks'         => $request->get('banks'),
            'home_indoor'   => $request->get('home_indoor'),
            'reconstructions' => $request->get('reconstructions'),
            'financial_aid_source'     => $request->get('financial_aid_source'),
            'non_financial_aid_source' => $request->get('non_financial_aid_source'),
            'persons_properties' => $request->get('persons_properties'),
            'persons_documents'  => $request->get('persons_documents'),
            'case_sponsorships'  => $request->get('case_sponsorships'),
            'case_payments'      => $request->get('case_payments'),
            'father_id'     => $request->get('father_id'),
            'mother_id'     => $request->get('mother_id'),
            'guardian_id'   => $request->get('guardian_id'),
            'father'        => $request->get('father'),
            'mother'        => $request->get('mother'),
            'guardian'      => $request->get('guardian'),
            'guardian_kinship'      => $request->get('guardian_kinship'),
            'family_payments'    => $request->get('family_payments'),
            'guardian_payments'  => $request->get('guardian_payments'),
            'visitor_note'  => $request->get('visitor_note'),
            'deserted'  => $request->get('deserted')
        ),$request->get('case_id'));

        $organization_id = $item->organization_id;
        $dirName = base_path('storage/app/templates/CaseForm');
        if (!is_dir($dirName)) {
            @mkdir($dirName, 0777, true);
        }
        $Template=\Common\Model\Template::where(['category_id'=>$item->category_id,'organization_id'=>$user->organization_id])->first();
        if($Template){
            $path = base_path('storage/app/'.$Template->filename);
        }
        else{
            if($this->type == 1){
                $default_template_id='Sponsorship-default-template';
            }else{
                $default_template_id='aid-default-template';
            }

            $default_template=\Setting\Model\Setting::where(['id'=>$default_template_id,'organization_id'=>$user->organization_id])->first();

            if($default_template){
                $path = base_path('storage/app/'.$default_template->value);
            }else{
                $path = base_path('storage/app/templates/'.$default_template_id .'.docx');
            }
        }

        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($path);

        $personalImagePath=PersonDocument::personalImagePath($item->person_id,$this->type);

        if(!is_null($personalImagePath)){
            $templateProcessor->setImg('img', array('src'=> $personalImagePath,'size'=>[200,350]));
        }else{
            $templateProcessor->setImg('img', array('src'=> base_path('storage/app/emptyUser.png'),'size'=>[200,350]));
        }

        $templateProcessor->setValue('date', date('y-m-d'));
        $templateProcessor->setValue('organization_name', $item->organization_name);
        $templateProcessor->setValue('category_name', $item->category_name);

        $templateProcessor->setValue('first_name', $item->first_name);
        $templateProcessor->setValue('second_name', $item->second_name);
        $templateProcessor->setValue('third_name', $item->third_name );
        $templateProcessor->setValue('last_name', $item->last_name );
        $templateProcessor->setValue('name', $item->full_name);
        $templateProcessor->setValue('en_first_name', $item->en_first_name);
        $templateProcessor->setValue('en_second_name', $item->en_second_name);
        $templateProcessor->setValue('en_third_name', $item->en_third_name);
        $templateProcessor->setValue('en_last_name', $item->en_last_name);
        $templateProcessor->setValue('en_name', $item->en_name);
        $templateProcessor->setValue('id_card_number', $item->id_card_number);
        $templateProcessor->setValue('card_type', $item->card_type);
        $templateProcessor->setValue('birthday', $item->birthday);
        $templateProcessor->setValue('age', $item->age);
        $templateProcessor->setValue('gender', $item->gender);
        $templateProcessor->setValue('gender_male', $item->gender_male);
        $templateProcessor->setValue('gender_female', $item->gender_female);
        $templateProcessor->setValue('birth_place', $item->birth_place);
        $templateProcessor->setValue('nationality', $item->nationality);
        $templateProcessor->setValue('refugee', $item->refugee);
        $templateProcessor->setValue('is_refugee', $item->is_refugee);
        $templateProcessor->setValue('per_refugee', $item->per_refugee);
        $templateProcessor->setValue('is_citizen', $item->is_citizen);
        $templateProcessor->setValue('per_citizen', $item->per_citizen);
        $templateProcessor->setValue('unrwa_card_number', $item->unrwa_card_number);
        $templateProcessor->setValue('marital_status_name', $item->marital_status_name);

        $templateProcessor->setValue('deserted', $item->deserted);
        $templateProcessor->setValue('is_deserted', $item->is_deserted);
        $templateProcessor->setValue('not_deserted', $item->not_deserted);

        $templateProcessor->setValue('receivables', $item->receivables);
        $templateProcessor->setValue('is_receivables', $item->is_receivables);
        $templateProcessor->setValue('is_ receivables', $item->not_receivables);
        $templateProcessor->setValue('not_receivables', $item->not_receivables);
        $templateProcessor->setValue('receivables_sk_amount', $item->receivables_sk_amount);

        $templateProcessor->setValue('is_qualified', $item->is_qualified);
        $templateProcessor->setValue('qualified', $item->qualified);
        $templateProcessor->setValue('not_qualified', $item->not_qualified);
        $templateProcessor->setValue('qualified_card_number', $item->qualified_card_number);

        $templateProcessor->setValue('has_commercial_records', $item->has_commercial_records);
        $templateProcessor->setValue('have_commercial_records', $item->have_commercial_records);
        $templateProcessor->setValue('not_have_commercial_records', $item->not_have_commercial_records);
        $templateProcessor->setValue('active_commercial_records', $item->active_commercial_records);
        $templateProcessor->setValue('gov_commercial_records_details', $item->gov_commercial_records_details);

        $templateProcessor->setValue('monthly_income', $item->monthly_income);
        $templateProcessor->setValue('actual_monthly_income', $item->actual_monthly_income);

        $templateProcessor->setValue('address', $item->address);
        $templateProcessor->setValue('country', $item->country);
        $templateProcessor->setValue('governarate', $item->governarate);
        $templateProcessor->setValue('nearLocation', $item->nearLocation);
        $templateProcessor->setValue('mosque_name', $item->mosque_name);

        if($this->type == 1){
            $templateProcessor->setValue('city', $item->city);
        }else{
            $templateProcessor->setValue('region', $item->region_name);
            $templateProcessor->setValue('square', $item->square_name);
        }

        $templateProcessor->setValue('street_address', $item->street_address);

        $templateProcessor->setValue('family_cnt', $item->family_cnt);
        $templateProcessor->setValue('male_live', $item->male_live);
        $templateProcessor->setValue('female_live', $item->female_live);
        $templateProcessor->setValue('spouses', $item->spouses);

        $templateProcessor->setValue('individual_count', $item->individual_count);
        $templateProcessor->setValue('male_brothers', $item->male_brothers);
        $templateProcessor->setValue('female_brothers', $item->female_brothers);
        $templateProcessor->setValue('family_count', $item->family_count);
        $templateProcessor->setValue('male_count', $item->male_count);
        $templateProcessor->setValue('female_count', $item->female_count);

        $templateProcessor->setValue('father_brothers', $item->father_brothers);
        $templateProcessor->setValue('female_father_brothers', $item->female_father_brothers);
        $templateProcessor->setValue('male_father_brothers', $item->male_father_brothers);

        $templateProcessor->setValue('mother_brothers', $item->mother_brothers);
        $templateProcessor->setValue('female_mother_brothers', $item->female_mother_brothers);
        $templateProcessor->setValue('male_mother_brothers', $item->male_mother_brothers);

        $templateProcessor->setValue('brothers', $item->brothers);
        $templateProcessor->setValue('children', $item->children);
        $templateProcessor->setValue('male_children', $item->male_children);
        $templateProcessor->setValue('female_children', $item->female_children);

        $templateProcessor->setValue('phone', $item->phone);
        $templateProcessor->setValue('primary_mobile', $item->primary_mobile);
        $templateProcessor->setValue('wataniya', $item->wataniya);
        $templateProcessor->setValue('secondary_mobile', $item->secondary_mobile );

        $templateProcessor->setValue('working', $item->working);
        $templateProcessor->setValue('works', $item->works);
        $templateProcessor->setValue('is_working', $item->is_working);
        $templateProcessor->setValue('not_working', $item->not_working);
        $templateProcessor->setValue('can_work', $item->can_work);
        $templateProcessor->setValue('can_works', $item->can_works);
        $templateProcessor->setValue('can_working', $item->can_working);
        $templateProcessor->setValue('can_not_working', $item->can_not_working);
        $templateProcessor->setValue('work_job', $item->work_job);
        $templateProcessor->setValue('work_reason', $item->work_reason);
        $templateProcessor->setValue('work_status', $item->work_status);
        $templateProcessor->setValue('work_wage', $item->work_wage);
        $templateProcessor->setValue('work_location', $item->work_location);
        $templateProcessor->setValue('has_other_work_resources', $item->has_other_work_resources);
        $templateProcessor->setValue('has_other_resources', $item->has_other_resources);
        $templateProcessor->setValue('not_has_other_resources', $item->not_has_other_resources);
        $templateProcessor->setValue('gov_work_details', $item->gov_work_details);

        $templateProcessor->setValue('health_status', $item->health_status);
        $templateProcessor->setValue('good_health_status', $item->good_health_status);
        $templateProcessor->setValue('chronic_disease', $item->chronic_disease);
        $templateProcessor->setValue('disabled', $item->disabled);
        $templateProcessor->setValue('diseases_name', $item->diseases_name);
        $templateProcessor->setValue('has_disease', $item->has_disease);
        $templateProcessor->setValue('has_health_insurance', $item->has_health_insurance);
        $templateProcessor->setValue('have_health_insurance', $item->have_health_insurance);
        $templateProcessor->setValue('not_have_health_insurance', $item->not_have_health_insurance);
        $templateProcessor->setValue('health_insurance_number', $item->health_insurance_number);
        $templateProcessor->setValue('health_insurance_type', $item->health_insurance_type);
        $templateProcessor->setValue('has_device', $item->has_device);
        $templateProcessor->setValue('use_device', $item->use_device);
        $templateProcessor->setValue('not_use_device', $item->not_use_device);
        $templateProcessor->setValue('used_device_name', $item->used_device_name);
        $templateProcessor->setValue('health_details', $item->health_details);
        $templateProcessor->setValue('gov_health_details', $item->gov_health_details);
        $templateProcessor->setValue('property_types', $item->property_types);
        $templateProcessor->setValue('rent_value', $item->rent_value);
        $templateProcessor->setValue('roof_materials', $item->roof_materials);
        $templateProcessor->setValue('habitable', $item->habitable);
        $templateProcessor->setValue('house_condition', $item->house_condition);
        $templateProcessor->setValue('residence_condition', $item->residence_condition);
        $templateProcessor->setValue('indoor_condition', $item->indoor_condition);
        $templateProcessor->setValue('rooms', $item->rooms);
        $templateProcessor->setValue('area', $item->area);
        $templateProcessor->setValue('repair_notes', $item->repair_notes);
        $templateProcessor->setValue('need_repair', $item->need_repair);
        $templateProcessor->setValue('repair', $item->repair);
        $templateProcessor->setValue('not_repair', $item->not_repair);

        $templateProcessor->setValue('needs', $item->needs);
        $templateProcessor->setValue('promised', $item->promised);
        $templateProcessor->setValue('if_promised', $item->if_promised);
        $templateProcessor->setValue('was_promised', $item->was_promised);
        $templateProcessor->setValue('not_promised', $item->not_promised);
        $templateProcessor->setValue('reconstructions_organization_name', $item->reconstructions_organization_name);

        $templateProcessor->setValue('visitor', $item->visitor);
        $templateProcessor->setValue('notes', $item->notes);
        $templateProcessor->setValue('visited_at', $item->visited_at);
        $templateProcessor->setValue('visitor_opinion', $item->visitor_opinion);
        $templateProcessor->setValue('visitor_card', $item->visitor_card);
        $templateProcessor->setValue('visitor_evaluation', $item->visitor_evaluation);
        $templateProcessor->setValue('very_bad_condition_evaluation', $item->very_bad_condition_evaluation);
        $templateProcessor->setValue('bad_condition_evaluation', $item->bad_condition_evaluation);
        $templateProcessor->setValue('good_evaluation', $item->good_evaluation);
        $templateProcessor->setValue('very_good_evaluation', $item->very_good_evaluation);
        $templateProcessor->setValue('excellent_evaluation', $item->excellent_evaluation);

        $templateProcessor->setValue('study', $item->study);
        $templateProcessor->setValue('type', $item->type);
        $templateProcessor->setValue('level', $item->level);
        $templateProcessor->setValue('year', $item->year);
        $templateProcessor->setValue('points', $item->points);
        $templateProcessor->setValue('school', $item->school);
        $templateProcessor->setValue('grade', $item->grade);
        $templateProcessor->setValue('authority', $item->authority);
        $templateProcessor->setValue('stage', $item->stage);
        $templateProcessor->setValue('degree', $item->degree);
        $templateProcessor->setValue('specialization', $item->specialization);
        $templateProcessor->setValue('prayer', $item->prayer);
        $templateProcessor->setValue('prayer_reason', $item->prayer_reason);
        $templateProcessor->setValue('quran_reason', $item->quran_reason);
        $templateProcessor->setValue('quran_parts', $item->quran_parts);
        $templateProcessor->setValue('quran_chapters', $item->quran_chapters);
        $templateProcessor->setValue('quran_center', $item->quran_center);
        $templateProcessor->setValue('save_quran', $item->save_quran);
        $templateProcessor->setValue('visitor_note', $item->visitor_note);
        $templateProcessor->setValue('guardian_kinship', $item->guardian_kinship);
        $templateProcessor->setValue('guardian_is', $item->guardian_is);
        $templateProcessor->setValue('mother_is_guardian', $item->mother_is_guardian);

        if (sizeof($item->banks)!=0){
            try{
                $templateProcessor->cloneRow('r_bank_name', sizeof($item->banks));
                $zxs=1;
                foreach($item->banks as $rec) {
                    $templateProcessor->setValue('r_bank_name#' . $zxs, $rec->bank_name);
                    $templateProcessor->setValue('r_branch_name#' . $zxs, $rec->branch);
                    $templateProcessor->setValue('r_account_owner#' . $zxs, $rec->account_owner);
                    $templateProcessor->setValue('r_account_number#' . $zxs, $rec->account_number);
                    if($rec->check == true){
                        $templateProcessor->setValue('r_default#' . $zxs, trans('aid::application.yes') );
                    }else{
                        $templateProcessor->setValue('r_default#' . $zxs, trans('aid::application.no') );
                    }
                    $zxs++;
                }
            }catch (\Exception $e){}
        }else{
            try{
                $templateProcessor->cloneRow('r_bank_name', 1);
                $templateProcessor->setValue('r_bank_name#' . 1, ' ');
                $templateProcessor->setValue('r_branch_name#' . 1, ' ');
                $templateProcessor->setValue('r_account_owner#' . 1, ' ');
                $templateProcessor->setValue('r_account_number#' . 1,' ');
                $templateProcessor->setValue('r_default#' . 1, ' ');
            }catch (\Exception $e){  }
        }

        if($this->type == 1){
            $setting_id = 'sponsorship_custom_form';
        }else{
            $setting_id = 'aid_custom_form';
        }

        $CaseCustomData =FormsCasesData::getCaseCustomData($item->case_id,$item->category_id,$item->organization_id,$setting_id);

        if (sizeof($CaseCustomData)!=0){
            try{
                $templateProcessor->cloneRow('row_custom_name', sizeof($CaseCustomData));
                $z=1;
                foreach($CaseCustomData as $cValue) {
                    $templateProcessor->setValue('row_custom_name#' . $z, $cValue['label']);
                    $templateProcessor->setValue('row_custom_value#' . $z, $cValue['value']);
                    $z++;
                }
            }catch (\Exception $e){

            }
        }
        else{
            try{
                $templateProcessor->cloneRow('row_custom_name',1);
                $templateProcessor->setValue('row_custom_name#' . 1, ' ');
                $templateProcessor->setValue('row_custom_value#' . 1, ' ');
            }catch (\Exception $e){  }
        }

        if (sizeof($item->home_indoor)!=0){
            try{
                $templateProcessor->cloneRow('row_es_name', sizeof($item->home_indoor));
                $z=1;
                foreach($item->home_indoor as $record) {
                    $templateProcessor->setValue('row_es_name#' . $z, $record->name);
                    $templateProcessor->setValue('row_es_if_exist#' . $z, $record->exist);
                    $templateProcessor->setValue('row_es_needs#' . $z, $record->needs);
                    $templateProcessor->setValue('row_es_condition#' . $z, $record->essentials_condition);
                    $templateProcessor->setValue('row_es_not_exist#' . $z, $record->not_exist);
                    $templateProcessor->setValue('row_es_very_bad_condition#' . $z, $record->very_bad_condition);
                    $templateProcessor->setValue('row_es_bad_condition#' . $z, $record->bad_condition);
                    $templateProcessor->setValue('row_es_good#' . $z, $record->good);
                    $templateProcessor->setValue('row_es_very_good#' . $z, $record->very_good);
                    $templateProcessor->setValue('row_es_excellent#' . $z, $record->excellent);
                    $z++;
                }
            }catch (\Exception $e){  }
        }
        else{
            try{
                $templateProcessor->cloneRow('row_es_name', 1);
                $templateProcessor->setValue('row_es_name#' . 1, ' ');
                $templateProcessor->setValue('row_es_if_exist#' . 1, ' ');
                $templateProcessor->setValue('row_es_needs#' . 1, ' ');
                $templateProcessor->setValue('row_es_condition#' . 1, ' ');
                $templateProcessor->setValue('row_es_not_exist#' . 1, ' ');
                $templateProcessor->setValue('row_es_very_bad_condition#' . 1, ' ');
                $templateProcessor->setValue('row_es_bad_condition#' . 1, ' ');
                $templateProcessor->setValue('row_es_good#' . 1, ' ');
                $templateProcessor->setValue('row_es_very_good#' . 1, ' ');
                $templateProcessor->setValue('row_es_excellent#' . 1, ' ');

            }catch (\Exception $e){  }
        }
        if (sizeof($item->persons_properties)!=0){
            try{
                $templateProcessor->cloneRow('row_pro_name', sizeof($item->persons_properties));
                $z2=1;
                foreach($item->persons_properties as $sub_record) {
                    $templateProcessor->setValue('row_pro_name#' . $z2, $sub_record->name);
                    $templateProcessor->setValue('row_pro_if_exist#' . $z2, $sub_record->has_property);
                    $templateProcessor->setValue('row_pro_exist#' . $z2, $sub_record->exist);
                    $templateProcessor->setValue('row_pro_not_exist#' . $z2, $sub_record->not_exist);
                    $templateProcessor->setValue('row_pro_count#' . $z2, $sub_record->quantity);
                    $z2++;
                }
            }catch (\Exception $e){  }
        }else{
            try{
                $templateProcessor->cloneRow('row_pro_name#', 1);
                $templateProcessor->setValue('row_pro_exist#' . 1, ' ');
                $templateProcessor->setValue('row_pro_not_exist#' . 1, ' ');
                $templateProcessor->setValue('row_pro_name#' . 1, ' ');
                $templateProcessor->setValue('row_pro_if_exist#' . 1, ' ');
                $templateProcessor->setValue('row_pro_count#' . 1, ' ');
            }catch (\Exception $e){  }
        }
        if (sizeof($item->financial_aid_source)!=0){
            try{
                $templateProcessor->cloneRow('row_fin_name', sizeof($item->financial_aid_source));
                $z3=1;
                foreach($item->financial_aid_source as $record) {
                    $templateProcessor->setValue('row_fin_name#' . $z3, $record->name);
                    $templateProcessor->setValue('row_fin_if_exist#' . $z3, $record->aid_take);
                    $templateProcessor->setValue('row_fin_take#' . $z3, $record->take);
                    $templateProcessor->setValue('row_fin_not_take#' . $z3, $record->not_take);
                    $templateProcessor->setValue('row_fin_count#' . $z3, $record->aid_value);
                    $templateProcessor->setValue('row_fin_currency#' . $z3, $record->currency_name);
                    $z3++;
                }
            }catch (\Exception $e){  }
        }else{
            try{
                $templateProcessor->cloneRow('row_fin_name', 1);
                $templateProcessor->setValue('row_fin_name#' . 1, ' ');
                $templateProcessor->setValue('row_fin_if_exist#' . 1, ' ');
                $templateProcessor->setValue('row_fin_count#' . 1, ' ');
                $templateProcessor->setValue('row_fin_take#' . 1, ' ');
                $templateProcessor->setValue('row_fin_not_take#' . 1, ' ');
                $templateProcessor->setValue('row_fin_currency#' . 1, ' ');
            }catch (\Exception $e){  }
        }
        if (sizeof($item->non_financial_aid_source)!=0){
            try{
                $templateProcessor->cloneRow('row_nonfin_name', sizeof($item->non_financial_aid_source));
                $z3=1;
                foreach($item->non_financial_aid_source as $record) {
                    $templateProcessor->setValue('row_nonfin_name#' . $z3, $record->name);
                    $templateProcessor->setValue('row_nonfin_if_exist#' . $z3, $record->aid_take);
                    $templateProcessor->setValue('row_nonfin_not_take#' . $z3, $record->not_take);
                    $templateProcessor->setValue('row_nonfin_take#' . $z3, $record->take);
                    $templateProcessor->setValue('row_nonfin_count#' . $z3, $record->aid_value);
                    $z3++;
                }
            }catch (\Exception $e){  }
        } else{
            try{
                $templateProcessor->cloneRow('row_nonfin_name', 1);
                $templateProcessor->setValue('row_nonfin_name#' . 1, ' ');
                $templateProcessor->setValue('row_nonfin_if_exist#' . 1, ' ');
                $templateProcessor->setValue('row_nonfin_take#' . 1, ' ');
                $templateProcessor->setValue('row_nonfin_not_take#' . 1, ' ');
                $templateProcessor->setValue('row_nonfin_count#' . 1, ' ');
            }catch (\Exception $e){  }
        }
        if (sizeof($item->persons_documents)!=0){
            try{
                $templateProcessor->cloneRow('row_doc_name', sizeof($item->persons_documents));
                $r2=1;
                foreach($item->persons_documents as $record) {
                    $templateProcessor->setValue('row_doc_name#' . $r2, $record->name);
                    $templateProcessor->setImg('row_doc_img#'. $r2, array('src'=> base_path('storage/app/').$record->filepath,'swh'=>'650'));
                    $r2++;
                }
            }catch (\Exception $e){  }
        } else{
            try{
                $templateProcessor->cloneRow('row_doc_name', 1);
                $templateProcessor->setValue('row_doc_name#' . 1, ' ');
                $templateProcessor->setValue('row_doc_img#' . 1, ' ');
            }catch (\Exception $e){  }
        }
        if (sizeof($item->family_member)!=0){
            try{
                $templateProcessor->cloneRow('fm_nam', sizeof($item->family_member));
                $z=1;
                foreach($item->family_member as $record) {
                    $templateProcessor->setValue('fm_inc#' . $z,$z);
                    $templateProcessor->setValue('fm_nam#' . $z, $record->name);
                    $templateProcessor->setValue('fm_bd#' . $z, $record->birthday);
                    $templateProcessor->setValue('fm_gen#' . $z, $record->gender);
                    $templateProcessor->setValue('fm_age#' . $z, $record->age);
                    $templateProcessor->setValue('fm_idc#' . $z, $record->id_card_number);
                    $templateProcessor->setValue('fm_ms#' . $z, $record->marital_status);
                    $templateProcessor->setValue('fm_kin#' . $z, $record->kinship_name);
                    $templateProcessor->setValue('fm_stg#' . $z, $record->stage);
                    $templateProcessor->setValue('fm_lvl#' . $z, $record->level);
                    $templateProcessor->setValue('fm_yer#' . $z, $record->year);
                    $templateProcessor->setValue('fm_grd#' . $z, $record->grade);
                    $templateProcessor->setValue('fm_scl#' . $z, $record->school);
                    $templateProcessor->setValue('fm_hs#' . $z, $record->health_status);
                    $templateProcessor->setValue('fm_disnm#' . $z, $record->diseases_name);
                    $templateProcessor->setValue('fm_working#' . $z, $record->working);
                    $templateProcessor->setValue('fm_job#' . $z, $record->work_job);
                    $z++;
                }
            }catch (\Exception $e){  }
        }
        else{
            try{
                $templateProcessor->cloneRow('fm_nam',1);
                $templateProcessor->setValue('fm_inc#' . 1,1);
                $templateProcessor->setValue('fm_scl#' . 1, ' ');
                $templateProcessor->setValue('fm_nam#' . 1, ' ');
                $templateProcessor->setValue('fm_bd#' . 1, ' ');
                $templateProcessor->setValue('fm_gen#' . 1, ' ');
                $templateProcessor->setValue('fm_age#' . 1, ' ');
                $templateProcessor->setValue('fm_idc#' . 1,' ');
                $templateProcessor->setValue('fm_ms#' . 1, ' ');
                $templateProcessor->setValue('fm_kin#' . 1, ' ');
                $templateProcessor->setValue('fm_stg#' . 1, ' ');
                $templateProcessor->setValue('fm_lvl#' . 1, ' ');
                $templateProcessor->setValue('fm_yer#' . 1, ' ');
                $templateProcessor->setValue('fm_grd#' . 1, ' ');
                $templateProcessor->setValue('fm_hs#' . 1, ' ');
                $templateProcessor->setValue('fm_disnm#' . 1, ' ');
                $templateProcessor->setValue('fm_working#' . 1, ' ');
                $templateProcessor->setValue('fm_job#' . 1, ' ');
            }catch (\Exception $e){  }
        }
        if($item->category_type == 2)
            $templateProcessor->setValue('deserted', $item->deserted);

        if($request->get('parent_detail')){
            $get=array('action' => 'show','id' => $item->father_id,
                'person'  => true, 'persons_i18n'  =>  true,
                'health'  =>  true,  'education'  =>  true, 'work'  =>  true);
            if($item->father_id != null){
                $get['id']=$item->father_id;
                $father =Person::fetch($get);
                $templateProcessor->setValue('father_first_name', $father->first_name);
                $templateProcessor->setValue('father_second_name', $father->second_name);
                $templateProcessor->setValue('father_third_name', $father->third_name );
                $templateProcessor->setValue('father_last_name', $father->last_name );
                $templateProcessor->setValue('father_name', $father->full_name);
                $templateProcessor->setValue('father_spouses', $father->spouses);
                $templateProcessor->setValue('father_id_card_number', $father->id_card_number);
                $templateProcessor->setValue('father_card_type', $father->card_type);
                $templateProcessor->setValue('father_birthday', $father->birthday);
                $templateProcessor->setValue('father_en_first_name', $father->en_first_name);
                $templateProcessor->setValue('father_en_second_name', $father->en_second_name);
                $templateProcessor->setValue('father_en_third_name', $father->en_third_name);
                $templateProcessor->setValue('father_en_last_name', $father->en_last_name);
                $templateProcessor->setValue('father_en_name', $father->en_name);
                $templateProcessor->setValue('father_health_status', $father->health_status);
                $templateProcessor->setValue('father_health_details', $father->health_details);
                $templateProcessor->setValue('father_diseases_name', $father->diseases_name);
                $templateProcessor->setValue('father_specialization', $father->specialization);
                $templateProcessor->setValue('father_degree', $father->degree);
                $templateProcessor->setValue('father_alive', $father->alive);
                $templateProcessor->setValue('father_is_alive', $father->is_alive);
                $templateProcessor->setValue('father_death_date', $father->death_date);
                $templateProcessor->setValue('father_death_cause_name', $father->death_cause_name);
                $templateProcessor->setValue('father_working', $father->working);
                $templateProcessor->setValue('father_work_job', $father->work_job);
                $templateProcessor->setValue('father_work_location', $father->work_location);
                $templateProcessor->setValue('father_monthly_income', $father->monthly_income);
                $templateProcessor->setValue('father_gender_male', $father->gender_male);
                $templateProcessor->setValue('father_gender_female', $father->gender_female);
                $templateProcessor->setValue('father_is_refugee', $father->is_refugee);
                $templateProcessor->setValue('father_per_refugee', $father->per_refugee);
                $templateProcessor->setValue('father_per_citizen', $father->per_citizen);
                $templateProcessor->setValue('father_father_brothers', $father->father_brothers);
                $templateProcessor->setValue('father_mother_brothers', $father->mother_brothers);
                $templateProcessor->setValue('father_brothers', $father->brothers);
                $templateProcessor->setValue('father_children', $father->children);
                $templateProcessor->setValue('father_male_children', $father->male_children);
                $templateProcessor->setValue('father_female_children', $father->female_children);
                $templateProcessor->setValue('father_is_working', $father->is_working);
                $templateProcessor->setValue('father_not_working', $father->not_working);
                $templateProcessor->setValue('father_can_working', $father->can_working);
                $templateProcessor->setValue('father_can_not_working', $father->can_not_working);
                $templateProcessor->setValue('father_health_status', $father->health_status);
                $templateProcessor->setValue('father_good_health_status', $father->good_health_status);
                $templateProcessor->setValue('father_chronic_disease', $father->chronic_disease);
                $templateProcessor->setValue('father_disabled', $father->disabled);
            } else{
                $templateProcessor->setValue('father_gender_male', ' ');
                $templateProcessor->setValue('father_gender_female', ' ');
                $templateProcessor->setValue('father_is_refugee', ' ');
                $templateProcessor->setValue('father_per_refugee', ' ');
                $templateProcessor->setValue('father_per_citizen', ' ');
                $templateProcessor->setValue('father_father_brothers', ' ');
                $templateProcessor->setValue('father_mother_brothers', ' ');
                $templateProcessor->setValue('father_brothers', ' ');
                $templateProcessor->setValue('father_children', ' ');
                $templateProcessor->setValue('father_male_children', ' ');
                $templateProcessor->setValue('father_female_children', ' ');
                $templateProcessor->setValue('father_is_working', ' ');
                $templateProcessor->setValue('father_not_working', ' ');
                $templateProcessor->setValue('father_can_working', ' ');
                $templateProcessor->setValue('father_can_not_working', ' ');
                $templateProcessor->setValue('father_health_status', ' ');
                $templateProcessor->setValue('father_good_health_status', ' ');
                $templateProcessor->setValue('father_chronic_disease', ' ');
                $templateProcessor->setValue('father_disabled', ' ');
                $templateProcessor->setValue('father_first_name', ' ');
                $templateProcessor->setValue('father_second_name', ' ');
                $templateProcessor->setValue('father_third_name', ' ');
                $templateProcessor->setValue('father_last_name', ' ');
                $templateProcessor->setValue('father_en_first_name', ' ');
                $templateProcessor->setValue('father_en_second_name', ' ');
                $templateProcessor->setValue('father_en_third_name', ' ');
                $templateProcessor->setValue('father_en_last_name', ' ');
                $templateProcessor->setValue('father_en_name', ' ');
                $templateProcessor->setValue('father_name', ' ');
                $templateProcessor->setValue('father_spouses', ' ');
                $templateProcessor->setValue('father_id_card_number',' ');
                $templateProcessor->setValue('father_card_type',' ');
                $templateProcessor->setValue('father_birthday', ' ');
                $templateProcessor->setValue('father_en_first_name',' ');
                $templateProcessor->setValue('father_en_second_name', ' ');
                $templateProcessor->setValue('father_en_third_name', ' ');
                $templateProcessor->setValue('father_en_name', ' ');
                $templateProcessor->setValue('father_health_status', ' ');
                $templateProcessor->setValue('father_health_details',' ');
                $templateProcessor->setValue('father_diseases_name', ' ');
                $templateProcessor->setValue('father_specialization', ' ');
                $templateProcessor->setValue('father_degree', ' ');
                $templateProcessor->setValue('father_alive', ' ');
                $templateProcessor->setValue('father_is_alive', ' ');
                $templateProcessor->setValue('father_death_date', ' ');
                $templateProcessor->setValue('father_death_cause_name', ' ');
                $templateProcessor->setValue('father_working', ' ');
                $templateProcessor->setValue('father_work_job', ' ');
                $templateProcessor->setValue('father_work_location', ' ');
                $templateProcessor->setValue('father_monthly_income', ' ');
            }
            if($item->mother_id != null){
                $get['id']=$item->mother_id;
                $mother =Person::fetch($get);
                $templateProcessor->setValue('mother_prev_family_name', $mother->prev_family_name);
                $templateProcessor->setValue('mother_first_name', $mother->first_name);
                $templateProcessor->setValue('mother_second_name', $mother->second_name);
                $templateProcessor->setValue('mother_third_name', $mother->third_name);
                $templateProcessor->setValue('mother_last_name', $mother->last_name);
                $templateProcessor->setValue('mother_name', $mother->full_name);
                $templateProcessor->setValue('mother_id_card_number', $mother->id_card_number);
                $templateProcessor->setValue('mother_card_type', $mother->card_type);
                $templateProcessor->setValue('mother_birthday', $mother->birthday);
                $templateProcessor->setValue('mother_marital_status_name', $mother->marital_status_name);
                $templateProcessor->setValue('mother_nationality', $mother->nationality);
                $templateProcessor->setValue('mother_en_first_name', $mother->en_first_name);
                $templateProcessor->setValue('mother_en_second_name', $mother->en_second_name);
                $templateProcessor->setValue('mother_en_third_name', $mother->en_third_name);
                $templateProcessor->setValue('mother_en_last_name', $mother->en_last_name);
                $templateProcessor->setValue('mother_en_name', $mother->en_name);
                $templateProcessor->setValue('mother_health_status', $mother->health_status);
                $templateProcessor->setValue('mother_health_details', $mother->health_details);
                $templateProcessor->setValue('mother_diseases_name', $mother->diseases_name);
                $templateProcessor->setValue('mother_specialization', $mother->specialization);
                $templateProcessor->setValue('mother_stage', $mother->stage);
                $templateProcessor->setValue('mother_alive', $mother->alive);
                $templateProcessor->setValue('mother_is_alive', $mother->is_alive);
                $templateProcessor->setValue('mother_death_date', $mother->death_date);
                $templateProcessor->setValue('mother_death_cause_name', $mother->death_cause_name);
                $templateProcessor->setValue('mother_working', $mother->working);
                $templateProcessor->setValue('mother_work_job', $mother->work_job);
                $templateProcessor->setValue('mother_work_location', $mother->work_location);
                $templateProcessor->setValue('mother_monthly_income', $mother->monthly_income);
                $templateProcessor->setValue('mother_gender_male', $mother->gender_male);
                $templateProcessor->setValue('mother_gender_female', $mother->gender_female);
                $templateProcessor->setValue('mother_is_refugee', $mother->is_refugee);
                $templateProcessor->setValue('mother_per_refugee', $mother->per_refugee);
                $templateProcessor->setValue('mother_per_citizen', $mother->per_citizen);
                $templateProcessor->setValue('mother_father_brothers', $mother->father_brothers);
                $templateProcessor->setValue('mother_mother_brothers', $mother->mother_brothers);
                $templateProcessor->setValue('mother_brothers', $mother->brothers);
                $templateProcessor->setValue('mother_children', $mother->children);
                $templateProcessor->setValue('mother_male_children', $mother->male_children);
                $templateProcessor->setValue('mother_female_children', $mother->female_children);
                $templateProcessor->setValue('mother_is_working', $mother->is_working);
                $templateProcessor->setValue('mother_not_working', $mother->not_working);
                $templateProcessor->setValue('mother_can_working', $mother->can_working);
                $templateProcessor->setValue('mother_can_not_working', $mother->can_not_working);
                $templateProcessor->setValue('mother_health_status', $mother->health_status);
                $templateProcessor->setValue('mother_good_health_status', $mother->good_health_status);
                $templateProcessor->setValue('mother_chronic_disease', $mother->chronic_disease);
                $templateProcessor->setValue('mother_disabled', $mother->disabled);
            } else{
                $templateProcessor->setValue('mother_gender_male', ' ');
                $templateProcessor->setValue('mother_gender_female', ' ');
                $templateProcessor->setValue('mother_is_refugee', ' ');
                $templateProcessor->setValue('mother_per_refugee', ' ');
                $templateProcessor->setValue('mother_per_citizen', ' ');
                $templateProcessor->setValue('mother_father_brothers', ' ');
                $templateProcessor->setValue('mother_mother_brothers', ' ');
                $templateProcessor->setValue('mother_brothers', ' ');
                $templateProcessor->setValue('mother_children', ' ');
                $templateProcessor->setValue('mother_male_children', ' ');
                $templateProcessor->setValue('mother_female_children', ' ');
                $templateProcessor->setValue('mother_is_working', ' ');
                $templateProcessor->setValue('mother_not_working', ' ');
                $templateProcessor->setValue('mother_can_working', ' ');
                $templateProcessor->setValue('mother_can_not_working', ' ');
                $templateProcessor->setValue('mother_health_status', ' ');
                $templateProcessor->setValue('mother_good_health_status', ' ');
                $templateProcessor->setValue('mother_chronic_disease', ' ');
                $templateProcessor->setValue('mother_disabled', ' ');
                $templateProcessor->setValue('mother_prev_family_name',' ');
                $templateProcessor->setValue('mother_first_name', ' ');
                $templateProcessor->setValue('mother_second_name', ' ');
                $templateProcessor->setValue('mother_third_name', ' ');
                $templateProcessor->setValue('mother_last_name',' ');
                $templateProcessor->setValue('mother_name', ' ');
                $templateProcessor->setValue('mother_id_card_number', ' ');
                $templateProcessor->setValue('mother_card_type', ' ');
                $templateProcessor->setValue('mother_birthday', ' ');
                $templateProcessor->setValue('mother_marital_status_name', ' ');
                $templateProcessor->setValue('mother_nationality', ' ');
                $templateProcessor->setValue('mother_en_first_name', ' ');
                $templateProcessor->setValue('mother_en_second_name', ' ');
                $templateProcessor->setValue('mother_en_third_name', ' ');
                $templateProcessor->setValue('mother_en_last_name', ' ');
                $templateProcessor->setValue('mother_en_name', ' ');
                $templateProcessor->setValue('mother_health_status', ' ');
                $templateProcessor->setValue('mother_health_details',' ');
                $templateProcessor->setValue('mother_diseases_name', ' ');
                $templateProcessor->setValue('mother_specialization',' ');
                $templateProcessor->setValue('mother_stage', ' ');
                $templateProcessor->setValue('mother_alive', ' ');
                $templateProcessor->setValue('mother_is_alive', ' ');
                $templateProcessor->setValue('mother_death_date', ' ');
                $templateProcessor->setValue('mother_death_cause_name', ' ');
                $templateProcessor->setValue('mother_working', ' ');
                $templateProcessor->setValue('mother_work_job', ' ');
                $templateProcessor->setValue('mother_work_location', ' ');
                $templateProcessor->setValue('mother_monthly_income', ' ');
            }
        }
        if($request->get('guardian_detail')){
            if($item->guardian_id != null){
                $guardian =Person::fetch(array('action'=>'show', 'id' => $item->guardian_id,'person' =>true, 'persons_i18n' => true, 'contacts' => true,  'education' => true,
                    'work' => true, 'residence' => true, 'banks' => true));

                $templateProcessor->setValue('guardian_first_name', $guardian->first_name);
                $templateProcessor->setValue('guardian_age', $guardian->age );
                $templateProcessor->setValue('guardian_second_name', $guardian->second_name);
                $templateProcessor->setValue('guardian_third_name', $guardian->third_name );
                $templateProcessor->setValue('guardian_last_name', $guardian->last_name );
                $templateProcessor->setValue('guardian_name', $guardian->full_name);
                $templateProcessor->setValue('guardian_id_card_number', $guardian->id_card_number);
                $templateProcessor->setValue('guardian_card_type', $guardian->card_type);
                $templateProcessor->setValue('guardian_birthday', $guardian->birthday);
                $templateProcessor->setValue('guardian_marital_status_name', $guardian->marital_status_name);
                $templateProcessor->setValue('guardian_birth_place', $guardian->birth_place);
                $templateProcessor->setValue('guardian_nationality', $guardian->nationality);
                $templateProcessor->setValue('guardian_country', $guardian->country);
                $templateProcessor->setValue('guardian_governarate', $guardian->governarate);
                $templateProcessor->setValue('guardian_city', $guardian->city);
                $templateProcessor->setValue('guardian_nearLocation', $guardian->nearLocation);
                $templateProcessor->setValue('guardian_mosque_name', $guardian->mosque_name);
                $templateProcessor->setValue('guardian_street_address', $guardian->street_address);
                $templateProcessor->setValue('guardian_address', $guardian->address);
                $templateProcessor->setValue('guardian_phone', $guardian->phone);
                $templateProcessor->setValue('guardian_primary_mobile', $guardian->primary_mobile);
                $templateProcessor->setValue('guardian_wataniya', $guardian->wataniya);
                $templateProcessor->setValue('guardian_secondery_mobile', $guardian->secondary_mobile );
                $templateProcessor->setValue('guardian_en_first_name', $guardian->en_first_name);
                $templateProcessor->setValue('guardian_en_second_name', $guardian->en_second_name);
                $templateProcessor->setValue('guardian_en_third_name', $guardian->en_third_name);
                $templateProcessor->setValue('guardian_en_last_name', $guardian->en_last_name);
                $templateProcessor->setValue('guardian_en_name', $guardian->en_name);
                $templateProcessor->setValue('guardian_property_types', $guardian->property_types);
                $templateProcessor->setValue('guardian_roof_materials', $guardian->roof_materials);
                $templateProcessor->setValue('guardian_area', $guardian->area);
                $templateProcessor->setValue('guardian_rooms', $guardian->rooms);
                $templateProcessor->setValue('guardian_residence_condition', $guardian->residence_condition);
                $templateProcessor->setValue('guardian_indoor_condition', $guardian->indoor_condition);
                $templateProcessor->setValue('guardian_stage', $guardian->stage);
                $templateProcessor->setValue('guardian_working', $guardian->working);
                $templateProcessor->setValue('guardian_work_job', $guardian->work_job);
                $templateProcessor->setValue('guardian_work_location', $guardian->work_location);
                $templateProcessor->setValue('guardian_monthly_income', $guardian->monthly_income);
                $templateProcessor->setValue('guardian_gender_male', $guardian->gender_male);
                $templateProcessor->setValue('guardian_gender_female', $guardian->gender_female);
                $templateProcessor->setValue('guardian_is_refugee', $guardian->is_refugee);
                $templateProcessor->setValue('guardian_per_refugee', $guardian->per_refugee);
                $templateProcessor->setValue('guardian_per_citizen', $guardian->per_citizen);
                $templateProcessor->setValue('guardian_father_brothers', $guardian->father_brothers);
                $templateProcessor->setValue('guardian_mother_brothers', $guardian->mother_brothers);
                $templateProcessor->setValue('guardian_brothers', $guardian->brothers);
                $templateProcessor->setValue('guardian_children', $guardian->children);
                $templateProcessor->setValue('guardian_male_children', $guardian->male_children);
                $templateProcessor->setValue('guardian_female_children', $guardian->female_children);
                $templateProcessor->setValue('guardian_is_working', $guardian->is_working);
                $templateProcessor->setValue('guardian_not_working', $guardian->not_working);
                $templateProcessor->setValue('guardian_can_working', $guardian->can_working);
                $templateProcessor->setValue('guardian_can_not_working', $guardian->can_not_working);
                $templateProcessor->setValue('guardian_health_status', $guardian->health_status);
                $templateProcessor->setValue('guardian_good_health_status', $guardian->good_health_status);
                $templateProcessor->setValue('guardian_chronic_disease', $guardian->chronic_disease);
                $templateProcessor->setValue('guardian_disabled', $guardian->disabled);
            }
            else{

                $templateProcessor->setValue('guardian_gender_male', ' ');
                $templateProcessor->setValue('guardian_gender_female', ' ');
                $templateProcessor->setValue('guardian_age',' ');
                $templateProcessor->setValue('guardian_is_refugee', ' ');
                $templateProcessor->setValue('guardian_per_refugee', ' ');
                $templateProcessor->setValue('guardian_per_citizen', ' ');
                $templateProcessor->setValue('guardian_father_brothers', ' ');
                $templateProcessor->setValue('guardian_mother_brothers', ' ');
                $templateProcessor->setValue('guardian_brothers', ' ');
                $templateProcessor->setValue('guardian_children', ' ');
                $templateProcessor->setValue('guardian_male_children', ' ');
                $templateProcessor->setValue('guardian_female_children', ' ');
                $templateProcessor->setValue('guardian_is_working', ' ');
                $templateProcessor->setValue('guardian_not_working', ' ');
                $templateProcessor->setValue('guardian_can_working', ' ');
                $templateProcessor->setValue('guardian_can_not_working', ' ');
                $templateProcessor->setValue('guardian_health_status', ' ');
                $templateProcessor->setValue('guardian_good_health_status', ' ');
                $templateProcessor->setValue('guardian_chronic_disease', ' ');
                $templateProcessor->setValue('guardian_disabled', ' ');
                $templateProcessor->setValue('guardian_first_name',' ');
                $templateProcessor->setValue('guardian_second_name', ' ');
                $templateProcessor->setValue('guardian_third_name', ' ');
                $templateProcessor->setValue('guardian_last_name', ' ');
                $templateProcessor->setValue('guardian_name', ' ');
                $templateProcessor->setValue('guardian_id_card_number', ' ');
                $templateProcessor->setValue('guardian_card_type', ' ');
                $templateProcessor->setValue('guardian_birthday', ' ');
                $templateProcessor->setValue('guardian_marital_status_name',' ');
                $templateProcessor->setValue('guardian_birth_place', ' ');
                $templateProcessor->setValue('guardian_nationality', ' ');
                $templateProcessor->setValue('guardian_country', ' ');
                $templateProcessor->setValue('guardian_governarate', ' ');
                $templateProcessor->setValue('guardian_city', ' ');
                $templateProcessor->setValue('guardian_nearLocation', ' ');
                $templateProcessor->setValue('guardian_mosque_name', ' ');
                $templateProcessor->setValue('guardian_street_address', ' ');
                $templateProcessor->setValue('guardian_address', ' ');
                $templateProcessor->setValue('guardian_phone', ' ');
                $templateProcessor->setValue('guardian_primary_mobile', ' ');
                $templateProcessor->setValue('guardian_wataniya', ' ');
                $templateProcessor->setValue('guardian_secondery_mobile', ' ');
                $templateProcessor->setValue('guardian_en_first_name', ' ');
                $templateProcessor->setValue('guardian_en_second_name', ' ');
                $templateProcessor->setValue('guardian_en_third_name', ' ');
                $templateProcessor->setValue('guardian_en_last_name', ' ');
                $templateProcessor->setValue('guardian_en_name', ' ');
                $templateProcessor->setValue('guardian_property_types', ' ');
                $templateProcessor->setValue('guardian_roof_materials', ' ');
                $templateProcessor->setValue('guardian_area', ' ');
                $templateProcessor->setValue('guardian_rooms', ' ');
                $templateProcessor->setValue('guardian_residence_condition', ' ');
                $templateProcessor->setValue('guardian_indoor_condition', ' ');
                $templateProcessor->setValue('guardian_stage',' ');
                $templateProcessor->setValue('guardian_working', ' ');
                $templateProcessor->setValue('guardian_work_job', ' ');
                $templateProcessor->setValue('guardian_work_location', ' ');
                $templateProcessor->setValue('guardian_monthly_income', ' ');
            }
        }

        $filename = $dirName.'/'.$item->category_name.' '.$item->full_name;
        $templateProcessor->saveAs($filename .".docx");
        $token = md5(uniqid());
        $zipFileName = storage_path('tmp/export_' . $token . '.zip');

        $zip = new \ZipArchive;
        if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
        {
            foreach ( glob( $dirName . '/*' ) as $fileName )
            {
                $file = basename( $fileName );
                $zip->addFile( $fileName, $file );
            }

            $zip->close();
            array_map('unlink', glob("$dirName/*.*"));
            rmdir($dirName);
        }

        return response()->json(['download_token' => $token]);

    }

    /************ GET CASE ATTACHMENTS *************/
    public function exportCaseDocument($id,Request $request)
    {
        $record =CaseModel::fetch(array(
            'action' => 'show',
            'category_type' => $this->type,
            'category'      => true,
            'full_name'      => true,
            'father_id'      => true,
            'mother_id'      => true,
            'guardian_id'      => true,
            'persons_documents'  =>true
        ),$id);

        if(sizeof($record->persons_documents) != 0){

            $dirName = base_path('storage/app/documents/Attachments');
            if (!is_dir($dirName)) {
                @mkdir($dirName, 0777, true);
            }
            $Subdir = base_path('storage/app/documents/Attachments/' .$record->full_name);
            if (!is_dir($Subdir)) {
                @mkdir($Subdir, 0777, true);
            }

            foreach ($record->persons_documents as $k => $v) {
                $extention = explode(".", $v->filepath);
                copy(base_path('storage/app/') . $v->filepath, $Subdir . '/' . $v->name . '.' . end($extention));
            }


            $token = md5(uniqid());
            $zipFileName = storage_path('tmp/export_' . $token . '.zip');
            $zip = new \ZipArchive;

            if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
            {
                $files = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dirName), \RecursiveIteratorIterator::LEAVES_ONLY);
                foreach ($files as $name => $file)
                {
                    if (!$file->isDir())
                    {
                        $filePath = $file->getRealPath();
                        $relativePath = substr($filePath, strlen($dirName) + 1);
                        $zip->addFile($filePath, $relativePath);
                    }
                }
                $zip->close();

                $dir=$dirName;
                if (is_dir($dir)) {
                    $objects = scandir($dir);
                    foreach ($objects as $object) {
                        if ($object != "." && $object != "..") {
                            if (filetype($dir."/".$object) == "dir"){

                                $objects_1 = scandir($dir."/".$object);
                                foreach ($objects_1 as $object1) {
                                    if ($object1 != "." && $object1 != "..") {
                                        unlink   ($dir."/".$object."/".$object1);
                                    }
                                }

                                rmdir($dir."/".$object);
                            }
                            else {
                                unlink   ($dir."/".$object);
                            }
                        }
                    }
                    rmdir($dir);
                }

                $case = CaseModel::fetch(array('full_name'=>true),$id);
                \Log\Model\Log::saveNewLog('CASE_EXPORTED',trans('common::application.Exported the documents attached to the aid case as') . ' :"'.$case->full_name. '" ');

                return response()->json([
                    'download_token' => $token,
                ]);

            }



        }else{
            return response()->json(['status' => false]);
        }
    }

    /************ UPDATE CASE STATUS *************/
    public function update(\Illuminate\Http\Request $request ,$id)
    {

        $response = array();
        $input=['reason' => strip_tags($request->get('reason'))];
        $rules= ['reason' => 'required'];

        $error = \App\Http\Helpers::isValid($input,$rules);
        if($error)
            return response()->json($error);

        $passed = true;
        $status = $request->get('status');
        $case =CaseModel::fetch(array('action' => 'edit', 'person'=> true,'full_name'=>true),$id);

        if($status == 0){
            if(!is_null($case->death_date)){
                return response()->json(["status" => 'failed', "msg"  => trans('application::application.you can not update status , deed') , $case]);

            }
        }
        $id_card_number = (int) $case->id_card_number ;
        if(!GovServices::checkCard($case->id_card_number) && $status == 0){
            return response()->json(["status" => 'failed', "msg"  => trans('application::application.you can not update status , invalid number') , $case]);
        }

        if($this->type == 2 && $status == 0){
            if($case->category_id == 5 && ($case->marital_status_id == 10 || $case->marital_status_id == '10')){
                return response()->json(["status" => 'failed', $case,"msg"  => trans('application::application.you can not update status , the person is single') , $case]);
            }
        }
        if($this->type == 2 && $status == 0){
            $mainConnector=OrgLocations::getConnected($case->organization_id);
            if(!is_null($case->adsdistrict_id)){
                if(!in_array($case->adsdistrict_id,$mainConnector)){
                    $passed = false;
                }else{
                    if(!is_null($case->adsregion_id)){
                        if(!in_array($case->adsregion_id,$mainConnector)){
                            $passed = false;
                        }else{

                            if(!is_null($case->adsneighborhood_id)){
                                if(!in_array($case->adsneighborhood_id,$mainConnector)){
                                    $passed = false;
                                }else{

                                    if(!is_null($case->adssquare_id)){
                                        if(!in_array($case->adssquare_id,$mainConnector)){
                                            $passed = false;
                                        }else{
                                            if(!is_null($case->adsmosques_id)){
                                                if(!in_array($case->adsmosques_id,$mainConnector)){
                                                    $passed = false;
                                                }
                                            }else{
                                                $passed = false;
                                            }
                                        }
                                    }else{
                                        $passed = false;
                                    }
                                }
                            }else{
                                $passed = false;
                            }
                        }
                    }else{
                        $passed = false;
                    }
                }
            }else{
                $passed = false;
            }
        }

        if($passed == true ){
            if(\Common\Model\AidsCases::where('id',$id)->update(['status' => $status])) {
                $user = \Auth::user();
                \Common\Model\CasesStatusLog::create(['case_id'=>$id, 'user_id'=>$user->id, 'reason'=>$request->get('reason') , 'status'=>$status, 'date'=>date("Y-m-d")]);
                \Log\Model\Log::saveNewLog('CASE_UPDATED',trans('common::application.Edited status for someone classified as an aid case') . ' "'.$case->full_name. '" ');
                return response()->json(["status" => 'success', "msg"  => trans('aid::application.The row is updated')]);
            }else{
                return response()->json(["status" => 'failed', "msg"  => trans('aid::application.There is no update')]);
            }
        }
        return response()->json(["status" => 'failed', "msg"  => trans('common::application.not in organization locations')]);
    }

    //************ SAVE CASE DETAILS ************//
    public function store(Request $request)
    {

        $user = \Auth::user();

        $options = ['mode' =>'create'];

        if($request->get('no_en')){
            $options['no_en']=true;
        }

        if($request->get('person_id')){
            $options['mode']= 'update';
            $options['person_id']= $request->get('person_id');
        }

        if($request->get('aidsLocations')){
            $options['aidsLocations']= true;
        }

        $rules= ValidatorModel::getPersonValidatorRules($request->get('priority'),$options);

        if($request->get('no_kinship')){
            $rules['kinship_id']='';
        }

        $error = \App\Http\Helpers::isValid($request->all(),$rules);
        if($error)
            return response()->json($error);

        $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$request->category_id)->first();

        $request->request->add(['category_type'=> $this->type]);
        $request->request->add(['user_id'=> $user->id]);
        $request->request->add(['organization_id'=> $user->organization_id]);

        $case_id = $request->case_id;

        $request->request->add(['family_cnt'=> 0 ]);
        $request->request->add(['spouses'=> 0 ]);
        $request->request->add(['male_live'=> 0 ]);
        $request->request->add(['female_live'=> 0 ]);

        $dataToSet = $request->all();
        $setOnTransfer = false;

        $person_ = null;
        $transfers_ar =['adscountry_id'=> $request->adscountry_id, 'adsdistrict_id'=> $request->adsdistrict_id,
                        'adsregion_id'=> $request->adsregion_id, 'adsneighborhood_id'=> $request->adsneighborhood_id,
                        'adssquare_id'=> $request->adssquare_id, 'adsmosques_id'=> $request->adsmosques_id,
                        'category_id' => $request->category_id];

        if($options['mode'] == 'update' && $this->type  == 2){
            $person_ = Person::where(function ($q) use ($request){
                                        $q->where('id_card_number',$request->id_card_number);
                                    })->first();
            if($person_){
                if($request->adsmosques_id != $person_->adsmosques_id){
                    $founded = OrgLocations::where(['organization_id'=>$user->organization_id ,'location_id'=>$request->adsmosques_id])->count();
                    if($founded == 0 ){
                        $transfers_ar['person_id'] = $person_->id;
                        $transfers_ar['category_id'] = $request->category_id;
                        $transfers_ar['prev_adscountry_id'] = $person_->adscountry_id;
                        $transfers_ar['prev_adsdistrict_id'] = $person_->adsdistrict_id;
                        $transfers_ar['prev_adsregion_id'] = $person_->adsregion_id;
                        $transfers_ar['prev_adsneighborhood_id'] = $person_->adsneighborhood_id;
                        $transfers_ar['prev_adssquare_id'] = $person_->adssquare_id;
                        $transfers_ar['prev_adsmosques_id'] = $person_->adsmosques_id;

                        $transferObj =  Transfers::where([$transfers_ar])->first();
                        if(!$transferObj){
                            Transfers::create(['created_at' => date('Y-m-d H:i:s'), 'person_id'=> $person_->id,
                                'prev_adscountry_id'=> $person_->adscountry_id, 'prev_adsdistrict_id'=> $person_->adsdistrict_id,
                                'prev_adsregion_id'=> $person_->adsregion_id, 'prev_adsneighborhood_id'=> $person_->adsneighborhood_id,
                                'prev_adssquare_id'=> $person_->adssquare_id, 'prev_adsmosques_id'=> $person_->adsmosques_id,
                                'adscountry_id'=> $request->adscountry_id, 'adsdistrict_id'=> $request->adsdistrict_id,
                                'adsregion_id'=> $request->adsregion_id, 'adsneighborhood_id'=> $request->adsneighborhood_id,
                                'adssquare_id'=> $request->adssquare_id, 'adsmosques_id'=> $request->adsmosques_id,
                                'category_id' => $request->category_id, 'user_id'=> $user->id,
                                'organization_id' => $user->organization_id]);

                            $dataToSet['adscountry_id'] = $person_->adscountry_id;
                            $dataToSet['adsdistrict_id'] = $person_->adsdistrict_id;
                            $dataToSet['adsregion_id'] = $person_->adsregion_id;
                            $dataToSet['adssquare_id'] = $person_->adssquare_id;
                            $dataToSet['adsneighborhood_id'] = $person_->adsneighborhood_id;
                            $dataToSet['adsmosques_id'] = $person_->adsmosques_id;
                            $setOnTransfer = true;
                            $orgs =  OrgLocations::LocationOrgs($request->adsmosques_id,$person_->id,$request->category_id);
                            $users =User::userHasPermission('reports.transfers.manage',$orgs);
                            $url = "#/transfers";

                            $full_name = $person_->first_name.' '.$person_->second_name.' '.$person_->third_name.' '.$person_->last_name;
                            if(sizeof($users)>0){
                                \Log\Model\Notifications::saveNotification([
                                    'user_id_to' =>$users,
                                    'url' =>$url,
                                    'message' =>  trans('common::application.he migrated') . ' :' . $full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name
                                ]);
                            }

                            \Log\Model\Log::saveNewLog('TRANSFER_CREATED',trans('common::application.he migrated') . ': ' . ' ' . $full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name);

                        }
                    }
                }
            }
        }

        $person = Person::savePerson($dataToSet);
        $passed = true;

        if (!Setting::getIgnoreGovServices()){
            $row = GovServices::byCard($person['id_card_number']);
            if($row['status'] != false){
                $record = $row['row'];
                $recordWithDetails = GovServices::setCaseFormDetails($record,$person,$request->id_card_number);

                $map = Person::PersonMap($record);
                $map['photo_url'] = $record->photo_url ;
                $map['photo_src'] = 'external' ;
                $map['photo_update_date'] = $record->photo_update_date ;
                $map['card_type']='1';
                if (substr($request->id_card_number, 0, 1) === '7') {
                    $map['card_type']='2';
                }

                $toUpdate = $recordWithDetails['person'];
                $toUpdate['person_id'] = $person['id'];
                CloneGovernmentPersons::saveNew((Object) $recordWithDetails['record']);
                $person_ = (object) Person::savePerson($toUpdate);

                if (sizeof($record->all_relatives) > 0 ){

                    $PARENT_RELATIVE_DESC = ["أب" , "أم","اب" , "ام"];
                    $SPONSES_RELATIVE_DESC = ["زوج/ة"];
                    $CHILD_RELATIVE_DESC = ["ابن/ة"];

                    foreach ($record->all_relatives as $key=> $relative) {

                        if(in_array($relative->RELATIVE_DESC ,$CHILD_RELATIVE_DESC) ||
                            in_array($relative->RELATIVE_DESC ,$SPONSES_RELATIVE_DESC)||
                            in_array($relative->RELATIVE_DESC ,$PARENT_RELATIVE_DESC)) {
                            $crd = $relative->IDNO_RELATIVE;
                            $map = GovServices::inputsMap($relative);
                            $map['kinship_id'] =$relative->kinship_id;
                            $map['kinship_name']=$relative->kinship_name;

                            $find=Person::where('id_card_number',$crd)->first();
                            if(!is_null($find)){
                                $map['person_id'] = $find->id;
                            }

                            if(in_array($relative->RELATIVE_DESC ,$CHILD_RELATIVE_DESC)){
                                if($person_->gender == 1){
                                    $map['father_id']= $person_->id;
                                }else{
                                    $map['mother_id']= $person_->id;
                                }
                            }

                            if(($person_->gender == 1) &&
                                in_array($relative->RELATIVE_DESC ,$SPONSES_RELATIVE_DESC)){
                                $map['husband_id']= $person_->id;
                            }

                            $relative_row = Person::savePerson($map);

                            if(in_array($relative->RELATIVE_DESC ,$PARENT_RELATIVE_DESC)){
                                if($person_->gender == 1){
                                    $person_->father_id= $relative_row['id'];
                                }else{
                                    $person_->mother_id= $relative_row['id'];
                                }
                            }
                            if(($person_->gender == 2) &&
                                in_array($relative->RELATIVE_DESC ,$SPONSES_RELATIVE_DESC)){
                                $person_->husband_id= $relative_row['id'];
                            }

                            CloneGovernmentPersons::saveNewWithRelation($crd,$relative);
                        }
                    }
                }
            }
        }

        $msg ='';
        if(isset($person['case_id'])){
            $case =CaseModel::fetch(array('action' => 'edit', 'person'=> true,'full_name'=>true),$person['case_id']);

            if($this->type == 2) {
                CaseModel::DisableCaseInNoConnected($case,$user->organization_id,$user->id);
                $founded = OrgLocations::where(['organization_id'=>$user->organization_id ,
                    'location_id'=>$case->adsmosques_id])->count();
                if($founded == 0 ){
                    $passed = false;
                }
            }

            if($passed == true){
                \Common\Model\AidsCases::where('id',$person['case_id'])->update(['status' => 0]);

                if($request->get('case_id') != null){
                    $action='CASE_UPDATED';
                    $message = trans('common::application.Has edited case data for'). '  : '.' "'.$case->full_name. ' " ';
                }else{
                    $action='CASE_CREATED';
                    $message= trans('common::application.Has added new case data for')  . '  : '.' "'.$case->full_name. ' " ';
                }

                \Log\Model\Log::saveNewLog($action,$message);

                /*
                if(!$setOnTransfer){
                    if($this->type == 2) {
                        $transferObj =  Transfers::where(['person_id'=> $person['id'], 'category_id' => $request->category_id,
                            'prev_adscountry_id'=> $person['adscountry_id'], 'prev_adsdistrict_id'=> $person['adsdistrict_id'],
                            'prev_adsregion_id'=> $person['adsregion_id'], 'prev_adsneighborhood_id'=> $person['adsneighborhood_id'],
                            'prev_adssquare_id'=> $person['adssquare_id'], 'prev_adsmosques_id'=> $person['adsmosques_id'],

                            'adscountry_id'=> $person['adscountry_id'], 'adsdistrict_id'=> $person['adsdistrict_id'],
                            'adsregion_id'=> $person['adsregion_id'], 'adsneighborhood_id'=> $person['adsneighborhood_id'],
                            'adssquare_id'=> $person['adssquare_id'], 'adsmosques_id'=> $person['adsmosques_id']])->first();
                        if(!$transferObj){
                            Transfers::create(['person_id'=> $person['id'], 'category_id' => $request->category_id,
                                'prev_adscountry_id'=> $person['adscountry_id'], 'prev_adsdistrict_id'=> $person['adsdistrict_id'],
                                'prev_adsregion_id'=> $person['adsregion_id'], 'prev_adsneighborhood_id'=> $person['adsneighborhood_id'],
                                'prev_adssquare_id'=> $person['adssquare_id'], 'prev_adsmosques_id'=> $person['adsmosques_id'],

                                'adscountry_id'=> $person['adscountry_id'], 'adsdistrict_id'=> $person['adsdistrict_id'],
                                'adsregion_id'=> $person['adsregion_id'], 'adsneighborhood_id'=> $person['adsneighborhood_id'],
                                'adssquare_id'=> $person['adssquare_id'], 'adsmosques_id'=> $person['adsmosques_id'],
                                'user_id'=> $user->id, 'organization_id' => $user->organization_id,
                                'created_at' => date('Y-m-d H:i:s')]);

                            $orgs =  OrgLocations::LocationOrgs($person['adsmosques_id'],$case->person_id,$request->category_id);
                            $users =User::userHasPermission('reports.transfers.manage',$orgs);
                            $url = "#/transfers";

                            if(sizeof($users)>0){
                                \Log\Model\Notifications::saveNotification([
                                    'user_id_to' =>$users,
                                    'url' =>$url,
                                    'message' =>  trans('common::application.he migrated') . ' :' . $case->full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name
                                ]);
                            }
                            \Log\Model\Log::saveNewLog('TRANSFER_CREATED',trans('common::application.he migrated') . ': ' . ' ' . $case->full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name);
                        }
                    }
                    else{
                        $relaysObj =  Relays::where(['person_id'=> $person['id'],'category_id' => $request->category_id])->first();

                        if(!$relaysObj){
                            Relays::create(['person_id'=> $person['id'],
                                'category_id' => $request->category_id,
                                'user_id'=> $user->id,
                                'organization_id' => $user->organization_id]);

                            $levelId = null;
                            if(is_null($case->adsmosques_id)) {
                                if(is_null($case->adssquare_id)) {
                                    if(is_null($case->adsneighborhood_id)) {
                                        if(is_null($case->adsregion_id)) {
                                            if(is_null($case->adsdistrict_id)) {
                                                if(is_null($case->adscountry_id)) {
                                                    $levelId = $case->adscountry_id ;
                                                }
                                            }else{
                                                $levelId = $case->adsdistrict_id ;
                                            }
                                        }else{
                                            $levelId = $case->adsregion_id ;
                                        }
                                    }else{
                                        $levelId = $case->adsneighborhood_id ;
                                    }
                                }else{
                                    $levelId = $case->adssquare_id ;
                                }
                            }
                            else{
                                $levelId = $case->adsmosques_id ;
                            }

                            $orgs =  OrgLocations::LocationOrgs($levelId,$case->person_id,$request->category_id);
                            $users =User::userHasPermission('reports.relays.confirm',$orgs);
                            if($request->type == 2) {
                                $url = "#/relays/aids";
                            }else{
                                $url = "#/relays/sponsorships";
                            }

                            if(sizeof($users)>0){
                                \Log\Model\Notifications::saveNotification([
                                    'user_id_to' =>$users,
                                    'url' =>$url,
                                    'message' =>  trans('common::application.he migrated') . ' :' . $case->full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name
                                ]);
                            }
                            \Log\Model\Log::saveNewLog('RELAY_CREATED',trans('common::application.he migrated') . ': ' . ' ' . $case->full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name);
                        }
                    }
                }
                */
                return response()->json(['status'=>'success','person_id'=>$person['id'],'case_id'=>$person['case_id'],'msg' => trans('common::application.action success')]);
            }
            else{

                if ($setOnTransfer) {
                    $msg = trans('common::application.the person sent to on transfer request');
                }

                if(is_null($case_id)){
                    $action='CASE_CREATED';
                    $message= trans('common::application.Added data') . trans('common::application.for').'  : '.' "'.$case->full_name. ' " ';
                    \Log\Model\Log::saveNewLog($action,$message);
                    CaseModel::destroy($person['case_id']);
                    $msg .= trans('common::application.not save and  disabled because it is not in organization locations');
                }
                else{
                    \Common\Model\AidsCases::where('id',$person['case_id'])->update(['status' => 1]);
                    $action='CASE_UPDATED';
                    $message = trans('common::application.Has edited data') . '  : '.' "'.$case->full_name. ' " ';
                    \Log\Model\Log::saveNewLog($action,$message);
                    \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id,
                        'reason'=>trans('common::application.Disabled your recorded status for')  . ' "'.$case->full_name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization') , 'status'=>1, 'date'=>date("Y-m-d")]);
                    \Log\Model\Log::saveNewLog('CASE_UPDATED',trans('common::application.Disabled your recorded status for')  . ' "'.$case->full_name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization'));
                    $msg .= trans('common::application.saved and  disabled because it is not in organization locations');
                }

                if(!$setOnTransfer) {
                    if($this->type == 2) {

                        $transferObj =  Transfers::where(['person_id'=> $person['id'], 'category_id' => $request->category_id,
                            'prev_adscountry_id'=> $case->adscountry_id, 'prev_adsdistrict_id'=> $case->adsdistrict_id,
                            'prev_adsregion_id'=> $case->adsregion_id, 'prev_adsneighborhood_id'=> $case->adsneighborhood_id,
                            'prev_adssquare_id'=> $case->adssquare_id, 'prev_adsmosques_id'=> $case->adsmosques_id,

                            'adscountry_id'=> $case->adscountry_id, 'adsdistrict_id'=> $case->adsdistrict_id,
                            'adsregion_id'=> $case->adsregion_id, 'adsneighborhood_id'=> $case->adsneighborhood_id,
                            'adssquare_id'=> $case->adssquare_id, 'adsmosques_id'=> $case->adsmosques_id])->first();

                        if(!$transferObj){
                            Transfers::create(['person_id'=> $person['id'], 'category_id' => $request->category_id,
                                'prev_adscountry_id'=> $case->adscountry_id, 'prev_adsdistrict_id'=> $case->adsdistrict_id,
                                'prev_adsregion_id'=> $case->adsregion_id, 'prev_adsneighborhood_id'=> $case->adsneighborhood_id,
                                'prev_adssquare_id'=> $case->adssquare_id, 'prev_adsmosques_id'=> $case->adsmosques_id,

                                'adscountry_id'=> $case->adscountry_id, 'adsdistrict_id'=> $case->adsdistrict_id,
                                'adsregion_id'=> $case->adsregion_id, 'adsneighborhood_id'=> $case->adsneighborhood_id,
                                'adssquare_id'=> $case->adssquare_id, 'adsmosques_id'=> $case->adsmosques_id,
                                'user_id'=> $user->id, 'organization_id' => $user->organization_id,
                                'created_at' => date('Y-m-d H:i:s')]);

                            $orgs =  OrgLocations::LocationOrgs($case->adsmosques_id,$case->person_id,$request->category_id);
                            $users =User::userHasPermission('reports.transfers.manage',$orgs);
                            $url = "#/transfers";

                            \Log\Model\Notifications::saveNotification([
                                'user_id_to' =>$users,
                                'url' =>$url,
                                'message' => trans('common::application.he migrated') . ': ' . ' ' . $case->full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name
                            ]);
                            \Log\Model\Log::saveNewLog('TRANSFER_CREATED',trans('common::application.he migrated') . ': ' . ' ' . $case->full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name);
                        }
                    }
                    else {
                        $relaysObj =  Relays::where(['person_id'=> $person['id'],'category_id' => $request->category_id])->first();

                        if(!$relaysObj){
                            Relays::create(['person_id'=> $person['id'],
                                'category_id' => $request->category_id,
                                'user_id'=> $user->id,
                                'organization_id' => $user->organization_id]);

                            $users =User::userHasPermission('reports.relays.confirm',[]);
                            $url = "#/relays/sponsorships";
                            \Log\Model\Notifications::saveNotification(['user_id_to' =>$users, 'url' =>$url,
                                'message' => trans('common::application.he migrated') . ': ' . ' ' . $case->full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name
                            ]);
                            \Log\Model\Log::saveNewLog('RELAY_CREATED',trans('common::application.he migrated') . ': ' . ' ' . $case->full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name);
                        }

                    }
                }

                return response()->json(["status" => 'failed_disabled', "msg"  =>$msg ]);
            }

        }
        else{

            if ($setOnTransfer) {
                $msg = trans('common::application.the person sent to on transfer request');
            }
            $msg .= trans('common::application.the person has active case on another organization');
            return response()->json(["status" => 'failed_disabled', "msg"  =>$msg ]);

        }

        return response()->json(['status'=>'failed','msg' =>trans('common::application.save not successful') ,'person' =>$person]);
    }

    /************ SOFT DELETE OF CASE *************/
    public function destroy($id,Request $request)
    {
        $response = array();
        $case = CaseModel::fetch(array('category_name' => true,'full_name'=>true),$id);
        if($case->category_type == 2){
            $message= trans('common::application.Deleted registered person record'). trans('common::application.As a case') . trans('common::application.aid') . ' "'.$case->full_name. '" ';
        }else{
            $message= trans('common::application.Deleted registered person record'). trans('common::application.As a case') . trans('common::application.sponsorship') . ' "'.$case->full_name. '" ';
        }

        if(\Common\Model\AidsCases::destroy($id))
        {
            \Common\Model\AidsCases::where('id',$id)->withTrashed()->update(['reason'=>$request->reason]);
            \Log\Model\Log::saveNewLog('CASE_DELETED',$message);
            $response["status"]= 'success';
            $response["msg"]= trans('setting::application.The row is deleted from db');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('setting::application.The row is not deleted from db');

        }
        return response()->json($response);
    }

    /************ RESTORE DELETED CASE *************/
    public function restore($id)
    {

        $response = array();

        $case = \Common\Model\AidsCases::where('id',$id)->withTrashed()->first();
        $person = Person::where('id',$case->person_id)->first();

        if(!is_null($person->death_date)){
            $response["status"]= 'failed';
            $response["msg"]= trans('aid::application.The row is not restored because the person was dead');
            return response()->json($response);
        }

        $passed = true;

        if($this->type == 2){
            $organization_id = $case->organization_id ;
            $mainConnector=OrgLocations::getConnected($organization_id);
            if(!is_null($person->adsdistrict_id)){
                if(!in_array($person->adsdistrict_id,$mainConnector)){
                    $passed = false;
                }else{
                    if(!is_null($person->adsregion_id)){
                        if(!in_array($person->adsregion_id,$mainConnector)){
                            $passed = false;
                        }else{

                            if(!is_null($person->adsneighborhood_id)){
                                if(!in_array($person->adsneighborhood_id,$mainConnector)){
                                    $passed = false;
                                }else{

                                    if(!is_null($person->adssquare_id)){
                                        if(!in_array($person->adssquare_id,$mainConnector)){
                                            $passed = false;
                                        }else{
                                            if(!is_null($person->adsmosques_id)){
                                                if(!in_array($person->adsmosques_id,$mainConnector)){
                                                    $passed = false;
                                                }
                                            }else{
                                                $passed = false;
                                            }
                                        }
                                    }else{
                                        $passed = false;
                                    }
                                }
                            }else{
                                $passed = false;
                            }
                        }
                    }else{
                        $passed = false;
                    }
                }
            }else{
                $passed = false;
            }


            if($passed == false){
                $response["status"]= 'failed';
                $response["msg"]= trans('aid::application.The row is not restored because the person  not in locations range');
                return response()->json($response);
            }
        }

        if(\Common\Model\AidsCases::where('id',$id)->withTrashed()->update(['deleted_at'=>null,'reason'=>null]))
        {
            $case = CaseModel::fetch(array('category_name' => true,'full_name'=>true),$id);
            if($case->category_type == 2){
                $message= trans('common::application.Restored the record of a registered person') . trans('common::application.As a case') . trans('common::application.aid') . ' "'.$case->full_name. '" ';
            }else{
                $message= trans('common::application.Restored the record of a registered person') . trans('common::application.As a case') . trans('common::application.sponsorship') . ' "'.$case->full_name. '" ';
            }

            \Log\Model\Log::saveNewLog('CASE_UPDATED',$message);
            $response["status"]= 'success';
            $response["msg"]= trans('aid::application.The row is restored');

        }else{
            $response["status"]= 'failed';
            $response["msg"]= trans('aid::application.The row is not restored');

        }
        return response()->json($response);

    }

    /************ GET CASE STATUS LOGS*************/
    public function getStatusLogs($id)
    {
        return response()->json(\Common\Model\CasesStatusLog::getLogs($id));
    }

    /************ CHECK BANK ACCOUNT *************/
    public function checkAccount($id,Request $request)
    {
        $account= \Common\Model\PersonModels\PersonBank::where(['bank_id'=>$id,'account_number'=>$request->get('account_number')])->first();
        if($account){
            return response()->json(['status'=>false ,'person_id' =>$account->person_id]);
        }
        return response()->json(['status'=>true,'person_id' => null]);
    }

    /************ SET AND RESET ATTACHMENT ************/
    public function setCaseAttachment(Request $request ,$id)
    {
        $response = array();
        $target = $id;
        $old_document_id = null;
        $document_type_id=(int)$request->document_type_id;

        if($this->type == 1) {
            $category_id=(int)$request->category_id;
            $mother_id=$request->mother_id;
            $father_id=$request->father_id;
            $guardian_id=$request->guardian_id;
            $entry = \Common\Model\CategoriesDocument::where(['category_id'=>$category_id,'document_type_id'=>$document_type_id])->first();
            if(isset($entry) && $entry != null){
                if($entry->scope == 1){
                    $target = $father_id;
                }else if($entry->scope == 2) {
                    $target = $mother_id;
                } else if ($entry->scope == 3 || $entry->scope == 0) {
                    $target = $id;
                } else if ($entry->scope == 4) {
                    $target = $guardian_id;
                }
            }
        }

        $attachment=\Common\Model\PersonDocument::where(['person_id' => $target ,'document_type_id'=>$document_type_id])->first();
        $person = Person::fetch(array('id' => $target));
        $name=$person->first_name.' '.$person->second_name.' '.$person->third_name.' '.$person->last_name;

        if($attachment){
            $old_document_id = $attachment->document_id;
            $msg = trans('common::application.Has replaced a file attached to the aid case') . ' : "'.$name. '" ';
            $action='CASE_UPDATED';
        }else{
            $attachment=new \Common\Model\PersonDocument();
            $attachment->person_id= $target;
            $attachment->document_type_id=$document_type_id;
            $msg=trans('common::application.Added a file attached to the aid status') . ' :  "'.$name. '" ';
            $action='CASE_CREATED';
        }
        $attachment->document_id=$request->document_id;

        if($attachment->save()){
            if(!is_null($old_document_id)){
                $file = File::findOrFail($id);

                \Illuminate\Support\Facades\Storage::delete($file->filepath);
                foreach (\Document\Model\FileRevision::allforFile($file->id) as $revision) {
                    \Illuminate\Support\Facades\Storage::delete($revision->filepath);
                }
                $file->delete();
            }
            \Log\Model\Log::saveNewLog($action,$msg);
            return response()->json(["status" => 'success' ,"msg" =>trans('aid::application.The Document is selected successfully')]);
        }

        return response()->json(["status" => 'failed' ,"msg" =>trans('aid::application.The Document selected failed')]);

    }

    public function setAttachment(Request $request ,$id)
    {
        $response = array();
        $document_type_id=$request->get('document_type_id');
        $old_document_id = null;
        $attachment=\Common\Model\PersonDocument::where(['person_id' => $id ,'document_type_id'=>$document_type_id])->first();

        $person = Person::fetch(array('id' => $id));
        $name=$person->first_name.' '.$person->second_name.' '.$person->third_name.' '.$person->last_name;

        if($attachment){
            $old_document_id = $attachment->document_id;
            $msg=trans('common::application.Has replaced a file attached to the aid case') . ' : "'.$name. '" ';
            $action='CASE_UPDATED';
        }else{
            $attachment=new \Common\Model\PersonDocument();
            $attachment->person_id=$id;
            $attachment->document_type_id=$document_type_id;
            $msg=trans('common::application.Added a file attached to the aid status') . ' :  "'.$name. '" ';
            $action='CASE_CREATED';
        }
        $attachment->document_id=$request->get('document_id');

        if($attachment->save()){
            \Log\Model\Log::saveNewLog($action,$msg);
            if(!is_null($old_document_id)){
                $file = File::findOrFail($id);

                \Illuminate\Support\Facades\Storage::delete($file->filepath);
                foreach (\Document\Model\FileRevision::allforFile($file->id) as $revision) {
                    \Illuminate\Support\Facades\Storage::delete($revision->filepath);
                }
                $file->delete();
            }

            return response()->json(["status" => 'success' ,"msg" =>trans('aid::application.The Document is selected successfully')]);
        }

        return response()->json(["status" => 'failed' ,"msg" =>trans('aid::application.The Document selected failed')]);

    }

    /************ EXPORT PDF ************/
    public function pdf(Request $request){

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $item =CaseModel::fetch(array(
            'action' => 'show',
            'category_type'      => $this->type,
            'category'      => $request->get('category'),
            'full_name'     => $request->get('full_name'),
            'family_member'     => $request->get('family_member'),
            'person'        => $request->get('person'),
            'persons_i18n'  => $request->get('persons_i18n'),
            'contacts'      => $request->get('contacts'),
            'education'      => $request->get('education'),
            'islamic'      => $request->get('islamic'),
            'health'      => $request->get('health'),
            'organization_name'          =>true,
            'work'          => $request->get('work'),
            'residence'     => $request->get('residence'),
            'case_needs'    => $request->get('case_needs'),
            'default_bank'         => $request->get('default_bank'),
            'banks'         => $request->get('banks'),
            'home_indoor'   => $request->get('home_indoor'),
            'reconstructions' => $request->get('reconstructions'),
            'financial_aid_source'     => $request->get('financial_aid_source'),
            'non_financial_aid_source' => $request->get('non_financial_aid_source'),
            'persons_properties' => $request->get('persons_properties'),
            'persons_documents'  => $request->get('persons_documents'),
            'case_sponsorships'  => $request->get('case_sponsorships'),
            'case_payments'      => $request->get('case_payments'),
            'father_id'     => $request->get('father_id'),
            'mother_id'     => $request->get('mother_id'),
            'guardian_id'   => $request->get('guardian_id'),
            'father'        => $request->get('father'),
            'mother'        => $request->get('mother'),
            'guardian'      => $request->get('guardian'),
            'guardian_kinship'      => $request->get('guardian_kinship'),
            'family_payments'    => $request->get('family_payments'),
            'guardian_payments'  => $request->get('guardian_payments'),
            'visitor_note'  => $request->get('visitor_note')
        ),$request->get('case_id'));
        $organization_name= $item->organization_name;
        $category_name= $item->category_name;

        $html='';
        PDF::SetTitle($item->category_name .' '.$item->organization_name);
        PDF::SetFont('aealarabiya', '', 18);
        PDF::SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        PDF::setLanguageArray(['a_meta_charset'=> 'UTF-8', 'a_meta_dir'=> 'rtl', 'a_meta_language'=> 'fa', 'w_page'=> 'page']);

        PDF::SetMargins(PDF_MARGIN_LEFT, 20, PDF_MARGIN_RIGHT);
//        PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
//        PDF::SetFooterMargin(PDF_MARGIN_FOOTER);
        PDF::SetAutoPageBreak(TRUE);
        $lg = Array();
        $lg['a_meta_charset'] = 'UTF-8';
        $lg['a_meta_dir'] = 'rtl';
        $lg['a_meta_language'] = 'fa';
        $lg['w_page'] = 'page';
        PDF::setLanguageArray($lg);
        PDF::setCellHeightRatio(1.5);

        $date = date('Y-m-d');
        $organization_name = $item->organization_name;
        $category_name = $item->category_name;
        $first_name = $item->first_name;
        $second_name = $item->second_name;
        $third_name = $item->third_name ;
        $last_name = $item->last_name ;
        $name = $item->full_name;
        $en_first_name = $item->en_first_name;
        $en_second_name = $item->en_second_name;
        $en_third_name = $item->en_third_name;
        $en_last_name = $item->en_last_name;
        $en_name = $item->en_name;
        $id_card_number = $item->id_card_number;
        $birthday = $item->birthday;
        $age = $item->age;
        $gender = $item->gender;
        $gender_male = $item->gender_male;
        $gender_female = $item->gender_female;
        $deserted = $item->gender;
        $is_deserted = $item->is_deserted;
        $not_deserted = $item->not_deserted;
        $birth_place = $item->birth_place;
        $nationality = $item->nationality;
        $refugee = $item->refugee;

        $is_refugee = $item->is_refugee;
        $per_refugee = $item->per_refugee;
        $per_citizen = $item->per_citizen;
        $unrwa_card_number = $item->unrwa_card_number;
        $marital_status_name = $item->marital_status_name;
        $monthly_income = $item->monthly_income;
        $address = $item->address;
        $country = $item->country;
        $governarate = $item->governarate;
        $city = $item->city;
        $nearLocation = $item->nearLocation;
        $mosque_name = $item->mosque_name;
        $street_address = $item->street_address;
        $individual_count = $item->individual_count;
        $individual_count = $item->family_count;
        $male_count = $item->male_count;
        $female_count = $item->female_count;
        $father_brothers = $item->father_brothers;
        $mother_brothers = $item->mother_brothers;
        $brothers = $item->brothers;
        $children = $item->children;
        $male_children = $item->male_children;
        $female_children = $item->female_children;
        $phone = $item->phone;
        $primary_mobile = $item->primary_mobile;
        $wataniya = $item->wataniya;
        $secondary_mobile = $item->secondary_mobile ;
        $property_types = $item->property_types;
        $rent_value = $item->rent_value;
        $roof_materials = $item->roof_materials;
        $habitable = $item->habitable;
        $house_condition = $item->house_condition;
        $residence_condition = $item->residence_condition;
        $indoor_condition = $item->indoor_condition;
        $rooms = $item->rooms;
        $area = $item->area;
        $working = $item->working;
        $works = $item->works;
        $is_working = $item->is_working;
        $not_working = $item->not_working;
        $can_work = $item->can_work;
        $can_works = $item->can_works;
        $can_working = $item->can_working;
        $can_not_working = $item->can_not_working;
        $work_job = $item->work_job;
        $work_reason = $item->work_reason;
        $work_status = $item->work_status;
        $work_wage = $item->work_wage;
        $work_location = $item->work_location;
        $needs = $item->needs;
        $promised = $item->promised;
        $if_promised = $item->if_promised;
        $was_promised = $item->was_promised;
        $not_promised = $item->not_promised;
        $reconstructions_organization_name = $item->reconstructions_organization_name;
        $visitor = $item->visitor;
        $notes = $item->notes;
        $visited_at = $item->visited_at;
        $health_status = $item->health_status;
        $good_health_status = $item->good_health_status;
        $chronic_disease = $item->chronic_disease;
        $disabled = $item->disabled;
        $health_details = $item->health_details;
        $diseases_name = $item->diseases_name;
        $study = $item->study;
        $type = $item->type;
        $level = $item->level;
        $year = $item->year;
        $points = $item->points;
        $school = $item->school;
        $grade = $item->grade;
        $authority = $item->authority;
        $stage = $item->stage;
        $degree = $item->degree;
        $specialization = $item->specialization;
        $prayer = $item->prayer;
        $prayer_reason = $item->prayer_reason;
        $quran_reason = $item->quran_reason;
        $quran_parts = $item->quran_parts;
        $quran_chapters = $item->quran_chapters;
        $quran_center = $item->quran_center;
        $save_quran = $item->save_quran;
        $visitor_note = $item->visitor_note;
        $guardian_kinship = $item->guardian_kinship;
        $guardian_is = $item->guardian_is;
        $mother_is_guardian = $item->mother_is_guardian;
        $deserted = $item->deserted;
        $family_cnt = $item->family_cnt;
        $spouses = $item->spouses;
        $male_live = $item->male_live;
        $female_live = $item->female_live;

        $personalImagePath=PersonDocument::personalImagePath($item->person_id,$this->type);
        if(!is_null($personalImagePath)){
            $img = $personalImagePath;
        }else{
            $img = base_path('storage/app/emptyUser1.png');
        }

        if($item->category_type  == 2) {

            $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
            $html.= '<h2 style="text-align:center;"> '.$category_name.'</h2>';
            $html .= '<table border="1" style="border: 1px solid black; box-sizing:border-box;">';

            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.full_name').' </b></td>
                       <td width="325" align="center" >'.$name.'</td>
                        <td width="88" align="center" rowspan="6" style="background-color: white" >'
                .'<img src="'.$img.'" alt="" height="140" />'.
                '</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.id_card').'  </b></td>
                       <td width="120" align="center">'.$id_card_number.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.nationality').'  </b></td>
                       <td width="105" align="center">'.$nationality.'</td>
                      
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.BIRTH_DT').' </b></td>
                       <td width="120" align="center" >'.$birthday.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.birth_place').'</b></td>
                       <td width="105" align="center">'.$birth_place.'</td>
                   </tr>';

            $html .= ' <tr>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b>  '.trans('common::application.SOCIAL_STATUS').'  </b></td>
                   <td width="70" align="center">'.$marital_status_name.'</td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b>  '.trans('common::application.gender').' </b></td>
                   <td width="70" align="center">'.$gender.'</td>
                   <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="70" align="center"><b>  '.trans('aid::application.deserted').'   </b></td>
                   <td width="80" align="center">'.$deserted.'</td>
               </tr>';

            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.citizen').'  / '.trans('common::application.refugee').'</b></td>
                       <td width="120" align="center">'.$refugee.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.unrwa_card_number').'</b></td>
                       <td width="105" align="center">'.$unrwa_card_number.'</td>
                   </tr>';

            $html .='</table>';
            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';

            $is_qualified = $item->is_qualified;
            $qualified_card_number = $item->qualified_card_number;
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.phone').'</b></td>
                       <td width="71" align="center">'.$family_cnt.'</td>
                        <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.is_qualified').'</b></td>
                       <td width="71" align="center">'.$is_qualified.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.qualified_card_number').'</b></td>
                       <td width="71" align="center">'.$qualified_card_number.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.spouses').'</b></td>
                       <td width="71" align="center">'.$spouses.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.male_live').' </b></td>
                       <td width="71" align="center">'.$male_live.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.female_live').'</b></td>
                       <td width="71" align="center">'.$female_live.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="95" align="center"><b>  '.trans('common::application.district').' </b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="105" align="center"><b>  '.trans('common::application.region_').' </b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="105" align="center"><b>   '.trans('common::application.nearlocation_').'</b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="105" align="center"><b>  '.trans('common::application.square').' </b></td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="103" align="center"><b>  '.trans('common::application.mosques').' </b></td>
                   </tr>';
            $district = $item->governarate;
            $region_name = $item->region_name;
            $nearLocation = $item->nearLocation;
            $square_name = $item->square_name;
            $mosque_name = $item->mosque_name;

            $html .= ' <tr>
                       <td width="95" align="center">'.$district.'</td>
                       <td width="105" align="center">'.$region_name.'</td>
                       <td width="105" align="center">'.$nearLocation.'</td>
                       <td width="105" align="center">'.$square_name.'</td>
                       <td width="103" align="center">'.$mosque_name.'</td>
                   </tr>';


            $html .='</table>';
            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                        '.trans('common::application.contact information').'
                        </b></td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.mobile').'</b></td>
                       <td width="156" align="center">'.$primary_mobile.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.secondery_mobile').'</b></td>
                       <td width="157" align="center">'.$secondary_mobile.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.phone').'</b></td>
                       <td width="156" align="center">'.$phone.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.wataniya').'</b></td>
                       <td width="157" align="center">'.$wataniya.'</td>
                   </tr>';
            $html .='</table>';


            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                        '.trans('common::application.commercial_records_info').'
                        </b></td>
                   </tr>';
            $has_commercial_records = $item->has_commercial_records;
            $active_commercial_records = $item->active_commercial_records;
            $gov_commercial_records_details = $item->gov_commercial_records_details;
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.has_commercial_records').'</b></td>
                       <td width="156" align="center">'.$has_commercial_records.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.active_commercial_records').'</b></td>
                       <td width="157" align="center">'.$active_commercial_records.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.gov_commercial_records_details').'</b></td>
                       <td width="412" align="center">'.$gov_commercial_records_details.'</td>
                   </tr>';
            $html .='</table>';


            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                       '.trans('common::application.works info').' 
                        </b></td>
                   </tr>';

            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.is_she_working?').'</b></td>
                       <td width="156" align="center">'.$working.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_job_').' </b></td>
                       <td width="157" align="center">'.$work_job.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.is_can_work').'</b></td>
                       <td width="156" align="center">'.$can_work.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_reason').' </b></td>
                       <td width="157" align="center">'.$work_reason.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_location').' </b></td>
                       <td width="156" align="center">'.$work_location.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.Work_Status').' </b></td>
                       <td width="157" align="center">'.$work_status.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.Wage category').':  </b></td>
                       <td width="156" align="center">'.$work_wage.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.monthly_income').'</b></td>
                       <td width="157" align="center">'. $monthly_income .'</td>
                   </tr>';

            $actual_monthly_income = $item->actual_monthly_income;
            $receivables = $item->receivables;
            $receivables_sk_amount = $item->receivables_sk_amount;
            $has_other_work_resources = $item->has_other_work_resources;

            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.actual_monthly_income').'</b></td>
                       <td width="156" align="center">'.$actual_monthly_income.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.has_other_work_resources').' </b></td>
                       <td width="157" align="center">'.$has_other_work_resources.'</td>
                       
                   </tr>';
            $html .= ' <tr>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.receivables').' </b></td>
                           <td width="156" align="center">'.$receivables.'</td>
                           <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.receivables_sk_amount').'</b></td>
                           <td width="157" align="center">'.$receivables_sk_amount.'</td>
                           
                       </tr>';

            $html .='</table>';


            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1">';
            $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                       '.trans('common::application.home info').'
                        </b></td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.property_type').'</b></td>
                       <td width="156" align="center">'.$property_types.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.property_type').'('.trans('common::application.for renter').') </b></td>
                       <td width="157" align="center">'.$rent_value.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.roof_material').'</b></td>
                       <td width="156" align="center">'.$roof_materials.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.residence_condition').'</b></td>
                       <td width="157" align="center">'.$house_condition.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.indoor_condition').'</b></td>
                       <td width="156" align="center">'.$residence_condition.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.habitable').'</b></td>
                       <td width="157" align="center">'.$habitable.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.home_area').'</b></td>
                       <td width="156" align="center">'.$area.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.rooms').'</b></td>
                       <td width="157" align="center">'.$rooms.'</td>
                   </tr>';
            $need_repair = $item->need_repair;
            $repair_notes = $item->repair_notes;
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="200" align="center"><b>'.
                trans('common::application.need_repair').'</b></td>
                       <td width="312" align="center">'.$need_repair.'</td>
                   </tr>';


            $html .='</table>';



            $html .='</body></html>';
            PDF::AddPage('P','A4');
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


            $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';

            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                      '.trans('common::application.health info').'
                        </b></td>
                   </tr>';

            $has_health_insurance = $item->has_health_insurance;
            $health_insurance_number = $item->health_insurance_number;
            $health_insurance_type = $item->health_insurance_type;

            $has_device = $item->has_device;
            $used_device_name = $item->used_device_name;

            $gov_health_details = $item->gov_health_details;

            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.has_health_insurance').' </b></td>
                       <td width="156" align="center">'.$has_health_insurance.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.health_insurance_number').'</b></td>
                       <td width="157" align="center">'.$health_insurance_number.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.health_insurance_type').' </b></td>
                       <td width="156" align="center">'.$health_insurance_type.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.has_device').'</b></td>
                       <td width="157" align="center">'.$has_device.'</td>
                   </tr>';

            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.used_device_name').'</b></td>
                       <td width="413" align="center">'.$used_device_name.'</td>
                   </tr>';

            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.disease_/_disability_details').'</b></td>
                       <td width="413" align="center">'.$health_details.'</td>
                   </tr>';

            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.gov_health_details').'</b></td>
                       <td width="413" align="center">'.$gov_health_details.'</td>
                   </tr>';
            $html .='</table>';

            $html .= '<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="300" align="center"><b> '.trans('common::application.reconstructions_promised').'? ('.trans('common::application.yes').' /'.trans('common::application.no').') </b></td>
                       <td width="213" align="center">' . $promised . '</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="300" align="center"><b> '.trans('common::application.ReconstructionOrg').'('.trans('common::application.if the answer to the previous question is yes').'):  </b></td>
                       <td width="213" align="center">' . $reconstructions_organization_name . '</td>
                   </tr>';
            $html .='</table>';

            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="height: 50px ; padding: 55px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b> 
                       '.trans('common::application.case_needs').'
                       </b></td>
                       <td width="415" align="center">' . $needs . '</td>
                   </tr>';

            $html .= ' <tr>
                       <td style="height: 50px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b>  '.trans("common::application.repair_notes").'</b></td>
                       <td width="415" align="center">' . $repair_notes . '</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 50px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b>  '.trans("common::application.social researcher's recommendations").'</b></td>
                       <td width="415" align="center">' . $notes . '</td>
                   </tr>';

            $visitor_opinion = $item->visitor_opinion;
            $html .= ' <tr>
                       <td style="height: 50px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><br><br><b>  '.trans("common::application.visitor_opinion").'</b></td>
                       <td width="415" align="center">' . $visitor_opinion . '</td>
                   </tr>';
            $html .='</table>';

            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';

            $visitor_card = $item->visitor_card;
            $visitor_evaluation = $item->visitor_evaluation_;
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.visitor_card').'</b></td>
                       <td width="156" align="center">' . $visitor_card . '</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.visitor').' </b></td>
                       <td width="157" align="center">' . $visitor . '</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.visited_at').'</b></td>
                       <td width="156" align="center">' . $visited_at . '</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.visitor_evaluation').' </b></td>
                       <td width="157" align="center">' . $visitor_evaluation . '</td>
                   </tr>';



            $html .='</table>';

            $html .='</body></html>';
            PDF::AddPage('P','A4');
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

            $setting_id = 'aid_custom_form';
            $CaseCustomData =FormsCasesData::getCaseCustomData($item->case_id,$item->category_id,$item->organization_id,$setting_id);
            if (sizeof($CaseCustomData)!=0){

                $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                $html.= '<h2 style="text-align:center;">  '.trans('common::application.Custom Form Information').'</h2>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                               <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.organization_data').'  </b></td>
                               <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="400" align="center"><b> '.trans('common::application.explanation').'   </b></td>
                           </tr>';
                $z=1;
                foreach($CaseCustomData as $record) {
                    $c_label = $record['label'];
                    $c_value = $record['value'];

                    if($record['type'] != 3){
                        $html .= ' <tr>
                                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="120" align="center"><b> '.$c_label.'  </b></td>
                                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="400" align="center"><b> '.$c_value.'   </b></td>
                                   </tr>';
                    }else{
                        $html .= ' <tr>
                       <td style="height: 50px ; line-height: 35px; padding: 25px 0 12px 0;" width="120" align="center"><b> '.$c_label.'  </b></td>
                       <td style="height: 50px ; line-height: 35px; padding: 15px 0 12px 0;" width="400" align="center"><b> '.$c_value.'   </b></td>
                   </tr>';

                    }
                    $z++;
                }
                $html .='</table>';
                $html .='</body></html>';

                PDF::SetMargins(PDF_MARGIN_LEFT, 2, PDF_MARGIN_RIGHT);
                PDF::AddPage('P','A4');
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
            }


            if($request->get('parent_detail')){

                $get=array('action' => 'show','id' => $item->father_id,
                    'person'  => true, 'persons_i18n'  =>  true,
                    'health'  =>  true,  'education'  =>  true, 'work'  =>  true);

                $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';

                if($item->father_id != null){
                    $get['id']=$item->father_id;
                    $father =Person::fetch($get);
                    $father_first_name= $father->first_name;
                    $father_second_name= $father->second_name;
                    $father_third_name= $father->third_name ;
                    $father_last_name= $father->last_name ;
                    $father_name= $father->full_name;
                    $father_spouses= $father->spouses;
                    $father_id_card_number= $father->id_card_number;
                    $father_birthday= $father->birthday;
                    $father_en_first_name= $father->en_first_name;
                    $father_en_second_name= $father->en_second_name;
                    $father_en_third_name= $father->en_third_name;
                    $father_en_last_name= $father->en_last_name;
                    $father_en_name= $father->en_name;
                    $father_health_status= $father->health_status;
                    $father_health_details= $father->health_details;
                    $father_diseases_name= $father->diseases_name;
                    $father_specialization= $father->specialization;
                    $father_degree= $father->degree;
                    $father_alive= $father->alive;
                    $father_is_alive= $father->is_alive;
                    $father_death_date= $father->death_date;
                    $father_death_cause_name= $father->death_cause_name;
                    $father_working= $father->working;
                    $father_work_job= $father->work_job;
                    $father_work_location= $father->work_location;
                    $father_monthly_income= $father->monthly_income;
                    $father_gender_male= $father->gender_male;
                    $father_gender_female= $father->gender_female;
                    $father_is_refugee= $father->is_refugee;
                    $father_per_refugee= $father->per_refugee;
                    $father_per_citizen= $father->per_citizen;
                    $father_father_brothers= $father->father_brothers;
                    $father_mother_brothers= $father->mother_brothers;
                    $father_brothers= $father->brothers;
                    $father_children= $father->children;
                    $father_male_children= $father->male_children;
                    $father_female_children= $father->female_children;
                    $father_is_working= $father->is_working;
                    $father_not_working= $father->not_working;
                    $father_can_working= $father->can_working;
                    $father_can_not_working= $father->can_not_working;
                    $father_health_status= $father->health_status;
                    $father_good_health_status= $father->good_health_status;
                    $father_chronic_disease= $father->chronic_disease;
                    $father_disabled= $father->disabled;


                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                    '.trans('common::application.father-info').' 
                        </b></td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.full_name').'</b></td>
                       <td width="413" align="center">'.$father_name.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.id_card').'</b></td>
                       <td width="156" align="center">'.$father_id_card_number.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.BIRTH_DT').'</b></td>
                       <td width="156" align="center">'.$father_birthday.'</td>
                       </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.is_she_alive?').'</b></td>
                       <td width="156" align="center">'.$father_alive.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.death_date').'</b></td>
                       <td width="157" align="center">'.$father_death_date.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.death_cause_id').' </b></td>
                       <td width="413" align="center">'.$father_death_cause_name.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_number_of_spouses').'</b></td>
                       <td width="156" align="center">'.$father_spouses.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.monthly_income').' </b></td>
                       <td width="157" align="center">'.$grade.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.qualification').'</b></td>
                       <td width="156" align="center">'.$father_degree.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.Specialization').' </b></td>
                       <td width="157" align="center">'.$father_specialization.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.is_he_working').'</b></td>
                       <td width="156" align="center">'.$father_working.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_job_').'</b></td>
                       <td width="157" align="center">'.$father_work_job.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_location').' </b></td>
                       <td width="413" align="center">'.$father_work_location.'</td>
                   </tr>';

                    if($item->father_dead == 0){
                        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.health_condition').' </b></td>
                       <td width="156" align="center">'.$father_health_status.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.disease_id').'</b></td>
                       <td width="157" align="center">'.$father_diseases_name.'</td>
                   </tr>';
                        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.disease_/_disability_details').'</b></td>
                       <td width="413" align="center">'.$father_health_details.'</td>
                   </tr>';
                    }
                    $html .='</table>';
                    $html .='<div style="height: 40px; color: white"> - </div>';
                }

                if($item->mother_id != null){
                    $get['id']=$item->mother_id;
                    $mother =Person::fetch($get);
                    $mother_prev_family_name= $mother->prev_family_name;
                    $mother_first_name= $mother->first_name;
                    $mother_second_name= $mother->second_name;
                    $mother_third_name= $mother->third_name;
                    $mother_last_name= $mother->last_name;
                    $mother_name= $mother->full_name;
                    $mother_id_card_number= $mother->id_card_number;
                    $mother_birthday= $mother->birthday;
                    $mother_marital_status_name= $mother->marital_status_name;
                    $mother_nationality= $mother->nationality;
                    $mother_en_first_name= $mother->en_first_name;
                    $mother_en_second_name= $mother->en_second_name;
                    $mother_en_third_name= $mother->en_third_name;
                    $mother_en_last_name= $mother->en_last_name;
                    $mother_en_name= $mother->en_name;
                    $mother_health_status= $mother->health_status;
                    $mother_health_details= $mother->health_details;
                    $mother_diseases_name= $mother->diseases_name;
                    $mother_specialization= $mother->specialization;
                    $mother_stage= $mother->stage;
                    $mother_alive= $mother->alive;
                    $mother_is_alive= $mother->is_alive;
                    $mother_death_date= $mother->death_date;
                    $mother_death_cause_name= $mother->death_cause_name;
                    $mother_working= $mother->working;
                    $mother_work_job= $mother->work_job;
                    $mother_work_location= $mother->work_location;
                    $mother_monthly_income= $mother->monthly_income;
                    $mother_gender_male= $mother->gender_male;
                    $mother_gender_female= $mother->gender_female;
                    $mother_is_refugee= $mother->is_refugee;
                    $mother_per_refugee= $mother->per_refugee;
                    $mother_per_citizen= $mother->per_citizen;
                    $mother_father_brothers= $mother->father_brothers;
                    $mother_mother_brothers= $mother->mother_brothers;
                    $mother_brothers= $mother->brothers;
                    $mother_children= $mother->children;
                    $mother_male_children= $mother->male_children;
                    $mother_female_children= $mother->female_children;
                    $mother_is_working= $mother->is_working;
                    $mother_not_working= $mother->not_working;
                    $mother_can_working= $mother->can_working;
                    $mother_can_not_working= $mother->can_not_working;
                    $mother_health_status= $mother->health_status;
                    $mother_good_health_status= $mother->good_health_status;
                    $mother_diseases_name= $mother->diseases_name;
                    $mother_chronic_disease= $mother->chronic_disease;
                    $mother_disabled= $mother->disabled;


                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                    '.trans('common::application.mother-info').'  
                        </b></td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.full_name').'</b></td>
                       <td width="413" align="center">'.$mother_name.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.id_card').'</b></td>
                       <td width="156" align="center">'.$mother_id_card_number.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.BIRTH_DT').'</b></td>
                       <td width="156" align="center">'.$mother_birthday.'</td>
                       </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.prev_family_name').'</b></td>
                       <td width="156" align="center">'.$mother_prev_family_name.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.SOCIAL_STATUS').'</b></td>
                       <td width="157" align="center">'.$mother_marital_status_name.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.is_she_alive?').'</b></td>
                       <td width="156" align="center">'.$mother_is_alive.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.death_date').'</b></td>
                       <td width="157" align="center">'.$mother_death_date.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.death_cause_id').' </b></td>
                       <td width="413" align="center">'.$mother_death_cause_name.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_educational_level').' </b></td>
                       <td width="156" align="center">'.$mother_stage.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.Specialization').' </b></td>
                       <td width="157" align="center">'.$mother_specialization.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.is_he_working').'</b></td>
                       <td width="156" align="center">'.$mother_working.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_job_').'</b></td>
                       <td width="157" align="center">'.$mother_work_job.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_location').' </b></td>
                       <td width="156" align="center">'.$mother_work_location.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.monthly_income').'</b></td>
                       <td width="157" align="center">'.$mother_monthly_income.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.health_condition').' </b></td>
                       <td width="156" align="center">'.$mother_health_status.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.disease_id').'</b></td>
                       <td width="157" align="center">'.$mother_diseases_name.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.disease_/_disability_details').'</b></td>
                       <td width="413" align="center">'.$mother_health_details.'</td>
                   </tr>';
                    $html .='</table>';
                }



                $html .= '</body></html>';
                PDF::AddPage('P', 'A4');
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

            }

            if (sizeof($item->family_member)!=0){

                $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                $html.= '<h2 style="text-align:center;"> '.trans('common::application.family-data').'

    ('. sizeof($item->family_member) .')</h2>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.name').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.id_card').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.BIRTH_DT').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="50" align="center"><b> '.trans('common::application.gender').'</b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.SOCIAL_STATUS').'</b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.health_condition').' </b></td>
                   </tr>';
                $z=1;
                foreach($item->family_member as $record) {
                    $fm_nam = $record->name;
                    $fm_idc = $record->id_card_number;
                    $fm_bd = $record->birthday;
                    $fm_gen = $record->gender;
                    $fm_age = $record->age;
                    $fm_ms = $record->marital_status;
                    $fm_kin = $record->kinship_name;
                    $fm_stg = $record->stage;
                    $fm_lvl = $record->level;
                    $fm_yer = $record->year;
                    $fm_grd = $record->grade;
                    $fm_scl = $record->school;
                    $fm_hs = $record->health_status;
                    $fm_disnm = $record->diseases_name;
                    $fm_working = $record->working;
                    $fm_job = $record->work_job;
                    $html .= ' <tr>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="120" align="center"><b> '.$fm_nam.'  </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_idc.' </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_bd.'   </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="50" align="center"><b>'.$fm_gen.'</b></td> 
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_ms.' </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$fm_hs.'   </b></td>
                   </tr>';
                    $z++;
//                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="50" align="center"><b>'.$fm_kin.'</b></td>
//                                    <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_working.'   </b></td>
//                <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$fm_job.'   </b></td>
                }
                $html .='</table>';
                $html .='</body></html>';

                PDF::SetMargins(PDF_MARGIN_LEFT, 2, PDF_MARGIN_RIGHT);
                PDF::AddPage('P','A4');
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
            }

            if (sizeof($item->home_indoor)!=0){
                $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                $html.= '<h2 style="text-align:center;"> '.trans('common::application.furniture-info').'</h2>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="200" align="center"><b> '.trans('common::application.content').'  </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.exist').'/'.trans('common::application.not exist').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_status').'   </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.needs').'</b></td>
                   </tr>';

                foreach($item->home_indoor as $record) {

                    $row_es_name = $record->name;
                    $row_es_if_exist= $record->exist;
                    $row_es_needs=is_null($record->needs) ? ' ' : $record->needs;
                    $row_es_condition=is_null($record->essentials_condition) ? ' ' : $record->essentials_condition;

                    $html .= ' <tr>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="200" align="center"><b> '.$row_es_name.'  </b></td>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$row_es_if_exist.' </b></td>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$row_es_condition.'   </b></td>
                       <td style="height: 22px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b>'.$row_es_needs .'</b></td>
                   </tr>';
                }
                $html .='</table>';
                $html .='</body></html>';

                PDF::AddPage('P','A4');
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
            }
            if (sizeof($item->financial_aid_source)!=0){

                $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                $html.= '<h2 style="text-align:center;"> '.trans('common::application.concrete foreign aid data').'</h2>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="200" align="center"><b>  '.trans('common::application.Name of the organization source of assistance').'  </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="130" align="center"><b> '.trans('common::application.Does the family receive help from org?').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.quantity').'   </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.currency_name').' </b></td>
                   </tr>';

                foreach($item->financial_aid_source as $record) {
                    $row_fin_name = $record->name;
                    $row_fin_if_exist= $record->aid_take;
                    $row_fin_count= $record->aid_value;
                    $row_fin_currency= $record->currency_name;
                    $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="200" align="center"><b> '.$row_fin_name.'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="130" align="center"><b> '.$row_fin_if_exist.' </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$row_fin_count.'   </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="80" align="center"><b>'.$row_fin_currency.'</b></td>
                   </tr>';
                }
                $html .='</table>';
                $html .='</body></html>';
                PDF::AddPage('P','A4');
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


            }
            if (sizeof($item->non_financial_aid_source)!=0){
                $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                $html.= '<h2 style="text-align:center;"> '.trans('common::application.concrete foreign aid data').'</h2>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="250" align="center"><b>  '.trans('common::application.Name of the organization source of assistance').'  </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="150" align="center"><b> '.trans('common::application.Does the family receive help from org?').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.quantity').'   </b></td>
                   </tr>';

                foreach($item->non_financial_aid_source as $record) {
                    $row_non_fin_name = $record->name;
                    $row_non_fin_if_exist= $record->aid_take;
                    $row_non_fin_count= $record->aid_value;
                    $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="250" align="center"><b> '.$row_non_fin_name.'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="150" align="center"><b> '.$row_non_fin_if_exist.' </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$row_non_fin_count.'   </b></td>
                   </tr>';

                }

                $html .='</table>';

                $html .='</body></html>';
                PDF::AddPage('P','A4');
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
            }
            if (sizeof($item->persons_properties)!=0){
                $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                $html.= '<h2 style="text-align:center;"> '.trans('common::application.Family property data').'</h2>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="250" align="center"><b>  '.trans('common::application.Naming property').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="140" align="center"><b> '.trans('common::application.exist').'/'.trans('common::application.not exist').' </b></td>
                       <td style="height: 30px ; line-height: 40px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.quantity').'   </b></td>
                   </tr>';
                foreach($item->persons_properties as $record) {
                    $pro_name = $record->name;
                    $has_property= $record->has_property;
                    $quantity= $record->quantity;
                    $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="250" align="center"><b> '.$pro_name.'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="140" align="center"><b> '.$has_property.' </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0;" width="120" align="center"><b> '.$quantity.'   </b></td>
                   </tr>';

                }
                $html .='</table>';

                $html .='</body></html>';
                PDF::AddPage('P','A4');
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);  }

            if (sizeof($item->persons_documents)!=0){
                $i=0;
                foreach($item->persons_documents as $record) {
                    $doc_name = $record->name;
                    $img8=base_path('storage/app/').$record->filepath;
                    $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important; text-align: center !important;">';
                    $hig='700';
                    if($i == 0){
                        $html.= '<h2 style="text-align:center;"> '.trans('common::application.photos of attachments').'</h2>';
                        $hig='650';
                    }
                    $html.= '<h4 style="text-align:right;">'.$doc_name.' : </h4>';
                    $html.='<img src="'.$img8.'" alt="" height="'.$hig.'"  width="510"/>';
                    $html .='</body></html>';
                    PDF::SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


                    $i++;

                }
            }

        }else{
            $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
            $html.= '<h2 style="text-align:center;"> '.$category_name.'</h2>';
            $html .= '<table border="1" style="border: 1px solid black; box-sizing:border-box;">';

            $html .= ' <tr>
                       <td style=" height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.full_name').' </b></td>
                       <td width="325" align="center" >'.$name.'</td>
                        <td width="88" align="center" rowspan="6" style="background-color: white" >'
                .'<img src="'.$img.'" alt="" height="140" />'.
                '</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.id_card').'  </b></td>
                       <td width="120" align="center">'.$id_card_number.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.nationality').'  </b></td>
                       <td width="105" align="center">'.$nationality.'</td>
                      
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.BIRTH_DT').' </b></td>
                       <td width="120" align="center" >'.$birthday.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.birth_place').'</b></td>
                       <td width="105" align="center">'.$birth_place.'</td>
                   </tr>';

            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.SOCIAL_STATUS').' </b></td>
                       <td width="120" align="center">'.$marital_status_name.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.gender').' </b></td>
                       <td width="105" align="center">'.$gender.'</td>
                   </tr>';

            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.citizen').'  / '.trans('common::application.refugee').'</b></td>
                       <td width="120" align="center">'.$refugee.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.unrwa_card_number').'</b></td>
                       <td width="105" align="center">'.$unrwa_card_number.'</td>
                   </tr>';

            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.address').':  </b></td>
                       <td width="325" align="center">'.$address.'</td>
                   </tr>';

            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.guardian_kinship').'</b></td>
                       <td width="413" align="center">' . $guardian_kinship . '</td>
                   </tr>';
            $html .='</table>';
            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                        '.trans('common::application.contact information').'
                        </b></td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.mobile').'</b></td>
                       <td width="156" align="center">'.$primary_mobile.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.secondery_mobile').'</b></td>
                       <td width="157" align="center">'.$secondary_mobile.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.phone').'</b></td>
                       <td width="156" align="center">'.$phone.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.wataniya').'</b></td>
                       <td width="157" align="center">'.$wataniya.'</td>
                   </tr>';
            $html .='</table>';
            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                    '.trans('common::application.education info').'
                        </b></td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.is study').'</b></td>
                       <td width="156" align="center">'.$study.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_type_of_study').'</b></td>
                       <td width="157" align="center">'.$type.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.authority_of_study').'</b></td>
                       <td width="156" align="center">'.$authority.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_educational_level').'</b></td>
                       <td width="156" align="center">'.$stage.'</td>
                       </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.qualification').'</b></td>
                       <td width="156" align="center">'.$degree.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_name_of_education_organization').'</b></td>
                       <td width="157" align="center">'.$school.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.year').' </b></td>
                       <td width="156" align="center">'.$year.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.the_grade').' </b></td>
                       <td width="157" align="center">'.$grade.'</td>
                   </tr>';

            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_study_level').'</b></td>
                       <td width="156" align="center">'.$level.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.points').'</b></td>
                       <td width="157" align="center">'.$points.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.Specialization').'</b></td>
                       <td width="156" align="center">'.$specialization.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  </b></td>
                       <td width="157" align="center">'.''.'</td>
                   </tr>';
            $html .='</table>';

            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                      '.trans('common::application.health info').'
                        </b></td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.health_condition').' </b></td>
                       <td width="156" align="center">'.$health_status.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.disease_id').'</b></td>
                       <td width="157" align="center">'.$diseases_name.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.disease_/_disability_details').'</b></td>
                       <td width="413" align="center">'.$health_details.'</td>
                   </tr>';
            $html .='</table>';

            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                     '.trans('common::application.islamic info').'
                        </b></td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.Dose_he_pray?').'</b></td>
                       <td width="156" align="center">'.$prayer.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.save_quran').'</b></td>
                       <td width="157" align="center">'.$save_quran.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.not_prayer_reason').'</b></td>
                       <td width="413" align="center">'.$prayer_reason.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.quran_parts').'</b></td>
                       <td width="156" align="center">'.$quran_parts.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.quran_chapters').'</b></td>
                       <td width="157" align="center">'.$quran_chapters.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.not save quran reason').'  </b></td>
                       <td width="413" align="center">'.$quran_reason.'</td>
                   </tr>';
            $html .='</table>';
            $html .='<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                       '.trans('common::application.works info').' 
                        </b></td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.is_she_working?').'</b></td>
                       <td width="156" align="center">'.$working.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_job_').' </b></td>
                       <td width="157" align="center">'.$work_job.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_location').' </b></td>
                       <td width="156" align="center">'.$work_location.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.Work_Status').' </b></td>
                       <td width="157" align="center">'.$work_status.'</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.Wage category').':  </b></td>
                       <td width="156" align="center">'.$work_wage.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.monthly_income').'</b></td>
                       <td width="157" align="center">'.' '.'</td>
                   </tr>';
            $html .='</table>';
            $html .='</body></html>';
            PDF::AddPage('P','A4');
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

            $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
            $html .= '<div style="height: 5px; color: white"> - </div>';
            $html .= '<table border="1" style="">';
            $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                       '.trans('common::application.home info').'
                        </b></td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.property_type').'</b></td>
                       <td width="156" align="center">' . $property_types . '</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.property_type').'('.trans('common::application.for renter').') </b></td>
                       <td width="157" align="center">' . $rent_value . '</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.roof_material').'</b></td>
                       <td width="156" align="center">' . $roof_materials . '</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.residence_condition').'</b></td>
                       <td width="157" align="center">' . $house_condition . '</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.indoor_condition').'</b></td>
                       <td width="156" align="center">' . $residence_condition . '</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.habitable').'</b></td>
                       <td width="157" align="center">' . $habitable . '</td>
                   </tr>';
            $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.home_area').'</b></td>
                       <td width="156" align="center">' . $area . '</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.rooms').'</b></td>
                       <td width="157" align="center">' . $rooms . '</td>
                   </tr>';
            $html .= '</table>';

            if($request->get('guardian_detail')){
                if($item->guardian_id != null){
                    $guardian =Person::fetch(array('action'=>'show', 'id' => $item->guardian_id,'person' =>true, 'persons_i18n' => true,
                        'contacts' => true,  'education' => true,
                        'work' => true, 'residence' => true, 'banks' => true));

                    $guardian_first_name= $guardian->first_name;
                    $guardian_age= $guardian->age ;
                    $guardian_second_name= $guardian->second_name;
                    $guardian_third_name= $guardian->third_name ;
                    $guardian_last_name= $guardian->last_name ;
                    $guardian_name= $guardian->full_name;
                    $guardian_id_card_number= $guardian->id_card_number;
                    $guardian_birthday= $guardian->birthday;
                    $guardian_marital_status_name= $guardian->marital_status_name;
                    $guardian_birth_place= $guardian->birth_place;
                    $guardian_nationality= $guardian->nationality;
                    $guardian_country= $guardian->country;
                    $guardian_governarate= $guardian->governarate;
                    $guardian_city= $guardian->city;
                    $guardian_nearLocation= $guardian->nearLocation;
                    $guardian_mosque_name= $guardian->mosque_name;
                    $guardian_street_address= $guardian->street_address;
                    $guardian_address= $guardian->address;
                    $guardian_phone= $guardian->phone;
                    $guardian_primary_mobile= $guardian->primary_mobile;
                    $guardian_wataniya= $guardian->wataniya;
                    $guardian_secondery_mobile= $guardian->secondary_mobile ;
                    $guardian_en_first_name= $guardian->en_first_name;
                    $guardian_en_second_name= $guardian->en_second_name;
                    $guardian_en_third_name= $guardian->en_third_name;
                    $guardian_en_last_name= $guardian->en_last_name;
                    $guardian_en_name= $guardian->en_name;
                    $guardian_property_types= $guardian->property_types;
                    $guardian_rent_value= $guardian->rent_value;
                    $guardian_roof_materials= $guardian->roof_materials;
                    $guardian_area= $guardian->area;
                    $guardian_rooms= $guardian->rooms;
                    $guardian_residence_condition= $guardian->residence_condition;
                    $guardian_house_condition = $guardian->house_condition;
                    $guardian_indoor_condition= $guardian->indoor_condition;
                    $guardian_stage= $guardian->stage;
                    $guardian_working= $guardian->working;
                    $guardian_can_work= $guardian->can_work;
                    $guardian_work_job= $guardian->work_job;
                    $guardian_work_reason= $guardian->work_reason;
                    $guardian_work_status= $guardian->work_status;
                    $guardian_work_wage= $guardian->work_wage;
                    $guardian_work_location= $guardian->work_location;
                    $guardian_monthly_income= $guardian->monthly_income;
                    $guardian_gender_male= $guardian->gender_male;
                    $guardian_gender_female= $guardian->gender_female;
                    $guardian_is_refugee= $guardian->is_refugee;
                    $guardian_per_refugee= $guardian->per_refugee;
                    $guardian_per_citizen= $guardian->per_citizen;
                    $guardian_father_brothers= $guardian->father_brothers;
                    $guardian_mother_brothers= $guardian->mother_brothers;
                    $guardian_brothers= $guardian->brothers;
                    $guardian_children= $guardian->children;
                    $guardian_male_children= $guardian->male_children;
                    $guardian_female_children= $guardian->female_children;
                    $guardian_is_working= $guardian->is_working;
                    $guardian_not_working= $guardian->not_working;
                    $guardian_can_working= $guardian->can_working;
                    $guardian_can_not_working= $guardian->can_not_working;
                    $guardian_health_status= $guardian->health_status;
                    $guardian_good_health_status= $guardian->good_health_status;
                    $guardian_chronic_disease= $guardian->chronic_disease;
                    $guardian_disabled= $guardian->disabled;
                    $guardian_study = $guardian->study;
                    $guardian_type = $guardian->type;
                    $guardian_level = $guardian->level;
                    $guardian_year = $guardian->year;
                    $guardian_points = $guardian->points;
                    $guardian_school = $guardian->school;
                    $guardian_grade = $guardian->grade;
                    $guardian_authority = $guardian->authority;
                    $guardian_stage = $guardian->stage;
                    $guardian_degree = $guardian->degree;
                    $guardian_specialization = $guardian->specialization;

                    $html .='<div style="height: 5px; color: white"> - </div>';
//                    $html.= '<h2 style="text-align:center;">  '.trans('common::application.maintainer info').' </h2>';

                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                        '.trans('common::application.maintainer info').'
                        </b></td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.full_name').'</b></td>
                       <td width="413" align="center">'.$guardian_name.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.id_card').'</b></td>
                       <td width="156" align="center">'.$guardian_id_card_number.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.BIRTH_DT').'</b></td>
                       <td width="156" align="center">'.$guardian_birthday.'</td>
                       </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.SOCIAL_STATUS').'</b></td>
                       <td width="156" align="center">'.$guardian_marital_status_name.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.nationality').'</b></td>
                       <td width="157" align="center">'.$guardian_nationality.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.address').'</b></td>
                       <td width="413" align="center">'.$guardian_address.'</td>
                   </tr>';
                    $html .='</table>';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td style="text-align:center; height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                                               '.trans('common::application.contact information').'
                        </b></td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.mobile').'</b></td>
                       <td width="156" align="center">'.$guardian_primary_mobile.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.secondery_mobile').'</b></td>
                       <td width="157" align="center">'.$guardian_secondery_mobile.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.phone').'</b></td>
                       <td width="156" align="center">'.$guardian_phone.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.wataniya').'</b></td>
                       <td width="157" align="center">'.$guardian_wataniya.'</td>
                   </tr>';
                    $html .='</table>';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1">';
                    $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                      '.trans('common::application.home info').'
                        </b></td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.property_type').'</b></td>
                       <td width="156" align="center">'.$guardian_property_types.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.property_type').'('.trans('common::application.for renter').') </b></td>
                       <td width="157" align="center">'.$guardian_rent_value.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.roof_material').'</b></td>
                       <td width="156" align="center">'.$guardian_roof_materials.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.residence_condition').'</b></td>
                       <td width="157" align="center">'.$guardian_house_condition.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.indoor_condition').'</b></td>
                       <td width="156" align="center">'.$guardian_residence_condition.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.habitable').'</b></td>
                       <td width="157" align="center">'.$guardian_indoor_condition.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.home_area').'</b></td>
                       <td width="156" align="center">'.$guardian_area.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.rooms').'</b></td>
                       <td width="157" align="center">'.$guardian_rooms.'</td>
                   </tr>';
                    $html .='</table>';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                       '.trans('common::application.works info').' 
                        </b></td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.is_she_working?').'  </b></td>
                       <td width="156" align="center">'.$guardian_working.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_job_').' </b></td>
                       <td width="157" align="center">'.$guardian_work_job.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_location').' </b></td>
                       <td width="156" align="center">'.$guardian_work_location.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.Work_Status').' </b></td>
                       <td width="157" align="center">'.$guardian_work_status.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.Wage category').':  </b></td>
                       <td width="156" align="center">'.$guardian_work_wage.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.monthly_income').'</b></td>
                       <td width="157" align="center">'.$guardian_monthly_income.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>   '.trans('common::application.is_can_work').' </b></td>
                       <td width="156" align="center">'.$guardian_can_work.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.work_reason').' </b></td>
                       <td width="157" align="center">'.$guardian_work_reason.'</td>
                   </tr>';
                    $html .='</table>';
                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                    '.trans('common::application.education info').'
                        </b></td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.is study').'</b></td>
                       <td width="156" align="center">'.$guardian_study.'</td>
                      <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_educational_level').'</b></td>
                       <td width="157" align="center">'.$guardian_stage.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.qualification').'</b></td>
                       <td width="156" align="center">'.$guardian_degree.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.Specialization').'</b></td>
                       <td width="157" align="center">'.$guardian_specialization.'</td>
                   </tr>';


                    $html .='</table>';

                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                      '.trans('common::application.health info').'
                        </b></td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.health_condition').' </b></td>
                       <td width="156" align="center">'.$health_status.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.disease_id').'</b></td>
                       <td width="157" align="center">'.$diseases_name.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.disease_/_disability_details').'</b></td>
                       <td width="413" align="center">'.$health_details.'</td>
                   </tr>';
                    $html .='</table>';

                }
            }

            $html .= '</body></html>';
            PDF::AddPage('P', 'A4');
            PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

            if($request->get('parent_detail')){

                $get=array('action' => 'show','id' => $item->father_id,
                    'person'  => true, 'persons_i18n'  =>  true,
                    'health'  =>  true,  'education'  =>  true, 'work'  =>  true);

                $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';

                if($item->father_id != null){
                    $get['id']=$item->father_id;
                    $father =Person::fetch($get);
                    $father_first_name= $father->first_name;
                    $father_second_name= $father->second_name;
                    $father_third_name= $father->third_name ;
                    $father_last_name= $father->last_name ;
                    $father_name= $father->full_name;
                    $father_spouses= $father->spouses;
                    $father_id_card_number= $father->id_card_number;
                    $father_birthday= $father->birthday;
                    $father_en_first_name= $father->en_first_name;
                    $father_en_second_name= $father->en_second_name;
                    $father_en_third_name= $father->en_third_name;
                    $father_en_last_name= $father->en_last_name;
                    $father_en_name= $father->en_name;
                    $father_health_status= $father->health_status;
                    $father_health_details= $father->health_details;
                    $father_diseases_name= $father->diseases_name;
                    $father_specialization= $father->specialization;
                    $father_degree= $father->degree;
                    $father_alive= $father->alive;
                    $father_is_alive= $father->is_alive;
                    $father_death_date= $father->death_date;
                    $father_death_cause_name= $father->death_cause_name;
                    $father_working= $father->working;
                    $father_work_job= $father->work_job;
                    $father_work_location= $father->work_location;
                    $father_monthly_income= $father->monthly_income;
                    $father_gender_male= $father->gender_male;
                    $father_gender_female= $father->gender_female;
                    $father_is_refugee= $father->is_refugee;
                    $father_per_refugee= $father->per_refugee;
                    $father_per_citizen= $father->per_citizen;
                    $father_father_brothers= $father->father_brothers;
                    $father_mother_brothers= $father->mother_brothers;
                    $father_brothers= $father->brothers;
                    $father_children= $father->children;
                    $father_male_children= $father->male_children;
                    $father_female_children= $father->female_children;
                    $father_is_working= $father->is_working;
                    $father_not_working= $father->not_working;
                    $father_can_working= $father->can_working;
                    $father_can_not_working= $father->can_not_working;
                    $father_health_status= $father->health_status;
                    $father_good_health_status= $father->good_health_status;
                    $father_chronic_disease= $father->chronic_disease;
                    $father_disabled= $father->disabled;


                    $html .='<div style="height: 5px; color: white"> - </div>';
                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                    '.trans('common::application.father-info').' 
                        </b></td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.full_name').'</b></td>
                       <td width="413" align="center">'.$father_name.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.id_card').'</b></td>
                       <td width="156" align="center">'.$father_id_card_number.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.BIRTH_DT').'</b></td>
                       <td width="156" align="center">'.$father_birthday.'</td>
                       </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.is_she_alive?').'</b></td>
                       <td width="156" align="center">'.$father_alive.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.death_date').'</b></td>
                       <td width="157" align="center">'.$father_death_date.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.death_cause_id').' </b></td>
                       <td width="413" align="center">'.$father_death_cause_name.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_number_of_spouses').'</b></td>
                       <td width="156" align="center">'.$father_spouses.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.monthly_income').' </b></td>
                       <td width="157" align="center">'.$grade.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.qualification').'</b></td>
                       <td width="156" align="center">'.$father_degree.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.Specialization').' </b></td>
                       <td width="157" align="center">'.$father_specialization.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.is_he_working').'</b></td>
                       <td width="156" align="center">'.$father_working.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_job_').'</b></td>
                       <td width="157" align="center">'.$father_work_job.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_location').' </b></td>
                       <td width="413" align="center">'.$father_work_location.'</td>
                   </tr>';

                    if($item->father_dead == 0){
                        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.health_condition').' </b></td>
                       <td width="156" align="center">'.$father_health_status.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.disease_id').'</b></td>
                       <td width="157" align="center">'.$father_diseases_name.'</td>
                   </tr>';
                        $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.disease_/_disability_details').'</b></td>
                       <td width="413" align="center">'.$father_health_details.'</td>
                   </tr>';
                    }
                    $html .='</table>';
                    $html .='<div style="height: 40px; color: white"> - </div>';
                }

                if($item->mother_id != null){
                    $get['id']=$item->mother_id;
                    $mother =Person::fetch($get);
                    $mother_prev_family_name= $mother->prev_family_name;
                    $mother_first_name= $mother->first_name;
                    $mother_second_name= $mother->second_name;
                    $mother_third_name= $mother->third_name;
                    $mother_last_name= $mother->last_name;
                    $mother_name= $mother->full_name;
                    $mother_id_card_number= $mother->id_card_number;
                    $mother_birthday= $mother->birthday;
                    $mother_marital_status_name= $mother->marital_status_name;
                    $mother_nationality= $mother->nationality;
                    $mother_en_first_name= $mother->en_first_name;
                    $mother_en_second_name= $mother->en_second_name;
                    $mother_en_third_name= $mother->en_third_name;
                    $mother_en_last_name= $mother->en_last_name;
                    $mother_en_name= $mother->en_name;
                    $mother_health_status= $mother->health_status;
                    $mother_health_details= $mother->health_details;
                    $mother_diseases_name= $mother->diseases_name;
                    $mother_specialization= $mother->specialization;
                    $mother_stage= $mother->stage;
                    $mother_alive= $mother->alive;
                    $mother_is_alive= $mother->is_alive;
                    $mother_death_date= $mother->death_date;
                    $mother_death_cause_name= $mother->death_cause_name;
                    $mother_working= $mother->working;
                    $mother_work_job= $mother->work_job;
                    $mother_work_location= $mother->work_location;
                    $mother_monthly_income= $mother->monthly_income;
                    $mother_gender_male= $mother->gender_male;
                    $mother_gender_female= $mother->gender_female;
                    $mother_is_refugee= $mother->is_refugee;
                    $mother_per_refugee= $mother->per_refugee;
                    $mother_per_citizen= $mother->per_citizen;
                    $mother_father_brothers= $mother->father_brothers;
                    $mother_mother_brothers= $mother->mother_brothers;
                    $mother_brothers= $mother->brothers;
                    $mother_children= $mother->children;
                    $mother_male_children= $mother->male_children;
                    $mother_female_children= $mother->female_children;
                    $mother_is_working= $mother->is_working;
                    $mother_not_working= $mother->not_working;
                    $mother_can_working= $mother->can_working;
                    $mother_can_not_working= $mother->can_not_working;
                    $mother_health_status= $mother->health_status;
                    $mother_good_health_status= $mother->good_health_status;
                    $mother_diseases_name= $mother->diseases_name;
                    $mother_chronic_disease= $mother->chronic_disease;
                    $mother_disabled= $mother->disabled;


                    $html .= '<table border="1" style="">';
                    $html .= ' <tr>
                       <td  style="text-align:center;height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="513" align="center"><b>  
                    '.trans('common::application.mother-info').'  
                        </b></td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.full_name').'</b></td>
                       <td width="413" align="center">'.$mother_name.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.id_card').'</b></td>
                       <td width="156" align="center">'.$mother_id_card_number.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.BIRTH_DT').'</b></td>
                       <td width="156" align="center">'.$mother_birthday.'</td>
                       </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.prev_family_name').'</b></td>
                       <td width="156" align="center">'.$mother_prev_family_name.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.SOCIAL_STATUS').'</b></td>
                       <td width="157" align="center">'.$mother_marital_status_name.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.is_she_alive?').'</b></td>
                       <td width="156" align="center">'.$mother_is_alive.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.death_date').'</b></td>
                       <td width="157" align="center">'.$mother_death_date.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.death_cause_id').' </b></td>
                       <td width="413" align="center">'.$mother_death_cause_name.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.the_educational_level').' </b></td>
                       <td width="156" align="center">'.$mother_stage.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.Specialization').' </b></td>
                       <td width="157" align="center">'.$mother_specialization.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.is_he_working').'</b></td>
                       <td width="156" align="center">'.$mother_working.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_job_').'</b></td>
                       <td width="157" align="center">'.$mother_work_job.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.work_location').' </b></td>
                       <td width="156" align="center">'.$mother_work_location.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.monthly_income').'</b></td>
                       <td width="157" align="center">'.$mother_monthly_income.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.health_condition').' </b></td>
                       <td width="156" align="center">'.$mother_health_status.'</td>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>'.trans('common::application.disease_id').'</b></td>
                       <td width="157" align="center">'.$mother_diseases_name.'</td>
                   </tr>';
                    $html .= ' <tr>
                       <td style="height: 20px ; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b>  '.trans('common::application.disease_/_disability_details').'</b></td>
                       <td width="413" align="center">'.$mother_health_details.'</td>
                   </tr>';
                    $html .='</table>';
                }



                $html .= '</body></html>';
                PDF::AddPage('P', 'A4');
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

            }

            $setting_id = 'sponsorship_custom_form';
            $CaseCustomData =FormsCasesData::getCaseCustomData($item->case_id,$item->category_id,$item->organization_id,$setting_id);
            if (sizeof($CaseCustomData)!=0){

                $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                $html.= '<h2 style="text-align:center;">  '.trans('common::application.Custom Form Information').'</h2>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                               <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.organization_data').'  </b></td>
                               <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="400" align="center"><b> '.trans('common::application.explanation').'   </b></td>
                           </tr>';
                $z=1;
                foreach($CaseCustomData as $record) {
                    $c_label = $record['label'];
                    $c_value = $record['value'];

                    if($record['type'] != 3){
                        $html .= ' <tr>
                                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="120" align="center"><b> '.$c_label.'  </b></td>
                                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="400" align="center"><b> '.$c_value.'   </b></td>
                                   </tr>';
                    }else{
                        $html .= ' <tr>
                       <td style="height: 50px ; line-height: 35px; padding: 25px 0 12px 0;" width="120" align="center"><b> '.$c_label.'  </b></td>
                       <td style="height: 50px ; line-height: 35px; padding: 15px 0 12px 0;" width="400" align="center"><b> '.$c_value.'   </b></td>
                   </tr>';

                    }
                    $z++;
                }
                $html .='</table>';
                $html .='</body></html>';

                PDF::SetMargins(PDF_MARGIN_LEFT, 2, PDF_MARGIN_RIGHT);
                PDF::AddPage('P','A4');
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
            }
            if (sizeof($item->family_member)!=0){

                $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 table, th, td { border-collapse: collapse;  font-size: 9px; font: "Simplified Arabic" ;} 
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important;">';
                $html.= '<h2 style="text-align:center;"> '.trans('common::application.family-data').'

    ('. sizeof($item->family_member) .')</h2>';
                $html .= '<table border="1" style="">';
                $html .= ' <tr>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="120" align="center"><b> '.trans('common::application.name').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.id_card').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.BIRTH_DT').'  </b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="50" align="center"><b> '.trans('common::application.gender').'</b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="80" align="center"><b> '.trans('common::application.SOCIAL_STATUS').'</b></td>
                       <td style="height: 20px ; line-height: 25px; padding: 15px 0 12px 0; background-color:#e4f5ff;color:#000000;" width="100" align="center"><b> '.trans('common::application.health_condition').' </b></td>
                   </tr>';
                $z=1;
                foreach($item->family_member as $record) {
                    $fm_nam = $record->name;
                    $fm_idc = $record->id_card_number;
                    $fm_bd = $record->birthday;
                    $fm_gen = $record->gender;
                    $fm_age = $record->age;
                    $fm_ms = $record->marital_status;
                    $fm_kin = $record->kinship_name;
                    $fm_stg = $record->stage;
                    $fm_lvl = $record->level;
                    $fm_yer = $record->year;
                    $fm_grd = $record->grade;
                    $fm_scl = $record->school;
                    $fm_hs = $record->health_status;
                    $fm_disnm = $record->diseases_name;
                    $fm_working = $record->working;
                    $fm_job = $record->work_job;
                    $html .= ' <tr>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="120" align="center"><b> '.$fm_nam.'  </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_idc.' </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_bd.'   </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="50" align="center"><b>'.$fm_gen.'</b></td> 
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_ms.' </b></td>
                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$fm_hs.'   </b></td>
                   </tr>';
                    $z++;
//                       <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="50" align="center"><b>'.$fm_kin.'</b></td>
//                                    <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="80" align="center"><b> '.$fm_working.'   </b></td>
//                <td style="height: 18px ; line-height: 20px; padding: 15px 0 12px 0;" width="100" align="center"><b> '.$fm_job.'   </b></td>
                }
                $html .='</table>';
                $html .='</body></html>';

                PDF::SetMargins(PDF_MARGIN_LEFT, 2, PDF_MARGIN_RIGHT);
                PDF::AddPage('P','A4');
                PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
            }
            if (sizeof($item->persons_documents)!=0){
                $i=0;
                foreach($item->persons_documents as $record) {
                    $doc_name = $record->name;
                    $img8=base_path('storage/app/').$record->filepath;
                    $html = '<!doctype html><html lang="ar">
                             <head>
                             <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                             <style>
                                 .control-label {  padding-top: 7px;     margin-bottom: 0; text-align: right; }
                                 * , body {font-weight:normal !important; font-size: 10px;}
                                 .heading-style{text-align: center !important; font-weight: bold; border: 1px solid black;}
                                 .bolder-content{text-align: right !important; vertical-align: middle!important;}
                                 .vertical-middle{ vertical-align: middle !important;}
                            </style>
                            </head>
                             <body style="font-weight: normal !important; text-align: center !important;">';
                    $hig='700';
                    if($i == 0){
                        $html.= '<h2 style="text-align:center;"> '.trans('common::application.photos of attachments').'</h2>';
                        $hig='650';
                    }
                    $html.= '<h4 style="text-align:right;">'.$doc_name.' : </h4>';
                    $html.='<img src="'.$img8.'" alt="" height="'.$hig.'"  width="510"/>';
                    $html .='</body></html>';
                    PDF::SetMargins(PDF_MARGIN_LEFT, 5, PDF_MARGIN_RIGHT);
                    PDF::AddPage('P','A4');
                    PDF::writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


                    $i++;

                }
            }
        }

        $dirName = base_path('storage/app/templates/CaseForm');
        if (!is_dir($dirName)) {
            @mkdir($dirName, 0777, true);
        }

        PDF::Output($dirName.'/'.$item->category_name.' '.$item->full_name . '.pdf', 'F');
        $token = md5(uniqid());
        $zipFileName = storage_path('tmp/export_' . $token . '.zip');

        $zip = new \ZipArchive;
        if ( $zip->open( $zipFileName, \ZipArchive::CREATE ) === true )
        {
            foreach ( glob( $dirName . '/*' ) as $fileName )
            {
                $file = basename( $fileName );
                $zip->addFile( $fileName, $file );
            }

            $zip->close();
            array_map('unlink', glob("$dirName/*.*"));
            rmdir($dirName);
        }

        return response()->json(['download_token' => $token]);


    }

    public function attachmentInstruction()
    {
        return response()->json(['download_token' => 'instruction-attachment-zip']);
//        return response()->file(base_path('storage/app/templates/instruction-cheque-banks-template.pdf'));
    }

    /************ IMPORT CASES ************/
    public function import(Request $request){

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $validFile=true;

        $duplicated =0;
        $old_person=0;
        $old_case=0;
        $blocked=0;
        $processed=[];
        $success =0;
        $not_on_region =0;
        $acive_on_other_org =0;
        $father_id =Null;
        $mother_id =Null;
        $guardian_id =Null;
        $invalid_cards=[];
        $result=[];
        $processed_=[];
        $notUpdate_ =["first_name",  "second_name", "third_name", "last_name",
            "prev_family_name", "gender", "marital_status_id", "birthday", "death_date" ];

        $notUpdate =["id_card_number", "first_name",  "second_name", "third_name", "last_name",
            "prev_family_name", "gender", "marital_status_id", "birthday", "death_date" ];

        $importFile=$request->file ;
        if ($importFile->isValid()) {

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){
                        if($this->type == 1){

                            $constraint=[
                                "rkm_alhoy_alab", "noaa_albtak_alab", "alasm_alaol_aarby_alab", "alasm_althany_aarby_alab",
                                "alasm_althalth_aarby_alab", "alaaael_aarby_alab", "alasm_alaol_anjlyzy_alab", "alasm_althany_anjlyzy_alab", "alasm_althalth_anjlyzy_alab",
                                "alaaael_anjlyzy_alab", "tarykh_almylad_alab", "aadd_alzojat_alab", "hl_yaaml_alab", "alothyf_alab",
                                "mkan_alaaml_alab", "aldkhl_alshhry_shykl_alab", "almoehl_alaalmy_alab", "altkhss_alab", "tarykh_alofa_alab", "sbb_alofa_alab",
                                "rkm_alhoy_alam","noaa_albtak_alam","alasm_alaol_aarby_alam","alasm_althany_aarby_alam","alasm_althalth_aarby_alam"
                                ,"alaaael_aarby_alam", "asm_alaaael_alsabk_alam","alasm_alaol_anjlyzy_alam"
                                ,"alasm_althany_anjlyzy_alam", "alasm_althalth_anjlyzy_alam", "alaaael_anjlyzy_alam",
                                "tarykh_almylad_alam", "alhal_alajtmaaay_alam","aljnsy_alam","hl_taaml_alam",
                                "alothyf_alam", "mkan_alaaml_alam", "aldkhl_alshhry_shykl_alam", "almoehl_alaalmy_alam",
                                "altkhss_alam", "alhal_alshy_alam", "almrd_ao_aleaaak_alam"
                                ,"tfasyl_alhal_alshy_alam", "tarykh_alofa_alam", "sbb_alofa_alam",
                                "hl_alam_maayl", "rkm_alhoy_almaayl","noaa_albtak_almaayl","alasm_alaol_aarby_almaayl"
                                ,"alasm_althany_aarby_almaayl", "alasm_althalth_aarby_almaayl","alaaael_aarby_almaayl"
                                ,"alasm_alaol_anjlyzy_almaayl","alasm_althany_anjlyzy_almaayl","alasm_althalth_anjlyzy_almaayl"
                                ,"alaaael_anjlyzy_almaayl","tarykh_almylad_almaayl", "alhal_alajtmaaay_almaayl","aljnsy_almaayl"
                                ,"sl_alkrab", "hl_yaaml_almaayl", "alothyf_almaayl", "mkan_alaaml_almaayl",
                                "aldkhl_alshhry_shykl_almaayl","almoehl_alaalmy_almaayl", "almrhl_aldrasy_almaayl","aldol_almaayl","almhafth_almaayl"
                                ,"almdyn_almaayl", "almntk_almaayl", "almsjd_almaayl","tfasyl_alaanoan_almaayl",
                                "mlky_almnzl_almaayl", "skf_almnzl_almaayl", "hal_almskn_almaayl",
                                "hal_alathath_almaayl", "aadd_alghrf_almaayl", "almsah_balmtr_almaayl", "kym_alayjar_almaayl",
                                "rkm_joal_asasy_almaayl", "rkm_joal_ahtyaty_almaayl", "rkm_alotny_almaayl", "rkm_hatf_almaayl",
                                "asm_albnk_almaayl", "alfraa_almaayl", "asm_sahb_alhsab_almaayl", "rkm_alhsab_almaayl",
                                "rkm_alhoy","noaa_albtak","alasm_alaol_aarby","alasm_althany_aarby","alasm_althalth_aarby"
                                ,"alaaael_aarby","alasm_alaol_anjlyzy","alasm_althany_anjlyzy"
                                ,"alasm_althalth_anjlyzy","alaaael_anjlyzy","tarykh_almylad","mkan_almylad","aljns","aljnsy","aldol","almhafth","almdyn","almntk"
                                ,"almsjd", "tfasyl_alaanoan","rkm_joal_asasy","rkm_joal_ahtyaty","rkm_hatf","asm_albnk","alfraa"
                                ,"asm_sahb_alhsab","rkm_alhsab","hl_yaaml","alothyf","mkan_alaaml","aldkhl_alshhry_shykl",
                                "mlky_almnzl", "skf_almnzl", "hal_alathath", "hal_almskn", "aadd_alghrf", "kably_almnzl_llskn", "odaa_almnzl", "almsah_balmtr",
                                "kym_alayjar","alhal_alshy","almrd_ao_aleaaak","tfasyl_alhal_alshy","ydrs_ao_mtaalm","noaa_aldras","jh_aldras","almoehl_alaalmy"
                                ,"almrhl_aldrasy", "altkhss","alsf", "alsn_aldarsy","asm_almdrs","akhr_ntyj","almsto_althsyl"
                                ,"hl_ysly","sbb_trk_alsla","hl_yhfth_alkran","aadd_alajza","aadd_alsor","sbb_trk_althfyth","mlthk_balmsjd"];

                        }
                        else{

                            $constraint =  ["rkm_alhoy","noaa_albtak","alasm_alaol_aarby","alasm_althany_aarby","alasm_althalth_aarby",
                                "alaaael_aarby","alasm_alaol_anjlyzy","alasm_althany_anjlyzy","alasm_althalth_anjlyzy",
                                "alaaael_anjlyzy","tarykh_almylad","mkan_almylad","aljns","aljnsy","aadd_alzojat",
                                "alhal_alajtmaaay","mhjor","hal_almoatn","rkm_krt_altmoyn","aldol","almhafth","almntk",
                                "alhy","almrbaa","almsjd","tfasyl_alaanoan","rkm_joal_asasy","rkm_joal_ahtyaty",
                                "rkm_alotny","rkm_hatf","asm_albnk","alfraa","asm_sahb_alhsab","rkm_alhsab"
                                ,"hl_yaaml","kadr_aal_alaaml","sbb_aadm_alkdr","hal_alaaml","alothyf"
                                ,"mkan_alaaml","fe_alajr","hl_ldyh_msadr_dkhl_akhr"
                                ,"aldkhl_alshhry_shykl","aldkhl_alshhry_alfaaly_shykl",'hl_ldyh_thmm_maly','kym_althmm_balshykl',"mlky_almnzl"
                                ,"skf_almnzl","kably_almnzl_llskn","odaa_almnzl","aadd_alghrf"
                                ,"hal_alathath","hal_almskn","almsah_balmtr","kym_alayjar","hl_bhaj_ltrmym_llmoaem"
                                ,"osf_odaa_almnzl","hl_ldyh_tamyn_shy","noaa_altamyn_alshy","rkm_altamyn_alshy","hl_ystkhdm_jhaz"
                                ,"asm_aljhaz_almstkhdm","alhal_alshy","almrd_ao_alaaaak","tfasyl_alhal_alshy",
                                "hl_oaadt_balbna_mn_kbl_okal_alghoth_ao_jh_akhr","asm_almoess_alty_oaadt_balbna","ahtyajat_alaaael_bshd"
                                ,"asm_albahth_alajtmaaay","rkm_hoy_albahth","tosyat_albahth_alajtmaaay","ray_albahth_alajtmaaay",
                                "tkyym_albahth_lhal_almnzl","tarykh_alzyar"];

                        }
                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if($validFile){
                            $errors=[];
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if($total == 0){
                                return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                            }
                            if ($total < 2000) {
                                $user = \Auth::user();
                                $organization_id = $user->organization_id;
                                $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$request->category_id)->first();

                                foreach ($records as $key =>$value) {
                                    $setOnTransfer = false;

                                    if(in_array($value['rkm_alhoy'],$processed)){
                                        $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'duplicated'];
                                        $duplicated++;
                                    } else{
                                        if(strlen($value['rkm_alhoy'])  <= 9) {
                                            $id_card_number = (int) $value['rkm_alhoy'];

                                            if(BlockIDCard::isBlock($value['rkm_alhoy'],$request->category_id,1)){

                                                $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'blocked'];
                                                $blocked++;
                                                $processed[]=$value['rkm_alhoy'];
                                            }
                                            else{
                                                if(GovServices::checkCard($id_card_number)){
                                                    $isUnique = Person::isUnique($id_card_number);
                                                    if($isUnique['status'] == false) {
                                                        if(Setting::getIgnoreGovServices()){
                                                            $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'not_registered'];
                                                            $invalid_cards[] =$value['rkm_alhoy'];
                                                        }
                                                        else{
                                                            $gov_data= GovServices::byCard($id_card_number);
                                                            if($gov_data['status'] == true) {
                                                                $caseRaw = $gov_data['row'];
                                                                $remap=array();
                                                                $data = CaseModel::reMapInputs($value);

                                                                $case=null;
                                                                if(sizeof($data['case']) >0){
                                                                    $case=CaseModel::filterInputs(true,$request->category_id,$this->type,$data['case']);

                                                                    if(sizeof($case['errors']) >0 ){
                                                                        foreach ($case['errors'] as $ek =>$ev){
                                                                            $errors[] =['id_card_number'=>$id_card_number , 'error' => trans('common::application.'.$ek) , 'reason' => $ev];
                                                                        }
                                                                    }
                                                                }
                                                                if(sizeof($data['mother']) >0){
                                                                    $mother_data = CaseModel::filterInputs(false,null,null,$data['mother']);
                                                                    if(sizeof($mother_data['errors']) >0 ){
                                                                        foreach ($mother_data['errors'] as $ek =>$ev){
                                                                            $errors[] =['id_card_number'=>$mother_data['id_card_number'] , 'error' => trans('common::application.'.$ek) , 'reason' => $ev];
                                                                        }
                                                                    }

                                                                    $mother_data['id_card_number'] = (int) $mother_data['id_card_number'];
                                                                    if(strlen($mother_data['id_card_number'])  <= 9) {
                                                                        if(GovServices::checkCard($mother_data['id_card_number'])){
                                                                            $mother_gov_data= GovServices::byCard($mother_data['id_card_number']);
                                                                            if($mother_gov_data['status'] == true) {
                                                                                $motherIsUnique = Person::isUnique($mother_data['id_card_number']);
                                                                                if($motherIsUnique['status'] == false) {
                                                                                    $caseRaw['MOTHER_IDNO'] = $mother_data['id_card_number'];
                                                                                    CloneGovernmentPersons::saveNew((Object) $mother_gov_data['row']);
                                                                                    $MapBasicToUpdate_M= Person::mapBasicToUpdate($mother_gov_data['row']);

                                                                                    $mother_data['update_en'] = true;
                                                                                    $en = ['en_first_name' , 'en_second_name' ,'en_third_name','en_last_name'];
                                                                                    foreach ($en as $ino){
                                                                                        if(isset($MapBasicToUpdate_M[$ino])){
                                                                                            $mother_data[$ino] =$MapBasicToUpdate_M[$ino];
                                                                                        }
                                                                                    }
                                                                                    foreach ($notUpdate as $kim){
                                                                                        if(isset($MapBasicToUpdate_M[$kim])){
                                                                                            $mother_data[$kim] =$MapBasicToUpdate_M[$kim];
                                                                                        }
                                                                                    }
                                                                                    $mother = Person::savePerson($mother_data);
                                                                                    $case['mother_id']=$mother['id'];
                                                                                }else{
                                                                                    $case['mother_id']=$motherIsUnique['row']['id'];
                                                                                }

                                                                                if(isset($mother_data['mother_is_guardian'])){
                                                                                    if($mother_data['mother_is_guardian'] == trans('common::application.yes') || $mother_data['mother_is_guardian'] == '1'|| $mother_data['mother_is_guardian'] == 1){
                                                                                        $case['guardian_update']= true;
                                                                                        $case['guardian_id']=$case['mother_id'];
                                                                                        $object= \Setting\Model\KinshipI18n::where(['name'=>trans('common::application.mother') , 'language_id'=>1])->first();
                                                                                        $case['kinship_id']= is_null($object)? null : (int) $object->kinship_id;
                                                                                    }
                                                                                }

                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                if(sizeof($data['father']) >0){
                                                                    $father_data = CaseModel::filterInputs(false,null,null,$data['father']);

                                                                    if(sizeof($father_data['errors']) >0 ){
                                                                        foreach ($father_data['errors'] as $ek =>$ev){
                                                                            $errors[] =['id_card_number'=>$father_data['id_card_number'] , 'error' => trans('common::application.'.$ek) , 'reason' => $ev];
                                                                        }
                                                                    }

                                                                    if(strlen($father_data['id_card_number'])  <= 9) {
                                                                        if(GovServices::checkCard($father_data['id_card_number'])){
                                                                            $father_gov_data= GovServices::byCard($father_data['id_card_number']);
                                                                            if($father_gov_data['status'] == true) {
                                                                                $caseRaw['FATHER_IDNO'] = $father_data['id_card_number'];
                                                                                CloneGovernmentPersons::saveNew((Object) $father_gov_data['row']);
                                                                                $fatherIsUnique = Person::isUnique($father_data['id_card_number']);
                                                                                if($fatherIsUnique['status'] == false) {
                                                                                    $MapBasicToUpdate_F= Person::mapBasicToUpdate($father_gov_data['row']);
                                                                                    $father_data['update_en'] = true;
                                                                                    $en = ['en_first_name' , 'en_second_name' ,'en_third_name','en_last_name'];
                                                                                    foreach ($en as $ino){
                                                                                        if(isset($MapBasicToUpdate_F[$ino])){
                                                                                            $father_data[$ino] =$MapBasicToUpdate_F[$ino];
                                                                                        }
                                                                                    }
                                                                                    foreach ($notUpdate as $kif){
                                                                                        if(isset($MapBasicToUpdate_F[$kif])){
                                                                                            $father_data[$kif] =$MapBasicToUpdate_F[$kif];
                                                                                        }
                                                                                    }

                                                                                    $father = Person::savePerson($father_data);
                                                                                    $case['father_id']=$father['id'];
                                                                                }else{
                                                                                    $case['father_id']=$fatherIsUnique['row']['id'];
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                if(sizeof($data['guardian']) >0){
                                                                    $guardian_data = CaseModel::filterInputs(false,null,null,$data['guardian']);

                                                                    if(sizeof($guardian_data['errors']) >0 ){
                                                                        foreach ($guardian_data['errors'] as $ek =>$ev){
                                                                            $errors[] =['id_card_number'=>$guardian_data['id_card_number'] , 'error' => trans('common::application.'.$ek) , 'reason' => $ev];
                                                                        }
                                                                    }

                                                                    $guardian_data['id_card_number'] = (int)$guardian_data['id_card_number'];
                                                                    if(strlen($guardian_data['id_card_number'])  <= 9) {
                                                                        if(GovServices::checkCard($guardian_data['id_card_number'])){
                                                                            $guardian_gov_data= GovServices::byCard($guardian_data['id_card_number']);
                                                                            if($guardian_gov_data['status'] == true) {
                                                                                CloneGovernmentPersons::saveNew((Object) $guardian_gov_data['row']);
                                                                                $guardianIsUnique = Person::isUnique($guardian_data['id_card_number']);
                                                                                if($guardianIsUnique['status'] == false) {
                                                                                    $MapBasicToUpdate_G= Person::mapBasicToUpdate($guardian_gov_data['row']);

                                                                                    $guardian_data['update_en'] = true;
                                                                                    $en = ['en_first_name' , 'en_second_name' ,'en_third_name','en_last_name'];
                                                                                    foreach ($en as $ino){
                                                                                        if(isset($MapBasicToUpdate_G[$ino])){
                                                                                            $guardian_data[$ino] =$MapBasicToUpdate_G[$ino];
                                                                                        }
                                                                                    }

                                                                                    foreach ($notUpdate as $kif){
                                                                                        if(isset($MapBasicToUpdate_G[$kif])){
                                                                                            $guardian_data[$kif] =$MapBasicToUpdate_G[$kif];
                                                                                        }
                                                                                    }

                                                                                    $guardian = Person::savePerson($guardian_data);
                                                                                    $guardian_id=$guardian['id'];

                                                                                }else{
                                                                                    $guardian_id=$guardianIsUnique['row']['id'];
                                                                                }

                                                                                if(isset($guardian_data['kinship_id'])){
                                                                                    $case['kinship_id']= $guardian_data['kinship_id'];
                                                                                    $case['guardian_id']= $guardian_id;
                                                                                    $case['guardian_update']= true;
                                                                                }

                                                                            }
                                                                        }
                                                                    }
                                                                }


                                                                $MapBasicToUpdate= Person::mapBasicToUpdate($caseRaw);

                                                                $case['update_en'] = true;
                                                                $en = ['en_first_name' , 'en_second_name' ,'en_third_name','en_last_name'];
                                                                foreach ($en as $ino){
                                                                    if(isset($MapBasicToUpdate[$ino])){
                                                                        $case[$ino] =$MapBasicToUpdate[$ino];
                                                                    }
                                                                }

                                                                foreach ($notUpdate as $ino){
                                                                    if(isset($MapBasicToUpdate[$ino])){
                                                                        $case[$ino] =$MapBasicToUpdate[$ino];
                                                                    }
                                                                }

                                                                CloneGovernmentPersons::saveNew((Object) $caseRaw);
                                                                $person =Person::savePerson($case);
                                                                $person_id = $person['id'];

                                                                $person_ =Person::where('id',$person_id)->first();
                                                                $passed = true;

                                                                if (sizeof($caseRaw->all_relatives) > 0 ){

                                                                    $PARENT_RELATIVE_DESC = ["أب" , "أم","اب" , "ام"];
                                                                    $SPONSES_RELATIVE_DESC = ["زوج/ة"];
                                                                    $CHILD_RELATIVE_DESC = ["ابن/ة"];

                                                                    foreach ($caseRaw->all_relatives as $kkk=> $relative) {
                                                                        if(in_array($relative->RELATIVE_DESC ,$CHILD_RELATIVE_DESC) ||
                                                                            in_array($relative->RELATIVE_DESC ,$SPONSES_RELATIVE_DESC)||
                                                                            in_array($relative->RELATIVE_DESC ,$PARENT_RELATIVE_DESC)) {
                                                                            $crd = $relative->IDNO_RELATIVE;
                                                                            $map = GovServices::inputsMap($relative);
                                                                            $map['kinship_id'] =$relative->kinship_id;
                                                                            $map['kinship_name']=$relative->kinship_name;

                                                                            $find=Person::where('id_card_number',$crd)->first();
                                                                            if(!is_null($find)){
                                                                                $map['person_id'] = $find->id;
                                                                            }

                                                                            if(in_array($relative->RELATIVE_DESC ,$CHILD_RELATIVE_DESC)){
                                                                                if($person_->gender == 1){
                                                                                    $map['father_id']= $person_->id;
                                                                                }else{
                                                                                    $map['mother_id']= $person_->id;
                                                                                }
                                                                            }

                                                                            if(($person_->gender == 1) &&
                                                                                in_array($relative->RELATIVE_DESC ,$SPONSES_RELATIVE_DESC)){
                                                                                $map['husband_id']= $person_->id;
                                                                            }

                                                                            $relative_row = Person::savePerson($map);

                                                                            if(in_array($relative->RELATIVE_DESC ,$PARENT_RELATIVE_DESC)){
                                                                                if($person_->gender == 1){
                                                                                    $person_->father_id= $relative_row['id'];
                                                                                }else{
                                                                                    $person_->mother_id= $relative_row['id'];
                                                                                }
                                                                            }
                                                                            if(($person_->gender == 2) &&
                                                                                in_array($relative->RELATIVE_DESC ,$SPONSES_RELATIVE_DESC)){
                                                                                $person_->husband_id= $relative_row['id'];
                                                                            }

                                                                            CloneGovernmentPersons::saveNewWithRelation($crd,$relative);
                                                                        }
                                                                    }
                                                                }

                                                                if($this->type == 2){
                                                                    CaseModel::DisablePersonInNoConnected($person_,$user->organization_id,$user->id);
                                                                    $mainConnector=OrgLocations::getConnected($user->organization_id);
                                                                    if(!is_null($person_->adsdistrict_id)){
                                                                        if(!in_array($person_->adsdistrict_id,$mainConnector)){
                                                                            $passed = false;
                                                                        }else{
                                                                            if(!is_null($person_->adsregion_id)){
                                                                                if(!in_array($person_->adsregion_id,$mainConnector)){
                                                                                    $passed = false;
                                                                                }else{

                                                                                    if(!is_null($person_->adsneighborhood_id)){
                                                                                        if(!in_array($person_->adsneighborhood_id,$mainConnector)){
                                                                                            $passed = false;
                                                                                        }else{

                                                                                            if(!is_null($person_->adssquare_id)){
                                                                                                if(!in_array($person_->adssquare_id,$mainConnector)){
                                                                                                    $passed = false;
                                                                                                }else{
                                                                                                    if(!is_null($person_->adsmosques_id)){
                                                                                                        if(!in_array($person_->adsmosques_id,$mainConnector)){
                                                                                                            $passed = false;
                                                                                                        }
                                                                                                    }else{
                                                                                                        $passed = false;
                                                                                                    }
                                                                                                }
                                                                                            }else{
                                                                                                $passed = false;
                                                                                            }
                                                                                        }
                                                                                    }else{
                                                                                        $passed = false;
                                                                                    }
                                                                                }
                                                                            }else{
                                                                                $passed = false;
                                                                            }
                                                                        }
                                                                    }else{
                                                                        $passed = false;
                                                                    }
                                                                }

                                                                if($passed == true) {
                                                                    $success++;
                                                                }
                                                                else{
                                                                    CaseModel::where(['person_id'=>$person_id, 'organization_id' => $user->organization_id ,
                                                                        'category_id'=>$request->category_id])->delete();

                                                                    $full_name = $person_->first_name. ' ' .$person_->second_name. ' ' .$person_->third_name. ' ' .$person_->last_name;

                                                                    // aids
                                                                    if($this->type == 2) {
                                                                        $transferObj =  Transfers::where(['person_id'=> $case->person_id,
                                                                            'category_id' => $request->category_id ,
                                                                            'prev_adscountry_id'=> $person['prev_adscountry_id'], 'prev_adsdistrict_id'=> $person['prev_adsdistrict_id'],
                                                                            'prev_adsregion_id'=> $person['prev_adsregion_id'], 'prev_adsneighborhood_id'=> $person['prev_adsneighborhood_id'],
                                                                            'prev_adssquare_id'=> $person['prev_adssquare_id'], 'prev_adsmosques_id'=> $person['prev_adsmosques_id'],
                                                                            'adscountry_id'=> $person_->adscountry_id, 'adsdistrict_id'=> $person_->adsdistrict_id,
                                                                            'adsregion_id'=> $person_->adsregion_id, 'adsneighborhood_id'=> $person_->adsneighborhood_id,
                                                                            'adssquare_id'=> $person_->adssquare_id, 'adsmosques_id'=> $person_->adsmosques_id])->first();

                                                                        if($person_->prev_adsmosques_id != $person_->adsmosques_id){
                                                                            if(!$transferObj){
                                                                                Transfers::create([ 'organization_id' => $user->organization_id ,
                                                                                    'user_id' => $user->id ,
                                                                                    'category_id' => $request->category_id ,
                                                                                    'person_id' => $person_id,
                                                                                    'prev_adscountry_id'=> $person['prev_adscountry_id'], 'prev_adsdistrict_id'=> $person['prev_adsdistrict_id'],
                                                                                    'prev_adsregion_id'=> $person['prev_adsregion_id'], 'prev_adsneighborhood_id'=> $person['prev_adsneighborhood_id'],
                                                                                    'prev_adssquare_id'=> $person['prev_adssquare_id'], 'prev_adsmosques_id'=> $person['prev_adsmosques_id'],
                                                                                    'adscountry_id'=> $person_->adscountry_id, 'adsdistrict_id'=> $person_->adsdistrict_id,
                                                                                    'adsregion_id'=> $person_->adsregion_id, 'adsneighborhood_id'=> $person_->adsneighborhood_id,
                                                                                    'adssquare_id'=> $person_->adssquare_id, 'adsmosques_id'=> $person_->adsmosques_id,
                                                                                    'created_at' => date('Y-m-d H:i:s')]);

                                                                                $levelId = null;
                                                                                if(is_null($person_->adsmosques_id)) {
                                                                                    if(is_null($person_->adssquare_id)) {
                                                                                        if(is_null($person_->adsneighborhood_id)) {
                                                                                            if(is_null($person_->adsregion_id)) {
                                                                                                if(is_null($person_->adsdistrict_id)) {
                                                                                                    if(is_null($person_->adscountry_id)) {
                                                                                                        $levelId = $person_->adscountry_id ;
                                                                                                    }
                                                                                                }else{
                                                                                                    $levelId = $person_->adsdistrict_id ;
                                                                                                }
                                                                                            }else{
                                                                                                $levelId = $person_->adsregion_id ;
                                                                                            }
                                                                                        }else{
                                                                                            $levelId = $person_->adsneighborhood_id ;
                                                                                        }
                                                                                    }else{
                                                                                        $levelId = $person_->adssquare_id ;
                                                                                    }
                                                                                }
                                                                                else{
                                                                                    $levelId = $person_->adsmosques_id ;
                                                                                }

                                                                                $orgs =  OrgLocations::LocationOrgs($levelId,$person_->id,$request->category_id);
                                                                                $users =User::userHasPermission('reports.transfers.manage',$orgs);
                                                                                $url = "#/transfers";
                                                                                \Log\Model\Notifications::saveNotification([
                                                                                    'user_id_to' =>$users,
                                                                                    'url' =>$url,
                                                                                    'message' => trans('common::application.he migrated') . ': ' . ' ' . $full_name . ' , '.
                                                                                        trans('common::application.As a case') . ': ' . $categoryObj->name
                                                                                ]);

                                                                                \Log\Model\Log::saveNewLog('RELAY_CREATED',
                                                                                    trans('common::application.he migrated') . ': ' . ' ' . $full_name. ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name);
                                                                            }
                                                                        }

                                                                    }
                                                                    else{

                                                                        $relaysObj =  Relays::where(['person_id'=>$person_id,'category_id' => $request->category_id])->first();
                                                                        if(!$relaysObj){
                                                                            Relays::create(['person_id'=> $person_id,
                                                                                'category_id' => $request->category_id,
                                                                                'user_id'=> $user->id,
                                                                                'organization_id' => $user->organization_id]);

                                                                            $users =User::userHasPermission('reports.relays.confirm',[]);
                                                                            $url = "#/relays/sponsorships";
                                                                            \Log\Model\Notifications::saveNotification([
                                                                                'user_id_to' =>$users,
                                                                                'url' =>$url,
                                                                                'message' => trans('common::application.he migrated') . ': ' . ' ' . $full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name
                                                                            ]);
                                                                            \Log\Model\Log::saveNewLog('RELAY_CREATED',trans('common::application.he migrated') . ': ' . ' ' . $full_name. ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name);
                                                                        }
                                                                    }
                                                                    $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'out_of_regions'];
                                                                    $not_on_region++;
                                                                }
                                                            }else{
                                                                $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'invalid_id_card_number'];
                                                                $invalid_cards[] =$value['rkm_alhoy'];
                                                            }
                                                        }
                                                    }
                                                    else {
                                                        $person=$isUnique['row'];
                                                        $person_id=$person->id;
                                                        $id_card_number_=$person->id_card_number;

                                                        $remap=array();
                                                        $data = CaseModel::reMapInputs($value);
                                                        $case=null;

                                                        if(sizeof($data['case']) >0){
                                                            $case=CaseModel::filterInputs(true,$request->category_id,$this->type,$data['case']);

                                                            if(sizeof($case['errors']) >0 ){
                                                                foreach ($case['errors'] as $ek =>$ev){
                                                                    $errors[] =['id_card_number'=>$id_card_number , 'error' => trans('common::application.'.$ek) , 'reason' => $ev];
                                                                }
                                                            }


                                                        }

                                                        if(sizeof($data['mother']) >0){
                                                            $mother_data = CaseModel::filterInputs(false,null,null,$data['mother']);

                                                            if(sizeof($mother_data['errors']) >0 ){
                                                                foreach ($mother_data['errors'] as $ek =>$ev){
                                                                    $errors[] =['id_card_number'=>$mother_data['id_card_number'] , 'error' => trans('common::application.'.$ek) , 'reason' => $ev];
                                                                }
                                                            }

                                                            $mother_data['id_card_number'] = (int) $mother_data['id_card_number'];
                                                            if(strlen($mother_data['id_card_number'])  <= 9) {
                                                                if(GovServices::checkCard($mother_data['id_card_number'])){
                                                                    if (!Setting::getIgnoreGovServices()){
                                                                        $mother_gov_data= GovServices::byCard($mother_data['id_card_number']);
                                                                        if($mother_gov_data['status'] == true) {
                                                                            CloneGovernmentPersons::saveNew((Object) $mother_gov_data['row']);
                                                                            $motherIsUnique = Person::isUnique($mother_data['id_card_number']);
                                                                            if($motherIsUnique['status'] == false) {
                                                                                $MapBasicToUpdate_M= Person::mapBasicToUpdate($mother_gov_data['row']);
                                                                                foreach ($notUpdate as $kim){
                                                                                    if(isset($MapBasicToUpdate_M[$kim])){
                                                                                        $mother_data[$kim] =$MapBasicToUpdate_M[$kim];
                                                                                    }
                                                                                }
                                                                                $mother = Person::savePerson($mother_data);
                                                                                $case['mother_id']=$mother['id'];
                                                                            }else{
                                                                                $case['mother_id']=$motherIsUnique['row']['id'];
                                                                            }

                                                                            if(isset($mother_data['mother_is_guardian'])){
                                                                                if($mother_data['mother_is_guardian'] == trans('common::application.yes') || $mother_data['mother_is_guardian'] == '1'|| $mother_data['mother_is_guardian'] == 1){
                                                                                    $case['guardian_update']= true;
                                                                                    $case['guardian_id']=$case['mother_id'];
                                                                                    $object= \Setting\Model\KinshipI18n::where(['name'=>trans('common::application.mother') , 'language_id'=>1])->first();
                                                                                    $case['kinship_id']= is_null($object)? null : (int) $object->kinship_id;
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        if(sizeof($data['father']) >0){
                                                            $father_data = CaseModel::filterInputs(false,null,null,$data['father']);

                                                            if(sizeof($father_data['errors']) >0 ){
                                                                foreach ($father_data['errors'] as $ek =>$ev){
                                                                    $errors[] =['id_card_number'=>$father_data['id_card_number'] , 'error' => trans('common::application.'.$ek) , 'reason' => $ev];
                                                                }
                                                            }

                                                            $father_data['id_card_number'] = (int)$father_data['id_card_number'];
                                                            if(strlen($father_data['id_card_number'])  <= 9) {
                                                                if(GovServices::checkCard($father_data['id_card_number'])){
                                                                    if (!Setting::getIgnoreGovServices()){
                                                                        $father_gov_data= GovServices::byCard($father_data['id_card_number']);
                                                                        if($father_gov_data['status'] == true) {
                                                                            CloneGovernmentPersons::saveNew((Object) $father_gov_data['row']);

                                                                            $fatherIsUnique = Person::isUnique($father_data['id_card_number']);
                                                                            if($fatherIsUnique['status'] == false) {
                                                                                $MapBasicToUpdate_F= Person::mapBasicToUpdate($father_gov_data['row']);
                                                                                foreach ($notUpdate as $ki){
                                                                                    $father_data[$ki] =$MapBasicToUpdate_F[$ki];
                                                                                }

                                                                                $father = Person::savePerson($father_data);
                                                                                $case['father_id']=$father['id'];

                                                                            }else{
                                                                                $case['father_id']=$fatherIsUnique['row']['id'];
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }

                                                        if(sizeof($data['guardian']) >0){
                                                            $guardian_data = CaseModel::filterInputs(false,null,null,$data['guardian']);

                                                            if(sizeof($guardian_data['errors']) >0 ){
                                                                foreach ($guardian_data['errors'] as $ek =>$ev){
                                                                    $errors[] =['id_card_number'=>$guardian_data['id_card_number'] , 'error' => trans('common::application.'.$ek) , 'reason' => $ev];
                                                                }
                                                            }

                                                            $guardian_data['id_card_number'] = (int)$guardian_data['id_card_number'];
                                                            if(strlen($guardian_data['id_card_number'])  <= 9) {
                                                                if(GovServices::checkCard($guardian_data['id_card_number'])){
                                                                    if (!Setting::getIgnoreGovServices()){
                                                                        $guardian_gov_data= GovServices::byCard($guardian_data['id_card_number']);
                                                                        if($guardian_gov_data['status'] == true) {
                                                                            CloneGovernmentPersons::saveNew((Object) $guardian_gov_data['row']);
                                                                            $guardianIsUnique = Person::isUnique($guardian_data['id_card_number']);
                                                                            if($guardianIsUnique['status'] == false) {
                                                                                $MapBasicToUpdate_G= Person::mapBasicToUpdate($guardian_gov_data['row']);
                                                                                foreach ($notUpdate as $ki){
                                                                                    $guardian_data[$ki] =$MapBasicToUpdate_G[$ki];
                                                                                }

                                                                                $guardian = Person::savePerson($guardian_data);
                                                                                $guardian_id=$guardian['id'];

                                                                            }else{
                                                                                $guardian_id=$guardianIsUnique['row']['id'];
                                                                            }

                                                                            if(isset($guardian_data['kinship_id'])){
//                                                            $object= \Setting\Model\KinshipI18n::where(['name'=>$guardian_data['kinship_id'] , 'language_id'=>1])->first();
//                                                            $case['kinship_id']= is_null($object)? null : (int) $object->kinship_id;
                                                                                $case['kinship_id']= $guardian_data['kinship_id'];
                                                                                $case['guardian_id']= $guardian_id;
                                                                                $case['guardian_update']= true;
                                                                            }
                                                                        }


                                                                    }
                                                                }
                                                            }

                                                        }

                                                        $case['person_id']=$person_id;

                                                        foreach ($notUpdate_ as $ino){
                                                            if(isset($case[$ino])){
                                                                unset($case[$ino]);
                                                            }
                                                        }
                                                        $person_ =Person::where('id',$person_id)->first();

                                                        if($case['adsmosques_id'] != $person_->adsmosques_id){
                                                            $founded = OrgLocations::where(['organization_id'=>$user->organization_id ,'location_id'=>$case['adsmosques_id']])->count();
                                                            if($founded == 0 ){
                                                                $transferObj =  Transfers::where(['person_id'=> $person_->id,
                                                                    'prev_adscountry_id'=> $person_->adscountry_id,
                                                                    'prev_adsdistrict_id'=> $person_->adsdistrict_id,
                                                                    'prev_adsregion_id'=> $person_->adsregion_id,
                                                                    'prev_adsneighborhood_id'=> $person_->adsneighborhood_id,
                                                                    'prev_adssquare_id'=> $person_->adssquare_id,
                                                                    'prev_adsmosques_id'=> $person_->adsmosques_id,
                                                                    'adscountry_id'=> $case['adscountry_id'],
                                                                    'adsdistrict_id'=> $case['adsdistrict_id'],
                                                                    'adsregion_id'=> $case['adsregion_id'],
                                                                    'adsneighborhood_id'=> $case['adsneighborhood_id'],
                                                                    'adssquare_id'=> $case['adssquare_id'],
                                                                    'adsmosques_id'=> $case['adsmosques_id'],
                                                                    'category_id' => $request->category_id, ])->first();

                                                                if(!$transferObj){
                                                                    Transfers::create(['created_at' => date('Y-m-d H:i:s'), 'person_id'=> $person_->id,
                                                                        'prev_adscountry_id'=> $person_->adscountry_id,
                                                                        'prev_adsdistrict_id'=> $person_->adsdistrict_id,
                                                                        'prev_adsregion_id'=> $person_->adsregion_id,
                                                                        'prev_adsneighborhood_id'=> $person_->adsneighborhood_id,
                                                                        'prev_adssquare_id'=> $person_->adssquare_id,
                                                                        'prev_adsmosques_id'=> $person_->adsmosques_id,
                                                                        'adscountry_id'=> $case['adscountry_id'],
                                                                        'adsdistrict_id'=> $case['adsdistrict_id'],
                                                                        'adsregion_id'=> $case['adsregion_id'],
                                                                        'adsneighborhood_id'=> $case['adsneighborhood_id'],
                                                                        'adssquare_id'=> $case['adssquare_id'],
                                                                        'adsmosques_id'=> $case['adsmosques_id'],
                                                                        'category_id' => $request->category_id, 'user_id'=> $user->id,
                                                                        'organization_id' => $user->organization_id]);

                                                                    $levelId = $case['adsmosques_id'];

                                                                    $case['adscountry_id'] = $person_->adscountry_id;
                                                                    $case['adsdistrict_id'] = $person_->adsdistrict_id;
                                                                    $case['adsregion_id'] = $person_->adsregion_id;
                                                                    $case['adssquare_id'] = $person_->adssquare_id;
                                                                    $case['adsneighborhood_id'] = $person_->adsneighborhood_id;
                                                                    $case['adsmosques_id'] = $person_->adsmosques_id;
                                                                    $setOnTransfer = true;
                                                                    $orgs =  OrgLocations::LocationOrgs($levelId,$person_->id,$request->category_id);
                                                                    $users =User::userHasPermission('reports.transfers.manage',$orgs);
                                                                    $url = "#/transfers";

                                                                    $full_name = $person_->first_name.' '.$person_->second_name.' '.$person_->third_name.' '.$person_->last_name;
                                                                    if(sizeof($users)>0){
                                                                        \Log\Model\Notifications::saveNotification([
                                                                            'user_id_to' =>$users,
                                                                            'url' =>$url,
                                                                            'message' =>  trans('common::application.he migrated') . ' :' . $full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name
                                                                        ]);
                                                                    }

                                                                    \Log\Model\Log::saveNewLog('TRANSFER_CREATED',trans('common::application.he migrated') . ': ' . ' ' . $full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name);

                                                                }
                                                            }
                                                        }

                                                        $person =Person::savePerson($case);
                                                        CaseModel::DisablePersonInNoConnected($person_,$user->organization_id,$user->id);
                                                        $passed = true;

                                                        if($this->type == 2) {
                                                            $mainConnector=OrgLocations::getConnected($user->organization_id);
                                                            if(!is_null($person_->adsdistrict_id)){
                                                                if(!in_array($person_->adsdistrict_id,$mainConnector)){
                                                                    $passed = false;
                                                                }else{
                                                                    if(!is_null($person_->adsregion_id)){
                                                                        if(!in_array($person_->adsregion_id,$mainConnector)){
                                                                            $passed = false;
                                                                        }else{

                                                                            if(!is_null($person_->adsneighborhood_id)){
                                                                                if(!in_array($person_->adsneighborhood_id,$mainConnector)){
                                                                                    $passed = false;
                                                                                }else{

                                                                                    if(!is_null($person_->adssquare_id)){
                                                                                        if(!in_array($person_->adssquare_id,$mainConnector)){
                                                                                            $passed = false;
                                                                                        }else{
                                                                                            if(!is_null($person_->adsmosques_id)){
                                                                                                if(!in_array($person_->adsmosques_id,$mainConnector)){
                                                                                                    $passed = false;
                                                                                                }
                                                                                            }else{
                                                                                                $passed = false;
                                                                                            }
                                                                                        }
                                                                                    }else{
                                                                                        $passed = false;
                                                                                    }
                                                                                }
                                                                            }else{
                                                                                $passed = false;
                                                                            }
                                                                        }
                                                                    }else{
                                                                        $passed = false;
                                                                    }
                                                                }
                                                            }else{
                                                                $passed = false;
                                                            }
                                                        }


                                                        if($passed == true) {

                                                            if($person['old_case'] == true){
                                                                $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'old_case'];
                                                                $old_case++;
                                                            }else{
                                                                $processed_[]=$value['rkm_alhoy'];
                                                                $success++;

                                                            }

                                                            if(CaseModel::where(['person_id'=>$person_id,
                                                                'organization_id' => $user->organization_id ,
                                                                'category_id'=>$request->category_id])->first()){
//                                                            $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'old_case'];
//                                                            $old_case++;
                                                            }
                                                            else{
                                                                $dates = ['birthday','visited_at','death_date'];
                                                                $carMap =   ["tosyat_albahth_alajtmaaay"=>'notes', "tarykh_alzyar" => "visited_at", "asm_albahth_alajtmaaay" => "visitor"];
                                                                $case = new CaseModel();
                                                                $case->person_id=$person_id;
                                                                $case->category_id=$request->category_id;
                                                                $case->organization_id=$user->organization_id;
                                                                $case->user_id=$user->id;
                                                                $case->rank=0;
                                                                foreach ($carMap as $k=>$v) {
                                                                    if(isset($value[$k])){
                                                                        if(in_array($v,$dates)){
                                                                            $case->$v=date('Y-m-d',strtotime($value[$k]));
                                                                        }else{
                                                                            $case->$v=$value[$k];
                                                                        }
                                                                    }
                                                                }
                                                                $case->save();
                                                                CaseModel::where(['id' => $case->id ])->update(['rank' =>\Common\Model\AbstractCases::updateRank($case->id)]);
                                                            }

                                                        }
                                                        else{
                                                            $case = CaseModel::where(['person_id'=>$person_id,
                                                                'organization_id' => $user->organization_id ,
                                                                'category_id'=>$request->category_id])->first();
                                                            if($case){
                                                                $full_name = $person_->first_name. ' ' .$person_->second_name. ' ' .$person_->third_name. ' ' .$person_->last_name;
                                                                \Common\Model\AidsCases::where('id',$person_id)->update(['status' => 1]);
                                                                $action='CASE_UPDATED';
                                                                $message=trans('common::application.Has edited data') . '  : '.' "'.$full_name. ' " ';
                                                                \Log\Model\Log::saveNewLog($action,$message);
                                                                \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id,
                                                                    'reason'=>trans('common::application.Disabled your recorded status for')  . ' "'.$full_name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization') , 'status'=>1, 'date'=>date("Y-m-d")]);
                                                                \Log\Model\Log::saveNewLog('CASE_UPDATED',trans('common::application.Disabled your recorded status for')  . ' "'.$full_name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization'));
                                                            }


                                                            if($this->type == 2) {

                                                                if(!$setOnTransfer){
                                                                    $transferObj =  Transfers::where(['person_id'=> $person_id,
                                                                        'prev_adscountry_id'=> $person_->adscountry_id,
                                                                        'prev_adsdistrict_id'=> $person_->adsdistrict_id,
                                                                        'prev_adsregion_id'=> $person_->adsregion_id,
                                                                        'prev_adsneighborhood_id'=> $person_->adsneighborhood_id,
                                                                        'prev_adssquare_id'=> $person_->adssquare_id,
                                                                        'prev_adsmosques_id'=> $person_->adsmosques_id,
                                                                        'adscountry_id'=> $person_->adscountry_id,
                                                                        'adsdistrict_id'=> $person_->adsdistrict_id,
                                                                        'adsregion_id'=> $person_->adsregion_id,
                                                                        'adsneighborhood_id'=> $person_->adsneighborhood_id,
                                                                        'adssquare_id'=> $person_->adssquare_id,
                                                                        'adsmosques_id'=> $person_->adsmosques_id,
                                                                        'category_id' => $request->category_id])->first();

                                                                    if(!$transferObj){
                                                                        Transfers::create(['person_id'=> $person_id,
                                                                            'prev_adscountry_id'=> $person_->adscountry_id,
                                                                            'prev_adsdistrict_id'=> $person_->adsdistrict_id,
                                                                            'prev_adsregion_id'=> $person_->adsregion_id,
                                                                            'prev_adsneighborhood_id'=> $person_->adsneighborhood_id,
                                                                            'prev_adssquare_id'=> $person_->adssquare_id,
                                                                            'prev_adsmosques_id'=> $person_->adsmosques_id,
                                                                            'adscountry_id'=> $person_->adscountry_id,
                                                                            'adsdistrict_id'=> $person_->adsdistrict_id,
                                                                            'adsregion_id'=> $person_->adsregion_id,
                                                                            'adsneighborhood_id'=> $person_->adsneighborhood_id,
                                                                            'adssquare_id'=> $person_->adssquare_id,
                                                                            'adsmosques_id'=> $person_->adsmosques_id,
                                                                            'category_id' => $request->category_id,
                                                                            'user_id'=> $user->id, 'organization_id' => $user->organization_id ,
                                                                            'created_at' => date('Y-m-d H:i:s')]);

                                                                        $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$request->category_id)->first();

                                                                        $levelId = null;
                                                                        if(is_null($person_->adsmosques_id)) {
                                                                            if(is_null($person_->adssquare_id)) {
                                                                                if(is_null($person_->adsneighborhood_id)) {
                                                                                    if(is_null($person_->adsregion_id)) {
                                                                                        if(is_null($person_->adsdistrict_id)) {
                                                                                            if(is_null($person_->adscountry_id)) {
                                                                                                $levelId = $person_->adscountry_id ;
                                                                                            }
                                                                                        }else{
                                                                                            $levelId = $person_->adsdistrict_id ;
                                                                                        }
                                                                                    }else{
                                                                                        $levelId = $person_->adsregion_id ;
                                                                                    }
                                                                                }else{
                                                                                    $levelId = $person_->adsneighborhood_id ;
                                                                                }
                                                                            }else{
                                                                                $levelId = $person_->adssquare_id ;
                                                                            }
                                                                        }
                                                                        else{
                                                                            $levelId = $person_->adsmosques_id ;
                                                                        }

                                                                        $orgs =  OrgLocations::LocationOrgs($levelId,$person_->id,$request->category_id);
                                                                        $users =User::userHasPermission('reports.transfers.manage',$orgs);
                                                                        $url = "#/transfers";
                                                                        \Log\Model\Notifications::saveNotification([
                                                                            'user_id_to' =>$users,
                                                                            'url' =>$url,
                                                                            'message' => trans('common::application.he migrated') . ': ' . ' ' . $full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name
                                                                        ]);

                                                                        \Log\Model\Log::saveNewLog('TRANSFERS_CREATED',trans('common::application.he migrated') . ': ' . ' ' . $full_name. ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name);
                                                                    }
                                                                }

                                                            }else{

                                                                $relaysObj =  Relays::where(['person_id'=>$person_id,'category_id' => $request->category_id])->first();

                                                                if(!$relaysObj){
                                                                    Relays::create(['person_id'=> $person_id,
                                                                        'category_id' => $request->category_id,
                                                                        'user_id'=> $user->id,
                                                                        'organization_id' => $user->organization_id]);

                                                                    $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$request->category_id)->first();
                                                                    $users =User::userHasPermission('reports.relays.confirm',[]);
                                                                    $url = "#/relays/sponsorships";
                                                                    \Log\Model\Notifications::saveNotification([
                                                                        'user_id_to' =>$users,
                                                                        'url' =>$url,
                                                                        'message' => trans('common::application.he migrated') . ': ' . ' ' . $full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name
                                                                    ]);

                                                                    \Log\Model\Log::saveNewLog('RELAY_CREATED',trans('common::application.he migrated') . ': ' . ' ' . $full_name. ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name);
                                                                }
                                                            }
                                                            $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'out_of_regions'];
                                                            $not_on_region++;
                                                        }

                                                    }
                                                    $processed[]=$value['rkm_alhoy'];
                                                }else{
                                                    $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'invalid_id_card_number'];
                                                    $invalid_cards[] =$value['rkm_alhoy'];
                                                }
                                            }
                                        }
                                        else{
                                            $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'invalid_id_card_number'];
                                            $invalid_cards[] =$value['rkm_alhoy'];
                                        }
                                    }
                                }
                                if( ($success == $total)){
                                    return response()->json(['status' => 'success','msg'=> trans('common::application.Successfully imported all cases')]);
                                }
                                else{
                                    $response = [];
                                    if($old_case == $total){
                                        $response['status'] = 'failed';
                                        $response['msg'] =  trans('common::application.All names entered are previously registered for the same type');
                                    }
                                    if($blocked == $total){
                                        $response['status'] = 'failed';
                                        $response['msg'] =  trans('common::application.All card entered are previously blocked to add to this category');
                                    }


                                    if($success != 0){
                                        $response['status'] = 'success';
                                        $response['msg'] = trans('common::application.Some cases were imported successfully, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.The number of incorrect or not registered ID numbers in the system') .' :  ' .sizeof($invalid_cards) . ' ,  ' .
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.How many names you have saved but not saved as cases outside your organization').' :  ' .$not_on_region . ' ,  ' .
                                            trans('common::application.Number of previously blocked to add to this category') .' :  ' .$blocked . ' ,  ' .
                                            trans('common::application.Number of previously registered case names') .' :  ' .$old_case . ' ,  ' .
                                            trans('common::application.active on other organization') .' :  ' .$acive_on_other_org . ' ,  ' .
                                            trans('common::application.Number of repeated cases') .' :  '.$duplicated ;
                                    }
                                    else{
                                        $response['status'] = 'failed';
                                        $response['msg'] = trans('common::application.no cases were imported , as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.The number of incorrect or not registered ID numbers in the system') .' :  ' .sizeof($invalid_cards) . ' ,  ' .
                                            trans('common::application.How many names you have saved but not saved as cases outside your organization').' :  ' .$not_on_region . ' ,  ' .
                                            trans('common::application.Number of previously blocked to add to this category') .' :  ' .$blocked . ' ,  ' .
                                            trans('common::application.Number of previously registered case names') .' :  ' .$old_case . ' ,  ' .
                                            trans('common::application.active on other organization') .' :  ' .$acive_on_other_org . ' ,  ' .
                                            trans('common::application.Number of repeated cases') .' :  '.$duplicated;

                                    }

                                    if (sizeof($result) > 0 || sizeof($errors) > 0 || sizeof($processed_) > 0) {
                                        $token = md5(uniqid());
                                        \Excel::create('export_' . $token, function ($excel) use ($result,$processed_,$errors) {
                                            if(sizeof($processed_) > 0){
                                                $excel->sheet(trans('common::application.saved'), function($sheet) use($processed_) {

                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setHeight(1,40);
                                                    $sheet->getDefaultStyle()->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                    ]);

                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));

                                                    $z= 2;
                                                    foreach($processed_ as $v){
                                                        $sheet ->setCellValue('A'.$z,$v);
                                                        $z++;
                                                    }

                                                });
                                            }
                                            if(sizeof($result) > 0){
                                                $excel->sheet(trans('common::application.excluded'), function ($sheet) use ($result) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('B1',trans('common::application.reason'));
                                                    $z= 2;
                                                    foreach($result as $v){
                                                        $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                        $sheet ->setCellValue('B'.$z,trans('common::application.'.$v['reason']));
                                                        $z++;
                                                    }
                                                });

                                            }

                                            if(sizeof($errors) > 0){
                                                $excel->sheet(trans('common::application.data_errors'), function ($sheet) use ($errors) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('B1',trans('common::application.field_name'));
                                                    $sheet ->setCellValue('C1',trans('common::application.reason'));
                                                    $z= 2;
                                                    foreach($errors as $v){
                                                        $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                        $sheet ->setCellValue('B'.$z,$v['error']);
                                                        $sheet ->setCellValue('C'.$z,$v['reason']);
                                                        $z++;
                                                    }
                                                });

                                            }
                                        })->store('xlsx', storage_path('tmp/'));
                                        $response["download_token"] = $token;
                                        $response["msg"] = $response["msg"] . '  , ' . trans('aid::application.all restricted_');
                                    }

                                    return response()->json($response);
                                }


                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.The number of records in the file must not exceed 2000 records') ]);
                        }

                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }

    /************ IMPORT FAMILY ************/
    public function importFamily(Request $request){

        $user = Auth::user();

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');
        $validFile=true;

        $duplicated =0;
        $processed_ =[];
        $errors =[];
        $old_person=0;
        $not_person=0;
        $processed=[];
        $success =0;
        $result = [];
        $father_id =Null;
        $mother_id =Null;
        $guardian_id =Null;
        $invalid_cards=[];
        $notUpdate_ =[ "first_name",  "second_name", "third_name", "last_name",
            "prev_family_name", "gender", "marital_status_id", "birthday", "death_date" ];

        $notUpdate =["id_card_number", "first_name",  "second_name", "third_name", "last_name",
            "prev_family_name", "gender", "marital_status_id", "birthday", "death_date" ];

        $importFile=$request->file ;
        if ($importFile->isValid()) {

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){
                        if($this->type == 1){
                            $constraint=["rkm_alhoy_alab","alasm_alaol_aarby_alab","alasm_althany_aarby_alab","alasm_althalth_aarby_alab"
                                ,"alaaael_aarby_alab","rkm_alhoy_alam","alasm_alaol_aarby_alam","alasm_althany_aarby_alam","alasm_althalth_aarby_alam",
                                "alaaael_aarby_alam","rkm_alhoy_almaayl","alasm_alaol_aarby_almaayl","alasm_althany_aarby_almaayl","alasm_althalth_aarby_almaayl"
                                ,"alaaael_aarby_almaayl","sl_alkrab","rkm_alhoy","alasm_alaol_aarby","alasm_althany_aarby"
                                ,"alasm_althalth_aarby","alaaael_aarby","alasm_alaol_anjlyzy","alasm_althany_anjlyzy"
                                ,"alasm_althalth_anjlyzy","alaaael_anjlyzy","tarykh_almylad","mkan_almylad","aljns","aljnsy","aldol","almhafth",
                                "almdyn","almntk","almsjd","tfasyl_alaanoan","rkm_joal_asasy","rkm_joal_ahtyaty","rkm_hatf","asm_albnk",
                                "alfraa","asm_sahb_alhsab","rkm_alhsab","hl_yaaml","alothyf","mkan_alaaml" ,"aldkhl_alshhry_shykl",
                                "mlky_almnzl","skf_almnzl","aadd_alghrf","hal_alathath", "hal_almskn", "almsah_balmtr", "kym_alayjar",
                                "alhal_alshy","almrd_ao_aleaaak","tfasyl_alhal_alshy","ydrs_ao_mtaalm"
                                ,"noaa_aldras","jh_aldras","almoehl_alaalmy","almrhl_aldrasy","altkhss","alsf","alsn_aldarsy"
                                ,"asm_almdrs","akhr_ntyj","almsto_althsyl","hl_ysly","sbb_trk_alsla"
                                ,"hl_yhfth_alkran","aadd_alajza","aadd_alsor","sbb_trk_althfyth","mlthk_balmsjd"];



                        }
                        else{
                            $constraint =  ["rkm_alhoy_rb_alasr", "alasm_alaol_aarby_rb_alasr", "alasm_althany_aarby_rb_alasr", "alasm_althalth_aarby_rb_alasr",
                                "alaaael_aarby_rb_alasr", "sl_alkrab_rb_alasr", "rkm_alhoy", "alasm_alaol_aarby", "alasm_althany_aarby", "alasm_althalth_aarby",
                                "alaaael_aarby", "alasm_alaol_anjlyzy", "alasm_althany_anjlyzy", "alasm_althalth_anjlyzy", "alaaael_anjlyzy",
                                "tarykh_almylad", "mkan_almylad", "alhal_alajtmaaay", "aljns", "aljnsy", "hl_yaaml",
                                "alothyf","mkan_alaaml", "aldkhl_alshhry_shykl", "alhal_alshy", "almrd_ao_aleaaak", "tfasyl_alhal_alshy",
                                "ydrs_ao_mtaalm", "noaa_aldras", "jh_aldras", "almoehl_alaalmy", "almrhl_aldrasy", "altkhss", "alsf", "alsn_aldarsy", "asm_almdrs",
                                "akhr_ntyj", "almsto_althsyl", "hl_ysly", "sbb_trk_alsla",
                                "hl_yhfth_alkran","aadd_alajza","aadd_alsor","sbb_trk_althfyth", "mlthk_balmsjd"];
                        }

                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {
                                foreach ($records as $key =>$value) {
                                    if(in_array($value['rkm_alhoy'],$processed)){
                                        $result[] = ['id_card_number' => $value['rkm_alhoy'], 'reason' => 'duplicated'];
                                        $duplicated++;
                                    }
                                    else{
                                        $id_card_number =(int)$value['rkm_alhoy'];
                                        if(strlen($id_card_number)  <= 9) {
                                            $person_data=[];
                                            if(GovServices::checkCard($id_card_number) && !Setting::getIgnoreGovServices()) {
                                                $gov_data= GovServices::byCard($id_card_number);
                                                if($gov_data['status'] == true) {
                                                    CloneGovernmentPersons::saveNew($gov_data['row']);
                                                    $isUnique = Person::isUnique($id_card_number);
                                                    $data = Person::reMapInputs($value);

                                                    $person_data=null;
                                                    if(sizeof($data['person']) >0){
                                                        $person_data = Person::filterInputs($data['person']);

                                                        if(sizeof($person_data['errors']) >0 ){
                                                            foreach ($person_data['errors'] as $ek =>$ev){
                                                                $errors[] =['id_card_number'=>$id_card_number , 'error' => trans('common::application.'.$ek) , 'reason' => $ev];
                                                            }
                                                        }
                                                    }

                                                    if($isUnique['status'] == false) {
                                                        $MapBasicToUpdate= Person::mapBasicToUpdate($gov_data['row']);
                                                        $person_data['update_en'] = true;

                                                        $en = ['en_first_name' , 'en_second_name' ,'en_third_name','en_last_name'];

                                                        foreach ($en as $ino){
                                                            if(isset($MapBasicToUpdate[$ino])){
                                                                $person_data[$ino] =$MapBasicToUpdate[$ino];
                                                            }
                                                        }
                                                        foreach ($notUpdate as $ino){
                                                            if(isset($MapBasicToUpdate[$ino])){
                                                                $person_data[$ino] =$MapBasicToUpdate[$ino];
                                                            }
                                                        }
                                                        $processed_[]=$value['rkm_alhoy'];
                                                        $success++;
                                                    }
                                                    else {
                                                        $result[] = ['id_card_number' => $value['rkm_alhoy'], 'reason' => 'old_person'];
                                                        $old_person++;
                                                        $person=$isUnique['row'];
                                                        $person_data['person_id']=$person->id;
                                                        foreach ($notUpdate_ as $ino){
                                                            if(isset($person_data[$ino])){
                                                                unset($person_data[$ino]);
                                                            }
                                                        }
                                                    }

                                                    if($this->type != 1){
                                                        if(sizeof($data['father']) >0){
                                                            $father_data = Person::filterInputs($data['father']);
                                                            if(sizeof($father_data['errors']) >0 ){
                                                                foreach ($father_data['errors'] as $ek =>$ev){
                                                                    $errors[] =['id_card_number'=>$father_data['id_card_number'] , 'error' => trans('common::application.'.$ek) , 'reason' => $ev];
                                                                }
                                                            }

                                                            if(strlen($father_data['id_card_number'])  <= 9) {
                                                                if(isset($father_data['kinship_id'])){
                                                                    if(!is_null($father_data['kinship_id']) && $father_data['kinship_id'] != " "){
                                                                        if(GovServices::checkCard($father_data['id_card_number'])) {
                                                                            $fatherId = Person::getId($father_data['id_card_number']);
                                                                            if($fatherId){
                                                                                $person_data['l_person_id']=$fatherId;
                                                                                $person_data['l_person_update']=true;
                                                                                $person_data['kinship_id']=$father_data['kinship_id'];
                                                                            }
                                                                        }

                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }

                                                    if(sizeof($person_data) >0){
                                                        $person =Person::savePerson($person_data);
                                                        $person_id=$person['id'];

                                                        if($this->type ==1){
                                                            if(sizeof($data['mother']) >0){
                                                                $update_mother=['person_id'=>$person_id];
                                                                $mother_data = Person::filterInputs($data['mother']);
                                                                if(sizeof($mother_data['errors']) >0 ){
                                                                    foreach ($mother_data['errors'] as $ek =>$ev){
                                                                        $errors[] =['id_card_number'=>$mother_data['id_card_number'] , 'error' => trans('common::application.'.$ek) , 'reason' => $ev];
                                                                    }
                                                                }

                                                                if(strlen($mother_data['id_card_number'])  <= 9) {

                                                                    if(GovServices::checkCard($mother_data['id_card_number'])){
                                                                        $motherId = Person::getId($mother_data['id_card_number']);
                                                                        if($motherId){
                                                                            $update_mother['mother_id']=$motherId;
                                                                            $update_mother['l_person_id']=$motherId;
                                                                            $update_mother['l_person_update']=true;
                                                                            $object= \Setting\Model\KinshipI18n::where(['name'=>trans('common::application.mother') , 'language_id'=>1])->first();
                                                                            $update_mother['kinship_id']= is_null($object)? null : (int) $object->kinship_id;
                                                                            Person::savePerson($update_mother);
                                                                        }
                                                                    }

                                                                }
                                                            }

                                                            if(sizeof($data['father']) >0){
                                                                $update_father=['person_id'=>$person_id];
                                                                $father_data = Person::filterInputs($data['father']);
                                                                if(sizeof($father_data['errors']) >0 ){
                                                                    foreach ($father_data['errors'] as $ek =>$ev){
                                                                        $errors[] =['id_card_number'=>$father_data['id_card_number'] , 'error' => trans('common::application.'.$ek) , 'reason' => $ev];
                                                                    }
                                                                }
                                                                if(strlen($father_data['id_card_number'])  <= 9) {
                                                                    if(GovServices::checkCard($father_data['id_card_number'])) {
                                                                        $fatherId = Person::getId($father_data['id_card_number']);
                                                                        if($fatherId) {
                                                                            $update_father['l_person_id']=$fatherId;
                                                                            $update_father['father_id']=$fatherId;
                                                                            $update_father['l_person_update']=true;
                                                                            $object= \Setting\Model\KinshipI18n::where(['name'=>trans('common::application.father') , 'language_id'=>1])->first();
                                                                            $update_father['kinship_id']= is_null($object)? null : (int) $object->kinship_id;
                                                                            Person::savePerson($update_father);
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                            if(sizeof($data['guardian']) >0){
                                                                $update_guardian=['person_id'=>$person_id];
                                                                $guardian_data = Person::filterInputs($data['guardian']);
                                                                if(sizeof($guardian_data['errors']) >0 ){
                                                                    foreach ($guardian_data['errors'] as $ek =>$ev){
                                                                        $errors[] =['id_card_number'=>$guardian_data['id_card_number'] , 'error' => trans('common::application.'.$ek) , 'reason' => $ev];
                                                                    }
                                                                }
                                                                if(isset($guardian_data['kinship_id'])){
                                                                    if(strlen($guardian_data['id_card_number'])  <= 9) {
                                                                        if(GovServices::checkCard($guardian_data['id_card_number'])) {
                                                                            $guardianId = Person::getId($guardian_data['id_card_number']);
                                                                            if($guardianId) {
                                                                                $update_guardian['l_person_update']=true;
                                                                                $update_guardian['l_person_id']= $guardianId;
                                                                                $update_guardian['guardian_id']= $guardianId;
                                                                                $update_guardian['guardian_update']=true;
                                                                                $object= \Setting\Model\KinshipI18n::where(['name'=>$guardian_data['kinship_id'] , 'language_id'=>1])->first();
                                                                                $update_guardian['kinship_id']= is_null($object)? null : (int) $object->kinship_id;
                                                                                $update_guardian['guardian_update']= true;
                                                                                Person::savePerson($update_guardian);
                                                                            }

                                                                        }
                                                                    }


                                                                }

                                                            }
                                                        }
                                                    }


                                                } else {
                                                    $result[] = ['id_card_number' => $value['rkm_alhoy'], 'reason' => 'not_person'];
                                                    $not_person++;
                                                }


                                            }else{
                                                $result[] = ['id_card_number' => $value['rkm_alhoy'], 'reason' => 'not_person'];
                                                $not_person++;
                                            }
                                        }else{
                                            $result[] = ['id_card_number' => $value['rkm_alhoy'], 'reason' => 'not_person'];
                                            $not_person++;
                                        }
                                        $processed[]=$value['rkm_alhoy'];
                                    }
                                }

                                if( $success == $total && ($old_person ==0 && $not_person == 0 )){
                                    return response()->json(['status' => 'success','msg'=> trans('common::application.Successfully imported all individuals')]);
                                }
                                else{

                                    if($success != 0){
                                        $response["status"] = 'success';
                                        $response["msg"] =
                                            trans('common::application.Some individuals were imported successfully, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.The number of incorrect or not registered ID numbers in the system') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.Number of previously registered individuals whose data has been updated') .' :  ' .$old_person . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated ;
                                    }else{
                                        $response["status"] = 'failed';
                                        $response["msg"] =
                                            trans('common::application.No cases were imported, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.Number of individuals imported').' :  ' .$success . ' ,  ' .
                                            trans('common::application.The number of incorrect or not registered ID numbers in the system') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.Number of previously registered individuals whose data has been updated') .' :  ' .$old_person . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated
                                        ;
                                    }

                                    if (sizeof($result) > 0 || sizeof($errors) > 0 || sizeof($processed_) > 0) {
                                        $token = md5(uniqid());
                                        \Excel::create('export_' . $token, function ($excel) use ($result,$processed_,$errors) {
                                            if(sizeof($processed_) > 0){
                                                $excel->sheet(trans('common::application.saved'), function($sheet) use($processed_) {

                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setHeight(1,40);
                                                    $sheet->getDefaultStyle()->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                    ]);

                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));

                                                    $z= 2;
                                                    foreach($processed_ as $v){
                                                        $sheet ->setCellValue('A'.$z,$v);
                                                        $z++;
                                                    }

                                                });
                                            }
                                            if(sizeof($result) > 0){
                                                $excel->sheet(trans('common::application.excluded'), function ($sheet) use ($result) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);
                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('B1',trans('common::application.reason'));
                                                    $z= 2;
                                                    foreach($result as $v){
                                                        $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                        $sheet ->setCellValue('B'.$z,trans('common::application.'.$v['reason']));
                                                        $z++;
                                                    }
                                                });

                                            }

                                            if(sizeof($errors) > 0){
                                                $excel->sheet(trans('common::application.data_errors'), function ($sheet) use ($errors) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('B1',trans('common::application.field_name'));
                                                    $sheet ->setCellValue('C1',trans('common::application.reason'));
                                                    $z= 2;
                                                    foreach($errors as $v){
                                                        $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                        $sheet ->setCellValue('B'.$z,$v['error']);
                                                        $sheet ->setCellValue('C'.$z,$v['reason']);
                                                        $z++;
                                                    }
                                                });

                                            }

                                        })->store('xlsx', storage_path('tmp/'));
                                        $response["download_token"] = $token;
                                        $response["msg"] = $response["msg"] . '  , ' . trans('aid::application.all restricted_');
                                    }

                                    return response()->json($response);

                                }
                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }

                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);

            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }

    public function importTpUpdateTemplate(){
        return response()->json(['download_token' => 'import-to-update-template']);
    }

    /************ UPDATE STATUS FROM FILE ************/
    public function importToUpdate(Request $request){


        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $validFile=false;

        $success =0;
        $duplicated =0;
        $not_person=0;
        $active_in_other =0;
        $theSame=0;
        $invalid_card=0;
        $not_case=0;
        $restricted=0;
        $dead_person =0;
        $processed=[];
        $invalid_cards=[];

        $importFile=$request->file ;
        if ($importFile->isValid()) {

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){
                        foreach ($keys as $item) {
                            if($item == 'rkm_alhoy'){
                                $validFile = true;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {
                                $user = \Auth::user();

                                foreach ($records as $key =>$value) {
                                    if(in_array($value['rkm_alhoy'],$processed)){
                                        $invalid_cards[]=['card'=>$value['rkm_alhoy'],'reason'=>'duplicated'];
                                        $duplicated++;
                                    }
                                    else{
                                        if(strlen($value['rkm_alhoy'])  <= 9) {

                                            $id_card_number = (int)$value['rkm_alhoy'];

                                            if(GovServices::checkCard($id_card_number)){
                                                $person=Person::fetch(array(
                                                    'id_card_number' => $id_card_number,
                                                    'category_id' => $request->get('category_id'),
                                                    'organization_id' => $user->organization_id,
                                                    'action'         => 'show'
                                                ));

                                                if($person){

                                                    if($person->case_id != null){
                                                        $passed = true;

                                                        if( !($person->death_date == '-') && $request->status == 0){
                                                            $invalid_cards[]=['card'=>$value['rkm_alhoy'],'reason'=>'dead_person'];
                                                            $dead_person++;

                                                        }else{
                                                            if($person->case_status == $request->status){
                                                                $invalid_cards[]=['card'=>$value['rkm_alhoy'],'reason'=>'has_same_status'];
                                                                $theSame++;
                                                            }else{

                                                                if($request->category_id == 5  && $request->status == 0  && ( $person->marital_status_id == 10 ||  $person->marital_status_id == '10')){
                                                                    $invalid_cards[]=['card'=>$value['rkm_alhoy'],'reason'=>'single_person'];
                                                                    $restricted++;
                                                                }else{
                                                                    if($this->type == 2 && $request->status == 0 && $person->active_case_count != 0){
                                                                        $invalid_cards[]=['card'=>$value['rkm_alhoy'],'reason'=>'active_in_other'];
                                                                        $active_in_other++;
                                                                    }else{
                                                                        if($this->type == 2 && $request->status == 0){

                                                                            $mainConnector=OrgLocations::getConnected($user->organization_id);
                                                                            if(!is_null($person->adsdistrict_id)){
                                                                                if(!in_array($person->adsdistrict_id,$mainConnector)){
                                                                                    $passed = false;
                                                                                }else{
                                                                                    if(!is_null($person->adsregion_id)){
                                                                                        if(!in_array($person->adsregion_id,$mainConnector)){
                                                                                            $passed = false;
                                                                                        }else{

                                                                                            if(!is_null($person->adsneighborhood_id)){
                                                                                                if(!in_array($person->adsneighborhood_id,$mainConnector)){
                                                                                                    $passed = false;
                                                                                                }else{

                                                                                                    if(!is_null($person->adssquare_id)){
                                                                                                        if(!in_array($person->adssquare_id,$mainConnector)){
                                                                                                            $passed = false;
                                                                                                        }else{
                                                                                                            if(!is_null($person->adsmosques_id)){
                                                                                                                if(!in_array($person->adsmosques_id,$mainConnector)){
                                                                                                                    $passed = false;
                                                                                                                }
                                                                                                            }else{
                                                                                                                $passed = false;
                                                                                                            }
                                                                                                        }
                                                                                                    }else{
                                                                                                        $passed = false;
                                                                                                    }
                                                                                                }
                                                                                            }else{
                                                                                                $passed = false;
                                                                                            }
                                                                                        }
                                                                                    }else{
                                                                                        $passed = false;
                                                                                    }
                                                                                }
                                                                            }else{
                                                                                $passed = false;
                                                                            }
                                                                        }

                                                                        if($passed){
                                                                            $reason = null;

                                                                            if(isset($value['alsbb'])){
                                                                                if(!is_null($value['alsbb']) && $value['alsbb'] != '' && $value['alsbb'] != ' '){
                                                                                    $reason = $value['alsbb'];
                                                                                }
                                                                            }
                                                                            \Common\Model\CasesStatusLog::create(['case_id'=>$person->case_id,
                                                                                'user_id'=>$user->id,
                                                                                'reason'=>$reason ,
                                                                                'status'=>$request->status,
                                                                                'date'=>date("Y-m-d")]);
                                                                            \Log\Model\Log::saveNewLog('CASE_UPDATED',trans('common::application.Edited status for someone classified as an aid case')  . ' "'.$person->full_name. '" ');
                                                                            \Common\Model\AidsCases::where('id',$person->case_id)->update(['status' => $request->status]);
                                                                            $success++;
                                                                        }else{
                                                                            $invalid_cards[]=['card'=>$value['rkm_alhoy'],'reason'=>'restricted_region'];
                                                                            $restricted++;
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }

                                                    }else{
                                                        $not_case++;
                                                        $invalid_cards[]=['card'=>$value['rkm_alhoy'],'reason'=>'not_case_for_selected_category'];
                                                    }
                                                }else{
                                                    $invalid_cards[]=['card'=>$value['rkm_alhoy'],'reason'=>'not_person'];
                                                    $not_person++;

                                                }
                                                $processed[]=$value['rkm_alhoy'];
                                            }
                                            else{
                                                $invalid_card++;
                                                $invalid_cards[]=['card'=>$value['rkm_alhoy'],'reason'=>'invalid_card'];
                                            }
                                        }else{
                                            $invalid_card++;
                                            $invalid_cards[]=['card'=>$value['rkm_alhoy'],'reason'=>'invalid_card'];
                                        }
                                    }


                                }

                                if( $success == $total && ($not_case ==0 && $not_person ==0 && $invalid_card ==0
                                        && $active_in_other ==0 && $duplicated ==0 && $restricted ==0)){
                                    return response()->json(['status' => 'success','msg'=>trans('common::application.action success')]);
                                }
                                else{

                                    if($not_case == $total){
                                        return response()->json(['status' => 'failed' , 'error_type' =>' ' ,
                                            'msg' =>trans('common::application.All ID numbers entered are not registered as cases for the same label type') ]);
                                    }

                                    if($not_person == $total){
                                        return response()->json(['status' => 'failed' , 'error_type' =>' ' ,
                                            'msg' =>trans('common::application.All ID numbers entered are not registered as persons in the system')  ]);
                                    }

                                    $response = array();

                                    if($success != 0){
                                        $response = ['status' => 'success','invalid_cards'=>$invalid_cards,'msg'=>
                                            trans('common::application.The status of some individuals has been modified successfully').' :  '. $total . ' ,  '.
                                            trans('common::application.The number of cases whose status has been modified') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of cases with the same new status') .' :  ' .$theSame . ' ,  ' .
                                            trans('aid::application.Number of cases active in other') .' :  ' .$active_in_other . ' ,  ' .
                                            trans('common::application.Number of unregistered ID numbers for the same classification') .' :  ' .$not_case . ' ,  ' .
                                            trans('common::application.The number of ID numbers not registered in the system') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.The number of incorrect or not registered ID numbers in the system') .' :  ' .$invalid_card . ' ,  ' .
                                            trans('common::application.The number of ID numbers whose status was prevented') .' :  ' .$restricted . ' ,  ' .
                                            trans('common::application.number of dead_person') .' :  ' .$dead_person  . ' ,  ' .
                                            trans('common::application.Number of duplicates').' :  '.$duplicated
                                        ];
                                    }else{
                                        $response = ['status' => 'failed_','invalid_cards'=>$invalid_cards,'msg'=>
                                            trans('common::application.The status is not modified for any case').' :  '. $total . ' ,  '.
                                            trans('common::application.The number of cases whose status has been modified') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.Number of cases with the same new status') .' :  ' .$theSame . ' ,  ' .
                                            trans('aid::application.Number of cases active in other') .' :  ' .$active_in_other . ' ,  ' .
                                            trans('common::application.Number of unregistered ID numbers for the same classification') .' :  ' .$not_case . ' ,  ' .
                                            trans('common::application.The number of ID numbers not registered in the system') .' :  ' .$not_person . ' ,  ' .
                                            trans('common::application.The number of incorrect or not registered ID numbers in the system') .' :  ' .$invalid_card . ' ,  ' .
                                            trans('common::application.The number of ID numbers whose status was prevented') .' :  ' .$restricted . ' ,  ' .
                                            trans('common::application.number of dead_person') .' :  ' .$dead_person  . ' ,  ' .
                                            trans('common::application.the file will be downloaded for ids has not been modified').' :  '.$duplicated
                                        ];

                                    }

                                    if(sizeof($invalid_cards) > 0){
                                        $response['msg']= trans('common::application.the file will be downloaded for ids has not been modified').$response['msg'] ;

                                        $data=$invalid_cards;
                                        $rows=[];
                                        foreach($data as $key =>$value){
                                            $rows[$key][trans('aid::application.#')]=$key+1;
                                            foreach($value as $k =>$v){
                                                if($k != 'case_id' && $k != 'id' && $k != 'sponsor_id'&& $k != 'flag'&& $k != 'date_flag'){
                                                    if($k =='card'){
                                                        $rows[$key][trans('aid::application.id_card_number')]= $v;
                                                    }
                                                    $translate=' ';
                                                    if($k =='reason'){
                                                        if($v =='single_person'){ $translate= trans('aid::application.single_person');}
                                                        if($v =='has_same_status'){ $translate= trans('aid::application.has_same_status');}
                                                        if($v =='duplicated'){ $translate= trans('aid::application.duplicated');}
                                                        if($v =='not_case'){ $translate= trans('aid::application.not_case');}
                                                        if($v =='active_in_other'){ $translate= trans('aid::application.active_in_other');}
                                                        if($v =='dead_person'){ $translate= trans('aid::application.dead_person');}
                                                        if($v =='not_person'){ $translate= trans('aid::application.not_person');}

                                                        if($v =='not_case_for_selected_category'){ $translate= trans('aid::application.not_case_of_selected_category');}
                                                        if($v =='restricted_region'){ $translate= trans('aid::application.restricted_region');}
                                                        if($v =='invalid_card'){ $translate= trans('aid::application.invalid_card');}
                                                        $rows[$key][trans('aid::application.not_update_reason')]= $translate;
                                                    }
                                                }
                                            }
                                        }

                                        $token = md5(uniqid());
                                        \Excel::create('export_' . $token, function($excel) use($rows) {
                                            $excel->sheet(trans('common::application.Detection of individuals whose status has not been modified'), function($sheet) use($rows) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' =>[
                                                        'name'      =>  'Simplified Arabic',
                                                        'size'      =>  12,
                                                        'bold'      =>  true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:C1")->applyFromArray($style);
                                                $sheet->setHeight(1,30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' =>[
                                                        'name'      =>  'Simplified Arabic',
                                                        'size'      =>  12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
                                                $sheet->fromArray($rows);

                                            });
                                        })->store('xlsx', storage_path('tmp/'));
                                        $response['download_token'] = $token;
                                    }
                                    return response()->json($response);

                                }
                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }

    // check Cases using excel sheet according card number
    public function check(Request $request)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit',-1);

        $type = $this->type;
        $ctype = $request->checkType;

        $validFile=true;

        $duplicated =0;
        $invalid_cards =0;
        $success =0;

        $progressed=[];
        $passed=[];

        $importFile=$request->file ;
        if ($importFile->isValid()) {
            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();

                    if(sizeof($keys) >0 ){

                        $validFile = false;
                        foreach ($keys as $item) {
                            if ( $item == 'rkm_alhoy' ) {
                                $validFile = true;
                                break;
                            }
                        }

                        if($validFile){
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if ($total > 0) {
                                $passed = [];
                                $cards_list = [];
                                $cases =[];
                                $pass_card =[];
                                $cases_list_final =[];
                                $child = false; $wives = false;$parents = false;

                                if ($ctype == 2 || $ctype == '2'){ $wives = true; }
                                if ($ctype == 3 || $ctype == '3'){ $child = true; }
                                if ($ctype == 4 || $ctype == '5'){ $parents = true; }

                                foreach ($records as $key =>$value) {

                                    if(!( is_null($value['rkm_alhoy']) || $value['rkm_alhoy'] == ' ' || $value['rkm_alhoy'] == '')){
                                        if(!in_array($value['rkm_alhoy'],$passed)){
                                            $passed[]=$value['rkm_alhoy'];

                                            if(strstr($value['rkm_alhoy'], '700')){
                                                if(strpos($value['rkm_alhoy'], '700') == 0 ){
                                                    $cards_list[]= $value['rkm_alhoy'];
                                                }
                                            }
                                        }
                                    }
                                }

                                $old_cards = [];
                                if (sizeof($cards_list) > 0){
                                    $old_card_check = Person::oldCardCheck($cards_list);

                                    if (sizeof($old_card_check) > 0){
                                        foreach ($old_card_check as $key =>$value){
                                            $old_cards[] = $value->old_id_card_number;
                                        }
                                    }

                                }

                                $final = array_diff($passed,$old_cards);

                                $cases_list = \Common\Model\CaseModel::filterCasesByCard($final,$this->type);
                                foreach ($cases_list as $key =>$value) {
                                    if(!in_array($value->id_card_number,$cases)){
                                        if ($ctype == 1){
                                            $cases_list_final[]=$value;
                                            $cases[]=$value->id_card_number;
                                        }
                                        else{
                                            $cases[]=$value->id_card_number;
                                            $keys_options=array(
                                                'action' => 'show',
                                                'category_type' => $type,
                                                'category' => false,
                                                'gender' => false,
                                                'family_member' => true,
                                                'full_name' => false,
                                                'visitor_note' => false,
                                                'person' => false,
                                                'persons_i18n' => false,
                                                'contacts' => true,
                                                'education' => false,
                                                'islamic' => false,
                                                'health' => false,
                                                'work' => false,
                                                'residence' => false,
                                                'case_needs' => false,
                                                'banks' => false,
                                                'home_indoor' => false,
                                                'reconstructions' => false,
                                                'financial_aid_source' => false,
                                                'non_financial_aid_source' => false,
                                                'persons_properties' => false,
                                                'persons_documents' => false,
                                                'case_sponsorships' => false,
                                                'case_payments' => false,
                                                'father_id' => true,
                                                'mother_id' => true,
                                                'guardian_id' => true,
                                                'father' => false,
                                                'mother' => false,
                                                'guardian' => true,
                                                'guardian_kinship' => true,
                                                'family_payments' => false,
                                                'guardian_payments' => false,
                                                'parents' => $parents,
                                                'childs' => $child,
                                                'wives' => $wives
                                            );
                                            $members = CaseModel::fetch($keys_options, $value->case_id);
                                            if (sizeof($members['family_member']) > 0) {
                                                foreach ($members['family_member'] as $kk => $member) {
                                                    if(!in_array($member->id_card_number,$pass_card)){

                                                        $arr = [];
                                                        if ($type == 1) {
                                                            $arr['father_name'] = $value->name;
                                                            $arr['father_id_card_number'] = $value->id_card_number;

                                                            $arr['mother_name'] = $value->mother_name;
                                                            $arr['mother_id_card_number'] = $value->mother_id_card_number;

                                                            $arr['guardian_name'] = $value->guardian_name;
                                                            $arr['guardian_id_card_number'] = $value->guardian_id_card_number;
                                                        } else {
                                                            $arr['father_name'] = $value->name;
                                                            $arr['father_id_card_number'] = $value->id_card_number;
                                                        }

                                                        $pass_card[] = $member->id_card_number;
                                                        $arr['kinship_name'] = $member->kinship_name;
                                                        foreach ($member as $k_ =>$v_){
                                                            $arr[$k_]=$v_;
                                                        }
                                                        $row = (array)$arr;
                                                        $cases_list_final[]=$row;
                                                    }
                                                }
                                            }

                                        }
                                    }
                                }

                                $exclude = array_diff($final,$cases);

                                $response=['status' => 'failed' ,
                                    'msg' => trans('common::application.The Total Of Records Count Is')  .' :  ' .$total . ' ,  ' .
                                        trans('common::application.The number of names that have fetched data') .' :  ' . sizeof($cases) . ' ,  ' .
                                        trans('common::application.old card number') .' :  ' . sizeof($old_cards) . ' ,  ' .
                                        trans('common::application.Number of unregistered case') .' :  ' .sizeof($exclude) ];

                                $response['status_'] =$this->type;
                                if(sizeof($final) > 0 ){
                                    $response['status'] = 'success';
                                }

                                if(sizeof($exclude) > 0 || sizeof($cases_list_final) > 0|| sizeof($old_cards) > 0){

                                    $data=[];
                                    if(sizeof($cases_list_final) > 0){
                                        foreach($cases_list_final as $key =>$value){
                                            $data[$key][trans('aid::application.#')]=$key+1;
                                            $category_name = '';
                                            foreach($value as $k =>$v){
                                                if (!($k == 'kinship_id' || $k == 'id'|| $k == 'l_person_id' || $k == 'marital_status_id')){
                                                    if($v == null){
                                                        $v=' ';
                                                    }
                                                    if(substr( $k, 0, 7 ) === "mother_"){
                                                        $data[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.mother')  ." ) "]= str_replace("-","،",$v);
                                                    }elseif(substr( $k, 0, 7 ) === "father_"){
                                                        if ($ctype == 1){
                                                            $data[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.family_res')  ." ) "]= str_replace("-","،",$v);
                                                        }else{
                                                            $data[$key][trans('sponsorship::application.'.substr($k,7))." ( ".trans('sponsorship::application.family_res')  ." ) "]= str_replace("-","،",$v);
                                                        }
                                                    }elseif(substr( $k, 0, 9 ) === "guardian_"){
                                                        $data[$key][trans('sponsorship::application.'.substr($k,9))." ( ".trans('sponsorship::application.guardian')  ." ) " ]= str_replace("-","،",$v);
                                                    }
                                                    elseif($k === "kinship_name"){
                                                        $f_ke = trans('sponsorship::application.kinship_name')." ( ".trans('sponsorship::application.family_res')  ." ) ";
                                                        $data[$key][$f_ke]= $v;
                                                    }else{
                                                        $data[$key][trans('aid::application.' . $k)]= str_replace("-"," ",$v) ;
                                                    }


                                                    if($this->type = 1 && $k =='category_name'){
                                                        $category_name = $v ;
                                                    }
                                                }
                                            }

                                            if($this->type = 1){
                                                $data[$key][trans('aid::application.category_name')]=$category_name;
                                            }

                                        }
                                    }

                                    $token = md5(uniqid());
                                    $response['download_token'] = $token;
                                    $sheet_title = trans('common::application.cases_data');
                                    if ($ctype != 1){
                                        $sheet_title = trans('common::application.family-data');
                                    }
                                    \Excel::create('export_' . $token, function($excel) use($exclude,$data,$old_cards,$sheet_title) {
                                        $excel->setTitle(trans('common::application.cases_data'));
                                        $excel->setDescription(trans('common::application.cases_data'));


                                        if(sizeof($data) > 0){
                                            $excel->sheet($sheet_title, function ($sheet) use ($data) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1:AE1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => ['name' => 'Simplified Arabic', 'size' => 12, 'bold' => false]
                                                ];
                                                $sheet->getDefaultStyle()->applyFromArray($style);
                                                $sheet->fromArray($data);

                                            });
                                        }
                                        if(sizeof($exclude) > 0){
                                            $excel->sheet(trans('common::application.excluded'), function ($sheet) use ($exclude) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $z= 2;
                                                foreach($exclude as $v){
                                                    $sheet ->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }
                                            });

                                        }
                                        if(sizeof($old_cards) > 0){
                                            $excel->sheet(trans('common::application.old card number'), function ($sheet) use ($old_cards) {
                                                $sheet->setStyle([
                                                    'font' => [
                                                        'name' => 'Calibri',
                                                        'size' => 11,
                                                        'bold' => true
                                                    ]
                                                ]);
                                                $sheet->setAllBorders('thin');
                                                $sheet->setfitToWidth(true);
                                                $sheet->setRightToLeft(true);
                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => true
                                                    ]
                                                ];

                                                $sheet->getStyle("A1")->applyFromArray($style);
                                                $sheet->setHeight(1, 30);

                                                $style = [
                                                    'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                    'font' => [
                                                        'name' => 'Simplified Arabic',
                                                        'size' => 12,
                                                        'bold' => false
                                                    ]
                                                ];

                                                $sheet->getDefaultStyle()->applyFromArray($style);
                                                $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                $z= 2;
                                                foreach($old_cards as $v){
                                                    $sheet ->setCellValue('A'.$z,$v);
                                                    $z++;
                                                }
                                            });

                                        }
                                    })->store('xlsx', storage_path('tmp/'));
                                }

                                return response()->json($response);
                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);

                    }
                }

                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);

            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);

    }

    // import cases with card and mobile only //////////////////
    public function importCards(Request $request){

        ini_set('max_execution_time', 0);
        set_time_limit(0);
        Ini_set('memory_limit','2048M');

        $PARENT_RELATIVE_DESC = ["أب" , "أم","اب" , "ام"];
        $SPONSES_RELATIVE_DESC = ["زوج/ة"];
        $CHILD_RELATIVE_DESC = ["ابن/ة"];

        $validFile=true;

        $duplicated =0;
        $old_person=0;
        $old_case=0;
        $blocked=0;
        $processed=[];
        $success =0;
        $not_on_region =0;
        $acive_on_other_org =0;
        $father_id =Null;
        $mother_id =Null;
        $guardian_id =Null;
        $invalid_cards=[];
        $result=[];
        $processed_=[];
        $notUpdate_ =["first_name",  "second_name", "third_name", "last_name",
            "prev_family_name", "gender", "marital_status_id", "birthday", "death_date" ];

        $notUpdate =["id_card_number", "first_name",  "second_name", "third_name", "last_name",
            "prev_family_name", "gender", "marital_status_id", "birthday", "death_date" ];

        $importFile=$request->file ;
        if ($importFile->isValid()) {

            $path =  $importFile->getRealPath();
            $excel = \PHPExcel_IOFactory::load( $path );
            $sheets = $excel->getSheetNames();
            if(\App\Http\Helpers::sheetFound("data",$sheets)){
                $first=Excel::selectSheets('data')->load($path)->first();
                if(!is_null($first)){
                    $keys = $first->keys()->toArray();
                    if(sizeof($keys) >0 ){
                        $constraint =  ["rkm_alhoy","rkm_aljoal"];
                        foreach ($constraint as $item) {
                            if ( !in_array($item, $keys) ) {
                                $validFile = false;
                                break;
                            }
                        }
                        if($validFile){
                            $errors=[];
                            $records= \Excel::selectSheets('data')->load($path)->get();
                            $total=sizeof($records);

                            if($total == 0){
                                return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.the file is empty') ]);
                            }

                            if ($total < 2000) {
                                $user = \Auth::user();
                                $categoryObj = \Common\Model\Categories\AidsCategories::where('id',$request->category_id)->first();
                                foreach ($records as $key =>$value) {
                                    $setOnTransfer = false;
                                    if(in_array($value['rkm_alhoy'],$processed)){
                                        $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'duplicated'];
                                        $duplicated++;
                                    }
                                    else{
                                        if(strlen($value['rkm_alhoy'])  <= 9) {
                                            $id_card_number = (int) $value['rkm_alhoy'];

                                            if(BlockIDCard::isBlock($value['rkm_alhoy'],$request->category_id,1)){
                                                $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'blocked'];
                                                $blocked++;
                                                $processed[]=$value['rkm_alhoy'];
                                            }
                                            else{
                                                if(GovServices::checkCard($id_card_number)){
                                                    $isUnique = Person::isUnique($id_card_number);
                                                    if($isUnique['status'] == false) {
                                                        $gov_data= GovServices::byCard($id_card_number);
                                                        if($gov_data['status'] == true) {
                                                            CloneGovernmentPersons::saveNew((Object) $gov_data['row']);

                                                            $data = CaseModel::reMapInputs($value);
                                                            $case=null;
                                                            if(sizeof($data['case']) >0){
                                                                $case=CaseModel::filterInputs(true,$request->category_id,$this->type,$data['case']);
                                                            }

                                                            $MapBasicToUpdate= Person::mapBasicToUpdate($gov_data['row']);

                                                            $case['update_en'] = true;
                                                            $en = ['en_first_name' , 'en_second_name' ,'en_third_name','en_last_name'];
                                                            foreach ($en as $ino){
                                                                if(isset($MapBasicToUpdate[$ino])){
                                                                    $case[$ino] =$MapBasicToUpdate[$ino];
                                                                }
                                                            }

                                                            foreach ($notUpdate as $ino){
                                                                if(isset($MapBasicToUpdate[$ino])){
                                                                    $case[$ino] =$MapBasicToUpdate[$ino];
                                                                }
                                                            }

                                                            $case['adscountry_id'] =$request->adscountry_id;
                                                            $case['adsdistrict_id'] =$request->adsdistrict_id;
                                                            $case['adsregion_id'] =$request->adsregion_id;
                                                            $case['adssquare_id'] =$request->adssquare_id;
                                                            $case['adsneighborhood_id'] =$request->adsneighborhood_id;
                                                            $case['adsmosques_id'] =$request->adsmosques_id;

                                                            if (isset($value['rkm_aljoal'])) {
                                                                $case['primary_mobile'] = $value['rkm_aljoal'];
                                                            }

                                                            $person =Person::savePerson($case);
                                                            $person_id = $person['id'];

                                                            $person_ =Person::where('id',$person_id)->first();

                                                            foreach ($gov_data['row']->all_relatives as $k_=> $relative) {

                                                                if(in_array($relative->RELATIVE_DESC ,$CHILD_RELATIVE_DESC) ||
                                                                    in_array($relative->RELATIVE_DESC ,$SPONSES_RELATIVE_DESC)||
                                                                    in_array($relative->RELATIVE_DESC ,$PARENT_RELATIVE_DESC)) {
                                                                    $crd = $relative->IDNO_RELATIVE;
                                                                    $map = GovServices::inputsMap($relative);
                                                                    $map['kinship_id'] =$relative->kinship_id;
                                                                    $map['kinship_name']=$relative->kinship_name;

                                                                    $find=Person::where('id_card_number',$crd)->first();
                                                                    if(!is_null($find)){
                                                                        $map['person_id'] = $find->id;
                                                                    }

                                                                    if(in_array($relative->RELATIVE_DESC ,$CHILD_RELATIVE_DESC)){
                                                                        if($person_->gender == 1){
                                                                            $map['father_id']= $person_->id;
                                                                        }else{
                                                                            $map['mother_id']= $person_->id;
                                                                        }
                                                                    }

                                                                    if(($person_->gender == 1) &&
                                                                        in_array($relative->RELATIVE_DESC ,$SPONSES_RELATIVE_DESC)){
                                                                        $map_['husband_id']= $person_->id;
                                                                    }

                                                                    $relative_row = Person::savePerson($map_);

                                                                    if(in_array($relative->RELATIVE_DESC ,$PARENT_RELATIVE_DESC)){
                                                                        if($person_->gender == 1){
                                                                            $person_->father_id= $relative_row['id'];
                                                                        }else{
                                                                            $person_->mother_id= $relative_row['id'];
                                                                        }
                                                                    }
                                                                    if(($person_->gender == 2) &&
                                                                        in_array($relative->RELATIVE_DESC ,$SPONSES_RELATIVE_DESC)){
                                                                        $person_->husband_id= $relative_row['id'];
                                                                    }

                                                                    CloneGovernmentPersons::saveNewWithRelation($crd,$relative);
                                                                }
                                                            }

                                                            CaseModel::DisablePersonInNoConnected($person_,$user->organization_id,$user->id);
                                                            $passed = true;

                                                            if($this->type == 2){
                                                                $founded = OrgLocations::where(['organization_id'=>$user->organization_id ,
                                                                    'location_id'=>$person_->adsmosques_id])->count();
                                                                if($founded == 0 ){
                                                                    $passed = false;
                                                                }

                                                            }

                                                            if($passed == true) {
                                                                $success++;
                                                            }
                                                            else{
                                                                CaseModel::where(['person_id'=>$person_id, 'organization_id' => $user->organization_id ,
                                                                    'category_id'=>$request->category_id])->delete();

                                                                $full_name = $person_->first_name. ' ' .$person_->second_name. ' ' .$person_->third_name. ' ' .$person_->last_name;
                                                                $transferObj =  Transfers::where(['person_id'=> $person_->id,
                                                                    'prev_adscountry_id'=> $person['prev_adscountry_id'], 'prev_adsdistrict_id'=> $person['prev_adsdistrict_id'],
                                                                    'prev_adsregion_id'=> $person['prev_adsregion_id'], 'prev_adsneighborhood_id'=> $person['prev_adsneighborhood_id'],
                                                                    'prev_adssquare_id'=> $person['prev_adssquare_id'], 'prev_adsmosques_id'=> $person['prev_adsmosques_id'],
                                                                    'adscountry_id'=> $request->adscountry_id, 'adsdistrict_id'=> $request->adsdistrict_id,
                                                                    'adsregion_id'=> $request->adsregion_id, 'adsneighborhood_id'=> $request->adsneighborhood_id,
                                                                    'adssquare_id'=> $request->adssquare_id, 'adsmosques_id'=> $request->adsmosques_id,
                                                                    'category_id' => $request->category_id])->first();

                                                                if(!$transferObj){
                                                                    Transfers::create(['created_at' => date('Y-m-d H:i:s'), 'person_id'=> $person_->id,
                                                                        'prev_adscountry_id'=> $person['prev_adscountry_id'], 'prev_adsdistrict_id'=> $person['prev_adsdistrict_id'],
                                                                        'prev_adsregion_id'=> $person['prev_adsregion_id'], 'prev_adsneighborhood_id'=> $person['prev_adsneighborhood_id'],
                                                                        'prev_adssquare_id'=> $person['prev_adssquare_id'], 'prev_adsmosques_id'=> $person['prev_adsmosques_id'],
                                                                        'adscountry_id'=> $request->adscountry_id, 'adsdistrict_id'=> $request->adsdistrict_id,
                                                                        'adsregion_id'=> $request->adsregion_id, 'adsneighborhood_id'=> $request->adsneighborhood_id,
                                                                        'adssquare_id'=> $request->adssquare_id, 'adsmosques_id'=> $request->adsmosques_id,
                                                                        'category_id' => $request->category_id, 'user_id'=> $user->id,
                                                                        'organization_id' => $user->organization_id]);

                                                                    $levelId = null;
                                                                    if(is_null($person_->adsmosques_id)) {
                                                                        if(is_null($person_->adssquare_id)) {
                                                                            if(is_null($person_->adsneighborhood_id)) {
                                                                                if(is_null($person_->adsregion_id)) {
                                                                                    if(is_null($person_->adsdistrict_id)) {
                                                                                        if(is_null($person_->adscountry_id)) {
                                                                                            $levelId = $person_->adscountry_id ;
                                                                                        }
                                                                                    }else{
                                                                                        $levelId = $person_->adsdistrict_id ;
                                                                                    }
                                                                                }else{
                                                                                    $levelId = $person_->adsregion_id ;
                                                                                }
                                                                            }else{
                                                                                $levelId = $person_->adsneighborhood_id ;
                                                                            }
                                                                        }else{
                                                                            $levelId = $person_->adssquare_id ;
                                                                        }
                                                                    }
                                                                    else{
                                                                        $levelId = $person_->adsmosques_id ;
                                                                    }

                                                                    $orgs =  OrgLocations::LocationOrgs($levelId,$person_->id,$request->category_id);
                                                                    $users =User::userHasPermission('reports.transfers.manage',$orgs);
                                                                    $url = "#/transfers";

                                                                    \Log\Model\Notifications::saveNotification([
                                                                        'user_id_to' =>$users,
                                                                        'url' =>$url,
                                                                        'message' => trans('common::application.he migrated') . ': ' . ' ' . $full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name
                                                                    ]);

                                                                    \Log\Model\Log::saveNewLog('TRANSFER_CREATED',trans('common::application.he migrated') . ': ' . ' ' . $full_name. ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name);
                                                                }

                                                                $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'out_of_regions'];
                                                                $not_on_region++;
                                                            }
                                                        }else{
                                                            $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'invalid_id_card_number'];
                                                            $invalid_cards[] =$value['rkm_alhoy'];
                                                        }

                                                    }
                                                    else {
                                                        $person=$isUnique['row'];
                                                        $person_id=$person->id;

                                                        $data = CaseModel::reMapInputs($value);
                                                        $case=null;

                                                        if(sizeof($data['case']) >0){
                                                            $case=CaseModel::filterInputs(true,$request->category_id,$this->type,$data['case']);
                                                        }

                                                        $case['person_id']=$person_id;
                                                        $case['adscountry_id'] =$request->adscountry_id;
                                                        $case['adsdistrict_id'] =$request->adsdistrict_id;
                                                        $case['adsregion_id'] =$request->adsregion_id;
                                                        $case['adssquare_id'] =$request->adssquare_id;
                                                        $case['adsneighborhood_id'] =$request->adsneighborhood_id;
                                                        $case['adsmosques_id'] =$request->adsmosques_id;
                                                        $case['primary_mobile'] = $value['rkm_aljoal'];

                                                        foreach ($notUpdate_ as $ino){
                                                            if(isset($case[$ino])){
                                                                unset($case[$ino]);
                                                            }
                                                        }

                                                        $person_ =Person::where('id',$person_id)->first();


                                                        if($request->adsmosques_id != $person_->adsmosques_id){
                                                            $founded = OrgLocations::where(['organization_id'=>$user->organization_id , 'location_id'=>$request->adsmosques_id])->count();
                                                            if($founded == 0 ){
                                                                $transferObj =  Transfers::where(['person_id'=> $person_->id,
                                                                    'prev_adscountry_id'=> $person_->adscountry_id, 'prev_adsdistrict_id'=> $person_->adsdistrict_id,
                                                                    'prev_adsregion_id'=> $person_->adsregion_id, 'prev_adsneighborhood_id'=> $person_->adsneighborhood_id,
                                                                    'prev_adssquare_id'=> $person_->adssquare_id, 'prev_adsmosques_id'=> $person_->adsmosques_id,
                                                                    'adscountry_id'=> $request->adscountry_id, 'adsdistrict_id'=> $request->adsdistrict_id,
                                                                    'adsregion_id'=> $request->adsregion_id, 'adsneighborhood_id'=> $request->adsneighborhood_id,
                                                                    'adssquare_id'=> $request->adssquare_id, 'adsmosques_id'=> $request->adsmosques_id,
                                                                    'category_id' => $request->category_id])->first();
                                                                if(!$transferObj){
                                                                    Transfers::create(['created_at' => date('Y-m-d H:i:s'), 'person_id'=> $person_->id,
                                                                        'prev_adscountry_id'=> $person_->adscountry_id, 'prev_adsdistrict_id'=> $person_->adsdistrict_id,
                                                                        'prev_adsregion_id'=> $person_->adsregion_id, 'prev_adsneighborhood_id'=> $person_->adsneighborhood_id,
                                                                        'prev_adssquare_id'=> $person_->adssquare_id, 'prev_adsmosques_id'=> $person_->adsmosques_id,
                                                                        'adscountry_id'=> $request->adscountry_id, 'adsdistrict_id'=> $request->adsdistrict_id,
                                                                        'adsregion_id'=> $request->adsregion_id, 'adsneighborhood_id'=> $request->adsneighborhood_id,
                                                                        'adssquare_id'=> $request->adssquare_id, 'adsmosques_id'=> $request->adsmosques_id,
                                                                        'category_id' => $request->category_id, 'user_id'=> $user->id,
                                                                        'organization_id' => $user->organization_id]);

                                                                    $levelId = $case['adsmosques_id'];
                                                                    $case['adscountry_id'] = $person_->adscountry_id;
                                                                    $case['adsdistrict_id'] = $person_->adsdistrict_id;
                                                                    $case['adsregion_id'] = $person_->adsregion_id;
                                                                    $case['adssquare_id'] = $person_->adssquare_id;
                                                                    $case['adsneighborhood_id'] = $person_->adsneighborhood_id;
                                                                    $case['adsmosques_id'] = $person_->adsmosques_id;
                                                                    $setOnTransfer = true;

                                                                    $orgs =  OrgLocations::LocationOrgs($levelId,$person_->id,$request->category_id);
                                                                    $users =User::userHasPermission('reports.transfers.manage',$orgs);
                                                                    $url = "#/transfers";

                                                                    $full_name = $person_->first_name.' '.$person_->second_name.' '.$person_->third_name.' '.$person_->last_name;
                                                                    if(sizeof($users)>0){
                                                                        \Log\Model\Notifications::saveNotification([
                                                                            'user_id_to' =>$users,
                                                                            'url' =>$url,
                                                                            'message' =>  trans('common::application.he migrated') . ' :' . $full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name
                                                                        ]);
                                                                    }

                                                                    \Log\Model\Log::saveNewLog('TRANSFER_CREATED',trans('common::application.he migrated') . ': ' . ' ' . $full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name);

                                                                }
                                                            }
                                                        }

                                                        $person =Person::savePerson($case);
                                                        CaseModel::DisablePersonInNoConnected($person_,$user->organization_id,$user->id);
                                                        $passed = true;

                                                        if($this->type == 2) {
                                                            $founded = OrgLocations::where(['organization_id'=>$user->organization_id ,
                                                                'location_id'=>$person_->adsmosques_id])->count();
                                                            if($founded == 0 ){
                                                                $passed = false;
                                                            }
                                                        }


                                                        if($passed == true) {

                                                            if($person['old_case'] == true){
                                                                $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'old_case'];
                                                                $old_case++;
                                                            }else{
                                                                $processed_[]=$value['rkm_alhoy'];
                                                                $success++;

                                                            }

                                                            if(CaseModel::where(['person_id'=>$person_id,
                                                                'organization_id' => $user->organization_id ,
                                                                'category_id'=>$request->category_id])->first()){
//                                                            $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'old_case'];
//                                                            $old_case++;
                                                            }
                                                            else{
                                                                $dates = ['birthday','visited_at','death_date'];
                                                                $carMap =   ["tosyat_albahth_alajtmaaay"=>'notes', "tarykh_alzyar" => "visited_at", "asm_albahth_alajtmaaay" => "visitor"];
                                                                $case = new CaseModel();
                                                                $case->person_id=$person_id;
                                                                $case->category_id=$request->category_id;
                                                                $case->organization_id=$user->organization_id;
                                                                $case->user_id=$user->id;
                                                                $case->rank=0;
                                                                foreach ($carMap as $k=>$v) {
                                                                    if(isset($value[$k])){
                                                                        if(in_array($v,$dates)){
                                                                            $case->$v=date('Y-m-d',strtotime($value[$k]));
                                                                        }else{
                                                                            $case->$v=$value[$k];
                                                                        }
                                                                    }
                                                                }
                                                                $case->save();
                                                                CaseModel::where(['id' => $case->id ])->update(['rank' =>\Common\Model\AbstractCases::updateRank($case->id)]);
                                                            }

                                                        }
                                                        else{
                                                            $case = CaseModel::where(['person_id'=>$person_id,
                                                                'organization_id' => $user->organization_id ,
                                                                'category_id'=>$request->category_id])->first();
                                                            if($case){
                                                                $full_name = $person_->first_name. ' ' .$person_->second_name. ' ' .$person_->third_name. ' ' .$person_->last_name;
                                                                \Common\Model\AidsCases::where('id',$person_id)->update(['status' => 1]);
                                                                $action='CASE_UPDATED';
                                                                $message=trans('common::application.Has edited data') . '  : '.' "'.$full_name. ' " ';
                                                                \Log\Model\Log::saveNewLog($action,$message);
                                                                \Common\Model\CasesStatusLog::create(['case_id'=>$case->id, 'user_id'=>$user->id,
                                                                    'reason'=>trans('common::application.Disabled your recorded status for')  . ' "'.$full_name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization') , 'status'=>1, 'date'=>date("Y-m-d")]);
                                                                \Log\Model\Log::saveNewLog('CASE_UPDATED',trans('common::application.Disabled your recorded status for')  . ' "'.$full_name. '" ' .trans('common::application.Because it does not belong to the areas attached to you as an organization'));
                                                            }

                                                            if(!$setOnTransfer) {
                                                                $transferObj =  Transfers::where([ 'person_id'=> $person_->id,
                                                                    'prev_adscountry_id'=> $person_->adscountry_id, 'prev_adsdistrict_id'=> $person_->adsdistrict_id,
                                                                    'prev_adsregion_id'=> $person_->adsregion_id, 'prev_adsneighborhood_id'=> $person_->adsneighborhood_id,
                                                                    'prev_adssquare_id'=> $person_->adssquare_id, 'prev_adsmosques_id'=> $person_->adsmosques_id,
                                                                    'adscountry_id'=> $request->adscountry_id, 'adsdistrict_id'=> $request->adsdistrict_id,
                                                                    'adsregion_id'=> $request->adsregion_id, 'adsneighborhood_id'=> $request->adsneighborhood_id,
                                                                    'adssquare_id'=> $request->adssquare_id, 'adsmosques_id'=> $request->adsmosques_id,
                                                                    'category_id' => $request->category_id])->first();

                                                                if(!$transferObj){
                                                                    Transfers::create(['created_at' => date('Y-m-d H:i:s'), 'person_id'=> $person_->id,
                                                                        'prev_adscountry_id'=> $person_->adscountry_id, 'prev_adsdistrict_id'=> $person_->adsdistrict_id,
                                                                        'prev_adsregion_id'=> $person_->adsregion_id, 'prev_adsneighborhood_id'=> $person_->adsneighborhood_id,
                                                                        'prev_adssquare_id'=> $person_->adssquare_id, 'prev_adsmosques_id'=> $person_->adsmosques_id,
                                                                        'adscountry_id'=> $request->adscountry_id, 'adsdistrict_id'=> $request->adsdistrict_id,
                                                                        'adsregion_id'=> $request->adsregion_id, 'adsneighborhood_id'=> $request->adsneighborhood_id,
                                                                        'adssquare_id'=> $request->adssquare_id, 'adsmosques_id'=> $request->adsmosques_id,
                                                                        'category_id' => $request->category_id, 'user_id'=> $user->id,
                                                                        'organization_id' => $user->organization_id]);


                                                                    $levelId = null;
                                                                    if(is_null($person_->adsmosques_id)) {
                                                                        if(is_null($person_->adssquare_id)) {
                                                                            if(is_null($person_->adsneighborhood_id)) {
                                                                                if(is_null($person_->adsregion_id)) {
                                                                                    if(is_null($person_->adsdistrict_id)) {
                                                                                        if(is_null($person_->adscountry_id)) {
                                                                                            $levelId = $person_->adscountry_id ;
                                                                                        }
                                                                                    }else{
                                                                                        $levelId = $person_->adsdistrict_id ;
                                                                                    }
                                                                                }else{
                                                                                    $levelId = $person_->adsregion_id ;
                                                                                }
                                                                            }else{
                                                                                $levelId = $person_->adsneighborhood_id ;
                                                                            }
                                                                        }else{
                                                                            $levelId = $person_->adssquare_id ;
                                                                        }
                                                                    }
                                                                    else{
                                                                        $levelId = $person_->adsmosques_id ;
                                                                    }

                                                                    $orgs =  OrgLocations::LocationOrgs($levelId,$person_->id,$request->category_id);
                                                                    $users =User::userHasPermission('reports.transfers.manage',$orgs);
                                                                    $url = "#/transfers";

                                                                    \Log\Model\Notifications::saveNotification([
                                                                        'user_id_to' =>$users,
                                                                        'url' =>$url,
                                                                        'message' => trans('common::application.he migrated') . ': ' . ' ' . $full_name . ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name
                                                                    ]);

                                                                    \Log\Model\Log::saveNewLog('TRANSFER_CREATED',trans('common::application.he migrated') . ': ' . ' ' . $full_name. ' , '. trans('common::application.As a case') . ': ' . $categoryObj->name);

                                                                }
                                                            }
                                                            $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'out_of_regions'];
                                                            $not_on_region++;
                                                        }

                                                    }
                                                    $processed[]=$value['rkm_alhoy'];
                                                }
                                                else{
                                                    $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'invalid_id_card_number'];
                                                    $invalid_cards[] =$value['rkm_alhoy'];
                                                }
                                            }

                                        }else{
                                            $result[] = ['id_card_number' =>  $value['rkm_alhoy'], 'reason' => 'invalid_id_card_number'];
                                            $invalid_cards[] =$value['rkm_alhoy'];
                                        }
                                    }
                                }
                                if( ($success == $total)){
                                    return response()->json(['status' => 'success','msg'=> trans('common::application.Successfully imported all cases')]);
                                }
                                else{
                                    $response = [];
                                    if($old_case == $total){
                                        $response['status'] = 'failed';
                                        $response['msg'] =  trans('common::application.All names entered are previously registered for the same type');
                                    }
                                    if($blocked == $total){
                                        $response['status'] = 'failed';
                                        $response['msg'] =  trans('common::application.All card entered are previously blocked to add to this category');
                                    }


                                    if($success != 0){
                                        $response['status'] = 'success';
                                        $response['msg'] = trans('common::application.Some cases were imported successfully, as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.The number of incorrect or not registered ID numbers in the system') .' :  ' .sizeof($invalid_cards) . ' ,  ' .
                                            trans('common::application.Number of imported cases') .' :  ' .$success . ' ,  ' .
                                            trans('common::application.How many names you have saved but not saved as cases outside your organization').' :  ' .$not_on_region . ' ,  ' .
                                            trans('common::application.Number of previously blocked to add to this category') .' :  ' .$blocked . ' ,  ' .
                                            trans('common::application.Number of previously registered case names') .' :  ' .$old_case . ' ,  ' .
                                            trans('common::application.active on other organization') .' :  ' .$acive_on_other_org . ' ,  ' .
                                            trans('common::application.Number of repeated cases') .' :  '.$duplicated ;
                                    }
                                    else{
                                        $response['status'] = 'failed';
                                        $response['msg'] = trans('common::application.no cases were imported , as the total number of records') .' :  '. $total . ' ,  '.
                                            trans('common::application.The number of incorrect or not registered ID numbers in the system') .' :  ' .sizeof($invalid_cards) . ' ,  ' .
                                            trans('common::application.How many names you have saved but not saved as cases outside your organization').' :  ' .$not_on_region . ' ,  ' .
                                            trans('common::application.Number of previously blocked to add to this category') .' :  ' .$blocked . ' ,  ' .
                                            trans('common::application.Number of previously registered case names') .' :  ' .$old_case . ' ,  ' .
                                            trans('common::application.active on other organization') .' :  ' .$acive_on_other_org . ' ,  ' .
                                            trans('common::application.Number of repeated cases') .' :  '.$duplicated;

                                    }

                                    if (sizeof($result) > 0 || sizeof($errors) > 0 || sizeof($processed_) > 0) {
                                        $token = md5(uniqid());
                                        \Excel::create('export_' . $token, function ($excel) use ($result,$processed_,$errors) {
                                            if(sizeof($processed_) > 0){
                                                $excel->sheet(trans('common::application.saved'), function($sheet) use($processed_) {

                                                    $sheet->setRightToLeft(true);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setHeight(1,40);
                                                    $sheet->getDefaultStyle()->applyFromArray([
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font'      =>['name' =>'Simplified Arabic', 'size'  =>  11]
                                                    ]);

                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));

                                                    $z= 2;
                                                    foreach($processed_ as $v){
                                                        $sheet ->setCellValue('A'.$z,$v);
                                                        $z++;
                                                    }

                                                });
                                            }
                                            if(sizeof($result) > 0){
                                                $excel->sheet(trans('common::application.excluded'), function ($sheet) use ($result) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('B1',trans('common::application.reason'));
                                                    $z= 2;
                                                    foreach($result as $v){
                                                        $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                        $sheet ->setCellValue('B'.$z,trans('common::application.'.$v['reason']));
                                                        $z++;
                                                    }
                                                });

                                            }

                                            if(sizeof($errors) > 0){
                                                $excel->sheet(trans('common::application.data_errors'), function ($sheet) use ($errors) {
                                                    $sheet->setStyle([
                                                        'font' => [
                                                            'name' => 'Calibri',
                                                            'size' => 11,
                                                            'bold' => true
                                                        ]
                                                    ]);
                                                    $sheet->setAllBorders('thin');
                                                    $sheet->setfitToWidth(true);
                                                    $sheet->setRightToLeft(true);
                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => true
                                                        ]
                                                    ];

                                                    $sheet->getStyle("A1:B1")->applyFromArray($style);
                                                    $sheet->setHeight(1, 30);

                                                    $style = [
                                                        'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                                        'font' => [
                                                            'name' => 'Simplified Arabic',
                                                            'size' => 12,
                                                            'bold' => false
                                                        ]
                                                    ];

                                                    $sheet->getDefaultStyle()->applyFromArray($style);
//                                                    $sheet->fromArray($rows);
                                                    $sheet->getStyle("A1:A1")->applyFromArray(['font' => ['bold' => true]]);
                                                    $sheet ->setCellValue('A1',trans('common::application.id_card_number'));
                                                    $sheet ->setCellValue('B1',trans('common::application.field_name'));
                                                    $sheet ->setCellValue('C1',trans('common::application.reason'));
                                                    $z= 2;
                                                    foreach($errors as $v){
                                                        $sheet ->setCellValue('A'.$z,$v['id_card_number']);
                                                        $sheet ->setCellValue('B'.$z,$v['error']);
                                                        $sheet ->setCellValue('C'.$z,$v['reason']);
                                                        $z++;
                                                    }
                                                });

                                            }
                                        })->store('xlsx', storage_path('tmp/'));
                                        $response["download_token"] = $token;
                                        $response["msg"] = $response["msg"] . '  , ' . trans('aid::application.all restricted_');
                                    }

                                    return response()->json($response);
                                }
                            }
                            return response()->json(['status' => 'failed' ,'error_type' =>' ' , 'msg' => trans('common::application.The number of records in the file must not exceed 2000 records') ]);
                        }
                        return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required column')]);
                    }
                }
                return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' =>trans('common::application.the file is empty')  ]);
            }
            return response()->json(['status' => 'failed', 'error_type' => ' ', 'msg' => trans('common::application.the file does not have the required data sheet')]);
        }
        return response()->json(['status' => 'failed' , 'error_type' =>' ' , 'msg' => trans('common::application.invalid file') ]);
    }

}
