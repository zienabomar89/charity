<?php

namespace Common\Controller\VisitorNotes;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Common\Model\AbstractVisitorNotes;
use Common\Model\AidsVisitorNotes;
use Common\Model\SponsorshipVisitorNotes;

class AbstractVisitorNotesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index()
    {
        $user = \Auth::user();
        $request = request();
        return response()->json(AbstractVisitorNotes::getData($this->target,$user->organization_id,$request->itemsCount));
    }

    // create new object model
    public function store(Request $request)
    {
        $user = \Auth::user();
        $response = array();

        $rules=[
                'category_id' => 'required|integer',
                'for_male' => 'required', 'for_female' => 'required',
                'en_for_male' => 'required', 'en_for_female' => 'required'
            ];

        $exist = 0 ;
        $category_id = $request->category_id;

        if($this->target ==1){
            $exist=AidsVisitorNotes::where(['category_id' =>$category_id,'organization_id' =>$user->organization_id])->count();
        }else{
            $exist=SponsorshipVisitorNotes::where(['category_id' =>$category_id,'organization_id' =>$user->organization_id])->count();
        }

        if($exist !=0){
            $response["msg"]['category_id']=[trans('common::application.This value is already in use') ];
            return response()->json(['status' => 'failed_valid','msg' => $response]);
        }

        $error = \App\Http\Helpers::isValid($request->all(),$rules);
        if($error)
            return response()->json($error);

        $note=$this->getModel();
        $note->organization_id=$user->organization_id;

        $inputs=[ 'category_id' ,  'for_male' ,  'en_for_male' , 'for_female', 'en_for_female'];
        foreach ($inputs as $key) {
            if (isset($request->$key)) {
                $note->$key=$request->$key;
            }
        }
        if($note->save()){
            $category = \Common\Model\Categories\AidsCategories::where('id',$category_id)->first();
            \Log\Model\Log::saveNewLog('VISITOR_NOTES_CREATED', trans("common::application.Added a researcher's opinion record to"). ' " '.$category->name  . ' " ');
            return response()->json(['status' => 'success','msg' => trans('setting::application.The row is inserted to db')]);
        }

        return response()->json(['status' => 'failed','msg' => trans('setting::application.The row is not insert to db')]);
    }

    // get existed object model by id
    public function show($id)
    {
        $note  =$this->findOrFail($id);
        $this->authorize('view', $note);
        return response()->json($note);
    }

    // update existed object model by id
    public function update(Request $request, $id)
    {
        $note = $this->findOrFail($id);
        $this->authorize('update', $note);

        $rules=[
            'category_id' => 'required|integer',
            'for_male' => 'required', 'for_female' => 'required',
            'en_for_male' => 'required', 'en_for_female' => 'required'
        ];

        $category_id = $request->category_id;

        $error = \App\Http\Helpers::isValid($request->all(),$rules);
        if($error)
            return response()->json($error);

        $inputs=[ 'category_id' ,  'for_male' ,  'en_for_male' , 'for_female', 'en_for_female'];
        foreach ($inputs as $key) {
            if (isset($request->$key)) {
                $note->$key=$request->$key;
            }
        }
        if($note->save()){
            $category = \Common\Model\Categories\AidsCategories::where('id',$category_id)->first();
            \Log\Model\Log::saveNewLog('VISITOR_NOTES_CREATED',trans("common::application.Updated the researcher's opinion record") . ' " '.$category->name  . ' " ');
            return response()->json(['status' => 'success','msg' => trans('setting::application.The row is updated')]);
        }

        return response()->json(['status' => 'failed','msg' => trans('setting::application.There is no update')]);
    }

    // delete existed object model by id
    public function destroy($id,Request $request)
    {
        $row = $this->findOrFail($id);
        $this->authorize('delete', $row);
        $category_id = $row->category_id;

        if($row->delete()){
            $category = \Common\Model\Categories\AidsCategories::where('id',$category_id)->first();
            \Log\Model\Log::saveNewLog('VISITOR_NOTES_DELETED',trans("common::application.Deleted the researcher's opinion record") . ' " '.$category->name  . ' " ');
             return response()->json(['status' => 'success','msg' => trans('setting::application.The row is deleted from db')]);
        }

        return response()->json(['status' => 'failed','msg' => trans('setting::application.The row is not deleted from db')]);

    }

}
