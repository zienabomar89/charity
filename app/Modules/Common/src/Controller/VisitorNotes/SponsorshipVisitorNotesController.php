<?php
namespace Common\Controller\VisitorNotes;

use Common\Model\AbstractVisitorNotes;
use Common\Model\SponsorshipVisitorNotes;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class SponsorshipVisitorNotesController  extends AbstractVisitorNotesController
{
    protected $target = AbstractVisitorNotes::TARGET_SPONSORSHIPS;

    // initialize SponsorshipVisitorNotes model object to use in process
    protected function getModel()
    {
        return new SponsorshipVisitorNotes();
    }

    // get SponsorshipVisitorNotes model using id
    protected function findOrFail($id)
    {
        $sponsorships = SponsorshipVisitorNotes::findOrFail($id);
        $obj= \Common\Model\Categories\SponsorshipCategories::findOrFail($sponsorships->category_id);
        if ($obj->type != $this->target) {
            throw (new ModelNotFoundException())->setModel(get_class($sponsorships));
        }
        return $sponsorships;
    }


    // get SponsorshipVisitorNotes rows
    public function index()
    {
        $this->authorize('manage', SponsorshipVisitorNotes::class);
        return parent::index();
    }

    // create new object model
    public function store(Request $request)
    {
        $this->authorize('create', SponsorshipVisitorNotes::class);
        return parent::store($request);
    }

    // get template instruction
    public function templateInstruction()
    {
        return response()->json(['download_token' => 'notes-sponsorship-template']);
//        return response()->file(base_path('storage/app/templates/notes-sponsorship-template.pdf'));
    }

}