<?php
namespace Common\Controller\VisitorNotes;

use Common\Model\AbstractVisitorNotes;
use Common\Model\AidsVisitorNotes;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class AidsVisitorNotesController  extends AbstractVisitorNotesController
{
    protected $target = AbstractVisitorNotes::TARGET_AIDS;


    // initialize AidsVisitorNotes model object to use in process
    protected function getModel()
    {
        return new AidsVisitorNotes();
    }

    // get AidsVisitorNotes model using id
    protected function findOrFail($id)
    {
        $aids = AidsVisitorNotes::findOrFail($id);

        $obj= \Common\Model\Categories\AidsCategories::findOrFail($aids->category_id);
        if ($obj->type != $this->target) {
            throw (new ModelNotFoundException())->setModel(get_class($aids));
        }
        return $aids;
    }


    // get AidsVisitorNotes rows
    public function index()
    {
        $this->authorize('manage', AidsVisitorNotes::class);
        return parent::index();
    }

    // create new object model
    public function store(Request $request)
    {
        $this->authorize('create', AidsVisitorNotes::class);
        return parent::store($request);
    }

    // get template instruction 
    public function templateInstruction()
    {
        return response()->json(['download_token' => 'notes-aid-template']);
//        return response()->file(base_path('storage/app/templates/notes-aid-template.pdf'));
    }

}