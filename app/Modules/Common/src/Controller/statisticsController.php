<?php

namespace Common\Controller;

use App\Http\Controllers\Controller;
use Auth\Model\UserOrg;
use Illuminate\Http\Request;
use Organization\Model\Organization;
use Organization\Model\OrgSponsors;
use Setting\Model\PaymentCategoryI18n;

class statisticsController  extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    // create general statistic  ( sponsorships-sponsor or vouchers-sponsor or charity-act or payments
    public function getStatistic(\Illuminate\Http\Request $request)
    {
        try {
            $language_id =  \App\Http\Helpers::getLocale();
            $selectedCategory = [];
            $selectedCategoryIds = null;
            $selectedOrg = [];
            $selectedOrgIds = [];
            $selectedSponsor = [];
            $selectedSponsorIds = [];

            $type=$request->get('type');
            $action=$request->get('action');
            $user = \Auth::user();
            $UserType=$user->type;
            $UserOrg=$user->organization;
            $organization_id = $user->organization_id;
            $UserOrgLevel =$UserOrg->level;
            $categories = null;
            $first=null;
            $last=null;

            if($type =='sponsorships-sponsor' || $type =='vouchers-sponsor' || $type =='charity-act'|| $type =='payments') {
                if($request->get('first') && $request->get('first') != "" && $request->get('first') !=null){
                    $first= date('Y-m-d',strtotime($request->get('first')));
                }else{
                    $first =date('Y-01-01');
                }
                if($request->get('first') && $request->get('first') != "" && $request->get('first') !=null){
                    $last= date('Y-m-d',strtotime($request->get('last')));
                }else{
                    $last =date('Y-12-31');
                }
            }

            if(sizeof($request->selected) >0){
                $selectedOrg = $request->selected;
            }

            if(sizeof($request->selectedSponsor) > 0){
                $selectedSponsorIds = $selectedSponsor = $request->selectedSponsor;
            }else{

                if($UserType == 2){
                    $selectedSponsorIds =OrgSponsors::getOrgsConnector(UserOrg::getUserOrgIds($user->id));
                }else{
                    if($user->super_admin || $UserOrg->level == Organization::LEVEL_BRANCH_CENTER){
                        $list_ = Organization::where('type', 2)->get();
                        if(sizeof($list_) >0){
                            foreach ($list_ as $key => $value){
                                $selectedSponsorIds[]=$value->id;
                            }
                        }
                    }else{
                        $selectedSponsorIds = OrgSponsors::getConnector($organization_id);
                    }
                }
            }
            $SponsorJoin =  implode(',',$selectedSponsorIds);

            $Orgs= \DB::table('char_organizations')
                          ->where(function ($q) use ($organization_id,$selectedOrg,$user) {
                                $q->where('type', 1);
                                $q->whereNull('deleted_at');
                                if(sizeof($selectedOrg) > 0 ){
                                    $q->whereIn('id',$selectedOrg);
                                }else{
                                    if($user->type == 2) {
                                        $q->where(function ($anq) use ($organization_id,$user) {
                                            $anq->where('id',$organization_id);
                                            $anq->orwherein('id', function($decq) use($user) {
                                                $decq->select('organization_id')
                                                    ->from('char_user_organizations')
                                                    ->where('user_id', '=', $user->id);
                                            });
                                        });

                                    }else{
                                        $q->where(function ($anq) use ($organization_id) {
                                            /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                                            $anq->wherein('id', function($decq) use($organization_id) {
                                                $decq->select('descendant_id')
                                                    ->from('char_organizations_closure')
                                                    ->where('ancestor_id', '=', $organization_id);
                                            });
                                        });
                                    }
                                }
                            });

            if($type =='charity-act'){
                $Orgs=$Orgs->selectRaw(" char_organizations.id, 
                                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name,

                                 ( get_voucher_count_(char_organizations.id,'organizations','$first','$last',Null,'$SponsorJoin') +
                                   get_organization_payments_cases_count_(char_organizations.id,'organizations','$first','$last',null,'$SponsorJoin')
                                 ) as count,
                                 ( COALESCE(get_organization_vouchers_value_(char_organizations.id,'organizations','$first','$last',null,'$SponsorJoin'),0) +
                                   COALESCE(get_organization_payments_cases_amount_(char_organizations.id,'organizations','$first','$last',null,'$SponsorJoin'),0)
                                ) AS amount");


            }else{
                $Orgs=$Orgs->selectRaw("char_organizations.id, char_organizations.name");
            }


            $Orgs = $Orgs ->orderBy('char_organizations.name')->get();

            if(sizeof($selectedOrg) > 0 ){
                $selectedOrgIds = $selectedOrg ;
            }else{
                foreach ($Orgs as $key=>$value){
                    $selectedOrgIds[]=$value->id;
                }
            }

            $OrgJoin =  implode(',',$selectedOrgIds);

            if(sizeof($request->selectedCategory) >0){
                $selectedCategory = $request->selectedCategory;
                $selectedCategoryIds =  implode(',',$selectedCategory);
            }

            if($type =='vouchers-sponsor' || $type =='sponsorships-sponsor' || $type =='payments'){

                $sponsors= \DB::table('char_organizations')
                            ->where(function ($q) use ($user,$selectedSponsor,$selectedOrg,$UserOrgLevel,$selectedSponsorIds) {
                                $q->where('type', 2);
                                $q->whereNull('deleted_at');
                                $q->whereIn('id',$selectedSponsorIds);
                            });


                if($type =='sponsorships-sponsor'){
                    $sponsors->selectRaw("char_organizations.id,
                                            CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name,
                                          COALESCE(get_org_payments_cases_amount_(char_organizations.id,'sponsors','$first','$last','$selectedCategoryIds' ,'$OrgJoin'),0) AS amount,
                                          COALESCE(get_org_payments_cases_count_(char_organizations.id,'sponsors','$first','$last','$selectedCategoryIds' ,'$OrgJoin'),0) AS count");
                }elseif($type =='payments'){


                    $categories = PaymentCategoryI18n::where(function ($q) use ($selectedCategory,$language_id) {

                        $q ->where('language_id',$language_id);
                        if(sizeof($selectedCategory) > 0){
                            $q ->whereIn('payment_category_id',$selectedCategory);
                        }

                    })->orderBy('name', 'asc')->get();

                    if(!(sizeof($selectedCategory) > 0)){
                        foreach ($categories as $key=>$value){
                            $selectedCategory[]=$value->payment_category_id;
                        }

                        $selectedCategoryIds =  implode(',',$selectedCategory);
                    }

                    $sponsors->selectRaw("char_organizations.id,                       
                      CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name,
                        get_organization_payments_category_count(char_organizations.id,'sponsors','$first','$last','$selectedCategoryIds', '$OrgJoin') as count,
                        COALESCE(get_organization_payments_category_amount(char_organizations.id,'sponsors','$first','$last','$selectedCategoryIds' ,'$OrgJoin'),0) AS amount");
                }else{
                    $sponsors->selectRaw("char_organizations.id, 
                                            CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name,
                                          get_voucher_count_(char_organizations.id,'sponsors','$first','$last',Null, '$OrgJoin') as count,
                                          COALESCE(get_organization_vouchers_value_(char_organizations.id,'sponsors','$first','$last',Null, '$OrgJoin'),0) AS amount");
                }
                $sponsors=$sponsors->orderBy('char_organizations.name')->get();

                if(sizeof($sponsors) == 0){
                    return response()->json(['status' => false]);
                }

            }

            if($type =='sponsorships-sponsor'){
                $this->authorize('sponsorshipsStatistic', \Common\Model\SponsorshipsCases::class);

                $total_amount=0;
                $total_count=0;
                $sponsorship_sub_result=[];

                $sponsorship_categories = \Common\Model\Categories\SponsorshipCategories::
                where(function ($q) use ($selectedCategory) {
                    $q->where('type',1);
                    if(sizeof($selectedCategory) > 0){
                        $q ->whereIn('id',$selectedCategory);
                    }
                })
                    ->selectRaw("CASE  WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS name")
                    ->orderBy('name', 'asc')
                    ->get();

                foreach($sponsorship_categories as $kkk=>$vvv){
                    $sponsorship_sub_total_amount=0;
                    $sponsorship_sub_total_count=0;

                    $sponsorship_set= \DB::table('char_organizations')
                        ->where(function ($q) use ($user,$selectedSponsor,$selectedOrg,$UserOrgLevel,$selectedSponsorIds) {
                            $q->where('type', 2);
                            $q->whereNull('deleted_at');
                            $q->whereIn('id',$selectedSponsorIds);
                        })
                        ->selectRaw("char_organizations.id,
                                                CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name,
                                     get_organization_payments_cases_count_(char_organizations.id,'sponsors','$first','$last','$vvv->id', '$OrgJoin') as count,
                                     COALESCE(get_organization_payments_cases_amount_(char_organizations.id,'sponsors','$first','$last','$vvv->id' ,'$OrgJoin'),0) AS amount
                                    ")
                        ->orderBy('char_organizations.name')->get();

                    foreach($sponsorship_set as $k=>$v){
                        $sponsorship_sub_total_amount = $sponsorship_sub_total_amount + (int)($v->amount);
                        $sponsorship_sub_total_count = $sponsorship_sub_total_count + (int)($v->count);
                    }

                    $sponsorship_sub_result[]=['id' => $vvv->id ,'name' => $vvv->name ,'sub_total_count' => $sponsorship_sub_total_count ,'sub_total_amount' => $sponsorship_sub_total_amount , 'set' =>(object) $sponsorship_set];

                    $total_amount=$total_amount +$sponsorship_sub_total_amount;
                    $total_count=$total_count +$sponsorship_sub_total_count;

                }

                $response=['status' =>true,'result' =>$sponsorship_sub_result,
                    'sponsors'=>$sponsors,'descendants'=>$Orgs,'amount'=>$total_amount,'count'=>$total_count];
                if($request->get('action') =='show'){
                    return response()->json($response);
                }
                else{

                    $first=date('d-m-Y',strtotime($request->get('first')));
                    $last=date('d-m-Y',strtotime($request->get('last')));

                    $token = md5(uniqid());
                    \Excel::create('export_' . $token, function($excel) use($response,$first,$last){
                        $excel->setTitle(trans('lang.excel_description_b'));
                        $excel->sheet(trans('sponsorship::application.statistic'), function($sheet) use($response,$first,$last){

                            $Index = [
                                'C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                                'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
                                'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
                                'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ',
                                'DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ',
                                'EA','EB','EC','ED','EE','EF','EG','EH','EI','EJ','EK','EL','EM','EN','EO','EP','EQ','ER','ES','ET','EU','EV','EW','EX','EY','EZ',
                                'FA','FB','FC','FD','FE','FF','FG','FH','FI','FJ','FK','FL','FM','FN','FO','FP','FQ','FR','FS','FT','FU','FV','FW','FX','FY','FZ',
                                'GA','GB','GC','GD','GE','GF','GG','GH','GI','GJ','GK','GL','GM','GN','GO','GP','GQ','GR','GS','GT','GU','GV','GW','GX','GY','GZ',
                                'HA','HB','HC','HD','HE','HF','HG','HH','HI','HJ','HK','HL','HM','HN','HO','HP','HQ','HR','HS','HT','HU','HV','HW','HX','HY','HZ',
                                'IA','IB','IC','ID','IE','IF','IG','IH','II','IJ','IK','IL','IM','IN','IO','IP','IQ','IR','IS','IT','IU','IV','IW','IX','IY','IZ',
                                'JA','JB','JC','JD','JE','JF','JG','JH','JI','JJ','JK','JL','JM','JN','JO','JP','JQ','JR','JS','JT','JU','JV','JW','JX','JY','JZ',
                                'KA','KB','KC','KD','KE','KF','KG','KH','KI','KJ','KK','KL','KM','KN','KO','KP','KQ','KR','KS','KT','KU','KV','KW','KX','KY','KZ',
                                'LA','LB','LC','LD','LE','LF','LG','LH','LI','LJ','LK','LL','LM','LN','LO','LP','LQ','LR','LS','LT','LU','LV','LW','LX','LY','LZ',
                                'MA','MB','MC','MD','ME','MF','MG','MH','MI','MJ','MK','ML','MM','MN','MO','MP','MQ','MR','MS','MT','MU','MV','MW','MX','MY','MZ',
                            ];

                            $sheet->setOrientation('landscape');
                            $sheet->setRightToLeft(true);

                            $i=sizeof($response['sponsors']);
                            $style_052 = array(
                                 'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 15,
                                    'bold' => true
                                ] ,
                                'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )
                            );

                            $sheet->setCellValue('A1',trans('sponsorship::application.statistic'). '  ' .trans('sponsorship::application.range') . ' ( ' . $first . ' : ' .  $last .  ' ) ' );

                            $sheet->mergeCells('A1:'.$Index[($i*2)+1].'1');
                            $sheet->setHeight(array(1=> 45));
                            $sheet->getStyle('A1')->applyFromArray($style_052);

                            $sheet ->setCellValue('A2',trans('sponsorship::application.#'));
                            $sheet ->setCellValue('B2',trans('sponsorship::application.organizations_name'));
                            $sheet ->setCellValue('B3',trans('common::application.type'));
                            $sheet->mergeCells('A2:A3');

                            $sheet->setWidth(array('B'=> 25));
                            $style_20 = array(
                                 'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 10,
                                    'bold' => true
                                ],
                                'fill' => array(
                                    'type' => 'solid',
                                    'color' => array('rgb' => 'D3D3D3')
                                ) ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );
                            $sheet->getStyle('A2')->applyFromArray($style_20);
                            $sheet->getStyle('B2')->applyFromArray($style_20);
                            $sheet->getStyle('B3')->applyFromArray($style_20);

                            $sheet ->setCellValue($Index[$i*2].'2',trans('common::application.total'));
                            $sheet->mergeCells($Index[$i*2].'2:'.$Index[($i*2)+1].'2');

                            $sheet ->setCellValue($Index[$i*2].'3',trans('common::application.counts'));
                            $sheet ->setCellValue($Index[($i*2)+1].'3',trans('common::application.amount in shikel'));


                            $sheet->setWidth(array($Index[$i*2]=> 6,$Index[($i*2)+1]=> 6));

                            $style_320 = array(
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 11,
                                    'bold' => true
                                ],
                                'fill' => array(
                                    'type' => 'solid',
                                    'color' => array('rgb' => 'D3D3D3')
                                ) ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );
                            $style_2222 = array(
                                 'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 10,
                                    'bold' => true
                                ] ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );
                            $style_1 = array(
                                 'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 11,
                                    'bold' => true
                                ],
                                'fill' => array(
                                    'type' => 'solid',
                                    'color' => array('rgb' => 'D3D3D3')
                                ) ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );
                            $style_2 = array(
                                 'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 10,
                                    'bold' => true
                                ] ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );
                            $style_26 = array(
                                 'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 11,
                                    'bold' => true,
                                ],
                                'fill' => array(
                                    'type' => 'solid',
                                    'color' => array('rgb' => 'D3D3D3')
                                ),
                                'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );

                            $sheet->getStyle($Index[$i*2].'2')->applyFromArray($style_320);
                            $sheet->getStyle($Index[$i*2].'3')->applyFromArray($style_2222);
                            $sheet->getStyle($Index[($i*2)+1].'3')->applyFromArray($style_2222);

                            $z=0;
                            foreach($response['sponsors'] as $k=>$v )
                            {
                                $sheet ->setCellValue($Index[$z].'2',$v->name);

                                $sheet->mergeCells($Index[$z].'2:'.$Index[$z+1].'2');
                                $sheet ->setCellValue($Index[$z].'3',trans('common::application.counts'));
                                $sheet ->setCellValue($Index[$z+1].'3',trans('common::application.amount in shikel'));

                                $sheet->setHeight(array(2=>65, 3 => 30));
                                $sheet->setWidth(array($Index[$z]=> 6, $Index[$z] => 6));
                                $sheet->getStyle($Index[$z].'2')->applyFromArray($style_1);
                                $sheet->getStyle($Index[$z].'3')->applyFromArray($style_2);
                                $sheet->getStyle($Index[$z+1].'3')->applyFromArray($style_2);


                                $z=$z+2;
                            }

                            $zz=4;
                            foreach($response['result'] as $key => $value)
                            {
                                $sheet ->setCellValue('A'.$zz,$key+1);
                                $sheet ->setCellValue('B'.$zz,$value['name']);

                                $z=0;
                                foreach($value['set'] as $k2=>$v2 )
                                {
                                    $sheet ->setCellValue($Index[$z].$zz,$v2->count);
                                    $sheet ->setCellValue($Index[$z+1].$zz,$v2->amount);

                                    $z=$z+2;
                                }

                                $sheet ->setCellValue($Index[$z].$zz,$value['sub_total_count']);
                                $sheet ->setCellValue($Index[$z+1].$zz,$value['sub_total_amount']);

                                $zz++;
                            }

                            $s= 4 +sizeof($response['result']);
                            $sheet ->setCellValue('A'.$s,trans('common::application.total'));
                            $sheet->mergeCells('A'.$s.':B'.$s);

                            $sheet->getStyle('A'.$s)->applyFromArray($style_26);


                            $z=0;
                            foreach($response['sponsors'] as $k=>$v )
                            {
                                $sheet ->setCellValue($Index[$z].$s,$v->count);
                                $sheet ->setCellValue($Index[$z+1].$s,$v->amount);

                                $sheet->setHeight(array(2=>65, 3 => 30));
                                $sheet->setWidth(array($Index[$z]=> 6, $Index[$z] => 6));
                                $sheet->getStyle($Index[$z].$s)->applyFromArray($style_26);
                                $sheet->getStyle($Index[$z+1].$s)->applyFromArray($style_26);
                                $z=$z+2;
                            }
                            $sheet ->setCellValue($Index[$z].$s,$response['count']);
                            $sheet ->setCellValue($Index[$z+1].$s,$response['amount']);
                            $sheet->getStyle($Index[$z].$s)->applyFromArray($style_26);
                            $sheet->getStyle($Index[$z+1].$s)->applyFromArray($style_26);
                            $style = [
                                 'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Simplified Arabic',
                                    'size' => 11,
                                    'bold' => true
                                ] ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )
                            ];

                            $sheet->getDefaultStyle()->applyFromArray($style);
                        });
                    })
                        ->store('xlsx', storage_path('tmp/'));
                    return response()->json([
                        'download_token' => $token,
                    ]);


                }

            }
            else if($type =="islamic-commitment"){
                $this->authorize('islamicCommitmentStatistic', \Common\Model\SponsorshipsCases::class);
                $statistics=\Common\Model\PersonModels\PersonIslamic::get_org_islamic_statistic($request->all(),$Orgs);
                if($action =='show'){
                    return response()->json(['status' =>true,'result'=>$statistics,'descendants' =>$Orgs]);
                }else{
                    if(sizeof($statistics) !=0) {
                        $data=[];
                        foreach ($statistics as $key => $value) {
                            $data[$key]['#'] = $key + 1;
                            foreach ($value as $k => $v) {
                                if($k != 'id'){
                                    $data[$key][$k] = $v;
                                }
                            }
                        }
                        $token = md5(uniqid());
                        \Excel::create('export_' . $token, function($excel) use($data){
                            $excel->setTitle(trans('lang.excel_description_b'));
                            $excel->sheet(trans('common::application.islamic-commitment-statistic_'), function($sheet) use($data){
                                $sheet->setOrientation('landscape');

                                $sheet ->setCellValue('A1',trans('sponsorship::application.islamic-commitment-statistic'));
                                $sheet ->setCellValue('A2',trans('sponsorship::application.#'));
                                $sheet ->setCellValue('B2',trans('sponsorship::application.organizations_name'));
                                $sheet ->setCellValue('C2',trans('sponsorship::application.enrollment of save qurancenter'));
                                $sheet ->setCellValue('H2',trans('sponsorship::application.go to the mosque but not go to save qurancenter'));
                                $sheet ->setCellValue('M2',trans('sponsorship::application.Uncommitted'));
                                $sheet ->setCellValue('R2',trans('sponsorship::application.total'));

                                $CellValue = ['C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q'];

                                $CellValue_1 = ['C','H','M'];
                                $CellValue_2 = ['D','I','N'];
                                $CellValue_3 = ['E','J','O'];
                                $CellValue_4 = ['F','K','P'];
                                $total = ['G','L','Q'];


                                foreach($total as $key => $value)
                                {
                                    $sheet ->setCellValue($value.'3',trans('sponsorship::application.sub total'));
                                }

                                foreach($CellValue_1 as $key => $value)
                                {
                                    $sheet ->setCellValue($value.'3',trans('sponsorship::application.under 8'));
                                }

                                foreach($CellValue_2 as $key => $value)
                                {
                                    $sheet ->setCellValue($value.'3',trans('sponsorship::application.8-12'));
                                }

                                foreach($CellValue_3 as $key => $value)
                                {
                                    $sheet ->setCellValue($value.'3',trans('sponsorship::application.12-15'));
                                }

                                foreach($CellValue_4 as $key => $value)
                                {
                                    $sheet ->setCellValue($value.'3',trans('sponsorship::application.above 15'));
                                }

                                $mergeCells=['A1:R1','A2:A3','B2:B3','C2:G2','H2:L2','M2:q2','R2:R3'];
                                foreach($mergeCells as $key => $value)
                                {
                                    $sheet->mergeCells($value);
                                }


                                $sheet->setRightToLeft(true);
                                $style = [
                                     'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                    'font' => [
                                        'name' => 'Simplified Arabic',
                                        'size' => 11,
                                        'bold' => true
                                    ]
                                ];

                                $sheet->getDefaultStyle()->applyFromArray($style);
                                $sheet->freezeFirstRow();
                                $arry = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R'];
                                $count=sizeof($data)+3;

                                foreach($arry as $key => $value)
                                {
                                    $target= $value.'1:'.$value. $count;
                                    $sheet->cell($target, function($cell) {
                                        $cell->setAlignment('center');
                                        $cell->setValignment('center');
                                        $cell->setFont(array(
                                            'family'     => 'Simplified Arabic',
                                            'size'       => '11',
                                            'bold'       =>  true
                                        ));
                                        $cell->setBorder(array(
                                            'top'   => array(
                                                'style' => 'solid'
                                            ),
                                        ));
                                    });
                                    $sheet->setFreeze($value.'1');
                                    $sheet->setFreeze($value.'2');
                                    $sheet->setFreeze($value.'3');
                                }

                                $sheet->setShowGridlines(false);
                                $sheet->setHeight(array(1 => 35,2 =>30, 3 => 30));
                                $sheet->setWidth(array('A'=> 5, 'B'=>35, 'C' => 10));

                                $arr_2 = ['D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R'];
                                foreach($arr_2 as $key => $value)
                                {
                                    $sheet->setWidth([$value => 10]);
                                }

                                $i=4;
                                foreach($data as $key => $value)
                                {
                                    $z = 0; // first key of $arry
                                    $sheet->setHeight(array($i => 25));
                                    foreach($value as $k =>$v) {
                                        $sheet ->setCellValue($arry[$z] .$i,$v);
                                        $z++;
                                    }
                                    $i++;
                                }

                                $range='A1:R'.$count=sizeof($data)+3;
                                $sheet->setBorder($range, 'thin');

                            });
                        })
                            ->store('xlsx', storage_path('tmp/'));
                        return response()->json([
                            'download_token' => $token,
                        ]);

                    }
                }
            }
            else if($type =='vouchers-sponsor'){
                $this->authorize('vouchersStatistic', \Common\Model\AidsCases::class);
                $categories = \Aid\Model\VoucherCategories::orderBy('name', 'asc')->get();

                $total_amount=0;
                $total_count=0;
                $sub_result=[];

                foreach($categories as $kk=>$vv){
                    $sub_total_amount=0;
                    $sub_total_count=0;

                    $set= \DB::table('char_organizations')
                        ->where(function ($q) use ($user,$organization_id,$selectedSponsor,$selectedOrg) {
                            $q->where('char_organizations.type', 2);
                            $q->whereNull('char_organizations.deleted_at');
                            if(sizeof($selectedSponsor) > 0 ){
                                $q->whereIn('id',$selectedSponsor);
                            }else{
                                $q->whereIn('id', function($query) use($organization_id,$selectedOrg,$user) {
                                    $query->select('sponsor_id')->from('char_organization_sponsors');
                                    if(sizeof($selectedOrg) > 0 ){
                                        $query->whereIn('organization_id',$selectedOrg);
                                    }else{
                                        if($user->type == 2) {
                                            $query->where(function ($anq) use ($organization_id,$user) {
                                                $anq->where('organization_id',$organization_id);
                                                $anq->orwherein('organization_id', function($decq) use($user) {
                                                    $decq->select('organization_id')
                                                        ->from('char_user_organizations')
                                                        ->where('user_id', '=', $user->id);
                                                });
                                            });

                                        }else{
                                            $query->where(function ($anq) use ($organization_id) {
                                                /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                                                $anq->wherein('organization_id', function($decq) use($organization_id) {
                                                    $decq->select('descendant_id')
                                                        ->from('char_organizations_closure')
                                                        ->where('ancestor_id', '=', $organization_id);
                                                });
                                            });
                                        }
                                    }
                                });
                            }
                        })
                        ->selectRaw("char_organizations.id, 
                                                CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name,
                                     get_voucher_count_(char_organizations.id,'sponsors','$first','$last','$vv->id', '$OrgJoin') as count,
                                     COALESCE(get_organization_vouchers_value_(char_organizations.id,'sponsors','$first','$last','$vv->id', '$OrgJoin'),0) AS amount")
                        ->orderBy('char_organizations.name')->get();

                    foreach($set as $k=>$v){
                        $sub_total_amount = $sub_total_amount + (int)($v->amount);
                        $sub_total_count = $sub_total_count + (int)($v->count);
                    }

                    $sub_result[]=['id' => $vv->id ,'name' => $vv->name ,'sub_total_count' => $sub_total_count ,'sub_total_amount' => $sub_total_amount , 'set' =>(object) $set];

                    $total_amount=$total_amount +$sub_total_amount;
                    $total_count=$total_count +$sub_total_count;

                }

                $response=['status' =>true,'result' =>$sub_result,'sponsors'=>$sponsors,'descendants'=>$Orgs,'amount'=>$total_amount,'count'=>$total_count];

                if($request->get('action') =='show'){
                    return response()->json($response);
                }
                else{
                    $first=date('d-m-Y',strtotime($request->get('first')));
                    $last=date('d-m-Y',strtotime($request->get('last')));

                    $token = md5(uniqid());
                    \Excel::create('export_' . $token, function($excel) use($response,$first,$last){
                        $excel->setTitle(trans('lang.excel_description_b'));
                        $excel->sheet(trans('sponsorship::application.statistic'), function($sheet) use($response,$first,$last){

                            $Index = [
                                'C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                                'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
                                'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
                                'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ',
                                'DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ',
                                'EA','EB','EC','ED','EE','EF','EG','EH','EI','EJ','EK','EL','EM','EN','EO','EP','EQ','ER','ES','ET','EU','EV','EW','EX','EY','EZ',
                                'FA','FB','FC','FD','FE','FF','FG','FH','FI','FJ','FK','FL','FM','FN','FO','FP','FQ','FR','FS','FT','FU','FV','FW','FX','FY','FZ',
                                'GA','GB','GC','GD','GE','GF','GG','GH','GI','GJ','GK','GL','GM','GN','GO','GP','GQ','GR','GS','GT','GU','GV','GW','GX','GY','GZ',
                                'HA','HB','HC','HD','HE','HF','HG','HH','HI','HJ','HK','HL','HM','HN','HO','HP','HQ','HR','HS','HT','HU','HV','HW','HX','HY','HZ',
                                'IA','IB','IC','ID','IE','IF','IG','IH','II','IJ','IK','IL','IM','IN','IO','IP','IQ','IR','IS','IT','IU','IV','IW','IX','IY','IZ',
                                'JA','JB','JC','JD','JE','JF','JG','JH','JI','JJ','JK','JL','JM','JN','JO','JP','JQ','JR','JS','JT','JU','JV','JW','JX','JY','JZ',
                                'KA','KB','KC','KD','KE','KF','KG','KH','KI','KJ','KK','KL','KM','KN','KO','KP','KQ','KR','KS','KT','KU','KV','KW','KX','KY','KZ',
                                'LA','LB','LC','LD','LE','LF','LG','LH','LI','LJ','LK','LL','LM','LN','LO','LP','LQ','LR','LS','LT','LU','LV','LW','LX','LY','LZ',
                                'MA','MB','MC','MD','ME','MF','MG','MH','MI','MJ','MK','ML','MM','MN','MO','MP','MQ','MR','MS','MT','MU','MV','MW','MX','MY','MZ',
                            ];

                            $sheet->setOrientation('landscape');
                            $sheet->setRightToLeft(true);

                            $i=sizeof($response['descendants']);
                            $style_052 = array(
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 15,
                                    'bold' => true
                                ] ,
                                'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )
                            );

                            $sheet->setCellValue('A1',trans('aid::application.sponsor-voucher-statistic'). '  ' .trans('aid::application.range') . ' ( ' . $first . ' : ' .  $last .  ' ) ' );
                            $sheet->mergeCells('A1:'.$Index[($i*2)+1].'1');
                            $sheet->setHeight(array(1=> 45));
                            $sheet->getStyle('A1')->applyFromArray($style_052);

                            $sheet ->setCellValue('A2',trans('aid::application.#'));
                            $sheet ->setCellValue('B2',trans('aid::application.voucher_sponsor_name'));
                            $sheet ->setCellValue('B3',trans('common::application.type'));
                            $sheet->mergeCells('A2:A3');

                            $sheet->setWidth(array('B'=> 25));
                            $style_20 = array(
                                 'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 10,
                                    'bold' => true
                                ],
                                'fill' => array(
                                    'type' => 'solid',
                                    'color' => array('rgb' => 'D3D3D3')
                                ) ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );
                            $sheet->getStyle('A2')->applyFromArray($style_20);
                            $sheet->getStyle('B2')->applyFromArray($style_20);
                            $sheet->getStyle('B3')->applyFromArray($style_20);

                            $sheet ->setCellValue($Index[$i*2].'2',trans('common::application.total'));
                            $sheet->mergeCells($Index[$i*2].'2:'.$Index[($i*2)+1].'2');

                            $sheet ->setCellValue($Index[$i*2].'3',trans('common::application.counts'));
                            $sheet ->setCellValue($Index[($i*2)+1].'3',trans('common::application.amount in shikel'));


                            $sheet->setWidth(array($Index[$i*2]=> 6,$Index[($i*2)+1]=> 6));

                            $style_320 = array(
                                 'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 11,
                                    'bold' => true
                                ],
                                'fill' => array(
                                    'type' => 'solid',
                                    'color' => array('rgb' => 'D3D3D3')
                                ) ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );
                            $style_2222 = array(
                                 'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 10,
                                    'bold' => true
                                ] ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );


                            $style_1 = array(
                                 'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 11,
                                    'bold' => true
                                ],
                                'fill' => array(
                                    'type' => 'solid',
                                    'color' => array('rgb' => 'D3D3D3')
                                ) ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );
                            $style_2 = array(
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 10,
                                    'bold' => true
                                ] ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );

                            $style_26 = array(
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 11,
                                    'bold' => true,
                                ],
                                'fill' => array(
                                    'type' => 'solid',
                                    'color' => array('rgb' => 'D3D3D3')
                                ),
                                'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );
                            $sheet->getStyle($Index[$i*2].'2')->applyFromArray($style_320);
                            $sheet->getStyle($Index[$i*2].'3')->applyFromArray($style_2222);
                            $sheet->getStyle($Index[($i*2)+1].'3')->applyFromArray($style_2222);


                            $z=0;
                            foreach($response['sponsors'] as $k=>$v )
                            {
                                $sheet ->setCellValue($Index[$z].'2',$v->name);

                                $sheet->mergeCells($Index[$z].'2:'.$Index[$z+1].'2');
                                $sheet ->setCellValue($Index[$z].'3',trans('common::application.counts'));
                                $sheet ->setCellValue($Index[$z+1].'3',trans('common::application.amount in shikel'));

                                $sheet->setHeight(array(2=>65, 3 => 30));
                                $sheet->setWidth(array($Index[$z]=> 6, $Index[$z] => 6));
                                $sheet->getStyle($Index[$z].'2')->applyFromArray($style_1);
                                $sheet->getStyle($Index[$z].'3')->applyFromArray($style_2);
                                $sheet->getStyle($Index[$z+1].'3')->applyFromArray($style_2);


                                $z=$z+2;
                            }

                            $zz=4;
                            foreach($response['result'] as $key => $value)
                            {
                                $sheet ->setCellValue('A'.$zz,$key+1);
                                $sheet ->setCellValue('B'.$zz,$value['name']);

                                $z=0;
                                foreach($value['set'] as $k2=>$v2 )
                                {
                                    $sheet ->setCellValue($Index[$z].$zz,$v2->count);
                                    $sheet ->setCellValue($Index[$z+1].$zz,$v2->amount);

                                    $z=$z+2;
                                }

                                $sheet ->setCellValue($Index[$z].$zz,$value['sub_total_count']);
                                $sheet ->setCellValue($Index[$z+1].$zz,$value['sub_total_amount']);

                                $zz++;
                            }


                            $s= 5 + sizeof($response['result']);

                            $sheet ->setCellValue('A'.$s,trans('common::application.total'));
                            $sheet->mergeCells('A'.$s.':B'.$s);

                            $sheet->getStyle('A'.$s)->applyFromArray($style_26);



                            $z=0;
                            foreach($response['sponsors'] as $k=>$v )
                            {
                                $sheet ->setCellValue($Index[$z].$s,$v->count);
                                $sheet ->setCellValue($Index[$z+1].$s,$v->amount);

                                $sheet->setHeight(array(2=>65, 3 => 30));
                                $sheet->setWidth(array($Index[$z]=> 6, $Index[$z] => 6));
                                $sheet->getStyle($Index[$z].$s)->applyFromArray($style_26);
                                $sheet->getStyle($Index[$z+1].$s)->applyFromArray($style_26);
                                $z=$z+2;
                            }
                            $sheet ->setCellValue($Index[$z].$s,$response['count']);
                            $sheet ->setCellValue($Index[$z+1].$s,$response['amount']);
                            $sheet->getStyle($Index[$z].$s)->applyFromArray($style_26);
                            $sheet->getStyle($Index[$z+1].$s)->applyFromArray($style_26);
                            $style = [
                              'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Simplified Arabic',
                                    'size' => 11,
                                    'bold' => true
                                ] ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )
                            ];

                            $sheet->getDefaultStyle()->applyFromArray($style);
                        });
                    })
                        ->store('xlsx', storage_path('tmp/'));

                    return response()->json(['status' =>true,'download_token' => $token]);
                }

            }
            else if($type =='charity-act'){
                $this->authorize('charityActStatistic', \Common\Model\AidsCases::class);
                $categories = \Aid\Model\VoucherCategories::orderBy('name', 'asc')->get();
                $total_amount=0;
                $total_count=0;
                $sub_result=[];
                foreach($categories as $kk=>$vv){
                    $sub_total_amount=0;
                    $sub_total_count=0;

                    $set= \DB::table('char_organizations')
                        ->where(function ($q) use ($user,$organization_id,$selectedOrg) {
                            $q->where('type', 1);
                            $q->whereNull('deleted_at');
                            if(sizeof($selectedOrg) > 0 ){
                                $q->whereIn('id',$selectedOrg);
                            }else{
                                if($user->type == 2) {
                                     $q->where(function ($anq) use ($organization_id,$user) {
                                        $anq->where('id',$organization_id);
                                        $anq->orwherein('id', function($decq) use($user) {
                                            $decq->select('organization_id')
                                                ->from('char_user_organizations')
                                                ->where('user_id', '=', $user->id);
                                        });
                                    });

                                }else{
                                    $q->where(function ($anq) use ($organization_id) {
                                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                                        $anq->wherein('id', function($decq) use($organization_id) {
                                            $decq->select('descendant_id')
                                                ->from('char_organizations_closure')
                                                ->where('ancestor_id', '=', $organization_id);
                                        });
                                    });
                                }
                            }

                        })
                        ->selectRaw("char_organizations.id, 
                                                CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name,
                                     get_voucher_count_(char_organizations.id,'organizations','$first','$last','$vv->id','$SponsorJoin') as count,
                                     (COALESCE(get_organization_vouchers_value_(char_organizations.id,'organizations','$first','$last','$vv->id','$SponsorJoin'),0)) AS amount")
                        ->orderBy('char_organizations.name')->get();

                    foreach($set as $k=>$v){
                        $sub_total_amount = $sub_total_amount + (int)($v->amount);
                        $sub_total_count = $sub_total_count + (int)($v->count);
                    }
                    $sub_result[]=['id' => $vv->id ,'name' => $vv->name ,'sub_total_count' => $sub_total_count ,'sub_total_amount' => $sub_total_amount , 'set' =>(object) $set];
                    $total_amount=$total_amount +$sub_total_amount;
                    $total_count=$total_count +$sub_total_count;
                }

                /****************************************************************************************************/
                $sponsorship_sub_result=[];
                $sponsorship_categories = \Common\Model\Categories\SponsorshipCategories::where('type',1)->orderBy('name', 'asc')
                    ->selectRaw("CASE  WHEN $language_id = 1 THEN char_categories.name Else char_categories.en_name END  AS name")->get();
                foreach($sponsorship_categories as $kkk=>$vvv){
                    $sponsorship_sub_total_amount=0;
                    $sponsorship_sub_total_count=0;
                    $sponsorship_set= \DB::table('char_organizations')
                        ->where(function ($q) use ($user,$organization_id,$selectedOrg) {
                            $q->where('type', 1);
                            $q->whereNull('deleted_at');
                            if(sizeof($selectedOrg) > 0 ){
                                $q->whereIn('id',$selectedOrg);
                            }else{
                                if($user->type == 2) {
                                    $q->where(function ($anq) use ($organization_id,$user) {
                                        $anq->where('id',$organization_id);
                                        $anq->orwherein('id', function($decq) use($user) {
                                            $decq->select('organization_id')
                                                ->from('char_user_organizations')
                                                ->where('user_id', '=', $user->id);
                                        });
                                    });

                                }else{
                                    $q->where(function ($anq) use ($organization_id) {
                                        /*$ancestor = \Organization\Model\Organization::getAncestor($organization_id);*/
                                        $anq->wherein('id', function($decq) use($organization_id) {
                                            $decq->select('descendant_id')
                                                ->from('char_organizations_closure')
                                                ->where('ancestor_id', '=', $organization_id);
                                        });
                                    });
                                }
                            }

                        })
                        ->selectRaw("char_organizations.id,
                        CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name,
                                     get_organization_payments_cases_count_(char_organizations.id,'organizations','$first','$last','$vvv->id','$SponsorJoin') as count,
                                     COALESCE(get_organization_payments_cases_amount_(char_organizations.id,'organizations','$first','$last','$vvv->id','$SponsorJoin'),0) AS amount")
                        ->orderBy('char_organizations.name')

                        ->get();


                    foreach($sponsorship_set as $k=>$v){
                        $sponsorship_sub_total_amount = $sponsorship_sub_total_amount + (int)($v->amount);
                        $sponsorship_sub_total_count = $sponsorship_sub_total_count + (int)($v->count);
                    }

                    $sponsorship_sub_result[]=['id' => $vvv->id ,'name' => $vvv->name ,'sub_total_count' => $sponsorship_sub_total_count ,'sub_total_amount' => $sponsorship_sub_total_amount , 'set' =>(object) $sponsorship_set];

                    $total_amount=$total_amount +$sponsorship_sub_total_amount;
                    $total_count=$total_count +$sponsorship_sub_total_count;

                }

                $response=['status' =>true,'result' =>$sub_result,'sponsorship_result' =>$sponsorship_sub_result,'descendants'=>$Orgs,'amount'=>$total_amount,'count'=>$total_count];

                if($request->get('action') =='show'){
                    return response()->json($response);
                }
                else{


                    $first=date('d-m-Y',strtotime($request->get('first')));
                    $last=date('d-m-Y',strtotime($request->get('last')));

                    $token = md5(uniqid());
                    \Excel::create('export_' . $token, function($excel) use($response,$first,$last){
                        $excel->setTitle(trans('lang.excel_description_b'));
                        $excel->sheet(trans('sponsorship::application.statistic'), function($sheet) use($response,$first,$last){

                            $Index = [
                                'C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                                'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
                                'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
                                'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ',
                                'DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ',
                                'EA','EB','EC','ED','EE','EF','EG','EH','EI','EJ','EK','EL','EM','EN','EO','EP','EQ','ER','ES','ET','EU','EV','EW','EX','EY','EZ',
                                'FA','FB','FC','FD','FE','FF','FG','FH','FI','FJ','FK','FL','FM','FN','FO','FP','FQ','FR','FS','FT','FU','FV','FW','FX','FY','FZ',
                                'GA','GB','GC','GD','GE','GF','GG','GH','GI','GJ','GK','GL','GM','GN','GO','GP','GQ','GR','GS','GT','GU','GV','GW','GX','GY','GZ',
                                'HA','HB','HC','HD','HE','HF','HG','HH','HI','HJ','HK','HL','HM','HN','HO','HP','HQ','HR','HS','HT','HU','HV','HW','HX','HY','HZ',
                                'IA','IB','IC','ID','IE','IF','IG','IH','II','IJ','IK','IL','IM','IN','IO','IP','IQ','IR','IS','IT','IU','IV','IW','IX','IY','IZ',
                                'JA','JB','JC','JD','JE','JF','JG','JH','JI','JJ','JK','JL','JM','JN','JO','JP','JQ','JR','JS','JT','JU','JV','JW','JX','JY','JZ',
                                'KA','KB','KC','KD','KE','KF','KG','KH','KI','KJ','KK','KL','KM','KN','KO','KP','KQ','KR','KS','KT','KU','KV','KW','KX','KY','KZ',
                                'LA','LB','LC','LD','LE','LF','LG','LH','LI','LJ','LK','LL','LM','LN','LO','LP','LQ','LR','LS','LT','LU','LV','LW','LX','LY','LZ',
                                'MA','MB','MC','MD','ME','MF','MG','MH','MI','MJ','MK','ML','MM','MN','MO','MP','MQ','MR','MS','MT','MU','MV','MW','MX','MY','MZ',
                            ];

                            $sheet->setOrientation('landscape');
                            $sheet->setRightToLeft(true);

                            $i=sizeof($response['descendants']);
                            $style_052 = array(
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 15,
                                    'bold' => true
                                ] ,
                                'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )
                            );

                            $sheet->setCellValue('A1',trans('sponsorship::application.statistic'). '  ' .trans('sponsorship::application.range') . ' ( ' . $first . ' : ' .  $last .  ' ) ' );

                            $sheet->mergeCells('A1:'.$Index[($i*2)+1].'1');
                            $sheet->setHeight(array(1=> 45));
                            $sheet->getStyle('A1')->applyFromArray($style_052);

                            $sheet ->setCellValue('A2',trans('sponsorship::application.#'));
                            $sheet ->setCellValue('B2',trans('sponsorship::application.organizations_name'));
                            $sheet ->setCellValue('B3',trans('sponsorship::application.project'));
                            $sheet->mergeCells('A2:A3');

                            $sheet->setWidth(array('B'=> 25));
                            $style_20 = array(
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 10,
                                    'bold' => true
                                ],
                                'fill' => array(
                                    'type' => 'solid',
                                    'color' => array('rgb' => 'D3D3D3')
                                ) ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );
                            $sheet->getStyle('A2')->applyFromArray($style_20);
                            $sheet->getStyle('B2')->applyFromArray($style_20);
                            $sheet->getStyle('B3')->applyFromArray($style_20);

                            $sheet ->setCellValue($Index[$i*2].'2',trans('common::application.total'));
                            $sheet->mergeCells($Index[$i*2].'2:'.$Index[($i*2)+1].'2');

                            $sheet ->setCellValue($Index[$i*2].'3',trans('common::application.counts'));
                            $sheet ->setCellValue($Index[($i*2)+1].'3',trans('common::application.amount in shikel'));


                            $sheet->setWidth(array($Index[$i*2]=> 6,$Index[($i*2)+1]=> 6));

                            $style_320 = array(
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 11,
                                    'bold' => true
                                ],
                                'fill' => array(
                                    'type' => 'solid',
                                    'color' => array('rgb' => 'D3D3D3')
                                ) ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );
                            $style_2222 = array(
                               'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 10,
                                    'bold' => true
                                ] ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );


                            $style_1 = array(
                               'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 11,
                                    'bold' => true
                                ],
                                'fill' => array(
                                    'type' => 'solid',
                                    'color' => array('rgb' => 'D3D3D3')
                                ) ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );
                            $style_2 = array(
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 10,
                                    'bold' => true
                                ] ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );

                            $style_26 = array(
                               'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 11,
                                    'bold' => true,
                                ],
                                'fill' => array(
                                    'type' => 'solid',
                                    'color' => array('rgb' => 'D3D3D3')
                                ),
                                'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );
                            $sheet->getStyle($Index[$i*2].'2')->applyFromArray($style_320);
                            $sheet->getStyle($Index[$i*2].'3')->applyFromArray($style_2222);
                            $sheet->getStyle($Index[($i*2)+1].'3')->applyFromArray($style_2222);


                            $z=0;
                            foreach($response['descendants'] as $k=>$v )
                            {
                                $sheet ->setCellValue($Index[$z].'2',$v->name);

                                $sheet->mergeCells($Index[$z].'2:'.$Index[$z+1].'2');
                                $sheet ->setCellValue($Index[$z].'3',trans('common::application.counts'));
                                $sheet ->setCellValue($Index[$z+1].'3',trans('common::application.amount in shikel'));

                                $sheet->setHeight(array(2=>65, 3 => 30));
                                $sheet->setWidth(array($Index[$z]=> 6, $Index[$z] => 6));
                                $sheet->getStyle($Index[$z].'2')->applyFromArray($style_1);
                                $sheet->getStyle($Index[$z].'3')->applyFromArray($style_2);
                                $sheet->getStyle($Index[$z+1].'3')->applyFromArray($style_2);


                                $z=$z+2;
                            }

                            $sheet ->setCellValue('A4',trans('common::application.aid projects'));
                            $sheet->mergeCells('A4:B4');



                            $sheet->getStyle('A4')->applyFromArray($style_26);



                            $zz=5;
                            foreach($response['result'] as $key => $value)
                            {
                                $sheet ->setCellValue('A'.$zz,$key+1);
                                $sheet ->setCellValue('B'.$zz,$value['name']);

                                $z=0;
                                foreach($value['set'] as $k2=>$v2 )
                                {
                                    $sheet ->setCellValue($Index[$z].$zz,$v2->count);
                                    $sheet ->setCellValue($Index[$z+1].$zz,$v2->amount);

                                    $z=$z+2;
                                }

                                $sheet ->setCellValue($Index[$z].$zz,$value['sub_total_count']);
                                $sheet ->setCellValue($Index[$z+1].$zz,$value['sub_total_amount']);

                                $zz++;
                            }



                            $sheet ->setCellValue('A'.(5 + sizeof($response['result'])),trans('common::application.sponsorship projects'));
                            $sheet->mergeCells('A'.(5 + sizeof($response['result'])).':B'.(5 + sizeof($response['result'])));

                            $sheet->getStyle('A'.(5 + sizeof($response['result'])))->applyFromArray($style_26);


                            $zz= 6 + sizeof($response['result']);
                            foreach($response['sponsorship_result'] as $key => $value)
                            {
                                $sheet ->setCellValue('A'.$zz,$key+1);
                                $sheet ->setCellValue('B'.$zz,$value['name']);

                                $z=0;
                                foreach($value['set'] as $k2=>$v2 )
                                {
                                    $sheet ->setCellValue($Index[$z].$zz,$v2->count);
                                    $sheet ->setCellValue($Index[$z+1].$zz,$v2->amount);

                                    $z=$z+2;
                                }

                                $sheet ->setCellValue($Index[$z].$zz,$value['sub_total_count']);
                                $sheet ->setCellValue($Index[$z+1].$zz,$value['sub_total_amount']);

                                $zz++;
                            }

                            $s=6 +sizeof($response['result'])+ sizeof($response['sponsorship_result']);
                            $sheet ->setCellValue('A'.$s,trans('common::application.totals'));
                            $sheet->mergeCells('A'.$s.':B'.$s);

                            $sheet->getStyle('A'.$s)->applyFromArray($style_26);



                            $z=0;
                            foreach($response['descendants'] as $k=>$v )
                            {
                                $sheet ->setCellValue($Index[$z].$s,$v->count);
                                $sheet ->setCellValue($Index[$z+1].$s,$v->amount);

                                $sheet->setHeight(array(2=>65, 3 => 30));
                                $sheet->setWidth(array($Index[$z]=> 6, $Index[$z] => 6));
                                $sheet->getStyle($Index[$z].$s)->applyFromArray($style_26);
                                $sheet->getStyle($Index[$z+1].$s)->applyFromArray($style_26);
                                $z=$z+2;
                            }
                            $sheet ->setCellValue($Index[$z].$s,$response['count']);
                            $sheet ->setCellValue($Index[$z+1].$s,$response['amount']);
                            $sheet->getStyle($Index[$z].$s)->applyFromArray($style_26);
                            $sheet->getStyle($Index[$z+1].$s)->applyFromArray($style_26);
                            $style = [
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Simplified Arabic',
                                    'size' => 11,
                                    'bold' => true
                                ] ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )
                            ];

                            $sheet->getDefaultStyle()->applyFromArray($style);
                        });
                    })
                        ->store('xlsx', storage_path('tmp/'));

                    return response()->json([
                        'download_token' => $token,
                    ]);

                }

            }
            else if($type =='payments'){
//                $this->authorize('vouchersStatistic', \Common\Model\AidsCases::class);

                $total_amount=0;
                $total_count=0;
                $sub_result=[];

                foreach($categories as $kk=>$vv){
                    $sub_total_amount=0;
                    $sub_total_count=0;

                    $vv->set  = \DB::table('char_organizations')
                        ->where(function ($q) use ($user,$organization_id,$selectedSponsor,$selectedOrg,$UserOrgLevel,$selectedSponsorIds) {
                            $q->where('type', 2);
                            $q->whereNull('deleted_at');
                            $q->whereIn('id',$selectedSponsorIds);
                        })
                        ->selectRaw("char_organizations.id,                
                                 CASE  WHEN $language_id = 1 THEN char_organizations.name Else char_organizations.en_name END  AS name,

                        get_organization_payments_cases_category_count(char_organizations.id,'sponsors','$first','$last','$vv->payment_category_id', '$OrgJoin') as count,
                        COALESCE(get_organization_payments_cases_category_amount(char_organizations.id,'sponsors','$first','$last','$vv->payment_category_id' ,'$OrgJoin'),0) AS amount
")
                        ->orderBy('char_organizations.name')->get();

                    foreach($vv->set as $k=>$v){
                        $sub_total_amount = $sub_total_amount + (int)($v->amount);
                        $sub_total_count = $sub_total_count + (int)($v->count);
                    }

                    $vv->sub_total_count = $sub_total_count;
                    $vv->sub_total_amount = $sub_total_amount;


                    $total_amount=$total_amount +$sub_total_amount;
                    $total_count=$total_count +$sub_total_count;

                }

                $response=['status' =>true,'result' =>$categories, 'sponsors'=>$sponsors,'descendants'=>$Orgs, 'amount'=>$total_amount, 'count'=>$total_count];

                if($request->get('action') =='show'){
                    return response()->json($response);
                }
                else{

                    $first=date('d-m-Y',strtotime($request->get('first')));
                    $last=date('d-m-Y',strtotime($request->get('last')));

                    $token = md5(uniqid());
                    \Excel::create('export_' . $token, function($excel) use($response,$first,$last){
                        $excel->setTitle(trans('lang.excel_description_b'));
                        $excel->sheet(trans('sponsorship::application.statistic'), function($sheet) use($response,$first,$last){

                            $Index = [
                                'C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
                                'AA','AB','AC','AD','AE','AF','AG','AH','AI','AJ','AK','AL','AM','AN','AO','AP','AQ','AR','AS','AT','AU','AV','AW','AX','AY','AZ',
                                'BA','BB','BC','BD','BE','BF','BG','BH','BI','BJ','BK','BL','BM','BN','BO','BP','BQ','BR','BS','BT','BU','BV','BW','BX','BY','BZ',
                                'CA','CB','CC','CD','CE','CF','CG','CH','CI','CJ','CK','CL','CM','CN','CO','CP','CQ','CR','CS','CT','CU','CV','CW','CX','CY','CZ',
                                'DA','DB','DC','DD','DE','DF','DG','DH','DI','DJ','DK','DL','DM','DN','DO','DP','DQ','DR','DS','DT','DU','DV','DW','DX','DY','DZ',
                                'EA','EB','EC','ED','EE','EF','EG','EH','EI','EJ','EK','EL','EM','EN','EO','EP','EQ','ER','ES','ET','EU','EV','EW','EX','EY','EZ',
                                'FA','FB','FC','FD','FE','FF','FG','FH','FI','FJ','FK','FL','FM','FN','FO','FP','FQ','FR','FS','FT','FU','FV','FW','FX','FY','FZ',
                                'GA','GB','GC','GD','GE','GF','GG','GH','GI','GJ','GK','GL','GM','GN','GO','GP','GQ','GR','GS','GT','GU','GV','GW','GX','GY','GZ',
                                'HA','HB','HC','HD','HE','HF','HG','HH','HI','HJ','HK','HL','HM','HN','HO','HP','HQ','HR','HS','HT','HU','HV','HW','HX','HY','HZ',
                                'IA','IB','IC','ID','IE','IF','IG','IH','II','IJ','IK','IL','IM','IN','IO','IP','IQ','IR','IS','IT','IU','IV','IW','IX','IY','IZ',
                                'JA','JB','JC','JD','JE','JF','JG','JH','JI','JJ','JK','JL','JM','JN','JO','JP','JQ','JR','JS','JT','JU','JV','JW','JX','JY','JZ',
                                'KA','KB','KC','KD','KE','KF','KG','KH','KI','KJ','KK','KL','KM','KN','KO','KP','KQ','KR','KS','KT','KU','KV','KW','KX','KY','KZ',
                                'LA','LB','LC','LD','LE','LF','LG','LH','LI','LJ','LK','LL','LM','LN','LO','LP','LQ','LR','LS','LT','LU','LV','LW','LX','LY','LZ',
                                'MA','MB','MC','MD','ME','MF','MG','MH','MI','MJ','MK','ML','MM','MN','MO','MP','MQ','MR','MS','MT','MU','MV','MW','MX','MY','MZ',
                            ];

                            $sheet->setOrientation('landscape');
                            $sheet->setRightToLeft(true);

                            $i=sizeof($response['sponsors']);
                            $style_052 = array(
                               'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 15,
                                    'bold' => true
                                ] ,
                                'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )
                            );

                            $sheet->setCellValue('A1',trans('sponsorship::application.statistic'). '  ' .trans('sponsorship::application.range') . ' ( ' . $first . ' : ' .  $last .  ' ) ' );

                            $sheet->mergeCells('A1:'.$Index[($i*2)+1].'1');
                            $sheet->setHeight(array(1=> 45));
                            $sheet->getStyle('A1')->applyFromArray($style_052);

                            $sheet ->setCellValue('A2',trans('sponsorship::application.#'));
                            $sheet ->setCellValue('B2',trans('sponsorship::application.organizations_name'));
                            $sheet ->setCellValue('B3',trans('common::application.type'));
                            $sheet->mergeCells('A2:A3');

                            $sheet->setWidth(array('B'=> 25));
                            $style_20 = array(
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 10,
                                    'bold' => true
                                ],
                                'fill' => array(
                                    'type' => 'solid',
                                    'color' => array('rgb' => 'D3D3D3')
                                ) ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );
                            $sheet->getStyle('A2')->applyFromArray($style_20);
                            $sheet->getStyle('B2')->applyFromArray($style_20);
                            $sheet->getStyle('B3')->applyFromArray($style_20);

                            $sheet ->setCellValue($Index[$i*2].'2',trans('common::application.total'));
                            $sheet->mergeCells($Index[$i*2].'2:'.$Index[($i*2)+1].'2');

                            $sheet ->setCellValue($Index[$i*2].'3',trans('common::application.counts'));
                            $sheet ->setCellValue($Index[($i*2)+1].'3',trans('common::application.amount in shikel'));


                            $sheet->setWidth(array($Index[$i*2]=> 6,$Index[($i*2)+1]=> 6));

                            $style_320 = array(
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 11,
                                    'bold' => true
                                ],
                                'fill' => array(
                                    'type' => 'solid',
                                    'color' => array('rgb' => 'D3D3D3')
                                ) ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );
                            $style_2222 = array(
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 10,
                                    'bold' => true
                                ] ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );
                            $style_1 = array(
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 11,
                                    'bold' => true
                                ],
                                'fill' => array(
                                    'type' => 'solid',
                                    'color' => array('rgb' => 'D3D3D3')
                                ) ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );
                            $style_2 = array(
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 10,
                                    'bold' => true
                                ] ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );
                            $style_26 = array(
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Calibri',
                                    'size' => 11,
                                    'bold' => true,
                                ],
                                'fill' => array(
                                    'type' => 'solid',
                                    'color' => array('rgb' => 'D3D3D3')
                                ),
                                'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )

                            );

                            $sheet->getStyle($Index[$i*2].'2')->applyFromArray($style_320);
                            $sheet->getStyle($Index[$i*2].'3')->applyFromArray($style_2222);
                            $sheet->getStyle($Index[($i*2)+1].'3')->applyFromArray($style_2222);

                            $z=0;
                            foreach($response['sponsors'] as $k=>$v )
                            {
                                $sheet ->setCellValue($Index[$z].'2',$v->name);

                                $sheet->mergeCells($Index[$z].'2:'.$Index[$z+1].'2');
                                $sheet ->setCellValue($Index[$z].'3',trans('common::application.counts'));
                                $sheet ->setCellValue($Index[$z+1].'3',trans('common::application.amount in shikel'));

                                $sheet->setHeight(array(2=>65, 3 => 30));
                                $sheet->setWidth(array($Index[$z]=> 6, $Index[$z] => 6));
                                $sheet->getStyle($Index[$z].'2')->applyFromArray($style_1);
                                $sheet->getStyle($Index[$z].'3')->applyFromArray($style_2);
                                $sheet->getStyle($Index[$z+1].'3')->applyFromArray($style_2);


                                $z=$z+2;
                            }

                            $zz=4;
                            foreach($response['result'] as $key => $value)
                            {
                                $sheet ->setCellValue('A'.$zz,$key+1);
                                $sheet ->setCellValue('B'.$zz,$value['name']);

                                $z=0;
                                foreach($value['set'] as $k2=>$v2 )
                                {
                                    $sheet ->setCellValue($Index[$z].$zz,$v2->count);
                                    $sheet ->setCellValue($Index[$z+1].$zz,$v2->amount);

                                    $z=$z+2;
                                }

                                $sheet ->setCellValue($Index[$z].$zz,$value['sub_total_count']);
                                $sheet ->setCellValue($Index[$z+1].$zz,$value['sub_total_amount']);

                                $zz++;
                            }

                            $s= 4 +sizeof($response['result']);
                            $sheet ->setCellValue('A'.$s,trans('common::application.total'));
                            $sheet->mergeCells('A'.$s.':B'.$s);

                            $sheet->getStyle('A'.$s)->applyFromArray($style_26);


                            $z=0;
                            foreach($response['sponsors'] as $k=>$v )
                            {
                                $sheet ->setCellValue($Index[$z].$s,$v->count);
                                $sheet ->setCellValue($Index[$z+1].$s,$v->amount);

                                $sheet->setHeight(array(2=>65, 3 => 30));
                                $sheet->setWidth(array($Index[$z]=> 6, $Index[$z] => 6));
                                $sheet->getStyle($Index[$z].$s)->applyFromArray($style_26);
                                $sheet->getStyle($Index[$z+1].$s)->applyFromArray($style_26);
                                $z=$z+2;
                            }
                            $sheet ->setCellValue($Index[$z].$s,$response['count']);
                            $sheet ->setCellValue($Index[$z+1].$s,$response['amount']);
                            $sheet->getStyle($Index[$z].$s)->applyFromArray($style_26);
                            $sheet->getStyle($Index[$z+1].$s)->applyFromArray($style_26);
                            $style = [
                                'alignment' => ['horizontal'=>'center','vertical'=>'center','wrap'=>true],
                                'font' => [
                                    'name' => 'Simplified Arabic',
                                    'size' => 11,
                                    'bold' => true
                                ] ,'borders' => array(
                                    'allborders' => array(
                                        'style' => 'thin'
                                    )
                                )
                            ];

                            $sheet->getDefaultStyle()->applyFromArray($style);
                        });
                    })
                        ->store('xlsx', storage_path('tmp/'));

                    return response()->json([
                        'download_token' => $token,
                    ]);

                }

            }



            return response()->json($type);
        } catch (\Exception $e) {

        }
    }

}
